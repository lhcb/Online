#!/bin/bash
. /group/online/dataflow/scripts/preamble.sh
export NODENAME=`python -c "print '$HOST'.split('.')[0]"`
#
export ONLINETASKS=/group/online/dataflow/templates
export PARTITION=LHCb
#
export PARTITIONOPTS=/group/online/dataflow/options/${PARTITION}/${PARTITION}_Info.opts
export msg_svc=LHCb::FmcMessageSvc
export LOGFIFO=/tmp/logGaudi.fifo
export PYTHONPATH=/group/online/dataflow/cmtuser/Online_v4r43/MyOnline/python:/group/online/dataflow/options/${PARTITION}/STORAGE:$PYTHONPATH
export MINITERM='xterm  -ls -132 -geometry 132x12 -title '
export BIGTERM='xterm  -ls -132 -geometry 132x55 -title '
#
start_gaudi()
{
    $MINITERM ${1}@${HOST} -sl 30000  -e "export UTGID=${1};   exec -a \${UTGID} $GAUDIONLINEROOT/$CMTCONFIG/Gaudi.exe libGaudiOnline.so OnlineTask -tasktype=LHCb::Class1Task -msgsvc=$msg_svc -auto -main=$ONLINETASKS/options/Main.opts -opt=\"${2}\""&
}
#
#  Monitors:
#
$BIGTERM MBMMon@${HOST}     -e "export UTGID=${NODENAME}/MBMMon;    exec -a \${UTGID}  $GAUDIONLINEROOT/$CMTCONFIG/Gaudi.exe libOnlineKernel.so mbm_mon"&
#
start_gaudi  CALD0701_Merger  command="import Cald07_tests as t;t.runMerger()"
start_gaudi  CALD0701_Writer  ./CalWriter.opts
#
