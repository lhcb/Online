#!/bin/bash
#
#  Kill ing the default dataflow tasks
#
#  TANServ
#
#
#    TANServ  \
#
for task in  \
    TaskSupervisor \
    webDID \
    pingSrv_u \
    FileDeleter \
    ROcollect \
    ROpublish \
    LogPVSSSrv \
    LogPVSS \
    HLTEQBridge \
    \*_BusyMon  \
    PropertyServer \
    TorrentPublish;
do
    do_on_alldataflow tmKill -s 9 ${task};
done;
#
#  Special handling for the processes on ECS03
#
for task in \
    TANServ \
    webDID \
    FarmStatSrv_0 \
    FarmStatSrv_1 \
    Hlt_DeferSrv \
    Hlt_Hlt1Srv \
    HLTFileEqualizer \
    FarmStatusTrans \
    Hlt1Srv2fmc01 \
    Hlt_Hlt1DQSrv  \
    BootMonSrv_0 \
    ROBridge_0 \
    ROLogger_0 \
    PropertyServer \
    PropertyInfo \
    DidServer \
    ElogExtract \
    ElogPublish; 
do
    tmKill -N ecs03 -m ecs03 -s 9 ${task};
    tmKill -N ecs04 -m ecs04 -s 9 ${task};
done;
#
#
#
