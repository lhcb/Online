import OnlineEnv as Online

#------------------------------------------------------------------------------------------------
def _run(app,prt=True):                      return (app,Online.end_config(prt))
#------------------------------------------------------------------------------------------------
def runMerger():
  mepMgr   = Online.mepManager(Online.PartitionID,Online.PartitionName,['EVENT','SEND'],False)
  evtdata  = Online.evtDataSvc()
  evtPers  = Online.rawPersistencySvc()
  merger   = Online.evtMerger(buffer='SEND',name='Writer',location='/Event/DAQ/RawEvent',routing=0x800,datatype=Online.MDF_NONE)
  res      = Online.application('NONE',\
                                extsvc=[Online.monSvc(),\
                                        mepMgr,\
                                        Online.mbmSelector('EVENT')\
                                        ],\
                                runable=Online.evtRunable(mepMgr),algs=[merger])
  ###msgSvc().OutputLevel = 1
  return _run(res)
