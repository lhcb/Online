#!/bin/bash
if test -z "${1}"; then
  export ONLINE=`dirname $PWD`/OnlineDev_v6r15;
else 
  export ONLINE=`dirname $PWD`/OnlineDev_$1;
fi;
echo "OnlineDev satelite is at: $ONLINE";
CWD=`pwd`;
CMTUSER=`dirname $CWD`;
rm -f ONLINE;
rm -f  setup.x86_64-*;
rm -rf InstallArea;
rm -rf build.x86_64-*;
ln -s ${ONLINE} ONLINE;
export CMTPROJECTPATH=${CMTUSER}:$CMTPROJECTPATH;
#MYPLATFORMS=x86_64-centos7-gcc62-opt
#export ALL_CMT_PLATFORMS=MYPLATFORMS
for i in ${ALL_CMT_PLATFORMS}; do
    export USE_CMAKE=1;
    export CMTCONFIG=${i};
    export OnlineDev_DIR=`pwd`/ONLINE/InstallArea/${CMTCONFIG};
    make -j 5 install;
done;
source /group/online/dataflow/scripts/shell_macros.sh;
cmsetup;
