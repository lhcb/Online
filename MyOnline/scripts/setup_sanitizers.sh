#!/bin/bash
export LATEST=/cvmfs/lhcbdev.cern.ch/nightlies/lhcb-sanitizers/latest;
export CMTPROJECTPATH=${LATEST}:/cvmfs/lhcb.cern.ch/lib/lcg/app/releases:/cvmfs/lhcb.cern.ch/lib/lcg/external;
export CMAKE_MODULE_PATH=${CMTPROJECTPATH};
export CMAKE_PREFIX_PATH=${CMTPROJECTPATH};
export CMTCONFIG=x86_64_v2-centos7-gcc12-dbg+ubsan;

export BINARY_TAG=$CMTCONFIG;
export ALL_CMT_PLATFORMS="";

use_lsan()     { __use_comp v2-centos7 gcc12 dbg+lsan;    }
use_asan()     { __use_comp v2-centos7 gcc12 dbg+asan;    }
use_ubsan()    { __use_comp v2-centos7 gcc12 dbg+ubsan;   }
use_ulubsan()  { __use_comp v2-centos7 gcc12 dbg+alubsan; }

cmake_sanitizer()    {
    curr_online=`pwd`;
    lb-set-platform ${CMTCONFIG}
    export BINARY_TAG=$CMTCONFIG;
    export ALL_CMT_PLATFORMS="${CMTCONFIG}";
    export LHCb_DIR=${LATEST}/LHCb/InstallArea/${CMTCONFIG};
    export Gaudi_DIR=${LATEST}/Gaudi/InstallArea/${CMTCONFIG};
    export Detector_DIR=${LATEST}/Detector/InstallArea/${CMTCONFIG};
    cmake -S ${curr_online} -B ${curr_online}/build.${BINARY_TAG} -DCMAKE_TOOLCHAIN_FILE=/cvmfs/lhcb.cern.ch/lib/lhcb/lcg-toolchains/LCG_103/${BINARY_TAG}.cmake -GNinja;
}
