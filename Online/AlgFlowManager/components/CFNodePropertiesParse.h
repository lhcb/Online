/*****************************************************************************\
 * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

/// Framework include files
#include "CFNodeType.h"
#include <Gaudi/Parsers/Factory.h>

/// System include files
#include <string>
#include <tuple>
#include <vector>

BOOST_FUSION_ADAPT_STRUCT( Online::NodeDefinition,
                           ( std::string, name )( std::string, type )( std::vector<std::string>, children )( bool,
                                                                                                             ordered ) )

namespace Gaudi {
  namespace Parsers {
    template <typename Iterator, typename Skipper>
    struct NodeDefGrammar : qi::grammar<Iterator, Online::NodeDefinition(), Skipper> {
      typedef Online::NodeDefinition ResultT;
      NodeDefGrammar() : NodeDefGrammar::base_type( NodeDef_literal ) {
        NodeDef_literal = '(' >> gstring >> ',' >> gstring >> ',' >> gvector >> ',' >> gbool >> ')';
      }
      qi::rule<Iterator, ResultT(), Skipper>                     NodeDef_literal;
      StringGrammar<Iterator, Skipper>                           gstring;
      BoolGrammar<Iterator, Skipper>                             gbool;
      VectorGrammar<Iterator, std::vector<std::string>, Skipper> gvector;
    };
    REGISTER_GRAMMAR( Online::NodeDefinition, NodeDefGrammar );
  } // namespace Parsers
} // namespace Gaudi

namespace std {
  ostream& operator<<( ostream& s, Online::nodeType const& m );
  ostream& operator<<( ostream& s, Online::NodeDefinition const& m );
} // namespace std
