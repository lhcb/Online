//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//  ========================================================================
//
//  Author    : Markus Frank
//
//  ========================================================================
#ifndef ONLINE_GAUDIONLINE_IRAWEVENTCREATOR_H
#define ONLINE_GAUDIONLINE_IRAWEVENTCREATOR_H

/// Framework include files
#include <Gaudi/PluginService.h>
#include <GaudiKernel/StatusCode.h>

/// C++ include files
#include <string>
#include <vector>

class IDataProviderSvc;

/// Online namespace declaration
namespace Online  {

  class IRawEventCreator   {
  public:
    using Factory = Gaudi::PluginService::Factory<IRawEventCreator*()>;
  public:
    virtual ~IRawEventCreator() = default;
    virtual StatusCode put(IDataProviderSvc* svc, const std::string& location, const std::vector<void*>& banks) = 0;
    virtual StatusCode put(IDataProviderSvc* svc, const std::string& location, const unsigned char* start, const unsigned char* end) = 0;
  };
}
#endif // ONLINE_GAUDIONLINE_IRAWEVENTCREATOR_H
