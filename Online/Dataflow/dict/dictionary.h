//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  dictionary.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
// Disable some diagnostics for ROOT dictionaries
#ifdef __GNUC__
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#pragma GCC diagnostic ignored "-Wdeprecated"
#pragma GCC diagnostic ignored "-Wunused"
#endif

/// Framework include files
#include <Dataflow/Properties.h>

///  Online namespace declaration
namespace Online  {

  class DataflowComponent;

  int dataflow_python_task(std::vector<std::pair<std::string,std::string> >&& options);

  class PythonTask  {
  public:
    PythonTask() = default;
    ~PythonTask() = default;
    int run(std::vector<std::pair<std::string,std::string> >& options)   {
      return dataflow_python_task(std::move(options));
    }
  };

  struct PropertyResult  {
    std::string data;
    int status;
    PropertyResult() : status(0) {}
    PropertyResult(const std::string& d, int s) : data(d), status(s) {}
    PropertyResult(const PropertyResult& c) : data(c.data), status(c.status) {}
    ~PropertyResult() {}
  };
  class DataflowComponentProperties  {
  public:
    static PropertyResult getProperty(DataflowComponent* action, const std::string& name);
    static int setProperty(DataflowComponent* action, const std::string& name, const std::string& value);
  };
}

// -------------------------------------------------------------------------
// Regular dictionaries
// -------------------------------------------------------------------------
#if defined(__CINT__) || defined(__MAKECINT__) || defined(__CLING__) || defined(__ROOTCLING__)
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ namespace Online;

#pragma link C++ class Online::PythonTask;
#pragma link C++ class Online::PropertyResult;
#pragma link C++ class Online::DataflowComponentProperties;
#pragma link C++ function Online::dataflow_python_task;

#if 0
/// Framework include files
#include <Dataflow/ComponentContainer.h>
#include <Dataflow/DataflowContext.h>
#include <Dataflow/DataflowTask.h>
#include <Dataflow/MBMClient.h>
#include <Dataflow/Component.h>
#include <Dataflow/DataflowCatalog.h>
#include <Dataflow/OutputLogger.h>
#include <Dataflow/ComponentHandler.h>
#include <Dataflow/DataflowComponent.h>

#pragma link C++ class Online::DataflowComponent;
#pragma link C++ class Online::ComponentContainer;
#pragma link C++ class Online::DataflowManager;
#pragma link C++ class Online::DataflowContext;
#pragma link C++ class Online::DataflowContext::Manager;
#pragma link C++ class Online::DataflowContext::Logger;
#pragma link C++ class Online::DataflowContext::EventData;
#pragma link C++ class Online::DataflowContext::Incident;
#pragma link C++ class Online::DataflowContext::Options;
#pragma link C++ class Online::DataflowContext::Monitor;
#pragma link C++ class Online::DataflowTask;
#pragma link C++ class Online::DataflowIncident;
#pragma link C++ class Online::UnmanagedContainer;

/// Framework include files
#include <Dataflow/ControlPlug.h>
#include <Dataflow/Incidents.h>
#include <Dataflow/UnmanagedContainer.h>
#include <Dataflow/Receiver.h>
#include <Dataflow/Sender.h>
#include <Dataflow/NETSelector.h>

#pragma link C++ namespace Online::Control;
#pragma link C++ enum Online::Control::State;
#pragma link C++ enum Online::Control::SubState;
#pragma link C++ enum Online::Control::Transitions;
#pragma link C++ class Online::ComponentHandler;
#pragma link C++ class Online::ControlPlug;
#pragma link C++ class Online::MBMClient;
#pragma link C++ class Online::OutputLogger;
#pragma link C++ class Online::MBMClient;
#pragma link C++ class Online::Sender;
#pragma link C++ class Online::Receiver;
#pragma link C++ class Online::NETSelector;
#endif

#else

#include <Dataflow/DataflowComponent.h>

Online::PropertyResult
Online::DataflowComponentProperties::getProperty(Online::DataflowComponent* action,
						 const std::string& name)  {
  if ( action->hasProperty(name) )  {
    return PropertyResult(action->property(name).str(),1);
  }
  return PropertyResult("",0);
}
int Online::DataflowComponentProperties::setProperty(Online::DataflowComponent* action,
						     const std::string& name,
						     const std::string& value)  {
  if ( action->hasProperty(name) )  {
    action->property(name).str(value);
    return 1;
  }
  return 0;
}
#endif
