//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  CommandStructure.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_COMMANDSTRUCTURE_H
#define ONLINE_DATAFLOW_COMMANDSTRUCTURE_H

// Framework include files
#include <RTL/rtl.h>

// C/C++ include files
#include <cstring>

///  Online namespace declaration
namespace Online  {

  /// Class to transfer commands to target object(s)
  /**
   *   \author  M.Frank
   *   \version 1.0
   */
  class CommandStructure   {
  private:
    /// Default constructor
    CommandStructure() = default;
  public:
    class Descriptor  {
    public:
      int node   = 0;
      int pid    = 0;
      int cmd_id = 0;
      int cmd    = 0;
    };
    /// Default constructor
    static CommandStructure* allocate(int cmd, const char* data);
    /// Copy constructor
    CommandStructure(const CommandStructure& copy) = delete;
    /// Move constructor
    CommandStructure(CommandStructure&& copy) = delete;
    /// Default assignment operator
    CommandStructure operator=(const CommandStructure& copy) = delete;
    /// Destructor
    ~CommandStructure() {}
    const Descriptor& descriptor()  const  {  return *(Descriptor*)this; }
    Descriptor&       descriptor()         {  return *(Descriptor*)this; }
    const char* data()  const      
    {  const char* c = (const char*)this;   return c+sizeof(Descriptor); }
  };

  /// Default constructor
  CommandStructure* CommandStructure::allocate(int c, const char* d)  {
    static int node_hash = 0;//hash32(RTL::nodeNameShort());
    static int cmd_count = 1;
    size_t  len = ::strlen(d)+1;
    char*   buf = (char*)::operator new(len+sizeof(int));
    Descriptor* dsc = (Descriptor*)buf;
    dsc->cmd    = c;
    dsc->cmd_id = ++cmd_count;
    dsc->pid    = ::lib_rtl_pid();
    dsc->node   = node_hash;
    ::strncpy(buf+sizeof(Descriptor),d,len);
    return (CommandStructure*)buf;
  }
}

#endif //  ONLINE_DATAFLOW_COMMANDSTRUCTURE_H
