//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  ComponentContainer.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_COMPONENTCONTAINER_H
#define ONLINE_DATAFLOW_COMPONENTCONTAINER_H

// Framework include files
#include <Dataflow/DataflowComponent.h>

//  Online namespace declaration
namespace Online  {

  // Forward declarations
  class DataflowIncident;
  class ComponentHandler;

  /// ComponentContainer to group coherent action sequences
  /**
   *
   * @author  Markus Frank
   * @version 1.0
   */
  class ComponentContainer : public DataflowComponent  {
  protected:
    typedef std::vector<std::string>  ComponentNames;
    typedef std::vector<Component*>   Components;
    typedef int (ComponentHandler::*handler_func_t)(Component*) const;

    /// Embedded components
    Components      components;
    /// Names of embedded components
    ComponentNames  componentNames;

    /// Property: Mode AND - Require all components to succeed.
    bool            mode_AND = false;
    /// Property: Mode AND - Require all components to succeed.
    bool            mode_OR  = false;

    /// Execute transition action
    int action(const char* transition, handler_func_t pmf);

  public:
    /// Initializing constructor
    ComponentContainer(const std::string& name, Context& ctxt);
    /// Default destructor
    virtual ~ComponentContainer();
    /// Initialize the MBM server
    virtual int initialize()  override;
    /// Data processing overload: process event
    virtual int execute(const Context::EventData& event)  override;
  };
}      // end namespace Online
#endif //  ONLINE_DATAFLOW_COMPONENTCONTAINER_H
