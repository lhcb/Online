//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  ComponentFunctors.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_COMPONENTFUNCTORS_H
#define ONLINE_DATAFLOW_COMPONENTFUNCTORS_H

// Framework include files
#include <Dataflow/DataflowManager.h>
#include <Dataflow/DataflowComponent.h>

///  Online namespace declaration
namespace Online  {

  /// Select dataflow component by name match
  class SelectComponentByName : public DataflowManager::ComponentManip  {
  public:
    /// Reference to the selected component object
    Component*    selected = 0;
    /// Required component name
    std::string   name;
    /// required component type
    unsigned long type = DataflowContext::Manager::ALL;
  public:
    /// Default constructor
    SelectComponentByName() = delete;
    /// Default copy constructor
    SelectComponentByName(const SelectComponentByName&) = delete;
    /// Default move constructor
    SelectComponentByName(const SelectComponentByName&&) = delete;
    /// Assignment operator
    SelectComponentByName& operator=(const SelectComponentByName&) = delete;
    /// Initializing constructor
    SelectComponentByName(const std::string& nam, unsigned long type=DataflowContext::Manager::ALL);
    /// Default destructor
    virtual ~SelectComponentByName() = default;
    /// Callback function. When component is selected, return negative value
    virtual long operator()(int typ, Component* component)  override; 
  };

  /// Initializing constructor
  inline SelectComponentByName::SelectComponentByName(const std::string& nam, unsigned long typ)
    : name(nam), type(typ)
  {
  }

  /// Base class to collect components according to criteria
  template <typename T, typename CRITERIA> 
    class SelectComponents : public DataflowManager::ComponentManip  {
  public:
    /// Reference to the selected components
    T&              selected;
    /// Required component criteria
    const CRITERIA& criteria;
  public:
    /// Default constructor
    SelectComponents() = delete;
    /// Default copy constructor
    SelectComponents(const SelectComponents&) = default;
    /// Default move constructor
    SelectComponents(SelectComponents&&) = default;
    /// Assignment operator
    SelectComponents& operator=(const SelectComponents&) = default;
    /// Initializing constructor
    SelectComponents(T& buffer, const CRITERIA& criteria);
    /// Default destructor
    virtual ~SelectComponents() = default;
    /// Callback function. When component is selected, return negative value
    virtual long operator()(int typ, Component* component)  override    {
      if ( criteria(typ, component) ) selected.insert(selected.end(),component);
      return 1;
    }
  };

  /// Initializing constructor
  template <typename T, typename CRITERIA> 
    inline SelectComponents<T,CRITERIA>::SelectComponents(T& b, const CRITERIA& c)
    : selected(b), criteria(c)
  {
  }

  /// Criterium selecting everything
  struct DefaultCriterium {
    bool operator()(int, DataflowComponent*) const  {  return true; }
  };

  template <typename T, typename C> 
    inline SelectComponents<T,C> selectComponent(T& b, const C& c)   {
    return SelectComponents<T,C>(b,c);
  }
}      //  End namespace Online
#endif //  ONLINE_DATAFLOW_COMPONENTFUNCTORS_H
