//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  ComponentHandler.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_COMPONENTHANDLER_H
#define ONLINE_DATAFLOW_COMPONENTHANDLER_H

// Framework include files
#include <Dataflow/DataflowContext.h>

///  Online namespace declaration
namespace Online {

  class DataflowComponent;
  class DataflowContext;

  /// Helper to handle component transitions
  /** @class ComponentHandler ComponentHandler.h Dataflow/ComponentHandler.h
   *
   * @author  Markus Frank
   * @version 1.0
   */
  class ComponentHandler {
  public:

    typedef DataflowComponent Component;
    typedef DataflowContext Context;

  public:
    /// Context reference
    Context& context;
    /// Initializing constructor
    ComponentHandler(Context& ctxt) : context(ctxt) {}
    /// Construct the component
    DataflowComponent* construct(const std::string& name_type) const;
    /// Initialize the data flow component. 
    int initialize(Component* c) const;
    /// Finalize the data flow component. 
    int finalize(Component* c) const;
    /// Start the data flow component. 
    int start(Component* c) const;
    /// Stop the data flow component. 
    int stop(Component* c) const;
    /// Destruct the data flow component (call destructor). 
    int destruct(Component* c) const;
    /// Pause the data flow component. 
    int pause(Component* c) const;
    /// Continuing the data flow component. 
    int continuing(Component* c) const;
    /// Execute the event loop
    int execute(Component* c, const Context::EventData& event)  const;
    /// Cancel the data flow component. 
    int cancel(Component* c) const;
    /// Enable the data flow component. 
    int enable(Component* c) const;
    /// Throw a formatted exception in the event of a failure
    int fail(Component* c, const char* msg, ...) const;
    /// Set component properties
    int setProperties(Component* c) const;
  };
}      // end namespace Online
#endif //  ONLINE_DATAFLOW_COMPONENTHANDLER_H
