//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  ControlPlug.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_CONTROLPLUG_H
#define ONLINE_DATAFLOW_CONTROLPLUG_H

// Framework include files
#include <Dataflow/DataflowComponent.h>
#include <CPP/Interactor.h>

// Forward declarations

///  Online namespace declaration
namespace Online  {

  /// Control namespace for dataflow tasks
  namespace Control  {
    
    enum Transitions { 
      CONFIGURE=1, 
      INITIALIZE, 
      START,
      ACTIVATE,
      ENABLE, 
      DISABLE, 
      PRESTOP, 
      STOP,
      FINALIZE,
      TERMINATE,
      UNLOAD, 
      PAUSE,
      CONTINUE,
      ERROR,
      CANCEL=900,
      CONNECT_DIM=1000,
      FIRE_INCIDENT=1100
    };
    enum State  {
      ST_OFFLINE   = 'U',
      ST_NOT_READY = 'N',
      ST_READY     = 'r',
      ST_ACTIVE    = 'A',
      ST_RUNNING   = 'R',
      ST_STOPPED   = 'S',
      ST_PAUSED    = 'P',
      ST_ERROR     = 'E',
      TR_ERROR     = ERROR
    };
    enum SubState  {
      SUCCESS_ACTION = 'S',
      EXEC_ACTION    = 'E',
      FAILED_ACTION  = 'F',
      UNKNOWN_ACTION = 'U'
    };
  }

  /// DIM command target for dataflow applications
  /** @class ControlPlug ControlPlug.h Dataflow/ControlPlug.h
   *
   *
   *  \author  Markus Frank
   *  \version 1.0
   *  \ingroup DATAFLOW
   */
  class ControlPlug : public DataflowComponent  {
  public:

    /// Monitoring structure for farm status displays
    /** @class ControlPlug::Monitor ControlPlug.h Dataflow/ControlPlug.h
     *  \author  Markus Frank
     *  \version 1.0
     *  \ingroup DATAFLOW
     */
    struct Monitor {
      unsigned long lastCmd, doneCmd;
      int   pid;
      char  targetState, state, metaState, pad;
      int   partitionID;
    };


  protected:
    /// ROcollect monitoring structure
    Monitor     m_monitor;
    /// Name of the current state
    std::string m_stateName;
    /// Name of the previous state
    std::string m_prevStateName;

    /// Send IOC message to target interactor
    void send(int cmd, void* data=0);
  public:
    /// Initializing constructor
    ControlPlug(const std::string& name, Context& context);
    /// Default destructor
    virtual ~ControlPlug();
    /// Access to state name
    const std::string& stateName() const  { return m_stateName; }
    /// (Re)connect DIM services
    virtual int connect() = 0;
    /// Disconnect DIM services
    virtual int disconnect() = 0;
    /// Publish the current state
    virtual int publishState() = 0;
    /// Publish the monitor information
    virtual int publishMonitor() = 0;

    /// Translate integer state to string name
    static std::string stateName(int state);
    /// Declare FSM state
    virtual int _declareState(const std::string& new_state);
    /// Declare FSM state
    virtual int declareState(Control::State state);
    /// Declare FSM sub-state
    virtual int declareSubState(Control::SubState state);
    /// Declare the target state of the transition
    virtual int declareTargetState();
    /// Set transition target state
    virtual int setTargetState(Control::State target);
    /// Access the current object state
    Control::State currentState() const;
    /// Access the object's target state
    Control::State targetState() const;
    /// Invoke transtion to target state or other commands
    virtual int invoke(const std::string& cmd);
  };
}      // end namespace Online
#endif //  ONLINE_DATAFLOW_CONTROLPLUG_H
