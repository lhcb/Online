//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  Printout.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_DATACOMPRESSION_H
#define ONLINE_DATAFLOW_DATACOMPRESSION_H

/// Framework include files
#include <Dataflow/DataflowContext.h>

/// C/C++ include files
#include <cstdint>

///  Online namespace declaration
namespace Online  {

  ///  DataCompression namespace declaration
  namespace DataCompression  {

    /*
      The algorithm applied is the ROOT compression mechanism.
      ROOT provides 4 different algorithm types:
      - ZLIB
      - ZSTD
      - LZMA
      - LZ4

      Option "alglevel" is used to specify the compression level:
      compress = 0 objects written to this file will not be compressed.
      compress = 1 minimal compression level but fast.
      ....
      compress = 9 maximal compression level but slow.
    */

    /// Flags to select data compression algorithm
    enum compression_alg_t  {
      ZLIB = 1,    //  == RCompressionSetting::EAlgorithm::kZLIB
      LZMA = 2,    //  == RCompressionSetting::EAlgorithm::kLZMA
      LZ4  = 4,    //  == RCompressionSetting::EAlgorithm::kLZ4
      ZSTD = 5,    //  == RCompressionSetting::EAlgorithm::kZSTD
      NONE = 999,  //
    };

    /// Data compression name
    std::string compressionType(compression_alg_t algtype);

    /// Compress data buffer
    DataflowStatus compress(  compression_alg_t algtype, int level,
                              uint8_t* tar,       std::size_t tar_len,
                              const uint8_t* src, std::size_t src_len,
                              std::size_t& new_len);

    /// Decompress data buffer
    DataflowStatus decompress(int level,
                              uint8_t* tar,       std::size_t tar_len,
                              const uint8_t* src, std::size_t src_len,
                              std::size_t& new_len);

  }    // end namespace DataCompression
}      // end namespace Online
#endif //  ONLINE_DATAFLOW_DATACOMPRESSION_H
