//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  DataReceiver.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_DATARECEIVER_H
#define ONLINE_DATAFLOW_DATARECEIVER_H


#ifndef TRANSFER_NS
#error This header file is not intended for user inclusion, but only to instantiate component factories!
#endif

// Framework include files
#include <Dataflow/Receiver.h>
#include <WT/wt_facilities.h>
#include <RTL/rtl.h>

// C/C++ include files
#include <mutex>
#include <cstring>

///  Online namespace declaration
namespace Online  {

  int dataflow_task_lock();
  int dataflow_task_trylock();
  int dataflow_task_unlock();

  using namespace TRANSFER_NS;

  /// Implementation of a generic network DataReceiver component
  /**
   *  Standard component template to implment DataReceiver component objects.
   *  The template is meant to be used by various DataTransfer
   *  implementations such as
   *
   *  - NET/DataTransfer
   *
   *  - ASIO/Transfer
   *
   *  - ZMQTransfer/Transfer
   *
   *  All these implementations have the same interface, which
   *  allows to transfer data between nodes.
   *
   *  @author Markus Frank
   */
  class DataReceiver : public Receiver  {

    typedef DataReceiver self_t;
    typedef std::lock_guard<std::mutex> LOCK;

  protected:
    std::mutex m_lock       { };
    /// Pointer to netplug device
    NET*       m_netPlug    {nullptr};
    /// Property: Number of thread instances used by the network library
    int        m_nThreads   {2};
    /// Property: Direct MBM execution, no asychronous declaration, no intermediate buffer
    int        m_mbm_direct {true};
    /// Flag
    bool       m_finish     {false};

    static void handle_death(const netheader_t& hdr, void* par, netentry_t*)  {
      self_t* p = reinterpret_cast<self_t*>(par);
      LOCK    lock(p->m_lock);
      int     sc = dataflow_task_trylock();
      if ( sc == 1 ) {
	p->taskDead(hdr.name);
        dataflow_task_unlock();
      }
    }

    static void handle_request(const netheader_t& hdr, void* par, netentry_t* entry)  {
      self_t* ptr = reinterpret_cast<self_t*>(par);
      int     sc = net_receive(ptr->m_netPlug, entry, nullptr, 0);
      if ( sc == NET_SUCCESS )  {
	LOCK lock(ptr->m_lock);
	sc = dataflow_task_trylock();
	if ( sc == 1 ) {
	  std::string source(hdr.name);
	  ptr->handleRequest(ptr->receivers().size(), source,source);
	  dataflow_task_unlock();
	  return;
	}
	// Things go awfully wrong....e.g. finalize was called during data taking.
	::fprintf(stdout,"Loosing event request....was finalize called ?\n");
	::fflush(stdout);
      }
    }

    static void handle_event(const netheader_t& hdr, void* param, netentry_t* entry)  {
      self_t* rcv = reinterpret_cast<self_t*>(param);
      std::string source(hdr.name);
      const auto* ent = rcv->receiver(source);
      if ( !ent ) {
	// In case the sender did not send the source request, add it on the fly
	auto ret = rcv->handleRequest(rcv->receivers().size(), source, source);
	if ( ret.first == DF_SUCCESS )   {
	  ent = ret.second;
	}
      }
      if ( !ent ) {
	::fprintf(stdout,"Loosing EVENT....Cannot register data source:%s.\n",hdr.name);
	::fflush(stdout);
	return;
      }
      try   {
	dataflow_task_trylock();
	std::unique_ptr<char []> buffer;
	if ( !rcv->m_mbm_direct )    {
	  // Get a bit more buffer space. We have to debug this memory overwrite!
	  buffer.reset(new char[hdr.size+128]);
	  int status = net_receive(rcv->m_netPlug, entry, buffer.get(), hdr.size);
	  if ( status != NET_SUCCESS )  {
	    ::fprintf(stdout,"Receiver: FAILED to receive data load from %s",hdr.name);
	    ::fflush(stdout);
	    dataflow_task_unlock();
	    return;
	  }
	}
	LOCK lock(rcv->m_lock);
	if ( DF_SUCCESS != rcv->handleData(*ent, entry, std::move(buffer), hdr.size) )   {
	}
      }
      catch(...)   {
      }
      dataflow_task_unlock();
    }

  public:

    /// Standard algorithm constructor
  DataReceiver(const std::string& nam, DataflowContext& ctxt) : Receiver(nam, ctxt)
      {
	declareProperty("NumThreads", m_nThreads = 2);
	declareProperty("DirectMBM",  m_mbm_direct = true);
      }

    /// Standard Destructor
    virtual ~DataReceiver()   { }

    /// Subscribe to network requests
    virtual int subscribeNetwork()  override  {
      std::string self = RTL::processName();
      m_finish = false;
      m_netPlug = net_init(self, m_nThreads, NET_SERVER);
      net_subscribe(m_netPlug, this, WT_FACILITY_CBMEVENT, handle_event, handle_death);
      net_subscribe(m_netPlug, this, WT_FACILITY_CBMCONNECT, handle_request, handle_death);
      return DF_SUCCESS;
    }

    /// Unsubscribe from network requests
    virtual int unsubscribeNetwork()  override  {
      m_finish = true;
      if ( m_netPlug )  {
	net_unsubscribe(m_netPlug, this, WT_FACILITY_CBMEVENT);
	net_unsubscribe(m_netPlug, this, WT_FACILITY_CBMCONNECT);
	net_close(m_netPlug);
	m_netPlug = 0;
      }
      return DF_SUCCESS;
    }

    /// Rearm network request for a single source
    virtual int rearmNetRequest(const RecvEntry& src)  override  {
      int sc = net_send(m_netPlug, "EVENT_REQUEST", 14, src.service, WT_FACILITY_CBMREQEVENT);
      return (sc == NET_SUCCESS) ? DF_SUCCESS : DF_ERROR;
    }

    /// Copy event data into buffer manager
    virtual int copyEventData(void* ctxt, void* to, const std::unique_ptr<char []>& from, size_t len)  override  {
      if ( m_mbm_direct )    {
	netentry_t* entry = (netentry_t*)ctxt;
	int status = net_receive(m_netPlug, entry, to, len);
	if ( status != NET_SUCCESS )  {
	  error("FAILED to receive data load of size %ld", len);
	  return DF_ERROR;
	}
      }
      else  {
        ::memcpy(to, from.get(), len);
      }
      return DF_SUCCESS;
    }

    /// Networking layer overload [Net producer+consumer]: Cancel current I/O
    virtual int cancelNetwork()  override   {
      net_cancel(m_netPlug);
      return DF_SUCCESS;
    }
  };
}      // end namespace Online

#include <Dataflow/Plugins.h>
DECLARE_DATAFLOW_COMPONENT_NS(Online,DataReceiver)

#endif //  ONLINE_DATAFLOW_DATARECEIVER_H
