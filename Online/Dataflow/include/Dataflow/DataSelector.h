//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  DATASelector.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_DATASELECTOR_H
#define ONLINE_DATAFLOW_DATASELECTOR_H

#ifndef TRANSFER_NS
#error This header file is not intended for user inclusion, but only to instantiate component factories!
#endif

// Framework include files
#include <Dataflow/NETSelector.h>

///  Online namespace declaration
namespace Online  {

  /// Buffer Manager service for the dataflow
  /**
   * @author  Markus Frank
   * @version 1.0
   */
  class DataSelector : public NETSelector  {
  protected:
    typedef TRANSFER_NS::NET NET;
    /// NET Plugin reference
    NET* m_netPlug = nullptr;

  public:
    /// Start datawork activity
    virtual int net_start()  override;
    /// Rearm network event receiving
    virtual int net_rearm()  override;
    /// Shutdown network event receiving
    virtual int net_shutdown()  override;

  public:
    /// Initializing constructor
    DataSelector(const std::string& name, Context& ctxt);
    /// Default destructor
    virtual ~DataSelector();
    /// Access to the data plug
    NET* plug() const  {  return m_netPlug; }
  };
}      // end namespace Online
#endif //  ONLINE_DATAFLOW_DATASELECTOR_H

/// Framework includes
//  #include <Dataflow/DataSelector.h>
#include <Dataflow/Plugins.h>
#include <WT/wtdef.h>

using namespace Online;
using namespace TRANSFER_NS;

// Instantiation of a static factory class used by clients to create instances of this service
DECLARE_DATAFLOW_COMPONENT_NS(Online,DataSelector)


namespace data_selector_helpers  {
  static void handle_death(const netheader_t& h, void* param, netentry_t*) {
    DataSelector* p = (DataSelector*)param;
    p->taskDead(h.name);
  }

  static void handle_event(const netheader_t& header, void* param, netentry_t* entry)  {
    DataSelector* p = (DataSelector*)param;
    char* buff = new char[header.size];
    int sc = net_receive(p->plug(), entry, buff, header.size);
    (sc == NET_SUCCESS) ? p->handleData(header.name,header.size,buff) : delete [] buff;
  }
}

/// Initializing constructor
DataSelector::DataSelector(const std::string& nam, Context& ctxt)
: NETSelector(nam, ctxt), m_netPlug(nullptr)
{
}

/// Default destructor
DataSelector::~DataSelector()   {
}

/// Start network activity
int DataSelector::net_start()  {
  if ( !m_netPlug )  {
    m_netPlug = net_init(RTL::processName(), m_numThreads);
  }
  return net_subscribe(m_netPlug, this,
		       WT_FACILITY_CBMEVENT,
		       data_selector_helpers::handle_event,
		       data_selector_helpers::handle_death);
}

/// Rearm network event receiving
int DataSelector::net_rearm()  {
  if ( m_netPlug )  {
    int sc = net_send(m_netPlug,&m_request,sizeof(m_request),m_input,WT_FACILITY_CBMREQEVENT);
    return sc == NET_SUCCESS ? DF_SUCCESS : DF_ERROR;
  }
  return DF_ERROR;
}

/// Shutdown network event receiving
int DataSelector::net_shutdown()  {
  if ( m_netPlug )  {
    net_unsubscribe(m_netPlug,this,WT_FACILITY_CBMEVENT);
    net_close(m_netPlug);
    m_netPlug = 0;
  }
  return DF_SUCCESS;
}
