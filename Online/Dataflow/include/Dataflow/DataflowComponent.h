//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  DataflowComponent.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_DATAFLOWCOMPONENT_H
#define ONLINE_DATAFLOW_DATAFLOWCOMPONENT_H

// Framework include files
#include <Dataflow/Properties.h>
#include <Dataflow/DataflowContext.h>

// C/C++ include files
#include <array>
#include <cstdarg>

///  Online namespace declaration
namespace Online  {

  /// Forward declarations
  class DataflowIncident;
  class DataflowContext;

  /// Base class for all components interacting with the dataflow in LHCb
  /** @class DataflowComponent DataflowComponent.h Dataflow/DataflowComponent.h
   *
   * Generic dataflow component, which may take over service work or algorithmic
   * work. The components are managed by the DataflowManager instance.
   * The generic stae machine is as follows:
   *
   *
   *  construct         |    A         destruct
   *                    V    |
   *                    Alive
   *  initialize()      |    A         finalize()
   *                    V    |
   *                    Initialized
   *  start()           |    A         stop()
   *                    V    |
   *                    Running
   *  pause()           |    A         continuing()
   *                    V    |
   *                    Paused
   *
   * All components execute at the same time all transitions.
   *
   * -- Output logging:
   *    for logging levels please see the DD4hep file Printout.h
   *
   * @author  Markus Frank
   * @version 1.0
   */
  class DataflowComponent  {
  public:
    friend class JobOptions;
  public:
    /// Component property manager
    PropertyManager  properties;

  public:
    typedef DataflowComponent Component;
    typedef DataflowContext   Context;

    /// Name of the component
    std::string name;
    /// Reference to the dataflow context
    Context&    context;
    /// Property: Output level
    int         outputLevel;
    /// Property: Generic user defined event flag. Bits>10 are for users
    int         processingFlag;
    
  protected:
    /// Printout forward to message handler component
    int _print(int severity, const std::string& nam, const char* fmt, va_list& args) const;

  public:
    /// Output helper: yes or no
    inline const char* yes_no(bool value)      {  return value ? "YES"  : "NO ";    }
    /// Output helper: true or false
    inline const char* true_false(bool value)  {  return value ? "true" : "false "; }

    /// Create (and register) a new component
    static DataflowComponent* create(const std::string& name_type_id, Context& ctxt);

    /// Initializing constructor
    DataflowComponent(const std::string& name, Context& ctxt);
    /// Default destructor
    virtual ~DataflowComponent();
    /// Query internal interface to the component
    template <typename Q> Q* query()  { return dynamic_cast<Q*>(this);  }

    /// Handy for printouts...
    const char* cname() const         { return name.c_str();            }
    /** Basic interface for component configuration             */
    /// Initialize the data flow component. Default implementation is empty.
    virtual int initialize();
    /// Start the data flow component. Default implementation is empty.
    virtual int start();
    /// Stop the data flow component. Default implementation is empty.
    virtual int stop();
    /// Finalize the data flow component. Default implementation is empty.
    virtual int finalize();
    /// Continuing the data flow component. Default implementation is empty.
    virtual int continuing();
    /// Pause the data flow component. Default implementation is empty.
    virtual int pause();
    /// Execute runable. Default implementation throws an exception.
    virtual int run();
    /// Execute event. Default implementation is empty.
    virtual int execute(const Context::EventData& event);
    /// Cancel the data flow component. 
    virtual int cancel();
    /// Enable the data flow component. 
    virtual int enable();

    /** Property handling                                       */
    /// Declare property
    template <typename T> Component& declareProperty(const std::string& nam, T& val);
    /// Declare property
    template <typename T> Component& declareProperty(const char* nam, T& val);

    /// Check property for existence
    bool hasProperty(const std::string& name) const;
    /// Access single property
    Property& property(const std::string& name);
    /// Set my own properties from the context
    int setProperties();
    /// Dump the properties of the component
    void printProperties();

    /** Monitoring handling                                     */
    /// Declare monitoring item to the monitoring component: scalar item
    template <typename T> 
    const Component& declareMonitor(const char*        owner,
                                    const char*        nam,
                                    const T&           val,
                                    const std::string& desc="") const;

    /// Declare monitoring item to the monitoring component: scalar item (owner=self)
    template <typename T>
    const Component& declareMonitor(const std::string& owner,
                                    const std::string& nam,
                                    const T&           val,
                                    const std::string& desc="") const;

    /// Declare monitoring item to the monitoring component (owner=self): scalar item
    template <typename T>
    const Component& declareMonitor(const char*        nam,
                                    const T&           val,
                                    const std::string& desc="") const;

    /// Declare monitoring item to the monitoring component (owner=self): scalar item
    template <typename T> 
    const Component& declareMonitor(const std::string& nam,
                                    const T&           val,
                                    const std::string& desc="") const;

    /// Declare monitoring item to the monitoring component: scalar item
    template <typename T, std::size_t length> 
    const Component& declareMonitorArray(const char*        owner,
                                         const char*        nam,
                                         const std::array<T,length>& val,
                                         const std::string& desc="") const;

    /// Declare monitoring item to the monitoring component: scalar item (owner=self)
    template <typename T, std::size_t length>
    const Component& declareMonitorArray(const std::string& owner,
                                         const std::string& nam,
                                         const std::array<T,length>& val,
                                         const std::string& desc="") const;

    /// Declare monitoring item to the monitoring component (owner=self): scalar item
    template <typename T, std::size_t length>
    const Component& declareMonitorArray(const char*        nam,
                                         const std::array<T,length>& val,
                                         const std::string& desc="") const;

    /// Declare monitoring item to the monitoring component (owner=self): scalar item
    template <typename T, std::size_t length> 
    const Component& declareMonitorArray(const std::string& nam,
                                         const std::array<T,length>& val,
                                         const std::string& desc="") const;

    /// Declare pointer/array monitoring item with size in bytes to the monitoring component (owner=self)
    template <typename T>
    const Component& declareMonitor(const char*        owner,
                                    const char*        nam,
                                    const T*           val,
                                    std::size_t        bytes,
                                    const std::string& desc="") const;
    
    /// Declare pointer/array monitoring item to the monitoring component with size in bytes (owner=self)
    template <typename T>
    const Component& declareMonitor(const std::string& owner,
                                    const std::string& nam,
                                    const T*           val,
                                    std::size_t        bytes,
                                    const std::string& desc="") const;

    /// Declare pointer monitoring item to the monitoring component with size in bytes (owner=self)
    template <typename T>
    const Component& declareMonitor(const char*        nam,
                                    const T*           val,
                                    std::size_t        bytes,
                                    const std::string& desc="") const;

    /// Declare monitoring item to the monitoring component
    template <typename T> 
    const Component& declareMonitor(const std::string& nam,
                                    const T*           val,
                                    std::size_t        bytes,
                                    const std::string& desc) const;

    /// Declare pointer monitoring item with format and size to the monitoring component
    template <typename T>
    const Component& declareMonitor(const char*        owner,
                                    const char*        nam,
                                    const T*           val,
                                    std::size_t        bytes,
                                    const std::string& format,
                                    const std::string& desc) const;
    
    /// Declare pointer/array monitoring item with format and size to the monitoring component
    template <typename T>
    const Component& declareMonitor(const std::string& owner,
                                    const std::string& nam,
                                    const T*           val,
                                    std::size_t        bytes,
                                    const std::string& format,
                                    const std::string& desc) const;

    /// Declare pointer/structure monitoring item with format and size to the monitoring component (owner=self)
    template <typename T>
    const Component& declareMonitor(const char*        nam,
                                    const T*           val,
                                    std::size_t        bytes,
                                    const std::string& format,
                                    const std::string& desc) const;
    
    /// Declare pointer/structure monitoring item with size and format to the monitoring component (owner=self)
    template <typename T>
    const Component& declareMonitor(const std::string& nam,
                                    const T*           val,
                                    std::size_t        bytes,
                                    const std::string& format,
                                    const std::string& desc) const;

    /// Declare monitoring item:  Pairs of int/long
    template <typename T>
    const Component& declareMonitor(const char*        owner,
                                    const char*        nam,
                                    const T&           val1,
                                    const T&           val2,
                                    const std::string& desc) const;

    /// Declare monitoring item:  Pairs of int/long
    template <typename T>
    const Component& declareMonitor(const std::string& owner,
                                    const std::string& nam,
                                    const T&           val1,
                                    const T&           val2,
                                    const std::string& desc) const;

    /// Declare monitoring item:  Pairs of int/long
    template <typename T>
    const Component& declareMonitor(const char*        nam,
                                    const T&           val1,
                                    const T&           val2,
                                    const std::string& desc) const;

    /// Declare monitoring item:  Pairs of int/long
    template <typename T>
    const Component& declareMonitor(const std::string& nam,
                                    const T&           val1,
                                    const T&           val2,
                                    const std::string& desc) const;

   
    /// Undeclare monitoring information item
    void undeclareMonitor(const std::string& owner, const std::string& nam) const;
    /// Undeclare monitoring information item
    void undeclareMonitor(const std::string& nam) const;
    /// Undeclare monitoring information
    void undeclareMonitors(const std::string& owner) const;
    /// Undeclare monitoring information
    void undeclareMonitors() const;

    /** Incident handling interface                             */
    /// Incident handler interface: Incident-subscription for this component 
    int subscribeIncident(const std::string& incident_name);
    /// Incident handler interface: un-subscribe this component from incident
    int unsubscribeIncident(const std::string& incident_name);
    /// Incident handler interface: un-subscribe this component from all incident
    int unsubscribeIncidents();
    /// Incident handler interface: Invoke incident
    int fireIncident(const DataflowIncident& incident);
    /// Incident handler interface: Invoke incident
    int fireIncident(const std::string& type);
    /// Incident handler callback: Inform that a new incident has occured
    virtual void handle(const DataflowIncident& incident);

    /** Output logging helpers.                                 */
    /// Generic output logging
    void output(int level, const char* msg,...)   const;

    /// Always printout handling
    void always(const char* msg, ...) const;
    /// Always printout handling
    void always(const std::string& msg) const    {  always(msg.c_str());   }

    /// Debug printout handling
    void debug(const char* msg, ...) const;
    /// Debug printout handling
    void debug(const std::string& msg) const    {  debug(msg.c_str());   }
    /// Info printout handling
    void info(const char* msg, ...) const;
    /// Info printout handling
    void info(const std::string& msg) const     {  info(msg.c_str());    }
    /// Warning printout handling
    void warning(const char* msg, ...) const;
    /// Warning printout handling
    void warning(const std::string& msg) const  {  warning(msg.c_str()); }
    /// Error handling. Returns error code
    int error(const std::string& msg) const     {  return error(msg.c_str());   }
    /// Error handling. Returns error code
    int error(const char* msg, ...) const;
    /// Error handling: print exception. Technically returns error code.
    int error(const std::exception& e, const char* msg, ...) const;
    /// Error handling: print exception. Technically returns error code.
    int error(const std::exception& e, const std::string& msg) const;
    /// Error handling: print exception. Technically returns error code.
    //int error(const std::exception& e, const std::string& msg, ...) const;
    /// Warning printout handling
    int fatal(const char* msg, ...) const;
    /// Warning printout handling
    int fatal(const std::string& msg) const     {  return fatal(msg.c_str());   }
    /// Error handling. Technically returns error code. Throws internally exception
    int throwError(const std::string& msg) const;
    /// Error handling. Technically returns error code. Throws internally exception
    int throwError(const char* msg, ...) const;
#if !defined(__ROOTCLING__)
    /// Always printout handling
    void always(const char* msg, va_list& args) const;
    /// Debug printout handling
    void debug(const char* msg, va_list& args) const;
    /// Info printout handling
    void info(const char* msg, va_list& args) const;
    /// Warning printout handling
    void warning(const char* msg, va_list& args) const;
    /// Error handling. Returns error code
    int error(const char* msg, va_list& args) const;
    /// Warning printout handling
    int fatal(const char* msg, va_list& args) const;
    /// Error handling. Technically returns error code. Throws internally exception
    int throwError(const char* msg, va_list& args) const;
#endif
  };

  /// Declare property
  template <typename T> inline DataflowComponent& 
  DataflowComponent::declareProperty(const std::string& nam, T& val)     {
    this->properties.add(nam, val);
    return *this;
  }

  /// Declare property
  template <typename T> inline DataflowComponent& 
  DataflowComponent::declareProperty(const char* nam, T& val)     {
    this->properties.add(nam, val);
    return *this;
  }
  
  /// Declare monitoring item to the monitoring component: scalar item
  template <typename T> inline const DataflowComponent& 
  DataflowComponent::declareMonitor(const char* owner, const char* nam, const T& val, const std::string& desc) const {
    return this->declareMonitor(std::string(owner), std::string(nam), val, desc);
  }
  /// Declare monitoring item to the monitoring component: scalar item
  template <typename T> inline const DataflowComponent& 
  DataflowComponent::declareMonitor(const char* nam, const T& val, const std::string& desc) const {
    return this->declareMonitor(this->name, std::string(nam), val, desc);
  }
  
  /// Declare monitoring item:  Pairs of int/long
  template <typename T> inline const DataflowComponent&
  DataflowComponent::declareMonitor(const char* own, const char* nam, const T& v1, const T& v2, const std::string& dsc) const {
    return this->declareMonitor(std::string(own), std::string(nam), v1, v2, dsc);
  }

  /// Declare monitoring item:  Pairs of int/long
  template <typename T> inline const DataflowComponent&
  DataflowComponent::declareMonitor(const char* nam, const T& val1, const T& val2, const std::string& desc) const {
    return this->declareMonitor(this->name, std::string(nam), val1, val2, desc);
  }

  /// Declare pointer/array monitoring item to the monitoring component with size in bytes (owner=self)
  template <typename T> inline const DataflowComponent&
  DataflowComponent::declareMonitor(const char* nam, const T* val, std::size_t bytes, const std::string& desc) const {
    return this->declareMonitor(this->name, std::string(nam), val, bytes, desc);
  }

  /// Declare pointer/array monitoring item to the monitoring component with size in bytes
  template <typename T> inline const DataflowComponent&
  DataflowComponent::declareMonitor(const char* o, const char* n, const T* val, std::size_t len, const std::string& dsc) const {
    return this->declareMonitor(std::string(o), std::string(n), val, len, dsc);
  }

  /// Declare pointer/structure monitoring item to the monitoring component with size in bytes and specific format
  template <typename T> inline const DataflowComponent&
  DataflowComponent::declareMonitor(const char* owner,
                                    const char* nam,
                                    const T*    val,
                                    std::size_t bytes,
                                    const std::string& fmt,
                                    const std::string& desc) const
  {
    return this->declareMonitor(std::string(owner),std::string(nam), val, bytes, fmt, desc);
  }

  /// Declare monitoring item to the monitoring component: scalar item
  template <typename T, std::size_t length> inline const DataflowComponent&
  DataflowComponent::declareMonitorArray(const char*        owner,
                                         const char*        nam,
                                         const std::array<T,length>& val,
                                         const std::string& desc) const   {
    return this->declareMonitor(owner, nam, &val.at(0), val.size()*sizeof(T), desc);
  }

  /// Declare monitoring item to the monitoring component: scalar item (owner=self)
  template <typename T, std::size_t length> inline const DataflowComponent&
  DataflowComponent::declareMonitorArray(const std::string& owner,
                                         const std::string& nam,
                                         const std::array<T,length>& val,
                                         const std::string& desc) const   {
    return this->declareMonitor(owner, nam, &val.at(0), val.size()*sizeof(T), desc);
  }

  /// Declare monitoring item to the monitoring component (owner=self): scalar item
  template <typename T, std::size_t length> inline const DataflowComponent&
  DataflowComponent::declareMonitorArray(const char*        nam,
                                         const std::array<T,length>& val,
                                         const std::string& desc) const   {
    return this->declareMonitor(this->name, nam, &val.at(0), val.size()*sizeof(T), desc);
  }

  /// Declare monitoring item to the monitoring component (owner=self): scalar item
  template <typename T, std::size_t length> inline const DataflowComponent&
  DataflowComponent::declareMonitorArray(const std::string& nam,
                                         const std::array<T,length>& val,
                                         const std::string& desc) const   {
    return this->declareMonitor(this->name, nam, &val.at(0), val.size()*sizeof(T), desc);
  }

}      // end namespace Online
#endif //  ONLINE_DATAFLOW_DATAFLOWCOMPONENT_H
