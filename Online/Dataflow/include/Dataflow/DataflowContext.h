//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  DataflowContext.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_DATAFLOWCONTEXT_H
#define ONLINE_DATAFLOW_DATAFLOWCONTEXT_H

// Framework include files
#include <Dataflow/Properties.h>

// C/C++ include files
#include <typeinfo>
#include <cstdarg>
#include <cstdlib>
#include <cstdint>
#include <string>
#include <algorithm>

///  Online namespace declaration
namespace Online  {

  enum DataflowStatus  {
    DF_ERROR        = 0,  // 0
    DF_SUCCESS      = 1,  // 1<<0
    DF_CANCELLED    = 2,  // 1<<1
    DF_TIMEOUT      = 4,  // 1<<2
    DF_BADTRANSFER  = 8,  // 1<<3
    DF_PASSED       = 1<<5,
    DF_EXECUTED     = 1<<6,
    DF_CONTINUE     = 1<<7
  };

  // Forward declarations
  class DataflowComponent;
  class DataflowIncident;
  class DataflowManager;
  class DataflowTask;
  class MBMClient;
  class EventContext;
  class event_header_t;

  /// Main framework context for all dataflow components
  /** @class DataflowContext DataflowContext.h Dataflow/DataflowContext.h
   *
   *  \author  Markus Frank
   *  \version 1.0
   */
  class DataflowContext  {
  public:
    /// The only class, which can manage the context
    friend class DataflowManager;

  public:
    /// Task definition
    typedef DataflowTask Task;

    /// Public interface for event data
    /**
     *
     *  \author  Markus Frank
     *  \version 1.0
     */
    struct EventData {
      event_header_t* header  = 0;
      char*           data    = 0;
      int             release = 0;
      int             length  = 0;
      int             type    = 0;
      unsigned int    mask[4] = {0,0,0,0};
      EventContext*   context;
      /// Default constructor
      EventData() = default;
      /// Copy constructor
      EventData(const EventData& copy) = default;
      /// Destructor
      ~EventData();
      /// Assignment operator
      EventData& operator=(const EventData& copy) = default;
      /// Get data by proper type
      template <typename T> T*       as()        { return (T*)data;  }
      /// Get data by proper type (const)
      template <typename T> const T* as()  const { return (T*)data;  }
    };


    /// Public interface for output logging
    /**
     *
     *  \author  Markus Frank
     *  \version 1.0
     */
    class Logger  {
    public:
      /// Access the output level
      int& level;
      /// Default constructor
      Logger(int& lvl, DataflowContext& context);
      /// Default destructor
      virtual ~Logger();
      /// Write output .....
      virtual size_t print(int severity, const char* source, const char* fmt, ...) = 0;
      /// Write output .....
      virtual size_t print(int severity, const char* source, const char* fmt, va_list& args) = 0;
    };

    /// Public interface for the manager
    /**
     *
     *  \author  Markus Frank
     *  \version 1.0
     */
    class Manager  {
    public:
      /// Enum for all known different component types
      enum enabled_type  {
        RUNABLE   = 1<<2,
        MANAGED   = 1<<3,
        SERVICE   = 1<<4,
        ALGORITHM = 1<<5,
        OUTPUT    = 1<<6,
        ALL       = RUNABLE|MANAGED|OUTPUT|SERVICE|ALGORITHM
      };
    
    public:
      /// Default destructor
      virtual ~Manager();
      /// Get acces to the underlying object if necessary
      virtual DataflowManager& manager() = 0;
      /// Get acces to the underlying object if necessary (const)
      virtual const DataflowManager& manager() const = 0;
      /// Update run number for publication
      virtual void setRunNumber(uint32_t run_number) = 0;
      /// Access current run number
      virtual uint32_t currentRunNumber() const = 0;
      /// Check enabled status of the manager
      virtual int is_enabled(enabled_type flags)   const = 0;
    };

    /// Public interface for the incident service
    /**
     *
     *  \author  Markus Frank
     *  \version 1.0
     */
    class Incident  {
    public:
      /// Default destructor
      virtual ~Incident();
      /// Unsubscribe component from all incidents
      virtual int unsubscribe(DataflowComponent* c) = 0;
      /// Unsubscribe component from a specific incident
      virtual int unsubscribe(DataflowComponent* c, const std::string& type) = 0;
      /// Subscribe component to a specific incident
      virtual int subscribe(DataflowComponent* c, const std::string& type) = 0;
      /// Fire an incident to notify other components
      virtual int fire(DataflowComponent* c, const DataflowIncident& incident) = 0;
    };

    /// Public interface for the job options service
    /**
     *
     *  \author  Markus Frank
     *  \version 1.0
     */
    class Options  {
    public:
      typedef std::pair<std::string,std::string> Prop;
      typedef std::vector<Prop>                  Catalog;

    public:
      /// Default destructor
      virtual ~Options();
      /// Clear all stored properties
      virtual void clear() = 0;
      /// Load a job options file
      virtual int load(const std::string& options, const std::string& path="") = 0;
      /// Add a client property to the options catalog
      virtual void addProperty(const std::string& client, Prop&& p) = 0;
      /// Set the component's properties
      virtual int setProperties(const std::string& client, PropertyManager& properties) = 0;
      /// Get the component properties of a given client
      virtual const Catalog& getProperties( const std::string& client) const = 0;
    };

    /// Public interface for the monitor service
    /**
     *
     *  \author  Markus Frank
     *  \version 1.0
     */
    class Monitor  {
    public:
      /// Default destructor
      virtual ~Monitor();
      /// Monitoring interface: Declare monitoring entity (simple scalar)
      virtual int declare(const std::string&    parent,
                          const std::string&    nam,
                          const void*           ptr,
                          const std::type_info& data_typ,
                          const std::string&    desc) = 0;
      /// Monitoring interface: Declare monitoring entiry (ratio of two numbers)
      virtual int declare(const std::string&    parent,
                          const std::string&    nam,
                          const void*           ptr1,
                          const void*           ptr2,
                          const std::type_info& data_typ,
                          const std::string&    desc) = 0;
      /// Monitoring interface: Declare monitoring entity (pointer with size)
      virtual int declare(const std::string&    parent,
                          const std::string&    nam,
                          const void*           ptr,
                          size_t                data_length,
                          const std::type_info& data_typ,
                          const std::string&    desc) = 0;
      /// Monitoring interface: Declare monitoring entity (structure with given format)
      virtual int declare(const std::string&    parent,
                          const std::string&    nam,
                          const void*           ptr,
                          size_t                data_length,
                          const std::type_info& data_typ,
                          const std::string&    fmt,
                          const std::string&    desc) = 0;
      /// Monitoring interface: Undeclare single monitoring item
      virtual int undeclare(const std::string& parent,const std::string& nam) = 0; 
      /// Monitoring interface: Undeclare all monitoring items
      virtual int undeclare(const std::string& parent) = 0; 
      /// Monitoring interface: Update monitoring items
      virtual int update(const std::string& parent,const std::string& item, int run_number) = 0;
      /// Monitoring interface: Reset some/all monitoring items
      virtual int reset(const std::string& parent,const std::string& what) = 0;
      /// Update current run number context for updates
      virtual void setRunNo(uint32_t runno) = 0;
    };

    /// Universal global task identifier
    std::string utgid;
    /// Reference to the manager object
    Manager&    manager;
    /// Reference to task envelope
    Task*       task;
    /// reference to the output logger
    Logger*     logger;
    /// Reference to the job options component
    Options*    options;
    /// Reference to the incident handler object
    Incident*   incidents;
    /// Reference to the monitoring object
    Monitor*    monitor;
    /// Reference to the buffer manager client
    MBMClient*  mbm;

  private:
    /// NO Default constructor
    DataflowContext() = delete;
    /// NO Copy constructor
    DataflowContext(const DataflowContext& copy) = delete;
    /// Initializing constructor
    DataflowContext(Manager& mgr);
    /// Default destructor
    virtual ~DataflowContext();
    /// NO object assignment
    DataflowContext& operator=(const DataflowContext& copy) = delete;
  };

  /// Destructor
  inline DataflowContext::EventData::~EventData()  {
    if ( release && data ) delete [] data;
  }

  /// Default constructor
  inline DataflowContext::Logger::Logger(int& lvl, DataflowContext& context) : level(lvl)  {
    context.logger = this;
  }
}      // end namespace Online

#endif //  ONLINE_DATAFLOW_MONITORING_H
