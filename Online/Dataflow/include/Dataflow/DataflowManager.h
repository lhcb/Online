//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  DataflowManager.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_DATAFLOWMANAGER_H
#define ONLINE_DATAFLOW_DATAFLOWMANAGER_H

// C/C++ include files
#include <memory>

// Framework include files
#include <Dataflow/DataflowComponent.h>

///  Online namespace declaration
namespace Online  {

  // Forward declarations
  class DataflowIncident;
  class ComponentHandler;

  /// Main framework manager for all dataflow components
  /** @class DataflowManager DataflowManager.h Dataflow/DataflowManager.h
   *
   *  This is the main application object and the 'master of the universe'
   *  This object sequences all algorithms ans services according to the
   *  invoked FSM transitions.
   *
   *  The manager also allows to create managed components on demand.
   *
   * \author  Markus Frank
   * \version 1.0
   */
  class DataflowManager : public DataflowContext::Manager, public DataflowComponent  {
  public:
    enum other_enabled_type  {
      SETUP     = 1<<30,
      ALWAYS    = 1<<11
    };

    /// Component manipulation interface
    class ComponentManip   {
    public:
      typedef DataflowComponent Component;
      typedef ComponentManip    self_t;

      /// Default constructor
      ComponentManip() = default;
      /// Default copy constructor
      ComponentManip(const ComponentManip& copy) = default;
      /// Default move constructor
      ComponentManip(ComponentManip&& copy) = default;
      /// Default destructor
      virtual ~ComponentManip() = default;
      /// Assignment operator
      ComponentManip& operator=(const ComponentManip& copy) = default;
      /// Callback function
      /** 
       *  Note:
       *  -- a negative return code breaks the scan
       *  -- positive return codes are added and returned to the client
       */
      virtual long operator()(int typ, Component* component) = 0;
    };

  private:
    typedef std::vector<std::string>  ComponentNames;
    typedef std::vector<Component*>   Components;

    std::string      runableName      {         };
    std::string      taskType         {         };
    Components       setup            {         };
    Components       always           {         };
    Components       managed          {         };
    Components       outputs          {         };
    Components       services         {         };
    Components       algorithms       {         };

    /// Property: Setup entity component declarations
    ComponentNames   setupNames       {         };
    /// Property: Output stream like component declarations
    ComponentNames   outputNames      {         };
    /// Property: Service like component declarations
    ComponentNames   serviceNames     {         };
    /// Property: Algorithm like component declarations
    ComponentNames   algorithmNames   {         };
    /// Counter of consecutive bad events
    long             numBadEvent      {       0 };
    /// Property: Max number of consecutive bad (NON MDF) events (Default: 2)
    long             maxBadEvent      {       2 };
    /// Property: Execute shutdown sequence for (algs, services,...) in reverse order
    int              reverseShutdown  {       0 };
    /// Monitoring quantity: Current run number
    uint32_t         currentRun       {     100 };
    /// State (follows the state of the ControlsPlug
    int              state            {     'U' };
    /// Flag to enable/disable fine grained event processing
    unsigned int     enabled          {    0xFF };
    /// Flag to enable/disable event processing 'en block'
    unsigned int     enableAlgorithms {    0xFF };

  protected:
    typedef int (ComponentHandler::*handler_func_t)(Component*) const;

    /// Access known component by name. If not found exception!
    Component* getComponent(const std::string& nam)  const;
    /// Access known component by name. If not found NO exception is thrown!
    Component* getComponentUnchecked(const std::string& nam)  const;
    /// Create unique context object
    static DataflowContext* make_context(DataflowContext::Manager* mgr);

    /// Execute transition action
    int action(const char* transition, handler_func_t func, bool reverse = false);
    /// Execute full transition action
    int invoke(const char* transition, handler_func_t func, int target_state);
    
  public:
    /// NO Default constructor
    DataflowManager() = delete;
    /// NO Copy constructor
    DataflowManager(const DataflowManager& copy) = delete;
    /// Initializing constructor
    DataflowManager(const std::string& nam, const ComponentNames& infrastructure=ComponentNames());
    /// Default destructor
    virtual ~DataflowManager();
    /// NO object assignment
    DataflowManager& operator=(const DataflowManager& copy) = delete;

  public:

    /// Check enabled status of the manager
    virtual int is_enabled(enabled_type flags)   const  override
    { return (enabled&flags)==flags ? flags : 0;   }
    /// Get acces to the underlying object if necessary
    virtual DataflowManager& manager() override
    { return *this;                                }
    /// Get acces to the underlying object if necessary (const)
    virtual const DataflowManager& manager() const override
    { return *this;                                }

    /// Add a managed component: Set state according to my state and then alog with the others.
    virtual Component* getManagedComponent(const std::string& type_name, bool create);
    /// Execute actions on all components. Invoke callback
    long for_each(ComponentManip& handler);

    /// Execute actions on all components. Invoke callback on predefined set
    long for_each(int typ, ComponentManip& handler);

    /// Configure the instance (create components)
    virtual int configure();
    /// Terminate the data flow component. 
    virtual int terminate();
    /// Initialize the data flow component. 
    virtual int initialize()  override;
    /// Start the data flow component. 
    virtual int start()  override;
    /// Stop the data flow component. 
    virtual int stop()  override;
    /// Finalize the data flow component. 
    virtual int finalize()  override;
    /// Pause the data flow component. 
    virtual int pause()  override;
    /// Continuing the data flow component. 
    virtual int continuing()  override;
    /// Run runable component (option: runableName)
    virtual int run()  override;
    /// Cancel the data flow component. 
    virtual int cancel()  override;
    /// Enable the data flow component. 
    virtual int enable()  override;

    /// Incident handler implemenentation: Inform that a new incident has occured
    virtual void handle(const DataflowIncident& inc) override;
    /// Update run number for publication
    virtual void setRunNumber(uint32_t run_number)  override;
    /// Access current run number
    virtual uint32_t currentRunNumber() const override;
    /// Execute event. 
    virtual int execute(const Context::EventData& event)  override;
  };
}      // end namespace Online

#endif //  ONLINE_DATAFLOW_MONITORING_H
