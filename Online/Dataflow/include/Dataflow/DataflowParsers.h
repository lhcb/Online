//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  DataflowParsers.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_PARSERS_H
#define ONLINE_DATAFLOW_PARSERS_H

#if defined(DD4HEP_NEED_EVALUATOR)
#include <XML/config.h>
#include <XML/Evaluator.h>
namespace dd4hep  {
  XmlTools::Evaluator& evaluator();
  inline XmlTools::Evaluator& g4Evaluator() { return evaluator(); }
}
#endif

// Standard Dataflow parser handling
#include <DD4hep/Parsers.h>
#include <DD4hep/ToStream.h>

#endif //  ONLINE_DATAFLOW_PARSERS_H
