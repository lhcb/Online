//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  DataflowTask.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_DATAFLOWTASK_H
#define ONLINE_DATAFLOW_DATAFLOWTASK_H

/// Framework includes
#include <Dataflow/DataflowComponent.h>
#include <Dataflow/ControlPlug.h>
#include <CPP/Interactor.h>

///  Online namespace declaration
namespace Online  {

  /// Forward declarations
  class ControlPlug;

  /// Basic task instance
  /** @class DataflowTask DataflowTask.h GaudiOnline/DataflowTask.h
   *
   * The DataflowTask is an object to encapsulate applications,
   * which execute the basic FSM state machinery given by
   * the FSMs in the HLT farm.
   *  
   * These FSMs look like this:
   *
   *                     +----------+                        
   *                     |  UNKNOWN |                      
   *                     +----------+                         
   *                         |  ^                  
   *          DataflowTask() |  | unload()
   *                         |  |        
   *                         v  |                  
   *                     +----------+       +---------+
   *                     | NOT_READY|<------+  ERROR  |<------+
   *                     +----------+       +---------+       |
   *                         |  ^                             |
   *                         |  |                             |
   *              configure()|  |reset()              error() |
   *                         |  |                             |
   *                         |  |                             |
   *                         V  |                             |
   *        start()      +----------+                         |
   *      +--------------| READY    |                         |
   *      |              +----------+                         |
   *      |                  |  ^                             |
   *      V                  |  |                             |
   *  +---------+            |  |                             |
   *  | ACTIVE  |            |  |                             |
   *  +---------+            |  |                             |
   *      |                  |  |                             |
   *      |          start() |  | stop()                      |
   *      |                  V  |                             |
   *      | go()         +-----------+                        |
   *      +------------->| RUNNING   |------------------------+
   *                     +-----------+
   *                         |  ^                             
   *                         |  |                             
   *               pause()   |  |  continue()                             
   *                         |  |                             
   *                         V  |                             
   *                     +-----------+
   *                     | PAUSED    |
   *                     +-----------+
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \ingroup DATAFLOW
   */
  class DataflowTask : public DataflowComponent, public CPP::Interactor  {
  public:

  protected:
    /// Commander
    ControlPlug* plug          = 0;
    /// Property: Control plug type
    std::string plugType;
    /// Property: Data flow task class
    std::string taskClass;
    /// Property: Task options
    std::string options;
    /// Property: Delay at initialize in seconds
    double      delay_initialize = -1.0;
    /// Property: Delay at finalize in seconds
    double      delay_finalize = -1.0;
    /// Property: Delay at start in seconds
    double      delay_start    = -1.0;
    /// Property: Delay at stop in seconds
    double      delay_stop     = -1.0;
    /// Property: Delay at pause in seconds
    double      delay_pause    = -1.0;
    /// Property: Delay at cancel in seconds
    double      delay_cancel   = -1.0;
    /// Property: Delay at continue in seconds
    double      delay_continue = -1.0;
    /// Property: Delay at enable in seconds
    double      delay_enable   = -1.0;
    /// Property: Automatic configuration en/dis-abled
    bool        auto_startup   = false;
    /// Property: Automatic termination en/dis-abled
    bool        auto_shutdown  = false;
    /// Property: Enable pause processing. Default: false
    bool        have_pause     = true;

    /// Delay execution with sleep
    virtual int delay(double seconds);
    /// (Re)connect DIM services
    virtual int connect(ControlPlug* cmd=0);
    /// Disconnect DIM services
    virtual int disconnect();
  public:
    /// Declare FSM state
    virtual int declareState(Control::State state)  const;  

  protected:
    /// Declare FSM sub-state
    virtual int declareSubState(Control::SubState state)  const;  
    /// Declare current FSM state
    virtual int declareCurrentState()  const;
    /// Declare the target state of the transition
    virtual int declareTargetState()  const;  
    /// Set transition target state
    virtual int setTargetState(Control::State target)  const;  
    
    /// Access the raw manager interface
    DataflowManager& manager() const;

    /// Indicate failure and take appropriate actions
    virtual int failed();

    /// Configure the instance (create components)
    virtual int app_configure();
    /// Initialize the dataflow task instance.
    virtual int app_initialize();
    /// Start the dataflow task instance.
    virtual int app_start();
    /// Stop the dataflow task instance.
    virtual int app_stop();
    /// Finalize the dataflow task instance.
    virtual int app_finalize();
    /// Terminate the dataflow task instance.
    virtual int app_terminate();
    /// Pause the dataflow task instance
    virtual int app_pause();
    /// Continue the dataflow task instance
    virtual int app_continue();
    /// Cancel the dataflow task instance
    virtual int app_cancel();
    /// Enable the dataflow task instance
    virtual int app_enable();

  public:
    /// Initializing constructor
    DataflowTask(const std::string& nam, Context& ctxt);
    /// Default destructor
    virtual ~DataflowTask();

    /// ControlPlug invocation:  Configure the instance (create components)
    virtual int configure();
    /// ControlPlug invocation:  Terminate the dataflow task instance.
    virtual int terminate();
    /// ControlPlug invocation:  Handling entering the error state
    virtual int gotoerror();
    /// ControlPlug invocation:  Disable trigger
    virtual int disable();
    /// ControlPlug invocation:  Unload and exit the application
    virtual int unload();
    /// ControlPlug invocation:  Activate the application (future)
    virtual int activate();

    /// ControlPlug invocation:  Initialize the dataflow task instance.
    virtual int initialize()  override;
    /// ControlPlug invocation:  Start the dataflow task instance.
    virtual int start()  override;
    /// ControlPlug invocation:  Stop the dataflow task instance.
    virtual int stop()  override;
    /// ControlPlug invocation:  Finalize the dataflow task instance.
    virtual int finalize()  override;
    /// ControlPlug invocation:  Transition to pause
    virtual int pause()  override;
    /// ControlPlug invocation:  Continue after pause
    virtual int continuing()  override;
    /// ControlPlug invocation:  Enable trigger
    virtual int enable()  override;
    /// Cancel pending I/O request etc. Stop processing
    virtual int cancel() override;

    /// Run the task
    virtual int run()  override;

    /// Interactor overload: Command handler
    virtual void handle(const Event& event)  override;
    /// Incident handler callback: Inform that a new incident has occured
    virtual void handle(const DataflowIncident& incident)  override;
  };

}      // end namespace Online

#endif //  ONLINE_DATAFLOW_DATAFLOWTASK_H
