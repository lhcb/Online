//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  Delay.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_DELAY_H
#define ONLINE_DATAFLOW_DELAY_H

// Framework include files
#include <Dataflow/DataflowComponent.h>
#include <CPP/Interactor.h>

///  Online namespace declaration
namespace Online  {

  /// Delay component to be applied in the manager sequence
  /**
   * Delays are specified in the job options as floating point numbers.
   * The delay unit is in seconds.
   *
   * @author  Markus Frank
   * @version 1.0
   */
  class Delay : public DataflowComponent, public CPP::Interactor  {
  protected:

    enum _commands {
      CMD_AUTO_PAUSE,
      CMD_AUTO_CONTINUE,
      CMD_AUTO_ERROR,
    };
    
    /// Delay time in seconds and fractions for initialize()
    double m_initDelay;
    /// Delay time in seconds and fractions for start()
    double m_startDelay;
    /// Delay time in seconds and fractions for stop()
    double m_stopDelay;
    /// Delay time in seconds and fractions for finalize()
    double m_finiDelay;
    /// Delay time in seconds and fractions for terminate()
    double m_terminateDelay;
    /// Delay time in seconds and fractions for pause() and DAQ_PAUSE
    double m_pauseDelay;
    /// Delay time in seconds and fractions for continuing()
    double m_continueDelay;
    /// Delay time in seconds and fractions for DAQ_CANCEL
    double m_cancelDelay;
    /// Delay time in seconds and fractions for DAQ_START_TRIGGER
    double m_startTrgDelay;
    /// Delay time in seconds and fractions for DAQ_STOP_TRIGGER
    double m_stopTrgDelay;
    /// Delay time in seconds and fractions for enable()
    double m_enableDelay;
    /// Delay time in seconds and fractions for execute()
    double m_eventDelay;
    /// Delay to invoke auto-transition to PAUSED
    double m_autoPause;
    /// Delay to invoke auto-transition from PAUSED to RUNNING
    double m_autoContinue;
    /// Delay to invoke auto-transition to ERROR (for debugging....)
    double m_autoError;

    /// Sleep function
    void delay(const char* reason, double timeout)  const;

  public:
    /// Initializing constructor
    Delay(const std::string& name, Context& ctxt);

    /// Default destructor
    virtual ~Delay();

    /// Initialize the delay component
    virtual int initialize()  override;

    /// Start the delay component
    virtual int start()  override;

    /// Stop the delay component
    virtual int stop()  override;

    /// Finalize the delay component
    virtual int finalize()  override;

    /// Enable the delay component
    virtual int enable()  override;

    /// Continuing the data flow component.
    virtual int continuing()  override;

    /// Pause the data flow component.
    virtual int pause()  override;

    /// Data processing overload: process event
    virtual int execute(const Context::EventData& event)  override;

    /// Incident handler implemenentation: Inform that a new incident has occured
    virtual void handle(const DataflowIncident& inc)  override;

    /// Interactor overload: Command handler
    virtual void handle(const CPP::Event& ev)  override;
  };
}      // end namespace Online
#endif //  ONLINE_DATAFLOW_DELAY_H
