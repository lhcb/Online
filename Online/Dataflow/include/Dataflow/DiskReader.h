//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  BurstReader.h
//--------------------------------------------------------------------------
//
//  Package    : GaudiOnline
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_INPUTREADER_H
#define ONLINE_DATAFLOW_INPUTREADER_H

// Framework includes
#include <Dataflow/DataflowComponent.h>
#include <EventData/RawFile.h>
#include <MBM/BufferInfo.h>
#include <MBM/Producer.h>
#include <RTL/rtl.h>

// C/C++ include files
#include <sys/stat.h>
#include <mutex>
#include <ctime>
#include <set>

///   Online namespace declaration
namespace Online  {

  /// Basic HLT2 file reader for the deferred triggering
  /** Class definition of BurstReader.
   *
   * This is the online extension of the runnable of the application manager.
   * The runable handles the actual run() implementation of the
   * ApplicationMgr object.
   *
   * @author Markus Frank
   *
   * @version 1.5
   */
  class DiskReader : public DataflowComponent  {
  public:
    typedef std::vector<std::string> StringV;

    enum {
      GO_DONT_PROCESS=0,
      GO_PROCESS=1,
      GO_GENTLE_STOP=2
    };
    enum {
      AUTO_INPUT_TYPE = 0,
      MDF_INPUT_TYPE = 1,
      MEP_INPUT_TYPE = 2
    };

    /// I/O structure to read disk based files
    /** Class definition of DiskIO.
     *
     * @author Markus Frank
     * @version 1.5
     */
    class DiskIO  final  {
    public:
      typedef std::map<std::string, RawFile>    FileMap;

    public:
      /// Reference to the reader object
      DiskReader&              reader;
      /// List of files to process
      FileMap                  files;
      /// Array of allowed runs to be read from disk
      StringV&                 allowedRuns;
      /// Property: List of directories containing data files
      StringV&                 dirList;
      /// List of files, where something fishy happened while opening
      std::set<std::string>&   badFiles;
      /// File prefix to check for files to read
      std::string&             filePrefix;
      /// Flag to check if datafile is supposed to be mmap'ed
      int&                     map_file;
      /// Flag to delete files if opening fails
      int&                     fail_delete;
      /// Flag to delete files if opening succeeds
      int&                     ok_delete;
      /// Flag to steer directory scanning
      int&                     scan_recursive;
    public:
      /// Scan directory for matching files
      size_t  scan_directory(const std::string& dir_name);
      /// Scan directory for matching files
      size_t  scan_files();
      /// Add single file to work list
      void    add_file(const std::string& path, RawFile&& file);
      /// Open a new data file
      RawFile openFile();

      /// Standard constructor
      DiskIO(DiskReader& rdr);
      /// Default destructor
      ~DiskIO() = default;
    };

  protected:
    friend class DiskIO;
    typedef std::pair<long, unsigned char*> MemBuffer;
    //typedef std::map<std::string, RawFile> FileMap;
    /// Flag indicating that MBM event retrieval is active
    bool                     m_receiveEvts      { false };
    /// Lock handle
    lib_rtl_lock_t           m_lock             { nullptr };
    /// Reference to buffer manager producer
    MBM::Producer*           m_producer         { nullptr };
    /// Decompression buffer
    MemBuffer                deCompress         { 0, nullptr};

    /// Property: Partition name string
    std::string              m_partitionName    { };
    /// Property: Run-type string
    std::string              m_activity         { };
    /// Property: Stream identifier
    std::string              m_stream           { };
    /// Property: Name of the 'GO' command which should be used. If empty feature is disabled.
    std::string              m_goService        { };
    /// Property: Buffer name for data output
    std::string              m_buffer           { };
    /// Property: File prefix to select files from data directory
    std::string              m_filePrefix       { };
    /// Property: Path to the file containing broken nodes, where no reading should happen
    std::string              m_brokenHostsFile  { };
    /// Property: Name of the DIM run number service (Ignored if not set)
    std::string              m_runNumberService;
    /// Property: List of directories containing data files
    StringV                  m_dirList;
    /// Property: List of runs to be processed (as strings!)
    StringV                  m_allowedRuns;
    /// Property: Buffer names to be checked on special transitions
    StringV                  m_mbmNames;
    /// Property: Maximum number of seconds to wait for consumers (default: 20 seconds)
    int                      m_maxConsWait;
    /// Property: Maximum number of seconds to wait for buffers being empty (default: 1000 seconds)
    int                      m_maxPauseWait;
    /// Property: Time for initial sleep before starting to process files (default: 10 sec)
    int                      m_initialSleep;
    /// Property: Time for sleep before finishing the PAUSE transition (default: 5 sec)
    int                      m_pauseSleep;
    /// Property: Scan input directories recursively
    int                      m_scanRecursive;
    /// Property: Inhibit rescan of the source directory
    int                      m_rescan;
    /// Property: Maximum number of events to be read between (start - stop)
    long                     m_max_events;
    /// Property: Maximum number of events to be read from 1 single input file
    long                     m_max_events_per_file;
    /// Property: Disable automatic check to determine MDF file type and require it (0=auto, 1=MDF, 2=MEP)
    int                      m_inputType;
    /// Property: On open failed: delete file
    int                      m_openFailDelete;
    /// Property: Flag to indicate if files should be deleted
    int                      m_deleteFiles;
    /// Property: Flag to require consumer presence checks
    int                      m_requireConsumers   { 1 };
    /// Property: Packing factor if multiple events should be declared
    int                      m_packingFactor      { 1 };
    /// Property: Pre-allocation size if multiple events should be declared
    long                     m_preAllocSize       { 0 };
    /// Property: Artificial delay in micro seconds between each MBM declaration
    int                      m_muDelay            { 0 };
    /// Property: Memory map files rather than posix open
    int                      m_mmapFiles          { 0 };
    /// Property: Flag to re-use same input file for debugging
    int                      m_reuse_file         { 0 };
    /// Property: Flag to patch ODIN bank and keep the same run-number for N events
    int                      m_patch_odin         { 0 };
    /// Property: Checksum type to verify the checksum in the MDF headers (if present)
    int                      m_checksumType       { 0 };
    /// Property: Flag to print the checksum to the output
    bool                     m_printChecksum      { false };
    /// Property: Flag to indicate if in premature stop/pause the file reminder should be saved.
    bool                     m_saveRest           { true };

    /// Monitoring quantity: Number of event bursts queued for processing
    long                     m_burstsIN           { 0 };
    /// Monitoring quantity: Number of events queued for processing
    long                     m_eventsIN           { 0 };
    /// Monitoring quantity: Number of events which finished processing
    long                     m_eventsOUT          { 0 };
    /// Monitoring quantity: Number of events per current input file
    long                     m_evtCountFile       { 0 };
    /// Monitoring quantity: Number of decompression actions
    long                     m_evtDecompressions  { 0 };
    /// Monitoring quantity: Currently processed run number
    int                      m_currentRun         { 0 };
    /// Monitoring quantity: Number of files to be opened
    int                      m_filesOpened        { 0 };
    /// Monitoring quantity: Number of files closed
    int                      m_filesClosed        { 0 };
    /// Monitoring quantity: Number of files successfully opened
    int                      m_filesOpenOK        { 0 };
    /// Monitoring quantity: Number of files failed to be opened
    int                      m_filesOpenERROR     { 0 };
    
    /// DIM service ID for publishing currentRun
    int                      m_runSvcID           { 0 };
    /// DIM service ID for 'GO' command
    int                      m_goSvcID            { 0 };
    /// DIM value of GO service
    int                      m_goValue            { 0 };
    /// DIM service ID for accessing run number if m_runNumberService is set
    int                      m_runNumberSvcID     { 0 };
    /// DIM value of run number service
    int                      m_runNumberValue     { -1 };

    /// Flag to indicate if the node is in the broken hosts file and hence disabled
    bool                     m_disabled           { false };
    /// Flag to indicate if PAUSE incident was received or should be sent.
    bool                     m_goto_paused        { false };
    /// Buffer information blocks to be checked before special transitions are completed
    MBM::BufferInfo*         m_mbmInfos           { nullptr };
    /// List of files, where something fishy happened while opening
    std::set<std::string>    m_badFiles           { };
    /// Mutex to protect producer from unsolicited cancel requests
    std::mutex               m_prodLock           { };
    /// Mutex to protect cancel/finalize when consumers are checked.
    std::mutex               m_infoLock           { };
    /// String keyed map of buffer information blocks.
    std::map<std::string,MBM::BufferInfo*> m_mbmInfo { };

    /// Counter of events processed between start-stop
    long                     m_evtCountStartStop   { 0 };

    
    class MBMAllocator : public RawFile::Allocator  {
    public:
      DiskReader* reader = 0;
      MBMAllocator() = delete;
      MBMAllocator( MBMAllocator&& copy ) = delete;
      MBMAllocator( const MBMAllocator& copy ) = delete;
      MBMAllocator(DiskReader* rdr) : reader(rdr) {}
      virtual ~MBMAllocator() = default;
      MBMAllocator& operator=( const MBMAllocator& copy ) = delete;
      virtual unsigned char* operator()(size_t length)   override  final;
    };
    friend class MBMAllocator;

    // Helper routines

    /// DIM command service callback
    static void go_handler(void* tag, void* address, int* size);
    /// DIM command service callback
    static void run_number_change_handler(void* tag, void* address, int* size);
    /// DIM service update handler
    static void run_no_update(void* tag, void** buf, int* size, int* first);

    static bool check_consumers_partid(const MBM::BufferInfo& info, int pid);
    static bool check_consumers(const MBM::BufferInfo& info, int pid, int evtyp);

    /// Load single MDF event with optional decompression
    long load_mdf_event(RawFile&     file,
			std::pair<long,unsigned char*>  output,
			std::pair<long,unsigned char*>& decomp );

    /// Interprete file name with replacement strings if present
    std::string  interprete_file_tags(const std::string& file_name)  const;
    /// Interprete file name with replacement strings if present
    std::string  interprete_file_name(const std::string& file_name, const std::string& run_number)  const;
    /// Update run number service
    void         updateRunNumber(int new_run);
    /// If the GO service is present, wait until processing is enabled
    virtual int  waitForGo();
    /// Wait until consumers are ready (if required)
    virtual int  waitForConsumers(const std::string& buffer_name);
    /// Wait until event buffers are empty before finishing....
    virtual void waitForPendingEvents(int seconds);
    /// Patch the string array with allowed runs
    virtual void checkAllowedRuns();

    struct LoadResult  {
      size_t length;
      int    packing;
      RawFile::EventType type;
    };
    
    /// Runable implementation : Run the class implementation. May be overloaded by sub-classes
    virtual int        i_run();

  public:
    /// Load event data from file into the buffer manager
    virtual LoadResult load_event_data(RawFile::Allocator& allocator, RawFile& file);
    /// Declare event frame to the buffer manager to trigger clients
    virtual int        declare_data(const char* fname, const LoadResult& load);
    /// Handle MBM request: load event data and declare to MBM
    virtual int        handle_data_request(RawFile& file);

    virtual void       save_file_rest(RawFile& file);
    /// Close file if the mapping should not be explicitly maintained
    virtual long       closeFile(RawFile& file, bool drop_mapping = false);

  public:
    /// Standard algorithm constructor
    DiskReader(const std::string& name, DataflowContext& context);
    /// No default constructor
    DiskReader() = delete;
    /// No move constructor
    DiskReader(DiskReader&& copy) = delete;
    /// No copy constructor
    DiskReader(const DiskReader& copy) = delete;
    /// Standard Destructor
    virtual ~DiskReader();
    /// IService implementation: initialize the service
    virtual int initialize()  override;
    /// Low level overload from Service base class: sysStart
    virtual int start()  override;
    /// IService implementation: finalize the service
    virtual int finalize()  override;
    /// Low level overload from Service base class: sysStop
    virtual int stop()  override;
    /// Stop gracefully execution of the dataflow component
    virtual int pause()  override;
    /// Continue the data flow component.
    virtual int continuing()  override;
    /// Cancel I/O operations of the dataflow component
    virtual int cancel()  override;
    /// Incident handler implemenentation: Inform that a new incident has occured
    virtual void handle(const DataflowIncident& inc)  override;
    /// IRunable implementation : Run the class implementation
    virtual int run()  override;
  };
}       // End namespace Online
#endif  // ONLINE_DATAFLOW_INPUTREADER_H
