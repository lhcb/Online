//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  EventContext.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_EVENTCONTEXT_H
#define ONLINE_DATAFLOW_EVENTCONTEXT_H

// Framework include files

// C/C++ include files
#include <map>
#include <typeinfo>
#include <stdexcept>

///  Online namespace declaration
namespace Online  {
 
 /// Event context structure
  /** @class EventContext EventContext.h Dataflow/EventContext.h
   *
   *  Poor man's transient data store.
   *  Access is by key (unsigned integer) creted e.g. with the 
   *  hash32 algorithm from a suitable string representation.
   *
   *  \author  Markus Frank
   *  \version 1.0
   */
  class EventContext  {
  protected:
    /// Base class definition of a context item. Internal use only.
    /**
     *  \author  Markus Frank
     *  \date    18/10/2016
     */
    class EventItem {
    public:
      /// Underlying data type
      const std::type_info& type;
      /// Cast helper
      template <typename T> inline T* cast() { return (T*)this; }

    public: 
      /// Default constructor
      EventItem(const std::type_info& t);
      /// Default destructor
      virtual ~EventItem();
    };

    /// Class definition of a concrete context item. Internal use only.
    /**
     *  \author  Markus Frank
     *  \date    18/10/2016
     */
    template<typename T> class Item : public EventItem  {
    public:
      T data;
      /// Default constructor
      Item();
      /// Default destructor
      virtual ~Item() = default;
    };

    /// The mapped data items
    std::map<unsigned int,EventItem*> data;

    /// Retrieve item by key and type
    EventItem* item(unsigned int key, const std::type_info& type)  const;
    
  public:
    /// Default constructor
    EventContext();
    /// Default destructor
    virtual ~EventContext();

    /// Check existence of an item by key
    bool exists(unsigned int key)  const;
    /// Check existence of an item by key and type
    bool exists(unsigned int key, const std::type_info& type)  const;
    /// Add a new data item by key and type
    template<typename T> T& add(unsigned int key);
    /// Retrieve item by key and type
    template<typename T> T& get(unsigned int key)  const;
  };

  /// Default constructor
  inline EventContext::EventItem::EventItem(const std::type_info& t) : type(t) {}

  /// Default constructor
  template <typename T> inline EventContext::Item<T>::Item() : EventItem(typeid(T)), data() {}

  /// Retrieve item by key and type
  template<typename T> inline T& EventContext::get(unsigned int key)  const {
    // Safe operatoin: All checks performed inside!
    return item(key,typeid(T))->cast<Item<T> >()->data;
  }

  /// Add a new data item by key and type
  template<typename T> T& EventContext::add(unsigned int key)   {
    Item<T>* ptr;
    if ( exists(key) ) throw std::runtime_error("A event item with this key exists already.");
    data.insert(make_pair(key,ptr=new Item<T>()));
    return ptr->data;
  }
}      //  end namespace Online
#endif //  ONLINE_DATAFLOW_EVENTCONTEXT_H
