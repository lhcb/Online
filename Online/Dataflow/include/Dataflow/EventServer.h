//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  EventServer.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Description: Runable to serve independent network clients with event
//               data. Requests are purely served on demand ie. each
//               client has to ask for every event to be processed.
//
//  Author     : M.Frank
//  Created    : 4/10/2016
//
//==========================================================================
#ifndef ONLINE_DATAFLOW_EVENTSERVER_H
#define ONLINE_DATAFLOW_EVENTSERVER_H 1

#ifndef TRANSFER_NS
#error This header file is not intended for user inclusion, but only to instantiate component factories!
#endif

// Framework includes
#include <Dataflow/DataflowComponent.h>
#include <MBM/bmdef.h>
#include <MBM/Consumer.h>
#include <MBM/Requirement.h>
#include <RTL/rtl.h>

// C/C++ include files
#include <mutex>
#include <list>
#include <map>

// Forward declarations

///   Online namespace declaration
namespace Online  {

  /// Network event data server for miscaneleous clients.
  /** @class EventServer EventServer.h Dataflow/EventServer.h
   *
   * Runable to serve independent network clients with event
   * data. requests are purely served on demand ie. each
   * client has to ask for every event to be processed.
   *
   *  @author Markus Frank
   *  @version 1.0
   */
  class EventServer : public DataflowComponent  {

  protected:
    enum RunableStates { WAIT_REQ, WAIT_EVT, ACTION_EVT, DONE };
    /// Request entry definition
    typedef std::pair<MBM::Requirement,TRANSFER_NS::netentry_t*> ReqEntry;
    /// Definition of the container containing waiting clients
#ifdef EventServer_USE_MAP
    typedef std::map<std::string, ReqEntry > Recipients;
#else
    typedef std::pair<std::string,ReqEntry> Recipient;
    typedef std::list<Recipient> Recipients;
#endif
    /// Number of events to be processed
    int                  m_evtMax = 1;
    /// Property: Print delivery after each m_printNum events sent
    int                  m_printNum = 1000;
    /// Property: Number of thread instances used by the network library
    int                  m_nThreads = 1;
    /// Property: Exchange request types with the server
    int                  m_changeRequest = false;
    /// Property: String representation of basic request (trigger and veto masks will be taken from requestors)
    std::string          m_req;
    /// Property: String containing node_name::UTGID of event data provider task
    std::string          m_input    {"Event"};

    /// Monitoring quantity: Number of events received from buffer manager
    long                 m_eventsIN = 0;
    /// Monitoring quantity: Number of events sent to client(s)
    long                 m_eventsOUT = 0;
    /// Reference to MBM consumer to access event data
    MBM::Consumer*       m_consumer = 0;
    /// Reference to network plug to send events
    TRANSFER_NS::NET*    m_netPlug  = 0;
    /// Container of currently waiting event receivers
    Recipients           m_recipients;
    /// Event flag to wake up the server when a new request arrives
    lib_rtl_event_t      m_suspend  {0};
    /// Lock to protect internal data structures
    std::mutex           m_lock;
    /// Parses requirement data buffer
    MBM::Requirement     m_request;
    /// State variable of the consumer obejct
    int                  m_consState = WAIT_REQ;
    /// Flag to indicate a pending event request
    int                  m_have_request = 0;

  public:
    /// Standard Constructor
    EventServer(const std::string& nam, Context& svcLoc);
    /// Standard Destructor
    virtual ~EventServer();
    /// DataflowComponent implementation: initialize the service
    virtual int  initialize()  override;
    /// DataflowComponent implementation: start the service
    virtual int  start()  override;
    /// DataflowComponent implementation: stop the service
    virtual int  stop()  override;
    /// DataflowComponent implementation: finalize the service
    virtual int  finalize()  override;
    /// DataflowComponent implementation: cancel the service
    virtual int  cancel()  override;
    /// Incident handler implemenentation: Inform that a new incident has occured
    virtual void handle(const DataflowIncident& inc)  override;
    /// IRunable implementation : Run the class implementation
    virtual int run()  override;
    /// Remove a given target process from the list of pending clients
    virtual void removeTarget(const std::string& src);
    /// Callback to handle a new request of a client to receive a given event
    virtual void handleEventRequest(const TRANSFER_NS::netheader_t& hdr, void* ptr);
    /// Rescan client tables, reformulate possibly pending requests and renew the request
    virtual void restartRequests();
    /// Send event data to a list of waiting clients
    virtual int sendEvent();
    /// Shutdown all event requests from the network
    virtual void shutdownRequests();
    /// Accessor the netplu object
    TRANSFER_NS::NET* netPlug() const {  return m_netPlug; }
  };

  /** @class EventRequestServerRunable EventRequestServerRunable.h Dataflow/EventRequestServerRunable.h
   *
   * Runable to serv independent network clients with event
   * data. requests are purely served on demand ie. each
   * client has to ask for every event to be processed.
   * 
   * Contrary to the base class only events the request type 
   * of the server requirement is served. Clients can only get 
   * events without specifying special trigger masks etc.
   *
   *  @author Markus Frank
   *  @version 1.0
   */
  class EventRequestServer : public EventServer  {
  public:
    /// Standard Constructor
    EventRequestServer(const std::string& nam, Context& svcLoc);
    /// Standard Destructor
    virtual ~EventRequestServer();
    /// DataflowComponent implementation: start the service
    //virtual int start()  override;

    /// Rescan client tables, reformulate possibly pending requests and renew the request
    virtual void restartRequests()  override;
    /// Send event data to a list of waiting clients
    virtual int sendEvent()  override;
  };
}      // End namespace Online
#endif // ONLINE_DATAFLOW_EVENTSERVER_H

//====================================================================
//  EventServer
//--------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Description: Runable to serv independent network clients with event
//               data. requests are purely served on demand ie. each
//               client has to ask for every event to be processed.
//
//  Author     : M.Frank
//  Created    : 4/10/2016
//
//====================================================================
#include <Dataflow/Incidents.h>
#include <Dataflow/MBMClient.h>
#include <WT/wt_facilities.h>
#include <RTL/bits.h>

using namespace std;
using namespace Online;
using namespace TRANSFER_NS;

namespace event_server_helpers  {
  static void handle_death(const netheader_t& hdr, void* param, netentry_t*)    {  
    EventServer* p = (EventServer*)param;
    p->removeTarget(hdr.name);
  }

  static void handle_req(const netheader_t& hdr, void* param, netentry_t* entry)  {
    EventServer* p = (EventServer*)param;
    try  {
      p->handleEventRequest(hdr, entry);
    }
    catch(...)  {
    }
  }
}

// Standard Constructor
EventServer::EventServer(const string& nam, Context& svcLoc)   
: Component(nam, svcLoc)
{
  declareProperty("EvtMax",        m_evtMax);
  declareProperty("REQ",           m_req);
  declareProperty("Input",         m_input);
  declareProperty("PrintNum",      m_printNum);
  declareProperty("NumThreads",    m_nThreads);
  declareProperty("ChangeRequest", m_changeRequest);
  ::lib_rtl_create_event(0,&m_suspend);
}

// Standard Destructor
EventServer::~EventServer()  {
  ::lib_rtl_delete_event(m_suspend);
  m_suspend = 0;
}

// DataflowComponent implementation: initialize the service
int EventServer::initialize()   {
  int sc = Component::initialize();
  if ( sc != DF_SUCCESS )  {
    return error("Failed to initialize service base class.");
  }
  std::string self = RTL::processName();
  m_netPlug = net_init(self, m_nThreads, NET_SERVER);
  net_subscribe(netPlug(), this,
		WT_FACILITY_CBMREQEVENT,
		event_server_helpers::handle_req,
		event_server_helpers::handle_death);
  subscribeIncident("DAQ_CANCEL");
  declareMonitor("Events","IN",m_eventsIN=0,"Number of events processed");
  declareMonitor("Events","OUT",m_eventsOUT=0,"Number of events processed");
  declareMonitor("RequestPending",m_have_request,"Flag if event request is pending");
  //::lib_rtl_clear_event(m_suspend);
  return sc;
}

// DataflowComponent implementation: start the service
int EventServer::start()   {
  int sc = Component::start();
  if ( sc == DF_SUCCESS )  {
    lock_guard<mutex> lock(m_lock);
    m_request.parse(m_req);
    this->m_consumer = context.mbm->createConsumer(m_input,RTL::processName());
    if ( !this->m_consumer )  {
      return error("Failed to create consumer for MBM buffer:%s",m_input.c_str());
    }
    // If we do not constantly update the requests, we have to issue it at least once.
    if ( !m_changeRequest )   {
      this->m_consumer->addRequest(m_request);
      m_have_request = 1;
    }
    m_consState = WAIT_REQ;
    return sc;
  }
  return error("Failed to start service base class.");
}

// DataflowComponent implementation: stop the service
int EventServer::stop()   {
  int sc = Component::stop();
  if ( sc != DF_SUCCESS )  {
    return error("Failed to stop service base class.");
  }
  if ( this->m_consumer ) {
    MBM::Consumer* cons = nullptr;
    lock_guard<mutex> lock(m_lock);
    if ( this->m_consumer )    {
      cons = this->m_consumer;
      this->m_consumer = nullptr;
      try  {
	if ( m_have_request )  {
	  m_have_request = 0;
	  cons->delRequest(m_request);
	}
      }
      catch(...)   {
      }
      detail::deletePtr(cons);
    }
    m_recipients.clear();
  }
  return sc;
}

// DataflowComponent implementation: finalize the service
int EventServer::finalize()     {
  unsubscribeIncidents();
  shutdownRequests();
  m_recipients.clear();
  detail::deletePtr(this->m_consumer);
  undeclareMonitors();
  undeclareMonitors("Events");
  return Component::finalize();
}

/// DataflowComponent implementation: cancel the service
int EventServer::cancel()  {
  m_consState = DONE;
  if ( this->m_consumer ) {
    this->m_consumer->cancel();
  }
  // First clear pending event flags, then set the event, so that the loop exits
  ::lib_rtl_clear_event(m_suspend);
  ::lib_rtl_set_event(m_suspend);
  return DF_SUCCESS;
}

/// Shutdown all event requests from the network
void EventServer::shutdownRequests() {
  if ( netPlug() )  {
    net_unsubscribe(netPlug(),this,WT_FACILITY_CBMREQEVENT);
    net_close(netPlug());
  }
  m_recipients.clear();
  m_netPlug = 0;
}

/// Incident handler implemenentation: Inform that a new incident has occured
void EventServer::handle(const DataflowIncident& inc)    {
  info("Got incident: %s of type:%s.",inc.name.c_str(),inc.type.c_str());
  if ( inc.type == "DAQ_CANCEL" )  {
    // cancel();
  }
  else if ( inc.type == "DAQ_ENABLE" )  {
    m_consState = m_recipients.empty() ? WAIT_REQ : WAIT_EVT;
  }
}

void EventServer::removeTarget(const string& src)   {
  lock_guard<mutex> lock(m_lock);
#ifdef EventServer_USE_MAP
  Recipients::iterator j = m_recipients.find(src);
#else
  Recipients::iterator j = m_recipients.end();
  for(Recipients::iterator i=m_recipients.begin(); i!=m_recipients.end(); ++i)  {
    if ( src == (*i).first ) {
      j = i;
      break;
    }
  }
#endif
  if ( j != m_recipients.end() )   {
    try {
      m_recipients.erase(j);
      if ( m_consState != DONE ) {
        restartRequests();
      }
    }
    catch(...) {
      error("Exception in removeTarget from %s",src.c_str());
    }
  }
}

/// Handle request from new network data consumer
void EventServer::handleEventRequest(const netheader_t& hdr, void* ptr)   {
  bool empty;
  char buff[2048];
  string src = hdr.name;
  netentry_t* e = (netentry_t*)ptr;
  MBM::Requirement* r = (MBM::Requirement*)buff;
  int sc = net_receive(netPlug(), e, buff, hdr.size);
  if ( sc == NET_SUCCESS )  {
    //info("Got event request from "+src);
    try {
      lock_guard<mutex> lock(m_lock);
      empty = m_recipients.empty();
#ifdef EventServer_USE_MAP
      m_recipients[src] = make_pair(*r,e);
#else
      // We only allow one request per client
      bool insert = true;
      for (const auto& rec : m_recipients )   {
	if ( rec.first == src ) {
	  insert = false;
	  break;
	}
      }
      if ( insert )   {
	m_recipients.push_back(make_pair(src,make_pair(*r,e)));
      }
      else  {
	info("IGNORE Request from %s",src.c_str());
      }
#endif
      if ( empty && m_consState != DONE )  {
	::lib_rtl_set_event(m_suspend);
      }
      //info("Received event request from "+src);
    }
    catch(...) {
      info("Exception in handleEventRequest from %s",src.c_str());
    }
  }
}

/// Restart event request handling
void EventServer::restartRequests()  {
  MBM::Requirement old = m_request;
  for(int j=0; j<BM_MASK_SIZE;++j)  {
    m_request.trmask[j] = 0;
    m_request.vetomask[j] = ~0;
  }
  for(Recipients::iterator i=m_recipients.begin(); i!=m_recipients.end(); ++i)  {
    MBM::Requirement& r = (*i).second.first;
    for(int k=0; k<BM_MASK_SIZE; ++k)  {
      m_request.trmask[k] |= r.trmask[k];
      m_request.vetomask[k] &= r.vetomask[k];
    }
  }
  //debug("restartRequests: Recipients:%d\n", int(m_recipients.size()));
  if ( !this->m_consumer )   {
    return;
  }
  if ( m_have_request && m_changeRequest )   {
    this->m_consumer->delRequest(old);
    m_have_request = 0;
  }
  if ( m_recipients.size() > 0 )  {
    // If there is a valid mask, we add a new request
    for(int j=0; j<BM_MASK_SIZE;++j)  {
      if ( m_request.trmask[j] != 0 )  {
	if ( m_changeRequest )   {
	  this->m_consumer->addRequest(m_request);
	  m_have_request = 1;
	}
	m_consState = WAIT_EVT;
	return;
      }
    }
  }
}

int EventServer::sendEvent()  {
  int cnt = 0;
  lock_guard<mutex> lock(m_lock);
  std::vector<std::string> bad_or_served_guys;
  const MBM::EventDesc& e = this->m_consumer->event();
  for(auto i=m_recipients.begin(); i!=m_recipients.end(); )  {
    MBM::Requirement& r = (*i).second.first;
    //if ( e.type == r.evtype )  {
      if ( mask_or_ro2(e.mask, r.trmask,sizeof(e.mask)/sizeof(e.mask[0])) )  {
        if ( !mask_and_ro2(e.mask,r.vetomask,sizeof(e.mask)/sizeof(e.mask[0])) )  {
          int sc = net_send(m_netPlug,e.data,e.len,(*i).first,WT_FACILITY_CBMEVENT);
          if ( sc==NET_SUCCESS )   {
            bad_or_served_guys.push_back((*i).first);
	    ++m_eventsOUT;
            ++cnt;
          }
	  else if ( sc == NET_TASKNOTFOUND )  {
            bad_or_served_guys.push_back((*i).first);
	    error("Cannot Send event data to %s rc=%d  NET_TASKNOTFOUND Client removed.",(*i).first.c_str(), sc);
	  }
	  else if ( sc == NET_TERRIBLE )  {
            bad_or_served_guys.push_back((*i).first);
	    error("Cannot Send event data to %s rc=%d  NET_TERRIBLE Client removed.",(*i).first.c_str(), sc);
	  }
	  else if ( sc == NET_CONNCLOSED )  {
            bad_or_served_guys.push_back((*i).first);
	    error("Cannot Send event data to %s rc=%d  NET_CONNCLOSED Client removed.",(*i).first.c_str(), sc);
	  }
	  else if ( sc == NET_TASKDIED )   {
            bad_or_served_guys.push_back((*i).first);
	    error("Cannot Send event data to %s rc=%d  NET_TASKDIED Client removed.",(*i).first.c_str(), sc);
	  }
	  else if ( sc == NET_MSG_TERMINATE )   {
            bad_or_served_guys.push_back((*i).first);
	    error("Cannot Send event data to %s rc=%d  NET_MSG_TERMINATE Client removed.",(*i).first.c_str(), sc);
	  }
	  else  {
            bad_or_served_guys.push_back((*i).first);
	    error("Cannot Send event data to %s rc=%d  UNKNOWN Client removed.",(*i).first.c_str(), sc);
	  }
	  break;
        }
      }
      //}
    ++i;
  }
  for(auto i=bad_or_served_guys.begin(); i!=bad_or_served_guys.end(); ++i)   {
#ifdef EventServer_USE_MAP
    Recipients::iterator j=m_recipients.find(*i);
#else
    Recipients::iterator j=m_recipients.begin();
    for(; j!=m_recipients.end(); ++j)
      if ( (*j).first == *i ) break;
#endif
    if ( j != m_recipients.end() ) m_recipients.erase(j);
  }
  if ( m_recipients.empty() )  {
    if ( this->m_consumer && m_changeRequest )   {
      this->m_consumer->delRequest(m_request);
      m_have_request = 0;
    }
    ::lib_rtl_clear_event(m_suspend);
  }
  if ( cnt==0 )   {
    //error("No mask match possible!");
  }
  return cnt>0 ? DF_SUCCESS : DF_ERROR;
}

/// IRunable implementation : Run the class implementation
int EventServer::run()   {
  int  sc;
  bool empty;
  int  printN = 0;
  while ( 1 )  {
    m_lock.lock();
    switch(m_consState)  {
    case WAIT_REQ:
      //info("Wait for event requests......");
      if ( m_consState == DONE )  {
        info("End of event requests reached. Stopping...");
        ::lib_rtl_clear_event(m_suspend);
        m_lock.unlock();
        return DF_SUCCESS;
      }
      empty = m_recipients.empty();
      if ( empty ) ::lib_rtl_clear_event(m_suspend);
      // Unlock now and allow for new requests being added
      m_lock.unlock();
      if ( empty && m_consState != DONE ) {
        ::lib_rtl_wait_for_event(m_suspend);
      }
      // Check if we got a cancel in the meantime
      if ( m_consState == DONE )  {
        info("End of event requests reached. Stopping...");
        ::lib_rtl_clear_event(m_suspend);
        return DF_SUCCESS;
      }
      restartRequests();
      break;
    case WAIT_EVT:
      m_lock.unlock();
      sc = this->m_consumer->getEvent();
      ++m_eventsIN;
      if ( sc == MBM_REQ_CANCEL )  {
	// We got a cancel. Hence the next command shall be stop.
        m_lock.unlock();
        return DF_SUCCESS;
      }
      else if ( m_consState == DONE )  {
	// We got a cancel, but maybe there is still an event present,
	// which needs to be freed.
	this->m_consumer->freeEvent();
        m_lock.unlock();
        return DF_SUCCESS;
      }
      else if ( sc == MBM_NORMAL )  {
        try {
	  m_lock.unlock();
          sendEvent();
          if ( ((++printN)%m_printNum) == 0 ) {
            info("Delivered %9d events to clients.",printN);
          }
        }
        catch(...)  {
          info("Exception....");
        }
	this->m_consumer->freeEvent();
        if ( m_consState == DONE )  {
          return DF_SUCCESS;
        }
        m_consState = WAIT_REQ;
      }
      break;
    case DONE:
      m_lock.unlock();
      return DF_SUCCESS;
    }
  }
}

// Standard Constructor
EventRequestServer::EventRequestServer(const std::string& nam, Context& svc)   
: EventServer(nam, svc)
{
  m_changeRequest = false;
}

/// Standard Destructor
EventRequestServer::~EventRequestServer()  {
}

/// Send event data to a list of waiting clients
int EventRequestServer::sendEvent()  {
  const MBM::EventDesc& e = this->m_consumer->event();
  lock_guard<mutex> lock(m_lock);
  Recipients& targets = m_recipients;
  for(auto& i : targets ) {
    int sc = net_send(m_netPlug,e.data,e.len,i.first,WT_FACILITY_CBMEVENT);
    if ( sc != NET_SUCCESS )   {
      error("Cannot Send event data to "+i.first);
    }
    if ( !this->m_consumer ) break;
  }
  m_recipients.clear();
  return DF_SUCCESS;
}

/// Restart event request handling
void EventRequestServer::restartRequests()  {
  if ( this->m_consumer && !m_recipients.empty() )  {
    m_consState = WAIT_EVT;
  }
}

/// Declare factory entries:
#include <Dataflow/Plugins.h>
DECLARE_DATAFLOW_COMPONENT_NS(Online,EventServer)
DECLARE_DATAFLOW_COMPONENT_NS(Online,EventRequestServer)
