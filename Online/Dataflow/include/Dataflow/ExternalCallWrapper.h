//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : Online/GaudiOnlineTests
//
//  Author     : Markus Frank
//==========================================================================
#ifndef DATAFLOW_EXTERNALCALL_WRAPPER_H
#define DATAFLOW_EXTERNALCALL_WRAPPER_H

// Include files from the framework
#include <Dataflow/DataflowComponent.h>
#include <Dataflow/Incidents.h>
#include <CPP/Interactor.h>

/// C/C++ include files

/// Online namespace declaration
namespace Online    {

  /// Generic class to wrapp standalone non-dataflow executable within dataflow application
  /** Generic class to wrapp standalone non-dataflow executable within dataflow application
   *
   * \version 1.0
   * \author  M.Frank
   */
  class ExternalCallWrapper : public DataflowComponent, public CPP::Interactor  {
  public:

    /// Command enumerator
    enum _commands {
      CMD_RUNNING = 1,
      CMD_PAUSE,
      CMD_CANCEL,
      CMD_START_TRIGGER,
      CMD_STOP_TRIGGER
    };
    
    /// Property: transition name when the process to be started
    std::string               proc_start;
    /// Property: transition name when the process to be stopped
    std::string               proc_stop;
    
    /// Start the process using the stub
    virtual int start_external_call(const std::string& when) = 0;
    /// Stop the process using the stub
    virtual int stop_external_call(const std::string& when) = 0;

    virtual int initialize_external_call();
  public:
    /// Default constructor
    ExternalCallWrapper(const std::string& name, Context& ctxt);

    /// Standard destructor
    virtual ~ExternalCallWrapper() = default;

    /// Initialize the component
    virtual int initialize()  override;

    /// Start the component
    virtual int start()  override;

    /// Stop the component
    virtual int stop()  override;

    /// Finalize the component
    virtual int finalize()  override;

    /// Enable the data flow component. 
    virtual int enable()  override;
    
    /// Cancel the data flow component. 
    virtual int cancel()  override;
    
    /// Pause the data flow component. 
    virtual int pause()  override;

    /// Incident handler implemenentation: Inform that a new incident has occured
    virtual void handle(const DataflowIncident& inc)  override;
    
    /// Interactor overload: Command handler
    virtual void handle(const CPP::Event& ev)  override;
  };
}    // End namespace Online
#endif // DATAFLOW_EXTERNALCALL_WRAPPER_H
