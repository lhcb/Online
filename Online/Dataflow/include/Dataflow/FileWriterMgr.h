//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  Filewriter.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Beat Jost
//==========================================================================
#ifndef ONLINE_DATAFLOW_FILEWRITEMGR_H
#define ONLINE_DATAFLOW_FILEWRITEMGR_H 1

// Framework include files
#include <Dataflow/DataflowComponent.h>
#include <MBM/Requirement.h>

// C/C++ include files
#include <vector>
#include <list>

// Forward declarations
namespace MBM  {
  class Consumer;
  class EventDesc;
}

/// Online namespace declaration
namespace Online   {

  /// Forward declarations
  class FileWriter;

  /// Writer manager class definition
  class FileWriterMgr: public DataflowComponent  {
  public:

    /// Event processor class definition
    class Processor : public DataflowComponent {
    public:
      typedef std::vector<std::string>      Requirements;
      typedef std::vector<MBM::Requirement> BinRequirements;

    protected:
      /// Property: Request specification
      Requirements    m_req;
      BinRequirements m_breq;

    public:
      Processor(const std::string& nam, Context& ctxt);
      virtual ~Processor();
      /// Service overload: initialize()
      virtual int initialize()  override;
      /// Service overload: finalize()
      virtual int finalize()  override;
      virtual std::vector<std::string> getRequirements() const;
      virtual bool matchRequirements(const Context::EventData& e)  const;
      virtual int  execute(const Context::EventData& e) override = 0;
    };

    typedef std::list<Processor*>           ProcList;
    typedef std::vector<std::string>        ProcessorList;
    typedef std::vector<std::string>        Requirements;

  protected:
    MBM::Consumer*   m_consumer;
    /// Property: Request specification
    MBM::Requirement m_requirement;
    /// List of attached components
    ProcList         m_procList;
    /// Property: List of sub-components to be called
    ProcessorList    m_processors;
    /// Property: Name of the data input buffer
    std::string      m_input;
    /// Property: Requirement as job option in string form
    std::string      m_req;
    /// Monitoring quantity: Number of Event in
    long             m_evtIn;
    /// Monitoring quantity: Number of input frames from MBM
    long             m_framesIn;
    /// Flag to cancel properly
    bool             m_receiveEvts;

  public:
    /// Standard Constructor
    FileWriterMgr(const std::string& name, Context& context);
    /// Standard Destructor
    virtual ~FileWriterMgr();
    /// Service overload: initialize()
    virtual int initialize()  override;
    /// Start the dataflow component
    virtual int start()  override;
    /// Stop the dataflow component
    virtual int stop()  override;
    /// Service overload: finalize()
    virtual int finalize()  override;
    /// Cancel the data flow component. 
    virtual int cancel()  override;
    /// Enable the data flow component. 
    virtual int enable()  override;
    /// Process events. Input buffer is ALWAYS in mdf bank format.
    virtual int execute(const Context::EventData& event)  override;
    /// Process event loop as main runable
    virtual int run()  override;
  };
}      // End namespace Online
#endif //  ONLINE_DATAFLOW_FILEWRITEMGR_H
