//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  MBMWriter.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_MBMWRITER_H
#define ONLINE_DATAFLOW_MBMWRITER_H

// Framework include files
#include <Dataflow/DataflowComponent.h>

// Forward declarations
namespace MBM {  class Producer;  }

///  Online namespace declaration
namespace Online  {

  // Forward declarations

  /// MBMWriter to group coherent action sequences
  /**
   *
   * @author  Markus Frank
   * @version 1.0
   */
  class MBMWriter : public DataflowComponent  {
  protected:
    /// Property: Buffer name
    std::string    m_buffer;
    /// Reference to producer object
    MBM::Producer* m_producer;
    /// Helper variable to indicate if we are waiting for MBM actions
    bool           m_waiting;
  public:
    /// Initializing constructor
    MBMWriter(const std::string& name, Context& ctxt);
    /// Default destructor
    virtual ~MBMWriter();
    /// Start the data flow component. Default implementation is empty.
    virtual int start()  override;
    /// Stop the data flow component. Default implementation is empty.
    virtual int stop()  override;
     /// Cancel the data flow component. 
    virtual int cancel()  override;
   /// Data processing overload: process event
    virtual int execute(const Context::EventData& event)  override;
  };
}      // end namespace Online
#endif //  ONLINE_DATAFLOW_MBMWRITER_H
