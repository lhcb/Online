//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  OutputLogger.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_OUTPUTLOGGER_H
#define ONLINE_DATAFLOW_OUTPUTLOGGER_H

/// Framework include files
#include <Dataflow/DataflowComponent.h>
#include <RTL/Logger.h>

/// C/C++ include files
#include <memory>

/// Forward declarations
class SvcCatalog;

///  Online namespace declaration
namespace Online  {

  /// Output Logger service for the dataflow
  /** @class OutputLogger OutputLogger.h Dataflow/OutputLogger.h
   *
   *
   * @author  Markus Frank
   * @version 1.0
   */
  class OutputLogger : public DataflowComponent, public DataflowContext::Logger  {
  protected:
    /// Property: Output format
    std::string m_fmt;
    /// Property: Path to output device (file, fifo, ...)
    std::string m_logDevice;
    /// Property: Name of the logger device type
    std::string m_logDeviceType;

    /// Reference to logger device
    std::shared_ptr<RTL::Logger::LogDevice> m_logDev;

  public:
    /// Initializing constructor
    OutputLogger(const std::string& name, Context& framework);
    /// Default destructor
    virtual ~OutputLogger();
    /// Initialize the data flow component. Default implementation is empty.
    virtual int initialize()  override;
    /// Finalize the data flow component. Default implementation is empty.
    virtual int finalize()  override;
    /// Write output .....
    virtual size_t print(int severity, const char* source, const char* fmt, ...)  override;
    /// Write output .....
    virtual size_t print(int severity, const char* source, const char* fmt, va_list& args)  override;
  };

}      // end namespace Online

#endif //  ONLINE_DATAFLOW_OUTPUTLOGGER_H
