//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  Properties.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_PROPERTIES_H
#define ONLINE_DATAFLOW_PROPERTIES_H

// Framework include files
#include <DD4hep/Primitives.h>
#include <DD4hep/ComponentProperties.h>

// If we do not redifine the DD4hep namespace, we have to import it!
#ifndef dd4hep
namespace Online  {
  // We import EVERYTHING onto the Online namespace!
  using namespace dd4hep;
}
#endif

#endif //  ONLINE_DATAFLOW_PROPERTIES_H
