//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  PropertyPrintout.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_PROPERTYPRINTOUT_H
#define ONLINE_DATAFLOW_PROPERTYPRINTOUT_H

// Framework include files
#include <Dataflow/Properties.h>
#include <DD4hep/Printout.h>

///  Online namespace declaration
namespace Online  {

  /// Functor to print properties of a component
  /**
   *  \author  M.Frank
   *  \version 1.0
   *  \ingroup DD4HEP_SIMULATION
   */
  class PropertyPrintout  {
  protected:
    /// Reference to component name
    const std::string& name;

  public:
    /// Constructor
    PropertyPrintout(const std::string& c);
    /// Operator for print action
    void operator()(const std::pair<std::string,Property>& p)  {
      printout(INFO, name, "%s.%s = %s", name.c_str(),
	       p.first.c_str(), p.second.str().c_str());
    }
  };
  inline PropertyPrintout::PropertyPrintout(const std::string& c) : name(c)  {
  }

}      // end namespace Online

#endif //  ONLINE_DATAFLOW_PROPERTYPRINTOUT_H
