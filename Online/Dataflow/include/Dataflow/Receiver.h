//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  Receiver.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_RECEIVER_H
#define ONLINE_DATAFLOW_RECEIVER_H

// Framework include files
#include <Dataflow/DataflowComponent.h>
#include <MBM/Producer.h>
#include <RTL/rtl.h>
#include <memory>
#include <mutex>

///  Online namespace declaration
namespace Online  {

  /// Buffer Manager service for the dataflow
  /** @class Receiver Receiver.h Dataflow/Receiver.h
   *
   *  This class handles - depending on the setup - all work to be done in
   *  order to transfer data over the network.
   *  The algorithm implements a request scheme, which allows clients to
   *  request a single event, which will be served on a first-in-first-derved basis.
   *
   *  This partial implementation handles the riquired functionality, but does not
   *  implement the actual networking capabilities. An suitable network messaging
   *  facility, which is name based may be used for a complete implementation.
   *
   * Note:
   * 1) The consumer executes on the monitoring farm.
   *    Consumer means "consumer of network data"
   * 2) The producer executes on the storage layer.
   *    The producer produces data to the network.
   *
   * It is _always_ the consumer who is responsible for the
   * data request. The the producer dies, it's the consumer
   * which must ask for the data _again_.
   *
   *
   *  +-------------------+  +---------------------+
   *  | NetworkDataSender |  | NetworkDataReceiver |
   *  +-------------------+  +---------------------+
   *         |                           |                     Initialization:
   *         |  subscribeNetwork()       |                     
   *         +-------------------------->+  addDataSource()    Producer sends data-sink request
   *         |                           +-----+               to sink. Sink will add the data
   *         |                           |     |               source.
   *         |                           +<----+               Then data source suspends access
   *         +-----+                     |                     to MBM events by suspending the
   *         |     | suspendEvents()     |                     event selector until a requests
   *         |<----+                     |                     for an event from the data sink. 
   *         |                           |                     is received.
   *         |         rearmNetRequest() |     
   *         +<--------------------------+                     Upon request receival the sender
   *         |                           |                     resumes access to events from MBM
   *         +-----+                     |
   *         |     |  resumeEvents()     |
   *         +<----+                     |
   *         |                           |
   *  ====================================================
   *         |                           |
   *  +----->+                           |     
   *  |      |                           |                     Event loop execution:
   *  |      +-----+                     |
   *  |      |     |  resumeEvents()     |
   *  |      +<----+                     |                     When an event is received
   *  |      |                           |                     (execute is called), the data
   *  |      | execute/sendData()        |                     are sent to the sink and the
   *  |      +-------------------------->+                     access to events is again
   *  |      |                           |                     suspended until a new request
   *  |      | execute/suspendEvents()   |                     arrives (rearm by receiver)
   *  |      +-----+                     |                     and the loop continues....
   *  |      |     |                     |
   *  |      +<----+                     |
   *  |      |                           |
   *  |      |         rearmNetRequest() |     
   *  |      +<--------------------------+
   *  |      |
   *  +------+
   *
   * @author  Markus Frank
   * @version 1.0
   */
  class Receiver : public DataflowComponent  {
  protected:
    struct RecvEntry  {
      std::string              name, service;
      long                     identifier  { 0 };
      Receiver*                handler { nullptr };
      std::unique_ptr<char []> buffer  {   };
      size_t                   size    { 0 };
      RecvEntry() = default;
      RecvEntry(RecvEntry&& e);
      RecvEntry(const RecvEntry& e) = delete;
      RecvEntry(Receiver* h, const std::string& n, const std::string& s, long id);
      RecvEntry& operator=(RecvEntry&& e);
      RecvEntry& operator=(const RecvEntry& e) = delete;
      std::unique_ptr<Receiver::RecvEntry> clone()  const;
    };
    typedef std::vector<RecvEntry>                Receivers;
    /// Property: [Consumer] MBM buffer name
    std::string     m_buffer;
    /// Property: Require event request from source before sending
    bool            m_useRequests   = false;
    /// Property: Declare event asynchronously to MBM after receiving
    bool            m_declareAsynch = false;
    /// Property: Additional AND mask to declare event (default: 0)
    int             m_routingMask   = 0;
    /// Property: Additional OR mask to declare event (default: 0)
    int             m_vetoMask      = 0;
    /// Property: Sleep interval on error
    int             m_errorDelay    = 100;

    /// Monitoring item: Total number of events received
    long            m_recvReq       = 0; 
    /// Monitoring item: Total number of events declared to MBM
    long            m_evtDecl       = 0; 
    /// Monitoring item: Total number of receive errors
    long            m_recvError     = 0;
    /// Monitoring item: Total number of bytes received from clients
    long            m_recvBytes     = 0;
    /// Monitoring item: Backlog of buffered events
    int             m_backlog       = 0;

    /// Do not issue backlog messages too often
    int             m_lastbacklog   = 0;
    /// Flag track event receiving loop.
    bool            m_recvEvents    = false;
    /// Pointer to MBM producer, where received data should be delivered.
    MBM::Producer*  m_prod          = 0;
    /// [Network Consumer] Queue of data sources
    Receivers       m_receivers;
    /// Mutex to protect producer from unsolicited cancel requests
    std::mutex      m_prodLock;

    /// WT callback for asynchronous request rearm
    static int rearm_net_request(unsigned int facility,void* param);
    /// Retrieve receiver entry by name
    virtual const RecvEntry* receiver(const std::string& nam)  const;
    /// Rearm network request for a single event source
    virtual int rearmRequest(const RecvEntry& src);

  public:
    /// Initializing constructor
    Receiver(const std::string& name, Context& ctxt);
    /// Default destructor
    virtual ~Receiver() = default;
    /// Initialize the data flow component
    virtual int initialize()  override;
     /// Start the data flow component
    virtual int start()  override;
    /// Stop the data flow component
    virtual int stop()  override;
     /// Finalize the data flow component
    virtual int finalize()  override;
    /// Incident handler implemenentation: Inform that a new incident has occured
    void handle(const DataflowIncident& inc)  override;

    /// Access to receivers queue
    const Receivers& receivers();
    /// Callback on task dead notification
    virtual int taskDead(const std::string& task_name);
    /// Networking layer overload: Subscribe to network requests
    virtual int subscribeNetwork() = 0;
    /// Networking layer overload: Unsubscribe from network requests
    virtual int unsubscribeNetwork() = 0;

    /// Networking layer overload [Net consumer]: Formulate network request for a single event source
    virtual int createNetRequest(const RecvEntry& src);
    /// Networking layer overload [Net consumer]: Reset event request
    virtual int resetNetRequest(const RecvEntry& src);
    /// Networking layer overload [Net consumer]: Rearm network request for a single event source
    virtual int rearmNetRequest(const RecvEntry& src) = 0;
    /// Networking layer overload [Net consumer]: Cancel network request for a single event source
    virtual void deleteNetRequest(RecvEntry& src);
    /// Networking layer overload [Net consumer]: Reset event request and insert entry into data queue of the buffer manager
    virtual int handleData(const RecvEntry& client, void* context, std::unique_ptr<char []>&& buffer, size_t len);

    /// Set MBM masks to producer declare data
    void setMbmMask(MBM::EventDesc& e)   const;
    /// Copy event data into buffer manager
    virtual int copyEventData(void* context, void* to, const std::unique_ptr<char []>& buffer, size_t len);
    /// Networking layer overload [Net consumer]: Handle event data source registration
    virtual std::pair<int, const RecvEntry*>
    handleRequest(int clientID,const std::string& source,const std::string& svc);
    /// Networking layer overload [Net consumer]: Handle event declaration into the buffer manager
    virtual int declareData(const RecvEntry& client, void* context, std::unique_ptr<char []>&& buffer, size_t len);
    /// Networking layer overload [Net producer+consumer]: Cancel current I/O
    virtual int cancelNetwork() = 0;
  };

  inline const Receiver::Receivers& Receiver::receivers()  {
    return m_receivers;
  }

  inline Receiver::RecvEntry::RecvEntry(Receiver* h, const std::string& n, const std::string& s, long id)
    : name(n), service(s), identifier(id), handler(h) {
  }
  
  inline Receiver::RecvEntry::RecvEntry(RecvEntry&& e)
    : name(e.name), service(e.service), identifier(e.identifier), handler(e.handler) {
  }

  inline Receiver::RecvEntry& Receiver::RecvEntry::operator=(RecvEntry&& e)  {
    if ( this != &e )  {
      name       = e.name;
      service    = e.service;
      identifier = e.identifier;
      handler    = e.handler;
      buffer.release();
      size = 0;
    }
    return *this;
  }

}      // end namespace Online

#endif //  ONLINE_DATAFLOW_RECEIVER_H
