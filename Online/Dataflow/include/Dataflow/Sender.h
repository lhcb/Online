//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  Sender.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_SENDER_H
#define ONLINE_DATAFLOW_SENDER_H

// Framework include files
#include <Dataflow/DataflowComponent.h>
#include <RTL/rtl.h>

// C/C++ include files
#include <mutex>
#include <list>

///  Online namespace declaration
namespace Online  {

  /// Buffer Manager service for the dataflow
  /** @class Sender Sender.h Dataflow/Sender.h
   *
   *  This class handles the evacuation of events from the HLT farm to the storage layer.
   *
   *
   *  +-------------------+  +---------------------+
   *  | NetworkDataSender |  | NetworkDataReceiver |
   *  +-------------------+  +---------------------+
   *         |                           |                     Initialization:
   *         |  subscribeNetwork()       |                     
   *         +-------------------------->+  addDataSource()    Producer sends data-sink request
   *         |                           +-----+               to sink. Sink will add the data
   *         |                           |     |               source.
   *         |                           +<----+               Then data source suspends access
   *         +-----+                     |                     to MBM events by suspending the
   *         |     | suspendEvents()     |                     event selector until a requests
   *         |<----+                     |                     for an event from the data sink. 
   *         |                           |                     is received.
   *         |         rearmNetRequest() |     
   *         +<--------------------------+                     Upon request receival the sender
   *         |                           |                     resumes access to events from MBM
   *         +-----+                     |
   *         |     |  resumeEvents()     |
   *         +<----+                     |
   *         |                           |
   *  ====================================================
   *         |                           |
   *  +----->+                           |     
   *  |      |                           |                     Event loop execution:
   *  |      +-----+                     |
   *  |      |     |  resumeEvents()     |
   *  |      +<----+                     |                     When an event is received
   *  |      |                           |                     (execute is called), the data
   *  |      | execute/sendData()        |                     are sent to the sink and the
   *  |      +-------------------------->+                     access to events is again
   *  |      |                           |                     suspended until a new request
   *  |      | execute/suspendEvents()   |                     arrives (rearm by receiver)
   *  |      +-----+                     |                     and the loop continues....
   *  |      |     |                     |
   *  |      +<----+                     |
   *  |      |                           |
   *  |      |         rearmNetRequest() |     
   *  |      +<--------------------------+
   *  |      |
   *  +------+
   *
   * @author  Markus Frank
   * @version 1.0
   */
  class Sender : public DataflowComponent  {
  public:
    /** @class Sender::Recipient Sender.h GaudiOnline/Sender.h
      *
      */
    class Recipient  {
    public:
      std::string name       { };
      long        identifier { 0 };
      Sender*     handler    { nullptr };

      Recipient();
      Recipient(const Recipient& e);
      Recipient(Sender* h, const std::string& n, long id);
      Recipient& operator=(const Recipient& e);
    };
    /// Definition of recipient queue
    typedef std::list<Recipient>     Recipients;
    /// Defintiion of data sinks queue
    typedef std::vector<std::string> DataSinks;

  protected:
    /// Property: [Producer] Data sink name
    std::string         m_target;
    /// Property: [Producer] Multiple data sinks
    DataSinks           m_sinks;
    /// Property: [Producer] Require event request from source before sending
    bool                m_useEventRequests;
    /// Property: [Sender] Retry N times to re-establish the connection if sending fails
    int                 m_retrySending;
    /// Property: [Sender] Invoke DAQ_PAUSE incident on error
    bool                m_pauseOnError      { 1 };
    /// Property: [Sender] Delay before resending after failure
    int                 m_sendErrorDelay;
    /// Property: [Sender] Delay sending frequency. Slepp milliseconds after send
    int                 m_sendDelay         { 0 };
    /// Property: [Sender] Routing bits assigned to the event structure (Default: 0/No-action)
    int                 m_routingBits       { 0 };
    /// Property: [Sender] Send selectively partial frames in case the event header is bad
    int                 m_sendPartialFrames { 0 };
    /// Property: Max. number of network initialization attempts
    int                 m_netinit_retries   { 5 };
    /// Property: Sleep in milli-seconds between 2 network initialization attempts   
    int                 m_netinit_retry_sleep { 500 };

    /// Monitoring item: Total number of items sent to receiver(s)
    long                m_sendReq           { 0 };
    /// Monitoring item: Total number of send errors to receiver(s)
    long                m_sendError         { 0 };
    /// Monitoring item: Total number of bytes sent to receiver(s)
    long                m_sendBytes         { 0 };
    /// Monitoring item: Total number of events with bad header
    long                m_badHeader         { 0 };

    /// Internal lock to synchronize access to queue of targets with pending event requests
    std::mutex          m_lock;
    /// [Network Producer] Queue of targets with pending event requests
    Recipients          m_recipients;
    /// Flag to indicate if events should further be processed
    int                 m_sendEvents     {0};
    /// Flag to indicate if data sending is enabled
    int                 m_sendEnabled    {0};

  public:
    /// Initializing constructor
    Sender(const std::string& name, Context& ctxt);
    /// Default destructor
    virtual ~Sender();
    /// Initialize the data sender component
    virtual int initialize()  override;
    /// Finalize the data sender component
    virtual int finalize()  override;
    /// Cancel the data sender component
    virtual int cancel()  override;
    /// Pause the data sender component
    virtual int pause()  override;
    /// Data processing overload: Send data record to network client
    virtual int execute(const Context::EventData& event)  override;

    /// Callback on task dead notification
    virtual int taskDead(const std::string& task_name);
    /// Networking layer overload: Handle client request to receive event over the network
    virtual int handleRequest(long clientID,const std::string& source,const char* buf);
    /// Networking layer overload: Add request to request queue
    virtual int addRequest(const Recipient& task);
    /// Networking layer overload [Net producer]: Send data to target destination
    virtual int sendData(const Recipient& tar, const void* data, size_t len) = 0;
    /// Networking layer overload [Net producer]: Subscribe to network requests
    virtual int subscribeNetwork() = 0;
    /// Networking layer overload [Net producer]: Unsubscribe from network requests
    virtual int unsubscribeNetwork() = 0;
    /// Networking layer overload [Net producer+consumer]: Cancel current I/O
    virtual int cancelNetwork() = 0;
  };

  inline Sender::Recipient::Recipient() : identifier(0), handler(0) {
  }

  inline Sender::Recipient::Recipient(const Recipient& e) 
    : name(e.name), identifier(e.identifier), handler(e.handler) {
  }
  
  inline Sender::Recipient::Recipient(Sender* h, const std::string& n, long id)
    : name(n), identifier(id), handler(h) {
  }

  inline Sender::Recipient& Sender::Recipient::operator=(const Recipient& e)  {
    this->name = e.name;
    this->identifier = e.identifier;
    this->handler = e.handler;
    return *this;
  }

}      // end namespace Online

#endif //  ONLINE_DATAFLOW_SENDER_H
