//	============================================================
//
//	FileWriterSvc.h
//	------------------------------------------------------------
//
//	Package   : GaudiOnline
//
//	Author    : Markus Frank
//
//	===========================================================
#ifndef ONLINE_DATAFLOW_TRIGGERBITCOUNTER
#define ONLINE_DATAFLOW_TRIGGERBITCOUNTER 1

// Framework include files
#include <EventData/raw_bank_offline_t.h>
#include <Dataflow/FileWriterMgr.h>
#include <RTL/rtl.h>

/// C/C++ include files
#include <map>
#include <ctime>
#include <cstring>
#include <filesystem>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>

// Forward declarations
namespace MBM  {
  class Consumer;
  class EventDesc;
}

/// Online namespace declaration
namespace Online  {

  template<typename T> class RunListItem  {
  public:
    int         m_runno   { 0 };
    T           m_count   { 0 };
    std::time_t m_LastCnt { 0 };
    
    RunListItem(int runno, T &Count, time_t LastCnt)      {
      m_runno = runno;
      m_count = Count;
      m_LastCnt = LastCnt;
    }
    RunListItem(int runno)  {
      m_runno = runno;
    }
    void reset()   {
      m_runno = 0;
      m_LastCnt = 0;
      ::memset(&m_count, 0, sizeof(T));
    }
  };

  template<typename T> class RunMap : public std::map<unsigned int, void*>  {
  private:
    FILE           *m_f        { nullptr };
    std::string     m_fname    { };
    int             m_fdesc    { -1 };
    void           *m_runPtr   { nullptr };
    std::size_t     m_secsiz   { 0UL };
    RunListItem<T> *m_lastPtr  { nullptr };
  public:
    void setFile(std::string fn)   {
      m_fname = fn;
      std::string dyr = std::filesystem::path(m_fname).parent_path().string();
      try   {
	std::filesystem::create_directories(dyr);
      }
      catch (...)  {
	::lib_rtl_output(LIB_RTL_FATAL, "Cannot create Directory %s\n", dyr.c_str());
      }
    }
    void openBackingStore()  {
      std::size_t page_size = (std::size_t) sysconf (_SC_PAGESIZE);
      m_secsiz = (10000*sizeof(RunListItem<T>)+page_size)& ~page_size;
      m_fdesc = ::open(m_fname.c_str(), O_RDWR+O_CREAT+O_EXCL,S_IRWXU+S_IRWXG+S_IRWXO);
      if ( m_fdesc >= 0 ) {  // File didn't exist, was created..
	close(m_fdesc);
	int status = ::truncate(m_fname.c_str(), m_secsiz);
	if (status != 0)   {
	  int ierr = errno;
	  ::lib_rtl_output(LIB_RTL_FATAL, "Cannot extend file %s Errno %d\n",m_fname.c_str(),ierr);
	}
	m_fdesc = ::open(m_fname.c_str(), O_RDWR+O_CREAT,S_IRWXU+S_IRWXG+S_IRWXO);
	if (m_fdesc < 0)   {
	  int ierr = errno;
	  ::lib_rtl_output(LIB_RTL_FATAL, "Cannot open file %s Errno %d\n",m_fname.c_str(),ierr);
	}
	m_runPtr = ::mmap(0, m_secsiz, PROT_READ+PROT_WRITE,MAP_SHARED,m_fdesc,0);
	if (m_runPtr==MAP_FAILED)   {
	  int ierr = errno;
	  ::lib_rtl_output(LIB_RTL_FATAL, "Cannot map file %s Errno %d\n",m_fname.c_str(),ierr);
	}
	::memset(m_runPtr,0,m_secsiz);
	this->clear();
	m_lastPtr = (RunListItem<T> *)m_runPtr;
      }
      else  {
	this->clear();
	m_fdesc = ::open(m_fname.c_str(),O_RDWR+O_CREAT,S_IRWXU+S_IRWXG+S_IRWXO);
	if (m_fdesc <0)   {
	  ::lib_rtl_output(LIB_RTL_FATAL, "Cannot Open file %s\n",m_fname.c_str());
	  m_runPtr = 0;
	}
	else  {
	  m_runPtr = ::mmap(0,m_secsiz,PROT_READ+PROT_WRITE,MAP_SHARED,m_fdesc,0);
	  RunListItem<T> *rptr = (RunListItem<T> *)m_runPtr;
	  m_lastPtr = rptr;
	  while (rptr->m_runno >0)   {
	    this->insert(std::make_pair(rptr->m_runno,rptr));
	    rptr++;
	    m_lastPtr = rptr;
	  }
	}
      }
    }
    RunListItem<T> *newEntry(int rn)    {
      RunListItem<T> *p = m_lastPtr;
      m_lastPtr = p+1;
      p->reset();
      p->m_runno = rn;
      return p;
    }
    void sync()   {
      ::msync(m_runPtr,m_secsiz,MS_ASYNC);
    }
  };

  class TriggerBitCounter: public FileWriterMgr::Processor  {
  protected:
    /// Monitoring quantity: Number of Event in
    long        m_EvIn;
    std::string m_BackupFile;
    std::string m_BackupDirectory;

  public:

    ///Clear Counters
    void clearCounters();
    
    /// Standard Constructor
    TriggerBitCounter(const std::string& name, Context& svc);
    /// Standard Destructor
    virtual ~TriggerBitCounter();
    /// Service overload: initialize()
    virtual int initialize() override;
    /// Service overload: start()
    virtual int start() override;
    /// Service overload: stop()
    virtual int stop() override;
    /// Service overload: finalize()
    virtual int finalize() override;
  };
}      // End namespace Online
#endif //  ONLINE_DATAFLOW_TRIGGERBITCOUNTER
