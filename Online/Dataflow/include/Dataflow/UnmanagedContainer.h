//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  UnmanagedContainer.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_UNMANAGEDCONTAINER_H
#define ONLINE_DATAFLOW_UNMANAGEDCONTAINER_H

// Framework include files
#include <Dataflow/DataflowComponent.h>

///  Online namespace declaration
namespace Online  {

  // Forward declarations
  class DataflowIncident;
  class ComponentHandler;

  /// UnmanagedContainer to group coherent action sequences
  /**
   *
   * @author  Markus Frank
   * @version 1.0
   */
  class UnmanagedContainer : public DataflowComponent  {
  protected:
    typedef std::vector<std::string>  ComponentNames;
    typedef std::vector<Component*>   Components;
    typedef int (ComponentHandler::*handler_func_t)(Component*) const;

    /// Embedded components
    Components      components;
    /// Names of embedded components
    ComponentNames  componentNames;

    /// Execute transition action
    int action(const char* transition, handler_func_t pmf);

  public:
    /// Initializing constructor
    UnmanagedContainer(const std::string& name, Context& ctxt);
    /// Default destructor
    virtual ~UnmanagedContainer();
    /// Initialize the MBM server
    virtual int initialize()  override;
    /// Start the MBM server
    virtual int start()  override;
    /// Stop the MBM server
    virtual int stop()  override;
    /// Finalize the MBM server
    virtual int finalize()  override;
    /// Continuing the data flow component.
    virtual int continuing()  override;
    /// Pause the data flow component.
    virtual int pause()  override;
    /// Pause the data flow component.
    virtual int cancel()  override;
    /// Data processing overload: process event
    virtual int execute(const Context::EventData& event)  override;
  };
}      // end namespace Online
#endif //  ONLINE_DATAFLOW_UNMANAGEDCONTAINER_H
