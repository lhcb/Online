//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  Socket.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_ZMQ_SOCKET_H
#define ONLINE_DATAFLOW_ZMQ_SOCKET_H

#include <ZMQ/zmq.hpp>

class TanMessage;

/// ZMQ namespace definition
namespace ZMQ {

  /// ZeroMQ socket wrapper
  /**
   *  \author  M.Frank
   *  \version 1.0
   *  \ingroup DATAFLOW
   */
  class Socket : public zmq::socket_t  {
    int m_error, m_flags;
  public:
    /// Standard constructor
    Socket(int type_);
    /// Default destructor
    virtual ~Socket();

    /// Access error condition code
    int error() const  { return m_error; }
    /// Check if socket I/O got cancelled
    bool isCancelled() const;
    /// Connect socket to target address
    int connect(const std::string& binding);
    /// Send data to socket
    int send(const void* buffer, size_t len);
    /// Receive data from socket
    int recv(void* buffer, size_t len, int tmo);
  };
}       // End namespace ZMQ     
#endif  /* ONLINE_DATAFLOW_ZMQ_SOCKET_H  */
