#==========================================================================
#  LHCb Online software suite
#--------------------------------------------------------------------------
# Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
# All rights reserved.
#
# For the licensing terms see OnlineSys/LICENSE.
#
# Author     : M.Frank
#
#==========================================================================
from __future__ import print_function
__version__ = "1.0"
__author__  = "Markus Frank <Markus.Frank@cern.ch>"

from builtins import str
from builtins import range
from builtins import object
import os
import sys
import time
import copy
import pydim
import threading
import OnlineEnv as OnlineEnv
from   Online.MessageLevels import *

#----------------------------------------------------------------------------------------
class Monitor(object):
  def __init__(self, partitionid):
    self.done   = int(time.time())
    self.last   = int(time.time())
    self.pid    = os.getpid()
    self.partID = partitionid

#----------------------------------------------------------------------------------------
class FSM(object):
  """
    No need to take locks. Everything is protected by the global DIM lock.
  """
  ST_OFFLINE    = 'OFFLINE'
  ST_UNKNOWN    = 'UNKNOWN'
  ST_NOT_READY  = 'NOT_READY'
  ST_READY      = 'READY'
  ST_RUNNING    = 'RUNNING'
  ST_STOPPED    = 'READY'
  ST_PAUSED     = 'PAUSED'
  ST_ERROR      = 'ERROR'
  Meta = {ST_OFFLINE: 'O', ST_UNKNOWN: 'U', ST_NOT_READY: 'N', ST_READY: 'r', ST_RUNNING: 'R', ST_STOPPED: 'r', ST_PAUSED: 'r', ST_ERROR: 'E'}  
  CMD_CONFIGURE = 'configure'
  CMD_START     = 'start'
  CMD_STOP      = 'stop'
  CMD_RESET     = 'reset'
  CMD_CONTINUE  = 'continue'
  CMD_PAUSE     = 'pause'
  CMD_LOAD      = 'load'
  CMD_UNLOAD    = 'unload'
  CMD_START_TRIGGER = 'start_trigger'
  CMD_STOP_TRIGGER = 'stop_trigger'
  
  CMD_GET_STATE = '!state'
  CMD_FORCE_RESET = 'RESET'

  #--------------------------------------------------------------------------------------
  def __init__(self, name, partitionid=0, auto=False):
    self.monitor  = Monitor(partitionid)
    self.name   = name
    self.__auto   = auto
    self.__keep   = True
    self.__state  = FSM.ST_NOT_READY
    self.previousState = FSM.ST_OFFLINE
    self.__cmdID  = pydim.dis_add_cmnd(name,'C',self.callback,1)
    self.__svcID  = pydim.dis_add_service(name+'/status', 'C', self.service, 1)
    self.__fsmID  = pydim.dis_add_service(name+'/fsm_status','L:2;I:1;C:4;I:1',self.fsm_service, 4)
    log(MSG_INFO,'DIM Command starting.... ')
    pydim.dis_update_service(self.__svcID, (self.__state,))
    pydim.dis_update_service(self.__fsmID, self.fsm_service(self))

  #--------------------------------------------------------------------------------------
  def state(self):
    return self.__state

  #--------------------------------------------------------------------------------------
  def keepAlive(self):
    return self.__keep

  #--------------------------------------------------------------------------------------
  def start(self):
    log(MSG_INFO,"+++ Start serving DIM....")
    pydim.dis_start_serving(self.name)
    ## self.handleCommand(FSM.CMD_LOAD)
    if self.__auto:
      self.handleCommand(FSM.CMD_CONFIGURE)
      self.handleCommand(FSM.CMD_START)

  #--------------------------------------------------------------------------------------
  def stop(self):
    pydim.dis_stop_serving()
    pydim.dis_remove_service(self.__cmdID)
    pydim.dis_remove_service(self.__svcID)
    pydim.dis_remove_service(self.__fsmID)
    log(MSG_DEBUG,"+++ Stop serving DIM and exit process")
    sys.exit(0)

  #--------------------------------------------------------------------------------------
  def wait_for(self):
    while self.keepAlive():
      time.sleep(0.05)
    self.stop()

  #--------------------------------------------------------------------------------------
  def service(self,tag):
    return self.__state

  #--------------------------------------------------------------------------------------
  def inState(self, states):
    if type(states) is list:
      for s in states: 
        if self.__state == s: return True
    elif type(states) is tuple:
      for s in states: 
        if self.__state == s: return True
    elif type(states) is set:
      for s in states: 
        if self.__state == s: return True
    elif self.__state == states:
      return True
    return False

  #--------------------------------------------------------------------------------------
  def fsm_service(self,tag):
    meta = FSM.Meta[self.__state]
    self.monitor.metastate = meta+meta+'S'+' '
    self.monitor.last = self.monitor.done
    self.monitor.done = int(time.time())
    result = (int(self.monitor.last), 
              int(self.monitor.done), 
              int(self.monitor.pid), 
              self.monitor.metastate,
              int(self.monitor.partID),)
    done = time.strftime('%b%d-%H%M%S',time.localtime(self.monitor.last))
    last = time.strftime('%b%d-%H%M%S',time.localtime(self.monitor.done))
    if self.keepAlive():
      log(MSG_INFO,f'+++ Publish FSM state: {str(self.__state):16s} last: {last} done: {done}  '+
          f'metastate: pid:{self.monitor.pid} meta:{self.monitor.metastate} partition:{int(self.monitor.partID)}')
    return result

  def _setState(self, new_state):
    self.previousState = self.__state
    self.__state = new_state

  #--------------------------------------------------------------------------------------
  def handleCommand(self,cmd):
    self.monitor.last = int(time.time())
    if self.keepAlive():
      log(MSG_INFO,'+++ Handle DIM Command: '+str(cmd))
    sys.stdout.flush()
    cb = 'handle'+cmd[0].upper()+cmd[1:]
    if hasattr(self,cb):
      if not getattr(self,cb)():
        self.state = FSM.ST_ERROR
        if hasattr(self,'on'+FSM.ST_ERROR):
          getattr(self,'on'+FSM.ST_ERROR)()
        cmd = ''

    if cmd == FSM.CMD_CONFIGURE:
      self._setState(FSM.ST_READY)
    elif cmd == FSM.CMD_START:
      self._setState(FSM.ST_RUNNING)
    elif cmd == FSM.CMD_STOP:
      self._setState(FSM.ST_STOPPED)
    elif cmd == FSM.CMD_RESET:
      self._setState(FSM.ST_NOT_READY)
    elif cmd == FSM.CMD_CONTINUE:
      self._setState(FSM.ST_RUNNING)
    elif cmd == FSM.CMD_PAUSE:
      self._setState(FSM.ST_PAUSED)
    elif cmd == FSM.CMD_LOAD:
      self._setState(FSM.ST_NOT_READY)
    elif cmd == FSM.CMD_UNLOAD:
      self._setState(FSM.ST_OFFLINE)
    elif cmd == FSM.CMD_FORCE_RESET:
      self._setState(FSM.ST_OFFLINE)
    elif cmd == FSM.CMD_GET_STATE:
      pass
    elif cmd == FSM.CMD_STOP_TRIGGER:
      return
    elif cmd == FSM.CMD_START_TRIGGER:
      return
    else:
      self._setState(FSM.ST_ERROR)

    if hasattr(self,'on'+self.__state):
      getattr(self,'on'+self.__state)()

    if hasattr(self,'onStateChange'):
      getattr(self,'onStateChange')(self.__state)

    pydim.dis_update_service(self.__svcID,(self.__state,))
    if self.__fsmID != 0: pydim.dis_update_service(self.__fsmID)

    if cmd == FSM.CMD_UNLOAD or cmd == FSM.CMD_FORCE_RESET:
      log(MSG_DEBUG,'+++ End of processing reached. Set exit event.')
      # pydim.dis_stop_serving()
      self.__keep = False

  #--------------------------------------------------------------------------------------
  def callback(self, *args):
    log(MSG_DEBUG,'+++ FSM status callback. Args are %s'%str(args))
    r = args[0][0]
    r = r[:r.find('\0')]
    self.handleCommand(r)

#--------------------------------------------------------------------------
def handle_execute(self, timeout):
  goto_pause = time.time() + self.timeout
  while time.time() < goto_pause:
    time.sleep(0.1)
  self.handleCommand(FSM.CMD_PAUSE)

#--------------------------------------------------------------------------
class DimTask(FSM):
  #------------------------------------------------------------------------
  def __init__(self, utgid, partitionid):
    log(MSG_INFO,"+++ Starting task: utgid: %s partition-id: %d output level: %s"%(utgid, partitionid, OutputLevel.name(),))
    FSM.__init__(self, name=utgid, partitionid=partitionid, auto=False)
    self.run_thread = None
    self.timeout = None
    self.runID = None

  #------------------------------------------------------------------------
  def onStateChange(self, state):
    if self.keepAlive():
      log(MSG_ALWAYS,'+++ Current state: %s'%(state,))
    if self.timeout and state == self.ST_RUNNING:
      import threading
      log(MSG_ALWAYS,'+++ Starting timeout thread for going to pause. Timeout: %d seconds', self.timeout)
      self.run_thread = threading.Thread(target=handle_execute, args=(self, self.timeout,))
      self.run_thread.start()

  #------------------------------------------------------------------------
  def run_number_callback(self, tag):
    return (self.run_number, )

  #------------------------------------------------------------------------
  def enable_run_numbers(self, partition, run_number):
    svc = partition + '/RunInfo/RunNumber'
    self.run_number = run_number
    self.runID = pydim.dis_add_service(svc, 'I', self.run_number_callback, 1)
    pydim.dis_update_service(self.runID, self.run_number_callback(self))

  #------------------------------------------------------------------------
  def run(self, timeout=None):
    self.timeout = timeout
    self.start()
    if self.runID:
      pydim.dis_update_service(self.runID)
    if self.run_thread:
        self.run_thread.join()
    self.wait_for()

#--------------------------------------------------------------------------
def runVoidTask(name=None,auto=False):
  from Online import MessageLevels
  utgid = ''
  partition = 0x103
  if name is not None: utgid = name
  for i in range(len(sys.argv)):
    a = sys.argv[i]
    if a[:1] == '-a': auto = True
    if a[:2] == '-u': utgid = sys.argv[i+1]
    if a[:4] == '-par': partition = int(sys.argv[i+1])
    if a[:4] == '-pri': MessageLevels.minPrintLevel = int(sys.argv[i+1])
    if a[:4] == '-sle': time.sleep(60)

  if not len(utgid) and 'UTGID' in os.environ:
    utgid = os.environ['UTGID']
  log(MSG_INFO,"FSM task: utgid:  %s   auto: %s  print level: %d"%(utgid,str(auto), minPrintLevel,))
  fsm = FSM(name=utgid, partitionid=partition, auto=auto)
  fsm.start()
  fsm.wait_for()

#--------------------------------------------------------------------------
def start_dummy_python_task(partitionid=0x103, output_level=MSG_INFO, timeout=None):
  minPrintLevel = output_level
  task = DimTask(utgid=utgid, partitionid=partitionid)
  task.run(timeout)

#--------------------------------------------------------------------------
def RunNumberService(run_number=200001, output_level=None):
  import OnlineEnv
  if not output_level:
    output_level = OnlineEnv.OutputLevel
  minPrintLevel = output_level
  task = DimTask(utgid=utgid, partitionid=OnlineEnv.PartitionID)
  task.enable_run_numbers(partition=OnlineEnv.PartitionName, run_number=run_number)
  task.run()

#--------------------------------------------------------------------------
Task   = start_dummy_python_task
Reader = start_dummy_python_task

#----------------------------------------------------------------------------------------
if __name__ == "__main__":
  import pdb
  #pdb.set_trace()
  runVoidTask()
