#===============================================================================
#  LHCb Online software suite
#-------------------------------------------------------------------------------
# Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
# All rights reserved.
#
# For the licensing terms see OnlineSys/LICENSE.
#
#===============================================================================
#
#  Module initialization script for dataflow
#
#  \author   M.Frank
#  \version  1.0
#  \date     01/08/2023
#
#===============================================================================
from __future__ import absolute_import, unicode_literals
from builtins import str
from builtins import object
import imp
Online = None

CHECKSUM_NONE    = 0
CHECKSUM_HASH32  = 1
CHECKSUM_CRC32   = 2
CHECKSUM_CRC16   = 3
CHECKSUM_CRC8    = 4
CHECKSUM_ADLER32 = 5
CHECKSUM_XOR     = 6

def loaddataflow():
  import os
  import sys
  import platform
  if not os.getenv('DIM_DNS_NODE',None):
    os.environ['DIM_DNS_NODE'] = 'localhost'

  if not os.getenv('ROOTSYS',None):
    root_sys = os.path.dirname(os.path.dirname(os.popen("which root.exe","r").readlines()[0][:-1]))
    os.environ['ROOTSYS'] = root_sys
    #print('INFO:    +++ Set ROOTSYS to '+root_sys)
  #
  # Add ROOT to the python path in case it is not yet there....
  sys.path.append(os.environ['ROOTSYS'] + os.sep + 'lib')
  from ROOT import gSystem
  #
  print('INFO:   Loading library: libDataflowDict')
  result = gSystem.Load("libDataflowDict")
  if result < 0:
    raise Exception('dataflow.py: Failed to load the dataflow library libDataflowDict: ' + gSystem.GetErrorStr())
  print('INFO:   Import Online namespace from ROOT')
  from ROOT import Online as module
  return module

# We are nearly there ....
name_space = __import__(__name__)

def import_namespace_item(ns, nam):
  scope = getattr(name_space, ns)
  attr  = getattr(scope, nam)
  setattr(name_space, nam, attr)
  return attr

def import_root(nam):
  setattr(name_space, nam, getattr(ROOT, nam))

# ---------------------------------------------------------------------------
#
try:
  if not Online:
    Online = loaddataflow()
  import ROOT
except Exception as X:
  import sys
  print('ERROR:  +--%-100s--+'%(100 * '-'), )
  print('ERROR:  |  %-100s  |'%('Failed to load dataflow framework library:'), )
  print('ERROR:  |  %-100s  |'%(str(X)), )
  print('ERROR:  +--%-100s--+'%(100 * '-'), )
  sys.exit(1)

from Online.MessageLevels import *

# ---------------------------------------------------------------------------
def import_online():
  #import_namespace_item('Online', 'ComponentContainer')
  #import_namespace_item('Online', 'ControlPlug')
  #import_namespace_item('Online', 'Control')
  #import_namespace_item('Online', 'DataflowComponent')
  #import_namespace_item('Online', 'DataflowComponentProperties')
  #import_namespace_item('Online', 'DataflowContext')
  #import_namespace_item('Online', 'DataflowTask')
  #import_namespace_item('Online', 'MBMClient')
  #import_namespace_item('Online', 'OutputLogger')
  #import_namespace_item('Online', 'Property')
  #import_namespace_item('Online', 'PropertyManager')
  import_namespace_item('Online', 'PythonTask')
  #import_namespace_item('Online', 'UnmanagedContainer')

import_online()

# ------------------------Generic STL stuff can be accessed using std:  -----
import cppyy
std        = cppyy.gbl.std
std_vector = std.vector
std_list   = std.list
std_map    = std.map
std_pair   = std.pair
# ---------------------------------------------------------------------------

all_components = {}

class component_type:
  def __init__(self, type, name):
    self.name = name
    self.type = type

Monitoring  = component_type(type='Dataflow_DIMMonitoring',   name='Monitoring')
Logger      = component_type(type='Dataflow_OutputLogger',    name='Logger')
JobOptions  = component_type(type='Dataflow_JobOptions',      name='JobOptions')
Incidents   = component_type(type='Dataflow_IncidentHandler', name='IncidentHandler')
ControlPlug = component_type(type='Dataflow_DimControlPlug',  name='Plug')
mbm_server  = component_type(type='Dataflow_MBMServer',       name='MBM')
mbm_client  = component_type(type='Dataflow_MBMClient',       name='MBM')

# ---------------------------------------------------------------------------
class property_manager(object):
  def __init__(self, name):
    self.name = name
  def __repr__(self):
    return self.name
  def print_properties(self):
    for k,v in self.__dict__.items():
      print(str(k),'=',str(v))
  def options(self):
    opts = []
    for k, v in self.__dict__.items():
      if k == 'name': continue
      if k == 'type': continue
      if isinstance(v,list):
        val = [str(i) for i in v]
      elif isinstance(v,tuple):
        val = [str(i) for i in v]
      else:
        val = v
      o = std.make_pair['std::string','std::string'](self.name+'.'+str(k), str(val))
      opts.append(o)
    return opts

# ---------------------------------------------------------------------------
class application_interface(property_manager):
  def __init__(self, clazz):
    property_manager.__init__(self,'__MAIN__')
    self.Class = clazz

  def run(self):
    global std_vector
    global all_components
    options = std_vector['std::pair<std::string,std::string>']()
    opts    = self.options()
    for o in opts: options.push_back(o)
    for n,c in all_components.items():
      if n.find(self.name) == 0: continue
      opts = c.options()
      for o in opts: options.push_back(o)
    all_components = {}
    opts = None
    return PythonTask().run(options)

# ---------------------------------------------------------------------------
class component_interface(property_manager):
  def __init__(self, type, name):
    property_manager.__init__(self,name)
    self.type = type
  def __repr__(self):
    return self.type+'/'+self.name

# ---------------------------------------------------------------------------
def Application(clazz):
  global all_components
  name = '__MAIN__'
  if name not in all_components:
    app = application_interface(clazz)
    app.Class = clazz
    all_components[name] = app
  return all_components[name]

# ---------------------------------------------------------------------------
def Component(type, name=None):
  global all_components
  if not name:
    name = type
    itms = type.split('/')
    if len(itms) > 1:
      type = itms[0]
      name = itms[1]
  if name not in all_components:
    all_components[name] = component_interface(type,name)
  return all_components[name]
