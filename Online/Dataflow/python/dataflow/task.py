#==========================================================================
#  LHCb Online software suite
#--------------------------------------------------------------------------
# Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
# All rights reserved.
#
# For the licensing terms see OnlineSys/LICENSE.
#
# Author     : M.Frank
#
#==========================================================================
#
# Gaudi online tests
#
#==========================================================================
__version__ = "1.0"
__author__  = "Markus Frank <Markus.Frank@cern.ch>"

import os, sys
from dataflow import *

global online_environment
online_environment = None

#--------------------------------------------------------------------------
class online_env:
  def __init__(self):
    self.PartitionID      = os.getpid()&0xFFFF
    self.PartitionIDName  = '%04X'%(self.PartitionID,)
    self.PartitionName    = '0x%04X'%(self.PartitionID,)
    self.PartitionBuffers = True
    self.Activity         = "PHYSICS"
    self.TAE              = 0
    self.OutputLevel      = OutputLevel.INFO
    self.AcceptRate       = 0.0001
    self.EnableWriting    = 1
    self.HasAlignment     = 0
    self.HasMonitoring    = 1
    self.HasStorage       = 0
    self.passThroughDelay = 0
    self.RunType          = "PHYSICS"
    self.MBM_setup        = mbm_flags('Events')+mbm_flags('Output')

#--------------------------------------------------------------------------
def get_online_env():
  global online_environment
  if online_environment is None:
    online_environment = online_env()
  return online_environment

#--------------------------------------------------------------------------
def set_online_env(env):
  global online_environment
  online_environment = env
  return online_environment
    
#--------------------------------------------------------------------------
def mbm_flags(which):
  if isinstance(which,(list,tuple)):
    flgs = ''
    for i in which:
      flgs = flgs + ' ' + mbm_flags(i)
    return flg[1:]
  elif isinstance(which, str) and which == 'Events':
    return '-s=1000000 -e=50 -u=30 -b=15 -t=1 -y -i=Events -f -c '
  elif isinstance(which, str) and which == 'Output':
    return '-s=100000 -e=50 -u=15 -b=12 -t=1 -y -i=Output -f -c'
  return '-s=100000 -e=50 -u=15 -b=12 -t=1 -y -i='+which+' -f -c'

#--------------------------------------------------------------------------
def mbm_default_requirement(event_type=2, user_type='VIP', percent=100.0):
  return  f'EvType={event_type};' + \
    'TriggerMask=0xffffffff,0xffffffff,0xffffffff,0xffffffff;' + \
    'VetoMask=0,0,0,0;MaskType=ANY;UserType=%s;Frequency=PERC;Perc=%7.1f'%(user_type, percent,)

#--------------------------------------------------------------------------
def _run_task(task, execute):
  if execute:
    return task.run()
  return task

#--------------------------------------------------------------------------
class basic_task:
  """
      Process setup class to easily configure dataflow processes
  """
  #------------------------------------------------------------------------
  def __init__(self, clazz, output_level=1):
    self.output_level = output_level
    self.app = Application(clazz)
    self.app.OutputLevel = output_level
    self.manager = Component('DataflowManager/Manager')
    self.manager.ReverseShutdown = 1
    self.manager.MaxBadEvent = 10000
    self.exit_component = None
    self.writerManager = None
    self.runable = None
    self.algorithms = []
    self.services = []
    self.setup = []

  #------------------------------------------------------------------------
  def create_component(self, type, name, output_level=None):
    """ Create arbitrary component and set its name and output level
    """
    comp = Component(type+'/'+name)
    if output_level:
      comp.OutputLevel = output_level
    else:
      comp.OutputLevel = self.output_level
    return comp

  #------------------------------------------------------------------------
  def setup_logger(self, type=Logger.type, name=Logger.name):
    """ Setup output logging for the dataflow task
    """
    print('INFO:   +++ Setting up output logger component')
    log = self.create_component(type=type, name=name)
    log.LogDeviceType = "RTL::Logger::LogDevice"
    log.Format        = '%-20SOURCE %-8LEVEL'
    log.OutputLevel   = self.output_level
    self.logger = log
    return self

  #------------------------------------------------------------------------
  def setup_ui(self, type='Dataflow_UI', name='UI'):
    """ Setup UI service for the dataflow task.
    """
    ui = self.create_component(type=type, name=name)
    self.services.append(ui)
    self.ui = ui
    return self

  #------------------------------------------------------------------------
  def set_automatic(self, auto_startup, auto_finalize):
    """ Setup AutoStart and AutoStop machinery
    """
    self.app.Automatic = auto_startup
    self.app.Terminate = auto_finalize
    self.app.PrintOptions = True
    return self

  #------------------------------------------------------------------------
  def setup_exit(self, when="(start)"):
    print('INFO:   +++ Setting up exit component')
    self.exit_component = Component('Dataflow_ExitPlugin/Exit')
    self.exit_component.PrintPID = False    
    self.exit_component.When = when
    return self

  #------------------------------------------------------------------------
  def setup_monitoring(self, type=Monitoring.type, name=Monitoring.name):
    global online_environment
    mon = self.create_component(type=type, name=name)
    mon.PartitionName = online_environment.PartitionName
    mon.ExpandNameInfix       = "<proc>/"
    mon.UniqueServiceNames    = 1
    mon.ExpandCounterServices = 1
    mon.CounterUpdateInterval = 5
    mon.DimUpdateInterval     = 30
    self.app.Monitoring = type
    self.monitoring = mon
    return self

  #------------------------------------------------------------------------
  def setup_delay_service(self, name='Delay', type='Dataflow_Delay', when={}):
    delay = self.create_component(type=type, name=name)
    for k,v in when.items():
      setattr(delay, k, v)
    self.services.append(delay)
    return delay

  #------------------------------------------------------------------------
  def setup_delay_algorithm(self, name='Delay', type='Dataflow_Delay', when={}):
    delay = self.create_component(type=type, name=name)
    for k,v in when.items():
      setattr(delay, k, v)
    self.algorithms.append(delay)
    return delay

  #------------------------------------------------------------------------
  def setup_basics(self, auto_startup=False, auto_finalize=False, exit=False, ui=True, monitoring=True):
    """ Do basic task setup
    """
    if os.getenv('DF_DEBUG'):
      self.app.Debug = True
    self.setup_logger()
    if ui:
      self.setup_ui()

    self.set_automatic(auto_startup=auto_startup, auto_finalize=auto_finalize)
    if monitoring:
      self.setup_monitoring()
    if isinstance(exit,str):
      self.setup_exit(exit)
    elif exit:
      self.setup_exit()
    return self    

  #------------------------------------------------------------------------
  def setup_numa(self, type='Dataflow_NumaControl', name='Numa'):
    print('INFO:   +++ Setting up numa component')
    numa            = self.create_component(type=type, name=name)
    numa.When       = "(initialize)"
    numa.BindCPU    = True
    numa.BindMemory = True
    numa.PrintPID   = False
    numa.CPUMask    = [1,0,1,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    numa.Print      = "(all)(result)"
    self.services.append(numa)
    return numa

  #------------------------------------------------------------------------
  def run(self):
    # Basic infrastructure setup
    if len(self.setup):
      self.manager.Setup = self.setup
    # Setup the dataflow services
    if self.exit_component:
      self.services.append(self.exit_component)
    if len(self.services):
      self.manager.Services = self.services
    # setup the dataflow algorithms
    if len(self.algorithms):
      self.manager.Algorithms = self.algorithms
    # Setup runable if defined
    if self.runable:
      self.manager.Runable = self.runable

    print('INFO:   +++ Starting dataflow process')
    sys.stdout.flush()
    sys.stderr.flush()
    self.app.run()
    sys.stdout.flush()
    sys.stderr.flush()
    return self

  #------------------------------------------------------------------------
  def setup_mbm_server_component(self, flags):
    global online_environment
    if not flags:
      raise KeyError('setup_mbm_server_component: No MBM server flags provided!')
    mbm = self.create_component(type='Dataflow_MBMServer', name='MBM')
    mbm.PartitionBuffers = online_environment.PartitionBuffers
    mbm.PartitionName    = online_environment.PartitionName
    mbm.PartitionID      = online_environment.PartitionID
    mbm.InitFlags        = flags
    self.setup.append(mbm)
    self.mbm = mbm
    return self

  #------------------------------------------------------------------------
  def setup_mbm_client_component(self, name, buffers, connect='initialize'):
    global online_environment
    client = self.create_component(type='Dataflow_MBMClient', name=name)
    client.PartitionBuffers = True
    client.PartitionName    = online_environment.PartitionName
    client.PartitionID      = online_environment.PartitionID
    client.Buffers          = buffers
    client.OutputLevel      = MSG_WARNING
    if connect:
      client.ConnectWhen    = connect
    self.services.append(client)
    return client

  #------------------------------------------------------------------------
  def setup_mbm_writer_component(self, buffer, type='Dataflow_MBMWriter', name='Writer'):
    writer = self.create_component(type=type, name=name)
    writer.Buffer = buffer
    self.algorithms.append(writer)
    return writer

  #------------------------------------------------------------------------
  def setup_mbm_selector_component(self, name='EventSelector', type='Dataflow_MBMSelector', buffer=None):
    selector = self.create_component(type=type, name=name)
    selector.REQ1 = mbm_default_requirement(event_type=2,user_type='ONE')
    if buffer:
      selector.Input = buffer
    self.services.append(selector)
    return selector
  
  #------------------------------------------------------------------------
  def setup_mbm_summary(self, regex='', type='Dataflow_MBMSummary', name='Summary', when=None, delay=-1):
    sum = self.create_component(type=type, name=name)
    sum.ShowStates = False
    sum.Regex = regex
    if delay > 0:
      sum.Delay = 1000*delay
    if when:
      sum.When = when
    self.services.append(sum)
    return sum

  #------------------------------------------------------------------------
  def setup_mbm_summary_algorithm(self, regex='', type='Dataflow_MBMSummary', name='Summary', when=None, delay=-1):
    sum = self.create_component(type=type, name=name)
    sum.ShowStates = False
    sum.Regex = regex
    if delay > 0:
      sum.Delay = 1000*delay
    if when:
      sum.When = when
    self.algorithms.append(sum)
    return sum

  #------------------------------------------------------------------------
  def setup_net_sender_component(self, target, name='Sender', type='Dataflow_AsioSender', retries=-1):
    sender = self.create_component(type=type, name=name)
    sender.PauseOnError = True
    sender.DataSink = target
    sender.Delay = 0
    if retries > 0:
      sender.NetInitRetries = retries
      sender.NetInitRetrySleep = 1500
    tan_host = os.getenv('TAN_HOST','localhost')
    tan_port = int(os.getenv('TAN_PORT','-1'))
    print(f'INFO:   +++ Network component Sender[{name}/{type}]  target:{str(target)} TAN host:{tan_host} TAN port:{tan_port}')
    self.algorithms.append(sender)
    return sender

  #------------------------------------------------------------------------
  def setup_net_receiver_component(self, name='Receiver', type='Dataflow_AsioReceiver',
                                   buffer='Events',
                                   threads=3):
    receiver = self.create_component(type=type, name=name)
    receiver.Buffer     = buffer
    receiver.NumThreads = threads
    receiver.ErrorDelay = 2 # 2 seconds delay of workers crash...
    tan_host = os.getenv('TAN_HOST','localhost')
    tan_port = int(os.getenv('TAN_PORT','-1'))
    print(f'INFO:   +++ Network component Receiver[{name}/{type}]  TAN host:{tan_host} TAN port:{tan_port}')
    self.services.append(receiver)
    return receiver

  #------------------------------------------------------------------------
  def setup_net_eventserver_component(self, name='Client', type='Dataflow_AsioEventServer', buffer='Events', request='None'):
    selector = self.create_component(type=type, name=name)
    if not request:
      request = mbm_default_requirement(event_type=2)
    selector.REQ = request
    selector.Input = buffer
    selector.ChangeRequest = 0 # Must be (int)!
    tan_host = os.getenv('TAN_HOST','localhost')
    tan_port = int(os.getenv('TAN_PORT','-1'))
    print(f'INFO:   +++ Network component EventServer[{name}/{type}]  TAN host:{tan_host} TAN port:{tan_port}')
    return selector

  #------------------------------------------------------------------------
  def setup_net_eventclient_component(self, name='Selector', type='Dataflow_AsioSelector', target=None, request='None'):
    selector = self.create_component(type=type, name=name)
    if not request:
      request = mbm_default_requirement(event_type=2)
    selector.Input  = input
    if request:
      selector.REQ1 = request
    selector.Pause = True
    if target:
      selector.Input = target
    tan_host = os.getenv('TAN_HOST','localhost')
    tan_port = int(os.getenv('TAN_PORT','-1'))
    print(f'INFO:   +++ Network component network EventSelector[{name}/{type}]  input:{str(target)} TAN host:{tan_host} TAN port:{tan_port}')
    return selector

  #------------------------------------------------------------------------
  def setup_eventloop_wrapper(self, callable, name='Wrap', type='Dataflow_RunableWrapper'):
    wrapper = self.create_component(type=type, name=name)
    wrapper.Callable = callable
    self.runable = wrapper
    self.services.append(wrapper)
    return wrapper
 
  #------------------------------------------------------------------------
  def setup_gen_reader_component(self, reader, buffer):
    global online_environment 
    reader.PartitionName     = online_environment.PartitionName
    reader.Activity          = online_environment.Activity
    reader.Buffer            = buffer
    reader.BrokenHosts       = ''
    reader.GoService         = ''
    reader.RequireConsumers  = 0
    reader.MMapFiles         = 0
    reader.ReuseFile         = 0
    reader.Rescan            = 0
    reader.DeleteFiles       = 0
    reader.SaveRest          = 0
    reader.MaxPauseWait      = 1
    reader.PauseSleep        = 1
    reader.InitialSleep      = 1
    reader.MuDelay           = 0

  #------------------------------------------------------------------------
  def setup_file_reader_component(self, name, buffer, delay=0.0):
    reader  = self.create_component(type='Dataflow_BurstReader', name=name)
    self.setup_gen_reader_component(reader=reader, buffer=buffer)
    reader.AllowedRuns          = ['*']
    reader.PatchOdin            = 5000000
    if (isinstance(delay,float) or isinstance(delay,int)) and delay > 0:
      reader.MuDelay = int(delay)
    return reader

  #------------------------------------------------------------------------
  def setup_file_writer_component(self, output, type='Dataflow_StorageWriter', name='Writer'):
    global online_environment
    writer = self.create_component(type=type, name=name)
    writer.PartitionName     = online_environment.PartitionName
    writer.Activity          = online_environment.Activity
    writer.Stream            = "Full"
    writer.PollTimeout       = 100000 # micro-seconds
    writer.CancelTimeout     = 5      # seconds
    writer.EnableWriting     = 1
    writer.RunList           = []
    writer.ThreadFileQueues  = False
    writer.IdleTimeout       = 500 # [seconds]
    writer.NumThreads        = 1
    writer.NumBuffers        = 1
    writer.MinFileSizeMB     = 0
    writer.MaxFileSizeMB     = 500
    writer.BufferSizeMB      = 4
    writer.HaveFileDB        = 0
    writer.FDBVersion        = 0
    writer.MinAllowedRunno   = 0
    writer.MaxAllowedRunno   = 100000000
    writer.HaveStats         = True

    if output and isinstance(output, str):
      writer.FileName        = output
    writer.OutputType        = "POSIX"
    self.algorithms.append(writer)
    return writer

  #------------------------------------------------------------------------
  def setup_hlt2_reader_component(self, name, buffer='Events', runs=[ "*" ], delay=None):
    reader = self.create_component(type='Dataflow_StorageReader', name=name, output_level=MSG_ERROR)
    self.setup_gen_reader_component(reader=reader, buffer=buffer)
    if not runs:
      runs = [ '%s/Full/%010d'%(online_environment.PartitionName, 12345, ) ]
    reader.DataType          = 'POSIX'
    reader.AllowedRuns       = runs
    reader.PackingFactor     = 10
    reader.Server            = ''
    reader.FilePrefix        = "${PARTITION}/${RUN}"
    reader.GoService         = ''
    reader.Rescan            = 0
    if (isinstance(delay,float) or isinstance(delay,int)) and delay > 0:
      reader.MuDelay = int(delay)
    return reader

  #------------------------------------------------------------------------
  def setup_hlt1_reader_component(self, name, server, buffer='Events', runs=[ "*" ], delay=None):
    reader = self.setup_hlt2_reader_component(name=name, buffer=buffer, runs=runs, delay=delay)
    reader.Server            = str(server)
    reader.DataType          = 'NETWORK'
    reader.NumThreads        = 1
    reader.NumBuffers        = 2
    return reader

  #------------------------------------------------------------------------
  def setup_hlt1_writer_component(self, server, type='Dataflow_StorageWriter', name='Writer'):
    writer = self.setup_file_writer_component(type=type, name=name, output=None)
    writer.Server          = server
    writer.BufferSizeMB    = 100
    writer.PollTimeout     = 100000
    writer.OutputType      = "NETWORK"
    writer.FileName        = "/objects/data/${PARTITION}/${STREAM}/${RUN}/Run_${RUN}_${SEQ}.mdf"
    writer.FDBVersion      = 1
    writer.WriteErrorRetry = 5
    return writer

  #------------------------------------------------------------------------
  def setup_writer_manager_component(self, type='Dataflow_FileWriterMgr', name='WrManager', input=None, requirement=None):
    if not self.writerManager:
      mgr = self.create_component(type='Dataflow_FileWriterMgr', name='WrManager')
      if requirement:
        mgr.Requirement = requirement
      else:
        mgr.Requirement = "EvType=2;" + \
          "TriggerMask=0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF;" + \
          "VetoMask=0x0,0x0,0x0,0x0;MaskType=ANY;UserType=VIP;Frequency=PERC;Perc=100.0"
        
      if input:
        mgr.Input = input
      mgr.Processors = []
      self.writerManager = mgr
      self.services.append(mgr)
    return self.writerManager

  #------------------------------------------------------------------------
  def setup_align_writer_component(self, name='Test', type='Dataflow_FileWriter', requirements=None, steering=None):
    global online_environment
    writer = self.create_component(type=type, name=name)
    writer.OutputLevel = OutputLevel.DEBUG
    writer.Stream      = name
    writer.SizeLimit   = 1024*1024*1024
    writer.MaxEvents   = 1000000
    writer.UseAllRuns  = 1
    writer.DeviceList  = [ '.' ]
    writer.NodePattern = ''
    writer.FileCloseDelay = 1
    writer.FilePrefix  = '../data/${STREAM}/${RUN}/Run_${RUN}_${TIME}.mdf'
    writer.PartitionName = online_environment.PartitionName
    if requirements:
      writer.Requirements = requirements
    if steering:
      writer.DIMSteering = 1
    else:
      writer.DIMSteering = 0
    self.services.append(writer)
    return writer
  
  #------------------------------------------------------------------------
  def setup_event_generator_component(self, name, half_window=0, packing=1):
    runable = self.create_component(type='Dataflow_MDFGenerator', name=name)
    runable.HalfWindow = half_window
    runable.PackingFactor = packing
    self.runable = runable
    self.services.append(runable)
    return runable
  #------------------------------------------------------------------------
  def setup_generator_task(self, name, run, bursts, buffer, half_window, packing):
    client = self.setup_mbm_client_component(name='MBM', buffers=[buffer], connect='initialize')
    gene   = self.setup_event_generator_component(name=name, half_window=half_window, packing=packing)
    gene.RunNumber = run
    gene.MaxBursts = bursts
    writer = self.setup_mbm_writer_component(name='Writer', buffer=buffer)
    return gene

  #------------------------------------------------------------------------
  def setup_file_writer_task(self, name, buffer, output):
    client = self.setup_mbm_client_component(name='MBM', buffers=[buffer], connect='initialize')
    select = self.setup_mbm_selector_component(name='Selector', buffer=buffer)
    writer = self.setup_file_writer_component(name=name, output=output)
    self.setup_eventloop_wrapper(select)
    return (client, select, writer)

  #------------------------------------------------------------------------
  def setup_mbm_writer_task(self, name='Writer', inbuffer='Events', outbuffer='Output'):
    client = self.setup_mbm_client_component(name='MBM', buffers=[inbuffer, outbuffer], connect='initialize')
    select = self.setup_mbm_selector_component(name='Selector', buffer=inbuffer)
    writer = self.setup_mbm_writer_component(name=name, buffer=outbuffer)
    self.setup_eventloop_wrapper(select)
    return (client, select, writer)

  #------------------------------------------------------------------------
  def setup_hlt1_writer_task(self, name, buffer, server):
    client = self.setup_mbm_client_component(name='MBM', buffers=[buffer], connect='initialize')
    select = self.setup_mbm_selector_component(name='Selector', buffer=buffer)
    writer = self.setup_hlt1_writer_component(name=name, server=server)
    self.setup_eventloop_wrapper(select)
    return (client, select, writer)

  #------------------------------------------------------------------------
  def setup_net_sender_task(self, target, name='Sender', type='Dataflow_AsioSender', buffer='Events'):
    client = self.setup_mbm_client_component(name='MBM', buffers=[buffer], connect='initialize')
    select = self.setup_mbm_selector_component(name='Selector', buffer=buffer)
    select.REQ1 = mbm_default_requirement(event_type=2,user_type='VIP')
    sender = self.setup_net_sender_component(target=target, name=name, type=type, retries=5)
    self.setup_eventloop_wrapper(select)
    return (client, select, sender)

  #------------------------------------------------------------------------
  def setup_net_receiver_task(self, name='Receiver', type='Dataflow_AsioReceiver', buffer='Events', threads=3):
    client = self.setup_mbm_client_component(name='MBM', buffers=[buffer], connect='initialize')
    receiver = self.setup_net_receiver_component(name=name, type=type, buffer=buffer, threads=threads)
    return (client, receiver)

  #------------------------------------------------------------------------
  def setup_net_eventserver_task(self, name='Client', type='Dataflow_AsioEventServer', buffer='Events', request=None):
    client = self.setup_mbm_client_component(name='MBM', buffers=[buffer], connect='initialize')
    selector = self.setup_net_eventserver_component(name=name, type=type, buffer=buffer, request=request)
    return (client, selector, self.setup_eventloop_wrapper(selector))

  #------------------------------------------------------------------------
  def setup_net_eventclient_task(self, name='Selector', type='Dataflow_AsioSelector', buffer='Events', target=None, request=None):
    client = self.setup_mbm_client_component(name='MBM', buffers=[buffer], connect='initialize')
    selector = self.setup_net_eventclient_component(name=name, type=type, target=target, request=request)
    self.setup_mbm_writer_component(name='Writer', buffer=buffer)
    return (client, selector, self.setup_eventloop_wrapper(selector))

  #------------------------------------------------------------------------
  def setup_align_writer_task(self, name, type='Dataflow_FileWriter', buffer='Events', max_events=None, requirement=None, steering=None):
    client = self.setup_mbm_client_component(name='MBM', buffers=[buffer], connect='initialize')
    writer = self.setup_align_writer_component(name=name, type=type, requirements={requirement}, steering=steering)
    writer.UseAllRuns = 1
    if max_events:
      writer.MaxEvents = max_events
    mgr = self.setup_writer_manager_component(type='Dataflow_FileWriterMgr', name='WrManager', input=buffer)
    mgr.Processors.append(type + '/' + writer.name)
    self.setup_eventloop_wrapper(mgr)
    return (client, mgr, writer)

#--------------------------------------------------------------------------
def MDFGen(buffer='Events', run=12345, bursts=10000, half_window=0, packing=1, execute=True):
  global online_environment
  import OnlineEnvBase as online_environment
  print('INFO:   +++ Generating run: %d  bursts: %d packing: %d half_window: %s'%(run, bursts, half_window, packing))
  task = basic_task('Class2', output_level=online_environment.OutputLevel)
  task.setup_basics()
  task.HavePause = True
  task.setup_generator_task(name='Generator',
                            run=run,
                            bursts=bursts,
                            buffer=buffer,
                            half_window=half_window,
                            packing=packing)
  task.setup_delay_algorithm(name='Delay', when={'Start':10, 'Pause': 15})
  return _run_task(task=task, execute=execute)

#--------------------------------------------------------------------------
def TAEGen(run=12345, bursts=100, half_window=7, packing=1000, execute=True):
  return MDFGen(run=run, bursts=bursts, half_window=half_window, packing=packing, execute=execute)

#--------------------------------------------------------------------------
def DatafileReader(input=None, prefix=None, opts={}):
  """
  Possible options are:
  'input':      <string>    File name or directory with input files
  'batch':                  Run in batch without prompts to continue
  'debug':                  Set debug printouts on MSG_INFO -> MSG_DEBUG
  'monitoring':             Enable DIM monitoring using Gaucho
  'headers':                Dump TELL1 event header structures
  'odin':                   Dump ODIN bank structures
  'banks':                  Dump banks data content

  if 'input' is explicitly given, opts may be a set rather than a dictionary!

  """
  global online_environment
  online_environment = online_env()
  moni = 'monitoring' in opts
  if not input and isinstance(opts,dict):
    input = opts.get('input')
  output_level = MSG_INFO
  if 'debug' in opts:
    output_level = MSG_DEBUG

  task = basic_task('Class0', output_level=output_level)
  task.setup_basics(auto_startup=True, auto_finalize=False, monitoring=moni)

  reader = task.create_component(type='Dataflow_DiskReader', name='Reader')
  task.setup_gen_reader_component(reader=reader, buffer='')
  reader.AllowedRuns  = ['*']
  reader.MuDelay      = 0
  reader.Rescan       = 0
  reader.ConsumerWait = 0
  reader.FilePrefix   = ''
  if prefix and isinstance(prefix, str):
    reader.FilePrefix = prefix
  reader.Directories  = []
  task.services.append(reader)

  selector = task.create_component(type='Dataflow_FileSelector', name='EventSelector')
  selector.Requests = [mbm_default_requirement(event_type=2)]
  selector.Reader   = 'Dataflow__DiskReader/Reader'
  selector.Files    = []
  selector.VerifyData = True
  task.services.append(selector)
  task.setup_eventloop_wrapper(callable=selector)

  if input:
    if isinstance(input,str) and os.path.isdir(input):
      reader.Directories = [input]
    elif isinstance(input,str):
      selector.Files = [input]
    elif isinstance(input,list):
      selector.Files = input
    elif isinstance(input,tuple):
      selector.Files = [f for f in input]
  
  if 'headers' in opts:
    alg = task.create_component(type='Dataflow_Tell1HeaderDump', name='TELL1')
    alg.Print = 1.0
    task.algorithms.append(alg)
  if 'odin' in opts:
    alg = task.create_component(type='Dataflow_OdinDump', name='ODIN')
    alg.Print = 1.0
    task.algorithms.append(alg)
  if 'tae' in opts:
    alg = task.create_component(type='Dataflow_TAEDump', name='TAE')
    alg.Print = 1.0
    task.algorithms.append(alg)
  if not 'batch' in opts:
    alg = task.create_component(type='Dataflow_Prompt', name='PROMPT')
    task.algorithms.append(alg)
  if 'banks' in opts or 'bankheaders' in opts:
    alg = task.create_component(type='Dataflow_Tell1Dump', name='BANKS')
    alg.Print   = 1.0
    alg.Full    = True
    if 'bankheaders' in opts:
      alg.Full  = False
    alg.Dump    = True
    alg.Summary = True
    task.algorithms.append(alg)
    if not 'batch' in opts:
      alg = task.create_component(type='Dataflow_Prompt', name='BnkPrompt')
      task.algorithms.append(alg)
  return _run_task(task=task, execute=True)
#--------------------------------------------------------------------------
  
