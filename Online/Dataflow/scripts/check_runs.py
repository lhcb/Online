from __future__ import print_function
#*****************************************************************************\
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
#                                                                             *
# This software is distributed under the terms of the GNU General Public      *
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
#                                                                             *
# In applying this licence, CERN does not waive the privileges and immunities *
# granted to it by virtue of its status as an Intergovernmental Organization  *
# or submit itself to any jurisdiction.                                       *
#*****************************************************************************/
import os, sys, time

file_dir  = '/daqarea/lhcb/data/2017/RAW/TEST_MARKUS_CALIB/LHCb/EOF_CALIB17'
runs      = os.listdir(file_dir)
first_run = 190960
last_run  = 999999
run_nums  = []
all_files = []

runs.sort()
for i in runs:
  runno = int(i)
  if runno>=first_run and runno<last_run:
    run_nums.append(runno)
    files = ['%s/%s/%s'%(file_dir,i,f,) for f in os.listdir('%s/%s'%(file_dir,i,))]
    files.sort()
    all_files.append([i,files])



for run,files in all_files:
  print('echo "++++   Checking RUN: %s  (%d files)";'%(run,len(files),))
  for f in files:
    print('echo  "  ++ Checking file     %s";'%(f,))
    print('echo "      Size:     `stat --printf=\\"%%s\\" %s` bytes.";'%(f,))
    print('echo "      Result: `/admin/RunDatabase/python/cntMdfEvents_skipCorruption \\"%s\\"`";'%(f,))
