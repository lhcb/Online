//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  Fifo Logger implementation
//--------------------------------------------------------------------------
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================

/// Framework include files
#include "DebugClient.h"
#include <CPP/Event.h>
#include <CPP/Interactor.h>

/// C/C++ include files
#include <mutex>
#include <regex>
#include <string>
#include <memory>
#include <thread>

class debugger::DebugClient::implementation : public CPP::Interactor  {
  typedef std::unique_ptr<std::thread> thread_t;

  /// Helper to explore services
  class client_inventory_t    {
  private:
    /// DIM callback to supply serving log messages
    static void handle_response(void* tag, void* buff, int* size);

  private:
    Interactor* _parent         { nullptr };
    long        _dns_id         { 0 };
    int         _server_list_id { 0 };

  public:
    /// Standard constructor
    client_inventory_t(Interactor* parent, long dns);
    /// Default destructor
    ~client_inventory_t();
  };

  /// Helper to interact with debugger on remote node
  class client_t   {
  private:
    Interactor* _parent       { nullptr };
    std::string _node;
    std::string _name;
    std::string _command;
    long        _dns_id       { 0 };
    int         _dim_log_id   { 0 };

  private:
    /// DIM callback to supply serving log messages
    static void handle_log(void* tag, void* buff, int* size);

  public:
    /// Standard constructor
    client_t(Interactor* parent, long dns, const std::string& node, const std::string& name);
    /// Default destructor
    ~client_t();
    /// Access client name
    const std::string& name() const { return _name; }
    /// Access client node name
    const std::string& node() const { return _node; }
    /// Send command to remote client
    int send_command(const std::string& cmd);
  };

  typedef std::map<std::string, std::shared_ptr<client_t> > client_map_t;
  typedef std::unique_ptr<client_inventory_t> inventory_t;

  client_map_t _all_clients;
  client_map_t _selected_clients;
  inventory_t  _client_inventory;

  std::mutex  _dim_vars_lock  { };
  long        _dic_dns_id     { 0L };
  std::string _dim_dns_name   { };

  std::string _current_node   { };
  std::string _node_match     { };
  std::string _process_match  { };
  std::regex  _server_match   { };

  thread_t    _input          { };
  bool        _is_running     { false };

  std::string match_from_command(const std::string& cmd);

  /// Generic output helper
  void output(FILE* file, const char* tag, const char* fmt, va_list& args);

  /// Add handler for a single server process
  int _add_server(const std::string& node, const std::string& server);

  /// Remove handler for a single server process
  int _remove_server(const std::string& server);

  /// Start the standard input message pump
  void _process_stdin();

  /// Re-establish command prompt
  void make_prompt();

public:
  /// Default constructor
  implementation();

  /// Default destructor
  ~implementation();

  /// Set server match when asking for possible clients
  void set_server_match(const std::string& match);

  /// Output helper for logging messages
  void log(const char* fmt, ...);

  /// Output helper for error messages
  void error(const char* fmt, ...);

  /// Output helper to log messages with tag
  void print(const char* tag, const char* fmt, ...);

  /// Helper to call when a command is not understood
  void command_ignored(const std::string& cmd)  {
    this->error("Command ignored: %s", cmd.c_str());
  }

  /// Run the remote debugger
  void run(const std::string& dns);

  /// Select single node for debugging
  bool select_node(const std::string& node);

  /// Add nodes to selected node set according to <regex>
  bool select_nodes(const std::string& cmd);

  /// Remove nodes from selected node set according to <regex>
  bool unselect_nodes(const std::string& cmd);

  /// Broadcast single command to clients
  bool broadcast_command(const std::string& cmd, const std::string& match = "");

  /// Process a single command request
  bool process_command(const std::string& cmd);

  /// Interactor interrupt handler callback
  virtual void handle(const Event& event)  override;
};

/// Framework include files
#include <CPP/IocSensor.h>
#include <RTL/strdef.h>
#include <RTL/Sys.h>
#include <RTL/rtl.h>
#include <dim/dis.h>
#include <dim/dic.h>

/// C/C++ include files
#include <cerrno>
#include <cstdlib>
#include <cstring>
#include <cstdarg>
#include <csignal>
#include <system_error>

#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>

enum {
  CMD_PRINT,
  CMD_ADD = '+',
  CMD_REMOVE = '-',
  CMD_STUCK = '!',
  CMD_EXIT,
};
using namespace debugger;

#define VAR_ARGS(args, fmt)    va_list args;  va_start(args, fmt)

DebugClient::implementation::client_inventory_t::client_inventory_t(Interactor* parent, long dns)
  : _parent(parent), _dns_id(dns)
{
  std::string svc = "DIS_DNS/SERVER_LIST";
  this->_server_list_id = ::dic_info_service_dns(this->_dns_id, svc.c_str(),MONITORED,0,0,0,handle_response,(long)this,0,0);
}

DebugClient::implementation::client_inventory_t::~client_inventory_t()   {
  if ( this->_server_list_id ) ::dic_release_service(this->_server_list_id);
  this->_server_list_id = 0;
}

/// DIM callback to supply serving log messages
void DebugClient::implementation::client_inventory_t::handle_response(void* tag, void* buff, int* size)   {
  const char* msg = (const char*)buff;
  if ( msg && *msg && *size>0 )  {
    auto* it = *(DebugClient::implementation::client_inventory_t**)tag;
    if ( *msg == '+' )   {
      IocSensor::instance().send(it->_parent, CMD_ADD, new std::string(msg+1));
    }
    else if ( *msg == '!' )   {
      IocSensor::instance().send(it->_parent, CMD_ADD, new std::string(msg+1));
    }
    else if ( *msg == '-' )   {
      IocSensor::instance().send(it->_parent, CMD_REMOVE, new std::string(msg+1));
    }
    else  {
      IocSensor::instance().send(it->_parent, CMD_ADD, new std::string(msg));
    }
  }
}

DebugClient::implementation::client_t::client_t(Interactor* parent, long dns, const std::string& node, const std::string& name)
  : _parent(parent), _node(node), _name(name), _dns_id(dns)
{
  std::string svc = this->_name + "/output"; 
  this->_command  = this->_name + "/command";
  this->_dim_log_id = ::dic_info_service_dns(this->_dns_id, svc.c_str(),MONITORED,0,0,0,handle_log,(long)this,0,0);
}

DebugClient::implementation::client_t::~client_t()  {
  if ( this->_dim_log_id ) ::dic_release_service(this->_dim_log_id);
  this->_dim_log_id = 0;
}

int DebugClient::implementation::client_t::send_command(const std::string& cmd)   {
  int ret = ::dic_cmnd_service_dns(this->_dns_id, this->_command.c_str(), (void*)cmd.c_str(), cmd.length()+1);
  return ret;
}

/// DIM callback to supply serving log messages
void DebugClient::implementation::client_t::handle_log(void* tag, void* buff, int* size)   {
  auto* it = *(DebugClient::implementation::client_t**)tag;
  if ( buff && size && *size )    {
    const char* add = (const char*)buff;
    std::string msg(add, add+*size);
    IocSensor::instance().send(it->_parent, CMD_PRINT, new std::string(std::move(msg)));
  }
}

/// Default constructor
DebugClient::implementation::implementation()   {
  this->_current_node = RTL::str_upper(RTL::nodeNameShort());
}

/// Default destructor
DebugClient::implementation::~implementation()   {
}

/// Generic output helper
void DebugClient::implementation::output(FILE* file, const char* tag, const char* fmt, va_list& args)    {
  char text[4096];
  size_t len = 0;
  if ( !this->_current_node.empty() )   {
    ::snprintf(text+len, sizeof(text)-len, "%-9s", this->_current_node.c_str());
  }
  if ( !this->_process_match.empty() )  {
    len += ::snprintf(text+len, sizeof(text)-len, "'%s' ", this->_process_match.c_str());
  }
  if ( tag && ::strlen(tag) > 0 )   {
    len += ::snprintf(text+len, sizeof(text)-len, "%-9s", tag);
  }
  len += ::vsnprintf(text+len, sizeof(text)-len, fmt, args);
  va_end(args);
  text[len] = '\n';
  text[len+1] = 0;
  ::write(::fileno(file), text, len+1);
  ::fflush(file);
}

/// Output helper for logging messages
void DebugClient::implementation::log(const char* fmt, ...)   {
  if ( fmt )   {
    VAR_ARGS(args, fmt);
    this->output(stdout, "INFO", fmt, args);
  }
}

/// Output helper for error messages
void DebugClient::implementation::error(const char* fmt, ...)   {
  if ( fmt )   {
    VAR_ARGS(args, fmt);
    this->output(stderr, "ERROR", fmt, args);
  }
}

/// Output helper to log messages with tag
void DebugClient::implementation::print(const char* tag, const char* fmt, ...)   {
  if ( fmt )   {
    VAR_ARGS(args, fmt);
    this->output(stdout, tag, fmt, args);
  }
}

/// Re-establish command prompt
void DebugClient::implementation::make_prompt()   {
  ::fprintf(stdout, "%s (gdb) ", _current_node.c_str());
  ::fflush(stdout);
}

std::string DebugClient::implementation::match_from_command(const std::string& cmd)   {
  auto items = RTL::str_split(RTL::str_replace(cmd,"  "," "), " ");
  return (items.size() > 1) ? items[1] : std::string();
}

/// Set server match when asking for possible clients
void DebugClient::implementation::set_server_match(const std::string& match)   {
  int flags = std::regex_constants::icase | std::regex_constants::ECMAScript;
  this->_server_match = std::regex(match, (std::regex_constants::syntax_option_type)flags);
}

/// Add nodes to selected node set according to <regex>
bool DebugClient::implementation::select_nodes(const std::string& match)   {
  if ( !this->_all_clients.empty() )    {
    size_t count = 0;
    int flags = std::regex_constants::icase | std::regex_constants::ECMAScript;
    std::regex rg(match, (std::regex_constants::syntax_option_type)flags);
    for( const auto& c : this->_all_clients )    {
      std::smatch sm;
      bool stat = std::regex_match(c.second->node(), sm, rg);
      if ( !stat ) stat = std::regex_match(c.second->name(), sm, rg);
      if ( stat )    {
	this->_selected_clients.insert(c);
	++count;
      }
    }
    if ( count > 0 )
      this->log("Selected %ld client nodes according to node match: %s", count, match.c_str());
    else
      this->error("Selected %ld client nodes according to node match: %s", count, match.c_str());
    this->make_prompt();
    return true;
  }
  this->error("No clients connected. Please check your server match!");
  this->make_prompt();
  return true;
}

/// Remove nodes from selected node set according to <regex>
bool DebugClient::implementation::unselect_nodes(const std::string& match)   {
  if ( !this->_selected_clients.empty() )    {
    size_t count = 0;
    int flags = std::regex_constants::icase | std::regex_constants::ECMAScript;
    std::regex rg(match, (std::regex_constants::syntax_option_type)flags);
    std::vector<std::string> remove;
    for( const auto& c : this->_selected_clients )    {
      std::smatch sm;
      bool stat = std::regex_match(c.second->node(), sm, rg);
      if ( !stat ) stat = std::regex_match(c.second->name(), sm, rg);
      if ( stat )    {
	remove.emplace_back(c.first);
	++count;
      }
    }
    for( const auto& c : remove )    {
      auto i = this->_selected_clients.find(c);
      if ( i != this->_selected_clients.end() )
	this->_selected_clients.erase(i);
    }
    if ( count > 0 )
      this->log("Removed %ld client nodes from selected node set. Now %ld clients.",
		count, this->_selected_clients.size());
    else
      this->error("Removed ZERO client nodes from selected node set. Now %ld clients.",
		  this->_selected_clients.size());
    this->make_prompt();
    return true;
  }
  this->error("No clients connected. Please check your server match!");
  this->make_prompt();
  return true;
}

/// Select single node for debugging
bool DebugClient::implementation::select_node(const std::string& node)   {
  this->_selected_clients.clear();
  for( const auto& c : this->_all_clients )    {
    if ( ::strcasecmp(c.second->node().c_str(), node.c_str()) == 0 )   {
      this->_current_node = c.second->node();
      this->_selected_clients.insert(c);
      this->log("Selected node %s ONLY.", c.second->node().c_str());
      return true;
    }
    if ( ::strcasecmp(c.second->name().c_str(), node.c_str()) == 0 )   {
      this->_current_node = c.second->node();
      this->_selected_clients.insert(c);
      this->log("Selected node %s ONLY.", c.second->name().c_str());
      return true;
    }
  }
  this->error("No node was selected with name or service %s.", node.c_str());
  return true;
}

/// Broadcast single command to clients
bool DebugClient::implementation::broadcast_command(const std::string& cmd, const std::string& match)   {
  if ( !this->_selected_clients.empty() )    {
    std::size_t count = 0;
    if ( match.empty() )    {
      for( const auto& c : this->_selected_clients )    {
	int sc = c.second->send_command(cmd);
	if ( 1 == sc ) ++count;
      }
    }
    else   {
      int flags = std::regex_constants::icase | std::regex_constants::ECMAScript;
      std::regex rg(match, (std::regex_constants::syntax_option_type)flags);
      for( const auto& c : this->_selected_clients )    {
	std::smatch sm;
	bool stat = std::regex_match(c.second->node(), sm, rg);
	if ( !stat ) stat = std::regex_match(c.second->name(), sm, rg);
	if ( stat )    {
	  int sc = c.second->send_command(cmd);
	  if ( 1 == sc ) ++count;
	}
      }
    }
    if ( count != this->_selected_clients.size() )
      this->error("Not all processes could be reached: %ld out of %ld",
		  count, this->_selected_clients.size());
    else
      this->log("Propagated command: %s to %ld clients.", cmd.c_str(), count);
    this->make_prompt();
    return true;
  }
  this->error("No clients nodes were selected to accept debugger commands!");
  this->make_prompt();
  return true;
}

/// Process a single command request
bool DebugClient::implementation::process_command(const std::string& cmd)   {
  if ( !cmd.empty() && cmd[0] == '.' )    {
    if ( 0 == ::strncasecmp(cmd.c_str(),".quit",2) )   {
      this->_is_running = false;
      IocSensor::instance().send(this, CMD_EXIT, nullptr);
      return false;
    }
    if ( 0 == ::strncasecmp(cmd.c_str(), ".end", 4) )   {
      return this->broadcast_command(".end", this->match_from_command(cmd));
    }
    if ( 0 == ::strncasecmp(cmd.c_str(), ".kill", 5) )   {
      return this->broadcast_command(".kill", this->match_from_command(cmd));
    }
    if ( 0 == ::strncasecmp(cmd.c_str(), ".stop", 5) )   {
      return this->broadcast_command(".stop", this->match_from_command(cmd));
    }
    if ( 0 == ::strncasecmp(cmd.c_str(), ".start", 6) )   {
      return this->broadcast_command(".start", this->match_from_command(cmd));
    }
    if ( 0 == ::strncasecmp(cmd.c_str(), ".status", 7) )   {
      return this->broadcast_command(".status", this->match_from_command(cmd));
    }
    if ( 0 == ::strncasecmp(cmd.c_str(), ".interrupt", 4) )   {
      return this->broadcast_command(".interrupt", this->match_from_command(cmd));
    }
    if ( 0 == ::strncasecmp(cmd.c_str(), ".list", 4) )   {
      return this->broadcast_command(cmd);
    }
    if ( 0 == ::strncasecmp(cmd.c_str(), ".attach", 4) )   {
      return this->broadcast_command(cmd);
    }
    if ( 0 == ::strncasecmp(cmd.c_str(), ".select", 4) )   {
      return this->select_node(this->_current_node);
    }
    if ( 0 == ::strncasecmp(cmd.c_str(), ".add", 4) )   {
      this->_node_match = this->match_from_command(cmd);
      return this->select_nodes(this->_node_match);
    }
    if ( 0 == ::strncasecmp(cmd.c_str(), ".remove", 4) )   {
      std::string match = this->match_from_command(cmd);
      return this->unselect_nodes(match);
    }
    if ( 0 == ::strncasecmp(cmd.c_str(), ".clear", 4) )   {
      this->_selected_clients.clear();
      this->_process_match = "";
      this->_node_match = "";
      return this->broadcast_command(".stop");
    }
    if ( 0 == ::strncasecmp(cmd.c_str(), ".help", 4) )   {
      this->print("HELP", "Debug client commands:");
      this->print("HELP", ".end              Finalize the debug session end debugger and server.");
      this->print("HELP", ".stop             Stop underlying debugger  on all selected nodes.");
      this->print("HELP", ".start            Start underlying debugger on all selected nodes.");
      this->print("HELP", ".kill             Kill remote debugger server.");
      this->print("HELP", ".add <regex>      Add nodes to selected node set according to <regex>");
      this->print("HELP", "                  - Can be called multiple times. Use clear to reset.");
      this->print("HELP", ".remove <regex>   Remove nodes from selected node set according to <regex>");
      this->print("HELP", "                  - Can be called multiple times.");
      this->print("HELP", ".clear            Clear the selected node set and the utgid mask.");
      this->print("HELP", ".attach <regex>   Attach debugger to process matching <utgid>");
      this->print("HELP", "                  - Use <node>, <NODE> tags to dynamically construct <utgid>");
      this->print("HELP", "                  - Selecting <regex> MUST be unique!");
      this->print("HELP", "                  - Verify <regex> with .list <regex> first!");
      this->print("HELP", ".list <regex>     List all processes with a UTGID matching the regular expression.");
      this->print("HELP", "                  - Use <node>, <NODE> tags to dynamically construct <utgid>");
      this->print("HELP", ".help             Print THIS help menu");
      this->print("HELP", "");
      this->print("HELP", "---> All commands not starting with a '.' are directly propagated to gdb (if running)!");
      this->print("HELP", "help              Print gdb help");
      this->make_prompt();
      return true;
    }
    std::error_code ec(EINVAL, std::system_category());
    this->error("Client command unknown: %s [%s]", cmd.c_str(), ec.message().c_str());
    this->make_prompt();
    return true;
  }
  else if ( !cmd.empty() )   {
    return this->broadcast_command(cmd);
  }
  return true;
}

/// Start the standard input message pump
void DebugClient::implementation::_process_stdin()   {
  this->_input = std::make_unique<std::thread>([this] {
      std::string text;
      while( this->_is_running )    {
	char c = 0;
	int nb = ::read(STDIN_FILENO,&c,1);
	if ( nb == 1 )   {
	  if ( c == '\n' )   {
	    if ( !process_command(text) )    {
	      break;
	    }
	    text = "";
	    continue;
	  }
	  text += c;
	}
      }
    });
}

/// Run the remote debugger
void DebugClient::implementation::run(const std::string& dns)   {
  int port = ::dis_get_dns_port();
  this->_dim_dns_name = dns;
  this->_dic_dns_id = ::dic_add_dns(this->_dim_dns_name.c_str(), port);
  this->_client_inventory = std::make_unique<client_inventory_t>(this, this->_dic_dns_id);
  this->_is_running = true;

  this->_process_stdin();
  IocSensor::instance().run();
}

/// Add handler for a single server process
int DebugClient::implementation::_add_server(const std::string& node, const std::string& server)    {
  std::smatch sm;
  bool stat = std::regex_match(server, sm, this->_server_match);
  if ( stat )    {
    std::lock_guard<std::mutex> lock(_dim_vars_lock);
    auto i = this->_all_clients.find(server);
    if ( i == this->_all_clients.end() )   {
      auto j = this->_selected_clients.find(server);
      if ( j == this->_selected_clients.end() )   {
	this->log("+++ Adding debug source: %s", server.c_str());
	this->_all_clients.emplace(server, std::make_shared<client_t>(this, this->_dic_dns_id, node, server));
	return 1;
      }
      this->_all_clients.insert(std::make_pair(j->first, j->second));
      return 1;
    }
  }
  return 0;
}

/// Remove handler for a single server process
int DebugClient::implementation::_remove_server(const std::string& server)    {
  std::lock_guard<std::mutex> lock(_dim_vars_lock);
  auto i = this->_all_clients.find(server);
  if ( i != this->_all_clients.end() )   {
    this->log("+++ Remove debug source: %s", server.c_str());
    this->_all_clients.erase(i);
    return 1;
  }
  return 0;
}

namespace   {
  void get_service_node(char* s, std::string& svc, std::string& node) {
    char* at = strchr(s,'@');
    *at = 0;
    svc = s;
    node = at+1;
  }
}

/// Interactor interrupt handler callback
void DebugClient::implementation::handle(const CPP::Event& event)         {
  if ( event.eventtype == CPP::IocEvent )  {
    switch(event.type)   {
    case CMD_EXIT:
      ::exit(0);
      break;

    case CMD_PRINT:   {
      auto msg = std::make_unique<std::string>(std::move(*event.iocPtr<std::string>()));
      this->print(0, msg->c_str());
      break;
    }

    case CMD_STUCK:
    case CMD_ADD:   {
      auto msg = std::make_unique<std::string>(std::move(*event.iocPtr<std::string>()));
      char *at, *p = (char*)msg->c_str(), *last = (char*)msg->c_str();
      std::string svc, node;
      int count = 0;
      while ( last != 0 && (at=strchr(p,'@')) != 0 )  {
	last = strchr(at,'|');
	if ( last ) *last = 0;
	get_service_node(p, svc, node);
	count += this->_add_server(node, svc);
	p = last+1;
      }
      if ( count > 0 ) this->make_prompt();
      break;
    }

    case CMD_REMOVE:   {
      auto msg = std::make_unique<std::string>(std::move(*event.iocPtr<std::string>()));
      char *at, *p = (char*)msg->c_str(), *last = (char*)msg->c_str();
      std::string svc, node;
      int count = 0;
      while ( last != 0 && (at=strchr(p,'@')) != 0 )  {
	last = strchr(at,'|');
	if ( last ) *last = 0;
	get_service_node(p, svc, node);
	count += this->_remove_server(svc);
	p = last+1;
      }
      if ( count > 0 ) this->make_prompt();
      break;
    }
    default:
      break;
    }
  }
}


/// Default constructor
DebugClient::DebugClient()   {
  this->imp = std::make_unique<implementation>();
}

/// Default destructor
DebugClient::~DebugClient()    {
  this->imp->print("QUIT","Shut down debug server: %s", RTL::processName().c_str());
  this->imp->broadcast_command(".stop");
  this->imp.reset();
}

static std::unique_ptr<DebugClient> s_server {nullptr};

/// Initialize the debug server
extern "C" void debug_server_initialize()   {
  if ( !s_server )   {
    s_server = std::make_unique<DebugClient>();
  }
}

/// Shutdown the debug server
extern "C" void debug_server_finalize()    {
  if ( s_server )   {
    s_server.reset();
  }
}

static void server_help()    {
  ::fprintf(stderr,"Usage: debug_client -arg [-arg...] \n"
	    "  -servers=<regex>            Regular expression to connect to GDB processes at DNS  \n"
	    "  -dns=<node>                 Set DIC DNS node to connect to servers.                \n"
	    " \n"
	    );
  ::exit(EINVAL);
}

int main(int argc, char** argv)   {
  RTL::CLI cli(argc, argv, server_help);
  std::string dns, srv_match;

  cli.getopt("dns",   3, dns);
  cli.getopt("servers", 3, srv_match);

  auto server = std::make_unique<DebugClient>();
  server->imp->set_server_match(srv_match);
  if ( dns.empty() ) server_help();
  server->imp->run(dns);
  return 0;
}
