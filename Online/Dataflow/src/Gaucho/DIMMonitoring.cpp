//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  DIMMonitoring.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

// Include files
#include "DIMMonitoring.h"

#include <Dataflow/Plugins.h>
#include <Dataflow/Incidents.h>
#include <Gaucho/MonitorInterface.h>
#include <RTL/rtl.h>
#include <RTL/strdef.h>
#include <dim/dis.hxx>

// AIDA include files
#include <AIDA/IHistogram1D.h>
#include <AIDA/IHistogram2D.h>
#include <AIDA/IHistogram3D.h>
#include <AIDA/IProfile1D.h>
#include <AIDA/IProfile2D.h>

// ROOT include files
#include <TEnv.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TROOT.h>
#include <TProfile.h>
#include <TProfile2D.h>

// C/C++ include files
#include <cstdint>

using namespace Online;

// Factory for instantiation of service objects
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_DIMMonitoring,DIMMonitoring)

namespace Online  {
  template <typename TO> TO* cast_histo(const AIDA::IBaseHistogram* aidahist);
}

DIMMonitoring::DIMMonitoring(const std::string& nam, Context& ctxt)
  : DataflowComponent(nam,ctxt)
{
  m_started       = false;
  m_monsysrecover = 0;
  m_runno = 0;
  declareProperty("UniqueServiceNames",          m_uniqueServiceNames = 0);
  declareProperty("disableMonRate",              m_disableMonRate = 0);
  declareProperty("disableDimPropServer",        m_disableDimPropServer = 0);
  declareProperty("disableDimCmdServer",         m_disableDimCmdServer = 0);
  // declareProperty("disableDimRcpGaucho",      m_disableDimRcpGaucho = 0);

  declareProperty("disableMonObjectsForBool",    m_disableMonObjectsForBool = 1);
  declareProperty("disableMonObjectsForInt",     m_disableMonObjectsForInt = 0);
  declareProperty("disableMonObjectsForLong",    m_disableMonObjectsForLong = 0);
  declareProperty("disableMonObjectsForDouble",  m_disableMonObjectsForDouble = 0);
  declareProperty("disableMonObjectsForString",  m_disableMonObjectsForString = 1);
  declareProperty("disableMonObjectsForPairs",   m_disableMonObjectsForPairs = 1);
  declareProperty("disableMonObjectsForHistos",  m_disableMonObjectsForHistos = 0);

  declareProperty("disableDeclareInfoBool",      m_disableDeclareInfoBool = 1);
  declareProperty("disableDeclareInfoInt",       m_disableDeclareInfoInt = 0);
  declareProperty("disableDeclareInfoLong",      m_disableDeclareInfoLong = 0);
  declareProperty("disableDeclareInfoDouble",    m_disableDeclareInfoDouble = 0);
  declareProperty("disableDeclareInfoString",    m_disableDeclareInfoString = 1);
  declareProperty("disableDeclareInfoPair",      m_disableDeclareInfoPair = 1);
  declareProperty("disableDeclareInfoFormat",    m_disableDeclareInfoFormat = 0);
  declareProperty("disableDeclareInfoHistos",    m_disableDeclareInfoHistos = 0);
  declareProperty("maxNumCountersMonRate",       m_maxNumCountersMonRate = 1000);
  declareProperty("DimUpdateInterval",           m_updateInterval = 20);
  declareProperty("CounterUpdateInterval",       m_counterInterval = 0);
  declareProperty("ExpandCounterServices",       m_expandCounterServices=0);
  declareProperty("ExpandNameInfix",             m_expandInfix="");
  declareProperty("PartitionName",               m_partname="LHCb");
  declareProperty("ProcessName",                 m_ProcName="");
  declareProperty("ProgramName",                 m_ProgName="");
  declareProperty("DontResetCountersonRunChange",m_dontResetCountersonRunChange=false);
  declareProperty("UseDStoreNames",              m_useDStoreNames=false);
  declareProperty("CounterClasses",m_counterClasses);
  declareProperty("HistogramClasses",m_histogramClasses);
  TH1D::SetDefaultSumw2();
  TH2D::SetDefaultSumw2();
  TProfile::SetDefaultSumw2();
}

DIMMonitoring::~DIMMonitoring()    {
  m_MonIntf.reset();
}

int DIMMonitoring::initialize()   {
  int sc = Component::initialize();
  if (m_counterInterval == 0)
    m_counterInterval = m_updateInterval;
  if ( 0 != m_disableMonRate)  {
    debug("MonRate process is disabled.");
  }
  if (m_ProcName == "")    {
    m_ProcName = RTL::processName();
  }
  if (m_expandCounterServices)    {
    m_expandInfix = RTL::str_replace(m_expandInfix,"<part>",   m_partname);
    m_expandInfix = RTL::str_replace(m_expandInfix,"<proc>",   m_ProcName);
    m_expandInfix = RTL::str_replace(m_expandInfix,"<program>",m_ProgName);
  }
  SubSysParams cpars;
  cpars.ratePrefix   = "R_";
  cpars.dontclear    = m_dontResetCountersonRunChange;
  cpars.expandInfix  = m_expandInfix;
  cpars.expandnames  = m_expandCounterServices;
  cpars.updatePeriod = m_counterInterval;
  cpars.type         = MONSUBSYS_Counter;
  SubSysParams hpars;
  hpars.ratePrefix   = "R_";
  hpars.dontclear    = m_dontResetCountersonRunChange;
  hpars.updatePeriod = m_updateInterval;
  hpars.type         = MONSUBSYS_Histogram;
  m_MonIntf = std::make_unique<MonitorInterface>(RTL::processName(),
						 std::move(cpars),
						 std::move(hpars));
  m_MonIntf->applyCounterClasses(m_counterClasses);
  m_MonIntf->applyHistogramClasses(m_histogramClasses);

  subscribeIncident("DAQ_RUNNING");
  subscribeIncident("DAQ_STOPPED");
  subscribeIncident("DAQ_FINALIZE");
  return sc;
}

/// Incident handler implemenentation: Inform that a new incident has occured
void DIMMonitoring::handle(const DataflowIncident& inc) {
  if ( inc.type == "DAQ_RUNNING" ) {
    //  CALLGRIND_START_INSTRUMENTATION
    debug("Starting publishing monitor information using DIM....");
    //  setProperties();
    //  dis_set_debug_on();
    if ( m_MonIntf )  {
      m_MonIntf->start();
    }
    setRunNo(m_runno);
    m_started = true;
  }
  else if ( inc.type == "DAQ_FINALIZE" )   {
    m_started = false;
    stopUpdate();
  }
  else if ( inc.type == "DAQ_STOPPED" )   {
    if ( m_runno > 100 && m_eor_runno != m_runno )  {
      m_MonIntf->eorUpdate(m_eor_runno = m_runno);
    }
  }
}

/// Monitoring interface: Undeclare single monitoring item
int DIMMonitoring::undeclare(const std::string& owner, const std::string& nam)  {
  if ( m_MonIntf )  {
    std::string oname = !owner.empty() ? owner : std::string("");
    m_MonIntf->undeclare(oname+"/"+nam);
  }
  return DF_SUCCESS;
} 

/** Undeclare monitoring information
    @param owner Owner identifier of the monitoring information
*/
int DIMMonitoring::undeclare(const std::string& owner)  {
  if ( m_MonIntf )  {
    std::string oname = !owner.empty() ? owner : std::string("");
    m_MonIntf->undeclareAll(oname);
  }
  return DF_SUCCESS;
}

void DIMMonitoring::stopUpdate()  {
  if ( m_MonIntf )  {
    m_MonIntf->stop();
    stopSaving();
  }
}

int DIMMonitoring::finalize()  {
  unsubscribeIncidents();
  dim_lock();
  if (m_started)    {
    m_started = false;
    if ( m_MonIntf )  {
      m_MonIntf->stop();
    }
  }
  dim_unlock();
  m_MonIntf.reset();
  debug("Finalized successfully");
  return Component::finalize();
}

void DIMMonitoring::setRunNo(uint32_t runno)  {
  bool is_eor_update = (runno > m_runno) && (m_runno != m_eor_runno) && m_runno > 100; 
  m_runno = runno;
  m_MonIntf->setRunNo(runno);
  if ( is_eor_update )   {
    m_MonIntf->eorUpdate(m_eor_runno = m_runno);
  }
}

template<class T> int
DIMMonitoring::i_declareCounter(const std::string& owner,
                                const std::string& nam,
                                T&  var,
                                const std::string& desc)
{
  debug("Declare counter %s.%s of type %s",
        owner.c_str(),nam.c_str(),typeName(typeid(T)).c_str());
  std::string newName = nam;
  if (nam.find("COUNTER_TO_RATE") != std::string::npos)    {
    newName = extract("COUNTER_TO_RATE", nam);
    newName = owner+"/"+newName;
  }
  else if ( !nam.empty() && !owner.empty() )
    newName = owner+"/"+nam;
  else if ( nam.empty()   )
    newName = owner;
  else if ( owner.empty() )
    newName = nam;

  m_MonIntf->declareCounter(newName,var,desc);
  return DF_SUCCESS;
}

template<class T> int
DIMMonitoring::i_declareContainer(const std::string& owner,
                                  const std::string& nam,
                                  const std::string& fmt,
                                  T&                 var,
                                  const std::string& desc)
{
  std::string newName = nam;
  std::size_t size = var.size() * sizeof(typename T::value_type);
  debug("Declare container counter %s.%s of type %s size: %ld",
        owner.c_str(),nam.c_str(),typeName(typeid(T)).c_str(), size);
  if ( !nam.empty() && !owner.empty() )
    newName = owner+"/"+nam;
  else if ( nam.empty()   )
    newName = owner;
  else if ( owner.empty() )
    newName = nam;
  if ( 0 == size )   {
    this->throwError("declareContainer [%s]: %s", newName.c_str(),
                     "Monitored STL containers must have a non-NULL size!");
  }
  m_MonIntf->declareCounter(newName, fmt, &var.at(0), size, desc);
  return DF_SUCCESS;
}

template<class T> int
DIMMonitoring::i_declarePair(const std::string& owner,
			     const std::string& nam,
			     T&  var1,
			     T&  var2,
			     const std::string& desc)
{
  debug("Declare Pair %s.%s of type %s",
        owner.c_str(), nam.c_str(), typeName(typeid(T)).c_str());
  std::string newName = nam;
  if (nam.find("COUNTER_TO_RATE") != std::string::npos)    {
    newName = extract("COUNTER_TO_RATE", nam);
    newName = owner+"/"+newName;
  }
  else if ( !nam.empty() && !owner.empty() )
    newName = owner+"/"+nam;
  else if ( nam.empty()   )
    newName = owner;
  else if ( owner.empty() )
    newName = nam;
  m_MonIntf->declarePair(newName,var1,var2,desc);
  return DF_SUCCESS;
}

int DIMMonitoring::i_unsupported(const std::string& own, const std::string& nam, const std::type_info& type)  {
  return error("Monitoring: %s/%s -> declare(%s)) not implemented",
	       own.c_str(),nam.c_str(),typeName(type).c_str());
}

template<class T>
int DIMMonitoring::declare_histogram(const std::string& owner, const std::string& nam, T* var, const std::string& descr)
{
  if ( 0 != m_disableDeclareInfoHistos )    {
    debug("m_disableDeclareInfoHistos DISABLED ");
    return DF_SUCCESS;
  }
  if ( m_disableMonObjectsForHistos != 0 )    {
    return error("DIMMonitoring doesn't have support for declaring histograms/profiles without using MonObjects ");
  }
  std::string hnam = (nam.find(owner) == std::string::npos) ? owner+"/"+nam : nam;
  m_MonIntf->declareHistogram(hnam,var,descr);
  return DF_SUCCESS;
}

/// Monitoring interface: Declare monitoring entity (Ratio of two numbers)
int DIMMonitoring::declare(const std::string& owner,
                           const std::string& nam,
                           const void* var1,
                           const void* var2,
                           const std::type_info& typ,
                           const std::string& desc)
{
  if ( typ == typeid(int16_t) )
    return i_declarePair(owner, nam, *(int16_t*)var1,  *(int16_t*)var2, desc) ;
  else if ( typ == typeid(uint16_t) )
    return i_declarePair(owner, nam, *(uint16_t*)var1, *(uint16_t*)var2, desc) ;
  else if ( typ == typeid(int32_t) )
    return i_declarePair(owner, nam, *(int32_t*)var1,  *(int32_t*)var2, desc);
  else if ( typ == typeid(uint32_t) )
    return i_declarePair(owner, nam, *(uint32_t*)var1, *(uint32_t*)var2, desc);
  else if ( typ == typeid(int64_t) )
    return i_declarePair(owner, nam, *(int64_t*)var1,  *(int64_t*)var2, desc) ;
  else if ( typ == typeid(uint64_t) )
    return i_declarePair(owner, nam, *(uint64_t*)var1, *(uint64_t*)var2, desc) ;

  return i_unsupported(owner,nam,typ);
}

/// Monitoring interface: Declare monitoring entity: Scalar item
int DIMMonitoring::declare(const std::string&    owner,
                           const std::string&    nam,
                           const void*           var,
                           const std::type_info& typ,
                           const std::string&    desc)
{
  AIDA::IBaseHistogram *dyn_var = (AIDA::IBaseHistogram*)const_cast<void*>(var);
  if ( typ == typeid(bool) )  {
    return i_unsupported(owner,nam,typ);
  }
  else if ( typ == typeid(int8_t) )   {
    return (0 == m_disableDeclareInfoInt) ? i_declareCounter(owner,nam,*(int8_t*)var,desc) : DF_SUCCESS;
  }
  else if ( typ == typeid(int16_t) )   {
    return (0 == m_disableDeclareInfoInt) ? i_declareCounter(owner,nam,*(int16_t*)var,desc) : DF_SUCCESS;
  }
  else if ( typ == typeid(int32_t) )   {
    return (0 == m_disableDeclareInfoInt) ? i_declareCounter(owner,nam,*(int32_t*)var,desc) : DF_SUCCESS;
  }
  else if ( typ == typeid(int64_t) )  {
    return (0 == m_disableDeclareInfoLong) ? i_declareCounter(owner,nam,*(int64_t*)var,desc) : DF_SUCCESS;
  }
  else if ( typ == typeid(uint8_t) )  {
    return (0 == m_disableDeclareInfoInt) ? i_declareCounter(owner,nam,*(uint8_t*)var,desc) : DF_SUCCESS;
  }
  else if ( typ == typeid(uint16_t) )  {
    return (0 == m_disableDeclareInfoInt) ? i_declareCounter(owner,nam,*(uint16_t*)var,desc) : DF_SUCCESS;
  }
  else if ( typ == typeid(uint32_t) )  {
    return (0 == m_disableDeclareInfoInt) ? i_declareCounter(owner,nam,*(uint32_t*)var,desc) : DF_SUCCESS;
  }
  else if ( typ == typeid(uint64_t) )  {
    return (0 == m_disableDeclareInfoLong) ? i_declareCounter(owner,nam,*(uint64_t*)var,desc) : DF_SUCCESS;
  }
  else if ( typ == typeid(double) )  {
    return i_declareCounter(owner,nam,*(double*)var,desc);
  }
  else if ( typ == typeid(float) )  {
    return i_declareCounter(owner,nam,*(float*)var,desc);
  }
  else if ( typ == typeid(std::string) )  {
    return i_unsupported(owner,nam,typ);
  }
  /// Atomic scalars
  else if ( typ == typeid(std::atomic<int8_t>) )  {
    return i_declareCounter(owner,nam,*(std::atomic<int8_t>*)var,desc);
  }
  else if ( typ == typeid(std::atomic<int16_t>) )  {
    return i_declareCounter(owner,nam,*(std::atomic<int16_t>*)var,desc);
  }
  else if ( typ == typeid(std::atomic<int32_t>) )  {
    return i_declareCounter(owner,nam,*(std::atomic<int32_t>*)var,desc);
  }
  else if ( typ == typeid(std::atomic<int64_t>) )  {
    return i_declareCounter(owner,nam,*(std::atomic<int64_t>*)var,desc);
  }
  else if ( typ == typeid(std::atomic<uint8_t>) )  {
    return i_declareCounter(owner,nam,*(std::atomic<uint8_t>*)var,desc);
  }
  else if ( typ == typeid(std::atomic<uint16_t>) )  {
    return i_declareCounter(owner,nam,*(std::atomic<uint16_t>*)var,desc);
  }
  else if ( typ == typeid(std::atomic<uint32_t>) )  {
    return i_declareCounter(owner,nam,*(std::atomic<uint32_t>*)var,desc);
  }
  else if ( typ == typeid(std::atomic<uint64_t>) )  {
    return i_declareCounter(owner,nam,*(std::atomic<uint64_t>*)var,desc);
  }
  else if ( typ == typeid(std::atomic<float>) )  {
    return i_declareCounter(owner,nam,*(std::atomic<float>*)var,desc);
  }
  else if ( typ == typeid(std::atomic<double>) )  {
    return i_declareCounter(owner,nam,*(std::atomic<double>*)var,desc);
  }
  /// Vector entities
  else if ( typ == typeid(std::vector<int8_t>) )  {
    return i_declareContainer(owner,nam,"i",*(std::vector<int8_t>*)var,desc);
  }
  else if ( typ == typeid(std::vector<int16_t>) )  {
    return i_declareContainer(owner,nam,"i",*(std::vector<int16_t>*)var,desc);
  }
  else if ( typ == typeid(std::vector<int32_t>) )  {
    return i_declareContainer(owner,nam,"i",*(std::vector<int32_t>*)var,desc);
  }
  else if ( typ == typeid(std::vector<int64_t>) )  {
    return i_declareContainer(owner,nam,"x",*(std::vector<int64_t>*)var,desc);
  }
  else if ( typ == typeid(std::vector<uint8_t>) )  {
    return i_declareContainer(owner,nam,"i",*(std::vector<uint8_t>*)var,desc);
  }
  else if ( typ == typeid(std::vector<uint16_t>) )  {
    return i_declareContainer(owner,nam,"i",*(std::vector<uint16_t>*)var,desc);
  }
  else if ( typ == typeid(std::vector<uint32_t>) )  {
    return i_declareContainer(owner,nam,"i",*(std::vector<uint32_t>*)var,desc);
  }
  else if ( typ == typeid(std::vector<uint64_t>) )  {
    return i_declareContainer(owner,nam,"x",*(std::vector<uint64_t>*)var,desc);
  }
  else if ( typ == typeid(std::vector<float>) )  {
    return i_declareContainer(owner,nam,"f",*(std::vector<float>*)var,desc);
  }
  else if ( typ == typeid(std::vector<double>) )  {
    return i_declareContainer(owner,nam,"d",*(std::vector<double>*)var,desc);
  }
  
  else if ( typ == typeid(std::pair<double,double>) )  {
    return i_unsupported(owner,nam,typ);
  }
  //  else if ( typ == typeid(TH3D) )  {
  //    return declare_histogram(owner,nam,(TH3D*)var,desc);
  //  }
  else if ( typ == typeid(TH2D) )  {
    return declare_histogram(owner,nam,(TH2D*)var,desc);
  }
  else if ( typ == typeid(TH1D) )  {
    return declare_histogram(owner,nam,(TH1D*)var,desc);
  }
  else if ( typ == typeid(TProfile) )  {
    return declare_histogram(owner,nam,(TProfile*)var,desc);
  }
  else if ( typ == typeid(TProfile2D) )  {
    return declare_histogram(owner,nam,(TProfile2D*)var,desc);
  }
  //  else if ( typ == typeid(AIDA::IHistogram3D) && cast_histo<AIDA::IHistogram3D>(dyn_var) )  {
  //    TH3D* h = cast_histo<TH3D>(dyn_var);
  //    return declare_histogram(owner,nam,h,desc);
  //  }
  else if ( typ == typeid(AIDA::IHistogram2D) && cast_histo<AIDA::IHistogram2D>(dyn_var) )  {
    TH2D* h = cast_histo<TH2D>(dyn_var);
    return declare_histogram(owner,nam,h,desc);
  }
  else if ( typ == typeid(AIDA::IHistogram1D) && cast_histo<AIDA::IHistogram1D>(dyn_var) )  {
    TH1D* h = cast_histo<TH1D>(dyn_var);
    return declare_histogram(owner,nam,h,desc);
  }
  else if ( typ == typeid(AIDA::IProfile2D) && cast_histo<AIDA::IProfile2D>(dyn_var) )  {
    TProfile2D* h = cast_histo<TProfile2D>(dyn_var);
    return declare_histogram(owner,nam,h,desc);
  }
  else if ( typ == typeid(AIDA::IProfile1D) && cast_histo<AIDA::IProfile1D>(dyn_var) )  {
    TProfile* h = cast_histo<TProfile>(dyn_var);
    return declare_histogram(owner,nam,h,desc);
  }
  else if ( cast_histo<AIDA::IBaseHistogram>(dyn_var) )  {
    return error("Unknown histogram type (%s). Source:%s.%s",
		 typeName(typeid(var)).c_str(),owner.c_str(),nam.c_str());
  }
  return i_unsupported(owner,nam,typ);
}

/// Monitoring interface: Declare monitoring entity (array with size)
int DIMMonitoring::declare(const std::string&    owner, 
                           const std::string&    nam, 
                           const void*           var,
                           size_t                size,
                           const std::type_info& typ,
                           const std::string&    desc)
{
  std::string format;
  std::string cname;
  if ( !nam.empty() && !owner.empty() ) cname = owner+"/"+nam;
  else if ( nam.empty()   ) cname = owner;
  else if ( owner.empty() ) cname = nam;

  //  char fch = ::tolower(format[0]);
  if ( typ == typeid(int8_t*) )
    m_MonIntf->declareCounter<int8_t*>(cname, "x", (int8_t*)var, size, desc);
  else if ( typ == typeid(uint8_t*) )
    m_MonIntf->declareCounter<uint8_t*>(cname, "x", (uint8_t*)var, size, desc);
  else if ( typ == typeid(int16_t*) )
    m_MonIntf->declareCounter<int16_t*>(cname, "x", (int16_t*)var, size, desc);
  else if ( typ == typeid(uint16_t*) )
    m_MonIntf->declareCounter<uint16_t*>(cname, "x", (uint16_t*)var, size, desc);
  else if ( typ == typeid(int32_t*) )
    m_MonIntf->declareCounter<int32_t*>(cname, "i", (int32_t*)var, size, desc);
  else if ( typ == typeid(uint32_t*) )
    m_MonIntf->declareCounter<uint32_t*>(cname, "i", (uint32_t*)var, size, desc);
  else if ( typ == typeid(int64_t*) )
    m_MonIntf->declareCounter<int64_t*>(cname, "x", (int64_t*)var, size, desc);
  else if ( typ == typeid(uint64_t*) )
    m_MonIntf->declareCounter<uint64_t*>(cname, "x", (uint64_t*)var, size, desc);
  else if ( typ == typeid(long long int*) )
    m_MonIntf->declareCounter<long long int*>(cname, "x", (long long int*)var, size, desc);
  else if ( typ == typeid(float*) )
    m_MonIntf->declareCounter<float*>(cname, "f", (float*)var, size, desc);
  else if ( typ == typeid(double*) )
    m_MonIntf->declareCounter<double*>(cname, "d", (double*)var, size, desc);

  return DF_SUCCESS;
}

/// Monitoring interface: Declare monitoring entity (structure with given format)
int DIMMonitoring::declare(const std::string&     owner,
                           const std::string&     nam,
                           const void*            /* ptr          */,
                           std::size_t            /* data_length  */,
                           const std::type_info&  typ,
                           const std::string&     /* fmt          */,
                           const std::string&     /* desc         */)
{
  return i_unsupported(owner, nam, typ);
}

std::string DIMMonitoring::extract(const std::string mascara, std::string value)   const {
  if (value.find(mascara) != std::string::npos) {
    value.erase(0, mascara.size()+1);
    value.erase(value.size() - 1, 1);
  }
  return value;
}

std::string DIMMonitoring::infoOwnerName( const Component* owner )  const {
  return owner ? owner->name : std::string();
}

/// Monitoring interface: Update monitoring items
int DIMMonitoring::update(const std::string& , const std::string& , int runno)  {
  info("Monitor Service: Issuing EOR Update for Run %d",runno);
  m_MonIntf->eorUpdate(runno);
  return DF_SUCCESS;
}

/// Monitoring interface: Reset some/all monitoring items
int DIMMonitoring::reset(const std::string& owner, const std::string& what)  {
  if ( RTL::str_upper(what) == "HISTOS" )  {
    resetHistos(owner);
  }
  return DF_SUCCESS;
}

void DIMMonitoring::updatePerSvc(unsigned long ref)  {
  m_MonIntf->update(ref);
}

void DIMMonitoring::resetHistos(const std::string& )  {
  m_MonIntf->resetHistos();
}

void DIMMonitoring::lock(void)  {
  m_MonIntf->lock();
}

void DIMMonitoring::unlock(void)  {
  m_MonIntf->unlock();
}

void DIMMonitoring::startSaving(std::shared_ptr<TaskSaveTimer>& timer)   {
  m_MonIntf->startSaving(timer);
}

void DIMMonitoring::stopSaving()   {
  if (m_MonIntf!= 0)
    {
      m_MonIntf->stopSaving();
    }
}
