//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  DIMMonitoring.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_DIMMONITORING_H
#define ONLINE_DATAFLOW_DIMMONITORING_H

/// Framework include files
#include <Dataflow/DataflowComponent.h>
#include <Dataflow/DataflowContext.h>
#include <Gaucho/TaskSaveTimer.h>
#include <Gaucho/ObjMap.h>

/// C/C++ include files
#include <typeinfo>
#include <string>
#include <map>
#include <set>

// Forward declarations
class DimService;
namespace AIDA { class IBaseHistogram; }

namespace Online   {

  class MonitorInterface;

  /// Dataflow monitoring facility using DIM.
  /** @class DIMMonitoring DIMMonitoring.h Gaucho/DIMMonitoring.h
   *
   *  This class implements the IDIMMonitoring interface, and publishes Gaudi variables
   *  to outside monitoring processes with Dim.
   *
   *  A DimPropServer is started which takes string commands in the format
   *  Algorithm.Property and returns the value of the property.
   *
   *  @author Philippe Vannerem
   *  @author Jose Helder Lopes Jan. 2005
   *  @author Jose Helder Lopes 2006/12/26: Modified to publish general c-structures and AIDA::IProfile1D
   *  @author Juan Otalora Goicochea 2007/11/20: MonObjects
   *  @author Markus Frank   Fork off for Dataflow
   */
  class DIMMonitoring : public DataflowContext::Monitor, public DataflowComponent  {

  public:
    typedef std::map<std::string,void*>                InfoMap;
    typedef std::set<std::string>                      ComponentItems;
    typedef std::map<const Component*,ComponentItems > InfoNamesMap;

  private:

    /// Properties:
    std::string    m_partname;
    std::string    m_ProcName;
    std::string    m_ProgName;
    std::string    m_expandInfix;

    int            m_updateInterval;
    int            m_counterInterval;
    int            m_uniqueServiceNames;
    bool           m_expandCounterServices;
    bool           m_dontResetCountersonRunChange;
    bool           m_useDStoreNames;
    int            m_maxNumCountersMonRate;

    /// Properties steering the published values
    int            m_disableMonObjectsForBool;
    int            m_disableMonObjectsForInt;
    int            m_disableMonObjectsForLong;
    int            m_disableMonObjectsForDouble;
    int            m_disableMonObjectsForString;
    int            m_disableMonObjectsForPairs;
    int            m_disableMonObjectsForHistos;

    int            m_disableDeclareInfoBool;
    int            m_disableDeclareInfoInt;
    int            m_disableDeclareInfoLong;
    int            m_disableDeclareInfoDouble;
    int            m_disableDeclareInfoString;
    int            m_disableDeclareInfoPair;
    int            m_disableDeclareInfoFormat;
    int            m_disableDeclareInfoHistos;

    int            m_disableMonRate;
    int            m_disableDimPropServer;
    int            m_disableDimCmdServer;
    //int          m_disableDimRcpGaucho;

    std::vector<std::string> m_counterClasses;
    std::vector<std::string> m_histogramClasses;

  private:
    std::unique_ptr<MonitorInterface> m_MonIntf;
    bool           m_started;
    int            m_monsysrecover;
    uint32_t       m_runno;
    uint32_t       m_eor_runno;

    // ----------------------------------------------------------------------------------------------
    std::pair<std::string, std::string> registerDimSvc(const std::string& name, const std::string& dimPrefix, const Component* owner, bool isComment);
    void undeclService(std::string infoName);
    void updateService(std::string infoName, bool endOfRun);

    std::string infoOwnerName(const Component* owner)  const;
    std::string extract(const std::string mascara, std::string value)  const;
    void stopUpdate();
    void makeHistSubSys();
    void makeCounterSubSys();

    virtual int i_unsupported(const std::string& owner, const std::string& name, const std::type_info& typ);

    template<class T>
    int declare_histogram(const std::string& owner,
                          const std::string& nam,
                          T* var,
                          const std::string& desc);

    template<class T>
    int i_declareCounter(const std::string& oname,
                         const std::string& name,
                         T&  var,
                         const std::string& desc);

    template<class T>
    int i_declareContainer(const std::string& oname,
                           const std::string& name,
                           const std::string& fmt,
                           T&  var,
                           const std::string& desc);

    template<class T>
    int i_declarePair(const std::string& oname,
                      const std::string& name,
                      T&  var1,
                      T&  var2,
                      const std::string& desc);

  public:
    DIMMonitoring(const std::string& name, Context& ctxt);
    virtual ~DIMMonitoring();
    virtual int initialize()  override;
    virtual int finalize()  override;

    void lock(void);
    void unlock(void);

    /// Incident handler implemenentation: Inform that a new incident has occured
    virtual void handle(const DataflowIncident& inc)  override;

    /// Monitoring interface: Declare monitoring entity: Scalar item
    virtual int  declare(const std::string&    owner,
                         const std::string&    nam,
                         const void*           ptr,
                         const std::type_info& typ,
                         const std::string&    desc)  override;
    /// Monitoring interface: Declare monitoring entity (Ratio of two numbers)
    virtual int  declare(const std::string&    owner,
                         const std::string&    nam,
                         const void*           var1,
                         const void*           var2,
                         const std::type_info& typ,
                         const std::string&    desc)  override;
    /// Monitoring interface: Declare monitoring entity (array with size)
    virtual int declare(const std::string&     owner,
                        const std::string&     nam,
                        const void*            ptr,
                        std::size_t            data_length,
                        const std::type_info&  typ,
                        const std::string&     desc)  override;
    /// Monitoring interface: Declare monitoring entity (structure with given format)
    virtual int declare(const std::string&     owner,
                        const std::string&     nam,
                        const void*            ptr,
                        std::size_t            data_length,
                        const std::type_info&  typ,
                        const std::string&     fmt,
                        const std::string&     desc)  override;
    /// Monitoring interface: Undeclare single monitoring item
    virtual int undeclare(const std::string& parent,const std::string& nam)  override;
    /// Monitoring interface: Undeclare monitoring information
    virtual int undeclare(const std::string& parent)  override;
    /// Monitoring interface: Update monitoring items
    virtual int update(const std::string& parent,const std::string& name, int runno)  override;
    /// Monitoring interface: Reset some/all monitoring items
    virtual int reset(const std::string& parent,const std::string& what)  override;


    virtual void startSaving(std::shared_ptr<TaskSaveTimer>& timer);
    virtual void stopSaving();

    void updatePerSvc(unsigned long ref);

    /** Update all monitoring information
        @param owner Owner identifier of the monitoring information
    */
    void updateAll(bool, const std::string&) {};

    void resetHistos(const std::string& parent);

    /// Update current run number context for updates
    virtual void setRunNo(uint32_t runno) override;

    /// Access to the histogram object map. If not existing NULL is retuned.
    const MonitorObjects* histObjMap()  const;
    /// Access to the counter object map. If not existing NULL is retuned.
    const MonitorObjects* cntrObjMap()  const;

  public:
    void enableMonObjectsForBool()       {  m_disableMonObjectsForBool    = 0;  }
    void enableMonObjectsForInt()        {  m_disableMonObjectsForInt     = 0;  }
    void enableMonObjectsForLong()       {  m_disableMonObjectsForLong    = 0;  }
    void enableMonObjectsForDouble()     {  m_disableMonObjectsForDouble  = 0;  }
    void enableMonObjectsForString()     {  m_disableMonObjectsForString  = 0;  }
    void enableMonObjectsForPairs()      {  m_disableMonObjectsForPairs   = 0;  }
    void enableMonObjectsForHistos()     {  m_disableMonObjectsForHistos  = 0;  }


    void disableMonObjectsForBool()      {  m_disableMonObjectsForBool    = 1;  }
    void disableMonObjectsForInt()       {  m_disableMonObjectsForInt     = 1;  }
    void disableMonObjectsForLong()      {  m_disableMonObjectsForLong    = 1;  }
    void disableMonObjectsForDouble()    {  m_disableMonObjectsForDouble  = 1;  }
    void disableMonObjectsForString()    {  m_disableMonObjectsForString  = 1;  }
    void disableMonObjectsForPairs()     {  m_disableMonObjectsForPairs   = 1;  }
    void disableMonObjectsForHistos()    {  m_disableMonObjectsForHistos  = 1;  }
  };
}      // End namespace Online
#endif // ONLINE_DATAFLOW_DIMMONITORING_H
