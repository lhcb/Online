//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
// Created on: May 28, 2021
//==========================================================================

/// Framework include files
#include <hist_adder/eor_check.h>
#include <RTL/strdef.h>
#include <RTL/rtl.h>

/// C/C++ include files
#include <filesystem>
#include <iostream>

namespace fs = std::filesystem;

/// Initializing constructor
Online::histos::eor_check::eor_check(monitor& m)
  : mon(m)
{
}

std::size_t Online::histos::eor_check::check_file(const fs::path& path)   {
  auto dirs = RTL::str_split(path.string(), "/");
  if ( dirs.size() > 0 )   {
    std::string fname = dirs[dirs.size()-1];
    auto comps = RTL::str_split(path.string(), "-");
    if ( comps.size() > 2 )   {
      char* end = nullptr;
      errno = 0;
      int32_t runno = ::strtol(comps[1].c_str(), &end, 10);
      if ( errno == EINVAL || errno == ERANGE )   {
	std::cout << "Invalid file name: " << path.string() << std::endl;
	return 0;
      }
      if ( runno > this->last_run_number )   {
	this->last_run_number = runno;
      }
      std::size_t i1 = 0, i2 = 0;
      if ( comps.size() == 4 && comps[3] == "EOR" )
	this->eor_files[runno] += 1;
      else
	this->histogram_files[runno] += 1;
      i1 = this->histogram_files[runno];
      i2 = this->eor_files[runno];
      return (i1 == 1 || i2 == 1) && (i1 + i2) == 1 ? 1 : 0;
    }
  }
  return 0;
}

std::size_t Online::histos::eor_check::check_directory(const fs::path& path)   {
  std::error_code ec;
  std::size_t new_runs = 0;
  if ( fs::is_directory(path, ec) )    {
    for(auto it = fs::directory_iterator(path); it != fs::directory_iterator(); ++it)   {
      new_runs += this->check_directory(*it);
    }
  }
  else if ( fs::is_regular_file(path, ec) )   {
    new_runs += this->check_file(path);
  }
  return new_runs;
}

/// Verify that this run has histogram files
std::size_t Online::histos::eor_check::check_disk()    {
  std::error_code ec;
  std::size_t new_runs = 0;
  fs::path hist_path = this->base_directory;
  if ( fs::exists(hist_path, ec) )    {
    if ( fs::is_directory(hist_path, ec) )    {
      for(auto it = fs::directory_iterator(hist_path); it != fs::directory_iterator(); ++it)   {
	new_runs += this->check_directory(*it);
      }
    }
  }
  return new_runs;
}

