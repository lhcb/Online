//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
// Created on: May 28, 2021
//==========================================================================

/// Framework include files
#include <hist_adder/file_generator.h>
#include <hist_adder/monitor.h>
#include <RTL/strdef.h>

/// ROOT include files
#include <TSystem.h>
#include <TError.h>
#include <TFile.h>

/// C/C++ include files
#include <filesystem>

namespace fs = std::filesystem;

/// Initializing constructor
Online::histos::file_generator::file_generator(monitor& m)
  : mon(m), task_name("invalid")
{
}

std::string Online::histos::file_generator::temporary_file_name(const replacements_t& replacements)   const   {
  std::string    pattern      = temporary_pattern.empty() ? path_pattern : temporary_pattern;
  fs::path       fname        = output_file_name(pattern, replacements);
  fs::path       dir          = output_file_name(temporary_directory, replacements);
  fs::path       name         = fs::path(output_file_name(fname.filename().string(), replacements));
  fs::path       temp_path    = dir / name;
  return temp_path.string();
}

std::string Online::histos::file_generator::output_file_name(const replacements_t& replacements)   const   {
  return output_file_name(path_pattern, replacements);
}

std::string Online::histos::file_generator::output_file_name(const std::string& pattern,
							     const replacements_t& replacements)   const   {
  std::string file = pattern;
  for( const auto& r : replacements )  {
    file = RTL::str_replace(file, r.first, r.second);
  }
  std::string pid = std::to_string(long(::lib_rtl_pid()));
  pid = (pid+"              ").substr(0, 6);
  file = RTL::str_replace(file, "${PID}", pid);
  return file;
}

Online::histos::file_generator::replacements_t
Online::histos::file_generator::file_replacements(int runno)   const   {
  std::string run  = std::to_string(runno);
  if ( task_name.empty() || run.empty() )   {
    return {};
  }
  struct tm tm;
  char timestr[128];
  std::time_t now = ::time(0);
  std::string clk      = std::to_string(long(clock()));
  std::string run100   = std::to_string(long(runno/100)*100);
  std::string run1000  = std::to_string(long(runno/1000)*1000);
  std::string run10000 = std::to_string(long(runno/10000)*10000);
  std::vector<std::pair<std::string, std::string> > replacements;

  ::localtime_r(&now, &tm);
  ::strftime(timestr, sizeof(timestr), "%Y%m%dT%H%M%S", &tm);
  replacements.emplace_back(std::make_pair("${CLOCK}",     clk));
  replacements.emplace_back(std::make_pair("${TIME}",      timestr));
  replacements.emplace_back(std::make_pair("${TASK}",      task_name));
  replacements.emplace_back(std::make_pair("${PARTITION}", partition));
  replacements.emplace_back(std::make_pair("${RUN}",       run));
  replacements.emplace_back(std::make_pair("${RUN100}",    run100));
  replacements.emplace_back(std::make_pair("${RUN1000}",   run1000));
  replacements.emplace_back(std::make_pair("${RUN10000}",  run10000));
  return replacements;
}

/// Open new output file according to runb information
template <> std::unique_ptr<TFile> Online::histos::file_generator::open<void>(int runno)  const   {
  replacements_t repl  = file_replacements(runno);
  std::string    fname = output_file_name(path_pattern, repl);
  if ( fname.empty() )   {
    mon.updateLog(LIB_RTL_FATAL,
                      "No run number present to creating output directory [Invalid call]");
    ::exit(ENOENT);
  }
  FileStat_t stat;
  fs::path dname = fs::path(fname).parent_path();
  if ( 0 != gSystem->GetPathInfo(dname.c_str(), stat) )  {
    if ( gSystem->mkdir(dname.c_str(), kTRUE) != 0 )   {
      mon.updateLogMsg(LIB_RTL_FATAL, "Error creating output directory: %s", dname.c_str());
      ::exit(ENOENT);
    }
  }
  std::unique_ptr<TFile> output(TFile::Open(fname.c_str(), "RECREATE"));
  if ( !output || output->IsZombie())   {
    mon.updateLogMsg(LIB_RTL_FATAL, "Error opening histogram output: %s", fname.c_str());
    ::exit(ENOENT);
  }
  return output;
}

