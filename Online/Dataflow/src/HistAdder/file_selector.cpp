//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
// Created on: May 28, 2021
//==========================================================================

/// Framework include files
#include <hist_adder/file_selector.h>
#include <hist_adder/file_generator.h>
#include <hist_adder/hist_adder.h>
#include <hist_adder/run_adder.h>
#include <hist_adder/hist_info.h>
#include <hist_adder/monitor.h>
#include <RTL/strdef.h>

/// C/C++ include files
#include <ctime>
#include <cmath>
#include <thread>
#include <sstream>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/types.h>

namespace fs = std::filesystem;

namespace {
  std::mutex root_protection_lock;
}

/// Online namespace declaration
namespace Online   {

  /// Histograms sub-namespace
  namespace histos  {

    class child_pool   {
      int count_children { 1 };
      monitor& mon;
      std::unique_ptr<std::thread> wait_kids;
      std::map<int32_t, std::unique_ptr<file_selector::child_t> > children;
    public:
      std::mutex  pool_lock;
    public:
      child_pool(monitor& m) : mon(m) {}
      ~child_pool() = default;
      static std::shared_ptr<child_pool> instance(monitor& m);
      std::pair<bool, std::unique_ptr<file_selector::child_t> > remove(pid_t pid);
      bool insert(std::unique_ptr<file_selector::child_t>&& child);
      void wait_children();
    };
  }    // End namespace histos
}      // End namespace Online

std::shared_ptr<Online::histos::child_pool>
Online::histos::child_pool::instance(monitor& m)   {
  static std::shared_ptr<child_pool> pool;
  if ( !pool )   {
    pool = std::make_shared<child_pool>(m);
    pool->wait_kids.reset(new std::thread([]() { pool->wait_children(); } ));
  }
  return pool;
}

std::pair<bool, std::unique_ptr<Online::histos::file_selector::child_t> >
Online::histos::child_pool::remove(pid_t pid)   {
  std::lock_guard<std::mutex> guard(pool_lock);
  auto iter = children.find(pid);
  if ( iter != children.end() )   {
    auto ret = std::make_pair(true, std::move(iter->second));
    children.erase(iter);
    return ret;
  }
  return std::make_pair(false, std::unique_ptr<file_selector::child_t>());
}

bool Online::histos::child_pool::insert(std::unique_ptr<file_selector::child_t>&& child)   {
  std::lock_guard<std::mutex> guard(pool_lock);
  auto iter = children.find(child->pid);
  if ( iter == children.end() )   {
    return children.emplace(child->pid, std::move(child)).second;
  }
  return false;
}

void Online::histos::child_pool::wait_children()   {
  do   {
    int status = 0;
    auto w_pid = ::waitpid(-1, &status, WNOHANG);
    if ( w_pid > 0 )  {  {
	std::lock_guard<std::mutex> guard(pool_lock);
	std::map<int32_t, std::unique_ptr<file_selector::child_t> >::iterator iter = children.find(w_pid);
	if ( iter != children.end() )   {
	  if ( iter->second.get() == nullptr )    {
	    mon.updateLogMsg(LIB_RTL_ALWAYS,
			     "PID: %d: ALARM: INVALID Child !!!!\n", ::lib_rtl_pid());
	  }
	  mon.updateLogMsg(LIB_RTL_ALWAYS,"PID: %d: Child with PID %d finished. "
			   "Status: %d [# Files: %ld, %s] Finished: %ld\n",
			   ::lib_rtl_pid(), w_pid, status,
			   iter->second->files.size(),
			   iter->second->pattern.c_str(),
			   iter->second->selector->children.size());
	  iter->second->selector->children.emplace(iter->first, std::move(iter->second));
	  children.erase(iter);
	  continue;
	}
      }
      mon.updateLogMsg(LIB_RTL_ALWAYS,
		       "PID: %d: Unsolicited child exit with PID %d. Status: %d\n",
		       ::lib_rtl_pid(), w_pid, status);
    }
    ::lib_rtl_sleep(100);
  } while ( count_children );
}

/// Initializing constructor
Online::histos::file_selector::file_selector(monitor& m, file_generator& output)
  : mon(m), out_file_gen(output)
{
  all_children = child_pool::instance(this->mon);
}

/// Default destructor
Online::histos::file_selector::~file_selector()  {
  histogram_files.clear();
  all_children.reset();
  children.clear();
}

bool Online::histos::file_selector::unlink_file(int /* flag */,
						const std::string& file_name)  const  {
  if ( !file_name.empty() )    {
    ::unlink(file_name.c_str());
    return true;
  }
  return false;
}

/// Delete empty histogram directories
void Online::histos::file_selector::delete_directory(const std::filesystem::path& entry, bool current)  {
  std::error_code ec;
  if ( fs::exists(entry, ec) )    {
    if ( fs::is_directory(entry, ec) )    {
      for(auto it = fs::directory_iterator(entry); it != fs::directory_iterator(); ++it)  {
	delete_directory(*it, true);
      }
      if ( current && fs::is_empty(entry, ec) )   {
	struct stat stat;
	if ( ::stat(entry.c_str(), &stat) != -1 )   {
	  if ( ::time(0) - stat.st_ctime > 200 )   {
	    mon.updateLogMsg(LIB_RTL_INFO, "Delete disk directory: %s", entry.c_str());
	    fs::remove(entry, ec);
	  }
	}
      }
    }
  }
}

/// Add all matching histograms to the list of files to be merged
void Online::histos::file_selector::load_directory(const std::filesystem::path& entry,
						   int32_t runno,
						   std::map<int32_t, std::vector<std::string> >& files)
{
  std::error_code ec;
  if ( fs::exists(entry, ec) )    {
    if ( fs::is_directory(entry, ec) )    {
      mon.updateLogMsg(LIB_RTL_INFO, "Load disk directory: %s", entry.c_str());
      for(auto it = fs::directory_iterator(entry); it != fs::directory_iterator(); ++it)
	load_directory(*it, runno, files);
    }
    if ( fs::is_regular_file(entry, ec) )   {
      Online::histos::hist_info info(entry);
      if ( info.runno > 100 )  {
	if ( (runno < 0) || (runno == info.runno) )   {
	  files[info.runno].emplace_back(entry.string());
	}
      }
    }
  }
}

/// Select histogram files according to the input path and the requested run-number
std::size_t Online::histos::file_selector::select(const std::string& input, int32_t runno)   {
  std::error_code ec;
  fs::path directory(input);
  if ( fs::exists(directory, ec) )    {
    if ( fs::is_directory(directory, ec) )    {
      std::stringstream state;
      std::size_t num_files = 0;
      load_directory(directory, runno, this->histogram_files);
      for( const auto& f : this->histogram_files ) num_files += f.second.size();
      state << "Loaded directory: " << directory.string() 
	    << " found " << this->histogram_files.size()
	    << " runs to merge with " << num_files << " files." << std::endl;
      mon.updateLog(LIB_RTL_INFO, state.str());
      return this->histogram_files.size();
    }
    throw std::runtime_error("File path is not a directory: "+directory.string()+" ["+ec.message()+"]");
  }
  throw std::runtime_error("Non existing file path: "+directory.string()+" ["+ec.message()+"]");
}

/// Add up a defined chunk of histogram files
std::pair<std::size_t, std::string>
Online::histos::file_selector::process_chunk(int32_t runno,
					     std::vector<std::string>&& files)   {
  std::stringstream state;
  std::size_t file_count { 0 };
  std::size_t total_file_count { 0 };
  run_adder adder(mon, out_file_gen, runno);
  run_adder::updates_t run_statistics;
  for ( const auto& file : files )   { 
    auto updates = adder.load(file);
    ++file_count;
    ++total_file_count;
    run_statistics += updates;
    state.str("");
    state << "Run: "         << std::setw(7) << runno
	  << " added No."    << std::setw(4) << file_count << " : " << file
	  << " histograms: " << (updates.loaded + updates.added) << std::endl;
    this->mon.updateLog(LIB_RTL_INFO, state.str());
  }
  auto ret = adder.close_output();
  state.str("");
  state << "Run: "           << std::setw(7) << runno
	<< " statistics: "
	<< " No.files: "     << std::setw(4) << run_statistics.contributions
	<< " histograms: "   << (run_statistics.loaded + run_statistics.added) << std::endl;
  this->mon.updateLog(LIB_RTL_INFO, state.str());
  /// Clean up input files
  if ( this->cleanup_inputs )   {
    fs::path directory;
    for ( const auto& file : files )  {
      directory = file;
      this->unlink_file(INPUT_FILE, file);
    }
    if ( !directory.empty() && !(0 == directory.string().find("/tmp")) )   {
      std::error_code ec;
      for( int i=0; i<2; ++i )  {
	directory = directory.parent_path();
	if ( fs::exists(directory, ec) && fs::is_empty(directory, ec) )   {
	  fs::remove(directory, ec);
	}
      }
    }
  }
  return { total_file_count, ret.second };
}

/// Add up a defined chunk of histogram files
std::size_t
Online::histos::file_selector::submit_chunk(int32_t runno,
					    std::vector<std::string>&& files)   {
  if ( files.empty() )    {
    return 0;
  }
  else if ( !multi_process )    {
    return process_chunk(runno, std::move(files)).first;
  }
  else  {
    /// We simply generate output files using the process PID in the temporary directory
    auto replacements = out_file_gen.file_replacements(runno);
    std::string pidp = "${PID}";
    mon.lock();
    pid_t pid = ::fork();
    if ( pid == 0 )   {             // Child
      auto len = files.size();
      std::string spid = std::to_string(long(::lib_rtl_pid()));
      ::lib_rtl_sleep(100);
      mon.unlock();
      replacements.emplace_back(std::make_pair(pidp, spid));
      out_file_gen.path_pattern = use_temporary
	? out_file_gen.temporary_file_name(replacements)
	: out_file_gen.output_file_name(replacements);
      auto result = process_chunk(runno, std::move(files));
      if ( result.first == len )   {
	::_exit(0);
      }
      ::_exit(EINVAL);
    }
    else if ( pid > 0 )   {          // Parent
      mon.unlock();
      std::string spid = std::to_string(long(pid));
      replacements.emplace_back(make_pair("${PID}", spid));
      std::string file_pattern = use_temporary
	? out_file_gen.temporary_file_name(replacements)
	: out_file_gen.output_file_name(replacements);
      std::unique_ptr<child_t> child = std::make_unique<child_t>();
      child->pid       = pid;
      child->run       = runno;
      child->state     = "running";
      child->temporary = true;
      child->selector  = this;
      child->files     = std::move(files);
      child->pattern   = file_pattern;
      mon.updateLogMsg(LIB_RTL_ALWAYS,
		       "PID: %d: Child with PID %d starting. Output: %s\n",
		       lib_rtl_pid(), pid, child->pattern.c_str());
      all_children->insert(std::move(child));
    }
    else if (pid < 0)    {        // failed to fork
      char text[512];
      int  err = errno;
      mon.unlock();
      ::snprintf(text,sizeof(text),
		 "[ERROR] Failed to fork child: %s [%d].\n",
		 ::strerror(err), err);
      ::write(STDOUT_FILENO,text,strlen(text));
      ::_exit(EXIT_FAILURE);
      return 0;
    }
  }
  return files.size();
}

std::pair<std::size_t, std::size_t>
Online::histos::file_selector::submit_runs(std::map<int, run_chunk_t>&& files)   {
  std::size_t total_file_count = 0;
  std::size_t total_chunk_count = 0;
  std::size_t the_chunk_size = this->chunk_size;
  std::stringstream state;
  /// Now process the accumulated files:
  for ( const auto& run : files )   {
    run_chunk_t chunk_content;
    std::size_t chunk_count = 0;
    state.str("");
    mon.updateRun(run.first);
    ///
    /// Require at least some parallism. Otherwise small runs take forever.
    if ( run.second.size() < 8*8 )  {
      the_chunk_size = 8;
    }
    else if ( run.second.size() > 20*this->chunk_size )  {
      the_chunk_size = run.second.size()/20 + 1;
    }
    else  {
      the_chunk_size = run.second.size()/16 + 1;
    }
    for ( const auto& file : run.second )   { 
      chunk_content.emplace_back(file);
      if ( the_chunk_size > 0 && chunk_content.size() >= the_chunk_size )   {
	std::lock_guard<std::mutex> lock(root_protection_lock);
	total_file_count += submit_chunk(run.first, std::move(chunk_content));
	chunk_content.clear();
	++total_chunk_count;
	++chunk_count;
      }
    }
    if ( !chunk_content.empty() )   {
      total_file_count += submit_chunk(run.first, std::move(chunk_content));
      ++total_chunk_count;
      ++chunk_count;
    }
    state << "===> Processing run number: " << run.first
	  << " Number of files: " << run.second.size()
	  << " Number of chunks: " << chunk_count
	  << std::endl;
    mon.updateLog(LIB_RTL_INFO, state.str());
  }
  return std::make_pair(total_chunk_count, total_file_count);
}

/// Merge selected histogram files
std::size_t Online::histos::file_selector::merge(std::map<int, run_chunk_t>&& files)   {
  auto ret = this->submit_runs(std::move(files));
  std::size_t num_processes = ret.first;
  std::size_t total_file_count = ret.second;
  std::map<int32_t, std::vector<std::string> > child_products;
  std::size_t proc_count = this->wait_children(num_processes);
  if ( proc_count != num_processes )   {
    this->mon.updateLogMsg(LIB_RTL_ERROR,"Histo file processing error: # processes: %ld #processed: %ld\n",
			   proc_count, num_processes);
    return 0;
  }
  /// Children are filled after all children have processed
  for(const auto& child : this->children )  {
    const auto& c = child.second;
    child_products[c->run].emplace_back(c->pattern);
  }

#if 1
  for( auto& r : child_products )   {
    std::size_t len = r.second.size();
    std::vector<std::string> entries = r.second;
    std::lock_guard<std::mutex> guard(root_protection_lock);
    auto res = this->process_chunk(r.first, std::move(r.second));
    for( const auto& file : entries )   {
      this->mon.updateLogMsg(LIB_RTL_ALWAYS,
			     "Merge output: %s\n", file.c_str());
    }
    if ( res.first == len )    {
      this->mon.updateLogMsg(LIB_RTL_ALWAYS,
			     "Summary: Successfully merged %ld inputs to %s\n",
			     len, res.second.c_str());
      continue;
    }
    this->mon.updateLogMsg(LIB_RTL_ERROR,
			   "Summary merge error: %ld inputs, "
			   "but merged only %ld files %ld\n", len, res.first);
  }
#else
  /// Lets sum up all the fragment created by the children
  file_selector merge_sel(mon, out_file_gen);
  merge_sel.chunk_size    = this->chunk_size;
  merge_sel.use_temporary = merge_sel.chunk_size > this->children.size();
  merge_sel.multi_process = 1;
  merge_sel.merge(std::move(child_products));
#endif

  /// Clean up intermediate summaries
  if ( this->cleanup_temporary )   {
    for(const auto& child : this->children )  {
      unlink_file(TEMPORARY_FILE, child.second->pattern);
    }
  }
  this->children.clear();
  return total_file_count;
}

/// If multi-process: wait for the children to finish
std::size_t
Online::histos::file_selector::wait_children(std::size_t expected)   {
  if ( multi_process )    {
    std::size_t count = 0;
    do { {
	std::lock_guard<std::mutex> guard(all_children->pool_lock);
	count = children.size();
      }
      if ( count < expected )   {
	::lib_rtl_sleep(100);
      }
    } while ( count < expected );
  }
  return expected;
}
