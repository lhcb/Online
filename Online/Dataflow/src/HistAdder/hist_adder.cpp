//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
// Created on: May 28, 2021
//==========================================================================

/// Framework include files
#include <hist_adder/hist_adder.h>
#include <hist_adder/run_adder.h>
#include <hist_adder/monitor.h>
#include <RTL/rtl.h>

/// C/C++ include files

namespace fs = std::filesystem;

/// Initializing constructor
Online::histos::hist_adder::hist_adder(monitor& m, file_generator& output)
  : mon(m), out_file_gen(output)
{
}

/// Merge selected histogram files
std::size_t Online::histos::hist_adder::merge()   {
  std::size_t total_file_count = 0;
  std::stringstream state;

  /// Now process the accumulated files:
  for ( const auto& run : this->histogram_files )   {
    state.str("");
    state << "===> Processing run number: " << run.first;
    mon.updateLog(LIB_RTL_INFO, state.str());
    mon.updateRun(run.first);
    std::size_t file_count = 0;
    run_adder adder(mon, out_file_gen, run.first);
    run_adder::updates_t run_statistics;
    for ( const auto& file : run.second )   { 
      auto updates = adder.load(file);
      ++file_count;
      ++total_file_count;
      run_statistics += updates;
      state.str("");
      state << "Added file: "    << file
	    << " run: "          << run.first
	    << " files in run: " << file_count
	    << " histograms: "   << (updates.loaded + updates.added);
      mon.updateLog(LIB_RTL_INFO, state.str());
    }
    adder.close_output();
    state.str("");
    state << "Run statistics: "
	  << " run: "          << run.first
	  << " files in run: " << run_statistics.contributions
	  << " histograms: "   << (run_statistics.loaded + run_statistics.added);
    mon.updateLog(LIB_RTL_INFO, state.str());
  }
  return total_file_count;
}
