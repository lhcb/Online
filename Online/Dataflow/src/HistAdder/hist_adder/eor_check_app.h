//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
// Created on: May 28, 2021
//==========================================================================
#ifndef DATAFLOW_HISTADDER_EOR_CHECK_APP_H
#define DATAFLOW_HISTADDER_EOR_CHECK_APP_H

/// Framework include files
#include <hist_adder/run_db.h>
#include <hist_adder/monitor.h>
#include <hist_adder/eor_check.h>
#include <CPP/Interactor.h>
#include <RTL/rtl.h>

/// C/C++ include files
#include <memory>
#include <string>
#include <vector>


/// Online Namespace
namespace Online   {

  /// Namespace for the histogram adder interface
  namespace histos   {
 
    /// Histogram adder application
    /**
     *
     *  \author  M.Frank
     *  \version 1.0
     *  \date    02.02.2021
     */
    class eor_check_app : public CPP::Interactor  {
    public:
      std::string                      partition;
      std::string                      input_directory;
      std::unique_ptr<rpc::run_db>     rundb;

      int last_known_run               {  0 };
      int disk_scan_interval           { 60 };
      int rundb_scan_interval          { 60 };
      int rundb_days_to_scan           {  5 };

      struct run_entry  {
	std::time_t start;
	std::time_t end;
	int state;
      };
      
      std::map<int32_t, run_entry>   runs_todo;
      std::map<int32_t, run_entry>   runs_on_disk;

      monitor                          mon;
      eor_check                        checker;

      enum interrupt_types {
	SCAN_DISK         = 1,
	SCAN_RUNDB        = 2,
      };

    public:
      /// Default constructor
      eor_check_app();
      /// Default destructor
      virtual ~eor_check_app() = default;
      /// Initialize database access
      void init();

      void check_disk();
      void check_rundb();
      void missing_run(int32_t run, const run_entry& entry);
      /// Interactor interrupt handler callback
      virtual void handle(const CPP::Event& event)  override;
    };
  }     // End namespace storage
}       // End namespace Online
#endif  // DATAFLOW_HISTADDER_EOR_CHECK_APP_H
