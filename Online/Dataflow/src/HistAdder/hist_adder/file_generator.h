//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
// Created on: May 28, 2021
//==========================================================================
#ifndef ONLINE_DATAFLOW_FILEGENERATOR_H
#define ONLINE_DATAFLOW_FILEGENERATOR_H

/// C/C++ include files
#include <string>
#include <vector>
#include <memory>

// Forward declarations
class TFile;

/// Online namespace declaration
namespace Online   {

  /// Histograms sub-namespace
  namespace histos  {
    
    /// Forward declarations
    class monitor;
    
    /// Open output file according to setup parameters
    /**
     *  \author  M.Frank
     *  \version 1.0
     */
    class file_generator   {
    public:
      typedef std::vector<std::pair<std::string, std::string> > replacements_t;

    public:
      /// Monitor interface
      monitor& mon;
      /// Path prefix
      std::string  path_pattern {};
      /// Temporary directory for intermediate merge results
      std::string  temporary_directory {};
      /// File name pattern for temporary files
      std::string  temporary_pattern {};
      /// Partition name (if used for pattern filling)
      std::string  partition    {};
      std::string  task_name    {};

      std::string temporary_file_name(const replacements_t& replacements)   const;
      std::string output_file_name(const replacements_t& replacements)   const;
      std::string output_file_name(const std::string& pattern,
				   const replacements_t& replacements)   const;

      replacements_t file_replacements(int runno)   const;
    public:
      /// Initializing constructor
      file_generator(monitor& mon);
      /// Copy constructor
      file_generator(const file_generator& copy) = default;
      /// Move constructor
      file_generator(file_generator&& copy) = default;
      /// Default destructor
      file_generator() = delete;
      /// Default destructor
      virtual ~file_generator() = default;
      /// Copy assignment
      file_generator& operator= (const file_generator& copy) = delete;
      /// Move assignment
      file_generator& operator= (file_generator&& copy) = delete;      
      /// Open new output file according to runb information
      template <typename implementation> std::unique_ptr<TFile> open(int runno)  const;
    };
  }    // End namespace histos
}      // End namespace Online

#endif // ONLINE_DATAFLOW_HISTFILEGENERATOR_H
