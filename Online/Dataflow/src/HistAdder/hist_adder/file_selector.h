//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
// Created on: May 28, 2021
//==========================================================================
#ifndef ONLINE_DATAFLOW_HISTFILESELECTOR_H
#define ONLINE_DATAFLOW_HISTFILESELECTOR_H

/// Framework include files
#include <RTL/Logger.h>

/// C/C++ include files
#include <map>
#include <vector>
#include <string>
#include <mutex>
#include <filesystem>

// Forward declarations

/// Online namespace declaration
namespace Online   {

  /// Histograms sub-namespace
  namespace histos  {

    /// Forward declarations
    class monitor;
    class child_pool;
    class file_selector;
    class file_generator;
    
    /// Generic adder interface
    /**
     *  \author  M.Frank
     *  \version 1.0
     */
    class file_selector  {
    public:
      class child_t   {
      public:
	int32_t  pid;
	uint32_t run;
	uint8_t  temporary;
	std::string state;
	std::string pattern;
	std::vector<std::string> files;
	file_selector* selector { nullptr };

      public:
	/// Copy constructor
	child_t(const child_t& copy) = default;
	/// Move constructor
	child_t(child_t&& copy) = default;
	/// Default destructor
	child_t() = default;
	/// Default destructor
	virtual ~child_t() = default;
	/// Copy assignment
	child_t& operator= (const child_t& copy) = default;
	/// Move assignment
	child_t& operator= (child_t&& copy) = default;
      };

      /// Monitor interface
      monitor& mon;
      /// Output file generator
      file_generator& out_file_gen;
      /// List of histogram files to be merged.
      typedef std::vector<std::string> run_chunk_t;
      std::map<int32_t, run_chunk_t>   histogram_files;
      std::shared_ptr<child_pool>      all_children;
      std::map<int32_t, std::unique_ptr<child_t> > children;
      std::size_t chunk_size  { 0 };
      int   multi_process     { 0 };
      int   cleanup_inputs    { 0 };
      int   cleanup_temporary { 1 };
      int   use_temporary     { 1 };
      enum { INPUT_FILE, TEMPORARY_FILE };
      
    public:
      /// Initializing constructor
      file_selector(monitor& monitor, file_generator& output);
      /// Copy constructor
      file_selector(const file_selector& copy) = default;
      /// Move constructor
      file_selector(file_selector&& copy) = delete;
      /// Default destructor
      virtual ~file_selector();
      /// Copy assignment
      file_selector& operator= (const file_selector& copy) = delete;
      /// Move assignment
      file_selector& operator= (file_selector&& copy) = delete;

      bool unlink_file(int flag, const std::string& file_name)  const;
      
      /// Select histogram files according to the input path and the requested run-number
      std::size_t select(const std::string& input, int32_t runno);
      /// Merge all selected histogram files according to run number
      std::size_t merge(std::map<int32_t, run_chunk_t>&& files);
      /// Add up a defined chunk of histogram files
      std::pair<std::size_t, std::string> process_chunk(int32_t run, std::vector<std::string>&& files);
      /// Add up a defined chunk of histogram files
      std::size_t submit_chunk(int32_t run, std::vector<std::string>&& files);
      /// If multi-process: wait for the children to finish
      std::size_t wait_children(std::size_t expected);
      /// Submit multiple runs
      std::pair<std::size_t, std::size_t>
      submit_runs(std::map<int, run_chunk_t>&& files);
      /// Add all matching histograms to the list of files to be merged
      void load_directory(const std::filesystem::path& entry,
			  int32_t runno,
			  std::map<int32_t, std::vector<std::string> >& files);
      /// Delete empty histogram directories
      void delete_directory(const std::filesystem::path& entry, bool current);
    };
  }    // End namespace histos
}      // End namespace Online
#endif // ONLINE_DATAFLOW_HISTFILESELECTOR_H
