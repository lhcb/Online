//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_STORAGE_FDB_SQLDB_H
#define ONLINE_STORAGE_FDB_SQLDB_H

// Framework include files
#include <sqldb/sqldb.h>

// C/C++ include files
#include <system_error>
#include <cstdint>

/// Online Namespace
namespace Online   {

  /// Namespace for the histogram adder interface
  namespace histos   {
    
    /// Interface class to database handler
    /**
     *  To implement a new database technology in principle only
     *  this class needs to be re-implemented e.g. using MYSQL.
     *
     *  \author  M.Frank
     *  \version 1.0
     *  \date    02.02.2021
     */
    class processing_db   {
    public:
      bool             _inited { false };
      int              _debug  { 0 };
      std::string      _path;
      sqldb::database  database;
      sqldb::statement insert_record;
      sqldb::statement query_record_run;
      sqldb::statement query_record_range;
      sqldb::statement query_record_state;
      sqldb::statement delete_record;
      sqldb::statement state_update_record;

      static constexpr int STATE_ENDED   = 5;
      static constexpr int STATE_MERGING = 6;
      static constexpr int STATE_MERGED  = 7;
      static constexpr int STATE_READ    = 8;
      static constexpr int STATE_FAILED  = 9;

      struct run_t   {
	int32_t   run_number;
	int32_t   state { 0 };
	time_t    start { 0 };
	time_t    enter { 0 };
	time_t    time  { 0 };
      };
      typedef std::vector<run_t> runs_t;
      
    public:

      /// Initializing constructor
      processing_db(const std::string& p, int dbg);

      /// Default destructor
      virtual ~processing_db();

      /// Initialize object
      std::error_code init(const std::string& db_name);

      /// Finalize object
      void fini();

      /// Add a new object to the database
      std::error_code add(run_t run);
	
      /// Remove an object from the database
      std::error_code del(int32_t runno);

      /// Check if run record was entered previously
      std::pair<std::error_code, bool> exists(int32_t runno);

      /// Set new state to run record
      std::error_code set_state(int32_t runno, int state);

      /// Check the existence of a given object in the database
      std::error_code query_run(int32_t runno, run_t& file);

      /// Query runs by run number range
      std::error_code query_run_range(int32_t first_runno,
				      int32_t last_runno,
				      std::vector<run_t>& runs);

      /// Query runs by state
      std::error_code query_runs_by_state(int32_t state, std::vector<run_t>& runs);
    };
  }     // End namespace storage
}       // End namespace Online
#endif  // ONLINE_STORAGE_FDB_SQLDB_H
