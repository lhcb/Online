//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
// Created on: May 28, 2021
//==========================================================================
#ifndef ONLINE_DATAFLOW_RUN_ADDER_H
#define ONLINE_DATAFLOW_RUN_ADDER_H

/// C/C++ include files
#include <map>
#include <vector>
#include <filesystem>

// Forward declarations
class TH1;
class TDirectory;

/// Online namespace declaration
namespace Online   {

  /// Histograms sub-namespace
  namespace histos  {

    /// Forward declarations
    class monitor;
    class hist_info;
    class file_generator;

    class run_adder  {
    public:
      class updates_t   {
      public:
	std::size_t path_fixes     { 0 };
	std::size_t contributions  { 0 };
	std::size_t added          { 0 };
	std::size_t loaded         { 0 };
      public:
	updates_t& operator +=(const updates_t& a)   {
	  this->contributions  += a.contributions;
	  this->path_fixes  += a.path_fixes;
	  this->loaded  += a.loaded;
	  this->added  += a.added;
	  return *this;
	}
      };
      std::map<std::string, std::vector<TH1*> > histograms;
      class updates_t             run_update_info;
      class monitor&              mon;
      const class file_generator& gen;

    protected:
      int32_t                  m_run_number {0};

    public:
      updates_t collect(const std::string& dir_name, TDirectory& dir);

    public:
      run_adder(class monitor& mon, const class file_generator& gen, int32_t run);
      virtual ~run_adder() = default;
      updates_t load(const std::filesystem::path& histos);
      std::pair<std::size_t, std::string> close_output();
    };
  }
}
#endif // ONLINE_DATAFLOW_HISTRUN_ADDER_H

