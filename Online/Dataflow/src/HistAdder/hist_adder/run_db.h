//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
// Created on: May 28, 2021
//==========================================================================
#ifndef ONLINE_RPC_RUNDB_H
#define ONLINE_RPC_RUNDB_H

/// Framework include files
#include <RPC/RpcClientHandle.h>
#include <RPC/XMLRPC.h>

/// C/C++ include files
#include <string>
#include <vector>
#include <map>

/// RPC namespace declaration
namespace rpc {

  /// Query XMLRPC interface of the run database for information
  /**
   *  \author  M.Frank
   *  \version 1.0
   */
  class run_db  {
  public:
    typedef long                       status_t;
    typedef std::vector<int>           int_options_t;
    typedef std::vector<std::string>   string_options_t;
    typedef std::vector<xmlrpc::Tuple> runs_t;
    /// Reference to the RPC client handle for the interaction
    XmlRpcClientH handler;

  public:
    /// Initializing constructor
    run_db(XmlRpcClientH&& client);
    /// Initializing constructor
    run_db(const std::string& node, int port);
    /// Initializing constructor
    run_db(const std::string& node, const std::string& port);
    /// Default destructor
    virtual ~run_db() = default;

    /// Access runs from the run database according to output destination, state, partition and start-timee
    std::pair<status_t, runs_t>
      get_runs(const string_options_t& fields,
	       const string_options_t& destination,
	       const int_options_t& states,
	       const std::string& partition,
	       const std::string& start_time,
	       const std::string& end_time = "",
	       const std::string& started_before = "");
    /// Access runs from the run database according to output destination, state, partition and start-timee
    std::pair<status_t, std::vector<long> >
      get_runs(const string_options_t& destination,
	       const int_options_t& states,
	       const std::string& partition,
	       const std::string& start_time,
	       const std::string& end_time = "",
	       const std::string& started_before = "");
    /// Access runs from the run database according to output destination, state, partition and first known run
    std::pair<status_t, std::vector<std::pair<long, struct tm> > >
      get_runs_start(const string_options_t& destination,
		     const int_options_t& states,
		     const std::string& partition,
		     const std::string& start_time,
		     const std::string& end_time = "",
		     const std::string& started_before = "");
    /// Access runs from the run database according to output destination, state, partition and first known run
    xmlrpc::Tuple
      get_runs_start(const string_options_t& fields,
		     const string_options_t& destination,
		     const int_options_t& states,
		     const std::string& partition,
		     const std::string& start_time,
		     const std::string& end_time = "",
		     const std::string& started_before = "");
  };
}      // End namespace RPC
#endif // ONLINE_RPC_RUNDB_H
