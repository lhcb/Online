//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
// Created on: April 28, 2023
//==========================================================================

/// Framework include files
#include <hist_adder/run_db.h>
#include <hist_adder/processing_db.h>
#include <hist_adder/file_generator.h>
#include <hist_adder/file_selector.h>
#include <hist_adder/monitor.h>
#include <CPP/Interactor.h>

/// C/C++ include files
#include <condition_variable>
#include <filesystem>
#include <memory>
#include <string>
#include <vector>
#include <thread>
#include <mutex>

/// Online Namespace
namespace Online   {

  /// Namespace for the histogram adder interface
  namespace histos   {

    // Forward declarations
    class processing_db;
    
    /// Histogram adder application
    /**
     *
     *  \author  M.Frank
     *  \version 1.0
     *  \date    02.02.2021
     */
    class hist_adder_app : public CPP::Interactor  {
      struct worker_t : public std::thread {
	bool busy = false;
	std::time_t started = 0;
	using std::thread::thread;
      };

    public:
      std::string                      input_directory;
      std::mutex                       hist_data_lock;
      std::condition_variable          hist_data_available;
      std::unique_ptr<rpc::run_db>     rundb;
      std::unique_ptr<processing_db>   procdb;

      std::string procdb_file          { "ProcessingTestDB.dat" };
      std::string command_file         { "commands.txt" };
      int last_known_run               {   0 };
      int disk_scan_interval           {  60 };
      int rundb_scan_interval          {  60 };
      int procdb_scan_interval         {  60 };
      int procdb_debug                 {   1 };
      int cleanup_inputs               {   0 };
      int rundb_days_to_scan_first     { 200 };
      int rundb_days_to_scan           {   5 };
      int multi_process                {   1 };
      std::size_t num_parallel_adders  {   1 };
      
      std::size_t chunk_size           { 100 };
      std::mutex                       worker_lock;
      std::vector<worker_t*>           add_workers;
      std::set<int32_t>                runs_todo;
      std::set<int32_t>                runs_on_disk;

      monitor                          mon;
      file_generator                   outgen;

      enum interrupt_types {
	SCAN_DISK         = 1,
	SCAN_RUNDB        = 2,
	SCAN_PROCESSINGDB = 3,
      };

    public:
      /// Default constructor
      hist_adder_app();
      /// Default destructor
      virtual ~hist_adder_app() = default;
      /// Initialize database access
      void init();

      void add_histograms(worker_t* worker, std::time_t start);
      bool add_partition_run_histograms(int32_t runno, std::time_t start_time);
      void check_disk();
      void check_rundb();
      void check_processingdb();
      std::size_t num_idle_workers() const;
      /// Interactor interrupt handler callback
      virtual void handle(const CPP::Event& event)  override;
    };
  }     // End namespace storage
}       // End namespace Online

//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
// Created on: April 28, 2023
//==========================================================================

/// Framework include files
#include <CPP/TimeSensor.h>
#include <CPP/IocSensor.h>
#include <CPP/Event.h>
#include <RTL/strdef.h>
#include <RTL/rtl.h>
#include <RTL/Sys.h>

#include <iostream>

using namespace Online::histos;

/// Default constructor
hist_adder_app::hist_adder_app()
  :  outgen(mon)
{
}

/// Initialize database access
void hist_adder_app::init()  {
  rundb  = std::make_unique<rpc::run_db>("rundb11.lbdaq.cern.ch", 8080);
  procdb = std::make_unique<processing_db>(procdb_file, procdb_debug);
  for( std::size_t ithread=0; ithread< num_parallel_adders; ++ithread )  {
    add_workers.emplace_back(new worker_t([this, ithread]() { this->add_histograms(this->add_workers[ithread],0); }));
  }
}


std::size_t hist_adder_app::num_idle_workers() const   {
  std::size_t count = 0;
  for( const auto* w : add_workers )
    if ( !w->busy ) ++count;
  return count;
}

bool hist_adder_app::add_partition_run_histograms(int32_t runno, std::time_t /* run_start_time */)  {
  mon.updateLogMsg(LIB_RTL_ALWAYS,"Starting to add histograms of %s for run number %d\n",
		   outgen.task_name.c_str(), runno);
  std::string env = std::getenv("FARMCONFIGROOT") ? std::getenv("FARMCONFIGROOT") : ""; 
  std::string run10000 = std::to_string(long(runno/10000)*10000);
  std::string run1000  = std::to_string(long(runno/1000)*1000);
  std::stringstream command, input;
  input << this->input_directory << "/" << run10000 << "/" << run1000 << "/" << runno;

  command << env << "/HLT2_merge_hist.sh "
	  << " -input="      << input.str()
	  << " -output="     << this->outgen.path_pattern
	  << " -task="       << this->outgen.task_name
	  << " -partition="  << this->outgen.partition
	  << " -temporary="  << this->outgen.temporary_directory
	  << " -chunk_size=" << this->chunk_size
	  << " -print="      << "INFO";
  command << std::ends;
  mon.updateLog(LIB_RTL_ALWAYS,command.str());
  file_generator generator(outgen);
  file_selector  selector(mon, generator);
  selector.cleanup_inputs = this->cleanup_inputs;
  /// char timestr[128];
  /// struct tm tm;
  /// ::localtime_r(&run_start_time, &tm);
  /// ::strftime(timestr, sizeof(timestr), "%Y%m%dT%H%M%S-EOR", &tm);
  generator.path_pattern = outgen.path_pattern +
    ///  "/" + outgen.partition +
    ///  "/" + outgen.task_name +
    "/" + run10000 + "/" + run1000 +
    "/" + outgen.task_name + "-run" + std::to_string(long(runno)) +
    /// "_" + timestr +
    ".root";
  generator.temporary_pattern = "${PARTITION}_HLT${PID}_${TASK}_0-${RUN}-${TIME}-EOR.root";
#if 0
  char timestr[128];
  struct tm tm;
  ::localtime_r(&run_start_time, &tm);
  ::strftime(timestr, sizeof(timestr), "%Y%m%dT%H%M%S-EOR", &tm);
  generator.path_pattern = outgen.path_pattern +
    "/" + run10000 + "/" + run1000 + "/" +
    "LHCb2_HLTX"+std::to_string(clk&9999)+"_"+outgen.task_name + "_0-" + std::to_string(long(runno)) +
    "_" + timestr +
    ".root";
#endif
  try  {
    selector.chunk_size = this->chunk_size;
    selector.select(input.str(), runno);
    selector.multi_process = this->multi_process;
    selector.merge(std::move(selector.histogram_files));
    return true;
  }
  catch(const std::exception& e)   {
    mon.updateLog(LIB_RTL_ALWAYS,"+++ FAILED to merge run %d: %s", runno, e.what());
  }
  return false;
}

/// Worker callback scanning the various queues for work
void hist_adder_app::add_histograms(worker_t* worker, std::time_t start)  {
  auto okToStartAdding = [&] { return !this->runs_todo.empty();  };  
  this->mon.updateLogMsg(LIB_RTL_ALWAYS,"Histogram adder worker started.\n");
  while ( 1 )  {
    std::time_t run_start { 0 };
    int32_t run_number    { 0 };

    worker->busy = false;
    {
      std::unique_lock<std::mutex> lock { hist_data_lock };
      if ( this->hist_data_available.wait_for( lock, std::chrono::seconds(1), okToStartAdding ) )  {
	if ( !this->runs_todo.empty() && !runs_on_disk.empty() )  {
	  int32_t runno = *(this->runs_todo.begin());
	  auto it_on_disk = this->runs_on_disk.find(runno);
	  if ( !command_file.empty() )   {
	    std::time_t now = ::time(nullptr);
	    if ( now - start > 900 )   {
	      start = now;
	      char buff[16*1024];
	      ::memset(buff, 0, sizeof(buff));
	      if ( RTL::SysFile(this->command_file).read(buff,sizeof(buff)) > 0 )   {
		if ( RTL::str_upper(buff).find("STOP") != std::string::npos )  {
		  this->mon.updateLogMsg(LIB_RTL_ALWAYS,
					 "STOP requested. Worker exiting");
		  return;
		}
	      }
	    }
	  }
	  if ( runno != 0 && it_on_disk != this->runs_on_disk.end() )   {
	    processing_db::run_t run;
	    worker->busy = true;
	    worker->started = ::time(0);
	    this->procdb->set_state(runno, this->procdb->STATE_MERGING);
	    this->runs_todo.erase(this->runs_todo.begin());
	    this->runs_on_disk.erase(it_on_disk);
	    this->procdb->query_run(runno, run);
	    run_start  = run.start;
	    run_number = runno;
	  }
	}
      }
      ::lib_rtl_sleep(1000);
    }
    if ( run_number != 0 )   {
      bool result = this->add_partition_run_histograms(run_number, run_start);  {
	std::unique_lock<std::mutex> lock { this->hist_data_lock };
	if ( result )  {
	  this->procdb->set_state(run_number, this->procdb->STATE_MERGED);
	}
	else  {
	  this->procdb->set_state(run_number, this->procdb->STATE_FAILED);
	}
      }
      /// We seem to have a memory leak. Check the memory usage and exit if above threshold
      RTL::StatusProcess process;
      if ( 1 == RTL::read(process, ::lib_rtl_pid()) )   {
	double mem = double(process.vmSize + process.vmRSS)/(1024.0);
	if ( mem > 2.5*1024 )  {
	  this->mon.updateLogMsg(LIB_RTL_ALWAYS, "Memory usage: %9.2f MB above threshold. Exiting.", mem);
	  for( std::size_t cnt=1; ; ++cnt )  {
	    std::size_t cnt_idle = this->num_idle_workers();
	    std::size_t cnt_work = this->add_workers.size();
	    if ( (cnt%120) == 0 )  {
	      this->mon.updateLogMsg(LIB_RTL_ALWAYS,"Waiting for %ld workers to finish.",
				     cnt_work - cnt_idle);
	    }
	    ::lib_rtl_sleep(1000);
	    ///
	    if( cnt_idle != cnt_work )
	      break;
	  }
	  ::exit(0);
	}
	this->mon.updateLogMsg(LIB_RTL_ALWAYS,"Memory usage: %9.2f MB [below threshold]", mem);
      }
    }
  }
}

void hist_adder_app::check_disk()  {
  Online::histos::file_selector selector(this->mon, this->outgen);
  selector.select(this->input_directory, -1);
  std::lock_guard<std::mutex> lock { hist_data_lock };
  runs_on_disk.clear();
  for( const auto& run : selector.histogram_files )   {
    if ( !run.second.empty() )   {
      runs_on_disk.insert(run.first);
    }
  }
  if ( this->cleanup_inputs )   {
    selector.delete_directory(this->input_directory, false);
  }
}

void hist_adder_app::check_processingdb()   {
  std::vector<processing_db::run_t> runs;
  if ( 0 == procdb->query_runs_by_state(procdb->STATE_ENDED, runs).value() )  {
    mon.updateLogMsg(LIB_RTL_ALWAYS,"Checking processing db.\n");
    if ( !runs.empty() )  {
      std::lock_guard<std::mutex> lock { hist_data_lock };
      mon.updateLogMsg(LIB_RTL_ALWAYS,"Adding %ld runs to worker todo-list.\n", runs.size());
      for ( const auto& r : runs )  {
	if ( runs_on_disk.find(r.run_number) == runs_on_disk.end() )   {
	  continue;
	}
	runs_todo.insert(r.run_number);
	if ( r.run_number > last_known_run )  {
	  last_known_run = r.run_number;
	}
      }
      hist_data_available.notify_all();
    }
  }
}

void hist_adder_app::check_rundb()  {
  /*  RunDB states:
   *
   *  1 : ACTIVE,   2 : ENDED,   3 : MIGRATING,
   *  4 : MIGRATED, 5 : CREATED, 6 : TRANSFERRED,
   *  7 : DEFERRED, 8 : ALIGNED
   * 
   *  On the first go take a 200 day snapshot, then only less days
   */
  static int first = 1;
  char start_time[512];
  std::vector<int> states { 2, 3, 4, 6 };
  std::string partition = this->outgen.partition;
  std::vector<std::string> destinations { "OFFLINE", "LOCAL" };
  unsigned long days = first
    ? this->rundb_days_to_scan_first
    : this->rundb_days_to_scan;
  ::time_t   now = ::time(0) - days*24UL*3600UL;
  struct tm *tm  = ::localtime(&now);

  first = false;
  ::strftime(start_time, sizeof(start_time), "%Y-%m-%dT%H:%M:%S", tm);
  auto runs = rundb->get_runs_start(destinations, states, partition, start_time);
  if ( runs.first )  {
    std::size_t count = 0;
    for( auto run : runs.second )  {
      int run_number = run.first;
      if ( runs_on_disk.find(run_number) == runs_on_disk.end() )   {
	continue;
      }
      if ( procdb->exists(run_number).second )  {
	continue;
      }
      processing_db::run_t entry;
      std::string tim = ::asctime(&run.second);
      tim = tim.substr(0, tim.length()-1);
      entry.run_number = run_number;
      entry.state = procdb->STATE_ENDED;
      entry.start = ::mktime(&run.second);
      entry.enter = entry.start;
      entry.time  = entry.start;
      procdb->add(entry);
      mon.updateLogMsg(LIB_RTL_ALWAYS,"Added run %8d State: ENDED Date: %s\n", run_number, tim.c_str());
      ++count;
    }
    if ( count > 0 )
      mon.updateLogMsg(LIB_RTL_ALWAYS,"Added %ld entries to processing database.\n", count);
    else
      mon.updateLogMsg(LIB_RTL_ALWAYS,"No new runs for histogram processing detected.\n");
    return;
  }
  mon.updateLogMsg(LIB_RTL_ERROR,"Failed to access the run database.\n");
}

/// Interactor interrupt handler callback
void hist_adder_app::handle(const CPP::Event& ev)  {
  try  {
    switch(ev.eventtype) {
    case IocEvent: {
      switch(ev.type) {
      case SCAN_DISK:
	if ( this->num_idle_workers() > 0 && this->runs_todo.size() < 2 ) this->check_disk();
	TimeSensor::instance().add(this, this->disk_scan_interval, SCAN_DISK);
	break;
      case SCAN_RUNDB:
	if ( this->num_idle_workers() > 0 && this->runs_todo.size() < 2 ) this->check_rundb();
	TimeSensor::instance().add(this, this->rundb_scan_interval, SCAN_RUNDB);
	break;
      case SCAN_PROCESSINGDB:
	if ( this->num_idle_workers() > 0 && this->runs_todo.size() < 2 ) this->check_processingdb();
	TimeSensor::instance().add(this, this->procdb_scan_interval, SCAN_PROCESSINGDB);
	break;
      default:
	break;
      };
      break;
    }
    
    case TimeEvent:  {
      long cmd = long(ev.timer_data);
      switch(cmd) {
      case SCAN_DISK:
      case SCAN_RUNDB:
      case SCAN_PROCESSINGDB:
	IocSensor::instance().send(this, cmd, this);
	break;
      default:
	break;
      }
      break;
    }

    default:
      break;
    }
  }
  catch(const std::exception& e)   {
    mon.updateLogMsg(LIB_RTL_ALWAYS,"+++ EXCEPTION: Scanning run database: %s.\n", e.what());
  }
  catch(...)   {
    mon.updateLogMsg(LIB_RTL_ALWAYS,"+++ EXCEPTION: Scanning run database: UNKNOWN EXCEPTION.\n");
  }
}

extern "C" int run_hist_app(int argc, char** argv)  {
  hist_adder_app app;
  RTL::CLI cli(argc, argv, [](int ac, char** av){
    std::cout << "run_hist_app  -opt [-opt]                                                                          \n\n"
	      << "  Parameter for histogram merging:                                                                   \n"
	      << "    -input=<input-directory>        Path to histogram data input directory                           \n"
	      << "                                    Will be recursively scanned for ROOT files                       \n"
	      << "    -output=<output-directory>      Path to place summarized files                                   \n"
	      << "    -cleanup_inputs=<value>         Cleanup input files (default: 0, no cleanup. values: 0,1)        \n"
	      << "    -temporary=<temp-directory>     Path to place summarized files                                   \n"
	      << "    -task=<task-name>               Task name for output histograms                                  \n"
	      << "    -chunk_size=<number>            Chunk size for multi process execution                           \n"
	      << "    -partition=<partition-name>     Partition name for output histograms                             \n"
	      << "  Parameter for merge server app                                                                     \n"
	      << "    -disk-scan-interval=<seconds>   Update interval to look for runs on disk <input>.                \n"
	      << "    -rundb-scan-interval=<seconds>  Update interval to look for runs in state ENDED in the run db    \n"
	      << "    -rundb-first-scan-interval=<seconds>  First update interval to look for runs in state ENDED      \n"
	      << "    -procdb-scan-interval=<seconds> Update interval to look for work in the processing db            \n"
	      << "    -days-to-scan=<number>          Number of days to search the run database at startup             \n"
	      << "    -procdb-file=<path>             Path to the processing DB sqlite file                            \n"
	      << "    -print=<print-level>            Printout level VERBOSE, DEBUG,...,FATAL                         \n\n"
	      << " \n";
    RTL::CLI::print_args(ac, av);
    ::exit(EINVAL);
  });

  std::string prt = "INFO";
  cli.getopt("rundb-first-days-to-scan", 9, app.rundb_days_to_scan_first);
  cli.getopt("rundb-days-to-scan",   9, app.rundb_days_to_scan);
  cli.getopt("rundb-scan-interval",  9, app.rundb_scan_interval);
  cli.getopt("procdb-file",          9, app.procdb_file);
  cli.getopt("procdb-scan-interval", 9, app.procdb_scan_interval);
  app.procdb_debug = cli.getopt("procdb-debug", 9) ? 1 : 0;
  cli.getopt("disk-scan-interval",   8, app.disk_scan_interval);
  cli.getopt("cleanup_inputs",       6, app.cleanup_inputs);
  cli.getopt("command-file",         7, app.command_file);

  cli.getopt("input_directory",      5, app.input_directory);
  cli.getopt("output_directory",     6, app.outgen.path_pattern);
  cli.getopt("task",                 4, app.outgen.task_name);
  cli.getopt("partition",            3, app.outgen.partition);
  cli.getopt("temporary_directory",  5, app.outgen.temporary_directory);
  cli.getopt("chunk_size",           5, app.chunk_size);
  cli.getopt("parallel_adders",      4, app.num_parallel_adders);
  cli.getopt("multi_process",        4, app.multi_process);
  cli.getopt("print",                3, prt);

  if ( app.outgen.partition.empty() ||
       app.outgen.task_name.empty() ||
       app.outgen.path_pattern.empty() ||
       app.outgen.temporary_directory.empty() )
    {
      cli.call_help();
    }
  RTL::Logger::LogDevice::setGlobalDevice(Online::histos::logger(), prt);
  Online::histos::logger()->compileFormat("%-38SOURCE %-8LEVEL");
  
  app.init();
  IocSensor::instance().send(&app, hist_adder_app::SCAN_DISK, &app);
  IocSensor::instance().send(&app, hist_adder_app::SCAN_RUNDB, &app);
  IocSensor::instance().send(&app, hist_adder_app::SCAN_PROCESSINGDB, &app);
  app.mon.updateLogMsg(LIB_RTL_ALWAYS,
		       "+++ Starting histogram adder application %s.\n",
		       RTL::processName().c_str());
  IocSensor::instance().run();
  return 0;
}
