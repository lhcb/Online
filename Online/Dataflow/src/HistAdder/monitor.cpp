//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
// Created on: May 28, 2021
//==========================================================================
#include <hist_adder/monitor.h>

/// Framework include files
#include <RTL/Logger.h>
#include <RTL/rtl.h>
#include <dim/dis.h>

/// C/C++ include files
#include <mutex>
#include <cstdarg>
#include <iostream>

namespace {

  struct DimLock  {
    DimLock()   { ::dim_lock();   }
    ~DimLock()  { ::dim_unlock(); }
  };
  constexpr static const char* LOGGER_TAG = "Handler";
}

namespace Online  {
  namespace histos  {
    std::shared_ptr<RTL::Logger::LogDevice>& logger()   {
      static std::shared_ptr<RTL::Logger::LogDevice> logger =
	RTL::Logger::LogDevice::existsGlobalDevice() 
	? RTL::Logger::LogDevice::getGlobalDevice()
	: std::make_shared<RTL::Logger::LogDevice>();
      return logger;
    }
  }
}

/// Default constructor
Online::histos::monitor::monitor()   {
  std::string nam, proc = RTL::processName();
  nam = proc + "/log";
  log_id = ::dis_add_service(nam.c_str(),"C",0,0,monitor::feedLog,(long)this);
  nam = proc + "/currentRun";
  run_id = ::dis_add_service(nam.c_str(),"I",0,0,monitor::feedRun,(long)this);
}

/// Default destructor
Online::histos::monitor::~monitor()   {
  if ( log_id ) {
    ::dis_remove_service(log_id);
    log_id = 0;
  }
  if ( run_id ) {
    ::dis_remove_service(run_id);
    run_id = 0;
  }
}

/// Lock monitor (and DIM!)
void Online::histos::monitor::lock()  {
  ::dim_lock();
}

/// Unlock monitor (and DIM!)
void Online::histos::monitor::unlock()  {
  ::dim_unlock();
}

/// Update logger info
void Online::histos::monitor::updateLog(int severity, const char* format, ...)   {
  if ( severity >= ::lib_rtl_log_level() ) {
    char msg[4096];
    va_list args;
    va_start(args, format);
    /* std::size_t len = */ ::vsnprintf(msg, sizeof(msg), format, args);
    va_end(args);
    logger()->printout(severity, LOGGER_TAG, msg);
    this->update(log_id, msg);
  }
}

/// Update logger info
void Online::histos::monitor::updateLogMsg(int severity, const char* format, ...)   {
  if ( severity >= ::lib_rtl_log_level() ) {
    char msg[4096];
    va_list args;
    va_start(args, format);
    /* std::size_t len = */ ::vsnprintf(msg, sizeof(msg), format, args);
    va_end(args);
    logger()->printout(severity, LOGGER_TAG, msg);
    this->update(log_id, msg);
  }
}

/// Update logger info
void Online::histos::monitor::updateLog(int severity, const std::string& msg)   {
  if ( severity >= ::lib_rtl_log_level() ) {
    logger()->printout(severity, LOGGER_TAG, msg.c_str());
    this->update(log_id, msg.c_str());
  }
}

/// Update dim network logger
void Online::histos::monitor::update(int svc_id, const char* msg)   {
  {
    DimLock guard;
    log_info = msg;
  }
  ::dis_update_service( svc_id );
}

/// Update run info
void Online::histos::monitor::updateRun(int32_t runno)   {
  run = runno;
  ::dis_update_service( run_id );  
}

/// DIS data feed callback
void Online::histos::monitor::feedLog(void* tag, void** buf, int* size, int* )   {
  monitor* mon = *(monitor**)tag;
  if ( buf )   {
    *buf = const_cast<char*>(mon->log_info.c_str());
  }
  if ( size )   {
    *size = mon->log_info.length()+1;
  }
}

/// DIS data feed callback
void Online::histos::monitor::feedRun(void* tag, void** buf, int* size, int* )   {
  monitor* mon = *(monitor**)tag;
  *(int*)buf = mon->run;
  *size = sizeof(mon->run);
}
