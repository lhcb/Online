//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================
// Framework include files
#include <hist_adder/processing_db.h>
#include <sqldb/sqlite.h>
#include <RTL/strdef.h>
#include <RTL/rtl.h>

// C/C++ files
#include <system_error>

using namespace Online::histos;

/// Initializing constructor
processing_db::processing_db(const std::string& p, int dbg)
  : _debug(dbg), _path(p)
{
  this->init(_path);
}

/// Default destructor
processing_db::~processing_db() {
  this->fini();
  if ( this->database.is_valid() ) {
    this->database.commit();
    this->database.close();
  }
}

/// Initialize object
std::error_code processing_db::init(const std::string& db_name) {
  if ( !this->_inited )   {
    std::string err;
    const char* nam = db_name.c_str();
    this->database  = sqldb::database::open<sqlite>(db_name, err);
    if ( this->database.is_valid() )   {
      const char* sql = "CREATE TABLE IF NOT EXISTS Runs ("
	"RunNumber INT PRIMARY KEY, "
	"State     INT NOT NULL, "
	"StartTime INT NOT NULL, "
	"HistStart INT NOT NULL, "
	"HistTime  INT NOT NULL)";
      if ( this->database.execute(err, sql) != sqldb::OK )  {
	::lib_rtl_output(LIB_RTL_ERROR,"%s: Create table: SQL error: %s", nam, err.c_str());
	return std::make_error_code(std::errc::permission_denied);
      }
      ::lib_rtl_output(LIB_RTL_INFO,"FDB Database server: Database %s", nam);
      ::lib_rtl_output(LIB_RTL_INFO,"FDB Database server: Table FILES created successfully");
      /// Create prepared statement to insert records
      this->insert_record.prepare(database,
				  "INSERT INTO Runs (RunNumber, State, StartTime, HistStart, HistTime) "
				  "VALUES (?, ?, ?, ?, ?)");
      this->delete_record.prepare(database,
				  "DELETE FROM Runs WHERE RunNumber=?1 ");
      this->query_record_run.prepare (database,
				  "SELECT RunNumber, State, StartTime, HistStart, HistTime FROM Runs "
				  "WHERE RunNumber = ?1");
      this->query_record_range.prepare (database,
				  "SELECT RunNumber, State, StartTime, HistStart, HistTime FROM Runs "
				  "WHERE RunNumber >= ?1 AND RunNumber <= ?2 ORDER BY RunNumber ASC");
      this->query_record_state.prepare (database,
				  "SELECT RunNumber, State, StartTime, HistStart, HistTime FROM Runs "
				  "WHERE State = ?1 ORDER BY RunNumber ASC");
      this->state_update_record.prepare (database,
				  "UPDATE Runs SET State=?1, HistTime=?2 WHERE RunNumber=?3 ");
      this->_inited = true;
      return std::make_error_code(std::errc(0));
    }
    ::lib_rtl_output(LIB_RTL_INFO,"%s: FAILED to access db: [%s]", nam, err.c_str());
    return std::make_error_code(std::errc::permission_denied);
  }
  return std::make_error_code(std::errc(0));
}

/// Finalize object
void processing_db::fini() {
  if ( this->_inited )   {
    this->insert_record.finalize();
    this->delete_record.finalize();
    this->query_record_run.finalize();
    this->query_record_range.finalize();
    this->query_record_state.finalize();
    this->state_update_record.finalize();
    this->database.close();
    this->_inited = false;
  }
}

/// Check the existence of a given object in the database
std::error_code
processing_db::query_run(int32_t runno, run_t& run) {
  this->query_record_run.reset();
  this->query_record_run.bind(0, runno);
  int ret = this->query_record_run.execute();
  while ( (ret = this->query_record_run.fetch_one()) == sqldb::OK ) {
    run.run_number = this->query_record_run.get<int>(0);
    run.state = this->query_record_run.get<int>(1);
    run.start = this->query_record_run.get<int>(2);
    run.enter = this->query_record_run.get<int>(3);
    run.time  = this->query_record_run.get<int>(4);
    this->query_record_run.reset();
    return std::make_error_code(std::errc(0));
  }
  return std::make_error_code(std::errc::no_such_file_or_directory);
}

/// Query runs by run number range
std::error_code
processing_db::query_run_range(int32_t first_runno, int32_t last_runno, std::vector<run_t>& runs) {
  this->query_record_range.reset();
  this->query_record_range.bind(0, first_runno);
  this->query_record_range.bind(1, last_runno);
  int ret = this->query_record_range.execute();
  while ( (ret = this->query_record_range.fetch_one()) == sqldb::OK ) {
    run_t run;
    run.run_number = this->query_record_range.get<int>(0);
    run.state = this->query_record_range.get<int>(1);
    run.start = this->query_record_range.get<int>(2);
    run.enter = this->query_record_range.get<int>(3);
    run.time  = this->query_record_range.get<int>(4);
    runs.emplace_back(run);
  }
  this->query_record_state.reset();
  if ( !runs.empty() )
    return std::make_error_code(std::errc(0));
  return std::make_error_code(std::errc::no_such_file_or_directory);
}

/// Query runs by state
std::error_code
processing_db::query_runs_by_state(int32_t state, std::vector<run_t>& runs) {
  run_t run;
  this->query_record_state.reset();
  this->query_record_state.bind(0, state);
  int ret = this->query_record_state.execute();
  while ( (ret = this->query_record_state.fetch_one()) == sqldb::OK ) {
    run.run_number = this->query_record_state.get<int>(0);
    run.state = this->query_record_state.get<int>(1);
    run.start = this->query_record_state.get<int>(2);
    run.enter = this->query_record_state.get<int>(3);
    run.time  = this->query_record_state.get<int>(4);
    runs.emplace_back(run);
  }
  this->query_record_state.reset();
  if ( !runs.empty() )
    return std::make_error_code(std::errc(0));
  return std::make_error_code(std::errc::no_such_file_or_directory);
}

/// Add a new object to the database
std::error_code processing_db::add(run_t run)  {
  this->insert_record.reset();
  if ( 0 == run.time ) run.time = ::time(0);
  this->insert_record.bind(0, run.run_number);
  this->insert_record.bind(1, run.state);
  this->insert_record.bind(2, run.start);
  this->insert_record.bind(3, run.enter);
  this->insert_record.bind(4, run.time);
  this->database.begin();
  int ret = this->insert_record.execute();
  this->database.commit();
  this->insert_record.reset();
  if ( ret == sqldb::DONE )   {
    return std::make_error_code(std::errc(0));
  }
  else if ( ret != sqldb::OK )    {
    return std::make_error_code(std::errc::file_exists);    
  }
  return std::make_error_code(std::errc(0));
}

/// Remove an object from the database
std::error_code processing_db::del(int32_t runno)  {
  this->delete_record.bind(0, runno);
  this->database.begin();
  int ret = this->delete_record.execute();
  this->delete_record.reset();
  this->database.commit();
  if ( ret == sqldb::OK )
    return std::make_error_code(std::errc(0));
  else if ( ret == sqldb::DONE )
    return std::make_error_code(std::errc(0));
  return std::make_error_code(std::errc::no_such_file_or_directory);
}

/// Set new state to run record
std::error_code
processing_db::set_state(int32_t runno, int state)  {
  int32_t now = ::time(0);
  this->state_update_record.bind(0, state);
  this->state_update_record.bind(1, now);
  this->state_update_record.bind(2, runno);
  this->database.begin();
  int ret = this->state_update_record.execute();
  this->state_update_record.reset();
  this->database.commit();
  if ( ret == sqldb::DONE )
    return std::make_error_code(std::errc(0));
  else if ( ret == sqldb::OK )
    return std::make_error_code(std::errc(0));
  return std::make_error_code(std::errc::no_such_file_or_directory);
}

/// Check if run record was entered previously
std::pair<std::error_code, bool> processing_db::exists(int32_t runno)    {
  run_t run;
  std::error_code ret = this->query_run(runno, run);
  if ( ret.value() == 0 || std::errc(ret.value()) == std::errc::no_such_file_or_directory )
    return std::make_pair(std::make_error_code(std::errc(0)), ret.value() == 0);
  return std::make_pair(ret, false);
}

#include <RTL/rtl.h>
#include <unistd.h>
#include <iostream>

namespace  {
  void help(int ac, char** av)  {
    std::cout <<
      "Usage:  histprocessing_db_init -arg [-arg]                                    \n\n"
      "     -p=<db-path>                     Create database if not existing.          \n"
      "                                                                                \n"
      "     -help              Print this help output                                  \n"       
      "     Arguments given: ";
    RTL::CLI::print_args(ac,av);
    std::cout << std::endl << std::flush;
    ::exit(EINVAL);
  }
  std::string get_path(int argc, char** argv)  {
    RTL::CLI cli(argc, argv, help);
    std::string path;
    cli.getopt("path", 1, path);
    if ( path.empty() || cli.getopt("help",4) != nullptr )   {
      cli.call_help();
      ::exit(EINVAL);
    }
    return path;
  }
  long get_turns(int argc, char** argv)  {
    long turns = 1000;
    RTL::CLI cli(argc, argv, help);
    cli.getopt("turns", 1, turns);
    return turns;
  }
#if 0
  long get_no_time(int argc, char** argv)  {
    RTL::CLI cli(argc, argv, help);
    return 0 != cli.getopt("no-time", 7);
  }
#endif
}
//
/// Create new database
extern "C" int test_histprocessing_db_init(int argc, char** argv)   {
  std::string path = get_path(argc, argv);
  if ( ::access(path.c_str(),R_OK|W_OK) == 0 )  {
    std::error_code err(EEXIST, std::generic_category());
    std::cout << "FAILED to create histogram database " << path
	      << "[error code:" << err.value() <<", " << err.message() << "]" << std::endl;
    return err.value();
  }
  processing_db handler(path, true);
  processing_db::run_t run;
  handler.query_run(0, run);
  if ( ::access(path.c_str(),R_OK|W_OK) == 0 )  {
    std::cout << "Successfully created histogram database " << path << "." << std::endl;
    return 0;
  }
  std::error_code err(ENOENT, std::generic_category());
  std::cout << "FAILED to create histogram database " << path
	    << "[error code:" << err.value() <<", " << err.message() << "]" << std::endl;
  return err.value();
}
//
/// Test filling the database
extern "C" int test_histprocessing_db_fill(int argc, char** argv)   {
  std::string path = get_path(argc, argv);
  long turns = get_turns(argc, argv);
  processing_db handler(path, true);
  processing_db::run_t run;
  for( long i=0; i < turns; ++i)   {
    run.run_number = i+1000;
    run.state = 1;
    std::error_code ec = handler.add(run);
    if ( ec )  {
      std::cout << "FAILED to populate histogram database " << path
		<< "[error code:" << ec.value() <<", " << ec.message() << "]" << std::endl;
      return ec.value();
    }
  }
  std::cout << "Successfully populated histogram database " << path
	    << " with " << turns << " entries." << std::endl;
  return 0;
}
//
/// Test reading back from the database
extern "C" int test_histprocessing_db_read(int argc, char** argv)   {
  std::string path = get_path(argc, argv);
  if ( ::access(path.c_str(),R_OK|W_OK) == 0 )  {
    //bool no_time = get_no_time(argc, argv);
    struct tm tm_enter, tm_time;
    char t1[128], t2[128];
    int32_t turns = 0;
    std::error_code ec;
    processing_db handler(path, true);
    while( !ec )   {
      processing_db::run_t run;
      int32_t run_number = turns+1000;
      ec = handler.query_run(run_number, run);
      if ( !ec )   {
	++turns;
	::localtime_r(&run.enter, &tm_enter);
	::localtime_r(&run.time,  &tm_time);
	::strftime(t1, sizeof(t1), "%Y-%m-%dT%H:%M:%S", &tm_enter);
	::strftime(t2, sizeof(t2), "%Y-%m-%dT%H:%M:%S", &tm_time);
	::lib_rtl_output(LIB_RTL_ALWAYS,"Run: %6d state: %3d Time: %s - %s",
			 run.run_number, run.state, t1, t2);
      }
    }
    if ( 0 == turns )  {
      std::cout << "FAILED: Read " << turns << " records from histogram database " << path << "." << std::endl;
      return EINVAL;
    }
    std::cout << "Successfully read " << turns << " records from histogram database " << path << "." << std::endl;
    return 0;
  }
  std::error_code err(ENOENT, std::generic_category());
  std::cout << "FAILED to access histogram database " << path
	    << "[error code:" << err.value() <<", " << err.message() << "]" << std::endl;
  return err.value();
}

/// Test updating records the database
extern "C" int test_histprocessing_db_update(int argc, char** argv)   {
  std::string path = get_path(argc, argv);
  if ( ::access(path.c_str(),R_OK|W_OK) == 0 )  {
    int32_t count = 0;
    std::error_code ec;
    processing_db::run_t run;
    processing_db handler(path, true);
    while ( !ec )   {
      int32_t run_number = count+1000;
      ec = handler.query_run(run_number, run);
      if ( ec )   {
	std::cout << "FAILED to update histogram database " << path
		  << "[error code:" << ec.value() <<", " << ec.message() << "]" << std::endl;
	break;
      }
      ec = handler.set_state(run_number, 123);
      if ( ec )  {
	std::cout << "FAILED to update histogram database " << path
		  << "[error code:" << ec.value() <<", " << ec.message() << "]" << std::endl;
	break;
      }
      ++count;
    }
    std::cout << "Updated " << count
	      << " records in histogram database " << path << "." << std::endl;
    return 0;
  }
  std::error_code err(ENOENT, std::generic_category());
  std::cout << "FAILED to access histogram database " << path
	    << "[error code:" << err.value() <<", " << err.message() << "]" << std::endl;
  return err.value();
}
