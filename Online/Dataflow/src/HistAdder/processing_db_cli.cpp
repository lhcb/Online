//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include <hist_adder/processing_db.h>
#include <RTL/rtl.h>

/// C/C++ include files
#include <iostream>
#include <iomanip>
#include <cstring>
#include <vector>
#include <limits>
#include <string>

using namespace Online::histos;

namespace  {
  enum db_action_t  {
    DO_NOTHING,
    DO_CREATE,
    DO_DELETE,
    DO_MODIFY,
    DO_PRINT,
  };
  ///
  std::string get_second_arg(const std::string& s)   {
    std::size_t idx = s.find("=");
    if ( idx == std::string::npos ) return {};
    return s.substr(idx+1);
  }
  ///
  std::string run_state(int state)   {
    switch(state)   {
    case processing_db::STATE_ENDED:     return "ENDED";
    case processing_db::STATE_MERGED:    return "MERGED";
    case processing_db::STATE_MERGING:   return "MERGING";
    case processing_db::STATE_READ:      return "READ";
    case processing_db::STATE_FAILED:    return "FAILED";
    default:                             return "UNKNOWN";
    }
  }
  ///
  std::string run_time(std::time_t time)   {
    std::string res = ::ctime(&time);
    return res.substr(0, res.length()-1);
  }
  ///
  void print_run(const processing_db::run_t& run)    {
    ::lib_rtl_output(LIB_RTL_ALWAYS,
		     "+++ Run: %8d State: %-16s Start:%s Enter: %s Last: %s",
		     run.run_number,
		     run_state(run.state).c_str(),
		     run_time(run.start).c_str(),
		     run_time(run.enter).c_str(),
		     run_time(run.time).c_str());
  }
  ///
  void print_run(processing_db& procdb, int32_t runno)   {
    processing_db::run_t run;
    auto ret = procdb.query_run(runno, run);
    if ( !ret )   {
      print_run(run);
      return;
    }
    ::lib_rtl_output(LIB_RTL_ALWAYS, "+++ FAILED to access Run: %8d [%s]",
		     runno, ret.message().c_str());
  }
  ///
  void create_run(processing_db& procdb, int32_t runno, int32_t state)   {
    processing_db::run_t entry;
    entry.run_number = runno;
    entry.state   = state;
    entry.start = ::time(0);
    entry.enter = entry.start;
    entry.time  = entry.start;
    std::error_code ret = procdb.add(entry);
    if ( ret )    {
      ::lib_rtl_output(LIB_RTL_ALWAYS, "+++ FAILED to create Run: %8d [%s]",
		       runno, ret.message().c_str());
      return;
    }
    ::lib_rtl_output(LIB_RTL_ALWAYS, "+++ CREATED Run: %8d", runno);
  }
  ///
  void delete_run(processing_db& procdb, int32_t runno)   {
    std::error_code ret = procdb.del(runno);
    if ( ret )    {
      ::lib_rtl_output(LIB_RTL_ALWAYS, "+++ FAILED to delete Run: %8d [%s]",
		       runno, ret.message().c_str());
      return;
    }
    ::lib_rtl_output(LIB_RTL_ALWAYS, "+++ DELETED Run: %8d", runno);
  }
  
}

extern "C" int processing_db_cli(int argc, char** argv)    {
  std::vector<std::string> states;
  std::vector<int>         int_states, run_numbers;
  std::string              start_time, end_time, procdb_file;
  int                      first_run = std::numeric_limits<int>::min();
  int                      last_run  = std::numeric_limits<int>::max();
  int                      procdb_debug = 0;
  db_action_t              procdb_action = DO_NOTHING;

  for(int i = 1; i < argc && argv[i]; ++i)  {
    if ( 0 == ::strcmp("-d",argv[i]) )
      procdb_debug = 1;
    else if ( 0 == ::strncmp("--debug",argv[i],7) )
      procdb_debug = 1;
    else if ( 0 == ::strcmp("-C",argv[i]) )
      procdb_action = DO_CREATE;
    else if ( 0 == ::strncmp("--create",argv[i],8) )
      procdb_action = DO_CREATE;
    else if ( 0 == ::strcmp("-D",argv[i]) )
      procdb_action = DO_DELETE;
    else if ( 0 == ::strncmp("--delete",argv[i],8) )
      procdb_action = DO_DELETE;
    else if ( 0 == ::strcmp("-p",argv[i]) )
      procdb_action = DO_PRINT;
    else if ( 0 == ::strncmp("--print",argv[i],7) )
      procdb_action = DO_PRINT;
    else if ( 0 == ::strcmp("-m",argv[i]) )
      procdb_action = DO_MODIFY;
    else if ( 0 == ::strncmp("--modify",argv[i],8) )
      procdb_action = DO_MODIFY;
    else if ( 0 == ::strcmp("-r",argv[i]) )
      run_numbers.emplace_back(::atol(argv[++i]));
    else if ( 0 == ::strncmp("--run=",argv[i],6) )
      run_numbers.emplace_back(::atol(get_second_arg(argv[i]).c_str()));
    else if ( 0 == ::strcmp("-F",argv[i]) )
      procdb_file = argv[++i];
    else if ( 0 == ::strncmp("--file=",argv[i],7) )
      procdb_file = get_second_arg(argv[i]);
    else if ( 0 == ::strcmp("-f",argv[i]) )
      first_run = ::atol(argv[++i]);
    else if ( 0 == ::strncmp("--first-run=",argv[i],12) )
      first_run = ::atol(get_second_arg(argv[i]).c_str());
    else if ( 0 == ::strcmp("-l",argv[i]) )
      last_run = ::atol(argv[++i]);
    else if ( 0 == ::strncmp("--last-run=",argv[i],11) )
      last_run = ::atol(get_second_arg(argv[i]).c_str());
    else if ( 0 == ::strcmp("-S",argv[i]) )
      states.push_back(argv[++i]);
    else if ( 0 == ::strncmp("--state=",argv[i],8) )
      states.push_back(get_second_arg(argv[i]));
    else  {
      if ( std::string(argv[i]).find("-h") == std::string::npos )  {
	std::cout << "Offending option: " << argv[i] << std::endl;
      }
      std::cout <<
        "Usage: rundb_query_runs  -arg [-arg]                                          \n\n"
	"     Query the LHCb run database for run information                          \n\n"
	"                                                                                \n"
	"     -F <procdb-file>                 Specify processing DB file                \n"
	"     --file=<procdb-file>             dto.                                      \n"
	"     -C  --create                     Insert new record into the database       \n"
	"     -D  --delete                     Delete specified entry from the DB        \n"
	"     -m  --modify                     Modify specified entry in the DB          \n"
	"     -p  --print                      Print  specified entries                  \n"
	"                                                                                \n"
	"     -S <state-name>                  Select runs by State                      \n"
	"     --state=<state-name>             dto.                                      \n"
	"                                      Valid options: ENDED, MERGING, MERGED     \n"
	"                                      Multiple values possible.                 \n"
	"     -r <run-number>                  Select runs by run-number                 \n"
	"     --run=<run-number>               dto.                                      \n"
	"     -f <run-number>                  Select run-range: first run number.       \n"
	"     --first-run=<run-number>         dto.                                      \n"
	"     -l <run-number>                  Select run-range: last run number.        \n"
	"     --last-run=<run-number>          dto.                                      \n"
	"                                                                                \n"
        "     -help              Print this help output                                  \n"       
        "     Arguments given: ";
      RTL::CLI::print_args(argc,argv);
      std::cout << std::endl << std::flush;
      ::exit(EINVAL);
    }
  }

  for( const auto& s : states )   {
    if ( s == "ENDED" )
      int_states.emplace_back(processing_db::STATE_ENDED);
    else if ( s == "MERGED" )
      int_states.emplace_back(processing_db::STATE_MERGED);
    else if ( s == "MERGING" )
      int_states.emplace_back(processing_db::STATE_MERGING);
    else if ( s == "READ" )
      int_states.emplace_back(processing_db::STATE_READ);
    else  {
      ::lib_rtl_output(LIB_RTL_ALWAYS,"+++ UNKNOWN run state: %s", s.c_str());
      return EINVAL;
    }
  }
  if ( first_run >= 0 && last_run <= 10000000 )   {
  }
  try   {
    if ( procdb_file.empty() )   {
      ::lib_rtl_output(LIB_RTL_ALWAYS,"+++ UNKNOWN processing DB file.");
      return EINVAL;
    }
    else if ( int_states.empty() &&
	 run_numbers.empty()       &&
	 first_run == std::numeric_limits<int>::min() &&
	 last_run  == std::numeric_limits<int>::max() )  {
      ::lib_rtl_output(LIB_RTL_ALWAYS,"+++ UNKNOWN processing criteria: No runs and no run-states supplied.");
      return EINVAL;
    }

    processing_db procdb(procdb_file, procdb_debug);

    if ( !run_numbers.empty() )   {
      for( int32_t runno : run_numbers )   {
	if ( procdb_action == DO_MODIFY && !int_states.empty() )    {
	  auto ret = procdb.set_state(runno, int_states[0]);
	  if ( !ret )
	    print_run(procdb, runno);
	  else
	    ::lib_rtl_output(LIB_RTL_ALWAYS, "+++ FAILED to modify Run: %8d [%s] state: %s",
			     runno, ret.message().c_str(), states[0].c_str());
	}
	else if ( procdb_action == DO_PRINT )    {
	  print_run(procdb, runno);
	}
	else if ( procdb_action == DO_DELETE )    {
	  delete_run(procdb, runno);
	}
	else if ( procdb_action == DO_CREATE )    {
	  create_run(procdb, runno, processing_db::STATE_ENDED);
	}
	else   {
	  ::lib_rtl_output(LIB_RTL_ALWAYS,"+++ UNKNOWN processing action: %d [Ignored].", procdb_action);
	  return EINVAL;
	}
      }
      return 0;
    }
    else if ( first_run >=0 || last_run <= 10000000 )   {
      processing_db::runs_t runs;
      auto result = procdb.query_run_range(first_run, last_run, runs);
      if ( !result )   {
	if ( procdb_action == DO_PRINT )  {
	  for( const auto& run : runs )
	    print_run(run);
	  return 0;
	}
	::lib_rtl_output(LIB_RTL_ALWAYS,"+++ UNKNOWN processing action: %d [Ignored].", procdb_action);
	return EINVAL;
      }
      ::lib_rtl_output(LIB_RTL_ALWAYS,"+++ DB Query error: %d [%s]", result.value(), result.message().c_str());
      return EINVAL;
    }
    else if ( !int_states.empty() )   {
      for( int32_t state : int_states )   {
	processing_db::runs_t runs_by_state;
	auto result = procdb.query_runs_by_state(state, runs_by_state);
	if ( !result )   {
	  if ( procdb_action == DO_PRINT )    {
	    for( const auto& run : runs_by_state )
	      print_run(run);
	    continue;
	  }
	  else   {
	    ::lib_rtl_output(LIB_RTL_ALWAYS,"+++ UNKNOWN processing action: %d [Ignored].", procdb_action);
	    return EINVAL;
	  }
	}
      }
      return 0;
    }
  }
  catch(const std::exception& e)    {
    std::cout << "Exception occurred durin RPC call: " << e.what() << std::endl;
    ::exit(EINVAL);
  }
  return 0;
}
