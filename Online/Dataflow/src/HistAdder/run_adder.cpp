//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
// Created on: May 28, 2021
//==========================================================================

/// Framework include files
#include <RTL/strdef.h>
#include <hist_adder/monitor.h>
#include <hist_adder/hist_info.h>
#include <hist_adder/run_adder.h>
#include <hist_adder/file_generator.h>


/// ROOT include files
#include <TFile.h>
#include <TKey.h>
#include <TH1.h>

namespace fs = std::filesystem;

std::ostream& operator<<(std::ostream& os, const Online::histos::hist_info& info)  {
  os << "  Task:   " << info.task
     << "  Run no: " << info.runno
     << "  EOR: " << (info.eor ? "YES" : " NO");
  return os;
}

Online::histos::hist_info::hist_info(const std::filesystem::path& path)  {
  auto dirs = RTL::str_split(path.string(), "/");
  if ( dirs.size() > 0 )   {
    std::string fname = dirs[dirs.size()-1];
    fname = fname.substr(0, fname.find("."));
    auto fcomp = RTL::str_split(fname, "-");
    task = fcomp[0];
    run  = fcomp[1];
    time = fcomp[2];
    eor = fcomp.size() == 4;
    ::sscanf(run.c_str(), "%d", &runno);
  }
}

Online::histos::hist_info::operator std::vector<std::pair<std::string, std::string> > () const   {
  std::vector<std::pair<std::string, std::string> > result;
  result.emplace_back(std::make_pair("${TASK}", task));
  result.emplace_back(std::make_pair("${RUN}",  run));
  result.emplace_back(std::make_pair("${TIME}", time));
  result.emplace_back(std::make_pair("${EOR}", std::to_string(int(eor ? 1 : 0))));
  return result;
}

Online::histos::run_adder::run_adder(class monitor& m,
				     const class file_generator& g,
				     int32_t run)
  : mon(m), gen(g), m_run_number(run)
{
}

Online::histos::run_adder::updates_t
Online::histos::run_adder::collect(const std::string& dir_name, TDirectory& dir)    {
  updates_t updates;
  const TList* keys = dir.GetListOfKeys();
  TDirectory * new_directory = nullptr;
  for(int i=0; i < keys->GetSize(); ++i)    {
    TKey* key = reinterpret_cast<TKey*>(keys->At(i));
    std::unique_ptr<TObject> obj(key->ReadObj());
    if ( TNamed* named = dynamic_cast<TNamed*>(obj.get()) )   {
      std::string name = (dir_name + "/") + named->GetName();
      if ( TDirectory* subdir = dynamic_cast<TDirectory*>(named) )   {
	updates += collect(name, *subdir);
	continue;
      }
      else if ( TH1* hist = dynamic_cast<TH1*>(named) )  {
	std::string type = named->IsA()->GetName();
	TH1* found = nullptr;
	std::string test = hist->GetName();
	auto it = histograms.find(dir_name);
	if ( it != histograms.end() )   {
	  for( TH1* h : it->second )  {
	    if ( test == h->GetName() )   {
	      found = h;
	      break;
	    }
	  }
	}
	if ( found )   {
	  name = (dir_name + "/") + found->GetName();
	  found->Add(hist, 1.0);
	  ++updates.added;
	  mon.updateLogMsg(LIB_RTL_DEBUG,
			   "   ++> Update histogram [%s] %s",
			   type.c_str(), name.c_str());
	  continue;
	}
	else    {
	  TDirectory::TContext ctxt(new_directory);
	  mon.updateLogMsg(LIB_RTL_DEBUG,
			   "   ++> Load   histogram [%s] %s",
			   type.c_str(), name.c_str());
	  TH1* hnew = dynamic_cast<TH1*>(hist->Clone(hist->GetName()));
	  histograms[dir_name].emplace_back(hnew);
	  ++updates.loaded;
	  continue;
	}
      }
    }
    mon.updateLogMsg(LIB_RTL_WARNING,
		     "   ++> IGNORE OBJECT:    %s [%s]",
		     key->GetName(), obj->IsA()->GetName());
  }
  return updates;
}

Online::histos::run_adder::updates_t
Online::histos::run_adder::load(const std::filesystem::path& histos)   {
  std::unique_ptr<TFile> file(TFile::Open(histos.c_str(), "READ"));
  if (!file || file->IsZombie()) {
    mon.updateLogMsg(LIB_RTL_ERROR,
		     "Error opening histogram file: %s", histos.c_str());
    ::exit(ENOENT);
  }
  mon.updateLogMsg(LIB_RTL_DEBUG, "Opening contribution: %s", histos.c_str());
  updates_t& updates = this->run_update_info;
  ++updates.contributions;
  updates += collect("", *file);
  return updates;
}

std::pair<std::size_t, std::string>
Online::histos::run_adder::close_output()   {
  std::unique_ptr<TFile> output = gen.open<void>(m_run_number);
  std::string fname = output->GetName();
  std::size_t bytes = 0;

  mon.updateLogMsg(LIB_RTL_DEBUG, "Opened histogram output: %s", fname.c_str());
  for( const auto& e : histograms )   {
    auto dname = e.first;
    mon.updateLogMsg(LIB_RTL_DEBUG, "   ++> Output directory %s", dname.c_str());
    if ( 0 == output->mkdir(dname.c_str()+1, "", kTRUE) )   {
      mon.updateLogMsg(LIB_RTL_ERROR,
		       "Failed to create ROOT directory: %s", dname.c_str());
    }
    TDirectory* out = output->GetDirectory(dname.c_str());
    TDirectory::TContext ctxt(gDirectory, out);
    out->cd();
    for( const auto* histogram : e.second )   {
      std::string name = histogram->GetName();
      std::string type = histogram->IsA()->GetName();
      std::size_t num_bytes = out->WriteTObject(histogram);
      bytes += num_bytes;
      mon.updateLogMsg(LIB_RTL_DEBUG, "   ++> Output histogram [%s] %s  [%ld bytes]",
		       type.c_str(), name.c_str(), num_bytes);
    }
  }
  output->Close();
  output.reset();
  for( auto& e : histograms )   {
    for( auto* histogram : e.second )   {
      delete histogram;
    }
    e.second.clear();
  }
  mon.updateLogMsg(LIB_RTL_ALWAYS,
		   "Closing output %s after adding %4lu contributions,"
		   " %ld histograms %ld path-fixes",
		   fname.c_str(), run_update_info.contributions, 
		   run_update_info.added+run_update_info.loaded,
		   run_update_info.path_fixes);
  return { bytes, fname };
}
