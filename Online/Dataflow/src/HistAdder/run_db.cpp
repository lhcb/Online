//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
// Created on: May 28, 2021
//==========================================================================

/// Framework include files
#include <RPC/XMLRPC.h>
#include <HTTP/HttpClient.h>
#include <hist_adder/run_db.h>

/// C/C++ include files
#include <iostream>
#include <sstream>
#include <vector>
#include <string>

// Data marshalling for the runlist query
using rundb_runs_t        = std::vector<long>;
using rundb_runs_result_t = std::pair<long, rundb_runs_t>;

namespace xmlrpc {
  template <>  rundb_runs_result_t XmlCoder::to() const {
    rundb_runs_result_t val;
    // element is of type <value>. Need to get to the <array> to decode
    auto elts  = Array(element.child(_rpcU(array))).elements();
    val.first  = XmlCoder(elts[0]).to<long>();
    val.second = XmlCoder(elts[1]).to<rundb_runs_t >();
    return val;
  }
  template <> const XmlCoder& XmlCoder::from(const rundb_runs_result_t& val) const {
    Array(element).add(XmlCoder().from<long>(val.first));
    Array(element).add(XmlCoder().from<rundb_runs_t>(val.second));
    return *this;
  }
}
XMLRPC_IMPLEMENT_OBJECT_MARSCHALLING(array,rundb_runs_result_t)

namespace xmlrpc {
  using long_string_pair = std::pair<long, std::string>;
  template <> long_string_pair  XmlCoder::to() const {
    long_string_pair val;
    // element is of type <value>. Need to get to the <array> to decode
    auto elts  = Array(element.child(_rpcU(array))).elements();
    val.first  = XmlCoder(elts[0]).to<long>();
    val.second = XmlCoder(elts[1]).to<std::string>();
    return val;
  }
  template <> const XmlCoder& XmlCoder::from(const long_string_pair& val) const {
    Array(element).add(XmlCoder().from<long>(val.first));
    Array(element).add(XmlCoder().from<std::string>(val.second));
    return *this;
  }
}
XMLRPC_IMPLEMENT_OBJECT_MARSCHALLING(array,xmlrpc::long_string_pair);
XMLRPC_IMPLEMENT_CONTAINER_CONVERSION(std::vector,xmlrpc::long_string_pair);

namespace xmlrpc {
  using rundb_runs_data_t = std::vector<std::pair<long, std::string> >;
  using rundb_runs_data_result_t = std::pair<long, rundb_runs_data_t>;
  template <>  rundb_runs_data_result_t XmlCoder::to() const {
    rundb_runs_data_result_t val;
    // element is of type <value>. Need to get to the <array> to decode
    auto elts  = Array(element.child(_rpcU(array))).elements();
    val.first  = XmlCoder(elts[0]).to<long>();
    val.second = XmlCoder(elts[1]).to<rundb_runs_data_t>();
    return val;
  }
  template <> const XmlCoder& XmlCoder::from(const rundb_runs_data_result_t& val) const {
    Array(element).add(XmlCoder().from<long>(val.first));
    Array(element).add(XmlCoder().from<rundb_runs_data_t>(val.second));
    return *this;
  }
}
XMLRPC_IMPLEMENT_OBJECT_MARSCHALLING(array,xmlrpc::rundb_runs_data_t)
XMLRPC_IMPLEMENT_OBJECT_MARSCHALLING(array,xmlrpc::rundb_runs_data_result_t)

namespace xmlrpc {
  using long_time_pair = std::pair<long, struct tm>;
  template <> long_time_pair  XmlCoder::to() const {
    long_time_pair val;
    // element is of type <value>. Need to get to the <array> to decode
    auto elts  = Array(element.child(_rpcU(array))).elements();
    val.first  = XmlCoder(elts[0]).to<long>();
    val.second = XmlCoder(elts[1]).to<struct tm>();
    return val;
  }
  template <> const XmlCoder& XmlCoder::from(const long_time_pair& val) const {
    Array(element).add(XmlCoder().from<long>(val.first));
    Array(element).add(XmlCoder().from<struct tm>(val.second));
    return *this;
  }
}
XMLRPC_IMPLEMENT_CONTAINER_CONVERSION(std::vector,xmlrpc::long_time_pair);

namespace xmlrpc {
  using rundb_runs_time_t        = std::vector<long_time_pair>;
  using rundb_runs_time_result_t = std::pair<long, rundb_runs_time_t>;
  template <>  rundb_runs_time_result_t XmlCoder::to() const {
    rundb_runs_time_result_t val;
    // element is of type <value>. Need to get to the <array> to decode
    auto elts  = Array(element.child(_rpcU(array))).elements();
    val.first  = XmlCoder(elts[0]).to<long>();
    val.second = XmlCoder(elts[1]).to<rundb_runs_time_t>();
    return val;
  }
  template <> const XmlCoder& XmlCoder::from(const rundb_runs_time_result_t& val) const {
    Array(element).add(XmlCoder().from<long>(val.first));
    Array(element).add(XmlCoder().from<rundb_runs_time_t>(val.second));
    return *this;
  }
}

XMLRPC_IMPLEMENT_OBJECT_MARSCHALLING(array,xmlrpc::rundb_runs_time_t)
XMLRPC_IMPLEMENT_OBJECT_MARSCHALLING(array,xmlrpc::rundb_runs_time_result_t)

namespace xmlrpc {
  using tuple_result_t = std::pair<long, Tuple>;

  template <>  tuple_result_t XmlCoder::to() const  {
    tuple_result_t val;
    // element is of type <value>. Need to get to the <array> to decode
    auto elts  = Array(element.child(_rpcU(array))).elements();
    val.first  = XmlCoder(elts[0]).to<long>();
    val.second = XmlCoder(elts[1]).to<Tuple>();
    return val;
  }

  template <> const XmlCoder& XmlCoder::from(const tuple_result_t& val) const  {
    Array(element).add(XmlCoder().from<long>(val.first));
    Array(element).add(XmlCoder().from<Tuple>(val.second));
    return *this;
  }
}
XMLRPC_IMPLEMENT_OBJECT_MARSCHALLING(array,xmlrpc::tuple_result_t)

namespace xmlrpc {
  using tuple_set_result_t = std::pair<long, std::vector<Tuple> >;

  template <>  tuple_set_result_t XmlCoder::to() const {
    tuple_set_result_t val;
    // element is of type <value>. Need to get to the <array> to decode
    auto elts  = Array(element.child(_rpcU(array))).elements();
    val.first  = XmlCoder(elts[0]).to<long>();
    val.second = XmlCoder(elts[1]).to<std::vector<Tuple> >();
    return val;
  }

  template <> const XmlCoder& XmlCoder::from(const tuple_set_result_t& val) const {
    Array(element).add(XmlCoder().from<long>(val.first));
    Array(element).add(XmlCoder().from<std::vector<Tuple> >(val.second));
    return *this;
  }
}
XMLRPC_IMPLEMENT_OBJECT_MARSCHALLING(array,xmlrpc::tuple_set_result_t)

namespace {
  template <typename T> rpc::XmlRpcClientH _create_client(const std::string& node, const T& port)  {
    return rpc::client<http::HttpClient>(node, port);
  }

  /**
     def get_runs(self,
     fields=["runID"],
     runID=None,
     fillID=None,
     state=None, # can be a list
     runType=None,
     partitionName=None,
     partitionID=None,
     startTime=None,
     endTime=None,
     destination=None, # can be a list
     orderBy=None,
     fileName=None,
     fileID=None,
     no=100,
     offset=None,
     startedBefore=None,
     orderDesc=None,
     ):

  **/
  /// Access runs from the run database according to output destination, state, partition and start-timee
  xmlrpc::MethodResponse _get_runs(rpc::XmlRpcClientH& handler,
				   const rpc::run_db::string_options_t& fields,
				   const rpc::run_db::string_options_t& destinations,
				   const rpc::run_db::int_options_t&    states,
				   const std::string& partition,
				   const std::string& start_time,
				   const std::string& end_time,
				   const std::string& started_before)
  {
    int num_rows = 100;
    xmlrpc::MethodCall call;
    call.setMethod("getRuns")
      .param(fields)
      .param()
      .param()
      .param(states)
      .param()
      .param(partition)
      .param();
    start_time.empty() ? call.param() : call.param(start_time);
    end_time.empty() ? call.param() : call.param(end_time);
    destinations.empty() ? call.param() : call.param(destinations);
    call.param()
      .param()
      .param()
      .param(num_rows)
      .param();
    started_before.empty() ? call.param() : call.param(started_before);
    call.param();
    
    int dbg = handler.debug();
    if ( dbg > 2 ) std::cout << call.str() << std::endl;
    xmlrpc::MethodResponse resp = handler.call(call);
    if ( dbg > 2 ) std::cout << resp.str() << std::endl;
    return resp;
  }
}

/// Initializing constructor
rpc::run_db::run_db(rpc::XmlRpcClientH&& cl)
  : handler(std::move(cl))
{
} 

/// Initializing constructor
rpc::run_db::run_db(const std::string& node, int port)
  : handler(_create_client(node, port))
{
}

/// Initializing constructor
rpc::run_db::run_db(const std::string& node, const std::string& port)
  : handler(_create_client(node, port))
{
}

/// Access runs from the run database according to output destination, state, partition and start-timee
std::pair<rpc::run_db::status_t, rpc::run_db::runs_t> 
rpc::run_db::get_runs(const string_options_t& fields,
		      const string_options_t& destinations,
		      const int_options_t&    states,
		      const std::string&      partition,
		      const std::string&      start_time,
		      const std::string&      end_time,
		      const std::string&      started_before)   {
  auto resp = _get_runs(this->handler, fields, destinations, states,
			partition, start_time, end_time, started_before);
  std::cout << resp.str() << std::endl;
  xmlrpc::tuple_set_result_t ret;
  if ( fields.size() == 1 )   {
    auto r = resp.data<xmlrpc::tuple_result_t>();
    xmlrpc::Tuple t = xmlrpc::Tuple(r.second.root.child(_rpcU(data)));
    ret.first = r.first;
    for(const auto& f : t.fields)    {
      ret.second.emplace_back(xmlrpc::Tuple(f));
    }
  }
  else   {
    auto r = resp.data<xmlrpc::tuple_set_result_t>();
    ret.first = r.first;
    for( auto& t : r.second )   {
      auto clone = t.root.clone(nullptr);
      ret.second.emplace_back(xmlrpc::Tuple(clone.child(_rpcU(data))));
    }
  }
  std::cout << "Size of tuple set: " << ret.second.size() << std::endl;
  for(std::size_t i=0; i < ret.second.size(); ++i)   {
    const xmlrpc::Tuple& tuple = ret.second[i];
    const xmlrpc::Tuple::Field& root = tuple.root;
    std::cout << tuple.root.parent().tag() << " : " << root.get<std::string>() << std::endl;
    std::cout << "Tuple : " << tuple.str() << std::endl;
    for(const auto& f : tuple.fields) 
      std::cout << "+++ " << f.tag() << ":   " << f.type()
		<< " = " << f.get<std::string>()
		<< std::endl;
  }
  return ret;
}

/// Access runs from the run database according to output destination, state, partition and start-timee
std::pair<rpc::run_db::status_t, rundb_runs_t>
rpc::run_db::get_runs(const string_options_t& destinations,
		      const int_options_t&    states,
		      const std::string&      partition,
		      const std::string&      start_time,
		      const std::string&      end_time,
		      const std::string&      started_before)   {
  std::vector<std::string> fields = { "runID" };
  auto resp = _get_runs(this->handler, fields, destinations, states, partition, start_time, end_time, started_before);
  auto runs = resp.data<rundb_runs_result_t>();
  return runs;
}

/// Access runs from the run database according to output destination, state, partition and start-timee
std::pair<rpc::run_db::status_t, std::vector<std::pair<long, struct tm> > >
rpc::run_db::get_runs_start(const string_options_t& destinations,
			    const int_options_t&    states,
			    const std::string&      partition,
			    const std::string&      start_time,
			    const std::string&      end_time,
			    const std::string&      started_before)   {
  std::vector<std::string> fields = { "runID", "startTime" };
  auto resp = _get_runs(this->handler, fields, destinations, states, partition, start_time, end_time, started_before);
  auto runs = resp.data<std::pair<rpc::run_db::status_t, std::vector<std::pair<long, struct tm> > > >();
  return runs;
}

/// Access runs from the run database according to output destination, state, partition and first known run
xmlrpc::Tuple
rpc::run_db::get_runs_start(const string_options_t& fields,
			    const string_options_t& destinations,
			    const int_options_t&    states,
			    const std::string&      partition,
			    const std::string&      start_time,
			    const std::string&      end_time,
			    const std::string&      started_before)
{
  using namespace xmlrpc;
  auto resp = _get_runs(this->handler, fields, destinations, states, partition, start_time, end_time, started_before);
  Tuple ret;  {
    ret.root = resp.value.parent();
    ret.set_document(resp);
  }
  if ( ret.is<Array>() )  {
    auto elements  = ret.get<Array>().elements();
    Tuple code(elements[0]);
    if ( code.get<int>() == 1 )    {
      Tuple(elements[1]).get<Array>();
      return ret;
    }
  }
  return ret;
}
