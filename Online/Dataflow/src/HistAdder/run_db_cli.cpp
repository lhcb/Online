//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
// Created on: May 28, 2021
//==========================================================================

/// Framework include files
#include <hist_adder/run_db.h>
#include <RTL/rtl.h>

/// C/C++ include files
#include <iostream>
#include <iomanip>
#include <cstring>
#include <vector>
#include <string>

namespace  {
  std::string get_second_arg(const std::string& s)   {
    std::size_t idx = s.find("=");
    if ( idx == std::string::npos ) return {};
    return s.substr(idx+1);
  }
}

extern "C" int rundb_query_runs(int ac, char** av)    {
  std::string runtype, partition, partitionid;
  std::string start_time, end_time, started_before;
  std::vector<std::string> destinations, fields;
  std::vector<int> states;
  int max_rows = 100;

  for(int i = 1; i < ac && av[i]; ++i)  {
    if ( 0 == ::strcmp("-p",av[i]) )
      partition = av[++i];
    else if ( 0 == ::strncmp("--partition=",av[i],12) )
      partition = get_second_arg(av[i]);
    else if ( 0 == ::strcmp("-f",av[i]) )
      fields.emplace_back(av[++i]);
    else if ( 0 == ::strncmp("--field=",av[i],8) )
      fields.emplace_back(get_second_arg(av[i]));
    else if ( 0 == ::strcmp("-P",av[i]) )
      partitionid = av[++i];
    else if ( 0 == ::strncmp("--partitionid=",av[i],14) )
      partitionid = get_second_arg(av[i]);
    else if ( 0 == ::strcmp("-r",av[i]) )
      runtype = av[++i];
    else if ( 0 == ::strncmp("--runtype=",av[i],10) )
      runtype = get_second_arg(av[i]);
    else if ( 0 == ::strcmp("-S",av[i]) )
      states.push_back(::atol(av[++i]));
    else if ( 0 == ::strncmp("--state=",av[i],8) )
      states.push_back(::atol(get_second_arg(av[i]).c_str()));
    else if ( 0 == ::strcmp("-R",av[i]) )
      max_rows = ::atol(av[++i]);
    else if ( 0 == ::strncmp("--rows=",av[i],7) )
      max_rows = ::atol(get_second_arg(av[i]).c_str());
    else if ( 0 == ::strcmp("-d",av[i]) )
      destinations.push_back(av[++i]);
    else if ( 0 == ::strncmp("--destination=",av[i],14) )
      destinations.push_back(get_second_arg(av[i]));
    else if ( 0 == ::strcmp("-b",av[i]) )
      started_before = av[++i];
    else if ( 0 == ::strncmp("--before=",av[i],9) )
      started_before = get_second_arg(av[i]);
    else if ( 0 == ::strcmp("-s",av[i]) )
      start_time = av[++i];
    else if ( 0 == ::strncmp("--start-time=",av[i],13) )
      start_time = get_second_arg(av[i]);
    else if ( 0 == ::strcmp("-e",av[i]) )
      end_time = av[++i];
    else if ( 0 == ::strncmp("--end-time=",av[i],11) )
      end_time = get_second_arg(av[i]);
    else  {
      if ( std::string(av[i]).find("-h") == std::string::npos )  {
	std::cout << "Offending option: " << av[i] << std::endl;
      }
      std::cout <<
        "Usage: rundb_query_runs  -arg [-arg]                                          \n\n"
	"     Query the LHCb run database for run information                          \n\n"
	"     -f <data-field-name>             Select data field to retrieve             \n"
	"     --field=<data-field-name>        dto.                                      \n"
	"                                      Multiple values possible.                 \n"
	"                                      possible choices:                         \n"
	"                                      runID          fillID       state         \n"
	"                                      partitionName  partitionID  startTime     \n"
	"                                      endTime         runType     destination   \n"
	"                                                                                \n"
	"     -p <partition>                   Select runs by partition name             \n"
	"     --partition=<partition-name>     dto.                                      \n"
	"     -P <partition>                   Select runs by partition ID               \n"
	"     --partitionid=<partition-name>   dto.                                      \n"
	"     -d <string>                      Select runs by data destination           \n"
	"     --destination=<destination-name> dto.                                      \n"
	"                                      Valid options: LOCAL, EOS, OFFLINE        \n"
	"                                      Multiple values possible.                 \n"
	"     -S <number>                      Select runs by State                      \n"
	"     --state=<number>                 dto.                                      \n"
	"                  Valid options: 1 : ACTIVE,   2 : ENDED,   3 : MIGRATING,      \n"
	"                                 4 : MIGRATED, 5 : CREATED, 6 : TRANSFERRED,    \n"
	"                                 7 : DEFERRED, 8 : ALIGNED                      \n"
	"                                      Multiple values possible.                 \n"
	"     -r <run-type>                    Select runs by run-type                   \n"
	"     --runtype=<run-type>             dto.                                      \n"
	"     -b <start-time>                  Select runs started before time           \n"
	"     --before=<start-time>            dto.                                      \n"
	"     -s <start-time>                  Select runs after specified time          \n"
	"     --start-time=<start-time>        dto.                                      \n"
	"     -e <end-time>                    Select runs ended before specified time   \n"
	"     --end-time=<end-time>            dto.                                      \n"
	"                                      Time format(iso8601): YYYY-mm-DDTHH::MM:SS\n"
	"     -R <number-of-rows>              Restrict return. Default: 100             \n"
	"     --rows=<number-of-rows>          dto.                                      \n"
	"                                                                                \n"
        "     -help              Print this help output                                  \n"       
        "     Arguments given: ";
      RTL::CLI::print_args(ac,av);
      std::cout << std::endl << std::flush;
      ::exit(EINVAL);
    }
  }

  using namespace xmlrpc;
  if ( max_rows > 1000 )   {
  }
  if ( fields.empty() )    {
    fields = { "runID", "startTime" };
  }
  Tuple ret;
  try   {
    rpc::run_db db("rundb11.lbdaq.cern.ch", 8080);
    ret = db.get_runs_start(fields, destinations, states, partition, start_time, end_time, started_before);
  }
  catch(const std::exception& e)    {
    std::cout << "Exception occurred durin RPC call: " << e.what() << std::endl;
    ::exit(EINVAL);
  }
  if ( ret.is<Array>() )  {
    auto elements  = ret.get<Array>().elements();
    Tuple code(elements[0]);
    if ( code.get<int>() == 1 )    {
      Array runs = Tuple(elements[1]).get<Array>();
      auto  run_set = runs.elements();
      std::cout << "Size of run set: " << run_set.size() << std::endl;
      for(std::size_t i=0; i < run_set.size(); ++i)   {
	Tuple run(run_set[i]);
	if ( run.is<Array>() )   {
	  auto params = run.get<Array>().elements();
	  for( std::size_t j=0; j<params.size(); ++j )   {
	    Array::Entry f(params[j]);
	    std::string tag = f.tag();
	    std::string typ = f.type();
	    std::cout << std::left << std::setw(6) << std::dec << i
		      << " +++ " << std::left << std::setw(24) << fields[j]
		      << tag    << " = " << std::left << std::setw(32);
	    if ( typ == f.type_name<int>() )
	      std::cout << f.get<int>();
	    else if ( typ == f.type_name<double>() )
	      std::cout << f.get<double>();
	    else if ( typ == f.type_name<std::string>() )
	      std::cout << f.get<std::string>();
	    else if ( typ == f.type_name<tm>() )  {
	      auto tim = f.get<tm>();
	      std::string time_str = ::asctime(&tim);
	      std::cout << time_str.substr(0, time_str.length()-1);
	    }
	    else  {
	      std::cout << "UNKNOWN DATA TYPE";
	    }
	    std::cout << "  [" << typ << "]" << std::endl;
	  }
	  std::cout << std::endl;
	}
      }
      std::cout << "Got " << run_set.size() << " run_db entries " << std::endl;
    }
  }
  return 0;
}
