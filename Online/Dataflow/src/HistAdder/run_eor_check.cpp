//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
// Created on: April 28, 2023
//==========================================================================

/// Framework include files
#include <hist_adder/eor_check_app.h>
#include <CPP/IocSensor.h>
#include <RTL/rtl.h>
#include <iostream>

extern "C" int run_eor_check(int argc, char** argv)  {
  using Online::histos::eor_check_app;
  eor_check_app app;
  RTL::CLI cli(argc, argv, [](int ac, char** av)  {
    std::cout << "run_hist_app  -opt [-opt]                                                                          \n\n"
	      << "  Parameter for histogram merging:                                                                   \n"
	      << "    -input=<input-directory>        Path to histogram data input directory                           \n"
	      << "                                    Will be recursively scanned for ROOT files                       \n"
	      << "    -partition=<partition-name>     Partition name for output histograms                             \n"
	      << "  Parameter for merge server app                                                                     \n"
	      << "    -disk-scan-interval=<seconds>   Update interval to look for runs on disk <input>.                \n"
	      << "    -rundb-scan-interval=<seconds>  Update interval to look for runs in state ENDED in the run db    \n"
	      << "    -days-to-scan=<number>          Number of days to search the run database at startup             \n"
	      << "    -print=<print-level>            Printout level VERBOSE, DEBUG,...,FATAL                         \n\n"
	      << " \n";
    RTL::CLI::print_args(ac, av);
    ::exit(EINVAL);
  });

  std::string prt = "INFO";
  cli.getopt("input_directory",      5, app.checker.base_directory);
  cli.getopt("partition",            3, app.partition);
  cli.getopt("rundb-days-to-scan",   9, app.rundb_days_to_scan);
  cli.getopt("rundb-scan-interval",  9, app.rundb_scan_interval);
  cli.getopt("disk-scan-interval",   8, app.disk_scan_interval);
  cli.getopt("print",                3, prt);

  RTL::Logger::LogDevice::setGlobalDevice(Online::histos::logger(), prt);
  app.init();
  IocSensor::instance().send(&app, eor_check_app::SCAN_RUNDB, &app);
  IocSensor::instance().run();
  return 0;
}
