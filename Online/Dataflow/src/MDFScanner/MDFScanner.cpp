//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  MDFScanner.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

#include "MDFScanner.h"
#include <EventData/RawFile.h>
#include <RTL/readdir.h>
#include <iostream>
#include <sys/stat.h>
#include <fcntl.h>
#include <iostream>
#include <cstring>
#include <cerrno>
#ifdef _WIN32
#include <io.h>
#else
#include <unistd.h>
#define O_BINARY 0
#endif

using namespace Online;
using namespace std;

/// Scan one single file and check for errors
int MDFScanner::scanFile(const string& fname)   {
  off_t offset = 0;
  RawFile mdf(fname);
  int fd = mdf.open();
  if ( fd != -1 )   {
    while(1)  {
      int size_buf[3] = {0,0,0};
      //off_t file_position = ::lseek(fd, 0, SEEK_CUR);
      int status = mdf.read((unsigned char*)size_buf, sizeof(size_buf));
      if (status <= 0)   {
	printout(INFO,"MDFScanner","End-Of-File %s after %ld bytes.",fname.c_str(),offset);
	goto Done;
      }
      else  {
	int  evt_size = size_buf[0];
	bool is_mdf   = evt_size > 0 && size_buf[0] == size_buf[1] && size_buf[0] == size_buf[2];
	offset += sizeof(size_buf);
	if ( !is_mdf )   {
	  long skip = mdf.scanToNextMDF();
	  if ( skip > 0 )   {
	    continue;
	  }
	  goto Done;
	}
	unsigned char* buffer = (unsigned char*)::operator new(evt_size);
	::memcpy(buffer,size_buf,sizeof(size_buf));
	status = mdf.read(buffer+sizeof(size_buf), evt_size-sizeof(size_buf));
	if (status <= 0)   {
	  printout(INFO,"MDFScanner","End-Of-File %s after %ld bytes.",fname.c_str(),offset);
	  goto Done;
	}
	offset += evt_size-sizeof(size_buf);
	int ret = mdf.checkRecord(offset,buffer,evt_size);
	if ( ret != 1 )   {
	}
      }
    }
  Done:
    ::close(fd);
  }
  return 0;
}

/// Scan directory for matching files
size_t MDFScanner::scanDirectory(const string& dir_name)   {
  if ( !dir_name.empty() )   {
    DIR* dir = opendir(dir_name.c_str());
    if (dir)  {
      struct dirent *entry;
      while ((entry = ::readdir(dir)) != 0)    {
	if ( ::strstr(entry->d_name,".raw") )  {
	  m_files.insert(make_pair(entry->d_name,dir_name + "/" + entry->d_name));
	}
      }
      ::closedir(dir);
      printout(INFO,"MDFScanner","Scanned directory %s: %d files.",dir_name.c_str(), int(m_files.size()));
      return m_files.size();
    }
    string err = RTL::errorString();
    printout(ERROR,"MDFScanner","Failed to open directory: %s",!err.empty() ? err.c_str() : "????????");
  }
  return 0;
}

/// Scan directory for matching files
size_t MDFScanner::scanFiles()   {
  m_files.clear();
  for(size_t i=0; i<m_dirList.size(); ++i)  {
    scanDirectory(m_dirList[i]);
  }
  return m_files.size();
}

/// Open a new data file
int MDFScanner::openFile()   {
  while (m_files.size() > 0)  {
    StringMap::iterator i = m_files.begin();
    string fname = (*i).second;
    m_files.erase(i);
    RawFile mdf(fname);
    int fd = mdf.open();
    if ( -1 == fd )
      continue;
    return fd;
  }
  return 0;
}

namespace {
  void help()  {
  }
}

extern "C" int mdf_file_scanner(int argc, char** argv)   {
  RTL::CLI cli(argc,argv,help);
  return 0;
}
