//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  MDFScanner.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef DATAFLOW_MDF_SCANNER_H
#define DATAFLOW_MDF_SCANNER_H

/// Framework include files
#include <RTL/rtl.h>
#include <Dataflow/Printout.h>
#include <Tell1Data/Tell1Decoder.h>

/// C/C++ include files
#include <map>
#include <vector>

/// Online namespace declaration
namespace Online  {
  
  class MDFScanner  {
    typedef std::map<std::string,std::string> StringMap;
    StringMap                m_files;
    std::vector<std::string> m_dirList;
    
  public:

    /// Scan directory for matching files
    size_t scanDirectory(const std::string& dir_name);
    /// Scan directory for matching files
    size_t scanFiles();
    /// Open a new data file
    int openFile();
    /// Scan one single file and check for errors
    int scanFile(const std::string& file);
    /// Scan single record and check for consistency
    int checkRecord(size_t offset, const void* data, size_t length);
  };
}
#endif /* DATAFLOW_MDF_SCANNER_H  */
