//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  StorageWriter.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include "StorageWriter.h"
#include <Dataflow/Plugins.h>
#include <Dataflow/Printout.h>
#include <Storage/fdb_client.h>
#include <EventData/RawFile.h>
#include <EventData/odin_t.h>
#include <EventData/bank_types_t.h>
#include <EventData/event_header_t.h>
#include <PCIE40Data/pcie40decoder.h>
#include <CPP/mem_buff.h>
#include <RTL/strdef.h>
#include <RTL/rtl.h>

/// ROOT include files
#include <TFile.h>
#include <TSystem.h>
#include <TUrl.h>

/// C/C++ include files
#include <filesystem>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <unistd.h>
#include <fcntl.h>
#include <cstring>
#include <sstream>
#include <ctime>

namespace {
  static constexpr long MBYTE = (1024e0*1024e0);
  static constexpr const char* NULL_Device = "/null";
}

#include <zlib.h>

using namespace Online;
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_StorageWriter,StorageWriter)

namespace  {
  bool _check_nfs_access(const std::string& dir_name)    {
    std::error_code ec;
    struct stat dirfs, rootfs;
    std::filesystem::path check_dir = std::filesystem::absolute(dir_name, ec);
    
    /// We need to check the parent directory which exists to check if the volumes are different:
    while( check_dir.string().find("/",1) != std::string::npos )    {
      check_dir = check_dir.parent_path();
      if ( std::filesystem::exists(check_dir, ec) ) break;
    }
    // Get the device ID of the ROOT directory:
    if ( 0 != ::stat("/", &rootfs) ) {
      errno = ENODEV;
      return false;
    }
    // Get the device ID of the output volume:
    // std::cout << "Checking: " << check_dir.string() << std::endl;
    if ( 0 != ::stat(check_dir.string().c_str(), &dirfs) ) {
      errno = ENODEV;
      return false;
    }
    // The two devices must be different:
    // std::cout << "Checking: " << dirfs.st_dev << " <> " << rootfs.st_dev << std::endl;
    if ( dirfs.st_dev == rootfs.st_dev )    {
      errno = ENODEV;
      return false;
    }
    /// Usable output area.....
    return true;
  }
}

struct StorageWriter::POSIX_FILE   {
public:
  gzFile      gzip       { nullptr };
  RawFile     file       {   };
  std::mutex  lock       {   };
  uint64_t    length     { 0 };
  uint32_t    run        { 0 };
  uint32_t    num_events { 0 };
  uint32_t    num_writes { 0 };
  std::time_t last_write { 0 };
  
public:
  POSIX_FILE() = default;
  POSIX_FILE(POSIX_FILE&& copy) = delete;
  POSIX_FILE(const POSIX_FILE& copy) = delete;
  POSIX_FILE& operator=(POSIX_FILE&& copy) = delete;
  POSIX_FILE& operator=(const POSIX_FILE& copy) = delete;
  ~POSIX_FILE() = default;

  void close()   {
    if ( gzip != nullptr )  {
      ::gzclose_w(gzip);
      gzip = nullptr;
    }
    if ( this->file.isOpen() )   {
      this->file.close();
    }
    this->length = 0;
    this->run = 0;
  }
  int enable_compression(std::size_t buffer_len, int level, int strategy=Z_DEFAULT_STRATEGY)   {
    if ( this->file.isOpen() )  {
      gzip = ::gzdopen(this->file.fileno(), "w");
    }
    if ( nullptr == gzip )  {
      return -1;
    }
    if ( 0 != ::gzbuffer(gzip, buffer_len) )   {
      ::gzclose(gzip);
      gzip = nullptr;
      return -1;
    }
    if ( 0 != ::gzsetparams(gzip, level, strategy) )  {
      ::gzclose(gzip);
      gzip = nullptr;
      return -1;
    }
    return 0;
  }
  int open(const std::string& fname, int verifyNFS)   {
    std::filesystem::path parent = std::filesystem::path(fname).parent_path();
    if ( verifyNFS )   {
      if ( !_check_nfs_access(parent.string()) )   {
	errno = ENODEV;
	return -1;
      }
    }
    if ( 0 == RawFile::mkdir(parent.c_str(),0777) )   {
      this->file.setName(fname);
      this->length = 0;
      this->run = 0;
      return this->file.openWrite(false);
    }
    return -1;
  }
  const char* name() const   {
    return this->file.cname();
  }
  bool isOpen() const       {
    return this->file.isOpen();
  }
  long write(const void* buff, std::size_t len)  {
    if ( gzip )  {
      return ::gzwrite(gzip, buff, len);
    }
    return this->file.write(buff, len);
  }
};

/// ROOT implementation
struct StorageWriter::ROOT_FILE   {
public:
  std::unique_ptr<TFile> file       { };
  uint64_t               length     { 0 };
  uint32_t               run        { 0 };
  uint32_t               num_events { 0 };
  uint32_t               num_writes { 0 };
  std::time_t            last_write { 0 };
  std::mutex             lock       { };
public:
  ROOT_FILE() = default;
  ROOT_FILE(ROOT_FILE&& copy) = delete;
  ROOT_FILE(const ROOT_FILE& copy) = delete;
  ROOT_FILE& operator=(ROOT_FILE&& copy) = delete;
  ROOT_FILE& operator=(const ROOT_FILE& copy) = delete;
  ~ROOT_FILE() = default;
  void close()   {
    this->length = 0;
    this->run = 0;
    if ( this->file ) this->file->Close();
    this->file.reset();
  }
  int open(const std::string& fname, int verifyNFS)   {
    std::filesystem::path parent = std::filesystem::path(fname).parent_path();
    if ( verifyNFS )    {
      if ( !_check_nfs_access(parent.string()) )   {
	errno = ENODEV;
	return -1;
      }
    }
    void* dir = gSystem->OpenDirectory(parent.c_str());
    if ( dir )   {
      gSystem->FreeDirectory(dir);
    }
    else if ( 0 != gSystem->mkdir(parent.c_str(), kTRUE) )    {
      errno = gSystem->GetErrno();
      return -1;
    }
    TUrl    url(fname.c_str());
    TString opts = "filetype=raw", proto, spec, tmp = url.GetOptions();
    if ( tmp.Length() > 0 ) {
      opts += "&";
      opts += url.GetOptions();
    }
    url.SetOptions( opts );
    proto = url.GetProtocol();
    spec  = (proto == "file" || proto == "http") ? fname + "?filetype=raw" : url.GetUrl();
    this->file.reset(TFile::Open(spec, "RECREATE", "", 0));
    this->length = 0;
    this->run = 0;
    if ( this->file && !this->file->IsZombie() )
      return 1;
    errno = gSystem->GetErrno();
    this->file.reset();
    return -1;
  }
  /// No-op. Cannot be implemented for ROOT
  int enable_compression(std::size_t, int , int = 0)   {
    return 0;
  }
  const char* name() const   {
    return this->file->GetName();
  }
  bool isOpen() const   {
    return this->file.get() ? this->file->IsOpen() : false;
  }
  long write(const void* buff, std::size_t len)  {
    Bool_t ret = this->file->WriteBuffer((const char*)buff, len);
    if ( kTRUE == ret )   {
      errno = gSystem->GetErrno();
      return -1;
    }
    return len;
  }
};

/// Initializing constructor
StorageWriter::StorageWriter(const std::string& nam, Context& ctxt)
  : Component(nam, ctxt)
{
  std::string fname = "/${PARTITION}/${RUN1000}/Run_${RUN}_${NODE}_${TIME}_${PID}_${SEQ}.mdf";
  this->declareProperty("Server",            m_server);
  this->declareProperty("FDBVersion",        m_fdb_version       = 0);
  this->declareProperty("BufferSizeMB",      m_buffer_size       = 1024);
  this->declareProperty("WriteErrorRetry",   m_write_error_retry = 10);
  this->declareProperty("WriteErrorSleep",   m_write_error_sleep = 2000);
  this->declareProperty("PollTimeout",       m_poll_tmo          = 1000);
  this->declareProperty("IdleTimeout",       m_idle_tmo          = 20);
  this->declareProperty("StopTimeout",       m_stop_tmo          = 10000);
  this->declareProperty("CancelTimeout",     m_cancel_tmo        = 100);
  this->declareProperty("NumBuffers",        m_num_buffers       = 2);
  this->declareProperty("NumThreads",        m_num_threads       = 1);
  this->declareProperty("MinFileSizeMB",     m_min_file_size_MB  = 0);
  this->declareProperty("MaxFileSizeMB",     m_max_file_size_MB  = 5000);
  this->declareProperty("DebugClient",       m_debug_client      = 0);
  this->declareProperty("OutputType",        m_output_type       = "network");
  this->declareProperty("HaveFileDB",        m_have_file_db      = 0);
  this->declareProperty("EnableWriting",     m_enable_writing    = 1);
  this->declareProperty("VerifyNFS",         m_verify_nfs        = 1);
  this->declareProperty("MEP2MDF",           m_mep2mdf           = 1);
  this->declareProperty("MaxEvents",         m_max_events        = std::numeric_limits<int>::max());
  this->declareProperty("MinAllowedRunno",   m_min_allowed_runno = 200000);
  this->declareProperty("MaxAllowedRunno",   m_max_allowed_runno = 500000);
  this->declareProperty("Stream",            m_stream            = "RAW");
  this->declareProperty("Activity",          m_activity          = "");
  this->declareProperty("RunList",           m_run_list);
  this->declareProperty("PartitionName",     m_partition_name    = "LHCb");
  this->declareProperty("FileName",          m_file_name         = fname);
  this->declareProperty("ThreadFileQueues",  m_threadFileQueues  = false);
  this->declareProperty("HaveStats",         m_have_stats        = false);
  this->declareProperty("ChecksumType",      m_checksumType      = 0);
  this->declareProperty("PrintChecksum",     m_printChecksum     = false);
  this->declareProperty("CompressionLevel",  m_compressionLevel  = 0);
  this->declareProperty("CompressionType",   m_compressionTypeName = "NONE");
  this->declareProperty("FileCompression",   m_fileCompression   = false);
  storage::error_check_enable(false);
}

/// Default destructor
StorageWriter::~StorageWriter()   {
}

/// Initialize the MBM server
int StorageWriter::initialize()  {
  int32_t sc = this->Component::initialize();
  if ( sc == DF_SUCCESS )  {
    storage::uri_t url(this->m_server);
    this->m_buffer_size *= MBYTE;
    this->m_cancelled   = 0;
    this->m_curr_run = 0;

    if ( this->m_file_name.find(":") != std::string::npos )
      this->m_output_type_id = ROOT_STORAGE, m_output_type = "ROOT";
    else if ( ::strcasecmp(this->m_output_type.c_str(), "NFS") == 0 )
      this->m_output_type_id = POSIX_STORAGE;
    else if ( ::strcasecmp(this->m_output_type.c_str(), "ROOT") == 0 )
      this->m_output_type_id = ROOT_STORAGE;
    else if ( ::strcasecmp(this->m_output_type.c_str(), "POSIX") == 0 )
      this->m_output_type_id = POSIX_STORAGE;
    else if ( ::strcasecmp(this->m_output_type.c_str(), "NETWORK") == 0 )
      this->m_output_type_id = NETWORK_STORAGE;

    for(Buffer& b : this->m_free)
      ::free(b.buffer);
    this->m_free.clear();
    for(std::size_t i=0; i<this->m_num_buffers; ++i)   {
      Buffer b;
      b.buffer  = (uint8_t*)::malloc(m_buffer_size+1024);
      b.pointer = b.buffer;
      this->m_free.push_back(b);
    }
    this->m_compressionLevel &= 0xF;
    this->m_compressionType = DataCompression::NONE;
    if ( this->m_compressionTypeName == "NONE" )
      this->m_compressionType = DataCompression::NONE;
    else if ( this->m_compressionTypeName == "ZLIB" )
      this->m_compressionType = DataCompression::ZLIB;
    else if ( this->m_compressionTypeName == "ZSTD" )
      this->m_compressionType = DataCompression::ZSTD;
    else if ( this->m_compressionTypeName == "LZMA" )
      this->m_compressionType = DataCompression::LZMA;
    else if ( this->m_compressionTypeName == "LZ4" )
      this->m_compressionType = DataCompression::LZ4;
    else
      except("Unknown compression algorithm name: %s", this->m_compressionTypeName.c_str());

    this->m_num_active_buffers = this->m_active.size();
    this->m_num_free_buffers   = this->m_free.size();
    this->m_num_todo_buffers   = this->m_todo.size();

    this->declareMonitor("Events","OUT",     m_events_OUT=0,           "Number of events processed");
    this->declareMonitor("Events","DROPPED", m_events_DROP=0,          "Number of events processed");
    this->declareMonitor("Bursts","OUT",     m_burstsOUT=0,            "Number of bursts processed");
    this->declareMonitor("FilesOpen",        m_filesOpen=0,            "Number of output files currently open");
    this->declareMonitor("FilesOpened",      m_filesOpened=0,          "Number of output files opened");
    this->declareMonitor("FilesClosed",      m_filesClosed=0,          "Number of output files closed");
    this->declareMonitor("BytesOut",         m_bytesOut=0,             "Number of bytes collected");
    this->declareMonitor("BytesOutUnComp",   m_bytesOutUncompressed=0, "Number of bytes uncompressed");
    this->declareMonitor("BytesDropped",     m_bytesDropped=0,         "Number of bytes dropped");
    this->declareMonitor("BadHeader",        m_badHeader=0,            "Number of MDF events with bad header structure");
    this->declareMonitor("NumActiveBuffers", m_num_active_buffers,     "Number of currently active buffers");
    this->declareMonitor("NumTodoBuffers",   m_num_todo_buffers,       "Number of current todo buffers");
    this->declareMonitor("NumFreeBuffers",   m_num_free_buffers,       "Number of currently free buffers");

    this->m_shutdown = false;
    for(std::size_t i=0; i<this->m_num_threads; ++i)
      this->m_threads.emplace_back(std::make_unique<std::thread>([this]{ this->process_buffers(); }));
    storage::error_check_enable(this->outputLevel < INFO);
    if ( this->decode_run_list() != DF_SUCCESS )    {
      return DF_ERROR;
    }
  }
  return sc;
}

/// Initialize the MBM server
int StorageWriter::start()  {
  int32_t sc = this->Component::start();
  if ( sc != DF_SUCCESS )  {
    return sc;
  }
  if ( this->decode_run_list() != DF_SUCCESS )    {
    return DF_ERROR;
  }
  this->m_last_event_stamp = ::time(0);
  this->m_bytesOutUncompressed += 0;
  this->m_shutdown = false;
  this->m_cancelled = 0;
  this->m_curr_run = 0;
  return sc;
}

/// Stop the data flow component. Default implementation is empty.
int StorageWriter::stop()  {
  this->m_cancelled = ::time(0);
  if ( !this->m_active.empty() )   {
    std::lock_guard<std::mutex> bufferLock(this->m_buffer_lock);
    while ( !this->m_active.empty() )   {
      auto buff = std::move(m_active.begin()->second);
      m_active.erase(m_active.begin());
      this->m_num_active_buffers = this->m_active.size();
      if ( buff.buffer != nullptr && buff.pointer > buff.buffer )   {
	this->m_todo.push_back(buff);
	this->m_num_todo_buffers = this->m_todo.size();
      }
    }
  }
  if ( this->m_stop_tmo > 0 )   {
    std::time_t now = ::time(0);
    while ( this->m_num_todo_buffers != 0 && (::time(0)-now)*1000 < this->m_stop_tmo )   {
      ::lib_rtl_sleep(100);
    }
    if ( this->m_num_todo_buffers > 0 )  {
      warning("+++ Leaving stop with %d buffers still to be committed. "
	      "May increase property 'StopTimeout' (%d msec)",
	      this->m_num_todo_buffers, this->m_stop_tmo);
    }
  }
  ::lib_rtl_usleep(m_poll_tmo);
  this->m_curr_run = 0;
  return this->Component::stop();
}

/// Cancel the data flow component. Default implementation is empty.
int StorageWriter::cancel()  {
  this->m_cancelled = ::time(0);
  return this->Component::cancel();
}

/// Finalize the MBM server
int StorageWriter::finalize()  {
  if ( !this->m_active.empty() )   {
    std::lock_guard<std::mutex> bufferLock(this->m_buffer_lock);
    while ( !this->m_active.empty() )   {
      auto buff = std::move(m_active.begin()->second);
      m_active.erase(m_active.begin());
      this->m_num_active_buffers = this->m_active.size();
      if ( buff.buffer != nullptr && buff.pointer > buff.buffer )   {
	this->m_todo.push_back(buff);
	this->m_num_todo_buffers = this->m_todo.size();
      }
    }
  }
  ::lib_rtl_usleep(this->m_poll_tmo);
  this->m_curr_run = 0;
  this->m_shutdown = true;
  for(size_t i=0; i<this->m_num_threads; ++i)  {
    this->m_threads[i]->join();
    this->m_threads[i].reset();
  }
  this->m_threads.clear();
  for(Buffer& b : this->m_free)
    ::free(b.buffer);
  this->m_free.clear();
  this->m_num_free_buffers = this->m_free.size();
  int32_t sc = this->Component::finalize();

  if ( m_have_stats )   {
    print_statistics();
  }
  return sc;
}

/// Print statistics of the work done
void StorageWriter::print_statistics()   const {
  const char* line = "+----------------------------------------------------------------";
  always(line);
  always("| Overview of monitoring items:");
  always("| Number of events written to output:               %12ld", m_events_OUT);
  always("| Number of events not written and dropped:         %12ld", m_events_DROP);
  always("| Number of bursts submitted to output:             %12ld", m_burstsOUT);
  always("| Number of files opened to write output:           %12ld", m_filesOpened);
  always("| Number of files closed to write output:           %12ld", m_filesClosed);
  always("| Number of writte errors:                          %12ld", m_writeErrors);
  always("| Number of bytes written to output:                %12ld", m_bytesOut);
  always("| Number of bytes non-compressed written to output: %12ld", m_bytesOutUncompressed);
  always("| Number of bytes dropped from output:              %12ld", m_bytesDropped);
  always("| Number of events with a bad header structure:     %12ld", m_badHeader);
  always("| Number of currently active buffers:               %12ld", m_num_active_buffers);
  always("| Number of current todo buffers:                   %12ld", m_num_todo_buffers);
  always("| Number of currently free buffers:                 %12ld", m_num_free_buffers);
  always(line);
  always("| Files successfully written:");
  always(line);
  if ( !m_good_statistics.empty() )   {
    for( const auto& entry : m_good_statistics )   {
      const auto& e = entry.second;
      always("| %-44s Run: %9d  %16s  %9d events %12d bytes ",
	     entry.first.c_str(), e.run,
	     ::lib_rtl_timestr("%Y-%m-%d %H:%M:%S", &e.time),
	     e.events, e.bytes);
    }
    always(line);
  }
  if ( !m_err_statistics.empty() )   {
    always("| Files NOT WRITTEN due to errors:");
    always(line);
    for( const auto& entry : m_err_statistics )   {
      const auto& e = entry.second;
      always("| %-44s Run: %9d  %16s  %9d events %12d bytes ",
	     entry.first.c_str(), e.run,
	     ::lib_rtl_timestr("%Y-%m-%d %H:%M:%S", &e.time),
	     e.events, e.bytes);
    }
    always(line);
  }
}

/// Decode the run-list to determine proper file names for HLT2
int StorageWriter::decode_run_list()  {
  int32_t status = DF_SUCCESS;
  this->m_run_partitions.clear();
  for( const auto& run_part : this->m_run_list )   {
    int32_t run_num = 0;
    auto items = RTL::str_split(run_part,"/");
    if ( items.size() == 2 && 1 == ::sscanf(items[1].c_str(),"%010d",&run_num) )   {
      std::string part = items[0];
      this->m_run_partitions.insert(std::make_pair(run_num, part));
      //
      // We need to update the partition name for runs selected for HLT2 to get the output names correctly.
      // Note:
      // If after a STOP_RUN new runs are selected this will inevitably lead to an inconsistency!
      // This only fixes the problem if runs from the SAME partition are selected!
      //
      this->m_partition_name = part;
      continue;
    }
    this->error("Invalid run-list encountered. Failed to decode. Item: %s", run_part.c_str());
    status = DF_ERROR;
  }
  return status;
}

/// Check if an active buffer can be retired
bool StorageWriter::can_retire_buffer_unlocked(const Buffer& buffer, time_t now)   const    {
  /// Check if event limit is reached
  if ( buffer.num_events >= this->m_max_events )  {
    return true;
  }
  /// Use large timeout before really dropping buffer.
  /// Fast changes during saving are handled in this->get_buffer()
  if ( (now - this->m_last_event_stamp) > this->m_idle_tmo )   {
    return true;
  }
  /// Check if the buffer is from a previous run:
  /// Move to storage after buffer idle timeout
  if ( buffer.run_number < this->m_curr_run && (now - buffer.last_write) > this->m_idle_tmo )  {
    return true;
  }
  return false;
}

/// Aquire fresh buffer to save event data
StorageWriter::Buffer& StorageWriter::get_buffer(uint32_t run, int64_t length)   {
  static Buffer empty;
  std::time_t now;
  {
    if ( run > this->m_curr_run )   {
      this->m_curr_run = run;
    }
    std::lock_guard<std::mutex> bufferLock(this->m_buffer_lock);
    bool erase_buffer = false;
    now = ::time(0);
    do   {
      erase_buffer = false;
      /// Check for outdated buffers from previous runs
      for(auto ib = this->m_active.begin(); ib != this->m_active.end(); ++ib )   {
	/// Check if there is not sufficient buffer space or can be erased
	auto& buffer = (*ib).second;
	erase_buffer =
	  ((buffer.pointer - buffer.buffer) + length >= this->maxBufferSize())
	  || (buffer.num_events+1 > this->m_max_events )
	  || this->can_retire_buffer_unlocked((*ib).second, now);
	if ( erase_buffer )   {
	  this->m_todo.emplace_back(buffer);
	  this->m_active.erase(ib);
	  break;
	}
      }
    }  while ( erase_buffer == true );
    ///
    /// All buffers in the active list are now usable if the run-number fits:
    /// Check if there are already active buffers availible with sufficient space
    for(auto ib = this->m_active.begin(); ib != this->m_active.end(); ++ib )   {
      if ( (*ib).first == run )   {
	auto& buff = (*ib).second;
	if ( (buff.pointer - buff.buffer) + length < this->maxBufferSize() )   {
	  buff.last_write = now;
	  return buff;
	}
      }
    }
    this->m_num_active_buffers = this->m_active.size();
    this->m_num_todo_buffers = this->m_todo.size();
  }
  ///
  /// No buffer availible for writing: Need to poll for buffers in the free list
  while( !this->m_shutdown )   {{
      std::lock_guard<std::mutex> bufferLock(this->m_buffer_lock);
      if ( !this->m_free.empty() )   {
	this->m_active.emplace(run, m_free.back());
	this->m_free.pop_back();
	this->m_num_active_buffers = this->m_active.size();
	this->m_num_free_buffers = this->m_free.size();
	for(auto& b : this->m_active)   {
	  if ( b.first == run )   {
	    b.second.pointer   = b.second.buffer;
	    b.second.last_write = ::time(0);
	    b.second.run_number = run;
	    b.second.num_events = 0;
	    return b.second;
	  }
	}
      }
    }
    if ( this->m_poll_tmo > 0 )  {
      ::lib_rtl_usleep(m_poll_tmo);
    }
    {
      now = ::time(0);
      std::lock_guard<std::mutex> bufferLock(this->m_buffer_lock);
      for(auto ib = this->m_active.begin(); ib != this->m_active.end(); ++ib )   {
	if ( this->can_retire_buffer_unlocked((*ib).second, now)
	     || (((*ib).second.num_events+1) > this->m_max_events) )  {
	  this->m_todo.emplace_back((*ib).second);
	  this->m_active.erase(ib);
	  break;  // Should be enough to move them one-by-one
	}
      }
      this->m_num_active_buffers = this->m_active.size();
      this->m_num_todo_buffers = this->m_todo.size();
    }
  }
  empty = { };
  return empty;
}

/// Check the allowed run number range. If current run is not contained, drop buffer
bool StorageWriter::check_run_range(uint32_t run, bool do_print)   {
  if ( run == 0 || run < this->m_min_allowed_runno || run > this->m_max_allowed_runno )    {
    if ( do_print )   {
      this->error("Execute: Bad run number encountered. Drop buffer: Allowed: [%d,%d] run: %d",
		  this->m_min_allowed_runno, this->m_max_allowed_runno, run);
    }
    return false;
  }
  return true;
}

/// Append data to current buffer. If too small allocate a new buffer
int StorageWriter::save_buffer(uint32_t run, const void* data, int64_t length)   {
  if ( this->maxBufferSize() < length )   {
    this->error("Execute: Failed to allocate buffer [%ld bytes]. "
		"BUFFER SIZE TOO SMALL! Change options!", length);
    ++this->m_events_DROP;
    return DF_ERROR;
  }
  if ( !this->check_run_range(run) )   {
    info("Execute: Failed to save event. Run number out of range: %d", run);
    ++this->m_events_DROP;
    return DF_CONTINUE;
  }
  Buffer& buff = this->get_buffer(run, length);
  
  if ( this->m_shutdown && buff.buffer == nullptr )   {
    info("Execute: Failed to allocate buffer. Drop event. [Shutdown requested]");
    ++this->m_events_DROP;
    return DF_SUCCESS;
  }
  if ( buff.buffer == nullptr )   {
    info("Execute: Failed to allocate buffer. Drop event. [Internal error ?????]");
    ++this->m_events_DROP;
    return DF_CONTINUE;
  }
  this->m_last_event_stamp = buff.last_write;
  ::memcpy(buff.pointer, data, length);
  buff.pointer += length;
  ++buff.num_events;
  ++this->m_events_OUT;
  return DF_SUCCESS;
}

/// Convert PCIE40 MEP to MDF and save it.
int StorageWriter::save_pcie40_as_mdf(const uint8_t* start, int64_t len)   {
  auto* mep_start = (pcie40::mep_header_t*)start;
  auto* mep_end   = pcie40::add_ptr<pcie40::mep_header_t>(start, len);
  pcie40::decoder_t decoder;
  int64_t nev = 0;

  for(const pcie40::mep_header_t *m, *mep = mep_start; mep < mep_end; )   {
    if ( mep->is_valid() )   {
      const pcie40::multi_fragment_t *mfp = mep->multi_fragment(0);
      std::unique_ptr<pcie40::event_collection_t> ev;
      uint16_t packing = mfp->header.packing;
      nev += packing;
      decoder.decode(ev, mep);
      /// Save the events one by one to the buffer
      for( auto* e=ev->begin(); e != ev->end(); e=ev->next(e) )  {
	uint32_t      hdrvsn = 3;
	std::size_t   hdrlen = event_header_t::sizeOf(hdrvsn);
	std::size_t   length = e->total_length();
	const auto* odin = e->bank_collection(0)->at(0);

	if ( odin && odin->data() )   {
	  const auto* sodin  = odin->begin<run3_odin_t>();
	  int32_t curr_run   = sodin->run_number();
	  int32_t curr_orbit = sodin->orbit_id();
	  int32_t curr_bunch = sodin->bunch_id();

	  if ( !this->check_run_range(curr_run) )   {
	    ++this->m_events_DROP;
	    return DF_CONTINUE;
	  }
	  Buffer& buff = this->get_buffer(curr_run, length + hdrlen);
	  if ( this->m_shutdown && buff.buffer == nullptr )   {
	    info("Execute: Failed to allocate buffer. Drop event. [Shutdown requested]");
	    ++this->m_events_DROP;
	    return DF_SUCCESS;
	  }
	  if (buff.buffer == nullptr )   {
	    info("Execute: Failed to allocate buffer. Drop event. [Internal error ?????]");
	    ++this->m_events_DROP;
	    return DF_CONTINUE;
	  }

	  const uint8_t*  b_beg = buff.pointer;
	  event_header_t* hdr = (event_header_t*)buff.pointer;
	  hdr->setChecksum(0);
	  hdr->setCompression(0);
	  hdr->setHeaderVersion(hdrvsn);
	  hdr->setSpare(0);
	  hdr->setDataType(event_header_t::BODY_TYPE_BANKS);
	  hdr->setSubheaderLength(sizeof(event_header_t::Header1));
	  hdr->setSize(length);
	  event_header_t::SubHeader h = hdr->subHeader();
	  h.H1->setTriggerMask(~0u,~0u,~0u,~0u);
	  h.H1->setRunNumber(curr_run);
	  h.H1->setOrbitNumber(curr_orbit);
	  h.H1->setBunchID(curr_bunch);
	  buff.pointer += hdrlen;
	  for( std::size_t i=0, n=e->num_bank_collections(); i<n; ++i)   {
	    buff.pointer = (uint8_t*)e->bank_collection(i)->copy_data(buff.pointer);
	  }

	  if ( buff.pointer-b_beg != hdr->recordSize() )  {
	    this->error("++ Event length inconsistency: %ld <> %ld %ld %ld",
			buff.pointer-b_beg, hdr->recordSize(), length, hdrlen);
	  }
	  /// Set checksum to buffer
	  if ( this->m_checksumType != 0 )   {
	    static int32_t constexpr chkOffset = 4*sizeof(int32_t);
	    const void* chkBegin = lib_rtl_add_ptr(hdr, chkOffset);
	    uint64_t    chkSize  = length + hdrlen - chkOffset;
	    uint32_t    chkSum   = genChecksum(this->m_checksumType, chkBegin, chkSize);
	    hdr->setChecksum(chkSum);
	  }
	  if ( this->m_printChecksum )  {
	    this->always("Generated checksum: %08X", hdr->checkSum());
	  }
	  ++buff.num_events;
	  ++this->m_events_OUT;
	  this->m_bytesOutUncompressed += length;
	  continue;
	}
	++this->m_events_DROP;
      }
      /// Move to the next MEP if any
      if ( m = mep->next(); m > mep )  {
	mep = m;
	continue;
      }
    }
    break;
  }
  return nev > 0 ? DF_SUCCESS : DF_ERROR;
}

/// Save PCIE40 MEP as is
int StorageWriter::save_pcie40_as_mep(const uint8_t* start, int64_t len)   {
  auto* mep_start = (pcie40::mep_header_t*)start;
  auto* mep_end   = pcie40::add_ptr<pcie40::mep_header_t>(start, len);

  /// For each MEP in the frame: Search for the odin bank and dump the MEP frame
  for(const pcie40::mep_header_t *m, *mep = mep_start; mep < mep_end; )   {
    if ( !mep->is_valid() )   {
      /// If we do not have a MEP header: stop processing
      ++this->m_badHeader;
      return DF_SUCCESS;
    }
    std::size_t num_fragments = mep->num_source;
    for( uint32_t i = 0; i < num_fragments; ++i )    {
      const pcie40::multi_fragment_t *mfp  = mep->multi_fragment(i);
      const pcie40::frontend_data_t  *curr = mfp->data();
      const std::size_t align = mfp->header.alignment;
      const uint8_t     *typs = mfp->types();
      const uint16_t    *lens = mfp->sizes();

      for (std::size_t cnt=0, n=mfp->packingFactor(); cnt<n; ++cnt, ++typs, ++lens)  {
	auto typx   = *typs;
	auto length = *lens;
	if ( typx == bank_types_t::ODIN )   {
	  const auto* sodin  = (run3_odin_t*)curr;
	  int32_t run  = sodin->run_number();
	  if ( !this->check_run_range(run) )   {
	    ++this->m_events_DROP;
	    return DF_CONTINUE;
	  }
	  int32_t sc = this->save_buffer(run, mep, len);
	  if ( DF_SUCCESS != sc )   {
	    return DF_SUCCESS;
	  }
	  this->m_bytesOutUncompressed += len;
          curr = nullptr;
	  break;
	}
	/// If we have written *this* mep, break to the outer loop and see if there are other MEPs
	if ( curr == nullptr )   {
	  break;
	}
	curr = curr->next(length, align);
      }
    }
    /// Move to the next MEP if any
    if ( m = mep->next(); m > mep )  {
      mep = m;
      continue;
    }
  }
  return DF_SUCCESS;
}

/// Save MDF frame or MDF burst
int StorageWriter::save_mdf_buffer(const uint8_t* start, int64_t len)   {
  long num_bad_headers = 0;
  /// Auto detect data type: now check for MDF data type
  for( const uint8_t *begin=start, *end=start + len; start < end; )   {
    auto* header = (event_header_t*)start;
    if ( !header->is_mdf() )   {
      this->warning("save_mdf: Encountered invalid MDF header: (%d %d %d). Skip %ld bytes of data.",
		    header->size0(), header->size1(), header->size2(), start-begin);
      ++this->m_badHeader;
      return DF_SUCCESS;
    }
    auto*    hdr    = header->subHeader().H1;
    uint32_t length = header->size0();
    uint32_t run    = hdr->runNumber();
    if ( !this->check_run_range(run) )   {
      start += length;
      ++num_bad_headers;
      ++this->m_badHeader;
      ++this->m_events_DROP;
      this->warning("save_mdf: Encountered not allowed run: %d  ---> SKIP Event.", run);
      continue;
    }
    if ( run > this->m_curr_run )   {
      this->m_curr_run = run;
    }
    if ( this->context.manager.currentRunNumber() != run )   {
      this->context.manager.setRunNumber(run);
    }
    this->m_bytesOutUncompressed += length;
    header->setChecksum(0);
    int32_t sc = 0;
    static int32_t constexpr chkOffset = 4*sizeof(int32_t);
    bool have_compression = header->compression() == 0 && this->m_compressionLevel > 0;
    if ( !m_fileCompression && have_compression )   {
      /// Need to compress the MDF frame
      std::size_t compressed_length = 0;
      std::size_t offset = event_header_t::sizeOf(header->headerVersion());
      mem_buff    buff(length);

      /// First copy the evemt header to the buffer:
      buff.copy(start, offset);
      /// Now append the payload data to the buffer
      sc = DataCompression::compress(m_compressionType,   m_compressionLevel,
				     buff.begin()+offset, length-offset,
				     start+offset,        length-offset,
				     compressed_length);
      /// Only accept if the compression actually helped!
      if ( sc == DF_SUCCESS && (compressed_length < length-offset) )  {
	int32_t cmp      = int32_t(double(length) / double(compressed_length)) - 1;
	int32_t compress = (m_compressionType & 0xF) + (( cmp > 0xF ? 0xF : cmp ) << 4);
	header = buff.begin<event_header_t>();
	header->setSize(compressed_length);
	header->setCompression(compress);
	header->setChecksum(0);
	/// Set checksum to buffer
	if ( this->m_checksumType != 0 )   {
	  const void* chkBegin = lib_rtl_add_ptr(header, chkOffset);
	  uint64_t    chkSize  = compressed_length + offset - chkOffset;
	  uint32_t    chkSum   = genChecksum(this->m_checksumType, chkBegin, chkSize);
	  header->setChecksum(chkSum);
	}
	if ( this->m_printChecksum )  {
	  this->always("Generated checksum: %08X", header->checkSum());
	}
	sc = this->save_buffer(run, buff.begin(), offset+compressed_length);
      }
      else  {
	/// Set checksum to buffer
	if ( this->m_checksumType != 0 )   {
	  const void* chkBegin = lib_rtl_add_ptr(header, chkOffset);
	  uint64_t    chkSize  = length - chkOffset;
	  uint32_t    chkSum   = genChecksum(this->m_checksumType, chkBegin, chkSize);
	  header->setChecksum(chkSum);
	}
	if ( this->m_printChecksum )  {
	  this->always("Generated checksum: %08X", header->checkSum());
	}
	sc = this->save_buffer(run, header, length);
      }
    }
    else   {
      /// Set checksum to buffer
      if ( this->m_checksumType != 0 )   {
	const void* chkBegin = lib_rtl_add_ptr(header, chkOffset);
	uint64_t    chkSize  = length - chkOffset;
	uint32_t    chkSum   = genChecksum(this->m_checksumType, chkBegin, chkSize);
	header->setChecksum(chkSum);
      }
      if ( this->m_printChecksum )  {
	this->always("Generated checksum: %08X", header->checkSum());
      }
      // No compression requested
      sc = this->save_buffer(run, header, length);
    }
    if ( DF_SUCCESS != sc )   {
      this->warning("save_mdf: Dropped MDF event. Failed to save buffer.");
      return DF_SUCCESS;
    }
    start += length;
  }
  if ( num_bad_headers > 0 )   {
    this->warning("save_mdf: Dropped %ld MDF frames due to bad header structure.", num_bad_headers);
  }
  return DF_SUCCESS;
}

/// Data processing overload: Write the data record to disk
int StorageWriter::execute(const Context::EventData& event)  {
  int32_t status = DF_SUCCESS;

  this->m_last_event_stamp = ::time(0);
  /// Extend idle time if there are still events coming
  if ( this->m_cancelled > 0 )  {
    this->m_cancelled = ::time(0);
    ++this->m_events_DROP;
  }
  else if ( this->m_enable_writing )   {
    try  {
      std::size_t len   = event.length;
      auto*       start = (uint8_t*)event.data;

      /// Extend idle time if there are still events coming
      if ( this->m_cancelled > 0 )  {
	this->m_cancelled = ::time(0);
      }

      /// Auto detect data type: first check for PCIE40 MEP format
      auto* mep_hdr = (pcie40::mep_header_t*)start;
      if( mep_hdr->is_valid() )   {
	if ( this->m_mep2mdf )   {
	  status = this->save_pcie40_as_mdf(start, int64_t(mep_hdr->size*sizeof(uint32_t)));
	  if ( status == DF_SUCCESS ) ++this->m_burstsOUT;
	  return status;
	}
	else   {
	  status = this->save_pcie40_as_mep(start, int64_t(mep_hdr->size*sizeof(uint32_t)));
	  if ( status == DF_SUCCESS ) ++this->m_burstsOUT;
	  return status;
	}
      }

      /// Auto detect data type: first check for MDF/BURST format
      auto* mdf_hdr = (event_header_t*)start;
      if ( mdf_hdr->is_mdf() )   {
	status = this->save_mdf_buffer(start, (int64_t)len);
	if ( status == DF_SUCCESS ) ++this->m_burstsOUT;
	return status;
      }
      this->error("Execute: Cannot determine event type. Drop event buffer.");
      return status;
    }
    catch(const std::exception& e)   {
      this->error(e,"Execute: Error processing event.");
    }
    catch(...)  {
      this->error("Execute: UNKOWN error processing event.");
    }
  }
  else   {
    ++this->m_events_DROP;
  }
  return status;
}

/// Construct file name
std::string StorageWriter::makeFileName(int32_t run)    {
  char text[128];
  struct timeval tv;
  std::string file_name = this->m_file_name;
  std::string part = this->m_partition_name;
  ::gettimeofday(&tv, nullptr);
  struct tm *timeinfo = ::localtime(&tv.tv_sec);

  ++this->m_sequence_number;
  if ( !this->m_run_partitions.empty() )    {
    auto irun = this->m_run_partitions.find(run);
    if ( irun != this->m_run_partitions.end() )
      part = (*irun).second;
  }
  ::strftime(text, sizeof(text), "%Y%m%d-%H%M%S-", timeinfo);
  ::snprintf(text+::strlen(text), sizeof(text)-::strlen(text),"%03ld", tv.tv_usec/1000);
  file_name  = RTL::str_replace(file_name,  "${TIME}",      text);
  file_name  = RTL::str_replace(file_name,  "${NODE}",      RTL::nodeNameShort());
  file_name  = RTL::str_replace(file_name,  "${STREAM}",    this->m_stream);
  file_name  = RTL::str_replace(file_name,  "${ACTIVITY}",  this->m_activity);
  file_name  = RTL::str_replace(file_name,  "${PARTITION}", part);
  ::snprintf(text, sizeof(text), "%010d", int32_t((run/1000)*1000));
  file_name  = RTL::str_replace(file_name,  "${RUN1000}",   text);
  ::snprintf(text, sizeof(text), "%010d", run);
  file_name  = RTL::str_replace(file_name,  "${RUN}",       text);
  ::snprintf(text, sizeof(text), "%04ld", m_sequence_number);
  file_name  = RTL::str_replace(file_name,  "${SEQ}",       text);
  ::snprintf(text, sizeof(text), "%06d", ::lib_rtl_pid());
  file_name  = RTL::str_replace(file_name,  "${PID}",       text);
  file_name  = RTL::str_expand_env(file_name);
  return file_name;
}

/// Thread invocation routine to save assembled buffers to the disk server
template <typename OUT_TYPE>
int StorageWriter::process_posix_buffers(std::mutex& queue_lock, 
					 std::map<uint32_t,std::unique_ptr<OUT_TYPE> >& open_files)
{
  auto add_db_entry = [this] (size_t len, const std::string& fname)   {
    if ( this->m_have_file_db )   {
      http::HttpReply  reply;
      try   {
	std::string   url;
	client_t cl(this->m_fdb_version);
	storage::uri_t srv(this->m_server);
	storage::client::reqheaders_t hdrs;
	cl.fdbclient =
	  storage::client::create<storage::client::sync>(srv.host, srv.port, 10000, this->m_debug_client);
	if ( this->m_have_file_db == 2 )   {
	  hdrs.emplace_back(http::constants::location, fname);
	}
	reply = cl.save_object_record(fname, url, len, hdrs);
	if ( reply.status == reply.permanent_redirect || reply.status == reply.continuing )  {
	  /// OK. the registration was now successful
	  return true;
	}
	else   {
	  this->error("posix_buffers: FAILED accessing FDB server: %s",
		      http::HttpReply::stock_status(reply.status).c_str());
	  return false;
	}
      }
      catch(const std::exception& e)  {
	this->error("posix_buffers: Exception accessing FDB server: %s", e.what());
	return false;
      }
      catch(...)  {
	this->error("posix_buffers: UNKNOWN Exception accessing FDB server");
	return false;
      }
    }
    return true;
  };
  auto close_output = [this,add_db_entry](std::unique_ptr<OUT_TYPE>& output, const char* reason, int32_t max_retry)   {
    if ( output.get() && output->isOpen() )   {
      std::lock_guard<std::mutex> file_lock(output->lock);
      if ( output->isOpen() )   {
	std::size_t len = output->length;
	std::string nam = output->name();

	if ( m_have_stats )   {
	  auto& e = m_good_statistics[nam];
	  e.events = output->num_events;
	  e.bytes  = output->length;
	  e.run    = output->run;
	  e.time   = output->last_write;
	}
	output->close();
	this->warning("Closed %s after %ld MB [%s].", nam.c_str(), len/MBYTE, reason);
	++this->m_filesClosed;
	--this->m_filesOpen;
	int32_t  nretry  = max_retry;
	bool status = false;
	do   {
	  status = add_db_entry(len, nam.c_str());
	  if ( !status ) ::lib_rtl_sleep(1000);
	} while ( !status && --nretry > 0 );
	if ( !status )   {
	  this->error("Cannot register %s with %ld MB [%s] (ignored after %d retries).",
		      nam.c_str(), len/MBYTE, reason, max_retry);
	}
	return status;
      }
    }
    output.reset();
    return true;
  };

  std::time_t last_check = ::time(0);
  std::time_t now;
  while( 1 )   {
    // First check buffers: are there any to be retired ?
    Buffer b; {
      std::lock_guard<std::mutex> bufferLock(this->m_buffer_lock);
      now = ::time(0);
      /// Erase all buffers from the active list which need to be saved!
      for( bool erased=false; erased == true; )  {
	erased = false;
	// Check for outdated buffers when being idle:
	for(auto ib = this->m_active.begin(); ib != this->m_active.end(); ++ib )   {
	  bool erase_buffer = this->can_retire_buffer_unlocked(ib->second, now);
	  if ( erase_buffer )   {
	    auto& buff = ib->second;
	    this->m_todo.emplace_back(buff);
	    this->m_active.erase(ib);
	    erased = true;
	    break;
	  }
	}
      }

      this->m_num_todo_buffers   = this->m_todo.size();
      this->m_num_free_buffers   = this->m_free.size();
      this->m_num_active_buffers = this->m_active.size();
      /// Check for exit condition
      if ( this->m_free.size() == this->m_num_buffers && this->m_shutdown )   {
	std::lock_guard<std::mutex> outputLock(queue_lock);
	/// Shutdown (reset) requested. Close output and register file. If not possible -- nothing we can do!
	for(auto& output : open_files )
	  close_output(output.second, "shutdown", 0);
	this->info("process: Exit condition detected. Leaving submit thread.");
	return DF_SUCCESS;
      }
      else if ( !this->m_todo.empty() )   {
	b = this->m_todo.back();
	this->m_todo.pop_back();
      }
      this->m_num_todo_buffers = this->m_todo.size();
    }
    if ( (now - last_check) >= this->m_idle_tmo || 
	 (now - this->m_last_event_stamp) > this->m_idle_tmo )   {
      std::lock_guard<std::mutex> outputLock(queue_lock);
      std::vector<int32_t> to_erase;
      now = ::time(0);
      for(auto& output : open_files )   {
	auto run = output.first;
	if ( this->m_curr_run >= run )   {
	  // if the current run is still the active run AND
	  // there are still events (very slowly) flowing, do not close prematurely
	  // unless the file is nearly full (90%).
	  // This is mainly for HLT2, where accepted events may be very few only....
	  auto& file = output.second;
	  bool close_file = 
	    ((now - file->last_write) > 2*this->m_idle_tmo ) ||
	    ((now - this->m_last_event_stamp) > 2*this->m_idle_tmo);
	  if ( close_file )   {
	    close_output(file, "output-idle", 1);
	    to_erase.emplace_back(run);
	  }
	}
      }
      for(auto run : to_erase)   {
	auto it = open_files.find(run);
	if ( it != open_files.end() ) open_files.erase(it);
      }
      last_check = now;
    }
    if ( !b.buffer )   {
      if ( m_poll_tmo > 0 )  {
	::lib_rtl_usleep(m_poll_tmo);
      }
      continue;
    }
    else if ( b.buffer )   {
      long  len = b.pointer - b.buffer;
      if ( len > 0 )   {
	queue_lock.lock();
	auto& output = open_files[b.run_number];
	if ( output.get() )   {
	  if ( output->length > 0 && output->length + len > this->m_max_file_size_MB * MBYTE )
	    close_output(output, "size-limit", 1);
	  else if ( output->num_events >= this->m_max_events )
	    close_output(output, "event-limit", 1);
	  else if ( (now - output->last_write) > 3*this->m_idle_tmo )
	    close_output(output, "output-idle", 1);
	}
	if ( !output.get() )   {
	  output = std::make_unique<OUT_TYPE>();
	}
	std::string close_reason;
	{  /// New lock scope
	  std::lock_guard<std::mutex> file_lock(output->lock);
	  now = ::time(0);
	  if ( !output->isOpen() )   {
	    std::string fname = this->makeFileName(b.run_number);
	    if ( output->open(fname, this->m_verify_nfs) <= 0 )   {
	      this->error("Failed to open output %s. [%s]", fname.c_str(), RTL::errorString(errno).c_str());
	      this->fireIncident("DAQ_ERROR");
	      return DF_ERROR;
	    }
	    if ( this->m_fileCompression )   {
	      if ( 0 != output->enable_compression(this->m_buffer_size, this->m_compressionLevel) )   {
		this->warning("FAILED to enable file compression for %s", output->name());
	      }
	      this->info("File compression enabled for %s", output->name());
	    }
	    this->warning("Opened %s", output->name());
	    output->run = b.run_number;
	    ++this->m_filesOpened;
	    ++this->m_filesOpen;
	  }
	  output->last_write = now;
	  queue_lock.unlock();

	  long ret = output->write(b.buffer, len);
	  if ( ret == long(len) )    {
	    output->num_events += b.num_events;
	    output->last_write = now;
	    output->length += len;
	    ++output->num_writes;
	    if ( output->num_events >= this->m_max_events )   {
	      close_reason = "event-limit";
	    }
	    std::lock_guard<std::mutex> counterLock(this->m_counter_lock);
	    this->m_bytesOut += len;
	  }
	  else    {
	    this->error("Failed to write buffer. Close file %s [%s]", output->name(), RTL::errorString(errno).c_str());
	    close_reason = "write-failed";
	    std::lock_guard<std::mutex> counterLock(this->m_counter_lock);
	    this->m_bytesDropped += len;
	    ++this->m_writeErrors;
	  }
	}
	if ( !close_reason.empty() )  {
	  close_output(output, close_reason.c_str(), 1);
	}
      }
      {
	std::lock_guard<std::mutex> bufferLock(this->m_buffer_lock);
	b.pointer = b.buffer;
	this->m_free.push_back(b);
	this->m_num_free_buffers = this->m_free.size();
      }
      continue;
    }
  }
  {
    std::lock_guard<std::mutex> outputLock(queue_lock);
    for(auto& output : open_files )
      close_output(output.second, "ending", 0);
  }
  return DF_SUCCESS;
}

/// Thread invocation routine to save assembled buffers to the disk server
int StorageWriter::process_network_buffers()    {
  while( 1 )   {
    Buffer b; {
      std::lock_guard<std::mutex> bufferLock(this->m_buffer_lock);
      if ( this->m_free.size() == this->m_num_buffers && this->m_shutdown )   {
	this->info("process: Exit condition detected. Leaving submit thread.");
	return DF_SUCCESS;
      }
      else if ( !this->m_todo.empty() )   {
	b = this->m_todo.back();
	this->m_todo.pop_back();
      }
      this->m_num_todo_buffers = this->m_todo.size();
    }
    if ( b.buffer )   {
      std::size_t len = b.pointer - b.buffer;
      /// Skip files with less than a minimum bytes
      if ( len > this->m_min_file_size_MB*MBYTE )  {
	if ( this->write_buffer(b) != DF_SUCCESS )   {
	  /// Set the writer into error state and inhibit all further consumption of events
	  std::lock_guard<std::mutex> counterLock(this->m_counter_lock);
	  this->m_bytesDropped += len;
	  ++this->m_writeErrors;
	  this->fireIncident("DAQ_ERROR");
	  this->error("Execute: Failed to write buffer to file. [%s]. Processing will stop.",
		      std::error_condition(errno,std::system_category()).message().c_str());
	  return DF_ERROR;
	}
	std::lock_guard<std::mutex> counterLock(this->m_counter_lock);
	this->m_bytesOut += len;
      }
      else   {
	this->info("Skip mini-file with only %ld bytes",int64_t(len));
	std::lock_guard<std::mutex> counterLock(this->m_counter_lock);
	this->m_bytesDropped += len;
      }
      {
	std::lock_guard<std::mutex> bufferLock(this->m_buffer_lock);
	b.pointer = b.buffer;
	this->m_free.push_back(b);
	this->m_num_free_buffers = this->m_free.size();
      }
      continue;
    }
    if ( this->m_poll_tmo > 0 )  {
      ::lib_rtl_usleep(this->m_poll_tmo);
    }
  }
  return DF_SUCCESS;
}

/// Thread invocation routine to save assembled buffers to the disk server
int StorageWriter::process_buffers()    {
  switch(this->m_output_type_id)  {
  case NETWORK_STORAGE:
    return this->process_network_buffers();
  case ROOT_STORAGE:
    if ( this->m_threadFileQueues )   {
      std::mutex queue_lock;
      std::map<uint32_t,std::unique_ptr<ROOT_FILE> > files;
      return this->process_posix_buffers(queue_lock, files);
    }
    return this->process_posix_buffers(this->m_output_lock,this->m_open_root_files);
  case POSIX_STORAGE:
  default:
    if ( this->m_threadFileQueues )   {
      std::mutex queue_lock;
      std::map<uint32_t,std::unique_ptr<POSIX_FILE> > files;
      return this->process_posix_buffers(queue_lock, files);
    }
    return this->process_posix_buffers(this->m_output_lock,this->m_open_posix_files);
  }
}

/// Print server's HttpReply structure
void StorageWriter::print_reply(const char* prefix, const http::HttpReply& reply)   const  {
  std::string line;
#if 0
  std::stringstream str;
  reply.print(str,"");
  do {
    getline(str, line);
    error("write_buffer-reply<%s>: %s", prefix, line.c_str());
  } while( str.good() && !str.eof() );
#endif
  switch(reply.status)   {
  case http::HttpReply::ok:
  case http::HttpReply::accepted:
  case http::HttpReply::permanent_redirect:
    line = http::HttpReply::stock_status(reply.status);
    this->info("write_buffer-reply<%s>: %s", prefix, line.c_str());
    break;
    
  case http::HttpReply::internal_server_error:
  case http::HttpReply::not_implemented:
  case http::HttpReply::bad_gateway:
  case http::HttpReply::service_unavailable:
  case http::HttpReply::gateway_timeout:
  case http::HttpReply::conflict:
  case http::HttpReply::unauthorized:
  case http::HttpReply::bad_request:
  case http::HttpReply::no_content:
  default:
    line = http::HttpReply::stock_status(reply.status);
    this->error("write_buffer-reply<%s>: %s", prefix, line.c_str());
    break;
  }
}

/// Write multi event buffer to file. Eventually open a new file....
int StorageWriter::write_buffer(const Buffer& buff)    {
  std::string fname       = this->makeFileName(buff.run_number);
  int32_t     num_retries = this->m_write_error_retry;
  std::size_t len         = buff.pointer - buff.buffer;
  double      kB          = double(len)/1024e0;
  bool        process     = (this->m_cancelled == 0) || (::time(0) - this->m_cancelled < this->m_cancel_tmo);
  http::HttpReply  reply;

  if ( fname == NULL_Device )   {
    this->warning("write_buffer: Dump %.2f %cB to **NULL device**. "
		  "If this is not a test immediately call DAQ Piquet!",
		  kB>1e4 ? kB/1024e0 : kB, kB>1e4 ? 'M' : 'k');
    if ( m_have_stats )   {
      auto& e = m_good_statistics[fname];
      e.events = buff.num_events;
      e.bytes  = len;
      e.run    = buff.run_number;
      e.time   = buff.last_write;
    }
    ++this->m_filesOpened;
    ++this->m_filesClosed;
    return DF_SUCCESS;
  }

  /// Implement here a retry mechanism starting with registering the DB record
  while ( process && num_retries > 0 )  {
    try   {
      std::string url, err;
      client_t cl(this->m_fdb_version);
      storage::uri_t srv(this->m_server);
      cl.fdbclient =
	storage::client::create<storage::client::sync>(srv.host, srv.port, 10000, this->m_debug_client);

      reply = cl.save_object_record(fname, url, len);

      if ( reply.status == reply.permanent_redirect )  {
	int32_t num_file_retries = 3;
	/// OK. the registration was now successful. Send the data.
	/// Implement here a retry mechanism sending the data.
	process = (this->m_cancelled == 0) || (::time(0) - this->m_cancelled < this->m_cancel_tmo);
	++this->m_filesOpen;
	++this->m_filesOpened;
	while ( process )  {
	  try   {
	    reply = cl.save_object_data(url, buff.buffer, len);
	    if ( reply.status == reply.created )  {
	      void (Component::*prt)(const char*,...) const = &Component::info;
	      if ( this->m_cancelled > 0 ) prt = &Component::warning;
	      ((*this).*prt)("write_buffer: Saved '%s' %.2f %cB", url.c_str(),
			      kB>1e4 ? kB/1024e0 : kB, kB>1e4 ? 'M' : 'k');
	      ++this->m_filesClosed;
	      if ( m_have_stats )   {
		auto& e = m_good_statistics[fname];
		e.events = buff.num_events;
		e.bytes  = len;
		e.run    = buff.run_number;
		e.time   = buff.last_write;
	      }
	      return DF_SUCCESS;
	    }
	  }
	  catch(const std::exception& e)  {
	    this->error("write_buffer: Exception while sending data: %s",e.what());
	  }
	  catch(...)  {
	    this->error("write_buffer: Exception while sending data: UNKNOWN Exception");
	  }
	  process  = (m_cancelled == 0) || (::time(0) - m_cancelled < m_cancel_tmo);
	  if ( --num_file_retries <= 0 ) process = false;
	  err = http::HttpReply::stock_status(reply.status);
	  this->warning("%s: retry(2) %d / %d [%s]", fname.c_str(),
			this->m_write_error_retry-num_file_retries, this->m_write_error_retry,
			err.substr(0,err.find("\r\n")).c_str());
	  ::lib_rtl_sleep(this->m_write_error_sleep);
	}
      }
      else  {
	this->print_reply("dbase-server", reply);
      }
    }
    catch(const std::exception& e)  {
      this->error("write_buffer: Exception while connecting: %s",e.what());
      reply.status = http::HttpReply::bad_gateway;
    }
    catch(...)  {
      this->error("write_buffer: Exception while connecting: UNKNOWN Exception");
      reply.status = http::HttpReply::bad_gateway;
    }
    process = (this->m_cancelled == 0) || (::time(0) - this->m_cancelled < this->m_cancel_tmo);
    --num_retries;
  }
  if ( m_have_stats )   {
    auto& e = m_err_statistics[fname];
    e.events = buff.num_events;
    e.bytes  = len;
    e.run    = buff.run_number;
    e.time   = buff.last_write;
  }
  this->error("HTTP: %s DATA LOSS!!!! [%d] %s. (1) Need to throw away data to avoid duplicates!",
	      fname.c_str(), int32_t(reply.status), http::HttpReply::stock_status(reply.status).c_str());
  return DF_ERROR;
}

