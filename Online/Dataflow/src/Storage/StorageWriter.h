//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  StorageWriter.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_STORAGEWRITER_H
#define ONLINE_DATAFLOW_STORAGEWRITER_H

// Framework include files
#include <Dataflow/DataflowComponent.h>
#include <Dataflow/DataCompression.h>
#include <Storage/fdb_client.h>

/// C/C++ include files
#include <cstdint>
#include <memory>
#include <limits>
#include <thread>
#include <ctime>
#include <mutex>
#include <map>

// Forward declarations

///  Online namespace declaration
namespace Online  {

  // Forward declarations
  namespace storage   {
    class fdb_client;
  }
  
  /// StorageWriter to group coherent action sequences
  /**
   *
   * @author  Markus Frank
   * @version 1.0
   */
  class StorageWriter : public DataflowComponent  {
  public:
    typedef storage::fdb_client          client_t;
    typedef std::unique_ptr<std::thread> thread_t;

    struct Buffer   {
      /// Pointer to the start of the event buffer
      uint8_t* buffer         { nullptr };
      /// Current pointer inside the event buffer
      uint8_t* pointer        { nullptr };
      /// Associated run number
      uint32_t run_number     { 0 };
      /// Number of accumulated events
      uint32_t num_events     { 0 };
      /// Last access time
      std::time_t last_write  { 0 };
      /// Default constructor
      Buffer() = default;
      /// Used length of the buffer
      std::size_t used_length()  const   { return this->pointer - this->buffer;  }
    };

    struct POSIX_FILE;
    struct ROOT_FILE;
    std::map<uint32_t,std::unique_ptr<POSIX_FILE> > m_open_posix_files  { };
    std::map<uint32_t,std::unique_ptr<ROOT_FILE> >  m_open_root_files   { };

  protected:
    /// Property: FDB server name
    std::string              m_server               { };
    /// Property: Partition name string
    std::string              m_partition_name       { };
    /// Property: Run-type string
    std::string              m_activity             { };
    /// Property: Stream identifier
    std::string              m_stream               { };
    /// File Name Pattern
    std::string              m_file_name            { };
    /// Steer disk type processing (nfs, network, ...)
    std::string              m_output_type          { };
    /// Property: Runlist to determine the input partition by run number
    std::vector<std::string> m_run_list             { };
    /// Property: Buffer size
    int64_t                  m_buffer_size;
    /// Property: Number of parallel event buffers
    std::size_t              m_num_buffers          { 0 };
    /// Property: Number of event sender threads
    std::size_t              m_num_threads          { 0 };
    /// Property: Minimal file size in MBytes
    std::size_t              m_min_file_size_MB     { 0 };
    /// Property: Maximal file size in MBytes (used if writing to posix file system)
    std::size_t              m_max_file_size_MB     { 512 };
    /// Property: Sleep in milliseconds between retries when write connection fails
    int                      m_write_error_sleep    { 0 };
    /// Property: Number of retries when write connection fails
    int                      m_write_error_retry    { 0 };
    /// Property: Poll timeout to detect transfer buffers [microseconds]
    int                      m_poll_tmo             { 100 };
    /// Property: Cancel timeout to empty pending buffers [milli-seconds]
    int                      m_cancel_tmo           { 100 };
    /// Property: Stop timeout to empty pending buffers [milli-seconds]
    int                      m_stop_tmo             { 10000 };
    /// Property: Idle timeout to close potentially open files [seconds]
    int                      m_idle_tmo             { 20 };
    /// Property: Use FDB client when writing POSIX/ROOT
    int                      m_have_file_db         { 0 };
    /// Property: Debug FDB client
    int                      m_debug_client         { 0 };
    /// Property: Have threaded file queues
    int                      m_fdb_version          { 0 };
    /// Property: Enable/disable output to file
    int                      m_enable_writing       { 1 };
    /// Property: Verify that the output directory is NFS/CEPH mounted
    int                      m_verify_nfs           { 1 };
    /// Property: On occurrence of a MEP: convert it automatically to MDF format
    int                      m_mep2mdf              { 1 };
    /// Property: Maximum events allowed per file
    uint32_t                 m_max_events           { std::numeric_limits<int>::max()  };
    /// Property: Sanity checks: Minimal required run-number
    uint32_t                 m_min_allowed_runno    { 0 };
    /// Property: Sanity checks: Minimal allowed run-number
    uint32_t                 m_max_allowed_runno    { std::numeric_limits<uint32_t>::max() };
    /// Property: Compression type. Default: NONE, optional: ZLIB, ZSTD, LZMA, LZ4
    std::string              m_compressionTypeName  { "NONE" };
    /// Property: Compression level. 0: no compression, otherwise definition of the comnpression algorithm
    int                      m_compressionLevel     { 0 };
    /// Property: Flag enable gzip'ed full file compression
    bool                     m_fileCompression      { false };
    /// Property: Checksum algorithm type. 0=No checksum, 1=hash32, 2=crc32, 3=crc16, 4=crc8. 5=adler32, 6=xor
    int                      m_checksumType         { 0 };
    /// Property: Flag to print the checksum to the output
    bool                     m_printChecksum        { false };

    /// Property: Have threaded file queues
    bool                     m_threadFileQueues     { false };
    /// Property: Accumulate statistics and dump at finalize
    bool                     m_have_stats           { false };

    /// Monitoring quantity: Number of events written to output
    long                     m_events_OUT           { 0 };
    /// Monitoring quantity: Number of events not written and dropped
    long                     m_events_DROP          { 0 };
    /// Monitoring quantity: Number of bursts submitted to output
    long                     m_burstsOUT            { 0 };
    /// Monitoring quantity: Number of files currently open to write output
    long                     m_filesOpen            { 0 };
    /// Monitoring quantity: Number of files opened to write output
    long                     m_filesOpened          { 0 };
    /// Monitoring quantity: Number of files closed to write output
    long                     m_filesClosed          { 0 };
    /// Monitoring quantity: Number of writte errors
    long                     m_writeErrors          { 0 };
    /// Monitoring quantity: Number of bytes written to output
    long                     m_bytesOut             { 0 };
    /// Monitoring quantity: Number of uncompressed bytes written to output
    long                     m_bytesOutUncompressed { 0 };
    /// Monitoring quantity: Number of bytes dropped from output
    long                     m_bytesDropped         { 0 };
    /// Monitoring quantity: Number of events with a bad header structure
    long                     m_badHeader            { 0 };
    /// Monitoring quantity: Number of currently active buffers
    std::size_t              m_num_active_buffers   { 0 };
    /// Monitoring quantity: Number of current todo buffers
    std::size_t              m_num_todo_buffers     { 0 };
    /// Monitoring quantity: Number of currently free buffers
    std::size_t              m_num_free_buffers     { 0 };

    /// Buffer handling thread
    std::vector<thread_t>    m_threads;
    /// Mutex to lock the event buffer queues when filling/saving
    std::mutex               m_buffer_lock;
    /// Mutex to lock the output queues when filling/saving
    std::mutex               m_output_lock;
    /// Mutex to lock counters
    std::mutex               m_counter_lock;

    /// Decoded runlist for proper partition names
    std::map<int32_t, std::string> m_run_partitions;
    /// Free buffers to be filled when writing
    std::vector<Buffer>      m_free                 { };
    /// List of filled buffers to be dumped to storage device
    std::vector<Buffer>      m_todo                 { };
    /// Active buffers being filled
    std::map<uint32_t, Buffer> m_active             { };

    DataCompression::compression_alg_t m_compressionType   { DataCompression::NONE };
    uint32_t                 m_curr_run             { 0 };
    std::size_t              m_sequence_number      { 0 };
    time_t                   m_last_event_stamp     { 0 };

    enum output_type_t  {
      NETWORK_STORAGE = 1,
      POSIX_STORAGE = 2,
      ROOT_STORAGE = 3
    };
    /// Flag with preprocessed output type
    output_type_t            m_output_type_id       { NETWORK_STORAGE };
    /// Flag to detect cancellation in the event processing thread
    time_t                   m_cancelled            { 0 };
    /// Flag to indicate the ongoing shutdown process
    bool                     m_shutdown             { false };

    /// Fiule based statistics information
    struct FileInfo   {
      std::size_t events      { 0 };
      std::size_t bytes       { 0 };
      uint32_t    run         { 0 };
      std::time_t time        { 0 };
    };
    /// Data output statistics (only written if property HaveStats is set).
    std::map<std::string, FileInfo> m_good_statistics { };
    std::map<std::string, FileInfo> m_err_statistics  { };
  protected:
    /// Print statistics of the work done
    void print_statistics()  const;
    /// Decode the run-list to determine proper file names for HLT2
    int decode_run_list();
    /// Construct file name
    std::string makeFileName(int run);
    /// Check the allowed run number range. If current run is not contained, drop buffer
    bool check_run_range(uint32_t run, bool do_print_msg=true);

    /// Thread entry routine to process buffers
    int         process_buffers();
    int         process_network_buffers();
    template <typename OUT_TYPE> 
      int process_posix_buffers(std::mutex& queue_lock, 
				std::map<uint32_t,std::unique_ptr<OUT_TYPE> >& open_files);

    /// Get current buffer. If empty check for new one...
    Buffer&     get_buffer(uint32_t run, int64_t length);
    /// Append data to current buffer. If too small allocate a new buffer
    int         save_buffer(uint32_t run, const void* data, int64_t length);
    /// Convert PCIE40 MEP to MDF and save it.
    int         save_pcie40_as_mdf(const uint8_t* start, int64_t length);
    /// Save PCIE40 MEP as is
    int         save_pcie40_as_mep(const uint8_t* start, int64_t length);
    /// Save MDF frame or MDF burst
    int         save_mdf_buffer(const uint8_t* start, int64_t length);

    /// Maximum available buffer size
    int64_t     maxBufferSize() const        { return this->m_buffer_size; }
    /// Write multi event buffer to file. Eventually open a new file....
    int         write_buffer(const Buffer& buffer);
    /// Print server's HttpReply structure
    void        print_reply(const char* prefix, const http::HttpReply& reply)   const;

    bool        can_retire_buffer_unlocked(const Buffer& buffer, time_t now)   const;

  public:
    /// Initializing constructor
    StorageWriter(const std::string& name, Context& ctxt);
    /// Default destructor
    virtual ~StorageWriter();
    /// Initialize the data flow component. Default implementation is empty.
    virtual int initialize()  override;
    /// Start the data flow component. Default implementation is empty.
    virtual int start()  override;
    /// Stop the data flow component. Default implementation is empty.
    virtual int stop()  override;
    /// Finalize the data flow component. Default implementation is empty.
    virtual int finalize()  override;
    /// Cancel the data flow component. 
    virtual int cancel()  override;
    /// Data processing overload: process event
    virtual int execute(const Context::EventData& event)  override;
  };
}      // end namespace Online
#endif // ONLINE_DATAFLOW_STORAGEWRITER_H
