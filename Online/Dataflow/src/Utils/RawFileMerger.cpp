//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  Filemerger.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

#include "RawFileMerger.h"

// Framework include files
#include <PCIE40Data/pcie40encoder.h>
#include <EventData/bank_header_t.h>
#include <EventData/event_header_t.h>
#include <EventData/RawFile.h>

#include <RTL/rtl.h>
#include <RZip.h>

// C/C++ include files
#include <system_error>
#include <filesystem>
#include <stdexcept>
#include <iostream>
#include <random>
#include <cmath>
#include <ctime>
#include <iomanip>

namespace pcie40 = Online::pcie40;
using namespace Online;
using namespace std;

namespace {

  class file_writer : public pcie40::encoder_t::output_t    {
    Online::RawFile* file = 0;

  public:
    file_writer(Online::RawFile* f) : file(f) {}
    virtual size_t output(const void* buffer, size_t len)  override
    {  return file->write(buffer, len);  }
  };

  /// Decompress opaque data buffer
  bool decompressBuffer( int algtype, unsigned char* tar, size_t tar_len, 
			 const unsigned char* src, size_t src_len,
			 size_t& new_len ) {
    int in_len, out_len, res_len = 0;
    switch ( algtype ) {
    case 0:
      if ( tar != src && tar_len >= src_len ) {
	new_len = src_len;
	::memcpy( tar, src, src_len );
	return true;
      }
      break;
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
    case 8:
    case 9:
      in_len  = src_len;
      out_len = tar_len;
      ::R__unzip( &in_len, (unsigned char*)src, &out_len, tar, &res_len);
      if ( res_len > 0 ) {
	new_len = res_len;
	return true;
      }
      break;
    default:
      break;
    }
    return false;
  }
}

class RawFileMerger::event_output : public RawFile   {
  using RawFile::RawFile;
};

class RawFileMerger::event_mixer  {
public:
  std::default_random_engine             generator;
  std::uniform_real_distribution<double> distribution {0.0,1.0};
  std::vector<std::string> inputs, signals;
  Online::RawFile          input, signal;
  double                   fraction = 0e0;
  size_t                   num_evt = 0, num_sig = 0;

public:
  /// Default constructor
  event_mixer() = default;
  /// Default destructor
  ~event_mixer() = default;
  /// Check if input data are still present
  bool done()  const   {
    if ( fraction > 0e0 && !signal.isOpen() && signals.empty() ) return true;
    if ( !input.isOpen() && inputs.empty() ) return true;
    return false;
  }
  /// Generic buffer read
  long  read(std::vector<std::string>& names, Online::RawFile& file, unsigned char* buff, size_t len)  {
    auto expected = Online::RawFile::MDF_INPUT_TYPE;
    auto found    = Online::RawFile::AUTO_INPUT_TYPE;
    while ( !file.isOpen() && !names.empty() )   {
      auto nam = names.back();
      file = Online::RawFile(nam);
      names.pop_back();
      if ( file.open() != -1 ) break;
      std::cout << " ...skipping ..." << std::endl;
    }
    if ( !file.isOpen() ) return -1;
    return file.read_event(expected, found, buff, len);
  }
  /// Read buffer with event mix
  long read(unsigned char* buff, size_t len)  {
    bool sig = (fraction > 0e0) && (distribution(generator) < fraction);
    sig ? ++num_sig : ++num_evt;
    return sig
      ? this->read(signals, signal, buff, len)
      : this->read(inputs, input, buff, len);
  }
};

/// Initializing constructor
RawFileMerger::RawFileMerger(const char* output_name, OutputType output_type)
  : outputName(output_name), outputType(output_type)
{
  this->mixer = make_unique<event_mixer>();
}

/// Default destructor
RawFileMerger::~RawFileMerger()   {
}

/// Open new output file
int RawFileMerger::open_output()    {
  string fname = this->outputName;
  if ( write_many )    {
    stringstream str;
    str << this->outputName << "." << this->out_seq_no;
    ++this->out_seq_no;
    fname = str.str();
  }
  this->output = make_unique<event_output>(fname);
  this->output->openWrite(false);
  if ( this->output->isOpen() )   {
    return 1;
  }
  cout << "Output file: " << this->output->name() << " cannot be opened [" << RTL::errorString() << "]" << endl;
  return 0;
}

/// Start merge cycle: open output file
int RawFileMerger::begin()   {
  return this->begin(this->outputName.c_str(), this->outputType);
}

/// Start merge cycle: open output file
int RawFileMerger::begin(const char* output_name, OutputType output_type)   {
  this->mixer = make_unique<event_mixer>();
  this->out_seq_no = 0;
  if ( !this->output->isOpen() )    {
    this->outputName = output_name;
    this->outputType = output_type;
    return this->open_output();
  }
  cout << "Output file: " << this->output->name() << " already open. Instruction forbidden." << endl;
  return 0;
}

/// End the file merging cycle and close the output file
int RawFileMerger::end()   {
  if ( this->output->isOpen() )   {
    size_t bytes = this->output->data_size();
    this->output->reset();
    cout << "Output file " << this->output->name() << " closed after " << bytes << " bytes." << endl;
    return 1;
  }
  cout << "Output file NOT open. Instruction forbidden." << endl;
  return 0;
}

/// Add new input source to the merger object
size_t RawFileMerger::add(const char* file_name)   {
  namespace fs = std::filesystem;
  fs::path path(file_name);
  error_code ec;
  
  if ( !fs::exists(path, ec) )   {
    cout << "Input " << file_name << " is not accessible: " << ec.message() << endl;
    return 0;
  }
  else if ( fs::is_directory(path, ec) )   {
    for(auto const& dir : std::filesystem::directory_iterator{path})   {
      fs::path dir_path(dir);
      if ( dir_path.string() == "." )
	continue;
      if ( dir_path.string() == ".." )
	continue;

      fs::path p = fs::absolute(dir_path, ec);
      /// Resolve symbolic links if necessary
      if ( fs::is_symlink(path) )
	path = fs::absolute(fs::read_symlink(path, ec), ec);

      /// Check for directories: if required recurse
      if ( fs::is_directory(path, ec) )   {
	if ( fs::is_empty(path, ec) )
	  continue;
	// If we would like to add whole trees, enable this...
	// this->add(path.c_str());
	continue;
      }
      this->mixer->inputs.emplace_back(path);
    }
    cout << "Scanned directory " << file_name
	 << ": " << this->mixer->inputs.size() << " files." << endl;
    return this->mixer->inputs.size();
  }
  cout << " --> Add file entry: " << file_name << endl;
  this->mixer->inputs.emplace_back(file_name);
  return this->mixer->inputs.size();
}

/// Merge a maximum of 'max_evts' events to the output file
int RawFileMerger::merge(size_t max_evts)   {
  if ( this->output->isOpen() )   {
    if ( this->outputType == PCIE40 )   {
      return merge_pcie40(max_evts);
    }
    return merge_tell1(max_evts);
  }
  cout << "Output file NOT open. Instruction forbidden." << endl;
  return 0;
}

/// Merge a maximum of 'max_evts' events to the output file in TELL1 format
int RawFileMerger::merge_tell1(size_t max_evts)   {
  size_t      num_bytes = 0;
  size_t      decomp_len = 0;
  size_t      buff_len = 10*1024*1024;
  uint8_t*    data_ptr = nullptr;
  unique_ptr<uint8_t []> decompressed_buffer {};
  unique_ptr<uint8_t []> compressed_buffer(new uint8_t[buff_len]);

  for (size_t events = max_evts; events; --events )    {
    long data_len = this->mixer->read(compressed_buffer.get(), buff_len);
    if ( data_len > 0 )   {
      event_header_t* hdr = (event_header_t*)compressed_buffer.get();
      size_t   hdr_len = hdr->sizeOf(hdr->headerVersion());
      int  compression = hdr->compression();
      data_ptr = compressed_buffer.get();
      if ( compression > 0 )   {
	size_t expand     = (hdr->compression() >> 4) + 1;
	size_t alloc_len  = hdr_len + (expand+1) * hdr->size();
	size_t new_length = 0;
	if ( decomp_len < alloc_len )  {
	  decompressed_buffer.reset(new uint8_t[decomp_len=1.5*alloc_len]);
	}
	if ( decompressBuffer(compression,
			      decompressed_buffer.get() + hdr_len, decomp_len-hdr_len, 
			      compressed_buffer.get()   + hdr_len, hdr->size(), new_length) )   {
	  data_ptr = decompressed_buffer.get();
	  hdr = (event_header_t*)data_ptr;
	  hdr->setHeaderVersion( 3 );
	  hdr->setDataType(event_header_t::BODY_TYPE_BANKS);
	  hdr->setSubheaderLength(sizeof(event_header_t::Header1));
	  hdr->setSize(new_length);
	  hdr->setCompression(0);
	  hdr->setChecksum(0);
	  hdr->setSpare(0);
	  data_len = hdr_len + hdr->size();
	}
	else   {
	  continue;
	}
      }
      long len = this->output->write(data_ptr, data_len);
      if ( len != data_len )   {
	cout << "++ Failed to write output record to file: " << this->output->name() << endl;
	break;
      }
      num_bytes += len;
      if ( num_bytes >= this->max_file_size )   {
	if ( write_many )    {
	  this->output->reset();
	  if ( !this->open_output() ) break;
	  num_bytes = 0;
	}
	else   {
	  break;
	}
      }
    }
  }
  cout << "++ Wrote" << setw(5) << int(num_bytes/1024/1024) << " MB "
       << " to " << this->output->name()
       << endl;
  return 1;
}

/// Merge a maximum of 'max_evts' events to the output file in PCIE40 format
int RawFileMerger::merge_pcie40(size_t max_evts)   {
  pcie40::encoder_t encoder;
  pcie40::encoder_t::tae_options tae_opts {0, 0e0};
  file_writer writer {this->output.get()};
  size_t      eid = 0;
  size_t      num_bytes = 0;
  size_t      decomp_len = 0;
  size_t      buff_len = 10*1024*1024;
  uint8_t*    data_ptr = nullptr;
  unique_ptr<uint8_t []> decompressed_buffer {};
  unique_ptr<uint8_t []> compressed_buffer(new uint8_t[buff_len]);

  for (size_t events = max_evts; events; --events )    {
    long ret = this->mixer->read(compressed_buffer.get(), buff_len);
    if ( ret > 0 )   {
      event_header_t* hdr = (event_header_t*)compressed_buffer.get();
      size_t   hdr_len = hdr->sizeOf(hdr->headerVersion());
      int  compression = hdr->compression();
      uint8_t* start   = compressed_buffer.get() + hdr_len;
      data_ptr = compressed_buffer.get();
      if ( compression > 0 )   {
	size_t expand     = (hdr->compression() >> 4) + 1;
	size_t alloc_len  = hdr_len + (expand+1) * hdr->size();
	size_t new_length = 0;
	if ( decomp_len < alloc_len )  {
	  decompressed_buffer.reset(new uint8_t[decomp_len=1.5*alloc_len]);
	}
	if ( decompressBuffer(compression,
			      decompressed_buffer.get() + hdr_len, decomp_len-hdr_len, 
			      compressed_buffer.get()   + hdr_len, hdr->size(), new_length) )   {
	  data_ptr = decompressed_buffer.get();
	  hdr = (event_header_t*)data_ptr;
	  hdr->setHeaderVersion( 3 );
	  hdr->setDataType(event_header_t::BODY_TYPE_BANKS);
	  hdr->setSubheaderLength(sizeof(event_header_t::Header1));
	  hdr->setSize(new_length);
	  hdr->setCompression(0);
	  hdr->setChecksum(0);
	  hdr->setSpare(0);
	  start = data_ptr + hdr_len;
	  ret   = hdr_len + hdr->size();
	}
	else   {
	  continue;
	}
      }
      encoder.append(++eid, start, ret - hdr_len);
      if ( encoder.packingFactor() == this->packing )  {
	auto written = encoder.write_record(writer, tae_opts);
	encoder.reset();
	num_bytes += written.second;
	cout << "++ Wrote"      << this->output->name() << endl
	     << "   Sources: "    << written.first
	     << " Filesize:"    << setw(5) << int(num_bytes/1024/1024) << " MB "
	     << " Packing: "    << this->packing
	     << " Background:"  << setw(7) << this->mixer->num_evt
	     << " Signal:"      << setw(7) << this->mixer->num_sig
	     << endl;
	if ( num_bytes >= this->max_file_size )   {
	  if ( write_many )    {
	    this->output->reset();
	    if ( !this->open_output() ) break;
	    writer    = file_writer{this->output.get()};
	    num_bytes = 0;
	  }
	  else   {
	    break;
	  }
	}
      }
      continue;
    }
    if ( this->mixer->done() ) break;
  }
  return 1;
}

