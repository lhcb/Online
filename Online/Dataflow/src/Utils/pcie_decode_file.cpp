//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include <PCIE40Data/pcie40decoder.h>
#include <EventData/raw_bank_online_t.h>
#include <EventData/event_header_t.h>
#include <EventData/RawFile.h>
#include <EventData/odin_t.h>
#include <CPP/mem_buff.h>
#include <RTL/strdef.h>
#include <RTL/rtl.h>

// C/C++ include files
#include <filesystem>
#include <stdexcept>
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <cstring>
#include <cerrno>
#include <cstdio>
#include <vector>
#include <set>

namespace {
  const char* get_arg(const char* arg)   {
    const char* p = ::strchr(arg,'=');
    if ( p ) return p+1;
    throw std::runtime_error(std::string("Invalid argument: ")+arg);
  }

  bool prompt()   {
  re_check:
    std::cout << "====>  Continue to dump the next event  [q,Q to quit]: " << std::flush;
  re_prompt:
    int c = std::getchar();
    if ( c == -1  ) goto re_prompt;
    if ( c == 10  ) return true;  // <ENTER>
    if ( c == 'q' ) return false;
    if ( c == 'Q' ) return false;
    goto re_check;
  }

  long write_mdf_file(Online::RawFile& output, const Online::pcie40::event_collection_t& coll, long max_events=std::numeric_limits<long>::max())   {
    using namespace Online;
    mem_buff buffer;
    long     num_events = 0;

    for(const auto* e=coll.begin(); e != coll.end(); e = coll.next(e))   {
      if ( num_events >= max_events )   {
	::printf("+++ Event limit for output file %s reached.\n", output.name().c_str());
	break;
      }
      else  {
	const auto odin = e->get_odin_bank();
	const auto hlt1 = e->get_bank(bank_types_t::HltRoutingBits);
	std::size_t hdr_len  = event_header_t::sizeOf(3);
	std::size_t data_len = e->total_length();

	buffer.allocate(data_len+hdr_len);
	::memset(buffer.ptr(), 0, hdr_len);
	buffer.set_cursor(hdr_len);
	e->copy_data(buffer.ptr(), 0xCBCB);
	buffer.set_cursor(hdr_len+data_len);

	event_header_t* header = buffer.begin<event_header_t>();
	header->setHeaderVersion(event_header_t::CURRENT_VERSION);
	header->setSpare(0);
	header->setChecksum(0);
	header->setCompression(0);
	header->setSize(data_len);
	header->setDataType(event_header_t::BODY_TYPE_BANKS);
	header->setSubheaderLength(sizeof(event_header_t::Header1));

	auto* sub_hdr = header->subHeader().H1;
	if ( !odin.first )   {
	  sub_hdr->setRunNumber(0);
	  sub_hdr->setOrbitNumber(0);
	  sub_hdr->setBunchID(0);
	}
	else if ( odin.first->version() < 7 )   {
	  const auto* o = reinterpret_cast<const run2_odin_t*>(odin.second);
	  sub_hdr->setRunNumber(o->run_number());
	  sub_hdr->setOrbitNumber(o->orbit_id());
	  sub_hdr->setBunchID(o->bunch_id());
	}
	else    {
	  const auto* o = reinterpret_cast<const run3_odin_t*>(odin.second);
	  sub_hdr->setRunNumber(o->run_number());
	  sub_hdr->setOrbitNumber(o->orbit_id());
	  sub_hdr->setBunchID(o->bunch_id());
	}
	const uint32_t mask[] = { ~0x0U, ~0x0U, ~0x0U, ~0x0U };
	sub_hdr->setTriggerMask(hlt1.first ? reinterpret_cast<const uint32_t*>(hlt1.second) : mask);
	if ( std::size_t(output.write(buffer.begin(), buffer.used())) != buffer.used() )   {
	  auto err = RTL::errorString();
	  ::printf("+++ FAILED to write event to MDF file: %s [%s]\n",
		   output.name().c_str(), err.c_str());
	  ::exit(errno);
	}
	++num_events;
      }
    }
    return num_events;
  }
}

extern "C" int pcie_decode_file(int argc, char* argv[])    {
  namespace fs = std::filesystem;
  namespace pcie40 = Online::pcie40;

  class help  {
  public:
    static void show()  {
      std::cout <<
	"Usage: pcie_decode_file -option [-option]                           \n"
	"  --input=<file>      input file name  (multiple entries allowed)   \n"
	"  -i <file>           dto.                                          \n"
	"  --directory=<file>  use directory for input files                 \n"
	"  -D <file>           dto.                                          \n"
	"  --output=<file>     Output MDF: file name. Default: no output     \n"
	"  -o <file>           dto.                                          \n"
	"  --dump=mep          Dump MEP headers                              \n"
	"  --dump=mfp          Dump MEP headers                              \n"
	"  --dump=events       Dump Event headers                            \n"
	"  --dump=collections  Dump bank collection summaries                \n"
	"  --dump=banks        Dump bank headers                             \n"
	"  --dump=summary      Dump summary after processing                 \n"
	"  -d <item>           dto.                                          \n"
	"  --break             Break before next chunk of printout           \n"
	"  -b                  dto.                                          \n"
	"  --events=<number>   Number of events to dump.                     \n"
	"  -e <number>         dto.                                          \n"
	"  --no-measurements   No measurement printout.                      \n"
	"  -n                  dto.                                          \n"
		<< std::endl;
    }
  };
  char *endptr;
  long num_events = -1;
  bool measure  = true;
  bool do_break = false;
  bool dump_mep = false;
  bool dump_mfp = false;
  bool dump_events = false;
  bool dump_summary = false;
  bool dump_bank_headers = false;
  bool dump_bank_collections = false;
  std::string output_file;
  std::vector<std::string> inputs;
  std::vector<std::string> directories;
  try   {
    for(int i = 1; i < argc && argv[i]; ++i)  {
      if ( 0 == ::strncasecmp("--input",argv[i],4) )   {
	inputs.push_back(get_arg(argv[i]));
      }
      else if ( 0 == ::strcasecmp("-i",argv[i]) )   {
	inputs.push_back(argv[++i]);
      }
      else if ( 0 == ::strncasecmp("--directory",argv[i],4) )   {
	directories.push_back(get_arg(argv[i]));
      }
      else if ( 0 == ::strcmp("-D",argv[i]) )   {
	directories.push_back(argv[++i]);
      }
      else if ( 0 == ::strncasecmp("--break",argv[i],4) )   {
	do_break = true;
      }
      else if ( 0 == ::strcasecmp("-b",argv[i]) )   {
	do_break = true;
      }
      else if ( 0 == ::strncasecmp("--dump",argv[i],4) )    {
	if ( ::strncmp(get_arg(argv[i]), "mep", 3) == 0 )   {
	  dump_summary = true;
	  dump_mep = true;
	}
	else if ( ::strncmp(get_arg(argv[i]), "mfp", 3) == 0 )   {
	  dump_summary = true;
	  dump_mfp = true;
	  dump_mep = true;
	}
	else if ( ::strncmp(get_arg(argv[i]), "summary", 3) == 0 )   {
	  dump_summary = true;
	}
	else if ( ::strncmp(get_arg(argv[i]), "events", 3) == 0 )   {
	  dump_summary = true;
	  dump_events = true;
	  dump_mep = true;
	}
	else if ( ::strncmp(get_arg(argv[i]), "collections", 3) == 0 )   {
	  dump_bank_collections = true;
	  dump_summary = true;
	  dump_events = true;
	  dump_mep = true;
	}
	else if ( ::strncmp(get_arg(argv[i]), "banks", 3) == 0 )   {
	  dump_bank_collections = true;
	  dump_bank_headers = true;
	  dump_summary = true;
	  dump_events = true;
	  dump_mep = true;
	}
      }
      else if ( 0 == ::strncasecmp("-d",argv[i], 4) )  {
	const char* a2 = argv[++i];
	if ( ::strncmp(a2, "mep", 3) == 0 )   {
	  dump_summary = true;
	  dump_mep = true;
	}
	else if ( ::strncmp(a2, "summary", 3) == 0 )   {
	  dump_summary = true;
	}
	else if ( ::strncmp(a2, "mfp", 3) == 0 )   {
	  dump_summary = true;
	  dump_mfp = true;
	  dump_mep = true;
	}
	else if ( ::strncmp(a2, "events", 3) == 0 )   {
	  dump_summary = true;
	  dump_events = true;
	  dump_mep = true;
	}
	else if ( ::strncmp(a2, "collections", 3) == 0 )   {
	  dump_summary = true;
	  dump_bank_collections = true;
	  dump_events = true;
	  dump_mep = true;
	}
	else if ( ::strncmp(a2, "banks", 3) == 0 )   {
	  dump_summary = true;
	  dump_bank_collections = true;
	  dump_bank_headers = true;
	  dump_events = true;
	  dump_mep = true;
	}
      }
      else if ( 0 == ::strncasecmp("--events",argv[i],7) )    {
	num_events = ::strtol(get_arg(argv[i]), &endptr, 10);
      }
      else if ( 0 == ::strncasecmp("-e",argv[i],2) )  {
	num_events = ::strtol(argv[++i], &endptr, 10);
      }
      else if ( 0 == ::strncasecmp("--no-measurements",argv[i],8) )   {
	measure = false;
      }
      else if ( 0 == ::strcasecmp("-n",argv[i]) )   {
	measure = false;
      }
      else if ( 0 == ::strncasecmp("--output",argv[i],7) )    {
        output_file = get_arg(argv[i]);
      }
      else if ( 0 == ::strncasecmp("-o",argv[i],2) )  {
	output_file = argv[++i];
      }
      else   {
	help::show();
	throw std::runtime_error(std::string("Invalid argument: ")+argv[i]);
      }
    }
  }
  catch(const std::exception& e)   {
    std::cout << "Exception: " << e.what() << std::endl << std::endl;
    help::show();
    return EINVAL;
  }

  if ( !directories.empty() )   {
    for( const auto& dir : directories )   {
      std::error_code ec;
      fs::path path = RTL::str_expand_env(dir);
      if ( !fs::exists(path, ec) || !fs::is_directory(path, ec) )   {
	std::cout << "+++ Ignore directory: " << path.string() 
		  << " [" << ec.message() << "]" << std::endl;
	continue;
      }
      for(auto it = fs::directory_iterator(path); it != fs::directory_iterator(); ++it)   {
	fs::path e = *it;
	if ( fs::exists(e, ec) )   {
	  inputs.push_back(e.string());
	  continue;
	}
	std::cout << "+++ Ignore directory entry: " << e.string() 
		  << " [" << ec.message() << "]" << std::endl;
      }
    }
  }

  if ( inputs.empty() )   {
    std::cout << "No input files given!" << std::endl << std::endl;
    help::show();
    return ENOENT;
  }

  pcie40::printer_t printer;
  pcie40::decoder_t decoder;
  std::unique_ptr<pcie40::event_collection_t> coll;

  clock_t start = clock();
  clock_t end   = start;
  std::size_t    n_evt_total = 0;
  std::size_t    n_evt       = 0;
  std::size_t    n_mep       = 0;
  std::size_t    bufflen     = 0;
  unsigned char* buff        = nullptr;
  std::unique_ptr<Online::RawFile> output;
  long num_mdf_towrite = num_events > 0 ? num_events : std::numeric_limits<long>::max();
  long num_mdf_output  = num_events > 0 ? num_events : std::numeric_limits<long>::max();

  if ( !output_file.empty() )   {
    output_file = RTL::str_expand_env(output_file);
    output = std::make_unique<Online::RawFile>(output_file);
    if ( -1 == output->openWrite(false) )   {
      return errno;
    }
  }

  for(std::size_t i=0; i< inputs.size(); ++i)   {
    std::size_t total = 0;
    std::error_code ec;
    fs::path path = RTL::str_expand_env(inputs[i]);
    if ( !fs::exists(path, ec) )   {
      std::cout << "+++ Ignore input: " << path.string() 
		<< " [" << ec.message() << "]" << std::endl;
      continue;
    }
    Online::RawFile input(path.string());
    if ( input.open() < 0 )  {
      std::cout << "FAILED to open file: " << inputs[i] << std::endl;
      continue;
    }
    std::cout << "+++ Successfully opened file: " << inputs[i] << std::endl;
    while(1)   {
      pcie40::mep_header_t header;
      std::size_t len = input.read(&header, sizeof(header));
      if ( len == sizeof(header) )   {
	std::size_t rec_len = header.size*sizeof(uint32_t);
	total += len;
	if ( bufflen < rec_len + sizeof(header) )  {
	  bufflen = rec_len + sizeof(header);
	  if ( buff ) delete [] buff;
	  buff = new unsigned char[bufflen];
	}
	::memcpy(buff, &header, sizeof(header));
	len = input.read(buff + sizeof(header), rec_len - sizeof(header));
	if ( len == rec_len - sizeof(header) )   {
	  char text[512];
	  total += len;
	  const pcie40::mep_header_t* mep = (pcie40::mep_header_t*)buff;
	  if ( !mep->is_valid() )   {
	    break;
	  }
	  else   {
	    const pcie40::multi_fragment_t *mfp = mep->multi_fragment(0);
	    uint16_t packing = mfp->header.packing;
	    n_evt += packing;
	    if ( dump_events || (dump_mep&&dump_mfp) )   {
	      std::cout << "+=========================================================================+" << std::endl;
	    }
	    if ( dump_mep )   {
	      ::snprintf(text, sizeof(text),
			 "|   Reading mep[%04ld] at %p length:%7d end: %p  packing:%8d  sources:%d magic:%04X total events:%7ld", 
			 n_mep, (void*)mep, mep->size, (void*)mep->next(), int(packing), 
			 int(mep->num_source), int(mep->magic), n_evt);
	      std::cout << text << std::endl;
	    }
	  }
	  if ( dump_mfp )   {
	    std::size_t num_fragments = mep->num_source;
	    for( uint32_t imfp = 0; imfp < num_fragments; ++imfp )    {
	      const auto* mfp = mep->multi_fragment(imfp);
	      const auto& hdr = mfp->header;
	      const char* tag = mfp->is_valid() ? "" : "CORRUPTED ";
	      ::snprintf(text, sizeof(text),
			 "|   -> %sMFP[%4d]  [%p,%p] magic:%04X "
			 "pack:%5d eid:%8ld srcid:%5d len:%8d vsn:%d align:%d",
			 tag, imfp, (void*)mfp, pcie40::add_ptr<void>(mfp,hdr.size),
			 int(hdr.magic), int(hdr.packing), long(hdr.event_id),
			 int(hdr.source_id), int(hdr.size), int(hdr.version),
			 int(hdr.alignment));
	      std::cout << text << std::endl;
	    }
	  }
	  coll.reset();

	  //   coll_reset ? coll.reset() : (void)decoder.initialize(coll, packing);
	  decoder.decode(coll, mep);
	  if ( output && num_mdf_towrite > 0 )   {
	    long written = write_mdf_file(*output, *coll, num_mdf_towrite);
	    num_mdf_towrite -= written;
	  }
	  if ( !dump_events )    {
	    num_events += coll->size();
	  }
	  else  {
	    std::size_t count = 0;
	    if ( dump_mep )   {
	      std::cout << "+ Got MEP with " << coll->size() << " events." << std::endl;
	    }
	    for(const auto* e=coll->begin(); e != coll->end(); e = coll->next(e), ++count)   {
	      if ( dump_bank_collections )   {
		std::cout << "+ ==>  ===================================================================+" << std::endl;
	      }
	      std::size_t normal_banks_num  = 0;
	      std::size_t special_banks_num = 0;
	      std::size_t normal_data_len   = 0;
	      std::size_t special_data_len  = 0;

	      ++n_evt_total;
	      for(std::size_t ie=0, ne=e->num_bank_collections(); ie<ne; ++ie)   {
		std::map<int,std::size_t> normal_types, special_types;
		const auto* bc = e->bank_collection(ie);
		for(const auto* b=bc->begin(); b != bc->end(); b = bc->next(b))  {
		  normal_data_len += b->size();
		  normal_banks_num++;
		}
		for(const auto* b=bc->special_begin(); b != bc->special_end(); b = bc->next(b))  {
		  special_data_len += b->size();
		  special_banks_num++;
		}
	      }
	      ::snprintf(text, sizeof(text), "|   -> Event No. %6ld %6ld bytes. %4ld collections  "
			 "banks:%4ld [%6ld bytes] specials:%4ld [%6ld bytes]",
			 count, e->total_length(), e->num_bank_collections(),
			 normal_banks_num, normal_data_len, special_banks_num, special_data_len);
	      std::cout << text << std::endl;

	      for(std::size_t ie=0, ne=e->num_bank_collections(); ie<ne; ++ie)   {
		std::map<int,std::size_t> normal_types, special_types;
		const auto* bc = e->bank_collection(ie);
		normal_data_len  = 0;
		special_data_len = 0;
		for(const auto* b=bc->begin(); b != bc->end(); b = bc->next(b))  {
		  normal_data_len += b->size();
		  normal_types[b->type()] += 1;
		}
		for(const auto* b=bc->special_begin(); b != bc->special_end(); b = bc->next(b))  {
		  special_data_len += b->size();
		  special_types[b->type()] += 1;
		}
		if ( dump_bank_collections )   {
		  std::cout << "|   -->  Bank collection: "  << ie << "  "
		       << std::setw(3) << bc->num_banks()    << " / "
		       << std::setw(3) << bc->num_special()  << " banks  "
		       << std::setw(5) << bc->total_length() << " bytes  "
		       << std::endl;
		  if ( bc->num_banks() > 0 )   {
		    std::cout << "|   ---> Data:   "  << std::setw(6) << normal_data_len << " bytes  Types: ";
		    for(const auto& in : normal_types)  {
		      std::cout << in.first << "("
				<< Online::event_print::bankType(in.first)
				<< "): " << in.second << "  ";
		    }
		    std::cout << std::endl;
		    if ( dump_bank_headers )   {
		      for(const auto* b=bc->begin(); b != bc->end(); b = bc->next(b))
			std::cout << "|   ----> " << *b << std::endl;
		    }
		  }
		  if ( bc->num_special() > 0 )   {
		    std::cout << "|   ---> Special:" << std::setw(6) << special_data_len << " bytes  Types: ";
		    for(const auto& is : special_types)   {
		      std::cout << is.first << "("
				<< Online::event_print::bankType(is.first)
				<< "): " << is.second << "  ";
		    }
		    std::cout << std::endl;
		    if ( dump_bank_headers )   {
		      for(const auto* b=bc->special_begin(); b != bc->special_end(); b = bc->next(b))
			std::cout << "|   ----> " << *b << std::endl;
		    }
		  }
		}
		if ( dump_bank_headers && do_break )	{	/// Break in interactive running
		  if ( num_events > 0 )  {
		    if ( std::size_t(num_events) <= n_evt_total ) goto Done;
		  }
		  else if ( !prompt() ) goto Done;
		}
	      }

	      if ( num_events > 0 )  {
		if ( std::size_t(num_events) <= n_evt_total ) goto Done;
	      }
	      else if ( dump_bank_collections && do_break )	{	/// Break in interactive running
		if ( !prompt() ) goto Done;
	      }
	    }
	  }
	  if ( num_events > 0 )  {
	    if ( std::size_t(num_events) <= n_evt_total ) goto Done;
	  }
	  else if ( dump_events && do_break )  {		/// Break in interactive running
	    if ( !prompt() ) goto Done;
	  }
	  n_mep++;
	  continue;
	}
      }
    Done:
      std::cout << "+++ End of file: " << inputs[i] << " [" << total << " bytes read]" << std::endl;
      break;
    }
  }

  if ( buff ) delete [] buff;
  end = clock();
  if ( measure )  {
    std::printf(" %ld Events Processed: Fill all banks      %8ld %8ld ticks/event\n",
		n_evt, long(end-start), long(end-start)/std::max(1UL,n_evt));
  }
  else if ( dump_summary )  {
    std::printf(" %ld Events Processed: Fill all banks\n",n_evt);
  }
  if ( dump_summary )  {
    printer.print_summary(coll);
  }
  if ( output )   {
    std::printf("+++ Closing output file %s after writing %ld events, %ld bytes.\n",
		output->name().c_str(), num_mdf_output-num_mdf_towrite, output->position());
    output->close();
    output.reset();
  }
  return 0;
}

extern "C" int pcie_convert_mep2mdf(int argc, char* argv[])    {
  class help  {
  public:
    static void show()  {
      std::cout <<
	"Usage: pcie_decode_file -option [-option]                           \n"
	"  --input=<file>      input file name  (multiple entries allowed)   \n"
	"  -i <file>           dto.                                          \n"
	"  --directory=<file>  use directory for input files                 \n"
	"  -D <file>           dto.                                          \n"
	"  --output=<file>     Output MDF: file name. Default: no output     \n"
	"  -o <file>           dto.                                          \n"
	"  --summary           Dump summary after processing                 \n"
	"  -s                  dto.                                          \n"
	"  --events=<number>   Number of events to dump.                     \n"
	"  -e <number>         dto.                                          \n"
	"  --no-measurements   No measurement printout.                      \n"
	"  -n                  dto.                                          \n"
		<< std::endl;
    }
  };
  std::vector<char*> args;
  args.push_back(argv[0]);
  try   {
    for(int i = 1; i < argc && argv[i]; ++i)  {
      if ( 0 == ::strncasecmp("--input",argv[i],4) )   {
	args.push_back(argv[i]);
      }
      else if ( 0 == ::strcasecmp("-i",argv[i]) )   {
	args.push_back(argv[i]);
	args.push_back(argv[++i]);
      }
      else if ( 0 == ::strncasecmp("--output",argv[i],4) )   {
	args.push_back(argv[i]);
      }
      else if ( 0 == ::strcasecmp("-o",argv[i]) )   {
	args.push_back(argv[i]);
	args.push_back(argv[++i]);
      }
      else if ( 0 == ::strncasecmp("--directory",argv[i],4) )   {
	args.push_back(argv[i]);
      }
      else if ( 0 == ::strcmp("-D",argv[i]) )   {
	args.push_back(argv[i]);
	args.push_back(argv[++i]);
      }
      else if ( 0 == ::strncasecmp("--events",argv[i],7) )    {
	args.push_back(argv[i]);
      }
      else if ( 0 == ::strncasecmp("-e",argv[i],2) )  {
	args.push_back(argv[i]);
	args.push_back(argv[++i]);
      }
      else if ( 0 == ::strncasecmp("--no-measurements",argv[i],8) )   {
	args.push_back(argv[i]);
      }
      else if ( 0 == ::strcasecmp("-n",argv[i]) )   {
	args.push_back(argv[i]);
      }
      else if ( 0 == ::strncasecmp("--summary",argv[i],7) )    {
	args.push_back((char*)"--dump=summary");
      }
      else if ( 0 == ::strncasecmp("-s",argv[i],7) )    {
	args.push_back((char*)"--dump=summary");
      }
      else if ( 0 == ::strncasecmp("-o",argv[i],2) )  {
	args.push_back(argv[i]);
	args.push_back(argv[++i]);
      }
      else   {
	throw std::runtime_error(std::string("Invalid argument: ")+argv[i]);
      }
    }
  }
  catch(const std::exception& e)   {
    std::cout << "Exception: " << e.what() << std::endl << std::endl;
    help::show();
    return EINVAL;
  }
  return pcie_decode_file(int(args.size()), &args[0]);
}
