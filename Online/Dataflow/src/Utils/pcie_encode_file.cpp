//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include <PCIE40Data/pcie40encoder.h>
#include <EventData/event_header_t.h>
#include <EventData/RawFile.h>
#include <RTL/Logger.h>
#include <RTL/rtl.h>

// C/C++ include files
#include <iostream>

using namespace Online;

namespace {
  void help ()    {
    std::cout << "pcie_encode_file -option [-option]"
	      << "  -input=<file>       input file name   "
	      << "  -output=<file>      output file name  "
	      << "  -events=<number>    number of events to be processed"
	      << "  -packing=<number>   packing factor for MEP creation"
	      << std::endl;
  }
  class file_writer : public pcie40::encoder_t::output_t  {
    RawFile* file = 0;
  public:
    file_writer(RawFile* f) : file(f) {}
    virtual size_t output(const void* buffer, size_t len)  override
    {  return file->write(buffer, len);  }
  };
}

extern "C" int pcie_encode_file(int argc, char* argv[])    {
  pcie40::encoder_t encoder;
  RTL::CLI cli(argc, argv, help);
  std::string input, output;
  size_t packing_factor = 1, events = 99999999999UL;

  RTL::Logger::install_rtl_printer(LIB_RTL_INFO);
  encoder.patch_source_ids = cli.getopt("patch-sourceids", 5) != 0;
  cli.getopt("input",   1, input);
  cli.getopt("output",  1, output);
  cli.getopt("events",  1, events);
  cli.getopt("packing", 1, packing_factor);

  if ( input.empty() || output.empty() )   {
    help();
    return ENOENT;
  }

  RawFile in(input), out(output);
  if ( in.open() == -1 )    {
    std:: cout << "Invalid input file name: " << input << std::endl;
    return ENOENT;
  }
  std::cout << "++ Successfully opened file: " << input << std::endl;
  if ( out.openWrite() == -1 )   {
    std:: cout << "Invalid output file name: " << output << std::endl;
    return EINVAL;
  }
  std::cout << "++ Successfully opened file: " << output << std::endl;

  size_t num_evt = 0;
  size_t eid = 0;
  size_t buff_len = 10*1024*1024;
  unsigned char* buffer = new unsigned char[buff_len];
  file_writer writer {&out};

  std::cout << std::flush;
  for (; events; --events )    {
    RawFile::EventType expected = RawFile::MDF_INPUT_TYPE;
    RawFile::EventType found    = RawFile::AUTO_INPUT_TYPE;
    long rec_len = in.read_event(expected, found, buffer, buff_len);
    if ( rec_len > 0 )   {
      const event_header_t* hdr = (const event_header_t*)buffer;
      size_t hdr_len = hdr->sizeOf(hdr->headerVersion());
      encoder.append(++eid, buffer + hdr_len, rec_len - hdr_len);
      if ( encoder.packingFactor() == packing_factor )  {
	auto ret = encoder.write_record(writer);
	std::cout << "++ Wrote " << ret.first << " sources " 
		  << ret.second << " bytes. Packing:" << packing_factor << std::endl;
	num_evt += packing_factor;
	encoder.reset();
      }
    }
    if ( !in.isOpen() )  {
      std::cout << "++ End of file: " << input << std::endl;
      break;
    }
  }
  if ( encoder.packingFactor() > 0 )  {
    num_evt += packing_factor;
    encoder.write_record(writer);
    encoder.reset();
  }
  std::cout << "++ Closing output file: " << output 
	    << " after encoding " << num_evt << " events. " << std::endl;
  out.close();
  std::cout << "++ Closing input file: " << input 
	    << " after encoding " << num_evt << " events. " << std::endl;
  in.close();
  std::cout << std::flush;
  delete [] buffer;
  return 0;
}
