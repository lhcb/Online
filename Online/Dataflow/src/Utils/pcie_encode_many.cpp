//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
// Created on: May 28, 2021
//==========================================================================
#include <PCIE40Data/pcie40encoder.h>
#include <EventData/RawFile.h>
#include <EventData/bank_header_t.h>
#include <EventData/event_header_t.h>
#include <RZip.h>

using namespace std;
namespace pcie40 = Online::pcie40;

// C/C++ include files
#include <stdexcept>
#include <iostream>
#include <random>
#include <cmath>
#include <ctime>

namespace {
  class file_writer : public pcie40::encoder_t::output_t    {
    Online::RawFile* file = 0;
  public:
    file_writer(Online::RawFile* f) : file(f) {}
    virtual size_t output(const void* buffer, size_t len)  override
    {  return file->write(buffer, len);  }
  };
  const char* get_arg(const char* arg)   {
    const char* p = ::strchr(arg,'=');
    if ( p ) return p+1;
    throw runtime_error(string("Invalid argument: ")+arg);
  }

  class event_mixer  {
  public:
    std::default_random_engine             generator;
    std::uniform_real_distribution<double> distribution {0.0,1.0};
    std::vector<std::string> inputs, signals;
    Online::RawFile          input, signal;
    double                   fraction = 0e0;
    size_t                   num_evt = 0, num_sig = 0;

  public:
    /// Default constructor
    event_mixer() = default;
    /// Default destructor
    ~event_mixer() = default;
    /// Check if input data are still present
    bool done()  const   {
      if ( fraction > 0e0 && !signal.isOpen() && signals.empty() ) return true;
      if ( !input.isOpen() && inputs.empty() ) return true;
      return false;
    }
    /// Generic buffer read
    long  read(std::vector<std::string>& names, Online::RawFile& file, unsigned char* buff, size_t len)  {
      auto expected = Online::RawFile::MDF_INPUT_TYPE;
      auto found    = Online::RawFile::AUTO_INPUT_TYPE;
      while ( !file.isOpen() && !names.empty() )   {
	auto nam = names.back();
	file = Online::RawFile(nam);
	names.pop_back();
	if ( file.open() != -1 ) break;
	std::cout << " ...skipping ..." << std::endl;
      }
      if ( !file.isOpen() ) return -1;
      return file.read_event(expected, found, buff, len);
    }
    /// Read buffer with event mix
    long read(unsigned char* buff, size_t len)  {
      bool sig = (fraction > 0e0) && (distribution(generator) < fraction);
      sig ? ++num_sig : ++num_evt;
      return sig
	? this->read(signals, signal, buff, len)
	: this->read(inputs, input, buff, len);
    }
  };

  /// Decompress opaque data buffer
  bool decompressBuffer( int algtype, unsigned char* tar, size_t tar_len, 
			 const unsigned char* src, size_t src_len,
			 size_t& new_len ) {
    int in_len, out_len, res_len = 0;
    switch ( algtype ) {
    case 0:
      if ( tar != src && tar_len >= src_len ) {
	new_len = src_len;
	::memcpy( tar, src, src_len );
	return true;
      }
      break;
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
    case 8:
    case 9:
      in_len  = src_len;
      out_len = tar_len;
      ::R__unzip( &in_len, (unsigned char*)src, &out_len, tar, &res_len);
      if ( res_len > 0 ) {
	new_len = res_len;
	return true;
      }
      break;
    default:
      break;
    }
    return false;
  }
}

#include <iomanip>

using namespace Online;
using namespace std;

extern "C" int pcie_encode_many(int argc, char* argv[])    {
  class help  {
  public:
    static void show()  {
      std::cout <<
	"Usage: pcie_encode_files -option [-option]                            \n"
	"  --input=<file>      input file name  (multiple entries allowed)     \n"
	"  -i <file>           dto.                                            \n"
	"  --signal=<file>     Signal file name (multiple entries allowed)     \n"
	"  -s <file>           dto.                                            \n"
	"  --fraction=<number> Fraction of signal events in output file        \n"
	"  -f <number>         dto.                                            \n"
	"  --output=<file>     Output file name                                \n"
	"  -o <file>           dto.                                            \n"
	"  --size=<number>     Maximal output file size in MB                  \n"
	"  -L <number>         dto.                                            \n"
	"  --packing=<number>  Packing factor of MEPs created.                 \n"
	"  -p <number>         dto.                                            \n"
	"  --events=<number>   Maximal number of output MEPs.                  \n"
	"  -e <number>         dto.                                            \n"
	"  --tae=<fraction>    Inject TAE frames with a fraction.              \n"
	"  -t <fraction>       dto.                                            \n"
	"  --tae-half-window=<number> TAE half window size.                    \n"
	"  -w <number>         dto.                                            \n"
	"  --skip=<number>     Skip bank type (multipe entries allowed)        \n"
	"  --patch_source_ids  Patch source IDs (with APPROXIMATE detector id) \n"
	"  --help              Show this help.                                 \n"
	"  -h  or -?           dto.                                            \n"
		<< std::endl;
    }
  };
  pcie40::encoder_t::tae_options tae_opts {0, 0e0};
  std::vector<uint8_t> skip_banks;
  event_mixer mixer;
  string      output;
  size_t      packing = 1;
  bool        patch_source_ids = false;
  size_t      events = std::numeric_limits<size_t>::max();
  size_t      max_file_size = std::numeric_limits<size_t>::max();
  
  try   {
    for(int i = 1; i < argc && argv[i]; ++i)  {
      if ( 0 == ::strncasecmp("--input",argv[i],4) )
	mixer.inputs.push_back(get_arg(argv[i]));
      else if ( 0 == ::strcasecmp("-i",argv[i]) )
	mixer.inputs.push_back(argv[++i]);
      else if ( 0 == ::strncasecmp("--signal",argv[i],5) )
	mixer.signals.push_back(get_arg(argv[i]));
      else if ( 0 == ::strcasecmp("-s",argv[i]) )
	mixer.signals.push_back(argv[++i]);
      else if ( 0 == ::strncasecmp("--output",argv[i],4) )
	output               = get_arg(argv[i]);
      else if ( 0 == ::strcasecmp("-o",argv[i]) )
	output               = argv[++i];
      else if ( 0 == ::strncasecmp("--fraction",argv[i],4) )
	mixer.fraction       = ::strtod(get_arg(argv[i]), nullptr);
      else if ( 0 == ::strcasecmp("-f",argv[i]) )
	mixer.fraction       = ::strtod(argv[++i], nullptr);
      else if ( 0 == ::strncasecmp("--events",argv[i],4) )
	events               = ::strtol(get_arg(argv[i]), nullptr, 10);
      else if ( 0 == ::strcasecmp("-e",argv[i]) )
	events               = ::strtol(argv[++i], nullptr, 10);
      else if ( 0 == ::strncasecmp("--packing",argv[i],6) )
	packing              = ::strtol(get_arg(argv[i]), nullptr, 10);
      else if ( 0 == ::strcasecmp("-p",argv[i]) )
	packing              = ::strtol(argv[++i], nullptr, 10);
      else if ( 0 == ::strncasecmp("--size",argv[i],5) )
	max_file_size        = ::strtol(get_arg(argv[i]), nullptr, 10)*1024*1024;
      else if ( 0 == ::strcasecmp("-L",argv[i]) )
	max_file_size        = ::strtol(argv[++i], nullptr, 10)*1024*1024;
      else if ( 0 == ::strcasecmp("--tae",argv[i]) )
	tae_opts.fraction    = ::strtod(get_arg(argv[i]), nullptr);
      else if ( 0 == ::strcasecmp("-t",argv[i]) )
	tae_opts.fraction    = ::strtod(argv[++i], nullptr);
      else if ( 0 == ::strncasecmp("--tae-half-window",argv[i],7) )
	tae_opts.half_window = ::strtol(get_arg(argv[i]), nullptr, 10);
      else if ( 0 == ::strcasecmp("-w",argv[i]) )
	tae_opts.half_window = ::strtol(argv[++i], nullptr, 10);
      else if ( 0 == ::strncasecmp("--skip",argv[i],5) )
	skip_banks.push_back(::strtol(get_arg(argv[i]), nullptr, 10));
      else if ( 0 == ::strncasecmp("--patch_source_ids",argv[i],10) )
	patch_source_ids = true;
      else if ( 0 == ::strcasecmp("--help",argv[i]) )  {
	help::show();
	return EINVAL;
      }
      else if ( 0 == ::strcasecmp("-h",argv[i]) )  {
	help::show();
	return EINVAL;
      }
      else if ( 0 == ::strcasecmp("-?",argv[i]) )  {
	help::show();
	return EINVAL;
      }
      else
	throw runtime_error(string("Invalid argument: ")+argv[i]);
    }
  }
  catch(const std::exception& e)   {
    cout << "Exception: " << e.what() << endl << endl;
    help::show();
    return EINVAL;
  }
  if ( mixer.inputs.empty() )   {
    cout << "No input files given!" << endl << endl;
    help::show();
    return ENOENT;
  }
  else if ( output.empty() )   {
    cout << "No output files given!" << endl << endl;
    help::show();
    return ENOENT;
  }
  else if ( mixer.fraction > 0e0 && mixer.signals.empty() )   {
    cout << "If you supply a signal fraction you also have to supply signal files!" << endl << endl;
    help::show();
    return ENOENT;
  }
  else if ( mixer.fraction == 0e0 && !mixer.signals.empty() )   {
    cout << "If you supply a signal files, the signal fraction must be NONZERO!" << endl << endl;
    help::show();
    return EINVAL;
  }
  else if ( tae_opts.fraction > 0e0 && tae_opts.half_window < 1 )   {
    cout << "If you want to inject TAE frames, you MUST supply a half window size!" << endl << endl;
    help::show();
    return EINVAL;
  }

  ::srand(::time(nullptr));
  RawFile out(output);
  if ( out.openWrite() == -1 )   {
    cout << "Invalid output file name: " << output << endl;
    return ENOENT;
  }
  size_t eid = 0;
  size_t num_bytes = 0;
  size_t buff_len = 10*1024*1024;
  size_t decomp_len = 0;
  unsigned char* decompressed_ptr = nullptr;
  unsigned char* decompressed_buffer = new unsigned char[16];
  unsigned char* compressed_buffer   = new unsigned char[buff_len];

  file_writer writer {&out};
  pcie40::encoder_t encoder;

  encoder.skip_banks = skip_banks;
  encoder.patch_source_ids = patch_source_ids;
  for (; events; --events )    {
    long ret = mixer.read(compressed_buffer, buff_len);
    if ( ret > 0 )   {
      event_header_t* hdr = (event_header_t*)compressed_buffer;
      size_t   hdr_len = hdr->sizeOf(hdr->headerVersion());
      int  compression = hdr->compression();
      unsigned char* start = compressed_buffer + hdr_len;
      decompressed_ptr = compressed_buffer;
      if ( compression > 0 )   {
	size_t expand     = (hdr->compression() >> 4) + 1;
	size_t alloc_len  = hdr_len + (expand+1) * hdr->size();
	size_t new_length = 0;
	if ( decomp_len < alloc_len )  {
	  decomp_len = 1.5*alloc_len;
	  delete [] decompressed_buffer;
	  decompressed_buffer = new unsigned char[decomp_len];
	}
	if ( decompressBuffer(compression,
			      decompressed_buffer + hdr_len, decomp_len-hdr_len, 
			      compressed_buffer   + hdr_len, hdr->size(), new_length) )   {
	  decompressed_ptr = decompressed_buffer;
	  hdr = (event_header_t*)decompressed_ptr;
	  hdr->setHeaderVersion( 3 );
	  hdr->setDataType(event_header_t::BODY_TYPE_BANKS);
	  hdr->setSubheaderLength(sizeof(event_header_t::Header1));
	  hdr->setSize(new_length);
	  hdr->setCompression(0);
	  hdr->setChecksum(0);
	  hdr->setSpare(0);
	  start = decompressed_ptr + hdr_len;
	  ret   = hdr_len + hdr->size();
	}
	else   {
	  continue;
	}
      }
      encoder.append(++eid, start, ret - hdr_len);
      if ( encoder.packingFactor() == packing )  {
	auto written = encoder.write_record(writer, tae_opts);
	encoder.reset();
	num_bytes += written.second;
	cout << "++ Wrote"
	     << " Sources: "    << written.first
	     << " Filesize:"    << setw(5) << int(num_bytes/1024/1024) << " MB "
	     << " Packing: "    << packing
	     << " Background:"  << setw(7) << mixer.num_evt
	     << " Signal:"      << setw(7) << mixer.num_sig
	     << endl;
	if ( num_bytes >= max_file_size ) break;
      }
      continue;
    }
    if ( mixer.done() ) break;
  }

  /// Write the reminder to the output file
  if ( encoder.packingFactor() > 0 )  {
    encoder.write_record(writer);
    encoder.reset();
  }
  out.close();
  cout << "++ Closed file after encoding " << mixer.num_evt << " events" << endl;
  delete [] decompressed_buffer;
  delete [] compressed_buffer;
  return 1;
}
