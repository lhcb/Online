//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include <EventData/RawFile.h>
#include <EventData/bank_header_t.h>
#include <EventData/event_header_t.h>
#include <Tell1Data/Tell1Decoder.h>
#include <RTL/strdef.h>
#include <RTL/rtl.h>

#include <RZip.h>

// C/C++ include files
#include <filesystem>
#include <stdexcept>
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <cstring>
#include <cstdio>
#include <vector>
#include <set>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>

using namespace Online;

namespace {
  static constexpr int peek_size = int(8*sizeof(int));

  typedef std::pair<long, unsigned char*> MemBuffer;

  const char* get_arg(const char* arg)   {
    const char* p = ::strchr(arg,'=');
    if ( p ) return p+1;
    throw std::runtime_error(std::string("Invalid argument: ")+arg);
  }

  bool prompt()   {
  re_check:
    std::cout << "====>  Continue to dump the next event  [q,Q to quit]: " << std::flush;
  re_prompt:
    int c = std::getchar();
    if ( c == -1  ) goto re_prompt;
    if ( c == 10  ) return true;  // <ENTER>
    if ( c == 'q' ) return false;
    if ( c == 'Q' ) return false;
    goto re_check;
  }

  long load_mdf_event(RawFile&     file,
		      std::pair<long,unsigned char*>  output,
		      std::pair<long,unsigned char*>& decomp )
  {
    auto*  mdf_hdr   = (event_header_t*)output.second;
    long   status    = 0;
    int    compress  = mdf_hdr->compression();
    long   data_size = mdf_hdr->recordSize();
    if ( compress > 0 )   {
      int  new_size  = 0;
      int  tgt_size  = 0;
      int  src_size  = data_size;
      long hdr_len   = mdf_hdr->sizeOf(mdf_hdr->headerVersion());

      if ( decomp.first < data_size )   {
	decomp.first  = data_size*1.2;
	if ( decomp.second ) delete [] decomp.second;
	decomp.second = new unsigned char[decomp.first];
      }
      ::memcpy(decomp.second, output.second, peek_size);
      status = file.read(decomp.second + peek_size, data_size - peek_size);
      if ( status < data_size - peek_size )   {
	return -1;
      }
      if ( hdr_len-peek_size > 0 )   {
	::memcpy(output.second, decomp.second, hdr_len);
      }
      if ( 0 == ::R__unzip_header(&src_size, decomp.second + hdr_len, &tgt_size) )   {
	if ( output.first < (long)tgt_size )   {
	  return 0;
	}
	::R__unzip( &src_size, decomp.second + hdr_len,
		    &tgt_size, output.second + hdr_len,
		    &new_size);
	if ( new_size > 0 )   {
	  event_header_t* hdr = (event_header_t*)output.second;
	  hdr->setHeaderVersion( 3 );
	  hdr->setDataType(event_header_t::BODY_TYPE_BANKS);
	  hdr->setSubheaderLength(sizeof(event_header_t::Header1));
	  hdr->setSize(new_size);
	  hdr->setCompression(0);
	  hdr->setChecksum(0);
	  hdr->setSpare(0);
	  return hdr_len + new_size;
	}
      }
      return -1;
    }
    if ( output.first < data_size )   {
      return 0;
    }
    status = file.read(output.second + peek_size, data_size - peek_size);
    if ( status < data_size - peek_size )   {
      return -1;
    }
    return data_size;
  }
}

extern "C" int tell1_check_file(int argc, char* argv[])    {
  namespace fs = std::filesystem;
  class help  {
  public:
    static void show()  {
      std::cout <<
	"Usage: tell1_check_file -option [-option]                                \n"
	"  --input=<file>      input file name  (multiple entries allowed)        \n"
	"  -i <file>           dto. If a directory is given all files are scanned \n"
	"  --directory=<file>  use directory for input files                 \n"
	"  -D <file>           dto.                                          \n"
	"  --dump=events       Dump Event headers                            \n"
	"  --dump=banks        Dump bank headers                             \n"
	"  --dump=data         Dump bank data                                \n"
	"  -d <item>           dto.                                          \n"
	"  --break             Break before next chunk of printout           \n"
	"  -b                  dto.                                          \n"
	"  --events=<number>   Number of events to dump.                     \n"
	"  -e <number>         dto.                                          \n"
	"  --no-measurements   No measurement printout.                      \n"
	"  -n                  dto.                                          \n"
	   << std::endl;
    }
  };
  char *endptr;
  long num_events = -1;
  bool measure  = true;
  bool do_break = false;
  bool dump_events = false;
  bool dump_bank_data = false;
  bool dump_bank_headers = false;
  std::set<uint32_t>       sel_evts;
  std::vector<std::string> inputs;
  std::vector<std::string> directories;
  try   {
    for(int i = 1; i < argc && argv[i]; ++i)  {
      if ( 0 == ::strncasecmp("--input",argv[i],4) )   {
	inputs.push_back(get_arg(argv[i]));
      }
      else if ( 0 == ::strcasecmp("-i",argv[i]) )   {
	inputs.push_back(argv[++i]);
      }
      else if ( 0 == ::strncasecmp("--directory",argv[i],4) )   {
	directories.push_back(get_arg(argv[i]));
      }
      else if ( 0 == ::strcmp("-D",argv[i]) )   {
	directories.push_back(argv[++i]);
      }
      else if ( 0 == ::strncasecmp("--select",argv[i],4) )   {
	sel_evts.insert(::atol(get_arg(argv[i])));
      }
      else if ( 0 == ::strcasecmp("-s",argv[i]) )   {
	sel_evts.insert(::atol(argv[++i]));
      }
      else if ( 0 == ::strncasecmp("--break",argv[i],4) )   {
	do_break = true;
      }
      else if ( 0 == ::strcasecmp("-b",argv[i]) )   {
	do_break = true;
      }
      else if ( 0 == ::strncasecmp("--dump",argv[i],6) )    {
	if ( strncmp(get_arg(argv[i]), "events", 3) == 0 )   {
	  dump_events = true;
	}
	else if ( strncmp(get_arg(argv[i]), "banks", 3) == 0 )   {
	  dump_bank_headers = true;
	  dump_events = true;
	}
	else if ( strncmp(get_arg(argv[i]), "data", 3) == 0 )   {
	  dump_bank_headers = true;
	  dump_bank_data = true;
	  dump_events = true;
	}
      }
      else if ( 0 == ::strncasecmp("-d",argv[i],2) )  {
	const char* a2 = argv[++i];
	if ( strncmp(a2, "events", 3) == 0 )   {
	  dump_events = true;
	}
	else if ( strncmp(a2, "banks", 3) == 0 )   {
	  dump_bank_headers = true;
	  dump_events = true;
	}
	else if ( strncmp(a2, "data", 3) == 0 )   {
	  dump_bank_headers = true;
	  dump_bank_data = true;
	  dump_events = true;
	}
      }
      else if ( 0 == ::strncasecmp("--events",argv[i],7) )    {
	num_events = ::strtol(get_arg(argv[i]), &endptr, 10);
      }
      else if ( 0 == ::strncasecmp("-e",argv[i],2) )  {
	num_events = ::strtol(argv[++i], &endptr, 10);
      }
      else if ( 0 == ::strncasecmp("--no-measurements",argv[i],8) )   {
	measure = false;
      }
      else if ( 0 == ::strcasecmp("-n",argv[i]) )   {
	measure = false;
      }
      else   {
	throw std::runtime_error(std::string("Invalid argument: ")+argv[i]);
      }
    }
  }
  catch(const std::exception& e)   {
    std::cout << "Exception: " << e.what() << std::endl << std::endl;
    help::show();
    return EINVAL;
  }

  if ( !directories.empty() )   {
    for( const auto& dir : directories )   {
      std::error_code ec;
      fs::path path = RTL::str_expand_env(dir);
      if ( !fs::exists(path, ec) || !fs::is_directory(path, ec) )   {
	std::cout << "+++ Ignore directory: " << path.string() 
		  << " [" << ec.message() << "]" << std::endl;
	continue;
      }
      for(auto it = fs::directory_iterator(path); it != fs::directory_iterator(); ++it)   {
	fs::path e = *it;
	if ( fs::exists(e, ec) )   {
	  inputs.push_back(e.string());
	  continue;
	}
	std::cout << "+++ Ignore directory entry: " << e.string() 
		  << " [" << ec.message() << "]" << std::endl;
      }
    }
  }

  if ( inputs.empty() )   {
    std::cout << "No input files given!" << std::endl << std::endl;
    help::show();
    return ENOENT;
  }

  std::size_t  num_evt_total = 0;
  std::size_t  num_bytes_total = 0;
  clock_t start, end;
  char text[1024];

  start = clock();
  for(std::size_t i=0; i < inputs.size(); ++i)   {
    std::error_code ec;
    fs::path path = RTL::str_expand_env(inputs[i]);
    if ( !fs::exists(path, ec) )   {
      std::cout << "+++ Ignore input: " << path.string() 
		<< " [" << ec.message() << "]" << std::endl;
      continue;
    }
    Online::RawFile input(path.string());
    if ( input.open() < 0 )  {
      std::cout << "FAILED to open file: " << inputs[i] << std::endl;
      continue;
    }
    ::snprintf(text, sizeof(text), "+++ Opened file     %s Size on disk: %9ld bytes",
	       inputs[i].c_str(), input.data_size());
    std::cout << text << std::endl;

    long num_evt_file = 0;
    long num_bytes_file = 0;
    MemBuffer deCompress;
    while(1)   {
      int      size[peek_size/sizeof(int)];
      uint8_t *data_ptr, *alloc_ptr;
      off_t    position = input.position();
      int      status   = input.read(size, peek_size);
      auto    *mdf_hdr  = (event_header_t*)size;

      if ( status < int(peek_size) )   {
	goto Done;
      }
      if ( mdf_hdr->is_mdf() )   {
	int  compress   = mdf_hdr->compression();
	long hdr_len    = mdf_hdr->sizeOf(mdf_hdr->headerVersion());
	long alloc_size = mdf_hdr->recordSize();
	++num_evt_file;
	++num_evt_total;
	if ( compress > 0 )   {
	  alloc_size = hdr_len + (mdf_hdr->compression() + 1) * mdf_hdr->size();
	}
	alloc_ptr = data_ptr = new uint8_t[alloc_size];
	::memcpy(data_ptr, size, peek_size);
	long length = load_mdf_event(input, MemBuffer(alloc_size,data_ptr), deCompress);
	if ( length <= 0 )   {
	  delete [] alloc_ptr;
	  goto Done;
	}
	num_bytes_file  += alloc_size;
	num_bytes_total += alloc_size;
	try  {
	  auto* hdr = (event_header_t*)data_ptr;
	  if ( !sel_evts.empty() )    {
	    if ( sel_evts.find(num_evt_file) == sel_evts.end() )  {
	      continue;
	    }
	  }
	  if ( dump_events && (dump_bank_headers || dump_bank_data) )   {
	    std::cout << "+ ====================================================================================+" << std::endl;
	  }
	  if ( dump_events )   {
	    ::snprintf(text, sizeof(text), "| -->  TELL1 event %8ld: Buffer:%p End:%p Size:(%d,%d,%d) Comp:%3u HdrVsn:%u",
		       num_evt_file, (void*)hdr->data(), (void*)hdr->end(), hdr->size0(),
		       hdr->size1(), hdr->size2(), hdr->compression(), hdr->headerVersion());
	    std::cout << text << std::endl;
	  }
	  /// Check raw bank record
	  checkRawBanks(hdr->data(), hdr->end(), true, true);
	  /// Do printout if requested
	  if ( dump_bank_headers )   {
	    const uint8_t* evt_start = hdr->data();
	    while ( evt_start < hdr->end() )   {
	      const auto* b = (raw_bank_offline_t*)evt_start;
	      std::size_t len = b->totalSize();
	      evt_start += len;
	      ::snprintf(text, sizeof(text), "|   -> Tell1 bank: %s",
			 event_print::bankHeader(b).c_str());
	      std::cout << text << std::endl;
	      if ( dump_bank_data )    {
		auto lines = event_print::bankData(b, 10);
		for(const auto& l : lines)   {
		  ::snprintf(text, sizeof(text), "|            Data: %s", l.c_str());
		  std::cout << text << std::endl;
		}
	      }
	      // Protect against infinite loop in case of data corruption
	      if ( 0 == len ) break;
	    }
	  }
	}
	catch(const std::exception& e)   {
	  ::snprintf(text, sizeof(text), "+++ MDF Exception:  %s File position: %9ld", e.what(), position);
	  std::cout << text << std::endl;
	}
	catch(...)   {
	  ::snprintf(text, sizeof(text), "+++ UNKNOWN MDF Exception. File position: %9ld", position);
	  std::cout << text << std::endl;
	}
	delete [] alloc_ptr;
      }
      else   {
	::snprintf(text, sizeof(text), "+++ INVALID MDF Header: %s after %9ld bytes. File position: %9ld",
		   inputs[i].c_str(), num_bytes_file, position);
	std::cout << text << std::endl;
	break;
      }
      
      /// Maximum event number reached
      if ( num_events > 0 && num_evt_total >= std::size_t(num_events) )   {
	inputs.clear();
	goto End;
      }
      continue;

    Done:
      ::snprintf(text, sizeof(text), "+++ End of file     %s: %9ld events [%9ld bytes]",
		 inputs[i].c_str(), num_evt_file, num_bytes_file);
      std::cout << text << std::endl;
      break;
    }

    if ( do_break && !prompt() ) break;
  }

 End:
  end = clock();
  if ( measure )  {
    std::cout << "+++ " <<  num_evt_total << " Events Processed with "
	      << num_bytes_total << " bytes  "
	      << "Time (ticks): " << long(end-start) 
	      << " (total) " << long(end-start)/num_evt_total
	      << " (ticks/event)" << std::endl;
  }
  else  {
    std::cout << "+++ " <<  num_evt_total << " Events Processed with "
	      << num_bytes_total << " bytes." << std::endl;
  }
  return 0;
}
