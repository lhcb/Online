//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include <EventHandling/FileEventAccess.h>
#include <EventData/bank_header_t.h>
#include <EventData/RawFile.h>
#include <PCIE40Data/pcie40.h>
#include <RTL/Logger.h>
#include <RTL/rtl.h>

// C/C++ include files
#include <stdexcept>
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <cstring>
#include <thread>
#include <cstdio>
#include <map>
#include <unistd.h>

using namespace std;
namespace pcie40 = Online::pcie40;

using namespace Online;

namespace {
  const char* get_arg(const char* arg)   {
    const char* p = ::strchr(arg,'=');
    if ( p ) return p+1;
    throw runtime_error(string("Invalid argument: ")+arg);
  }

  bool prompt()   {
  re_check:
    cout << "====>  Continue to dump the next event  [q,Q to quit]: " << flush;
  re_prompt:
    int c = std::getchar();
    if ( c == -1  ) goto re_prompt;
    if ( c == 10  ) return true;  // <ENTER>
    if ( c == 'q' ) _exit(0);
    if ( c == 'Q' ) _exit(0);
    goto re_check;
  }

  /// Main function running the event buffer manager
  void event_reader(FileEventAccess& access, size_t high_mark)   {
    while ( 1 )   {
      if ( access.eventsBuffered() < high_mark )
	access.fill_cache();
      ::lib_rtl_usleep(10);
    }
  }
}

extern "C" int test_event_access(int argc, char* argv[])    {
  class help  {
  public:
    static void show()  {
      std::cout <<
	"Usage: pcie40_decode_file -option [-option]                         \n"
	"  --input=<file>      input file name  (multiple entries allowed)   \n"
	"  -i <file>           dto.                                          \n"
	"  --dump=events       Dump Event headers                            \n"
	"  --dump=collections  Dump bank collection summaries                \n"
	"  --dump=banks        Dump bank headers                             \n"
	"  -d <item>           dto.                                          \n"
	"  --packing=<number>  If MDF: burst packing factor                  \n"
	"  -p <number>         dto.                                          \n"
	"  --prefix=<string>   File prefix for selection                     \n"
	"  -q <string>         dto.                                          \n"
	"  --break             Break before next chunk of printout           \n"
	"  -b                  dto.                                          \n"
	"  --debug             Debug ON                                      \n"
		<< std::endl;
    }
  };

  std::vector<std::string> inputs;
  std::string file_prefix = "";
  bool dump_events = false;
  bool dump_bank_headers = false;
  bool dump_bank_collections = false;
  bool do_break = true;
  bool dbg = false;
  int packing = 1;
  try   {
    for(int i = 1; i < argc && argv[i]; ++i)  {
      if ( 0 == ::strncasecmp("--input",argv[i],4) )   {
	inputs.push_back(get_arg(argv[i]));
      }
      else if ( 0 == ::strcasecmp("-i",argv[i]) )   {
	inputs.push_back(argv[++i]);
      }
      else if ( 0 == ::strncasecmp("--dump",argv[i],4) )    {
	if ( strncmp(get_arg(argv[i]), "events", 3) == 0 )   {
	  dump_events = true;
	}
	else if ( strncmp(get_arg(argv[i]), "collections", 3) == 0 )   {
	  dump_bank_collections = true;
	  dump_events = true;
	}
	else if ( strncmp(get_arg(argv[i]), "banks", 3) == 0 )   {
	  dump_bank_collections = true;
	  dump_bank_headers = true;
	}
      }
      else if ( 0 == ::strncasecmp("-d",argv[i],4) )  {
	const char* a2 = argv[++i];
	if ( strncmp(a2, "events", 3) == 0 )   {
	  dump_events = true;
	}
	else if ( strncmp(a2, "collections", 3) == 0 )   {
	  dump_bank_collections = true;
	  dump_events = true;
	}
	else if ( strncmp(a2, "banks", 3) == 0 )   {
	  dump_bank_collections = true;
	  dump_bank_headers = true;
	  dump_events = true;
	}
      }
      else if ( 0 == ::strncasecmp("--packing",argv[i],5) )   {
	packing = ::strtol(get_arg(argv[i]), nullptr, 10);
      }
      else if ( 0 == ::strcasecmp("-p",argv[i]) )   {
	packing = ::strtol(argv[++i], nullptr, 10);
      }
      else if ( 0 == ::strncasecmp("--prefix",argv[i],5) )   {
	file_prefix = get_arg(argv[i]);
      }
      else if ( 0 == ::strcasecmp("-q",argv[i]) )   {
	file_prefix = argv[++i];
      }
      else if ( 0 == ::strncasecmp("--break",argv[i],4) )   {
	do_break = true;
      }
      else if ( 0 == ::strcasecmp("-b",argv[i]) )   {
	do_break = true;
      }
      else if ( 0 == ::strncasecmp("--debug",argv[i],4) )   {
        dbg = true;
      }
      else   {
	throw runtime_error(string("Invalid argument: ")+argv[i]);
      }
    }
  }
  catch(const std::exception& e)   {
    cout << "Exception: " << e.what() << endl << endl;
    help::show();
    return EINVAL;
  }
  if ( inputs.empty() )   {
    cout << "No input files given!" << endl << endl;
    help::show();
    return ENOENT;
  }

  RTL::Logger::install_rtl_printer(LIB_RTL_DEBUG);
  RTL::Logger::print_startup("Event data access test starting");
  auto log = make_unique<RTL::Logger>(RTL::Logger::getGlobalDevice(), "Test", LIB_RTL_DEBUG);
  
  size_t  n_evt = 0;
  clock_t start, end;
  FileEventAccess access(std::move(log));
  access.config.sources       = inputs;
  access.config.packingFactor = packing;
  access.config.allowedRuns   = {"*"};
  access.config.prefix        = file_prefix;
  cout << "Params: packing: " << access.config.packingFactor;
  cout << " inputs: ";
  for(const auto& i : access.config.sources) cout << i << " ";
  cout << " allowed runs: ";
  for(const auto& r : access.config.allowedRuns) cout << r << " ";
  cout << " file prefix: "    << access.config.prefix
       << endl;

  access.connect({"EvType=2;TriggerMask=0xffffffff,0xffffffff,0xffffffff,0xffffffff;VetoMask=0,0,0,0;MaskType=ANY;UserType=ONE;Frequency=PERC;Perc=100.0"});
  start = clock();

  auto reader = make_unique<thread>(event_reader,ref(access), 100);
  log = make_unique<RTL::Logger>(RTL::Logger::getGlobalDevice(), "Test", LIB_RTL_DEBUG);
  size_t nburst = 0;
  bool   _stop = dbg;
  while(1)   {
    if ( _stop )   {
      while(_stop) lib_rtl_sleep(1000);
    }
    auto burst = access.dequeueBurst();
    if ( burst )  {
      ++nburst;
      for(size_t nevt=burst->num_bx(); nevt > 0; --nevt)  {
	auto e = access.dequeueEvent(burst);
	if ( e.second )   {
	  // typedef std::shared_ptr<guard_t>          shared_guard_t;
	  // typedef std::pair<size_t, shared_guard_t> event_t;
	  typedef std::vector<std::pair<const bank_header_t*, const void*> > evt_data_t;
	  const auto record(e.second->at(e.first));
	  auto typ = e.second->type();
	  evt_data_t evt_data;
	  evt_data.reserve(512);
	  if ( typ == event_traits::tell1::data_type )   {
	    const event_traits::tell1::event_type& event = *record.tell1_event;
	    auto* rec_start = record.data + event.sizeOf(event.headerVersion());
	    auto* rec_end   = record.data + event.size();
	    while ( rec_start < rec_end )  {
	      auto* b = (raw_bank_offline_t*)start;
	      evt_data.emplace_back(b, b->data());
	      rec_start += b->totalSize();
	    }
	  }
	  else if ( typ == event_traits::tell40::data_type )   {
	    const event_traits::tell40::event_type& event = *record.tell40_event;
	    for(size_t i=0; i < event.num_bank_collections(); ++i)  {
	      const auto* bc = event.bank_collection(i);
	      for(const auto* b=bc->begin(); b != bc->end(); b = bc->next(b))   {
		evt_data.emplace_back(b, b->data());
	      }
	      for(const auto* b=bc->special_begin(); b != bc->special_end(); b = bc->next(b))   {
		evt_data.emplace_back(b, b->data());
	      }
	    }
	  }
	  else   {
	    log->error("+++ Algorithm failed [Invalid event type].");
	  }
	  if ( dump_events )    {
	    if ( dump_bank_collections )   {
	      log->always("+=====================================================================");
	    }
	    size_t data_len = 0;
	    std::map<int,size_t> bank_types, bank_data_len;
	    for(const auto& b : evt_data)   {
	      data_len += b.first->size();
	      bank_types[b.first->type()] += 1;
	      bank_data_len[b.first->type()] += b.first->size();
	    }
	    log->always("| ++  Burst[%s]: %ld Event: %ld   %ld banks  %ld bytes",
			typ==event_traits::tell1::data_type ? "TELL1" : "PCIE40",
			nburst, nevt, evt_data.size(), data_len);
	    if ( dump_bank_collections )   {
	      for ( const auto& t : bank_types)   {
		log->always("| ++====>  Bank collection: %3d %-15s Number: %4d  %6ld",
			    t.first, event_print::bankType(t.first).c_str(),
			    t.second, bank_data_len[t.first]);
		if ( dump_bank_headers )   {
		  for(const auto& b : evt_data)   {
		    if ( b.first->type() == t.first )   {
		      log->always("+++  %s", event_print::bankHeader(b.first).c_str());
		    }
		  }
		}
	      }
	    }
	  }
	  ++n_evt;
	  if ( do_break && !prompt() )
	    goto Done;
	}
      }
    }
  }
 Done:
  end = clock();
  printf(" %ld Events Processed: Fill all banks      %8ld %8ld ticks/event\n",
	 n_evt, long(end-start), long(end-start)/max(n_evt,1UL));
  return 0;
}
