//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  ZMQReceiver.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//  Description: Network event data receiver 
//               using ZeroMQ asynchronous data I/O
//
//  Author     : Markus Frank
//==========================================================================

#define TRANSFER_NS ZMQ
#define EventServer        Dataflow_ZMQEventServer
#define EventRequestServer Dataflow_ZMQEventRequestServer
#define DataSelector       Dataflow_ZMQSelector
#define DataSender         Dataflow_ZMQSender
#define DataReceiver       Dataflow_ZMQReceiver

#include <NET/Transfer.h>
#include <Dataflow/EventServer.h>
#include <Dataflow/DataSelector.h>
#include <Dataflow/DataSender.h>
#include <Dataflow/DataReceiver.h>
