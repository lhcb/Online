//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  AmsComponents.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//  Description: Network event data server 
//               using boost::asio asynchronous data I/O
//
//  Author     : Markus Frank
//==========================================================================

#define TRANSFER_NS        AMS
#define EventServer        Dataflow_AmsEventServer
#define EventRequestServer Dataflow_AmsEventRequestServer
#define DataSelector       Dataflow_AmsSelector
#define DataSender         Dataflow_AmsSender
#define DataReceiver       Dataflow_AmsReceiver

#include <NET/Transfer.h>
#include <Dataflow/EventServer.h>
#include <Dataflow/DataSelector.h>
#include <Dataflow/DataSender.h>
#include <Dataflow/DataReceiver.h>
