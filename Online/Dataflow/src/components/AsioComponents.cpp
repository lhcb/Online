//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  AsioComponents.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//  Description: Network event data server 
//               using boost::asio asynchronous data I/O
//
//  Author     : Markus Frank
//==========================================================================

#define TRANSFER_NS        BoostAsio
#define EventServer        Dataflow_AsioEventServer
#define EventRequestServer Dataflow_AsioEventRequestServer
#define DataSelector       Dataflow_AsioSelector
#define DataSender         Dataflow_AsioSender
#define DataReceiver       Dataflow_AsioReceiver

#include <NET/Transfer.h>
#include <Dataflow/EventServer.h>
#include <Dataflow/DataSelector.h>
#include <Dataflow/DataSender.h>
#include <Dataflow/DataReceiver.h>
