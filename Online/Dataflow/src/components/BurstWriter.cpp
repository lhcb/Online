//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  BurstWriter.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include "BurstWriter.h"
#include <Dataflow/Plugins.h>
#include <Tell1Data/Tell1Decoder.h>
#include <RTL/rtl.h>

/// C/C++ include files
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <sstream>

using namespace std;
using namespace Online;
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_BurstWriter,BurstWriter)

namespace  {
  int file_write(int file, const uint8_t* data, long len)   {
    const uint8_t* p = data;
    long tmp = len;
    while (tmp > 0)  {
      long sc = ::write(file, p + len - tmp, tmp);
      if (sc > 0)
	tmp -= sc;
      else return 0;
    }
    return 1;
  }
}

/// Initializing constructor
BurstWriter::BurstWriter(const string& nam, Context& ctxt)
  : Component(nam, ctxt), m_distribution(0.0, 1.0)
{
  declareProperty("Directory",     m_directory     = ".");
  declareProperty("FilePrefix",    m_prefix        = "Run_");
  declareProperty("BufferSize",    m_bufferSize    = 4*1024*1024);
  declareProperty("MaxFileSizeMB", m_maxfileSizeMB = 1024);
  declareProperty("Fraction",      m_fraction      = 1.0);
  declareProperty("DailyFiles",    m_dailyFiles    = false);
  ::setlocale(LC_ALL, "");
}

/// Default destructor
BurstWriter::~BurstWriter()   {
}

/// Initialize the MBM server
int BurstWriter::initialize()  {
  int sc = Component::initialize();
  if( sc == DF_SUCCESS )  {
    m_buffer = (uint8_t*)::malloc(m_bufferSize+1024);
    m_bufferPtr = m_buffer;
  }
  m_maxFileSize = m_maxfileSizeMB * (1<<20);
  m_curr_run_number = 0;
  m_bufferLen = 0;
  m_fileBytes = 0;
  return sc;
}

/// Initialize the MBM server
int BurstWriter::start()  {
  int sc = Component::start();
  if( sc == DF_SUCCESS )  {
  }
  m_curr_run_number = 0;
  m_bufferLen = 0;
  return sc;
}

/// Stop the data flow component. Default implementation is empty.
int BurstWriter::stop()  {
  flush_file();
  return Component::stop();
}

/// Finalize the MBM server
int BurstWriter::finalize()  {
  int sc = Component::finalize();
  if( m_buffer )  {
    ::free(m_buffer);
    m_buffer = 0;
  }
  return sc;
}

/// Cancel the data flow component. 
int BurstWriter::cancel()   {
  return Component::cancel();
}

/// Write multi event buffer to file. Eventually open a new file....
int BurstWriter::writeBuffer()    {
  if( m_fileBytes+m_bufferLen > m_maxFileSize )    {
    if( m_file > 0 ) ::close(m_file);
    m_file = 0;
  }
  if( 0 == m_file )   {
    char textStr[64];
    struct stat stat;
    stringstream fname;
    struct tm currtime;
    time_t tim = ::time(0);
    ::localtime_r(&tim, &currtime);
    if( -1 == ::stat(m_directory.c_str(), &stat) )   {
      error("The directory %s dies not exist -- You must create it by hand first.", m_directory.c_str());
      return DF_ERROR;
    }
    fname << m_directory;
    if( m_dailyFiles )    {
      ::snprintf(textStr,sizeof(textStr), "/%04d", currtime.tm_year+1900);
      fname << textStr;
      ::mkdir(fname.str().c_str(), 0777);      
      ::snprintf(textStr,sizeof(textStr), "/%02d", currtime.tm_mon+1);
      fname << textStr;
      ::mkdir(fname.str().c_str(), 0777);  
      ::snprintf(textStr,sizeof(textStr), "/%02d", currtime.tm_mday);
      fname << textStr;
      ::mkdir(fname.str().c_str(), 0777);  
    }
    if( -1 == ::stat(fname.str().c_str(), &stat) )   {
      error("Failed to create/access the directory: %s [%s]",
	    fname.str().c_str(), RTL::errorString().c_str());
      return DF_ERROR;
    }
    ::snprintf(textStr,sizeof(textStr), "%08d_", m_curr_run_number);
    fname << '/' << m_prefix << textStr;
    ::strftime (textStr,sizeof(textStr), "%EY.%m.%d-%H.%M.%S.mdf",&currtime);
    fname << textStr;
    info("Opening new file: %s", fname.str().c_str());
    m_file = ::open(fname.str().c_str(),
		    O_CREAT|O_EXCL|O_LARGEFILE|O_NOATIME|O_WRONLY,
		    S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH);
    if( m_file <= 0 )    {
      error("Execute: Failed to open data file: %s [%s]",
	    fname.str().c_str(), RTL::errorString(errno).c_str());
      return DF_ERROR;
    }
    m_fileBytes = 0;
  }
  int ret = file_write(m_file, m_buffer, m_bufferLen) ? DF_SUCCESS : DF_ERROR;
  if( ret <= 0 )   {
    error_code ec(errno, system_category());
    error("Execute: Failed to write buffer to file. [%s]",
	  RTL::errorString().c_str());
    return DF_ERROR;
  }
  m_fileBytes += m_bufferLen;
  m_bufferPtr = m_buffer;
  m_bufferLen = 0;
  return DF_SUCCESS;
}

/// Flush existing file buffer to disk and close file
int BurstWriter::flush_file()  {
  if( m_bufferLen > 0 )  {
    if( writeBuffer() != DF_SUCCESS )   {
      error("Execute: Failed to write buffer to file. [%s]",
	    RTL::errorString(errno).c_str());
      return DF_ERROR;
    }
    if( m_file > 0 ) ::close(m_file);
    m_file      = 0;
    m_bufferPtr = m_buffer;
    m_fileBytes = 0;
    m_bufferLen = 0;
  }
  return DF_SUCCESS;
}

/// Data processing overload: Write the data record to disk
int BurstWriter::execute(const Context::EventData& event)  {
  int sc = DF_ERROR;
  try  {
    uint8_t* start  = (uint8_t*)event.data;
    uint8_t* end    = start + event.length;

    while( start < end )   {
      auto* header = (event_header_t*)start;
      long  length = header->size0();
      if( m_fraction >= 1.0 || m_distribution(m_generator) <= m_fraction )   {
	// Check bank's magic word and check on bank type and length
	if( length == header->size1() && length == header->size2() )  {
	  if( 0 == m_curr_run_number )   {
	    m_curr_run_number = header->subHeader().H1->runNumber();
	  }
	  else if( header->subHeader().H1->runNumber() != m_curr_run_number )   {
	    /// Run number change. Flush data and close the current file
	    flush_file();
	  }
	  if( m_bufferLen + length > long(m_bufferSize) )   {
	    if( writeBuffer() != DF_SUCCESS )   {
	      error("Execute: Failed to write buffer to file. [%s]",
		    RTL::errorString(errno).c_str());
	      return DF_ERROR;
	    }
	  }
	  ::memcpy(m_bufferPtr, header, length);
	  m_bufferPtr += length;
	  m_bufferLen += length;
	}
      }
      start += length;
    }
    return DF_SUCCESS;
  }
  catch( const exception& e )   {
    error(e,"Execute: Error processing event.");
  }
  catch( ... )  {
    error("Execute: UNKOWN error processing event.");
  }
  return sc;
}
