//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  RoutingBitsPrescaler.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include <Dataflow/DataflowComponent.h>

// C/C++ include files
#include <vector>

/// Online namespace declaration
namespace Online  {

  /// Component to select events by trigger type
  /**
   *
   *  \author  Markus Frank
   *  \version 1.0
   */
  class CalibAndTriggerTypeFilter : public DataflowComponent {
  public: 
    /// Standard constructor
    CalibAndTriggerTypeFilter(const std::string& name, Context& ctxt); 
    virtual ~CalibAndTriggerTypeFilter() {} ///< Destructor
    virtual int execute(const Context::EventData& event)  override;    ///< Algorithm execution
  protected:
    std::vector< int > m_triggerTypesToPass;
    std::vector< int > m_calibTypesToPass;
  };
}

// Framework include files
#include <EventData/odin_t.h>
#include <EventData/raw_bank_offline_t.h>

Online::CalibAndTriggerTypeFilter::CalibAndTriggerTypeFilter(const std::string& nam, Context& ctxt)
  : DataflowComponent(nam, ctxt), m_triggerTypesToPass(), m_calibTypesToPass()
{
  declareProperty("TriggerTypesToPass", m_triggerTypesToPass = { 4 , 7 } );
  declareProperty("CalibTypesToPass" ,  m_calibTypesToPass   = { 2 , 3 } ) ;
}

int Online::CalibAndTriggerTypeFilter::execute(const Context::EventData& event) {
  processingFlag &= ~DF_PASSED;
  if( event.data )  {
    // We do not pass unless ODIN information fits!
    processingFlag &= ~DF_PASSED;

    for(const char* ptr=event.data, *end=event.data+event.length; ptr<end; )  {
      raw_bank_offline_t *b = (raw_bank_offline_t*)ptr;
      ptr += b->totalSize();
      if( b->type() == bank_types_t::ODIN )  {
	run2_odin_t *odin = b->begin<run2_odin_t>();
	// Check trigger type first
	if( find( m_triggerTypesToPass.begin(), 
		  m_triggerTypesToPass.end(), 
		  odin->trigger_type()) != m_triggerTypesToPass.end() )  {
	  processingFlag |= DF_PASSED;
	  if( ( odin->trigger_type() == OdinData::CalibrationTrigger ) &&
	      ( find( m_calibTypesToPass.begin(), 
		      m_calibTypesToPass.end(), 
		      odin->readout_type()) == m_calibTypesToPass.end() ) )
	    processingFlag &= ~DF_PASSED;
	}
	else {
	  processingFlag &= ~DF_PASSED;
	}
      }
    }
  }
  return DF_SUCCESS;
}

#include <Dataflow/Plugins.h>
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_CalibAndTriggerTypeFilter,CalibAndTriggerTypeFilter)

