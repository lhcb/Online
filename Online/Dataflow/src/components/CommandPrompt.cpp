//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : DAQ/MDF
//
//  Author     : Markus Frank
//==========================================================================

/// Include files from Gaudi
#include <Dataflow/DataflowComponent.h>

/// C/C++ include files
#include <mutex>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <unistd.h>
#include <signal.h>

/// Online namespace declaration
namespace Online  {

  /// Class to halt event loop with interactive response
  /** Class to halt event loop with interactive response
   *
   *  \author Markus Frank
   *  \date   2024-06-13
   */
  class CommandPrompt : public DataflowComponent {
    struct sigaction m_old_handler;
    std::mutex       m_promptLock;
    std::string      m_prompt;

  public:

    /// Initializing constructor
    CommandPrompt(const std::string& nam, Context& ctxt)
      : Component(nam, ctxt)
    {
      declareProperty("Prompt", m_prompt = "<ENTER> to continue q to quit: ");
    }
    
    /// Default destructor
    virtual ~CommandPrompt() = default;
 
    /// Print message
    static void print(const char* str)  {
      std::cout << std::flush;
      std::cerr << std::flush;
      std::fflush(stdout); 
      std::fflush(stderr);
      write(STDOUT_FILENO, str, ::strlen(str));
    }

    /// Interrupt handler
    static void sigint(int /* sig_num */, siginfo_t*, void*)  {
      print("Interrupt caught. Exiting.....\n");
      _exit(0);
    }

    /// Initialize the algorithm
    int initialize()  override  {
      struct sigaction handler;
      int sc = this->Component::initialize();
      handler.sa_handler = 0;
      handler.sa_sigaction = sigint;
      handler.sa_flags = SA_SIGINFO;
      ::sigaction(SIGINT, &handler, &m_old_handler);
      return sc;
    }

    /// Start the algorithm
    int finalize()  override  {
      ::sigaction(SIGINT, &this->m_old_handler, NULL);
      return this->Component::start();
    }
    
    /// Main execution callback
    int execute(const Context::EventData& /* event */)  override  {
      std::lock_guard<std::mutex> lock(m_promptLock);
    Next:
      this->print(this->m_prompt.c_str());
    Next2:
      int c = std::getchar();
      if( c == -1 )  {
	goto Next2;
      }
      if( c == 10 )  { // <ENTER>
	return DF_SUCCESS;
      }
      if( ::toupper(c) != 'Q' )  {
	goto Next;
      }
      this->print("Exiting.....\n");
      ::_exit(0);
      return DF_SUCCESS;
    }
  };
}

/// Declare factory for object creation
#include <Dataflow/Plugins.h>
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_Prompt,CommandPrompt)
