//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  DataflowComponent.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include <Dataflow/Plugins.h>

#include <Dataflow/OutputLogger.h>
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_OutputLogger,OutputLogger)
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_FmcLogger,OutputLogger)

#include <Dataflow/ComponentContainer.h>
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_ComponentContainer,ComponentContainer)

#include <Dataflow/UnmanagedContainer.h>
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_UnmanagedContainer,UnmanagedContainer)

#include <Dataflow/MBMClient.h>
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_MBMClient,MBMClient)

#include <Dataflow/MBMWriter.h>
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_MBMWriter,MBMWriter)

#include <Dataflow/DiskReader.h>
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_DiskReader,DiskReader)
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_BurstReader,DiskReader)

#include <Dataflow/FileWriterMgr.h>
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_FileWriterMgr,FileWriterMgr)

#include <Dataflow/Delay.h>
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_Delay,Delay)

#include "MBMServer.h"
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_MBMServer,MBMServer)

#include "Monitoring.h"
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_Monitoring,Monitoring)

#include "JobOptions.h"
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_JobOptions,JobOptions)

#include "IncidentHandler.h"
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_IncidentHandler,IncidentHandler)

#include "Sequence.h"
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_Sequence,Sequence)

#include "FileWriter.h"
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_FileWriter,FileWriter)

#include "LumiCounter.h"
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_LumiCounter,LumiCounter)

#include "EmptyProcessor.h"
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_EmptyProcessor,EmptyProcessor)

#include "RunableWrapper.h"
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_RunableWrapper,RunableWrapper)
