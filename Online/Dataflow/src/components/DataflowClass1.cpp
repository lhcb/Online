//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  Receiver.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include <Dataflow/DataflowTask.h>

///  Online namespace declaration
namespace Online  {

  /// Functor to print properties of a component
  /**
   *  \author  M.Frank
   *  \version 1.0
   *  \ingroup DD4HEP_SIMULATION
   */
  class DataflowClass1 : public DataflowTask  {
  public:
    /// Initializing constructor
    DataflowClass1(const std::string& nam, Context& ctxt);
    /// Default destructor
    virtual ~DataflowClass1();
    /// Configure the instance (create components)
    virtual int configure()  override;
    /// Initialize the dataflow task instance.
    virtual int initialize()  override;
    /// Start the dataflow task instance.
    virtual int start()  override;
    /// Stop the dataflow task instance.
    virtual int stop()  override;
    /// Finalize the dataflow task instance.
    virtual int finalize()  override;
    /// Terminate the dataflow task instance.
    virtual int terminate()  override;
    /// Unload and exit the application
    virtual int unload()  override;
  };
}      // end namespace Online

/// Framework includes
#include <Dataflow/ControlPlug.h>
#include <RTL/rtl.h>

using namespace std;
using namespace Online;

/// Initializing constructor
DataflowClass1::DataflowClass1(const std::string& nam, Context& ctxt)
  : DataflowTask(nam,ctxt)
{
}

/// Default destructor
DataflowClass1::~DataflowClass1()  {
}

/// Configure the instance (create components)
int DataflowClass1::configure()  {
  int ret = app_configure();
  if( ret == DF_SUCCESS )  {
    return DataflowTask::configure();
  }
  return failed();
}

/// Initialize the dataflow task instance.
int DataflowClass1::initialize()  {
  int ret = app_initialize();
  if( ret == DF_SUCCESS )  {
    return DataflowTask::initialize();
  }
  return failed();
}

/// Start the dataflow task instance.
int DataflowClass1::start()  {
  int ret = app_start();
  if( ret == DF_SUCCESS )  {
    return DataflowTask::start();
  }
  return failed();
}

/// Stop the dataflow task instance.
int DataflowClass1::stop()  {
  int ret = app_stop();
  if( ret == DF_SUCCESS )  {
    return DataflowTask::stop();
  }
  return failed();
}

/// Finalize the dataflow task instance.
int DataflowClass1::finalize()  {
  int ret = app_finalize();
  if( ret == DF_SUCCESS )  {
    return DataflowTask::finalize();
  }
  return failed();
}

/// Terminate the dataflow task instance.
int DataflowClass1::terminate()  {
  app_terminate();
  return DataflowTask::terminate();
}

/// Final act of the process: unload image
int DataflowClass1::unload()  {
  declareState(Control::ST_OFFLINE);
  ::lib_rtl_sleep(100);
  ::_exit(0);
  return DF_SUCCESS;
}

#include <Dataflow/Plugins.h>
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_Class1,DataflowClass1)
