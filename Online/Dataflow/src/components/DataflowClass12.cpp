//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  Receiver.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include <Dataflow/DataflowTask.h>

///  Online namespace declaration
namespace Online  {

  /// Functor to print properties of a component
  /**
   *  \author  M.Frank
   *  \version 1.0
   *  \ingroup DD4HEP_SIMULATION
   */
  class DataflowClass12 : public DataflowTask  {
  public:
    /// Initializing constructor
    using DataflowTask::DataflowTask;
    /// Configure the instance (create components)
    virtual int configure()  override;
    /// Initialize the dataflow task instance.
    virtual int initialize()  override;
    /// Start the dataflow task instance.
    virtual int start()  override;
    /// Stop the dataflow task instance.
    virtual int stop()  override;
    /// Finalize the dataflow task instance.
    virtual int finalize()  override;
    /// Terminate the dataflow task instance.
    virtual int terminate()  override;
  };
}      // end namespace Online

/// Configure the instance (create components)
int Online::DataflowClass12::configure()  {
  int ret = app_configure();
  if ( ret == DF_SUCCESS )  {
    return DataflowTask::configure();
  }
  return failed();
}

/// Initialize the dataflow task instance.
int Online::DataflowClass12::initialize()  {
  int ret = app_initialize();
  if ( ret != DF_SUCCESS )  {
    return failed();
  }
  ret = app_start();
  if ( ret != DF_SUCCESS )  {
    return failed();
  }
  return DataflowTask::initialize();
}

/// Start the dataflow task instance.
int Online::DataflowClass12::start()  {
  return DataflowTask::start();
}

/// Stop the dataflow task instance.
int Online::DataflowClass12::stop()  {
  return DataflowTask::stop();
}

/// Finalize the dataflow task instance.
int Online::DataflowClass12::finalize()  {
  int ret = app_stop();
  if ( ret == DF_SUCCESS )  {
  }
  ret = app_finalize();
  if ( ret == DF_SUCCESS )  {
    return DataflowTask::finalize();
  }
  return failed();
}

/// Terminate the dataflow task instance.
int Online::DataflowClass12::terminate()  {
  app_terminate();
  return DataflowTask::terminate();
}

#include <Dataflow/Plugins.h>
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_Class12,DataflowClass12)
