//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  DataflowClass2.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include <Dataflow/DataflowTask.h>

///  Online namespace declaration
namespace Online  {

  /// Functor to print properties of a component
  /**
   *  \author  M.Frank
   *  \version 1.0
   *  \ingroup DD4HEP_SIMULATION
   */
  class DataflowClass2 : public DataflowTask  {
  public:
    /// Initializing constructor
    DataflowClass2(const std::string& nam, Context& ctxt);
    /// Default destructor
    virtual ~DataflowClass2() = default;
    /// Configure the instance (create components)
    virtual int configure()  override;
    /// Start the dataflow task instance.
    virtual int start()  override;
    /// Stop the dataflow task instance.
    virtual int stop()  override;
    /// Terminate the dataflow task instance.
    virtual int terminate()  override;
  };

}      // end namespace Online

using namespace std;
using namespace Online;

/// Initializing constructor
DataflowClass2::DataflowClass2(const std::string& nam, Context& ctxt)
  : DataflowTask(nam,ctxt)
{
}

/// Configure the instance (create components)
int DataflowClass2::configure()  {
  int ret = app_configure();
  if( ret == DF_SUCCESS )  {
    return DataflowTask::configure();
  }
  return failed();
}

/// Start the dataflow task instance.
int DataflowClass2::start()  {
  int ret;
  if( !options.empty() )    {
    // Force reload of the options. Then start the application
    context.options->clear();
    ret = context.options->load(options);
    if( ret != DF_SUCCESS ) 
      return error("Failed to load job options: %s.", options.c_str());
  }
  ret = app_initialize();
  if( ret == DF_SUCCESS )  {
    ret = app_start();
    if( ret == DF_SUCCESS )  {
      return DataflowTask::start();
    }
  }
  return failed();
}

/// Stop the dataflow task instance.
int DataflowClass2::stop()  {
  int ret = app_stop();
  if( ret != DF_SUCCESS )  {}  // Handle error ?
  ret = app_finalize();
  if( ret != DF_SUCCESS )  {}  // Handle error ?
  return DataflowTask::stop();
}

/// Terminate the dataflow task instance.
int DataflowClass2::terminate()  {
  app_terminate();
  return DataflowTask::terminate();
}

#include <Dataflow/Plugins.h>
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_Class2,DataflowClass2)
