//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  DUMMYMonitoring.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_DUMMYMONITORING_H
#define ONLINE_DATAFLOW_DUMMYMONITORING_H

/// Framework include files
#include <Dataflow/DataflowComponent.h>
#include <Dataflow/DataflowContext.h>
#include <Dataflow/Incidents.h>
#include <Dataflow/Plugins.h>

/// C/C++ include files
#include <typeinfo>
#include <string>
#include <map>
#include <set>

namespace AIDA { class IBaseHistogram; }

namespace Online   {

  /// Dataflow monitoring facility using DUMMY.
  /** @class DummyMonitoring DummyMonitoring.h Gaucho/DummyMonitoring.h
   *
   *  This class implements the IDummyMonitoring interface, and publishes Gaudi variables
   *  to outside monitoring processes with Dummy.
   *
   *  A DummyPropServer is started which takes string commands in the format
   *  Algorithm.Property and returns the value of the property.
   *
   *  @author Philippe Vannerem
   *  @author Jose Helder Lopes Jan. 2005
   *  @author Jose Helder Lopes 2006/12/26: Modified to publish general c-structures and AIDA::IProfile1D
   *  @author Juan Otalora Goicochea 2007/11/20: MonObjects
   *  @author Markus Frank   Fork off for Dataflow
   */
  class DummyMonitoring : public DataflowContext::Monitor, public DataflowComponent  {

  protected:
    /// Properties:
    std::string    m_partname;
    std::string    m_ProcName;
    std::string    m_ProgName;
    std::string    m_expandInfix;

    int   m_updateInterval;
    int   m_CounterInterval;
    int   m_uniqueServiceNames;
    bool  m_expandCounterServices;
    bool  m_DontResetCountersonRunChange;
    bool  m_useDStoreNames;
    int   m_maxNumCountersMonRate;
    /// Properties steering the published values
    int   m_disableMonObjectsForBool;
    int   m_disableMonObjectsForInt;
    int   m_disableMonObjectsForLong;
    int   m_disableMonObjectsForDouble;
    int   m_disableMonObjectsForString;
    int   m_disableMonObjectsForPairs;
    int   m_disableMonObjectsForHistos;

    int   m_disableDeclareInfoBool;
    int   m_disableDeclareInfoInt;
    int   m_disableDeclareInfoLong;
    int   m_disableDeclareInfoDouble;
    int   m_disableDeclareInfoString;
    int   m_disableDeclareInfoPair;
    int   m_disableDeclareInfoFormat;
    int   m_disableDeclareInfoHistos;

    int   m_disableMonRate;
    int   m_disableDimPropServer;
    int   m_disableDimCmdServer;
    //int m_disableDimRcpGaucho;

    std::vector<std::string> m_CounterClasses;
    std::vector<std::string> m_HistogramClasses;

    
  protected:
    template<class T>
    int declare_histogram(const std::string& owner, const std::string& nam, T* var, const std::string& desc);

  public:
    DummyMonitoring(const std::string& name, Context& ctxt);
    virtual ~DummyMonitoring() = default;
    virtual int initialize()  override;
    virtual int finalize()  override;

    /// Incident handler implemenentation: Inform that a new incident has occured
    virtual void handle(const DataflowIncident& inc)  override;

    /// Monitoring interface: Declare monitoring entity (simple scalar)
    virtual int  declare(const std::string&    owner,
                         const std::string&    nam,
                         const void*           ptr,
                         const std::type_info& typ,
                         const std::string&    desc)  override;
    
    /// Monitoring interface: Declare monitoring entiry (ratio of two numbers)
    virtual int  declare(const std::string&    owner,
                         const std::string&    nam,
                         const void*           ptr1,
                         const void*           ptr2,
                         const std::type_info& typ,
                         const std::string&    desc)  override;

    /// Monitoring interface: Declare monitoring entity (pointer to array with size)
    virtual int declare(const std::string&    parent,
                        const std::string&    nam,
                        const void*           ptr,
                        size_t                data_length,
                        const std::type_info& data_typ,
                        const std::string&    desc)   override;
    
    /// Monitoring interface: Declare monitoring entity (structure with given format)
    virtual int declare(const std::string&     owner,
                        const std::string&     nam,
                        const void*            ptr,
                        std::size_t            data_length,
                        const std::type_info&  typ,
                        const std::string&     fmt,
                        const std::string&     desc)  override;

    /// Monitoring interface: Undeclare single monitoring item
    virtual int undeclare(const std::string& parent,const std::string& nam)  override;
    /// Monitoring interface: Undeclare monitoring information
    virtual int undeclare(const std::string& parent)  override;
    /// Monitoring interface: Update monitoring items
    virtual int update(const std::string& parent,const std::string& name, int runno )  override;
    /// Monitoring interface: Reset some/all monitoring items
    virtual int reset(const std::string& parent,const std::string& what)  override;
    /// Update current run number context for updates
    virtual void setRunNo(uint32_t runno) override;
  };
}      // End namespace Online
#endif // ONLINE_DATAFLOW_DUMMYMONITORING_H

using namespace Online;

// Factory for instantiation of service objects
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_DummyMonitoring,DummyMonitoring)

DummyMonitoring::DummyMonitoring(const std::string& nam, Context& ctxt)
  : DataflowComponent(nam,ctxt)
{
  declareProperty("UniqueServiceNames",          m_uniqueServiceNames = 0);
  declareProperty("disableMonRate",              m_disableMonRate = 0);
  declareProperty("disableDimPropServer",        m_disableDimPropServer = 0);
  declareProperty("disableDimCmdServer",         m_disableDimCmdServer = 0);
  // declareProperty("disableDimRcpGaucho",      m_disableDimRcpGaucho = 0);

  declareProperty("disableMonObjectsForBool",    m_disableMonObjectsForBool = 1);
  declareProperty("disableMonObjectsForInt",     m_disableMonObjectsForInt = 0);
  declareProperty("disableMonObjectsForLong",    m_disableMonObjectsForLong = 0);
  declareProperty("disableMonObjectsForDouble",  m_disableMonObjectsForDouble = 0);
  declareProperty("disableMonObjectsForString",  m_disableMonObjectsForString = 1);
  declareProperty("disableMonObjectsForPairs",   m_disableMonObjectsForPairs = 1);
  declareProperty("disableMonObjectsForHistos",  m_disableMonObjectsForHistos = 0);

  declareProperty("disableDeclareInfoBool",      m_disableDeclareInfoBool = 1);
  declareProperty("disableDeclareInfoInt",       m_disableDeclareInfoInt = 0);
  declareProperty("disableDeclareInfoLong",      m_disableDeclareInfoLong = 0);
  declareProperty("disableDeclareInfoDouble",    m_disableDeclareInfoDouble = 0);
  declareProperty("disableDeclareInfoString",    m_disableDeclareInfoString = 1);
  declareProperty("disableDeclareInfoPair",      m_disableDeclareInfoPair = 1);
  declareProperty("disableDeclareInfoFormat",    m_disableDeclareInfoFormat = 0);
  declareProperty("disableDeclareInfoHistos",    m_disableDeclareInfoHistos = 0);
  declareProperty("maxNumCountersMonRate",       m_maxNumCountersMonRate = 1000);
  declareProperty("DimUpdateInterval",           m_updateInterval = 20);
  declareProperty("CounterUpdateInterval",       m_CounterInterval =0);
  declareProperty("ExpandCounterServices",       m_expandCounterServices=0);
  declareProperty("ExpandNameInfix",             m_expandInfix="");
  declareProperty("PartitionName",               m_partname="LHCb");
  declareProperty("ProcessName",                 m_ProcName="");
  declareProperty("ProgramName",                 m_ProgName="");
  declareProperty("DontResetCountersonRunChange",m_DontResetCountersonRunChange=false);
  declareProperty("UseDStoreNames",              m_useDStoreNames=false);
  declareProperty("CounterClasses",              m_CounterClasses);
  declareProperty("HistogramClasses",            m_HistogramClasses);
}

int DummyMonitoring::initialize()   {
  return Component::initialize();
}

int DummyMonitoring::finalize()  {
  return Component::finalize();
}

/// Incident handler implemenentation: Inform that a new incident has occured
void DummyMonitoring::handle(const DataflowIncident& inc) {
  if( inc.type == "DAQ_RUNNING" ) {
  }
  else if( inc.type == "DAQ_STOPPED" ) {
  }
}

void DummyMonitoring::setRunNo(uint32_t /* runno */)  {
}

/// Monitoring interface: Undeclare single monitoring item
int DummyMonitoring::undeclare(const std::string& /* owner */, const std::string& /* nam */)  {
  return DF_SUCCESS;
} 

/** Undeclare monitoring information
    @param owner Owner identifier of the monitoring information
*/
int DummyMonitoring::undeclare(const std::string& /* owner */)  {
  return DF_SUCCESS;
}

/// Monitoring interface: Declare monitoring entity (simple scalar)
int DummyMonitoring::declare(const std::string&    /* owner  */, 
                             const std::string&    /* nam    */, 
                             const void*           /* var    */,
                             const std::type_info& /* typ    */,
                             const std::string&    /* desc   */)
{
  return DF_SUCCESS;
}

/// Monitoring interface: Declare monitoring entiry (ratio of two numbers)
int DummyMonitoring::declare(const std::string&    /* owner  */, 
                             const std::string&    /* nam    */, 
                             const void*           /* var1   */,
                             const void*           /* var2   */,
                             const std::type_info& /* typ    */,
                             const std::string&    /* desc   */)
{
  return DF_SUCCESS;
}

// Monitoring interface: Declare monitoring entity (pointer with size)
int DummyMonitoring::declare(const std::string&    /* owner  */, 
                             const std::string&    /* nam    */, 
                             const void*           /* var    */,
                             std::size_t           /* size   */,
                             const std::type_info& /* typ    */,
                             const std::string&    /* desc   */)
{
  return DF_SUCCESS;
}

// Monitoring interface: Declare monitoring entity (structure with given format)
int DummyMonitoring::declare(const std::string&    /* owner  */, 
                             const std::string&    /* nam    */, 
                             const void*           /* var    */,
                             std::size_t           /* size   */,
                             const std::type_info& /* typ    */,
                             const std::string&    /* format */,
                             const std::string&    /* desc   */)
{
  return DF_SUCCESS;
}

/// Monitoring interface: Update monitoring items
int DummyMonitoring::update(const std::string& , const std::string& , int)  {
  return DF_SUCCESS;
}

/// Monitoring interface: Reset some/all monitoring items
int DummyMonitoring::reset(const std::string& /* owner */, const std::string& /* what */)  {
  return DF_SUCCESS;
}

