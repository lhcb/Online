//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  EmptyProcessor.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include "EmptyProcessor.h"
#include <Dataflow/Incidents.h>
#include <RTL/rtl.h>

using namespace Online;

/// Initializing constructor
EmptyProcessor::EmptyProcessor(const std::string& nam, Context& ctxt)
  : Component(nam, ctxt)
{
}

/// Default destructor
EmptyProcessor::~EmptyProcessor()   {
}

/// Initialize the MBM server
int EmptyProcessor::initialize()  {
  int sc = Component::initialize();
  info("Processing initialize....");
  subscribeIncident("DAQ_PAUSE");
  subscribeIncident("DAQ_CANCEL");
  return sc;
}

/// Start the MBM server
int EmptyProcessor::start()  {
  info("Processing start....");
  return Component::start();
}

/// Finalize the MBM server
int EmptyProcessor::finalize()  {
  info("Processing finalize....");
  unsubscribeIncidents();
  return Component::finalize();
}

/// Stop the MBM server
int EmptyProcessor::stop()  {
  info("Processing stop....");
  return Component::stop();
}

/// Pause the data flow component. Default implementation is empty.
int EmptyProcessor::pause()  {
  info("Processing pause....");
  return DataflowComponent::pause();
}

/// Continuing the data flow component. Default implementation is empty.
int EmptyProcessor::continuing()  {
  info("Processing continue....");
  return DataflowComponent::continuing();
}

/// Data processing overload: Send data record to network client
int EmptyProcessor::execute(const Context::EventData& /* event */)  {
  //info("Processing event: length: %8d  at %p ....",event.length,event.data);
  return DF_SUCCESS;
}

/// Incident handler implemenentation: Inform that a new incident has occured
void EmptyProcessor::handle(const DataflowIncident& inc)    {
  info("Got incident: %s ot type:%s",inc.name.c_str(), inc.type.c_str());
}
