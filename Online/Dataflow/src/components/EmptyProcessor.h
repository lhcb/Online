//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  EmptyProcessor.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_EMPTYPROCESSOR_H
#define ONLINE_DATAFLOW_EMPTYPROCESSOR_H

// Framework include files
#include <Dataflow/DataflowComponent.h>

///  Online namespace declaration
namespace Online  {

  /// EmptyProcessor component to be applied in the manager sequence
  /**
   * EmptyProcessor: Debugging entity.
   * Follow all transitions and issue response 
   * on the info level.
   *
   * @author  Markus Frank
   * @version 1.0
   */
  class EmptyProcessor : public DataflowComponent  {
  public:
    /// Initializing constructor
    EmptyProcessor(const std::string& name, Context& ctxt);
    /// Default destructor
    virtual ~EmptyProcessor();
    /// Initialize the MBM server
    virtual int initialize()  override;
    /// Start the MBM server
    virtual int start()  override;
    /// Stop the MBM server
    virtual int stop()  override;
    /// Finalize the MBM server
    virtual int finalize()  override;
    /// Continuing the data flow component.
    virtual int continuing()  override;
    /// Pause the data flow component.
    virtual int pause()  override;
    /// Data processing overload: process event
    virtual int execute(const Context::EventData& event)  override;
    /// Incident handler implemenentation: Inform that a new incident has occured
    virtual void handle(const DataflowIncident& inc)  override;
  };
}      // end namespace Online
#endif //  ONLINE_DATAFLOW_EMPTYPROCESSOR_H
