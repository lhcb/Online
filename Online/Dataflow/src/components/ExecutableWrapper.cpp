//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : Online/GaudiOnlineTests
//
//  Author     : Markus Frank
//==========================================================================

// Include files from the framework
#include <Dataflow/ExternalCallWrapper.h>
#include <RTL/Process.h>

/// C/C++ include files
#include <memory>
#include <thread>

/// Online namespace declaration
namespace Online    {

  /// Generic class to wrapp standalone non-dataflow executable within dataflow application
  /** Generic class to wrapp standalone non-dataflow executable within dataflow application
   *
   * \version 1.0
   * \author  M.Frank
   */
  class ExecutableWrapper : public ExternalCallWrapper  {
  public:
    /// Property: arguments for the process to be executed
    std::vector<std::string>  arguments;
    /// Property: executable name of the process to be executed
    std::string               executable;
    /// Property: process name of the process to be executed
    std::string               process_name;
    /// Property: Log file for sub-process
    std::string               log_file;
    /// Property: replacement tag to get proper outputs for qmtest
    std::map<std::string,std::string> replacements;
    /// Property: Sleep before start
    int                       sleep_pre_start { 0 };
    /// Property: Sleep after start
    int                       sleep_start { 0 };
    /// Property: Sleep after stop
    int                       sleep_stop  { 0 };
    /// Property: Restart setup (see Process::ensure)
    int                       ensure      { 0 };
    /// Property: Print command when task is started/stopped
    bool                      print_cmd   { true };
    bool                      kill_only   { false };

    /// Process stub
    std::unique_ptr<RTL::Process> process;
    std::unique_ptr<std::thread>  proc_thread;
    bool                      need_watching { true };
    
    /// Start the process using the stub
    virtual int start_external_call(const std::string& when)  final override;
    /// Stop the process using the stub
    virtual int stop_external_call(const std::string& when)  final override;

    /// Print process exit status if NON-NULL
    void print_process_status()  const;
    /// Build valid command line including replacements
    std::string command_line()   const;
    
  public:
    /// Default constructor
    ExecutableWrapper(const std::string& name, Context& ctxt);

    /// Standard destructor
    virtual ~ExecutableWrapper();

    /// Initialize the component
    virtual int initialize()  override;
  };
}    // End namespace Online

/// Include files from the framework
#include <Dataflow/Plugins.h>
#include <RTL/DllAccess.h>
#include <RTL/strdef.h>
#include <RTL/rtl.h>

DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_ExecutableWrapper,ExecutableWrapper)
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_SubprocessWrapper,ExecutableWrapper)

using namespace Online;

/// Initializing constructor
ExecutableWrapper::ExecutableWrapper(const std::string& nam, Context& ctxt)
  : ExternalCallWrapper(nam, ctxt)
{
  declareProperty("Replacements",   this->replacements);
  declareProperty("Process",        this->process_name);
  declareProperty("Executable",     this->executable);
  declareProperty("Arguments",      this->arguments);
  declareProperty("PrintCommand",   this->print_cmd = true);
  declareProperty("Sleep_Prestart", this->sleep_pre_start = 0);
  declareProperty("Sleep_Start",    this->sleep_start = 0);
  declareProperty("Sleep_Stop",     this->sleep_stop = 0);
  declareProperty("Ensure",         this->ensure = 0);
  declareProperty("LogFile",        this->log_file = "");
  declareProperty("KillOnly",       this->kill_only = false);
}

/// Standard destructor
ExecutableWrapper::~ExecutableWrapper()   {
  this->start_external_call("destruction");
  this->stop_external_call("destruction");
}

/// Build valid command line including replacements
std::string ExecutableWrapper::command_line()   const   {
  if( this->process )   {
    std::string com = this->process->bash_command();
    if( !this->replacements.empty() )  {
      for(const auto& r : this->replacements)
	com = RTL::str_replace(com, r.first, r.second);
    }
    return com;
  }
  return "";
}

/// Print process exit status if NON-NULL
void ExecutableWrapper::print_process_status()  const  {
  if( this->process )   {
    auto status = this->process->exit_status();
    if( status != 0 )   {
      auto command = this->command_line();
      always("+++ FAILED to start process:%s. exit status: %d [%s]",
	     command.c_str(), status, ::RTL::errorString(status).c_str());
    }
  }
}

/// Start the process using the stub
int ExecutableWrapper::start_external_call(const std::string& when)   {
  if( this->proc_start == when )   {
    if( this->sleep_pre_start > 0 )  {
      ::lib_rtl_sleep(1000*this->sleep_pre_start);
    }
    this->need_watching = true;
    this->proc_thread = std::make_unique<std::thread>([this,when]()  {
	std::string com = this->command_line();
	if( this->print_cmd )  {
	  always("(%s) +++ Start process: %s", when.c_str(), com.c_str());
	}
	// std::string cmd = "bash -c \"" + com + "\"";
	// RTL::Process::setDebug(true);
	// ::system(cmd.c_str());
	/// Spawning a process for one reason or another does not work.
	/// Have to find out why. This would be much better!
	if( this->ensure > 0 )
	  this->process->ensure(this->ensure, 1000);
	else
	  this->process->start();
	int sc = this->process->wait_for(100, 0);
	if( sc == RTL::Process::ENDED )   {
	  this->print_process_status();
	  if( this->process->exit_status() != 0 )   {
	    this->fireIncident("DAQ_ERROR");
	    return;
	  }
	}
	// OK: Seems the process startup worked
	while( this->need_watching )   {
	  ::lib_rtl_sleep(100);
	  sc = this->process->wait_for(500, 0);
	  if( sc == RTL::Process::ENDED )   {
	    this->print_process_status();
	    return;
	  }
	  if( !this->need_watching )  {
	    return;
	  }
	}
      });
    if( this->sleep_start > 0 )  {
      ::lib_rtl_sleep(1000*this->sleep_start);
    }
  }
  return DF_SUCCESS;
}

/// Stop the process using the stub
int ExecutableWrapper::stop_external_call(const std::string& when)   {
  if( this->proc_stop == when )   {
    const auto& nam = this->process->name();
    if( this->sleep_stop > 0 )  {
      ::lib_rtl_sleep(1000*this->sleep_stop);
    }
    if( this->print_cmd )   {
      always("(%s) +++ Stop  process: %s",
	     when.c_str(), this->process->bash_command().c_str());
    }
    if( !this->process->is_running() )   {
      debug("(%s) +++ Stop  process: %s is already DEAD. Nothing to do.",
	    when.c_str(), nam.c_str());
    }
    this->need_watching = false;
    if( this->proc_thread )  {
      this->proc_thread->join();
    }
    int sc = this->process->wait_for(100, 0);
    // Try to stop gracefully the process
    // 1) First check if it ended by itself.
    if( sc == RTL::Process::ENDED )   {
      always("Process %s exited.", nam.c_str());
    }
    else   {
      for(int i=0; i<10; ++i)   {
	sc = this->process->wait_for(200);
	if( sc == RTL::Process::ENDED )   {
	  always("Process %s exited.", nam.c_str());
	  break;
	}
	else if( !this->process->is_running() )    {
	  always("Process %s exited.", nam.c_str());
	  break;
	}
      }
      // Invoke GDB backtrace to see why the bugger hangs:
      if( this->process->is_running() )   {
	int pid = this->process->pid();
	::lib_rtl_backtrace(pid);
      }
      this->process->wait_for(100);
      if( this->process->is_running() )   {
	// 2) Send SIGTERM and see it the process reacts
	if( !this->kill_only )  {
	  this->process->stopall();
	}
	sc = this->process->wait_for(500, 0);
	if( sc == RTL::Process::ENDED )   {
	  always("Process %s exited.", nam.c_str());
	}
	else  {
	  // 3) Send SIGKILL to the process
	  this->process->killall();
	  sc = this->process->wait_for(5000, 0);
	  if( sc == RTL::Process::ENDED )   {
	    always("Process %s exited.", nam.c_str());
	  }
	}
      }
    }
  }
  return DF_SUCCESS;
}

/// Initialize the component
int ExecutableWrapper::initialize()  {
  this->setProperties();
  if( !this->process )   {
    std::string log = this->log_file;
    if( RTL::str_upper(log) == "YES" )  {
      log = this->process_name + ".log";
    }
    this->process = 
      std::make_unique<RTL::Process>(this->process_name,
				     this->executable,
				     this->arguments,
				     log);
  }
  return ExternalCallWrapper::initialize();
}
