//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  FileCleanup.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_FILECLEANUP_H
#define ONLINE_DATAFLOW_FILECLEANUP_H

/// Framework include files
#include <Dataflow/DataflowComponent.h>

/// C/C++ include files
#include <vector>
#include <ctime>

///  Online namespace declaration
namespace Online  {

  /// FileCleanup component to be applied in the manager sequence
  /**
   *  Decode a frame of Tell1 banks in the LHCb online system.
   *
   * @author  Markus Frank
   * @version 1.0
   */
  class FileCleanup : public DataflowComponent  {
  protected:
    /// Property: Directory to manage
    std::string  m_directory;
    /// Property: Partition name string
    std::string  m_partitionName;
    /// Property: Run-type string
    std::string  m_activity;
    /// Property: Stream identifier
    std::string  m_stream;
    /// Property: Maximum number of files to keep
    std::size_t  m_numFiles     { 10 };
    /// Property: Minimal cleanup interval in seconds
    long         m_interval     {  0 };

    /// Time stamp of the last cleanup action
    std::time_t  m_last_cleanup {  0 };
    
  public:
    /// Initializing constructor
    FileCleanup(const std::string& name, Context& ctxt);
    /// Default destructor
    virtual ~FileCleanup() = default;
    /// Data processing overload: process event
    virtual int execute(const Context::EventData& event)  override;
  };
}      // end namespace Online
#endif //  ONLINE_DATAFLOW_FILECLEANUP_H
//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  FileCleanup.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include <Dataflow/EventContext.h>
#include <Dataflow/Plugins.h>
#include <RTL/strdef.h>
#include <RTL/rtl.h>

/// C/C++ include files
#include <filesystem>
#include <map>

DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_FileCleanup,FileCleanup)

/// Initializing constructor
Online::FileCleanup::FileCleanup(const std::string& nam, Context& ctxt)
: Component(nam,ctxt)
{
  declareProperty("PartitionName", m_partitionName = "");
  declareProperty("Stream",        m_stream    =  "");
  declareProperty("Activity",      m_activity  =  "");
  declareProperty("Directory",     m_directory =  "");
  declareProperty("NumberOfFiles", m_numFiles  =  10);
  declareProperty("Interval",      m_interval  = 120);
  m_last_cleanup = ::time(nullptr);
}

/// Data processing overload: process event
int Online::FileCleanup::execute(const Context::EventData& /* event */)  {
  namespace fs = std::filesystem;
  if( !m_directory.empty() && m_numFiles > 0 )    {
    std::time_t now = ::time(nullptr);
    if( m_interval == 0 || (now - m_last_cleanup) > m_interval )   {
      std::string dir_name = m_directory;
      std::string node = RTL::nodeNameShort();
      dir_name = RTL::str_replace(dir_name, "${NODE}",      node);
      dir_name = RTL::str_replace(dir_name, "${STREAM}",    this->m_stream);
      dir_name = RTL::str_replace(dir_name, "${ACTIVITY}",  this->m_activity);
      dir_name = RTL::str_replace(dir_name, "${PARTITION}", this->m_partitionName);

      std::error_code ec;
      fs::path dir = dir_name;
      if( fs::exists(dir, ec) && fs::is_directory(dir, ec) )    {
	std::multimap<std::time_t, fs::path> files, to_remove;
	for(auto it = fs::directory_iterator(dir); it != fs::directory_iterator(); ++it)   {
	  fs::path entry = *it;
	  if( fs::is_regular_file(entry) )   {
	    fs::file_time_type ftime = fs::last_write_time(entry);
	    std::time_t tim = std::chrono::duration_cast<std::chrono::seconds>(ftime.time_since_epoch()).count();
	    files.emplace(tim, entry);
	  }
	}
	while( files.size() > m_numFiles && !files.empty() )   {
	  auto it = files.begin();
	  fs::remove(it->second, ec);
	  files.erase(it);
	}
      }
      else   {
	error("File cleanup directory %s is not a valid directory!", dir.string().c_str());
      }
      m_last_cleanup = ::time(nullptr);
    }
  }
  this->processingFlag |= DF_PASSED;
  return DF_SUCCESS;
}
