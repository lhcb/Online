//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  FileSelector.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_NETSELECTOR_H
#define ONLINE_DATAFLOW_NETSELECTOR_H

// Framework include files
#include <Dataflow/EventRunable.h>
#include <Dataflow/DiskReader.h>
#include <EventData/RawFile.h>
#include <MBM/Requirement.h>
#include <RTL/rtl.h>

// C/C++ include files
#include <mutex>
#include <memory>

///  Online namespace declaration
namespace Online  {

  /// Forward declarations
  class DiskReader;

  /// Buffer Manager service for the dataflow
  /**
   * @author  Markus Frank
   * @version 1.0
   */
  class FileSelector : public EventRunable  {

  public:
    typedef Context::EventData     EventData;
    typedef MBM::Requirement       Requirement;

  protected:
    /// Property: Input buffer name
    std::string  m_readerName          {         };
    /// Property: to indicate to pause on error
    bool         m_gotoPause           {   false };
    /// Property: to indicate to pause on error
    bool         m_verifyData          {   false };
    /// Property: to sleep for some time before pause [unit: milli-seconds]
    int          m_pauseSleep          {       0 };
    /// Property: Event type of received event
    int          m_event_type          {      -1 };
    /// Property: Triggermask of received event
    std::vector<int> m_event_mask      { ~0x0, ~0x0, ~0x0, ~0x0 };
    /// Property: File list to be processed
    std::vector<std::string> m_files   {         };

    /// Property: Maximal number of events to receive between start-stop
    long         m_maxEvents           {      -1 };
    /// Property: Printout frequence
    float        m_printFreq           {     0e0 };
    /// Property: Requirement properties
    std::vector<std::string> m_Rqs     {         };

    /// Decoded requirements
    Requirement  m_request             {         };
    /// Time stamp of the last event received
    time_t       m_lastEvent           {       0 };
    /// Monitoring quantity: Number of events requested
    long         m_reqCount            {       0 };
    /// Input read failures
    long         m_inputError          {       0 };
    /// Total input byte count
    long         m_inputBytes          {       0 };
    /// Total event count
    long         m_inputCount          {       0 };

    /// Space allocator
    RawFile::HeapAllocator m_allocator {         };
    /// Input file
    RawFile                m_input     {         };
    /// Name of the current input source
    std::string            m_current   {         };
    /// Reference to the slave disk reader
    DiskReader*            m_reader    { nullptr };
    /// DiskIO handler
    std::unique_ptr<DiskReader::DiskIO> m_io {   };
    /// Number of reads in the furrent input source
    std::size_t            m_num_reads {       0 };
    /// Lock handle to suspend/resume operations
    std::mutex             m_dataLock  {         };
    bool                   m_cancelled { false   };

  public:

    /** Event access interface for sub-classes */
    /// Event access interface: Get next event from wherever
    virtual int getEvent(Context::EventData& event)  override;
    /// Event access interface: Release event. Default implementation is empty
    virtual int freeEvent(Context::EventData& event)  override;
    /// Event access interface: Shutdown event access in case of forced exit request
    virtual int shutdown()  override;
    /// Event access interface: Rearm event request to data source
    virtual int rearmEvent()  override;
    /// Event access interface: Cancel I/O operations of the dataflow component
    virtual int cancelEvents()  override;

  public:
    /// Initializing constructor
    FileSelector(const std::string& name, Context& ctxt);
    /// Default destructor
    virtual ~FileSelector();

    /// Initialize the component
    virtual int initialize()  override;
    /// Start the data flow component.
    virtual int start()  override;
    /// Stop the data flow component.
    virtual int stop()  override;
    /// Finalize the component
    virtual int finalize()  override;
    /// Incident handler implemenentation: Inform that a new incident has occured
    virtual void handle(const DataflowIncident& inc)  override;

  };
}      // end namespace Online
#endif //  ONLINE_DATAFLOW_NETSELECTOR_H

//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  FileSelector.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
//#include <Dataflow/FileSelector.h>
#include <Dataflow/DataflowManager.h>
#include <Dataflow/Incidents.h>
#include <Dataflow/Plugins.h>
#include <EventData/odin_t.h>
#include <Tell1Data/Tell1Decoder.h>
#include <RTL/Compress.h>
#include <WT/wtdef.h>

// C/C++ include files
#include <cstring>

DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_FileSelector,FileSelector)

namespace {
  template <typename BANK_TYPE> inline bool checkBank(const BANK_TYPE* bank)  {
    return
      // Check for Tell1 bank magic word or PCIE40 magic word
      (bank->magic() == BANK_TYPE::MagicPattern || bank->magic() == 0xFACE) &&
      (bank->size() >= 0) &&
      (bank->type() >= 0) && 
      (bank->type() < Online::bank_types_t::LastType);
  }
}

/// Initializing constructor
Online::FileSelector::FileSelector(const std::string& nam, Context& ctxt)
: EventRunable(nam, ctxt)
{
  declareProperty("Reader",           m_readerName   = "Dataflow__DiskReader/DiskReader");
  declareProperty("Pause",            m_gotoPause    = false);
  declareProperty("PauseSleep",       m_pauseSleep   = 0);
  declareProperty("MaxEvents",        m_maxEvents    = -1);
  declareProperty("PrintFreq",        m_printFreq    = 0.0);
  declareProperty("VerifyData",       m_verifyData   = false);
  declareProperty("EventType",        m_event_type   = EVENT_TYPE_EVENT);
  declareProperty("EventMask",        m_event_mask   = {~0x0, ~0x0, ~0x0, ~0x0});
  declareProperty("Files",            m_files);
  declareProperty("Requests",         m_Rqs);
}

/// Default destructor
Online::FileSelector::~FileSelector()   {
}

/// Initialize the NET server
int Online::FileSelector::initialize()  {
  int sc = EventRunable::initialize();
  if( sc == DF_SUCCESS )  {
    for( int j=0; j < BM_MASK_SIZE; ++j )  {
      this->m_request.trmask[j] = 0;
      this->m_request.vetomask[j] = ~0;
    }
    for( const auto& r : m_Rqs )  {
      if( !r.empty() )   {
	Requirement req;
	req.parse(r);
	for( int k=0; k < BM_MASK_SIZE; ++k )  {
	  m_request.trmask[k]   |= req.trmask[k];
	  m_request.vetomask[k] &= req.vetomask[k];
	}
	continue;
      }
      break;
    }
    while( m_event_mask.size() < 4 ) m_event_mask.push_back(~0x0);

    auto* comp = this->context.manager.manager().getManagedComponent(this->m_readerName, true);
    if( comp )   {
      this->m_reader = dynamic_cast<DiskReader*>(comp);
      if( !this->m_reader )   {
	return this->error("Cannot access DiskReader interface of component: '%s'",
			   this->m_readerName.c_str());
      }
    }
    this->m_io = std::make_unique<DiskReader::DiskIO>(*this->m_reader);
    if( m_files.empty() )   {
      this->m_io->scan_files();
    }
    else  {
      for( const auto& f : m_files )  {
	this->m_io->add_file(f, RawFile(f));
      }
    }

    declareMonitor("RequestCount", m_reqCount=0,   "Event request counter");
    declareMonitor("EventsRcv",    m_inputCount=0, "Total number of items received.");
    declareMonitor("ErrorsRcv",    m_inputError=0, "Total number of receive errors.");
    declareMonitor("BytesRcv",     m_inputBytes=0, "Total number of bytes received from clients.");
    this->m_cancelled = false;
  }
  return sc;
}

/// Initialize the event processor
int Online::FileSelector::start()  {
  int sc = this->EventRunable::start();
  this->m_lastEvent = ::time(0);
  this->m_num_reads = 0;
  if( sc == DF_SUCCESS )  {
    this->m_cancelled = false;
    sc = this->rearmEvent();
    this->m_inputCount = 0;
    this->always(sc == DF_SUCCESS
		 ? "start: Network event access initialized."
		 : "start: FAILED to initialize Network event access.");
    return sc;
  }
  return sc;
}

/// Initialize the event processor
int Online::FileSelector::stop()  {
  this->shutdown();
  this->info("Event summary [%s]: Requested:%ld Received:%ld Delivered:%ld Errors:%ld",
	     this->m_readerName.c_str(), this->m_reqCount, this->m_inputCount,
	     this->m_inputCount, this->m_inputError);
  this->m_inputCount = 0;
  return this->EventRunable::stop();
}

/// Finalize the event processor
int Online::FileSelector::finalize()  {
  this->m_io.reset();
  this->m_readerName = "";
  this->m_reader = nullptr;
  return this->EventRunable::finalize();
}

/// Cancel I/O operations of the dataflow component
int Online::FileSelector::cancelEvents()  {
  this->m_cancelled = true;
  return DF_SUCCESS;
}

/// Incident handler implemenentation: Inform that a new incident has occured
void Online::FileSelector::handle(const DataflowIncident& inc)   {
  this->EventRunable::handle(inc);
}

/// Shutdown event access in case of forced exit request
int Online::FileSelector::shutdown()   {
  this->m_lastEvent = time(0);
  return DF_SUCCESS;
}

/// Release event
int Online::FileSelector::freeEvent(Context::EventData& event)   {
  event.length = 0;
  return DF_SUCCESS;
}

/// Rearm event request to data source
int Online::FileSelector::rearmEvent()  {
  if( m_maxEvents < 0 || m_inputCount < m_maxEvents )   {
    return DF_SUCCESS;
  }
  // If we have an event limit set: make sure it is obeyed
  // and that we go to PAUSED if it is requested.
  // We should never get here if no event limit is set!
  this->warning("Event limit reached. No more requests to %s.",this->m_readerName.c_str());
  if( m_gotoPause )   {
    if( m_pauseSleep > 0 ) ::lib_rtl_sleep(m_pauseSleep);
    fireIncident("DAQ_PAUSE");
  }
  return DF_SUCCESS;
}

/// Get next event from wherever
int Online::FileSelector::getEvent(Context::EventData& ev)   {
  std::lock_guard<std::mutex> lck (this->m_dataLock);
  if( this->m_cancelled )  {
    return DF_CANCELLED;
  }
  ++this->m_reqCount;
  if( !m_input.isOpen() )  {
    if( !m_current.empty() )   {
      this->info("Close input %s after %ld events reads.", m_current.c_str(), m_num_reads);
      m_input.close();
    }
    m_input = m_io->openFile();
    if( !m_input.isOpen() )   {
      this->warning("No more files to be processed. End of event input.");
      fireIncident("DAQ_SHUTDOWN");
      return DF_CANCELLED;
    }
    if( !compress::init_data_reading(m_input) )  {
      this->error("Failed to initialize data access for %s", m_input.cname());
    }
    m_current = m_input.name();
    m_num_reads = 0;
  }
  auto result = m_reader->load_event_data(m_allocator, m_input);
  if( result.type != RawFile::MDF_INPUT_TYPE )  {
    // Error
    ++this->m_inputError;
  }
  ++this->m_inputCount;
  ++this->m_num_reads;
  ev.length  = result.length;
  ev.header  = (event_header_t*)this->m_allocator.data();
  ev.data    = (char*)this->m_allocator.data();
  ev.type    = this->m_event_type;
  ev.mask[0] = this->m_event_mask[0];
  ev.mask[1] = this->m_event_mask[1];
  ev.mask[2] = this->m_event_mask[2];
  ev.mask[3] = this->m_event_mask[3];
  ev.release = 0;
  if( this->m_verifyData )  {
    auto [start, end] = ev.header->data_frame();
    if( !checkRawBanks(start, end, false, false) )  {
      this->error("Event: %ld Event in file: %ld Start:%p End:%p  Bad bank found!",
		  this->m_inputCount, this->m_num_reads, start, end);
      /// Print TELL1 header
      auto header_lines = event_print::headerData(ev.header);
      for( const auto& l : header_lines )
	this->error("  %s", l.c_str());
      /// Print ODIN bank
      for( auto* p=start; p < end; )  {
	auto* bank = (raw_bank_offline_t*)p;
	if( bank->type() == bank_types_t::ODIN )  {
	  auto o = std::make_pair(bank, bank->data());
	  auto odin_lines = event_print::odin_data(o);
	  for( const auto& l : odin_lines )
	    this->error("  |%s", l.c_str());    
	}
	if( bank->totalSize() <= 0 )  break;
	p += bank->totalSize();
      }
      /// Print bank headers
      for( auto* p=start; p < end; )  {
	auto* bank = (raw_bank_offline_t*)p;
	if( !checkBank(bank) )  {
	  this->error("  | Bad Bank: %p  -> %s",
		      (void*)bank, event_print::bankHeader(bank).c_str());
	}
	else  {
	  this->info("  |     Bank: %p  -> %s",
		     (void*)bank, event_print::bankHeader(bank).c_str());
	}
	if( bank->totalSize() <= 0 )  {
	  this->error("  | Negative raw bank size: %d bytes. Stop scan.", bank->totalSize());
	  break;
	}
	p += bank->totalSize();
      }
    }
  }
  return DF_SUCCESS;
}
