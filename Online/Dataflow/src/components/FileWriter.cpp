//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  Filewriter.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Beat Jost
//==========================================================================

/// Framework includes
#include "FileWriter.h"
#include <Dataflow/Incidents.h>
#include <Tell1Data/Tell1Decoder.h>

#include <RTL/strdef.h>
#include <RTL/rtl.h>

#include <ctime>
#include <regex>
#include <cerrno>
#include <fstream>
#include <filesystem>

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/statvfs.h>
#include <dim/dic.hxx>

namespace fs = std::filesystem;

using namespace Online;

class FileWriter::SteeringInfo : DimInfo    {
public:
  int&        variable;
  FileWriter& parent;

  SteeringInfo(const std::string& nam, int &var, FileWriter& par)
    : DimInfo(nam.c_str(), -1), variable(var), parent(par)
  {
  }
  void infoHandler() override  {
    int value = getInt();
    if( value == 0 )      {
      parent.m_curr_run = 0;
    }
    else if( value == 1 ) {
      parent.clearCounters();
      parent.m_run_step = parent.m_run_step + 1;
    }
    else  {
      return;
    }
    variable = value;
    parent.debug("+++ Stream: %s DIM steering flag set to: %d",
		 parent.m_stream.c_str(), variable);
  }
};

/// RunDesc class definition
/**
 *  @author Beat Jost
 *  @version 1.0
 */
class FileWriter::RunDesc  {
public:
  FileDescr   *file_desc        { nullptr };
  long         num_files        { 0 };
  long         events_written   { 0 };
  long         bytes_written    { 0 };
  unsigned int run_number       { 0 };
  RunDesc() = default;
  RunDesc(int runno) : run_number(runno) {}
};

/// FileDescr class definition
/**
 *  @author Beat Jost
 *  @version 1.0
 */
class FileWriter::FileDescr    {
public:
  std::string    fileName;
  RunDesc       *runDescriptor  { nullptr };
  unsigned int   run_number     { 0 };
  unsigned int   seq_number     { 0 };
  int            fileHandle     { 0 };
  int            state          { 0 };
  long           events_written { 0 };
  long           bytes_written  { 0 };
  std::time_t    closeAt        { 0 };
  std::time_t    lastWrite      { 0 };
  FileDescr(int handle, const std::string& name, int st, RunDesc *r)
    : fileName(name), runDescriptor(r), run_number(r->run_number),
      seq_number(r->num_files+1), fileHandle(handle), state(st)
  {
  }
};

// Standard Constructor
FileWriter::FileWriter(const std::string& nam, Context& ctxt)
  : Processor(nam, ctxt)
{
  this->m_filePrefix = "/alignment/${PARTITION}/${STREAM}/${RUN}/Run_${RUN}_${NODE}_${SEQ}.dat";
  this->declareProperty("FilePrefix",     this->m_filePrefix);
  this->declareProperty("DeviceList",     this->m_deviceList     = { "/hlt2"});
  this->declareProperty("SizeLimit",      this->m_sizeLimit      =  500);
  this->declareProperty("EventLimit",     this->m_eventLimit     = -1);
  this->declareProperty("FileCloseDelay", this->m_fileCloseDelay =  10);
  this->declareProperty("MaxFilesOpen",   this->m_maxFilesOpen   =  1000);
  this->declareProperty("MaxEvents",      this->m_maxEvents      = -1);
  this->declareProperty("EventFraction",  this->m_evfrac         =  1.0);
  this->declareProperty("DIMSteering",    this->m_dimSteering    =  0);
  this->declareProperty("DIMMonitoring",  this->m_dimMonitoring  =  0);
  this->declareProperty("NodePattern",    this->m_nodePattern    =  "..eb..");
  this->declareProperty("ExitOnError",    this->m_ExitOnError    =  true);
  this->declareProperty("Stream",         this->m_stream         =  "RAW");
  this->declareProperty("Activity",       this->m_activity       =  "");
  this->declareProperty("PartitionName",  this->m_partitionName  =  "LHCb");
  this->declareProperty("UseAllRuns",     this->m_use_all_runs   =  0);
  this->m_thread = std::make_unique<std::thread>([this] { this->close_files(); });
}

// Standard Destructor
FileWriter::~FileWriter()   {
  this->m_texit = true;
  this->m_thread->join();
  this->m_thread.reset();
}

int FileWriter::initialize()   {
  std::error_code ec;
  int sc = Processor::initialize();
  std::string node = RTL::nodeNameShort();
  this->m_writeEnabled = 1;
  if( !this->m_nodePattern.empty() )   {
    const char* pattern = this->m_nodePattern.c_str();
    auto reg_nodePattern = std::regex(pattern, std::regex_constants::icase);
    this->m_writeEnabled = std::regex_search(node, reg_nodePattern) ? 1 : 0;
  }
  if (sc == DF_SUCCESS)    {
    std::vector<std::string> to_keep;
    this->subscribeIncident("DAQ_RUN_CHANGE");
    this->declareMonitor("EventsIn",     this->m_events_in = 0L,     "Number of Events received.");
    this->declareMonitor("EventsOut",    this->m_events_out = 0L,    "Number of Events written.");
    this->declareMonitor("BytesOut",     this->m_bytesOut = 0L,      "Number of Bytes Written to File");
    this->declareMonitor("FilesOpen",    this->m_filesOpen,          "Number of files opened");
    this->declareMonitor("FilesClosed",  this->m_filesClosed,        "Number of files closed");
    this->declareMonitor("WriteEnabled", this->m_writeEnabled,       "Number bad frames");
    this->declareMonitor("BadFrames",    this->m_num_bad_frames = 0, "Number bad frames");
    this->declareMonitor("Step",         this->m_run_step = 0,       "Sequence number of run-step");
    this->m_sizeLimit *= 1024 * 1024;
    for( std::string dir_name : m_deviceList )  {
      dir_name = RTL::str_replace(dir_name, "${NODE}",      node);
      dir_name = RTL::str_replace(dir_name, "${STREAM}",    this->m_stream);
      dir_name = RTL::str_replace(dir_name, "${ACTIVITY}",  this->m_activity);
      dir_name = RTL::str_replace(dir_name, "${PARTITION}", this->m_partitionName);
      this->info("Check directory: %s", dir_name.c_str());

      fs::path dir(dir_name);
      if( fs::exists(dir, ec) )    {
	auto path = fs::path(dir).lexically_normal();
	this->info("Use    directory: %s", path.c_str());
	to_keep.push_back(path.string());
      }
      else if( fs::create_directories(dir, ec) || ec.value() == EEXIST )    {
	auto path = fs::path(dir).lexically_normal();
	this->info("Create directory: %s", path.c_str());
	to_keep.push_back(path.string());
      }
      else    {
	this->error("+++ Cannot create directory %s", dir.c_str());
      }
    }
    this->m_deviceList = to_keep;
    return DF_SUCCESS;
  }
  return this->error("+++ Failed to initialize service base class.");
}

int FileWriter::start()   {
  this->Processor::start();
  if( this->m_dimSteering != 0 )    {
    if( !this->m_steeringSvc )	{
      std::string svcnam = this->m_partitionName + "/" + name + "/Control";
      this->m_steeringSvc = std::make_unique<SteeringInfo>(svcnam, this->m_steeringData, *this);
      this->debug("+++ Created DIM steering info %s", svcnam.c_str());
    }
    this->clearCounters();
    this->m_maxEvents  = -1;
  }
  this->m_stopped = false;
  return DF_SUCCESS;
}

int FileWriter::stop()    {
  // Scan the run list and check for the files which were not written the longest.
  this->m_stopped = true;
  {
    std::lock_guard<std::mutex> lock(this->m_runlock);
    for(const auto& r : this->m_runList )   {
      if( r.second->file_desc )   {
	this->markClose(r.second->file_desc, 0);
      }
    }
    this->m_runList.clear();
  }
  return this->Processor::stop();
}

int FileWriter::finalize()   {
  this->m_run_step = 0;
  this->clearCounters();
  this->m_stopped = true;
  this->m_steeringSvc.reset();
  this->clearCounters();
  this->unsubscribeIncidents();
  this->undeclareMonitors();
  return this->Processor::finalize();
}

/// Incident handler implemenentation: Inform that a new incident has occured
void FileWriter::handle(const DataflowIncident& inc)  {
  if (inc.type == "DAQ_RUN_CHANGE" )  {
    this->clearCounters();
  }
}

/// Reset relevant counters for steering
void FileWriter::clearCounters()   {
  this->m_events_out = 0;
  this->m_events_in  = 0;
  this->m_bytesOut   = 0;
}

/// Process single event. Input buffer is ALWAYS in mdf event or burst format.
int FileWriter::execute(const Context::EventData& e)   {
  unsigned int runno  = 0;
  int          status = DF_SUCCESS;
  char*        beg    = (char*)e.data;
  char*        end    = beg + e.length;
  Context::EventData event;
  while( beg < end )  {
    auto* hdr = (event_header_t*)beg;
    if( !hdr->is_mdf() )    {
      this->error("+++ Event buffer does not have MDF format. Cannot write output!");
      if( ++this->m_num_bad_frames > 3 ) ::lib_rtl_sleep(10000);
      return DF_SUCCESS;
    }
    auto  trigger_mask = hdr->subHeader().H1->triggerMask();
    runno = hdr->subHeader().H1->runNumber();
    this->m_events_in++;
    if( this->m_dimSteering != 0 && this->m_steeringData == 0 )   {
      goto Close;
    }
    else if( !this->m_writeEnabled || this->m_deviceList.empty() || this->m_stopped )   {
      goto Close;
    }
    else if( runno > 0 && runno < this->m_curr_run )    {
      goto Close;
    }
    else if( runno > 0 && runno != this->m_curr_run )    {
      this->m_curr_run = runno;
      this->queue_close_files( runno );
      this->clearCounters();
      /// Reset event counter if for every run a sample should be extracted
      if( this->m_use_all_runs )  {
	this->m_evt_number = 0;
      }
    }
    else if( this->m_maxEvents > 0 && this->m_evt_number >= this->m_maxEvents )   {
      goto Close;
    }
    event.release = 0;
    event.data    = beg;
    event.header  = hdr;
    event.length  = hdr->size0();
    event.type    = e.type;
    event.mask[0] = trigger_mask[0];
    event.mask[1] = trigger_mask[1];
    event.mask[2] = trigger_mask[2];
    event.mask[3] = trigger_mask[3];
    /// Advance event start to next MDF
    beg          += hdr->size0();

    /// Check requirement match
    if( !matchRequirements(event) )
      continue;

    m_evt_number++;
    if( this->m_evfrac > 0e0 && this->m_evfrac < 1e0 )   {
      double frac = double(::rand()) / double(RAND_MAX);
      if( frac > this->m_evfrac )
	continue;
    }
    std::lock_guard<std::mutex> lock(this->m_runlock);
    if( this->m_stopped )   {
      return DF_SUCCESS;
    }
    RunDesc *rd = this->m_runList[runno];
    if( rd == nullptr )    {
      rd = this->m_runList[runno] = new RunDesc(runno);
      this->createMonitoringInfo(runno);
    }
    else if( this->m_maxEvents > 0 && rd->events_written >= this->m_maxEvents )  {
      goto Close;
    }
    /// Event accepted. Write it to output
    /// If there is no file attached to this run: open a new one
    if( rd->file_desc == nullptr )    {
      rd->file_desc = this->openFile(rd);
      ++rd->num_files;
      ++this->m_filesOpen;
      this->m_run_step = 0;
    }
    else if( this->m_sizeLimit  > 0 && rd->file_desc->bytes_written >= this->m_sizeLimit )
      goto Close;
    else if( this->m_eventLimit > 0 && rd->file_desc->events_written >= this->m_eventLimit )
      goto Close;

    status = this->writeEvent(rd, hdr);
    if( status != DF_SUCCESS )   {
      ++this->m_num_bad_frames;
      return status;
    }
    this->m_num_bad_frames = 0;
    this->processingFlag |= DF_PASSED;
  }
  return status;

 Close: // Escape line if no event need to written
  return queue_close_files(runno);
}

/// Queue no longer files for closing
int FileWriter::queue_close_files(unsigned int runno)  {
  std::time_t now = ::time(0);
  std::vector<std::pair<unsigned int,FileDescr*> > files;
  std::lock_guard<std::mutex> lock(this->m_runlock);
  for( auto& r : m_runList )   {
    bool mark_close = false;
    auto* fd = r.second->file_desc;
    if( !fd )
      continue;
    else if( runno > 0 && r.first > runno )
      continue;
    else if( this->m_sizeLimit  > 0 && fd->bytes_written >= this->m_sizeLimit )
      mark_close = true;
    else if( this->m_eventLimit > 0 && fd->events_written >= this->m_eventLimit )
      mark_close = true;
    else if( now > (fd->lastWrite+this->m_fileIdleDelay) )
      mark_close = true;
    else if( r.first < (runno+1) )
      mark_close = true;

    if( mark_close )  {
      this->debug("+++ Run: %d Queue close [seq:%5d, events: %ld, %7d kB] %s",
		  fd->run_number, fd->seq_number,
		  fd->events_written, int(fd->bytes_written/1024),
		  fd->fileName.c_str());
      files.emplace_back(std::make_pair(r.first,r.second->file_desc));
      r.second->file_desc = nullptr;
    }
  }
  for(auto i=files.begin(); i != files.end(); ++i)   {
    this->markClose(i->second, 0);
  }
  return DF_SUCCESS;
}

/// Write single event to disk. Input buffer is ALWAYS in mdf bank format.
int FileWriter::writeEvent(RunDesc* run_desc, const event_header_t* mdf)   {
  std::time_t  now   = ::time(nullptr);
  unsigned int runno = run_desc->run_number;
  auto* fileDesc = run_desc->file_desc;

  if (fileDesc == 0)   {
    auto ec = std::make_error_code(std::errc(errno));
    this->error("+++ File Write Error: Cannot open output file: Run: %d Error: %d: [%s]",
		runno, ec.value(), ec.message().c_str());
    ::lib_rtl_sleep(2000);
    return DF_ERROR;
  }

  ssize_t status = this->write(fileDesc, mdf, mdf->recordSize());
  if (status != -1)    {
    this->m_events_out       += 1;
    this->m_bytesOut         += mdf->recordSize();
    fileDesc->lastWrite       = now;
    fileDesc->bytes_written  += mdf->recordSize();
    fileDesc->events_written += 1;
    run_desc->bytes_written  += mdf->recordSize();
    run_desc->events_written += 1;

    if     ( this->m_sizeLimit  > 0 && fileDesc->bytes_written >= this->m_sizeLimit )
      this->markClose(fileDesc, this->m_fileCloseDelay);
    else if( this->m_eventLimit > 0 && fileDesc->events_written >= this->m_eventLimit )
      this->markClose(fileDesc, this->m_fileCloseDelay);
    else if( this->m_maxEvents  > 0 && run_desc->events_written >= this->m_maxEvents )
      this->markClose(fileDesc, this->m_fileCloseDelay);
    return DF_SUCCESS;
  }
  return this->error("+++ Failed to write event for run:%d to file %s.",
		     runno, fileDesc->fileName.c_str());
}

void FileWriter::close_files()  {
  while( !this->m_texit )    {   {
      /// This needs to be protected. Beware of scope!
      std::lock_guard<std::mutex> lock(this->m_listlock);
      std::time_t now = ::time(0);
      for( auto i = this->m_fileCloseList.begin(); i != this->m_fileCloseList.end(); ++i )  {
	auto* fd = *i;
	if( fd && now > fd->closeAt )    {
	  this->debug("+++ Run: %d Close file. [seq:%5d, events: %ld, %7d kB] %s",
		      fd->run_number, fd->seq_number,
		      fd->events_written, int(fd->bytes_written/1024),
		      fd->fileName.c_str());
	  ::close(fd->fileHandle);
	  ++this->m_filesClosed;
	  delete fd;
	  this->m_fileCloseList.erase(i);
	  if( 0 == (this->m_filesClosed%100) )   {
	    this->info("+++ Wrote a total of %ld files Events: %8ld [%7.1f GB]",
		       this->m_filesClosed, this->m_events_out,
		       double(this->m_bytesOut)/double(1024*1024*1024));
	  }
	  break;
	}
      }
    }
    ::lib_rtl_sleep(200);
  }
}

void FileWriter::markClose(FileDescr* fd, int delay)   {
  if( fd )    {
    /// This needs to be protected
    std::lock_guard<std::mutex> lock(this->m_listlock);
    fd->runDescriptor->file_desc = nullptr;
    fd->closeAt = ::time(0) + delay;
    fd->state   = C_CLOSED;
    this->m_fileCloseList.push_back(fd);
    --this->m_filesOpen;
    this->debug("+++ Run: %d Mark close. [seq:%5d, events: %ld, %7d kB] %s",
		  fd->run_number, fd->seq_number,
		  fd->events_written, int(fd->bytes_written/1024),
		  fd->fileName.c_str());
  }
}

FileWriter::FileDescr *FileWriter::openFile(RunDesc* rd)    {
  char text[80];
  std::string  node = RTL::nodeNameShort();
  unsigned int runn = rd->run_number;
  std::string  file_name = this->m_filePrefix;
  std::string  file_time = this->fileTime();

  file_name  = RTL::str_replace(file_name,  "${TIME}",      file_time);
  file_name  = RTL::str_replace(file_name,  "${TIMET}",     this->fileTimeT());
  file_name  = RTL::str_replace(file_name,  "${NODE}",      node);
  file_name  = RTL::str_replace(file_name,  "${STREAM}",    this->m_stream);
  file_name  = RTL::str_replace(file_name,  "${ACTIVITY}",  this->m_activity);
  file_name  = RTL::str_replace(file_name,  "${PARTITION}", this->m_partitionName);
  ::snprintf(text, sizeof(text), "%010d", int((runn/1000)*1000));
  file_name  = RTL::str_replace(file_name,  "${RUN1000}",   text);
  ::snprintf(text, sizeof(text), "%010d", runn);
  file_name  = RTL::str_replace(file_name,  "${RUN}",       text);
  ::snprintf(text, sizeof(text), "%010ld", rd->num_files);
  file_name  = RTL::str_replace(file_name,  "${SEQ}",       text);
  ::snprintf(text, sizeof(text), "%06d", ::lib_rtl_pid());
  file_name  = RTL::str_replace(file_name,  "${PID}",       text);

  int indx = getDevice(this->m_deviceList);
  if( indx < 0 )    {
    if( m_ExitOnError )   {
      this->fatal("===========> FATAL: Cannot find a device with free space. Exiting...");
      ::exit(0);
    }
    else    {
      this->info("+++ Cannot find a device with free space.");
      return nullptr;
    }
  }

  std::string flname  = this->m_deviceList[indx] + "/" + file_name;
  fs::path file = fs::path(flname).lexically_normal();
  fs::path dir  = file.parent_path();
  if( !dir.empty() && !fs::exists(dir) )   {
    std::error_code ec;
    if( !fs::create_directories(dir, ec) )  	{
      // Another node may have created the directory in the meantime. Double check!
      if( !fs::exists(dir, ec) )   {
	this->error("+++ Cannot Create Directory %s %d: [%s]",
		    dir.c_str(), ec.value(), ec.message().c_str());
	return nullptr;
      }
    }
  }

  int fh = ::open(file.c_str(),
		  O_RDWR + O_CREAT + O_APPEND + O_LARGEFILE + O_NOATIME,
		  S_IRWXU | S_IRWXG | S_IRWXO);
  if( fh ==-1 )    {
    auto ec = std::make_error_code(std::errc(errno));
    std::string fmt = "===========> Cannot open file %s %d: [%s]";
    if (errno == EROFS)  {
      fmt = "===========> File System is Read-Only. Cannot open file %s %d: [%s]";
    }
    this->warning(fmt.c_str(), file.c_str(), errno, ec.message().c_str());
    return nullptr;
  }

  FileDescr *desc = new FileDescr(fh, file_name, C_OPEN, rd);
  this->debug("+++ Open file [%s] Seq:%5d  %s",
	      this->m_stream.c_str(), desc->seq_number, file.c_str());
  return desc;
}

int FileWriter::getDevice(std::vector<std::string>& devlist)  {
  std::ifstream infile("/proc/mounts");
  std::string   line;

  std::map<std::string, fsblkcnt_t> devFree;

  // Get the amount of free space in all mount points that contain a
  // device
  while( getline(infile,line) ) {
    // The second item in the line is the mount point
    std::size_t idx = line.find(' ');
    std::size_t idq = line.find(' ', idx + 1);
    std::string root = line.substr(idx + 1, idq);

    for( const auto& dev : m_deviceList )   {
      // Use std::string::rfind to search for string root starting at
      // most at index 0, i.e. check if string dev starts with string
      // root. Also check that the mount point is not read-only
      if( dev.rfind(root, 0) != std::string::npos && line.find(" ro,", idq + 1) == std::string::npos )  {
        struct statvfs fsstat;
        int stat = ::statvfs(root.c_str(), &fsstat);
        if (stat == 0) {
          devFree.emplace(dev, fsstat.f_bfree);
        }
      }
    }
  }
  infile.close();

  // Find the device with the most free space
  int maxindx = 0;
  fsblkcnt_t fblock = 0;
  for( int i = 0; i < static_cast<int>(devlist.size()); i++ )  {
    std::string dev = devlist[i];
    if( devFree.count(dev) && devFree.find(dev)->second > fblock ) {
      fblock = devFree.find(dev)->second;
      maxindx = i;
    }
  }
  return maxindx;
}

void FileWriter::createMonitoringInfo(unsigned int runno)   {
  if( this->m_dimMonitoring )  {
    RunDesc *rd = this->m_runList[runno];
    std::string namePref = std::to_string(runno);
    std::string mnam = namePref + "/NoFiles";
    this->declareMonitor(namePref + "NoFiles", rd->num_files, "Number of Files");
    mnam = namePref + "/Events";
    this->declareMonitor(mnam, rd->events_written, "Number of Events");
    mnam = namePref + "/Bytes";
    this->declareMonitor(mnam, rd->bytes_written, "Number of Bytes");
  }
}

std::string FileWriter::fileTime()   {
  struct timeval tv;
  char buffer[128];
  ::gettimeofday(&tv, nullptr);
  struct tm *timeinfo = ::localtime(&tv.tv_sec);
  ::strftime(buffer, sizeof(buffer), "%Y%m%d-%H%M%S-", timeinfo);
  ::snprintf(buffer+::strlen(buffer), sizeof(buffer)-::strlen(buffer),"%03ld", tv.tv_usec/1000);
  return buffer;
}

std::string FileWriter::fileTimeT()   {
  struct timeval tv;
  char buffer[128];
  ::gettimeofday(&tv, nullptr);
  struct tm *timeinfo = ::localtime(&tv.tv_sec);
  ::strftime(buffer, sizeof(buffer), "%Y-%m-%dT%H:%M:%S.", timeinfo);
  ::snprintf(buffer+::strlen(buffer), sizeof(buffer)-::strlen(buffer),"%03ld", tv.tv_usec/1000);
  return buffer;
}

void FileWriter::handleFileWriteError()  {
#if 0
  std::string node = RTL::nodeNameShort();
  for (unsigned int i = 0; i < node.size(); i++)
    node[i] = toupper(node[i]);
  std::string cmdname = node + "_MEPRx_01/setOverflow";
  DimClient::sendCommand(cmdname.c_str(), 2);
#endif
}

ssize_t FileWriter::write(FileDescr* dsc, const void *buf, size_t n)   {
  const char *cbuf = (const char*) buf;
  std::size_t towrite = n;
  int    status    = 0;
  int    nintr_max = 2;
  int    null_max = 2;
  int    fd = dsc->fileHandle;

  while( towrite > 0 )    {
    status = ::write(fd, cbuf, towrite);
    if( status > 0 )	{
      towrite -= status;
      cbuf    += status;
      continue;
    }
    else if( status == 0 && --null_max < 0 )   {
      this->info("0 Bytes written! Ignoring MEP");
      status = -1;
      return status;
    }
    else if( status < 0 )	{
      auto err = std::make_error_code(std::errc(errno));
      if( err.value() == EIO || err.value() == ENOSPC )    {
	this->info("File Write Error (IO or NoSpace): Errno = %d [%s]",
		   err.value(), err.message().c_str());
	this->handleFileWriteError();
	status = -1;
	return status;
      }
      else if( err.value() == EINTR )    {
	nintr_max--;
	if (nintr_max > 0)   {
	  continue;
	}
	else	{
	  status = -1;
	  return status;
	}
      }
      else    {
	this->info("File Write Error: Errno = %d [%s]",
		   err.value(), err.message().c_str());
	this->handleFileWriteError();
	status = -1;
	return status;
      }
    }
  }
  status = 0;
  return status;
}
