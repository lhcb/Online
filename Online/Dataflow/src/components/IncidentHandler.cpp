//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  Incidents.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include "IncidentHandler.h"
#include <Dataflow/DataflowCatalog.h>

DATAFLOW_MAKE_CATALOG(IncidentCatalog,std::string,std::vector<Online::DataflowComponent*>)

/// Initializing constructor
Online::IncidentHandler::IncidentHandler(const std::string& nam, Context& ctxt)
  : Component(nam,ctxt)
{
  m_catalog = new IncidentCatalog();
}

/// Default destructor
Online::IncidentHandler::~IncidentHandler()   {
  detail::deletePtr(m_catalog);
}

/// Unsubscribe component from all incidents
int Online::IncidentHandler::unsubscribe(Component* c)   {
  if( c )   {
    for(auto i=begin(*m_catalog); i!=end(*m_catalog); ++i)  {
      auto& items = (*i).second;
      for( auto j=begin(items); j!=end(items); ++j )  {
	if( (*j) == c ) {
	  items.erase(j);
	  break;
	}
      }
    }
    return DF_SUCCESS;
  }
  return error("Cannot handle a invalid component!");
}

/// Unsubscribe component from a specific incident
int Online::IncidentHandler::unsubscribe(Component* c, const std::string& type)   {
  if( c )   {
    auto i= m_catalog->find(type);
    if( i != end(*m_catalog) )  {
      auto& items = (*i).second;
      for( auto j=begin(items); j!=end(items); ++j )  {
	if( (*j) == c ) {
	  items.erase(j);
	  return DF_SUCCESS;
	}
      }
    }
    return DF_ERROR;
  }
  return error("Cannot handle a invalid component!");
}

/// Subscribe component to a specific incident
int Online::IncidentHandler::subscribe(Component* c, const std::string& type)   {
  if( c )   {
    auto i= m_catalog->find(type);
    if( i == end(*m_catalog) )  {
      i = m_catalog->insert(make_pair(type, IncidentCatalog::mapped_type())).first;
    }
    auto comp = find((*i).second.begin(),(*i).second.end(),c);
    if( comp != (*i).second.end() )  {
      warning("Component %s is already subscribed to:%s",c->name.c_str(),type.c_str());
    }
    (*i).second.push_back(c);
    return DF_SUCCESS;
  }
  return error("Cannot handle a invalid component!");
}

/// Fire an incident to notify other components
int Online::IncidentHandler::fire(Component* , const DataflowIncident& incident)   {
  const auto i= m_catalog->find(incident.type);
  if( i != end(*m_catalog) )  {
    const auto& items = (*i).second;
    for( auto j=begin(items); j!=end(items); ++j )  {
      Component* c = *j;
      if( c ) c->handle(incident);
    }
  }
  return DF_SUCCESS;
}
