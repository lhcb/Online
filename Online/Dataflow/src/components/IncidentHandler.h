//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  IncidentHandler.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_INCIDENTHANDLER_H
#define ONLINE_DATAFLOW_INCIDENTHANDLER_H

// Framework include files
#include <Dataflow/DataflowComponent.h>
#include <Dataflow/Incidents.h>

#include <string>

///  Online namespace declaration
namespace Online  {

  // Forward declarations
  class IncidentCatalog;

  /// Incident handler for the online dataflow
  /**
   *
   * @author  Markus Frank
   * @version 1.0
   */
  class IncidentHandler : public DataflowComponent, public DataflowContext::Incident  {
  private:
    /// Container of all joboptions items
    IncidentCatalog* m_catalog;

  public:
    /// Initializing constructor
    IncidentHandler(const std::string& name, Context& framework);
    /// Default destructor
    virtual ~IncidentHandler();
    /// Unsubscribe component from all incidents
    virtual int unsubscribe(DataflowComponent* c)  override;
    /// Unsubscribe component from a specific incident
    virtual int unsubscribe(DataflowComponent* c, const std::string& type)  override;
    /// Subscribe component to a specific incident
    virtual int subscribe(DataflowComponent* c, const std::string& type)  override;
    /// Fire an incident to notify other components
    virtual int fire(DataflowComponent* c, const DataflowIncident& incident)  override;
  };
}      // end namespace Online

#endif //  ONLINE_DATAFLOW_INCIDENTHANDLER_H
