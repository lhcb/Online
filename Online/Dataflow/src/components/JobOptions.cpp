//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  JobOptions.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include "JobOptions.h"
#include <Dataflow/Printout.h>
#include <Options/Analyzer.h>
#include <Options/Messages.h>
#include <Options/Catalog.h>
#include <Options/Units.h>
#include <Options/PragmaOptions.h>
#include <Options/Node.h>
#include <RTL/rtl.h>

#include <iostream>
#include <sstream>

using namespace Online;
namespace gp = Gaudi::Parsers;

/// Raw options type
typedef std::map<std::string,std::vector<std::pair<std::string,std::string> >* > RawOptions;

/// Initializing constructor
JobOptions::JobOptions(const std::string& nam, Context& ctxt)
  : DataflowComponent(nam, ctxt)
{
  outputLevel = WARNING;
}

/// Default destructor
JobOptions::~JobOptions()   {
  m_catalog.clear();
}

/// Retrieve all clients with options
std::vector<std::string> JobOptions::getClients() const   {
  std::vector<std::string> result;
  result.reserve(m_catalog.size());
  for( const auto& i : m_catalog ) result.emplace_back(i.first);
  return result;
}

/// Add a client property to the options catalog
void JobOptions::addProperty(const std::string& client, Prop&& p)  {
  m_catalog[client].emplace_back(std::move(p));
}

/// Access the property of one client. Returns NULL if none
const JobOptions::PropertiesT& JobOptions::getProperties(const std::string& client) const  {
  static PropertiesT empty;
  auto p = m_catalog.find(client);
  return (p != m_catalog.end()) ? ((*p).second) : empty;
}

/// Output method to dump options
std::ostream& JobOptions::fillStream( std::ostream& o ) const   {
  // loop over the clients:
  for( const auto& iclient : m_catalog )  {
    o << "Client '" << iclient.first << "'" << std::endl;
    for( const auto& p : iclient.second )
      o << "\t" << p.first << " = " << p.second << std::endl;
  }
  return o;                                                   // RETURN
}

/// Clear all stored properties
void JobOptions::clear()   {
  m_catalog.clear();
}

/// Load a job options file
int JobOptions::load(const std::string& file, const std::string& path)   {
  std::string       search_path = path;
  gp::Messages      messages(outputLevel,::lib_rtl_output);
  gp::Catalog       catalog;
  gp::Units         units;
  gp::PragmaOptions pragma;
  gp::Node          ast;
  if( search_path.empty() )   {
    const char* p = std::getenv("JOBOPTIONS_PATH");
    if( p ) search_path = p;
  }
  messages.AddDebug("Reading options from the file '"+file+"'");
  int sc = gp::ReadOptions(file, search_path, &messages, &catalog, &units, &pragma, &ast);
  // --------------------------------------------------------------------------
  if( sc )    {
    if( pragma.IsPrintOptions() ) {
      std::cout << "Print options" << std::endl << catalog << std::endl;
    }
    if( pragma.IsPrintTree() ) {
      std::cout << "Print tree:" << std::endl << ast.ToString() << std::endl;
    }
    messages.AddInfo("Job options successfully read in from "+file);
    for( const auto&  cl : catalog )   {
      const std::string& client = cl.first;
      for( const auto& c : cl.second )   {
	addProperty(client, {c.NameInClient(),c.ValueAsString()});
      }
    }
    return DF_SUCCESS;
  } else {
    messages.AddFatal("Job options errors.");
  }
  // ----------------------------------------------------------------------------
  return DF_ERROR;
}

/// Set the component's properties
int JobOptions::setProperties(const std::string& client, PropertyManager& props)   {
  if( !client.empty() )  {
    int status = DF_SUCCESS;
    const auto& client_props = getProperties(client);
    for( const auto& p : client_props )  {
      try  {
	props[p.first].str(p.second);
      }
      catch( const std::exception& e )  {
	status = error(e,"Failed to set property %s.%s = %s.",
		       client.c_str(), p.first.c_str(), p.second.c_str());
      }
      catch( ... )  {
	status = error("Failed to set property %s.%s = %s.",
		       client.c_str(), p.first.c_str(), p.second.c_str());
      }
    }
    return status;
  }
  return error("Cannot set properties of an invalid component!");
}
