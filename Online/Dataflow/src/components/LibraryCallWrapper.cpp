//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : Online/GaudiOnlineTests
//
//  Author     : Markus Frank
//==========================================================================

// Include files from the framework
#include <Dataflow/ExternalCallWrapper.h>

/// C/C++ include files
#include <stdexcept>
#include <memory>
#include <thread>

/// Online namespace declaration
namespace Online    {

  /// Generic class to wrapp standalone non-dataflow executable within dataflow application
  /** Generic class to wrapp standalone non-dataflow executable within dataflow application
   *
   * \version 1.0
   * \author  M.Frank
   */
  class LibraryCallWrapper : public ExternalCallWrapper  {
  public:
    
    /// Property: arguments for the process to be executed
    std::vector<std::string>  arguments;
    /// Property: function name of the process to be executed
    std::string               function_name;
    /// Property: process name of the process to be executed
    std::string               library_name;
    /// Property: Log file for sub-process
    std::string               log_file;
    /// Property: replacement tag to get proper outputs for qmtest
    std::map<std::string,std::string> replacements;
    /// Property: Sleep before start
    int                       sleep_pre_start  {    0 };
    /// Property: Sleep after start
    int                       sleep_start      {    0 };
    /// Property: Sleep after stop
    int                       sleep_stop       {    0 };
    /// Property: Print command when task is started/stopped
    bool                      print_cmd        { true };
    /// Property: Execute the library call synchronously
    bool                      synchronous_call {    0 };

    /// Call status after execution
    int                       call_status      {    0 };
    /// Process stub
    std::unique_ptr<std::thread>  call_thread;
    /// Flag to supervise spawned process
    bool                          need_watching { true };

    /// Start the process using the stub
    virtual int start_external_call(const std::string& when) final override;
    /// Stop the process using the stub
    virtual int stop_external_call(const std::string& when)  final override;

    /// Build valid command line including replacements
    std::string command_line()   const;
   
  public:
    /// Default constructor
    LibraryCallWrapper(const std::string& name, Context& ctxt);
    /// Standard destructor
    virtual ~LibraryCallWrapper();
  };
}    // End namespace Online

/// Include files from the framework
#include <Dataflow/Plugins.h>
#include <RTL/DllAccess.h>
#include <RTL/strdef.h>
#include <RTL/rtl.h>

DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_LibraryCallWrapper,LibraryCallWrapper)

/// Initializing constructor
Online::LibraryCallWrapper::LibraryCallWrapper(const std::string& nam, Context& ctxt)
  : ExternalCallWrapper(nam, ctxt)
{
  declareProperty("Replacements",   this->replacements);
  declareProperty("Library",        this->library_name);
  declareProperty("Function",       this->function_name);
  declareProperty("Arguments",      this->arguments);
  declareProperty("PrintCommand",   this->print_cmd        = true);
  declareProperty("Sleep_Prestart", this->sleep_pre_start  = 0);
  declareProperty("Sleep_Start",    this->sleep_start      = 0);
  declareProperty("Sleep_Stop",     this->sleep_stop       = 0);
  declareProperty("LogFile",        this->log_file         = "");
  declareProperty("Synchronous",    this->synchronous_call = 0);
}

/// Standard destructor
Online::LibraryCallWrapper::~LibraryCallWrapper()   {
  this->start_external_call("destruction");
  this->stop_external_call("destruction");
}

/// Build valid command line including replacements
std::string Online::LibraryCallWrapper::command_line() const {
  std::stringstream str;
  str << this->library_name << " " << this->function_name;
  for(std::size_t i=0, n=this->arguments.size(); i<n; ++i)  {
    str << " " << this->arguments[i];
  }
  std::string com = str.str();
  if( !this->replacements.empty() )  {
    for(const auto& r : this->replacements)
      com = RTL::str_replace(com, r.first, r.second);
  }
  return com;
}

/// Start the process using the stub
int Online::LibraryCallWrapper::start_external_call(const std::string& when)   {
  if( this->proc_start == when )   {
    if( this->sleep_pre_start > 0 )  {
      ::lib_rtl_sleep(1000*this->sleep_pre_start);
    }
    this->need_watching = true;
    typedef int (*function_t)(int, char**);
    void *dll = LOAD_LIB(this->library_name.c_str());
    function_t function = nullptr;
    if( dll )   {
      function = (function_t)GETPROC(dll, this->function_name.c_str());
      if( function )   {
	this->call_status = 0;
	this->call_thread = std::make_unique<std::thread>([this,when,function]()  {
	    std::vector<char*> argv;
	    argv.push_back((char*)this->function_name.c_str());
	    for(const auto& a : this->arguments) argv.emplace_back((char*)a.c_str());
	    argv.push_back(0);
	    if( this->print_cmd )  {
	      always("(%s) +++ Start thread: %s", when.c_str(), this->command_line().c_str());
	    }
	    int argc = int(this->arguments.size()+1);
	    this->call_status = (*function)(argc, &argv[0]);
	    return this->call_status;
	  });
	if( this->sleep_start > 0 )  {
	  ::lib_rtl_sleep(1000*this->sleep_start);
	}
	if( this->synchronous_call && this->call_thread )   {
	  this->call_thread->join();
	  this->call_thread.reset();
	}
	return DF_SUCCESS;
      }
      throw std::runtime_error("Failed to access function "+this->function_name+" in library "+this->library_name);
    }
    std::error_code ec = RTL::get_error();
    throw std::runtime_error("Failed to load library "+this->library_name+" ["+ec.message()+"]");
  }
  return DF_SUCCESS;
}

/// Stop the process using the stub
int Online::LibraryCallWrapper::stop_external_call(const std::string& when)   {
  if( this->proc_stop == when )   {
    if( this->print_cmd )   {
      always("(%s) +++ Stop  thread: %s", when.c_str(), this->command_line().c_str());
    }
    this->need_watching = false;
    if( this->call_thread )  {
      this->call_thread->join();
    }
    if( this->sleep_stop > 0 )  {
      ::lib_rtl_sleep(1000*this->sleep_stop);
    }
  }
  return DF_SUCCESS;
}
