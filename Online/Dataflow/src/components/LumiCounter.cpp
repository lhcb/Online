//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  LumiCounter.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Beat Jost
//               Markus Frank
//==========================================================================

/// Framework include files
#include "LumiCounter.h"
#include <EventData/odin_t.h>
#include <EventData/event_header_t.h>
#include <EventData/raw_bank_offline_t.h>
#include <RTL/rtl.h>

Online::LumiCounter::LumiCounter(const std::string& nam, Context& ctxt)
  : TriggerBitCounter(nam, ctxt), m_runHistory(0)
{
  declareProperty("DontCloseRuns",m_dontCloseRuns=false);
  declareProperty("SynchInterval",m_synchInterval = 2);
  declareProperty("SynchPerEvent",m_synchPerEvent = true);
  declareProperty("MultiInstance",m_multiInstance=false);
  m_synchTimer = 0;
}

// Standard Destructor
Online::LumiCounter::~LumiCounter()  {
}

int Online::LumiCounter::initialize()  {
  this->TriggerBitCounter::initialize();
  if( m_multiInstance )  {
    m_BackupFile = m_BackupFile + "_" + RTL::processName();
  }
  m_runMap.setFile(m_BackupDirectory + m_BackupFile);
  m_runMap.openBackingStore();
  if( !m_synchPerEvent )  {
    m_synchTimer = new SynchTimer(this,m_synchInterval);
  }
  declareMonitor("EventsMatched", m_EvIn,"Event Matched Requirements");
  return DF_SUCCESS;
}

int Online::LumiCounter::start()  {
  this->TriggerBitCounter::start();
  for( auto i : m_runMap )  {
    auto rnstr = std::to_string(((LumiRunItem*) i.second)->m_runno);
    //    LumiRunItem *p = ((LumiRunItem*) i.second);
    //    declareMonitor("LumiCount/" + rnstr, "x", (void*) p->m_count,
    //        sizeof(p->m_count), "LumiEvent for run " + rnstr);
  }
  if( m_synchTimer != nullptr )  {
    m_synchTimer->strt();
  }
  return DF_SUCCESS;
}

int Online::LumiCounter::finalize()  {
  if( m_synchTimer != 0 )  {
    delete m_synchTimer;
  }
  undeclareMonitors();
  return this->TriggerBitCounter::finalize();
}

/// Process single event
int Online::LumiCounter::stop()  {
  //  context.monitor->update("", m_runNumber, this);
  if( m_synchTimer != 0 )  {
    m_synchTimer->stop();
  }
  m_runMap.sync();
  return this->TriggerBitCounter::stop();
}

/// Process single event. Input buffer is ALWAYS in mdf bank format.
int Online::LumiCounter::execute(const Context::EventData& e)  {
  std::time_t ctim = time(0);
  if( !matchRequirements(e) )  {
    return DF_SUCCESS;
  }
  m_EvIn++;
  LumiRunItem *ri = 0;
  auto *ev  = (raw_bank_offline_t*) e.data;
  auto *mdf = (event_header_t*) ev->data();
  unsigned int runnr = mdf->subHeader().H1->runNumber();
  auto rmapi = m_runMap.find(runnr);
  if( rmapi == m_runMap.end() )  {
    ri = m_runMap.newEntry(runnr);
    ri->m_LastCnt = ctim;
    m_runMap[runnr] = ri;
    //    string rnstr = std::to_string(runnr);
    //    declareMonitor("LumiCount/" + rnstr, "x", (void*) ri->m_count,
    //        sizeof(ri->m_count), "LumiEvent for run " + rnstr);
  }
  else  {
    ri = (LumiRunItem *) rmapi->second;
  }
#if 0
  if( runnr > m_runNumber )  {
    if( m_runNumber != 0 )  {
      context.monitor->update("", m_runNumber, this);
    }
    m_runNumber = runnr;
  }
#endif
  raw_bank_offline_t *odin = findOdinBank(e.data, e.length);
  if( odin == 0 )  {
    ::lib_rtl_output(LIB_RTL_ERROR, "Cannot Find ODIN Bank.");
    return DF_SUCCESS;
  }
  run2_odin_t *od = (run2_odin_t*) odin->data();
#if 0
  list<int> toDelete;
  for( auto i : m_runMap )  {
    if( i.first != m_runNumber )  {
      if( !m_dontCloseRuns )  {
	if ((ctim - ((LumiRunItem*) i.second)->m_LastCnt) > 100)  {
	  toDelete.insert(toDelete.end(), i.first);
	  string rnstr = std::to_string(i.first);
	  undeclareMonitor("LumiCount/" + rnstr);
	}
      }
    }
  }
  for( auto i : toDelete )  {
    rmapi = m_runMap.find(i);
    if (rmapi != m_runMap.end())
      m_runMap.erase(rmapi);
  }
#endif
  ri->m_LastCnt = ctim;
  ri->m_count[od->bx_type()]++;
  if (this->m_synchPerEvent)  {
    m_runMap.sync();
  }
  return DF_SUCCESS;
}

Online::raw_bank_offline_t*
Online::LumiCounter::findOdinBank(void *currevt, int evlen)  {
  void *evend = lib_rtl_add_ptr<void>(currevt, evlen);
  raw_bank_offline_t *bnk = (raw_bank_offline_t*) currevt;
  while( bnk < evend )  {
    if( bnk->magic() == raw_bank_offline_t::MagicPattern )  {
      if( bnk->type() == bank_types_t::ODIN )  {
        return bnk;
      }
      else  {
        bnk = lib_rtl_add_ptr<raw_bank_offline_t>(bnk,((bnk->size() + bnk->hdrSize() + 3) & 0xfffffffc));
        continue;
      }
    }
  }
  return 0;
}
