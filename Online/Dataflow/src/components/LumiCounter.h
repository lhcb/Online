//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  MBMServer.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_LUMICOUNTER
#define ONLINE_DATAFLOW_LUMICOUNTER 1

// Framework include files
#include <Dataflow/TriggerBitCounter.h>
#include <dim/dim.hxx>

///  Online namespace declaration
namespace Online  {

  /// Forward declarations
  class SynchTimer;
  
  /// Component to select/count luminosity events
  /**
   *
   *  \author  Markus Frank
   *  \version 1.0
   */
  class LumiCounter: public TriggerBitCounter   {
  public:
    typedef unsigned long        CounterType[4];
    typedef RunListItem<CounterType> LumiRunItem;

    /// The run-history file
    FILE*               m_runHistory;
    /// Declare Monitoring information on the current run
    CounterType         m_counter;
    /// Declare Monitoring information on the last set of runs
    RunMap<CounterType> m_runMap;
    /// Current run number
    bool                m_dontCloseRuns;
    bool                m_synchPerEvent;
    int                 m_synchInterval;
    SynchTimer         *m_synchTimer  { nullptr };
    bool                m_multiInstance;

    /// Find Tell1 Bank in event buffer
    raw_bank_offline_t *findOdinBank(void *currevt, int evlen);
    
  public:
    /// Standard Constructor
    LumiCounter(const std::string& nam, Context& ctxt);
    /// Default destructor
    virtual ~LumiCounter();
    /// Initialize the component
    virtual int initialize()  override;
    /// Start the component
    virtual int start()  override;
    /// Stop the component
    virtual int stop()  override;
    /// Finalize the component
    virtual int finalize()  override;
    /// Process single event. Input buffer is ALWAYS in mdf bank format.
    virtual int execute(const Context::EventData& e)  override;
  };

  /// Base class of syncronization timer based on DIM timers
  /**
   *
   *  \author  Markus Frank
   *  \version 1.0
   */
  class SynchTimer : public DimTimer  {
  public:
    LumiCounter *m_lumiCounter  { nullptr };
    int          m_time;
  public:
    /// Initializing constructor
    SynchTimer(LumiCounter *lc, int tim = 2)  {
      m_lumiCounter = lc;
      m_time = tim;
    }
    /// Start timer
    void strt()  {
      this->DimTimer::start(m_time);
    }
    /// Timer callback handler
    void timerHandler() override  {
      m_lumiCounter->m_runMap.sync();
      strt();
    }
  };
}      // End namespace LHCb
#endif //  ONLINE_DATAFLOW_LUMICOUNTER
