//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  MBMSelector.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include "MBMSelector.h"
#include <CPP/mem_buff.h>

// C/C++ include files

// Forward declarations

///  Online namespace declaration
namespace Online  {

  /// Buffer Manager service for the dataflow
  /**
   * @author  Markus Frank
   * @version 1.0
   */
  class MBMAccumulator : public MBMSelector  {
  protected:
    /// Maximal number of events to be accumulated
    size_t      m_maxEvents  = std::numeric_limits<size_t>::max();
    /// Size of the buffers to accumulate events
    size_t      m_bufferSize = std::numeric_limits<size_t>::min();
    /// Property: Name of the prescaler object (dataflow component) to select events
    std::string m_preScalerName  { "PreScaler" };

    /// Memory buffers to accumulate events
    mem_buff    m_buffer1, m_buffer2;
    /// Number of events in buffer 1
    size_t      m_numEvent1 = 0;
    /// Number of events in buffer 2
    size_t      m_numEvent2 = 0;

    /// Reference to the prescaler object
    DataflowComponent* m_preScaler { nullptr };

    int copy_accepted(const Context::EventData& ev);

    /** Event access interface for sub-classes */
    /// Event access interface: Get next event from wherever
    virtual int getEvent(Context::EventData& event)  override;

  public:
    /// Initializing constructor
    MBMAccumulator(const std::string& name, Context& ctxt);
    /// Default destructor
    virtual ~MBMAccumulator() = default;
    /// Start the data flow component.
    virtual int start()  override;
    /// Stop the data flow component.
    virtual int stop()  override;
  };
}  // end namespace Online
//==========================================================================
#include <EventData/event_header_t.h>
#include <Dataflow/DataflowManager.h>
#include <Dataflow/Plugins.h>
#include <MBM/bmdef.h>

// C/C++ include files
#include <cstring>

// Instantiation of a static factory class used by clients to create instances of this service
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_MBMAccumulator,MBMAccumulator)

using namespace std;
using namespace Online;

/// Initializing constructor
MBMAccumulator::MBMAccumulator(const string& nam, Context& ctxt)
  : MBMSelector(nam, ctxt)
{
  declareProperty("PreScaler",  m_preScalerName);
  declareProperty("BufferSize", m_bufferSize = 1024*1024);
  declareProperty("MaxEvents",  m_maxEvents);
}

/// Start the data flow component.
int MBMAccumulator::start()   {
  int sc = MBMSelector::start();
  m_buffer1.allocate(m_bufferSize);
  m_buffer2.allocate(m_bufferSize);
  m_numEvent1 = m_numEvent2 = 0;
  m_preScaler = context.manager.manager().getManagedComponent(m_preScalerName, false);
  if( !m_preScaler )    {
    fatal("Failed to access prescaler: %s", m_preScalerName.c_str());
    return DF_ERROR;
  }
  return sc;
}

/// Stop the data flow component.
int MBMAccumulator::stop()   {
  m_numEvent1 = m_numEvent2 = 0;
  m_preScaler = nullptr;
  return MBMSelector::stop();
}

int MBMAccumulator::copy_accepted(const Context::EventData& ev)    {
  const char* start = ev.data;
  const char* end   = start + ev.length;
  while ( start < end )   {
    const event_header_t* hdr = (const event_header_t*)start;
    size_t len = hdr->size0();

    if( !hdr->is_mdf() )   {
      return 0;
    }
    else if( m_cancelled > 0 )   {
      return (m_numEvent2 > 0 || m_buffer2.used()) ? 1 : 0;
    }
    Context::EventData event_data;
    auto mask = hdr->subHeader().Mask->triggerMask();
    event_data.data    = (char*)start;
    event_data.header  = (event_header_t*)start;
    event_data.length  = len;
    event_data.type    = EVENT_TYPE_EVENT;
    event_data.mask[0] = mask[0];
    event_data.mask[1] = mask[1];
    event_data.mask[2] = mask[2];
    event_data.mask[3] = mask[3];
    int sc = m_preScaler->execute(event_data);
    if( (sc == DF_SUCCESS) && (m_preScaler->processingFlag&DF_PASSED) )    {
      if( m_cancelled > 0 )   {
	return (m_numEvent2 > 0 || m_buffer2.used()) ? 1 : 0;
      }
      else if( (m_numEvent1+1) > m_maxEvents )   {
	m_buffer2.copy(m_buffer2.used(), start, len);
	++m_numEvent2;
      }
      else if( len + m_buffer1.used() >= m_bufferSize )   {
	m_buffer2.copy(m_buffer2.used(), start, len);
	++m_numEvent2;
      }
      else  {
	m_buffer1.copy(m_buffer1.used(), start, len);
	++m_numEvent1;
      }
    }
    start += len;
  }
  return (m_numEvent2 > 0 || m_buffer2.used()) ? 1 : 0;
}

/// Get next event from wherever
int MBMAccumulator::getEvent(Context::EventData& ev)   {
  Context::EventData event_data;
  ::memset(event_data.mask, 0xFF, sizeof(event_data.mask));
  m_buffer1.swap(m_buffer2);
  m_buffer2.reset();
  m_numEvent1 = m_numEvent2;
  m_numEvent2 = 0;
  if( m_numEvent1 < m_maxEvents && m_buffer1.used() < m_bufferSize )  {
    while( 0 == m_cancelled )   {
      int sc = this->MBMSelector::getEvent(event_data);
      if( m_cancelled > 0 )   {
	ev = Context::EventData();
	return DF_CANCELLED;
      }
      else if( sc == DF_SUCCESS )  {
	sc = copy_accepted(event_data);
	if( sc > 0 )
	  break;
	continue;
      }
      else if( sc == DF_CANCELLED )  {
	ev = Context::EventData();
	return DF_CANCELLED;
      }
      return DF_ERROR;
    }
    if( m_cancelled )   {
      ev = Context::EventData();
      return DF_CANCELLED;
    }
  }
  ev.data    = (char*)m_buffer1.begin();
  ev.header  = (event_header_t*)m_buffer1.begin();
  ev.length  = m_buffer1.used();
  ev.type    = event_data.type;
  ev.mask[0] = ~0x0;
  ev.mask[1] = ~0x0;
  ev.mask[2] = ~0x0;
  ev.mask[3] = ~0x0;
  return DF_SUCCESS;
}
