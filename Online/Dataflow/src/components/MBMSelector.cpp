//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  MBMSelector.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include "MBMSelector.h"
#include <Dataflow/MBMClient.h>
#include <Dataflow/Incidents.h>
#include <Dataflow/Plugins.h>
#include <MBM/Consumer.h>
#include <RTL/rtl.h>

// C/C++ include files
#include <cstring>

// Instantiation of a static factory class used by clients to create instances of this service
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_MBMSelector,MBMSelector)

using namespace std;
using namespace Online;

/// Initializing constructor
MBMSelector::MBMSelector(const string& nam, Context& ctxt)
  : EventRunable(nam, ctxt), m_consumer(0)
{
  declareProperty("Input", m_input = "");
  declareProperty("Decode",m_decode = false);
  declareProperty("HandleTimeout",m_handleTMO = false);
  declareProperty("REQ1", m_Rqs[0] = "");
  declareProperty("REQ2", m_Rqs[1] = "");
  declareProperty("REQ3", m_Rqs[2] = "");
  declareProperty("REQ4", m_Rqs[3] = "");
  declareProperty("REQ5", m_Rqs[4] = "");
  declareProperty("REQ6", m_Rqs[5] = "");
  declareProperty("REQ7", m_Rqs[6] = "");
  declareProperty("REQ8", m_Rqs[7] = "");
  declareProperty("Pause",m_gotoPause = false);
  declareProperty("PauseSleep",m_pauseSleep = 0);
  declareProperty("PrintFreq",m_printFreq = 0.0);
}

/// Default destructor
MBMSelector::~MBMSelector()   {
}

/// Get next event from wherever
int MBMSelector::getEvent(Context::EventData& ev)   {
  // If we are not enabled, return and exit the event loop
  if ( context.mbm )   {
    int sc = m_consumer->getEvent();
    if ( sc == MBM_NORMAL )  {
      const MBM::EventDesc& e = m_consumer->event();
      ev.data    = (char*)e.data;
      ev.header  = (event_header_t*)e.data;
      ev.length  = e.len;
      ev.type    = e.type;
      ev.mask[0] = e.mask[0];
      ev.mask[1] = e.mask[1];
      ev.mask[2] = e.mask[2];
      ev.mask[3] = e.mask[3];
      return DF_SUCCESS;
    }
    else if ( sc == MBM_REQ_CANCEL )  {
      ev = Context::EventData();
      return DF_CANCELLED;
    }
    return DF_ERROR;
  }
  /// If there is no MBM attached or MBM is inactive. Cancel all requests
  ev = Context::EventData();
  return DF_CANCELLED;
}

/// Event access interface: Cancel I/O operations of the dataflow component
int MBMSelector::cancelEvents()  {
  if ( context.mbm )  {
    context.mbm->cancelBuffers();
  }
  return DF_SUCCESS;
}

/// Shutdown event access in case of forced exit request
int MBMSelector::shutdown()   {
  if ( context.mbm )  {
    context.mbm->stop();
    context.mbm->finalize();
  }
  return DF_SUCCESS;
}

/// Release event
int MBMSelector::freeEvent(Context::EventData& /* event */)   {
  if ( m_consumer )  {
    int sc = m_consumer->freeEvent();
    if ( sc == MBM_NORMAL || sc == MBM_REQ_CANCEL )   {
      return DF_SUCCESS; 
    }
    return DF_ERROR;
  }
  return DF_SUCCESS;
}

/// Initialize the event processor
int MBMSelector::start()  {
  int sc = EventRunable::start();
  if ( context.mbm && sc == DF_SUCCESS )  {
    declareMonitor("RequestCount",m_reqCount=0, "Events requested counter");
    if ( sc == DF_SUCCESS )  {
      for ( m_nreqs=0; m_nreqs<8; ++m_nreqs )  {
	if ( !m_Rqs[m_nreqs].empty() )   {
	  m_Reqs[m_nreqs].parse(m_Rqs[m_nreqs]);
	  continue;
	}
	break;
      }
      m_consumer = context.mbm->createConsumer(m_input, RTL::processName());
      if ( 0 == m_consumer )  {
	throwError("Failed to create MBM Consumer for buffer '%s'",m_input.c_str());
      }
      for(int i=0; i<m_nreqs; ++i)
	m_consumer->addRequest(m_Reqs[i]);
      ::mbm_clear(m_consumer->id());
    }
  }
  return sc;
}

/// Initialize the event processor
int MBMSelector::stop()  {
  int status = EventRunable::stop();
  if ( m_consumer )   {
    try   {
      for( int i=0; i<m_nreqs; ++i )
	m_consumer->delRequest(m_Reqs[i]);
    }
    catch(...)   { }
    delete m_consumer;
    m_consumer = nullptr;
  }
  m_nreqs = 0;
  return status;
}
