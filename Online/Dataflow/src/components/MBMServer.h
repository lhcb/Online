//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  MBMServer.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_MBMSERVER_H
#define ONLINE_DATAFLOW_MBMSERVER_H

// Framework include files
#include <Dataflow/DataflowComponent.h>
#include <MBM/Producer.h>
#include <MBM/bmserver.h>

// C/C++ include files
#include <set>
#include <map>
#include <mutex>
#include <memory>

// Forward declarations
namespace MBM {
  class Requirement;
}

///  Online namespace declaration
namespace Online  {

  /// Buffer Manager service for the dataflow
  /** @class MBMServer MBMServer.h Dataflow/MBMServer.h
   *
   *
   * @author  Markus Frank
   * @version 1.0
   */
  class MBMServer : public DataflowComponent  {

  public:
    typedef std::vector<std::string>               ConsRequirement;
    typedef std::vector<std::string>               AccidentBuffers;
    typedef std::map<std::string, ConsRequirement> ConsRequirements;
    typedef std::map<std::string, ServerBMID>      ServedBuffers;

  private:
    /// Property: Initialization flags to possibly install MBM/MEP buffers
    std::string                    m_init_flags        { };
    /// Property: partition name used to connect to buffer managers (If set overrides partition identifier)
    std::string                    m_partition_name    { };
    /// Property: Buffer name to save accident events to
    std::string                    m_accident_output   { };
    /// Property: Transition of incident when to connect to the accident buffer
    std::string                    m_accident_connect  { };
    /// Property: Transition of incident when to disconnect from the accident buffer
    std::string                    m_accident_disconnect { };
    /// Property: Container of consumer requirements. Only valid if the buffers are held!
    ConsRequirements               m_consumer_requirements  { };
    /// Property: Buffer names with accident handlers attached
    AccidentBuffers                m_accident_buffers  { };
    /// Property: partition identifier used to connect to buffer managers
    int                            m_partition_id      { -1 };
    /// Property: Flag to indicate if buffer names should contain partition ID
    bool                           m_partition_buffers { false };

    /// Map of server BMIDs
    ServedBuffers                  m_server_bm     { };
    /// BMID of output buffer
    std::unique_ptr<MBM::Producer> m_output_prod   { };
    /// Lock of output buffer to handle multiple accident buffers
    std::mutex                     m_output_lock   { };

  public:
    /// Initializing constructor
    MBMServer(const std::string& name, Context& framework);
    /// Default destructor
    virtual ~MBMServer();
    /// Initialize the MBM server
    virtual int initialize()  override;
    /// Start MBM server
    virtual int start()  override;
    /// Stop MBM server
    virtual int stop()  override;
    /// Finalize the MBM server
    virtual int finalize()  override;
    /// Incident handler implemenentation: Inform that a new incident has occured
    virtual void handle(const DataflowIncident& inc)  override;

    /// Create proper buffer name depending on partitioning
    std::string bufferName(const std::string& nam)  const;
    /// Apply consumer requirements to MBM buffers
    int setConsumerRequirements();
    /// Apply single consumer reuirement to buffer
    int setConsumerRequirement(ServerBMID srvBM, 
			       const std::string& task_match, 
			       const MBM::Requirement& req);
    /// Subscribe to 'accident' events (client crashes etc.)
    int subscribeAccidents();
    /// Unsubscribe from 'accident' events (client crashes etc.)
    int unsubscribeAccidents();
    /// Handle accident events
    void handleAccident(ServerBMID bmid, const void* address, size_t len, int typ, const unsigned int mask[], const char* user);
  };
}      // end namespace Online
#endif //  ONLINE_DATAFLOW_MBMSERVER_H
