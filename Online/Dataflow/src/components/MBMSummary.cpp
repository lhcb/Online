//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : Online/GaudiOnlineTests
//
//  Author     : Markus Frank
//==========================================================================

// Include files from the framework
#include <Dataflow/DataflowComponent.h>

/// Online namespace declaration
namespace Online    {

  /// Generic MDF File I/O class to be used by the OnlineEvtApp
  /**
   *
   * \version 1.0
   * \author  M.Frank
   */
  class MBMSummary : public DataflowComponent  {
    /// Property: Define when to make snapshots
    std::vector<std::string> snapshots;
    /// Property: Regular expression for selective buffer summaries
    std::string expression;
    /// Property: Printout frequency if used as an algorithm
    long        printFreq     { -1 };
    /// Property: Delay before taking snapshot
    int         delay         { -1 };
    /// Property: Show task state in the MBM summary
    bool        show_states   {  1 };
    
    /// Event counter
    long        event_counter { 0 };
    /// Sleep function
    void make_snapshot(const char* state);

  public:
    /// Default constructor
    MBMSummary(const std::string& name, Context& ctxt);

    /// Standard destructor
    virtual ~MBMSummary() = default;

    /// Initialize the delay component
    virtual int initialize()  override;
    /// Start the delay component
    virtual int start()  override;
    /// Stop the delay component
    virtual int stop()  override;
    /// Finalize the delay component
    virtual int finalize()  override;
    /// Continuing the data flow component.
    virtual int continuing()  override;
    /// Pause the data flow component.
    virtual int pause()  override;
    /// Data processing overload: process event
    virtual int execute(const Context::EventData& event)  override;
    /// Incident handler implemenentation: Inform that a new incident has occured
    virtual void handle(const DataflowIncident& inc)  override;
  };
}    // End namespace Online

/// Include files from the framework
#include <Dataflow/Plugins.h>
#include <Dataflow/Incidents.h>
#include <MBM/Summary.h>
#include <RTL/rtl.h>

/// C/C++ include files
#include <cstring>

DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_MBMSummary,MBMSummary)

using namespace Online;

/// Initializing constructor
MBMSummary::MBMSummary(const std::string& nam, Context& ctxt)
  : DataflowComponent(nam, ctxt)
{
  declareProperty("Regex",      this->expression);
  declareProperty("When",       this->snapshots);
  declareProperty("Delay",      this->delay);
  declareProperty("PrintFreq",  this->printFreq);
  declareProperty("ShowStates", this->show_states);
}

    /// Sleep function
void MBMSummary::make_snapshot(const char* state)   {
  for(const auto& st : snapshots)   {
    if ( 0 == ::strcmp(st.c_str(), state) )   {
      if ( this->delay > 0 )   {
	::lib_rtl_sleep(this->delay);
      }
      MBM::Summary sum(expression, false);
      auto [status,lines] = sum.get_info(this->show_states);
      for(const auto& line : lines)
	always("%-10s %s\n", state, line.c_str());
    }
  }
}

/// Initialize the component
int MBMSummary::initialize()  {
  int sc = Component::initialize();
  if ( snapshots.empty() )   {
    snapshots.emplace_back("(start)");
    snapshots.emplace_back("(stop)");
  }
  make_snapshot("(initialize)");
  return sc;
}

/// Start the delay component
int MBMSummary::start()  {
  int sc = Component::start();
  make_snapshot("(start)");
  subscribeIncident("DAQ_PAUSE");
  subscribeIncident("DAQ_CANCEL");
  subscribeIncident("MBM_SNAPSHOT");
  return sc;
}

/// Finalize the delay component
int MBMSummary::finalize()  {
  make_snapshot("(finalize)");
  return Component::finalize();
}

/// Stop the delay component
int MBMSummary::stop()  {
  make_snapshot("(stop)");
  unsubscribeIncidents();
  return Component::stop();
}

/// Pause the data flow component. Default implementation is empty.
int MBMSummary::pause()  {
  make_snapshot("(pause)");
  return DataflowComponent::pause();
}

/// Continuing the data flow component. Default implementation is empty.
int MBMSummary::continuing()  {
  make_snapshot("(continue)");
  return DataflowComponent::continuing();
}

/// Data processing overload: Send data record to network client
int MBMSummary::execute(const Context::EventData& /* event */)  {
  ++this->event_counter;
  if ( this->printFreq > 0 && (this->event_counter%this->printFreq) == 0 )    {
    make_snapshot("(execute)");
  }
  return DF_SUCCESS;
}

/// Incident handler implemenentation: Inform that a new incident has occured
void MBMSummary::handle(const DataflowIncident& inc)    {
  if ( inc.type == "MBM_SNAPSHOT" )  {
    make_snapshot("(snapshot)");
  }
  else if ( inc.type == "DAQ_CANCEL" )  {
    make_snapshot("(cancel)");
  }
  else if ( inc.type == "DAQ_PAUSE" )  {
    make_snapshot("(pause)");
  }
}
