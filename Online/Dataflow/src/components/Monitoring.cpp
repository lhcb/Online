//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  Monitoring.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include "Monitoring.h"
#include <Dataflow/DataflowCatalog.h>

DATAFLOW_MAKE_CATALOG(MonitoringCatalog,Online::DataflowComponent*,std::vector<int>)

using namespace Online;

/// Initializing constructor
Monitoring::Monitoring(const std::string& nam, Context& ctxt)
  : Component(nam, ctxt)
{
  m_catalog = new MonitoringCatalog();
}

/// Default destructor
Monitoring::~Monitoring()   {
  detail::deletePtr(m_catalog);
}

#define IMPLEMENT_SINGLE_MONITOR(x)                         \
  namespace Online  {                                       \
    template <> int                                         \
    Monitoring::declareInfo<x>(const std::string& parent,   \
                               const std::string& nam,      \
                               const x& val,                \
                               const std::string& desc)  {  \
      return declare(parent, nam, &val, typeid(x), desc);   \
    }}

#define IMPLEMENT_PAIR_MONITOR(x)                                 \
  namespace Online  {                                             \
    template <> int                                               \
    Monitoring::declareInfo<x>(const std::string& parent,         \
                               const std::string& nam,            \
                               const x& val1,                     \
                               const x& val2,                     \
                               const std::string& desc)  {        \
      return declare(parent, nam, &val1, &val2, typeid(x), desc); \
    }}


#define IMPLEMENT_MONITOR(x)                    \
  IMPLEMENT_SINGLE_MONITOR(x)                   \
  IMPLEMENT_SINGLE_MONITOR(std::vector<x>)

IMPLEMENT_MONITOR(char)
IMPLEMENT_MONITOR(unsigned char)
IMPLEMENT_MONITOR(short)
IMPLEMENT_MONITOR(unsigned short)
IMPLEMENT_MONITOR(int)
IMPLEMENT_MONITOR(unsigned int)
IMPLEMENT_MONITOR(long)
IMPLEMENT_MONITOR(unsigned long)
IMPLEMENT_MONITOR(float)
IMPLEMENT_MONITOR(double)
IMPLEMENT_SINGLE_MONITOR(std::string)
IMPLEMENT_PAIR_MONITOR(int)
IMPLEMENT_PAIR_MONITOR(long)

/// Monitoring interface: Declare monitoring item to the monitoring component
int Monitoring::declare(const std::string&    parent,
                        const std::string&    nam,
                        const void*           ptr,
                        const std::type_info& typ,
                        const std::string&    desc)
{
  if ( parent.empty() )
    return error("Cannot monitor an item of an invalid component.");
  else if ( nam.empty() )
    return error("Cannot monitor an item of %s without a name.",parent.c_str());
  else if ( !ptr )
    return error("Cannot monitor an item %s.%s from a NULL pointer.",
                 parent.c_str(),nam.c_str());
  else if ( desc.empty() )  {
  }

  if ( typ == typeid(char) )  {
  }
  else if ( typ == typeid(unsigned char) )  {
  }
  return DF_SUCCESS;
}
int Monitoring::declare(const std::string&    parent,
                        const std::string&    nam,
                        const void*           ptr1,
                        const void*           ptr2,
                        const std::type_info& typ,
                        const std::string&    desc)
{
  if ( parent.empty() )
    return error("Cannot monitor an item of an invalid component.");
  else if ( nam.empty() )
    return error("Cannot monitor an item of %s without a name.",parent.c_str());
  else if ( !ptr1 )
    return error("Cannot monitor an item %s.%s from a NULL pointer.",
                 parent.c_str(),nam.c_str());
  else if ( !ptr2 )
    return error("Cannot monitor an item %s.%s from a NULL pointer.",
                 parent.c_str(),nam.c_str());
  else if ( desc.empty() )  {
  }

  if ( typ == typeid(char) )  {
  }
  else if ( typ == typeid(unsigned char) )  {
  }
  return DF_SUCCESS;
}

/// Monitoring interface: Declare monitoring entity (array pointer with size)
int Monitoring::declare(const std::string&    parent,
                        const std::string&    nam,
                        const void*           ptr,
                        std::size_t           data_length,
                        const std::type_info&,
                        const std::string&    desc)
{
  if ( parent.empty() )
    return error("Cannot monitor an item of an invalid component.");
  else if ( nam.empty() )
    return error("Cannot monitor an item of %s without a name.",parent.c_str());
  else if ( !ptr )
    return error("Cannot monitor an item %s.%s from a NULL pointer.",
                 parent.c_str(), nam.c_str());
  else if ( !data_length )
    return error("Cannot monitor an item %s.%s with NULL data length.",
                 parent.c_str(), nam.c_str());
  else if ( desc.empty() )  {
  }
  return DF_SUCCESS;
}

/// Monitoring interface: Declare monitoring entity (structure with given format)
int Monitoring::declare(const std::string&    parent,
                        const std::string&    nam,
                        const void*           ptr,
                        std::size_t           data_length,
                        const std::type_info&,
                        const std::string&    fmt,
                        const std::string&    desc)
{
  if ( parent.empty() )
    return error("Cannot monitor an item of an invalid component.");
  else if ( nam.empty() )
    return error("Cannot monitor an item of %s without a name.",parent.c_str());
  else if ( fmt.empty() )
    return error("Cannot monitor an item %s.%s without a valid format string.",
                 parent.c_str(), nam.c_str());
  else if ( !ptr )
    return error("Cannot monitor an item %s.%s from a NULL pointer.",
                 parent.c_str(), nam.c_str());
  else if ( !data_length )
    return error("Cannot monitor an item %s.%s [fmt:%s] with NULL data length.",
                 parent.c_str(), nam.c_str(), fmt.c_str());
  else if ( desc.empty() )  {
  }
  return DF_SUCCESS;
}

/// Monitoring interface: Undeclare monitoring information
int Monitoring::undeclare(const std::string& parent,const std::string& nam)   {
  if ( !parent.empty() && !nam.empty() )  {
    return DF_SUCCESS;
  }
  return error("Invalid client attempting to undeclare monitoring items!");
}

/// Monitoring interface: Undeclare monitoring information
int Monitoring::undeclare(const std::string& parent)   {
  if ( parent.empty() )  {
    return DF_SUCCESS;
  }
  return error("Invalid client attempting to undeclare monitoring items!");
}

/// Monitoring interface: Reset some/all monitoring items
int Monitoring::reset(const std::string& parent,const std::string& what)   {
  if ( parent.empty() ) {}
  if ( what == "Histos" )  {}
  return DF_SUCCESS;
}

/// Monitoring interface: Update monitoring items
int Monitoring::update(const std::string& parent,const std::string& item, int run_number)  {
  if ( !parent.empty() ) {
    if ( item.empty() ) {
      if ( run_number ) {
      }
    }
  }
  return DF_SUCCESS;
}
/// Update current run number context for updates
void Monitoring::setRunNo(uint32_t /*runno*/)  {
}
