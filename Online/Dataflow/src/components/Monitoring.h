//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  Monitoring.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_MONITORING_H
#define ONLINE_DATAFLOW_MONITORING_H

/// Framework include files
#include <Dataflow/DataflowComponent.h>

///  Online namespace declaration
namespace Online  {

  /// Forward declarations
  class MonitoringCatalog;

  /// Monitor service for the dataflow
  /** @class Monitoring Monitoring.h Dataflow/Monitoring.h
   *
   *
   * @author  Markus Frank
   * @version 1.0
   */
  class Monitoring : public DataflowComponent, public DataflowContext::Monitor  {
  private:
    /// Container of all monitoring items
    MonitoringCatalog* m_catalog;

  public:

    /// Initializing constructor
    Monitoring(const std::string& name, Context& framework);
    /// Default destructor
    virtual ~Monitoring();

    /** Monitoring handling  */
    /// Declare monitoring item to the monitoring component
    template <typename T> int declareInfo(const std::string& owner,
                      const std::string& nam,
                      const T& val,
                      const std::string& desc="");
    // Pairs...
    template <typename T> int declareInfo(const std::string& owner,
                      const std::string& nam,
                      const T& val1,
                      const T& val2,
                      const std::string& desc="");
    /// Declare monitoring item to the monitoring component
    template <typename T> int declareInfo(const char* owner,
					  const char* nam,
					  const T& val,
					  const char* desc=0)  {
      return declareInfo(owner, std::string(nam ? nam : ""), val, std::string(desc ? desc : ""));
    }
    
    /// Monitoring interface: Declare monitoring item to the monitoring component
    virtual int declare(const std::string&       owner,
                        const std::string&       nam,
                        const void*              ptr,
                        const std::type_info&    typ,
                        const std::string&       desc)  override;

    /// Monitoring interface: Declare monitoring entiry (ratio of two numbers)
    virtual int declare(const std::string&       owner,
                        const std::string&       nam,
                        const void*              ptr1,
                        const void*              ptr2,
                        const std::type_info&    typ,
                        const std::string&       desc)  override;

    /// Monitoring interface: Declare monitoring entity (pointer with size)
    virtual int declare(const std::string&       parent,
                        const std::string&       nam,
                        const void*              ptr,
                        size_t                   data_length,
                        const std::type_info&    data_typ,
                        const std::string&       desc)   override;
    
    /// Monitoring interface: Declare monitoring entity (structure with given format)
    virtual int declare(const std::string&       owner,
                        const std::string&       nam,
                        const void*              ptr,
                        std::size_t              data_length,
                        const std::type_info&    typ,
                        const std::string&       fmt,
                        const std::string&       desc)  override;
 
    /// Monitoring interface: Undeclare single monitoring item
    virtual int undeclare(const std::string& parent,const std::string& nam)  override;
    /// Monitoring interface: Undeclare monitoring information
    virtual int undeclare(const std::string& parent)  override;
    /// Monitoring interface: Update monitoring items
    virtual int update(const std::string& parent,const std::string& item, int run_number)  override;
    /// Monitoring interface: Reset some/all monitoring items
    virtual int reset(const std::string& parent,const std::string& what)  override;
    /// Update current run number context for updates
    virtual void setRunNo(uint32_t runno) override;
  };

}      // end namespace Online

#endif //  ONLINE_DATAFLOW_MONITORING_H
