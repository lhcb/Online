//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  NumaControl.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_NUMACONTROL_H
#define ONLINE_DATAFLOW_NUMACONTROL_H

// Framework include files
#include <Dataflow/DataflowComponent.h>
#include <RTL/strdef.h>
#include <RTL/Numa.h>

///  Online namespace declaration
namespace Online  {

  /// Forward declarations

  /// NumaControl component to be applied in the manager sequence
  /**
   *
   * @author  Markus Frank
   * @version 1.0
   */
  class NumaControl : public DataflowComponent  {
  protected:
    /// Property: Option to steer the printout.
    std::string      printOption;
    /// Property: String representation of the key to be added to the event context
    std::string      when;
    /// Property: Use PID for printout (Disable for qmtest!)
    bool             usePID = true;

    /// Numa actor
    RTL::Numa        numa;
    /// Warning printout handling
    void warning(const std::vector<std::string>& msg)   const   {
      for( const auto& m : msg )
	this->info(m);
    }

    /// Apply NUMA configuration settings according to job options
    void apply_numa_config()   {
      if ( -1 != numa.available() )   {
	std::string opt = RTL::str_lower(printOption);
	std::pair<int, std::vector<std::string> > ret = { -1, { } };
	/// Now optional printout
	if ( opt.find("all") != std::string::npos ) ret = numa.printSettings(usePID);
	warning(ret.second);
	if ( opt.find("initial") != std::string::npos ) ret = numa.printStatus(usePID);
	warning(ret.second);
	ret = numa.apply_config();
	warning(ret.second);
	if ( opt.find("result") != std::string::npos ) ret = numa.printStatus(usePID);
	warning(ret.second);
      }
    }

  public:
    /// Initializing constructor
    NumaControl(const std::string& nam, Context& ctxt)
    : Component(nam,ctxt)
    {
      declareProperty("When",       when="initialize");
      declareProperty("Print",      printOption);
      declareProperty("BindCPU",    numa.bindCPU);
      declareProperty("BindMemory", numa.bindMemory);
      declareProperty("CPUSlots",   numa.cpuSlots);
      declareProperty("CPUMask",    numa.cpuMask);
      declareProperty("PrintPID",   usePID);
    }
    /// Default destructor
    virtual ~NumaControl() = default;

    /// Initialize the numa controls component
    virtual int initialize()  override  {
      int sc = Component::initialize();
      if ( when.find("initialize") != std::string::npos )
	apply_numa_config();
      return sc;
    }

    /// Start the numa controls component
    virtual int start()  override  {
      int sc = Component::start();
      if ( when.find("start") != std::string::npos )
	apply_numa_config();
      return sc;
    }

    /// Stop the numa controls component
    virtual int stop()  override  {
      int sc = Component::stop();
      if ( when.find("stop") != std::string::npos )
	apply_numa_config();
      return sc;
    }

    /// Finalize the numa controls component
    virtual int finalize()  override  {
      int sc = Component::finalize();
      if ( when.find("finalize") != std::string::npos )
	apply_numa_config();
      return sc;
    }
  };
}      // end namespace Online
#endif //  ONLINE_DATAFLOW_NUMACONTROL_H

/// Framework includes
#include <Dataflow/Plugins.h>
#include <DD4hep/Primitives.h>
#include <RTL/rtl.h>

DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_NumaControl,NumaControl)
