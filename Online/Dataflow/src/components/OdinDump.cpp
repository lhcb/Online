//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : DAQ/MDF
//
//  Author     : Markus Frank
//==========================================================================

/// Include files from Gaudi
#include <Dataflow/DataflowComponent.h>
#include <EventData/raw_bank_offline_t.h>
#include <EventData/event_header_t.h>
#include <EventData/bank_header_t.h>
#include <EventData/odin_t.h>

/// C/C++ include files
#include <atomic>

/// Online namespace declaration
namespace Online  {

  /// Class to dump the ODIN bank from a single beam crossings
  /** Class to dump the ODIN bank from a single beam crossings
   *  Dump Odin bank from Tell1 input bank data
   *
   *  \author Markus Frank
   *  \date   2024-01-13
   */
  class OdinDump : public DataflowComponent {

    /// Property: Flag to print ODIN bank content
    double m_print  { 1.0 };
    
    /// Monitoring quantity: Number of run 1/2 ODIN banks
    mutable std::atomic<long> m_num_run2_odin  { 0 };

    /// Monitoring quantity: Number of run 3 ODIN banks
    mutable std::atomic<long> m_num_run3_odin  { 0 };
    
  public:

    /// Initializing constructor
    OdinDump(const std::string& nam, Context& ctxt)
      : Component(nam, ctxt)
    {
      declareProperty("Print", m_print = 1.0 );
    }
    
    /// Default destructor
    virtual ~OdinDump() = default;

    /// Initialize the algorithm
    int initialize()  override  {
      int sc = this->Component::initialize();
      declareMonitor("Run2OdinCount", m_num_run2_odin = 0, "Number of run 1/2 ODIN bank");
      declareMonitor("Run3OdinCount", m_num_run3_odin = 0, "Number of run 3   ODIN bank");
      return sc;
    }

    /// Start the algorithm
    int start()  override  {
      m_num_run2_odin = 0;
      m_num_run3_odin = 0;
      return this->Component::start();
    }
    
    /// Main execution callback
    int execute(const Context::EventData& event)  override  {
      const uint8_t* start = (uint8_t*)event.data;
      std::size_t len = event.length;
      /// Auto detect data type: now check for MDF data type
      for( const uint8_t *begin=start, *end=start + len; start < end; )   {
	auto* header = (event_header_t*)start;
	if( !header->is_mdf() )   {
	  this->warning("Encountered invalid MDF header: (%d %d %d). Skip %ld bytes of data.",
			header->size0(), header->size1(), header->size2(), start-begin);
	  return DF_SUCCESS;
	}
	bool do_print = !(m_print < 1.0 && m_print < (double(::rand()) / double(RAND_MAX)));
	for(auto [ptr, evt_end] = header->data_frame(); ptr < evt_end; )  {
	  const raw_bank_offline_t *bank = (raw_bank_offline_t*)ptr;
	  long bank_len = bank->totalSize();
	  if( bank_len <= 0 )  {
	    this->warning("Encountered bank with negative length: %s",
			  event_print::bankHeader(bank).c_str());
	    return DF_SUCCESS;
	  }
	  ptr += bank_len;
	  if( bank->type() == bank_types_t::ODIN )    {
	    if( bank->version() < 7 )
	      ++m_num_run2_odin;
	    else if( bank->version() >= 7 )
	      ++m_num_run3_odin;
	    else
	      continue;
	    if( do_print )   {
	      char c = '+';
	      auto lines = event_print::odin_data({bank, bank->data()});
	      for(const auto& line : lines)   {
		this->always("%c%s", c, line.c_str());
		c = '|';
	      }
	      if( bank->version() >= 7 )  {
		const run3_odin_t* sodin = (const run3_odin_t*)bank->data();
		if ( !sodin->is_tae() )  {
		  break;
		}
	      }
	    }
	  }
	}
	start += header->size0();
      }
      return DF_SUCCESS;
    }
  };
}

/// Declare factory for object creation
#include <Dataflow/Plugins.h>
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_OdinDump,OdinDump)
