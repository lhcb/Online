//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  RoutingBitsPrescaler.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include <Dataflow/DataflowComponent.h>
#include <Tell1Data/Tell1Decoder.h>
#include <MBM/Requirement.h>
#include <MBM/bmdef.h>
#include <RTL/rtl.h>

// C/C++ include files
#include <chrono>

/// Online namespace declaration
namespace Online    {

  /**@class RoutingBitsPrescaler RoutingBitsPrescaler.cpp
   *
   * Allows to prescale events given a certain trigger type mask.
   * If the mask is exactly ZERO, it is ignored. Otherwise only
   * a configurable fraction of events, with a non-zero and mask
   * with the ODIN triggertype pass.
   *
   * NOTE: IMPORTANT!!!
   *
   * The RoutingBitsPrescaler component can only handle
   * ONE SINGLE EVENT AT A TIME!
   * This component is not suited for handling bursts.
   * You MUST call the prescaler component seperately for each
   * subevent of a burst.
   * If you prescale an event burst you base the prescaling of the
   * entire burst on the first MDF frame. This is 99.99 % of all 
   * cases not what you want.
   *
   * @author:  M.Frank
   * @version: 1.0
   */
  class myRequirement: public MBM::Requirement  {
  public:
    using time_point = std::chrono::system_clock::time_point;
    using duration   = std::chrono::duration<long, std::ratio<1, 1000> >;
    std::string name;
    time_point  lastEventTime       { time_point(duration(0)) };
    time_point  nextEventNotBefore  { time_point(duration(0)) };
    duration    delayTime           {    0 };
    long        evCounter           {    0 };
    long        eventRejectRlimit   {    0 };
    double      rateLimitRetained   {  0.0 };
    float       rateLimit           { -1.0 };

    myRequirement(const std::string& reqstr) : MBM::Requirement()    {
      char *items[20];
      int   ikey = 0;
      char  rstr[1024];

      ::strcpy(rstr, reqstr.c_str());
      char* token = ::strtok(rstr, ";");
      while (token != NULL)    {
	items[ikey++] = token;
	token = ::strtok(NULL, ";");
      }
      // Requirement format:
      // "NAME=xyz;EvType=x;TriggerMask=0xfeedbabe,0xdeadfeed,0xdeadbabe,0xdeadaffe;
      //  VetoMask=0x,0x,0x,0x;MaskType=ANY/ALL;UserType=USER/VIP/ONE;
      //  Frequency=MANY/PERC;Perc=20.5"
      for( int i = 0; i < ikey; i++)       {
	char* keyw = ::strtok(items[i], "=");
	if (!keyw)
	  break;
	for( unsigned int j = 0; j <  std::strlen(keyw); j++)
	  keyw[j] = char(::toupper(keyw[j]));

	char* values = ::strtok(NULL, "=");
	if( std::strcmp(keyw, "NAME") == 0 )   {
	  name = values;
	  continue;
	}
	if( std::strcmp(keyw, "EVTYPE") == 0 )   {
	  std::sscanf(values, "%d", &evtype);
	  continue;
	}
	if( std::strcmp(keyw, "TRIGGERMASK") == 0 )   {
	  char *v = strtok(values, ",");
	  ::memset(trmask, 0, sizeof(trmask));
	  for( int j = 0; v != 0 && j < 4; j++)  {
	    ::sscanf(v, "%x", &trmask[j]);
	    v = ::strtok(NULL, ",");
	  }
	  continue;
	}
	if( std::strcmp(keyw, "VETOMASK") == 0 )    {
	  std::memset(vetomask, 0, sizeof(vetomask));
	  char *v = strtok(values, ",");
	  for( int j = 0; v != 0 && j < 4; j++)  {
	    ::sscanf(v, "%x", &vetomask[j]);
	    v = strtok(NULL, ",");
	  }
	}
	if( std::strcmp(keyw, "RATELIMIT") == 0 )    {
	  //char *v = strtok(values, ",");
	  std::sscanf(values, "%f", &rateLimit);
	  if (rateLimit > 0.0)  {
	    int idel = int(1000.0/rateLimit)+1;
	    delayTime = std::chrono::milliseconds(idel);
	  }
	  else	{
	    delayTime = duration(0);
	  }
	  ::lib_rtl_output(LIB_RTL_INFO,"Requirement %s applying rate limit %f Hz, delay %ld milliseconds\n",
			   name.c_str(),rateLimit,delayTime);
	}
	if( std::strcmp(keyw, "MASKTYPE") == 0 )    {
	  for( unsigned int j = 0; j < strlen(values); j++)
	    values[j] = char(::toupper(values[j]));

	  maskType = (std::strcmp(values, "ANY") == 0) ? BM_MASK_ANY : BM_MASK_ALL;
	  continue;
	}
	if( std::strcmp(keyw, "USERTYPE") == 0 )   {
	  for( unsigned int j = 0; j < std::strlen(values); j++)
	    values[j] = char(::toupper(values[j]));

	  if (strcmp(values, "USER") == 0)
	    userType = BM_REQ_USER;
	  else if (strcmp(values, "VIP") == 0)
	    userType = BM_REQ_VIP;
	  else if (strcmp(values, "ALL") == 0)
	    userType = BM_REQ_ALL;
	  else{
	    int tmp = *(int*) values;
	    tmp = tmp >> 8;
	    userType = (tmp << 8) + BM_REQ_ONE;
	  }
	  continue;
	}
	if( std::strcmp(keyw, "FREQUENCY") == 0 )    {
	  for( unsigned int j = 0; j < std::strlen(values); j++)
	    values[j] = char(::toupper(values[j]));

	  freqType = std::strcmp(values, "MANY") == 0 ? BM_FREQ_MANY : BM_FREQ_PERC;
	  continue;
	}
	if (strcmp(keyw, "PERC") == 0)    {
	  std::sscanf(values, "%f", &freq);
	  freq /= 100.0;
	  continue;
	}
      }
    }
  };

  class DatStream    {
  public:
    std::string name;
    long        count { 0 };
    int         bit   { 0 };
    int         mask  { 0 };
    int         word  { 0 };

  public:
    DatStream(std::string n, int b) : name(n), bit(b)  {
      mask = 1 << (bit % 32);
      word = bit / 32;
    }
    void clear()   {
      count = 0;
    }
  };

  class _RoutingBitsPrescaler: public DataflowComponent  {

  public:
    typedef std::vector<std::string>   Requirements;
    typedef std::vector<myRequirement> BinRequirements;

    /// Property: String representation of the requirements
    Requirements m_req;
    /// Property: Stream types defined
    std::map<std::string, int> m_streamTypes;

    /// Compiled requirements
    BinRequirements   m_breq;
    /// Decoded data streams
    std::vector<DatStream> m_streams;
    /// Counter for accepted events
    long m_evtAccepted  { 0 };


    _RoutingBitsPrescaler(const std::string& nam, Context& ctxt) :
      DataflowComponent(nam, ctxt)
    {
      unsigned int seed = detail::hash32((RTL::processName() + "@" + RTL::nodeName()).c_str());
      ::srand(seed);
      declareProperty("Requirements", m_req);
      declareProperty("StreamTypes",  m_streamTypes);
      m_evtAccepted = 0;
    }

    /// Default Destructor
    virtual ~_RoutingBitsPrescaler() = default;

    /// Algorithm overload: Initialize the algorithm
    virtual int initialize() override    {
      this->Component::initialize();

      m_breq.clear();
      for( const auto& s : m_req )
	m_breq.emplace_back(myRequirement(s));

      for( auto& i : m_breq )  {
	declareMonitor(i.name, i.evCounter = 0, "Accepted Events");
	declareMonitor(i.name+"/Retained",  i.rateLimitRetained = 0e0, "Rate Limit Retained Fraction");
	declareMonitor(i.name+"/RLimitRej", i.eventRejectRlimit = 0e0, "Rate Limit Rejected");
      }

      m_streams.clear();
      for( const auto& s : m_streamTypes )
	m_streams.emplace_back(DatStream(s.first, s.second));

      for( auto& s : m_streams)
	declareMonitor("Stream/" + s.name, s.count = 0, s.name + " Stream Counter");

      declareMonitor("TotalOut", m_evtAccepted = 0, "Total Events Sent");
      return DF_SUCCESS;
    }

    virtual int start() override  {
      Component::start();
      for( auto& s : m_streams )
	s.clear();

      m_evtAccepted = 0;
      return DF_SUCCESS;
    }

    /// Algorithm overload: Finalize the algorithm
    virtual int finalize() override  {
      this->undeclareMonitors();
      return this->Component::finalize();
    }

    bool matchTrMask(const myRequirement& r, const unsigned int *trm)  {
      bool tmatch = false, vmatch = false;
      if( r.maskType == BM_MASK_ANY )  {
	for( int i = 0; i < 4; i++)  {
	  tmatch = tmatch | ((r.trmask[i]   & trm[i]) != 0);
	  vmatch = vmatch | ((r.vetomask[i] & trm[i]) != 0);
	}
	if( !tmatch || vmatch )  {
	  return false;
	}
      }
      else     {
	for( int i = 0; i < 4; i++ )  {
	  tmatch = tmatch | ((r.trmask[i]   & trm[i]) != r.trmask[i]);
	  vmatch = vmatch | ((r.vetomask[i] & trm[i]) != 0);
	}
	if( !tmatch || vmatch )  {
	  return false;
	}
      }
      return true;
    }

    /// Algorithm overload: Event execution routine
    virtual int execute(const Context::EventData& event) override    {
      auto   trm = event.header->subHeader().H1->triggerMask();
      auto   now = std::chrono::system_clock::now();
      double frac = double(rand()) / double(RAND_MAX);
      bool   trmok = false;

      this->processingFlag = 0;
      for( auto& req : m_breq )  {
	trmok = trmok || matchTrMask(req, &trm[0]);
	if (trmok)    {
	  if( req.freq >= frac )  {
	    this->processingFlag |= DF_PASSED;
	    if( now > req.nextEventNotBefore )    {
	      req.evCounter++;
	      req.nextEventNotBefore = now + req.delayTime;
	      req.rateLimitRetained  = double(req.evCounter)/(req.evCounter+req.eventRejectRlimit);
	      break;
	    }
	    else  {
	      req.eventRejectRlimit++;
	      req.rateLimitRetained = double(req.evCounter)/(req.evCounter+req.eventRejectRlimit);
	      trmok = false;
	    }
	  }
	  else   {
	    trmok = false;
	  }
	}
      }
      if( !trmok )      {
	this->processingFlag &= ~DF_PASSED;
      }
      else      {
	m_evtAccepted++;
      }
      for( auto& s : m_streams )  {
	if( (trm[s.word] & (s.mask)) != 0 )   {
	  s.count++;
	}
      }
      return DF_SUCCESS;
    }
  };
}

#include <Dataflow/Plugins.h>
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online, Dataflow_RoutingBitsPrescaler,_RoutingBitsPrescaler)

