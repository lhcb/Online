//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  RunableWrapper.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include "RunableWrapper.h"
#include <Dataflow/DataflowManager.h>

// C/C++ include files
#include <cstring>

using namespace std;
using namespace Online;

/// Initializing constructor
RunableWrapper::RunableWrapper(const string& nam, Context& ctxt)
  : Component(nam, ctxt)
{ 
  this->declareProperty("Callable", m_callable);
}

/// Default destructor
RunableWrapper::~RunableWrapper()   {
}

/// Initialize the event processor
int RunableWrapper::stop()  {
  if( this->m_loop ) {
    this->m_loop->join();
    this->m_loop.reset();
  }
  return this->Component::stop();
}

/// Finalize the event processor
int RunableWrapper::finalize()  {
  this->m_wrapped = 0;
  if( this->m_loop ) {
    auto tmp = this->m_loop;
    this->m_loop.reset();
    tmp->join();
  }
  return this->Component::finalize();
}

/// Pause the data flow component.
int RunableWrapper::pause()  {
  this->m_wrapped = 0;
  if( this->m_loop ) {
    auto tmp = this->m_loop;
    this->m_loop.reset();
    tmp->join();
  }
  return this->Component::pause();
}

/// Continue the data flow component.
int RunableWrapper::continuing()  {
  if( this->run() == DF_SUCCESS ) {
    return this->Component::continuing();
  }
  return this->error("FAILED to restart event processor %s", m_callable.c_str());
}

/// Execute runable. Start processing thread
int RunableWrapper::run()  {
  DataflowManager&   manager   = this->context.manager.manager();
  DataflowComponent* component = manager.getManagedComponent(this->m_callable, true);
  if( component )  {
    this->m_wrapped = component;
    if( !this->m_loop )    {
      this->m_loop = make_shared<thread>([this,component]{
	try  {
	  component->run(); 
	}
	catch (const exception& e)  {
	  this->error(e,"Exception while running component: '%s'",this->m_callable.c_str());
	}
	catch (...)  {
	  this->error("UNKNOWN exception while running component: '%s'",this->m_callable.c_str());
	}
      });
    }
    if( this->m_loop )   {
      return DF_SUCCESS;
    }
    this->throwError("The event thread for component '%s' cannot be created!",m_callable.c_str());
  }
  return this->throwError("The requested component '%s' is not accessible!",m_callable.c_str());
}


















































































































































































































































