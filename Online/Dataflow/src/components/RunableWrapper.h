//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  RunableWrapper.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_EVENTPROCESSORWRAPPER_H
#define ONLINE_DATAFLOW_EVENTPROCESSORWRAPPER_H

/// Framework include files
#include <Dataflow/DataflowComponent.h>

/// C/C++ include files
#include <memory>
#include <thread>

///  Online namespace declaration
namespace Online  {

  /// Event processing service
  /**
   *  \author  Markus Frank
   *  \version 1.0
   */
  class RunableWrapper : public DataflowComponent  {
  protected:
    /// Event loop thread
    std::shared_ptr<std::thread> m_loop;
    /// Managed runable
    DataflowComponent*           m_wrapped = 0;    
    /// Property: Name of the callbale object started in "run()"
    std::string                  m_callable;

  public:
    /// Initializing constructor
    RunableWrapper(const std::string& name, Context& ctxt);
    /// Default destructor
    virtual ~RunableWrapper();
    /// Stop the data flow component.
    virtual int stop()  override;
    /// Finalize the component
    virtual int finalize()  override;
    /// Pause the data flow component.
    virtual int pause()  override;
    /// Continue the data flow component.
    virtual int continuing()  override;
    /// Execute runable. Start processing thread
    virtual int run()  override;
  };
}      // end namespace Online
#endif //  ONLINE_DATAFLOW_EVENTPROCESSORWRAPPER_H
