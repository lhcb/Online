//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  Sequence.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include "Sequence.h"
#include <Dataflow/ComponentHandler.h>

/// Initializing constructor
Online::Sequence::Sequence(const std::string& nam, Context& ctxt)
  : ComponentContainer(nam, ctxt)
{
  declareProperty("AND", m_and_type = true);
  declareProperty("OR",  m_or_type  = false);
}

/// Default destructor
Online::Sequence::~Sequence()   {
}

/// Initialize the MBM server
int Online::Sequence::initialize()  {
  int sc = ComponentContainer::initialize();
  if(  m_or_type  ) m_and_type = false; // This one got modified
  if( !m_and_type ) m_or_type  = false;  // dito.
  return sc;
}

/// Data processing overload: Send data record to network client
int Online::Sequence::execute(const Context::EventData& event)  {
  try  {
    int ret = DF_SUCCESS;
    ComponentHandler handler(context);
    for( const auto& i : components )  {
      int sc = handler.execute(i,event);
      if( sc != DF_SUCCESS )  {
	processingFlag &= ~DF_PASSED;
	ret = sc;
      }
      else if( m_and_type && ((i->processingFlag&DF_PASSED) == 0) ) {
	processingFlag &= ~DF_PASSED;
	return DF_SUCCESS;
      }
      else if( m_or_type &&  ((i->processingFlag&DF_PASSED) != 0) )  {
	processingFlag |= DF_PASSED;
	return DF_SUCCESS;
      }
    }
    return ret;
  }
  catch( const std::exception& e )   {
    error(e,"Execute: Error processing event.");
  }
  catch( ... )  {
    error("Execute: UNKOWN error processing event.");
  }
  return DF_SUCCESS;
}
