//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  Sequence.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_SEQUENCE_H
#define ONLINE_DATAFLOW_SEQUENCE_H

// Framework include files
#include <Dataflow/ComponentContainer.h>

///  Online namespace declaration
namespace Online  {

  /// Sequence component to be applied in the manager sequence
  /**
   * Sequences are specified in the job options as floating point numbers.
   * The sequence unit is in seconds.
   *
   * @author  Markus Frank
   * @version 1.0
   */
  class Sequence : public ComponentContainer  {
  protected:
    /// Property: AND - type algorithm
    bool m_and_type;
    /// Property: OR - type algorithm
    bool m_or_type;
  public:
    /// Initializing constructor
    Sequence(const std::string& name, Context& ctxt);
    /// Default destructor
    virtual ~Sequence();
    /// Initialize the MBM server
    virtual int initialize()  override;
    /// Data processing overload: Send data record to network client
    virtual int execute(const Context::EventData& event)  override;
  };
}      // end namespace Online
#endif //  ONLINE_DATAFLOW_SEQUENCE_H
