//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  SocketComponents.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//  Description: Network event data server 
//               using boost::asio asynchronous data I/O
//
//  Author     : Markus Frank
//==========================================================================

#define TRANSFER_NS        DataTransfer
#define EventServer        Dataflow_SocketEventServer
#define EventRequestServer Dataflow_SocketEventRequestServer
#define DataSelector       Dataflow_SocketSelector
#define DataSender         Dataflow_SocketSender
#define DataReceiver       Dataflow_SocketReceiver

#include <NET/Transfer.h>
#include <Dataflow/EventServer.h>
#include <Dataflow/DataSelector.h>
#include <Dataflow/DataSender.h>
#include <Dataflow/DataReceiver.h>
