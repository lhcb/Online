//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  Receiver.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include <Dataflow/DataflowComponent.h>

///  Online namespace declaration
namespace Online  {

  /// Components to wait for event actions.
  /**
   *  \author  M.Frank
   *  \version 1.0
   *  \ingroup DD4HEP_SIMULATION
   */
  template<typename T> class StandardRunable : public DataflowComponent  {
  public:
    int end;
  public:
    /// Initializing constructor
    StandardRunable(const std::string& nam, Context& ctxt);
    /// Default destructor
    virtual ~StandardRunable();
    /// Configure the instance (create components)
    virtual int run()  override;
  };

  /// Initializing constructor
  template <typename T> inline 
  StandardRunable<T>::StandardRunable(const std::string& nam, Context& ctxt)
    : DataflowComponent(nam,ctxt), end(0)
  {
    declareProperty("End",end);
  }

  /// Default destructor
  template <typename T> inline 
  StandardRunable<T>::~StandardRunable() {
  }

}      // end namespace Online

#include <Dataflow/Plugins.h>
#include <CPP/IocSensor.h>
#include <RTL/rtl.h>

namespace Online   {

  /// Default, 'do-nothing' runable
  namespace { struct _default {};   }
  typedef StandardRunable<_default> DefaultRunable;
  template <> int StandardRunable<_default>::run()  {
    return DF_SUCCESS;
  }

  /// Sensor runable. Hang in wtc_wait.
  namespace { struct _sensor {};   }
  typedef StandardRunable<_sensor> SensorRunable;
  template <> int StandardRunable<_sensor>::run()  {
    IOCSENSOR.run();
    return DF_SUCCESS;
  }

  /// Sleeper runable. Sleeps until the end flag ist set via the property mechanism.
  namespace { struct _sleep {};   }
  typedef StandardRunable<_sleep> SleepRunable;
  template <> int StandardRunable<_sleep>::run()  {
    while( !end )   {
      ::lib_rtl_sleep(1000);
    }
    return DF_SUCCESS;
  }
}

/// Declare the corresponding component factories:
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_DefaultRunable,DefaultRunable)
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_SensorRunable,SensorRunable)
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_SleepRunable,SleepRunable)
