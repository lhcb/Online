//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : DAQ/MDF
//
//  Author     : Markus Frank
//==========================================================================

/// Include files from Gaudi
#include <Dataflow/DataflowComponent.h>
#include <EventData/raw_bank_offline_t.h>
#include <EventData/event_header_t.h>
#include <EventData/bank_header_t.h>
#include <EventData/odin_t.h>
#include <Tell1Data/Tell1Decoder.h>

/// C/C++ include files
#include <climits>
#include <atomic>

/// Online namespace declaration
namespace Online  {

  /// Class to dump the ODIN bank from a single beam crossings
  /** Class to dump the ODIN bank from a single beam crossings
   *  Dump Odin bank from Tell1 input bank data
   *
   *  \author Markus Frank
   *  \date   2024-01-13
   */
  class TAEDump : public DataflowComponent {

    /// Property: Frequency to print bank content
    double m_print     { 1.0 };
    /// Property: Number of events where all dump flags should be considered true
    long   m_maxEvents { LONG_MAX };

    /// Monitoring quantity: Event counter
    long   m_numEvent  { 0 };
    
  public:

    /// Initializing constructor
    TAEDump(const std::string& nam, Context& ctxt)
      : Component(nam, ctxt)
    {
      declareProperty("Print",     m_print = 1.0 );
      declareProperty("MaxEvents", m_maxEvents = LONG_MAX );
    }
    /// Default destructor
    virtual ~TAEDump() = default;

    /// Initialize the algorithm
    int initialize()  override  {
      int sc = this->Component::initialize();
      declareMonitor("EventCount", m_numEvent = 0, "Number of events seen");
      return sc;
    }
    /// Algorithm initialization
    int finalize() override  {
      m_numEvent = 0;
      return this->Component::finalize();
    }
    /// Start the algorithm
    int start()  override  {
      m_numEvent = 0;
      return this->Component::start();
    }
    ///
    int print_event(const uint8_t* start, const uint8_t* end)  {
      bool do_print = !(m_print < 1.0 && m_print < (double(::rand()) / double(RAND_MAX)));
      if( do_print )  {
	++m_numEvent;
	for( const uint8_t* ptr=start; ptr < end; )  {
	  const raw_bank_offline_t *tae = (raw_bank_offline_t*)ptr;
	  if( tae->type() == bank_types_t::TAEHeader )  {
	    auto lines = event_print::taeData(tae);
	    for( std::size_t i=0; i < lines.size(); ++i )
	      this->always("%c %s", (i==0) ? '+' : '|', lines[i].c_str());
	  }
	  ptr += tae->totalSize();
	}
      }
      return DF_SUCCESS;
    }
    /// Main execution callback
    int execute(const Context::EventData& event)  override  {
      if( m_numEvent < m_maxEvents )   {
	std::size_t    len   = event.length;
	const uint8_t* start = (uint8_t*)event.data;
	/// Auto detect data type: now check for MDF data type
	for( const uint8_t *begin=start, *end=start + len; start < end; )  {
	  auto* header = (event_header_t*)start;
	  if( !header->is_mdf() )  {
	    this->warning("Encountered invalid MDF header: (%d %d %d). Skip %ld bytes of data.",
			  header->size0(), header->size1(), header->size2(), start-begin);
	    return DF_SUCCESS;
	  }
	  auto [first, last] = header->data_frame();
	  this->print_event(first, last);
	  start += header->size0();
	}
      }
      return DF_SUCCESS;
    }
  };
}

/// Declare factory for object creation
#include <Dataflow/Plugins.h>
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_TAEDump,TAEDump)
