//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  Tell1BankDecoder.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_TELL1BANKDECODER_H
#define ONLINE_DATAFLOW_TELL1BANKDECODER_H

// Framework include files
#include <Dataflow/DataflowComponent.h>

///  Online namespace declaration
namespace Online  {

  /// Tell1BankDecoder component to be applied in the manager sequence
  /**
   *  Decode a frame of Tell1 banks in the LHCb online system.
   *
   * @author  Markus Frank
   * @version 1.0
   */
  class Tell1BankDecoder : public DataflowComponent  {
  protected:
    /// Property: String representation of the key to be added to the event context
    std::string  m_keyString;
    /// Key value derived from the string property
    unsigned int m_key;
    
  public:
    /// Initializing constructor
    Tell1BankDecoder(const std::string& name, Context& ctxt);
    /// Default destructor
    virtual ~Tell1BankDecoder();
    /// Initialize the decoder component
    virtual int initialize()  override;
    /// Data processing overload: process event
    virtual int execute(const Context::EventData& event)  override;
  };
}      //  end namespace Online
#endif //  ONLINE_DATAFLOW_TELL1BANKDECODER_H

//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  Tell1BankDecoder.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include <EventData/raw_bank_offline_t.h>
#include <Tell1Data/Tell1Decoder.h>
#include <Dataflow/EventContext.h>
#include <DD4hep/Primitives.h>

// C/C++ include files
#include <vector>

#include <Dataflow/Plugins.h>
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_Tell1BankDecoder,Tell1BankDecoder)


/// Initializing constructor
Online::Tell1BankDecoder::Tell1BankDecoder(const std::string& nam, Context& ctxt)
: Component(nam,ctxt), m_key(0)
{
  declareProperty("Key", m_keyString="/Event/RawBanks");
}

/// Default destructor
Online::Tell1BankDecoder::~Tell1BankDecoder()  {
}

/// Initialize the decoder component
int Online::Tell1BankDecoder::initialize()  {
  int sc = Component::initialize();
  m_key  = detail::hash32(m_keyString);
  return sc;
}

/// Data processing overload: process event
int Online::Tell1BankDecoder::execute(const Context::EventData& event)  {
  using Banks = std::vector<raw_bank_offline_t*>;
  if( event.data )  {
    if( event.context )  {
      Banks& banks = event.context->add<Banks>(m_key);
      int sc = Online::decodeRawBanks(event.data, event.data+event.length, banks);
      if( sc == DF_SUCCESS )  {
	return DF_SUCCESS;
      }
    }
  }
  return DF_ERROR;
}
