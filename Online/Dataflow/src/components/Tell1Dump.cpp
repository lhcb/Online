//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : DAQ/MDF
//
//  Author     : Markus Frank
//==========================================================================

/// Include files from Gaudi
#include <Dataflow/DataflowComponent.h>
#include <EventData/raw_bank_offline_t.h>
#include <EventData/event_header_t.h>
#include <EventData/bank_header_t.h>
#include <EventData/odin_t.h>
#include <RTL/rtl.h>

/// C/C++ include files
#include <atomic>

/// Online namespace declaration
namespace Online  {

  /// Class to dump the bank data from a single beam crossings or TAE event
  /** Class to dump the bank data from a single beam crossings or TAE event
   *
   *  \author Markus Frank
   *  \date   2024-01-13
   */
  class Tell1Dump : public DataflowComponent {

    /// Property: Frequency to print bank content
    double m_print     {   1.0 };
    /// Property: DumpData:  If true, bank headers are dumped
    bool   m_dump      { false };
    /// Property: FullDump:  If true, full bank contents are dumped
    bool   m_full      { false };
    /// Property: Summary:   If true, print bank summary at stop
    bool   m_summary   {  true };
    /// Property: CheckData: If true, full bank contents are checked
    bool   m_check     { false };
    /// Property: CheckData: If true, full bank contents are checked
    bool   m_splitTAE  {  true };
    /// Property: Number of events where all dump flags should be considered true
    long   m_maxEvents {     0 };

    /// Monitoring quantity: Event counter
    long   m_numEvent  {     0 };
    /// Bank type counters
    std::map<bank_types_t::BankType, long> m_bankCounters;
    
  public:

    /// Initializing constructor
    Tell1Dump(const std::string& nam, Context& ctxt)
      : Component(nam, ctxt)
    {
      declareProperty("Full",      m_full );
      declareProperty("Dump",      m_dump );
      declareProperty("Print",     m_print );
      declareProperty("Summary",   m_summary );
      declareProperty("MaxEvents", m_maxEvents );
      declareProperty("SplitTAE",  m_splitTAE );
    }
    
    /// Default destructor
    virtual ~Tell1Dump() = default;

    /// Initialize the algorithm
    int initialize()  override  {
      int sc = this->Component::initialize();
      declareMonitor("EventCount", m_numEvent = 0, "Number of events seen");
      return sc;
    }
        /// Algorithm initialization
    int finalize() override  {
      if( m_summary )  {
	this->always("+----> Events total: %-6d", long(m_numEvent));
	for( const auto& cnt : m_bankCounters )  {
	  auto btyp = cnt.first;
	  this->always("| Found a total of %8d bank(s) of type %3d (%s)",
		       cnt.second, btyp, event_print::bankType(btyp).c_str());
	}
      }
      m_numEvent = 0;
      m_bankCounters.clear();
      return this->Component::finalize();
    }

    /// Start the algorithm
    int start()  override  {
      m_numEvent = 0;
      m_bankCounters.clear();
      return this->Component::start();
    }
    ///
    std::string _bx_offset(int32_t bx)  const  {
      std::string offset;
      int32_t cr = std::abs(bx);
      if ( cr > 99 ) offset += char('0'+((cr/100)%100));
      if ( cr > 9  ) offset += char('0'+((cr/10)%10));
      offset += char('0'+(cr%10));
      return offset;
    }

    using bankpointers_t = std::map<int, std::vector<std::pair<const bank_header_t*, const void*> > >;

    bankpointers_t collect(const uint8_t* start, const uint8_t* end)  {
      bankpointers_t bank_pointers;
      for( const uint8_t* ptr=start; ptr < end; )  {
	const bank_header_t *h = (bank_header_t*)ptr;
	if ( h->magic() == raw_bank_offline_t::MagicPattern )   {
	  bank_pointers[h->type()].emplace_back(h, lib_rtl_add_ptr(h, sizeof(bank_header_t)));
	}
	else  {
	  break;
	}
	++m_bankCounters[bank_types_t::BankType(h->type())];
	ptr += h->totalSize();
      }
      return bank_pointers;
    }

    void print_bank_collection(const std::string& which,
			       const bankpointers_t& bank_pointers)  {
      bool dmp = m_numEvent < m_maxEvents || m_dump;
      bool chk = m_numEvent < m_maxEvents || m_check;
      bool ful = m_numEvent < m_maxEvents || m_full;
      for( const auto& b : bank_pointers)  {
	const auto btyp = b.first;
	const auto& banks = b.second;
	int k = 0;
	if( dmp )  {
	  info("+----> Event No: %-6ld %s has %3d  bank(s) of type %3d (%s)",
	       long(m_numEvent), which.c_str(), banks.size(), btyp,
	       event_print::bankType(btyp).c_str());
	}
	for( const auto& bank : banks )  {
	  int cnt, inc = (btyp == bank_types_t::Rich) ? 64 : 32;
	  const bank_header_t* hdr = bank.first;
	  if( hdr->size() > 0 )  {
	    if( dmp )  {
	      info("+ Bank:  %s", event_print::bankHeader(hdr).c_str());
	    }
	    if( ful ) {
	      bool h  = true;
	      auto lines = event_print::bankData(bank.second, hdr->size(), 10);
	      for( const auto& line : lines )  {
		info(h ? "| Data:  %s" : "|        %s", line.c_str());
		h = false;
	      }
	    }
	    if( chk ) { // Check the patterns put in by RawEventCreator
	      int kc = k;
	      int ks = k+1;
	      if( hdr->type() != bank_types_t::DAQ )  {
		const int *p, *end;
		if( hdr->size() != inc*ks )  {
		  error("!! Bad bank size: %d expected: %d", hdr->size(), ks*inc);
		}
		if( hdr->sourceID() != kc )  {
		  error("!! Bad source ID: %d expected: %d", hdr->sourceID(), kc); 
		}
		for( p=(int*)bank.second, end=p+(hdr->size()/sizeof(int)), cnt=0; p != end; ++p, ++cnt)  {
		  if( *p != cnt )
		    error("!! Bad BANK DATA: %d", *p);
		}
		if( cnt != (inc*ks)/int(sizeof(int)) )  {
		  error("!! Bad amount of data in bank: %d word", cnt);
		}
	      }
	    }
	  }
	}
      }
    }
    
    int print_event(const uint8_t* start, const uint8_t* end)  {
      bool do_print = !(m_print < 1.0 && m_print < (double(::rand()) / double(RAND_MAX)));
      if( do_print )  {
        bankpointers_t bank_pointers;
	const raw_bank_offline_t *tae = (raw_bank_offline_t*)start;

	if( m_splitTAE && tae->type() == bank_types_t::TAEHeader )  {
	  const int*  data  = tae->begin<int>();
	  int32_t     count_bx  = tae->size()/sizeof(int)/3;
	  int32_t     half_win  = (count_bx-1)/2;
	  std::string prev = "/Prev", next = "/Next";

	  start += tae->totalSize();
	  this->always("+ TAE: Half window: %d", half_win);
	  for(int32_t i=-half_win; i<=half_win; ++i)   {
	    int32_t idx = i + half_win;
	    int32_t bx  = data[3*idx];
	    int32_t off = data[3*idx+1];
	    int32_t len = data[3*idx+2];
	    std::string item = bx == 0 ? "/Central" : bx<0 ? prev + _bx_offset(bx) : next + _bx_offset(-bx);
	    this->always("+------------------------------------------------------------------------------");
	    this->always("+ TAE fragment: %-16s  id: %2d bx: %2d offset:%8d bytes, length:%d bytes",
			 item.c_str(), idx, bx, off, len);
	    bank_pointers = collect(start+off, start+off+len);
	    print_bank_collection(item, bank_pointers);
	  }
	}
	else  {
	  collect(start, end);
	  print_bank_collection("/Central", bank_pointers);
	}
      }
      return DF_SUCCESS;
    }

    /// Main execution callback
    int execute(const Context::EventData& event)  override  {
      const uint8_t* start = (uint8_t*)event.data;
      std::size_t len = event.length;
      /// Auto detect data type: now check for MDF data type
      for( const uint8_t *begin=start, *end=start + len; start < end; )  {
	auto* header = (event_header_t*)start;
	if( !header->is_mdf() )  {
	  this->warning("Encountered invalid MDF header: (%d %d %d). Skip %ld bytes of data.",
			header->size0(), header->size1(), header->size2(), start-begin);
	  return DF_SUCCESS;
	}
	auto [first, last] = header->data_frame();
	++m_numEvent;
	print_event(first, last);
	start += header->size0();
      }
      return DF_SUCCESS;
    }
  };
}

/// Declare factory for object creation
#include <Dataflow/Plugins.h>
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_Tell1Dump,Tell1Dump)
