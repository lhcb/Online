//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : DAQ/MDF
//
//  Author     : Markus Frank
//==========================================================================

/// Include files from Gaudi
#include <Dataflow/DataflowComponent.h>
#include <EventData/event_header_t.h>

/// C/C++ include files
#include <atomic>

/// Online namespace declaration
namespace Online  {

  /// Class to dump the ODIN bank from a single beam crossings
  /** Class to dump the event header
   *  Dump Tell1 event header
   *
   *  \author Markus Frank
   *  \date   2024-01-13
   */
  class Tell1HeaderDump : public DataflowComponent {

    /// Property: Flag to print ODIN bank content
    double m_print  { 1.0 };
    /// Monitoring quantity: Number of events processed
    std::atomic<long> m_numEvent  { 0 };
    
  public:

    /// Initializing constructor
    Tell1HeaderDump(const std::string& nam, Context& ctxt)
      : Component(nam, ctxt)
    {
      declareProperty("Print", m_print = 1.0 );
    }
    
    /// Default destructor
    virtual ~Tell1HeaderDump() = default;

    /// Initialize the algorithm
    int initialize()  override  {
      int sc = this->Component::initialize();
      declareMonitor("NumEvent", m_numEvent, "Number of events");
      return sc;
    }

    /// Start the algorithm
    int start()  override  {
      m_numEvent = 0;
      return this->Component::start();
    }
    
    /// Main execution callback
    int execute(const Context::EventData& event)  override  {
      bool do_print = !(m_print < 1.0 && m_print < (double(::rand()) / double(RAND_MAX)));
      if ( do_print )   {
	auto lines = event_print::headerData(event.header);
	for(const auto& line : lines)   {
	  this->always(line);
	}
	++m_numEvent;
      }
      return DF_SUCCESS;
    }
  };
}

/// Declare factory for object creation
#include <Dataflow/Plugins.h>
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_Tell1HeaderDump,Tell1HeaderDump)
