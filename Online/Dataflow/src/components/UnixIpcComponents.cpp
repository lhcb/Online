//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  AsioIpcComponents.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//  Description: Network event data server 
//               using boost::asio asynchronous data I/O
//
//  Author     : Markus Frank
//==========================================================================

#define TRANSFER_NS        BoostAsioIpc
#define EventServer        Dataflow_UnixIpcEventServer
#define EventRequestServer Dataflow_UnixIpcEventRequestServer
#define DataSelector       Dataflow_UnixIpcSelector
#define DataSender         Dataflow_UnixIpcSender
#define DataReceiver       Dataflow_UnixIpcReceiver

#include <NET/Transfer.h>
#include <Dataflow/EventServer.h>
#include <Dataflow/DataSelector.h>
#include <Dataflow/DataSender.h>
#include <Dataflow/DataReceiver.h>
