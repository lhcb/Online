//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  BurstReader.h
//--------------------------------------------------------------------------
//
//  Package    : GaudiOnline
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_UPGRADEMEPREADER_H
#define ONLINE_DATAFLOW_UPGRADEMEPREADER_H

// Framework includes
#include <Dataflow/DiskReader.h>

// C/C++ include files

///   Online namespace declaration
namespace Online  {

  /// Basic HLT2 file reader for the deferred triggering
  /** Class definition of UpgradeMEPReader.
   *
   * This is the online extension of the runnable of the application manager.
   * The runable handles the actual run() implementation of the
   * ApplicationMgr object.
   *
   * @author Markus Frank
   *
   * @version 1.5
   */
  class UpgradeMEPReader : public DiskReader  {
  public:
    /// Header prefix length in MBM file
    size_t m_headerPrefixSize = 0u;

    size_t m_preload = 0u;

    int initialize() override;

    /// Runable implementation : Run the class implementation
    virtual int    i_run()  override;


  public:
    /// Standard Constructor
    UpgradeMEPReader(const std::string& nam, DataflowContext& ctxt);

    /// Standard Destructor
    virtual ~UpgradeMEPReader() = default;

  private:

    int i_run_preloaded();
    int i_run_default();

    std::vector<std::tuple<std::string, std::vector<unsigned char>>> m_preloaded;

  };
} // End namespace Online
#endif // ONLINE_DATAFLOW_UPGRADEMEPREADER_H

//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  UpgradeMEPReader.h
//--------------------------------------------------------------------------
//
//  Package    : GaudiOnline
//
//  Author     : Markus Frank
//==========================================================================

// Framework includes
#include <Dataflow/DataflowTask.h>
#include <Dataflow/MBMClient.h>
#include <Dataflow/Plugins.h>
#include <PCIE40Data/pcie40.h>
// C/C++ include files
#include <cstring>

// Instantiation of a static factory class used by clients to create instances of this service
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_UpgradeMEPReader,UpgradeMEPReader)

using namespace std;
using namespace Online;

/// Standard Constructor
UpgradeMEPReader::UpgradeMEPReader(const std::string& nam, DataflowContext& ctxt)
  : DiskReader(nam, ctxt)
{
  declareProperty("HeaderPrefixSize", m_headerPrefixSize = 0);
  declareProperty("Preload", m_preload = 0);
}

int Online::UpgradeMEPReader::initialize() {
  if (m_preload > 0) {
    m_preloaded.resize(m_preload);

    DiskIO io{*this};
    io.scan_files();
    RawFile current;

    size_t preload = 0;
    while (preload < m_preload) {
      if ( !current.isOpen() ) {
        current = io.openFile();
        if ( !current.isOpen() ) {
          error("Failed to read sufficient MEPs");
          return DF_ERROR;
        }
      }

      auto file_length = current.data_size();
      off_t file_position = current.position();
      pcie40::mep_header_t mep_hdr, *pmep_hdr;
      pmep_hdr = &mep_hdr;
      size_t data_len = 0;
      if ( sizeof(mep_hdr) != current.read(pmep_hdr, sizeof(mep_hdr)) ) {
        this->closeFile(current, true);
        error("Failed to read MEP header from %s", current.cname());
        continue;
      }
      data_len = pmep_hdr->size * sizeof(uint32_t);
      if ( data_len < 128 )   {
        this->closeFile(current, true);
        error("Invalid data length MEP %ld from %s", data_len, current.cname());
        continue;
      }
      else if ( file_length - file_position < data_len - sizeof(mep_hdr) )  {
        this->closeFile(current, true);
        error("Not enough data left for MEP of size %ld in %s", data_len, current.cname());
        continue;
      }
      else if ( !pmep_hdr->is_valid() )   {
        error("Invalid MEP header found. Drop rest of file %ld -> %ld %s. ",
              file_position, file_length, current.cname());
        this->closeFile(current, true);
        continue;
      }
      auto& [filename, buffer] = m_preloaded[preload];
      filename = current.name();
      buffer.resize(data_len);
      unsigned char* data = &buffer[0];
      ::memcpy(data, pmep_hdr, sizeof(mep_hdr));
      long rd_len = data_len - sizeof(mep_hdr);
      if ( rd_len != current.read(data + sizeof(mep_hdr), rd_len) )   {
        this->closeFile(current, true);
        error("Failed to read MEP of size %ld from %s", data_len, current.cname());
        continue;
      }
      else {
        pmep_hdr = reinterpret_cast<pcie40::mep_header_t*>(&data[0]);
        auto packing = pmep_hdr->multi_fragment(0)->header.packing;
        info("Preloaded MEP with packing factor %ld from %s [%03d/%d]", packing, current.cname(), preload + 1, m_preload);
      }

      if (static_cast<size_t>(current.position()) == current.data_size()) {
        this->closeFile(current, true);
      }
      ++preload;
    }
  }

  return DiskReader::initialize();
}

int Online::UpgradeMEPReader::i_run()  {
  if (m_preload > 0) {
    return i_run_preloaded();
  }
  else {
    return i_run_default();
  }
}

/// IRunable implementation : Run the class implementation
int Online::UpgradeMEPReader::i_run_preloaded()  {
  auto preloaded = m_preloaded.begin();
  int     partid = context.mbm->partitionID();
  MBM::BufferInfo* mbmInfo = m_mbmInfo[m_buffer];
  int    status = 0;
  int    mep_number = 0;
  int    cons_wait = 0;

  m_eventsIN = 0;
  m_goto_paused = true;
  //m_deleteFiles = false;
  //m_saveRest    = false;
  //m_mmapFiles   = true;
  m_receiveEvts = true;

  status = waitForGo();
  if ( status != DF_CONTINUE )  {
    return status;
  }

  while (1)  {
    if ( (m_goValue != GO_PROCESS) )   {
      /// Before actually declaring PAUSED, we wait until no events are pending anymore.
      waitForPendingEvents(m_maxPauseWait);
      /// Go to state PAUSED, all the work is done
      if ( m_goto_paused )  {
        // Sleep a bit before goung to pause
        info("Sleeping before going to PAUSE....");
        ::lib_rtl_sleep(1000*m_pauseSleep);
        fireIncident("DAQ_PAUSE");
        if ( context.task )   {
          context.task->declareState(Control::ST_PAUSED);
        }
      }
      info("Quitting...");
      break;
    }

    updateRunNumber(0);
    m_evtCountFile = 0;

    // loop over the events
    while ( m_receiveEvts && (m_goValue != GO_DONT_PROCESS) )   {
      if (m_goValue == GO_PROCESS)  {
        preloaded = m_preloaded.begin();
        m_evtCountFile = 0;

        int runno = 0;
        std::string filename = std::get<0>(*preloaded);
        size_t idx = filename.rfind('/')+1+m_filePrefix.length();
        const char* ptr = filename.c_str()+idx;
        if ( 1 != ::sscanf(ptr, "%010d", &runno) )   {
          ::sscanf(ptr, "%08d", &runno);
        }
        updateRunNumber(runno);
        /* std::size_t file_length = std::get<1>(*preloaded).size(); */
      }

      MBM::EventDesc& dsc = m_producer->event();
      MBMAllocator  allocator(this);

      pcie40::mep_header_t mep_hdr, *pmep_hdr;
      pmep_hdr = &mep_hdr;
      int           data_len = 0, event_type;
      uint16_t      packing = 0;
      // Check if data are still availible
      pmep_hdr = reinterpret_cast<pcie40::mep_header_t*>(std::get<1>(*preloaded).data());

      data_len    = pmep_hdr->size * sizeof(uint32_t);
      event_type  = EVENT_TYPE_MEP;
      if ( !pmep_hdr->is_valid() )   {
        error("Invalid MEP header found, skip preloaded MEP.");
        continue;
      }
      unsigned char* data   = allocator(data_len);
      if ( data == 0 || dsc.len == 0 )   { // MBM Error
        // Set back the file position to the beginning of the event and continue.
        // If there was a cancel in between, the file shall be saved.
        continue;
      }
      dsc.len = data_len;
      ::memcpy(data, std::get<1>(*preloaded).data(), data_len);
      //current.position(file_position + frame_len);

      pmep_hdr = (pcie40::mep_header_t*)data;
      packing  = pmep_hdr->multi_fragment(0)->header.packing;
      ++mep_number;
      m_eventsIN += packing;
      // If MEP, we emulate a trigger mask with the partition ID
      dsc.type    = event_type;
      dsc.mask[0] = partid;
      dsc.mask[1] = ~0x0;
      dsc.mask[2] = ~0x0;
      dsc.mask[3] = ~0x0;
      // Check if there are consumers pending before declaring the event.
      // This should be a rare case, since there ARE (were?) consumers.
      // Though: In this case the event is really lost!
      // But what can I do...
      cons_wait = m_maxConsWait;
      if ( m_requireConsumers )  {
        for(cons_wait = m_maxConsWait; !check_consumers(*mbmInfo,partid,dsc.type) && --cons_wait>=0; )  {
          ::lib_rtl_sleep(1000);
          if ( !m_receiveEvts ) cons_wait = 0; // Force writing back opened file in case if cancel etc.
        }
      }
      if ( cons_wait <= 0 )  {
        error("No consumers (partition:%d event type:%d) present to consume event %d",
              partid,dsc.type,mep_number);
        m_receiveEvts = false;
        /// Before actually declaring ERROR, we wait until no events are pending anymore.
        waitForPendingEvents(m_maxConsWait);
        /// Go to state PAUSED, all the work is done
        fireIncident("DAQ_ERROR");
        return DF_ERROR;
      }
      {
        lock_guard<mutex> lck (m_prodLock);
        //
        // Declare the event to the buffer manager
        //
        try {
          status = m_producer->declareEvent();
        }
        catch (const exception& e)        {
          info("Exception while delareEvent: %s Event:%d.",
               e.what(),mep_number);
          status = MBM_ERROR;
        }
        catch(...)   {
          info("UNKNOWN Exception while delareEvent: Event:%d File:%s.",
               mep_number, std::get<0>(*preloaded).c_str());
          status = MBM_ERROR;
        }
        if (status != MBM_NORMAL)    {
          continue;
        }
        //
        // Now send space
        //
        try  {
          status = m_producer->sendSpace();
        }
        catch (const exception& e)        {
          info("Exception while sendSpace: %s Event:%d File:%s.",
               e.what(),mep_number,std::get<0>(*preloaded).c_str());
          status = MBM_ERROR;
        }
        catch(...)   {
          info("UNKNOWN Exception while sendSpace: Event:%d File:%s.",
               mep_number,std::get<0>(*preloaded).c_str());
          status = MBM_ERROR;
        }
      }
      if (status != MBM_NORMAL)    {
        continue;
      }
      else  {
        m_eventsOUT += packing;
        m_evtCountFile++;
        // If we have exceeded the total number of events per file, close it!
        if ( m_max_events_per_file>0 && m_evtCountFile>m_max_events_per_file )  {
          ++preloaded;
        }
      }
    }
    ++preloaded;
  }
  m_eventsIN = 0;
  m_eventsOUT = 0;
  /// Before actually declaring PAUSED, we wait until no events are pending anymore.
  waitForPendingEvents(m_maxPauseWait);
  return DF_SUCCESS;
}

/// IRunable implementation : Run the class implementation
int Online::UpgradeMEPReader::i_run_default()  {
  DiskIO  io(*this);
  RawFile current;
  int     status;
  int     cons_wait;
  int     mep_number = 0;
  int     partid = context.mbm->partitionID();
  bool    files_processed = false;
  MBM::BufferInfo* mbmInfo = m_mbmInfo[m_buffer];
  size_t file_length = 0;

  m_eventsIN = 0;
  m_goto_paused = true;
  //m_deleteFiles = false;
  //m_saveRest    = false;
  //m_mmapFiles   = true;
  m_receiveEvts = true;

  status = waitForGo();
  if ( status != DF_CONTINUE )  {
    return status;
  }

  while (1)  {
    files_processed = (GO_PROCESS==m_goValue) && (io.scan_files() == 0);

    // Handle case to go to paused or change of the GO value
    if ( (m_goto_paused && files_processed) || (m_goValue != GO_PROCESS) )   {
      if ( current.isOpen() )  {
        if ( m_deleteFiles && m_saveRest ) current.saveRestOfFile();
	this->closeFile(current);
	updateRunNumber(0);
      }

      /// Before actually declaring PAUSED, we wait until no events are pending anymore.
      waitForPendingEvents(m_maxPauseWait);
      /// Go to state PAUSED, all the work is done
      if ( m_goto_paused )  {
        // Sleep a bit before goung to pause
        info("Sleeping before going to PAUSE....");
        ::lib_rtl_sleep(1000*m_pauseSleep);
        fireIncident("DAQ_PAUSE");
        if ( context.task )   {
          context.task->declareState(Control::ST_PAUSED);
        }
      }
      info("Quitting...");
      break;
    }
    else if ( files_processed ) {
      info("Locking event loop. Waiting for work....");
      updateRunNumber(0);
      ::lib_rtl_lock(m_lock);
    }
    // Again check if the GO value changed, since we may have been waiting in the lock!
    if ( !m_receiveEvts || (m_goValue == GO_DONT_PROCESS) )    {
      if ( current.isOpen() )  {
        if ( m_deleteFiles && m_saveRest ) current.saveRestOfFile();
	this->closeFile(current);
      }
      info("Quitting...");
      break;
    }
    updateRunNumber(0);
    m_evtCountFile = 0;
    files_processed = (GO_PROCESS != m_goValue) && io.scan_files() == 0;
    if ( files_processed )    {
      info("Exit event loop. No more files to process.");
      break;
    }
    // loop over the events
    while ( m_receiveEvts && (m_goValue != GO_DONT_PROCESS) )   {
      if ( !current.isOpen() && (m_goValue == GO_PROCESS) )  {
        current = io.openFile();
        m_evtCountFile = 0;
        if ( !current.isOpen() )   {
          files_processed = (m_goValue != GO_PROCESS) || (io.scan_files() == 0);
          if ( files_processed )    {
            break;
          }
        }
	else   {
	  int runno = 0;
	  size_t idx = current.name().rfind('/')+1+m_filePrefix.length();
	  const char* ptr = current.name().c_str()+idx;
	  if ( 1 != ::sscanf(ptr, "%010d", &runno) )   {
	    ::sscanf(ptr, "%08d", &runno);
	  }
	  updateRunNumber(runno);
	  file_length = current.data_size();
	}
      }
      if ( current.isOpen() )  {
	MBM::EventDesc& dsc = m_producer->event();
	MBMAllocator  allocator(this);
        off_t         file_position = current.position();
	pcie40::mep_header_t mep_hdr, *pmep_hdr;
	int           data_len = 0, event_type;
	uint16_t      packing = 0;
	long          frame_len = 0;
	// Check if data are still availible
	if ( m_mmapFiles && current.pointer()+sizeof(pcie40::mep_header_t) > current.end() )   {
	  this->closeFile(current, !m_mmapFiles);
	  continue;
	}
	if ( m_headerPrefixSize > 0 )  {
	  current.read(&frame_len, sizeof(long));
	}
	if ( sizeof(mep_hdr) != current.read(&mep_hdr, sizeof(mep_hdr)) )    {
	  this->closeFile(current, !m_mmapFiles);
	  continue;
	}

	data_len    = mep_hdr.size*sizeof(uint32_t);
	event_type  = EVENT_TYPE_MEP;
	if ( data_len <= 0 )   {
	  this->closeFile(current, !m_mmapFiles);
	  mep_number = 0;
	  continue;
	}
	else if ( data_len < 128 )   {
	  this->closeFile(current, !m_mmapFiles);
	  mep_number = 0;
	  continue;
	}
	else if ( m_mmapFiles && current.pointer()-sizeof(mep_hdr)+data_len > current.end() )   {
	  this->closeFile(current, !m_mmapFiles);
	  mep_number = 0;
	  continue;
	}
	else if ( file_length - file_position < data_len - sizeof(mep_hdr) )  {
	  this->closeFile(current, !m_mmapFiles);
	  mep_number = 0;
	  continue;
	}
	if ( !mep_hdr.is_valid() )   {
	  error("Invalif MEP header found. Drop rest of file %ld -> %ld %s. ",
		file_position, file_length, current.cname());
	  this->closeFile(current, !m_mmapFiles);
	  continue;
	}
	unsigned char* data   = allocator(data_len);
	long           rd_len = data_len-sizeof(mep_hdr);
	if ( data == 0 || dsc.len == 0 )   { // MBM Error
	  // Set back the file position to the beginning of the event and continue.
	  // If there was a cancel in between, the file shall be saved.
	  current.position(file_position);
	  continue;
	}
	dsc.len = data_len;
	::memcpy(data, &mep_hdr, sizeof(mep_hdr));
	if ( rd_len != current.read(data+sizeof(mep_hdr), rd_len) )   {
	  this->closeFile(current, !m_mmapFiles);
	  continue;
	}
	//current.position(file_position + frame_len);

	pmep_hdr = (pcie40::mep_header_t*)data;
	packing  = pmep_hdr->multi_fragment(0)->header.packing;
	++mep_number;
	m_eventsIN += packing;
	// If MEP, we emulate a trigger mask with the partition ID
	dsc.type    = event_type;
	dsc.mask[0] = partid;
	dsc.mask[1] = ~0x0;
	dsc.mask[2] = ~0x0;
	dsc.mask[3] = ~0x0;
	// Check if there are consumers pending before declaring the event.
	// This should be a rare case, since there ARE (were?) consumers.
	// Though: In this case the event is really lost!
	// But what can I do...
	cons_wait = m_maxConsWait;
	if ( m_requireConsumers )  {
	  for(cons_wait = m_maxConsWait; !check_consumers(*mbmInfo,partid,dsc.type) && --cons_wait>=0; )  {
	    ::lib_rtl_sleep(1000);
	    if ( !m_receiveEvts ) cons_wait = 0; // Force writing back opened file in case if cancel etc.
	  }
	}
	if ( cons_wait <= 0 )  {
	  error("No consumers (partition:%d event type:%d) present to consume event %d",
		partid,dsc.type,mep_number);
	  error("Save rest of file and finish. Skipping rest of file: %s",current.cname());
	  // If we did not see an MDF header, the file may also be corrupted:
	  if ( dsc.type == EVENT_TYPE_MEP )  {
	    error("If Moores are alive, this file may be corrupted: %s",current.cname());
	  }
	  m_receiveEvts = false;
	  current.position(file_position);
	  if ( m_deleteFiles && m_saveRest ) current.saveRestOfFile();
	  this->closeFile(current);
	  /// Before actually declaring ERROR, we wait until no events are pending anymore.
	  waitForPendingEvents(m_maxConsWait);
	  /// Go to state PAUSED, all the work is done
	  fireIncident("DAQ_ERROR");
	  return DF_ERROR;
	}
	{
	  lock_guard<mutex> lck (m_prodLock);
	  //
	  // Declare the event to the buffer manager
	  //
	  try {
	    status = m_producer->declareEvent();
	  }
	  catch (const exception& e)        {
	    info("Exception while delareEvent: %s Event:%d File:%s.",
		 e.what(),mep_number,current.cname());
	    status = MBM_ERROR;
	  }
	  catch(...)   {
	    info("UNKNOWN Exception while delareEvent: Event:%d File:%s.",
		 mep_number,current.cname());
	    status = MBM_ERROR;
	  }
	  if (status != MBM_NORMAL)    {
	    continue;
	  }
	  //
	  // Now send space
	  //
	  try  {
	    status = m_producer->sendSpace();
	  }
	  catch (const exception& e)        {
	    info("Exception while sendSpace: %s Event:%d File:%s.",
		 e.what(),mep_number,current.cname());
	    status = MBM_ERROR;
	  }
	  catch(...)   {
	    info("UNKNOWN Exception while sendSpace: Event:%d File:%s.",
		 mep_number,current.cname());
	    status = MBM_ERROR;
	  }
	}
	if (status != MBM_NORMAL)    {
	  continue;
	}
	else  {
	  m_eventsOUT += packing;
	  m_evtCountFile++;
	  // If we have exceeded the total number of events per file, close it!
	  if ( m_max_events_per_file>0 && m_evtCountFile>m_max_events_per_file )  {
	    this->closeFile(current);
	  }
	}
      }
    }
    if ( m_deleteFiles && m_saveRest ) current.saveRestOfFile();
    this->closeFile(current);
    // Bad file: Cannot read input (m_eventsIN==0)
    updateRunNumber(0);
    m_evtCountFile = 0;
  }
  m_eventsIN = 0;
  m_eventsOUT = 0;
  /// Before actually declaring PAUSED, we wait until no events are pending anymore.
  waitForPendingEvents(m_maxPauseWait);
  return DF_SUCCESS;
}
