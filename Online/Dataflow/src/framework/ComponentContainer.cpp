//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  ComponentContainer.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include <Dataflow/ComponentContainer.h>
#include <Dataflow/ComponentHandler.h>
#include <Dataflow/DataflowManager.h>

// C/C++ include files
#include <stdexcept>

/// Initializing constructor
Online::ComponentContainer::ComponentContainer(const std::string& nam, Context& ctxt)
  : Component(nam, ctxt)
{
  declareProperty("Components",componentNames);
  declareProperty("Mode_AND",  mode_AND);
  declareProperty("Mode_OR",   mode_OR);
}

/// Default destructor
Online::ComponentContainer::~ComponentContainer()   {
}

/// Execute transition action
int Online::ComponentContainer::action(const char* transition, handler_func_t pmf)  {
  try  {
    ComponentHandler handler(context);
    for( const auto& i : components ) (handler.*pmf)(i);
    return DF_SUCCESS;
  }
  catch( const std::exception& e )  {
    error(e,"%s: ComponentContainer failed.",transition);
  }
  catch( ... )  {
    error("%s: ComponentContainer failed [UNKOWN error]",transition);
  }
  return DF_ERROR;  
}

/// Initialize the MBM server
int Online::ComponentContainer::initialize()  {
  int sc = Component::initialize();
  if( sc == DF_SUCCESS )  {
    ComponentHandler handler(context);
    DataflowManager& mgr = context.manager.manager();
    for( const auto& i : componentNames )  {
      Component* c = mgr.getManagedComponent(i,true);
      components.push_back(c);
    }
  }
  return sc;
}

/// Data processing overload: Send data record to network client
int Online::ComponentContainer::execute(const Context::EventData& event)  {
  try  {
    int ret = DF_SUCCESS;
    ComponentHandler handler(context);

    if     ( mode_AND ) processingFlag |=  DF_PASSED;
    else if( mode_OR  ) processingFlag &= ~DF_PASSED;

    for( auto i : components )  {
      i->processingFlag &= ~DF_PASSED;
    }
    for( auto i : components )  {
      int sc = handler.execute(i,event);
      if( sc != DF_SUCCESS )  {
	processingFlag &= ~DF_PASSED;
	ret = sc;
      }
      if( mode_AND && (0 == (i->processingFlag&DF_PASSED)) )  {
	processingFlag &= ~DF_PASSED;
	return DF_SUCCESS;
      }
      else if( mode_OR && (0 != (i->processingFlag&DF_PASSED)) )  {
	processingFlag |= DF_PASSED;
	return DF_SUCCESS;
      }
    }
    return ret;
  }
  catch( const std::exception& e )  {
    error(e,"Execute: Error processing event.");
  }
  catch( ... )  {
    error("Execute: UNKOWN error processing event.");
  }
  return DF_SUCCESS;
}
