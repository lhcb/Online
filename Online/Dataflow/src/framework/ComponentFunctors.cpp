//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  Delay.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include <Dataflow/ComponentFunctors.h>

/// Callback function. When component is selected, return negative value
long Online::SelectComponentByName::operator()(int typ, Component* component)  {
  if ( 0 != (typ&type) && component->name == name )  {
    selected = component;
    return -1;
  }
  return 1;
}
