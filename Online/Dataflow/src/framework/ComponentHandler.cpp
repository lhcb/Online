//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  ComponentHandler.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include <Dataflow/ComponentHandler.h>
#include <Dataflow/DataflowComponent.h>
#include <Dataflow/Plugins.h>

/// Construct the component
Online::DataflowComponent*
Online::ComponentHandler::construct(const std::string& id)  const  {
  return Component::create(id,context);
}

/// Destruct the component
int Online::ComponentHandler::destruct(Component* c) const  {
  delete c;
  return DF_SUCCESS;
}

/// Set component properties
int Online::ComponentHandler::setProperties(Component* c) const  {
  int sc = c->setProperties();
  if( sc == DF_SUCCESS )   {
    c->debug("Successfully set properties to component.");
    return sc;
  }
  return fail(c,"Failed to set the component properties. Initialization will stop.");
}

/// Throw a formatted exception in the event of a failure
int Online::ComponentHandler::fail(DataflowComponent* c, const char* msg, ...) const  {
  va_list args;
  va_start(args, msg);
  return c->throwError(msg, args);
}

/// Initialize the data flow component. 
int Online::ComponentHandler::initialize(Component* c) const  {
  int sc = c->initialize();
  if( sc == DF_SUCCESS ) return sc;
  return fail(c,"Failed to initialize. Initialization will stop.");
}

/// Start the data flow component. 
int Online::ComponentHandler::start(Component* c)  const  {
  int sc = c->start();
  if( sc == DF_SUCCESS ) return sc;
  return fail(c,"Failed to start. Processing will stop.");
}

/// Stop the data flow component. 
int Online::ComponentHandler::stop(Component* c)  const  {
  int sc = c->stop();
  if( sc == DF_SUCCESS ) return sc;
  return fail(c,"Failed to stop. Processing will stop.");
}

/// Finalize the data flow component. 
int Online::ComponentHandler::finalize(Component* c)  const  {
  int sc = c->finalize();
  if( sc != DF_SUCCESS )  {
    c->warning("Failed to finalize. (Ignored, nothing we can really do about)");
  }
  return sc;
}

/// Pause the data flow component. 
int Online::ComponentHandler::pause(Component* c)  const  {
  int sc = c->pause();
  if( sc != DF_SUCCESS )  {
    return c->error("Failed to pause.");
  }
  return sc;
}

/// Pause the data flow component. 
int Online::ComponentHandler::cancel(Component* c)  const  {
  int sc = c->cancel();
  if( sc != DF_SUCCESS )  {
    return c->error("Failed to cancel.");
  }
  return sc;
}

/// Enable the data flow component. 
int Online::ComponentHandler::enable(Component* c)  const  {
  int sc = c->enable();
  if( sc != DF_SUCCESS )  {
    return c->error("Failed to enable.");
  }
  return sc;
}

/// Continuing the data flow component. 
int Online::ComponentHandler::continuing(Component* c)  const  {
  int sc = c->continuing();
  if( sc != DF_SUCCESS )  {
    return c->error("Failed to continuing.");
  }
  return sc;
}

/// Execute the event loop
int Online::ComponentHandler::execute(Component* c, const Context::EventData& event)  const  {
  if( (c->processingFlag&DF_EXECUTED) == 0 )  {
    int sc = c->execute(event);
    c->processingFlag |= DF_EXECUTED;
    if( sc != DF_SUCCESS )  {
      c->warning("Failed execute the event.");
    }
    return sc;
  }
  return DF_SUCCESS;
}
