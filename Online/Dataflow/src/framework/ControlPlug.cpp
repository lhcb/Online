//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  ControlPlug.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include <Dataflow/DataflowTask.h>
#include <Dataflow/ControlPlug.h>
#include <CPP/IocSensor.h>
#include <RTL/rtl.h>

// C/C++ include files
#include <sstream>

using namespace Online::Control;

#define ST_NAME_OFFLINE     "OFFLINE"
#define ST_NAME_ERROR       "ERROR"
#define ST_NAME_NOT_READY   "NOT_READY"
#define ST_NAME_READY       "READY"
#define ST_NAME_ACTIVE      "ACTIVE"
//#define ST_NAME_STOPPED     "STOPPED"
#define ST_NAME_STOPPED     "READY"
#define ST_NAME_RUNNING     "RUNNING"
#define ST_NAME_PAUSED      "PAUSED"

/// Initializing constructor
Online::ControlPlug::ControlPlug(const std::string& nam, Context& ctxt)
  : DataflowComponent(nam, ctxt),
    m_stateName(ST_NAME_NOT_READY), 
    m_prevStateName(ST_NAME_NOT_READY)
{
  m_monitor.targetState = m_monitor.state = ST_NOT_READY;
  m_monitor.lastCmd     = m_monitor.doneCmd = (int)::time(0);
  m_monitor.metaState   = SUCCESS_ACTION;
  m_monitor.pid         = ::lib_rtl_pid();
  m_monitor.partitionID = -1;
  m_monitor.pad         = 0;
}

/// Default destructor
Online::ControlPlug::~ControlPlug()   {
}

/// Send IOC message to target interactor
void Online::ControlPlug::send(int cmd, void* data)   {
  if ( context.task )  {
    IOCSENSOR.send(context.task, cmd, data);
    return;
  }
  throwError("Logics problem: No command target specified!");
}

/// Invoke transtion to target state or other commands
int Online::ControlPlug::invoke(const std::string& cmd)   {
  if ( cmd == "configure"  ) {
    setTargetState(ST_READY);
    send(INITIALIZE);
  }
  else if ( cmd == "start"      ) {
    setTargetState(ST_RUNNING);
    send(START);
  }
  else if ( cmd == "stop"       ) {
    setTargetState(ST_STOPPED);
    send(STOP);
  }
  else if ( cmd == "reset"      ) {
    setTargetState(ST_NOT_READY);
    send(FINALIZE);
    send(TERMINATE);
  }
  else if ( cmd == "pause"      ) {
    setTargetState(ST_PAUSED);
    send(PAUSE);
  }
  else if ( cmd == "continue"   ) {
    setTargetState(ST_RUNNING);
    send(CONTINUE);
  }
  else if ( cmd == "unload"     ) {
    setTargetState(ST_OFFLINE);
    send(CANCEL);
    send(UNLOAD);
  }
  else if ( cmd == "recover"    ) {
    setTargetState(ST_OFFLINE);
    send(CANCEL);
    send(UNLOAD);
  }
  else if ( cmd == "activate"   ) {
    setTargetState(ST_ACTIVE);
    send(ACTIVATE);
  }
  else if ( cmd == "cancel"     ) {
    send(CANCEL);
    return DF_SUCCESS;
  }
  /// Auxiliary, non-standard commands:
  else if ( cmd == "RESET"      ) {
    setTargetState(ST_OFFLINE);
    send(CANCEL);
    send(UNLOAD);
  }
  else if ( cmd == "error"      ) {
    setTargetState(ST_ERROR);
    send(CANCEL);
    send(ERROR);
  }
  else if ( cmd == "enable"     ) {
    send(ENABLE);
    return DF_SUCCESS;
  }
  else if ( cmd == "disable"    ) {
    send(DISABLE);
    return DF_SUCCESS;
  }
  else if ( cmd == "load"       ) {  // Ignore!
    return DF_SUCCESS;
  }
  else if ( cmd == "!state" || cmd == "get_state"    ) {
    std::string old_state = stateName();
    m_stateName = "";
    _declareState(old_state);
    return DF_SUCCESS;
  }
  else  {
    return DF_ERROR;
  }
  declareSubState(EXEC_ACTION);
  return DF_SUCCESS;
}

/// Translate integer state to string name
std::string Online::ControlPlug::stateName(int state)    {
  switch(state) {
  case ST_NOT_READY:
    return ST_NAME_NOT_READY;
  case ST_READY:
    return ST_NAME_READY;
  case ST_ACTIVE:
    return ST_NAME_ACTIVE;
  case ST_RUNNING:
    return ST_NAME_RUNNING;
  case ST_STOPPED:
    return ST_NAME_STOPPED;
  case ST_PAUSED:
    return ST_NAME_PAUSED;
  case ST_ERROR:
    return ST_NAME_ERROR;
  case ST_OFFLINE:
  default:
    return ST_NAME_OFFLINE;
  }
}

/// Declare process state to DIM service
int Online::ControlPlug::_declareState(const std::string& new_state)  {
  std::string old_state = m_stateName;
  m_prevStateName       = m_stateName;
  m_stateName           = new_state;
  publishState();
  if ( new_state == ST_NAME_ERROR )
    declareSubState(FAILED_ACTION);
  else if ( old_state == new_state )
    declareSubState(FAILED_ACTION);
  else
    declareSubState(SUCCESS_ACTION);
  return DF_SUCCESS;
}

/// Declare process state to DIM service
int Online::ControlPlug::declareState(State new_state)  {
  m_monitor.state = char(new_state);
  switch(new_state)   {
  case TR_ERROR:
  case ST_ERROR:
    return _declareState(ST_NAME_ERROR);
  case ST_NOT_READY:
    return _declareState(ST_NAME_NOT_READY);
  case ST_READY:
    return _declareState(ST_NAME_READY);
  case ST_ACTIVE:
    return _declareState(ST_NAME_ACTIVE);
  case ST_STOPPED:
    return _declareState(ST_NAME_STOPPED);
  case ST_RUNNING:
    return _declareState(ST_NAME_RUNNING);
  case ST_PAUSED:
    return _declareState(ST_NAME_PAUSED);
  case ST_OFFLINE:
  default:
    m_monitor.state = ST_OFFLINE;
    return _declareState(ST_NAME_OFFLINE);
  }
}

/// Declare FSM sub-state
int Online::ControlPlug::declareSubState(SubState new_state)  {
  m_monitor.metaState = char(new_state);
  switch(new_state)   {
  case SUCCESS_ACTION:
    m_monitor.doneCmd = (int)::time(0);
    break;
  case EXEC_ACTION:
    m_monitor.lastCmd = (int)::time(0);
    break;
  case FAILED_ACTION:
    m_monitor.doneCmd = (int)::time(0);
    break;
  case UNKNOWN_ACTION:
  default:
    m_monitor.doneCmd = (int)::time(0);
    m_monitor.metaState = ST_OFFLINE;
    break;
  }
  publishMonitor();
  return DF_SUCCESS;
}

/// Declare the target state of the transition
int Online::ControlPlug::declareTargetState()   {
  return declareState(Control::State(m_monitor.targetState));
}

/// Set transition target state
int Online::ControlPlug::setTargetState(Control::State target)  {
  m_monitor.targetState = target;
  return publishMonitor();
}

/// Access the current object state
Online::Control::State Online::ControlPlug::currentState() const   {
  return (Control::State)m_monitor.state;
}

/// Access the object's target state
Online::Control::State Online::ControlPlug::targetState() const   {
  return (Control::State)m_monitor.targetState;
}
 
