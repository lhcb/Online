//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  DataCompression.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include <Dataflow/DataCompression.h>
// ROOT data compression header
#include <RZip.h>

// C/C++ include files
#include <iostream>
#include <cstring>
#include <memory>

///  Online namespace declaration
namespace Online  {

  ///  DataCompression namespace declaration
  namespace DataCompression  {

    /// Data compression name
    std::string compressionType(compression_alg_t algtype)   {
      if ( algtype == ZLIB )
        return "ZLIB";
      else if ( algtype == ZSTD )
        return "ZSTD";
      else if ( algtype == LZMA )
        return "LZMA";
      else if ( algtype == LZ4 )
        return "LZ4";
      else
        return "UNKNOWN";
    }
    
    DataflowStatus compress( compression_alg_t algtype, int alglevel,
			     uint8_t* tar,       std::size_t tar_len,
			     const uint8_t* src, std::size_t src_len,
			     std::size_t& new_len )
    {
      ROOT::RCompressionSetting::EAlgorithm::EValues alg
	= ROOT::RCompressionSetting::EAlgorithm::kUseGlobal;
      int in_len, out_len, res_len = 0;
      switch ( alglevel ) {
      case 0:
	if ( tar == src ) {
	  new_len = src_len;
	  return DF_SUCCESS;
	} else if ( tar != src && tar_len >= src_len ) {
	  ::memcpy( tar, src, src_len );
	  new_len = src_len;
	  return DF_SUCCESS;
	}
	new_len = 0;
	return DF_ERROR;
      case 1:
      case 2:
      case 3:
      case 4:
      case 5:
      case 6:
      case 7:
      case 8:
      case 9:
	in_len  = src_len;
	out_len = tar_len;
	if ( algtype == ZLIB )
	  alg = ROOT::RCompressionSetting::EAlgorithm::kZLIB;
	else if ( algtype == ZSTD )
	  alg = ROOT::RCompressionSetting::EAlgorithm::kZSTD;
	else if ( algtype == LZMA )
	  alg = ROOT::RCompressionSetting::EAlgorithm::kLZMA;
	else if ( algtype == LZ4 )
	  alg = ROOT::RCompressionSetting::EAlgorithm::kLZ4;
	else
	  return DF_ERROR;

	::R__zipMultipleAlgorithm( alglevel, &in_len, (char*)src, &out_len, (char*)tar, &res_len, alg );
	if ( res_len == 0 || std::size_t( res_len ) >= src_len ) {
	  // this happens when the buffer cannot be compressed
	  res_len = 0;
	  return DF_ERROR;
	}
	new_len = res_len;
	return DF_SUCCESS;
      default:
	break;
      }
      return DF_ERROR;
    }

    /// Decompress opaque data buffer
    DataflowStatus decompress( int level,
			       uint8_t* tar,       std::size_t tar_len,
			       const uint8_t* src, std::size_t src_len,
			       std::size_t& new_len )
    {
      int in_len, out_len, res_len = 0;
      switch ( level )  {
      case 0:
	if ( tar != src && tar_len >= src_len ) {
	  new_len = src_len;
	  ::memcpy( tar, src, src_len );
	  return DF_SUCCESS;
	}
	break;
      case 1:
      case 2:
      case 3:
      case 4:
      case 5:
      case 6:
      case 7:
      case 8:
      case 9:
	in_len  = src_len;
	out_len = tar_len;
	::R__unzip( &in_len, (uint8_t*)src, &out_len, tar, &res_len );
	if ( res_len > 0 ) {
	  new_len = res_len;
	  return DF_SUCCESS;
	}
	break;
      default:
	break;
      }
      return DF_ERROR;
    }

  }    // end namespace DataCompression
}      // end namespace Online
