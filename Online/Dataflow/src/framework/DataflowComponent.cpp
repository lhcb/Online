//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  DataflowComponent.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include <Dataflow/DataflowComponent.h>
#include <Dataflow/PropertyPrintout.h>
#include <Dataflow/Incidents.h>
#include <Dataflow/Printout.h>
#include <Dataflow/Plugins.h>

#include <TH1.h>
#include <TH2.h>
#include <TH3.h>
#include <TProfile.h>
#include <TProfile2D.h>
#include <TProfile3D.h>

/// C/C++ include files
#include <atomic>
#include <cstdint>

using namespace Online;

namespace {

  /// Helper function to steer printing
  bool may_print(const DataflowComponent* c, int level)  {
    int lvl = c->context.logger->level;
    return level >= c->outputLevel || level >= lvl;
  }
}

/// Initializing constructor
DataflowComponent::DataflowComponent(const std::string& nam, Context& ctxt)
  : name(nam), context(ctxt)
{
  declareProperty("OutputLevel", outputLevel=INFO);
  if ( context.logger ) outputLevel = context.logger->level;
}

/// Default destructor
DataflowComponent::~DataflowComponent()   {
}

/// Create (and register) a new component
DataflowComponent* 
DataflowComponent::create(const std::string& name_type_id, Context& ctxt)   {
  return Plugins::createComponent(name_type_id, ctxt);
}

/// Initialize the data flow component. Default implementation is empty.
int DataflowComponent::initialize()  {
  outputLevel = context.logger->level;
  return setProperties();
}

/// Start the data flow component. Default implementation is empty.
int DataflowComponent::start()  {
  return DF_SUCCESS;
}

/// Stop the data flow component. Default implementation is empty.
int DataflowComponent::stop()  {
  return DF_SUCCESS;
}

/// Finalize the data flow component. Default implementation is empty.
int DataflowComponent::finalize()  {
  return DF_SUCCESS;
}

/// Pause the data flow component. Default implementation is empty.
int DataflowComponent::pause()  {
  return DF_SUCCESS;
}

/// Cancel the data flow component. Default implementation is empty.
int DataflowComponent::cancel()  {
  return DF_SUCCESS;
}

/// Enable the data flow component. Default implementation is empty.
int DataflowComponent::enable()  {
  return DF_SUCCESS;
}

/// Continuing the data flow component. Default implementation is empty.
int DataflowComponent::continuing()  {
  return DF_SUCCESS;
}

/// Execute runable. Default implementation throws an exception.
int DataflowComponent::run()   {
  return throwError("The default run-method is not supposed to be executed, "
		    "but overloaded by runable components.");
}

/// Execute event. Default implementation is empty.
int DataflowComponent::execute(const Context::EventData& /* event */)   {
  return DF_SUCCESS;
}

#define IMPLEMENT_SINGLE_MONITOR(x)					\
  namespace Online  {                                                   \
    template <> const DataflowComponent&                                \
    DataflowComponent::declareMonitor<x>(const std::string& n,          \
                                         const x&           v,          \
                                         const std::string& d)  const {	\
      context.monitor->declare(this->name,n,&v,typeid(v),d);            \
      return *this;                                                     \
    }                                                                   \
    template <> const DataflowComponent&                                \
    DataflowComponent::declareMonitor<x>(const std::string& o,          \
                                         const std::string& n,          \
                                         const x&           v,          \
                                         const std::string& d)  const {	\
      context.monitor->declare(o,n,&v,typeid(v),d);                     \
      return *this;                                                     \
    }}

#define IMPLEMENT_PAIR_MONITOR(x)					\
  namespace Online  {                                                   \
    template <> const DataflowComponent&                                \
    DataflowComponent::declareMonitor<x>(const std::string& n,          \
                                         const x& v1,                   \
                                         const x& v2,                   \
                                         const std::string& d)  const { \
      context.monitor->declare(this->name,n,&v1,&v2,typeid(v1),d);      \
      return *this;                                                     \
    }                                                                   \
    template <> const DataflowComponent&                                \
    DataflowComponent::declareMonitor<x>(const std::string& o,          \
                                         const std::string& n,          \
                                         const x& v1,                   \
                                         const x& v2,                   \
                                         const std::string& d)  const { \
      context.monitor->declare(o,n,&v1,&v2,typeid(v1),d);               \
      return *this;                                                     \
    }}
#define IMPLEMENT_POINTER_MONITOR(x)                                    \
  namespace Online  {                                                   \
    template <> const DataflowComponent&                                \
    DataflowComponent::declareMonitor<x>(const std::string& n,          \
                                         const x*           v,          \
                                         size_t             s,          \
                                         const std::string& d) const {  \
      context.monitor->declare(this->name,n,(const void*)v,		\
			       s,typeid(x*),d);				\
      return *this;                                                     \
    }                                                                   \
    template <> const DataflowComponent&                                \
    DataflowComponent::declareMonitor<x>(const std::string& o,          \
                                         const std::string& n,          \
                                         const x*           v,          \
                                         size_t             s,          \
                                         const std::string& d) const {  \
      context.monitor->declare(o,n,(const void*)v,s,typeid(x*),d);      \
      return *this;                                                     \
    }                                                                   \
    template <> const DataflowComponent&                                \
    DataflowComponent::declareMonitor<x>(const std::string& n,          \
                                         const x* v,                    \
                                         size_t s,                      \
                                         const std::string& f,          \
                                         const std::string& d) const {  \
      context.monitor->declare(this->name,n,(const void*)v,s,		\
			       typeid(x*),f,d);				\
      return *this;                                                     \
    }                                                                   \
    template <> const DataflowComponent&                                \
    DataflowComponent::declareMonitor<x>(const std::string& o,          \
                                         const std::string& n,          \
                                         const x* v,                    \
                                         size_t s,                      \
                                         const std::string& f,          \
                                         const std::string& d) const {  \
      context.monitor->declare(o,n,(void*)v,s,typeid(x*),f,d);          \
      return *this;                                                     \
    }                                                                   \
  }

#define IMPLEMENT_MONITOR(x)                    \
  IMPLEMENT_SINGLE_MONITOR(x)                   \
  IMPLEMENT_SINGLE_MONITOR(std::vector<x>)

IMPLEMENT_MONITOR(int8_t)
IMPLEMENT_MONITOR(uint8_t)
IMPLEMENT_MONITOR(int16_t)
IMPLEMENT_MONITOR(uint16_t)
IMPLEMENT_MONITOR(int32_t)
IMPLEMENT_MONITOR(uint32_t)
IMPLEMENT_MONITOR(int64_t)
IMPLEMENT_MONITOR(uint64_t)
IMPLEMENT_MONITOR(long long)
IMPLEMENT_MONITOR(float)
IMPLEMENT_MONITOR(double)
IMPLEMENT_PAIR_MONITOR(int8_t)
IMPLEMENT_PAIR_MONITOR(uint8_t)
IMPLEMENT_PAIR_MONITOR(int16_t)
IMPLEMENT_PAIR_MONITOR(uint16_t)
IMPLEMENT_PAIR_MONITOR(int32_t)
IMPLEMENT_PAIR_MONITOR(uint32_t)
IMPLEMENT_PAIR_MONITOR(int64_t)
IMPLEMENT_PAIR_MONITOR(uint64_t)

//IMPLEMENT_SINGLE_MONITOR(string)
IMPLEMENT_SINGLE_MONITOR(TH1D)
IMPLEMENT_SINGLE_MONITOR(TH2D)
IMPLEMENT_SINGLE_MONITOR(TH3D)
IMPLEMENT_SINGLE_MONITOR(TProfile)
IMPLEMENT_SINGLE_MONITOR(TProfile2D)
IMPLEMENT_SINGLE_MONITOR(TProfile3D)

IMPLEMENT_MONITOR(std::atomic<int8_t>)
IMPLEMENT_MONITOR(std::atomic<uint8_t>)
IMPLEMENT_MONITOR(std::atomic<int16_t>)
IMPLEMENT_MONITOR(std::atomic<uint16_t>)
IMPLEMENT_MONITOR(std::atomic<int32_t>)
IMPLEMENT_MONITOR(std::atomic<uint32_t>)
IMPLEMENT_MONITOR(std::atomic<int64_t>)
IMPLEMENT_MONITOR(std::atomic<uint64_t>)
IMPLEMENT_MONITOR(std::atomic<float>)
IMPLEMENT_MONITOR(std::atomic<double>)

//IMPLEMENT_POINTER_MONITOR(int8_t)
//IMPLEMENT_POINTER_MONITOR(uint8_t)
//IMPLEMENT_POINTER_MONITOR(int16_t)
//IMPLEMENT_POINTER_MONITOR(uint16_t)
IMPLEMENT_POINTER_MONITOR(int8_t)
IMPLEMENT_POINTER_MONITOR(uint8_t)
IMPLEMENT_POINTER_MONITOR(int16_t)
IMPLEMENT_POINTER_MONITOR(uint16_t)
IMPLEMENT_POINTER_MONITOR(int32_t)
IMPLEMENT_POINTER_MONITOR(uint32_t)
IMPLEMENT_POINTER_MONITOR(int64_t)
IMPLEMENT_POINTER_MONITOR(uint64_t)
IMPLEMENT_POINTER_MONITOR(long long)
IMPLEMENT_POINTER_MONITOR(float)
IMPLEMENT_POINTER_MONITOR(double)

/// Declare monitoring structure with a given format to the monitoring component
#if 0
const DataflowComponent&
DataflowComponent::declareMonitor(const std::string& nam, 
				  const std::string& fmt, 
				  const void*        val, 
				  size_t             bytes,
				  const std::string& desc) const
{
  context.monitor->declare(this->name, nam, fmt, val, bytes, desc);
  return *this;
}
#endif
/// Undeclare monitoring information item
void DataflowComponent::undeclareMonitor(const std::string& owner, const std::string& nam) const   {
  context.monitor->undeclare(owner,nam);
}

/// Undeclare monitoring information item
void DataflowComponent::undeclareMonitor(const std::string& nam) const   {
  context.monitor->undeclare(this->name,nam);
}

/// Undeclare monitoring information
void DataflowComponent::undeclareMonitors(const std::string& owner)  const  {
  context.monitor->undeclare(owner);
}

/// Undeclare monitoring information
void DataflowComponent::undeclareMonitors()  const  {
  context.monitor->undeclare(this->name);
}

/// Set my own properties from the context
int DataflowComponent::setProperties()  {
  return context.options->setProperties(name, properties);
}

/// Check property for existence
bool DataflowComponent::hasProperty(const std::string& nam) const   {
  return properties.exists(nam);
}

/// Access single property
Property& DataflowComponent::property(const std::string& nam)   {
  return properties[nam];
}

/// Dump the properties of the component
void DataflowComponent::printProperties()  {
  PropertyPrintout printer(name);
  properties.for_each(printer);
}

/// Incident handler interface: Incident-subscription
int DataflowComponent::subscribeIncident(const std::string& incident_name)    {
  if ( !incident_name.empty() ) 
    return context.incidents->subscribe(this,incident_name);
  return error("subscribeIncident> Invalid incident name: '%s'.",incident_name.c_str());
}

/// Incident handler interface: un-subscribe from incident
int DataflowComponent::unsubscribeIncident(const std::string& incident_name)   {
  if ( !incident_name.empty() )
    return context.incidents->unsubscribe(this,incident_name);
  return error("unsubscribeIncident> Invalid incident name: '%s'.",incident_name.c_str());
}

/// Incident handler interface: un-subscribe this component from all incident
int DataflowComponent::unsubscribeIncidents()    {
  return context.incidents->unsubscribe(this);
}

/// Invoke incident
int DataflowComponent::fireIncident(const DataflowIncident& incident)   {
  return context.incidents->fire(this, incident);
}

/// Invoke incident
int DataflowComponent::fireIncident(const std::string& type)  {
  return fireIncident(DataflowIncident(name,type));
}

/// Incident handler callback: Inform that a new incident has occured
void DataflowComponent::handle(const DataflowIncident& /* incident */)   {
}

/// Printout forward to message handler component
int DataflowComponent::_print(int severity, const std::string& source, const char* fmt, va_list& args)  const  {
  return context.logger->print(severity|MSG_PASS, source.c_str(), fmt, args);
}

/// Always printout handling
void DataflowComponent::always(const char* fmt,...) const   {
  va_list args;
  va_start(args, fmt);
  this->_print(ALWAYS, name, fmt, args);
}

/// Always printout handling
void DataflowComponent::always(const char* fmt, va_list& args) const   {
  this->_print(ALWAYS, name, fmt, args);
}

/// Debug printout handling
void DataflowComponent::debug(const char* fmt,...) const   {
  if ( may_print(this,DEBUG) )  {
    va_list args;
    va_start(args, fmt);
    this->_print(DEBUG, name, fmt, args);
  }
}

/// Debug printout handling
void DataflowComponent::debug(const char* fmt, va_list& args) const   {
  if ( may_print(this,DEBUG) )  {
    this->_print(DEBUG, name, fmt, args);
  }
}

/// Info printout handling
void DataflowComponent::info(const char* fmt, ...) const   {
  if ( may_print(this,INFO) )  {
    va_list args;
    va_start(args, fmt);
    this->_print(INFO, name, fmt, args);
    va_end(args);
  }
}

#if 0
/// Info printout handling
void DataflowComponent::info(const std::string& fmt, ...) const   {
  if ( may_print(this,INFO) )  {
    va_list args;
    va_start(args, &fmt);
    this->_print(INFO, name, fmt, args);
    va_end(args);
  }
}
#endif
/// Info printout handling
void DataflowComponent::info(const char* fmt, va_list& args) const  {
  if ( may_print(this,INFO) )  {
    this->_print(INFO, name, fmt, args);
  }
}

/// Warning printout handling
void DataflowComponent::warning(const char* fmt, ...) const   {
  if ( may_print(this,WARNING) )  {
    va_list args;
    va_start(args, fmt);
    this->_print(WARNING, name, fmt, args);
    va_end(args);
  }
}

/// Warning printout handling
void DataflowComponent::warning(const char* fmt, va_list& args) const   {
  if ( may_print(this,WARNING) )  {
    this->_print(WARNING, name, fmt, args);
  }
}
#if 0
/// Warning printout handling
void DataflowComponent::warning(const std::string& fmt, ...) const   {
  if ( may_print(this,WARNING) )  {
    va_list args;
    va_start(args, &fmt);
    this->_print(WARNING, name, fmt, args);
    va_end(args);
  }
}

/// Error handling. Returns error code
int DataflowComponent::error(const std::string& fmt, ...) const   {
  if ( may_print(this,ERROR) )  {
    va_list args;
    va_start(args, &fmt);
    this->_print(ERROR, name, fmt, args);
    va_end(args);
  }
  return DF_ERROR;
}
#endif
/// Error handling. Returns error code
int DataflowComponent::error(const char* fmt, ...) const   {
  if ( may_print(this,ERROR) )  {
    va_list args;
    va_start(args, fmt);
    this->_print(ERROR, name, fmt, args);
    va_end(args);
  }
  return DF_ERROR;
}

/// Error handling. Technically returns error code. Throws internally exception
int DataflowComponent::error(const char* fmt, va_list& args) const   {
  if ( may_print(this,ERROR) )  {
    this->_print(ERROR, name, fmt, args);
  }
  return DF_ERROR;
}

/// Error handling: print exception. Technically returns error code.
int DataflowComponent::error(const std::exception& e, const char* fmt, ...) const   {
  if ( may_print(this,ERROR) )  {
    std::string format = "Exception: ";
    format += e.what();
    format += " ";
    format += fmt;
    va_list args;
    va_start(args, fmt);
    this->_print(ERROR, name, format.c_str(), args);
    va_end(args);
  }
  return DF_ERROR;
}
/// Error handling: print exception. Technically returns error code.
int DataflowComponent::error(const std::exception& e, const std::string& msg) const   {
  if ( may_print(this,ERROR) )  {
    return this->error(e, "Exception: %s %s", e.what(), msg.c_str());
  }
  return DF_ERROR;
}
#if 0
/// Error handling: print exception. Technically returns error code.
int DataflowComponent::error(const std::exception& e, const std::string& fmt, ...) const   {
  if ( may_print(this,ERROR) )  {
    std::string fmt = "Exception: ";
    fmt += e.what();
    fmt += " ";
    fmt += fmt;
    va_list args;
    va_start(args, &fmt);
    this->_print(ERROR, name, fmt.c_str(), args);
    va_end(args);
  }
  return DF_ERROR;
}
#endif

/// Fatal handling. Technically returns error code. Throws internally exception
int DataflowComponent::fatal(const char* fmt, va_list& args) const   {
  if ( may_print(this,FATAL) )  {
    this->_print(FATAL, name, fmt, args);
  }
  return DF_ERROR;
}
#if 0
/// Fatal handling. Returns error code
int DataflowComponent::fatal(const std::string& fmt, ...) const   {
  if ( may_print(this,FATAL) )  {
    va_list args;
    va_start(args, &fmt);
    this->_print(FATAL, name, fmt, args);
    va_end(args);
  }
  return DF_ERROR;
}
#endif
/// Fatal handling. Returns error code
int DataflowComponent::fatal(const char* fmt, ...) const   {
  if ( may_print(this,FATAL) )  {
    va_list args;
    va_start(args, fmt);
    this->_print(FATAL, name, fmt, args);
    va_end(args);
  }
  return DF_ERROR;
}

/// Error handling. Technically returns error code. Throws internally exception
int DataflowComponent::throwError(const std::string& fmt) const   {
  error(fmt.c_str());
  throw std::runtime_error(fmt);
  return DF_ERROR;
}

/// Error handling. Technically returns error code. Throws internally exception
int DataflowComponent::throwError(const char* fmt,...) const   {
  va_list args;
  va_start(args, fmt);
  except(name.c_str(), fmt, args);
  va_end(args);
  return DF_ERROR;
}

/// Error handling. Technically returns error code. Throws internally exception
int DataflowComponent::throwError(const char* fmt, va_list& args) const   {
  except(name.c_str(), fmt, args);
  return DF_ERROR;
}

/// Generic output logging
void DataflowComponent::output(int level, const char* fmt,...)   const   {
  if ( may_print(this,level) )  {
    va_list args;
    va_start(args, fmt);
    this->_print(PrintLevel(level), name, fmt, args);
    va_end(args);
  }
}
