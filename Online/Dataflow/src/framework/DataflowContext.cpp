//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  DataflowContext.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include <Dataflow/DataflowContext.h>
#include <RTL/rtl.h>

using namespace std;
using namespace Online;

/// Initializing constructor
DataflowContext::DataflowContext(Manager& mgr) 
  : manager(mgr), logger(0), options(0), incidents(0), monitor(0), mbm(0)
{
  utgid = RTL::processName();
}

/// Default destructor
DataflowContext::~DataflowContext()  {
}

/// Default destructor
DataflowContext::Logger::~Logger()  {
}

/// Default destructor
DataflowContext::Manager::~Manager()  {
}

/// Default destructor
DataflowContext::Incident::~Incident()  {
}

/// Default destructor
DataflowContext::Options::~Options()  {
}

/// Default destructor
DataflowContext::Monitor::~Monitor()  {
}
