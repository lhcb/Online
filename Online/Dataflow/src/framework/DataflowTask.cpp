//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  Receiver.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include <Dataflow/DataflowManager.h>
#include <Dataflow/DataflowTask.h>
#include <Dataflow/ControlPlug.h>
#include <Dataflow/Incidents.h>
#include <CPP/IocSensor.h>
#include <CPP/Event.h>
#include <RTL/rtl.h>

using namespace std;
namespace Online {
  int dataflow_task_trylock()  {
    return DF_SUCCESS;
  }
  int dataflow_task_lock()  {
    return DF_SUCCESS;
  }
  int dataflow_task_unlock()  {
    return DF_SUCCESS;
  }
}

using namespace Online;
using namespace Online::Control;

/// Initializing constructor
DataflowTask::DataflowTask(const std::string& nam, Context& ctxt)
  : DataflowComponent(nam, ctxt)
{
  context.task = this;
  declareProperty("AutoShutdown",    auto_shutdown);
  declareProperty("AutoStart",       auto_startup);
  declareProperty("PlugType",        plugType);
  declareProperty("Options",         options);
  declareProperty("HavePause",       have_pause);
  declareProperty("CancelDelay",     delay_cancel);
  declareProperty("PauseDelay",      delay_pause);
  declareProperty("InitializeDelay", delay_initialize);
  declareProperty("FinalizeDelay",   delay_finalize);
  declareProperty("StartDelay",      delay_start);
  declareProperty("StopDelay",       delay_stop);
  declareProperty("EnableDelay",     delay_enable);
  declareProperty("ContinueDelay",   delay_continue);
}

/// Default destructor
DataflowTask::~DataflowTask()  {
}

/// Access the raw manager interface
DataflowManager& DataflowTask::manager() const  {
  DataflowManager& m = context.manager.manager();
  return m;
}

/// Delay execution with sleep
int DataflowTask::delay(double seconds)   {
  if ( seconds > 1e-6 )   {
    int32_t usecs = int(seconds) * 1e6;
    ::lib_rtl_usleep(usecs);
  }
  return DF_SUCCESS;
}

/// Run the task
int DataflowTask::run()   {
  setProperties();
  if ( !plugType.empty() )  {
    Component* c = Component::create(plugType,context);
    plug = c ? c->query<ControlPlug>() : 0;
    if ( !plug )  {
      throwError("Failed to create communication plug!");
    }
    c->setProperties();
    plug->connect();
  }
  context.task->configure();
  if ( auto_startup )  {
    info("Initiate auto-startup sequence....");
    IOCSENSOR.send(this, INITIALIZE, nullptr);
    IOCSENSOR.send(this, START, nullptr);
  }
  IOCSENSOR.run();
  return DF_SUCCESS;
}

int DataflowTask::connect(ControlPlug* cmd) {
  if ( plug )  {
    plug->disconnect();
  }
  plug = cmd;
  if ( plug )  {
    return plug->connect();
  }
  return DF_SUCCESS;
}

int DataflowTask::disconnect() {
  return (plug) ? plug->disconnect() : DF_SUCCESS;
}

/// Declare FSM state
int DataflowTask::declareState(Control::State state)  const   {
  return (plug) ? plug->declareState(state) : DF_SUCCESS;
}

/// Declare current FSM state
int DataflowTask::declareCurrentState()  const   {
  return (plug) ? declareState(plug->currentState()) : DF_SUCCESS;
}

/// Declare FSM sub-state
int DataflowTask::declareSubState(Control::SubState state)  const   {
  return (plug) ? plug->declareSubState(state) : DF_SUCCESS;
}

/// Declare the target state of the transition
int DataflowTask::declareTargetState()  const   {
  return (plug) ? plug->declareTargetState() : DF_SUCCESS;
}

/// Set transition target state
int DataflowTask::setTargetState(Control::State target)  const   {
  return (plug) ? plug->setTargetState(target) : DF_SUCCESS;
}

/// Interactor overload: Command handler
void DataflowTask::handle(const Event& ev)   {
  if(ev.eventtype == IocEvent)  {
#define _CASE(x)  case x: action = #x;
    const char* action = "UNKNOWN";
    int sc = DF_ERROR;
    try  {
      switch(ev.type) {
        _CASE(UNLOAD)       sc=unload();                              break;
        _CASE(INITIALIZE)   sc=initialize();                          break;
        _CASE(ACTIVATE)     sc=activate();                            break;
        _CASE(START)        sc=start();                               break;
        _CASE(ENABLE)       sc=enable();                              break;
        _CASE(DISABLE)      sc=disable();                             break;
        _CASE(CANCEL)       sc=cancel();                              break;
        _CASE(STOP)         sc=stop();                                break;
        _CASE(FINALIZE)     sc=finalize();                            break;
        _CASE(TERMINATE)    sc=terminate();                           break;
        _CASE(PAUSE)        sc=pause();                               break;
        _CASE(CONTINUE)     sc=continuing();                          break;
        _CASE(ERROR)        sc=declareState(ST_ERROR);                break;
        _CASE(FIRE_INCIDENT)sc = fireIncident(*(std::string*)ev.data);
        delete (std::string*)ev.data;                                 break;
      default:      sc=error("Got Unkown action request:%d",ev.type); break;
      }
      declareSubState((sc==DF_SUCCESS) ? SUCCESS_ACTION : FAILED_ACTION);
      return;
    }
    catch(const std::exception& e)  {
      error("Exception executing action: %s [%s]", action, e.what());
      declareSubState(FAILED_ACTION);
    }
    catch(...)  {
      error("Unknown exception executing action: %s", action);
      declareSubState(FAILED_ACTION);
    }
  }
  error("Got Unkown event request: %d",ev.eventtype);
}

/// Configure the instance (create components)
int DataflowTask::app_configure()  {
  return DF_SUCCESS;
}

/// Initialize the dataflow task instance.
int DataflowTask::app_initialize()  {
  delay(delay_initialize);
  return manager().initialize();
}

/// Start the dataflow task instance.
int DataflowTask::app_start()  {
  delay(delay_start);
  return manager().start();
}

/// Stop the dataflow task instance.
int DataflowTask::app_stop()  {
  cancel();
  delay(delay_stop);
  return manager().stop();
}

/// Finalize the dataflow task instance.
int DataflowTask::app_finalize()  {
  delay(delay_finalize);
  fireIncident("DAQ_FINALIZE");
  return manager().finalize();
}

/// Terminate the dataflow task instance.
int DataflowTask::app_terminate()  {
  return manager().terminate();
}

/// Pause the dataflow task instance.
int DataflowTask::app_pause()  {
  delay(delay_pause);
  //cancel();
  return manager().pause();
}

/// Continue the dataflow task instance.
int DataflowTask::app_continue()  {
  fireIncident("DAQ_CONTINUE");
  return manager().continuing();
}

/// Cancel the dataflow task instance.
int DataflowTask::app_cancel()  {
  delay(delay_cancel);
  fireIncident("DAQ_CANCEL");
  return manager().cancel();
}

/// ControlPlug invocation:  Enable trigger
int DataflowTask::app_enable()   {
  fireIncident("DAQ_ENABLED");
  return manager().enable();
}

/// ControlPlug invocation:  Initialize the dataflow task instance.
int DataflowTask::configure()  {
  subscribeIncident("DAQ_ERROR");
  subscribeIncident("DAQ_PAUSE");
  subscribeIncident("DAQ_ENABLE");
  subscribeIncident("DAQ_SHUTDOWN");
  subscribeIncident("DAQ_STOP_TRIGGER");
  subscribeIncident("DAQ_START_TRIGGER");

  fireIncident("DAQ_CONFIGURED");
  if ( auto_startup ) setTargetState(Control::ST_READY);
  return declareTargetState();
}

/// ControlPlug invocation:  Initialize the dataflow task instance.
int DataflowTask::initialize()  {
  fireIncident("DAQ_INITIALIZED");
  if ( auto_startup ) setTargetState(Control::ST_RUNNING);
  return declareTargetState();
}

/// ControlPlug invocation:  Start the dataflow task instance.
int DataflowTask::start()  {
  enable();
  fireIncident("DAQ_RUNNING");
  declareTargetState();
  if ( auto_shutdown )  {
    fireIncident("DAQ_SHUTDOWN");
  }
  DataflowManager& mgr = context.manager.manager();
  return mgr.run();
}

/// ControlPlug invocation:  Stop the dataflow task instance.
int DataflowTask::stop()  {
  fireIncident("DAQ_STOPPED");
  if ( auto_shutdown ) setTargetState(Control::ST_READY);
  return declareTargetState();
}

/// ControlPlug invocation:  Finalize the dataflow task instance.
int DataflowTask::finalize()  {
  fireIncident("DAQ_FINALIZED");
  return declareTargetState();
}

/// ControlPlug invocation:  Finalize the dataflow task instance.
int DataflowTask::terminate()  {
  fireIncident("DAQ_TERMINATED");
  if ( auto_shutdown ) setTargetState(Control::ST_NOT_READY);
  return declareTargetState();
}

/// ControlPlug invocation:  Transition to pause  ( RUNNING -> PAUSED )
int DataflowTask::pause()   {
  debug("Executing pause. Application callback enabled: %s",yes_no(have_pause));
  if ( have_pause )   {
    int sc = app_pause();
    if ( sc != DF_SUCCESS )  {
      return declareCurrentState();
    }
  }
  return declareTargetState();
}

/// ControlPlug invocation:  Continue after pause
int DataflowTask::continuing()   {
  if ( have_pause )   {
    int sc = app_continue();
    if ( sc != DF_SUCCESS )  {
      return declareCurrentState();
    }
  }
  return declareTargetState();
}

/// ControlPlug invocation:  Continue after pause
int DataflowTask::activate()   {
  fireIncident("DAQ_ACTIVATED");
  return declareTargetState();
}

/// ControlPlug invocation:  Enable trigger
int DataflowTask::enable()   {
  return app_enable();
}

/// ControlPlug invocation:  Disable trigger
int DataflowTask::disable()   {
  fireIncident("DAQ_DISABLED");
  return declareTargetState();
}

/// Final act of the process: unload image
int DataflowTask::unload()  {
  app_terminate();
  declareState(ST_OFFLINE);
  debug("+++ Dataflow task is now unloading");
  ::lib_rtl_sleep(100);
  ::_exit(0);
  return DF_SUCCESS;
}

/// ControlPlug invocation:  Invoke transition to error state                ( ****      -> Error   )
int DataflowTask::gotoerror()  {
  cancel();
  IOCSENSOR.send(this, Control::ERROR, (void*)0);
  return DF_SUCCESS;
}

/// Indicate failure and take appropriate actions
int DataflowTask::failed()  {
  declareState(ST_ERROR);
  return DF_ERROR;
}

/// Cancel pending I/O request etc. Stop processing
int DataflowTask::cancel()  {
  return app_cancel();
}

/// Incident handler callback: Inform that a new incident has occured
void DataflowTask::handle(const DataflowIncident& incident)   {
  if ( incident.type == "DAQ_ERROR" )  {
    declareState(ST_ERROR);
  }
  else if ( incident.type == "DAQ_ENABLE" )  {
  }
  else if ( incident.type == "DAQ_STOP_TRIGGER" )  {
  }
  else if ( incident.type == "DAQ_START_TRIGGER" )  {
  }
  else if ( incident.type == "DAQ_CANCEL" )  {
  }
  else if ( incident.type == "DAQ_PAUSE" )  {
    declareState(ST_PAUSED);
  }
  else if ( incident.type == "DAQ_SHUTDOWN" )  {
    info("Initiate auto-shutdown sequence....");
    IOCSENSOR.send(this,CANCEL,(void*)0);
    IOCSENSOR.send(this,STOP,(void*)0);
    IOCSENSOR.send(this,FINALIZE,(void*)0);
    IOCSENSOR.send(this,TERMINATE,(void*)0);
    IOCSENSOR.send(this,UNLOAD,(void*)0);
  }
}
