//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  Dataflow_run_manager.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include <Dataflow/DataflowManager.h>
#include <Dataflow/DataflowTask.h>
#include <Dataflow/Printout.h>
#include <RTL/Logger.h>
#include <RTL/strdef.h>
#include <RTL/rtl.h>
#include <dim/dis.h>
#include <dim/dic.h>

/// C/C++ include files
#include <stdexcept>
#include <thread>
#include <memory>

namespace {

  using Opts = std::vector<std::pair<std::string,std::string> >;

  void help()  {
    ::printf("dataflow_run_task -opt=<value> [-opt]                        \n"
	     "   -options=<file-name>    Main processing options.          \n"
	     "   -class=<task-class>     Task class (Class0,Class1,Class2) \n"
	     "   -automatic              Auto-startup mode (self config)   \n"
	     "\n");
    exit(0);
  }

  struct Dataflow_setup  {
    std::string class_type    {};
    std::string option_file   {};
    std::string dns_host      {};
    std::string printing      {"INFO"};
    std::string plug_type     {"Dataflow_DimControlPlug"};
    std::string option_type   {"Dataflow_JobOptions"};
    std::string monitor_type  {"Dataflow_Monitoring"};
    std::string logger_type   {"Dataflow_OutputLogger"};
    std::string inc_hdlr_type {"Dataflow_IncidentHandler"};
    bool        automatic     = false;
    bool        terminate     = false;
    bool        debug         = false;
    bool        print_options = false;
    bool        have_pause    = false;
    int         dns_port      = -1;
    int         print_level   = Online::INFO;
    void configure()   {
      switch(::toupper(printing[0]))  {
      case '7':
      case 'A':  print_level = Online::ALWAYS;   break;
      case '6':
      case 'F':  print_level = Online::FATAL;    break;
      case '5':
      case 'E':  print_level = Online::ERROR;    break;
      case '4':
      case 'W':  print_level = Online::WARNING;  break;
      case '3':
      case 'I':  print_level = Online::INFO;  break;
      case '2':
      case 'D':  print_level = Online::DEBUG;    break;
      case '1':
      case 'V':  print_level = Online::VERBOSE;  break;
      default:   print_level = Online::DEBUG;    break;
      }
      if( debug )   {
	::lib_rtl_wait_debugger(true);
      }

      auto logger = std::make_shared<RTL::Logger::LogDevice>();
      RTL::Logger::LogDevice::setGlobalDevice(logger, print_level);
      RTL::Logger log(RTL::Logger::LogDevice::getGlobalDevice(),"Controller", print_level);

      if( plug_type.find('/') == std::string::npos ) plug_type += "/Plug";
      if( logger_type.find('/')  == std::string::npos ) logger_type  += "/Logger";
      if( option_type.find('/')  == std::string::npos ) option_type  += "/JobOptions";
      if( monitor_type.find('/')  == std::string::npos ) monitor_type  += "/Monitoring";
      if( inc_hdlr_type.find('/')  == std::string::npos ) inc_hdlr_type  += "/IncidentHandler";
      if( !dns_host.empty() )  {
	::dis_set_dns_node(dns_host.c_str());
	::dic_set_dns_node(dns_host.c_str());
      }
      if(  dns_port > 0     )  {
	::dis_set_dns_port(dns_port);
	::dic_set_dns_port(dns_port);
      }
    }
    int run(Opts&& opts)   {
      int sc;
      Online::DataflowManager manager("Manager",{option_type,logger_type,inc_hdlr_type,monitor_type});
      Online::DataflowContext& ctxt = manager.context;

      ctxt.logger->level = print_level;
      if( !option_file.empty() )   {
        sc = ctxt.options->load(option_file);
	if( sc != Online::DF_SUCCESS ) 
	  return manager.error("Failed to load job options: %s.",option_file.c_str());
      }
      if( !opts.empty() )    {
	for( const auto& o : opts )   {
	  std::size_t idx = o.first.find('.');
	  const std::string& comp  = o.first.substr(0,idx);
	  const std::string& name  = o.first.substr(idx+1);
	  ctxt.options->addProperty(comp, {name,o.second});
	}
	opts.clear();
      }
      if( !class_type.empty() )  {
	manager.properties["TaskType"] = "Dataflow_"+class_type+"/Task";
      }
      // Ensure all output streams are properly flushed before starting real execution
      std::ios_base::sync_with_stdio(true);
      std::cout << std::flush;
      ::fflush(stdout);
      ::fflush(stderr);
      // Now we can start:
      if( (sc=manager.configure()) != Online::DF_SUCCESS ) 
	return manager.error("Failed to configure manager. [sc=%d]",sc);
      if( ctxt.task )  {
	ctxt.task->properties["Options"]      = option_file;
	ctxt.task->properties["AutoShutdown"] = terminate;
	ctxt.task->properties["AutoStart"]    = automatic;
	ctxt.task->properties["PlugType"]     = plug_type;
	ctxt.task->properties["HavePause"]    = have_pause;
	ctxt.task->run();
      }
      return 1;
    }
  };
}

extern "C" int dataflow_run_task(int argc, char** argv)  {
  Dataflow_setup setup;
  RTL::CLI cli(argc,argv,help);
  setup.automatic  = cli.getopt("automatic",3) != 0;
  setup.terminate  = cli.getopt("terminate",3) != 0;
  setup.debug      = cli.getopt("debug",3) != 0;
  setup.have_pause = cli.getopt("pause",3) != 0;

  cli.getopt("class",      3, setup.class_type);
  cli.getopt("options",    4, setup.option_type);
  cli.getopt("opts",       4, setup.option_file);
  cli.getopt("incidents",  3, setup.inc_hdlr_type);
  cli.getopt("plug",       3, setup.plug_type);
  cli.getopt("setup",      3, setup.option_type);
  cli.getopt("monitoring", 3, setup.monitor_type);
  cli.getopt("msgsvc",     3, setup.logger_type);
  cli.getopt("print",      3, setup.printing);
  cli.getopt("dns-host",   6, setup.dns_host);
  cli.getopt("dns-port",   6, setup.dns_port);
  setup.configure();
  return setup.run(Opts());
}

///  Online namespace declaration
namespace Online  {
  int dataflow_python_task(Opts&& options)   {    
    if( !options.empty() )   {
      Dataflow_setup setup;
      Opts client_opts;

      ::lib_rtl_output(LIB_RTL_INFO,"Executing python startup of dataflow task....\n");
      for(const auto& o : options)   {
	if( o.first.find("__MAIN__.") == std::string::npos )
	  break;
	else if( o.first == "__MAIN__.PrintOptions" )
	  setup.print_options = ::toupper(o.second[0]) == 'T';
      }
      {
	Opts unknown_opts;
	char* end = nullptr;
	for(const auto& o : options)   {
	  std::size_t idx = o.first.find('.');
	  const std::string& comp  = o.first.substr(0,idx);
	  const std::string& name  = o.first.substr(idx+1);
	  if( setup.print_options )  {
	    ::lib_rtl_output(LIB_RTL_INFO," %s.%s = %s\n",comp.c_str(), name.c_str(), o.second.c_str());
	  }
	  if( comp == "__MAIN__" )   {
	    if(      0 == ::strcasecmp(name.c_str(), "OutputLogger") )
	      setup.logger_type = o.second;
	    else if( 0 == ::strcasecmp(name.c_str(), "Monitoring") )
	      setup.monitor_type = o.second;
	    else if( 0 == ::strcasecmp(name.c_str(), "JobOptions") )
	      setup.option_type = o.second;
	    else if( 0 == ::strcasecmp(name.c_str(), "ControlPlug") )
	      setup.plug_type = o.second;
	    else if( 0 == ::strcasecmp(name.c_str(), "OutputLevel") )
	      setup.printing = o.second;
	    else if( 0 == ::strcasecmp(name.c_str(), "IncidentHandler") )
	      setup.inc_hdlr_type = o.second;
	    else if( 0 == ::strcasecmp(name.c_str(), "Class") )
	      setup.class_type = o.second;
	    else if( 0 == ::strcasecmp(name.c_str(), "Options") )
	      setup.option_file = o.second;
	    else if( 0 == ::strcasecmp(name.c_str(), "PrintOptions") )
	      setup.print_options = ::toupper(o.second[0]) == 'T';
	    else if( 0 == ::strcasecmp(name.c_str(), "Debug") )
	      setup.debug = ::toupper(o.second[0]) == 'T';
	    else if( 0 == ::strcasecmp(name.c_str(), "Automatic") )
	      setup.automatic = ::toupper(o.second[0]) == 'T';
	    else if( 0 == ::strcasecmp(name.c_str(), "Terminate") )
	      setup.terminate = ::toupper(o.second[0]) == 'T';
	    else if( 0 == ::strcasecmp(name.c_str(), "HavePause") )
	      setup.have_pause = ::toupper(o.second[0]) == 'T';
	    else if( 0 == ::strcasecmp(name.c_str(), "DnsHost") )
	      setup.dns_host = o.second;
	    else if( 0 == ::strcasecmp(name.c_str(), "DnsPort") )
	      setup.dns_port = ::strtol(o.second.c_str(), &end, 10);
	    else if( 0 == ::strcasecmp(name.c_str(), "name") )
	      continue;
	    else
	      unknown_opts.push_back(o);
	    continue;
	  }
	  client_opts.push_back(o);
	}
	if( !unknown_opts.empty() )   {
	  ::lib_rtl_output(LIB_RTL_ERROR,"Program configuration error: Unknwon options found\n");
	  for( const auto& o : unknown_opts )   {
	    std::size_t idx = o.first.find('.');
	    const std::string& comp  = o.first.substr(0,idx);
	    const std::string& name  = o.first.substr(idx+1);
	    ::lib_rtl_output(LIB_RTL_ERROR,"ERROR: Unknown program option %s.%s = %s\n",
			     comp.c_str(), name.c_str(), o.second.c_str());
	  }
	  throw std::runtime_error("ERROR: Failed to configure dataflow task!");
	}
      }
      options.clear();
      setup.configure();
      return setup.run(std::move(client_opts));
    }
    throw std::runtime_error("ERROR: No job options supplied!");
  }
}
