//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  Delay.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================


/// Framework includes
#include <Dataflow/Delay.h>
#include <Dataflow/Incidents.h>
#include <CPP/TimeSensor.h>
#include <CPP/IocSensor.h>
#include <CPP/Event.h>
#include <RTL/rtl.h>

using namespace Online;

/// Initializing constructor
Delay::Delay(const std::string& nam, Context& ctxt)
  : Component(nam, ctxt)
{
  declareProperty("Initialize",   m_initDelay      = 0.0);
  declareProperty("Start",        m_startDelay     = 0.0);
  declareProperty("Stop",         m_stopDelay      = 0.0);
  declareProperty("Finalize",     m_finiDelay      = 0.0);
  declareProperty("Pause",        m_pauseDelay     = 0.0);
  declareProperty("Terminate",    m_terminateDelay = 0.0);
  declareProperty("Cancel",       m_cancelDelay    = 0.0);
  declareProperty("Continue",     m_continueDelay  = 0.0);
  declareProperty("Enable",       m_enableDelay    = 0.0);
  declareProperty("Event",        m_eventDelay     = 0.0);
  declareProperty("StopTrigger",  m_stopTrgDelay   = 0.0);
  declareProperty("StartTrigger", m_startTrgDelay  = 0.0);
  declareProperty("AutoPause",    m_autoPause      = 0.0);
  declareProperty("AutoContinue", m_autoContinue   = 0.0);
  declareProperty("AutoError",    m_autoError      = 0.0);
}

/// Default destructor
Delay::~Delay()   {
  this->delay("terminate", m_terminateDelay);
}

/// Sleep function
void Delay::delay(const char* reason, double timeout)  const   {
  if ( timeout > 1E-6 )  {
    long useconds = long(timeout*1E6);
    debug("+++ Executing delay on '%s' : %.3f seconds.", reason, timeout);
    if ( useconds > 0 ) ::lib_rtl_usleep(useconds);
  }
}

/// Initialize the delay component
int Delay::initialize()  {
  int sc = Component::initialize();
  if ( m_pauseDelay > 0e0 )
    subscribeIncident("DAQ_PAUSE");
  if ( m_cancelDelay > 0e0 )
    subscribeIncident("DAQ_CANCEL");
  if ( m_autoPause > 0e0 || m_autoError > 0.0 )
    subscribeIncident("DAQ_RUNNING");
  if ( m_startTrgDelay > 0e0 )
    subscribeIncident("DAQ_START_TRIGGER");
  if ( m_stopTrgDelay > 0e0 )
    subscribeIncident("DAQ_STOP_TRIGGER");
  if ( sc == DF_SUCCESS )   {
    this->delay("initialize", m_initDelay);
  }
  return sc;
}

/// Start the delay component
int Delay::start()  {
  int sc = Component::start();
  if ( sc == DF_SUCCESS )   {
    this->delay("start", m_startDelay);
  }
  return sc;
}

/// Finalize the delay component
int Delay::finalize()  {
  this->delay("finalize", m_finiDelay);
  unsubscribeIncidents();
  return Component::finalize();
}

/// Stop the delay component
int Delay::stop()  {
  this->delay("stop", m_stopDelay);
  return Component::stop();
}

/// Stop the delay component
int Delay::enable()  {
  this->delay("enable", m_enableDelay);
  return Component::enable();
}

/// Pause the data flow component. Default implementation is empty.
int Delay::pause()  {
  this->delay("pause", m_pauseDelay);
  return DataflowComponent::pause();
}

/// Continuing the data flow component. Default implementation is empty.
int Delay::continuing()  {
  this->delay("continue", m_continueDelay);
  return DataflowComponent::continuing();
}

/// Data processing overload: Send data record to network client
int Delay::execute(const Context::EventData& /* event */)  {
  this->delay("execute", m_eventDelay);
  return DF_SUCCESS;
}

/// Incident handler implemenentation: Inform that a new incident has occured
void Delay::handle(const DataflowIncident& inc)    {
  debug("Got incident: %s ot type:%s",inc.name.c_str(), inc.type.c_str());
  if ( inc.type == "DAQ_PAUSE" )  {
    this->delay("daq_pause", m_pauseDelay);
    if ( m_autoContinue > 0e0 )   {
      always("+++ Got incident %s: trigger auto transition to %s after %f seconds",
	     inc.type.c_str(), "RUNNING", m_autoContinue);
      IocSensor::instance().send(this, CMD_AUTO_CONTINUE, nullptr);
    }
  }
  else if ( inc.type == "DAQ_CANCEL" )  {
    this->delay("daq_cancel", m_cancelDelay);
  }
  else if ( inc.type == "DAQ_RUNNING" )  {
    if ( m_autoPause > 0e0 )   {
      always("+++ Got incident %s: trigger auto transition to %s after %f seconds",
	     inc.type.c_str(), "PAUSED", float(m_autoPause));
      IocSensor::instance().send(this, CMD_AUTO_PAUSE, nullptr);
    }
    if ( m_autoError > 0e0 )   {
      always("+++ Got incident %s: trigger auto transition to %s after %f seconds",
	     inc.type.c_str(), "ERROR", float(m_autoError));
      IocSensor::instance().send(this, CMD_AUTO_ERROR, nullptr);
    }
  }
  else if ( inc.type == "DAQ_START_TRIGGER" )  {
    this->delay("daq_start_trigger", m_startTrgDelay);
  }
  else if ( inc.type == "DAQ_STOP_TRIGGER" )  {
    this->delay("daq_stop_trigger", m_stopTrgDelay);
  }
}

/// Interactor overload: Command handler
void Delay::handle(const CPP::Event& ev)   {
  if(ev.eventtype == TimeEvent)  {
    long data = (long)ev.timer_data;
    if ( data == CMD_AUTO_PAUSE )   {
      this->always("+++ Timeout expired: Invoke auto transition to PAUSED.");
      this->fireIncident("DAQ_PAUSE");
    }
    else if ( data == CMD_AUTO_CONTINUE )   {
      this->always("+++ Timeout expired: Invoke auto transition to RUNNING.");
      this->fireIncident("DAQ_CONTINUE");
    }
    else if ( data == CMD_AUTO_ERROR )   {
      this->always("+++ Timeout expired: Invoke auto transition to ERROR.");
      this->fireIncident("DAQ_ERROR");
    }
    return;
  }
  else if( ev.eventtype == IocEvent )  {
    if ( ev.type == CMD_AUTO_PAUSE )   {
      TimeSensor::instance().add(this,m_autoPause,(void*)CMD_AUTO_PAUSE);
    }
    else if ( ev.type == CMD_AUTO_ERROR )   {
      TimeSensor::instance().add(this,m_autoError,(void*)CMD_AUTO_ERROR);
    }
    return;
  }
  error("Got Unkown event request: %d",ev.eventtype);
}
