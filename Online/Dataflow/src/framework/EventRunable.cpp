//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  EventRunable.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include <Dataflow/EventRunable.h>
#include <Dataflow/DataflowManager.h>
#include <Dataflow/EventContext.h>
#include <Dataflow/Incidents.h>
#include <Dataflow/MBMClient.h>
#include <RTL/rtl.h>

// C/C++ include files
#include <cstring>

typedef std::lock_guard<std::mutex> Lock;

/// Copy the event data to local buffer
void Online::EventRunable::DataGuard::copy()  {
  if ( data )  {
    char* tmp = new char[length];
    ::memcpy(tmp,data,length);
    data = tmp;
    owns = true;
  }
}

/// Initializing constructor
Online::EventRunable::EventRunable(const std::string& nam, Context& ctxt)
  : Component(nam, ctxt)
{
  this->declareProperty("NumErrorToStop",   this->m_nerrStop     = -1);
  this->declareProperty("ForceTMOExit",     this->m_forceTMOExit =  0);
  this->declareProperty("CopyData",         this->m_copyData     =  false);
  this->declareProperty("SplitBurst",       this->m_split_burst  =  false);
  this->declareProperty("DelayStop",        this->m_delayStop    =  0);
  this->declareProperty("DelayCancel",      this->m_delayCancel  =  0);
}

/// Default destructor
Online::EventRunable::~EventRunable()   {
}

/// Initialize the MBM server
int Online::EventRunable::initialize()  {
  int sc = this->Component::initialize();
  this->m_process   = PROCESS_EVENTS;
  this->m_forceExit = false;
  this->m_cancelled = 0;
  if ( sc == DF_SUCCESS )   {
    this->subscribeIncident("DAQ_EXIT");
    this->subscribeIncident("DAQ_PAUSE");
    this->subscribeIncident("DAQ_CANCEL");
    this->subscribeIncident("DAQ_CONTINUE");
    this->subscribeIncident("DAQ_EVENT_TIMEOUT");
  }
  this->declareMonitor("Events", "IN",    this->m_evtIN    = 0);
  this->declareMonitor("Events", "ERROR", this->m_errCount = 0, "Event processing error counter");
  this->declareMonitor("EventCount",      this->m_evtCount = 0, "Events processing counter");
  this->declareMonitor("RequestCount",    this->m_reqCount = 0, "Events requested  counter");
  this->declareMonitor("FreeCount",       this->m_freeCount= 0, "Events released   counter");
  return sc;
}

/// Initialize the event processor
int Online::EventRunable::start()  {
  int sc = this->Component::start();
  this->m_process   = PROCESS_EVENTS;
  this->m_forceExit = false;
  this->m_cancelled = 0;
  this->m_errCount  = 0;
  this->m_evtIN     = 0;
  this->m_evtOUT    = 0;
  return sc;
}

/// Initialize the event processor
int Online::EventRunable::stop()  {
  if ( this->m_delayStop > 0 )   {
    while ( (::time(0)-this->m_cancelled) < this->m_delayStop )
      ::lib_rtl_sleep(100);
    this->m_process = STOP_EVENTS;
    this->cancelEvents();
  }
  this->m_process = STOP_EVENTS;
  if ( this->m_loop.get() ) {
    auto tmp = this->m_loop;
    this->m_loop.reset();
    tmp->join();
  }
  return this->Component::stop();
}

/// Finalize the event processor
int Online::EventRunable::finalize()  {
  this->unsubscribeIncidents();
  this->undeclareMonitors();
  this->undeclareMonitors("Events");
  return this->Component::finalize();
}

/// Cancel I/O operations of the dataflow component
int Online::EventRunable::cancel()  {
  this->m_cancelled = ::time(0);
  if ( this->m_delayCancel > 0 )   {
    while ( (::time(0)-this->m_cancelled) < this->m_delayCancel )
      ::lib_rtl_sleep(100);
  }
  return (this->m_delayStop == 0) ? this->cancelEvents() : DF_SUCCESS;
}

/// Execute runable. Start processing thread
int Online::EventRunable::run()  {
  this->m_loop = std::make_shared<std::thread>([this]{ this->processEvents(); });
  return DF_SUCCESS;
}

/// Pause the component. Halts the event loop after processing the current event
int Online::EventRunable::pause()  {
  this->m_process = HALT_EVENTS;
  //m_lock.lock();
  return DF_SUCCESS;
}

/// Re-enable the event loop and continue processing
int Online::EventRunable::continuing()   {
  this->m_process = PROCESS_EVENTS;
  this->m_lock.unlock();
  return DF_SUCCESS;
}

/// Incident handler implemenentation: Inform that a new incident has occured
void Online::EventRunable::handle(const DataflowIncident& inc)    {
  debug("Executing incident callback of type %s", inc.type.c_str());
  if( inc.type == "DAQ_CANCEL" )  {
    if( m_delayStop == 0 ) m_process = STOP_EVENTS;
    m_cancelled = ::time(0);
  }
  else if( inc.type == "DAQ_CONTINUE" )  {
    m_process = PROCESS_EVENTS;
  }
  else if( inc.type == "DAQ_PAUSE" )  {
    m_process = HALT_EVENTS;
  }
  else if( inc.type == "DAQ_EXIT" )  {
    m_forceExit = true;
  }
  else if( inc.type == "DAQ_EVENT_TIMEOUT" )  {
    m_eventTMO = true;
  }
}

/// Event access interface: Rearm event request to data source
int Online::EventRunable::rearmEvent()  {
  return DF_SUCCESS;
}

/// Event access interface: Shutdown event access in case of forced exit request
int Online::EventRunable::shutdown()   {
  return DF_SUCCESS;
}

/// Event access interface: Release event
int Online::EventRunable::freeEvent(Context::EventData& /* event */)   {
  return DF_SUCCESS;
}

/// Check if the event loop should be continued
int Online::EventRunable::continueProcessing()   {
  while(1)  {
    if ( this->m_forceExit )
      break;
    else if ( this->m_process == PROCESS_EVENTS )
      return 1;
    else if ( this->m_cancelled != 0 && 
	      this->m_delayStop > 0  && 
	      (::time(0)-this->m_cancelled) < this->m_delayStop )
      return 1;
    else if ( this->m_process == STOP_EVENTS )
      return 0;
    ::lib_rtl_sleep(100);
  }
  // If we got a forced exit, do not attempt to process a new event. Exit NOW!
  if ( this->m_forceExit )  {
    info("Data processing stops now. Calling _exit.");
    shutdown();
    ::_exit(0);
  }
  // If there were too many processing error: Issue error and stop
  else if ( (this->m_nerrStop > 0) && (this->m_errCount >= m_nerrStop) )  {
    fireIncident("DAQ_ERROR");
    return 0;
  }
  // Handle event processing timeouts
  else if ( m_eventTMO )  {
    fireIncident("DAQ_EVENT_TIMEOUT");
    if ( m_forceTMOExit )  {
      fireIncident("DAQ_ERROR");
      return 0;
    }
  }
  return 1;
}

/// Event access interface: Delayed cancel of event processing
int Online::EventRunable::cancelEvents()   {
  return DF_SUCCESS;
}

/// Execute the event loop
int Online::EventRunable::processEvents()   {
  DataflowManager& mgr = context.manager.manager();
  // Enter the main event processing loop
  while (1)  {
    // Get the next event and process it....
    int sc;
    DataGuard event;
    try  {
      if ( !continueProcessing() )  {
	return DF_SUCCESS;
      }
      //
      // Here normal event processing starts:
      //
      ++m_reqCount;
      sc = getEvent(event);
      if ( sc == DF_CANCELLED )   {
	return DF_SUCCESS;
      }
      else if ( sc == DF_ERROR && m_process == PROCESS_EVENTS )   {
	++m_errCount;
	continue;
      }
      else if ( !continueProcessing() ) {
	// Note this is NOT the same as the first clause.
	// An incident may have happened while waiting in getEvent().
	return DF_SUCCESS;
      }
      ++m_evtCount;
      if ( m_copyData ) { 
	Context::EventData tmp = event;
	event.copy();
	++m_freeCount;
	freeEvent(tmp);
      }
    }
    catch( const std::exception& e ) {
      info("Caught unhandled exception in event loop(access): %s",e.what());
      continue;
    }
    catch(...) {
      info("Caught unknown exception in event loop(access).");
      continue;
    }
    ++m_evtIN;
    sc = DF_ERROR;  {
      // From here on we have a locked operation.
      Lock lock(m_lock);
      try  {
	if ( m_split_burst )    {
	  /// TODO: Split burst!
	  std::unique_ptr<EventContext> evt_context(new EventContext());
	  event.context = evt_context.get();
	  fireIncident("DAQ_BEGIN_EVENT");
	  sc = mgr.execute(event);
	}
	else   {
	  std::unique_ptr<EventContext> evt_context(new EventContext());
	  event.context = evt_context.get();
	  fireIncident("DAQ_BEGIN_EVENT");
	  sc = mgr.execute(event);
	}
      }
      catch( const std::exception& e ) {
	info("Caught unhandled exception in main event loop: %s",e.what());
      }
      catch(...) {
	info("Caught unknown exception in main event loop.");
      }
      event.context = 0;
      ++m_evtOUT;
      if ( sc != DF_SUCCESS ) ++m_errCount;
      fireIncident("DAQ_END_EVENT");
    }
    if ( !m_copyData )   {
      ++m_freeCount;
      freeEvent(event);
    }
  }
  return DF_SUCCESS;
}
