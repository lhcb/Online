//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : Online/GaudiOnlineTests
//
//  Author     : Markus Frank
//==========================================================================

/// Framework include files
#include <Dataflow/ExternalCallWrapper.h>

/// Include files from the framework
#include <CPP/IocSensor.h>
#include <CPP/Event.h>

/// C/C++ include files
#include <stdexcept>

using namespace Online;

/// Initializing constructor
ExternalCallWrapper::ExternalCallWrapper(const std::string& nam, Context& ctxt)
  : DataflowComponent(nam, ctxt)
{
  declareProperty("Start",          this->proc_start = "initialize");
  declareProperty("Stop",           this->proc_stop  = "finalize");
}

int ExternalCallWrapper::initialize_external_call()  {
  return DF_SUCCESS;
}

/// Start the process using the stub
int ExternalCallWrapper::start_external_call(const std::string& when)   {
  throw std::runtime_error("When: "+when+". Invalid call to ExternalCallWrapper::start_external_call");
}

/// Stop the process using the stub
int ExternalCallWrapper::stop_external_call(const std::string& when)   {
  throw std::runtime_error("When: "+when+". Invalid call to ExternalCallWrapper::stop_external_call");
}

/// Initialize the component
int ExternalCallWrapper::initialize()  {
  int sc = DataflowComponent::initialize();
  if ( DF_SUCCESS == sc )   {
    sc = initialize_external_call();
    if ( DF_SUCCESS == sc )   {
      this->start_external_call("initialize");
      this->stop_external_call("initialize");

      if ( this->proc_start == "running" ) 
	this->subscribeIncident("DAQ_RUNNING");
      else if ( this->proc_start == "pause" ) 
	this->subscribeIncident("DAQ_PAUSE");
      else if ( this->proc_start == "cancel" ) 
	this->subscribeIncident("DAQ_CANCEL");
      else if ( this->proc_start == "start_trigger" ) 
	this->subscribeIncident("DAQ_START_TRIGGER");
      else if ( this->proc_start == "stop_trigger" ) 
	this->subscribeIncident("DAQ_STOP_TRIGGER");
    }
  }
  return sc;
}

/// Start the component
int ExternalCallWrapper::start()    {
  int sc = DataflowComponent::start();
  if ( sc == DF_SUCCESS )  {
    this->start_external_call("start");
    this->stop_external_call("start");
  }
  return sc;
}

/// Stop the component
int ExternalCallWrapper::stop()  {
  this->start_external_call("stop");
  this->stop_external_call("stop");
  return DataflowComponent::stop();
}

/// Finalize the component
int ExternalCallWrapper::finalize()  {
  this->start_external_call("finalize");
  this->stop_external_call("finalize");
  return DataflowComponent::finalize();
}

/// Enable the data flow component. 
int ExternalCallWrapper::enable()   {
  this->start_external_call("enable");
  this->stop_external_call("enable");
  return DataflowComponent::enable();
}

/// Cancel the data flow component. 
int ExternalCallWrapper::cancel()   {
  this->start_external_call("cancel");
  this->stop_external_call("cancel");
  return DataflowComponent::cancel();
}

/// Pause the data flow component. 
int ExternalCallWrapper::pause()   {
  this->start_external_call("pause");
  this->stop_external_call("pause");
  return DataflowComponent::pause();
}

/// Incident handler implemenentation: Inform that a new incident has occured
void ExternalCallWrapper::handle(const DataflowIncident& inc)    {
  
  debug("Got incident: %s ot type:%s", inc.name.c_str(), inc.type.c_str());
  if ( inc.type == "DAQ_PAUSE" )  {
    this->start_external_call("pause");
    this->stop_external_call("pause");
    IocSensor::instance().send(this, CMD_PAUSE, nullptr);
  }
  else if ( inc.type == "DAQ_CANCEL" )  {
    this->start_external_call("cancel");
    this->stop_external_call("cancel");
    IocSensor::instance().send(this, CMD_CANCEL, nullptr);
  }
  else if ( inc.type == "DAQ_RUNNING" )  {
    this->start_external_call("running");
    this->stop_external_call("running");
    IocSensor::instance().send(this, CMD_RUNNING, nullptr);
  }
  else if ( inc.type == "DAQ_START_TRIGGER" )  {
    this->start_external_call("start_trigger");
    this->stop_external_call("start_trigger");
    IocSensor::instance().send(this, CMD_START_TRIGGER, nullptr);
  }
  else if ( inc.type == "DAQ_STOP_TRIGGER" )  {
    this->start_external_call("stop_trigger");
    this->stop_external_call("stop_trigger");
    IocSensor::instance().send(this, CMD_STOP_TRIGGER, nullptr);
  }
}

/// Interactor overload: Command handler
void ExternalCallWrapper::handle(const CPP::Event& ev)   {
  if(ev.eventtype == TimeEvent)  {
    //long data = (long)ev.timer_data;
    return;
  }
  else if( ev.eventtype == IocEvent )  {
    switch(ev.type)   {
    case CMD_PAUSE:
      break;
    case CMD_CANCEL:
      break;
    case CMD_RUNNING:
      break;
    case CMD_START_TRIGGER:
      break;
    case CMD_STOP_TRIGGER:
      break;
    }
    return;
  }
  error("Got Unkown event request: %d",ev.eventtype);
}
