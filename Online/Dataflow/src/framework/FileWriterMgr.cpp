//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  Filewriter.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Beat Jost
//==========================================================================
#include <Dataflow/FileWriterMgr.h>
#include <Dataflow/MBMClient.h>
#include <Dataflow/Incidents.h>
#include <Dataflow/DataflowManager.h>
#include <Dataflow/ComponentHandler.h>

#include <RTL/rtl.h>
#include <MBM/mepdef.h>
#include <MBM/Consumer.h>
#include <MBM/Requirement.h>
#include <Tell1Data/Tell1Decoder.h>

// C/C++ include files
#include <limits>

using namespace std;
using namespace Online;
#define NO_EVENT_INPUT "None"

FileWriterMgr::Processor::Processor(const string& nam, Context& ctxt)
  : Component(nam, ctxt)
{
  declareProperty("Requirements", m_req);
}

FileWriterMgr::Processor::~Processor()   {
}

/// Service overload: initialize()
int FileWriterMgr::Processor::initialize()   {
  int sc = Component::initialize();
  m_breq.clear();
  for (auto i : m_req)  {
    MBM::Requirement r;
    r.parse(i);
    m_breq.insert(m_breq.end(),r);
  }
  return sc;
}

/// Service overload: finalize()
int FileWriterMgr::Processor::finalize()   {
  m_breq.clear();
  unsubscribeIncidents();
  undeclareMonitors();
  return Component::finalize();
}

std::vector<std::string> FileWriterMgr::Processor::getRequirements() const   {
  return m_req;
}

bool FileWriterMgr::Processor::matchRequirements(const Context::EventData &e)  const  {
  bool tmatch = false, vmatch = false;
  for (const auto& req : m_breq )    {
    for( int i=0; i < 4; i++ )   {
      tmatch = tmatch | ((req.trmask[i]   & e.mask[i]) != 0);
      vmatch = vmatch | ((req.vetomask[i] & e.mask[i]) !=0);
    }
  }
  if ( !tmatch || vmatch )   {
    return false;
  }
  bool keep = false, selected = false;
  float all_evts = (1e0 - std::numeric_limits<float>::epsilon());
  for ( const auto& req : m_breq )    {
    if ( req.freqType == BM_FREQ_PERC )   {
      float freq = req.freq / 100e0;  // requirement is in percent!
      if ( freq > 0 && freq < all_evts )   {
	double rndm = double(::rand()) / double(RAND_MAX);
	selected = true;
	if ( rndm <= freq )  {
	  keep = true;
	}
      }
    }
  }
  return selected ? keep : true;
}

// Standard Constructor
FileWriterMgr::FileWriterMgr(const string& nam, Context& ctxt) :
  Component(nam, ctxt), m_consumer(0), m_receiveEvts(false)
{
  m_req = "EvType=2;TriggerMask=0x0,0x0,0x0,0x0;VetoMask=0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF;MaskType=ANY;UserType=VIP;Frequency=PERC;Perc=100.0";
  declareProperty("Requirement", m_req);
  declareProperty("Input",       m_input = NO_EVENT_INPUT);
  declareProperty("Processors",  m_processors);
}

// Standard Destructor
FileWriterMgr::~FileWriterMgr()
{
}

int FileWriterMgr::initialize()
{
  int sc = Component::initialize();
  DataflowManager& mgr = context.manager.manager();
  ComponentHandler handler(context);
  for(const auto& i : m_processors)  {
    Component*  c = mgr.getManagedComponent(i,false);
    Processor* fs = c->query<Processor>();
    if ( fs )
      m_procList.insert(m_procList.end(),fs);
    else
      sc = DF_ERROR;
  }
  if (sc == DF_SUCCESS)
    {
      declareMonitor("EventsIn", m_evtIn = 0,    "Number of Events received.");
      declareMonitor("FramesIn", m_framesIn = 0, "Number of Frames received.");
      if ( m_input != NO_EVENT_INPUT )   {
	m_consumer = context.mbm->createConsumer(m_input,RTL::processName());
      }
      m_requirement.parse(m_req);
      return sc;
    }
  return error("Failed to initialize service base class.");
}

/// Start the dataflow component
int FileWriterMgr::start()   {
  int sc = Component::start();
  m_requirement.parse(m_req);
  for (auto fw : m_procList)  {
    std::vector<std::string> reqlist = fw->getRequirements();
    for (const auto& rq : reqlist )  {
      MBM::Requirement r;
      r.parse(rq);
      m_requirement.trmask[0]   |= r.trmask[0];
      m_requirement.trmask[1]   |= r.trmask[1];
      m_requirement.trmask[2]   |= r.trmask[2];
      m_requirement.trmask[3]   |= r.trmask[3];
      m_requirement.vetomask[0] &= r.vetomask[0];
      m_requirement.vetomask[1] &= r.vetomask[1];
      m_requirement.vetomask[2] &= r.vetomask[2];
      m_requirement.vetomask[3] &= r.vetomask[3];
    }
  }
  if ( m_input != NO_EVENT_INPUT )   {
    m_consumer->addRequest(m_requirement);
  }
  return sc;
}

/// Stop the dataflow component
int FileWriterMgr::stop()   {
  if ( m_consumer ) m_consumer->delRequest(m_requirement);
  m_requirement.parse(m_req);
  return Component::stop();
}

int FileWriterMgr::finalize()   {
  detail::deletePtr(m_consumer);
  unsubscribeIncidents();
  undeclareMonitors();
  return Component::finalize();
}

/// Cancel the data flow component. 
int FileWriterMgr::cancel()   {
  m_receiveEvts = false;
  if ( m_consumer ) m_consumer->cancel();
  return Component::cancel();
}

/// ControlPlug invocation:  Enable trigger
int FileWriterMgr::enable()   {
  m_receiveEvts = true;
  return Component::enable();
}

/// Process events. Input buffer is ALWAYS in mdf bank format.
int FileWriterMgr::execute(const Context::EventData& event)  {
  for(auto i : m_procList )    {
    i->execute(event);
  }
  this->processingFlag |= DF_PASSED;
  return DF_SUCCESS;
}

/// Process event loop as main runable
int FileWriterMgr::run()   {
  m_receiveEvts = true;
  for (int sc = m_consumer->getEvent(); sc == MBM_NORMAL && m_receiveEvts; sc = m_consumer->getEvent())   {
    const MBM::EventDesc& e = m_consumer->event();
    ++m_framesIn;
    switch ( e.type )	{
    case EVENT_TYPE_BURST:  
    case EVENT_TYPE_EVENT:	  {
      size_t len = e.len;
      char*  beg = (char*)e.data;
      char*  end = beg+len;
      Context::EventData event;
      while ( beg < end )  {
	auto* h = (event_header_t*)beg;
	if ( h->is_mdf() )   {
	  auto m = h->subHeader().H1->triggerMask();
	  auto r = h->subHeader().H1->runNumber();
	  m_evtIn++;
	  context.manager.setRunNumber(r);
	  event.release = 0;
	  event.data    = beg;
	  event.header  = h;
	  event.length  = h->size0();
	  event.type    = EVENT_TYPE_EVENT;
	  event.mask[0] = m[0];
	  event.mask[1] = m[1];
	  event.mask[2] = m[2];
	  event.mask[3] = m[3];
	  this->execute(event);
	  beg += h->recordSize();
	  continue;
	}
	error("Bad MDF frame encountered. Skipping frame with %ld bytes.",size_t(end-beg));
	break;
      }
      break;
    }
    case EVENT_TYPE_MEP:
      // MEPs are a bit more difficult, but if need arises, see in MEPSelector.cpp
      error("Unknown Event Data Type. [MEP] %d [Skip frame]",e.type);
      continue;

    default:
      error("Unknown Event Data Type. %d",e.type);
      continue;
    }
  }
  return DF_SUCCESS;
}
