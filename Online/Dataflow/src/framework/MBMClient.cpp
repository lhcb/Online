//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  MBMClient.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include <Dataflow/MBMClient.h>
#include <MBM/Producer.h>
#include <MBM/Consumer.h>
#include <MBM/Requirement.h>
#include <MBM/bmdef.h>
#include <RTL/rtl.h>

// C/C++ include files
#include <stdexcept>
#include <cctype>
#include <cstdio>

/// Initializing constructor
Online::MBMClient::MBMClient(const std::string& nam, Context& ctxt)
  : Component(nam, ctxt), m_partitionID(0x103)
{
  m_procName = RTL::processName();
  declareProperty("Buffers",              m_buffers);
  declareProperty("PartitionID",          m_partitionID      = 0xFFFF);
  declareProperty("PartitionName",        m_partitionName    = "");
  declareProperty("PartitionBuffers",     m_partitionBuffers = false);
  declareProperty("ConnectWhen",          m_connectWhen      = "initialize");
  declareProperty("DisconnectWhen",       m_disconnectWhen   = "");
  declareProperty("Communication",        m_communication    = "FIFO");
  declareProperty("EnableProcessing",     m_enabled          = true);
  declareProperty("InhibitCancel",        m_inhibitCancel    = false);
  context.mbm = this;
}

/// Default destructor
Online::MBMClient::~MBMClient()   {
}

/// Initialize the MBM client
int Online::MBMClient::initialize()  {
  if( Component::initialize() != DF_SUCCESS )
    return error("Failed to initialize base class Service.");
  if( !m_enabled )
    context.mbm = nullptr;
  return (context.mbm && m_connectWhen == "initialize") ? i_init() : DF_SUCCESS;
}

/// Start MEP manager service
int Online::MBMClient::start()  {
  if( Component::start() != DF_SUCCESS )
    return error("Failed to initialize base class Service.");
  if( !m_enabled )
    context.mbm = nullptr;
  int sc = (context.mbm && m_connectWhen == "start") ? i_init() : DF_SUCCESS;
  for(auto bmid : m_bmIDs)   {
    if( bmid != MBM_INV_DESC ) ::mbm_set_cancelled(bmid, 0);
  }
  return sc;
}

/// Finalize the MBM client
int Online::MBMClient::finalize()  {
  if (context.mbm )   {
    if( !m_disconnectWhen.empty() )   {
      i_fini(); // Do it unconditionally here if it has not been done at stop!
    }
    else if( m_connectWhen == "initialize") i_fini();
  }
  return Component::finalize();
}

/// Stop MEP manager service
int Online::MBMClient::stop()  {
  if (context.mbm )   {
    if( !m_disconnectWhen.empty() )   {
      if( m_disconnectWhen == "stop") i_fini();
    }
    else if( m_connectWhen == "start") i_fini();
  }
  return Component::finalize();
}

/// Initialize the MBM client
int Online::MBMClient::i_init()  {
  setProperties();
  m_procName = RTL::processName();
  if( DF_SUCCESS != connectBuffers() )
    return error("%s: Failed to connect to MBM buffers!",m_connectWhen.c_str());
  debug("%s: Connecting to MBM Buffers.",m_connectWhen.c_str());
  return DF_SUCCESS;
}

/// Finalize the MBM client
int Online::MBMClient::i_fini()  {
  info("Excluding from buffers. No more buffer access possible.");
  for( const auto& prod : m_sharedProducers )
    delete prod.second;
  m_sharedProducers.clear();
  m_buffMap.clear();
  for( BMID bmid : m_bmIDs )  {
    if( bmid != MBM_INV_DESC ) ::mbm_exclude(bmid);
  }
  m_bmIDs.clear();
  return DF_SUCCESS;
}

/// Create proper buffer name depending on partitioning
std::string Online::MBMClient::bufferName(const std::string& nam)  const   {
  std::string bm_name = nam;
  if( m_partitionBuffers ) {
    bm_name += "_";
    if( m_partitionName.empty() )   {
      char part_id[16];
      std::snprintf(part_id,sizeof(part_id),"%X",m_partitionID);
      bm_name += part_id;
    }
    else  {
      bm_name += m_partitionName;
    }
  }
  return bm_name;
}

/// Connect to optional MBM buffer
int Online::MBMClient::connectBuffer(const std::string& nam)   {
  std::string bm_name = bufferName(nam);
  if( m_buffMap.find(bm_name) == m_buffMap.end() ) {
    BMID bmid = MBM_INV_DESC;
    int flags = BM_INC_RDWR;
    if( m_communication == "FIFO" )
      bmid = ::mbm_include(bm_name.c_str(),m_procName.c_str(),m_partitionID,BM_COM_FIFO,flags);
    else if( m_communication == "ASIO" )
      bmid = ::mbm_include(bm_name.c_str(),m_procName.c_str(),m_partitionID,BM_COM_ASIO,flags);
    else if( m_communication == "UNIX" )
      bmid = ::mbm_include(bm_name.c_str(),m_procName.c_str(),m_partitionID,BM_COM_UNIX,flags);

    if( bmid == MBM_INV_DESC )  {
      return error("%s: Failed to connect to buffer '%s' as '%s'",
		   m_connectWhen.c_str(), bm_name.c_str(), m_procName.c_str());
    }
    m_bmIDs.push_back(bmid);
    m_buffMap[bm_name] = bmid;
    m_buffMap[nam] = bmid;
    debug("%s: Included in MBM buffer: '%s'", m_connectWhen.c_str(), nam.c_str());
  }
  return DF_SUCCESS;
}

/// Connect to specified buffers
int Online::MBMClient::connectBuffers()  {
  if( m_buffers.size() > 0 )  {
    for( const auto& nam : m_buffers )  {
      int sc = connectBuffer(nam);
      if( DF_SUCCESS != sc ) return DF_ERROR;
    }
  }
  return DF_SUCCESS;
}

/// Cancel MBM connections
int Online::MBMClient::cancelBuffers()  {
  if( !m_inhibitCancel )  {
    for( auto bmid : m_bmIDs )  {
      if( bmid != MBM_INV_DESC ) ::mbm_cancel_request(bmid);
    }
    debug("Cancelled pending MBM I/O requests.");
  }
  return DF_SUCCESS;
}

/// Cancel MBM connections
int Online::MBMClient::requestCancelBuffers()  {
  if( !m_inhibitCancel )  {
    for(auto bmid : m_bmIDs)  {
      if( bmid != MBM_INV_DESC ) ::mbm_set_cancelled(bmid, 1);
    }
    debug("Cancelled pending MBM I/O requests.");
  }
  return DF_SUCCESS;
}

/// Include into buffer manager as a client
BMID Online::MBMClient::access(const std::string& buffer)  {
  auto i = m_buffMap.find(buffer);
  if( i == m_buffMap.end() ) {
    std::string bm_name = bufferName(buffer);
    i = m_buffMap.find(bm_name);
  }
  if( i != m_buffMap.end() ) {
    BMID bmid = (*i).second;    
    return bmid;
  }
  //throwError("The buffer %s is not mapped. You cannot access it.",buffer.c_str());
  return MBM_INV_DESC;
}

/// Include into buffer manager as a client
BMID Online::MBMClient::include(const std::string& nam,const std::string& instance)  {
  if( context.mbm )   {
    BMID bmid = MBM_INV_DESC;
    int  flags = BM_INC_RDWR;
    if( (bmid=access(nam)) != MBM_INV_DESC )
      return bmid;
    std::string bm_name = bufferName(nam);
    if( (bmid=access(bm_name)) != MBM_INV_DESC )
      return bmid;
    else if( m_communication == "FIFO" )
      bmid = ::mbm_include(bm_name.c_str(),instance.c_str(),partitionID(),BM_COM_FIFO,flags);
    else if( m_communication == "ASIO" )
      bmid = ::mbm_include(bm_name.c_str(),instance.c_str(),partitionID(),BM_COM_ASIO,flags);
    else if( m_communication == "UNIX" )
      bmid = ::mbm_include(bm_name.c_str(),instance.c_str(),partitionID(),BM_COM_UNIX,flags);
    if( bmid != MBM_INV_DESC )  {
      m_bmIDs.push_back(bmid);
      m_buffMap[bm_name] = bmid;
      m_buffMap[nam] = bmid;
      return bmid;
    }
  }
  return 0;
}

/// Exclude from the buffer manager
int Online::MBMClient::exclude(BMID bmid)   {
  if( bmid != MBM_INV_DESC )  {
    for(auto i=m_bmIDs.begin(); i != m_bmIDs.end(); ++i)  {
      if( *i == bmid )  {
	int sc = ::mbm_exclude(bmid);
	if( sc == MBM_NORMAL )  {
	  for(auto j=m_buffMap.begin(); j != m_buffMap.end(); ++j)  {
	    if( (*j).second == bmid ) { m_buffMap.erase(j); break; }
	  }
	  // Second pass necessary, because we have possibly 2 entries!
	  for(auto j=m_buffMap.begin(); j != m_buffMap.end(); ++j)  {
	    if( (*j).second == bmid ) { m_buffMap.erase(j); break; }
	  }
	  m_bmIDs.erase(i);
	  return DF_SUCCESS;
	}
      }
    }
  }
  return DF_ERROR;
}

/// Create shared producer. These producers are owned by the MEPManager!
MBM::Producer*
Online::MBMClient::createSharedProducer(const std::string& buffer,const std::string& instance)  {
  if( buffer.empty() )   {
    throwError("EMPTY buffer name. Failed to create shared producer "+instance+" --> CHECK OPTIONS!");
  }
  else if( context.mbm )   {
    std::pair<std::string,std::string> key = std::make_pair(buffer, instance);
    auto i = m_sharedProducers.find(key);
    if( i == m_sharedProducers.end() )  {
      MBM::Producer* p = createProducer(buffer, instance);
      m_sharedProducers[key] = p;
      return p;
    }
    return (*i).second;
  }
  throwError("MBM access is NOT ENABLED. No producers can be created. Buffer:"+buffer+" instance:"+instance);
  return nullptr;
}

/// Create producer
MBM::Producer*
Online::MBMClient::createProducer(const std::string& buffer,const std::string& instance)  {
  if( buffer.empty() )   {
    throwError("EMPTY buffer name. Failed to create producer "+instance+" --> CHECK OPTIONS!");
  }
  else if( context.mbm )   {
    auto i = m_buffMap.find(buffer);
    if( i == m_buffMap.end() ) {
      std::string bm_name = bufferName(buffer);
      i = m_buffMap.find(bm_name);
    }
    if( i != m_buffMap.end() ) {
      BMID bmid = (*i).second;
      return new MBM::Producer(bmid, instance, partitionID());
    }
    throwError("Buffer '"+buffer+"' is not mapped. Failed to create producer '"+instance+"'");
  }
  throwError("MBM access is NOT ENABLED. No producers can be created. Buffer:'"+buffer+"' instance:"+instance);
  return nullptr;
}

/// Create consumer attached to a specified buffer
MBM::Consumer*
Online::MBMClient::createConsumer(const std::string& buffer,const std::string& instance)  {
  if( buffer.empty() )   {
    throwError("EMPTY buffer name. Failed to create consumer "+instance+" --> CHECK OPTIONS!");
  }
  else if( context.mbm )   {
    auto i = m_buffMap.find(buffer);
    if( i == m_buffMap.end() ) {
      std::string bm_name = bufferName(buffer);
      i = m_buffMap.find(bm_name);
    }
    if( i != m_buffMap.end() ) {
      BMID bmid = (*i).second;
      return new MBM::Consumer(bmid, instance, partitionID());
    }
    throwError("Buffer '"+buffer+"' is not mapped. Failed to create consumer "+instance);
  }
  throwError("MBM access is NOT ENABLED. No consumers can be created. Buffer:'"+buffer+"' instance:"+instance);
  return nullptr;
}
