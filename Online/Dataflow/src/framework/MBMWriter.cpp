//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  MBMWriter.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include <Dataflow/MBMWriter.h>
#include <Dataflow/MBMClient.h>
#include <MBM/Producer.h>

// C/C++ include files
#include <cstring>

/// Initializing constructor
Online::MBMWriter::MBMWriter(const std::string& nam, Context& ctxt)
  : Component(nam, ctxt), m_producer(0), m_waiting(false)
{
  declareProperty("Buffer", m_buffer="");
}

/// Default destructor
Online::MBMWriter::~MBMWriter()   {
}

/// Initialize the MBM server
int Online::MBMWriter::start()  {
  int sc = Component::start();
  if( sc == DF_SUCCESS )  {
    m_producer = context.mbm->createProducer(m_buffer,name);
    if( !m_producer )  {
      throwError("Failed to create producer %s to buffer %s!",
		 cname(), m_buffer.c_str());
    }
  }
  m_waiting = false;
  return sc;
}

/// Stop the data flow component. Default implementation is empty.
int Online::MBMWriter::stop()  {
  if( m_waiting ) context.mbm->cancelBuffers();
  if( m_waiting && m_producer ) m_producer->cancel();
  detail::deletePtr(m_producer);
  return Component::stop();
}

/// Cancel the data flow component. 
int Online::MBMWriter::cancel()   {
  if( m_waiting && m_producer ) m_producer->cancel();
  return Component::cancel();
}

/// Data processing overload: Send data record to network client
int Online::MBMWriter::execute(const Context::EventData& event)  {
  int sc = DF_ERROR;
  try  {
    m_waiting = true;
    if( !m_producer )  {
      throwError("Execute: No producer availible [Internal Error]");
    }
    sc = m_producer->getSpace(event.length);
    if( sc == MBM_REQ_CANCEL )  {
      sc = DF_CANCELLED;
    }
    else if( MBM_NORMAL == sc )  {
      MBM::EventDesc& e = m_producer->event();
      e.len     = event.length;
      e.type    = event.type;
      e.mask[0] = event.mask[0];
      e.mask[1] = event.mask[1];
      e.mask[2] = event.mask[2];
      e.mask[3] = event.mask[3];
      ::memcpy(e.data,event.data,event.length);
      sc = m_producer->spaceAction();
      if( sc == MBM_REQ_CANCEL )
	sc = DF_CANCELLED;
      else if( sc != MBM_NORMAL )  {
	sc = DF_ERROR;
      }
    }
  }
  catch( const std::exception& e )   {
    error(e,"Execute: Error processing event.");
  }
  catch( ... )  {
    error("Execute: UNKOWN error processing event.");
  }
  m_waiting = false;
  return sc;
}
