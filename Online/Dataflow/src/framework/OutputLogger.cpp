//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  OutputLogger.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include <Dataflow/OutputLogger.h>
#include <DD4hep/Printout.h>
#include <RTL/FmcLogDevice.h>
#include <RTL/Logger.h>
#include <RTL/rtl.h>

// C/C++ include files
#include <cstring>
#include <mutex>

namespace {

  bool s_locked_printouts = false;
  std::mutex output_lock;
  static std::size_t __print_func(void* p, dd4hep::PrintLevel lvl, const char* src, const char* fmt, va_list& args) {
    auto* logger = (Online::OutputLogger*)p;
    if( logger )  {
      if( s_locked_printouts )  {
	std::lock_guard<std::mutex> lk(output_lock);
	return logger->print(lvl,src,fmt,args);
      }
      return logger->print(lvl,src,fmt,args);
    }
    return ::fprintf(stderr,"ERROR NO LOGGER PRESENT!!!");
  }
  static std::size_t __rtl_print_func(void* p, int lvl, const char* fmt, va_list& args)  {
    return __print_func(p,dd4hep::PrintLevel(lvl),"RTL",fmt,args);
  }
}

/// Initializing constructor
Online::OutputLogger::OutputLogger(const std::string& nam, Context& ctxt)
  : Component(nam, ctxt), Context::Logger(outputLevel, ctxt)
{
  declareProperty("Format",        m_fmt = "%TIME%LEVEL %-16SOURCE");
  declareProperty("LogDeviceType", m_logDeviceType="RTL::Logger::LogDevice");
  declareProperty("LogPath",       m_logDevice="");
  declareProperty("LockedOutput",  s_locked_printouts);
  if( RTL::Logger::LogDevice::existsGlobalDevice() )
    m_logDev = RTL::Logger::LogDevice::getGlobalDevice();
  else
    m_logDev = std::make_shared<RTL::Logger::LogDevice>();
  /// Configure dd4hep printer
  setPrinter2(this,__print_func);
  /// Configure the rtl printer
  ::lib_rtl_install_printer(__rtl_print_func, this);
}

/// Default destructor
Online::OutputLogger::~OutputLogger()    {
  setPrinter2(0,0);
  ::lib_rtl_install_printer(0,0);
  m_logDev.reset();
}

/// Initialize the data flow component. Default implementation is empty.
int Online::OutputLogger::initialize()  {
  int sc = DataflowComponent::initialize();
  /// Configure the dd4hep printer properties
  /// Configure output
  if( RTL::Logger::LogDevice::existsGlobalDevice() )
    m_logDev = RTL::Logger::LogDevice::getGlobalDevice();
  else if( m_logDeviceType == "RTL::Logger::LogDevice" )
    m_logDev = std::make_shared<RTL::Logger::LogDevice>();
  else if( m_logDeviceType == "RTL::FmcLogDevice" && !m_logDevice.empty() )
    m_logDev = std::make_shared<RTL::FmcLogDevice>(m_logDevice);
  else if( m_logDeviceType == "RTL::FmcLogDevice" && std::getenv("LOGFIFO") != 0 )
    m_logDev = std::make_shared<RTL::FmcLogDevice>(std::getenv("LOGFIFO"));
  else 
    m_logDev = std::make_shared<RTL::FmcLogDevice>();
  m_logDev->setGlobalDevice(m_logDev, outputLevel);
  m_logDev->compileFormat(m_fmt);
  setPrintLevel(DEBUG);
  return sc;
}

/// Finalize the data flow component. Default implementation is empty.
int Online::OutputLogger::finalize()  {
  return DF_SUCCESS;
}

/// Write output .....
std::size_t
Online::OutputLogger::print(int severity, const char* source, const char* fmt, ...)   {
  if( severity >= this->outputLevel )   {
    va_list args;
    va_start(args, fmt);
    return print(severity, source, fmt, args);
  }
  return 0;
}

/// Write output .....
std::size_t
Online::OutputLogger::print(int severity, const char* source, const char* fmt, va_list& args)   {
  if( severity >= this->outputLevel )   {
    if( s_locked_printouts )  {
      std::lock_guard<std::mutex> lk(output_lock);
      return m_logDev->printout(severity, source, fmt, args);
    }
    return m_logDev->printout(severity, source, fmt, args);
  }
  va_end(args);
  return 0;
}

