//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  Plugins.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include <Dataflow/Plugins.h>
#include <DD4hep/Printout.h>

namespace Online {  namespace Plugins {
    /// Wrapper around DD4hep plugin mechanism to create data flow components
    DataflowComponent* createComponent(const std::string& id, DataflowContext& ctxt)   {
      std::pair<std::string,std::string> n(id,id);
      std::size_t idx = id.rfind('/');
      if ( idx != std::string::npos ) n = std::make_pair(id.substr(0,idx), id.substr(idx+1));
      DataflowComponent* object = dd4hep::PluginService::Create<DataflowComponent*>(n.first,n.second.c_str(),&ctxt);
      if ( !object ) {
	dd4hep::PluginDebug dbg;
	object = dd4hep::PluginService::Create<DataflowComponent*>(n.first,n.second.c_str(),&ctxt);
	if ( !object )  {
	  dd4hep::except("Dataflow","Failed to create component %s [Fatal]",id.c_str());
	}
      }
      return object;
    }
  }
}
