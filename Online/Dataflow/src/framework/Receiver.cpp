//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  Receiver.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include <Dataflow/Receiver.h>
#include <Dataflow/MBMClient.h>
#include <Dataflow/Incidents.h>
#include <EventData/event_header_t.h>
#include <PCIE40Data/pcie40.h>
#include <WT/wtdef.h>
#include <RTL/rtl.h>

// C/C++ include files
#include <cstring>
#include <memory>

namespace {
  void _doSleep(int errorDelay, bool* recvEvents)  {
    while(errorDelay>0 )  {
      if( !(*recvEvents) ) break;
      errorDelay-= 10;
    }
  }
}

std::unique_ptr<Online::Receiver::RecvEntry> Online::Receiver::RecvEntry::clone()  const   {
  auto e = std::make_unique<RecvEntry>(this->handler, this->name, this->service, this->identifier);
  e->size = 0;
  return e;
}

/// Initializing constructor
Online::Receiver::Receiver(const std::string& nam, Context& ctxt) : Component(nam, ctxt)
{
  ::wtc_init();
  declareProperty("Buffer",              m_buffer);
  declareProperty("UseEventRequests",    m_useRequests   = false);
  declareProperty("DeclareAsynchonously",m_declareAsynch = false);
  declareProperty("RoutingMask",         m_routingMask   = 0);
  declareProperty("VetoMask",            m_vetoMask      = 0);
  declareProperty("ErrorDelay",          m_errorDelay    = 100);
}

/// Initialize the MBM server
int Online::Receiver::initialize()  {
  int sc = Component::initialize();
  if( DF_SUCCESS == sc )  {
    try {
      m_recvEvents = true;
      declareMonitor("Events", "IN",  m_recvReq=0, "Total number of events received.");
      declareMonitor("Events", "OUT", m_evtDecl=0, "Total number of events declared to MBM.");
      declareMonitor("ErrorsIn",    m_recvError=0, "Total number of receive errors.");
      declareMonitor("BytesIn",     m_recvBytes=0, "Total number of bytes received from clients.");
      declareMonitor("Backlog",     m_backlog,     "Total number of unprocessed events in the backlog.");
      if( 0 == context.mbm )
	return error("Failed to access buffer manager service.");
      else if( DF_SUCCESS != (sc=subscribeNetwork()) )
	return sc;
      m_prod = context.mbm->createProducer(m_buffer,context.mbm->processName());
      ::wtc_subscribe(WT_FACILITY_DAQ_EVENT,0,rearm_net_request,this);
      return subscribeIncident("DAQ_CANCEL");
    }
    catch(const std::exception& e)  {
      std::error_code err = std::make_error_code(std::errc(errno));
      return error(e, "Failed to initialize ["+err.message()+"]");
    }
    catch(...)  {
      return error("Unknown exception during initialization.");
    }
  }
  return sc;
}

/// Start the data flow component. Default implementation is empty.
int Online::Receiver::start()   {
  m_recvEvents = true;
  m_recvReq = 0;
  m_recvError = 0;
  m_recvBytes = 0;
  if( m_prod ) m_prod->clear();
  return Component::start();
}

/// Stop the data flow component. Default implementation is empty.
int Online::Receiver::stop()   {
  m_recvEvents = false;
  info("Handled %ld input events. Output: %ld, errors: %ld events.",
       m_recvReq, m_evtDecl, m_recvError);
  return Component::stop();
}

/// Finalize the MBM server
int Online::Receiver::finalize()  {
  m_recvEvents = false;
  unsubscribeIncidents();
  ::wtc_flush(WT_FACILITY_DAQ_EVENT);
  ::wtc_remove(WT_FACILITY_DAQ_EVENT);
  unsubscribeNetwork(); // Cannot really do anything if this fails...except printing
  for(auto& i : m_receivers)
    deleteNetRequest(i);
  m_receivers.clear();
  detail::deletePtr(m_prod);
  undeclareMonitors("Events");
  undeclareMonitors();
  return Component::finalize();
}

/// Incident handler implemenentation: Inform that a new incident has occured
void Online::Receiver::handle(const DataflowIncident& inc)    {
  if( inc.type == "DAQ_CANCEL" )  {
    m_recvEvents = false;
    info("Executing DAQ_CANCEL");
    ::wtc_flush(WT_FACILITY_DAQ_EVENT);
    try  {
      cancelNetwork();
      int count = 100;
      while( --count >= 0 )  {
	if( m_prodLock.try_lock() )  {
	  context.mbm->cancelBuffers();
	  m_prodLock.unlock();
	  break;
	}
	::lib_rtl_sleep(20);
      }
      // This only happens if the event loop is stuck somewhere.
      if( count < 0 ) context.mbm->cancelBuffers();
    }
    catch(const std::exception& e)   {
      error(e,"Failed to handle dataflow incident:  %s.", inc.type.c_str());
    }
    catch(...)   {
      error("Failed to handle dataflow incident:  %s.", inc.type.c_str());
    }
  }
}

/// WT callback to declare asynchronously event data to MBM
int Online::Receiver::rearm_net_request(unsigned int,void* param)  {
  std::unique_ptr<RecvEntry> h((RecvEntry*)param);
  h->handler->declareData(*h, nullptr, std::move(h->buffer), h->size);
  return WT_SUCCESS;
}

/// Rearm request handling
std::pair<int, const Online::Receiver::RecvEntry*>
Online::Receiver::handleRequest(int clientID,const std::string& source, const std::string& svc)   {
  try {
    const RecvEntry* entry = receiver(source);
    if( nullptr == entry )  {
      m_receivers.push_back(RecvEntry(this,source,svc,clientID));
      entry = &m_receivers.back();
      if( DF_SUCCESS == createNetRequest(*entry) )  {
	debug("Added new datasource [%d]: host:%s [host:%s]",clientID,source.c_str(),svc.c_str());
        return {rearmRequest(*entry), entry};
      }
    }
    info("Unsolicited datasource request from: [%d] host:%s [%s]",clientID,source.c_str(),svc.c_str());
    return {DF_SUCCESS, entry};
  }
  catch(const std::exception& e)   {
    return {error(e,"Failed processing data source request:  [%d] %s.",clientID,source.c_str()),nullptr};
  }
  catch(...)   {
    return {error("Unknown exception during data source request from  [%d] %s.",clientID,source.c_str()),nullptr};
  }
}

// Producer implementation: Request event from data source
const Online::Receiver::RecvEntry* Online::Receiver::receiver(const std::string& nam)  const {
  for( const auto& i : m_receivers )  {
    if( 0 == ::strcasecmp(nam.c_str(), i.name.c_str()) )  {
      return &i;
    }
  }
  return nullptr;
}

/// Rearm network request for a single event source
int Online::Receiver::rearmRequest(const RecvEntry& entry)   {
  int sc = DF_SUCCESS;
  if( m_useRequests )  {
    sc = rearmNetRequest(entry);
    if( !m_recvEvents ) sc = DF_SUCCESS;
  }
  return sc;
}

// Create network request for a single source
int Online::Receiver::createNetRequest(const RecvEntry& /* entry */)  {
  return DF_SUCCESS;
}

// Reset event request
int Online::Receiver::resetNetRequest(const RecvEntry& /* entry */)  {
  return DF_SUCCESS;
}

// Rearm network request for a single source
void Online::Receiver::deleteNetRequest(RecvEntry& /* entry */)  {
}

/// Set MBM masks to producer declare data
void Online::Receiver::setMbmMask(MBM::EventDesc& e)   const {
  auto* hd  = reinterpret_cast<event_header_t*>(e.data);
  auto* mep = reinterpret_cast<pcie40::mep_header_t*>(e.data);
  e.mask[3] = ~0x0;
  if( mep->is_valid() )   {
    e.mask[0] = ~0x0;
    e.mask[1] = ~0x0;
    e.mask[2] = ~0x0;
    e.type = EVENT_TYPE_MEP;
    context.manager.setRunNumber(0);
  }
  else if( hd->is_mdf() && e.len > hd->size0() )   {
    /// Need to set all mask values to not block sub-events
    e.mask[0] = ~0x0;
    e.mask[1] = ~0x0;
    e.mask[2] = ~0x0;
    e.type = EVENT_TYPE_EVENT;  // burst type not used anymore!
    context.manager.setRunNumber(hd->subHeader().H1->runNumber());
  }
  else if( hd->is_mdf() && e.len == hd->size0() )   {
    auto m = hd->subHeader().H1->triggerMask();
    e.mask[0] = m[0];
    e.mask[1] = m[1];
    e.mask[2] = m[2];
    e.mask[3] = m[3];
    e.type = EVENT_TYPE_EVENT;
    context.manager.setRunNumber(hd->subHeader().H1->runNumber());
  }
  if( 0 != m_routingMask )  {
    e.mask[3] = ~0x0;
    e.mask[3] &= ~m_vetoMask;
    e.mask[3] &= m_routingMask;
  }
}

/// Reset event request and insert entry into data queue of the buffer manager
int Online::Receiver::handleData(const RecvEntry& client,
				 void* ctxt,
				 std::unique_ptr<char []>&& buffer,
				 std::size_t len)   {
  try  {
    // Need to clone entry - new message may be arriving before this one is handled...
    // (In mode without explicit rearm)
    if( m_declareAsynch )  {
      auto e = client.clone();
      e->buffer = std::move(buffer);
      e->size = len;
      ::wtc_insert(WT_FACILITY_DAQ_EVENT,e.release());
      int backlog = ++m_backlog;
      if( backlog%100 == 0 ) {
	if( m_lastbacklog != backlog ) {
	  m_lastbacklog = backlog;
	  error("Message backlog is now %d messages.",backlog);
	}
      }
      return DF_SUCCESS;
    }
    /// Declare data synchronously
    return this->declareData(client, ctxt, std::move(buffer), len);
  }
  catch(std::exception& exc)   {
    ++m_recvError;
    return error(exc,"While processing event receive request of %s.",client.name.c_str());
  }
  catch(...)   {
    ++m_recvError;
    return error("Unknown exception during event receive request of %s.",client.name.c_str());
  }
  return DF_ERROR;
}

// Copy event data into buffer manager
int Online::Receiver::copyEventData(void* /* ctxt */, void* to, 
			    const std::unique_ptr<char []>& buffer,
			    std::size_t len)
{
  ::memcpy(to, buffer.get(), len);
  return DF_SUCCESS;
}

// Handle event declaration into the buffer manager
int Online::Receiver::declareData(const RecvEntry& client, void* ctxt,
			  std::unique_ptr<char []>&& buffer,
			  std::size_t len)    {
  int ret = MBM_REQ_CANCEL;
  try {
    /// We have to protect the MBM interactions. SInce we have only ONE MBM inclusion,
    /// with multi-threaded ASIO receiving there might be multiple receives interleaving.
    std::lock_guard<std::mutex> lck (m_prodLock);
    ret = m_recvEvents ? m_prod->getSpace(len) : MBM_REQ_CANCEL;
    MBM::EventDesc& e = m_prod->event();
    if( MBM_NORMAL == ret ) {
      e.type = EVENT_TYPE_EVENT;
      e.len  = len;
      ret = copyEventData(ctxt, e.data, buffer, len) == DF_SUCCESS ? MBM_NORMAL : MBM_ERROR;
      if( MBM_NORMAL == ret )  {
	setMbmMask(e);
	ret = m_prod->sendEvent();
	if( MBM_NORMAL != ret )   {
	  error("Failed to declare network event data from: %s.",client.name.c_str());
	}
      }
    }
    else if( m_recvEvents )  {
      error("Failed to get space from buffer manager.");
    }
    if( MBM_NORMAL == ret )   {
      --m_backlog;
      ++m_recvReq;
      ++m_evtDecl;
      m_recvBytes += len;
      return m_recvEvents ? rearmRequest(client) : DF_SUCCESS;
    }
    if( m_recvEvents )   {
      if( ret != MBM_REQ_CANCEL ) rearmRequest(client);
      ++m_recvError;
    }
    _doSleep(m_errorDelay, &m_recvEvents);
    return DF_SUCCESS;
  }
  catch( std::exception& e )  {
    error(e,"While declaring event from: %s.",client.name.c_str());
  }
  catch( ... )  {
    error("Unknown exception when declaring event from: %s.",client.name.c_str());
  }
  ++m_recvError;
  _doSleep(m_errorDelay, &m_recvEvents);
  return m_recvEvents ? rearmRequest(client) : DF_SUCCESS;
}

// Callback on task dead notification
int Online::Receiver::taskDead(const std::string& task_name)  {
  if( m_recvEvents ) {
    info("Dead task: %s ...",task_name.c_str());
  }
  for(Receivers::iterator i=m_receivers.begin(); i!=m_receivers.end(); ++i)  {
    if( 0 == strcasecmp(task_name.c_str(),(*i).name.c_str()) )  {
      m_receivers.erase(i);
      return DF_SUCCESS;
    }
  }
  return DF_SUCCESS;
}
