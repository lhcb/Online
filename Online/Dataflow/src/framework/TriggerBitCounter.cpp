//  ============================================================
//
//  FileWriterSvc.cpp
//  ------------------------------------------------------------
//
//  Package   : GaudiOnline
//
//  Author    : Markus Frank
//
//  ===========================================================
#include <Dataflow/TriggerBitCounter.h>

Online::TriggerBitCounter::TriggerBitCounter(const std::string& nam, Context& ctxt)
  : Processor(nam, ctxt)
{
  declareProperty("BackupFile",m_BackupFile="LumiCounter1.file");
  declareProperty("BackupDirectory",m_BackupDirectory="/localdisk/CounterFiles/");
  clearCounters();
}

// Standard Destructor
Online::TriggerBitCounter::~TriggerBitCounter()  {
}

int Online::TriggerBitCounter::initialize()  {
  int sc = Processor::initialize();
  clearCounters();
  if( DF_SUCCESS == sc )   {
    declareMonitor("EvtsIn",  m_EvIn = 0, "Number of Events received.");
    return sc;
  }
  return error("Failed to initialize component base class.");
}

int Online::TriggerBitCounter::start()  {
  clearCounters();
  return Processor::start();
}

void Online::TriggerBitCounter::clearCounters()  {
  m_EvIn = 0;
}

int Online::TriggerBitCounter::stop()  {
  return Processor::stop();
}

int Online::TriggerBitCounter::finalize()  {
  clearCounters();
  return Processor::finalize();
}
