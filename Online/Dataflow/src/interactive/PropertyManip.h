//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  PropertyManip.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_PROPERTYMANIP_H
#define ONLINE_DATAFLOW_PROPERTYMANIP_H

// Framework include files
#include <Dataflow/CommandStructure.h>
#include <Dataflow/DataflowComponent.h>
#include <CPP/ObjectProperty.h>
#include <RPC/DimServer.h>

// C/C++ include files
#include <memory>
#include <mutex>

///  Online namespace declaration
namespace Online  {

  // Forward declarations
  class CommandStructure;

  /// PropertyManip component to be applied in the manager propertymanip
  /**
   * PropertyManip is a component, which allows to manipulate object 
   * properties in all components attached to the current process.
   * The manipulation is initiated by a an external stimulus
   * such as a DIM message etc.
   *
   * The component also allows to publish block information of the 
   * current process such as properties and monitoring entities.
   *
   * @author  Markus Frank
   * @version 1.0
   */
  class PropertyManip : public DataflowComponent  {
  protected:
    /// Definition of the clients collection type
    typedef std::vector<std::string>          Clients;
    /// Definition of the property collection type
    typedef std::vector<rpc::ObjectProperty>  Properties;
 
    /// Property: transition at which the publishing should start: "initialize" or "start"
    std::string  when;
    /// Property: string containing all interface names
    std::string  interfaces;

    /// Data buffer for the property block
    std::vector<char> propertyData;
    /// Data buffer for the command answer block
    std::string  cmdResult;
    /// Lock 
    std::mutex   lock;
    /// Dim service ID for the properties publication block
    int          dimSvcID = 0;
    /// Dim service ID for the command service
    int          dimCmdID = 0;
    /// Dim buffer allocation reservation
    int          dimMemSize = 1024*1024*2;
    /// Reference to the RPC object(s): /RPC2
    std::unique_ptr<rpc::DimServer> rpc2  {  };
    /// Reference to the RPC object(s): /XMLRPC
    std::unique_ptr<rpc::DimServer> xmlrpc  {  };
    /// Reference to the RPC object(s): /JSONRPC
    std::unique_ptr<rpc::DimServer> jsonrpc  {  };

  protected:
    /// Start all DIM publishing services
    void startServices();
    /// Start all DIM publishing services
    void stopServices();

    /// DIM callback to handle commands
    static void processRequest(void* tag, void* address, int* size);
    /// DIM callback to publish properties
    static void feedProperties(void* tag, void** address, int* size, int*);

    /// Handle command
    void handleCommand(char* data, size_t len);
    /// Handle manipulation of properties for WinCCOA
    void WinCCOASetProperties(char* address, size_t len);

    /// Publish property collection to DIM in WinCCOA format
    void publishProperties();

  public:
    /// Initializing constructor
    PropertyManip(const std::string& name, Context& ctxt);

    /// Default destructor
    virtual ~PropertyManip();

    /// Initialize the property publisher
    virtual int initialize()  override;

    /// Start the property publisher
    virtual int start()  override;

    /// Stop the property publisher
    virtual int stop()  override;

    /// Finalize the property publisher
    virtual int finalize()  override;

    /// Incident handler callback: Inform that a new incident has occured
    virtual void handle(const DataflowIncident& incident)  override;

    /// Update a given component property from the string representation
    virtual int setProperty(Property& property, const std::string& value);

    /// Update a given component property from the string representation
    virtual int setProperty(Component* component, const std::string& property, const std::string& value);

    /// Update a given component property from the string representation
    virtual int setPropertyEx(const std::string& component, const std::string& property, const std::string& value);

    /// Collect all known components
    virtual std::vector<Component*> components()  const;

    /// Collect all known components, order them by name
    virtual std::map<std::string,Component*> ordered_components()  const;

    /// Forced update of the properties
    virtual int updateProperties();

    /** UI interface  */
    /// Access the hosted client services
    Clients    clients()  const;
    /// Access all properties of an object
    Properties allProperties()  const;
    /// Access all properties with the same name from all clients
    Properties namedProperties(const std::string& name)  const;
    /// Access all properties of one remote client (service, ect.)
    Properties clientProperties(const std::string& client)  const;
    /// Access a single property of an object
    rpc::ObjectProperty property(const std::string& client,
				 const std::string& name)  const;
    /// Modify one single property
    void setProperty(const std::string& client,
		     const std::string& name,
		     const std::string& value);
    /// Modify one single property
    void setPropertyObject(const rpc::ObjectProperty& property);
    /// Modify a whole bunch of properties. Call returns the number of changes
    int  setProperties(const Properties& props);
    /// Modify all properties in allclients matching the name
    int  setPropertiesByName(const std::string& name,
			     const std::string& value);
    /// Invoke the interpreter and interprete the command line
    int interpreteCommand(const std::string& cmd)  const;
    /// Invoke the interpreter and interprete the command lines
    int interpreteCommands(const std::vector<std::string>& cmd)  const;
  };
}      // end namespace Online
#endif //  ONLINE_DATAFLOW_PROPERTYMANIP_H
