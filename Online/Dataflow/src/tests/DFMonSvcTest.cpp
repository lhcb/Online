/*
 * MonSvcTest.cpp
 *
 *  Created on: Jun 13, 2019
 *      Author: beat
 */
#include <Dataflow/DataflowComponent.h>
#include <Dataflow/Incidents.h>
#include <Dataflow/Plugins.h>
#include <RTL/rtl.h>
#include <TH1D.h>

#include <typeinfo>
#include <string>
#include <thread>
#include <atomic>
#include <map>
#include <set>

namespace Online  {
  class DFMonSvcTest;
  void runng(DFMonSvcTest *tis);
  using namespace std;
  using namespace Online;
  class DFMonSvcTest: public DataflowComponent  {
    public:
      int m_count = 0;
      int m_count1 = 0;
      double m_double;
      double m_darray[100];
      int m_iarray[100];
      unsigned int m_uiarray[100];
      unsigned long m_ularray[100];
      DataflowContext::Monitor *m_mons;
//      IHistogramSvc *m_hsvc;
      TH1D *m_h1;
      TH1D *m_h2;
      bool m_stop;
      float m_float;
      thread *m_thread = 0;
      int m_One;
      string m_SaveDir;
      bool m_SaveHists;
      int m_savePeriode;
      int m_count2 = 0;
      atomic<int> m_aint1;
      atomic<int> m_aint2;
      atomic<long> m_along1;
      atomic<long> m_along2;
      DFMonSvcTest(const string& nam, Context& ctxt) :
          DataflowComponent(nam, ctxt)
      {
        m_count = 0;
        m_double = 0.0;
        m_mons = ctxt.monitor;
        m_h1 = m_h2 = 0;
//        m_hsvc = 0;
        m_stop = false;
        m_float = 0.0;
        for (int i = 0; i < 100; i++)
        {
          m_darray[i] = 0.0;
          m_iarray[i] = 0;
        }
        m_One = 1;
        declareProperty("SaveDir", m_SaveDir = "/home/beat/hists");
        declareProperty("Save", m_SaveHists = false);
        declareProperty("SaveIntval", m_savePeriode = 60);

      }
      ~DFMonSvcTest()
      {

      }
      int initialize()  override
      {
//        IIncidentSvc *m_incidentSvc;
        DataflowComponent::initialize();
//        sc = serviceLocator()->service("MonitorSvc", m_mons, false);
//        sc = serviceLocator()->service("HistogramDataSvc", m_hsvc, true);
//        sc = serviceLocator()->service("IncidentSvc", m_incidentSvc, true);
//        if (!sc.isSuccess())
//        {
//          return sc;
//        }
        m_count1 = -5;
        m_count2 = 6;
        const string inc = "APP_RUNNING";
        subscribeIncident("APP_RUNNING");
        subscribeIncident("APP_STOPPED");
//        m_incidentSvc->addListener(this, "APP_RUNNING");
//        m_incidentSvc->addListener(this, "APP_STOPPED");
        m_h1 = new TH1D("Hist1", "a Histogram1", 100, 0.0, 100.0);
        m_h2 = new TH1D("Hist2", "a Histogram2", 100, 0.0, 100.0);
        declareMonitor("NumTask", m_One, "Number of Tasks");
        declareMonitor("Integer1", m_count, "An Integer");
        declareMonitor("Integer2", m_count, "An Integer");
        declareMonitor("Integer3", m_count, "An Integer");
        declareMonitor("Integer4", m_count, "An Integer");
        declareMonitor("Float1", m_float, "A Float");
        declareMonitor("Double1", m_double, "A Double");
        declareMonitor("Double2", m_double, "A Double");
        declareMonitor("Double3", m_double, "A Double");
        declareMonitor("Double4", m_double, "A Double");
        declareMonitor("DArray", "d", m_darray, sizeof(m_darray),
            "A Double Array");
        declareMonitor("IArray", "i", m_iarray, sizeof(m_iarray),
            "An Integer Array ");
        declareMonitor("UIArray", "i", m_uiarray, sizeof(m_uiarray),
            "An Unsigned Integer Array ");
        declareMonitor("ULArray", "i", m_ularray, sizeof(m_ularray),
            "An Unsigned Long Array ");
        declareMonitor("Hist1",  *m_h1,
            "A Histogram1");
        declareMonitor("Hist2", *m_h2,
            "A Histogram1");
        declareMonitor("Pair1", m_count1, m_count2, "A Pair1");
        declareMonitor("AtomicInt1", m_aint1, "A atomic int 1");
        declareMonitor("AtomicInt2", m_aint2, "A atomic int 2");
        declareMonitor("AtomicLong1", m_along1, "A atomic long 1");
        declareMonitor("AtomicLong", m_along2, "A atomic long 2");
        return DF_SUCCESS;
      }
      int start() override
      {
        m_stop = false;

        m_thread = new thread(runng, this);
        if (m_SaveHists)
        {
          string part = "Beat";
          string proc = RTL::processName();
//          m_mons->StartSaving(m_SaveDir, part, proc, m_savePeriode, 0);
        }
        m_thread->detach();
        DataflowComponent::start();
        return DF_SUCCESS;
      }
      int stop() override
      {
        DataflowComponent::stop();
        m_stop = true;
        return DF_SUCCESS;
      }
      int finalize() override
      {
        DataflowComponent::finalize();
        return DF_SUCCESS;
      }
      int running()
      {
        m_mons->setRunNo(1234);
        while (1)
        {
          m_double++;
          m_count++;
          for (int i = 0; i < 100; i++)
          {
            m_darray[i] = m_darray[i] + i;
            m_iarray[i] += i;
          }
          m_aint1++;
          m_along1++;
          usleep(500000);
          if (m_stop)
            return DF_SUCCESS;
        }
        return DF_SUCCESS;
      }
      void handle(const DataflowIncident& inc) override
      {
//        MsgStream log(msgSvc(), inc.name);
//        log << MSG::INFO << "MonSvcTest: Got incident from:" << inc..source()
//            << ": " << inc.type() << endmsg;
        if (inc.type == "APP_RUNNING")
        {
//        running().ignore();
        }
        else if (inc.type == "APP_STOPPED")
        {
        }
      }
  };
  void runng(DFMonSvcTest *tis)
  {
    tis->m_mons->setRunNo(1234);
    tis->running();
  }
}

DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online, Dataflow_DFMonSvcTest, DFMonSvcTest)

