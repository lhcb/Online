//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  Dataflow_Test_Manager.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include <Dataflow/DataflowManager.h>
#include <Dataflow/Incidents.h>
#include <Dataflow/Printout.h>
#include <RTL/rtl.h>

extern "C" int dataflow_test_manager(int argc, char** argv)  {
  using namespace Online;
  std::string opts, utgid = RTL::processName();
  RTL::CLI cli(argc,argv,[]() {});

  cli.getopt("options",3,opts);
  DataflowManager manager("Manager");
  int sc = manager.context.options->load(opts);
  if ( sc != DF_SUCCESS ) 
    return printout(FATAL,utgid,"Failed to load job options: %s [sc=%d]",opts.c_str(),sc);
  else if ( (sc=manager.configure()) != DF_SUCCESS )
    return printout(FATAL,utgid,"Failed to configure dataflow manager [sc=%d].",sc);
  else if ( (sc=manager.initialize()) != DF_SUCCESS )
    return printout(FATAL,utgid,"Failed to initialize dataflow manager [sc=%d].",sc);
  else if ( (sc=manager.start()) != DF_SUCCESS )
    return printout(FATAL,utgid,"Failed to start dataflow manager [sc=%d].",sc);
  else if ( (sc=manager.run()) != DF_SUCCESS )
    return printout(FATAL,utgid,"Failed to run dataflow manager [sc=%d].",sc);

  if ( (sc=manager.stop()) != DF_SUCCESS )
    printout(WARNING,utgid,"Failed to stop dataflow manager [sc=%d].",sc);
  if ( (sc=manager.finalize()) != DF_SUCCESS )
    printout(WARNING,utgid,"Failed to finalize dataflow manager [sc=%d].",sc);
  if ( (sc=manager.terminate()) != DF_SUCCESS )
    printout(WARNING,utgid,"Failed to terminate dataflow manager [sc=%d].",sc);
  return 1;
}
