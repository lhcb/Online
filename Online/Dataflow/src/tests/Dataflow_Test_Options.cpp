//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  Dataflow_Test1.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include <Dataflow/DataflowManager.h>
#include <Dataflow/Incidents.h>
#include <RTL/rtl.h>

namespace {

  class OptionsTest : public Online::DataflowComponent  {
  protected:
  public:
    /// Initializing constructor
    OptionsTest(const std::string& nam, Context& ctxt);
    /// Default destructor
    virtual ~OptionsTest() = default;
  };

}
extern "C" int dataflow_test_options(int argc, char** argv)  {
  RTL::CLI cli(argc,argv,[]() {});
  std::string opts;
  cli.getopt("options",3,opts);
  Online::DataflowManager manager("Manager");
  manager.context.options->load(opts);
  manager.setProperties();
  manager.printProperties();
  return 0;
}
