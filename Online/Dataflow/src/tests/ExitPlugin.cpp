//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  ExitPlugin.h
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_EXITPLUGIN_H
#define ONLINE_DATAFLOW_EXITPLUGIN_H

// Framework include files
#include <Dataflow/DataflowComponent.h>

///  Online namespace declaration
namespace Online  {

  /// Forward declarations
  class Bank;

  /// ExitPlugin component to be applied in the manager sequence
  /**
   *
   * @author  Markus Frank
   * @version 1.0
   */
  class ExitPlugin : public DataflowComponent  {
  protected:
    /// Property: Option to steer the exit.
    std::string      when;
    /// Property: forced exit or normal exit
    bool             forcedExit = false;
    /// Property: use pidfor printout (disable for QMtest!)
    bool             printPID = true;
    /// Exit the application
    void exitApplication();

  public:
    /// Initializing constructor
    ExitPlugin(const std::string& name, Context& ctxt);
    /// Default destructor
    virtual ~ExitPlugin();
    /// Initialize the numa controls component
    virtual int initialize()  override;
    /// Start the numa controls component
    virtual int start()  override;
    /// Stop the numa controls component
    virtual int stop()  override;
    /// Finalize the numa controls component
    virtual int finalize()  override;
  };
}      // end namespace Online
#endif //  ONLINE_DATAFLOW_EXITPLUGIN_H
//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  ExitPlugin.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include <Dataflow/Plugins.h>
#include <RTL/rtl.h>

using namespace std;
using namespace Online;

DECLARE_DATAFLOW_NAMED_COMPONENT(Dataflow_ExitPlugin,ExitPlugin)

/// Initializing constructor
ExitPlugin::ExitPlugin(const string& nam, Context& ctxt)
: Component(nam,ctxt)
{
  declareProperty("When",     when="start");
  declareProperty("Force",    forcedExit);
  declareProperty("PrintPID", printPID);
}

/// Default destructor
ExitPlugin::~ExitPlugin()   {
}

/// Apply NUMA configuration settings according to job options
void ExitPlugin::exitApplication()   {
  if ( printPID ) info("Exit process pid:%d",::lib_rtl_pid());
  if ( forcedExit ) ::_exit(0);
  ::exit(0);
}

/// Initialize the numa controls component
int ExitPlugin::initialize()  {
  int sc = Component::initialize();
  if ( when.find("initialize") != string::npos )
    exitApplication();
  return sc;
}

/// Start the numa controls component
int ExitPlugin::start()  {
  int sc = Component::start();
  if ( when.find("start") != string::npos )
    exitApplication();
  return sc;
}

/// Stop the numa controls component
int ExitPlugin::stop()  {
  int sc = Component::stop();
  if ( when.find("stop") != string::npos )
    exitApplication();
  return sc;
}

/// Finalize the numa controls component
int ExitPlugin::finalize()  {
  int sc = Component::finalize();
  if ( when.find("finalize") != string::npos )
    exitApplication();
  return sc;
}
