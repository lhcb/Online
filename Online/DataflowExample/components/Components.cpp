//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================

// Framework includes
#include <Dataflow/Plugins.h>

// Instantiation of a static factory class used by clients to create instances of this service
#include <DataflowExample/DataflowProducer.h>
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_ExampleProducer,DataflowProducer)
