//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================
#ifndef DATAFLOWEXAMPLE_DATAFLOWPRODUCER_H
#define DATAFLOWEXAMPLE_DATAFLOWPRODUCER_H

// Framework include files
#include <Dataflow/DataflowComponent.h>
#include <MBM/Producer.h>

/// C/C++ include files
#include <memory>
#include <mutex>

/// Forward declarations


///  Online namespace declaration
namespace Online  {

  /// Simple example of a dataflow component filling the buffer manager
  /** @class DataflowProducer DataflowProducer.h DataflowExample/DataflowProducer.h
   *
   *
   *  \author  Markus Frank
   *  \version 1.0
   */
  class DataflowProducer : public DataflowComponent  {
  protected:
    /// Lock handle
    std::mutex                     m_lock;
    std::mutex                     m_prodLock;
    /// Reference to buffer manager producer
    std::unique_ptr<MBM::Producer> m_producer;
    /// Property: Buffer name for data output
    std::string                    m_buffer;
    /// Monitoring: Number of frames declared to the buffer manager
    long                           m_eventsOUT = 0;
    /// Flag indicating that MBM event retrieval is active
    bool                           m_receiveEvts = false;

  public:
    /// Initializing constructor
    DataflowProducer(const std::string& name, Context& framework);
    /// Default destructor
    virtual ~DataflowProducer() = default;
    /// Service implementation: initialize the service
    virtual int initialize()  override;
    /// Service implementation: start of the service
    virtual int start()       override;
    /// Service implementation: stop the service
    virtual int stop()        override;
    /// Service implementation: finalize the service
    virtual int finalize()    override;

    /// Cancel I/O operations of the dataflow component
    virtual int cancel()  override;
    /// Stop gracefully execution of the dataflow component
    virtual int pause()   override;
    /// Incident handler implemenentation: Inform that a new incident has occured
    virtual void handle(const DataflowIncident& inc)  override;
    /// IRunable implementation : Run the class implementation
    virtual int run()     override;
  };
}      // end namespace Online
#endif // DATAFLOWEXAMPLE_DATAFLOWPRODUCER_H
