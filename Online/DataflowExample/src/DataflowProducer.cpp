//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Author     : M.Frank
//  Created    : 02/6/2020
//
//==========================================================================

// Framework include files
#include <DataflowExample/DataflowProducer.h>
#include <Dataflow/Incidents.h>
#include <Dataflow/MBMClient.h>
#include <RTL/rtl.h>

/// Standard Constructor
Online::DataflowProducer::DataflowProducer(const std::string& nam, DataflowContext& ctxt)
  : DataflowComponent(nam, ctxt)
{
  info("IN CONSTRUCTOR");	
  declareProperty("Buffer", m_buffer = "Events");
}

/// IService implementation: initialize the service
int Online::DataflowProducer::initialize()   {
  int sc = Component::initialize();
  
  info("IN INTIALIZE");

  info("m_buffer: %s \n",m_buffer.c_str());
  
  if ( sc != DF_SUCCESS )
  {
    return error("Failed to initialize service base class.");
  }
  else if ( !context.mbm )
  {
    return error("Failed to access MBM client.");
  }

  m_producer.reset(context.mbm->createProducer(m_buffer, RTL::processName()));
  if ( !m_producer.get() )  {
    return error("Fatal error: Failed to create MBM producer object.");
  }
  subscribeIncident("DAQ_PAUSE");
  subscribeIncident("DAQ_CANCEL");
  subscribeIncident("DAQ_STOP_TRIGGER");
  subscribeIncident("DAQ_START_TRIGGER");
  declareMonitor("Events","OUT", m_eventsOUT=0, "Number of events produced");
  m_lock.lock();
  return sc;
}

/// IService implementation: finalize the service
int Online::DataflowProducer::finalize()  {
  m_producer.reset();
  undeclareMonitors();
  undeclareMonitors("Events");
  return Component::finalize();
}

int Online::DataflowProducer::start()  {
  m_receiveEvts = true;
  m_lock.unlock();
  return DF_SUCCESS;
}

/// IService implementation: finalize the service
int Online::DataflowProducer::stop()   {
  m_receiveEvts = false;
  unsubscribeIncidents();
  if (m_receiveEvts)  {
    if (context.mbm)    {
      //context.mbm->cancelBuffers();
      int count = 100;
      while( --count >= 0 )  {
	if ( m_lock.try_lock() )  {
	  context.mbm->cancelBuffers();
	  m_lock.unlock();
	  break;
	}
	::lib_rtl_sleep(20);
      }
      // This only happens if the event loop is stuck somewhere.
      if ( count < 0 ) context.mbm->cancelBuffers();
    }
  }
  m_lock.unlock();
  return DF_SUCCESS;
}

/// Cancel I/O operations of the dataflow component
int Online::DataflowProducer::cancel()  {
  m_receiveEvts = false;
  m_lock.unlock();
  return DF_SUCCESS;
}

/// Stop gracefully execution of the dataflow component
int Online::DataflowProducer::pause()  {
  m_receiveEvts = false;
  if ( context.mbm )  {
    if (context.mbm)    {
      int count = 100;
      while( --count >= 0 )  {
	if ( m_prodLock.try_lock() )  {
	  context.mbm->cancelBuffers();
	  m_prodLock.unlock();
	  break;
	}
	::lib_rtl_sleep(20);
      }
      // This only happens if the event loop is stuck somewhere.
      if ( count < 0 ) context.mbm->cancelBuffers();
    }
    m_lock.unlock();
  }
  return DF_SUCCESS;
}

/// Incident handler implemenentation: Inform that a new incident has occured
void Online::DataflowProducer::handle(const DataflowIncident& inc)
{
  info("Got incident: %s of type %s", inc.name.c_str(), inc.type.c_str());
  if (inc.type == "DAQ_CANCEL" )  {
  }
  else if ( inc.type == "DAQ_PAUSE" )  {
    m_receiveEvts = false;
  }
  else if (inc.type == "DAQ_ENABLE")  {
    m_receiveEvts = true;
  }
  else if (inc.type == "DAQ_STOP_TRIGGER")  {
    m_receiveEvts = false;
  }
  else if (inc.type == "DAQ_START_TRIGGER")  {
    m_receiveEvts = true;
  }
}

/// IRunable implementation : Run the class implementation
int Online::DataflowProducer::run()  {
  int event_number = 0;

  m_receiveEvts = true;
  while (1)  {
    // loop over the events
    while ( m_receiveEvts )   {
      int status;
      int buf_size = 1024*sizeof(int);
      ++event_number;
      try   {
	status = m_producer->spaceRearm(buf_size);
      }
      catch (const std::exception& e)        {
	error("Exception while allocating space: %s Event:%d BuffSize:%d. ",
	      e.what(),event_number,buf_size);
	continue;
      }
      if (status == MBM_NORMAL)        {
	MBM::EventDesc& dsc = m_producer->event();
	int* ptr = (int*)dsc.data;
	dsc.len  = buf_size;
	dsc.type = EVENT_TYPE_EVENT;
	// If MEP, we emulate a trigger mask with the partition ID
	dsc.mask[0] = context.mbm->partitionID();
	dsc.mask[1] = ~0x0;
	dsc.mask[2] = ~0x0;
	dsc.mask[3] = ~0x0;
	for(size_t i=0; i<1024; ++i, ++ptr)
	  *ptr = i;

	/// Commit data to buffer manager
	{
	  std::lock_guard<std::mutex> lck (m_prodLock);
	  //
	  // Declare the event to the buffer manager
	  //
	  try {
	    status = m_producer->declareEvent();
	  }
	  catch (const std::exception& e)        {
	    info("Exception while delareEvent: %s Event:%d",e.what(),event_number);
	    status = MBM_ERROR;
	  }
	  catch(...)   {
	    info("UNKNOWN Exception while delareEvent: Event:%d",event_number);
	    status = MBM_ERROR;
	  }
	  if (status != MBM_NORMAL)    {
	    continue;
	  }
	  //
	  // Now send space
	  //
	  try  {
	    status = m_producer->sendSpace();
	  }
	  catch (const std::exception& e)        {
	    info("Exception while sendSpace: %s Event:%d",e.what(),event_number);
	    status = MBM_ERROR;
	  }
	  catch(...)   {
	    info("UNKNOWN Exception while sendSpace: Event:%d",event_number);
	    status = MBM_ERROR;
	  }
	}
	if (status != MBM_NORMAL)    {
	  continue;
	}
      }
    }
  }
  return DF_SUCCESS;
}
