#ifndef BU_H
#define BU_H 1

#include <memory>
#include <string>
#include <list>
#include <mutex>
#include <unordered_map>

#include "Dataflow/DataflowComponent.h"
#include "mbm_writer.hpp"
#include "MFP_tools.hpp"
#include "MEP_tools.hpp"
#include "transport_unit.hpp"
#include "Parallel_Comm.hpp"
#include "bw_mon.hpp"
#include "timer.hpp"
#include "buffer_interface.hpp"
#include "logger.hpp"
#include "file_writer.hpp"
#include <dim/dic.hxx>

namespace EB {
  using namespace Online;
  enum BU_buffer_types { dummy_MEP_buffer, shmem_MEP_buffer, MBM_MEP_buffer, MEP_injector_buffer };

  constexpr std::initializer_list<BU_buffer_types> all_BU_buffer_types =
    {dummy_MEP_buffer, shmem_MEP_buffer, MBM_MEP_buffer, MEP_injector_buffer};

  constexpr auto default_BU_buffer_type = BU_buffer_types::dummy_MEP_buffer;
  constexpr auto default_MBM_name = "Input";
  constexpr auto default_BU_buffer_size = 1;

  const std::string BU_buffer_type_to_string(BU_buffer_types type);

  std::ostream& operator<<(std::ostream& os, BU_buffer_types type);

  class BU : public Transport_unit {
  public:
    BU(const std::string& name, Context& framework);
    ~BU();

    /// Service implementation: initialize the service
    virtual int initialize() override;
    /// Service implementation: start of the service
    virtual int start() override;
    /// Service implementation: stop the service
    virtual int stop() override;
    /// Service implementation: finalize the service
    virtual int finalize() override;

    /// Cancel I/O operations of the dataflow component
    virtual int cancel() override;
    /// Stop gracefully execution of the dataflow component
    virtual int pause() override;
    /// Incident handler implemenentation: Inform that a new incident has occured
    virtual void handle(const DataflowIncident& inc) override;
    /// IRunable implementation : Run the class implementation
    virtual int run() override;

  private:
    int _my_idx;
    bool _receive_MEPs = false;
    bool _end_of_run = true;
    bool _old_end_of_run = false;

    // properties
    // per BU parameters
    // TODO continue implementing vector params
    // all the vectors are defied add length check and init value plus idx based config
    std::vector<int> _buffer_type;
    std::vector<int> _prop_buffer_sizes;
    std::vector<size_t> _buffer_sizes;
    int _prop_discard_buffer_size;
    std::vector<bool> _write_to_file;
    bool _write_to_file_single;
    bool _write_to_file_dim_enable;
    bool _write_to_file_per_run;
    std::string _MEP_dump_dim_service;
    std::vector<std::string> _mbm_name;
    // MEP injector properties
    std::string _MDF_filename;
    int _packing_factor;
    int _n_meps;
    // global parameters
    std::string _shmem_prefix;
    std::string _out_file_prefix;
    int _n_meps_to_file;
    int _stop_timeout;
    bool _sort_src_ids;

    // this may be included in Transport_unit
    // sizes of the current MFPs divided by RU and source
    std::vector<uint32_t> _sizes;
    // full size of the current MEP
    uint32_t _full_size_words;
    std::vector<EB::offset_type> _data_offset_words;
    // std::vector<MEP::src_id_type> _src_ids;
    std::vector<uint32_t> _dummy;
    bool _discarted;
    bool _incomplete;

    std::vector<EB::src_id_type> _src_ids;
    std::vector<EB::src_id_type> _sorted_src_ids;
    std::vector<std::string> _src_names;
    std::map<EB::src_id_type, std::string> _src_id_names_maps;
    std::vector<size_t> _src_id_idx_to_ru_src_idx;
    std::vector<size_t> _ru_src_idx_to_src_id_idx;

    std::vector<std::vector<int>> _shift_offset;

    int check_buffer();
    int config_buffer();
    int sort_src_ids();
    int map_src_id_names();
    int init_shift();
    int receive_sizes();
    int receive_src_ids();
    int receive_src_names();
    int linear_shift();
    int receive_MFPs(const std::vector<int>& shift_off_it);
    int get_next_MEP_space();
    int build_MEP_header();

    int connect_n_mep_info();

    void config_dummy();
    void config_shmem();
    void config_mbm();
    void config_MEP_injector();

    void reset_counters() override;
    void init_profiling() override;
    void update_profiling() override;

    // DEBUG file writer
    int reset_file_writer();
    bool is_file_writer_enable();
    bool is_meps_file_writer_continue();

    // monitoring counters
    uint64_t _MEP_count = 0;
    uint64_t _discarted_MEP_count = 0;
    uint64_t _incomplete_MEP_count = 0;
    uint64_t _corrupted_MEP_count = 0;
    std::unordered_map<src_id_type, int> _incomplete_MEP_srcs;

    // DF monitoring counters in the transport_unit class
    // net monitoring counters in the transport_unit class

    // DEBUG counters
    uint64_t _run_loop_iteration;

    // profiling counters
    double _receive_size_time_counter;
    double _calc_offsets_time_counter;
    double _build_header_time_counter;
    double _linear_shift_time_counter;
    double _receive_MFPs_time_counter;

    EB::Buffer_writer<EB::MEP>* _recv_buff;
    EB::Buffer_writer<EB::MEP>* _discard_buff;
    EB::MEP* _curr_MEP;

    Timer _receive_size_timer;
    Timer _calc_offsets_timer;
    Timer _build_header_timer;
    Timer _linear_shift_timer;
    Timer _receive_MFPs_timer;

    std::timed_mutex _loop_lock;
    std::timed_mutex _start_lock;

    // file writer
    File_writer<EB::MEP> _file_writer;
    int _n_meps_written_to_file;
    // DIM client for the file writer
    std::unique_ptr<DimInfo> _n_MEPS_info;
  };
} // namespace EB

#endif // BU_H
