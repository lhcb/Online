#ifndef MEP_TOOLS_H
#define MEP_TOOLS_H 1
#include <cstdint>
#include <cstdlib>
#include <iostream>
#include <iterator>
#include "MFP_tools.hpp"

namespace EB {
  typedef uint32_t offset_type;

  constexpr uint16_t MEP_magic = 0xCEFA;
  constexpr uint16_t MEP_wrap = 0xFFFF - MEP_magic;
  // size of a word in bytes (see p_words and offsets)
  constexpr size_t MEP_WORD_SIZE = (32 / 8);

  // integrity_check_error_values
  // value can be ORs in a bit mask
  enum MEP_integrity_check {
    MEP_OK = 0,
    MEP_magic_invalid = 1,
    MEP_ev_id_corruption = MEP_magic_invalid << 1,
    MEP_src_id_corruption = MEP_ev_id_corruption << 1,
  };

  struct __attribute__((__packed__)) MEP_header {
    // On valid packets 0xCEFA
    uint16_t magic;
    // Number of MFPs in this packet
    uint16_t n_MFPs;
    // Size of this packet in 32-bit words
    uint32_t p_words;

    // the return value of this methods is valid only id n_mps is set and they assume a contiguos memory layout
    // pointer to the src IDs array
    src_id_type* src_ids();
    const src_id_type* src_ids() const;
    // pointer to the src offsets array
    // The offset is the distance in 32-bit words between the n-th MFP in the packet and the start of this header
    offset_type* offsets();
    const offset_type* offsets() const;

    // header size in bytes
    size_t size() const;
    // header size in bytes including padding
    size_t mem_size() const;

    // print header fields
    std::string print() const;
  };

  // TODO find a better name for the namespace or the class the NS may be Online
  struct __attribute__((__packed__)) MEP {
    MEP_header header;

    // setter/getter for the validity of the MEP
    bool is_magic_valid() const;
    void set_magic_valid();

    // setter/getter for the wrap condition, this condition is used internally by the EB buffering. Wrap MEPs are not
    // valid and will should never go out of the EB.
    bool is_wrap() const;
    void set_wrap();

    // header size in bytes
    size_t header_size() const;
    // header size in bytes including padding
    size_t header_mem_size() const;
    // packet size in bytes
    size_t bytes() const;
    // sizes of the MFPs
    std::vector<size_t> get_mfp_sizes() const;
    // sizes of the events
    std::vector<size_t> get_ev_sizes() const;
    // sizes of the events
    std::vector<uint8_t> get_bx_types() const;

    // setter/getter for the validity of the MEP
    bool is_valid() const;
    MEP_integrity_check integrity_check() const;
    std::map<EB::ev_id_type, std::vector<EB::src_id_type>> get_ev_id_distribution() const;

    // pointer to the begining of the payload
    void* payload();
    const void* payload() const;

    EB::MFP* at(int n);
    const EB::MFP* at(int n) const;

    EB::MFP* operator[](int n);
    const EB::MFP* operator[](int n) const;

    // print header fields
    std::string print(bool MFP_header_only = true) const;

    // Iterator definition
    struct iterator {
      using iterator_category = std::forward_iterator_tag;
      using difference_type = std::ptrdiff_t;
      using value_type = EB::MFP;
      using pointer = EB::MFP*;
      using reference = EB::MFP&;

      // iterator(pointer mfp, offset_type* offset) : _offset(offset), _mfp(mfp) {}
      iterator(uintptr_t base_ptr, const offset_type* offset) : _offset(offset), _base_ptr(base_ptr) {}

      reference operator*() const;
      pointer operator->();
      iterator& operator++();

      iterator operator++(int);

      friend bool operator==(const iterator& a, const iterator& b);
      friend bool operator!=(const iterator& a, const iterator& b);

    private:
      const offset_type* _offset;
      uintptr_t _base_ptr;
      pointer _get_ptr() const;
    };

    // Const iterator definition
    struct const_iterator {
      using iterator_category = std::forward_iterator_tag;
      using difference_type = std::ptrdiff_t;
      using value_type = const EB::MFP;
      using pointer = const EB::MFP*;
      using reference = const EB::MFP&;

      const_iterator(uintptr_t base_ptr, const offset_type* offset) : _offset(offset), _base_ptr(base_ptr) {}

      reference operator*() const;
      pointer operator->();
      const_iterator& operator++();

      const_iterator operator++(int);
      friend bool operator==(const const_iterator& a, const const_iterator& b);
      friend bool operator!=(const const_iterator& a, const const_iterator& b);

    private:
      pointer _get_ptr() const;

      const offset_type* _offset;
      uintptr_t _base_ptr;
    };

    iterator begin() { return iterator(reinterpret_cast<uintptr_t>(this), header.offsets()); }
    const_iterator begin() const { return const_iterator(reinterpret_cast<uintptr_t>(this), header.offsets()); }
    // TODO this is a bit convoluted there is probably a better way of getting a valid end iterator
    iterator end() { return iterator(reinterpret_cast<uintptr_t>(this), header.offsets() + header.n_MFPs); }
    const_iterator end() const
    {
      return const_iterator(reinterpret_cast<uintptr_t>(this), header.offsets() + header.n_MFPs);
    }
    const_iterator cbegin() const { return const_iterator(reinterpret_cast<uintptr_t>(this), header.offsets()); }
    // TODO this is a bit convoluted there is probably a better way of getting a valid end iterator
    const_iterator cend() const
    {
      return const_iterator(reinterpret_cast<uintptr_t>(this), header.offsets() + header.n_MFPs);
    }
  };

  // calculates the size of a MEP header given the number of MFPs
  size_t mep_header_size(int n_MFPs);
  size_t mep_header_mem_size(int n_MFPs);

  // alignent in the mpf header in bytes
  constexpr size_t MEP_alignment = 4;

  bool operator==(const MEP::iterator& a, const MEP::iterator& b);
  bool operator!=(const MEP::iterator& a, const MEP::iterator& b);
  bool operator==(const MEP::const_iterator& a, const MEP::const_iterator& b);
  bool operator!=(const MEP::const_iterator& a, const MEP::const_iterator& b);

} // namespace EB
std::ostream& operator<<(std::ostream& os, const EB::MEP_header& header);
std::ostream& operator<<(std::ostream& os, const EB::MEP& mep);

#endif // MEP_TOOLS_H
