#ifndef FILE_MMAP
#define FILE_MMAP 1

#include <memory>
#include <sys/stat.h>
#include <fcntl.h>
#include <cstring>
#include <string>
#include <functional>
#include <sys/mman.h>
#include <unistd.h>

namespace EB {
  class File_mmap {
  public:
    File_mmap();
    File_mmap(const std::string& file_name);
    size_t size() const;
    size_t remaining_size() const;
    size_t read_size() const;
    std::string get_name() const;
    void* get_base() const;
    void* get_read() const;
    void* update_read(size_t size);
    void* reset_read();

  private:
    typedef std::shared_ptr<void> shared_ptr_t;
    std::string _file_name;
    shared_ptr_t _ptr;
    uintptr_t _read_ptr;
    size_t _size;
  };
} // namespace EB

#endif // FILE_MMAP