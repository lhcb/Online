#ifndef GENERIC_BLOCK_H
#define GENERIC_BLOCK_H 1

#include <array>
#include <cerrno>
#include <cstdint>
#include <cstdio>
#include <iostream>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdint.h>
#include <cstring>
#include <unordered_map>
#include <memory>

namespace EB {
  class Generic_block {
  private:
  protected:
    std::shared_ptr<char[]> _payload;
    // void* _payload;
    size_t _payload_alloc_size;
    // set to true if the payload buffer has been allocate by the object
    // bool free_payload;

    // static std::unordered_map<void*, int> ref_counter_map;

  public:
    Generic_block();
    Generic_block(std::shared_ptr<char[]> payload, size_t size);

    virtual ~Generic_block() {}

    std::shared_ptr<char[]> payload() const;

    void set_payload(std::shared_ptr<char[]> payload, size_t size);

    bool realloc_payload(size_t size);

    // virtual methods
    virtual size_t payload_size() const = 0;
  };
} // namespace EB

#endif // GENERIC_BLOCK_H
