#ifndef MDF_TOOLS_H
#define MDF_TOOLS_H 1

#include <array>
#include <cerrno>
#include <cstdint>
#include <cstdio>
#include <iostream>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdint.h>
#include "generic_block.hpp"
#include "tools.hpp"

namespace EB {
  struct __attribute__((__packed__)) generic_header {
    // MDF generic header field
    // size of the data block including the headers repeated 3 times for
    // redundancy
    uint32_t size[3] = {0, 0, 0};
    // base32 checksum 0 if not used
    uint32_t checksum = 0;
    // compression info 4 bits compression algorithm 4 bits compressio factor
    uint8_t compression_information = 0;
    // 4 bits header type 4 bit header length in 32 bit words
    uint8_t header_information = 0;
    // data type
    uint8_t data_type = 0;
    // spare field
    uint8_t spare = 0;

    bool is_valid() const { return (size[0] == size[1]) && (size[0] == size[2]); }
  };

  struct __attribute__((__packed__)) specific_header {
    // Trigger mask (if no mask is available, all bytes should be set to 0xFF)
    uint32_t trigger_mask[4] = {0, 0, 0, 0};
    // Run number
    uint32_t run = 0;
    // Orbit number
    uint32_t orbit = 0;
    // Bunch crossing ID
    uint32_t bxid = 0;
  };

  struct __attribute__((__packed__)) MDF_header {
    generic_header generic;
    specific_header specific;

    void print() const;
    // MDF_header(int fd, bool skip_payload = false);
  };

  class MDF_block : public Generic_block {
  public:
    MDF_header header;

    MDF_block();
    MDF_block(std::shared_ptr<char[]> payload, size_t size);
    MDF_block(int fd);
    MDF_block(int fd, std::shared_ptr<char[]> payload, size_t size);

    size_t payload_size() const override;

    // this function reads only the MDF header from the given fd
    // returns values
    // 0 success
    // EOF end of file
    // EINVAL truncated file or corrupted header
    int read_header(int fd);

    // this function reads the header and the paylod from the given
    // fd and saves the payload in an allocated buffer
    // if persist is not set the memory will freed by the denstructor
    // if persist is set the memory must be hanlded by the user
    // returns values
    // 0 success
    // EOF end of file
    // EINVAL truncated file or corrupted header
    int read_block(int fd);

    // this function reads the header and the paylod from the given
    // fd and saves the payload into the given buffer
    // the memory must be hanlded by the user
    // returns values
    // 0 success
    // EOF end of file
    // EINVAL truncated file or corrupted header
    int read_block(int fd, std::shared_ptr<char[]> buffer, size_t buffer_size);

    int write_block(int fd) const;
  };

} // namespace EB

#endif // MDF_TOOLS_H
