#ifndef TOOLS_H
#define TOOLS_H 1
#include <cerrno>
#include <cstdio>
#include <algorithm>
#include <cstdint>
#include <string>
#include <vector>
#include <numeric>
#include <stdexcept>
#include <sstream>
#include <type_traits>

#define ALLOC_CHECK(ptr)                                                            \
  {                                                                                 \
    if (ptr == NULL) {                                                              \
      fprintf(stderr, "[%s:%d] ERROR: %s \n", __FILE__, __LINE__, strerror(errno)); \
      exit(EXIT_FAILURE);                                                           \
    }                                                                               \
  }

namespace EB {
  inline size_t get_padding(const size_t base_size, const size_t alignment)
  {
    return (alignment - (base_size % alignment)) % alignment;
  }

  template<class Input_it, class T>
  inline int get_idx(Input_it first, Input_it last, const T& val)
  {
    int ret_val = -1;
    const Input_it val_it = std::find(first, last, val);
    if (val_it != last) {
      ret_val = std::distance(first, val_it);
    }
    return ret_val;
  }

  template<class T>
  inline size_t aligned_array_size(int len, const size_t alignment)
  {
    size_t base_size = len * sizeof(T);
    return base_size + get_padding(base_size, alignment);
  }

  template<class T>
  inline size_t aligned_array_end(T* base_ptr, int len, const size_t alignment)
  {
    size_t base_size = len * sizeof(T);
    size_t base_end = reinterpret_cast<uintptr_t>(base_ptr) % alignment + base_size;
    return base_size + get_padding(base_end, alignment);
  }

  timespec timespec_diff(timespec lhs, timespec rhs);

  timespec timespec_plus(timespec lhs, timespec rhs);

  clock_t timespec_to_ns(const timespec& lhs);

  clock_t timespec_diff_ns(const timespec& lhs, const timespec& rhs);
  clock_t timespec_plus_ns(const timespec& lhs, const timespec& rhs);

  std::string get_hostname_no_domain();

  int string_icompare(const std::string& lhs, const std::string& rhs);
  void string_toupper(std::string& str);
  void string_tolower(std::string& str);

  int get_idx_UTGID(const std::string& UTGID, const std::string& unit_name);
  int get_node_idx_UTGID(const std::string& UTGID);
  int get_node_idx_host(const std::string& host);
  std::string get_sub_detector_UTGID(const std::string& UTGID, const std::string& unit_name);
  int verify_UTGID(const std::string& UTGID, const std::string& unit_name, const std::string& hostname);

  std::vector<std::string> str_split(const std::string& string, const std::string& delim);

  // Finds all repeated elements in a sorted array and stores them in res, the return value is the new end iterator of
  // res
  template<class InIterator, class OutIterator>
  inline OutIterator find_all_rep(InIterator begin, InIterator end, OutIterator res)
  {
    InIterator it = begin;
    while ((it = std::adjacent_find(it, end)) != end) {
      *res = *it;
      res++;
      it++;
    }

    return res;
  }

  template<class InIterator, class OutIterator, class Predicate>
  inline OutIterator find_all_rep(InIterator begin, InIterator end, OutIterator res, Predicate pred)
  {
    InIterator it = begin;
    while ((it = std::adjacent_find(it, end, pred)) != end) {
      *res = *it;
      res++;
      it++;
    }

    return res;
  }

  template<class T>
  std::vector<size_t> sort_indices(const std::vector<T>& v)
  {
    std::vector<size_t> idx(v.size());
    std::iota(idx.begin(), idx.end(), 0);

    std::stable_sort(idx.begin(), idx.end(), [&v](size_t i1, size_t i2) { return v[i1] < v[i2]; });

    return idx;
  }

  template<class InputIt>
  std::vector<InputIt> sort_it(InputIt first, InputIt last)
  {
    std::vector<InputIt> sorted_it(std::distance(first, last));
    std::generate(sorted_it.begin(), sorted_it.end(), [begin = first]() mutable { return begin++; });

    std::stable_sort(sorted_it.begin(), sorted_it.end(), [](InputIt i1, InputIt i2) { return *i1 < *i2; });

    return sorted_it;
  }

  template<typename T>
  typename std::make_unsigned<T>::type safe_to_unsigned(const T& x)
  {
    typename std::make_unsigned<T>::type ret_val;
    if (x >= 0) {
      ret_val = x;
    } else {
      std::ostringstream err_mess;
      err_mess << "Invalid casting negative value " << x;
      throw std::runtime_error(err_mess.str());
    }

    return ret_val;
  }

} // namespace EB
#endif // TOOLS_H