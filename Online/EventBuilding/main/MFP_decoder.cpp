#include "EventBuilding/MFP_tools.hpp"
#include "EventBuilding/file_mmap.hpp"
#include "PCIE40Data/RawBank40.h"
#include "PCIE40Data/sodin.h"
#include "options.hpp"
#include <iostream>
#include <sys/stat.h>
#include <fcntl.h>
#include <cstring>
#include <sys/mman.h>

int main(int argc, char** argv)
{
  std::string file_name;
  bool verbose = false;
  bool SODIN_decode = false;
  void* read_ptr;
  EB::MFP* curr_MFP;
  EB::File_mmap file;
  EB::Options opt_parser(argc, argv);
  opt_parser.add_option("file_name", 'f', "MFP file name", true, file_name);
  opt_parser.add_flag("verbose", 'v', "Increase verbosity", false, verbose);
  opt_parser.add_flag("sodin", 's', "Decodes ODIN banks (if present)", false, SODIN_decode);
  if (!opt_parser.parse_args()) {
    opt_parser.print_usage();
    return -1;
  }

  try {
    file = EB::File_mmap(file_name);
  } catch (std::exception& e) {
    std::cerr << argv[0] << ": " << e.what() << std::endl;
    return errno;
  } catch (...) {
    std::cerr << argv[0] << ": unable to open file " << file_name << " unexpected exception" << std::endl;
    return -2;
  }

  read_ptr = file.get_read();
  while (file.update_read(sizeof(EB::MFP_header))) {
    // while((read_ptr + sizeof(EB::MFP_header)) - mmap_ptr < file_size){
    curr_MFP = reinterpret_cast<EB::MFP*>(read_ptr);
    // we already asked for the header size
    if (file.update_read(curr_MFP->bytes() - sizeof(EB::MFP_header)) == NULL) {
      std::cout << "End of file reached truncated MFP" << std::endl;
      break;
    }

    std::cout << curr_MFP->print(verbose) << std::endl;

    if (SODIN_decode) {
      if (EB::get_sys_id(curr_MFP->header.src_id) == static_cast<EB::src_id_type>(EB::sys_id::ODIN)) {
        for (auto bank_it = curr_MFP->begin(); bank_it != curr_MFP->end(); bank_it++) {
          std::cout << *(reinterpret_cast<Online::pcie40::sodin_t*>(&(*bank_it))) << std::endl;
          std::cout << "--------" << std::endl;
        }
      } else {
        std::cout << "No ODIN banks found" << std::endl;
      }
    }

    read_ptr = file.get_read();
  }

  return 0;
}