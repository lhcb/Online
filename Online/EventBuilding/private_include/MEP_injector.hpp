#ifndef MEP_INJECTOR_H
#define MEP_INJECTOR_H 1

#include "buffer_interface.hpp"
#include "dummy_mep_buffer_writer.hpp"
#include "mbm_writer.hpp"
#include "mdf_reader.hpp"
#include "EventBuilding/MEP_tools.hpp"
#include "file_writer.hpp"
#include <vector>
#include <memory>
#include <tuple>
#include <cassert>
#include <cstdint>
#include <memory>
#include <queue>
#include <string>
#include <atomic>
#include <thread>

namespace EB {
  class MEP_injector : public EB::Buffer_writer<EB::MEP> {
  public:
    // TODO we probably want to delete the default constructor
    MEP_injector() = default;
    MEP_injector(
      const char* MDF_filename,
      size_t buffer_size,
      int numa_node,
      int packing_factor,
      int n_meps,
      Online::DataflowComponent::Context& context,
      const std::string& instance,
      const std::string& name,
      bool write_to_file = false,
      const std::string out_file_name = "out.mep",
      int n_meps_to_file = 1);
    ~MEP_injector();

    // TODO src_id conversion
    // void set_detector_map(const std::vector<EB::type_id_pair_type>& src_map);

    std::vector<std::tuple<void*, size_t>> get_full_buffer() override;

    EB::MEP* try_write_next_element(size_t size) override;

    void write_complete() override;
    void write_discard() override;
    // The src id should be set by an external device or in the constructor of the derived class
    int get_src_id() const override;

  private:
    typedef Mbm_writer<EB::MEP> MBM_buff_type;

    int preload_MEPs();
    // based fragment size used for self buffer allocation
    // const size_t frag_size = 256;
    std::string _MDF_filename;
    Dummy_mep_buffer_writer _internal_buffer_writer;
    MBM_buff_type _external_buffer_writer;
    File_writer<EB::MEP> _file_writer;
    std::vector<std::shared_ptr<EB::MEP>> _MEPs;

    const size_t _frag_size = 1024;
    size_t _packing_factor;
    size_t _n_MEPs;
    std::queue<EB::MEP*> _write_ptrs;
    int _curr_mep_idx;
    int _numa_node;
    bool _write_to_file = false;
    std::string _out_file_name;
    int _n_meps_to_file;

    std::atomic<int> _copy_status;
    bool _thread_exit;
    std::thread _t_copy;
    void* _t_write_ptr;
    EB::MEP* _t_mep_ptr;

    static void thread_copy(bool* exit, std::atomic<int>* status, void** write_ptr, EB::MEP** mep_ptr);

    enum status_t { READY, PTR_READY, COPY_READY };
  };
} // namespace EB

#endif // MEP_INJECTOR_H
