#ifndef CIRCULAR_BUFFER_READER_H
#define CIRCULAR_BUFFER_READER_H 1

#include "buffer_interface.hpp"
#include "circular_buffer.hpp"
#include <cstddef>
#include <cstdint>
#include <deque>
#include <iostream>
#include <string>
#include <tuple>

namespace EB {
  template<class T, class B>
  class Circular_buffer_reader : public EB::Buffer_reader<T>, public Circular_buffer<B> {
  public:
    // alignment is explesser in 2^alignment min value 1
    Circular_buffer_reader(B&& backend) : Circular_buffer<B>(std::move(backend)) {}
    Circular_buffer_reader() : Circular_buffer<B>() {}
    virtual ~Circular_buffer_reader();

    std::vector<std::tuple<void*, size_t>> get_full_buffer() override;

    T* try_get_element() override;

    void read_complete() override;

    int get_src_id() const override;

    void flush() override;

    size_t get_buffer_size() const override;
    size_t get_buffer_occupancy() override;

    std::string get_name() const override;

  protected:
    void update_local_status();
  };
} // namespace EB

#include "circular_buffer_reader_impl.hpp"

#endif // CIRCULAR_BUFFER_READER_H
