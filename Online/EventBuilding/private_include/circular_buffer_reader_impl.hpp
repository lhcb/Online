#include "circular_buffer_reader.hpp"
#include <stdexcept>
#include "EventBuilding/tools.hpp"
#include <unistd.h>
#include <sys/stat.h> /* For mode constants */
#include <fcntl.h>    /* For O_* constants */
#include <sys/mman.h>
#include <cstring>
#include <sstream>
#include <string>
#include <memory>
#include <iostream>
#include <tuple>

template<class T, class B>
EB::Circular_buffer_reader<T, B>::~Circular_buffer_reader()
{}

template<class T, class B>
std::vector<std::tuple<void*, size_t>> EB::Circular_buffer_reader<T, B>::get_full_buffer()
{
  std::vector<std::tuple<void*, size_t>> ret_val;
  ret_val.emplace_back(this->_backend.get_buffer());
  return ret_val;
}

template<class T, class B>
void EB::Circular_buffer_reader<T, B>::update_local_status()
{
  if (!this->is_set()) {
    std::ostringstream err_mess;
    err_mess << __FILE__ << ": " << __FUNCTION__ << " on an invalid buffer";
    throw std::logic_error(err_mess.str());
  }
  this->_backend._local_buffer_status.write_stat = this->_backend._remote_buffer_status->write_stat;
}

template<class T, class B>
T* EB::Circular_buffer_reader<T, B>::try_get_element()
{
  T* ret_val = NULL;
  if (!this->is_set()) {
    std::ostringstream err_mess;
    err_mess << __FILE__ << ": " << __FUNCTION__ << " on an invalid buffer";
    throw std::logic_error(err_mess.str());
  }

  update_local_status();

  uintptr_t read_ptr = this->_backend._local_buffer_status.read_ptr();

  if (!this->_backend._local_buffer_status.is_empty()) {
    ret_val = reinterpret_cast<T*>(read_ptr + reinterpret_cast<uintptr_t>(this->_backend._buffer));
    if (ret_val->is_wrap()) {
      ret_val = reinterpret_cast<T*>(this->_backend._buffer);
      this->_backend._local_buffer_status.toggle_read_wrap();
      // std::cout << "reader wrap" << std::endl;
    }

    uintptr_t next_elem = reinterpret_cast<uintptr_t>(ret_val) + ret_val->bytes() +
                          get_padding(ret_val->bytes(), 1 << this->_backend._local_buffer_status.alignment);

    this->_backend._local_buffer_status.set_read_ptr(next_elem - reinterpret_cast<uintptr_t>(this->_backend._buffer));
  }

  return ret_val;
}

template<class T, class B>
void EB::Circular_buffer_reader<T, B>::flush()
{
  if (!this->is_set()) {
    std::ostringstream err_mess;
    err_mess << __FILE__ << ": " << __FUNCTION__ << " on an invalid buffer";
    throw std::logic_error(err_mess.str());
  }

  update_local_status();

  this->_backend._local_buffer_status.read_stat = this->_backend._local_buffer_status.write_stat;

  read_complete();
}

template<class T, class B>
void EB::Circular_buffer_reader<T, B>::read_complete()
{
  if (!this->is_set()) {
    std::ostringstream err_mess;
    err_mess << __FILE__ << ": " << __FUNCTION__ << " on an invalid buffer";
    throw std::logic_error(err_mess.str());
  }

  asm volatile("mfence" ::: "memory");
  this->_backend._remote_buffer_status->read_stat = this->_backend._local_buffer_status.read_stat;
}

template<class T, class B>
int EB::Circular_buffer_reader<T, B>::get_src_id() const
{
  return this->_backend._local_buffer_status.id;
}

template<class T, class B>
size_t EB::Circular_buffer_reader<T, B>::get_buffer_size() const
{
  return this->_backend._local_buffer_status.size;
}

template<class T, class B>
size_t EB::Circular_buffer_reader<T, B>::get_buffer_occupancy()
{
  update_local_status();
  return this->get_buffer_size() - (this->_backend._local_buffer_status.tail_free_space() +
                                    this->_backend._local_buffer_status.head_free_space());
}

template<class T, class B>
std::string EB::Circular_buffer_reader<T, B>::get_name() const
{
  return this->_backend.get_name();
}