#ifndef CIRCULAR_BUFFER_WRITER_H
#define CIRCULAR_BUFFER_WRITER_H 1

#include "buffer_interface.hpp"
#include "circular_buffer.hpp"
#include "buffer_writer_error.hpp"
#include <cstddef>
#include <cstdint>
#include <deque>
#include <iostream>
#include <string>
#include <tuple>

namespace EB {

  template<class T, class B>
  class Circular_buffer_writer : public EB::Buffer_writer<T>, public Circular_buffer<B> {
  public:
    // alignment is explesser in 2^alignment min value 1
    Circular_buffer_writer(B&& backend) : Circular_buffer<B>(std::move(backend)) {}
    Circular_buffer_writer() : Circular_buffer<B>() {}
    virtual ~Circular_buffer_writer();

    std::vector<std::tuple<void*, size_t>> get_full_buffer() override;

    T* try_write_next_element(size_t size) override;

    void write_complete() override;

    void write_discard() override;

    int get_src_id() const override;

    size_t free_space();

    std::string get_name() const override;

  protected:
    void update_local_status();
  };
} // namespace EB

#include "circular_buffer_writer_impl.hpp"

#endif // CIRCULAR_BUFFER_WRITER_H
