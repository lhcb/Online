#ifndef DUMMY_MEP_BUFFER_WRITER_H
#define DUMMY_MEP_BUFFER_WRITER_H 1
#include "EventBuilding/MEP_tools.hpp"
#include "buffer_interface.hpp"
#include "buffer_writer_error.hpp"
#include <memory>
#include <functional>

namespace EB {
  class Dummy_mep_buffer_writer : public EB::Buffer_writer<EB::MEP> {
  public:
    Dummy_mep_buffer_writer();
    Dummy_mep_buffer_writer(size_t buffer_size, int numa_node = -1);
    virtual ~Dummy_mep_buffer_writer();

    int reset_buffer(size_t buffer_size, int numa_node);

    EB::MEP* try_write_next_element(size_t size) override;

    std::vector<std::tuple<void*, size_t>> get_full_buffer() override;

    void write_complete() override;

    void write_discard() override;

    int get_src_id() const override { return 0; }

  private:
    typedef std::unique_ptr<void, std::function<void(void*)>> buffer_type;
    buffer_type _buffer;
    size_t _buffer_size;
    enum Buff_state { EMPTY, FULL };
    Buff_state _status;
  };
} // namespace EB

#endif // DUMMY_MEP_BUFFER_WRITER_H