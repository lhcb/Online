#ifndef DUMMY_MFP_BUFFER_H
#define DUMMY_MFP_BUFFER_H 1
#include <memory>
#include "EventBuilding/MFP_tools.hpp"
#include <vector>
#include "buffer_interface.hpp"

namespace EB {
  class Dummy_MFP_buffer : public EB::Buffer_reader<EB::MFP> {
  private:
    std::shared_ptr<char> _buffer;

    const char* _read_ptr;

  public:
    Dummy_MFP_buffer() = default;
    Dummy_MFP_buffer(std::shared_ptr<char>& buffer, size_t buffer_size, int src_id = 0);

    void set_buffer(std::shared_ptr<char>& buffer, size_t buffer_size, int src_id = 0);

    virtual EB::MFP* try_get_element() override;

    std::vector<std::tuple<void*, size_t>> get_full_buffer() override;

    virtual void read_complete() override;
    virtual void flush() override;

    virtual ~Dummy_MFP_buffer() = default;

    int get_src_id() const override;

    virtual size_t get_buffer_occupancy() override;

  protected:
    size_t get_size_at(int MFP_number = 1);
    char* get_ptr_at(int MFP_number = 1);
    std::tuple<char*, size_t> get_MFP_at(int MFP_number = 1);
    std::tuple<char*, size_t> operator[](int MFP_number);

    void update_read_ptr(int MFP_number = 1);

    int _src_id;
  };
} // namespace EB

#endif // DUMMY_MFP_BUFFER_H
