#ifndef MBMREADERDUMMYUNIT_HPP
#define MBMREADERDUMMYUNIT_HPP

#include "fbuff_api.hpp"
#include "Unit.hpp"
#include "common.hpp"
#include <ctime>
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "EventBuilding/MEP_tools.hpp"
// Framework include files

#include "Dataflow/Incidents.h"
#include "Dataflow/MBMClient.h"
#include "RTL/rtl.h"
#include "Dataflow/DataflowComponent.h"
#include "MBM/Consumer.h"
#include "MBM/Requirement.h"
#include "fbuff_api.hpp"
#include "shmem_buffer_reader.hpp"
#include "mbm_reader.hpp"
#include <memory>
#include <mutex>

///  Online namespace declaration
namespace Online {

  class MBM_reader_dummy_unit : public DataflowComponent, public Unit {

  public:
    /// Initializing constructor
    MBM_reader_dummy_unit(const std::string& name, Context& framework);
    /// Default destructor
    virtual ~MBM_reader_dummy_unit() = default;

    // methods
  protected:
    /// Service implementation: initialize the service
    virtual int initialize() override;
    /// Service implementation: start of the service
    virtual int start() override;
    /// Service implementation: stop the service
    virtual int stop() override;
    /// Service implementation: finalize the service
    virtual int finalize() override;

    /// Cancel I/O operations of the dataflow component
    virtual int cancel() override;
    /// Stop gracefully execution of the dataflow component
    virtual int pause() override;
    /// Incident handler implemenentation: Inform that a new incident has occured
    virtual void handle(const DataflowIncident& inc) override;
    /// IRunable implementation : Run the class implementation
    virtual int run() override;

  private:
    void read_meps();
    bool time_elapsed_check();

    // variables
  private:
    int64_t _now;
    int64_t _timer_start;
    int64_t _delay_timer;
    int64_t _probe_global_start;
    size_t _all_completed_bytes;
    size_t _current_probe_completed_bytes;
    int64_t _next_probing;
    std::string _m_buffer;
    /// Flag indicating that MBM event retrieval is active
    bool _m_receiveEvts = false;
    /// Property: Requirement as job option in string form
    EB::Buffer_reader<EB::MEP>* _recv_buff;
    std::string _m_req;
  };
} // end namespace Online

#endif
