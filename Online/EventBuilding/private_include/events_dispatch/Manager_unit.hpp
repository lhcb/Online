

#ifndef MANAGER_HPP
#define MANAGER_HPP

#include "Unit.hpp"
#include "common.hpp"
#include <assert.h>
#include <ctime>
#include <glib.h>
#include <limits>
#include <malloc.h>
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <thread>
#include <unistd.h>
#include <vector>

#include "Dataflow/DataflowComponent.h"
#include "MBM/Producer.h"

// Manager Unit class
// Used to implement Builder Units (Data Producers) to Filter Units (Data
// Consumers) scheduling
namespace Online {

  class Manager_unit : public DataflowComponent, public Unit {

  public:
    /// Initializing constructor
    Manager_unit(const std::string& nam, DataflowContext& ctxt);
    /// Default destructor
    virtual ~Manager_unit() = default;

  protected:
    /// Service implementation: initialize the service
    virtual int initialize() override;
    /// Service implementation: start of the service
    virtual int start() override;
    /// Service implementation: stop the service
    virtual int stop() override;
    /// Service implementation: finalize the service
    virtual int finalize() override;

    /// Cancel I/O operations of the dataflow component
    virtual int cancel() override;
    /// Stop gracefully execution of the dataflow component
    virtual int pause() override;
    /// Incident handler implemenentation: Inform that a new incident has occured
    virtual void handle(const DataflowIncident& inc) override;
    /// IRunable implementation : Run the class implementation
    virtual int run() override;

    // methods
  private:
    void get_ready_fus();
    void get_meps_bus();
    void grant_transmissions_to_bus();
    void initialize_receiving_fus();
    void initialize_receiving_bus();
    void send_bus_transmisison_grant(int buInd, int fuInd);
    int get_next_bu_rank_ready();
    // get the next Builder Unit to grant the BU-FU transmission pair
    bool get_next_free_fu_rank_ready_w_lowest_bytes(int* fu_index, int chosen_bu_index);
    // get the next Filter Unit to grant the BU-FU transmission pair
    bool get_next_bu_rank_ready_w_lowest_bytes(int* index);
    int get_bu_rank_ready_with_least_rus();
    int get_next_fu_ind();
    int get_next_bu_ind();
    // start transmissions to force necessary MPI resource allocation
    // invoked to mitigate / eliminate MPI init impact on the actual performance
    // measurements
    void mpi_warmup_run();
    // get next MEP size
    size_t get_next_bu_mep_size_to_transmit(int bu_ind);
    void erase_next_bu_mep_size(int bu_ind);
    void get_vector_of_meps(int bu_index, int entries_no);
    bool try_fu_alloc(int fu_ind, size_t size);

    // variables
  private:
    size_t* _ready_mep_array;
    std::vector<size_t>* _bu_meps_sizes_vectors;
    // variables
    size_t* _bus_total_granted_bytes_array;
    size_t* _fus_total_granted_bytes_array;

    int _how_many_ous;
    int _how_many_fus;

    int* _free_bu_grants;
    int* _trans_fu_no;

    int _bu_to_grant;
    int _fu_to_grant;
    // lookups for ranks that BUs have - goes togethet with busRanksReady
    int* _bus_ranks;
    int* _fus_ranks;
    size_t* _bus_meps_granted_bytes;
    int* _fus_ranks_ready;
    // array to limit BUs number assigned to a given FU to one
    int* _bu_grants;

    // holds which Bu has been assigned for a particular transmission to a given
    // FU
    int* _granted_bu_for_fu;
    MPI_Request* _fu_ready_requests;
    MPI_Request* _bu_ready_requests;
    MPI_Request* _trans_grant_requests;
    DISPATCH::mpi_fu_ready_msg* _fu_ready_messages;
    DISPATCH::mpi_bu_ready_msg* _bu_ready_messages;
    DISPATCH::mpi_bu_transmission_grant_msg* _trans_grant_messages;

    // global timestamp holding start of EM existence
    // USED to timestamp events that happened in the EM
    int64_t _start_probe;
  };
} // namespace Online

#endif
