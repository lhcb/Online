
#ifndef OUTPUTUNIT_HPP
#define OUTPUTUNIT_HPP

// Builder Unit class
// Used to implement Data Producer functions

#include <assert.h>
#include <ctime>
#include <malloc.h>
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "shmem_buffer_reader.hpp"
#include "mbm_reader.hpp"
#include "fbuff_api.hpp"
#include "Unit.hpp"
#include "common.hpp"
#include "EventBuilding/MEP_tools.hpp"
#include "Dataflow/DataflowComponent.h"
#include "MBM/Producer.h"
#include "buffer_interface.hpp"

namespace Online {

  class Output_unit : public DataflowComponent, public Unit {
  public:
    /// Initializing constructor
    Output_unit(const std::string& name, Context& framework);
    /// Default destructor
    virtual ~Output_unit() = default;

  protected:
    /// Service implementation: initialize the service
    virtual int initialize() override;
    /// Service implementation: start of the service
    virtual int start() override;
    /// Service implementation: stop the service
    virtual int stop() override;
    /// Service implementation: finalize the service
    virtual int finalize() override;

    /// Cancel I/O operations of the dataflow component
    virtual int cancel() override;
    /// Stop gracefully execution of the dataflow component
    virtual int pause() override;
    /// Incident handler implemenentation: Inform that a new incident has occured
    virtual void handle(const DataflowIncident& inc) override;
    /// IRunable implementation : Run the class implementation
    virtual int run() override;

  private:
    void start_send_data_to_fu(int rank, int slot);
    void signal_new_meps_ready(std::vector<mep_entry> new_mep_vector);
    bool time_elapsed_check();
    std::vector<mep_entry> get_meps_vector();
    void exchange_message_size(int fu_rank, size_t size);
    void get_meta(size_t size, int ind);
    void get_array_of_meps(std::vector<mep_entry> new_mep_vector);

    bool check_if_granted_transmission(int* ready_rank);
    void update_granted_transmissions();
    void start_receive_from_mu();
    // start transmissions to force necessary MPI resource allocation
    // invoked to mitigate the MPI init impact on the actual performance measurements
    void mpi_warmup_run();
    int get_free_slot();

  private:
    int _buffer_type;
    unsigned long int _total_serviced_events;
    // counter of all completed sends to count total average OU
    // throughput
    size_t _all_completed_bytes;
    // counter of all completed sends for the current probing period to
    // caltulate current BU throughput
    size_t _current_probe_completed_bytes;

    int64_t _next_probing;
    int* _started_credited_transmissions;
    bool* _credit_free_array;
    int* _credits_to_rank_array;

    int* _pending_credits_array;
    int _pending_credits_no;

    int64_t _now;
    int64_t _start_timer;
    int64_t _delay_timer;

    std::string _mbm_name;
    MPI_Request _bu_ready_requests;
    mpi_bu_ready_msg _bu_ready_message;
    MPI_Request _trans_granted_requests;
    mpi_bu_transmission_grant_msg _trans_granted_message;
    // MPI Message to establish message size
    mpi_bu_fu_mep_size_msg _bu_fu_mep_size_msg;
    MPI_Request** _dataTransmissionRequests;
    int64_t probe_global_start;
    // vector storing MEP entries
    std::vector<mep_entry> _ready_mep_vector;
    size_t* _ready_mep_array;
    mep_entry _current_transmission;
    EB::Buffer_reader<EB::MEP>* _recv_buff;
    std::string _m_req;
    current_transmission_metadata* _current_transmission_meta;
  };
} // namespace Online
#endif
