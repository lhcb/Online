#ifndef FILE_WRITER_H
#define FILE_WRITER_H 1

#include "buffer_interface.hpp"
#include <cstddef>
#include <cstdint>
#include <deque>
#include <iostream>
#include <string>
#include <tuple>
#include <cstdio>
#include <memory>
#include <functional>

namespace EB {
  template<class T>
  class File_writer {
  public:
    File_writer() = default;
    File_writer(const std::string& file_name);

    int write(const T* data);

    explicit operator bool() const noexcept;
    bool is_set() const noexcept;

  protected:
    typedef std::unique_ptr<FILE, std::function<void(FILE*)>> unique_file_type;
    std::string _file_name;
    unique_file_type _file;
  };
} // namespace EB

#include "file_writer_impl.hpp"

#endif // FILE_WRITER_H
