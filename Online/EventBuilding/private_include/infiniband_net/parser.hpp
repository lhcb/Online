/**
 * @file parser.hpp
 * @author Alberto Perro (alberto.perro@cern.ch)
 * @brief Parser for InfiniBand Library
 * @version 0.1.7
 * @date 24/03/2021
 *
 * @copyright Copyright (c) 2020-2021
 *
 */
#pragma once
#if !defined(PARSER_H)
#define PARSER_H
#include <fstream>
#include <vector>
#include <string>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <infiniband/verbs.h>
#include <iostream>
#include "proc.hpp"
#include "ArduinoJson.h"
#include <future>
#include <stdexcept>
#include <sstream>
#include <map>

namespace IB_verbs {
  class Parser {
  public:
    struct host_config {
      std::string hostname = "";
      int port = 0;
      std::string utgid = "";
      std::string ibdevs = "";
    };

    typedef std::vector<host_config> host_vec_t;
    Parser();
    Parser(const char* filename);
    Parser(const char* filename, const int rankid);
    ~Parser();
    std::string getMyHostname() const;
    int getTotalProcesses() const;
    int getIpPort() const;
    std::string getIbDevString() const;
    // TODO getters should be const
    int getIbNumaNode() const;
    int getIbDevNum() const;
    int getProcessId() const;
    int getLocalId() const;
    int getLocalGroup() const;
    host_vec_t getHostList() const;
    std::vector<int> getRanks() const;

  private:
    typedef std::map<std::string, int> map_utgid_rank_t;
    typedef std::map<std::string, std::vector<int>> map_host_local_ranks_t;
    int _globalProc = 0;
    int _localProc = 0;
    int _localGroup = 0;
    int _ipPort = 0;
    int _totalProcs = 0;
    int _ibNumaNode = -1;
    int _ibDevNum = -1;
    map_utgid_rank_t _utgid_rank_map;
    map_host_local_ranks_t _host_local_ranks_map;
    host_vec_t _hostvec;
    std::string _ibDevString = "";
    std::string _myHostName = "";
    int _getMyNetInfo();
    void _readHostFile(const char* filename, const int nProc);
    void _readHostFile(const char* filename);
    void _checkSelfData(int rank);
    void _extractData(const char* filename);
    void _getMyIbDevInfo();

    template<typename T>
    static T _safe_extract_key(const JsonObject& obj, const std::string& key)
    {
      auto val = obj[key];
      if (val.isNull()) {
        std::stringstream err_mess;
        err_mess << "Invalid config file missing key " << key;
        throw std::runtime_error(err_mess.str());
      }

      return val.as<T>();
    }
  };
} // namespace IB_verbs

#endif // PARSER_H
