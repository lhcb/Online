/**
 * @file sock.hpp
 * @author Alberto Perro (alberto.perro@cern.ch)
 * @brief Socket Communication for InfiniBand Library
 * @version 0.1.7
 * @date 24/03/2021
 *
 * @copyright Copyright (c) 2020-2021
 *
 */
#pragma once
#if !defined(SOCK_H)
#define SOCK_H
#include <vector>
#include <string>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <infiniband/verbs.h>
#include <iostream>
#include "proc.hpp"
#include <atomic>
#include <stdexcept>
#include <future>
#include <tuple>
#include <thread>

namespace IB_verbs {
  class Sock {
  public:
    Sock();
    ~Sock();
    struct QPInfo {
      uint16_t lid;
      uint32_t qp_num;
      uint32_t sync_qp_num;
      uint32_t procNum;
    };
    static int sendQP(const int rankid, Proc& rem);
    static void recvQPs(
      std::stop_token stoken,
      const int connection_timeout,
      const int rankid,
      std::vector<Proc>& hosts,
      std::atomic<int>& ret);
    static int join_threads(std::vector<std::thread>);
    typedef std::promise<std::tuple<int, QPInfo>> recv_QP_promise_t;
    typedef std::future<std::tuple<int, QPInfo>> recv_QP_future_t;
    static void recvQP(std::stop_token stoken, int fd, recv_QP_promise_t ret_promise);
  };
} // namespace IB_verbs
#endif // SOCK_H
