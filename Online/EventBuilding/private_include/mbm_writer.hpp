#ifndef MBM_WRITER_H
#define MBM_WRITER_H 1

#include "buffer_interface.hpp"
#include "Dataflow/MBMClient.h"
#include "Dataflow/DataflowComponent.h"
#include "MBM/Producer.h"
#include <cstddef>
#include <cstdint>
#include <deque>
#include <iostream>
#include <string>
#include <tuple>
#include <memory>

namespace EB {
  template<class T>
  class Mbm_writer : public EB::Buffer_writer<T> {
  public:
    // alignment is explesser in 2^alignment min value 1
    Mbm_writer() : EB::Buffer_writer<T>() {}
    // Mbm_writer(MBM::Producer* producer, int facility = 0);
    Mbm_writer(Online::DataflowComponent::Context& context, const std::string& instance, const std::string& name);
    virtual ~Mbm_writer();

    std::vector<std::tuple<void*, size_t>> get_full_buffer() override;

    T* try_write_next_element(size_t size) override;

    void write_complete() override;
    void write_discard() override;

    void cancel();

    size_t free_space();

    bool is_set();

    void reset(MBM::Producer* producer = NULL);

    int get_src_id() const override { return 0; }

  protected:
    Online::DataflowComponent::Context* _context;
    std::unique_ptr<MBM::Producer> _producer;
    // bool _ack_pending = false;
    enum Status { IDLE, ACK_PENDING };
    Status _status;
  };
} // namespace EB

#include "mbm_writer_impl.hpp"

#endif // MBM_WRITER_H
