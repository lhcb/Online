#ifndef MDF_READER_H
#define MDF_READER_H 1

#include "EventBuilding/mdf_tools.hpp"
#include <cstdio>
#include <cstring>
#include <fcntl.h>
#include <fstream>
#include <iostream>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <vector>
#include <exception>
#include <unordered_map>
#include "EventBuilding/raw_tools.hpp"

namespace EB {
  class MDF_reader {
  private:
    int _input_fd;

  public:
    MDF_reader() {}
    MDF_reader(const char* file_name);
    bool set_file(const char* file_name);
    int extract_event(EB::MDF_block& block);
    int extract_event(EB::MDF_block& block, void* buffer, size_t size);
    int extract_events(std::vector<EB::MDF_block>& blocks, int n_events);
    int extract_events(std::vector<EB::MDF_block>& blocks, int n_events, void* buffer, size_t size);
    int extract_events(std::vector<EB::MDF_block>& blocks);
    int extract_events(std::vector<EB::MDF_block>& blocks, void* buffer, size_t size);
  };
} // namespace EB

#endif // MDF_READER_H