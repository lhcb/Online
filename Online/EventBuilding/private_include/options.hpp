#ifndef OPTIONS_H
#define OPTIONS_H 1

#include <vector>
#include <string>
#include <functional>
#include <getopt.h>
#include <tuple>
#include <memory>

namespace EB {
  class Options {
  public:
    Options(int argc, char** argv);

    ~Options() = default;

    void add_option(
      const std::string& long_name,
      char short_name,
      const std::string& help,
      bool is_required,
      std::function<void(const char*)> user_funct,
      bool has_args = false);

    template<class T>
    void
    add_option(const std::string& long_name, char short_name, const std::string& help, bool is_required, T& out_var);

    void
    add_flag(const std::string& long_name, char short_name, const std::string& help, bool is_required, bool& out_flag);

    void
    add_counter(const std::string& long_name, char short_name, const std::string& help, bool is_required, int& counter);

    bool parse_args();

    void print_usage() const;

  private:
    typedef std::unique_ptr<char, std::function<void(char*)>> _long_names_unique_ptr_t;
    int _argc;
    char** _argv;
    std::vector<option> _options;
    std::vector<std::function<void(const char*)>> _lambdas;
    std::vector<char> _short_names;
    std::vector<_long_names_unique_ptr_t> _long_names;
    std::vector<bool> _required;
    std::vector<std::string> _help;
    size_t _max_text_aling = 0;

    std::string build_short_options();
    void apply_lambda(int idx);
    void print_option(int idx) const;
  };
} // namespace EB

#endif // OPTIONS_H