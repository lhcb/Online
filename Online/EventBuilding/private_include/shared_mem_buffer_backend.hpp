#ifndef SHARED_MEM_BUFFER_BACKEND_H
#define SHARED_MEM_BUFFER_BACKEND_H 1

#include "circular_buffer_status.hpp"
#include <cstddef>
#include <cstdint>
#include <iostream>
#include <string>
#include <tuple>
#include <memory>
#include <functional>
#include <sys/stat.h> /* For mode constants */
#include <numa.h>

namespace EB {
  class Shared_mem_buffer_backend {
  public:
    virtual ~Shared_mem_buffer_backend();
    Shared_mem_buffer_backend();
    Shared_mem_buffer_backend(
      const std::string& name,
      bool is_writer,
      bool allow_reconnect,
      bool allow_unlink,
      size_t size = 0,
      size_t alignment = 12,
      int id = 0,
      int numa_node = -1);
    Shared_mem_buffer_backend(Shared_mem_buffer_backend&& other);
    Shared_mem_buffer_backend& operator=(Shared_mem_buffer_backend&& other);

    std::tuple<void*, size_t> get_buffer();

    bool is_set();

    std::string get_name() const;

    // protected:
    uintptr_t _buffer;

    Circular_buffer_status* _remote_buffer_status;

    Circular_buffer_status _local_buffer_status;

  private:
    static constexpr mode_t permission_mask = S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IWOTH | S_IROTH;

    std::string _shmem_name;
    int _shmem_fd;

    bool _is_writer;
    bool _allow_unlink;

    // Unique ptrs to the shmem region
    typedef std::unique_ptr<void, std::function<void(void*)>> shmem_ptr_type;
    shmem_ptr_type _shm_area;

    std::string _reader_lock_name;
    int _reader_lock_fd;
    std::string _writer_lock_name;
    int _writer_lock_fd;

    void open_shmem();
    void create_shmem();
    void create_locks();
    void init_buffer(size_t size, size_t alignment, int id);
    void shm_map(int numa_node = -1);
    void close_shmem();
    void destroy_shmem();

    bool is_open();

    void try_lock_reader();
    void release_reader();

    void try_lock_writer();
    void release_writer();

    void close_fd(int& fd);

    void clean_up();
  };
} // namespace EB
#endif // SHARED_MEM_BUFFER_BACKEND_H