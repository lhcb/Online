#ifndef SHARED_PTR_BUFFER_BACKEND_H
#define SHARED_PTR_BUFFER_BACKEND_H 1

#include "circular_buffer_status.hpp"
#include <cstddef>
#include <cstdint>
#include <iostream>
#include <string>
#include <tuple>
#include <memory>
#include <numa.h>

namespace EB {
  class Shared_ptr_buffer_backend {
  public:
    virtual ~Shared_ptr_buffer_backend();
    Shared_ptr_buffer_backend(){};
    Shared_ptr_buffer_backend(
      size_t size,
      size_t alignment = 12,
      int id = 0,
      int numa_node = -1,
      const std::string name = "");

    std::tuple<void*, size_t> get_buffer();
    bool is_set();

    uintptr_t _buffer;

    std::shared_ptr<Circular_buffer_status> _remote_buffer_status;

    Circular_buffer_status _local_buffer_status;

    std::string get_name() const;

  private:
    std::shared_ptr<void> _buffer_ptr;
    std::string _name;
  };
} // namespace EB
#endif // SHARED_PTR_BUFFER_BACKEND_H