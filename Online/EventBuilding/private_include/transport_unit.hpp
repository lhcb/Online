#ifndef TRANSPORT_UNIT_H
#define TRANSPORT_UNIT_H 1

#include <memory>
#include <vector>
#include <cassert>
#include <exception>
#include <string>
#include "logger.hpp"
#include "Parallel_Comm.hpp"
#include "EventBuilding/MEP_tools.hpp"
#include "Dataflow/DataflowComponent.h"

namespace EB {
  using namespace Online;
  // static configuration
  bool is_bu(int rank, const std::vector<int>& n_rus_nic);
  bool is_ru(int rank, const std::vector<int>& n_rus_nic);

  class Transport_unit : public DataflowComponent {
  public:
    Transport_unit(const std::string& nam, Context& ctxt);
    virtual ~Transport_unit();

    // FOR DEBUG ONLY
    void set_rank(int rank);

    virtual int initialize() override;
    virtual int finalize() override;
    virtual int cancel() override;
    virtual int start() override;
    virtual int stop() override;

  protected:
    int _my_rank;
    int _world_size;
    int _numa_node;
    // Properties
    std::vector<int> _n_rus_per_nic;
    int _n_par_mess;
    std::vector<int> _prop_n_sources_per_ru;
    std::vector<unsigned int> _n_sources_per_ru;
    // _ru_ranks and _bu_ranks can either be self assigned or set via an opts file
    std::vector<int> _ru_ranks;
    std::vector<int> _bu_ranks;
    // simmetric shift pattern used when running with the same number of RUs and BUs
    std::vector<int> _shift_pattern;
    // shift pattern used to send to the RUs
    std::vector<int> _RU_shift_pattern;
    // shift pattern used to send to the BUs
    std::vector<int> _BU_shift_pattern;
    bool _MPI_errors_return;
    int _prop_barrier_type;
    int _prop_sync_mode;
    int _tree_barrier_size;
    IB_verbs::IB::barrier_t _barrier_type;
    IB_verbs::IB::syncmode_t _sync_mode;
    int _profiling_update_interval;
    bool _enable_profiling;
    int _IB_connection_timeout;
    int _sync_phase_freq;
    bool _use_sharp;

    // DF monitoring counters
    uint64_t _DF_events_out;
    uint64_t _DF_events_in;
    uint64_t _DF_events_err;

    // net monitoring counters in the transport_unit class
    uint64_t _bytes_out;
    uint64_t _bytes_in;

    // DEBUG counters
    uint64_t _barrier_count;
    uint64_t _pre_barrier_count;
    // profiling counters
    double _sync_time_counter;
    double _total_time_counter;

    // internal counter used to count the number of sync call
    // NOT reset at run change don't use for monitoring
    uint64_t _sync_count;

    // Profiling timers
    Timer _total_timer;
    Timer _sync_timer;

    // prefix sum of the number of sources up to the give idx, the last element is the total number of sources
    std::vector<unsigned int> _prefix_n_sources_per_ru;

    Log_stream logger;
    std::unique_ptr<IB_verbs::Parser> _ibParser;
    std::unique_ptr<Parallel_comm> _ibComm;
    std::string _ib_config_file = "";
    int sync(bool ignore_sync_freq);
    static std::vector<int> init_shift_pattern(const std::vector<int>& ranks);
    int check_shift_pattern(const std::vector<int>& ranks, const std::vector<int>& pattern);
    int init_pattern();
    int init_ranks();
    int check_ranks();
    int init_n_sources();
    virtual void reset_counters();
    virtual void update_profiling();
    virtual void init_profiling();
  };
} // namespace EB

#endif // TRANSPORT_UNIT_H
