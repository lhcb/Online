import argparse
from cmath import nan
import functools
import os
import re
import sys
import time
import pydim

# TODO move to a common area
global_profiling_counters = [
    "total_time_counter",
    "sync_time_counter",
]

RU_profiling_counters = [
    "load_mpfs_time_counter",
    "send_sizes_time_counter",
    "linear_shift_time_counter",
    "send_time_counter",
]

BU_profiling_counters = [
    "receive_size_time_counter",
    "calc_offsets_time_counter",
    "build_header_time_counter",
    "linear_shift_time_counter",
    "receive_MFPs_time_counter",
]

performance_counters = [
    "Events/OUT",
    "Events/IN",
    "bytes_out",
    "bytes_in",
]


def build_unit_service(partition, host, arch_name, unit, idx, metric, rate_enable, count_enable):
    ret_val = []
    if(rate_enable):
        ret_val.append(
            f'{partition}_{host.upper()}_{arch_name}_{idx}/R_{unit}/{metric}')
    if(count_enable):
        ret_val.append(
            f'{partition}_{host.upper()}_{arch_name}_{idx}/{unit}/{metric}')

    return ret_val


def build_services(bu_metrics, ru_metrics, mfp_metrics, hosts, partition, rate_enable, count_enable):
    services = []
    for host in hosts:
        host_metrics = []
        for metric in ru_metrics:
            if metric != '':
                for idx in range(2):
                    host_metrics.extend(build_unit_service(
                        partition, host, 'RU', 'RU', idx, metric, rate_enable, count_enable))

        for metric in bu_metrics:
            if metric != '':
                for idx in range(2):
                    host_metrics.extend(build_unit_service(
                        partition, host, 'BU', 'BU', idx, metric, rate_enable, count_enable))

        for metric in mfp_metrics:
            if metric != '':
                for idx in range(6):
                    host_metrics.extend(build_unit_service(
                        partition, host, 'MFPGen', 'MFP_generator', idx, metric, rate_enable, count_enable))

        services.append(host_metrics)

    return services


def monitor_counters(bu_metrics, ru_metrics, mfp_metrics, hosts, partition, measure_time, total_time, rate_enable, count_enable):
    services = build_services(bu_metrics, ru_metrics, mfp_metrics,
                              hosts, partition, rate_enable, count_enable)

    print_header = ['host'] + \
        [service[(service.find('/'))+1:] for service in services[0]]

    header_lens = list(map(len, print_header))
    hosts_lens = list(map(len, hosts))
    max_hosts_len = max(hosts_lens)
    header_lens[0] = max_hosts_len

    for length, field in zip(header_lens, print_header):
        print(f'{field:^{length}}', end=" | ")
    print()
    # print('host\t' + '\t'.join([service[(service.find('/'))+1:]
    # for service in services[0]]))
    start_time = time.time()

    while(time.time() - start_time < total_time):
        for host, host_services in zip(hosts, services):
            values = [host] + monitor_counter_list(host_services)
            for length, value in zip(header_lens, values):
                if(type(value) == float):
                    print(f'{value:^{length}.4}', end=" | ")
                else:
                    print(f'{value:^{length}}', end=" | ")
            print()
            # print(f'{host}\t\t' + '\t\t'.join(
            #     [f'{result:.5f}' for result in results]))

        time.sleep(measure_time)

        print()

        for length, field in zip(header_lens, print_header):
            print(f'{field:^{length}}', end=" | ")
        print()
        # print(
        #     'host\t' + '\t'.join([service[(service.find('/'))+1:] for service in services[0]]))


def monitor_counters_csv(bu_metrics, ru_metrics, mfp_metrics, hosts, partition, measure_time, total_time, rate, count_enable, out_filename, parallel_processes):
    services = build_services(bu_metrics, ru_metrics,
                              mfp_metrics, hosts, partition, rate, count_enable)
    flat_services = flatten_list(services)
    print('time,', ','.join(flat_services))
    start_time = time.time()

    while(time.time() - start_time < total_time):
        read_start = time.time()
        results = monitor_counter_list(flat_services)

        read_time = time.time() - read_start
        print(f'{(read_start - start_time) + read_time*0.5},' +
              ','.join([f'{result}' for result in results]))
        if (read_time < measure_time):
            time.sleep(measure_time - read_time)
        else:
            sys.stderr.write(
                f'WARNING data collection took too long. Elapsed time {read_time} sampling interval {measure_time}')


def flatten_list(x):
    return [item for sub_list in x for item in sub_list]


def monitor_counter(service):
    res = pydim.dic_sync_info_service(service)

    if (len(res) == 0):
        res = nan
    else:
        res = res[0]
    return res


def monitor_counter_list(services):
    res = [monitor_counter(service) for service in services]

    return res


def get_host_list(partition):
    server_list = pydim.dic_sync_info_service(
        'DIS_DNS/SERVER_LIST')[0].split('|')
    unit_match = re.compile(
        f'{partition}_(([A-Z0-9][A-Z0-9]EB[0-9][0-9])|(sodin[0-9][0-9]))_(BU|RU)', re.IGNORECASE)
    return sorted(set([server.split('@')[1] for server in server_list if unit_match.match(server) != None]))


def main():
    parser = argparse.ArgumentParser(
        description='EB counters reader script',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    host_group = parser.add_mutually_exclusive_group(required=False)

    host_group.add_argument(
        "-H",
        "--hosts",
        help='Comma separated list of hosts to monitor.',
        type=str)

    host_group.add_argument(
        "-f",
        "--host_file",
        help='Host file, one hostname per row. # comment',
        type=str)

    parser.add_argument("-D",
                        "--dim_dns",
                        help='Override the DIM_DNS_NODE env variable value.',
                        default=None,
                        type=str)

    parser.add_argument(
        "-p",
        "--partition",
        help='Partition to monitor.',
        type=str,
        default='LHCb',
        required=False)

    parser.add_argument(
        "--rate",
        help='Enable rate counter reading.',
        action='store_true',
        required=False)

    parser.add_argument(
        "--no_rate",
        help='Disable rate counter reading.',
        dest='rate',
        action='store_false',
        required=False)

    parser.add_argument(
        "--count",
        help='Enable counter reading.',
        action='store_true',
        default=True,
        required=False)

    parser.add_argument(
        "--no_count",
        help='Disable  counter reading.',
        dest='count',
        action='store_false',
        required=False)

    parser.add_argument(
        "-b",
        "--bu_counters",
        help='Comma separated list of BU counters to monitor.',
        type=str,
        default=','.join(BU_profiling_counters +
                         global_profiling_counters+performance_counters),
        required=False)

    parser.add_argument(
        "-r",
        "--ru_counters",
        help='Comma separated list of RU counters to monitor.',
        type=str,
        default=','.join(RU_profiling_counters +
                         global_profiling_counters+performance_counters),
        required=False)

    parser.add_argument(
        "-m",
        "--mfp_counters",
        help='Comma separated list of MFP_generator counters to monitor.',
        type=str,
        default='',
        required=False)

    parser.add_argument(
        "-t",
        "--time",
        help='Total Time interval in seconds.',
        type=float,
        required=True)

    parser.add_argument(
        "-s",
        "--sampling",
        help='Sampling time interval in seconds.',
        type=float,
        default=5.)

    parser.add_argument(
        "-c",
        "--csv_out",
        help='Enable csv output.',
        action='store_true'
    )

    args = parser.parse_args()

    env = os.environ.copy()

    if (args.dim_dns):
        env['DIM_DNS_NODE'] = args.dim_dns
        pydim.dic_set_dns_node(args.dim_dns)

    if (args.hosts):
        host_list = args.hosts.split(',')
    elif(args.host_file):
        with open(args.host_file, 'r') as host_file:
            no_comment_match = re.compile('([^#]*)(?:#)?.*')
            host_list = [match.group(1) for match in
                         [no_comment_match.match(line.strip())
                          for line in host_file.readlines()]
                         if match.group(1) != '']
    else:
        host_list = get_host_list(args.partition)

    host_list = list(set(host_list))

    bu_metrics = list(set(args.bu_counters.split(',')))
    ru_metrics = list(set(args.ru_counters.split(',')))
    mfp_metrics = list(set(args.mfp_counters.split(',')))

    bu_metrics.sort()
    ru_metrics.sort()
    mfp_metrics.sort()
    host_list.sort()

    if(args.csv_out):
        monitor_counters_csv(bu_metrics, ru_metrics, mfp_metrics, host_list,
                             args.partition, args.sampling, args.time, args.rate, args.count, args.csv_out, 4)
    else:
        monitor_counters(bu_metrics, ru_metrics, mfp_metrics, host_list,
                         args.partition, args.sampling, args.time, args.rate, args.count)


if __name__ == "__main__":
    main()
