import os
import pwd
import sys
import signal
import subprocess
import shlex
import argparse
import time
import re
import random
import distutils.core
import itertools
import json
import tempfile

mpi_base_options = shlex.split('-bind-to none')
gdb_base_command = shlex.split('gdb')
mpi_base_command = shlex.split('mpirun -oversubscribe')
mpi_ib_options = shlex.split('')
dataflow_task = shlex.split(
    'gentest.exe libDataflow.so dataflow_run_task -msg=Dataflow_OutputLogger -mon=Dataflow_DIMMonitoring'
)


def split_lst(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i:i + n]


def signal_handler(sig, frame):
    print('Caught SIGINT. Exiting...')

    processes = [
        frame.f_locals['mfp_gen_proc'], frame.f_locals['eb_proc'],
        frame.f_locals['mbm_proc']
    ]

    for process in processes:
        if process:
            process.kill()

    kill_exit = 0
    if (frame.f_locals['args'].kill):
        kill_exit += kill_gentest(frame.f_locals['hosts'], 32)
    else:
        kill_exit = 0

    sys.exit(kill_exit)


def kill_gentest(hosts, n_proc):
    kill_exit = 0
    hosts = list(set(hosts))
    for host_split in split_lst(hosts, n_proc):
        kills = [
            subprocess.Popen(shlex.split(f'ssh {host} killall gentest.exe'))
            for host in host_split
        ]

        kill_exit += sum([proc.wait() for proc in kills])

    return kill_exit


def shell_source(script):
    """Sometime you want to emulate the action of "source" in bash,
    settings some environment variables. Here is a way to do it."""
    pipe = subprocess.Popen(f'source {os.path.abspath(script)} ; env -0',
                            stdout=subprocess.PIPE,
                            shell=True)
    output = pipe.communicate()[0].decode("utf-8")
    env = dict(line.partition('=')[::2] for line in output.split('\0'))

    return env


def gen_env_file(env, tmp_dir=None):
    # remove empty keys and macros the MPI parser does't like macros
    def clean_funct(x):
        return (x != '') and ('()' not in x)

    env_list = (f'-x {var}\n' for var in filter(clean_funct, env.keys()))
    temp_file = tempfile.NamedTemporaryFile(mode='w',
                                            prefix='eb_env_',
                                            dir=tmp_dir)

    temp_file.writelines(env_list)
    temp_file.flush()

    return temp_file


def start_mbm(env, env_file_name, host_list, auto, debug_config):
    mpi_command = mpi_base_command.copy()
    # mpi gets env from a file
    mpi_command.extend(shlex.split(f'-tune {env_file_name}'))

    df_task_full = dataflow_task + \
        shlex.split(
            f'-class=Class0 -opts={env["EVENTBUILDINGROOT"]}/options/MBM_server.opts')
    if (auto):
        df_task_full.append('-auto')

    if (debug_config):
        gdb_runfile = debug_config['gdb_runfile']
        debug_list = [
            re.compile(item) for item in debug_config['mbm']['debug']
        ]
        xterm_list = [
            re.compile(item) for item in debug_config['mbm']['xterm']
        ]

    rank = 0
    xterm_ranks = []
    local_id = {host: 0 for host in host_list}
    for k, host in enumerate(host_list):
        mpi_command.extend(shlex.split(f'-H {host}'))
        mpi_command.extend(mpi_base_options)
        mpi_command.extend(
            shlex.split(
                f'-x DIM_HOST_NODE={host} -x UTGID=EB_{host.upper()}_MBM_{local_id[host]}'
            ))

        if (debug_config):
            debug_commands(host, rank, debug_list, xterm_list, gdb_runfile,
                           mpi_command, xterm_ranks)

        mpi_command.extend(df_task_full)
        mpi_command.append(':')
        rank += 1
        local_id[host] += 1

    if (xterm_ranks):
        mpi_command[1:1] = shlex.split('-xterm ' +
                                       ','.join(str(x) for x in xterm_ranks))

    return subprocess.Popen(mpi_command, env=env, cwd=env['PWD'], shell=False)


def start_mfp_gen(env, env_file_name, host_list, n_rus_per_nic,
                  n_sources_per_ru, auto, debug_config):
    mpi_command = mpi_base_command.copy()
    # mpi gets env from a file
    mpi_command.extend(shlex.split(f'-tune {env_file_name}'))

    df_task_full = dataflow_task + \
        shlex.split(
            f'-class=Class1 -opts={env["EVENTBUILDINGROOT"]}/options/MFP_generator.opts')
    if (auto):
        df_task_full.append('-auto')

    if (debug_config):
        gdb_runfile = debug_config['gdb_runfile']
        debug_list = [
            re.compile(item) for item in debug_config['mfp']['debug']
        ]
        xterm_list = [
            re.compile(item) for item in debug_config['mfp']['xterm']
        ]

    rank = 0
    ru_idx = 0
    xterm_ranks = []
    local_id = {host: 0 for host in host_list}
    for k, host in enumerate(host_list):
        for count in range(n_rus_per_nic[k]):
            for src_idx in range(n_sources_per_ru[ru_idx]):
                seed = int(4096 * random.random())
                mpi_command.extend(shlex.split(f'-H {host}'))
                mpi_command.extend(mpi_base_options)
                mpi_command.extend(
                    shlex.split(
                        f' -x DIM_HOST_NODE={host} -x UTGID=EB_{host.upper()}_MFPGen_{local_id[host]} -x RAND_SEED={seed} -x MFP_SRC_ID={20*(k+1) - local_id[host]}'
                    ))
                if (debug_config):
                    debug_commands(host, rank, debug_list, xterm_list,
                                   gdb_runfile, mpi_command, xterm_ranks)

                mpi_command.extend(df_task_full)
                mpi_command.append(':')
                rank += 1
                local_id[host] += 1
            ru_idx += 1

    if (xterm_ranks):
        mpi_command[1:1] = shlex.split('-xterm ' +
                                       ','.join(str(x) for x in xterm_ranks))

    return subprocess.Popen(mpi_command, env=env, cwd=env['PWD'], shell=False)


def start_eb(env, env_file_name, host_list, n_rus_per_nic, auto, debug_config):
    mpi_command = mpi_base_command.copy()
    # mpi gets env from a file
    mpi_command.extend(shlex.split(f'-tune {env_file_name}'))

    df_task_full = dataflow_task + \
        shlex.split(
            f'-class=Class1')
    if (auto):
        df_task_full.append('-auto')

    if (debug_config):
        gdb_runfile = debug_config['gdb_runfile']
        ru_debug_list = [
            re.compile(item) for item in debug_config['eb']['ru_debug']
        ]
        bu_debug_list = [
            re.compile(item) for item in debug_config['eb']['bu_debug']
        ]
        ru_xterm_list = [
            re.compile(item) for item in debug_config['eb']['ru_xterm']
        ]
        bu_xterm_list = [
            re.compile(item) for item in debug_config['eb']['bu_xterm']
        ]

    bu_local_id = {host: 0 for host in host_list}
    ru_local_id = {host: 0 for host in host_list}
    rank = 0
    xterm_ranks = []
    for k, host in enumerate(host_list):
        for count in range(n_rus_per_nic[k]):
            # RUs
            mpi_command.extend(shlex.split(f'-H {host}'))
            mpi_command.extend(mpi_base_options)
            mpi_command.extend(
                shlex.split(
                    f'-x DIM_HOST_NODE={host} -x UTGID=EB_{host.upper()}_RU_{ru_local_id[host]}'
                ))

            if (debug_config):
                debug_commands(host, rank, ru_debug_list, ru_xterm_list,
                               gdb_runfile, mpi_command, xterm_ranks)

            mpi_command.extend(df_task_full)
            mpi_command.append(
                f'-opts={env["EVENTBUILDINGROOT"]}/options/EB_RU.opts')
            mpi_command.append(':')

            ru_local_id[host] += 1
            rank += 1
        # BUs
        mpi_command.extend(shlex.split(f'-H {host}'))
        mpi_command.extend(mpi_base_options)
        mpi_command.extend(
            shlex.split(
                f'-x DIM_HOST_NODE={host} -x UTGID=EB_{host.upper()}_BU_{bu_local_id[host]}'
            ))

        if (debug_config):
            debug_commands(host, rank, bu_debug_list, bu_xterm_list,
                           gdb_runfile, mpi_command, xterm_ranks)

        mpi_command.extend(df_task_full)
        mpi_command.append(
            f'-opts={env["EVENTBUILDINGROOT"]}/options/EB_BU.opts')
        mpi_command.append(':')
        bu_local_id[host] += 1
        rank += 1

    if (xterm_ranks):
        mpi_command[1:1] = shlex.split('-xterm ' +
                                       '!,'.join(str(x) for x in xterm_ranks))

    return subprocess.Popen(mpi_command, env=env, cwd=env['PWD'], shell=False)


def debug_commands(host, rank, debug_list, xterm_list, gdb_runfile,
                   mpi_command, xterm_ranks_list):
    if (any((match.fullmatch(host) for match in debug_list))):
        mpi_command.extend(shlex.split(f'gdb -x {gdb_runfile} -args'))
        xterm_ranks_list.append(rank)
    elif (any((match.fullmatch(host) for match in xterm_list))):
        xterm_ranks_list.append(rank)


def set_env(vars_script):
    env = os.environ.copy()
    env.update(shell_source(vars_script))
    env['INFO_OPTIONS'] = f'{env["EVENTBUILDINGROOT"]}/options/OnlineEnv.opts'
    env['MBM_SETUP_OPTIONS'] = f'{env["EVENTBUILDINGROOT"]}/options/MBMSetup.opts'
    # FIXME temporary workaround for RHEL9 compatibility
    try:
        del env['BASH_FUNC_which%%']
    except KeyError:
        pass
    return env


def parse_DF_opts(opt_file_name):
    match = re.compile(
        '(?P<prop_name>[a-zA-Z0-9._]+)\s*=\s*{?(?P<prop_value>[a-zA-Z0-9_,$ \t"]+)}?\s*;\s*(?:[^/]{2})?'
    )
    with open(opt_file_name, 'r') as opt_file:
        properties = {
            match_line.group('prop_name'): match_line.group('prop_value')
            for line in opt_file.readlines()
            for match_line in [match.match(line)] if match_line != None
        }

    return properties


def parse_DF_opts_list(properties, prop_name, default, list_len):
    prop = properties.get(prop_name, '')
    val_type = type(default)
    if (prop != ''):
        ret_val = list(map(val_type, re.split('(?:\s+)?,(?:\s+)?', prop)))
    else:
        # default value
        ret_val = [default] * list_len

    if (len(ret_val) == 1):
        ret_val = [ret_val[0]] * list_len

    return ret_val


def parse_hostfile(hostfile_name):
    with open(hostfile_name, 'r') as hostfile:
        comment_match = re.compile('([^#]*)')
        matches = map(comment_match.search, hostfile.readlines())
        hosts = [match.group(0).strip() for match in matches if match != None]

    return hosts


def main():
    parser = argparse.ArgumentParser(
        description='EB startup script',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument(
        "-H",
        "--hostfile",
        help='Host file.'
        'Every line must contain an hostname. To run multiple units on the same host enter the host name multiple times.'
        ' Lines starting with # are threaded as comments.',
        type=str,
        required=True)

    parser.add_argument(
        "-s",
        "--setup",
        help='Setup file generated by cmsetup (like setup.x86_64-centos7-gcc9-do0.vars).',
        type=str,
        required=True)

    parser.add_argument(
        "-u",
        "--user",
        help='Prepend the user name to the shmem prefix user by the MFP generator and the RU.',
        action='store_true')

    parser.add_argument(
        "-d",
        "--debug",
        help='Debug json config file. Check the example file provided.\n Xterm running processes are not killed by MPI.',
        default=None,
        type=str)

    parser.add_argument(
        "-p",
        "--prefix",
        help='Shmem prefix user by the MFP generator and the RU.',
        type=str,
        default='RU')

    parser.add_argument(
        "-e",
        "--eb",
        help="Enable/Disable EB processes, select auto for automatic startup.",
        type=lambda x: x.lower()
        if x.lower() == 'auto' else bool(distutils.util.strtobool(x)),
        default='auto')

    parser.add_argument(
        "-m",
        "--mfp",
        help="Enable/Disable MFP generator processes, select auto for automatic startup.",
        type=lambda x: x.lower()
        if x.lower() == 'auto' else bool(distutils.util.strtobool(x)),
        default='auto')

    parser.add_argument(
        "-b",
        "--mbm",
        help="Enable/Disable MBM processes, select auto for automatic startup.",
        type=lambda x: x.lower()
        if x.lower() == 'auto' else bool(distutils.util.strtobool(x)),
        default='auto')

    parser.add_argument("-k",
                        "--kill",
                        help="Force killing of gentest.exe on all the hosts.",
                        action='store_true')

    parser.add_argument("-D",
                        "--dim_dns",
                        help='Override the DIM_DNS_NODE env variable value.',
                        default=None,
                        type=str)

    parser.add_argument(
        "-t",
        "--tmp",
        help='Shared tmp directory. This directory needs to be writable by the machine running this script and readable by all the machines.',
        default=os.getcwd(),
        type=str)

    args = parser.parse_args()

    env = set_env(args.setup)

    if (args.dim_dns):
        env['DIM_DNS_NODE'] = args.dim_dns

    # configures the shmem_prefix env variable according to the user specified options

    hosts = parse_hostfile(args.hostfile)

    if (args.debug):
        with open(args.debug, 'r') as debug_config_file:
            debug_config = json.load(debug_config_file)
    else:
        debug_config = None

    EB_transport_props = parse_DF_opts(
        f'{env["EVENTBUILDINGROOT"]}/options/EB_transport.opts')

    n_rus_per_nic = parse_DF_opts_list(EB_transport_props,
                                       'EB_transport.RUs_per_nic', 1,
                                       len(hosts))

    n_sources_per_ru = parse_DF_opts_list(EB_transport_props,
                                          'EB_transport.n_sources_per_ru', 1,
                                          sum(n_rus_per_nic))

    # write env setup file for mpi
    env_file = gen_env_file(env, args.tmp)

    if (args.mbm):
        mbm_proc = start_mbm(env, env_file.name, hosts, args.mbm == 'auto',
                             debug_config)
    else:
        mbm_proc = None

    time.sleep(5)

    if (args.mfp):
        mfp_gen_proc = start_mfp_gen(env, env_file.name, hosts, n_rus_per_nic,
                                     n_sources_per_ru, args.mfp == 'auto',
                                     debug_config)
    else:
        mfp_gen_proc = None

    time.sleep(5)

    if (args.eb):
        eb_proc = start_eb(env, env_file.name, hosts, n_rus_per_nic,
                           args.eb == 'auto', debug_config)
    else:
        eb_proc = None

    signal.signal(signal.SIGINT, signal_handler)
    print("Press Ctrl+C to exit.")
    signal.pause()


if __name__ == "__main__":
    main()
