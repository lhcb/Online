from multiprocessing import Pool
import os
import pwd
import sys
import signal
import subprocess
import shlex
import argparse
import time
import re
import random
import distutils.core
import itertools
import json
import tempfile
import datetime
import pydim
import numpy as np
import statistics
import run_eb
import gen_config


def scan_config(lines, tmp_folder, out_folder, fat_tree_file, hostfile_name,
                json_name, scan_idx, switch_radix, shift_pattern_file):
    hosts = gen_config.parse_file_lines(fat_tree_file, lines)

    host_list = gen_config.generate_hostfile(hosts)

    shift_pattern = []

    if (len(host_list) > 0):

        with open(f'{out_folder}/scan_{scan_idx}_{hostfile_name}',
                  'w') as outfile:
            outfile.write('\n'.join(host_list))

        # TODO hack to keep the json files
        json_config = gen_config.generate_json(hosts)

        with open(json_name, 'w') as outfile:
            json.dump(json_config, outfile, indent=2)

        with open(f'{out_folder}/scan_{scan_idx}.json', 'w') as outfile:
            json.dump(json_config, outfile, indent=2)

        shift_pattern = np.concatenate(
            gen_config.generate_shift(hosts, switch_radix))

        shift_pattern_property = 'EB_transport.shift_pattern = {' + ','.join(
            map(str, shift_pattern)) + '};\n'

        # TODO hack to keep the shift pattern files
        with open(shift_pattern_file, 'w') as outfile:
            outfile.write(shift_pattern_property)

        with open(f'{out_folder}/scan_{scan_idx}_shift_pattern.opts',
                  'w') as outfile:
            outfile.write(shift_pattern_property)
    else:
        print('This step has 0 valid nodes, skipping.')

    return host_list, shift_pattern


def scan_step(hostfile, env, tmp_folder, debug_config):
    hosts = run_eb.parse_hostfile(hostfile)

    EB_transport_props = run_eb.parse_DF_opts(
        f'{env["EVENTBUILDINGROOT"]}/options/EB_transport.opts')

    n_rus_per_nic = run_eb.parse_DF_opts_list(EB_transport_props,
                                              'EB_transport.RUs_per_nic', 1,
                                              len(hosts))

    n_sources_per_ru = run_eb.parse_DF_opts_list(
        EB_transport_props, 'EB_transport.n_sources_per_ru', 1,
        sum(n_rus_per_nic))

    # write env setup file for mpi
    env_file = run_eb.gen_env_file(env, tmp_folder)

    mfp_gen_proc = run_eb.start_mfp_gen(env, env_file.name, hosts,
                                        n_rus_per_nic, n_sources_per_ru, True,
                                        debug_config)

    time.sleep(2)

    eb_proc = run_eb.start_eb(env, env_file.name, hosts, n_rus_per_nic, True,
                              debug_config)

    time.sleep(2)

    return (mfp_gen_proc, eb_proc)


def check_warm_up(host, timeout=120):
    res = []

    service_name = f'EB_{host.upper()}_BU_0/R_BU/bytes_in'
    srv_id = pydim.dic_info_service(service_name,
                                    lambda x: res.append(x / 1e9 * 8))

    timeout_delta = datetime.timedelta(seconds=timeout)

    start_time = datetime.datetime.now()

    def stable_val(vec): return (abs(vec[-1] - vec[-2]) < 10) and (vec[
        -1] > 20) if len(vec) >= 2 else False

    while ((not stable_val(res))
           and ((datetime.datetime.now() - start_time) < timeout_delta)):
        time.sleep(10)

    pydim.dic_release_service(srv_id)

    if (stable_val(res)):
        print('warmup done')
    else:
        print('WARNING warmup timed out')


def check_data_df(services, total_time, frequency, partition='EB', parallel_processes=4):
    unique_services_list = list(set(services))
    # unique_host_list = [host_list[0]]

    res = {}
    services = {}

    for service in unique_services_list:
        # TODO change this to measure BW
        service_name = f'{partition}_{service}'
        service_fields = service_name.split('/')
        service_key = service_fields[0] + '/' + '/'.join(service_fields[2:])
        if 'R_' in service_fields[1]:
            service_key += '/rate'
        res[service_key] = list()
        services[service_key] = service_name

    start = time.time()
    serv_data = [(res[key], services[key]) for key in res.keys()]

    pool = Pool(processes=parallel_processes)
    while(time.time() - start < total_time):
        read_start = time.time()
        # for data, service in serv_data:
        # tmp_data = pydim.dic_sync_info_service(service)
        # if len(tmp_data) != 0:
        # data.append(tmp_data[0])
        # else:
        # data.append(None)
        new_data = pool.map(get_sync_service_series, services.values())
        for data_elem, new_data_elem in zip(res.values(), new_data):
            data_elem.append(new_data_elem)

        read_time = time.time() - read_start
        if (read_time < frequency):
            time.sleep(frequency - read_time)
        else:
            print(
                f'WARNING data collection took too long. Elapsed time {read_time} sampling interval {frequency}')

    pool.close()
    return res


def get_sync_service_series(service):
    data = pydim.dic_sync_info_service(service)
    if len(data) != 0:
        return data[0]
    else:
        return None


def check_data(host_list, total_time, counter_names, partition='EB', unit='BU'):
    unique_host_list = list(set(host_list))
    # unique_host_list = [host_list[0]]

    res = {
        key: [list() for k in range(len(unique_host_list) * 2)]
        for key in counter_names
    }
    ids = []

    k = 0
    for host in unique_host_list:
        for idx in range(2):
            # TODO change this to measure BW
            for counter in counter_names:
                service_name = f'{partition}_{host.upper()}_{unit}_{idx}/R_{unit}/{counter}'
                ids.append(
                    pydim.dic_info_service(
                        service_name,
                        lambda x, res=res[counter], k=k: res[k].append(x)))

            k += 1

    time.sleep(total_time)
    # for _ in range(total_time):
    # time.sleep(1)

    list(map(lambda idx: pydim.dic_release_service(idx), ids))

    return res


def window_scan(win_size, env, tmp, out_folder, fat_tree_file, host_file,
                json_file_name, switch_radix, pattern_file, selected_steps, duration):
    # get line count
    with open(fat_tree_file) as f:
        line_count = sum(1 for _ in f)

    with open(f'{out_folder}/scan_summary.dat', 'w') as summary_file:
        summary_file.write(f'# window step run win size {win_size}\n')

    lines = [
        list(range(n, n + win_size))
        for n in range(0, line_count - win_size + 1, 2)
    ]

    run_scan(lines, env, tmp, out_folder, fat_tree_file, host_file,
             json_file_name, switch_radix, pattern_file, selected_steps, duration)


def switch_window_scan(min_port, max_port, increment, env, tmp, out_folder,
                       fat_tree_file, host_file, json_file_name, switch_radix,
                       pattern_file, selected_steps, duration):
    # get line count
    with open(fat_tree_file) as f:
        line_count = sum(1 for _ in f)

    if ((line_count % switch_radix) != 0):
        print(f"WARNING number of entries in {fat_tree_file} are not multiple of the switch radix.\n"
              f"N entries {line_count} switch radix {switch_radix}")

    with open(f'{out_folder}/scan_summary.dat', 'w') as summary_file:
        summary_file.write(
            f'# switch window step run with range {min_port} {max_port} {increment}\n'
        )

    # TODO this is quite unreadable
    steps = range(min_port, max_port, increment)
    lines = [None] * len(steps)
    n_switches = line_count // switch_radix
    for idx, n in enumerate(steps):
        lines[idx] = [
            port for sw in range(0, n_switches)
            for port in range(sw * switch_radix + min_port, sw * switch_radix +
                              n + increment)
        ]

    run_scan(lines, env, tmp, out_folder, fat_tree_file, host_file,
             json_file_name, switch_radix, pattern_file, selected_steps, duration)


def switch_scan(pattern, env, tmp, out_folder, fat_tree_file, host_file,
                json_file_name, switch_radix, pattern_file, selected_steps, duration):
    # get line count
    with open(fat_tree_file) as f:
        line_count = sum(1 for _ in f)

    with open(f'{out_folder}/scan_summary.dat', 'w') as summary_file:
        summary_file.write(f'# switch step run with pattern {pattern}\n')

    switches = gen_config.parse_sequence(pattern)

    single_lines = [
        list(range(sw*switch_radix, (sw + 1)*switch_radix))
        for sw in switches
    ]

    lines = [sum(single_lines[: i + 1], []) for i in range(len(single_lines))]

    run_scan(lines, env, tmp, out_folder, fat_tree_file, host_file,
             json_file_name, switch_radix, pattern_file, selected_steps, duration)


def run_scan(lines, env, tmp, out_folder, fat_tree_file, host_file,
             json_file_name, switch_radix, pattern_file, steps, duration):

    metrics = ['bytes_in', 'Events/OUT']

    with open(f'{out_folder}/scan_summary.dat', 'a') as summary_file:
        summary_file.write('# step, Nu nodes | n dummy |' + '|'.join([
            f' avg {metric} | std {metric} | total {metric} '
            for metric in metrics
        ]) + '\n')

        if (steps == None):
            steps = list(range(len(lines)))

        for step_num, step_lines in [(idx, val)
                                     for idx, val in enumerate(lines)
                                     if idx in steps]:
            host_list, shift_pattern = scan_config(step_lines, tmp, out_folder,
                                                   fat_tree_file, host_file,
                                                   json_file_name, step_num,
                                                   switch_radix, pattern_file)

            if (len(host_list) > 0):
                n_dummy = np.count_nonzero(shift_pattern == -1)
                n_nodes = len(shift_pattern) - n_dummy

                summary_file.write(f'{step_num} {n_nodes} {n_dummy} ')
                run_eb.kill_gentest(host_list, 32)

                processes = scan_step(
                    f'{out_folder}/scan_{step_num}_{host_file}', env, tmp,
                    None)

                check_warm_up(host_list[0], 180)
                res = check_data(host_list, duration, metrics)

                for process in processes:
                    if process:
                        process.kill()

                run_eb.kill_gentest(host_list, 32)

                for metric in metrics:
                    with open(
                            f'{out_folder}/scan_{step_num}_{metric.replace("/","_")}_rate_raw.dat',
                            'w') as outfile:
                        outfile.write(f'#  raw scan step {step_num}\n')
                        outfile.write(f'# {"|".join(host_list)}\n')
                        for line in [
                                ' '.join(map(str, elem))
                                for elem in (zip(*res[metric]))
                        ]:
                            outfile.write(line + '\n')
                    data = np.concatenate(res[metric])
                    mean = data.mean()
                    std = data.std()
                    sum = data.sum()
                    summary_file.write(f'{mean} {std} {sum} ')

                summary_file.write('\n')
                summary_file.flush()


def main():
    parser = argparse.ArgumentParser(
        description='EB run scanner script',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument(
        "-s",
        "--setup",
        help='Setup file generated by cmsetup (like setup.x86_64-centos7-gcc9-do0.vars).',
        type=str,
        required=True)

    parser.add_argument(
        "-d",
        "--debug",
        help='Debug json config file. Check the example file provided.\n Xterm running processes are not killed by MPI.',
        default=None,
        type=str)

    parser.add_argument("-D",
                        "--dim_dns",
                        help='Override the DIM_DNS_NODE env variable value.',
                        default=None,
                        type=str)

    parser.add_argument(
        "-p",
        "--pattern",
        help='Shift pattern file name. This must match the file included in the transport unit options file',
        default='EB_transport_shift.opts',
        type=str)

    parser.add_argument(
        "-t",
        "--tmp",
        help='Shared tmp directory. This directory needs to be writable by the machine running this script and readable by all the machines.',
        default=os.getcwd(),
        type=str)

    parser.add_argument(
        "-T",
        "--time",
        help='Duration of a single step of the scan in seconds.',
        default=120,
        type=int)

    parser.add_argument(
        "-o",
        "--out",
        help='Shared output directory. This directory needs to be writable by the machine running this script and readable by all the machines.',
        default=os.getcwd(),
        type=str)

    parser.add_argument("-f",
                        "--fat_tree_file",
                        help='Optimal shift patter generated by opensm.',
                        type=str,
                        required=True)

    parser.add_argument(
        "-r",
        "--radix",
        help='switch radix (number of ports connected to the nodes).',
        type=int,
        default=20,
        required=False)

    parser.add_argument("-j",
                        "--json",
                        help='Ouput json config file name.',
                        type=str,
                        default='config.json',
                        required=False)

    parser.add_argument("-H",
                        "--hostfile",
                        help='Ouput host file.',
                        type=str,
                        default='host.txt',
                        required=False)

    parser.add_argument(
        "-e",
        "--step",
        help='Selects a subset of steps in the selected configuration e.g. 0,1,2,5-10',
        type=str,
        required=False,
        default=None)

    scan = parser.add_mutually_exclusive_group(required=True)

    scan.add_argument(
        "-S",
        "--switch",
        help='Scan pattern adds switches with a specific port pattern e.g. 0,1,2,5-10',
        type=str)

    scan.add_argument(
        "-N",
        "--node",
        nargs=3,
        metavar=('MIN', 'MAX', 'INCREMENT'),
        help='Node pattern adds nodes from a specific range (indices from the ftree config file)',
        type=int)

    scan.add_argument(
        "-W",
        "--window",
        help='Node pattern scans the full network with window of size WINDOW (indices from the ftree config file)',
        type=int)

    scan.add_argument(
        "-w",
        "--switch_window",
        nargs=3,
        metavar=('MIN', 'MAX', 'INCREMENT'),
        help='Scan pattern adds switches with a specific port range (indices from 0 to radix-1)',
        type=int)

    args = parser.parse_args()

    env = run_eb.set_env(args.setup)

    if (args.dim_dns):
        env['DIM_DNS_NODE'] = args.dim_dns
        pydim.dic_set_dns_node(args.dim_dns)

    if (args.step != None):
        steps = gen_config.parse_sequence(args.step)
    else:
        steps = None

    if (args.window != None):
        window_scan(args.window, env, args.tmp, args.out, args.fat_tree_file,
                    args.hostfile, args.json, args.radix, args.pattern, steps, args.time)
    elif (args.switch_window != None):
        switch_window_scan(args.switch_window[0], args.switch_window[1],
                           args.switch_window[2], env, args.tmp, args.out,
                           args.fat_tree_file, args.hostfile, args.json,
                           args.radix, args.pattern, steps, args.time)

    elif (args.switch != None):
        switch_scan(args.switch, env, args.tmp, args.out, args.fat_tree_file,
                    args.hostfile, args.json, args.radix, args.pattern, steps, args.time)
    return 0

    host_list = scan_config([0, 1], args.tmp, args.out, args.fat_tree_file,
                            args.hostfile, args.json, 0, args.radix,
                            args.pattern)

    if (len(host_list) > 0):
        scan_step(f'{args.out}/scan_{0}_{args.hostfile}', env, args.tmp, None)

        check_warm_up(host_list[0])
        check_data(host_list, 30)


if __name__ == "__main__":
    main()
