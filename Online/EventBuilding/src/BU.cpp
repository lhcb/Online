#include "EventBuilding/BU.hpp"
#include "PCIE40Data/sodin.h"
#include "pcie40_reader.hpp"
#include "shmem_buffer_writer.hpp"
#include "MEP_injector.hpp"
#include "dummy_mep_buffer_writer.hpp"
#include "watchdog.hpp"
#include "RTL/rtl.h"
#include "unused.hpp"
#include <sstream>
#include <algorithm>
#include <numeric>
#include <cstdlib>
#include <stdexcept>
#include <system_error>
#include <chrono>
#include <iterator>

const std::string EB::BU_buffer_type_to_string(BU_buffer_types type)
{
  switch (type) {
  case dummy_MEP_buffer: return "dummy_MEP_buffer";
  case shmem_MEP_buffer: return "shmem_MEP_buffer";
  case MBM_MEP_buffer: return "MBM_MEP_buffer";
  case MEP_injector_buffer: return "MEP_injector_buffer";
  default: return "Error unkown buffer type";
  }
}

std::ostream& EB::operator<<(std::ostream& os, BU_buffer_types type) { return os << BU_buffer_type_to_string(type); }

EB::BU::BU(const std::string& nam, DataflowContext& ctxt) : Transport_unit(nam, ctxt)
{
  // Property declaration
  declareProperty("buffer_type", _buffer_type);
  declareProperty("shmem_prefix", _shmem_prefix = std::string("BU"));
  declareProperty("buffer_size", _prop_buffer_sizes);
  declareProperty("discard_buffer_size", _prop_discard_buffer_size = 5);
  declareProperty("MBM_name", _mbm_name);
  declareProperty("MDF_filename", _MDF_filename);
  declareProperty("packing_factor", _packing_factor = 10);
  declareProperty("n_meps", _n_meps = 1);

  declareProperty("write_to_file_single", _write_to_file_single = false);
  declareProperty("write_to_file_dim_enable", _write_to_file_dim_enable = false);
  declareProperty("write_to_file_per_run", _write_to_file_per_run = false);
  declareProperty("write_to_file", _write_to_file);
  declareProperty("MEP_dump_dim_service", _MEP_dump_dim_service = std::string("LbEbRunInfoComm/dump_meps"));
  declareProperty("out_file_prefix", _out_file_prefix = std::string("BU"));
  declareProperty("n_meps_to_file", _n_meps_to_file = 1);
  declareProperty("stop_timeout", _stop_timeout = 10);
  declareProperty("sort_src_ids", _sort_src_ids = true);

  // Monitoring counters declaration
  declareMonitor("MEP_counter", _MEP_count, "Number of build MEPs in the RUN");
  declareMonitor("discarted_MEP_counter", _discarted_MEP_count, "Number of discarted MEPs in the RUN");
  declareMonitor("incomplete_MEP_counter", _incomplete_MEP_count, "Number of incomplete MEPs in the RUN");
  declareMonitor("corrupted_MEP_counter", _corrupted_MEP_count, "Number of corrupted MEPs in the RUN");

  // DEBUG counters
  declareMonitor("run_loop_iteration", _run_loop_iteration, "Iteration fo the run loop in the RUN");
  // profiling counters
  declareMonitor("receive_size_time_counter", _receive_size_time_counter);
  declareMonitor("calc_offsets_time_counter", _calc_offsets_time_counter);
  declareMonitor("build_header_time_counter", _build_header_time_counter);
  declareMonitor("linear_shift_time_counter", _linear_shift_time_counter);
  declareMonitor("receive_MFPs_time_counter", _receive_MFPs_time_counter);

  logger.set_name(name);
}

EB::BU::~BU() {}

int EB::BU::initialize()
{
  logger.info() << "initialize" << std::flush;
  int sc = Transport_unit::initialize();
  if (sc != DF_SUCCESS) {
    return error("Failed to initialize Transport_unit base class.");
  }

  _my_idx = get_idx(_bu_ranks.begin(), _bu_ranks.end(), _my_rank);
  if (_my_idx < 0) {
    logger.error() << __FUNCTION__ << " this node should not be a BU." << std::flush;
    return DF_ERROR;
  }

  sc = init_shift();
  if (sc != DF_SUCCESS) {
    logger.error() << __FUNCTION__ << " init shift failed" << std::flush;
    return DF_ERROR;
  }

  _sizes.resize(_prefix_n_sources_per_ru.back());
  std::fill(_sizes.begin(), _sizes.end(), 0);

  _data_offset_words.resize(_prefix_n_sources_per_ru[_ru_ranks.size()], 0);
  std::fill(_data_offset_words.begin(), _data_offset_words.end(), 0);

  _dummy.resize(_n_sources_per_ru.back());
  std::fill(_dummy.begin(), _dummy.end(), 0);

  sc = config_buffer();
  if (sc != DF_SUCCESS) {
    return sc;
  }

  sc = connect_n_mep_info();
  if (sc != DF_SUCCESS) {
    return sc;
  }

  // open DEBUG dump file
  _n_meps_written_to_file = 0;
  if (is_file_writer_enable() and !_write_to_file_per_run) {
    std::stringstream file_name;
    file_name << _out_file_prefix << "_" << _my_idx << ".mep";
    try {
      _file_writer = File_writer<EB::MEP>(file_name.str());
    } catch (const std::exception& e) {
      logger.warning() << "Unable to open DEBUG data to file. " << e.what() << ". Continuing..." << std::flush;
    }
  }

  // TODO check how error handling should be done here
  // returning DF_ERROR may not be enough
  return DF_SUCCESS;
}

int EB::BU::start()
{
  int ret_val = DF_SUCCESS;
  logger.info() << "start" << std::flush;
  auto buflist = _recv_buff->get_full_buffer();
  auto discard_buflist = _discard_buff->get_full_buffer();
  logger.debug() << "buffer information received" << std::flush;
  buflist.insert(
    buflist.begin(), std::make_move_iterator(discard_buflist.begin()), std::make_move_iterator(discard_buflist.end()));
  logger.debug() << "discard information received" << std::flush;

  // start verbs
  int sc = Transport_unit::start();
  if (sc != DF_SUCCESS) {
    return error("Failed to start Transport_unit base class.");
  }

  std::timed_mutex watchdog_mtx;
  {
    std::lock_guard lk(watchdog_mtx);
    Watchdog<int, std::ratio<1>> watchdog(
      watchdog_mtx, std::chrono::duration<int>(_IB_connection_timeout), [this]() { this->cancel(); });
    // exchange srcids
    ret_val = receive_src_ids();
    if (ret_val != DF_SUCCESS) {
      return ret_val;
    }

    // exchange src_names
    ret_val = receive_src_names();
    if (ret_val != DF_SUCCESS) {
      return ret_val;
    }
    // we unlock explicitly to prevent destructors races between the watchdog and the lock_guard
    watchdog_mtx.unlock();
  }

  ret_val = map_src_id_names();
  if (ret_val != DF_SUCCESS) {
    return ret_val;
  }

  ret_val = sort_src_ids();
  if (ret_val != DF_SUCCESS) {
    return ret_val;
  }

  // recv data mr
  logger.debug() << "init data MRs" << std::endl;
  for (auto& list_it : buflist) {
    ret_val = _ibComm->addMR((char*) std::get<void*>(list_it), std::get<size_t>(list_it));
    if (ret_val != DF_SUCCESS) {
      logger.error() << __FUNCTION__ << " addMR failed: " << strerror(errno) << std::flush;
      return ret_val;
    }
    logger.debug() << "MRs of size " << std::get<size_t>(list_it) << " allocated" << std::flush;
  }
  logger.debug() << "data MRs allocated" << std::flush;

  // recv sizes mr
  ret_val = _ibComm->addMR((char*) &_sizes, _sizes.size() * sizeof(uint32_t));
  if (ret_val != DF_SUCCESS) {
    logger.error() << __FUNCTION__ << " addMR failed: " << strerror(errno) << std::flush;
    return ret_val;
  }
  logger.debug() << "sizes MRs allocated" << std::flush;

  std::lock_guard<decltype(_start_lock)> guard(_start_lock);
  _receive_MEPs = true;
  reset_counters();
  _end_of_run = true;
  _old_end_of_run = true;
  return DF_SUCCESS;
}

int EB::BU::stop()
{
  int ret_val = DF_SUCCESS;
  logger.info() << "stop" << std::flush;
  std::unique_lock<std::timed_mutex> guard(_loop_lock, std::chrono::duration<int>(_stop_timeout));
  if (guard) {
    logger.info() << "stop executed properly" << std::flush;
  } else {
    logger.error() << "Unable to execute a clean stop. Aborting" << std::flush;
    ret_val = DF_ERROR;
  }

  _recv_buff->reset_cancel();
  _discard_buff->reset_cancel();

  int sc = Transport_unit::stop();
  if (sc != DF_SUCCESS) {
    return error("Failed to stop Transport_unit base class.");
  }
  if (ret_val != DF_SUCCESS) {
    return ret_val;
  }
  return ret_val;
}

int EB::BU::cancel()
{
  // TODO check race condition between start and cancel
  std::lock_guard<decltype(_start_lock)> guard(_start_lock);
  _receive_MEPs = false;
  Transport_unit::cancel();
  _recv_buff->cancel();
  _discard_buff->cancel();

  return DF_SUCCESS;
}

int EB::BU::pause()
{
  // TODO implement this
  logger.info() << "BU pause" << std::flush;
  // the EB does not go into pause
  // _receive_MEPs = false;
  return DF_SUCCESS;
}

int EB::BU::finalize()
{
  logger.info() << "BU finalize" << std::flush;
  delete _recv_buff;
  return Transport_unit::finalize();
}

void EB::BU::handle(const DataflowIncident& UNUSED(inc))
{
  logger.info() << "BU incident" << std::flush;
  //  TODO implement this
}

int EB::BU::check_buffer()
{
  int ret_val = DF_SUCCESS;

  _buffer_sizes.resize(_prop_buffer_sizes.size());
  std::transform(_prop_buffer_sizes.begin(), _prop_buffer_sizes.end(), _buffer_sizes.begin(), [](auto val) {
    return val * 1024 * 1024 * 1024UL;
  });

  if (_buffer_type.size() == 0) {
    _buffer_type.resize(_bu_ranks.size(), default_BU_buffer_type);
    logger.warning() << __FUNCTION__ << " no buffer types provided setting to default value "
                     << EB::default_BU_buffer_type << std::flush;
  } else if (_buffer_type.size() == 1) {
    _buffer_type.resize(_bu_ranks.size(), _buffer_type[0]);
    logger.warning() << __FUNCTION__ << " Single buffer type provided: " << _buffer_type[0] << std::flush;
  }

  if (_buffer_type.size() != _bu_ranks.size()) {
    logger.error() << __FUNCTION__ << " Configuration error: not enough buffer types provided " << _buffer_type.size()
                   << "/" << _bu_ranks.size() << std::flush;

    ret_val = DF_ERROR;
  }

  if (_buffer_sizes.size() == 0) {
    _buffer_sizes.resize(_bu_ranks.size(), default_BU_buffer_size);
    logger.warning() << __FUNCTION__ << " no buffer sizes provided setting to default value "
                     << EB::default_BU_buffer_size << std::flush;
  } else if (_buffer_sizes.size() == 1) {
    _buffer_sizes.resize(_bu_ranks.size(), _buffer_sizes[0]);
    logger.warning() << __FUNCTION__ << " Single buffer size provided: " << _buffer_sizes[0] << std::flush;
  }

  if (_buffer_sizes.size() != _bu_ranks.size()) {
    logger.error() << __FUNCTION__ << " Configuration error: not enough buffer sizes provided " << _buffer_sizes.size()
                   << "/" << _bu_ranks.size() << std::flush;

    ret_val = DF_ERROR;
  }

  if (_write_to_file.size() == 0) {
    _write_to_file.resize(_bu_ranks.size(), false);
    logger.warning() << __FUNCTION__ << " write to file was not set, setting it to the default value " << false
                     << std::flush;
  } else if (_write_to_file.size() == 1) {
    _write_to_file.resize(_bu_ranks.size(), _write_to_file[0]);
    logger.warning() << __FUNCTION__ << " Single write to file provided: " << _write_to_file[0] << std::flush;
  }

  if (_write_to_file.size() != _bu_ranks.size()) {
    logger.error() << __FUNCTION__ << " Configuration error: not enough write to file provided "
                   << _write_to_file.size() << "/" << _bu_ranks.size() << std::flush;

    ret_val = DF_ERROR;
  }

  if (_mbm_name.size() == 0) {
    _mbm_name.resize(_bu_ranks.size(), EB::default_MBM_name);
    logger.warning() << __FUNCTION__ << " no MBM names provided setting to default value " << EB::default_MBM_name
                     << std::flush;
  } else if (_mbm_name.size() == 1) {
    _mbm_name.resize(_bu_ranks.size(), _mbm_name[0]);
    logger.warning() << __FUNCTION__ << " single MBM name provided: " << _mbm_name[0] << std::flush;
  }

  if (_mbm_name.size() != _bu_ranks.size()) {
    logger.error() << __FUNCTION__ << " Configuration error: not enough MBM names provided " << _mbm_name.size() << "/"
                   << _bu_ranks.size() << std::flush;

    ret_val = DF_ERROR;
  }

  return ret_val;
}

int EB::BU::sort_src_ids()
{
  int ret_val = DF_SUCCESS;

  // TODO we can probably use the sorted array. It looks like the unsorted one is never used.
  _sorted_src_ids = _src_ids;
  _src_id_idx_to_ru_src_idx.resize(_src_ids.size());
  _ru_src_idx_to_src_id_idx.resize(_src_ids.size());
  std::sort(_sorted_src_ids.begin(), _sorted_src_ids.end());

  std::vector<src_id_type> duplicated_src_ids(_src_ids.size(), -1);
  auto duplicated_src_ids_end =
    find_all_rep(_sorted_src_ids.begin(), _sorted_src_ids.end(), duplicated_src_ids.begin());

  if (duplicated_src_ids.begin() != duplicated_src_ids_end) {
    logger.error() << "Duplicated src ids: ";
    for (auto it = duplicated_src_ids.begin(); it != duplicated_src_ids_end; it++) {
      logger.error() << std::hex << "0x" << *it << std::dec << " ";
    }
    logger.error() << std::flush;

    ret_val = DF_ERROR;
  }

  // TODO we need the sorted list to get the repeated values
  if (!_sort_src_ids) {
    _sorted_src_ids = _src_ids;
  }

  std::transform(
    _sorted_src_ids.begin(),
    _sorted_src_ids.end(),
    _src_id_idx_to_ru_src_idx.begin(),
    [&vec = _src_ids](src_id_type elem) { return get_idx(vec.begin(), vec.end(), elem); });

  std::transform(
    _src_ids.begin(), _src_ids.end(), _ru_src_idx_to_src_id_idx.begin(), [&vec = _sorted_src_ids](src_id_type elem) {
      return get_idx(vec.begin(), vec.end(), elem);
    });

  if (logger.is_active(Online::PrintLevel::DEBUG)) {
    logger.debug() << "Source id ordering: ";
    for (const auto& elem : _sorted_src_ids) {
      logger.debug() << std::hex << "0x" << elem << std::dec << ", ";
    }
    logger.debug() << std::flush;
  }

  return ret_val;
}

int EB::BU::connect_n_mep_info()
{
  auto UTGID_tokens = str_split(RTL::processName(), "_");
  _MEP_dump_dim_service = UTGID_tokens[0] + "_" + _MEP_dump_dim_service;
  try {
    _n_MEPS_info = std::make_unique<DimInfo>(_MEP_dump_dim_service.c_str(), 0);
  } catch (const std::exception& e) {
    logger.error() << __FUNCTION__ << e.what() << std::flush;
    return DF_ERROR;
  }

  // It is possible that the class is not ready and the data point to NULL
  // if we wait here we ensure no segfault later on
  while (_n_MEPS_info->getData() == NULL) {
    usleep(1);
  }

  return DF_SUCCESS;
}

int EB::BU::map_src_id_names()
{
  int ret_val = DF_SUCCESS;
  _src_id_names_maps.clear();
  for (size_t k = 0; k < _src_ids.size(); k++) {
    _src_id_names_maps[_src_ids[k]] = _src_names[k];
  }

  return ret_val;
}

int EB::BU::config_buffer()
{
  int ret_val = DF_SUCCESS;

  ret_val = check_buffer();
  if (ret_val != DF_SUCCESS) {
    return ret_val;
  }

  try {
    if (_buffer_type[_my_idx] == dummy_MEP_buffer) {
      config_dummy();
    } else if (_buffer_type[_my_idx] == shmem_MEP_buffer) {
      config_shmem();
    } else if (_buffer_type[_my_idx] == MBM_MEP_buffer) {
      if (!context.mbm) {
        ret_val = error("Failed to access MBM client.");
      } else {
        config_mbm();
      }
    } else if (_buffer_type[_my_idx] == MEP_injector_buffer) {
      // if (!context.mbm) {
      // ret_val = error("Failed to access MBM client.");
      // } else {
      config_MEP_injector();
      // }
    } else {
      logger.error() << __FUNCTION__ << " unsupported buffer type " << _buffer_type[_my_idx] << std::flush;
      ret_val = DF_ERROR;
    }

    _discard_buff = new Dummy_mep_buffer_writer(_prop_discard_buffer_size * 1024 * 1024 * 1024UL, _numa_node);
  } catch (const std::system_error& e) {
    logger.error() << __FUNCTION__ << " unable to configure buffer type" << BU_buffer_types(_buffer_type[_my_idx])
                   << ". " << e.what() << ": " << strerror(errno) << ". Error code: " << e.code() << std::flush;
    ret_val = DF_ERROR;
    return ret_val;
  } catch (const std::exception& e) {
    logger.error() << __FUNCTION__ << " unable to configure buffer type " << BU_buffer_types(_buffer_type[_my_idx])
                   << ". " << e.what() << ": " << strerror(errno) << std::flush;
    ret_val = DF_ERROR;
    return ret_val;
  } catch (...) {
    logger.error() << __FUNCTION__ << " unexpected exception buffer type " << BU_buffer_types(_buffer_type[_my_idx])
                   << std::flush;
    ret_val = DF_ERROR;
    return ret_val;
  }

  return ret_val;
}

void EB::BU::config_dummy() { _recv_buff = new Dummy_mep_buffer_writer(_buffer_sizes[_my_idx], _numa_node); }

void EB::BU::config_shmem()
{
  std::stringstream buff_name;
  buff_name << _shmem_prefix << "_" << _my_idx;
  _recv_buff = new Shmem_buffer_writer<EB::MEP>(buff_name.str(), _buffer_sizes[_my_idx]);
}

void EB::BU::config_mbm() { _recv_buff = new Mbm_writer<EB::MEP>(context, RTL::processName(), _mbm_name[_my_idx]); }

void EB::BU::config_MEP_injector()
{
  std::stringstream file_name;
  file_name << _out_file_prefix << "_" << _my_idx << "_injector"
            << ".mep";
  _recv_buff = new MEP_injector(
    _MDF_filename.c_str(),
    _buffer_sizes[_my_idx],
    _numa_node,
    _packing_factor,
    _n_meps,
    context,
    RTL::processName(),
    _mbm_name[_my_idx],
    _write_to_file[_my_idx],
    file_name.str(),
    _n_meps_to_file);
}

int EB::BU::init_shift()
{
  // There is at least on RU for every BU

  int id = 0;
  // this is the shift phase of the current unit i.e. the unit index plus the number of ghost before the unit itself

  std::vector<std::vector<int>> ru_blocks(_n_rus_per_nic.size());

  std::transform(_n_rus_per_nic.begin(), _n_rus_per_nic.end(), ru_blocks.begin(), [&id](int val) {
    std::vector<int> ret_val(val);
    std::iota(ret_val.begin(), ret_val.end(), id);
    id += val;
    return ret_val;
  });

  _shift_offset.resize(_RU_shift_pattern.size());
  std::transform(_RU_shift_pattern.rbegin(), _RU_shift_pattern.rend(), _shift_offset.begin(), [&ru_blocks](int val) {
    return (val >= 0) ? ru_blocks[val] : std::vector<int>(1, -1);
  });

  int shift_idx = get_idx(_BU_shift_pattern.begin(), _BU_shift_pattern.end(), _my_idx);

  logger.debug() << "shift idx " << shift_idx << std::flush;

  std::rotate(_shift_offset.begin(), _shift_offset.end() - shift_idx - 1, _shift_offset.end());

  if (logger.is_active(Online::PrintLevel::DEBUG)) {
    logger.debug() << "shift vector ";
    for (auto block : _shift_offset) {
      for (auto elem : block) {
        logger.debug() << elem << " ";
      }
      logger.debug() << ", ";
    }

    logger.debug() << std::flush;
  }

  return DF_SUCCESS;
}

int EB::BU::run()
{
  // reset the number of MEPs written to file
  _n_meps_written_to_file = 0;
  int ret_val = DF_SUCCESS;
  logger.info() << "BU run" << std::flush;
  // Transport_unit::run();
  init_profiling();
  while (_receive_MEPs) {
    // This lock is released when a full iteration is completed, and it used to detect stuck functions
    std::lock_guard<decltype(_loop_lock)> guard(_loop_lock);

    // additional sync barrier
    ret_val = sync(true);
    if (ret_val != DF_SUCCESS) {
      break;
    }

    logger.debug() << "recv sizes" << std::flush;
    ret_val = receive_sizes();

    // DF_CANCELLED means that the STOP signal has been received
    _build_header_timer.start();
    if (ret_val == DF_CANCELLED) {
      break;
    } else if (ret_val != DF_SUCCESS) {
      break;
    }

    if (!_end_of_run) {
      ret_val = get_next_MEP_space();
      if (ret_val != DF_SUCCESS) {
        break;
      }
      ret_val = build_MEP_header();
      if (ret_val != DF_SUCCESS) {
        break;
      }
    } else {
      _curr_MEP = NULL;
    }
    _build_header_timer.stop();

    logger.debug() << "shift" << std::flush;
    ret_val = linear_shift();
    if (ret_val != DF_SUCCESS) {
      break;
    }

    // At the end of the run there is no MEP to check
    if (!_end_of_run) {
      if (_incomplete) {
        _incomplete_MEP_count++;
        // The number of events can only be calculated for valid MEPs
        _DF_events_err += 1;
        _recv_buff->write_discard();
      } else if (!_curr_MEP->is_valid()) {
        // TODO check what to do, should this be fatal? Do we discard the data (how?)
        logger.error() << "CORRUPTED MEP received: ";
        if (!_curr_MEP->is_magic_valid()) {
          logger.error() << " corrupted header" << std::flush;
        } else {
          // logger.error() << _curr_MEP->print() << std::flush;
          logger.error() << "Inconsistent EV ids "
                         << "; ";
          auto sodin_ev_id = _curr_MEP->at(0)->header.ev_id;
          auto ev_id_distr = _curr_MEP->get_ev_id_distribution();
          logger.error() << "sodin ev ID " << sodin_ev_id << " n sources " << ev_id_distr[sodin_ev_id].size() << "; ";
          for (auto it = ev_id_distr.begin(); it != ev_id_distr.end(); it++) {
            auto ev_id = it->first;
            auto sources = it->second;
            if (ev_id != sodin_ev_id) {
              logger.error() << "ev ID " << ev_id << " sources: ";
              for (const auto& elem : sources) {
                logger.error() << std::hex << "0x" << elem << std::dec << "/" << _src_id_names_maps[elem] << ", ";
              }
              logger.error() << "; ";
            }
          }
          logger.error() << std::flush;
        }
        _recv_buff->write_discard();
        _corrupted_MEP_count++;
        // The number of events can only be calculated for valid MEPs
        _DF_events_err += 1;
      } else {
        int n_events = (_curr_MEP->at(0))->header.n_banks;
        _DF_events_in += n_events;
        // debug print
        if (logger.is_active(Online::PrintLevel::DEBUG)) {
          logger.debug() << "active MEP\n";
          if (logger.is_active(Online::PrintLevel::VERBOSE)) {
            logger.debug() << _curr_MEP->print(false);
            logger.debug() << std::flush;
          } else {
            logger.debug() << _curr_MEP->print(false);
          }
          logger.debug() << std::flush;
        }

        // debug write to file
        // TODO this should be moved in another function
        if (!_end_of_run && _old_end_of_run && _write_to_file_per_run && is_file_writer_enable()) {
          // reset the number of MEPs to file to retrigger is_file_writer_enable
          _n_meps_written_to_file = 0;
          // if no writer is enabled there is no need to open a new file
          if (is_meps_file_writer_continue()) {
            reset_file_writer();
          } else {
            // if no more MEPs are required we close the file
            _file_writer = File_writer<EB::MEP>();
          }
        }

        if (is_file_writer_enable() && is_meps_file_writer_continue() && _file_writer) {
          try {
            int err_val = _file_writer.write(_curr_MEP);
            if (err_val < _curr_MEP->bytes()) {
              logger.warning() << "Unable to write DEBUG data to file. Continuing..." << std::flush;
            }
          } catch (const std::exception& e) {
            logger.warning() << "Unable to write DEBUG data to file." << e.what() << ". Continuing..." << std::flush;
          }
          _n_meps_written_to_file++;
        }

        // update counters
        if (_discarted) {
          _discarted_MEP_count++;
        } else {
          _DF_events_out += n_events;
          _MEP_count++;
          _bytes_out += _curr_MEP->bytes();
        }
      }
      // Ack all the buffers
      // after write complete _curr_MEP is not valid
      try {
        _discard_buff->write_complete();
        _recv_buff->write_complete();
      } catch (const std::exception& e) {
        logger.error() << __FUNCTION__ << " " << e.what() << std::flush;
        ret_val = DF_ERROR;
        break;
      } catch (...) {
        logger.error() << __FUNCTION__ << " unexpected exception buffer write_complete" << std::flush;
        ret_val = DF_ERROR;
        break;
      }
      // _DF_events_out += n_events;
      _curr_MEP = NULL;
    }

    if (logger.is_active(Online::PrintLevel::DEBUG)) {
      logger.debug() << "sleep" << std::flush;
      sleep(1);
    }

    update_profiling();

    _run_loop_iteration++;
  }

  // this will trigger the transition of the FSM into error
  if (ret_val == DF_ERROR) {
    fireIncident("DAQ_ERROR");
  }

  return ret_val;
}

// TODO this function does too many things
int EB::BU::receive_sizes()
{
  _receive_size_timer.start();
  int ret_val = DF_SUCCESS;
  std::vector<uint32_t> wrids = _ibComm->ibGatherV(
    reinterpret_cast<char*>(_sizes.data()),
    _n_sources_per_ru.data(),
    _prefix_n_sources_per_ru.data(),
    sizeof(uint32_t),
    _ru_ranks,
    ret_val);
  if (ret_val != DF_SUCCESS) {
    return ret_val;
  }

  int test_recv_status;
  do {
    test_recv_status = _ibComm->ibTestRecvs(wrids);

    if (test_recv_status == -1) {
      logger.error() << __FUNCTION__ << " ibTestRecvs " << test_recv_status << std::flush;
      return DF_ERROR;
    }
  } while ((test_recv_status == 1) && (_receive_MEPs));

  if (!_receive_MEPs) {
    logger.info() << "STOP received cancelling pending IO" << std::flush;
    // TODO check if an IO canc is needed
    return DF_CANCELLED;
  }

  _receive_size_timer.stop();
  // TODO this may be a good spot to split this function
  _calc_offsets_timer.start();

  // TODO end of run detection may go into a different function
  _old_end_of_run = _end_of_run;
  _end_of_run = false;

  std::vector<int> no_data_src_ids;
  no_data_src_ids.reserve(_data_offset_words.size());
  // first offset is the header size
  // TODO mem size should already include padding
  _data_offset_words[0] = (EB::mep_header_mem_size(_data_offset_words.size()) +
                           get_padding(EB::mep_header_mem_size(_data_offset_words.size()), 4096)) /
                          EB::MEP_WORD_SIZE;
  for (size_t k = 1; k < _src_ids.size(); k++) {
    const auto ru_idx = _src_id_idx_to_ru_src_idx[k - 1];
    const auto src_id = _sorted_src_ids[k - 1];

    if (_sizes[ru_idx] == 0) {
      _end_of_run = true;
      no_data_src_ids.push_back(src_id);
    }

    // Every MFP should be page aligned
    size_t padded_size = (_sizes[ru_idx] + get_padding(_sizes[ru_idx], 4096)) / EB::MEP_WORD_SIZE;
    _data_offset_words[k] = _data_offset_words[k - 1] + padded_size;
  }

  const auto last_src_id = _sorted_src_ids[_data_offset_words.size() - 1];
  const auto last_ru_idx = _src_id_idx_to_ru_src_idx[_data_offset_words.size() - 1];

  // check the last valid element, the last element of sizes is set to 0 and is needed by the MPI_Gater call
  if (_sizes[last_ru_idx] == 0) {
    _end_of_run = true;
    no_data_src_ids.push_back(last_src_id);
  }

  _full_size_words = _data_offset_words[_data_offset_words.size() - 1] + _sizes[last_ru_idx] / EB::MEP_WORD_SIZE;

  _incomplete = false;

  if (_end_of_run) {
    if (_full_size_words == _data_offset_words[0]) {
      _full_size_words = 0;
      std::fill(_data_offset_words.begin(), _data_offset_words.end(), 0);
      logger.warning() << "end of run" << std::flush;
    } else {
      // If only a subset of the MFPs are 0-sized the MEP is incomplete the src ids are reported and the BU goes into
      // ERROR
      _incomplete = true;
      logger.error() << "Incomplete event! No data received from the following sources: ";
      for (const auto& src : no_data_src_ids) {
        logger.error() << std::hex << "0x" << src << std::dec << "/" << _src_id_names_maps[src] << " ";
        _incomplete_MEP_srcs[src]++;
      }

      logger.error() << std::flush;

      _end_of_run = false;
    }
  }

  // TODO this should be moved in another function
  if (!_end_of_run && _old_end_of_run) {
    // reset counters on the start of the new run
    reset_counters();
  }

  if (logger.is_active(Online::PrintLevel::DEBUG)) {
    logger.debug() << "sizes ";
    for (const auto& size : _sizes) {
      logger.debug() << size << " ";
    }
    logger.debug() << std::flush;

    logger.debug() << "offsets ";
    for (const auto& elem : _data_offset_words) {
      logger.debug() << elem << " ";
    }
    logger.debug() << std::flush;
    logger.debug() << "full MEP size " << _full_size_words * EB::MEP_WORD_SIZE << std::flush;
  }

  _calc_offsets_timer.stop();

  return DF_SUCCESS;
}

int EB::BU::receive_src_ids()
{
  logger.debug() << "receive src ids start" << std::flush;
  int ret_val = DF_SUCCESS;
  _src_ids.resize(_prefix_n_sources_per_ru.back());

  ret_val = _ibComm->ibGatherBlockV(
    reinterpret_cast<char*>(_src_ids.data()),
    _n_sources_per_ru.data(),
    _prefix_n_sources_per_ru.data(),
    sizeof(EB::src_id_type),
    _ru_ranks);
  if (ret_val != DF_SUCCESS) {
    logger.error() << __FUNCTION__ << " ibGatherBlockV failed " << ret_val << std::flush;
    return ret_val;
  }

  logger.debug() << "src ids written " << _src_ids.size() << ": " << std::flush;

  if (logger.is_active(Online::PrintLevel::DEBUG)) {
    for (const auto& id : _src_ids) {
      logger.debug() << id << "  ";
    }
    logger.debug() << std::flush;
  }

  _ibComm->ibDeregMRs(); // clear memory regions

  logger.debug() << "receive src ids end" << std::flush;
  return ret_val;
}

int EB::BU::receive_src_names()
{
  logger.debug() << "receive src names start" << std::flush;
  int ret_val = DF_SUCCESS;
  _src_names.resize(_prefix_n_sources_per_ru.back());
  auto tmp_buffer = new char[_src_names.size()][PCIe40_unique_name_str_length];

  ret_val = _ibComm->ibGatherBlockV(
    reinterpret_cast<char*>(tmp_buffer),
    _n_sources_per_ru.data(),
    _prefix_n_sources_per_ru.data(),
    PCIe40_unique_name_str_length,
    _ru_ranks);
  if (ret_val != DF_SUCCESS) {
    logger.error() << __FUNCTION__ << " size ibGatherBlockV failed " << ret_val << std::flush;
    return ret_val;
  }

  for (size_t k = 0; k < _src_names.size(); k++) {
    _src_names[k] = tmp_buffer[k];
    logger.debug() << "buffer name : " << _src_names[k] << std::endl;
  }

  logger.debug() << "src names written " << _src_names.size() << ": " << std::flush;

  if (logger.is_active(Online::PrintLevel::DEBUG)) {
    for (const auto& src_name : _src_names) {
      logger.debug() << src_name << "  ";
    }
    logger.debug() << std::flush;
  }

  delete tmp_buffer;

  _ibComm->ibDeregMRs(); // clear memory regions

  logger.debug() << "receive src names end" << std::flush;
  return ret_val;
}

int EB::BU::linear_shift()
{
  _linear_shift_timer.start();
  int ret_val = DF_SUCCESS;
  for (const auto& shift_vec : _shift_offset) {
    logger.debug() << "sync" << std::flush;
    _sync_timer.start();
    ret_val = sync(false);
    if (ret_val != DF_SUCCESS) {
      return ret_val;
    }
    _sync_timer.stop();
    _receive_MFPs_timer.start();
    logger.debug() << "recv MFPs" << std::flush;
    // we skip the ghost nodes
    if (shift_vec[0] != -1) {
      ret_val = receive_MFPs(shift_vec);
      if (ret_val != DF_SUCCESS) {
        return ret_val;
      }
    }

    _receive_MFPs_timer.stop();
    if (logger.is_active(Online::PrintLevel::DEBUG)) {
      logger.debug() << "on shift ";
      for (const auto& elem : shift_vec) {
        logger.debug() << elem << " ";
      }
      logger.debug() << std::flush;
    }
  }

  _linear_shift_timer.stop();
  return ret_val;
}

int EB::BU::receive_MFPs(const std::vector<int>& shift_vec)
{
  int comm_err = 0;
  std::vector<comm_recv> recv_vect;
  // TODO find a better value for this
  recv_vect.reserve(_n_sources_per_ru[0] * shift_vec.size());
  size_t total_size = 0;
  int src = 0;
  for (const auto& ru_idx : shift_vec) {
    src = _ru_ranks[ru_idx];
    for (unsigned int k = 0; k < _n_sources_per_ru[ru_idx]; k++) {
      auto idx = _prefix_n_sources_per_ru[ru_idx] + k;
      auto src_id_idx = _ru_src_idx_to_src_id_idx[idx];
      const auto& size = _sizes[idx];
      // Inplace construction with a proper initializer is more elegant than this
      recv_vect.emplace_back();
      auto& recv = recv_vect.back();
      if (_curr_MEP != NULL) {
        recv.recv_buff = reinterpret_cast<void*>(_curr_MEP->at(src_id_idx));
      } else {
        recv.recv_buff = NULL;
      }
      recv.datatype = sizeof(char);
      recv.source = src;
      recv.count = size;
      total_size += size;
    }
  }
  comm_err = _ibComm->receive(recv_vect);
  if (comm_err != DF_SUCCESS) {
    // TODO this happens if CANCELL is issued
    logger.error() << __FUNCTION__ << " receive " << comm_err << std::flush;
    return comm_err;
  }
  _bytes_in += total_size;
  return DF_SUCCESS;
}

int EB::BU::get_next_MEP_space()
{
  int ret_val = DF_SUCCESS;
  try {
    _curr_MEP = _recv_buff->try_write_next_element(_full_size_words * EB::MEP_WORD_SIZE);
  } catch (const std::exception& e) {
    logger.error() << __FUNCTION__ << " " << e.what() << std::flush;
    ret_val = DF_ERROR;
    return ret_val;
  } catch (...) {
    logger.error() << __FUNCTION__ << " unexpected exception recv_buff try_write_next_element" << std::flush;
    ret_val = DF_ERROR;
    return ret_val;
  }

  if (_curr_MEP == NULL) {
    logger.info() << "Receive buffer full discarding the MEP" << std::flush;

    try {
      _curr_MEP = _discard_buff->write_next_element(_full_size_words * EB::MEP_WORD_SIZE, ret_val);
    } catch (const std::exception& e) {
      logger.error() << __FUNCTION__ << " " << e.what() << std::flush;
      ret_val = DF_ERROR;
      return ret_val;
    } catch (...) {
      logger.error() << __FUNCTION__ << " unexpected exception discard_buff try_write_next_element" << std::flush;
      ret_val = DF_ERROR;
      return ret_val;
    }
    _discarted = true;
  } else {
    _discarted = false;
  }

  if (_curr_MEP == NULL) {
    logger.error() << "CURR MEP NULL ret val " << ret_val << std::flush;
  }

  return ret_val;
}

int EB::BU::build_MEP_header()
{
  // DBG memset
  if (logger.is_active(Online::PrintLevel::DEBUG)) {
    memset(_curr_MEP, 0xeb, _full_size_words * EB::MEP_WORD_SIZE);
  }
  // set magic
  _curr_MEP->set_magic_valid();
  _curr_MEP->header.n_MFPs = _data_offset_words.size();
  _curr_MEP->header.p_words = _full_size_words;
  ::memcpy(_curr_MEP->header.offsets(), _data_offset_words.data(), _data_offset_words.size() * sizeof(EB::offset_type));
  // TODO this list should be checked against the data in the MFP headers
  ::memcpy(_curr_MEP->header.src_ids(), _sorted_src_ids.data(), _sorted_src_ids.size() * sizeof(EB::src_id_type));

  return DF_SUCCESS;
}

void EB::BU::reset_counters()
{
  // reset counters of the base class
  Transport_unit::reset_counters();
  logger.info() << "resetting counters" << std::flush;
  _MEP_count = 0;
  _discarted_MEP_count = 0;
  _corrupted_MEP_count = 0;
  _incomplete_MEP_count = 0;
  _run_loop_iteration = 0;
  _incomplete_MEP_srcs.clear();
  _receive_size_time_counter = 0;
  _calc_offsets_time_counter = 0;
  _build_header_time_counter = 0;
  _linear_shift_time_counter = 0;
  _receive_MFPs_time_counter = 0;
}

int EB::BU::reset_file_writer()
{
  int ret_val = DF_SUCCESS;
  if (_curr_MEP == NULL) {
    logger.error() << __FUNCTION__ << ": curr_MEP is NULL, this function should be called on valid data." << std::flush;
    ret_val = DF_ERROR;
    return ret_val;
  }
  std::stringstream file_name;

  try {
    Online::pcie40::sodin_t* odin_bank = reinterpret_cast<Online::pcie40::sodin_t*>(_curr_MEP->at(0)->get_bank_n(0));

    file_name << _out_file_prefix << "_" << odin_bank->_run_number << "_" << RTL::processName() << ".mep";
    _file_writer = File_writer<MEP>(file_name.str());
  } catch (const std::exception& e) {
    logger.warning() << "Unable to open DEBUG data to file. " << e.what() << ". Continuing..." << std::flush;
  }

  return ret_val;
}

bool EB::BU::is_file_writer_enable()
{
  // if dim driver dump is enabled the value of meps to write is calculated from the DIM service
  return (_write_to_file[_my_idx] || (_write_to_file_single && (_my_idx == 0))) || _write_to_file_dim_enable;
}

bool EB::BU::is_meps_file_writer_continue()
{
  // if dim driver dump is enabled the value of meps to write is calculated from the DIM service
  return ((!_write_to_file_dim_enable) && (_n_meps_written_to_file < _n_meps_to_file)) ||
         ((_write_to_file_dim_enable) && (_n_meps_written_to_file < _n_MEPS_info->getInt()));
}

void EB::BU::update_profiling()
{
  if (_enable_profiling) {
    double total_time = _total_timer.get_elapsed_time_s();
    if (total_time > _profiling_update_interval) {
      // This is here to prevent double evaluation of the total_timer
      EB::Transport_unit::update_profiling();
      double MEP_rate = _MEP_count / _total_time_counter;
      double event_rate = _DF_events_out / _total_time_counter;
      double bw = (_bytes_in * 8 * 1e-9) / _total_time_counter;

      double receive_size_total_time = _receive_size_timer.get_elapsed_time_s();
      double calc_offsets_time = _calc_offsets_timer.get_elapsed_time_s();
      double receive_size_time = receive_size_total_time - calc_offsets_time;
      double build_header_time = _build_header_timer.get_elapsed_time_s();
      double linear_shift_time = _linear_shift_timer.get_elapsed_time_s();
      double receive_MFPs_time = _receive_MFPs_timer.get_elapsed_time_s();

      _receive_size_time_counter += receive_size_time;
      _calc_offsets_time_counter += calc_offsets_time;
      _build_header_time_counter += build_header_time;
      _linear_shift_time_counter += linear_shift_time;
      _receive_MFPs_time_counter += receive_MFPs_time;

      if (logger.is_active(Online::PrintLevel::INFO)) {
        logger.info() << "receive sizes " << receive_size_time << " s " << receive_size_time / total_time * 100 << " %"
                      << std::flush;
        logger.info() << "calculate offsets " << calc_offsets_time << " s " << calc_offsets_time / total_time * 100
                      << " %" << std::flush;
        logger.info() << "build header " << build_header_time << " s " << build_header_time / total_time * 100 << " %"
                      << std::flush;
        logger.info() << "linear shift " << linear_shift_time << " s " << linear_shift_time / total_time * 100 << " %"
                      << std::flush;
        logger.info() << "receive data " << receive_MFPs_time << " s " << receive_MFPs_time / total_time * 100 << " %"
                      << std::flush;

        logger.info() << "MEP rate " << MEP_rate << " MEPS/s" << std::flush;
        logger.info() << "event rate " << event_rate << " Hz" << std::flush;
        logger.info() << "BW " << bw << " Gb/s" << std::flush;
      }

      _receive_size_timer.reset();
      _calc_offsets_timer.reset();
      _build_header_timer.reset();
      _linear_shift_timer.reset();
      _receive_MFPs_timer.reset();
    }
  }
}

void EB::BU::init_profiling()
{
  EB::Transport_unit::init_profiling();

  if (_enable_profiling) {
    _receive_size_timer.stop();
    _receive_size_timer.reset();
    _calc_offsets_timer.stop();
    _calc_offsets_timer.reset();
    _build_header_timer.stop();
    _build_header_timer.reset();
    _linear_shift_timer.stop();
    _linear_shift_timer.reset();
    _receive_MFPs_timer.stop();
    _receive_MFPs_timer.reset();
  } else {
    _receive_size_timer.disable();
    _calc_offsets_timer.disable();
    _build_header_timer.disable();
    _linear_shift_timer.disable();
    _receive_MFPs_timer.disable();
  }
}
