#include "Parallel_Comm.hpp"
#include <exception>
#include <iostream>
#include <cstdlib>
#include <unistd.h>
#include <stdexcept>
#include <algorithm>
#include <numeric>
#include <sstream>
#include <numa.h>

using namespace Online;

std::ostream& EB::comm_op::print(std::ostream& os) const
{
  os << "size " << count * datatype << "\n";
  return os;
}

std::ostream& EB::comm_send::print(std::ostream& os) const
{
  os << "COMM SEND"
     << "\n";
  os << "destination " << destination << "\n";
  os << "buffer " << std::hex << "0x" << snd_buff << std::dec << "\n";
  return comm_op::print(os);
}

std::ostream& EB::comm_recv::print(std::ostream& os) const
{
  os << "COMM RECV"
     << "\n";
  os << "source " << source << "\n";
  os << "buffer " << std::hex << "0x" << recv_buff << std::dec << "\n";
  return comm_op::print(os);
}

#ifdef EB_USE_SHARP
int EB::oob_barrier(void* ctx)
{
  int ret_val;
  EB::Parallel_comm* obj = reinterpret_cast<EB::Parallel_comm*>(ctx);

  obj->logger.debug() << "running " << __FUNCTION__ << std::flush;
  ret_val = obj->ibBarrier(IB_verbs::IB::barrier_t::CENTRAL, IB_verbs::IB::syncmode_t::SENDRECV, false);
  if (ret_val != DF_SUCCESS) {
    obj->logger.error() << __FUNCTION__ << ": communication failed rank " << obj->getProcessID() << std::flush;
    return SHARP_COLL_ERROR;
  }
  return SHARP_COLL_SUCCESS;
}

int EB::oob_bcast(void* ctx, void* buff, int size, int root)
{
  EB::Parallel_comm* obj = reinterpret_cast<EB::Parallel_comm*>(ctx);
  int ret_val;

  obj->logger.debug() << "running " << __FUNCTION__ << std::flush;
  if (obj->getProcessID() == root) {
    // populating array of destinations
    std::vector<int> ranks(obj->getTotalProcesses());
    std::iota(ranks.begin(), ranks.end(), 0);
    // removing root to avoid self communication
    ranks[root] = ranks.back();
    ranks.pop_back();
    ret_val = obj->ibBroadcastV(reinterpret_cast<char*>(buff), 1, size, ranks);
  } else {
    std::vector<comm_recv> recvs(1);
    comm_recv tmp;
    tmp.count = 1;
    tmp.datatype = size;
    tmp.recv_buff = buff;
    tmp.source = root;
    recvs[0] = tmp;
    ret_val = obj->receive(recvs);
  }

  if (ret_val != DF_SUCCESS) {
    obj->logger.error() << __FUNCTION__ << ": communication failed root " << root << " size " << size << " buff "
                        << buff << " rank " << obj->getProcessID() << std::flush;
    return SHARP_COLL_ERROR;
  }
  return SHARP_COLL_SUCCESS;
}

int EB::oob_gather(void* ctx, int root, void* sbuff, void* rbuff, int len)
{
  int ret_val;
  EB::Parallel_comm* obj = reinterpret_cast<EB::Parallel_comm*>(ctx);

  obj->logger.debug() << "running " << __FUNCTION__ << std::flush;
  if (obj->getProcessID() == root) {
    std::vector<int> ranks(obj->getTotalProcesses() - 1);
    std::vector<unsigned int> data_sizes(obj->getTotalProcesses() - 1, len);
    std::vector<unsigned int> data_idxs(obj->getTotalProcesses() - 1);

    // populating array of sources
    // we skip the root node
    // this is also the array of idxs
    std::generate(ranks.begin(), ranks.end(), [n = 0, root]() mutable {
      auto val = n + static_cast<int>(n >= root);
      n++;
      return val;
    });
    std::copy(ranks.begin(), ranks.end(), data_idxs.begin());

    ret_val = obj->ibGatherBlockV(reinterpret_cast<char*>(rbuff), data_sizes.data(), data_idxs.data(), len, ranks);
    // memcpy the local element
    ::memcpy(reinterpret_cast<char*>(rbuff) + root * len, sbuff, len);
  } else {
    std::vector<comm_send> sends(1);
    comm_send tmp;
    tmp.count = 1;
    tmp.datatype = len;
    tmp.snd_buff = sbuff;
    tmp.destination = root;
    sends[0] = tmp;
    ret_val = obj->send(sends);
  }

  if (ret_val != DF_SUCCESS) {
    obj->logger.error() << __FUNCTION__ << ": communication failed root " << root << " size " << len << " sbuff "
                        << sbuff << " rbuff " << rbuff << " rank " << obj->getProcessID() << std::flush;
    return SHARP_COLL_ERROR;
  }
  return SHARP_COLL_SUCCESS;
}
#endif

EB::Parallel_comm::Parallel_comm(
  IB_verbs::Parser* prs,
  std::string logName,
  Online::PrintLevel logLevel,
  int connection_timeout,
  unsigned int tree_barrier_group_size,
  bool enable_sharp)
{
  logger.set_name(logName);
  logger.set_output_level(logLevel);
  _enable_sharp = enable_sharp;
  if (_ibInit(prs, connection_timeout, tree_barrier_group_size) != DataflowStatus::DF_SUCCESS) {
    throw std::runtime_error("cannot start verbs!!!");
  }
}

int EB::Parallel_comm::_ibInit(IB_verbs::Parser* ibprs, int connection_timeout, unsigned int tree_barrier_group_size)
{
  int devNuma = ibprs->getIbNumaNode();
  int numa_err = numa_run_on_node(devNuma);
  if (numa_err != 0) {
    logger.error() << __FUNCTION__ << " numa_run_on_node " << devNuma << ":" << strerror(errno) << std::flush;
    return Online::DF_ERROR;
  }

  logger.debug() << "running on NUMA node " << devNuma << std::flush;

  numa_set_preferred(devNuma);

  try {
    _ibObj = std::make_unique<IB_verbs::IB>(ibprs, connection_timeout, tree_barrier_group_size);
  } catch (std::exception& e) {
    logger.error() << "IB init failed: " << e.what() << std::flush;
    return DataflowStatus::DF_ERROR;
  }

  // Init sharp
  if (_enable_sharp) {
#ifdef EB_USE_SHARP
    int err = _initSharp(ibprs);
    if (err != DF_SUCCESS) {
      return err;
    }
#else
    logger.warning() << "Requesting sharp on a non sharp-compatible build. Sharp won't be enabled." << std::flush;
    _enable_sharp = false;
#endif
  }

  return DataflowStatus::DF_SUCCESS;
}

#ifdef EB_USE_SHARP
int EB::Parallel_comm::_initSharp(IB_verbs::Parser* ibprs)
{
  int err = 0;

  logger.debug() << "Sharp init start." << std::flush;

  // initializing the uni ptrs for the sharp environment
  _sharp_coll_ctx =
    sharp_coll_ctx_uniq_ptr_t(nullptr, [](struct sharp_coll_context* ctx) { sharp_coll_finalize(ctx); });
  _sharp_coll_comm =
    sharp_coll_comm_uniq_ptr_t(nullptr, [](struct sharp_coll_comm* comm) { sharp_coll_comm_destroy(comm); });

  sharp_coll_init_spec init_spec = {};
  sharp_coll_comm_init_spec comm_spec = {};
  init_spec.world_rank = ibprs->getProcessId();
  init_spec.world_size = ibprs->getTotalProcesses();
  init_spec.enable_thread_support = 0;
  init_spec.config = sharp_coll_default_config;
  auto dev_string = ibprs->getIbDevString() + ":1"; // adding port
  init_spec.config.ib_dev_list = dev_string.c_str();
  init_spec.world_local_rank = ibprs->getLocalId();
  init_spec.group_channel_idx = ibprs->getLocalId();
  init_spec.oob_colls.barrier = oob_barrier;
  init_spec.oob_colls.bcast = oob_bcast;
  init_spec.oob_colls.gather = oob_gather;
  init_spec.oob_ctx = (void*) this;

  logger.debug() << "Sharp coll init. Rank " << init_spec.world_rank << " local rank " << init_spec.world_local_rank
                 << std::flush;
  sharp_coll_context* tmp_ctx = nullptr;
  err = sharp_coll_init(&init_spec, &tmp_ctx);
  _sharp_coll_ctx.reset(tmp_ctx);
  if (err != SHARP_COLL_SUCCESS) {
    logger.error() << "Sharp init failed: " << sharp_coll_strerror(err) << std::flush;
    return DF_ERROR;
  }
  logger.debug() << "Sharp coll init done." << std::flush;

  /* create sharp group */
  comm_spec.rank = init_spec.world_rank;
  comm_spec.size = init_spec.world_size;
  comm_spec.oob_ctx = (void*) this;
  comm_spec.group_world_ranks = NULL;

  logger.debug() << "Sharp comm init. Rank " << comm_spec.rank << " size " << comm_spec.size << std::flush;
  sharp_coll_comm* tmp_comm = nullptr;
  err = sharp_coll_comm_init(_sharp_coll_ctx.get(), &comm_spec, &tmp_comm);
  _sharp_coll_comm.reset(tmp_comm);
  if (err != SHARP_COLL_SUCCESS) {
    logger.error() << "Comm init failed: " << sharp_coll_strerror(err) << std::flush;
    return DF_ERROR;
  }

  logger.debug() << "Sharp comm init done." << std::flush;

  logger.debug() << "Sharp init done." << std::flush;

  return DF_SUCCESS;
}
#endif

int EB::Parallel_comm::ibDeregMRs()
{
  try {
    _ibObj->ibDeregMRs();
  } catch (std::exception& e) {
    logger.error() << "IB dereg MRs: " << e.what() << std::flush;
    return DataflowStatus::DF_ERROR;
  }
  return DataflowStatus::DF_SUCCESS;
}

int EB::Parallel_comm::getTotalProcesses() { return _ibObj->getTotalProcesses(); }

int EB::Parallel_comm::getProcessID() { return _ibObj->getProcessID(); }

int EB::Parallel_comm::addMR(char* bufPtr, size_t bufSize, int access_flags)
{
  if (_ibObj->ibAddMR(bufPtr, bufSize, access_flags) == NULL) {
    return DataflowStatus::DF_ERROR;
  }
  return DataflowStatus::DF_SUCCESS;
}

#ifdef IB_DMABUF_SUPPORT
int EB::Parallel_comm::addMRDMABuf(char* bufPtr, size_t bufSize, int dma_buf_fd, uint64_t offset, int access_flags)
{
  if (_ibObj->ibAddMRDMABuf(bufPtr, bufSize, dma_buf_fd, offset, access_flags) == NULL) {
    return DataflowStatus::DF_ERROR;
  }
  return DataflowStatus::DF_SUCCESS;
}
#endif

void EB::Parallel_comm::ibKillBlocking() { _ibObj->ibKillBlocking(); }

int EB::Parallel_comm::send(const std::vector<comm_send>& sends)
{
  int ret;
  logger.debug() << "waiting sync: " << sends.front().destination << std::flush;
  ret = _syncRecv(sends.front().destination);
  if (ret != DataflowStatus::DF_SUCCESS) {
    return ret;
  }

  logger.debug() << "Posting SRs" << std::flush;
  ret = _sendV(sends);
  if (ret == -5) {
    return DataflowStatus::DF_CANCELLED;
  } else if (ret != 0) {
    return DataflowStatus::DF_ERROR;
  }

  return DataflowStatus::DF_SUCCESS;
}

int EB::Parallel_comm::receive(const std::vector<comm_recv>& recvs)
{
  int ret = DataflowStatus::DF_SUCCESS;
  logger.debug() << "Posting RRs" << std::flush;
  auto recvsid = _receiveV(recvs);
  logger.debug() << "send sync: " << recvs.front().source << std::flush;
  ret = _syncSend(recvs.front().source);
  if (ret != DataflowStatus::DF_SUCCESS) {
    return ret;
  }

  logger.debug() << "waiting recv completion" << std::flush;
  ret = _waitRecvs(recvsid);
  logger.debug() << "recv completed" << std::flush;
  return ret;
}

int EB::Parallel_comm::_waitRecvs(std::vector<uint32_t>& recvs)
{
  int ret = _ibObj->ibWaitAllRecvs(recvs);
  if (ret == 0) {
    return DataflowStatus::DF_SUCCESS;
  } else if (ret == -5) {
    return DataflowStatus::DF_CANCELLED;
  }
  return DataflowStatus::DF_ERROR;
}

int EB::Parallel_comm::_waitSends(std::vector<uint32_t>& sends)
{
  int ret = _ibObj->ibWaitAllSends(sends);
  if (ret == 0) {
    return DataflowStatus::DF_SUCCESS;
  } else if (ret == -5) {
    return DataflowStatus::DF_CANCELLED;
  }
  return DataflowStatus::DF_ERROR;
}

int EB::Parallel_comm::ibTestRecvs(std::vector<uint32_t>& recvs) { return _ibObj->ibTestAllRecvs(recvs); }

std::vector<uint32_t> EB::Parallel_comm::_receiveV(const std::vector<comm_recv>& recvs)
{
  std::vector<uint32_t> req_vec;
  for (auto& recv_it : recvs) {
    size_t size = recv_it.count * recv_it.datatype;
    uint32_t ret = _ibObj->ibRecv((char*) recv_it.recv_buff, size, recv_it.source);
    if (ret != 0) {
      req_vec.push_back(ret);
    } else {
      logger.error() << "IB recv FAILED with err: " << ret << std::flush;
    }
  }
  return req_vec;
}

int EB::Parallel_comm::_sendV(const std::vector<comm_send>& sends)
{
  std::vector<uint32_t> req_vec;
  for (auto& send_it : sends) {
    size_t size = send_it.count * send_it.datatype;
    uint32_t ret = _ibObj->ibSend((char*) send_it.snd_buff, size, send_it.destination);

    if (ret != 0) {
      req_vec.push_back(ret);
    } else {
      logger.error() << "IB send FAILED with err: " << ret << std::flush;
      return -1;
    }
  }

  return _ibObj->ibWaitAllSends(req_vec);
}

int EB::Parallel_comm::_syncSend(int dest)
{
  int rank = _ibObj->getProcessID();
  logger.debug() << rank << " S SYNC to " << dest << std::flush;
  uint32_t wid = _ibObj->ibSendSync(IB_verbs::IB::immediate_t::ONE_TO_ONE, dest);
  if (wid == 0) return DataflowStatus::DF_ERROR;
  int ret = _ibObj->ibWaitSyncSend(wid);
  if (ret == 0) {
    return DataflowStatus::DF_SUCCESS;
  } else if (ret == -5) {
    return DataflowStatus::DF_CANCELLED;
  }
  return DataflowStatus::DF_ERROR;
}

uint32_t EB::Parallel_comm::_syncRecv(int src)
{
  int rank = _ibObj->getProcessID();
  logger.debug() << rank << " R SYNC from " << src << std::flush;
  int ret = _ibObj->ibWaitSyncRecv(IB_verbs::IB::immediate_t::ONE_TO_ONE, src);
  if (ret == 0) {
    return DataflowStatus::DF_SUCCESS;
  } else if (ret == -5) {
    return DataflowStatus::DF_CANCELLED;
  }
  return DataflowStatus::DF_ERROR;
}

// TODO the sharp flag should be a new type
int EB::Parallel_comm::ibBarrier(IB_verbs::IB::barrier_t type, IB_verbs::IB::syncmode_t mode, bool use_sharp)
{
  int ret = 0;

  // for now we have a silent failover
  if (use_sharp && _enable_sharp) {
#ifdef EB_USE_SHARP
    // TODO add nb implementation
    int sharp_error = sharp_coll_do_barrier(_sharp_coll_comm.get());
    if (sharp_error != SHARP_COLL_SUCCESS) {
      logger.error() << "Paralle_Comm: SHaRP barrier failed: " << sharp_coll_strerror(sharp_error) << std::flush;
      return DataflowStatus::DF_ERROR;
    }
#else
    logger.error() << "Paralle_Comm: requesting sharp on a non sharp enabled build" << std::flush;
    return DataflowStatus::DF_ERROR;
#endif
  } else {

    ret = _ibObj->ibBarrier(type, mode);
    if (ret == -5) {
      return DataflowStatus::DF_CANCELLED;
    } else if (ret < 0) {
      logger.error() << "Parallel_Comm: Barrier Failed" << std::flush;
      return DataflowStatus::DF_ERROR;
    }
  }
  return DataflowStatus::DF_SUCCESS;
}

int EB::Parallel_comm::ibScatterV(char* data, size_t n_elem, size_t elem_size, std::vector<int>& dsts)
{
  std::vector<uint32_t> wrids;
  int ret_val = 0;
  uint32_t data_wrid = 0;
  for (size_t j = 0; j < dsts.size(); j++) {
    logger.debug() << "waiting sync from " << dsts.at(j) << std::flush;
    ret_val = _syncRecv(dsts.at(j));
    if (ret_val != DataflowStatus::DF_SUCCESS) {
      return ret_val;
    }
    size_t idx = j * n_elem * elem_size;
    logger.debug() << "SCATTER: sending sizes to " << dsts.at(j) << ": " << n_elem << std::flush;
    data_wrid = _ibObj->ibSend(data + idx, n_elem * elem_size, dsts.at(j));
    if (data_wrid == 0) return DataflowStatus::DF_ERROR;
    wrids.push_back(data_wrid);
  }
  ret_val = _waitSends(wrids);
  if (ret_val != DataflowStatus::DF_SUCCESS) {
    return ret_val;
  }
  return DataflowStatus::DF_SUCCESS;
}

int EB::Parallel_comm::ibBroadcastV(char* data, size_t n_elem, size_t elem_size, std::vector<int>& dsts)
{
  std::vector<uint32_t> wrids;
  int ret_val = 0;
  uint32_t data_wrid = 0;
  for (size_t j = 0; j < dsts.size(); j++) {
    logger.debug() << "waiting sync from " << dsts.at(j) << std::flush;
    ret_val = _syncRecv(dsts.at(j));
    if (ret_val != DataflowStatus::DF_SUCCESS) {
      return ret_val;
    }
    logger.debug() << "BCAST: sending to " << dsts.at(j) << ": " << n_elem << std::flush;
    data_wrid = _ibObj->ibSend(data, n_elem * elem_size, dsts.at(j));
    if (data_wrid == 0) return DataflowStatus::DF_ERROR;
    wrids.push_back(data_wrid);
  }
  ret_val = _waitSends(wrids);
  if (ret_val != DataflowStatus::DF_SUCCESS) {
    return ret_val;
  }
  return DataflowStatus::DF_SUCCESS;
}

int EB::Parallel_comm::ibGatherBlockV(
  char* data,
  unsigned int* data_sizes,
  unsigned int* data_idxs,
  size_t elem_size,
  std::vector<int>& remotes)
{
  int ret_val = 0;
  auto wrids = ibGatherV(data, data_sizes, data_idxs, elem_size, remotes, ret_val);
  if (ret_val == DF_CANCELLED) {
    return ret_val;
  } else if (ret_val != DF_SUCCESS) {
    logger.error() << __FUNCTION__ << " ibGatherV failed " << strerror(errno) << std::flush;
    return ret_val;
  }
  logger.debug() << "sync done" << std::flush;
  ret_val = _ibObj->ibWaitAllRecvs(wrids);
  if (ret_val == -5) {
    return DataflowStatus::DF_CANCELLED;
  } else if (ret_val < 0) {
    return DataflowStatus::DF_ERROR;
  }
  return DataflowStatus::DF_SUCCESS;
}

std::vector<uint32_t> EB::Parallel_comm::ibGatherV(
  char* data,
  unsigned int* data_sizes,
  unsigned int* data_idxs,
  size_t elem_size,
  std::vector<int>& remotes,
  int& ret_val)
{
  ret_val = DataflowStatus::DF_SUCCESS;
  std::vector<uint32_t> wrids;
  std::vector<uint32_t> syncids;
  uint32_t data_wrid, sync_wrid;
  for (size_t i = 0; i < remotes.size(); i++) {
    logger.debug() << "posting gather recv from " << remotes.at(i) << ": " << data_sizes[i] << ", "
                   << (uintptr_t) (data) + data_idxs[i] * elem_size << std::flush;
    data_wrid = _ibObj->ibRecv(data + data_idxs[i] * elem_size, data_sizes[i] * elem_size, remotes.at(i));
    if (data_wrid == 0) {
      ret_val = DataflowStatus::DF_ERROR;
      // TODO check if break is a better option
      return wrids;
    }
    wrids.push_back(data_wrid);
    logger.debug() << "sending sync to " << remotes.at(i) << std::flush;
    sync_wrid = _ibObj->ibSendSync(IB_verbs::IB::immediate_t::ONE_TO_ONE, remotes.at(i));
    if (sync_wrid == 0) {
      ret_val = DataflowStatus::DF_ERROR;
      // TODO check if break is a better option
      return wrids;
    }
    syncids.push_back(sync_wrid);
  }
  logger.debug() << "waiting for sync" << std::flush;
  int ret = _ibObj->ibWaitAllSyncSends(syncids);
  if (ret == -5) {
    ret_val = DataflowStatus::DF_CANCELLED;
  } else if (ret != 0) {
    ret_val = DataflowStatus::DF_ERROR;
  }
  logger.debug() << "sync done" << std::flush;
  return wrids;
}