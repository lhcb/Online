#include "EventBuilding/RU.hpp"
#include "shmem_buffer_reader.hpp"
#include <algorithm>
#include <list>
#include <numeric>
#include <stdexcept>
#include <sstream>
#include <cstring>
#include <chrono>
#include "MFP_preloader.hpp"
#include "RTL/rtl.h"
#include "EventBuilding/tools.hpp"
#include "unused.hpp"
#include "EventBuilding/raw_tools.hpp"
#include "watchdog.hpp"

const std::string EB::RU_buffer_type_to_string(RU_buffer_types type)
{
  switch (type) {
  case dummy_MFP_buffer: return "dummy_MFP_buffer";
  case shmem_MFP_buffer: return "shmem_MFP_buffer";
  case PCIe40_MFP_buffer: return "PCIe40_MFP_buffer";
  case PCIe40_frag_buffer: return "PCIe40_frag_reader";
  default: return "Error unkown buffer type";
  }
}

std::ostream& EB::operator<<(std::ostream& os, RU_buffer_types type) { return os << RU_buffer_type_to_string(type); }

EB::RU::RU(const std::string& nam, DataflowContext& ctxt) : Transport_unit(nam, ctxt)
{
  // TODO declare all the properties
  info("RU constructor");
  declareProperty("n_fragment", _n_fragment = 1);
  declareProperty("shmem_prefix", _shmem_prefix = std::string("RU"));
  declareProperty("buffer_type", _buffer_type);

  declareProperty("write_to_file", _write_to_file);
  declareProperty("out_file_prefix", _out_file_prefix = std::string("RU"));
  declareProperty("n_MFPs_to_file", _n_MFPs_to_file = 1);
  // TODO find a good default value
  declareProperty("PCIe40_ids", _PCIe40_ids);
  declareProperty("PCIe40_names", _PCIe40_names);
  declareProperty("buffer_sizes", _prop_buffer_sizes);
  declareProperty("MDF_filename", _MDF_filename);
  declareProperty("n_MFPs", _n_MFPs = 1);
  declareProperty("stop_timeout", _stop_timeout = 10);
  declareProperty("dummy_src_ids", _dummy_src_ids);
  declareProperty("SODIN_ID", _SODIN_ID = -1);
  declareProperty("SODIN_name", _SODIN_name = "");
  declareProperty("SODIN_stream", _SODIN_stream = 0);
  declareProperty("buffer_timeout", _buffer_timeout = 60);
  declareProperty("buffer_low_occupancy_thr", _buffer_low_occupancy_thr = 50);
  declareProperty("buffer_high_occupancy_thr", _buffer_high_occupancy_thr = 95);
  declareProperty("idle_monitor_service_name", _idle_monitor_service_name = std::string("idle_buffers"));

  // DEBUG counters
  declareMonitor("run_loop_iteration", _run_loop_iteration, "Iteration fo the run loop in the RUN");
  // profiling counters
  declareMonitor("load_mpfs_time_counter", _load_mpfs_time_counter);
  declareMonitor("send_sizes_time_counter", _send_sizes_time_counter);
  declareMonitor("linear_shift_time_counter", _linear_shift_time_counter);
  declareMonitor("send_time_counter", _send_time_counter);

  // Buffer occupancy monitoring
  for (size_t k = 0; k < _source_buffer_low_occ.size(); k++) {
    std::stringstream monitor_name;
    monitor_name << "source_" << k << "_buffer_low_occupancy";
    declareMonitor(monitor_name.str().c_str(), _source_buffer_low_occ[k]);
  }

  for (size_t k = 0; k < _source_buffer_med_occ.size(); k++) {
    std::stringstream monitor_name;
    monitor_name << "source_" << k << "_buffer_med_occupancy";
    declareMonitor(monitor_name.str().c_str(), _source_buffer_med_occ[k]);
  }

  for (size_t k = 0; k < _source_buffer_high_occ.size(); k++) {
    std::stringstream monitor_name;
    monitor_name << "source_" << k << "_buffer_high_occupancy";
    declareMonitor(monitor_name.str().c_str(), _source_buffer_high_occ[k]);
  }

  logger.set_name(name);
}

EB::RU::~RU() {}

int EB::RU::initialize()
{
  int sc = Transport_unit::initialize();
  if (sc != DF_SUCCESS) {
    return error("Failed to initialize Transport unit base class.");
  }

  _my_idx = get_idx(_ru_ranks.begin(), _ru_ranks.end(), _my_rank);
  if (_my_idx < 0) {
    logger.error() << " this process should not be a RU. Aborting." << std::flush;
    return DF_ERROR;
  }

  sc = init_shift();
  if (sc != DF_SUCCESS) {
    logger.error() << __FUNCTION__ << " init shift failed" << std::flush;
    return sc;
  }

  sc = init_dummy_src_ids();
  if (sc != DF_SUCCESS) {
    logger.error() << __FUNCTION__ << " init dummy src ids failed" << std::flush;
    return sc;
  }

  sc = init_dim_services();
  if (sc != DF_SUCCESS) {
    logger.error() << __FUNCTION__ << " init dim services failed" << std::flush;
    return sc;
  }

  return DF_SUCCESS;
}

int EB::RU::check_buffers()
{
  int ret_val = DF_SUCCESS;

  if (_buffer_type.size() == 0) {
    _buffer_type.resize(_ru_ranks.size(), default_RU_buffer_type);
    logger.warning() << __FUNCTION__ << " no buffer types provided setting to default value "
                     << EB::default_RU_buffer_type << std::flush;
  } else if (_buffer_type.size() == 1) {
    _buffer_type.resize(_ru_ranks.size(), _buffer_type[0]);
    logger.warning() << __FUNCTION__ << " Single buffer type provided: " << _buffer_type[0] << std::flush;
  }

  if (_buffer_type.size() != _ru_ranks.size()) {
    logger.error() << __FUNCTION__ << " Configuration error: not enough buffer types provided " << _buffer_type.size()
                   << "/" << _ru_ranks.size() << std::flush;

    ret_val = DF_ERROR;
  }

  if (_prop_buffer_sizes.size() == 0) {
    _prop_buffer_sizes.resize(_prefix_n_sources_per_ru[_ru_ranks.size()], default_RU_buffer_size);
    logger.warning() << __FUNCTION__ << " no buffer sizes provided setting to default value "
                     << EB::default_RU_buffer_size << std::flush;
  } else if (_prop_buffer_sizes.size() == 1) {
    _prop_buffer_sizes.resize(_prefix_n_sources_per_ru[_ru_ranks.size()], _prop_buffer_sizes[0]);
    logger.warning() << __FUNCTION__ << " Single buffer size provided: " << _prop_buffer_sizes[0] << std::flush;
  }

  if (_prop_buffer_sizes.size() != _prefix_n_sources_per_ru[_ru_ranks.size()]) {
    logger.error() << __FUNCTION__ << " Configuration error: not enough buffer sizes provided "
                   << _prop_buffer_sizes.size() << "/" << _prefix_n_sources_per_ru[_ru_ranks.size()] << std::flush;

    ret_val = DF_ERROR;
  }

  _buffer_sizes.resize(_prop_buffer_sizes.size());
  std::transform(_prop_buffer_sizes.begin(), _prop_buffer_sizes.end(), _buffer_sizes.begin(), [](auto val) {
    return val * 1024 * 1024 * 1024UL;
  });

  if (_write_to_file.size() == 0) {
    _write_to_file.resize(_ru_ranks.size(), false);
    logger.warning() << __FUNCTION__ << " write to file was not set, setting it to the default value " << false
                     << std::flush;
  } else if (_write_to_file.size() == 1) {
    _write_to_file.resize(_ru_ranks.size(), _write_to_file[0]);
    logger.warning() << __FUNCTION__ << " Single write to file provided: " << _write_to_file[0] << std::flush;
  }

  if (_write_to_file.size() != _ru_ranks.size()) {
    logger.error() << __FUNCTION__ << " Configuration error: not enough write to file provided "
                   << _write_to_file.size() << "/" << _bu_ranks.size() << std::flush;

    ret_val = DF_ERROR;
  }

  _buffer_sizes.resize(_prop_buffer_sizes.size());
  std::transform(_prop_buffer_sizes.begin(), _prop_buffer_sizes.end(), _buffer_sizes.begin(), [](auto val) {
    return val * 1024 * 1024 * 1024UL;
  });

  return ret_val;
}

int EB::RU::config_buffers()
{
  logger.debug() << __FUNCTION__ << " start" << std::flush;
  int ret_val = DF_SUCCESS;
  _buffers.reserve(_n_sources_per_ru[_my_idx]);

  ret_val = check_buffers();
  if (ret_val != DF_SUCCESS) {
    return ret_val;
  }

  try {
    if (_buffer_type[_my_idx] == dummy_MFP_buffer) {
      config_dummy();
    } else if (_buffer_type[_my_idx] == shmem_MFP_buffer) {
      config_shmem();
    } else if (_buffer_type[_my_idx] == PCIe40_frag_buffer) {
      config_PCIe40_frag();
    } else if (_buffer_type[_my_idx] == PCIe40_MFP_buffer) {
      config_PCIe40();
    } else {
      logger.error() << __FUNCTION__ << " unsupported buffer type " << _buffer_type[_my_idx] << std::flush;
      ret_val = DF_ERROR;
      return ret_val;
    }

    if (_write_to_file[_my_idx]) {
      std::stringstream file_name;
      file_name << _out_file_prefix << "_" << _my_idx << ".mfp";
      _file_writer = File_writer<EB::MFP>(file_name.str());
    }

  } catch (const std::system_error& e) {
    logger.error() << __FUNCTION__ << " unable to configure buffer type" << RU_buffer_types(_buffer_type[_my_idx])
                   << ". " << e.what() << ": " << strerror(errno) << ". Error code: " << e.code() << std::flush;
    ret_val = DF_ERROR;
    return ret_val;
  } catch (const std::exception& e) {
    logger.error() << __FUNCTION__ << " unable to configure buffer type " << RU_buffer_types(_buffer_type[_my_idx])
                   << ". " << e.what() << ": " << strerror(errno) << std::flush;
    ret_val = DF_ERROR;
    return ret_val;
  } catch (...) {
    logger.error() << __FUNCTION__ << " unexpected exception buffer type " << RU_buffer_types(_buffer_type[_my_idx])
                   << std::flush;
    ret_val = DF_ERROR;
    return ret_val;
  }

  logger.debug() << __FUNCTION__ << " end" << std::flush;
  return ret_val;
}

int EB::RU::start()
{
  int ret_val = DF_SUCCESS;
  info("RU start");
  int sc = Transport_unit::start();
  if (sc != DF_SUCCESS) {
    return error("Failed to start Transport_unit base class.");
  }
  info("RU config buffers");
  ret_val = config_buffers();
  if (ret_val != DF_SUCCESS) {
    return ret_val;
  }

  std::timed_mutex watchdog_mtx;
  {
    std::lock_guard lk(watchdog_mtx);
    Watchdog<int, std::ratio<1>> watchdog(
      watchdog_mtx, std::chrono::duration<int>(_IB_connection_timeout), [this]() { this->cancel(); });
    // exchange srcids
    info("RU send src ids");
    ret_val = send_src_ids();
    if (ret_val != DF_SUCCESS) {
      return ret_val;
    };

    // exchange srcnames
    info("RU send src names");
    ret_val = send_src_names();
    if (ret_val != DF_SUCCESS) {
      return ret_val;
    };
    // we unlock explicitly to prevent destructors races between the watchdog and the lock_guard
    watchdog_mtx.unlock();
  }

  logger.debug() << " init BUFFER REG" << std::endl;
  for (auto& buf : _buffers) {
    auto buflist = buf->get_full_buffer();
    for (std::tuple<void*, size_t>& list_it : buflist) {
      // DMABuf registration is required for PCIe40 compatibility on RHEL9
      if (_buffer_type[_my_idx] == PCIe40_MFP_buffer) {
#if defined(P40_DMABUF_SUPPORT) && defined(IB_DMABUF_SUPPORT)
        ret_val = _ibComm->addMRDMABuf(
          (char*) std::get<void*>(list_it),
          std::get<size_t>(list_it),
          static_cast<EB::PCIe40_MFP_reader*>(buf.get())->get_dmabuf_fd(),
          0,
          0x0);
#else
        // if the driver doesn't support DMABUF we roll back to ondemand paging
        ret_val = _ibComm->addMR((char*) std::get<void*>(list_it), std::get<size_t>(list_it), IBV_ACCESS_ON_DEMAND);
#endif
      } else {
        ret_val = _ibComm->addMR((char*) std::get<void*>(list_it), std::get<size_t>(list_it));
      }
      if (ret_val != DF_SUCCESS) {
        logger.error() << __FUNCTION__ << " unable to register memory for buffer " << std::get<void*>(list_it) << ": "
                       << strerror(errno) << std::flush;
        return ret_val;
      }
      logger.debug() << " BUFFER src " << buf->get_src_id() << " REG" << std::endl;
    }
  }
  logger.debug() << "data MRs allocated" << std::flush;
  // mpfs and sizes per destination per src
  size_t mfp_vec_size = _bu_ranks.size() * _buffers.size();
  selected_MFPs.resize(mfp_vec_size);
  sizes.resize(mfp_vec_size);
  _n_frags_per_dst.resize(_bu_ranks.size());
  //  TO DO
  ret_val = _ibComm->addMR((char*) &sizes, sizes.size() * sizeof(uint32_t));
  if (ret_val != DF_SUCCESS) {
    logger.error() << __FUNCTION__ << " addMR failed: " << strerror(errno) << std::flush;
    return ret_val;
  }
  // END TO DO
  logger.debug() << "size MRs allocated dim: " << mfp_vec_size << std::flush;
  std::lock_guard<decltype(_start_lock)> loop_guard(_start_lock);
  _send_MEPs = true;
  reset_counters();
  _end_of_run = true;
  return DF_SUCCESS;
}

int EB::RU::stop()
{
  int ret_val = DF_SUCCESS;
  logger.info() << "stop" << std::flush;
  std::unique_lock<std::timed_mutex> loop_guard(_loop_lock, std::chrono::duration<int>(_stop_timeout));
  if (loop_guard) {
    logger.info() << "stop executed properly" << std::flush;
  } else {
    logger.error() << "Unable to execute a clean stop. Aborting" << std::flush;
    ret_val = DF_ERROR;
  }
  int sc = Transport_unit::stop();
  if (sc != DF_SUCCESS) {
    return error("Failed to stop Transport_unit base class.");
  }
  if (ret_val != DF_SUCCESS) {
    return ret_val;
  }
  // undo the buffer allocation done in start
  // TODO check if the buffers should be cleaned if stop aborts
  _buffers.clear();

  return ret_val;
}

int EB::RU::cancel()
{
  // TODO implement this
  std::lock_guard<decltype(_start_lock)> guard(_start_lock);
  logger.info() << "cancel" << std::flush;
  _send_MEPs = false;
  Transport_unit::cancel();
  for (auto& buffer : _buffers) {
    buffer->cancel();
  }

  return DF_SUCCESS;
}

int EB::RU::pause()
{
  // _send_MEPs = false;
  // the EB does not go into pause
  return DF_SUCCESS;
}

int EB::RU::finalize()
{
  int ret_val = DF_SUCCESS;
  ret_val = Transport_unit::finalize();
  if (ret_val != DF_SUCCESS) {
    return ret_val;
  }

  ret_val = close_dim_services();
  if (ret_val != DF_SUCCESS) {
    return ret_val;
  }
  return ret_val;
}

void EB::RU::handle(const DataflowIncident& UNUSED(inc))
{
  info("RU incident");
  //  TODO implement this
}

void EB::RU::config_dummy()
{
  // fast access to my config
  auto my_buffer_sizes_begin = _buffer_sizes.begin() + _prefix_n_sources_per_ru[_my_idx];
  auto my_buffer_sizes_end = _buffer_sizes.begin() + _prefix_n_sources_per_ru[_my_idx + 1];
  // config already checked

  // loads data from the MDF file
  MFP_preloader preloader;
  if (!preloader.set_MDF_filename(_MDF_filename.c_str())) {
    std::stringstream error_mess;
    error_mess << __FUNCTION__ << ": unable to open MDF file " << _MDF_filename << " " << strerror(errno);
    throw std::runtime_error(error_mess.str());
  }

  preloader.set_n_MFPs(_n_MFPs);

  std::vector<MFP_preloader::buffer_ptr_type> buffers(_n_sources_per_ru[_my_idx]);
  std::vector<EB::type_id_pair_type> source_mapping(_n_sources_per_ru[_my_idx]);
  for (unsigned int k = 0; k < _n_sources_per_ru[_my_idx]; k++) {
    void* tmp_ptr;
    posix_memalign(&tmp_ptr, sysconf(_SC_PAGE_SIZE), *(my_buffer_sizes_begin + k));
    if (tmp_ptr == NULL) {
      std::stringstream error_mess;
      error_mess << __FUNCTION__ << ": unable to allocate buffer " << k << " " << strerror(errno);
      throw std::runtime_error(error_mess.str());
    }

    buffers[k] = MFP_preloader::buffer_ptr_type(reinterpret_cast<char*>(tmp_ptr), free);
    source_mapping[k] = EB::new_src_id_to_old(_dummy_src_ids[_prefix_n_sources_per_ru[_my_idx] + k]);
  }

  preloader.set_detector_map(source_mapping);
  std::vector<size_t> tmp_sizes(my_buffer_sizes_begin, my_buffer_sizes_end);
  preloader.set_buffers(buffers, tmp_sizes);

  preloader.set_packing_factor(_n_fragment);

  if (preloader.preload_MFPs() < 0) {
    std::stringstream error_mess;
    error_mess << __FUNCTION__ << ": unable to preload MFPs from file " << _MDF_filename;
    throw std::runtime_error(error_mess.str());
  }

  auto buffs = preloader.get_buffers();

  for (unsigned int k = 0; k < _n_sources_per_ru[_my_idx]; k++) {
    _buffers.emplace_back(new Dummy_MFP_buffer(
      std::get<0>(buffs)[k], std::get<2>(buffs)[k], _dummy_src_ids[_prefix_n_sources_per_ru[_my_idx] + k]));
  }
}

void EB::RU::config_shmem()
{
  int local_idx;

  local_idx = get_idx_UTGID(RTL::processName(), "RU");

  int idx_offset = 0;
  while (local_idx > 0) {
    idx_offset += _n_sources_per_ru[_my_idx - local_idx];
    local_idx--;
  }

  for (unsigned int k = 0; k < _n_sources_per_ru[_my_idx]; k++) {
    std::stringstream shmem_name;
    shmem_name << _shmem_prefix << "_" << k + idx_offset;
    logger.debug() << "UTGID " << RTL::processName() << " local index " << idx_offset << " opening shmem "
                   << shmem_name.str() << std::flush;
    _buffers.emplace_back(new Shmem_buffer_reader<EB::MFP>(shmem_name.str().c_str()));
  }
}

int EB::RU::stream_select(int id)
{
  int stream;
  if (_SODIN_ID != id) {
    stream = P40_DAQ_STREAM::P40_DAQ_STREAM_MAIN;
  } else {
    stream = static_cast<int>(P40_DAQ_STREAM::P40_DAQ_STREAM_ODIN0) + _SODIN_stream;
  }

  return stream;
}

int EB::RU::stream_select(const std::string& stream_name)
{
  int stream;
  if (_SODIN_name != stream_name) {
    stream = P40_DAQ_STREAM::P40_DAQ_STREAM_MAIN;
  } else {
    // TODO add multi stream support based on the _SODIN_stream variable
    stream = static_cast<int>(P40_DAQ_STREAM::P40_DAQ_STREAM_ODIN0) + _SODIN_stream;
  }

  return stream;
}

void EB::RU::config_PCIe40()
{
  if (_PCIe40_ids.size() == _prefix_n_sources_per_ru[_ru_ranks.size()]) {
    logger.info() << "PCIe40 selection by ID" << std::flush;
    // fast access to my config
    auto my_PCIe40_ids_begin = _PCIe40_ids.begin() + _prefix_n_sources_per_ru[_my_idx];
    auto my_PCIe40_ids_end = _PCIe40_ids.begin() + _prefix_n_sources_per_ru[_my_idx + 1];

    for (auto it = my_PCIe40_ids_begin; it != my_PCIe40_ids_end; it++) {
      logger.debug() << __FUNCTION__ << "configuring buffer " << *it << std::endl;
      _buffers.emplace_back(new PCIe40_MFP_reader(*it, stream_select(*it), _n_fragment));
    }
  } else if (_PCIe40_names.size() == _prefix_n_sources_per_ru[_ru_ranks.size()]) {
    logger.info() << "PCIe40 selection by name" << std::flush;
    // fast access to my config
    auto my_PCIe40_names_begin = _PCIe40_names.begin() + _prefix_n_sources_per_ru[_my_idx];
    auto my_PCIe40_names_end = _PCIe40_names.begin() + _prefix_n_sources_per_ru[_my_idx + 1];

    for (auto it = my_PCIe40_names_begin; it != my_PCIe40_names_end; it++) {
      logger.debug() << __FUNCTION__ << "configuring buffer " << *it << std::endl;
      _buffers.emplace_back(new PCIe40_MFP_reader(*it, stream_select(*it), _n_fragment));
    }
  } else {
    // In this set of functions (config_buffers) the errors are handled via expections
    std::ostringstream err_mess;
    err_mess << "RU " << _my_idx << " " << __FUNCTION__
             << ": Invalid PCIe40 configuration: not enough IDs or names provided. Got " << _PCIe40_names.size()
             << " names and " << _PCIe40_ids.size() << " ids for " << _prefix_n_sources_per_ru[_ru_ranks.size()]
             << " data sources";
    throw std::runtime_error(err_mess.str());
  }
}

void EB::RU::config_PCIe40_frag()
{
  // fast access to my config
  // buffer_sizes_are checked in the buffer_check method
  auto my_buffer_sizes_it = _buffer_sizes.begin() + _prefix_n_sources_per_ru[_my_idx];
  auto my_buffer_sizes_end = _buffer_sizes.begin() + _prefix_n_sources_per_ru[_my_idx + 1];
  auto my_buffer_sizes_size = std::distance(my_buffer_sizes_it, my_buffer_sizes_end);

  if (_PCIe40_ids.size() == _prefix_n_sources_per_ru[_ru_ranks.size()]) {
    logger.info() << "PCIe40 selection by ID" << std::flush;
    auto my_PCIe40_ids_it = _PCIe40_ids.begin() + _prefix_n_sources_per_ru[_my_idx];
    for (int k = 0; k < my_buffer_sizes_size; k++) {
      logger.debug() << __FUNCTION__ << "configuring buffer " << *(my_PCIe40_ids_it) << std::endl;
      _buffers.emplace_back(new PCIe40_frag_reader(
        *(my_PCIe40_ids_it),
        *(my_buffer_sizes_it),
        _n_fragment,
        EB::default_PCIe40_alignment,
        stream_select(*my_PCIe40_ids_it)));
      my_PCIe40_ids_it++;
      my_buffer_sizes_it++;
    }
  } else if (_PCIe40_names.size() == _prefix_n_sources_per_ru[_ru_ranks.size()]) {
    logger.info() << "PCIe40 selection by name" << std::flush;
    auto my_PCIe40_names_it = _PCIe40_names.begin() + _prefix_n_sources_per_ru[_my_idx];
    for (int k = 0; k < my_buffer_sizes_size; k++) {
      logger.debug() << __FUNCTION__ << "configuring buffer " << *(my_PCIe40_names_it) << std::endl;
      _buffers.emplace_back(new PCIe40_frag_reader(
        *(my_PCIe40_names_it),
        *(my_buffer_sizes_it),
        _n_fragment,
        EB::default_PCIe40_alignment,
        stream_select(*my_PCIe40_names_it)));
      my_PCIe40_names_it++;
      my_buffer_sizes_it++;
    }
  } else {
    // In this set of functions (config_buffers) the errors are handled via expections
    std::ostringstream err_mess;
    err_mess << "RU " << _my_idx << " " << __FUNCTION__
             << ": Invalid PCIe40 configuration: not enough IDs or names provided. Got " << _PCIe40_names.size()
             << " names and " << _PCIe40_ids.size() << " ids for " << _prefix_n_sources_per_ru[_ru_ranks.size()]
             << " data sources";
    throw std::runtime_error(err_mess.str());
  }
}

int EB::RU::init_shift()
{
  // precomputes the linear shift pattern
  // this is the shift phase of the current unit i.e. the RU block index plus the number of ghost before the unit
  // itself
  int ru_idx = 0;
  int ru_block_idx = 0;
  while (ru_idx + _n_rus_per_nic[ru_block_idx] <= _my_idx) {
    ru_idx += _n_rus_per_nic[ru_block_idx];
    ru_block_idx++;
  }

  _shift_offset = _BU_shift_pattern;
  int shift_idx = get_idx(_RU_shift_pattern.begin(), _RU_shift_pattern.end(), ru_block_idx);

  logger.debug() << "shift idx " << shift_idx << std::flush;

  std::rotate(_shift_offset.begin(), _shift_offset.begin() + shift_idx, _shift_offset.end());

  if (logger.is_active(Online::PrintLevel::DEBUG)) {
    logger.debug() << "shift vectors: ";
    for (auto elem : _shift_offset) {
      logger.debug() << elem << " ";
    }

    logger.debug() << std::flush;
  }

  return DF_SUCCESS;
}

int EB::RU::run()
{
  // Transport_unit::run();
  info("RU run");
  int ret_val = DF_SUCCESS;
  // TODO check if this is a good idea we may don't want to flush the buffers async
  if ((_buffer_type[_my_idx] == PCIe40_frag_buffer) || (_buffer_type[_my_idx] == PCIe40_MFP_buffer)) {
    for (const auto& buffer : _buffers) {
      buffer->flush();
    }
  }
  // reset the number of MFPs written to file
  _n_MFPs_written_to_file = 0;
  _received_data = std::vector<bool>(_n_sources_per_ru[_my_idx], false);
  _send_empty = false;
  init_profiling();
  while (_send_MEPs.load()) {
    // TODO add monitoring counters reset
    std::lock_guard<decltype(_loop_lock)> loop_guard(_loop_lock);
    // TODO add counter rest on end of run

    logger.debug() << "load" << std::flush;
    // TODO check for race conditions when start is executed
    if (_send_empty) {
      ret_val = load_empty();
      _send_empty = false;
    } else {
      ret_val = load_mfps();
    }

    // DF_CANCELLED means that the STOP signal has been received
    // DF_ERROR will trigger a transition into ERROR
    if (ret_val != DF_SUCCESS) {
      break;
    }
    // additional sync barrier
    ret_val = sync(true);
    if (ret_val != DF_SUCCESS) {
      break;
    }

    logger.debug() << "send size" << std::flush;
    ret_val = send_sizes();
    if (ret_val != DF_SUCCESS) {
      break;
    }

    logger.debug() << "shift" << std::flush;
    ret_val = linear_shift();
    if (ret_val != DF_SUCCESS) {
      break;
    }

    if (logger.is_active(Online::PrintLevel::DEBUG)) {
      logger.debug() << "sleep" << std::flush;
      sleep(1);
    }

    update_profiling();

    _run_loop_iteration++;
  }

  // this will trigger the transition of the FSM into error
  if (ret_val == DF_ERROR) {
    fireIncident("DAQ_ERROR");
  }

  return ret_val;
}

int EB::RU::send_sizes()
{
  _send_sizes_timer.start();
  int ret_val =
    _ibComm->ibScatterV(reinterpret_cast<char*>(sizes.data()), _buffers.size(), sizeof(uint32_t), _bu_ranks);
  _send_sizes_timer.stop();
  return ret_val;
}

int EB::RU::send_src_ids()
{
  logger.debug() << "send src ids start" << std::flush;
  int ret_val = DF_SUCCESS;
  std::vector<EB::src_id_type> temp;
  temp.reserve(_n_sources_per_ru[_my_idx]);
  for (const auto& buff : _buffers) {
    temp.push_back(buff->get_src_id());
  }
  logger.debug() << "send src ids " << temp.size() << std::flush;

  ret_val =
    _ibComm->ibBroadcastV(reinterpret_cast<char*>(temp.data()), temp.size(), sizeof(EB::src_id_type), _bu_ranks);
  if (ret_val != DF_SUCCESS) {
    logger.error() << __FUNCTION__ << " src id broadcast failed" << std::flush;
    return ret_val;
  }

  _ibComm->ibDeregMRs(); // clear memory regions
  logger.debug() << "send src ids end" << std::flush;
  return ret_val;
}

int EB::RU::send_src_names()
{
  logger.debug() << "send src names start" << std::flush;
  int ret_val = DF_SUCCESS;
  // TODO check memory allocation
  // TODO memory registration is probably not needed
  auto tmp_buffer = new char[_n_sources_per_ru[_my_idx]][PCIe40_unique_name_str_length];
  for (size_t k = 0; k < _buffers.size(); k++) {
    std::strncpy(tmp_buffer[k], _buffers[k]->get_name().c_str(), PCIe40_unique_name_str_length);
    // strncpy doesn't ensure a null terminated string adding terminator
    tmp_buffer[k][PCIe40_unique_name_str_length - 1] = '\0';
    logger.debug() << "buffer name : " << tmp_buffer[k] << std::endl;
  }
  logger.debug() << "send buffer names " << _buffers.size() << std::flush;

  ret_val = _ibComm->ibBroadcastV(
    reinterpret_cast<char*>(tmp_buffer), _buffers.size(), PCIe40_unique_name_str_length, _bu_ranks);
  if (ret_val != DF_SUCCESS) {
    logger.error() << __FUNCTION__ << " src id broadcast failed" << std::flush;
    return ret_val;
  }

  _ibComm->ibDeregMRs(); // clear memory regions
  delete[] tmp_buffer;
  logger.debug() << "send src names end" << std::flush;
  return ret_val;
}

int EB::RU::linear_shift()
{
  _linear_shift_timer.start();
  int ret_val = DF_SUCCESS;

  for (const auto& shift : _shift_offset) {
    // sync mpi barrier
    logger.debug() << "sync" << std::flush;
    ret_val = sync(false);
    if (ret_val != DF_SUCCESS) {
      return ret_val;
    }
    logger.debug() << "on shift " << shift << std::flush;
    // skip the ghost nodes
    if (shift != -1) {
      ret_val = send_MFPs(shift);
      if (ret_val != DF_SUCCESS) {
        return ret_val;
      }
    }
  }
  logger.debug() << "send complete" << std::flush;
  // TODO check if this is the best place for this piece of code
  update_buffer_usage();
  for (size_t src = 0; src < _buffers.size(); src++) {
    _buffers[src]->read_complete();
  }
  _linear_shift_timer.stop();
  return ret_val;
}

int EB::RU::send_MFPs(int shift_off)
{
  _send_timer.start();
  int ret_val = DF_SUCCESS;
  int comm_err;
  size_t MFPs_size = _buffers.size();
  // this may be done by shift_offset[shift]
  int dst = _bu_ranks[shift_off];
  size_t shift_idx = shift_off * _buffers.size();
  std::vector<comm_send> send_vec(MFPs_size);
  size_t total_size = 0;
  for (size_t k = 0; (k < MFPs_size); k++) {
    if (logger.is_active(Online::PrintLevel::DEBUG)) {
      logger.debug() << "Comm_Send MFPs " << k << " size " << sizes[shift_idx + k] << " dst " << dst << "\n";
      if (selected_MFPs[shift_idx + k]) {
        if (logger.is_active(Online::PrintLevel::VERBOSE)) {
          logger.debug() << reinterpret_cast<const EB::MFP*>(selected_MFPs[shift_idx + k])->print(false);
          logger.debug() << std::flush;
        } else {
          logger.debug() << reinterpret_cast<const EB::MFP*>(selected_MFPs[shift_idx + k])->print(true);
        }
      }
      logger.debug() << std::flush;
    }
    auto& send = send_vec.at(k);
    send.snd_buff = reinterpret_cast<const void*>(selected_MFPs[shift_idx + k]);
    send.count = sizes[shift_idx + k];
    send.datatype = sizeof(char);
    send.destination = dst;
    total_size += sizes[shift_idx + k];
  }
  comm_err = _ibComm->send(send_vec);
  if (comm_err == DF_CANCELLED) {
    ret_val = comm_err;
  } else if (comm_err != DF_SUCCESS) {
    logger.error() << __FUNCTION__ << " send " << comm_err << std::flush;
    ret_val = comm_err;
  }
  // k=0 first source is taken into account to the event flow
  if (selected_MFPs[shift_idx] != nullptr) {
    _DF_events_out += reinterpret_cast<const EB::MFP*>(selected_MFPs[shift_idx])->header.n_banks;
  }

  _MFP_count += _buffers.size();
  _bytes_out += total_size;
  _send_timer.stop();
  return ret_val;
}

int EB::RU::load_mfp(int src, MFP*& next_MFP)
{
  int ret_val = DF_SUCCESS;
  try {
    next_MFP = _buffers[src]->try_get_element();
  } catch (const std::exception& e) {
    logger.error() << __FUNCTION__ << " Exception while reading buffer " << src << " src " << std::hex << "0x"
                   << _buffers[src]->get_src_id() << std::dec << "/" << _buffers[src]->get_name() << " " << e.what()
                   << std::flush;
    ret_val = DF_ERROR;
    return ret_val;
  } catch (...) {
    logger.error() << __FUNCTION__ << " unexpected exception while reading buffer " << src << " src " << std::hex
                   << "0x" << _buffers[src]->get_src_id() << std::dec << "/" << _buffers[src]->get_name() << std::flush;
    ret_val = DF_ERROR;
    return ret_val;
  }
  return ret_val;
}

int EB::RU::load_mfps()
{
  _load_mpfs_timer.start();
  int ret_val = DF_SUCCESS;
  std::vector<EB::MFP*> ptrs(_bu_ranks.size());
  // We clear all the slots
  load_empty();
  // every dst will get a different MFP from every src
  for (size_t dst = 0; dst < _bu_ranks.size(); dst++) {
    size_t n_loaded_mfps = 0;
    std::vector<EB::Timer> idle_timers(_buffers.size());
    std::list<unsigned int> missing_srcs(_buffers.size());
    // reset the list of missing sources
    std::iota(missing_srcs.begin(), missing_srcs.end(), 0);
    // preserve the idle status between while loops iterations
    // we need one MFP from every src
    // loop until all elems in loaded are true
    update_dim_services();
    // iterate over the srcs
    while (!missing_srcs.empty()) {
      for (auto it = missing_srcs.begin(); it != missing_srcs.end(); it++) {
        // if we received a CANCEL signal we return immediately
        if (!_send_MEPs) {
          logger.info() << "STOP received cancelling pending IO" << std::flush;
          return DF_CANCELLED;
        }
        EB::MFP* next_MFP = NULL;
        idle_timers[*it].start();

        ret_val = load_mfp(*it, next_MFP);
        if (ret_val != DF_SUCCESS) {
          return ret_val;
        }

        _idle_buffer_mask[*it] = next_MFP == nullptr;
        if (next_MFP == nullptr) {
          if (idle_timers[*it].get_elapsed_time_s() > _buffer_timeout) {
            logger.warning() << __FUNCTION__ << " no data from buffer " << *it << " src ID " << std::hex << "0x"
                             << _buffers[*it]->get_src_id() << std::dec << "/" << _buffers[*it]->get_name()
                             << std::flush;
            // update the service
            idle_timers[*it].reset();
            update_dim_services();
          }
        } else {
          if (!next_MFP->is_valid()) {
            // this error is fatal, if the MFP stream is corrupted we can't read the size
            // The Tell40 reader will throw an exception before.
            logger.error() << "invalid MFP from src " << std::hex << "0x" << _buffers[*it]->get_src_id() << std::dec
                           << "/" << _buffers[*it]->get_name() << std::flush;
            _DF_events_err += 1;
            return DF_ERROR;
          }

          if (next_MFP->is_end_run()) {
            next_MFP = NULL;
            // we skip any consecutive flushes before the beginning of the run
            // This is a real end of run
            if (_received_data[*it]) {
              // the end of run will trigger a round of zero-sized MFPs
              _end_of_run = true;
              _send_empty = true;
              auto offset = reinterpret_cast<uintptr_t>(next_MFP) -
                            reinterpret_cast<uintptr_t>(std::get<void*>(_buffers[*it]->get_full_buffer()[0]));
              logger.warning() << "end of run. Offset " << std::hex << "0x" << offset << std::dec << std::flush;
              // end of run counts as loaded
              // we mark the source as received
              missing_srcs.erase(it--);
            }
          } else {
            // TODO check if this keeps coherency in case of truncated events
            // fragments from multiple data sources are counted only once
            if (*it == 0) {
              _DF_events_in += next_MFP->header.n_banks;
            }
            // assert the flag we received at least one MFP
            _received_data[*it] = true;

            if (_end_of_run) {
              // reset performance counters when a new run starts
              reset_counters();
            }
            _end_of_run = false;

            // print MFP for debug
            ret_val = debug_print_MFP(next_MFP);
            if (ret_val != DF_SUCCESS) {
              return ret_val;
            }

            // Write MFPs to file for debug
            ret_val = MFP_dump_to_file(next_MFP);
            if (ret_val != DF_SUCCESS) {
              return ret_val;
            }

            int idx = dst * _buffers.size() + *it;
            selected_MFPs[idx] = reinterpret_cast<const char*>(next_MFP);
            sizes[idx] = next_MFP->bytes();
            _bytes_in += sizes[idx];

            // we mark the source as received
            missing_srcs.erase(it--);
          }
        }
      }
    }
    if (_end_of_run) {
      break;
    }
  }
  _load_mpfs_timer.stop();

  return ret_val;
}

int EB::RU::debug_print_MFP(const MFP* next_MFP)
{
  int ret_val = DF_SUCCESS;
  if (logger.is_active(Online::PrintLevel::DEBUG)) {
    logger.debug() << "NEXT MFP\n";
    try {
      if (logger.is_active(Online::PrintLevel::VERBOSE)) {
        logger.debug() << next_MFP->print(true);
        logger.debug() << std::flush;
      } else {
        logger.debug() << next_MFP->print(false);
      }
    } catch (const std::exception& e) {
      logger.error() << __FUNCTION__ << " Exception while printing MFP " << next_MFP << " " << e.what() << std::flush;
      return DF_ERROR;
    }
    logger.debug() << std::flush;
  }

  return ret_val;
}

int EB::RU::MFP_dump_to_file(const MFP* next_MFP)
{
  int ret_val = DF_SUCCESS;
  if (_write_to_file[_my_idx] && (_n_MFPs_written_to_file < _n_MFPs_to_file)) {
    // TODO add error checking
    try {
      _file_writer.write(next_MFP);
    } catch (const std::exception& e) {
      logger.error() << __FUNCTION__ << " Exception while dumping MFP " << next_MFP << " to file " << e.what()
                     << std::flush;
      return DF_ERROR;
    }
    _n_MFPs_written_to_file++;
  }

  return ret_val;
}

int EB::RU::load_empty()
{
  _load_mpfs_timer.start();
  for (size_t src = 0; src < _buffers.size(); src++) {
    for (size_t dst = 0; dst < _bu_ranks.size(); dst++) {
      int idx = dst * _buffers.size() + src;
      selected_MFPs[idx] = NULL;
      sizes[idx] = 0;
    }
  }
  _load_mpfs_timer.stop();
  return DF_SUCCESS;
}

int EB::RU::init_dummy_src_ids()
{
  int ret_val = DF_SUCCESS;

  if (_dummy_src_ids.size() == 0) {
    _dummy_src_ids.resize(_prefix_n_sources_per_ru[_ru_ranks.size()]);
    // if src_ids are not assigned values from 0 to n-1 are used
    std::iota(_dummy_src_ids.begin(), _dummy_src_ids.end(), 0);
  }

  if (logger.is_active(PrintLevel::DEBUG)) {
    logger.debug() << "src ids: ";
    for (const auto& elem : _dummy_src_ids) {
      logger.debug() << std::hex << "0x" << elem << std::dec << " ";
    }
    logger.debug() << std::flush;
  }

  // check config

  if (_dummy_src_ids.size() != _prefix_n_sources_per_ru.back()) {
    logger.error() << __FUNCTION__ << " configuration error: incorrect number of src IDs. Got " << _dummy_src_ids.size()
                   << " IDs for " << _prefix_n_sources_per_ru.back() << " sources" << std::flush;
    return DF_ERROR;
  }

  return ret_val;
}

int EB::RU::init_dim_services()
{
  int ret_val = DF_SUCCESS;

  _idle_buffer_names = "";
  _idle_buffer_mask = std::vector<bool>(_n_sources_per_ru[_my_idx], false);
  _idle_monitor_service_name = RTL::processName() + "/" + _idle_monitor_service_name.c_str();
  _idle_buffer_service =
    std::make_unique<DimService>(_idle_monitor_service_name.c_str(), const_cast<char*>(_idle_buffer_names.c_str()));

  update_dim_services();

  return ret_val;
}

void EB::RU::update_dim_services()
{
  // we make a new string and update only if things has changed
  std::string new_idle_buffer_names("");
  for (size_t k = 0; k < _idle_buffer_mask.size(); k++) {
    if (_idle_buffer_mask[k]) {
      new_idle_buffer_names += _buffers[k]->get_name() + ",";
    }
  }

  // we remove the last , if present
  if (!new_idle_buffer_names.empty()) {
    new_idle_buffer_names.pop_back();
  }

  if (new_idle_buffer_names != _idle_buffer_names) {
    _idle_buffer_names = new_idle_buffer_names;
    _idle_buffer_service->updateService(const_cast<char*>(_idle_buffer_names.c_str()));
  }
}

int EB::RU::close_dim_services()
{
  int ret_val = DF_SUCCESS;

  _idle_buffer_service.reset();

  return ret_val;
}

void EB::RU::reset_counters()
{
  // reset counters of the base class
  Transport_unit::reset_counters();
  logger.info() << "resetting counters" << std::flush;
  _run_loop_iteration = 0;
  _load_mpfs_time_counter = 0;
  _send_sizes_time_counter = 0;
  _linear_shift_time_counter = 0;
  _send_time_counter = 0;

  _source_buffer_low_occ = {};
  _source_buffer_med_occ = {};
  _source_buffer_high_occ = {};
}

void EB::RU::update_buffer_usage()
{
  auto n_monitored_buffers = std::min(_source_buffer_low_occ.size(), _buffers.size());
  for (int k = 0; k < n_monitored_buffers; k++) {
    auto buff_percentage = double(_buffers[k]->get_buffer_occupancy()) / _buffers[k]->get_buffer_size() * 100;
    if (buff_percentage <= _buffer_low_occupancy_thr) {
      _source_buffer_low_occ[k]++;
    } else if (buff_percentage <= _buffer_high_occupancy_thr) {
      _source_buffer_med_occ[k]++;
    } else {
      _source_buffer_high_occ[k]++;
    }
  }
}

void EB::RU::update_profiling()
{
  if (_enable_profiling) {
    double total_time = _total_timer.get_elapsed_time_s();
    if (total_time > _profiling_update_interval) {
      // This is here to prevent double evaluation of the total_timer
      EB::Transport_unit::update_profiling();
      double bw = (_bytes_out * 8 * 1e-9) / _total_time_counter;

      double load_mpfs_time = _load_mpfs_timer.get_elapsed_time_s();
      double send_sizes_time = _send_sizes_timer.get_elapsed_time_s();
      double linear_shift_time = _linear_shift_timer.get_elapsed_time_s();
      double send_time = _send_timer.get_elapsed_time_s();

      _load_mpfs_time_counter += load_mpfs_time;
      _send_sizes_time_counter += send_sizes_time;
      _linear_shift_time_counter += linear_shift_time;
      _send_time_counter += send_time;

      if (logger.is_active(Online::PrintLevel::INFO)) {
        logger.info() << " load " << load_mpfs_time << " s " << load_mpfs_time / total_time * 100 << " %" << std::flush;
        logger.info() << " send size " << send_sizes_time << " s " << send_sizes_time / total_time * 100 << " %"
                      << std::flush;
        logger.info() << " linear shift " << linear_shift_time << " s " << linear_shift_time / total_time * 100 << " %"
                      << std::flush;
        logger.info() << " send data " << send_time << " s " << send_time / total_time * 100 << " %" << std::flush;
        logger.info() << " BW " << bw << " Gb/s" << std::flush;
      }

      _load_mpfs_timer.reset();
      _send_sizes_timer.reset();
      _linear_shift_timer.reset();
      _send_timer.reset();
    }
  }
}

void EB::RU::init_profiling()
{
  EB::Transport_unit::init_profiling();

  if (_enable_profiling) {
    _load_mpfs_timer.stop();
    _load_mpfs_timer.reset();
    _send_sizes_timer.stop();
    _send_sizes_timer.reset();
    _linear_shift_timer.stop();
    _linear_shift_timer.reset();
    _send_timer.stop();
    _send_timer.reset();
  } else {
    _load_mpfs_timer.disable();
    _send_sizes_timer.disable();
    _linear_shift_timer.disable();
    _send_timer.disable();
  }
}