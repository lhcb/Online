#include "buffer_writer_error.hpp"

// Error category definition
namespace {
  struct Buffer_writer_error_category : std::error_category {
    const char* name() const noexcept override;
    std::string message(int ev) const override;
  };

  const char* Buffer_writer_error_category::name() const noexcept { return "buffer_writer"; }
  std::string Buffer_writer_error_category::message(int ev) const
  {
    switch (static_cast<EB::Buffer_writer_error>(ev)) {
    case EB::Buffer_writer_error::ALLOC_FAILED: return "Unable to allocate memory";
    case EB::Buffer_writer_error::WRITE_FULL: return "Write to a full buffer";
    case EB::Buffer_writer_error::BUFFER_OVERFLOW: return "Buffer overflow";
    case EB::Buffer_writer_error::NOT_INIT: return "Write to a non initialized buffer";
    default: return "Undefined error";
    }
  }

  const Buffer_writer_error_category the_buffer_writer_error_category{};

} // namespace

std::error_code EB::make_error_code(EB::Buffer_writer_error e)
{
  return {static_cast<int>(e), the_buffer_writer_error_category};
}