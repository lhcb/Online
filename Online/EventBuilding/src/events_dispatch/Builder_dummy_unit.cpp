// Framework include files
#include "Dataflow/Incidents.h"
#include "Dataflow/DataflowComponent.h"
#include "events_dispatch/common.hpp"
#include "events_dispatch/Builder_dummy_unit.hpp"

#include <mutex>
#include <glib.h>

using namespace std;
using namespace Online;
using namespace DISPATCH;
using namespace EB;

/// Standard Constructor
Builder_dummy_unit::Builder_dummy_unit(const string& nam, DataflowContext& ctxt) : DataflowComponent(nam, ctxt), Unit()
{
  info("BDU: IN CONSTRUCTOR");
  this->_delay_timer = 0;

  declareProperty("how_many_ous", how_many_ous = 1);
  declareProperty("how_many_fus", how_many_fus = 1);
  declareProperty("buffer_type", _buffer_type = 1);
  declareProperty("MBM_name", _mbm_name = "Buffer");
}

/// IService implementation: initialize the service
int Builder_dummy_unit::initialize()
{
  info("BDU: IN INTIALIZE");
  int sc = Component::initialize();
  if (sc != DF_SUCCESS) return error("Failed to initialize base-class");
  MPI_Init(NULL, NULL);
  MPI_Comm_rank(MPI_COMM_WORLD, &(this->_rank));
  MPI_Comm_size(MPI_COMM_WORLD, &(this->_worldSize));

  info("BDU: MY RANK: %d, OUs NUMBER: %d, FUs NUMBER: %d", this->_rank, how_many_ous, how_many_fus);
  EB::Buffer_writer<EB::MEP>* write_buff;
  if (_buffer_type == 1) {
    int bdu_no = (this->_rank) - 1 - how_many_ous - how_many_fus;
    info("BDU: CREATED SHMEM NUMBER :  %d", bdu_no);

    const std::string shmem_name = get_shmem_name("BU", bdu_no).str();
    size_t buff_size =
      static_cast<size_t>(BUFFER_RECORD_SIZE_MB) * static_cast<size_t>(MAX_BUFFER_COUNT_BU) * 1024UL * 1024UL;
    write_buff = new Shmem_buffer_writer<EB::MEP>(shmem_name, buff_size);
  } else if (_buffer_type == 2) {
    info("WRITE BUFFER IS MBM\n");
    write_buff = new Mbm_writer<EB::MEP>(context, RTL::processName(), _mbm_name);
  } else {
    // fpisani possible uncorrect init if _buffer_size is not a defined value
    return error("Unsupported buffer type: %d", _buffer_type);
  }

  this->_buff_writer = write_buff;

  // disabled for the WinCC runs
  if (use_debug_mpi_barriers) MPI_Barrier(MPI_COMM_WORLD);
  return sc;
}

/// IService implementation: finalize the service
int Builder_dummy_unit::finalize()
{
  MPI_Finalize();
  return DF_SUCCESS;
}

/// TODO
int Builder_dummy_unit::stop() { return DF_SUCCESS; }

/// Cancel I/O operations of the dataflow component
int Builder_dummy_unit::cancel()
{
  _m_receiveEvts = false;
  return DF_SUCCESS;
}

/// Incident handler implemenentation: Inform that a new incident has occured
void Builder_dummy_unit::handle(const DataflowIncident& inc)
{
  info("BDU Got incident: %s of type %s", inc.name.c_str(), inc.type.c_str());
  // TODO implement this
}

int Builder_dummy_unit::pause()
{
  // TODO implement this
  info("BDU pause\n");
  return DF_SUCCESS;
}

/// IRunable implementation : Run the class implementation
int Builder_dummy_unit::run()
{
  info("BDU: IN RUN");

  int64_t probe_start;
  this->_probe_global_start = g_get_real_time();
  probe_start = _probe_global_start;
  this->_now = probe_start;
  this->_timer_start = probe_start;

  // fpisani removing unused variable
  // int64_t probe_now = g_get_real_time();

  info("BDU rank %d IN RUN: Warmup done.  Starting main loop \n", this->_rank);
  generate_meps();

  while (true) {
    if (time_elapsed_check() == true) {
      // fpisani removing unused variable
      // probe_now = g_get_real_time();

      // info(
      //  "[ %.1lf s ] BDU RANK %d : Generating data \n",
      //  static_cast<double>((probe_now) - (probe_global_start)) / static_cast<double>(US_S),
      //  rank);
      generate_meps();
    }
  }
  return DF_SUCCESS;
}

/// Service implementation: start of the service
// TODO
int Builder_dummy_unit::start()
{
  info("BDU: IN START");
  _m_receiveEvts = true;

  // disabled for the WinCC runs
  if (use_debug_mpi_barriers) MPI_Barrier(MPI_COMM_WORLD);
  return DF_SUCCESS;
}

void Builder_dummy_unit::generate_meps()
{

  FBUFF::generate_data<EB::MEP>(this->_buff_writer, _rank, gibit_events_in_one_quantum);
}

bool Builder_dummy_unit::time_elapsed_check()
{
  _delay_timer++;
  if (_delay_timer >= PROBE_EVERY_IT) {
    _delay_timer = 0;
    _now = g_get_real_time();
    if (_now - _timer_start >= TIME_ELAPSED_FOR_GENERATION) {
      _timer_start += TIME_ELAPSED_FOR_GENERATION;
      return true;
    }
  }
  return false;
}
