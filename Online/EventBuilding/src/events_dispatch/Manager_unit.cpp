#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <malloc.h>
#include <assert.h>
#include <ctime>
#include <thread>
#include <glib.h>
#include <limits>

#include "Dataflow/Incidents.h"
#include "Dataflow/MBMClient.h"
#include "RTL/rtl.h"

#include "events_dispatch/Unit.hpp"
#include "events_dispatch/Manager_unit.hpp"
#include "events_dispatch/common.hpp"
#include "unused.hpp"

using namespace Online;
using namespace std;
using namespace DISPATCH;

Manager_unit::Manager_unit(const string& nam, DataflowContext& ctxt) : DataflowComponent(nam, ctxt), Unit()
{
  declareProperty("how_many_ous", _how_many_ous = 1);
  declareProperty("how_many_fus", _how_many_fus = 1);
}

int Manager_unit::initialize()
{

  info("MU: IN INTIALIZE");

  int sc = Component::initialize();
  if (sc != DF_SUCCESS) return error("Failed to initialize base-class");
  MPI_Init(NULL, NULL);
  MPI_Comm_rank(MPI_COMM_WORLD, &(this->_rank));
  MPI_Comm_size(MPI_COMM_WORLD, &(this->_worldSize));

  info("MU: MY RANK: %d, OUs NUMBER: %d, FUs NUMBER: %d", this->_rank, _how_many_ous, _how_many_fus);

  this->_bu_to_grant = 0;
  this->_fu_to_grant = 0;

  this->_bus_ranks = new int[_how_many_ous];
  this->_fus_ranks = new int[_how_many_fus];
  this->_bus_meps_granted_bytes = new size_t[_how_many_ous];
  this->_fus_ranks_ready = new int[_how_many_fus];
  this->_bu_grants = new int[_how_many_ous];
  this->_free_bu_grants = new int[_how_many_ous];
  this->_granted_bu_for_fu = new int[_how_many_fus];
  this->_trans_fu_no = new int[_how_many_fus];

  this->_fu_ready_requests = new MPI_Request[_how_many_fus];
  this->_bu_ready_requests = new MPI_Request[_how_many_ous];

  this->_fu_ready_messages = new mpi_fu_ready_msg[_how_many_fus];
  this->_bu_ready_messages = new mpi_bu_ready_msg[_how_many_ous];

  this->_trans_grant_messages = new mpi_bu_transmission_grant_msg[_how_many_ous];
  this->_trans_grant_requests = new MPI_Request[_how_many_ous];

  this->_bu_meps_sizes_vectors = new std::vector<size_t>[_how_many_ous];

  this->_ready_mep_array = new size_t[MAX_MEPS];

  this->_bus_total_granted_bytes_array = new size_t[_how_many_ous];
  for (int rank_it = 0; rank_it != _how_many_ous; rank_it++) {
    _bus_total_granted_bytes_array[rank_it] = 0;
  }

  this->_fus_total_granted_bytes_array = new size_t[_how_many_fus];
  for (int rank_it = 0; rank_it != _how_many_fus; rank_it++) {
    _fus_total_granted_bytes_array[rank_it] = 0;
  }

  for (int rank_it = 0; rank_it != _how_many_ous; rank_it++) {
    this->_free_bu_grants[rank_it] = MAX_BU_CREDITS;
    this->_bus_meps_granted_bytes[rank_it] = 0;
    this->_bus_ranks[rank_it] = rank_it + 1;
    this->_trans_grant_requests[rank_it] = MPI_REQUEST_NULL;
    this->_bu_ready_requests[rank_it] = MPI_REQUEST_NULL;
    this->_bu_grants[rank_it] = 0;
  }

  for (int rank_it = 0; rank_it != _how_many_fus; rank_it++) {
    _fu_ready_requests[rank_it] = MPI_REQUEST_NULL;
    this->_fus_ranks_ready[rank_it] = 0;
    this->_fus_ranks[rank_it] = rank_it + 1 + _how_many_ous;
    info("MU: fu rank is %d for index %d \n");
  }

  // TODO - disable for the WinCC runs
  if (use_debug_mpi_barriers) MPI_Barrier(MPI_COMM_WORLD);

  return DF_SUCCESS;
}

int Manager_unit::finalize()
{
  MPI_Finalize();
  return DF_SUCCESS;
}
/// IService implementation: finalize the service
/// TODO
int Manager_unit::stop()
{
  // TODO implement this
  return DF_SUCCESS;
}

/// Cancel I/O operations of the dataflow component
int Manager_unit::cancel()
{
  // TODO implement this
  return DF_SUCCESS;
}

/// Stop gracefully execution of the dataflow component
int Manager_unit::pause()
{
  // TODO implement this
  return DF_SUCCESS;
}

/// Incident handler implemenentation: Inform that a new incident has occured
void Manager_unit::handle(const DataflowIncident& inc)
{
  // TODO implement this
  info("MU Got incident: %s of type %s", inc.name.c_str(), inc.type.c_str());
}

int Manager_unit::run()
{
  info("MU: IN RUN");
  if (DO_WARMUP) mpi_warmup_run();

  info("MU rank %d IN RUN: Warmup done.  Starting main loop \n", this->_rank);
  while (true) {
    get_ready_fus();
    get_meps_bus();
    grant_transmissions_to_bus();
  }

  return DF_SUCCESS;
}

int Manager_unit::start()
{
  info("MU: IN START");

  this->_start_probe = g_get_real_time();

  initialize_receiving_fus();
  initialize_receiving_bus();

  // TODO - disable for the WinCC runs
  if (use_debug_mpi_barriers) MPI_Barrier(MPI_COMM_WORLD);

  return DF_SUCCESS;
}

void Manager_unit::get_ready_fus()
{
  int recv_flag = false;
  int recv_index;

  MPI_Testany(_how_many_fus, _fu_ready_requests, &recv_index, &recv_flag, MPI_STATUS_IGNORE);
  if (recv_flag == true) {

    fu_status status = _fu_ready_messages[recv_index].status;

    // transmission finished - free resources
    if (status == TRANSMISSION_DONE) {
      int relieved_bu_rank = _granted_bu_for_fu[recv_index];
      _bu_grants[relieved_bu_rank]--;
      _fus_ranks_ready[recv_index]++;
    }

    MPI_Wait(&(_fu_ready_requests[recv_index]), MPI_STATUS_IGNORE);
    MPI_Irecv(
      &(_fu_ready_messages[recv_index]),
      sizeof(mpi_fu_ready_msg),
      MPI_CHAR,
      _fus_ranks[recv_index],
      MU_TAG,
      MPI_COMM_WORLD,
      &(_fu_ready_requests[recv_index]));
  }
}

void Manager_unit::grant_transmissions_to_bus()
{

  int fu_ind, ou_ind;
  bool rdy_ou = get_next_bu_rank_ready_w_lowest_bytes(&ou_ind);
  bool rdy_fu;
  if (rdy_ou) rdy_fu = get_next_free_fu_rank_ready_w_lowest_bytes(&fu_ind, ou_ind);
  if (rdy_ou && rdy_fu) {

    size_t mep_size = get_next_bu_mep_size_to_transmit(ou_ind);
    bool alloc_attempt = try_fu_alloc(fu_ind, mep_size);
    if (alloc_attempt != true) {
      return;
    }
    // alloc succesful, continuing

    erase_next_bu_mep_size(ou_ind);

    _bus_total_granted_bytes_array[ou_ind] += mep_size;
    _fus_total_granted_bytes_array[fu_ind] += mep_size;
    this->_bu_grants[ou_ind]++;
    this->_granted_bu_for_fu[fu_ind] = ou_ind;
    _fus_ranks_ready[fu_ind]--;

    send_bus_transmisison_grant(ou_ind, fu_ind);
  }
}

void Manager_unit::initialize_receiving_bus()
{
  for (int i = 0; i < _how_many_ous; i++) {
    MPI_Wait(&(_bu_ready_requests[i]), MPI_STATUS_IGNORE);
    MPI_Irecv(
      &(_bu_ready_messages[i]),
      sizeof(mpi_bu_ready_msg),
      MPI_CHAR,
      _bus_ranks[i],
      MU_TAG,
      MPI_COMM_WORLD,
      &(_bu_ready_requests[i]));
  }
}

void Manager_unit::initialize_receiving_fus()
{
  for (int i = 0; i < _how_many_fus; i++) {
    MPI_Wait(&(_fu_ready_requests[i]), MPI_STATUS_IGNORE);
    MPI_Irecv(
      &(_fu_ready_messages[i]),
      sizeof(mpi_fu_ready_msg),
      MPI_CHAR,
      _fus_ranks[i],
      MU_TAG,
      MPI_COMM_WORLD,
      &(_fu_ready_requests[i]));

    MPI_Wait(&(_fu_ready_requests[i]), MPI_STATUS_IGNORE);

    // info("EM INIT: GOT MESSAGE FROM FU RANK %d. ITS FREE SPACE %ld \n  ",fus_ranks[i],fus_free_bytes_array[i]);

    MPI_Irecv(
      &(_fu_ready_messages[i]),
      sizeof(mpi_fu_ready_msg),
      MPI_CHAR,
      _fus_ranks[i],
      MU_TAG,
      MPI_COMM_WORLD,
      &(_fu_ready_requests[i]));

    _fus_ranks_ready[i]++;
  }
}

void Manager_unit::get_meps_bus()
{
  int recv_flag = false;
  int recv_index;
  MPI_Testany(_how_many_ous, _bu_ready_requests, &recv_index, &recv_flag, MPI_STATUS_IGNORE);
  if (recv_flag == true) {
    // info("MU started exchanging message with BU \n");
    size_t untransmitted_meps_no = _bu_ready_messages[recv_index].untransmitted_meps_number;
    int bu_rank = _bu_ready_messages[recv_index].rank;
    MPI_Recv(
      _ready_mep_array,
      untransmitted_meps_no * sizeof(size_t),
      MPI_CHAR,
      bu_rank,
      MU_MEPS_TAG,
      MPI_COMM_WORLD,
      MPI_STATUS_IGNORE);

    get_vector_of_meps(recv_index, untransmitted_meps_no);

    // fpisani removing unused variable
    // int64_t now = g_get_real_time();
    // info("MU GOT OU MESSAGE \n");
    // info("[ %.2lf s ] EM GOT OU MESSAGE \n",(now-start_timer)/(double)US_S);
    MPI_Wait(&(_bu_ready_requests[recv_index]), MPI_STATUS_IGNORE);
    MPI_Irecv(
      &(_bu_ready_messages[recv_index]),
      sizeof(mpi_bu_ready_msg),
      MPI_CHAR,
      _bus_ranks[recv_index],
      MU_TAG,
      MPI_COMM_WORLD,
      &(_bu_ready_requests[recv_index]));
  }
}

void Manager_unit::send_bus_transmisison_grant(int ou_ind, int fu_ind)
{
  _trans_grant_messages[ou_ind].send_to_which_fu = _fus_ranks[fu_ind];
  MPI_Wait(&(_trans_grant_requests[ou_ind]), MPI_STATUS_IGNORE);
  MPI_Isend(
    &(_trans_grant_messages[ou_ind]),
    sizeof(mpi_bu_transmission_grant_msg),
    MPI_CHAR,
    _bus_ranks[ou_ind],
    MU_TAG,
    MPI_COMM_WORLD,
    &(_trans_grant_requests[ou_ind]));
}

int Manager_unit::get_bu_rank_ready_with_least_rus()
{
  this->_bu_to_grant++;
  if (this->_bu_to_grant >= _how_many_ous) this->_bu_to_grant = 0;
  return this->_bu_to_grant;
}

int Manager_unit::get_next_bu_ind()
{
  this->_bu_to_grant++;
  if (this->_bu_to_grant >= _how_many_ous) this->_bu_to_grant = 0;
  return this->_bu_to_grant;
}

int Manager_unit::get_next_fu_ind()
{
  this->_fu_to_grant++;
  if (this->_fu_to_grant >= _how_many_fus) this->_fu_to_grant = 0;
  return this->_fu_to_grant;
}

// fpisani unsed variable
bool Manager_unit::get_next_free_fu_rank_ready_w_lowest_bytes(int* fu_index, int UNUSED(chosen_bu_index))
{
  // info("EM in getting FU\n");
  int tested_fu_indx = this->_fu_to_grant;
  int min_indx;
  bool was_free = false;

  for (int i = 0; i != _how_many_fus; i++) {

    tested_fu_indx = get_next_fu_ind();

    if (_fus_ranks_ready[tested_fu_indx] != 0) {
      // info("EM HAS AVAILABLE FU RANK %d with granted credits so far:
      // %d\n",fusRanks[indx],fuGrantedCreditsArray[indx]);
      if (was_free == false) {
        was_free = true;
        min_indx = tested_fu_indx;
        *fu_index = tested_fu_indx;
      } else if (_fus_total_granted_bytes_array[min_indx] > _fus_total_granted_bytes_array[tested_fu_indx]) {
        // info("EM index min value: %d  current index value: %d min index: %d current index: %d
        // \n",fuGrantedCreditsArray[min_indx],fuGrantedCreditsArray[indx], min_indx,indx);
        // info("EM i : %d. MIN INDX %d TESTED FU INDX %d\n",i,min_indx,tested_fu_indx);
        min_indx = tested_fu_indx;
        *fu_index = min_indx;
      }
    }
  }
  return was_free;
}

bool Manager_unit::get_next_bu_rank_ready_w_lowest_bytes(int* bu_index)
{
  int tested_bu_indx = this->_bu_to_grant;
  int min_indx;
  bool was_free = false;

  for (int i = 0; i != _how_many_ous; i++) {
    tested_bu_indx = get_next_bu_ind();
    if (_bu_meps_sizes_vectors[tested_bu_indx].size() != 0 && this->_bu_grants[tested_bu_indx] < MAX_BU_CREDITS) {
      // info("EM HAS AVAILABLE OU RANK %d with granted credits so far:
      // %d\n",busRanks[indx],buGrantedCreditsArray[indx]);
      if (was_free == false) {
        was_free = true;
        min_indx = tested_bu_indx;
        *bu_index = tested_bu_indx;
      } else {
        if (_bus_total_granted_bytes_array[min_indx] > _bus_total_granted_bytes_array[tested_bu_indx]) {
          min_indx = tested_bu_indx;
          *bu_index = tested_bu_indx;
        }
      }
    }
  }
  return was_free;
}

void Manager_unit::mpi_warmup_run()
{
  /*
    // INITIALIZE BU->EM messages path (EVENTS READY)
    for (int i = 0; i < how_many_ous; i++) {
      MPI_Irecv(
        &(bu_ready_messages[i]),
        sizeof(mpi_bu_ready_msg),
        MPI_CHAR,
        bus_ranks[i],
        MU_TAG,
        MPI_COMM_WORLD,
        &(bu_ready_requests[i]));
      MPI_Wait(&(bu_ready_requests[i]), MPI_STATUS_IGNORE);
    }

    // INITIALIZE EM->OU messages path (GRANT TRANSMISSION)
    for (int i = 0; i < how_many_ous; i++) {
      MPI_Isend(
        &(trans_grant_messages[i]),
        sizeof(mpi_bu_transmission_grant_msg),
        MPI_CHAR,
        bus_ranks[i],
        MU_TAG,
        MPI_COMM_WORLD,
        &(trans_grant_requests[i]));
      MPI_Wait(&(trans_grant_requests[i]), MPI_STATUS_IGNORE);
      //
    MPI_Irecv(&(buReadyMessages[i]),sizeof(MpiBuReadyMsg),MPI_CHAR,busRanks[i],MU_TAG,MPI_COMM_WORLD,&(buReadyRequests[i]));
      // MPI_Wait(&(buReadyRequests[i]), MPI_STATUS_IGNORE);
    }

    // INITIALIZE FU->EM (READINESS)
    for (int i = 0; i < how_many_fus; i++) {
      MPI_Irecv(
        &(fu_ready_messages[i]),
        sizeof(mpi_fu_ready_msg),
        MPI_CHAR,
        fus_ranks[i],
        MU_TAG,
        MPI_COMM_WORLD,
        &(fu_ready_requests[i]));
      MPI_Wait(&(fu_ready_requests[i]), MPI_STATUS_IGNORE);
      // info("EM GOT FROM RANK  %d\n",fusRanks[i]);
    }
  */
}

size_t Manager_unit::get_next_bu_mep_size_to_transmit(int bu_ind)
{
  size_t size = _bu_meps_sizes_vectors[bu_ind][0];
  return size;
}

void Manager_unit::erase_next_bu_mep_size(int bu_ind)
{
  _bu_meps_sizes_vectors[bu_ind].erase(_bu_meps_sizes_vectors[bu_ind].begin());
}

bool Manager_unit::try_fu_alloc(int fu_ind, size_t size)
{
  MPI_Request alloc_request;
  // info("EM trying alloc with fu %d and size %ld", fus_ranks[fu_ind], size);
  mu_fu_alloc_msg msg;
  msg.size = size;
  MPI_Isend(
    &msg, sizeof(mu_fu_alloc_msg), MPI_CHAR, _fus_ranks[fu_ind], MU_FU_ALLOC_TAG, MPI_COMM_WORLD, &alloc_request);
  MPI_Wait(&alloc_request, MPI_STATUS_IGNORE);
  // info("EM sent the message \n");
  MPI_Recv(
    &msg, sizeof(mu_fu_alloc_msg), MPI_CHAR, _fus_ranks[fu_ind], MU_FU_ALLOC_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  // info("EM received FU feedback. Status: %d \n", msg.success);
  //
  //      sizeof(mpi_fu_ready_msg),
  //            MPI_CHAR,
  //
  return msg.success;
}

void Manager_unit::get_vector_of_meps(int bu_index, int entries_no)
{
  for (int i = 0; i != entries_no; i++) {
    _bu_meps_sizes_vectors[bu_index].push_back(_ready_mep_array[i]);
  }
}
