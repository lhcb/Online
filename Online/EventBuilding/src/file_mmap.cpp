#include "EventBuilding/file_mmap.hpp"
#include <system_error>

EB::File_mmap::File_mmap() : _file_name(""), _read_ptr(0), _size(0) {}

EB::File_mmap::File_mmap(const std::string& file_name) : _file_name(file_name)
{
  int fd = open(file_name.c_str(), O_RDONLY);
  if (fd == -1) {
    throw std::system_error(errno, std::generic_category(), "File_map unable to open file " + file_name);
  }

  struct stat file_stat;
  if (fstat(fd, &file_stat) == -1) {
    close(fd);
    throw std::system_error(errno, std::generic_category(), "File_map unable to stat file " + file_name);
  }

  _size = file_stat.st_size;

  // workaround for the lambda capture
  size_t local_size = _size;
  _ptr = shared_ptr_t(
    mmap(NULL, _size, PROT_READ, MAP_PRIVATE, fd, 0), [local_size](void* ptr) { munmap(ptr, local_size); });

  close(fd);
  if (_ptr.get() == MAP_FAILED) {
    throw std::system_error(errno, std::generic_category(), "File_map unable mmap file " + file_name);
  }

  reset_read();
}

size_t EB::File_mmap::size() const { return _size; }

size_t EB::File_mmap::remaining_size() const { return _size - (_read_ptr - reinterpret_cast<uintptr_t>(_ptr.get())); }

size_t EB::File_mmap::read_size() const { return (_read_ptr - reinterpret_cast<uintptr_t>(_ptr.get())); }

std::string EB::File_mmap::get_name() const { return _file_name; }

void* EB::File_mmap::get_base() const { return _ptr.get(); }

void* EB::File_mmap::get_read() const { return reinterpret_cast<void*>(_read_ptr); }

void* EB::File_mmap::update_read(size_t size)
{
  void* ret_val = nullptr;
  uintptr_t base_ptr = reinterpret_cast<uintptr_t>(_ptr.get());
  size_t requested_size = (_read_ptr + size) - base_ptr;
  if (requested_size <= _size) {
    _read_ptr += size;
    ret_val = reinterpret_cast<void*>(_read_ptr);
  }

  return ret_val;
}

void* EB::File_mmap::reset_read()
{
  _read_ptr = reinterpret_cast<uintptr_t>(_ptr.get());
  return reinterpret_cast<void*>(_read_ptr);
}