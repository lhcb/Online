#include "EventBuilding/generic_block.hpp"

EB::Generic_block::Generic_block() { _payload_alloc_size = 0; }

EB::Generic_block::Generic_block(std::shared_ptr<char[]> payload, size_t size) { set_payload(payload, size); }

std::shared_ptr<char[]> EB::Generic_block::payload() const { return _payload; }

void EB::Generic_block::set_payload(std::shared_ptr<char[]> payload, size_t size)
{
  if (!payload) {
    throw std::runtime_error("Invalid payload buffer");
  }
  _payload = payload;
  _payload_alloc_size = size;
}

bool EB::Generic_block::realloc_payload(size_t size)
{
  bool ret_val;

  if (_payload.use_count() <= 1) {
    set_payload(std::shared_ptr<char[]>(new char[size]), size);
    ret_val = true;
  } else {
    ret_val = false;
  }

  return ret_val;
}
