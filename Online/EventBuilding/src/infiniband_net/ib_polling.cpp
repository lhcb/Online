/**
 * @file ib_polling.cpp
 * @author Alberto Perro (alberto.perro@cern.ch)
 * @brief InfiniBand Communication Library - CQ polling methods
 * @version 0.1.7
 * @date 24/03/2021
 *
 * @copyright Copyright (c) 2020-2021
 *
 */

#include "ib.hpp"
#include <cstdio>
#include <ctime>
#include <chrono>
#include <cmath>

using namespace std;
#ifndef P_INFO
#define P_INFO "Rank " << this->_procNum << ":   "
#endif

int IB_verbs::IB::_pollRCQ()
{
  int n;
  int wr_ids = PARALLEL_WRS;
  struct ibv_wc wc[PARALLEL_WRS];
  if ((n = ibv_poll_cq(this->_recvcq.get(), wr_ids, wc)) < 0) {
    return -1;
  }
  // add new WCs to vector
  for (int i = 0; i < n; i++) {
    if (_wcrecv.count(wc[i].wr_id) != 0) {
      return -2;
    }
    _wcrecv[wc[i].wr_id] = wc[i];
#ifdef IB_DUMP_FILE
    _dump_wc(wc[i]);
#endif
  }
  return 0;
}

int IB_verbs::IB::_pollSCQ()
{
  int n;
  int wr_ids = PARALLEL_WRS;
  struct ibv_wc wc[PARALLEL_WRS];
  if ((n = ibv_poll_cq(this->_sendcq.get(), wr_ids, wc)) < 0) {
    return -1;
  }
  // add new WCs to vector
  for (int i = 0; i < n; i++) {
    if (_wcsend.count(wc[i].wr_id) != 0) {
      return -2;
    }
    _wcsend[wc[i].wr_id] = wc[i];
#ifdef IB_DUMP_FILE
    _dump_wc(wc[i]);
#endif
  }

  return 0;
}

// TODO this library should return the standard IB error codes
int IB_verbs::IB::ibTestSendCQ(uint32_t wrId)
{
  // check if anything new is present
  int ret_val = 0;
  unordered_map<uint32_t, ibv_wc>::iterator p = _wcsend.find(wrId);
  if (p != _wcsend.end()) {
    if (p->second.status != IBV_WC_SUCCESS) {
      ret_val = -1;
      return ret_val;
    } else {
      _wcsend.erase(p);
      return ret_val;
    }
  } else {
    // poll cq
    ret_val = _pollSCQ();
    if (ret_val != 0) {
      return ret_val;
    }
    p = _wcsend.find(wrId);
    if (p != _wcsend.end()) {
      if (p->second.status != IBV_WC_SUCCESS) {
        ret_val = -1;
        return ret_val;
      } else {
        _wcsend.erase(p);
        return 0;
      }
    }
  }
  return 1;
}

int IB_verbs::IB::ibTestRecvCQ(uint32_t wrId)
{
  // check if anything new is present
  int err_code = 0;
  unordered_map<uint32_t, ibv_wc>::iterator p = _wcrecv.find(wrId);
  if (p != _wcrecv.end()) {
    if (p->second.status != IBV_WC_SUCCESS) {
      return -1;
    } else {
      _wcrecv.erase(p);
      return 0;
    }
  } else {
    err_code = this->_pollRCQ();
    if (err_code != 0) {
      return err_code;
    }
    p = _wcrecv.find(wrId);
    if (p != _wcrecv.end()) {
      if (p->second.status != IBV_WC_SUCCESS) {
        return -1;
      } else {
        _wcrecv.erase(p);
        return 0;
      }
    }
  }
  return 1;
}

int IB_verbs::IB::ibWaitSendCQ(uint32_t wrId)
{
  int run = 1;
  while (run) {
    run = this->ibTestSendCQ(wrId);
    if (_kill_blocking) {
      _kill_blocking = false;
      return -5;
    }
  }
  return run;
}

int IB_verbs::IB::ibWaitRecvCQ(uint32_t wrId)
{
  int run = 1;
  while (run) {
    run = this->ibTestRecvCQ(wrId);
    if (_kill_blocking) {
      _kill_blocking = false;
      return -5;
    }
  }
  return run;
}

int IB_verbs::IB::ibWaitAllSends(std::vector<uint32_t>& wrs)
{
  int x = wrs.size();
  int ret = 0;
  while (x > 0) {
    if (_kill_blocking) {
      _kill_blocking = false;
      return -5;
    }
    for (size_t i = 0; i < wrs.size(); i++) {
      ret = this->ibTestSendCQ(wrs.at(i));
      if (ret == 0) {
        wrs.at(i) = wrs.back();
        wrs.pop_back();
        x--;
      } else if (ret < 0) {
        return -1;
      }
      // //cout << P_INFO << static_cast<double>(delta)<<"\n";
    }
  }
  return 0;
}
int IB_verbs::IB::ibWaitAllRecvs(std::vector<uint32_t>& wrs)
{
  int x = wrs.size();
  int ret = 0;
  while (x > 0) {
    if (_kill_blocking) {
      _kill_blocking = false;
      return -5;
    }
    for (size_t i = 0; i < wrs.size(); i++) {
      ret = this->ibTestRecvCQ(wrs.at(i));
      if (ret == 0) {
        wrs.at(i) = wrs.back();
        wrs.pop_back();
        x--;
      } else if (ret < 0) {
        return -1;
      }
    }
  }
  return 0;
}

std::vector<int> IB_verbs::IB::ibTestAnySends(std::vector<uint32_t>& wrs)
{
  int ret = 0;
  vector<int> res;
  for (size_t i = 0; i < wrs.size(); i++) {
    ret = this->ibTestSendCQ(wrs.at(i));
    res.push_back(ret);
  }
  return res;
}

std::vector<int> IB_verbs::IB::ibTestAnyRecvs(std::vector<uint32_t>& wrs)
{
  int ret = 0;
  vector<int> res;
  for (size_t i = 0; i < wrs.size(); i++) {
    ret = this->ibTestRecvCQ(wrs.at(i));
    res.push_back(ret);
  }
  return res;
}

int IB_verbs::IB::ibTestAllRecvs(std::vector<uint32_t>& wrs)
{
  int ret = 0;
  vector<int> res;
  for (size_t i = 0; i < wrs.size(); i++) {
    ret = this->ibTestRecvCQ(wrs.at(i));
    if (ret == 0) {
      wrs.at(i) = wrs.back();
      wrs.pop_back();
    } else if (ret == -1) {
      return -1;
    }
  }
  if (wrs.size() > 0) {
    return 1;
  }
  return 0;
}

std::vector<int> IB_verbs::IB::ibWaitAnySends(std::vector<uint32_t>& wrs)
{
  int ret = 0;
  bool run = true;
  vector<int> res(wrs.size());
  while (run) {
    if (_kill_blocking) {
      _kill_blocking = false;
      return {};
    }
    for (size_t i = 0; i < wrs.size(); i++) {
      ret = this->ibTestSendCQ(wrs.at(i));
      res.at(i) = ret;
    }
    for (size_t i = 0; i < res.size(); i++) {
      if (res.at(i) == 0) return res;
    }
  }
  return res;
}

std::vector<int> IB_verbs::IB::ibWaitAnyRecvs(std::vector<uint32_t>& wrs)
{
  int ret = 0;
  bool run = true;
  vector<int> res(wrs.size());
  while (run) {
    if (_kill_blocking) {
      _kill_blocking = false;
      return {};
    }
    for (size_t i = 0; i < wrs.size(); i++) {
      ret = this->ibTestRecvCQ(wrs.at(i));
      res.at(i) = ret;
    }
    for (size_t i = 0; i < res.size(); i++) {
      if (res.at(i) == 0) return res;
    }
  }
  return res;
}
