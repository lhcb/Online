/**
 * @file ib_sync.cpp
 * @author Alberto Perro (alberto.perro@cern.ch)
 * @brief InfiniBand Communication Library - Syncronization Methods
 * @version 0.1.7
 * @date 24/03/2021
 *
 * @copyright Copyright (c) 2020-2021
 *
 */
#include "ib.hpp"
#include <cstdio>
#include <ctime>
#include <chrono>
#include <cmath>

using namespace std;

constexpr int SYNC_BASELINE = 10;
#ifndef P_INFO
#define P_INFO "Rank " << this->_procNum << ":   "
#endif

void IB_verbs::IB::_syncFunc(std::stop_token stoken)
{
  while (!stoken.stop_requested()) {
    _pollSyncRCQ();
  }
}

int IB_verbs::IB::_syncInit()
{
  for (size_t i = 0; i < _hosts.size(); i++) {
    if (i != this->_procNum) {
      for (int k = 0; k < SYNC_BASELINE; k++) {
        int ret = ibRecvSync(i);
        if (ret == 0) return -1;
      }
    }
  }
  return 0;
}

uint32_t IB_verbs::IB::ibSendSync(immediate_t imm_code, uint32_t dest)
{
  Proc& host = _hosts.at(dest);
  ibv_send_wr* bad_send_wr;
  ibv_sge list;
  list.addr = (uintptr_t) NULL;
  list.length = 0;
  uint32_t wrId = _ibGetNextWrId(); // get random wr_id
  int nsge = 0;                     // zero-byte msg
  list.lkey = 0;
  ibv_send_wr send_wr;
  memset(&send_wr, 0, sizeof(send_wr));
  send_wr.wr_id = wrId;
  send_wr.sg_list = &list;
  send_wr.num_sge = nsge;
  send_wr.opcode = IBV_WR_RDMA_WRITE_WITH_IMM;
  send_wr.send_flags = IBV_SEND_SIGNALED | IBV_SEND_INLINE;
  send_wr.imm_data = htonl(imm_code);

#ifdef IB_DUMP_FILE
  ib_wr wr;
  wr.src_qp = host.sync_qp->qp_num;
  wr.qp_num = host.remote_qp_num;
  wr.receive = false;
  wr.opcode = send_wr.opcode;
  _dump_wr(wr);
#endif

  int ret = ibv_post_send(host.sync_qp.get(), &send_wr, &bad_send_wr);
  if (ret < 0) return 0;
  return wrId;
}

uint32_t IB_verbs::IB::ibRecvSync(uint32_t src)
{
  Proc& host = _hosts.at(src);
  ibv_recv_wr* bad_recv_wr;
  ibv_sge list;
  list.addr = (uintptr_t) NULL;
  list.length = 0;
  uint32_t wrId = _ibGetNextWrId(); // get random wr_id
  int nsge = 0;                     // zero-byte msg
  list.lkey = 0;
  ibv_recv_wr recv_wr;
  memset(&recv_wr, 0, sizeof(recv_wr));
  recv_wr.wr_id = wrId;
  recv_wr.sg_list = &list;
  recv_wr.num_sge = nsge;
  // //cout << P_INFO << "receiving on " << host.remote_qp_num[idx] << "-" << host.qp[idx]->qp_num << "\r";
#ifdef IB_DUMP_FILE
  ib_wr wr;
  wr.qp_num = host.qp->qp_num;
  wr.src_qp = host.remote_qp_num;
  wr.receive = true;
  _dump_wr(wr);
#endif
  int ret = ibv_post_recv(host.sync_qp.get(), &recv_wr, &bad_recv_wr);
  if (ret < 0) return 0;
  return wrId;
}

int IB_verbs::IB::_pollSyncRCQ()
{
  int n;
  int wr_ids = PARALLEL_WRS;
  struct ibv_wc wc[PARALLEL_WRS];
  if ((n = ibv_poll_cq(this->_syncRcq.get(), wr_ids, wc)) < 0) {
    return -1;
  }
  // add new WCs to vector
  for (int i = 0; i < n; i++) {
#ifdef IB_DUMP_FILE
    _dump_wc(wc[i]);
#endif
    uint32_t qp_n = wc[i].qp_num;
    uint64_t hash = ((uint64_t) qp_n << 32); // compute key
    hash |= ntohl(wc[i].imm_data);
    {
      const std::lock_guard<std::mutex> lock(this->_mtx_sync);
      if (_wcsyncr.count(hash) != 0) {
        return -2;
      }
      _wcsyncr[hash] = wc[i];
    }
    // post new recvs for consumed ones
    // cout << P_INFO << "received from " << _reverse__hosts[qp_n] << "  hash "<<(uintptr_t)hash<< endl;
    ibRecvSync(_reverse_hosts[qp_n]);
  }
  return 0;
}

int IB_verbs::IB::ibTestSyncSend(uint32_t wrId)
{
  int n;
  int wr_ids = PARALLEL_WRS;
  struct ibv_wc wc[PARALLEL_WRS];
  // check if anything new is present
  unordered_map<uint32_t, ibv_wc>::iterator p = _wcsyncs.find(wrId);
  if (p != _wcsyncs.end()) {
    if (p->second.status != IBV_WC_SUCCESS) {
      return -1;
    } else {
      _wcsyncs.erase(p);

      return 0;
    }
  } else {
    // poll cq
    if ((n = ibv_poll_cq(this->_syncScq.get(), wr_ids, wc)) < 0) {
      return -1;
    }
    // add new WCs to vector
    for (int i = 0; i < n; i++) {
      if (_wcsyncs.count(wc[i].wr_id) != 0) {
        return -2;
      }
      _wcsyncs[wc[i].wr_id] = wc[i];
#ifdef IB_DUMP_FILE
      _dump_wc(wc[i]);
#endif
    }
    p = _wcsyncs.find(wrId);
    if (p != _wcsyncs.end()) {
      if (p->second.status != IBV_WC_SUCCESS) {
        return -1;
      } else {
        _wcsyncs.erase(p);
        return 0;
      }
    }
  }
  return 1;
}

int IB_verbs::IB::ibWaitSyncSend(uint32_t wrId)
{
  int run = 1;
  while (run) {
    run = this->ibTestSyncSend(wrId);
    if (_kill_blocking) {
      _kill_blocking = false;
      return -5;
    }
  }
  return run;
}

int IB_verbs::IB::ibWaitAllSyncSends(std::vector<uint32_t>& wrs)
{
  int x = wrs.size();
  int ret = 0;
  while (x > 0) {
    if (_kill_blocking) {
      _kill_blocking = false;
      return -5;
    }
    for (size_t i = 0; i < wrs.size(); i++) {
      ret = this->ibTestSyncSend(wrs.at(i));
      if (ret == 0) {
        wrs.at(i) = wrs.back();
        wrs.pop_back();
        x--;
      } else if (ret < 0) {
        return -1;
      }
      // //cout << P_INFO << static_cast<double>(delta)<<"\n";
    }
  }
  return 0;
}

int IB_verbs::IB::_ibSync(uint64_t key)
{
  const std::lock_guard<std::mutex> lock(this->_mtx_sync);
  unordered_map<uint64_t, ibv_wc>::iterator p = _wcsyncr.find(key);
  if (p != _wcsyncr.end()) {
    if (p->second.status != IBV_WC_SUCCESS) {
      return -1;
    } else {
      _wcsyncr.erase(p);
      return 0;
    }
  }
  return 1;
}

int IB_verbs::IB::ibTestSyncRecv(immediate_t imm_code, uint32_t src)
{
  uint32_t lqp = _hosts.at(src).sync_qp->qp_num; // to do: manage multiple qps
  uint64_t key = ((uint64_t) lqp << 32);
  key |= imm_code;

  return this->_ibSync(key);
}

int IB_verbs::IB::ibWaitSyncRecv(immediate_t imm_code, uint32_t src)
{
  int run = 1;
  uint32_t lqp = _hosts.at(src).sync_qp->qp_num; // to do: manage multiple qps
  uint64_t key = ((uint64_t) lqp << 32);
  key |= imm_code;
  // cout << "searching for "<< (uintptr_t)key<<endl;
  while (run) {
    run = this->_ibSync(key);
    if (_kill_blocking) {
      _kill_blocking = false;
      return -5;
    }
  }
  return run;
}

int IB_verbs::IB::ibWaitAllSyncRecvs(std::vector<std::array<uint32_t, 2>>& wrs)
{
  size_t x = wrs.size();
  int ret = 0;
  std::vector<uint64_t> keys(x);
  keys.reserve(x);
  // compute keys
  for (size_t i = 0; i < x; i++) {
    uint32_t lqp = _hosts.at(wrs.at(i)[1]).sync_qp->qp_num; // to do: manage multiple qps
    uint64_t key = ((uint64_t) lqp << 32);
    key |= wrs.at(i)[0];
    keys.push_back(key);
  }
  while (x > 0) {
    if (_kill_blocking) {
      _kill_blocking = false;
      return -5;
    }
    for (size_t i = 0; i < keys.size(); i++) {
      ret = this->_ibSync(keys.at(i));
      if (ret == 0) {
        keys.at(i) = keys.back();
        keys.pop_back();
        x--;
      } else if (ret < 0) {
        return -1;
      }
    }
  }
  return 0;
}

uint32_t IB_verbs::IB::_writeSync(uint32_t dest)
{
  Proc& host = _hosts.at(dest);
  ibv_send_wr* bad_send_wr;
  size_t offset = _procNum * sizeof(char);
  *(_syncBuf.get() + offset) = 1;
  // cout << P_INFO << "writing to " << offset << endl;
  ibv_sge list = {.addr = (uintptr_t) _syncBuf.get() + offset, .length = sizeof(char), .lkey = _syncMR._mr->lkey};
  ibv_send_wr send_wr;
  memset(&send_wr, 0, sizeof(send_wr));
  uint32_t wrId = _ibGetNextWrId();
  send_wr.wr_id = wrId;
  send_wr.sg_list = &list;
  send_wr.num_sge = 1;
  send_wr.opcode = IBV_WR_RDMA_WRITE;
  send_wr.send_flags = IBV_SEND_SIGNALED | IBV_SEND_INLINE;
  send_wr.wr.rdma.remote_addr = host.sync.addr + offset;
  // cout << P_INFO << (uintptr_t) (host.sync.addr+offset) << endl;
  send_wr.wr.rdma.rkey = host.sync.rkey;
  int ret = ibv_post_send(host.sync_qp.get(), &send_wr, &bad_send_wr);
  if (ret < 0) return 0;
  return wrId;
}

char IB_verbs::IB::_readSync(uint32_t src)
{
  size_t offset = src * sizeof(char);
  // cout << P_INFO << "reading from " << offset << endl;
  char res = 0;
  if (*(_syncBuf.get() + offset) == 1) {
    *(_syncBuf.get() + offset) = 0; // consume sync
    res = 1;
  } else {
    res = 0;
  }
  return res;
}

uint32_t IB_verbs::IB::_ibRecvRDMAsyncMR(uint32_t src)
{
  Proc& host = _hosts.at(src);
  // post RR to get remote MR info
  uint32_t wr = this->ibRecv((char*) &host.sync, sizeof(Proc::MRInfo), src);
  return wr;
}

int IB_verbs::IB::_ibSendRDMAsyncMR(ibv_mr* mr, uint32_t dest)
{
  // post SR to send remote MR info
  if (mr == NULL) return -1;
  Proc::MRInfo sync_MRInfo = {
    .rkey = mr->rkey,
    .addr = (uintptr_t) mr->addr,
  };
  int ret = this->ibBlockSend((char*) &sync_MRInfo, sizeof(Proc::MRInfo), dest);
  return ret;
}