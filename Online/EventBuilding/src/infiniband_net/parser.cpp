/**
 * @file parser.hpp
 * @author Alberto Perro (alberto.perro@cern.ch)
 * @brief Parser for InfiniBand Library
 * @version 0.1.7
 * @date 24/03/2021
 *
 * @copyright Copyright (c) 2020-2021
 *
 */
#include "parser.hpp"
#include "EventBuilding/tools.hpp"
#include <cerrno>
#include <cstring>

using namespace std;

IB_verbs::Parser::Parser(const char* filename)
{
  _getMyNetInfo();
  _readHostFile(filename);
  _getMyIbDevInfo();
}

IB_verbs::Parser::Parser(const char* filename, const int rankid)
{
  _getMyNetInfo();
  _readHostFile(filename, rankid);
  _getMyIbDevInfo();
}

IB_verbs::Parser::~Parser() {}

int IB_verbs::Parser::getProcessId() const { return this->_globalProc; }

int IB_verbs::Parser::getLocalId() const { return this->_localProc; }

int IB_verbs::Parser::getLocalGroup() const { return this->_localGroup; }

int IB_verbs::Parser::getTotalProcesses() const { return this->_totalProcs; }

int IB_verbs::Parser::getIbDevNum() const { return this->_ibDevNum; }

int IB_verbs::Parser::getIbNumaNode() const { return this->_ibNumaNode; }

std::string IB_verbs::Parser::getMyHostname() const { return this->_myHostName; }

int IB_verbs::Parser::_getMyNetInfo()
{
  try {
    this->_myHostName = EB::get_hostname_no_domain();
  } catch (const std::exception& e) {
    return -1;
  }
  // case insensitive hosts
  EB::string_toupper(this->_myHostName);
  return 0;
}

void IB_verbs::Parser::_getMyIbDevInfo()
{
  int num_devs = 0;
  auto dev_list = ibv_get_device_list(&num_devs);
  if (dev_list == NULL) {
    string err_mess("Error while opening IB devices: ");
    err_mess += strerror(errno);
    throw runtime_error(err_mess);
  } else if (num_devs == 0) {
    throw runtime_error("cannot find IB devices");
  }
  this->_ibDevNum = -1;
  for (int i = 0; i < num_devs; i++) {
    string tmp(ibv_get_device_name(dev_list[i]));
    if (this->_ibDevString == tmp) {
      _ibDevNum = i;
      break;
    }
  }
  if (_ibDevNum == -1) {
    throw runtime_error("cannot find the device");
  }
  string path(dev_list[this->_ibDevNum]->ibdev_path);
  string numastr;
  ifstream numafile(path + "/device/numa_node");
  if (numafile.is_open()) {
    numafile >> numastr;
    this->_ibNumaNode = stoi(numastr);
    numafile.close();
  } else {
    this->_ibNumaNode = -1;
  }

  ibv_free_device_list(dev_list);
}

int IB_verbs::Parser::getIpPort() const { return this->_ipPort; }

std::string IB_verbs::Parser::getIbDevString() const { return this->_ibDevString; }

void IB_verbs::Parser::_readHostFile(const char* filename, int rank)
{
  _extractData(filename);
  // the cast to unsigned is fine because we check for negative values
  if ((rank < 0) || (static_cast<unsigned int>(rank) >= _hostvec.size())) {
    std::stringstream err_mess;
    err_mess << __FUNCTION__ << ": requested rank ID " << rank << " out of range.";
    throw runtime_error(err_mess.str());
  }
  _checkSelfData(rank);
}

void IB_verbs::Parser::_readHostFile(const char* filename)
{
  _extractData(filename);
  char* env = getenv("UTGID");
  if (env == NULL) {
    throw runtime_error("Cannot find UTGID env variable.");
  }
  string env_utgid(env);

  auto utgid_map_it = _utgid_rank_map.find(env_utgid);
  if (utgid_map_it == _utgid_rank_map.end()) {
    throw runtime_error("Haven't found this process in the config file, UTGID mismatch.");
  }

  int rank = utgid_map_it->second;
  _checkSelfData(rank);
}

void IB_verbs::Parser::_checkSelfData(int rank)
{
  std::string ib_devs = _hostvec[rank].ibdevs;
  std::string file_host_name = _hostvec[rank].hostname;
  if (EB::string_icompare(file_host_name, _myHostName) != 0) {
    stringstream err_mess;
    err_mess << "Hostname mismatch. File hostname " << file_host_name << " my hostname " << _myHostName;
    throw runtime_error(err_mess.str());
  }

  _ibDevString = ib_devs;
  _globalProc = rank;
  std::string host_ibdev = _myHostName + ib_devs;
  auto host_local_ranks_it = _host_local_ranks_map.find(host_ibdev);
  if (host_local_ranks_it == _host_local_ranks_map.end()) {
    std::stringstream err_mess;
    err_mess << "Unable to find local rank info for " << _myHostName << " " << ib_devs;
    for (const auto& elem : _host_local_ranks_map) {
      err_mess << elem.first << " ";
      for (const auto& vec_elem : elem.second) {
        err_mess << vec_elem << "|";
      }
      err_mess << ",";
    }
    throw runtime_error(err_mess.str());
  }

  auto rank_it = std::find(host_local_ranks_it->second.begin(), host_local_ranks_it->second.end(), _globalProc);

  if (rank_it == host_local_ranks_it->second.end()) {
    std::stringstream err_mess;
    err_mess << "Unable to find rank in local list " << _myHostName << " " << ib_devs << " rank " << _globalProc;
    throw runtime_error(err_mess.str());
  }

  _localProc = std::distance(host_local_ranks_it->second.begin(), rank_it);
  _localGroup = std::distance(_host_local_ranks_map.begin(), host_local_ranks_it);
}

void IB_verbs::Parser::_extractData(const char* filename)
{
  DynamicJsonDocument doc(1e6);
  // TODO add file check
  ifstream hostFile(filename);
  if (!hostFile.is_open()) {
    throw std::system_error(errno, std::system_category());
  }
  auto err_code = deserializeJson(doc, hostFile);
  if (err_code) {
    std::stringstream err_mess;
    err_mess << err_code.c_str();
    throw runtime_error(err_mess.str());
  }
  JsonArray hostsArray = _safe_extract_key<JsonArray>(doc.as<JsonObject>(), "hosts");

  // get hosts size
  int procs = hostsArray.size();
  // resize lut
  _hostvec.resize(procs);
  _utgid_rank_map.clear();
  this->_totalProcs = procs;
  // REMOTES

  for (int i = 0; i < procs; i++) {
    int rankid = _safe_extract_key<int>(hostsArray[i], "rankid");
    string utgid = _safe_extract_key<string>(hostsArray[i], "utgid");
    if ((rankid < 0) || (rankid >= procs)) {
      std::stringstream err_mess;
      err_mess << utgid << ": rank ID " << rankid << " out of range.";
      throw runtime_error(err_mess.str());
    }
    string curr_hostname = _safe_extract_key<string>(hostsArray[i], "hostname");
    // TODO implement a proper case insensitive class
    // hostname are case insensitive
    EB::string_toupper(curr_hostname);
    // look for port
    // we don't use safe extract key because the port field is optional
    int hostn = hostsArray[i]["port"].as<int>();
    // 0 is a not valid valid value and the default if the key is missing
    if (hostn == 0) {
      hostn = 10000 + rankid;
    }

    if (_hostvec[rankid].port != 0) {
      throw runtime_error(utgid + ": duplicate rank id.");
    }
    // this could be replaced by an emplaced value with {} but this code is more readable
    host_config tmp;
    tmp.hostname = curr_hostname;
    tmp.ibdevs = _safe_extract_key<string>(hostsArray[i], "ibdev");
    tmp.port = hostn;
    tmp.utgid = utgid;
    _hostvec[rankid] = tmp;
    _utgid_rank_map[utgid] = rankid;
    // local rank id for sharp
    std::string host_ibdev = curr_hostname + tmp.ibdevs;
    auto host_local_ranks_it = _host_local_ranks_map.find(host_ibdev);
    if (host_local_ranks_it != _host_local_ranks_map.end()) {
      host_local_ranks_it->second.push_back(rankid);
    } else {
      _host_local_ranks_map.emplace(std::make_pair(host_ibdev, std::vector<int>(1, rankid)));
    }
  }
  doc.clear();
  hostFile.close();
}

IB_verbs::Parser::host_vec_t IB_verbs::Parser::getHostList() const { return _hostvec; }

std::vector<int> IB_verbs::Parser::getRanks() const
{
  std::vector<int> rank_vector(_hostvec.size());
  std::iota(rank_vector.begin(), rank_vector.end(), 0);
  return rank_vector;
}
