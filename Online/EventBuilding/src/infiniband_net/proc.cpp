#include "proc.hpp"
#include <stdexcept>
#include <sstream>

IB_verbs::Proc::Proc() : _addrInfo(NULL, &freeaddrinfo)
{
  this->sockFd = 0;
  this->ibDevString = "";
  this->ibDev = 0;
  this->local_lid = 0;
  this->remote_lid = 0;
  this->next_qp = 0;
  this->qp = NULL;
  this->sync_qp = NULL;
}

void IB_verbs::Proc::set_addrInfo(const std::string& node, const std::string& service)
{
  addrinfo* result;
  addrinfo hints;
  memset(&hints, 0, sizeof(struct addrinfo));
  hints.ai_family = AF_INET;     /* Allow IPv4 */
  hints.ai_flags = AI_CANONNAME; /* For wildcard IP address */
  hints.ai_protocol = 0;         /* Any protocol */
  hints.ai_canonname = NULL;
  hints.ai_addr = NULL;
  hints.ai_next = NULL;

  int err = getaddrinfo(node.c_str(), service.c_str(), &hints, &result);
  if (err == 0) {
    _addrInfo.reset(result);
  } else {
    std::stringstream err_mess;
    err_mess << "Unable to get info for: " << node << ":" << service << ": " << gai_strerror(err);
    throw std::runtime_error(err_mess.str());
  }
}

addrinfo* IB_verbs::Proc::get_addrInfo() { return _addrInfo.get(); }