/**
 * @file sock.hpp
 * @author Alberto Perro (alberto.perro@cern.ch)
 * @brief Socket Communication for InfiniBand Library
 * @version 0.1.7
 * @date 24/03/2021
 *
 * @copyright Copyright (c) 2020-2021
 *
 */
#include "sock.hpp"
#include "timer.hpp"
#include <queue>
#include <unistd.h>
#include <fcntl.h>

using namespace std;

IB_verbs::Sock::Sock() {}
IB_verbs::Sock::~Sock() {}

void IB_verbs::Sock::recvQPs(
  std::stop_token stoken,
  const int connection_timeout,
  const int rankid,
  std::vector<Proc>& hosts,
  std::atomic<int>& ret)
{
  ret = 0;
  // vector<Proc>& hosts = const_cast<vector<Proc>&>(hosts);
  int connfd = socket(AF_INET, SOCK_STREAM, 0);

  // We set the connection socket to be non blocking to allow timeout
  fcntl(connfd, F_SETFL, O_NONBLOCK);

  // Preventing the OS from keeping the socket open after close()
  struct linger sl;
  sl.l_onoff = 1;  /* non-zero value enables linger option in kernel */
  sl.l_linger = 0; /* timeout interval in seconds */
  // TODO connfd should be closed on error
  if (setsockopt(connfd, SOL_SOCKET, SO_LINGER, &sl, sizeof(sl)) < 0) {
    close(connfd);
    // TODO find a better err code
    ret = -20;
    return;
  }
  int reuse_addr = 1;
  if (setsockopt(connfd, SOL_SOCKET, SO_REUSEADDR, &reuse_addr, sizeof(reuse_addr)) < 0) {
    close(connfd);
    // TODO find a better err code
    ret = -21;
    return;
  }

  sockaddr_in server;
  memcpy(&server, hosts.at(rankid).get_addrInfo()->ai_addr, sizeof(server));
  server.sin_addr.s_addr = INADDR_ANY;
  if (bind(connfd, (struct sockaddr*) &server, sizeof(server)) < 0) {
    close(connfd);
    ret = -1;
    return;
  }

  int host_size = hosts.size();

  if (listen(connfd, host_size) < 0) {
    close(connfd);
    ret = -2;
    return;
  }
  std::vector<recv_QP_future_t> ret_futures(host_size);
  std::vector<std::jthread> threads_vec(host_size);

  EB::Timer connection_timer;
  connection_timer.start();
  for (int i = 0; i < host_size; i++) {
    int fd = -1;

    if (
      stoken.stop_requested() ||
      ((connection_timeout != 0) && (connection_timer.get_elapsed_time_s() > connection_timeout))) {
      close(connfd);
      ret = -8;
      return;
    }

    while ((fd = accept(connfd, (struct sockaddr*) NULL, NULL)) == -1) {
      // Accepted connection don;t enter in this loop
      // Check errors on the listen socket
      if ((errno != EAGAIN) && (errno != EWOULDBLOCK)) {
        close(connfd);
        ret = -3;
        // pending threads are automatically joined
        return;
      }

      if (
        stoken.stop_requested() ||
        ((connection_timeout != 0) && (connection_timer.get_elapsed_time_s() > connection_timeout))) {
        close(connfd);
        ret = -8;
        return;
      }
      using namespace std::chrono_literals;
      std::this_thread::sleep_for(500ms);
    }
    recv_QP_promise_t ret_promise;
    ret_futures[i] = ret_promise.get_future();
    threads_vec[i] = std::jthread(recvQP, fd, std::move(ret_promise));
  }

  for (int i = 0; i < host_size; i++) {
    // FIXME the server timesout when it should not !!!!
    if (
      stoken.stop_requested() ||
      ((connection_timeout != 0) && (connection_timer.get_elapsed_time_s() > connection_timeout))) {
      close(connfd);
      ret = -8;
      return;
    }
    // TODO maybe we implement ret with a promise
    auto future_val = ret_futures[i].get();
    threads_vec[i].join();
    if (std::get<int>(future_val) == 0) {
      QPInfo& tmp_qp_info = std::get<QPInfo>(future_val);

      int idx = ntohl(tmp_qp_info.procNum);
      hosts.at(idx).remote_lid = ntohs(tmp_qp_info.lid);
      hosts.at(idx).remote_sync_qp_num = ntohl(tmp_qp_info.sync_qp_num);
      hosts.at(idx).remote_qp_num = ntohl(tmp_qp_info.qp_num);
    } else {
      // TODO we should return here otherwise we may overwrite error conditions
      ret = std::get<int>(future_val);
      return;
    }
  }

  if (close(connfd) != 0) {
    ret = -7;
    return;
  }
}

void IB_verbs::Sock::recvQP(std::stop_token stoken, int fd, recv_QP_promise_t ret_promise)
{
  int ret_val;
  if (fd < 0) {
    ret_promise.set_value({-3, {}});
    return;
  }

  QPInfo tmp_qp_info;
  ssize_t rcv = recv(fd, (char*) &tmp_qp_info, sizeof(QPInfo), MSG_WAITALL);
  if (rcv == sizeof(QPInfo)) {
    ret_val = 0;
  } else {
    ret_val = -4;
  }

  if (shutdown(fd, SHUT_RDWR) != 0) {
    ret_val = -5;
  }

  if (close(fd) != 0) {
    ret_val = -6;
  }

  ret_promise.set_value({ret_val, tmp_qp_info});
}

int IB_verbs::Sock::sendQP(const int rankid, Proc& rem)
{ // send local qp infos
  QPInfo tmp_qp_info;
  tmp_qp_info.lid = htons(rem.local_lid);
  tmp_qp_info.qp_num = htonl(rem.qp->qp_num);
  tmp_qp_info.sync_qp_num = htonl(rem.sync_qp->qp_num);
  tmp_qp_info.procNum = htonl(rankid);
  int connfd = socket(AF_INET, SOCK_STREAM, 0);
  if (connfd < 0) {
    return -1;
  }

  auto addrinfo = rem.get_addrInfo();
  if (connect(connfd, (sockaddr*) addrinfo->ai_addr, sizeof(*(addrinfo->ai_addr))) < 0) {
    return -1;
  }
  send(connfd, (char*) &tmp_qp_info, sizeof(QPInfo), 0);
  close(connfd);
  return 0;
}
