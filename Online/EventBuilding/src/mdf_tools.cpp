#include "EventBuilding/mdf_tools.hpp"
EB::MDF_block::MDF_block() : Generic_block() {}
EB::MDF_block::MDF_block(int fd) : Generic_block()
{

  int ret_val = read_block(fd);
  if (ret_val < 0) {
    throw std::system_error(-ret_val, std::generic_category(), "MDF_block");
  }
}

EB::MDF_block::MDF_block(std::shared_ptr<char[]> payload, size_t size) : Generic_block(payload, size) {}

EB::MDF_block::MDF_block(int fd, std::shared_ptr<char[]> payload, size_t size) : Generic_block(payload, size)
{
  int ret_val = read_block(fd);
  if (ret_val < 0) {
    throw std::system_error(-ret_val, std::generic_category(), "MDF_block");
  }
}

void EB::MDF_header::print() const
{
  std::cout << "is valid " << generic.is_valid() << std::endl;

  std::cout << "size " << generic.size[0] << std::endl;
  std::cout << "compression " << (generic.compression_information & 0xF) << std::endl;
  std::cout << "run " << specific.run << std::endl;
  std::cout << "bx id " << specific.bxid << std::endl;
  std::cout << "data type " << (uint32_t) generic.data_type << std::endl;
}

// TODO this should accomodate for header type variability
size_t EB::MDF_block::payload_size() const { return header.generic.size[0] - sizeof(header); }

int EB::MDF_block::read_header(int fd)
{
  int ret_val = 0;
  int n_bytes;

  n_bytes = read(fd, reinterpret_cast<char*>(&header), sizeof(header));

  if (n_bytes != sizeof(header)) {
    std::cerr << "end of file reached no more events" << std::endl;
    ret_val = EOF;
  } else if (!header.generic.is_valid()) {
    ret_val = -EINVAL;
  }

  return ret_val;
}

int EB::MDF_block::read_block(int fd)
{
  int ret_val = 0;
  int n_bytes;

  ret_val = read_header(fd);

  if (ret_val == 0) {
    size_t payload_size = this->payload_size();

    // allocating a new buffer and handling memory operations
    bool realloc_ok = true;
    if (_payload_alloc_size < payload_size) {
      realloc_ok = realloc_payload(payload_size);
    }

    if (realloc_ok) {
      n_bytes = read(fd, _payload.get(), payload_size);
      if (n_bytes == -1 || static_cast<size_t>(n_bytes) != payload_size) {
        std::cerr << "ERROR truncated file" << std::endl;
        ret_val = -EINVAL;
      }
    } else {
      ret_val = -ENOMEM;
    }
  }
  return ret_val;
}

int EB::MDF_block::read_block(int fd, std::shared_ptr<char[]> buffer, size_t buffer_size)
{
  set_payload(buffer, buffer_size);

  return read_block(fd);
}

int EB::MDF_block::write_block(int fd) const
{
  write(fd, &header, sizeof(header));
  write(fd, _payload.get(), payload_size());

  return 0;
}
