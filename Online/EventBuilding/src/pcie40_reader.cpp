#include "pcie40_reader.hpp"
#include "EventBuilding/tools.hpp"
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <limits>
#include <cstdlib>
#include <system_error>

EB::PCIe40_reader::PCIe40_reader() { init_internal_status(); }

EB::PCIe40_reader::PCIe40_reader(int id, int stream, bool enable_MFP, int packing_factor) : PCIe40_reader()
{
  std::error_code err_val = open(id, stream);
  if (err_val) {
    throw std::system_error(err_val, std::to_string(id));
  }

  if (enable_MFP) {
    err_val = configure_MFP(packing_factor);
    if (err_val) {
      throw std::system_error(err_val, std::to_string(id));
    }
  } else {
    err_val = configure_fragment();
    if (err_val) {
      throw std::system_error(err_val, std::to_string(id));
    }
  }
}

EB::PCIe40_reader::PCIe40_reader(const std::string& name, int stream, bool enable_MFP, int packing_factor) :
  PCIe40_reader()
{
  std::error_code err_val = open(name, stream);
  if (err_val) {
    throw std::system_error(err_val, name);
  }

  if (enable_MFP) {
    err_val = configure_MFP(packing_factor);
    if (err_val) {
      throw std::system_error(err_val, name);
    }
  } else {
    err_val = configure_fragment();
    if (err_val) {
      throw std::system_error(err_val, name);
    }
  }
}

EB::PCIe40_reader::~PCIe40_reader() { close(); }

void EB::PCIe40_reader::init_internal_status()
{
  _id = -1;
  _stream = P40_DAQ_STREAM::P40_DAQ_STREAM_NULL;
  _stream_fd = -1;
  _id_fd = -1;
  _ctrl_fd = -1;
  _meta_fd = -1;
  _dmabuf_fd = -1;
  _next_ev_id = default_next_ev_id;
  _buffer = NULL;
  _buffer_size = 0;
  _requested_size = 0;
  _available_data = 0;
  _name = "undefined_device";
}

EB::PCIe40_reader::PCIe40_reader(PCIe40_reader&& other) :
  _buffer(other._buffer), _buffer_size(other._buffer_size), _internal_read_off(other._internal_read_off),
  _device_read_off(other._device_read_off), _available_data(other._available_data),
  _requested_size(other._requested_size), _id(other._id), _stream(other._stream), _next_ev_id(other._next_ev_id),
  _id_fd(other._id_fd), _stream_fd(other._stream_fd), _ctrl_fd(other._ctrl_fd), _meta_fd(other._meta_fd),
  _dmabuf_fd(other._dmabuf_fd), _name(std::move(other._name)), PCIe40_alignment(other.PCIe40_alignment)
{
  // Reset internal status of the moved object
  other.init_internal_status();
}

EB::PCIe40_reader& EB::PCIe40_reader::operator=(PCIe40_reader&& other)
{
  // protection against self move
  if (this != &other) {
    // Close currently open stream
    close();

    _buffer = other._buffer;
    _buffer_size = other._buffer_size;
    _internal_read_off = other._internal_read_off;
    _device_read_off = other._device_read_off;
    _available_data = other._available_data;
    _requested_size = other._requested_size;
    _next_ev_id = other._next_ev_id;
    _stream = other._stream;
    _id_fd = other._id_fd;
    _stream_fd = other._stream_fd;
    _ctrl_fd = other._ctrl_fd;
    _meta_fd = other._meta_fd;
    _dmabuf_fd = other._dmabuf_fd;
    _name = std::move(other._name);
    PCIe40_alignment = other.PCIe40_alignment;

    // Reset internal status of the moved object
    other.init_internal_status();
  }

  return *this;
}

std::error_code EB::PCIe40_reader::open(const std::string& name, int stream)
{
  std::error_code err_code{};
  int id = p40_id_find(name.c_str());
  if (id >= 0) {
    err_code = open(id, stream);
  } else {

    err_code = PCIe40_errors::ID_FIND;
  }

  return err_code;
}

std::error_code EB::PCIe40_reader::open(int id, int stream)
{
  std::error_code err_code{};
  _id = id;
  _stream = static_cast<P40_DAQ_STREAM>(stream);

  if (is_open()) {
    close();
  }

  _id_fd = p40_id_open(_id);

  if (_id_fd < 0) {
    err_code = PCIe40_errors::ID_OPEN;
    close();
    return err_code;
  }

  _meta_fd = p40_stream_open(_id, P40_DAQ_STREAM_META);
  if (_meta_fd < 0) {
    err_code = PCIe40_errors::META_STREAM_OPEN;
    close();
    return err_code;
  }

  _stream_fd = p40_stream_open(_id, _stream);
  if (_stream_fd < 0) {
    err_code = PCIe40_errors::STREAM_OPEN;
    close();
    return err_code;
  }

  int enabled = p40_stream_enabled(_stream_fd);
  if (enabled < 0) {
    err_code = PCIe40_errors::STREAM_GET_ENABLED;
    close();
    return err_code;
  } else if (enabled == 0) {
    err_code = PCIe40_errors::STREAM_ENABLED;
    close();
    return err_code;
  }

  if (p40_stream_lock(_stream_fd) < 0) {
    err_code = PCIe40_errors::STREAM_LOCK;
    close();
    return err_code;
  }

  ssize_t size = p40_stream_get_host_buf_bytes(_stream_fd);
  if (size < 0) {
    err_code = PCIe40_errors::STREAM_SIZE;
    close();
    return err_code;
  }

  _buffer_size = size;
  _buffer = p40_stream_map(_stream_fd);
  if (_buffer == NULL) {
    err_code = PCIe40_errors::STREAM_MAP;
    close();
    return err_code;
  }

  _ctrl_fd = p40_ctrl_open(_id);
  if (_ctrl_fd < 0) {
    err_code = PCIe40_errors::CTRL_STREAM_OPEN;
    close();
    return err_code;
  }

// if the driver doesn't support DMABUF we use the default value of -1
#ifdef P40_DMABUF_SUPPORT
  _dmabuf_fd = p40_stream_get_dmabuf(_stream_fd);
  if (_dmabuf_fd < 0) {
    err_code = PCIe40_errors::DMABUF_OPEN;
    close();
    return err_code;
  }
#endif

  err_code = update_device_ptr();
  if (err_code) {
    close();
    return err_code;
  }

  _internal_read_off = _device_read_off;
  _requested_size = 0;

  char name[PCIe40_unique_name_str_length];

  if (p40_id_get_name_unique(_id_fd, name, sizeof(name)) != 0) {
    err_code = PCIe40_errors::GET_NAME;
    close();
    return err_code;
  }

  _name = name;

  int vers_id = p40_id_get_source(_id_fd);

  _src_id = (vers_id & (src_num_mask | src_subsystem_mask)) >> src_num_shift;
  _block_version = (vers_id & src_version_mask) >> src_version_shift;

  return err_code;
}

std::error_code EB::PCIe40_reader::set_packing_factor(int packing_factor)
{
  std::error_code err_code{};
  if (!is_open()) {
    err_code = PCIe40_errors::DEVICE_NOT_OPEN;
    return err_code;
  }

  int ret_code = p40_stream_set_meta_packing(_stream_fd, packing_factor);
  if (ret_code != 0) {
    err_code = PCIe40_errors::SET_META_PACKING;
    return err_code;
  }

  return err_code;
}

std::error_code EB::PCIe40_reader::set_truncation_threshold(int trunc_thr)
{
  std::error_code err_code{};
  if (!is_open()) {
    err_code = PCIe40_errors::DEVICE_NOT_OPEN;
    return err_code;
  }

  int ret_code = p40_ctrl_set_trunc_thres(_ctrl_fd, _stream, trunc_thr);
  if (ret_code != 0) {
    // TODO add error code

    err_code = PCIe40_errors::INVALID_TRUNCATION_THRESHOLD;
    return err_code;
  }

  return err_code;
}

std::error_code EB::PCIe40_reader::enable_MFP_stream()
{
  std::error_code err_code{};
  if (!is_open()) {
    err_code = PCIe40_errors::DEVICE_NOT_OPEN;
    return err_code;
  }

  int meta_mask = p40_stream_id_to_meta_mask(_id, _stream);
  int ret_code = p40_stream_enable_mask(_meta_fd, meta_mask);
  if (ret_code != 0) {
    err_code = PCIe40_errors::META_STREAM_ENABLE;
    return err_code;
  }

  return err_code;
}

std::error_code EB::PCIe40_reader::disable_MFP_stream()
{
  std::error_code err_code{};
  if (!is_open()) {
    err_code = PCIe40_errors::DEVICE_NOT_OPEN;
    return err_code;
  }

  int meta_mask = p40_stream_id_to_meta_mask(_id, _stream);
  int ret_code = p40_stream_disable_mask(_meta_fd, meta_mask);
  if (ret_code != 0) {
    err_code = PCIe40_errors::META_STREAM_DISABLE;
    return err_code;
  }

  return err_code;
}

std::error_code EB::PCIe40_reader::configure_MFP(int packing_factor)
{
  std::error_code err_code{};
  if (!is_open()) {
    err_code = PCIe40_errors::DEVICE_NOT_OPEN;
    return err_code;
  }

  err_code = enable_MFP_stream();
  if (err_code) {
    return err_code;
  }

  err_code = set_packing_factor(packing_factor);
  if (err_code) {
    return err_code;
  }

  err_code = set_truncation_threshold(p40_ctrl_get_spill_buf_size(_ctrl_fd, _stream));
  if (err_code) {
    return err_code;
  }

  return err_code;
}

std::error_code EB::PCIe40_reader::configure_fragment(int trunc_thr)
{
  std::error_code err_code{};
  if (!is_open()) {
    err_code = PCIe40_errors::DEVICE_NOT_OPEN;
    return err_code;
  }

  err_code = disable_MFP_stream();
  if (err_code) {
    return err_code;
  }

  err_code = set_truncation_threshold(trunc_thr);
  if (err_code) {
    return err_code;
  }

  return err_code;
}

std::error_code EB::PCIe40_reader::reset()
{
  std::error_code err_code{};
  if (!is_open()) {
    err_code = PCIe40_errors::DEVICE_NOT_OPEN;
    return err_code;
  }

  // ret_val = p40_ctrl_reset_logic(_ctrl_fd);
  int ret_code = p40_stream_reset_logic(_stream_fd);
  if (ret_code != 0) {
    err_code = PCIe40_errors::LOGIC_RESET;
    return err_code;
  }

  err_code = update_device_ptr();
  if (err_code) {
    return err_code;
  }

  _internal_read_off = _device_read_off;
  _requested_size = 0;
  update_usage();

  _next_ev_id = default_next_ev_id;

  return err_code;
}

std::string EB::PCIe40_reader::get_name() const { return _name; }

int EB::PCIe40_reader::get_dmabuf_fd() const { return _dmabuf_fd; }

void EB::PCIe40_reader::close()
{
  // Make sure that this function does not change errno, so it's easier to use in cleanups
  int saved_errno = errno;

  if (_stream_fd >= 0) {
    p40_stream_unlock(_stream_fd);
    // No need to call p40_stream_unmap: p40_stream_close does it for us.
    p40_stream_close(_stream_fd, _buffer);
  }
  if (_id_fd >= 0) {
    p40_id_close(_id_fd);
  }
  if (_ctrl_fd >= 0) {
    p40_ctrl_close(_ctrl_fd);
  }

  if (_meta_fd >= 0) {
    p40_stream_close(_meta_fd, NULL);
  }

  if (_dmabuf_fd >= 0) {
    ::close(_dmabuf_fd);
  }

  init_internal_status();

  errno = saved_errno;
}

bool EB::PCIe40_reader::is_open()
{
  return (_id_fd >= 0) && (_stream_fd >= 0) && (_meta_fd >= 0) && (_ctrl_fd >= 0) && (_buffer != NULL);
}

std::error_code EB::PCIe40_reader::update_device_ptr()
{
  std::error_code err_code;
  if (!is_open()) {
    err_code = PCIe40_errors::DEVICE_NOT_OPEN;
    return err_code;
  }

  auto device_read_off = p40_stream_get_host_buf_read_off(_stream_fd);

  if (device_read_off < 0) {
    err_code = PCIe40_errors::HOST_READ_OFF;
    return err_code;
  }

  _device_read_off = device_read_off;

  return err_code;
}

std::vector<std::tuple<void*, size_t>> EB::PCIe40_MFP_reader::get_full_buffer()
{
  std::vector<std::tuple<void*, size_t>> ret_val;
  if (is_open()) {
    // double the size because of the double mapping
    ret_val.emplace_back(std::make_pair(_buffer, 2 * _buffer_size));
  }

  return ret_val;
}

// TODO check if something special is needed for fragments
template<>
pcie40_frg_hdr* EB::PCIe40_reader::extract_element()
{
  pcie40_frg_hdr* ret_val = NULL;
  if (!is_open()) {
    throw std::system_error(PCIe40_errors::DEVICE_NOT_OPEN, get_name());
  }
  size_t size;
  if (_available_data >= sizeof(pcie40_frg_hdr)) {
    ret_val = reinterpret_cast<pcie40_frg_hdr*>(reinterpret_cast<uintptr_t>(_buffer) + _internal_read_off);
    if (ret_val->ghdr.is_invalid()) {
      throw std::system_error(PCIe40_errors::CORRUPTED_DATA, get_name());
    }

    size = ret_val->bytes();
    size += ret_val->padding();

    int err_code = update_read_off(size);
    if (err_code != 0) {
      // if the read offset can't be updated the data is not valid
      ret_val = NULL;
    } else {
      // check EV id validity
      ev_id_type ev_id = ret_val->evid();
      // after a flush we should restart from 0
      if (ev_id == MFP_end_run) {
        _next_ev_id = 0;
      } else if (ev_id != _next_ev_id) {
        std::stringstream err_mess;
        err_mess << "expected EV ID " << _next_ev_id << " stream EV ID " << ev_id;
        throw std::system_error(PCIe40_errors::CORRUPTED_EV_ID, err_mess.str());
      } else {
        _next_ev_id++;
      }
    }
  }

  return ret_val;
}

template<>
EB::MFP* EB::PCIe40_reader::extract_element()
{
  EB::MFP* ret_val = NULL;
  if (!is_open()) {
    throw std::system_error(PCIe40_errors::DEVICE_NOT_OPEN, get_name());
  }
  size_t size;
  if (_available_data >= sizeof(EB::MFP)) {
    ret_val = reinterpret_cast<EB::MFP*>(reinterpret_cast<uintptr_t>(_buffer) + _internal_read_off);
    // debug variable for the prints
    size_t orig_size;
    uint16_t orig_magic;
    if (!ret_val->is_valid(&orig_magic, &orig_size)) {
      // sync issue between the FPGA and the CPU the size sometimes is not updated properly
      if ((orig_size == 0) && ret_val->is_header_valid()) {
        return NULL;
      }
      std::stringstream err_mess;
      err_mess << "magic " << std::hex << "0x" << ret_val->header.magic << " offset 0x"
               << reinterpret_cast<uintptr_t>(ret_val) - reinterpret_cast<uintptr_t>(_buffer) << std::dec << " size "
               << ret_val->bytes() << " min size " << ret_val->header.header_size() << " original magic " << std::hex
               << "0x" << orig_magic << std::dec << " original size " << orig_size;
      throw std::system_error(PCIe40_errors::CORRUPTED_DATA, err_mess.str());
    }

    size = ret_val->bytes();
    // TODO get padding from device
    size += get_padding(size, 1 << PCIe40_alignment);

    int err_code = update_read_off(size);
    if (err_code != 0) {
      // if the read offset can't be updated the data is not valid
      ret_val = NULL;
    } else {
      // check EV id validity
      ev_id_type ev_id = ret_val->header.ev_id;
      // after a flush we should restart from 0
      if (ret_val->is_end_run()) {
        _next_ev_id = 0;
      } else if (ev_id != _next_ev_id) {
        std::stringstream err_mess;
        err_mess << "expected EV ID " << _next_ev_id << " stream EV ID " << ev_id;
        throw std::system_error(PCIe40_errors::CORRUPTED_EV_ID, err_mess.str());
      } else {
        _next_ev_id += ret_val->header.n_banks;
      }
    }
  }

  return ret_val;
}

int EB::PCIe40_reader::update_read_off(size_t size)
{
  int ret_val = 0;
  if (_available_data < size) {
    // The amount of data requested is more than the amount of data available in the buffer we can't update the read
    // offset
    ret_val = 1;
  } else {
    _internal_read_off += size;
    _requested_size += size;
    _available_data -= size;
    // wrap around (the memory region is mapped two times contiguosly)
    if (_internal_read_off > _buffer_size) {
      _internal_read_off -= _buffer_size;
    }
  }

  return ret_val;
}

void EB::PCIe40_reader::update_usage()
{
  auto device_available_data = p40_stream_get_host_buf_bytes_used(_stream_fd);
  if (device_available_data < 0) {
    throw std::system_error(PCIe40_errors::HOST_BYTES_USED, get_name());
  }
  _available_data = device_available_data - _requested_size;
}

size_t EB::PCIe40_reader::get_buffer_size() const { return _buffer_size; }

size_t EB::PCIe40_reader::get_buffer_occupancy()
{
  update_usage();
  return _available_data + _requested_size;
}

void EB::PCIe40_reader::ack_read()
{
  if (!is_open()) {
    throw std::system_error(PCIe40_errors::DEVICE_NOT_OPEN, get_name());
  }

  if (p40_stream_free_host_buf_bytes(_stream_fd, _requested_size) < 0) {
    throw std::system_error(PCIe40_errors::HOST_FREE_BUFF, get_name());
  }

  std::error_code err_code = update_device_ptr();

  if (err_code) {
    throw std::system_error(err_code, get_name());
  }

  _internal_read_off = _device_read_off;
  _requested_size = 0;
  update_usage();
}

void EB::PCIe40_reader::cancel_pending()
{
  // All the calls in this method are local and do not query the device
  _internal_read_off = _device_read_off;
  _available_data += _requested_size;
  _requested_size = 0;
}

std::string EB::PCIe40_MFP_reader::get_name() const { return EB::PCIe40_reader::get_name(); }

int EB::PCIe40_MFP_reader::get_dmabuf_fd() const { return EB::PCIe40_reader::get_dmabuf_fd(); }

EB::MFP* EB::PCIe40_MFP_reader::try_get_element()
{
  update_usage();
  return extract_element<EB::MFP>();
}

void EB::PCIe40_MFP_reader::read_complete() { ack_read(); }

void EB::PCIe40_MFP_reader::flush()
{
  std::error_code err_code;
  err_code = reset();
  if (err_code) {
    throw std::system_error(err_code, get_name());
  }
}

int EB::PCIe40_MFP_reader::get_src_id() const { return _src_id; }

size_t EB::PCIe40_MFP_reader::get_buffer_size() const { return PCIe40_reader::get_buffer_size(); }
size_t EB::PCIe40_MFP_reader::get_buffer_occupancy() { return PCIe40_reader::get_buffer_occupancy(); }

EB::PCIe40_MFP_reader::PCIe40_MFP_reader() : PCIe40_reader() {}

EB::PCIe40_MFP_reader::PCIe40_MFP_reader(int id, int stream, int packing_factor) :
  PCIe40_reader(id, stream, true, packing_factor)
{}

EB::PCIe40_MFP_reader::PCIe40_MFP_reader(const std::string& name, int stream, int packing_factor) :
  PCIe40_reader(name, stream, true, packing_factor)
{}

EB::PCIe40_frag_reader::PCIe40_frag_reader() : _pcie40_reader(), _packing_factor(1) {}

// TODO rename n_frag -> packing factor
EB::PCIe40_frag_reader::PCIe40_frag_reader(
  int id,
  size_t buffer_size,
  int n_frags,
  size_t buffer_alignment,
  int stream) :
  _pcie40_reader(id, stream, false)
{
  set_n_frags(n_frags);
  Shared_ptr_buffer_backend buffer_reader_backend(buffer_size, buffer_alignment);
  // TODO maybe there is a better way to copy this
  Shared_ptr_buffer_backend buffer_writer_backend(buffer_reader_backend);
  _internal_buffer_reader.reset_backend(std::move(buffer_reader_backend));
  _internal_buffer_writer.reset_backend(std::move(buffer_writer_backend));
}

EB::PCIe40_frag_reader::PCIe40_frag_reader(
  const std::string& name,
  size_t buffer_size,
  int n_frags,
  size_t buffer_alignment,
  int stream) :
  _pcie40_reader(name, stream, false)
{
  set_n_frags(n_frags);
  Shared_ptr_buffer_backend buffer_reader_backend(buffer_size, buffer_alignment);
  // TODO maybe there is a better way to copy this
  Shared_ptr_buffer_backend buffer_writer_backend(buffer_reader_backend);
  _internal_buffer_reader.reset_backend(std::move(buffer_reader_backend));
  _internal_buffer_writer.reset_backend(std::move(buffer_writer_backend));
}

void EB::PCIe40_frag_reader::set_n_frags(int n_frags)
{
  if ((n_frags > 0) && (n_frags <= std::numeric_limits<decltype(_packing_factor)>::max())) {
    _packing_factor = n_frags;
    _frag_list.reserve(_packing_factor);
    _type_list.reserve(_packing_factor);
    _size_list.reserve(_packing_factor);
  } else {
    throw std::system_error(PCIe40_errors::INVALID_PACKING_FACTOR, get_name());
  }
}

int EB::PCIe40_frag_reader::get_n_frags() const { return _packing_factor; }

EB::MFP* EB::PCIe40_frag_reader::try_get_element()
{
  if (!_pcie40_reader.is_open()) {
    throw std::system_error(PCIe40_errors::DEVICE_NOT_OPEN, get_name());
  }

  _pcie40_reader.update_usage();

  // all the fragments from the buffer are read and the MFPs are generated
  // TODO check if the FPGA is no overwhelming us
  if (!_flush_pending) {
    scan_frag_list();
  }

  std::error_code mfp_err = build_MFP();
  if (mfp_err) {
    if ((mfp_err == PCIe40_errors::INTERNAL_BUFFER_FULL) || (mfp_err == PCIe40_errors::INTERNAL_BUFFER_FULL_FLUSH)) {
      return NULL;
    } else {
      throw std::system_error(mfp_err, get_name());
    }
  }

  // _pcie40_reader.ack_read();
  // _internal_buffer_writer.write_complete();

  return _internal_buffer_reader.try_get_element();
}

void EB::PCIe40_frag_reader::scan_frag_list()
{
  if (!_pcie40_reader.is_open()) {
    throw std::system_error(PCIe40_errors::DEVICE_NOT_OPEN, get_name());
  }

  while (_frag_list.size() < _packing_factor) {
    pcie40_frg_hdr* next_frag = _pcie40_reader.extract_element<pcie40_frg_hdr>();
    // no more data into the device buffer
    if (next_frag == NULL) {
      _flush = false;
      break;
    } else if (next_frag->flushed()) {
      // The flush fragment is discarted
      // The first flush ends this loop and triggers the generation of an end of run MFP, all the subsequent
      // consecutive flushes are simply ignored
      if (!_flush) {
        _flush = true;
        break;
      } else {
        continue;
      }
    } else {
      // Valid data deassert the flush flag
      _flush = false;
    }

    // we strip off the fragment header
    size_t header_offset = sizeof(next_frag->le_evid) + sizeof(next_frag->ghdr);
    if (next_frag->bytes() < header_offset) {
      // Corrupted fragment we don't strip the header
      // TODO check if this should throw an exception
      header_offset = 0;
    }
    _frag_list.emplace_back(reinterpret_cast<void*>(reinterpret_cast<uintptr_t>(next_frag) + header_offset));
    _size_list.emplace_back(next_frag->bytes() - header_offset);
    _type_list.emplace_back(next_frag->ghdr.type());
    _total_size += _size_list.back() + get_padding(_size_list.back(), 1 << _align);
    // we store only the first ev_id
    if (_frag_list.size() == 1) {
      _ev_id = next_frag->evid();
      _version = next_frag->ghdr.version();
    }
  }
}

std::error_code EB::PCIe40_frag_reader::build_MFP()
{
  std::error_code err_val{};

  // TODO check how the flushed events should be handled
  if ((_frag_list.size() == _packing_factor) || _flush) {
    // If the buffer was full when writing flush the frag list is empty and no MFP should be generated
    if (_frag_list.size() > 0) {
      // TODO remove the padding for the last element
      size_t MFP_size =
        // EB::MFP_header_size(_frag_list.size(), 1 << _align) + _total_size - get_padding(_size_list.back(), 1 <<
        // _align);
        EB::MFP_header_size(_frag_list.size()) + _total_size;
      EB::MFP* new_MFP = _internal_buffer_writer.try_write_next_element(MFP_size);
      if (new_MFP != NULL) {
        new_MFP->set_header_valid();
        new_MFP->header.n_banks = _frag_list.size();
        new_MFP->header.packet_size = MFP_size;
        new_MFP->header.ev_id = _ev_id;
        new_MFP->header.src_id = _pcie40_reader._src_id;
        new_MFP->header.align = _align;
        new_MFP->header.block_version = _version;

        std::memcpy(new_MFP->header.bank_types(), _type_list.data(), _type_list.size() * sizeof(_type_list.front()));
        std::memcpy(new_MFP->header.bank_sizes(), _size_list.data(), _size_list.size() * sizeof(_size_list.front()));
        // copy the list of fragments
        EB::MFP::iterator MFP_it = new_MFP->begin();
        for (size_t index = 0; index < _frag_list.size(); index++) {
          std::memcpy(reinterpret_cast<void*>(&(*MFP_it)), _frag_list[index], _size_list[index]);
          MFP_it++;
        }

        // the last fragment has no padding
        // std::memcpy(reinterpret_cast<void*>(&(*MFP_it)), _frag_list.back(), _size_list.back());

        _internal_buffer_writer.write_complete();
        _pcie40_reader.ack_read();
        _frag_list.clear();
        _type_list.clear();
        _size_list.clear();
        _total_size = 0;
      } else {
        err_val = PCIe40_errors::INTERNAL_BUFFER_FULL;
        return err_val;
      }
    }

    if (_flush) {
      size_t MFP_size = EB::MFP_header_size(0);
      EB::MFP* new_MFP = _internal_buffer_writer.try_write_next_element(MFP_size);
      if (new_MFP != NULL) {
        _flush_pending = false;
        new_MFP->set_end_run();
        new_MFP->header.packet_size = MFP_size;
        _internal_buffer_writer.write_complete();
      } else {
        _flush_pending = true;
        err_val = PCIe40_errors::INTERNAL_BUFFER_FULL_FLUSH;
        return err_val;
      }
    }
  }

  return err_val;
}

void EB::PCIe40_frag_reader::read_complete() { _internal_buffer_reader.read_complete(); }

void EB::PCIe40_frag_reader::flush()
{
  // TODO check retrun value of reset
  std::error_code err_code;
  err_code = _pcie40_reader.reset();
  if (err_code) {
    throw std::system_error(err_code, get_name());
  }

  _internal_buffer_reader.flush();
}

int EB::PCIe40_frag_reader::get_src_id() const { return _pcie40_reader._src_id; }

size_t EB::PCIe40_frag_reader::get_buffer_size() const { return _pcie40_reader.get_buffer_size(); }
size_t EB::PCIe40_frag_reader::get_buffer_occupancy() { return _pcie40_reader.get_buffer_occupancy(); }

std::vector<std::tuple<void*, size_t>> EB::PCIe40_frag_reader::get_full_buffer()
{
  return _internal_buffer_reader.get_full_buffer();
}

std::string EB::PCIe40_frag_reader::get_name() const { return _pcie40_reader.get_name(); }