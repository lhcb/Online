#include "pcie40_reader_error.hpp"

// Error category definition
namespace {
  struct PCIe40_error_category : std::error_category {
    const char* name() const noexcept override;
    std::string message(int ev) const override;
  };

  const char* PCIe40_error_category::name() const noexcept { return "PCIe40_reader"; }
  std::string PCIe40_error_category::message(int ev) const
  {
    switch (static_cast<EB::PCIe40_errors>(ev)) {
    case EB::PCIe40_errors::ID_OPEN: return "Unable to open the device";
    case EB::PCIe40_errors::ID_FIND: return "Unable to find device name";
    case EB::PCIe40_errors::STREAM_OPEN: return "Unable to open the main stream";
    case EB::PCIe40_errors::META_STREAM_OPEN: return "Unable to open the meta stream";
    case EB::PCIe40_errors::META_STREAM_ENABLE: return "Unable to enable the meta stream";
    case EB::PCIe40_errors::META_STREAM_DISABLE: return "Unable to disable the meta stream";
    case EB::PCIe40_errors::CTRL_STREAM_OPEN: return "Unable to open the control stream";
    case EB::PCIe40_errors::STREAM_GET_ENABLED: return "Failed to get main stream status";
    case EB::PCIe40_errors::STREAM_ENABLED: return "Main stream disabled";
    case EB::PCIe40_errors::STREAM_LOCK: return "Unable to lock the main stream";
    case EB::PCIe40_errors::STREAM_SIZE: return "Unable to get the stream size";
    case EB::PCIe40_errors::STREAM_MAP: return "Unable to map the stream";
    case EB::PCIe40_errors::DMABUF_OPEN: return "Unable to open the DMABuf file descriptor";
    case EB::PCIe40_errors::HOST_READ_OFF: return "Unable to read the host read offset";
    case EB::PCIe40_errors::HOST_FREE_BUFF: return "Unable to free buffer space";
    case EB::PCIe40_errors::HOST_BYTES_USED: return "Unable to get the buffer occupancy";
    case EB::PCIe40_errors::GET_NAME: return "Unable to get device name";
    case EB::PCIe40_errors::DEVICE_NOT_OPEN: return "Requesting access on a not open device";
    case EB::PCIe40_errors::SET_META_PACKING: return "Unable to set the packing factor";
    case EB::PCIe40_errors::LOGIC_RESET: return "Unable to perform a logic reset";
    case EB::PCIe40_errors::CORRUPTED_DATA: return "Corrupted data in the stream";
    case EB::PCIe40_errors::CORRUPTED_EV_ID: return "Non contiguous ev ids in the stream";
    case EB::PCIe40_errors::INVALID_PACKING_FACTOR: return "Invalid packing factor";
    case EB::PCIe40_errors::INVALID_TRUNCATION_THRESHOLD: return "Invalid truncation threshold";
    case EB::PCIe40_errors::INTERNAL_BUFFER_FULL: return "Internal buffer full on data write";
    case EB::PCIe40_errors::INTERNAL_BUFFER_FULL_FLUSH: return "Internal buffer full on flush";
    default: return "Undefined error";
    }
  }

  const PCIe40_error_category the_PCIe40_error_category{};

} // namespace

std::error_code EB::make_error_code(EB::PCIe40_errors e) { return {static_cast<int>(e), the_PCIe40_error_category}; }