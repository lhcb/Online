#include "shared_mem_buffer_backend.hpp"
#include <cstdlib>
#include <sstream>
#include <stdexcept>
#include "EventBuilding/tools.hpp"
#include <unistd.h>
#include <fcntl.h> /* For O_* constants */
#include <sys/mman.h>
#include <cstring>
#include <sstream>
#include <string>
#include <memory>
#include <iostream>
#include <tuple>
#include <cerrno>
#include <cstring>

EB::Shared_mem_buffer_backend::Shared_mem_buffer_backend() :
  _buffer(0), _remote_buffer_status(nullptr), _local_buffer_status(), _shmem_name(""), _shmem_fd(-1), _is_writer(false),
  _allow_unlink(false), _reader_lock_name(""), _reader_lock_fd(-1), _writer_lock_name(""), _writer_lock_fd(-1)
{}

EB::Shared_mem_buffer_backend::Shared_mem_buffer_backend(Shared_mem_buffer_backend&& other) :
  _buffer(other._buffer), _remote_buffer_status(other._remote_buffer_status),
  _local_buffer_status(std::move(other._local_buffer_status)), _shmem_name(std::move(other._shmem_name)),
  _shmem_fd(other._shmem_fd), _is_writer(other._is_writer), _allow_unlink(other._allow_unlink),
  _shm_area(std::move(other._shm_area)), _reader_lock_name(std::move(other._reader_lock_name)),
  _reader_lock_fd(other._reader_lock_fd), _writer_lock_name(std::move(other._writer_lock_name)),
  _writer_lock_fd(other._writer_lock_fd)
{
  // put the class into a safe state
  other._buffer = 0;
  other._shmem_name = "";
  other._reader_lock_name = "";
  other._writer_lock_name = "";
  other._reader_lock_fd = -1;
  other._writer_lock_fd = -1;
  other._remote_buffer_status = nullptr;
}

EB::Shared_mem_buffer_backend::Shared_mem_buffer_backend(
  const std::string& name,
  bool is_writer,
  bool allow_reconnect,
  bool allow_unlink,
  size_t size,
  size_t alignment,
  int id,
  int numa_node) :
  _shmem_name(name),
  _is_writer(is_writer), _allow_unlink(allow_unlink)
{
  if (!is_writer) {
    try {
      open_shmem();
      try_lock_reader();
      shm_map();
    } catch (...) {
      clean_up();
      throw;
    }
  } else {
    if (allow_reconnect) {
      try {
        open_shmem();
        try_lock_writer();
        shm_map();
      } catch (...) {
        clean_up();
        // clean up resets the _shmem_name
        _shmem_name.assign(name);
      }
    }
    try {
      create_locks();
      try_lock_writer();
      try_lock_reader();
      create_shmem();
      init_buffer(size, alignment, id);
      release_reader();
      shm_map(numa_node);
    } catch (...) {
      clean_up();
      throw;
    }
  }
}

EB::Shared_mem_buffer_backend& EB::Shared_mem_buffer_backend::operator=(Shared_mem_buffer_backend&& other)
{
  this->clean_up();
  _buffer = other._buffer;
  _remote_buffer_status = other._remote_buffer_status;
  _local_buffer_status = std::move(other._local_buffer_status);
  _shmem_name = std::move(other._shmem_name);
  _shmem_fd = other._shmem_fd;
  _is_writer = other._is_writer;
  _allow_unlink = other._allow_unlink;
  _shm_area = std::move(other._shm_area);
  _reader_lock_name = std::move(other._reader_lock_name);
  _reader_lock_fd = other._reader_lock_fd;
  _writer_lock_name = std::move(other._writer_lock_name);
  _writer_lock_fd = other._writer_lock_fd;

  // put the class into a safe state
  other._buffer = 0;
  other._shmem_name = "";
  other._reader_lock_name = "";
  other._writer_lock_name = "";
  other._reader_lock_fd = -1;
  other._writer_lock_fd = -1;
  other._remote_buffer_status = nullptr;

  return *this;
}

EB::Shared_mem_buffer_backend::~Shared_mem_buffer_backend() { clean_up(); }

std::tuple<void*, size_t> EB::Shared_mem_buffer_backend::get_buffer()
{
  return std::make_tuple(reinterpret_cast<void*>(_buffer), _local_buffer_status.size);
}

bool EB::Shared_mem_buffer_backend::is_set() { return (_buffer != 0) && (_remote_buffer_status != nullptr); }
std::string EB::Shared_mem_buffer_backend::get_name() const { return _shmem_name; }

void EB::Shared_mem_buffer_backend::create_locks()
{
  _reader_lock_name = "/tmp/";
  _reader_lock_name += _shmem_name;
  _reader_lock_name += "_reader.lock";

  _writer_lock_name = "/tmp/";
  _writer_lock_name += _shmem_name;
  _writer_lock_name += "_writer.lock";

  auto old_mask = umask(~permission_mask);
  _reader_lock_fd = open(_reader_lock_name.c_str(), O_CREAT | O_RDWR, permission_mask);
  umask(old_mask);

  if (_reader_lock_fd < 0) {
    std::ostringstream err_mess;
    err_mess << __FILE__ << ":" << __LINE__ << " read lock open on shmem" << _shmem_name << ": " << strerror(errno);
    throw std::runtime_error(err_mess.str());
  }

  old_mask = umask(~permission_mask);
  _writer_lock_fd = open(_writer_lock_name.c_str(), O_CREAT | O_RDWR, permission_mask);
  umask(old_mask);

  if (_writer_lock_fd < 0) {
    std::ostringstream err_mess;
    err_mess << __FILE__ << ":" << __LINE__ << " write lock open on shmem" << _shmem_name << ": " << strerror(errno);
    throw std::runtime_error(err_mess.str());
  }
}

void EB::Shared_mem_buffer_backend::init_buffer(size_t size, size_t alignment, int id)
{
  // the max alignment size will be one page, because the memory mapped address is always page aligned it is not
  // possible to align to something bigger than a page, unless some offset is used
  // TODO investigate HUGE_PAGES

  long page_size = sysconf(_SC_PAGE_SIZE);

  if (1 << alignment > page_size) {
    std::ostringstream err_mess;
    err_mess << __FILE__ << ":" << __LINE__ << " ERROR alignment > page size on shmem " << _shmem_name
             << " alignment: " << (1 << alignment) << " page size: " << page_size;
    throw std::invalid_argument(err_mess.str());
  }

  _local_buffer_status.read_stat = 0;
  _local_buffer_status.write_stat = 0;
  _local_buffer_status.set_read_ptr(0);
  _local_buffer_status.set_write_ptr(0);

  _local_buffer_status.id = id;

  _local_buffer_status.size = size;
  _local_buffer_status.alignment = alignment;

  // mmapped memory is always page aligned

  size_t buffer_start = sizeof(_local_buffer_status) + get_padding(sizeof(_local_buffer_status), 1 << alignment);

  if (
    write(_shmem_fd, static_cast<const void*>(&(_local_buffer_status)), sizeof(_local_buffer_status)) !=
    sizeof(_local_buffer_status)) {
    std::ostringstream err_mess;
    err_mess << __FILE__ << ":" << __LINE__ << " ERROR write to shmem failed " << _shmem_name;
    throw std::runtime_error(err_mess.str());
  }

  if (lseek(_shmem_fd, 0, SEEK_SET) == -1) {
    std::ostringstream err_mess;
    err_mess << __FILE__ << ":" << __LINE__ << " lseek: " << strerror(errno);
    throw std::runtime_error(err_mess.str());
  }

  if (ftruncate(_shmem_fd, buffer_start + size) == -1) {
    std::ostringstream err_mess;
    err_mess << __FILE__ << ":" << __LINE__ << " ftruncate: " << strerror(errno);
    throw std::runtime_error(err_mess.str());
  }

  asm volatile("mfence" ::: "memory");
}

void EB::Shared_mem_buffer_backend::create_shmem()
{
  // int ret_val = 0;

  auto old_mask = umask(~permission_mask);
  int fd = shm_open(_shmem_name.c_str(), O_CREAT | O_RDWR, permission_mask);
  umask(old_mask);

  if (fd < 0) {
    std::ostringstream err_mess;
    err_mess << __FILE__ << ":" << __LINE__ << " create_shmem: " << strerror(errno);
    throw std::runtime_error(err_mess.str());
  }

  _shmem_fd = fd;
}

void EB::Shared_mem_buffer_backend::open_shmem()
{
  int fd = shm_open(_shmem_name.c_str(), O_RDWR, 0);
  if (fd < 0) {
    std::ostringstream err_mess;
    err_mess << __FILE__ << ":" << __LINE__ << " shm_open " << _shmem_name << " : " << strerror(errno);
    throw std::runtime_error(err_mess.str());
  }
  _shmem_fd = fd;

  _reader_lock_name = "/tmp/";
  _reader_lock_name += _shmem_name;
  _reader_lock_name += "_reader.lock";

  _writer_lock_name = "/tmp/";
  _writer_lock_name += _shmem_name;
  _writer_lock_name += "_writer.lock";

  _reader_lock_fd = open(_reader_lock_name.c_str(), O_RDWR);

  if (_reader_lock_fd < 0) {
    std::ostringstream err_mess;
    err_mess << __FILE__ << ":" << __LINE__ << " read lock open on shmem" << _shmem_name << ": " << strerror(errno);
    throw std::runtime_error(err_mess.str());
  }

  _writer_lock_fd = open(_writer_lock_name.c_str(), O_RDWR);

  if (_writer_lock_fd < 0) {
    std::ostringstream err_mess;
    err_mess << __FILE__ << ":" << __LINE__ << " write lock open on shmem" << _shmem_name << ": " << strerror(errno);
    throw std::runtime_error(err_mess.str());
  }
}

void EB::Shared_mem_buffer_backend::shm_map(int numa_node)
{
  struct stat shmem_stat;

  if (fstat(_shmem_fd, &shmem_stat) == -1) {
    std::ostringstream err_mess;
    err_mess << __FILE__ << ":" << __LINE__ << " fstat: " << strerror(errno) << std::endl;
    throw std::runtime_error(err_mess.str());
  }

  auto st_size = safe_to_unsigned(shmem_stat.st_size);

  if (st_size < sizeof(Circular_buffer_status)) {
    std::ostringstream err_mess;
    err_mess << __FILE__ << ":" << __LINE__ << " Shmem region " << _shmem_name
             << " too small to contain valid metadata";
    throw std::runtime_error(err_mess.str());
  }

  size_t read_size = read(_shmem_fd, &(_local_buffer_status), sizeof(Circular_buffer_status));
  lseek(_shmem_fd, 0, SEEK_SET);

  if (read_size != sizeof(Circular_buffer_status)) {
    std::ostringstream err_mess;
    err_mess << __FILE__ << ":" << __LINE__ << " ERROR: unable to read buffer status from shmem " << _shmem_name
             << " read " << read_size << "/" << sizeof(Circular_buffer_status) << " B" << std::endl;
    throw std::runtime_error(err_mess.str());
  }

  uintptr_t buffer_offset =
    sizeof(Circular_buffer_status) + get_padding(sizeof(Circular_buffer_status), 1 << _local_buffer_status.alignment);
  size_t shmem_size = buffer_offset + _local_buffer_status.size;

  if (st_size != shmem_size) {
    std::ostringstream err_mess;
    err_mess << __FILE__ << ":" << __LINE__ << " ERROR shmem data corrupted expected shmem file size " << shmem_size
             << " actual size " << st_size << std::endl;
    throw std::runtime_error(err_mess.str());
  }

  _shm_area =
    shmem_ptr_type(mmap(NULL, shmem_size, PROT_READ | PROT_WRITE, MAP_SHARED, _shmem_fd, 0), [shmem_size](void* ptr) {
      munmap(ptr, shmem_size);
    });

  if (_shm_area.get() == MAP_FAILED) {
    std::ostringstream err_mess;
    err_mess << __FILE__ << ":" << __LINE__ << " mmap: " << strerror(errno) << std::endl;
    throw std::runtime_error(err_mess.str());
  }

  if (numa_node != -1) {
    numa_tonode_memory(_shm_area.get(), shmem_size, numa_node);
  }

  _remote_buffer_status = reinterpret_cast<Circular_buffer_status*>(_shm_area.get());

  _buffer = reinterpret_cast<uintptr_t>(_shm_area.get()) + buffer_offset;

#ifdef DEBUG
  std::cout << "mapping shmem " << _shmem_name << " on fd " << _shmem_fd << std::endl;
  std::cout << "map ptr " << map_ptr << std::endl;
  std::cout << "buffer " << (void*) (_buffer) << std::endl;
  std::cout << "size " << (void*) (_local_buffer_status.size) << std::endl;
  std::cout << "alignment " << (1 << _local_buffer_status.alignment) << std::endl;
  std::cout << "read status " << (void*) (_local_buffer_status.read_stat) << std::endl;
  std::cout << "write status " << (void*) (_local_buffer_status.write_stat) << std::endl;
#endif // DEBUG
}

bool EB::Shared_mem_buffer_backend::is_open()
{
  return (_shmem_fd >= 0) && (_reader_lock_fd >= 0) && (_writer_lock_fd >= 0);
}

void EB::Shared_mem_buffer_backend::try_lock_reader()
{
  if (lockf(_reader_lock_fd, F_TLOCK, 0) != 0) {
    std::ostringstream err_mess;
    err_mess << "EB::Shared_mem_buffer_backend: Unable to lock read " << _shmem_name << " " << strerror(errno);
    throw std::runtime_error(err_mess.str());
  }
}

void EB::Shared_mem_buffer_backend::release_reader()
{
  if (lockf(_reader_lock_fd, F_ULOCK, 0) != 0) {
    std::ostringstream err_mess;
    err_mess << "EB::Shared_mem_buffer_backend: Unable to release read " << _shmem_name << " " << strerror(errno);
    throw std::runtime_error(err_mess.str());
  }
}

void EB::Shared_mem_buffer_backend::try_lock_writer()
{
  if (lockf(_writer_lock_fd, F_TLOCK, 0) != 0) {
    std::ostringstream err_mess;
    err_mess << "EB::Shared_mem_buffer_backend: Unable to lock write " << _shmem_name << " " << strerror(errno);
    throw std::runtime_error(err_mess.str());
  }
}

void EB::Shared_mem_buffer_backend::release_writer()
{
  if (lockf(_writer_lock_fd, F_ULOCK, 0) != 0) {
    std::ostringstream err_mess;
    err_mess << "EB::Shared_mem_buffer_backend: Unable to release write " << _shmem_name << " " << strerror(errno);
    throw std::runtime_error(err_mess.str());
  }
}

void EB::Shared_mem_buffer_backend::close_fd(int& fd)
{
  if (fd >= 0) {
    close(fd);
  }

  fd = -1;
}

void EB::Shared_mem_buffer_backend::clean_up()
{
  // no exceptions
  try {
    auto save_errno = errno;
    release_reader();
    release_writer();

    close_fd(_reader_lock_fd);
    close_fd(_writer_lock_fd);
    close_fd(_shmem_fd);

    if (_allow_unlink) {
      unlink(_reader_lock_name.c_str());
      unlink(_writer_lock_name.c_str());
      shm_unlink(_shmem_name.c_str());
    }

    _reader_lock_name.assign("");
    _writer_lock_name.assign("");
    _shmem_name.assign("");

    _buffer = 0;
    _remote_buffer_status = NULL;

    errno = save_errno;
  } catch (...) {
  }
}