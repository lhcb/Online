#include "shared_ptr_buffer_backend.hpp"
#include <cstdlib>
#include <sstream>

EB::Shared_ptr_buffer_backend::Shared_ptr_buffer_backend(
  size_t size,
  size_t alignment,
  int id,
  int numa_node,
  std::string name) :
  _remote_buffer_status(new Circular_buffer_status(size, alignment, id)),
  _local_buffer_status(size, alignment, id), _buffer_ptr(aligned_alloc(1 << alignment, size), [](auto p) { free(p); }),
  _name(name)
{
  if (_buffer_ptr.get() == NULL) {
    throw std::bad_alloc{};
  }

  if (numa_node != -1) {
    numa_tonode_memory(_buffer_ptr.get(), size, numa_node);
  }

  _buffer = reinterpret_cast<uintptr_t>(_buffer_ptr.get());
}

EB::Shared_ptr_buffer_backend::~Shared_ptr_buffer_backend() {}

std::tuple<void*, size_t> EB::Shared_ptr_buffer_backend::get_buffer()
{
  return std::make_tuple(_buffer_ptr.get(), _local_buffer_status.size);
}

bool EB::Shared_ptr_buffer_backend::is_set()
{
  return (_remote_buffer_status.get() != NULL) && (_remote_buffer_status.use_count() != 0) &&
         (_buffer_ptr.get() != NULL) && (_buffer_ptr.use_count() != 0);
}

std::string EB::Shared_ptr_buffer_backend::get_name() const { return _name; }