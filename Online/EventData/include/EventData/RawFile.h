//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  RawFile.h
//--------------------------------------------------------------------------
//
//  Author     : Markus Frank
//==========================================================================
#ifndef TELL1DATA_RAWFILE_H
#define TELL1DATA_RAWFILE_H

// Framework include files
#include <RTL/Datafile.h>
#include <CPP/mem_buff.h>

/// C/C++ include files
#include <map>
#include <string>
#include <vector>

/// Forward declarations

namespace Online  {

  /// Forward declarations
  class raw_bank_offline_t;
  
  /// Wrapper class to manager data files
  /**
   *  \author  M.Frank
   *  \version 1.0
   */
  class RawFile : public Datafile  {
  protected:
    struct memory_mapping_t   {
      /// Mapping address (if file is memory mapped)
      unsigned char* begin = nullptr;
      /// End of mapped memory region (if file is memory mapped)
      unsigned char* end   = nullptr;
      /// Current memory pointer (if file is memory mapped)
      unsigned char* ptr   = nullptr;
    };
    memory_mapping_t m_mem_map;    

  public:
    
    enum EventType {
      NO_INPUT_TYPE   = 0,
      AUTO_INPUT_TYPE = 1,
      MDF_INPUT_TYPE  = 2,
      MEP_INPUT_TYPE  = 3
    };

    /// Input buffer allocater signature
    struct Allocator {
      /// Default destructor
      virtual ~Allocator() = default;
      /// Allocation callback
      virtual unsigned char* operator()(std::size_t memsize) = 0;
    };

    /// In-place allocator using pre-defined buffer
    struct InPlaceAllocator : public Allocator {
    public:
      unsigned char* ptr = 0;
      std::size_t len = 0;
    public:
      /// Initializing constructor
      InPlaceAllocator(unsigned char* p, std::size_t l);
      /// Inhibit default assignment
      InPlaceAllocator() = delete;
      /// Inhibit move constructor
      InPlaceAllocator( InPlaceAllocator&& copy ) = delete;
      /// Inhibit copy constructor
      InPlaceAllocator( const InPlaceAllocator& copy ) = delete;
      /// Default destructor
      virtual ~InPlaceAllocator() = default;
      /// Inhibit copy assignment
      InPlaceAllocator& operator=( const InPlaceAllocator& copy ) = delete;
      /// Allocation callback
      virtual unsigned char* operator()(std::size_t length)   override  final {
	return (unsigned char*)((length<=len) ? ptr : 0);
      }
    };

    class HeapAllocator : public Allocator  {
    public:
      mem_buff    m_buffer {     };

    public:
      /// Default constructor
      HeapAllocator() = default;
      /// Inhibit move constructor
      HeapAllocator( HeapAllocator&& copy ) = delete;
      /// Inhibit copy constructor
      HeapAllocator( const HeapAllocator& copy ) = delete;
      /// Default destructor
      virtual ~HeapAllocator() = default;
      /// Inhibit copy assignment
      HeapAllocator& operator=( const HeapAllocator& copy ) = delete;
      /// Allocation callback
      virtual unsigned char* operator()(size_t length)   override  final  {
	return this->m_buffer.allocate(length, false);
      }
      virtual const unsigned char* data()  const final  {
	return this->m_buffer.begin();
      }
      virtual unsigned char* data()  final  {
	return this->m_buffer.begin();
      }
    };

  public:
    /// Constructor(s)
    using Datafile::Datafile;
    /// Assignment
    using Datafile::operator=;

    /// Read single event using dynamic allocation
    long read_event(EventType expected, EventType& found_type,
		    Allocator& allocator, unsigned char** data);

    /// Read single event into fixed buffer
    long read_event(EventType expected, EventType& found_type,
		    unsigned char* data, std::size_t len);

    /// Read multiple events into fixed buffer (burst mode)
    std::pair<long,long>
      read_multiple_events(std::size_t num_events,
			   EventType expected, EventType& found_type,
			   unsigned char* p, std::size_t len);

    /// Search bad file and try to seek the next event record. Returns offset to next event
    long scanToNextMDF();
    /// Save remainder of currently read file
    void saveRestOfFile();
    /// Scan single record and check for consistency
    int checkRecord(std::size_t offset, const void* data, std::size_t length)   const;
    /// Check if the event header is correct
    int checkEventHeader(std::size_t offset, const void* data, std::size_t length)   const;
    /// Check content of bank
    int checkBank(std::size_t offset, const void* data)   const;
    /// Dump a set of raw banks
    void dump(const unsigned char* data, std::size_t len)  const;
    /// Dump the content of a single raw bank to screen
    void dump (const raw_bank_offline_t* bank)  const;
  };

  inline RawFile::InPlaceAllocator::InPlaceAllocator(unsigned char* p, std::size_t l)
    : ptr(p), len(l)
  {
  }

}
#endif /* TELL1DATA_RAWFILE_H  */
