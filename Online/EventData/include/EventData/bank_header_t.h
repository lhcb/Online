//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  bank_header_t.h
//--------------------------------------------------------------------------
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_TELL1_BANK_HEADER_H
#define ONLINE_DATAFLOW_TELL1_BANK_HEADER_H 1

/// C/C++ include files
#include <cstdint>

/// Framework includes
#include <RTL/Pack.h>

/// Online namespace declaration
namespace Online    {

  /// Tell1 data output from the LHCb online system
  /** @class bank_header_t
   *
   * Raw data bank haeader as sent by the TELL1 boards of the LHCb RUN1/2 DAQ.
   *
   * For a detailed description of the raw bank format,
   * see <a href="https://edms.cern.ch/document/565851/5">EDMS-565851/5</a>
   *
   * Note concerning the changes done 06/03/2006:
   * - The bank size is in BYTES
   * - The full size of a bank in memory is long word (32 bit) aligned
   *   ie. a bank of 17 Bytes length actually uses 20 Bytes in memory.
   * - The bank length accessors size() and setSize() do not contain
   *   the bank header, ie. size() = (total size) - (header size).
   * - The length passed to the RawEvent::createBank should NOT
   *   contain the size of the header !
   * - If the full padded bank size is required use the utility
   *   function bank_header_t::totalSize = size + header size + padding size.
   *
   * @author Helder Lopes
   * @author Markus Frank
   * 
   */
  class PACKED_DATA bank_header_t   {
  protected:
    /// Magic word (by definition 0xCBCB)
    unsigned short m_magic   { 0 };
    /// Bank length in bytes (must be >= 0)
    unsigned short m_length  { 0 };
    /// Bank type (must be >= 0)
    unsigned char  m_type    { 0 };
    /// Version identifier (must be >= 0)
    unsigned char  m_version { 0 };
    /// Source ID (valid source IDs are > 0; invalid ones 0)
    short          m_sourceID { 0 };

  public:
    /// Default constructor
    bank_header_t() = default;
    /// Move constructor
    bank_header_t(bank_header_t&& copy) = default;
    /// Copy constructor
    bank_header_t(const bank_header_t& copy) = default;
    /// Move assignment
    bank_header_t& operator=(bank_header_t&& copy) = default;
    /// Copy assignment
    bank_header_t& operator=(const bank_header_t& copy) = default;
    /// Default destructor (NON virtual!)
    ~bank_header_t() = default;

    /// Access to magic word for integrity check
    int magic()  const               {  return m_magic;                            }

    /// Header size
    static int hdrSize()             {  return sizeof(bank_header_t);              }

    /// Return size of the data body part of the bank
    int size() const                 {  return m_length-hdrSize();                 }

    /// Set data size of the bank in bytes
    void setSize(std::size_t val)    {  m_length = (val&0xFFFF)+hdrSize();         }

    /// Access the full (padded) size of the bank
    static int total_bank_size(int length_in_bytes)  { 
      typedef unsigned int T;
      length_in_bytes = (length_in_bytes&0xFFFF)+hdrSize();
      return length_in_bytes%sizeof(T)==0 ? length_in_bytes : (length_in_bytes/sizeof(T)+1)*sizeof(T);
    }
    /// Access the full (padded) size of the bank
    int totalSize() const            {
      typedef unsigned int T;
      return m_length%sizeof(T)==0 ? m_length : (m_length/sizeof(T)+1)*sizeof(T);
    }
    /// Return bankType of this bank 
    int type() const                 {  return int(m_type);                        }

    /// Set the bank type
    void setType(unsigned char val)  {  m_type = val&0xFF;                         }

    /// Return version of this bank 
    int version() const              {  return m_version;                          }

    /// Set the version information of this bank
    void setVersion(int val)         {  m_version = (unsigned char)(val&0xFF);     }

    /// Return SourceID of this bank  (TELL1 board ID)
    int sourceID() const             {  return m_sourceID;                         }

    /// Set the source ID of this bank (TELL1 board ID)
    void setSourceID(int val)        {  m_sourceID = (unsigned short)(val&0xFFFF); }

    /// Return detector ID of this bank  (top 5 bits of the source ID)
    int detectorID() const           {  return (m_sourceID&0xFFFF) >> 11;          }

    /// Return local source ID of this bank  (lower 11 bits of the source ID)
    int channelID() const            {  return ((m_sourceID<<5)&0xFFFF) >> 5;         }
  };
}      // End namespace Online
#include <RTL/Unpack.h>

#include <vector>
#include <string>
#include <EventData/bank_types_t.h>

/// Online namespace declaration
namespace Online    {

  class raw_bank_online_t;
  class raw_bank_offline_t;

  /// Helper class to print bank headers
  namespace event_print {
    template <bank_types_t::BankType i>
    std::vector<std::string> bank_data(const void* bank, std::size_t len, std::size_t words_per_line);

    /// Retrieve dump of bank data in line format
    std::vector<std::string> bankData( const void* data, std::size_t len, std::size_t words_per_line);
    /// Retrieve dump of bank data in line format
    std::vector<std::string> bankData( const bank_header_t* header, const void* bank_data, std::size_t words_per_line);
    /// Retrieve dump of bank data in line format
    std::vector<std::string> bankData( const raw_bank_online_t* bank, std::size_t words_per_line);
    /// Retrieve dump of bank data in line format
    std::vector<std::string> bankData( const raw_bank_offline_t* bank, std::size_t words_per_line);
    /// Retrieve dump of bank data in line format
    std::vector<std::string> taeData( const bank_header_t* tae, const void* bank);
    /// Retrieve dump of bank data in line format
    std::vector<std::string> taeData( const raw_bank_offline_t* bank);

    /// Retrieve description of the raw bank hader
    std::string bankHeader( const bank_header_t* r );
    /// Retrieve bank type name from the raw bank
    std::string bankType( const bank_header_t* r );
    /// Retrieve bank type name from the raw bank type
    std::string bankType( int i );
    /// Retrieve detector name from the source ID / detector ID
    std::string detectorName(unsigned int detector_id);
    /// Retrieve detector name from the raw bank
    std::string detectorName(const bank_header_t* r);
  }

  namespace bank_header_manip  {
    /// Set the detector ID
    void setDetectorID(bank_header_t* header, unsigned int detector_id);
  }
}      // End namespace Online

/// C/C++ include files
#include <iostream>
#include <iomanip>

/// Output streamer
inline std::ostream& operator <<(std::ostream& os, const Online::bank_header_t& b)  {
  char text[256];
  ::snprintf(text,sizeof(text),
	     "bank_header: magic: %0x Length: %d Type: %d Version: %d SourceID: %0x",
	     int(b.magic()), int(b.totalSize()), int(b.type()), int(b.version()), int(b.sourceID()));
  os << text;
  return os;
}
#endif ///ONLINE_DATAFLOW_TELL1_BANK_HEADER_H
