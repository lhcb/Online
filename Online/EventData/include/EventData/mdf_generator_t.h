//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_EVENTDATA_MDF_GENERATOR_T
#define ONLINE_EVENTDATA_MDF_GENERATOR_T

// Framework include files
#include <CPP/mem_buff.h>
#include <EventData/event_header_t.h>
#include <EventData/run3_detector_bits.h>
#include <EventData/raw_bank_offline_t.h>

/// C/C++ include files
#include <cstdint>

/// Online namespace declaration
namespace Online  {

  /// Helper to generate MDF of TSE frames in memory
  /**
   *  \author  M.Frank
   *  \version 1.0
   */
  class mdf_generator_t    {
  public:
    struct bank_descr_t   {
      /// Number of banks to be generated
      std::size_t  count   { 1 };
      /// Minimal length if variable bank size
      std::size_t  length { 1 };
      /// Maximal length if variable bank size
      std::size_t  increment { 1 };
      /// Set integer pattern to banks
      int          set_pattern  {    true };
      /// Bank type
      bank_types_t::BankType   type      { bank_types_t::VP };
      /// Detector bits
      run3::run3_detector_bits detector  { run3::RO_BIT_ODIN };
    };

    /// Property: Bank description for generation
    std::vector<bank_descr_t> bank_types;
    /// Property: Packing factor
    std::size_t packing_factor       {       1 };
    /// Property: TAE: half window size. Must be 0 for MDF
    int32_t     half_window          {       0 };
    /// Property: Buffer pre-allocation size
    long        buffer_size          { 1000000 };
    /// Property: Automatically add odin bank
    int         have_odin            {    true };
    
  public:
    /// Default constructor
    mdf_generator_t() = default;
    /// Move constructor
    mdf_generator_t(mdf_generator_t&& copy) = default;
    /// Copy constructor
    mdf_generator_t(const mdf_generator_t& copy) = default;
    /// Default destructor
    ~mdf_generator_t() = default;
    /// Move assignment
    mdf_generator_t& operator=(mdf_generator_t&& copy) = default;
    /// Copy assignment
    mdf_generator_t& operator=(const mdf_generator_t& copy) = default;
    /// Generatro MDF or TAE frame according to setup parameters
    mem_buff generate(int32_t run_id, int32_t& event_id)  const;
  };
}      // End namespace Online
#endif // ONLINE_EVENTDATA_MDF_GENERATOR_T
