//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  odin_t.h
//--------------------------------------------------------------------------
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_EVENTDATA_ODIN_T_H
#define ONLINE_EVENTDATA_ODIN_T_H 1

// Framework include files. Note: pack.h must come last!
#include <PCIE40Data/sodin.h>
#include <RTL/Pack.h>

/// Online namespace declaration
namespace Online    {

  namespace OdinData  {
    /// Type of trigger broadcasted by ODIN
    enum TriggerType  {
      PhysicsTrigger     = 0,
      BeamGasTrigger     = 1,
      LumiTrigger        = 2,
      TechnicalTrigger   = 3,
      AuxiliaryTrigger   = 4,
      NonZSupTrigger     = 5,
      TimingTrigger      = 6,
      CalibrationTrigger = 7
    };
  }

  /** @class OnlineRunInfo OnlineRunInfo.h MDF/OnlineRunInfo.h
    *
    * Basic run information from ODIN bank.
    */
  class PACKED_DATA run2_odin_t  {
  public:
    uint32_t       _run_number;
    uint16_t       _event_type;
    uint16_t       _step_number;
    uint32_t       _orbit_id;
    uint64_t       _event_id;
    uint64_t       _gps_time;
    unsigned       _det_status   : 24;
    unsigned       errors        :  8;
    unsigned       _bunch_id     : 12;
    unsigned       _tae_window   :  3;
    unsigned       pad0          :  1;
    unsigned       _trigger_type :  3;
    unsigned       _readout_type :  2;
    unsigned       forceBit      :  1;
    unsigned       _bx_type      :  2;
    unsigned       bunchCurrent  :  8;
    uint32_t       _tck;

  public:
    /// Emulate SODIN access functions to ease interfacing
    uint32_t run_number()   const  { return _run_number;     }
    uint16_t event_type()   const  { return _event_type;     }
    uint16_t step_number()  const  { return _step_number;    }
    uint8_t  error_type()   const  { return errors;          }
    uint8_t  readout_type() const  { return _readout_type;   }
    uint8_t  trigger_type() const  { return _trigger_type;   }
    uint32_t tck()          const  { return _tck;            }
    uint16_t bunch_id()     const  { return _bunch_id;       }
    uint16_t orbit_id()     const  { return _orbit_id;       }
    uint8_t  bx_type()      const  { return _bx_type;        }
    uint64_t event_id()     const  { return _event_id;       }
    uint8_t  tae_window()   const  { return _tae_window;     }
    bool     tae_central()  const  { return 0 != (_trigger_type&OdinData::TimingTrigger); }
    bool     is_tae()       const  { return tae_central() || tae_window() > 0; }
  };

  using run3_odin_t = pcie40::sodin_t;
  
}      // End namespace Online
#include <RTL/Unpack.h>

/// C/C++ include files
#include <vector>
#include <string>

/// Online namespace declaration
namespace Online    {

  class bank_header_t;
  
  /// Helper class to print bank headers
  namespace event_print {
    /// Produce data lines with ODIN bank content
    std::vector<std::string> odin_data(const std::pair<const bank_header_t*, const void*>& bank);
    /// Produce data lines with ODIN bank content
    std::vector<std::string> odin_data(const run2_odin_t* b, int vsn);
    /// Produce data lines with ODIN bank content
    std::vector<std::string> odin_data(const run3_odin_t* b, int vsn);
  }
  
}      // End namespace Online
#endif ///ONLINE_EVENTDATA_ODIN_T_H
