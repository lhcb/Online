//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  raw_bank_offline_t.h
//--------------------------------------------------------------------------
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_EVENTDATA_RAW_BANK_OFFLINE_H
#define ONLINE_EVENTDATA_RAW_BANK_OFFLINE_H 1

/// Framework includes
#include <EventData/bank_types_t.h>
#include <EventData/bank_header_t.h>
#include <RTL/Pack.h>

/// Online namespace declaration
namespace Online    {

  /// Tell1 data output from the LHCb online system
  /** @class Online::rawbank_t rawbank_t.h
   *
   * Raw data bank sent by the TELL1 boards of the LHCb DAQ.
   *
   * For a detailed description of the raw bank format,
   * see <a href="https://edms.cern.ch/document/565851/5">EDMS-565851/5</a>
   *
   * Note concerning the changes done 06/03/2006:
   * - The bank size is in BYTES
   * - The full size of a bank in memory is long word (32 bit) aligned
   *   ie. a bank of 17 Bytes length actually uses 20 Bytes in memory.
   * - The bank length accessors size() and setSize() do not contain
   *   the bank header, ie. size() = (total size) - (header size).
   * - The length passed to the RawEvent::createBank should NOT
   *   contain the size of the header !
   * - If the full padded bank size is required use the utility
   *   function rawbank_offline_t::totalSize = size + header size + padding size.
   *
   * @author Helder Lopes
   * @author Markus Frank
   * created Tue Oct 04 14:45:20 2005
   * 
   */
  class PACKED_DATA raw_bank_offline_t : public bank_header_t, public bank_types_t {
  protected:
    /// Default Constructor
    raw_bank_offline_t() {}

    /// Default Destructor
    ~raw_bank_offline_t() {}

  public:   

    /// Magic pattern for Raw bank headers
    enum RawPattern{ MagicPattern=0xCBCB };

    /// Set magic word
    void setMagic()                  {  m_magic = MagicPattern;                    }

    /// Return pointer to begining of data body of the bank
    unsigned int* data()             {  return m_data;                             }

    /// Return pointer to begining of data body of the bank (const data access)
    const unsigned int* data() const {  return m_data;                             }

    /// Begin iterator 
    template <typename T> T* begin() {  return (T*)this->data();                   }

    /// End iterator 
    template <typename T> T* end()   {  return ((T*)this->data())+size()/sizeof(T);}

    /// Begin iterator over const iteration
    template <typename T> const T* begin() const  {  return (T*)this->data();      }

    /// End iterator of const iteration
    template <typename T>  const T* end() const
      {  return ((T*)this->data()) + size()/sizeof(T);                             }

    raw_bank_offline_t* next()  {  return (raw_bank_offline_t*)((char*)this + totalSize());          }
    const raw_bank_offline_t* next()  const
    {        return (const raw_bank_offline_t*)((const char*)this + totalSize());           }

  private:
    /// Opaque data block
    unsigned int   m_data[1];
  };   /// class raw_bank_offline_t
}      /// End namespace Online
#include <RTL/Unpack.h>
#endif /// ONLINE_EVENTDATA_RAW_BANK_OFFLINE_H
