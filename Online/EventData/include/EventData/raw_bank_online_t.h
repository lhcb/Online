//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//==========================================================================
#ifndef ONLINE_EVENTDATA_RAW_BANK_ONLINE_T_H
#define ONLINE_EVENTDATA_RAW_BANK_ONLINE_T_H

/// Framework includes
#include <EventData/bank_types_t.h>
#include <EventData/bank_header_t.h>
#include <RTL/Pack.h>

/// Online namespace declaration
namespace Online {

  /// Online PCIE40 Bank emulation
  /** @class Online::raw_bank_online_t
   *
   * Online Raw data bank created from MEPs sent by the PCIe40 or from MDF RawBanks read from file
   *
   * For a detailed description of the raw bank format,
   * see <a href="https://edms.cern.ch/document/565851/5">EDMS-565851/5</a>
   *
   * Note concerning the changes done 06/03/2006:
   * - The bank size is in BYTES
   * - The full size of a bank in memory is long word (32 bit) aligned
   *   ie. a bank of 17 Bytes length actually uses 20 Bytes in memory.
   * - The bank length accessors size() and setSize() do not contain
   *   the bank header, ie. size() = (total size) - (header size).
   * - The length passed to the RawEvent::createBank should NOT
   *   contain the size of the header !
   * - If the full padded bank size is required use the utility
   *   function raw_bank_online_t::totalSize = size + header size + padding size.
   *
   * @author Helder Lopes
   * @author Markus Frank
   * @author Beat Jost
   * created Tue Oct 04 14:45:20 2005
   *
   */
  class PACKED_DATA raw_bank_online_t : public bank_header_t, public bank_types_t  {
  private:
    /// Opaque data block
    const unsigned int *m_data {0};

  public:
    /// Dummy Constructor
    raw_bank_online_t() = default;

    /// Magic pattern for Raw bank headers
    enum RawPattern    { MagicPattern = 0xFACE };

    /// Set magic word
    void setMagic()                 {  m_magic = MagicPattern;                      }

    /// Return pointer to beginning of data body of the bank (const data access)
    const unsigned int* data() const{  return m_data;                                }

    void setData(const void *val)   {  m_data = (const unsigned int*) val;           }

    /// Begin iterator
    template<typename T> T* begin() {  return (T*) m_data;                           }

    /// End iterator
    template<typename T> T* end()   {  return ((T*) m_data) + size() / sizeof(T);    }

    /// Begin iterator over const iteration
    template<typename T> const T* begin() const { return (const T*) m_data;          }

    /// End iterator of const iteration
    template<typename T> const T* end() const  {
      return ((const T*) m_data) + size() / sizeof(T);
    }
  };
}      // End namespace Online
#include <RTL/Unpack.h>

/// Output streamer
inline std::ostream& operator <<(std::ostream& os, const Online::raw_bank_online_t& b)  {
  const Online::bank_header_t& h = b;
  return os << h;
}
#endif /* ONLINE_EVENTDATA_RAW_BANK_ONLINE_T_H */
