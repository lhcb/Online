//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//              B.Jost
//==========================================================================
#ifndef ONLINE_PCIE40DATA_PCIE40_H
#define ONLINE_PCIE40DATA_PCIE40_H

#define PCIE40_ENABLE_CHECKS  1

// Framework include files
#include <EventData/bank_types_t.h>
#include <EventData/bank_header_t.h>
#include <EventData/raw_bank_online_t.h>

// C/C++ include files
#include <stdexcept>
#include <cstdint>
#include <cstring>

#include <RTL/Pack.h>

/// Online namespace declaration
namespace Online {

  /// pcie40 namespace declaration
  namespace pcie40  {

    typedef raw_bank_online_t bank_t;

    inline size_t round_up(size_t value, size_t base)   {
      return (((value-1)/base + 1)*base);
    }

    template <typename T> inline T* add_ptr(void* ptr, unsigned long offs)   {
      return reinterpret_cast<T*>(((char*)ptr + offs));
    }
    template <typename T> inline const T* add_ptr(const void* ptr, unsigned long offs)   {
      return reinterpret_cast<const T*>(((const char*)ptr + offs));
    }

    static constexpr const unsigned long MAGIC_PATTERN = 0xFEEDFACEFEEDUL;

#if PCIE40_ENABLE_CHECKS
#define MAGIC_WORD  unsigned long magic = MAGIC_PATTERN;	\
    unsigned long pattern()  const {  return magic;           }	\
    void setPattern()              {  magic = MAGIC_PATTERN;  }
#define DATA_OFFSET(length)  length+sizeof(const unsigned long)
#else
#define MAGIC_WORD						\
    unsigned long pattern()  const {  return MAGIC_PATTERN;   }	\
    void setPattern()              {                          }
#define DATA_OFFSET(length)  length
#endif

    /// Forward declarations
    class mep_header_t;
    class event_t;
    class multi_fragment_t;
    class multi_fragment_header_t;
    class bank_collection_t;
    class event_collection_t;

    /// Class describing abstract pcie40 frontend data (opaque data block)
    /*
     * \author  B.Jost
     * \version 1.0
     * \date    25/10/2019
     */
    class PACKED_DATA frontend_data_t  {
      /// Default constructor
      frontend_data_t() = delete;
      /// Disabled move constructor
      frontend_data_t(frontend_data_t&& copy) = delete;
      /// Disabled copy constructor
      frontend_data_t(const frontend_data_t& copy) = delete;
      /// Disabled move assignment
      frontend_data_t& operator=(frontend_data_t&& copy) = delete;
      /// Disabled copy assignment
      frontend_data_t& operator=(const frontend_data_t& copy) = delete;

    public:
      /// Access to data block
      void *data();
      /// Access to data block (CONST)
      const void *data()  const;
      /// Jump to next data fragment including alignment
      frontend_data_t *next(int len, std::size_t align);
      /// Jump to next data fragment including alignment (CONST)
      const frontend_data_t *next(int len, std::size_t align)   const;
      /// Start pointer of the data fragment
      template <typename T = uint8_t> T* begin()     {
	return reinterpret_cast<T*>(this);
      }
      /// Start pointer of the data fragment (CONST)
      template <typename T = uint8_t> const T* begin()   const    {
	return reinterpret_cast<const T*>(this);
      }
    };

    /// Class describing a MFP header  (see EDMS 2100937 for details)
    /*
     * \author  M.Frank
     * \version 1.0
     * \date    25/10/2019
     */
    class PACKED_DATA multi_fragment_header_t   {
    public:
      /// Alignment of the type array in the payload
      static constexpr std::size_t types_alignment = 4;
      /// Alignment of the size array in the payload
      static constexpr std::size_t sizes_alignment = 4;
      /// Magic pattern definition for the multi_fragment_header type
      static constexpr uint16_t MagicPattern = 0x40CE;

    public:
      uint16_t magic      { MagicPattern }; /// Storage space for the magic pattern
      uint16_t packing    { 0 };            /// Number of Fragments in the MEO
      uint32_t size       { 0 };            /// Size of the subsequent Data (without header)
      uint64_t event_id   { 0 };            /// Event ID of the first event
      uint16_t source_id  { 0 };            /// Source ID
      uint8_t  alignment  { 0 };            /// Fragments in this packet are aligned to 2**ALIGN
      uint8_t  version    { 0 };            /// Version of the data format of the fragments

    public:
      /// Default constructor
      multi_fragment_header_t() = default;
      /// Disabled move constructor
      multi_fragment_header_t(multi_fragment_header_t&& copy) = delete;
      /// Disabled copy constructor
      multi_fragment_header_t(const multi_fragment_header_t& copy) = delete;
      /// Disabled move assignment
      multi_fragment_header_t& operator=(multi_fragment_header_t&& copy) = delete;
      /// Disabled copy assignment
      multi_fragment_header_t& operator=(const multi_fragment_header_t& copy) = delete;
    };

    /// Class describing a mfp (header + payload)  (see EDMS 2100937 for details)
    /*
     *
     * \author  B.Jost
     * \version 1.0
     * \date    25/10/2019
     */
    class PACKED_DATA multi_fragment_t   {
    private:
      /// Internal helper to access the data
      std::size_t data_offset()  const;

    public:
      typedef multi_fragment_header_t header_t;
      /// Fragment header
      header_t header;

    public:
      /// Default constructor
      multi_fragment_t() = default;
      /// Disabled move constructor
      multi_fragment_t(multi_fragment_t&& copy) = delete;
      /// Disabled copy constructor
      multi_fragment_t(const multi_fragment_t& copy) = delete;
      /// Disabled move assignment
      multi_fragment_t& operator=(multi_fragment_t&& copy) = delete;
      /// Disabled copy assignment
      multi_fragment_t& operator=(const multi_fragment_t& copy) = delete;
      /// Access the packing factor from the header
      unsigned int packingFactor() const {  return header.packing; }
      /// Check basic quantities for validity
      bool is_valid()   const;
      /// Pointer to the Types array
      const uint8_t *types()  const;
      /// Pointer to the array of bank payload sizes
      const uint16_t *sizes()  const;
      /// Pointer to the start of the data of the banks
      frontend_data_t *data();
      /// Pointer to the start of the data of the banks
      const frontend_data_t *data()  const;
      /// Access the next fragment in the list
      multi_fragment_t* next();
      /// Access the next fragment in the list
      const multi_fragment_t* next()   const;
      /// Get data by proper type
      template <typename T> T*       as();
      /// Get data by proper type (const)
      template <typename T> const T* as()  const;
    };

    /// Class describing the MEP header (see EDMS 2100937 for details)
    /*
     * \author  B.Jost
     * \version 1.0
     * \date    25/10/2019
     */
    class PACKED_DATA mep_header_t   {
    public:
      static constexpr uint16_t MagicPattern = 0xCEFA;  
      uint16_t magic           { MagicPattern };  /// Magic Word (0xCEFA)
      uint16_t num_source      { 0 };             /// Number of Sources (PCIe40s)
      uint32_t size            { 0 };             /// Length of the MEP in 4-byte words (with header)

    public:
      /// Check basic quantities for validity
      bool is_valid()   const;
      /// Access the table of offsets in the MEP header
      const uint32_t* fragment_offsets()  const;
      /// Offset of individual MFPs
      uint32_t fragment_offset(std::size_t which)  const;
      /// Access to individual MFPs
      multi_fragment_t* multi_fragment(std::size_t which);
      /// Access to individual MFPs (CONST)
      const multi_fragment_t* multi_fragment(std::size_t which) const;
      /// Support iterations: Jump to next MEP
      const mep_header_t* next()  const;
    };

    /// A variable size collection of raw banks
    /*
     * \author  M.Frank
     * \version 1.0
     * \date    25/10/2019
     */
    class bank_collection_t   {
      friend class event_t;

    private:
      /// Inhibit default constructor
      bank_collection_t() = default;

    public:
      MAGIC_WORD                    /// Magic word -- if enabled
      std::size_t   length;         /// Number of regular banks in the collection
      std::size_t   specials;       /// Numberof special banks
      std::size_t   capacity;       /// Capacity of the bank collection
      bank_t        overrun_bank;   /// Bank buffer to be used if there are too many sources.
      bank_t        banks[1];       /// List of bank headers
      static constexpr std::size_t data_offset = DATA_OFFSET(3*sizeof(std::size_t));

      /// Disabled move constructor
      bank_collection_t(bank_collection_t&& copy) = delete;
      /// Disabled copy constructor
      bank_collection_t(const bank_collection_t& copy) = delete;
      /// Disabled move assignment
      bank_collection_t& operator=(bank_collection_t&& copy) = delete;
      /// Disabled copy assignment
      bank_collection_t& operator=(const bank_collection_t& copy) = delete;

      /// Access number of regular banks
      std::size_t    num_banks()            const   {  return length;   }
      /// Access number of special banks
      std::size_t    num_special()          const   {  return specials; }
      /// Total capacity of banks
      std::size_t    max_banks()            const   {  return capacity; }
      /// Bank iteration: start of regular banks
      const bank_t*  begin()                const;
      /// Bank iteration: end marker of regular banks
      const bank_t*  end()                  const;
      /// Bank iteration: start of special banks
      const bank_t*  special_begin()        const;
      /// Bank iteration: end marker of special banks
      const bank_t*  special_end()          const;
      /// Bank iteration: access next element of the iteration
      const bank_t*  next(const bank_t* e)  const;
      /// Bank iteration: direct access
      const bank_t*  at(std::size_t which)       const;
      /// Bank iteration: direct access
      const bank_t*  operator[](std::size_t i)   const;
      /// Total length of the bank block in bytes
      std::size_t    total_length()         const;
      /// Copy data to target buffer
      void*          copy_data(void* pointer, unsigned short new_magic=0xFFFF)   const;
    };

    /// A full pcie40 event with multiple bank collection sorted by bank type
    /*
     * \author  B.Jost
     * \version 1.0
     * \date    25/10/2019
     */
    class event_t  {
    private:
      friend class event_collection_t;
      /// Inhibit default constructor
      event_t() = default;
    public:
      /// Magic word -- if enabled
      unsigned long magic { MAGIC_PATTERN };
      /// Event slot number
      std::size_t          slot { 0 };
      /// Flags structure
      union _flags  {
	unsigned long     raw;          /// Raw flag number
	struct _detail  {
	  uint16_t event_type;          /// Event type from ODIN bank
	  uint8_t  tae_entry;           /// TAE half window size
	  unsigned tae_central:   1;    /// Flag to indicate this is a central event
	  unsigned tae_first:     1;    /// Flag to indicate first event in TAE frame
	  unsigned have_odin:     1;    /// Flag to indicate an odin bank was found
	  unsigned nzs_mode:      1;    /// NZS flag from ODIN bank
	  unsigned trigger_type:  4;    /// Trigger type from ODIN bank
	  unsigned calib_type:    4;    /// Calibration type from ODIN bank
	  unsigned spare2:       28;    /// Spare 2
	} detail;
      } flags;
      /// Array of bank collections
      bank_collection_t banks[1];
      /// Offset to the data array
      static constexpr std::size_t data_offset = sizeof(std::size_t)+2*sizeof(unsigned long);

      /// Access magic word in the data
      unsigned long pattern()  const {  return magic;           }
      /// Set magic word in the data
      void setPattern()              {  magic = MAGIC_PATTERN;  }

    private:
      /// Disabled move constructor
      event_t(event_t&& copy) = delete;
      /// Disabled copy constructor
      event_t(const event_t& copy) = delete;
      /// Disabled move assignment
      event_t& operator=(event_t&& copy) = delete;
      /// Disabled copy assignment
      event_t& operator=(const event_t& copy) = delete;

    public:
      /// Access collection Offset according to source ID
      static std::size_t                    collection_offset_source_id(uint16_t src_id);
      /// Access collection Offset according to source ID and bank type
      static std::pair<bool,std::size_t>    collection_offset(uint16_t src_id, uint8_t type);
      /// Get bank data of a given source and type
      std::pair<std::size_t, const bank_t*> get(uint16_t src_id, uint8_t type)  const;
      /// Total number of bank collections availible
      std::size_t                           num_bank_collections()     const;
      /// Access to the bank collection indexed by subdetector
      const bank_collection_t*              bank_collection(std::size_t i)  const;
      /// Total memory size of the event block in bytes
      std::size_t                           total_length()             const;
      /// Copy event data to target buffer
      void*                                 copy_data(void* pointer,
						      unsigned short new_magic=0xFFFF)   const;
      /// Check TAE flags
      bool                                  is_tae()                   const;
      /// Check TAE central flag
      bool                                  is_tae_central()           const;
      /// Check TAE first flag
      bool                                  is_tae_first()             const;
      /// Access half window size (Only valid for central event!)
      uint8_t                               tae_half_window()          const;
      /// Access TAE entry within window (1 ... 2*half_window+1)
      uint8_t                               tae_entry()                const;
      /// Check if an ODIN bank is present
      bool                                  have_odin()                const;
      /// Check NZS flag from ODIN bank if present
      bool                                  nzs_mode()                 const;
      /// Access trigger type from ODIN bank
      uint8_t                               trigger_type()             const;
      /// Access calibration type from ODIN bank
      uint8_t                               calib_type()               const;
      /// Access event type from ODIN bank
      uint16_t                              event_type()               const;
      /// Access ODIN bank if present
      std::pair<const bank_header_t*, const void*> get_odin_bank()     const;
      /// Access first bank of a given type if present
      std::pair<const bank_header_t*, const void*> get_bank(int bank_typ) const;
    };

    /// Collection of mep events as they arrive from the event builder
    /*
     * \author  B.Jost
     * \version 1.0
     * \date    25/10/2019
     */
    class event_collection_t  {
    public:
      MAGIC_WORD                     /// Magic word -- if enabled
      const mep_header_t* mep;       /// Reference to MEP header
      std::size_t   length;          /// Number of events in the collection
      std::size_t   capacity;        /// Capacity of the event collection
      /// Flags structure
      union _flags  {
	unsigned long     raw;          /// Raw flag number
	struct _detail  {
	  uint32_t run_number;          /// Last known "good" run number
	  unsigned spare:        32;    /// Spare
	} detail;
      } flags;
      event_t  events[1];            /// Array of events in the data section

      static constexpr std::size_t data_offset = DATA_OFFSET(2*sizeof(std::size_t));

    private:
      /// Disabled default constructor
      event_collection_t() = delete;
      /// Disabled move constructor
      event_collection_t(event_collection_t&& copy) = delete;
      /// Disabled copy constructor
      event_collection_t(const event_collection_t& copy) = delete;
      /// Disabled move assignment
      event_collection_t& operator=(event_collection_t&& copy) = delete;
      /// Disabled copy assignment
      event_collection_t& operator=(const event_collection_t& copy) = delete;

    public:
      /// Check if event entries are present
      bool                empty()      const  {  return length == 0;  }
      /// Number of encapsulated event in the collection
      std::size_t         size()       const  {  return length;       }
      /// Number of encapsulated event in the collection
      std::size_t         num_events() const  {  return length;       }
      /// Capacity of the event block
      std::size_t         max_events() const  {  return capacity;     }
      /// Access to the MEP header structure
      const mep_header_t* mep_header() const  {  return mep;          }
      /// Event iteration: iteration start
      const event_t* begin()                        const;
      /// Event iteration: end marker
      const event_t* end()                          const;
      /// Event iteration: access the next event
      const event_t* next(const event_t* e)         const;
      /// Direct access to an event by index
      const event_t* at(std::size_t which)          const;
      /// Direct access to an event by index
      const event_t* operator[](std::size_t which)  const;
      /// Total memory size of the event collection block in bytes
      std::size_t         total_length()            const;
    };

    /// Constant parameters to access the various records
    /*
     * \author  B.Jost
     * \version 1.0
     * \date    25/10/2019
     */
    namespace params   {
      typedef uint16_t detsource_t;

      static constexpr std::size_t INVALID_BANK_TYPE   = ~0x0UL;
      
      static constexpr detsource_t detectorid_ODIN       = 0;
      static constexpr detsource_t detectorid_VPA        = 2;
      static constexpr detsource_t detectorid_VPC        = 3;
      static constexpr detsource_t detectorid_Rich1      = 4;
      static constexpr detsource_t detectorid_UTA        = 5;
      static constexpr detsource_t detectorid_UTC        = 6;
      static constexpr detsource_t detectorid_FTA        = 7;
      static constexpr detsource_t detectorid_FTC        = 8;
      static constexpr detsource_t detectorid_Rich2      = 9;
      static constexpr detsource_t detectorid_Plume      = 10;
      static constexpr detsource_t detectorid_Ecal       = 11;
      static constexpr detsource_t detectorid_Hcal       = 12;
      static constexpr detsource_t detectorid_MuonA      = 13;
      static constexpr detsource_t detectorid_MuonC      = 14;

      static constexpr detsource_t sourceid_TOP5       = (uint16_t)(0xFFFF<<11);
      static constexpr detsource_t sourceid_top5_ODIN  = (uint16_t)((detectorid_ODIN<<11)&sourceid_TOP5);
      static constexpr detsource_t sourceid_top5_VPA   = (uint16_t)((detectorid_VPA<<11)&sourceid_TOP5);
      static constexpr detsource_t sourceid_top5_VPC   = (uint16_t)((detectorid_VPC<<11)&sourceid_TOP5);
      static constexpr detsource_t sourceid_top5_UTA   = (uint16_t)((detectorid_UTA<<11)&sourceid_TOP5);
      static constexpr detsource_t sourceid_top5_UTC   = (uint16_t)((detectorid_UTC<<11)&sourceid_TOP5);
      static constexpr detsource_t sourceid_top5_FTA   = (uint16_t)((detectorid_FTA<<11)&sourceid_TOP5);
      static constexpr detsource_t sourceid_top5_FTC   = (uint16_t)((detectorid_FTC<<11)&sourceid_TOP5);
      static constexpr detsource_t sourceid_top5_Ecal  = (uint16_t)((detectorid_Ecal<<11)&sourceid_TOP5);
      static constexpr detsource_t sourceid_top5_Hcal  = (uint16_t)((detectorid_Hcal<<11)&sourceid_TOP5);
      static constexpr detsource_t sourceid_top5_MuonA = (uint16_t)((detectorid_MuonA<<11)&sourceid_TOP5);
      static constexpr detsource_t sourceid_top5_MuonC = (uint16_t)((detectorid_MuonC<<11)&sourceid_TOP5);
      static constexpr detsource_t sourceid_top5_Rich1 = (uint16_t)((detectorid_Rich1<<11)&sourceid_TOP5);
      static constexpr detsource_t sourceid_top5_Rich2 = (uint16_t)((detectorid_Rich2<<11)&sourceid_TOP5);
      static constexpr detsource_t sourceid_top5_Plume = (uint16_t)((detectorid_Plume<<11)&sourceid_TOP5);

      /// From fiberDB:
      /// select substr(name,0,5) as nn, 2*count(substr(name,0,5)) as cc
      /// from pcie40s
      /// where name like "%TEL%"
      /// group by nn
      /// NOTE: *2 ===> We cannot distinguish between side A and side C
      static constexpr std::size_t maxTell40ODIN       =     1;   // True counts
      static constexpr std::size_t maxTell40VPA        =    54*4; //  52  // * 2 if VPRetinaCluster
      static constexpr std::size_t maxTell40VPC        =    54;   //  52  // * 2 if VPRetinaCluster
      static constexpr std::size_t maxTell40UTA        =   115*2; // 108
      static constexpr std::size_t maxTell40UTC        =   115;   // 108
      static constexpr std::size_t maxTell40FTA        =   150*2; // 144
      static constexpr std::size_t maxTell40FTC        =   150;   // 144
      static constexpr std::size_t maxTell40Ecal       =   100;   //  72
      static constexpr std::size_t maxTell40Hcal       =   100;   //  20
      static constexpr std::size_t maxTell40MuonA      =    25*2; //  22
      static constexpr std::size_t maxTell40MuonC      =    25;   //  22
      static constexpr std::size_t maxTell40Rich1      =    90*2; //  80
      static constexpr std::size_t maxTell40Rich2      =    70;   //  64
      static constexpr std::size_t maxTell40Plume      =     5;   //   4
      static constexpr std::size_t maxTell40Other      =   100;
      static constexpr std::size_t maxTell40           = maxTell40ODIN + maxTell40VPA + maxTell40VPC
	+ maxTell40UTA + maxTell40UTC + maxTell40FTA + maxTell40FTC + maxTell40Ecal + maxTell40Hcal
	+ maxTell40MuonA + maxTell40MuonC + maxTell40Rich1 + maxTell40Rich2 + maxTell40Plume + maxTell40Other;

      static constexpr std::size_t collectionCapacity[] = {
	maxTell40ODIN,
	maxTell40VPA,
	maxTell40VPC,
	maxTell40UTA,
	maxTell40UTC,
	maxTell40FTA,
	maxTell40FTC,
	maxTell40Ecal,
	maxTell40Hcal,
	maxTell40MuonA,
	maxTell40MuonC,
	maxTell40Rich1,
	maxTell40Rich2,
	maxTell40Plume,
	maxTell40Other
      };
      static constexpr std::size_t collection_id_ODIN    = 0;

      static constexpr std::size_t collectionSizeODIN    = sizeof(bank_collection_t);
      static constexpr std::size_t collectionSizeVPA     = sizeof(bank_collection_t)+(maxTell40VPA*sizeof(bank_t));
      static constexpr std::size_t collectionSizeVPC     = sizeof(bank_collection_t)+(maxTell40VPC*sizeof(bank_t));
      static constexpr std::size_t collectionSizeUTA     = sizeof(bank_collection_t)+(maxTell40UTA*sizeof(bank_t));
      static constexpr std::size_t collectionSizeUTC     = sizeof(bank_collection_t)+(maxTell40UTC*sizeof(bank_t));
      static constexpr std::size_t collectionSizeFTA     = sizeof(bank_collection_t)+(maxTell40FTA*sizeof(bank_t));
      static constexpr std::size_t collectionSizeFTC     = sizeof(bank_collection_t)+(maxTell40FTC*sizeof(bank_t));
      static constexpr std::size_t collectionSizeEcal    = sizeof(bank_collection_t)+(maxTell40Ecal*sizeof(bank_t));
      static constexpr std::size_t collectionSizeHcal    = sizeof(bank_collection_t)+(maxTell40Hcal*sizeof(bank_t));
      static constexpr std::size_t collectionSizeMuonA   = sizeof(bank_collection_t)+(maxTell40MuonA*sizeof(bank_t));
      static constexpr std::size_t collectionSizeMuonC   = sizeof(bank_collection_t)+(maxTell40MuonC*sizeof(bank_t));
      static constexpr std::size_t collectionSizeRich1   = sizeof(bank_collection_t)+(maxTell40Rich1*sizeof(bank_t));
      static constexpr std::size_t collectionSizeRich2   = sizeof(bank_collection_t)+(maxTell40Rich2*sizeof(bank_t));
      static constexpr std::size_t collectionSizePlume   = sizeof(bank_collection_t)+(maxTell40Plume*sizeof(bank_t));
      static constexpr std::size_t collectionSizeOther   = sizeof(bank_collection_t)+(maxTell40Other*sizeof(bank_t));

      static constexpr std::size_t collectionOffset      = event_t::data_offset;
      static constexpr std::size_t collectionOffsetODIN  = collectionOffset;
      static constexpr std::size_t collectionOffsetVPA   = collectionOffsetODIN  + collectionSizeODIN;
      static constexpr std::size_t collectionOffsetVPC   = collectionOffsetVPA   + collectionSizeVPA;
      static constexpr std::size_t collectionOffsetUTA   = collectionOffsetVPC   + collectionSizeVPC;
      static constexpr std::size_t collectionOffsetUTC   = collectionOffsetUTA   + collectionSizeUTA;
      static constexpr std::size_t collectionOffsetFTA   = collectionOffsetUTC   + collectionSizeUTC;
      static constexpr std::size_t collectionOffsetFTC   = collectionOffsetFTA   + collectionSizeFTA;
      static constexpr std::size_t collectionOffsetEcal  = collectionOffsetFTC   + collectionSizeFTC;
      static constexpr std::size_t collectionOffsetHcal  = collectionOffsetEcal  + collectionSizeEcal;
      static constexpr std::size_t collectionOffsetMuonA = collectionOffsetHcal  + collectionSizeHcal;
      static constexpr std::size_t collectionOffsetMuonC = collectionOffsetMuonA + collectionSizeMuonA;
      static constexpr std::size_t collectionOffsetRich1 = collectionOffsetMuonC + collectionSizeMuonC;
      static constexpr std::size_t collectionOffsetRich2 = collectionOffsetRich1 + collectionSizeRich1;
      static constexpr std::size_t collectionOffsetPlume = collectionOffsetRich2 + collectionSizeRich2;
      static constexpr std::size_t collectionOffsetOther = collectionOffsetPlume + collectionSizePlume;

      static constexpr std::size_t collectionOffsets[]  = {
	collectionOffsetODIN,
	collectionOffsetVPA, 
	collectionOffsetVPC, 
	collectionOffsetUTA,
	collectionOffsetUTC,
	collectionOffsetFTA,
	collectionOffsetFTC,
	collectionOffsetEcal,
	collectionOffsetHcal,
	collectionOffsetMuonA,
	collectionOffsetMuonC,
	collectionOffsetRich1,
	collectionOffsetRich2,
	collectionOffsetPlume,
	collectionOffsetOther
      };
      static constexpr std::size_t num_bank_collection  = (sizeof(collectionOffsets)/sizeof(collectionOffsets[0]));
      static constexpr std::size_t eventRecordLength    = collectionOffsetOther + collectionSizeOther;
    }

    /// Access to data block
    inline void* frontend_data_t::data()    {
      return this;
    }

    /// Access to data block (CONST)
    inline const void* frontend_data_t::data()  const    {
      return this;
    }

    /// Jump to next data fragment including alignment
    inline frontend_data_t* frontend_data_t::next(int len, std::size_t align)    {
      return add_ptr<frontend_data_t>(this, round_up(len, 1<<align));
    }

    /// Jump to next data fragment including alignment (CONST)
    inline const frontend_data_t* frontend_data_t::next(int len, std::size_t align)   const    {
      return add_ptr<const frontend_data_t>(this, round_up(len, 1<<align));
    }

    /// Check basic quantities for validity
    inline bool multi_fragment_t::is_valid()   const    {
      return header.magic == header_t::MagicPattern && header.size > 0;
    }

    /// Internal helper to access the data
    inline std::size_t multi_fragment_t::data_offset()  const  {
      return sizeof(header)
	+ round_up(sizeof(uint8_t)  * this->header.packing, header_t::types_alignment)
	+ round_up(sizeof(uint16_t) * this->header.packing, header_t::sizes_alignment);
      // Old decoding aligned to 2**header.alignment, new decoding, sizes are aligned to 32 bits:
      //+ round_up(sizeof(uint16_t) * this->header.packing, 1<<this->header.alignment);
    }

    /// pointer to the Types array
    inline const uint8_t *multi_fragment_t::types()  const    {
      return add_ptr<uint8_t>(this, sizeof(multi_fragment_t));
    }

    /// Pointer to the array of bank payload sizes
    inline const uint16_t *multi_fragment_t::sizes()  const    {
      return add_ptr<uint16_t>(this, sizeof(multi_fragment_t) + round_up(this->header.packing, header_t::types_alignment));
    }

    /// Pointer to the start of the data of the Banks
    inline frontend_data_t* multi_fragment_t::data()    {
      return add_ptr<frontend_data_t>(this, this->data_offset());
    }

    /// Pointer to the start of the data of the Banks (CONST)
    inline const frontend_data_t* multi_fragment_t::data()   const    {
      return add_ptr<frontend_data_t>(this, this->data_offset());
    }

    /// Access the next fragment in the list
    inline multi_fragment_t *multi_fragment_t::next()   {
      return add_ptr<multi_fragment_t>(this, this->data_offset() + round_up(this->header.size,4));
    }

    /// Access the next fragment in the list (CONST)
    inline const multi_fragment_t *multi_fragment_t::next()  const    {
      return add_ptr<const multi_fragment_t>(this, this->data_offset() + round_up(this->header.size,4));
    }

    /// Get data by proper type
    template <typename T> inline T*       multi_fragment_t::as()        {
      return (T*)this->data();
    }
    /// Get data by proper type (const)
    template <typename T> inline const T* multi_fragment_t::as()  const {
      return (T*)this->data();
    }
#if defined(__clang__) || defined(__CLING__)
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warray-bounds"
#elif defined(__GNUC__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored   "-Warray-bounds"
#endif
    /// Check basic quantities for validity
    inline bool mep_header_t::is_valid()   const    {
      const uint32_t* h = add_ptr<uint32_t>(this, 0);
      return (this->magic == MagicPattern) && (this->num_source > 0) && (h[0] != h[1]) && (h[0] != h[2]);
    }
#if defined(__clang__) || defined(__CLING__)
#pragma clang diagnostic pop
#elif defined(__GNUC__)
#pragma GCC   diagnostic pop
#endif
    /// Access the table of offsets in the MEP header
    inline const uint32_t* mep_header_t::fragment_offsets()  const   {
      const std::size_t offsets = sizeof(mep_header_t) + round_up(sizeof(uint16_t)*this->num_source, 4);
      return add_ptr<uint32_t>(this, offsets);
    }

    /// Offset of individual MFPs
    inline uint32_t mep_header_t::fragment_offset(std::size_t which)  const   {
      const uint32_t* offsetN = this->fragment_offsets();
      return offsetN[which]*sizeof(uint32_t);
    }

    /// Access to individual MFPs
    inline multi_fragment_t* mep_header_t::multi_fragment(std::size_t which)  {
      return add_ptr<multi_fragment_t>(this, this->fragment_offset(which));
    }

    /// Access to individual MFPs (CONST)
    inline const multi_fragment_t* mep_header_t::multi_fragment(std::size_t which) const    {
      return add_ptr<multi_fragment_t>(this, this->fragment_offset(which));
    }

    /// Support iterations: Jump to next MEP
    inline const mep_header_t* mep_header_t::next()  const  {
      return add_ptr<mep_header_t>(this, this->size*sizeof(uint32_t));
    }

    /// Bank iteration: start of regular banks
    inline const bank_t* bank_collection_t::begin()  const {
      return this->banks; 
    }

    /// Bank iteration: start of special banks
    inline const bank_t* bank_collection_t::special_begin()  const   {
      return add_ptr<bank_t>(this->banks,sizeof(bank_t) * (this->capacity - this->specials));
    }

    /// Bank iteration: end marker of special banks
    inline const bank_t* bank_collection_t::special_end()  const   {
      return add_ptr<bank_t>(this->banks,sizeof(bank_t) * this->capacity);
    }

    /// Bank iteration: end marker of regular banks
    inline const bank_t* bank_collection_t::end()  const   {
      return add_ptr<bank_t>(this->banks,sizeof(bank_t) * this->length);
    }

    /// Bank iteration: access next element of the iteration
    inline const bank_t* bank_collection_t::next(const bank_t* e)  const   {
      return add_ptr<bank_t>(e,sizeof(bank_t));
    }

    /// Bank iteration: direct access
    inline const bank_t* bank_collection_t::at(std::size_t i)   const  {
      return add_ptr<bank_t>(this->banks,sizeof(bank_t)*i);
    }

    /// Bank iteration: direct access
    inline const bank_t* bank_collection_t::operator[](std::size_t i)   const  {
      return add_ptr<bank_t>(this->banks,sizeof(bank_t)*i);
    }

    /// Total length of the bank block in bytes
    inline std::size_t bank_collection_t::total_length()  const    {
      std::size_t len = 0;
      for( const auto* b=this->begin(); b != this->end(); b=this->next(b) )
	len += b->totalSize();
      for( const auto* b=this->special_begin(); b != this->special_end(); b=this->next(b) )
	len += b->totalSize();
      return len;
    }

    /// Copy data to target buffer
    inline void* bank_collection_t::copy_data(void* ptr, unsigned short new_magic)   const   {
      struct _bank_magic_t : public bank_header_t {
	static void set(void* p, unsigned short m)
	{ if ( m != 0xffff ) ((_bank_magic_t*)p)->m_magic = m; }
      };
      uint8_t* p = (uint8_t*)ptr;
      std::size_t len = bank_t::hdrSize();
      for( const auto* b=this->begin(); b != this->end(); b=this->next(b) )  {
	::memcpy(p,       b,         len);
	::memcpy(p + len, b->data(), b->totalSize()-len);
	_bank_magic_t::set(p, new_magic);
	p += b->totalSize();
      }
      for( const auto* b=this->special_begin(); b != this->special_end(); b=this->next(b) )  {
	::memcpy(p,       b,         len);
	::memcpy(p + len, b->data(), b->totalSize()-len);
	_bank_magic_t::set(p, new_magic);
	p += b->totalSize();
      }
      return p;
    }


    /// Check TAE flags
    inline bool event_t::is_tae()   const   {
      return this->flags.detail.tae_central || this->flags.detail.tae_entry>0;
    }
    
    /// Check TAE central flag
    inline bool event_t::is_tae_central()  const   {
      return this->flags.detail.tae_central;
    }

    /// Check TAE first flag
    inline bool event_t::is_tae_first()  const   {
      return this->flags.detail.tae_first;
    }

    /// Access TAE entry number
    inline uint8_t event_t::tae_entry()  const   {
      return this->flags.detail.tae_entry;
    }

    /// Access half window size  (Only valid for central event!)
    inline uint8_t event_t::tae_half_window()  const   {
      uint8_t w = this->flags.detail.tae_entry;
      return (w>0) ? w-1 : 0;
    }

    /// Check if an odin bank is present
    inline bool event_t::have_odin()  const   {
      return this->flags.detail.have_odin;
    }

    /// Check NZS flag from ODIN bank if present
    inline bool event_t::nzs_mode()  const  {
      return this->flags.detail.nzs_mode;
    }

    /// Access trigger type from ODIN bank
    inline uint8_t event_t::trigger_type()  const   {
      return this->flags.detail.trigger_type;
    }

    /// Access calibration type from ODIN bank
    inline uint8_t event_t::calib_type()  const   {
      return this->flags.detail.calib_type;
    }
    
    /// Access event type from ODIN bank
    inline uint16_t event_t::event_type()  const   {
      return this->flags.detail.event_type;
    }
    
    /// Access collection Offset according to source ID
    inline std::size_t event_t::collection_offset_source_id(uint16_t src_id)   {
      switch( src_id&params::sourceid_TOP5 )   {
      case params::sourceid_top5_ODIN:	return params::collectionOffsetODIN;
      case params::sourceid_top5_VPA:	return params::collectionOffsetVPA;
      case params::sourceid_top5_VPC:	return params::collectionOffsetVPC;
      case params::sourceid_top5_UTA:	return params::collectionOffsetUTA;
      case params::sourceid_top5_UTC:	return params::collectionOffsetUTC;
      case params::sourceid_top5_FTA:	return params::collectionOffsetFTA;
      case params::sourceid_top5_FTC:	return params::collectionOffsetFTC;
      case params::sourceid_top5_Ecal:	return params::collectionOffsetEcal;
      case params::sourceid_top5_Hcal:	return params::collectionOffsetHcal;
      case params::sourceid_top5_MuonA:	return params::collectionOffsetMuonA;
      case params::sourceid_top5_MuonC:	return params::collectionOffsetMuonC;
      case params::sourceid_top5_Rich1:	return params::collectionOffsetRich1;
      case params::sourceid_top5_Rich2:	return params::collectionOffsetRich2;
      case params::sourceid_top5_Plume:	return params::collectionOffsetPlume;
      default:                          return params::collectionOffsetOther;
      }
    }
    
    /// Access collection Offset according to source ID and bank type
    inline std::pair<bool,std::size_t> event_t::collection_offset(uint16_t src_id, uint8_t type)   {
      std::size_t offset = event_t::collection_offset_source_id(src_id&params::sourceid_TOP5);
      switch ( type ) {
      case bank_types_t::DaqErrorFragmentThrottled:
      case bank_types_t::DaqErrorBXIDCorrupted:
      case bank_types_t::DaqErrorSyncBXIDCorrupted:
      case bank_types_t::DaqErrorFragmentMissing:
      case bank_types_t::DaqErrorFragmentTruncated:
      case bank_types_t::DaqErrorIdleBXIDCorrupted:
      case bank_types_t::DaqErrorFragmentMalformed:
      case bank_types_t::DaqErrorEVIDJumped:
	// Error banks will be added at the end of each subdetector's bank collection
	return std::make_pair(false, offset);
      default:
	// Regular banks will be added at the beginning of each subdetector's bank collection
	return std::make_pair(true,offset);
      }
#if 0
      // This tries to order somehow the banks in each subdetector section
      // However, this fails if the FPGAs are reprogrammed to only produce TDET banks.....
      switch ( type ) {
      case bank_types_t::ODIN:
	return std::make_pair(true,offset);

      case bank_types_t::VP:                 // Does not exist in hardware. Only SIM
      case bank_types_t::Velo:
      case bank_types_t::VeloError:
      case bank_types_t::VPRetinaCluster:
      case bank_types_t::VeloThresholdScan:  // Hardware only ?
	return std::make_pair(true,offset);

      case bank_types_t::UT:
      case bank_types_t::UTFull:
      case bank_types_t::UTError:
      case bank_types_t::UTPedestal:
	return std::make_pair(true,offset);

      case bank_types_t::FTCluster:
      case bank_types_t::FTCalibration:
      case bank_types_t::FTGeneric:
      case bank_types_t::FTSpecial:
      case bank_types_t::FTNZS:
	return std::make_pair(true,offset);

      case bank_types_t::Rich:
      case bank_types_t::RichError:
      case bank_types_t::RichCommissioning:
	return std::make_pair(true,offset);
	
      case bank_types_t::HC:
      case bank_types_t::HCError:
      case bank_types_t::Calo:
      case bank_types_t::CaloSpecial:
      case bank_types_t::CaloError:
      case bank_types_t::EcalPacked:
      case bank_types_t::HcalPacked:
	return std::make_pair(true,offset);

      case bank_types_t::Muon:
      case bank_types_t::MuonFull:
      case bank_types_t::MuonError:
      case bank_types_t::MuonSpecial:
	return std::make_pair(true,offset);

      case bank_types_t::Plume:
      case bank_types_t::PlumeSpecial:
      case bank_types_t::PlumeError:
	return std::make_pair(true,offset);
	
      case bank_types_t::Other:
      case bank_types_t::TestDet:
	return std::make_pair(true,params::collectionOffsetOther);

      case bank_types_t::DaqErrorFragmentThrottled:
      case bank_types_t::DaqErrorBXIDCorrupted:
      case bank_types_t::DaqErrorSyncBXIDCurrupt:
      case bank_types_t::DaqErrorFragmentMissing:
      case bank_types_t::DaqErrorFragmentTruncated:
	switch( offset )   {
	case params::collectionOffsetODIN:
	case params::collectionOffsetVPA:
	case params::collectionOffsetVPC:
	case params::collectionOffsetUTA:
	case params::collectionOffsetUTC:
	case params::collectionOffsetFTA:
	case params::collectionOffsetFTC:
	case params::collectionOffsetEcal:
	case params::collectionOffsetHcal:
	case params::collectionOffsetMuonA:
	case params::collectionOffsetMuonC:
	case params::collectionOffsetRich1:
	case params::collectionOffsetRich2:
	case params::collectionOffsetPlume:
	  return std::make_pair(false, offset);      
	case params::collectionOffsetOther:
	default:
	  return std::make_pair(false,params::collectionOffsetOther);
	}
	
      default:
	return std::make_pair(false,params::INVALID_BANK_TYPE);
      }
#endif
    }

    /// Get bank data of a given source and type
    inline std::pair<std::size_t, const bank_t*> event_t::get(uint16_t src_id, uint8_t type)   const  {
      auto ret = event_t::collection_offset(src_id, type);
      const auto* c = add_ptr<bank_collection_t>(this->banks,ret.second);
      if ( ret.first )   {
	return std::make_pair(c->length,c->banks);
      }
      return std::make_pair(c->specials,add_ptr<bank_t>(c,(c->capacity - c->specials)*sizeof(bank_t)));
    }

    /// Total number of bank collections availible
    inline std::size_t event_t::num_bank_collections()  const    {
      return params::num_bank_collection;
    }

    /// Access to the bank collection indexed by subdetector
    inline const bank_collection_t* event_t::bank_collection(std::size_t which)   const    {
      return add_ptr<bank_collection_t>(this,params::collectionOffsets[which]);      
    }

    /// Total memory size of the event block in bytes
    inline std::size_t event_t::total_length()  const    {
      std::size_t len = 0;
      for( std::size_t i=0, n=this->num_bank_collections(); i<n; ++i)
	len += this->bank_collection(i)->total_length();
      return len;
    }
    
    /// Copy event data to target buffer
    inline void* event_t::copy_data(void* ptr, unsigned short new_magic)   const   {
      for( std::size_t i=0, n=this->num_bank_collections(); i<n; ++i)
	ptr = this->bank_collection(i)->copy_data(ptr, new_magic);
      return ptr;
    }

    /// Access ODIN bank if present
    inline std::pair<const bank_header_t*, const void*>
      event_t::get_odin_bank()   const   {
      for( std::size_t i=0, n=this->num_bank_collections(); i<n; ++i)  {
	const auto* bc = this->bank_collection(i);
	// ODIN should anyhow be the first collection
	for(const auto* b=bc->begin(); b != bc->end(); b = bc->next(b))  {
	  if ( b->type() == pcie40::bank_t::ODIN )
	    return { b, b->data() };
	}
      }
      return { nullptr, nullptr };
    }

    /// Access first bank of a given type if present
    inline std::pair<const bank_header_t*, const void*>
      event_t::get_bank(int bank_typ)   const   {
      for( std::size_t i=0, n=this->num_bank_collections(); i<n; ++i)  {
	const auto* bc = this->bank_collection(i);
	// ODIN should anyhow be the first collection
	for(const auto* b=bc->begin(); b != bc->end(); b = bc->next(b))  {
	  if ( b->type() == bank_typ )
	    return { b, b->data() };
	}
      }
      return { nullptr, nullptr };
    }

    /// Event iteration: iteration start
    inline const event_t* event_collection_t::begin()  const {
      return this->events; 
    }

    /// Event iteration: end marker
    inline const event_t* event_collection_t::end()  const   {
      return add_ptr<event_t>(this->events,params::eventRecordLength*this->length);
    }

    /// Event iteration: access the next event
    inline const event_t* event_collection_t::next(const event_t* e)  const   {
      return add_ptr<event_t>(e,params::eventRecordLength);
    }

    /// Direct access to an event by index
    inline const event_t* event_collection_t::operator[](std::size_t i)   const  {
      return add_ptr<event_t>(this->events,params::eventRecordLength*i);
    }

    /// Direct access to an event by index
    inline const event_t* event_collection_t::at(std::size_t i)   const  {
      return add_ptr<event_t>(this->events,params::eventRecordLength*i);
    }

    /// Total memory size of the event block in bytes
    inline std::size_t event_collection_t::total_length()  const    {
      std::size_t len = 0;
      for( const event_t* e=this->begin(); e != this->end(); e=this->next(e) )
	len += e->total_length();
      return len;
    }

  }  // namespace pcie40
}    // namepace Online

/// C/C++ include files
#include <vector>
#include <string>

/// Online namespace declaration
namespace Online  {

  /// Utility namespace for printing
  namespace event_print  {
    /// Format PCIE40 multi fragment header data to a set of lines for printing
    std::vector<std::string> headerData( const pcie40::multi_fragment_header_t* hdr );
    /// Format PCIE40 MEP header data to a set of lines for printing
    std::vector<std::string> headerData( const pcie40::mep_header_t* hdr );
    /// Format PCIE40 event header data to a set of lines for printing
    std::vector<std::string> headerData( const pcie40::event_t* hdr );
  }
}      // End namespace Online

#include <RTL/Unpack.h>
#endif // ONLINE_PCIE40DATA_PCIE40_H
