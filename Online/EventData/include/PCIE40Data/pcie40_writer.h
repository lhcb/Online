//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//              B.Jost
//==========================================================================
#ifndef ONLINE_PCIE40DATA_PCIE40_WRITER_H
#define ONLINE_PCIE40DATA_PCIE40_WRITER_H

// Framework include files
#include <PCIE40Data/pcie40.h>
#include <RTL/Pack.h>

// C/C++ include files

/// Online namespace declaration
namespace Online {

  /// pcie40 namespace declaration
  namespace pcie40  {

    class event_writer_t;
    class bank_collection_writer_t;
    class event_collection_writer_t;

    /// A variable size collection of raw banks with write access
    /*
     * \author  B.Jost
     * \version 1.0
     * \date    25/10/2019
     */
    class bank_collection_writer_t : public bank_collection_t  {
    private:
      bank_collection_writer_t() = delete;
      bank_collection_writer_t(bank_collection_writer_t&& copy) = delete;
      bank_collection_writer_t(const bank_collection_writer_t& copy) = delete;
      bank_collection_writer_t& operator=(bank_collection_writer_t&& copy) = delete;
      bank_collection_writer_t& operator=(const bank_collection_writer_t& copy) = delete;

    public:
      bank_t*        begin();
      bank_t*        end();
      bank_t*        special_begin();
      bank_t*        special_end();
      bank_t*        next(bank_t* e);
      bank_t*        at(std::size_t which);
      bank_t*        operator[](std::size_t which);
    };

    /// A full pcie40 event with multiple bank collection sorted by bank type with write access
    /*
     * \author  B.Jost
     * \version 1.0
     * \date    25/10/2019
     */
    class event_writer_t : public event_t {
    private:
      event_writer_t() = delete;
      event_writer_t(event_writer_t&& copy) = delete;
      event_writer_t(const event_writer_t& copy) = delete;
      event_writer_t& operator=(event_writer_t&& copy) = delete;
      event_writer_t& operator=(const event_writer_t& copy) = delete;

    public:
      std::pair<std::size_t, bank_t*> get(unsigned short src_id, unsigned char type);
      bank_collection_t* bank_collection(std::size_t which);
    };

    /// Collection of mep events as they arrive from the event builder with write access
    /*
     * \author  B.Jost
     * \version 1.0
     * \date    25/10/2019
     */
    class event_collection_writer_t : public event_collection_t  {
    private:
      event_collection_writer_t() = delete;
      event_collection_writer_t(event_collection_writer_t&& copy) = delete;
      event_collection_writer_t(const event_collection_writer_t& copy) = delete;
      event_collection_writer_t& operator=(event_collection_writer_t&& copy) = delete;
      event_collection_writer_t& operator=(const event_collection_writer_t& copy) = delete;

    public:
      event_writer_t* begin();
      event_writer_t* end();
      event_writer_t* next(event_writer_t* e);
      event_writer_t* at(std::size_t which);
      event_writer_t* operator[](std::size_t which);
    };

    inline bank_t* bank_collection_writer_t::begin()  {
      return banks; 
    }

    inline bank_t* bank_collection_writer_t::end()  {
      return add_ptr<bank_t>(banks, sizeof(bank_t) * length);
    }

    inline bank_t* bank_collection_writer_t::special_begin()   {
      return add_ptr<bank_t>(banks, sizeof(bank_t) * (capacity-specials));
    }

    inline bank_t* bank_collection_writer_t::special_end()   {
      return add_ptr<bank_t>(banks, sizeof(bank_t) * capacity);
    }

    inline bank_t* bank_collection_writer_t::next(bank_t* e)  {
      return add_ptr<bank_t>(e, sizeof(bank_t));
    }

    inline bank_t* bank_collection_writer_t::operator[](std::size_t which)  {
      return add_ptr<bank_t>(banks, sizeof(bank_t) * which);
    }

    inline bank_t* bank_collection_writer_t::at(std::size_t which)  {
      return add_ptr<bank_t>(banks, sizeof(bank_t) * which);
    }

    inline std::pair<std::size_t, bank_t*> event_writer_t::get(unsigned short src_id, unsigned char type)  {
      auto ret = event_t::collection_offset(src_id, type);
      auto*  c = add_ptr<bank_collection_t>(banks, ret.second);
      if ( ret.second == params::INVALID_BANK_TYPE )
	return std::make_pair(0, nullptr);
      else if ( ret.first )   {
	return std::make_pair(c->length, c->banks);
      }
      return std::make_pair(c->specials, add_ptr<bank_t>(c, (c->capacity - c->specials)*sizeof(bank_t)));
    }

    inline bank_collection_t* event_writer_t::bank_collection(std::size_t which)    {
      return add_ptr<bank_collection_t>(this, params::collectionOffsets[which]);      
    }

    inline event_writer_t* event_collection_writer_t::begin()  {
      return (event_writer_t*)events; 
    }

    inline event_writer_t* event_collection_writer_t::end()  {
      return add_ptr<event_writer_t>(events, params::eventRecordLength*length);
    }

    inline event_writer_t* event_collection_writer_t::next(event_writer_t* e)  {
      return add_ptr<event_writer_t>(e, params::eventRecordLength);
    }

    inline event_writer_t* event_collection_writer_t::operator[](std::size_t i)  {
      return add_ptr<event_writer_t>(events, params::eventRecordLength*i);
    }

    inline event_writer_t* event_collection_writer_t::at(std::size_t i)  {
      return add_ptr<event_writer_t>(events, params::eventRecordLength*i);
    }
  }  // namespace pcie40
}    // namepace Online

#include <RTL/Unpack.h>
#endif // ONLINE_PCIE40DATA_PCIE40_WRITER_H
