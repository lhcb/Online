//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//              B.Jost
//==========================================================================
#ifndef ONLINE_PCIE40DATA_PCIE40DECODER_H
#define ONLINE_PCIE40DATA_PCIE40DECODER_H

// Framework include files
#include <PCIE40Data/pcie40.h>

// C/C++ include files
#include <functional>
#include <memory>

/// Online namespace declaration
namespace Online {

  /// pcie40 namespace declaration
  namespace pcie40  {

    /// Printout/checking helper for the eventcollection object
    /*
     * \author  M.Frank
     * \version 1.0
     * \date    15/09/2019
     */
    class printer_t  final  {
    public:
      /// Check event collection object for consistency
      void check(const std::unique_ptr<event_collection_t>& event_collection)  const;
      /// Print summary of event collection object
      void print_summary(const std::unique_ptr<event_collection_t>& event_collection)   const;
    };

    /// Encoder class: Input MEP structures according to EDMS 2100937. Output: event collections.
    /*
     * \author  M.Frank
     * \version 1.0
     * \date    15/09/2019
     */
    class decoder_t  final  {
    public:
      enum { PRINT = 3, ERROR = 5 };
      typedef bool (*bank_selector)(unsigned char);
      typedef bool (*source_selector)(unsigned short);
      typedef std::function<void(int, const char* source, const char* msg)>  logger_t;
      class implementation_t;
      static void _prt(int lvl, const char* source, const char* msg);

    public:
      /// Debug flag
      bool              debug {false};
      implementation_t* implementation {nullptr};
    public:
      /// Default constructor
      decoder_t();
      /// Initializing constructor with printout object
      decoder_t(logger_t& l);
      /// INHIBIT Move constructor
      decoder_t(decoder_t&& copy) = delete;
      /// INHIBIT Copy constructor
      decoder_t(const decoder_t& copy) = delete;
      /// INHIBIT Move assignment
      decoder_t& operator=(decoder_t&& copy) = delete;
      /// INHIBIT Copy assignment
      decoder_t& operator=(const decoder_t& copy) = delete;
      /// Default destructor
      ~decoder_t();

      /// Access last known good run-rumber (if any at all, otherwise 1234)
      static uint32_t last_good_run_number();
      /// Reset last known good run-rumber (default: 1234)
      static void reset_last_good_run_number();

      /// Initialize user data structure
      event_collection_t* initialize(std::unique_ptr<event_collection_t>& event_collection, std::size_t num_events)  const;
      /// Decode MEP fragment
      std::pair<std::size_t, std::size_t>
      decode(std::unique_ptr<event_collection_t>& event_collection,
	     const mep_header_t* frags)  const;
      /// Decode MEP fragment with predicate
      std::pair<std::size_t, std::size_t>
      decode(std::unique_ptr<event_collection_t>& event_collection,
	     const mep_header_t* frags,
	     source_selector source_selector)  const;
    };

  }  // namespace pcie40
}    // namepace Online

#endif // ONLINE_PCIE40DATA_PCIE40DECODER_H
