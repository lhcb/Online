//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//==========================================================================
#ifndef ONLINE_PCIE40DATA_PCIE40_SODIN_H
#define ONLINE_PCIE40DATA_PCIE40_SODIN_H

// C/C++ include files
#include <ctime>
#include <cstdint>

// Framework include files. Note: pack.h must come last!
#include <RTL/Pack.h>

/// Online namespace declaration
namespace Online {

  /// pcie40 namespace declaration
  namespace pcie40  {

    class PACKED_DATA sodin_t   {
    public:
    uint32_t _run_number;
    uint16_t _event_type;
    uint16_t _step_number;
    uint64_t _gps_time;
    uint32_t _tck;
    uint32_t _partition_id;
    unsigned _bunch_id        : 12;
    unsigned _bx_type         :  2;
    unsigned _nzs_mode        :  1;
    unsigned _tae_central     :  1;
    unsigned _tae_entry       :  6;
    unsigned _step_run_enable :  1;
    unsigned _trigger_type    :  4;
    unsigned _tae_first       :  1;
    unsigned _calib_type      :  4;
    uint32_t _orbit_id;
    uint64_t _event_id;

    public:
    sodin_t() = delete;
    sodin_t(sodin_t&& copy) = delete;
	// F. Pisani a copy constructor is needed to make the object trivially copyable
    sodin_t(const sodin_t& copy) = default;
    sodin_t& operator=(sodin_t&& copy) = delete;
	// F. Pisani a copy assignment operator is needed to make the object trivially copyable
    sodin_t& operator=(const sodin_t& copy) = default;

    struct tm* localtime()   const  {
      long timeshift = _gps_time / (1000000000L);
      return ::localtime(&timeshift);
    }
    uint32_t run_number()      const    {      return _run_number;     }
    uint16_t event_type()      const    {      return _event_type;     }
    uint16_t step_number()     const    {      return _step_number;    }
    uint64_t gps_time()        const    {      return _gps_time;       }
    uint32_t tck()             const    {      return _tck;            }
    uint32_t partition_id()    const    {      return _partition_id;   }
    uint16_t bunch_id()        const    {      return _bunch_id;       }
    uint8_t  bx_type()         const    {      return _bx_type;        }
    bool     nzs_mode()        const    {      return _nzs_mode;       }
    bool     tae_central()     const    {      return _tae_central;    }
    bool     tae_first()       const    {      return _tae_first;      }
    uint8_t  tae_window()      const    {      return tae_central() ? 2*_tae_entry - 1 : 0;  }
    uint8_t  tae_half_window() const    {      return tae_central() ? _tae_entry-1 : 0;  }
    uint8_t  calib_type()      const    {      return _calib_type;     }
    uint8_t  trigger_type()    const    {      return _trigger_type;   }
    uint32_t orbit_id()        const    {      return _orbit_id;       }
    uint64_t event_id()        const    {      return _event_id;       }
    bool     is_tae()          const    {      return tae_central() || _tae_entry > 0; }
    };

  }  // namespace pcie40
}    // namepace Online
#include <RTL/Unpack.h>
#endif // ONLINE_PCIE40DATA_PCIE40_SODIN_H
