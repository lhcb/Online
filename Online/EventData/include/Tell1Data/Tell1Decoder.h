//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Author     : Markus Frank
//==========================================================================
#ifndef EVENTDATA_TELL1DECODER_H
#define EVENTDATA_TELL1DECODER_H

/// Framework include files
#include <EventData/event_header_t.h>
#include <EventData/raw_bank_offline_t.h>

/// C/C++ include files
#include <numeric>
#include <cstring>
#include <vector>
#include <map>

///   Online namespace declaration
namespace Online  {

  /// Forward declarations
  class event_header_t;
  class bank_header_t;
  class raw_bank_offline_t;
  typedef std::pair<char*,int> DataDescriptor;

  /// one-at-time hash function
  uint32_t hash32Checksum(const void* ptr, uint64_t len);
  /// Adler32 checksum
  uint32_t adler32Checksum(uint32_t adler, const char* buf, uint64_t len);
  /// Generate  Checksum
  uint32_t genChecksum(int flag, const void* ptr, uint64_t len);

  /// Check sanity of raw bank structure
  bool checkRawBank(const raw_bank_offline_t* b, bool throw_exc=true,bool print_cout = false);

  /// Check consistency of raw bank sequence using magic words
  bool checkRawBanks(const void* start, const void* end, bool throw_exc=true,bool print_cout = false);

  /// Check consistency of MEP multi event fragment
  bool checkRecord(const event_header_t* h, int opt_len, bool exc,bool prt);

  /// Count number of banks in the Tell1 event record
  long numberOfBanks(const event_header_t* h);
  
  /// Determine length of the sequential buffer from RawEvent object
  template <typename T> std::size_t rawEventLengthBanks(const T& banks)    {
    std::size_t len = 0;
    for(const auto* b : banks) { if ( b ) len += b->totalSize();  }
    return len;
  }

  /// Conditional decoding of raw buffer from MDF to vector of raw banks
  int decodeRawBanks(const void* start, const void* end, std::vector<raw_bank_offline_t*>& banks);

  /// Conditional decoding of raw buffer from MDF to bank offsets
  int decodeRawBanks(const void* start, const void* end, int* offsets, int* noffset);

  /// Conditional decoding of raw buffer from MDF to bank offsets. Data are passed as descriptor
  inline int decodeRawBanks(const DataDescriptor& data, int* offsets, int* noffset)    {
    const unsigned char* end = (const unsigned char*)data.first;
    return decodeRawBanks(data.first,end+data.second,offsets,noffset);
  }

  /// Copy RawEvent data from bank vectors to sequential buffer
  int encodeRawBank(const raw_bank_offline_t* b, char* const data, std::size_t size, std::size_t* length);

  /// Copy RawEvent data from bank vectors to sequential buffer
  template <typename T>
    int encodeRawBanks(const T& banks, char* const data, std::size_t size, bool skip_hdr_bank, std::size_t* length)  {
    std::size_t len = 0, s;
    for(const raw_bank_offline_t* b : banks)   {
      if ( skip_hdr_bank )  {
	if ( b->type() == raw_bank_offline_t::DAQ )   {
	  if ( b->version() == DAQ_STATUS_BANK ) {
	    continue;
	  }
	}
      }
      s = b->totalSize();
      if ( size >= (len+s) )  {
	::memcpy(data+len, b, s);
	len += s;
	continue;
      }
      return 0;
    }
    if ( length ) *length = len;
    return 1;
  }

  /// Returns the prefix on TES according to bx number, - is previous, + is next
  std::string rootFromBxOffset(int bxOffset);

  /// Access to the TAE bank (if present)
  raw_bank_offline_t* getTAEBank(const char* start);

  /// Return vector of TAE event names
  std::vector<std::string> buffersTAE(const char* start);

  /// Returns the offset of the TAE with respect to the central bx
  int bxOffsetTAE(const std::string& root);
}
#endif // EVENTDATA_TELL1DECODER_H
