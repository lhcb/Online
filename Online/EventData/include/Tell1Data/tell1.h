//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  Tell1Bank.h
//--------------------------------------------------------------------------
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_DATAFLOW_TELL1_TELL1_H
#define ONLINE_DATAFLOW_TELL1_TELL1_H 1

/// Framework includes
#include <EventData/run3_detector_bits.h>
#include <EventData/bank_header_t.h>
#include <EventData/raw_bank_offline_t.h>

/// C/C++ include files
#include <iostream>
#include <iomanip>

/// Output streamer
inline std::ostream& operator <<(std::ostream& os, const Online::bank_header_t& b)  {
  char text[256];
  ::snprintf(text,sizeof(text),
	     "Tell1Bank: magic: %0x Length: %d Type: %d Version: %d SourceID: %0x",
	     int(b.magic()), int(b.totalSize()), int(b.type()), int(b.version()), int(b.sourceID()));
  os << text;
  return os;
}
#endif ///ONLINE_DATAFLOW_TELL1_TELL1_H
