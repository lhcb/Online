//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  RawFile.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

/// Framework include files
#include <EventData/raw_bank_offline_t.h>
#include <EventData/event_header_t.h>
#include <EventData/RawFile.h>
#include <RTL/posix.h>
#include <RTL/Logger.h>
#include <RTL/rtl.h>

/// C/C++ include files
#include <system_error>
#include <filesystem>
#include <cstring>

#define HEAP_MEMORY_MAPPED (-100)

using namespace Online;
using namespace std;
namespace fs = std::filesystem;

/// Read single event using dynamic allocation
long RawFile::read_event(EventType       expected,
			 EventType&      found_type,
			 Allocator&      allocator,
			 unsigned char** data)   {
  int size_buf[3];
  off64_t file_position = this->seek(0, SEEK_CUR);
  int status = this->read((unsigned char*)size_buf, sizeof(size_buf));
  if ( data ) *data = 0;
  if ( status <= 0 )   {
    this->close();
    return -1;
  }
  int  evt_size = size_buf[0];
  bool is_mdf   = evt_size > 0 && size_buf[0] == size_buf[1] && size_buf[0] == size_buf[2];

  found_type = expected;
  if ( expected == AUTO_INPUT_TYPE )  {
    if ( is_mdf )  {
      ::lib_rtl_output(LIB_RTL_INFO,"RawFile: Input type AUTO: MDF record found. Switch to input mode MDF.");
      found_type = MDF_INPUT_TYPE;
    }
  }
  else if ( expected == MDF_INPUT_TYPE && !is_mdf )   {
    // Corrupted file: Try to fix it!
    long skip = this->scanToNextMDF();
    if ( skip > 0 )  { // Just move on to next record!
      ::lib_rtl_output(LIB_RTL_WARNING,"RawFile: Corrupted file found: %s at position %ld. "
		       "Skip %ld bytes after sweep to next MDF record.",
		       cname(), file_position, skip);
      return this->read_event(expected, found_type, allocator, data);
    }
    ::lib_rtl_output(LIB_RTL_ERROR,"RawFile: Corrupted file found: %s at position %ld. "
		     "Skip rest of file and delete it!",
		     cname(), file_position);
    this->close();
    found_type = MEP_INPUT_TYPE;
    return -1;
  }
  else if ( expected == MEP_INPUT_TYPE && is_mdf )   {
    ::lib_rtl_output(LIB_RTL_ERROR,"RawFile: Corrupted MEP file found: %s at position %ld. "
		     "looks like an MDF file. Event size:%d bytes.",
		     cname(), file_position, evt_size);
    this->seek(file_position, SEEK_SET);
    this->saveRestOfFile();
    this->fd = 0;
    found_type = MDF_INPUT_TYPE;
    return -1;	  // Error? Should I stop?
  }
  int  buf_size = evt_size;
  unsigned char* data_ptr = allocator(buf_size);
  unsigned char* read_ptr = 0;
  std::size_t read_len = 0;
  if ( 0 == data_ptr )   {
    this->seek(file_position, SEEK_SET);
    return -1;
  }
  if ( is_mdf )   {
    read_ptr = data_ptr;
    ::memcpy(read_ptr, size_buf, sizeof(size_buf));
    read_ptr += sizeof(size_buf);
    read_len  = evt_size-sizeof(size_buf);
  }
  else {
    this->seek(file_position, SEEK_SET);
    return -1;
  }
  status = this->read(read_ptr, read_len);
  if ( status <= 0 )   {  // End-of file, continue with next file
    reset();
    return -1;
  }
  if ( data ) *data = data_ptr;
  return buf_size;
}

/// Read single event into fixed buffer
long RawFile::read_event(EventType      expected,
			 EventType&     found_type,
			 unsigned char* data_ptr,
			 std::size_t    len)
{
  unsigned char* data;
  InPlaceAllocator alloc(data_ptr, len);
  return this->read_event(expected, found_type, alloc, &data);
}

/// Read multiple events into fixed buffer (burst mode)
std::pair<long,long>
RawFile::read_multiple_events(std::size_t    num_events,
			      EventType      expected,
			      EventType&     found_type,
			      unsigned char* p,
			      std::size_t    len)
{
  if ( p && len > 0 )   {
    std::size_t len_left  = len;
    std::size_t len_frame = 0;
    long        num_evts  = 0;
    unsigned char*  ptr = p;
    EventType found;
    off64_t file_position = this->seek(0, SEEK_CUR);
    for (std::size_t i=0; i<num_events && ptr < p+len; ++i)   {
      // Type is tricky here: auto-switch to concrete mode if AUTO is enabled
      long evt_len = this->read_event(expected, found, ptr, len_left);
      if ( expected != AUTO_INPUT_TYPE && found != expected )  {
	if ( this->fd > 0 ) this->seek(file_position, SEEK_SET);
	found_type = found;
	break;
      }
      else if ( expected != AUTO_INPUT_TYPE && found != expected )  {
	this->seek(file_position, SEEK_SET);
	found_type = found;
	break;
      }
      else if ( evt_len > 0 )   {
	file_position = this->seek(0, SEEK_CUR);
	found_type = found;
	expected   = found;
	len_left  -= evt_len;
	ptr       += evt_len;
	len_frame += evt_len;
	++num_evts;
	continue;
      }
      this->seek(file_position, SEEK_SET);
      break;
    }
    return make_pair(num_evts,long(len_frame));
  }
  return make_pair(-1,-1);
}

/// Search bad file and try to seek the next event record. Returns offset to next event
long RawFile::scanToNextMDF()    {
  unsigned char   buff[128], *bend = buff+sizeof(buff);
  off64_t         file_position = this->seek(0, SEEK_CUR);
  off64_t         hdr_len = event_header_t::sizeOf(3); // Header version 3!
  std::size_t     num_file_byte = 0, read_len = sizeof(buff)-hdr_len;
  unsigned char*  qstart = buff+hdr_len;
  int status = 1, count = 0, _debug = 0;

  ::memset(buff,0,sizeof(buff));
  while ( status>0 )  {
    status = this->read(qstart, read_len);
    if ( status > 0 )  {
      num_file_byte += read_len;
      for( unsigned char* p = qstart; p<bend; ++p, ++count )  {
	raw_bank_offline_t* bank = (raw_bank_offline_t*)p;
	if ( bank->magic() == raw_bank_offline_t::MagicPattern )   {
	  // Possible start of a new bank. See if MDF header preceeds it.
	  event_header_t* hdr  = (event_header_t*)(p-hdr_len);
	  if ( _debug )  {
	    //cout << "Bank[" << count << "]: " << EventPrintout::bankHeader(bank) << endl;
	  }
	  if ( hdr->headerVersion()>0 && hdr->headerVersion()<5 )  {
	    std::size_t siz = hdr->size0();
	    if ( _debug )  {
	      cout << "EventHeader:[" << count << ", " << long(p-qstart) << "] Size:"
		   << siz << " " << hdr->size1() << " " << hdr->size2()
		   << " Vsn:" << hdr->headerVersion() << endl;
	    }
	    if ( hdr->subheaderLength() < 64 )  {
	      if ( siz>0 && siz == hdr->size1() && siz == hdr->size2() )  {
		int off = (((unsigned char*)hdr)-qstart)-read_len;
		int skip = num_file_byte+off;
		// Set the file descriptor back to the beginning of the MDF header. Job done.
		seek(file_position+skip, SEEK_SET);
		cout << "[WARNING] Got it: EventHeader:[" << count << ", " << skip << "] Size:"
		     << hdr->size0() << " " << hdr->size1() << " " << hdr->size2()
		     << " Vsn:" << hdr->headerVersion() << endl << flush;
		return skip;
	      }
	    }
	  }
	  // Otherwise continue sweeping over the record to the next bank!
	  // At some point there MUST be a bank preceeded by a EventHeader
	  // or the file is at end!
	}
      }
      // Copy the last event_header_t::sizeOf(3) bytes to the beginning of the buffer
      // since it might contain the next MDF header data. Then restart the scan
      ::memcpy(buff,bend-hdr_len,hdr_len);
    }
    else  {  // Nothing can be read anymore: EOF
      this->seek(0, SEEK_END); // Position to end of file
      // This will make the next read fail!
      // +1: Just in case already the first read failed!
      //     The returned number is anyhow for diagnostics only
      return num_file_byte > 0 ? num_file_byte : 1;
    }
  }
  return 0;
}

/// Save remainder of currently read file
void RawFile::saveRestOfFile()   {
  RawFile out(this->path);
  ::lib_rtl_output(LIB_RTL_INFO,"RawFile: Saving rest of file[%d]: %s", this->fd, cname());
  if ( out.openWrite() == -1 )    {
    auto err = RTL::errorString();
    ::lib_rtl_output(LIB_RTL_ERROR,"RawFile: CANNOT Create file: %s: [%s]",
		     cname(), !err.empty() ? err.c_str() : "????????");
    return;
  }
  std::size_t cnt = 0;
  unsigned char buffer[10 * 1024];
  for (int ret; (ret = this->read(buffer, sizeof(buffer))) > 0; )  {
    if ( !out.write(buffer, ret) )      {
      auto err = RTL::errorString();
      ::lib_rtl_output(LIB_RTL_ERROR,"RawFile: CANNOT write file: %s: [%s]",
		       cname(), !err.empty() ? err.c_str() : "????????");
    }
    cnt += ret;
  }
  ::lib_rtl_output(LIB_RTL_INFO,"RawFile: Wrote %ld bytes to file:%s fd:%d",
		   cnt, out.cname(), out.fileno());
  out.close();
  ::close(this->fd);
  this->fd = -1;
}

/// Scan single record and check for consistency
int RawFile::checkRecord(std::size_t offset, const void* data, std::size_t length)  const    {
  if ( data && length )   {
    const char* ptr  = (const char*)data;
    bool bad_record  = this->checkEventHeader(offset, data, length) != 1;
    const event_header_t* hdr = (event_header_t*)ptr;
    const char* start = ptr + event_header_t::sizeOf(hdr->headerVersion());
    const char* end   = ptr + hdr->size();
    for( ; start < end; )  {
      const raw_bank_offline_t* b = (const raw_bank_offline_t*)start;
      bad_record = bad_record || (this->checkBank(start-ptr,b) == 1);
      if ( b->size() > 0 )   {
	start += b->size();
	continue;
      }
      break;
    }
    return bad_record ? 0 : 1;
  }
  return 0;
}

/// Scan single record and check for consistency
int RawFile::checkEventHeader(std::size_t offset, const void* data, std::size_t length)  const    {
  if ( data && length )   {
    bool bad_record = false;
    const char* ptr  = (const char*)data;
    const event_header_t* hdr = (const event_header_t*)ptr;

    if ( hdr->size0() != length )   {    /// Check the MDF header length(0)
      ::lib_rtl_output(LIB_RTL_ERROR,"RawFile: File:%s Offset:%ld Event has inconsistent size(0): [%d %d %d]",
	       cname(), offset, hdr->size0(), hdr->size1(), hdr->size2());
      bad_record = true;
    }
    if ( hdr->size1() != length )   {    /// Check the MDF header length(1)
      ::lib_rtl_output(LIB_RTL_ERROR,"RawFile: File:%s Offset:%ld Event has inconsistent size(1): [%d %d %d]",
	       cname(), offset, hdr->size0(), hdr->size1(), hdr->size2());
      bad_record = true;
    }
    if ( hdr->size1() != length )   {    /// Check the MDF header length(2)
      ::lib_rtl_output(LIB_RTL_ERROR,"RawFile: File:%s Offset:%ld Event has inconsistent size(1): [%d %d %d]",
	       cname(), offset, hdr->size0(), hdr->size1(), hdr->size2());
      bad_record = true;
    }
    if ( hdr->headerVersion() != 3 )   {
      ::lib_rtl_output(LIB_RTL_ERROR,"RawFile: File:%s Offset:%ld Event header has bad header version:%d [!=3]",
	       cname(), offset, hdr->headerVersion());
      bad_record = true;
    }
    return bad_record ? 0 : 1;
  }
  return 0;
}

/// Check content of bank
int RawFile::checkBank(std::size_t offset, const void* data)  const  {
  const raw_bank_offline_t* b = (const raw_bank_offline_t*)data;
  using event_print::bankHeader;

  // Check bank's magic word
  if ( b->magic() == raw_bank_offline_t::MagicPattern )  {
    // Crude check on bank type
    if ( b->type() < bank_types_t::LastType )  {
      // Crude check on bank length
      if ( b->size() >= 0 )  {
	// Now check source ID range:
	//// TBD !!
	return 1;
      }
      ::lib_rtl_output(LIB_RTL_ERROR,"RawFile: %s Offset:%ld Bad bank length found in Tell1 bank %p: %s",
	       cname(), offset, data, bankHeader(b).c_str());
      return 0;
    }
    ::lib_rtl_output(LIB_RTL_ERROR,"RawFile: %s Offset:%ld Unknown Bank type in Tell1 bank %p: %s",
	     cname(), offset, data, bankHeader(b).c_str());
    return 0;
  }
  ::lib_rtl_output(LIB_RTL_ERROR,"RawFile: %s Offset:%ld Bad magic pattern in Tell1 bank %p: %s",
	   cname(), offset, data, bankHeader(b).c_str());
  return 0;
}

/// Dump a set of raw banks
void RawFile::dump(const unsigned char* data, std::size_t len)   const  {
  const unsigned char* ptr = data;
  const unsigned char* end = data + len;
  while( ptr < end )   {
    const raw_bank_offline_t* bank = (raw_bank_offline_t*)ptr;
    if ( checkBank(ptr-data, ptr) )    {
      dump(bank);
      ptr += bank->totalSize();
      continue;
    }
    break;
  }
}

/// Dump the content of a single raw bank to screen
void RawFile::dump (const raw_bank_offline_t* bank)   const  {
  const raw_bank_offline_t* r = bank;
  int cnt, dump_flags = 3;
  bool ful = dump_flags&1;
  bool dmp = dump_flags&1;
  stringstream info;

  if ( dmp ) {
    info << "Bank:  [" << event_print::bankHeader(r) << "] " << endl;
  }
  if( ful ) {
    cnt = 0;
    const int* p;
    std::stringstream s;
    for(p=r->begin<int>(); p != r->end<int>(); ++p)  {
      s << std::hex << std::setw(8) << std::hex << *p << " ";
      if ( ++cnt == 10 ) {
	info << "  Data:" << s.str() << endl;
	s.str("");
	cnt = 0;
      }
    }
    if ( cnt > 0 ) info << "  Data:" << s.str() << endl;
  }
  cout << info.str();
}

extern "C" int mdf_file_read(int argc, char** argv)   {
  struct malloc_allocator final : public RawFile::Allocator {
    unsigned char* operator()(std::size_t memsize) override {
      return (unsigned char*)::malloc(memsize);
    }
  } allocator;
  int packing = 1;
  int memsize = 0;
  string input;
  RTL::CLI cli( argc, argv, []{} );
  cli.getopt("input", 3, input);
  cli.getopt("packing", 3, packing);
  cli.getopt("allocate", 3, memsize);
  RTL::Logger::install_rtl_printer(LIB_RTL_DEBUG);
  unsigned char* memptr = 0;
  if ( memsize > 0 ) memptr = (unsigned char*)::operator new(memsize);
  RawFile mdf(input);
  if ( -1 == mdf.open() )   {
    return -1;
  }
  for(long num_evt = 0, num_read = 0;;)   {
    const unsigned char* data = 0;
    RawFile::EventType   found_type;
    pair<long,long>      ret;
    ret = mdf.read_multiple_events(packing,
				   RawFile::MDF_INPUT_TYPE,
				   found_type,
				   memptr,
				   memsize);
    data = memptr;
    ++num_read;
    ::lib_rtl_output(LIB_RTL_INFO,"ReadMDF: ++ =========================================================== ++");
    ::lib_rtl_output(LIB_RTL_INFO,"ReadMDF: ++ Read %4ld events with %7ld bytes total.", ret.first, ret.second);
    const unsigned char* start = data;
    for(int i=0; i < ret.first; ++i)   {
      const auto* header = (const event_header_t*)start;
      std::size_t length = header->size0();
      std::size_t hdrlen = header->sizeOf(header->headerVersion());
      ::lib_rtl_output(LIB_RTL_INFO,"ReadMDF: ++ =========================================================== ++");
      ::lib_rtl_output(LIB_RTL_INFO,"ReadMDF: ++ Bank dump of subevent %3d Run:%d Orbit:%d bunchID:%d", i,
	       header->subHeader().H1->runNumber(),
	       header->subHeader().H1->orbitNumber(),
	       header->subHeader().H1->bunchID());
      ::lib_rtl_output(LIB_RTL_INFO,"ReadMDF: ++ =========================================================== ++");
      mdf.dump(start+hdrlen, length-hdrlen);
      start += length;
      ++num_evt;
    }
    ::lib_rtl_output(LIB_RTL_INFO,"ReadMDF: ++ =========================================================== ++");
    if ( ret.first < packing )   {
      ::lib_rtl_output(LIB_RTL_INFO,"ReadMDF: ++ Finished input source after %ld events and %ld read actions.",
	       num_evt, num_read);
      ::lib_rtl_output(LIB_RTL_INFO,"ReadMDF: ++ =========================================================== ++");
      break;
    }
  }
  ::operator delete(memptr);
  return 0;
}
