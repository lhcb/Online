/*****************************************************************************\
 * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include <Tell1Data/Tell1Decoder.h>
#include <EventData/run3_detector_bits.h>
#include <EventData/raw_bank_offline_t.h>
#include <EventData/raw_bank_online_t.h>
#include <EventData/odin_t.h>
#include <RTL/hash.h>

/// C/C++ include files
#include <iostream>
#include <iomanip>
#include <sstream>
#include <netinet/in.h>

using Online::raw_bank_offline_t;
static const char* s_checkLabel = "BankCheck    ERROR  ";

/* ========================================================================= */

static uint32_t xorChecksum(const int* ptr, uint64_t len)  {
  uint32_t checksum = 0;
  len = len/sizeof(int) + (len%sizeof(int) ? 1 : 0);
  for(const int *p=ptr, *end=p+len; p<end; ++p)  {
    checksum ^= *p;
  }
  return checksum;
}

/// Generate  Checksum
uint32_t Online::genChecksum(int flag, const void* ptr, uint64_t len)  {
  switch(flag)  {
  case 0:
    return 0;
  case 1:
    return RTL::hash32(ptr, len);
  case 2:
    len = (len/sizeof(int))*sizeof(int);
    return RTL::crc32(ptr, len);
  case 3:
    len = (len/sizeof(short))*sizeof(short);
    return RTL::crc16(ptr, len);
  case 4:
    return RTL::crc8(ptr, len);
  case 5:
    len = (len/sizeof(int))*sizeof(int);
    return RTL::adler32(1, ptr, len);
  case 6:
    return xorChecksum((const int*)ptr, len);
  case 22:  // Old CRC32 (fixed by now)
    return RTL::crc32(ptr, len);
  default:
    return ~0x0;
  }
}

static void print_previous_bank(const raw_bank_offline_t* prev) {
  char txt[255];
  if( prev == 0 )
    ::snprintf(txt,sizeof(txt),"%s Bad bank is the first bank in the MEP fragment.",s_checkLabel);
  else
    ::snprintf(txt,sizeof(txt),"%s Previous (good) bank [%p]: %s",s_checkLabel,
	       (void*)prev,Online::event_print::bankHeader(prev).c_str());
  std::cout << txt << std::endl;
}

/// Check sanity of raw bank structure
bool Online::checkRawBank(const raw_bank_offline_t* b, bool throw_exc, bool print_cout)  {
  using event_print::bankHeader;
  // Check bank's magic word: Either Tell1 magic word or PCIE40 magic word
  if( b->magic() == raw_bank_offline_t::MagicPattern || b->magic() == 0xFACE )  {
    // Crude check on the bank type
    if( b->type() < raw_bank_offline_t::LastType )  {
      // Crude check on the bank length
      if( b->size() >= 0 )  // Zero bank length is apparently legal....
	{
	  // Now check source ID range:
	  //// TBD !!
	  return true;
	}
      char txt2[255];
      ::snprintf(txt2, sizeof(txt2), "%s Bad bank length found in Tell1 bank %p: %s", 
		 s_checkLabel, reinterpret_cast<const void*>(b), bankHeader(b).c_str());
      if( print_cout ) std::cout << txt2 << std::endl;
      if( throw_exc  ) throw std::runtime_error(txt2);
      return false;
    }
    char txt1[255];
    ::snprintf(txt1, sizeof(txt1), "%s Unknown Bank type in Tell1 bank %p: %s", 
	       s_checkLabel, reinterpret_cast<const void*>(b), bankHeader(b).c_str());
    if( print_cout ) std::cout << txt1 << std::endl;
    if( throw_exc  ) throw std::runtime_error(txt1);
    return false;
  }
  // Error: Bad magic pattern; needs handling
  char txt0[255];
  ::snprintf(txt0, sizeof(txt0), "%s Bad magic pattern in Tell1 bank %p: %s",
	     s_checkLabel, reinterpret_cast<const void*>(b), bankHeader(b).c_str());
  if( print_cout ) std::cout << txt0 << std::endl;
  if( throw_exc  ) throw std::runtime_error(txt0);
  return false;
}

/// Check consistency of MEP fragment using magic bank patterns.
bool Online::checkRawBanks(const void* start, const void* end, bool exc, bool prt)  {
  char txt[255];
  raw_bank_offline_t* prev = 0;
  if( end >= start )  {
    for(raw_bank_offline_t* b=(raw_bank_offline_t*)start, *e=(raw_bank_offline_t*)end; b < e; )  {
      if( !checkRawBank(b,false,true) ) goto Error;  // Check bank sanity
      b = (raw_bank_offline_t*)(((const char*)b) + b->totalSize());
      prev = b;
    }
    return true;
  }
 Error:  // Anyhow only end up here if no exception was thrown...
  ::snprintf(txt, sizeof(txt), "%s Error in multi raw bank buffer start:%p end:%p",
	     s_checkLabel,reinterpret_cast<const void*>(start),reinterpret_cast<const void*>(end));
  if( prt ) {
    std::cout << txt << std::endl;
    print_previous_bank(prev);
  }
  if( exc ) throw std::runtime_error(txt);
  return false;
}

/// Check consistency of MEP multi event fragment
bool Online::checkRecord(const event_header_t* h, int opt_len, bool exc, bool prt)    {
  if( h )  {
    int  compress;
    char txt[255];
    if( h->size0() != h->size1() || h->size0() != h->size2() )  {
      ::snprintf(txt,sizeof(txt),"%s Inconsistent MDF header size: %u <-> %u <-> %u at %p",
		 s_checkLabel,h->size0(),h->size1(),h->size2(),reinterpret_cast<const void*>(h));
      goto Error;
    }
    if( opt_len != ~0x0 && std::size_t(opt_len) != h->size0() )  {
      ::snprintf(txt,sizeof(txt),"%s Wrong MDF header size: %u <-> %d at %p",
		 s_checkLabel,h->size0(),opt_len,reinterpret_cast<const void*>(h));
      goto Error;
    }
    compress  = h->compression()&0xF;
    if( compress )  {
      // No uncompressing here! Assume everything is OK.
      return true;
    }
    if( !checkRawBanks( h->data<char>(), h->end<char>(), exc, prt ) )  {
      ::snprintf(txt,sizeof(txt),"%s Error in multi raw bank buffer start:%p end:%p",
		 s_checkLabel, h->data<void>(), h->end<void>());
      goto Error;
    }
    return true;

  Error:  // Anyhow only end up here if no exception was thrown...
    if( prt ) std::cout << txt << std::endl;
    if( exc ) throw std::runtime_error(txt);
  }
  return false;
}

/// Count number of banks in the Tell1 event record
long Online::numberOfBanks(const event_header_t* h)    {
  if( h )  {
    uint8_t* start = ((uint8_t*)h) + sizeof(event_header_t) + h->subheaderLength();
    uint8_t* end   = ((uint8_t*)h) + h->size0();
    long count = 0;
    if( end >= start )  {
      for(raw_bank_offline_t* b=(raw_bank_offline_t*)start, *e=(raw_bank_offline_t*)end; b < e; )  {
	b = (raw_bank_offline_t*)(((const uint8_t*)b) + b->totalSize());
	++count;
      }
    }
    return count;
  }
  return -1;
}

/// Conditional decoding of raw buffer from MDF to vector of raw banks
int Online::decodeRawBanks(const void* start, const void* end, std::vector<raw_bank_offline_t*>& banks) {
  const uint8_t* s = (const uint8_t*)start;
  raw_bank_offline_t *prev = 0, *bank = (raw_bank_offline_t*)s;
  try {
    while (s < end)  {
      bank = (raw_bank_offline_t*)s;
      checkRawBank(bank,true,true);  // Check bank sanity
      banks.push_back(bank);
      s += bank->totalSize();
      prev = bank;
    }
    return 1;
  }
  catch(const std::exception& e) {
    print_previous_bank(prev);
  }
  catch(...) {
    print_previous_bank(prev);
  }
  throw std::runtime_error("Error decoding raw banks!");
}

/// Copy RawEvent data from bank vectors to sequential buffer
int Online::encodeRawBank(const raw_bank_offline_t* b, char* const data, std::size_t size, std::size_t* length)   {
  std::size_t s = b->totalSize();
  if( size >= s )  {
    ::memcpy(data, b, s);
    if( length ) *length = s;
    return 1;
  }
  return 0;
}

/// Returns the prefix on TES according to bx number, - is previous, + is next
std::string Online::rootFromBxOffset(int bxOffset) {
  if( 0 == bxOffset )
    return "/Event";
  if( 0 < bxOffset )
    return std::string("/Event/Next") + char('0'+bxOffset);
  return std::string("/Event/Prev") + char('0'-bxOffset);
}

/// Access to the TAE bank (if present)
raw_bank_offline_t* Online::getTAEBank(const char* start) {
  raw_bank_offline_t* b = (raw_bank_offline_t*)start;            // Get the first bank in the buffer
  if( b->type() == raw_bank_offline_t::TAEHeader ) {   // Is it the TAE bank?
    return b;
  }
  if( b->type() == raw_bank_offline_t::DAQ ) {         // Is it the TAE bank?
    start += b->totalSize();                   //
    b = (raw_bank_offline_t*)start;                     // If the first bank is a MDF (DAQ) bank,
  }                                            // then the second bank must be the TAE header
  if( b->type() == raw_bank_offline_t::TAEHeader ) {   // Is it the TAE bank?
    return b;
  }
  return nullptr;
}

/// Returns the offset of the TAE with respect to the central bx
int Online::bxOffsetTAE(const std::string& root) {
  std::size_t idx = std::string::npos;
  if( (idx=root.find("/Prev")) != std::string::npos )
    return -(root[idx+5]-'0');
  if( (idx=root.find("/Next")) != std::string::npos )
    return root[idx+5]-'0';
  return 0;
}

/// Return vector of TAE event names
std::vector<std::string> Online::buffersTAE(const char* start) {
  std::vector<std::string> result;
  raw_bank_offline_t* b = getTAEBank(start);
  if( b && b->type() == raw_bank_offline_t::TAEHeader ) {   // Is it the TAE bank?
    int nBlocks = b->size()/sizeof(int)/3;          // The TAE bank is a vector of triplets
    const int* block  = b->begin<int>();
    for(int nbl = 0; nBlocks > nbl; ++nbl, block +=3)
      result.push_back(rootFromBxOffset(*block));
  }
  return result;
}

/// Set the detector ID
void Online::bank_header_manip::setDetectorID(bank_header_t* header, uint32_t detector_id)   {
  uint16_t det_id  = uint16_t(detector_id&0xF) << 11;
  header->setSourceID(det_id + (0xFFFF & header->sourceID()));
}

/// Retrieve detector name from the source ID / detector ID
std::string Online::event_print::detectorName(uint32_t detector_id)  {
  if( detector_id > 16 )  {
    unsigned short the_det_id  = (detector_id&0xFFFF) >> 11;
    return detectorName(the_det_id);
  }
  switch(detector_id)    {
  case run3::RO_BIT_ODIN:    return "ODIN";
  case run3::RO_BIT_VELOA:   return "VELOA";
  case run3::RO_BIT_VELOC:   return "VELOC";
  case run3::RO_BIT_UTA:     return "UTA";
  case run3::RO_BIT_UTC:     return "UTC";
  case run3::RO_BIT_SFA:     return "SFA";
  case run3::RO_BIT_SFC:     return "SFC";
  case run3::RO_BIT_RICH1:   return "RICH1";
  case run3::RO_BIT_RICH2:   return "RICH2";
  case run3::RO_BIT_PLUME:   return "PLUME";
  case run3::RO_BIT_ECAL:    return "ECAL";
  case run3::RO_BIT_HCAL:    return "HCAL";
  case run3::RO_BIT_MUONA:   return "MUONA";
  case run3::RO_BIT_MUONC:   return "MUONC";
  case run3::RO_BIT_TDET:    return "TDET";
  default:  return "UNKNOWN";
  }
}

/// Retrieve detector name from the raw bank
std::string Online::event_print::detectorName(const bank_header_t* r)  {
  return r ? detectorName(r->detectorID()) : std::string("UNKNOWN");
}

/// Retrieve description of the raw bank hader
std::string Online::event_print::bankHeader(const bank_header_t* r)  {
  std::stringstream s; 
  unsigned short det_id  = r->detectorID();
  unsigned short channel = r->channelID();
  s << "Type:"      << std::setw(7) << bankType(r->type())
    << " / "        << std::setw(3) << int(r->type())
    << " Size:"     << std::setw(5) << int(r->size()) 
    << " Source:"   << std::setw(5) << int(r->sourceID())
    << " ["   << std::setw(2) << int(det_id) 
    << std::setw(7) << ("("+detectorName(det_id)+"),")
    << std::setw(4) << std::right << channel << "]"
    << " Vsn:"      << std::setw(2) << int(r->version()) 
    << " Magic: 0x" << std::setw(4) << std::setfill('0') << std::hex << r->magic();
  return s.str();
}

/// Retrieve bank type name from the raw bank 
std::string Online::event_print::bankType(const bank_header_t* r)  {
  if( r ) return bankType(r->type());
  return "BAD_BANK";
}

/// Retrieve bank type name from the raw bank type
std::string Online::event_print::bankType(int i)  {
#define PRINT(x)  case bank_types_t::x : return #x;
  switch(i)  {
    PRINT(L0Calo);                      //  0
    PRINT(L0DU);                        //  1
    PRINT(PrsE);                        //  2
    PRINT(EcalE);                       //  3
    PRINT(HcalE);                       //  4
    PRINT(PrsTrig);                     //  5
    PRINT(EcalTrig);                    //  6
    PRINT(HcalTrig);                    //  7
    PRINT(Velo);                        //  8
    PRINT(Rich);                        //  9
    PRINT(TT);                          // 10
    PRINT(IT);                          // 11
    PRINT(OT);                          // 12
    PRINT(Muon);                        // 13
    PRINT(L0PU);                        // 14
    PRINT(DAQ);                         // 15
    PRINT(ODIN);                        // 16
    PRINT(HltDecReports);               // 17
    PRINT(VeloFull);                    // 18
    PRINT(TTFull);                      // 19
    PRINT(ITFull);                      // 20
    PRINT(EcalPacked);                  // 21
    PRINT(HcalPacked);                  // 22
    PRINT(PrsPacked);                   // 23
    PRINT(L0Muon);                      // 24
    PRINT(ITError);                     // 25
    PRINT(TTError);                     // 26
    PRINT(ITPedestal);                  // 27
    PRINT(TTPedestal);                  // 28
    PRINT(VeloError);                   // 29
    PRINT(VeloPedestal);                // 30
    PRINT(VeloProcFull);                // 31
    PRINT(OTRaw);                       // 32
    PRINT(OTError);                     // 33
    PRINT(EcalPackedError);             // 34
    PRINT(HcalPackedError);             // 35  
    PRINT(PrsPackedError);              // 36
    PRINT(L0CaloFull);                  // 37
    PRINT(L0CaloError);                 // 38
    PRINT(L0MuonCtrlAll);               // 39
    PRINT(L0MuonProcCand);              // 40
    PRINT(L0MuonProcData);              // 41
    PRINT(L0MuonRaw);                   // 42
    PRINT(L0MuonError);                 // 43
    PRINT(GaudiSerialize);              // 44
    PRINT(GaudiHeader);                 // 45
    PRINT(TTProcFull);                  // 46
    PRINT(ITProcFull);                  // 47
    PRINT(TAEHeader);                   // 48
    PRINT(MuonFull);                    // 49
    PRINT(MuonError);                   // 50
    PRINT(TestDet);                     // 51
    PRINT(L0DUError);                   // 52
    PRINT(HltRoutingBits);              // 53
    PRINT(HltSelReports);               // 54
    PRINT(HltVertexReports);            // 55
    PRINT(HltLumiSummary);              // 56
    PRINT(L0PUFull);                    // 57
    PRINT(L0PUError);                   // 58
    PRINT(DstBank);                     // 59
    PRINT(DstData);                     // 60
    PRINT(DstAddress);                  // 61
    PRINT(FileID);                      // 62
    PRINT(VP);                          // 63   
    PRINT(FTCluster);                   // 64
    PRINT(VL);                          // 65
    PRINT(UT);                          // 66
    PRINT(UTFull);                      // 67
    PRINT(UTError);                     // 68
    PRINT(UTPedestal);                  // 69
    PRINT(HC);                          // 70
    PRINT(HltTrackReports);             // 71
    PRINT(HCError);                     // 72
    PRINT(VPRetinaCluster);             // 73
    PRINT(FTGeneric);                   // 74
    PRINT(FTCalibration);               // 75
    PRINT(FTNZS);                       // 76
    PRINT(Calo);                        // 77
    PRINT(CaloError);                   // 78
    PRINT(MuonSpecial);                 // 79
    PRINT(RichCommissioning);           // 80
    PRINT(RichError);                   // 81
    PRINT(FTSpecial);                   // 82
    PRINT(CaloSpecial);                 // 83
    PRINT(Plume);                       // 84
    PRINT(PlumeSpecial);                // 85
    PRINT(PlumeError);                  // 86
    PRINT(VeloThresholdScan);           // 87
    PRINT(FTError);                     // 88
    /// DAQ errors:
    PRINT(DaqErrorFragmentThrottled);   // 89
    PRINT(DaqErrorBXIDCorrupted);       // 90 
    PRINT(DaqErrorSyncBXIDCorrupted);   // 91
    PRINT(DaqErrorFragmentMissing);     // 92
    PRINT(DaqErrorFragmentTruncated);   // 93
    PRINT(DaqErrorIdleBXIDCorrupted);   // 94
    PRINT(DaqErrorFragmentMalformed);   // 95
    PRINT(DaqErrorEVIDJumped);          // 96
    PRINT(VeloSPPandCluster);           // 97
    PRINT(UTNZS);                       // 98
    PRINT(UTSpecial);                   // 99
    PRINT(DaqErrorAlignFifoFull);       // 100
    PRINT(DaqErrorFEfragSizeWrong);     // 101

  default:
    return "UNKNOWN";
#undef PRINT
  }
}

///
static std::string bx_offset(int32_t bx)  {
  std::string offset;
  int32_t cr = std::abs(bx);
  if( cr > 99 ) offset += char('0'+((cr/100)%100));
  if( cr > 9  ) offset += char('0'+((cr/10)%10));
  offset += char('0'+(cr%10));
  return offset;
}

/// Retrieve dump of bank data in line format
std::vector<std::string> Online::event_print::bankData( const void* data, std::size_t len, std::size_t words_per_line)  {
  std::size_t cnt = 0, num_words = (len+sizeof(int)-1)/sizeof(int);
  std::stringstream s;
  std::vector<std::string> lines;
  for( const int *p=(const int*)data, *e=p + num_words; p < e; ++p )  {
    s << std::hex << std::setw(8) << std::setfill('0') << std::hex << *p << " ";
    if( ++cnt == words_per_line ) {
      lines.emplace_back(s.str());
      s.str("");
      cnt = 0;
    }
  }
  if( cnt > 0 ) lines.emplace_back(s.str());
  return lines;
}

template <Online::bank_types_t::BankType i> std::vector<std::string>
Online::event_print::bank_data(const void* bank,
			       std::size_t len,
			       std::size_t words_per_line)
{
  return event_print::bankData(bank, len, words_per_line);
}

template <> std::vector<std::string>
Online::event_print::bank_data<Online::bank_types_t::ODIN>(const void* bank,
							   std::size_t /* len */,
							   std::size_t /* words_per_line */)
{
  auto* s = (Online::run3_odin_t*)bank;
  std::vector<std::string> lines;
  char text[512];
  ::snprintf(text, sizeof(text), "Run:%7d EID:%16ld Orbit:%8d Bunch:%5d Step:%4d",
	     s->run_number(), s->event_id(), s->orbit_id(), s->bunch_id(), s->step_number());
  lines.push_back(text);

  ::snprintf(text, sizeof(text), "Time:%16lX TCK:%08X NZS:%s TAE-frst:%s TAE-central:%s TAE-win:%3d",
	     s->gps_time(), s->tck(),
	     s->nzs_mode()    ? "YES" : "NO ",
	     s->tae_first()   ? "YES" : "NO ",
	     s->tae_central() ? "YES" : "NO ",
	     s->tae_window());
  lines.push_back(text);

  ::snprintf(text, sizeof(text), "Partition:%08X e_typ:%04X, bx_typ:%04X cal_typ:%04X trg_typ:%04X",
	     s->partition_id(), s->event_type(), s->bx_type(), s->calib_type(), s->trigger_type());
  lines.push_back(text);
  return lines;
}

/// Retrieve dump of bank data in line format
std::vector<std::string>
Online::event_print::bankData(const bank_header_t* header, const void* bank, std::size_t words_per_line)  {
  if( header->type() == bank_types_t::ODIN && header->version() == 7 )  {
    return bank_data<bank_types_t::ODIN>(bank, header->size(), words_per_line);
  }
  return event_print::bankData(bank, header->size(), words_per_line);
}

/// Retrieve dump of bank data in line format
std::vector<std::string> Online::event_print::bankData( const raw_bank_offline_t* bank, std::size_t words_per_line )  {
  return bankData(bank, bank->data(), words_per_line);
}

/// Retrieve dump of bank data in line format
std::vector<std::string> Online::event_print::bankData( const raw_bank_online_t* bank, std::size_t words_per_line )  {
  return bankData(bank, bank->data(), words_per_line);
}

/// Retrieve dump of bank data in line format
std::vector<std::string> Online::event_print::taeData( const bank_header_t* tae, const void* bank )  {
  char text[256];
  std::vector<std::string> lines;
  if( tae->type() == bank_types_t::TAEHeader )  {
    const int*  data      = (const int*)bank;
    int32_t     count_bx  = tae->size()/sizeof(int)/3;
    int32_t     half_win  = (count_bx-1)/2;
    std::string prev = "/Prev", next = "/Next";
    ::snprintf(text, sizeof(text), "TAE: Half window: %d", half_win);
    lines.emplace_back(text);
    for(int32_t i=-half_win; i<=half_win; ++i)   {
      int32_t idx = i + half_win;
      int32_t bx  = data[3*idx];
      int32_t off = data[3*idx+1];
      int32_t len = data[3*idx+2];
      std::string item = bx == 0 ? "/Central" : bx<0 ? prev + bx_offset(bx) : next + bx_offset(-bx);
      ::snprintf(text, sizeof(text),
		 "     fragment: %-16s  id: %2d bx: %2d offset:%8d bytes, length:%d bytes",
		 item.c_str(), idx, bx, off, len);
      lines.emplace_back(text);
    }
  }
  return lines;
}

/// Retrieve dump of bank data in line format
std::vector<std::string> Online::event_print::taeData( const raw_bank_offline_t* bank )  {
  return taeData(bank, bank->data());
}

/// Format MDF header data to a set of lines for printing
std::vector<std::string> Online::event_print::headerData( const event_header_t* hdr )  {
  std::vector<std::string> lines;
  std::stringstream str;
  char text[256];

  ::snprintf(text, sizeof(text), "+----> TELL1  header: @ %p", (void*)hdr);
  lines.emplace_back(text);
  ::snprintf(text, sizeof(text),"| Size: %5d %5d %5d  Checksum: 0x%08X compression: %3d Vsn: %3d",
	     hdr->size0(), hdr->size1(), hdr->size2(), hdr->checkSum(), hdr->compression(), hdr->headerVersion());
  lines.emplace_back(text);
  if( hdr->headerVersion() == 3 )   {
    const auto* h = hdr->subHeader().H1;
    const auto& m = h->triggerMask();
    ::snprintf(text, sizeof(text),"| Run: %6d  Orbit: 0x%08X Bunch: 0x%08X Mask: %08X %08X %08X %08X",
	       h->runNumber(), h->orbitNumber(), h->bunchID(), m[0], m[1], m[2], m[3]);
    lines.emplace_back(text);
  }
  return lines;
}

/// Produce data lines with ODIN bank content
std::vector<std::string> Online::event_print::odin_data(const std::pair<const bank_header_t*, const void*>& bank)   {
  const auto* h = bank.first;
  if( h->type() == bank_types_t::ODIN )    {
    if( h->version() < 7 )  {
      return odin_data(reinterpret_cast<const run2_odin_t*>(bank.second), h->version());
    }
    else if( h->version() == 7 )  {
      return odin_data(reinterpret_cast<const run3_odin_t*>(bank.second), h->version());
    }
    throw std::runtime_error("odin_data: Invalid ODIN bank version: " + std::to_string(h->version()));
  }
  throw std::runtime_error("odin_data: Invalid ODIN bank type: " + std::to_string(h->type()));
}

/// Produce data lines with ODIN bank content
std::vector<std::string> Online::event_print::odin_data(const run2_odin_t* b, int vsn)   {
  std::vector<std::string> lines;
  char text[256];
  ::snprintf(text, sizeof(text),
	     " ODIN bank %p [version:%d] Run:%8d event ID: 0x%08lX  Step:%5d TCK: 0x%08X",
	     (void*)b, vsn, b->run_number(), b->event_id(), b->step_number(), b->tck());
  lines.emplace_back(text);
  ::snprintf(text, sizeof(text),
	     "      Bunch: 0x%04x Orbit: 0x%04x  TAE: window: 0x%02x central: %s",
	     b->bunch_id(), b->orbit_id(), b->tae_window(), b->tae_central() ? "YES" : "NO ");
  lines.emplace_back(text);
  ::snprintf(text, sizeof(text),
	     "      (Hex)Types: bx: %02X event: %04X trigger: %02X",
	     b->bx_type(), b->event_type(), b->trigger_type());
  lines.emplace_back(text);
  return lines;
}

/// Produce data lines with ODIN bank content
std::vector<std::string> Online::event_print::odin_data(const run3_odin_t* b, int vsn)   {
  std::vector<std::string> lines;
  char text[256];
  ::snprintf(text, sizeof(text),
	     " ODIN bank %p [version:%d]  Run:%8d event ID: %10ld  Step:%5d GPS: 0x%016lX TCK: 0x%08X",
	     (void*)b, vsn, b->run_number(), b->event_id(), b->step_number(), b->gps_time(), b->tck());
  lines.emplace_back(text);
  ::snprintf(text, sizeof(text),
	     "      Bunch: 0x%04x Orbit: 0x%04x  TAE: entry: 0x%02x central: %-3s nzs: %-3s",
	     b->bunch_id(), b->orbit_id(), b->_tae_entry,
	     b->tae_central() ? "YES" : "NO",
	     b->nzs_mode()    ? "YES" : "NO");
  lines.emplace_back(text);
  ::snprintf(text, sizeof(text),
	     "      (Hex)Types: bx: %02X event: %04X trigger: %02X calib: 0x%02X",
	     b->bx_type(), b->event_type(), b->trigger_type(), b->calib_type());
  lines.emplace_back(text);
  return lines;
}

extern "C" int eventdata_list_bank_types(int, char**)   {
  using namespace Online;
  std::cout << "Known LHCb bnk types:" << std::endl
	    << "+----------+---------------------------------------------------------+" << std::endl
	    << "| int code | Mnemonic                                                |" << std::endl
	    << "+----------+---------------------------------------------------------+" << std::endl;
  for(int i=bank_types_t::L0Calo; i < bank_types_t::LastType; ++i)   {
    auto typ = bank_types_t::BankType(i);
    std::cout << "| " << std::setw(9) << std::left << i << "| "
	      << std::left << Online::event_print::bankType(typ)
	      << std::endl;
  }
  std::cout << "+----------+----------------------------------------------------------" << std::endl;
  return 0;
}
