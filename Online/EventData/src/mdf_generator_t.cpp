//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Author     : Markus Frank
//==========================================================================

/// Framework include files
#include <EventData/odin_t.h>
#include <EventData/mdf_generator_t.h>

/// Online namespace declaration
namespace Online    {

  namespace {
    std::pair<const bank_header_t*, const void*> make_bank(mem_buff& buff,
							   uint8_t   bnk_type,
							   uint8_t   bnk_detector_id,
							   uint16_t  bnk_source,
							   uint16_t  bnk_len,
							   uint8_t   bnk_version,
							   bool set = true)
    {
      auto tot_len = bank_header_t::total_bank_size(bnk_len*sizeof(uint32_t));
      raw_bank_offline_t* bnk = buff.get<raw_bank_offline_t>(tot_len);
      bnk->setMagic();
      bnk->setType(bnk_type);
      bnk->setVersion(bnk_version);
      bnk->setSourceID(bnk_source);
      bnk->setSize(bnk_len * sizeof(uint32_t));
      bank_header_manip::setDetectorID(bnk, bnk_detector_id);
      if ( set )   {
	uint32_t* p = bnk->begin<uint32_t>();
	for(uint32_t i=0; i < bnk_len; ++i)
	  *p++ = i+1;
      }
      return { bnk, bnk->begin<uint32_t>() };
    }
  }
}    // End namespace Online


/// Generatro MDF or TAE frame according to setup parameters
Online::mem_buff Online::mdf_generator_t::generate(int run_id, int& event_id)  const   {
  std::size_t buff_len = buffer_size * ((this->half_window > 0) ? (2*this->half_window + 1) : 1);
  mem_buff    buffer(buff_len);

  for( std::size_t pkg = 0; pkg < this->packing_factor; ++pkg )   {
    std::size_t mdf_offset = buffer.used();
    std::size_t tae_offset = 0;
    const std::array<unsigned int,4> mask { ~0x0U, ~0x0U, ~0x0U, ~0x0U };
    auto* e = new(buffer.get(event_header_t::sizeOf(3))) event_header_t();

    e->setChecksum(0);
    e->setCompression(0);
    e->setHeaderVersion(3);
    e->setDataType(event_header_t::BODY_TYPE_BANKS);
    e->setSubheaderLength(sizeof(event_header_t::Header1));

    if ( this->half_window > 0 )   {
      tae_offset = buffer.used();
      std::size_t tae_len = 3*(2*half_window+1)*sizeof(int32_t);
      auto tae = buffer.get<raw_bank_offline_t>(bank_header_t::total_bank_size(tae_len));
      tae->setMagic();
      tae->setType(bank_types_t::TAEHeader);
      tae->setSize(tae_len);
      tae->setVersion(0);
      tae->setSourceID(0);
      ::memset(tae->begin<int32_t>(), 0, tae_len);
    }
    std::size_t tae_start = buffer.used();
    for( int32_t bx = -this->half_window; bx <= this->half_window; ++bx )   {
      /// Add bank ODIN
      std::size_t bnk_start = buffer.used();

      ++event_id;

      int32_t orbit_id = event_id/10;
      int32_t bunch_id = event_id%100;

      if ( this->have_odin )   {
	auto        bnk_len = bank_header_t::total_bank_size(sizeof(run3_odin_t));
	auto*       bank = buffer.get<raw_bank_offline_t>(bnk_len);
	bank->setMagic();
	bank->setType(bank_types_t::ODIN);
	bank->setSize(sizeof(run3_odin_t));
	bank->setVersion(7);
	bank->setSourceID(0);
	auto* run = bank->begin<run3_odin_t>();
	::memset(run, 0, sizeof(run3_odin_t));
	run->_run_number = run_id;
	run->_orbit_id   = orbit_id;
	run->_event_id   = event_id;
	run->_bunch_id   = bunch_id;
	run->_gps_time     = 0x0UL;
	run->_step_number  = 0;
	run->_bx_type      = 0;
	run->_event_type   = 0;
	run->_trigger_type = 0;
	run->_nzs_mode     = 0;
	run->_calib_type   = 0;
	run->_tae_first    = 0;
	run->_tae_central  = 0;
	run->_tae_entry    = 0;
	if ( this->half_window > 0 )   {
	  if ( bx == 0 ) run->_tae_central = 1;
	  if ( bx == -this->half_window ) run->_tae_first = 1;
	  run->_tae_entry = bx + half_window + 1;
	}
      }
	
      /// Add banks ....
      for( const auto& desc : bank_types )   {
	std::size_t bank_len = desc.length;
	for( std::size_t i=0; i<desc.count; ++i, bank_len += desc.increment )  {
	  make_bank(buffer, desc.type, desc.detector, i, bank_len, 0, desc.set_pattern);
	}
      }
      e = buffer.at<event_header_t>(mdf_offset);
      if ( 0 == bx )    {
	auto* h = e->subHeader().H1;
	h->setTriggerMask(mask);
	h->setRunNumber(run_id);
	h->setOrbitNumber(orbit_id);
	h->setBunchID(bunch_id);
      }
      if ( tae_offset )    {
	int32_t  bx_offset = int32_t(bnk_start - tae_start);
	int32_t* tae_data  = buffer.at<raw_bank_offline_t>(tae_offset)->begin<int32_t>();
	tae_data   += 3 * (bx+half_window);
	tae_data[0] = bx;
	tae_data[1] = bx_offset;
	tae_data[2] = buffer.used() - bnk_start;
      }
    }
    /// Fix event header
    std::size_t evt_len = buffer.used() - mdf_offset;
    e = buffer.at<event_header_t>(mdf_offset);
    e->setSize(evt_len - e->sizeOf(3));
  }
  return buffer;
}
