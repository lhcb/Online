//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//              B.Jost
//==========================================================================

// Framework include files
#include <PCIE40Data/pcie40decoder.h>
#include <PCIE40Data/pcie40_writer.h>
#include <EventData/bank_types_t.h>
#include <EventData/odin_t.h>

// C/C++ include files
#include <cstdio>
#include <cstring>
#include <cstdarg>
#include <vector>
#include <map>
#include <unistd.h>

using namespace Online;
using namespace Online::pcie40;

#define __UNLIKELY( x ) __builtin_expect( ( x ), 0 )

namespace {

  static uint32_t s_last_good_run_number = 1234;

  void _error(const char* fmt,...)  {
    char text[1024];
    va_list args;
    va_start(args,fmt);
    ::vsnprintf(text, sizeof(text), fmt, args);
    va_end(args);
    text[sizeof(text)-1] = 0;
    ::write(fileno(stdout), text, strlen(text));
  }

  void _check(const event_collection_t& c)   {
    std::size_t num_bank_coll = 0;
    std::size_t num_event = 0;
    std::size_t num_bank = 0;
    if ( c.pattern() != MAGIC_PATTERN )   {
      _error("Bad magic pattern in event collection!\n");
    }
    for(std::size_t j=0; j < c.capacity; ++j)   {
      const event_t* e = c.at(j);
      if ( e->slot != j || e->pattern() != MAGIC_PATTERN )  {
	_error("Bad magic pattern in event!\n");
      }
      ++num_event;
      for(std::size_t i=0; i<sizeof(params::collectionOffsets)/sizeof(params::collectionOffsets[0]); ++i)   {
	const auto* b = add_ptr<bank_collection_t>(e,params::collectionOffsets[i]);
	if ( b->capacity != params::collectionCapacity[i] )  {
	  _error("Bad bank collection!\n");
	}
	if ( b->pattern() != MAGIC_PATTERN )  {
	  _error("Bad magic pattern in bank collection!\n");
	}
	++num_bank_coll;
	for(std::size_t k=0; k < b->capacity; ++k)   {
	  const bank_t* bnk = b->at(k);
	  if ( bnk->magic() != bank_t::MagicPattern )  {
	    _error("Bad magic pattern in raw bank!\n");
	  }
	  ++num_bank;
	}
      }
    }
    _error("Checked %ld event slots, %ld bank collections and %ld bank slots\n",
	   num_event, num_bank_coll, num_bank);
  }

  void _print(const event_collection_t& c)   {
    std::size_t num_bank_coll = 0;
    std::size_t num_event = 0;
    std::size_t num_bank = 0;
    _error("Dump PCIE40 event collection @%p capacity:%ld events magic:%p\n", &c, c.capacity, c.pattern());
    for(std::size_t j=0; j < c.capacity; ++j)   {
      const event_t* e = c.at(j);
      if ( e->slot != j || e->pattern() != MAGIC_PATTERN )  {
	_error("Bad magic pattern in event!\n");
      }
      ++num_event;
      for(std::size_t i=0; i<sizeof(params::collectionOffsets)/sizeof(params::collectionOffsets[0]); ++i)   {
	const auto* b = add_ptr<bank_collection_t>(e,params::collectionOffsets[i]);
	if ( b->capacity != params::collectionCapacity[i] )  {
	  _error("Bad bank collection!\n");
	}
	if ( b->pattern() != MAGIC_PATTERN )  {
	  _error("Bad magic pattern in bank collection!\n");
	}
	if ( j == 0 )   {
	  _error("\t bank collection @%p capacity:%3ld banks:%3ld special:%3ld magic:%p offset:%ld\n",
		 b, b->capacity, b->length, b->specials, b->pattern(), params::collectionOffsets[i]);
	}
	++num_bank_coll;
	for(std::size_t k=0; k < b->capacity; ++k)   {
	  const bank_t* bnk = b->at(k);
	  if ( bnk->magic() != bank_t::MagicPattern )  {
	    _error("Bad magic pattern in raw bank!\n");
	  }
	  ++num_bank;
	}
      }
    }
    _error("Checked %ld event slots, %ld bank collections and %ld bank slots\n",
	   num_event, num_bank_coll, num_bank);
  }
  inline bool take_any(unsigned short) { return true; }
}

class decoder_t::implementation_t  {
public:
  enum { PRINT = decoder_t::PRINT, ERROR = decoder_t::ERROR };
  decoder_t::logger_t log;

  implementation_t(decoder_t::logger_t& logger) : log(logger)   {
  }

  inline void print(int lvl, const char* fmt,...)  {
    if ( lvl > PRINT )   {
      char text[1024];
      va_list args;
      va_start(args,fmt);
      ::vsnprintf(text, sizeof(text), fmt, args);
      va_end(args);
      text[sizeof(text)-1] = 0;
      log(lvl, "pcie40decoder", text);
    }
  }
  
  inline bank_t* allocate_regular_bank(event_t* e, std::size_t offset)   {
    bank_collection_writer_t* c = add_ptr<bank_collection_writer_t>(e,offset);
    ++c->length;
#ifdef PCIE40_ENABLE_CHECKS
    if ( __UNLIKELY(c->length + c->specials > c->capacity) )  {
      this->print(ERROR, "Bank allocation error: Too many banks! Are there new sources?\n");
      return &c->overrun_bank;
    }
    else if ( __UNLIKELY(c->pattern() != MAGIC_PATTERN) )  {
      this->print(ERROR, "Bad magic pattern in bank collection!\n");
    }
    bank_t& b = c->banks[c->length-1];
    if ( __UNLIKELY(b.magic() != b.MagicPattern) )    {
      this->print(ERROR, "Bad magic pattern in bank!\n");
    }
    return &b;
#else
    return &c->banks[c->capacity - c->specials];
#endif
  }

  inline bank_t* allocate_special_bank(event_t* e, std::size_t offset)   {
    bank_collection_writer_t* c = add_ptr<bank_collection_writer_t>(e,offset);
    ++c->specials;
#ifdef PCIE40_ENABLE_CHECKS
    if ( __UNLIKELY(c->length + c->specials > c->capacity) )  {
      this->print(ERROR, "+++ Bank allocation error: Too many banks! Are there new sources?\n");
      return &c->overrun_bank;
    }
    else if ( __UNLIKELY(c->pattern() != MAGIC_PATTERN) )  {
      this->print(ERROR, "+++ Bad magic pattern in bank collection!\n");
    }
    bank_t& b = c->banks[c->capacity - c->specials];
    if ( __UNLIKELY(b.magic() != b.MagicPattern) )    {
      this->print(ERROR, "Bad magic pattern in bank!\n");
    }
    return &b;
#else
    return &c->banks[c->capacity - c->specials];
#endif
  }

  inline bank_t* allocate_bank(event_t* e, unsigned short src_id, unsigned char type)     {
    auto ret = event_t::collection_offset(src_id, type);
    if ( __UNLIKELY(ret.second == params::INVALID_BANK_TYPE) )
      return nullptr;
    else if ( __UNLIKELY(!ret.first) )
      return allocate_special_bank(e,ret.second);
    return allocate_regular_bank(e,ret.second);
  }

  std::size_t link_fragment(event_collection_writer_t* ec, const multi_fragment_t* mfp, bool debug)   {
    std::size_t           num_bank = 0;
    unsigned short        src_id  = mfp->header.source_id;
    const std::size_t     align   = mfp->header.alignment;
    const uint8_t         version = mfp->header.version;
    const uint8_t         *typs   = mfp->types();
    const uint16_t        *lens   = mfp->sizes();
    const frontend_data_t *curr   = mfp->data();
    //bool run = true;

    if ( (const char*)typs  > ((const char*)mfp)+6*sizeof(int) )   {
      this->print(ERROR,
		  "Inconsistent TYPES pointer. Drop MFP Source ID:%3d\n",
		  int(src_id));
      //while(run) ::sleep(1);
      return 0;
    }
    
    for (std::size_t j=0, cnt=0, n=mfp->packingFactor(); cnt<n; ++cnt, ++typs, ++lens)  {
      auto typx   = *typs;
      auto length = *lens;
      event_t*  e = ec->at(j);
      bank_t*   b = allocate_bank(e, src_id, typx);

      if ( nullptr == b )    {
	this->print(ERROR,
		    "+++ Ignore bank: Unknown bank type:%3d "
		    "Source ID:%3d Size:%4d vsn:%d j:%ld cnt:%ld n:%ld\n",
		    int(typx), int(src_id), int(length), int(version), j, cnt, n);
	//while(run) ::sleep(1);
	continue;
      }
      else  {
#ifdef PCIE40_ENABLE_CHECKS
	if ( __UNLIKELY(e->magic != MAGIC_PATTERN) )   {
	  this->print(ERROR, "Data corruption in event!\n");
	}
	if ( __UNLIKELY(b->magic() != b->MagicPattern) )   {
	  this->print(ERROR, "Data corruption in raw bank!\n");
	}
#endif
	const void* data = curr->data();
	if ( debug )    {
	  this->print(PRINT, "+++ Reading bank[%2ld] at %p len:%4d %p  typ:%3d src:%3d vsn:%d\n", 
		      cnt, (void*)data, int(length), (void*)((char*)data + length),
		      typx, src_id, version);
	}
	if ( typx == pcie40::bank_t::ODIN )   {
	  auto* sodin = (run3_odin_t*)data;
	  e->flags.detail.have_odin    = true;
	  e->flags.detail.tae_first    = sodin->_tae_first;
	  e->flags.detail.nzs_mode     = sodin->_nzs_mode;
	  e->flags.detail.calib_type   = sodin->_calib_type;
	  e->flags.detail.trigger_type = sodin->_trigger_type;
	  e->flags.detail.tae_central  = sodin->_tae_central;
	  e->flags.detail.tae_entry    = sodin->_tae_entry;
	  if ( 1 )    {
	    if ( sodin->_tae_first || sodin->_tae_central != 0 || sodin->_tae_entry > 0 )  {
	      this->print(PRINT, "+++ TAE Run: %d Event %lld: first: %d central: %d window: %d Trg-type: %d",
			  sodin->_run_number,
			  long(sodin->_event_type),
			  int(sodin->_tae_first),
			  int(sodin->_tae_central),
			  int(sodin->_tae_entry),
			  int(sodin->_trigger_type));
	    }
	  }
	  e->flags.detail.event_type   = sodin->_event_type;
	  s_last_good_run_number       = sodin->_run_number;
	}
	b->setMagic();
	b->setSize(length);
	b->setSourceID(src_id);
	b->setData(data);
	b->setType(typx);
	b->setVersion(version);
	++num_bank;
      }
      curr = curr->next(length, align);
      ++j;
    }
    return num_bank;
  }
  
  event_collection_t* _allocate(std::size_t num_events)    {
    std::size_t len = sizeof(event_collection_t) + num_events*params::eventRecordLength;
    event_collection_writer_t* c;
    void* ptr = ::operator new(len);

    ::memset(ptr,0,len);
    c = (event_collection_writer_t*)ptr;
    c->length = 0;
    c->mep = nullptr;
    c->capacity = num_events;
    c->flags.detail.run_number = s_last_good_run_number;
    c->setPattern();
    for(std::size_t j=0; j<num_events; ++j)   {
      auto* e = c->at(j);
      e->slot = j;
      e->flags.raw = 0;
      e->setPattern();
      for(std::size_t i=0; i<sizeof(params::collectionOffsets)/sizeof(params::collectionOffsets[0]); ++i)   {
	auto* b = add_ptr<bank_collection_writer_t>(e,params::collectionOffsets[i]);
	b->length   = 0;
	b->specials = 0;
	b->capacity = params::collectionCapacity[i];
	b->setPattern();
	b->overrun_bank.setMagic();
	for(std::size_t k=0; k < b->capacity; ++k)   {
	  bank_t* bnk = b->at(k);
	  bnk->setMagic();
	}
      }
    }
    return c;
  }
  
  void _initialize(event_collection_writer_t* ec)    {
    ec->length = 0;
    ec->mep = nullptr;
    ec->flags.detail.run_number = s_last_good_run_number;
    for(std::size_t j=0; j < ec->capacity; ++j)   {
      auto* e = ec->at(j);
      e->flags.raw = 0;
      for(std::size_t i=0; i<sizeof(params::collectionOffsets)/sizeof(params::collectionOffsets[0]); ++i)   {
	bank_collection_t* c = add_ptr<bank_collection_t>(e,params::collectionOffsets[i]);
	c->length   = 0;
	c->specials = 0;
	c->capacity = params::collectionCapacity[i];
	c->overrun_bank.setMagic();
      }
    }
  }
};

void printer_t::print_summary(const std::unique_ptr<event_collection_t>& coll)   const {
  if ( coll )  {
    _print(*coll);
    return;
  }
  _error("Invalid event_collection_t instance passed. Cannot print!");
}

void printer_t::check(const std::unique_ptr<event_collection_t>& coll)   const {
  if ( coll )  {
    _check(*coll);
    return;
  }
  _error("Invalid event_collection_t instance passed. Cannot check!");
}

void decoder_t::_prt(int, const char* source, const char* msg)  {
  _error("%s %s", source, msg);
}

/// Default constructor
decoder_t::decoder_t()   {
  logger_t log = decoder_t::_prt;
  implementation = new implementation_t(log);
}

/// Initializing constructor with printout object
decoder_t::decoder_t(logger_t& logger)   {
  implementation = new implementation_t(logger);
}

/// Default destructor
decoder_t::~decoder_t()   {
  delete implementation;
  implementation = nullptr;
}

/// Access last known good run-rumber
uint32_t decoder_t::last_good_run_number()   {
  return s_last_good_run_number;
}

/// Reset last known good run-rumber (default: 1234)
void decoder_t::reset_last_good_run_number()   {
  s_last_good_run_number = 1234;
}

/// Initialize user data structure
event_collection_t* decoder_t::initialize(std::unique_ptr<event_collection_t>& c, std::size_t num_events)  const  {
  if ( !c.get() || c->capacity < num_events )  {
    if ( c.get() ) ::operator delete(c.release());
    c.reset(implementation->_allocate(num_events));
    return c.get();
  }
  implementation->_initialize((event_collection_writer_t*)c.get());
  return c.get();
}

/// Decode MEP fragment
std::pair<std::size_t, std::size_t>
decoder_t::decode(std::unique_ptr<event_collection_t>& coll, 
		       const mep_header_t* mep)  const
{
  return decode(coll, mep, take_any);
}

/// Decode MEP fragment with predicate
std::pair<std::size_t, std::size_t>
decoder_t::decode(std::unique_ptr<event_collection_t>& ev_coll,
			      const mep_header_t* mep,
			      source_selector source_sel)  const
{
  std::size_t num_banks = 0;
  std::size_t num_fragments = 0;
  if ( mep )    {
    std::size_t num_err_packing = 0;
    num_fragments = mep->num_source;
    if ( num_fragments > 0 )   {
      const multi_fragment_t *last_mfp = nullptr;
      std::size_t packing  = mep->multi_fragment(0)->packingFactor();
      auto ec    = (event_collection_writer_t*)initialize(ev_coll, packing);
      ec->mep    = mep;
      ec->length = packing;
      for( uint32_t i = 0; i < num_fragments; ++i )    {
	const auto* mfp = mep->multi_fragment(i);
	const auto& hdr = mfp->header;
	uint16_t src_id = hdr.source_id;
	if ( !mfp->is_valid() )   {
	  implementation->print(ERROR,
				"+++ SKIP CORRUPTED MFP[%2d] at %p len:%6d %p  magic:%04X "
				"pack:%3d eid:%8ld src:%3d vsn:%d align:%d\n",
				i, (void*)mfp, int(hdr.size),
				(void*)((char*)mfp + hdr.size), 
				int(hdr.magic), int(hdr.packing), long(hdr.event_id),
				int(src_id), int(hdr.version),
				int(hdr.alignment));
	  if ( last_mfp )    {
	    const auto& lhdr = last_mfp->header;
	    uint16_t lsrc_id = lhdr.source_id;
	    implementation->print(ERROR,
				  "+++ LAST GOOD MFP[%2d] at %p len:%6d %p  magic:%04X "
				  "pack:%3d eid:%8ld src:%3d vsn:%d align:%d\n",
				  i, (void*)last_mfp, int(lhdr.size),
				(void*)((char*)last_mfp + lhdr.size), 
				int(lhdr.magic), int(lhdr.packing), long(lhdr.event_id),
				int(lsrc_id), int(lhdr.version),
				int(lhdr.alignment));
	  }
	  continue;
	}
	if ( debug )   {
	  implementation->print(PRINT,
				"+++ Reading mfp[%2d] at %p len:%6d %p  magic:%04X "
				"pack:%3d eid:%8ld src:%3d vsn:%d align:%d\n",
				i, (void*)mfp, int(hdr.size),
				(void*)((char*)mfp + hdr.size), 
				int(hdr.magic), int(hdr.packing), long(hdr.event_id),
				int(src_id), int(hdr.version),
				int(hdr.alignment));
	}
#ifdef PCIE40_ENABLE_CHECKS
	if ( mfp->packingFactor() != packing )   {
	  ++num_err_packing;
	}
#endif
	if ( !source_sel || source_sel(src_id&params::sourceid_TOP5) )   {
	  num_banks += implementation->link_fragment(ec, mfp, debug);
	}
	last_mfp = mfp;
      }
      if ( num_err_packing > 0 )  {
	implementation->print(ERROR,
			      "+++ Got %ld fragments with inconsistent packing factor!\n",
			      num_err_packing);
      }
      ec->flags.detail.run_number = s_last_good_run_number;
      return std::make_pair(num_fragments, num_banks);
    }
  }
  auto ec = (event_collection_writer_t*)initialize(ev_coll, 0);
  ec->flags.detail.run_number  = s_last_good_run_number;
  ec->length = 0;
  return std::make_pair(num_fragments, num_banks);
}

/// Format PCIE40 multi fragment header data to a set of lines for printing
std::vector<std::string> Online::event_print::headerData( const pcie40::multi_fragment_header_t* hdr )   {
  char text[256];
  std::vector<std::string> lines;
  ::snprintf(text, sizeof(text),
	     "+----> PCIE40 multi fragment header: @ %p len: %6d packing: %5d "
	     "eid:%-10ld srcid:%-4d align:%2d vsn:%2d pattern: %4X",
	     (void*)&hdr, hdr->size, hdr->packing, hdr->event_id, hdr->source_id,
	     hdr->alignment, hdr->version, hdr->magic);
  lines.emplace_back(text);
  return lines;
}

/// Format PCIE40 MEP header data to a set of lines for printing
std::vector<std::string> Online::event_print::headerData( const pcie40::mep_header_t* hdr )   {
  char text[256];
  std::vector<std::string> lines;
  ::snprintf(text, sizeof(text), "+----> PCIE40 mep header: @ %p len: %d sources:%d pattern: %4X",
	     (void*)&hdr, hdr->size, hdr->num_source, hdr->magic);
  lines.emplace_back(text);
  return lines;
}

/// Format PCIE40 event header data to a set of lines for printing
std::vector<std::string> Online::event_print::headerData( const pcie40::event_t* evt )   {
  char text[256];
  std::vector<std::string> lines;
  ::snprintf(text, sizeof(text), "+----> PCIE40 event: @ %p slot: %ld len: %ld num_coll: %ld pattern: %16lX",
	     (void*)evt, evt->slot, evt->total_length(), evt->num_bank_collections(), evt->magic);
  lines.emplace_back(text);
  ::snprintf(text, sizeof(text),
	     "|      e-typ:%-6d nzs:%s odin:%s trg-typ:%2d calib-typ:%2d "
	     "TAE: (entry:%d central:%s first:%s)",
	     evt->event_type(),
	     evt->nzs_mode()       ? "YES" : "NO",
	     evt->have_odin()      ? "YES" : "NO",
	     evt->trigger_type(),
	     evt->calib_type(),
	     evt->tae_entry(),
	     evt->is_tae_central() ? "YES" : "NO",
	     evt->is_tae_first()   ? "YES" : "NO");
  lines.emplace_back(text);
  return lines;
}
