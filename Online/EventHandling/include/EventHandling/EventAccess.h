//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Project    : Online
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_EVENTHANDLING_EVENTACCESS_H
#define ONLINE_EVENTHANDLING_EVENTACCESS_H

/// Framework include files
#include <EventHandling/Macros.h>
#include <EventHandling/EventTraits.h>
#include <CPP/Interactor.h>
#include <RTL/Logger.h>
#include <RTL/bits.h>
#include <RTL/Pack.h>

/// C/C++ include files
#include <list>
#include <vector>
#include <mutex>
#include <memory>
#include <atomic>
#include <limits>
#include <climits>
#include <stdexcept>
#include <condition_variable>

/// Online namespace declaration
namespace Online   {

  
  /// Base class to access online event data
  /** @class EventAccess
    *
    * @author  M.Frank
    * @version 1.0
    * @date    25/04/2019
    */
  class EventAccess : public CPP::Interactor  {
  public:
    
    typedef std::lock_guard<std::mutex>        lock_t;
    typedef event_traits::data_t               data_t;
    typedef event_traits::record_t             record_t;
    typedef const event_traits::data_t*        datapointer_t;
    typedef std::vector<bank_header_t*>        bank_collection_t;
    typedef event_traits::event_collection_t   event_collection_t;
    /// Base class to guard event bursts and protect against premature deletion
    /**
     *
     * \author  M.Frank
     * \version 1.0
     * \date    25/04/2019
     */
    class guard_t   {
    protected:
      std::atomic<std::size_t> bx_next   { 0 };
      std::vector<int>         crossings {   };
      std::size_t              num_evt   { 0 };
      long                     typ       { event_traits::none::data_type };

    public:
      /// Initializing constructor for multi MDF events
      guard_t(std::vector<int>&& bx, long t) : crossings(std::move(bx)), typ(t)  { }
      /// Default constructor
      guard_t() = delete;
      /// Move constructor
      guard_t(guard_t&& copy) = delete;
      /// Move assignment
      guard_t& operator=(guard_t&& copy) = delete;
      /// Inhibit copy assignment
      guard_t& operator=(const guard_t& copy) = delete;
      /// Default destructor
      virtual ~guard_t() = default;
      /// Careful: unprotected information access
      bool        empty()  const           {  return bx_next >= num_bx();    }
      /// Access the number of frames in the burst
      std::size_t num_events()   const     {  return num_evt;                }
      /// Access the number of Bx in the burst (if TAE: # of central events, num_bx else)
      std::size_t num_bx()   const         {  return crossings.size();       }
      /// Gaurd type
      long        type()   const           {  return typ;                    }
      /// Debugging: return type name of the burst ("TELL1" or "PCIE40")
      const char* type_name()  const;
      /// Access the current event frames in the burst (Num.Bx corrected for TAE)
      int32_t     next_bx()  {
	int bx = bx_next < crossings.size() ? crossings[bx_next] : -1;
	++bx_next;
	return bx;
      }
      /// Access Bx event record.
      virtual record_t at(std::size_t which) const  = 0;
    };

    typedef std::shared_ptr<guard_t>               shared_guard_t;
    typedef std::list<shared_guard_t>              burst_collection_t;
    typedef std::pair<std::size_t, shared_guard_t> event_t;


    /// Base class to hold basic configuration properties
    /**
     *
     * \author  M.Frank
     * \version 1.0
     * \date    25/04/2019
     */
    struct config_t  {
      /// Default constructor
      config_t() = default;
      /// Move constructor
      config_t(config_t&& copy) = default;
      /// Copy constructor
      config_t(const config_t& copy) = default;
      /// Default destructor
      ~config_t() = default;
      /// Assignment operator
      config_t& operator=(config_t&& copy) = default;
      /// Move assignment operator
      config_t& operator=(const config_t& copy) = default;
      /// Property: Print burst information every burstPrintCount events
      std::size_t  burstPrintCount   = std::numeric_limits<std::size_t>::max();
      /// Property: Maximum number of events to be processed
      std::size_t  maxEventsIn       = std::numeric_limits<std::size_t>::max();
      /// Property: Enable data verification for Tell1 banks
      bool         verifyBanks       = true;

      /** TAE handling  */
      /// Property: Expand TAE frames. Handle them like normal collisions otherwise
      bool         expandTAE         = true;
      /// Property: Ignore all TAE events to be passed to the user. Requires expandTAE=true
      bool         ignoreTAE         = false;
      /// Property: Only select TAE events. Requires expandTAE=true
      bool         onlyTAE           = false;
      /// Property: Drop edges at start/end of a MEP in case TAE handling is requested. Requires expandTAE=true
      bool         dropEdgesTAE      = true;
      /// Property: Convert edges at start/end of a MEP to single collisision entries if TAE. Requires expandTAE=true
      bool         convertEdgesTAE   = false;

      /** Event header based pre-selection for raw events, not PCIE40 */
      using header_mask_t = Bits::BitMask<4>;
      /// Property: Raw events with header: Check TRIGGER mask if not empty
      header_mask_t headerTriggerMask = header_mask_t(~0x0);
      /// Property: Raw events with header: Check VETO mask if not empty
      header_mask_t headerVetoMask    = header_mask_t(0x0);

    } *base_config = 0;

  public:
    /// Reference to output logger
    std::unique_ptr<RTL::Logger>  m_logger;
    /// Mutex to protect the burst queue
    std::mutex                    m_burstLock;
    /// Mutex to protect the event queue
    std::mutex                    m_eventLock;
    /// Mutex for condition variable when waiting from the frame producer
    std::mutex                    m_dataLock;
    /// Condition variable when waiting from the frame producer
    std::condition_variable       m_haveData;
    /// Container to bank referentes to MDF headers containing event blocks
    burst_collection_t            m_bursts;

    /// Definition of the monitoring data block
    /**
     *
     * \author  M.Frank
     * \version 1.0
     * \date    25/04/2019
     */
    struct monitor_t  {
      /// Monitoring item: Count of bursts which were received from the input source
      std::size_t                 burstsIn       = 0;
      /// Monitoring item: Count of bursts which started or finished processing
      std::size_t                 burstsOut      = 0;
      /// Monitoring item: Count of bursts which were released
      std::size_t                 burstsRelease  = 0;
      /// Monitoring item: Count of bursts which were released
      std::size_t                 burstsRequest  = 0;
      /// Monitoring item: Count of events which were received from the input source
      std::size_t                 eventsIn       = 0;
      /// Monitoring item: Count of events which started or finished processing
      std::size_t                 eventsOut      = 0;
      /// Monitoring item: Count of MDF events (debugging)
      std::size_t                 eventsMDF      = 0;
      /// Monitoring item: Count of events which are currently buffered    
      std::size_t                 eventsBuffered = 0;
      /// Monitoring item: Count the events with no data banks
      std::size_t                 eventsNoBanks  = 0;
      /// Monitoring item: Count the events with more banks than allowed
      std::size_t                 eventsMaxBanks = 0;
      /// Monitoring item: Count the events bad banks encountered
      std::size_t                 eventsBadBanks = 0;
      /// Monitoring item: Count the events with no data banks
      std::size_t                 eventsNoODIN   = 0;
      /// Monitoring item: Count the number of TAE events encountered
      std::size_t                 framesTAE      = 0;
      /// Monitoring item: Count the number of TAE fragments encountered
      std::size_t                 fragmentsTAE   = 0;
    } monitor;

    /// Property:  poll timeout to check if producers have delivered an event [milliseconds]
    int                           m_eventPollTMO = 100;
    /// Flag to indicate if event request got cancelled
    bool                          m_cancelled    = false;

    std::pair<long, unsigned char*> m_deCompress {0,nullptr};

    /// The event is a MDF with multiple events, which must be decoded
    std::pair<std::vector<int>, std::unique_ptr<event_traits::event_collection_t> >
    convertMultiMDF(datapointer_t start, std::size_t len);

    /// The event is a PCIE40 MEP structure with multiple events, which must be decoded
    std::pair<std::vector<int>, std::unique_ptr<pcie40::event_collection_t> >
    convertPCIE40MEP(datapointer_t start, std::size_t len);

  public:
    /// Default constructors and destructors / inhibits
    ONLINE_COMPONENT_CTORS(EventAccess);

    /// Default constructor
    EventAccess(std::unique_ptr<RTL::Logger>&& logger);

    /// Standard destructor
    virtual ~EventAccess();

    /// Print burst/event information
    virtual void printInfo()  const;

    /// Set the print frequency
    void setBurstPrintCount(std::size_t value) { base_config->burstPrintCount = value;     }

    /// Set flag to verify banks
    void setVerifyBanks(bool value)            { base_config->verifyBanks = value;         }

    /// Configure preselection using event header: VETO mask
    void setHeaderVetoMask(const std::vector<uint32_t>& mask);
    /// Configure preselection using event header: TRIGGER mask
    void setHeaderTriggerMask(const std::vector<uint32_t>& mask);

    /** Configure TAE handling   */
    /// Set TAE expansion flag
    void setExpandTAE(bool value)              { base_config->expandTAE = value;           }
    /// Set flag to ONLY accept TAE events
    void setOnlyTAE(bool value)                { base_config->onlyTAE = value;             }
    /// Set flag to entire IGNORE TAE events
    void setIgnoreTAE(bool value)              { base_config->ignoreTAE = value;           }
    /// Set flag to drop TAE at the PCIE40 MEP edges
    void setDropEdgesTAE(bool value)           { base_config->dropEdgesTAE = value;        }
    /// Set flag to convert TAE at the PCIE40 MEP edges to regulare crossings
    void setConvertEdgesTAE(bool value)        { base_config->convertEdgesTAE = value;     }

    /// Queue cancellation
    void queueCancel(bool value = true)        { m_cancelled = value;                      }

    /// Access cancellation flag
    bool           isCancelled()  const        { return m_cancelled;                       }

    /// Any events buffered anymore?
    bool           empty()  const              { return monitor.eventsBuffered == 0;       }

    /// Access number of events buffered (unsafe)
    std::size_t    eventsBuffered()   const    { return monitor.eventsBuffered;            }

    /// Access number of bursts buffered (unsafe)
    std::size_t    burstsBuffered()   const    { return monitor.burstsIn-monitor.burstsOut;}

    /// Insert new entry into burst queue with lock protection
    std::size_t    queueBurst(shared_guard_t&& burst);

    /// Dequeue burst if one is present with lock protection
    shared_guard_t dequeueBurst();

    /// Access thread safe number of queued bursts
    std::size_t    numQueuedBurst();

    /// Dequeue burst if present, otherwise wait for producer to emplace one.
    shared_guard_t waitBurst();

    /// Clear burst queue and update counters
    std::size_t    clear();
    
    /// Dequeue event if one is present with lock protection
    event_t        dequeueEvent(shared_guard_t& context);

    /// Dequeue event if present, otherwise wait for producer to emplace one.
    event_t        waitEvent(shared_guard_t& context);

    /// Fill the event data cache if necessary
    virtual int    fill_cache() = 0;

    /// Cancel all pending I/O requests to the buffer manager
    virtual int    cancel() = 0;

    /// Close connection to event data source
    virtual void   close() = 0;
  };
}        // End namespace Online
#include "RTL/Unpack.h"
#endif   // ONLINE_EVENTHANDLING_EVENTACCESS_H
