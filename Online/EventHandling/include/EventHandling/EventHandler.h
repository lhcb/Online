//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Project    : Online
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_EVENTHANDLING_EVENTHANDLER_H
#define ONLINE_EVENTHANDLING_EVENTHANDLER_H

/// Framework include files
#include <EventHandling/EventAccess.h>
#include <RTL/Logger.h>

/// C/C++ include files
#include <functional>
#include <map>

/// Forward declarations
namespace LHCb {  class RawBank;  }

/// Online namespace declaration
namespace Online  {

  /// Forward declarations
  class   EventOutput;
  class   bank_header_t;

  /// Online algorithm to handle event data
  /** 
    *
    * @author  M.Frank
    * @version 1.0
    * @date    25/04/2019
    */
  class EventHandler   {
  public:

    using lb_evt_data_t = std::vector<std::pair<const LHCb::RawBank*, const void*> >;
    using evt_data_t    = std::vector<std::pair<const bank_header_t*, const void*> >;
    using evt_desc_t    = std::pair<std::size_t, EventAccess::shared_guard_t>;
    using mask_t        = const std::vector<int>;

    enum dump_flags   {
      DUMP_EVENTS           = 1 << 1,
      DUMP_BANK_COLLECTIONS = 1 << 2,
      DUM_BANK_HEADERS      = 1 << 3
    };
    enum error_flags   {
      NO_MDF_ERROR         = 0,
      INVALID_MDF_FORMAT    = 1 << 1,
      INVALID_MAGIC_PATTERN = 1 << 2,
      INVALID_BANK_SIZE     = 1 << 3,
      TOO_MANY_BANKS        = 1 << 31
    };

  protected:
    /// Reference to output logger
    std::unique_ptr<RTL::Logger> m_logger;

  public:

    /// Default constructor
    EventHandler() = default;

    /// Default destructor
    virtual ~EventHandler() = default;

    /// Put bank container to specified location in the TES
    virtual void put_banks(const std::string& loc, evt_data_t&& event)  const = 0;
    
    /// Construct string with prefix of spillover events
    std::string _bx_offset(int bx)  const;

    bool        type_found(int required, const std::vector<int>& good_types)  const;

    /// Check if the event is a TAE event
    std::pair<unsigned int, bool> check_tae(const evt_desc_t& event)  const;
    
    /// Access to ODIN bank from the event record
    std::pair<const bank_header_t*, const void*> get_odin_bank(const event_header_t* ev)  const;
    /// Access to ODIN banks from the event record
    std::pair<const bank_header_t*, const void*> get_odin_bank(const evt_desc_t& event)  const;

    /// Put bank container to specified location in the TES
    void        put_event(const std::string& loc, const evt_desc_t& event, int32_t eid)  const;

    /// Extract event record from normal/TAE event frame
    std::pair<int32_t,evt_data_t> create_event_data(const evt_desc_t& event, int32_t bx)  const;

    /// Extract error banks from the event data
    evt_data_t  get_errors(event_traits::record_t event)  const;

    /// Extract error banks from the event data
    evt_data_t  get_errors(evt_desc_t& event)  const;

    /// Extract Tell1 banks from event buffer
    std::pair<int,evt_data_t> extract_tell1_banks(const unsigned char* start, const unsigned char* end)  const;

    /// Dump event structure to output
    void        dump_event(evt_desc_t& e, int flags)  const;

    /// Output MDF frame from the event data
    template <typename traits = void>
    std::size_t output_mdf(const evt_data_t& event, mask_t& mask, bool requireODIN, EventOutput& output)  const;

    /// Output MDF frame from the event data
    std::size_t output_mdf(const evt_data_t& event, int type, mask_t& mask, bool requireODIN, EventOutput& output)  const;

    /// Output MDF/TAE frame from the event data
    template <typename traits = void>
    std::size_t output_mdf(const evt_desc_t& event, mask_t& mask, bool requireODIN, EventOutput& output)  const;

    /// Output MDF/TAE frame from the event data
    std::size_t output_mdf(const std::vector<std::pair<int, const evt_data_t*> >& crossings, mask_t& mask, EventOutput& output)  const;

    /// Output TAE frame in MDF format
    template <typename traits = void>
    std::size_t output_tae(const evt_desc_t& event, mask_t& mask, EventOutput& output)  const;

    /// Output PCIE40 event in PCIE40-MEP format
    void        output_pcie40(const EventAccess::guard_t* guard, EventOutput& output)  const;

    /// Extract banks from event descriptor
    template <typename traits = void>
    std::pair<int,evt_data_t> extract_banks(const evt_desc_t& event)  const;

    /// On the occurrence of a TAE frame expand data and populate TES locations
    template <typename traits = void>
    std::size_t expand_tae(const std::string& prefix, const evt_desc_t& event)  const;

    /// Report MDF error
    template <typename T> T mdf_error_report(T return_value, int code)  const;


    /** Bank extraction interface  */

    /// Extract banks from a TELL1 data frame
    evt_data_t  extract_raw_banks(const uint8_t* start, const uint8_t* end, bank_types_t::BankType bank_type)  const;

    /// Extract banks from a TELL1 data frame (if TAE: according to TAE offset)
    evt_data_t  extract_raw_banks(const event_traits::tell1::event_type& evt,
				 bank_types_t::BankType bank_type,
				 long offset)  const;
    
    /// Extract banks from a TELL40 data frame (if TAE: according to TAE offset)
    evt_data_t  extract_raw_banks(const event_traits::tell40::event_type& evt,
				 bank_types_t::BankType bank_type)  const;

    /// Extract raw banks from access guard
    evt_data_t  extract_raw_banks(const EventAccess::guard_t& guard,
				 bank_types_t::BankType bank_type,
				 long event_id,
				 long offset)  const;

    /// Extract banks from event descriptor. If TAE: also extract subevent banks with bx information
    std::map<int32_t, evt_data_t> extract_raw_banks(const evt_desc_t& evt,
						    bank_types_t::BankType bank_type)  const;
    
    /** Bank access interface: Non-const handlers  */
    using bank_handler_t = std::function<bool(const std::pair<const bank_header_t*, const void*>&)>;

    /// Extract banks from a TELL1 data frame
    void access_raw_banks(const uint8_t* start, const uint8_t* end, bank_handler_t& handler)  const;

    /// Extract banks from a TELL1 data frame (if TAE: according to TAE offset)
    void access_raw_banks(const event_traits::tell1::event_type& evt, long offset, bank_handler_t& handler)  const;
    
    /// Extract banks from a TELL40 data frame (if TAE: according to TAE offset)
    void access_raw_banks(const event_traits::tell40::event_type& evt, bank_handler_t& handler)  const;

    /// Extract raw banks from access guard
    void access_raw_banks(const EventAccess::guard_t& guard, bank_handler_t& handler, long event_id, long offset)  const;

    /// Extract banks from event descriptor. If TAE: also extract subevent banks with bx information
    void access_raw_banks(const evt_desc_t& evt, bank_handler_t& handler)  const;

    /** Bank access interface: const handlers  */
    /// Extract banks from a TELL1 data frame
    void access_raw_banks(const uint8_t* start, const uint8_t* end, const bank_handler_t& handler)  const;

    /// Extract banks from a TELL1 data frame (if TAE: according to TAE offset)
    void access_raw_banks(const event_traits::tell1::event_type& evt, long offset, const bank_handler_t& handler)  const;
    
    /// Extract banks from a TELL40 data frame (if TAE: according to TAE offset)
    void access_raw_banks(const event_traits::tell40::event_type& evt, const bank_handler_t& handler)  const;

    /// Extract raw banks from access guard
    void access_raw_banks(const EventAccess::guard_t& guard, const bank_handler_t& handler, long event_id, long offset)  const;

    /// Extract banks from event descriptor. If TAE: also extract subevent banks with bx information
    void access_raw_banks(const evt_desc_t& evt, const bank_handler_t& handler)  const;

  };
}       // End namespace Online
#endif  // ONLINE_EVENTHANDLING_EVENTHANDLER_H
