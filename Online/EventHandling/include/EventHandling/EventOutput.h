//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Project    : Online
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_EVENTDATA_EVENTOUTPUT_H
#define ONLINE_EVENTDATA_EVENTOUTPUT_H

/// Framework include files
#include <EventHandling/Macros.h>
#include <CPP/Interactor.h>
#include <RTL/Logger.h>

/// C/C++ include files
#include <list>
#include <mutex>
#include <atomic>
#include <condition_variable>

/// Online namespace declaration
namespace Online   {

  /// Base class to output online event data
  /** @class EventOutput
    *
    * @author  M.Frank
    * @version 1.0
    * @date    25/04/2019
    */
  class EventOutput : public CPP::Interactor  {
  public:
    typedef std::lock_guard<std::mutex> lock_t;
    typedef uint8_t                     data_t;
    typedef const data_t*               datapointer_t;

    /// Base class to handle one atomic write operation
    /**
     *
     * @author  M.Frank
     * @version 1.0
     * @date    25/04/2019
     */
    class transaction_t   {
    public:
      /// Allocated data buffer
      data_t*      buffer   { 0 };
      /// Total AVAILIBLE buffer length
      std::size_t  size     { 0 };
      /// Total USED buffer length
      std::size_t  length   { 0 };
      /// Start of the current event transaction
      std::size_t  start    { 0 };
      /// Number of events contained
      std::size_t  events   { 0 };
    public:
      /// Default constructor
      transaction_t() = default;
      /// Move constructor
      transaction_t(transaction_t&& copy) = default;
      /// Inhibit copy aconstructor
      transaction_t(const transaction_t& copy) = delete;
      /// Move assignment
      transaction_t& operator=(transaction_t&& copy) = default;
      /// Inhibit copy assignment
      transaction_t& operator=(const transaction_t& copy) = delete;
      /// Default destructor
      virtual ~transaction_t() = default;
      /// Are there any data contained ?
      virtual bool         empty()          const {  return length == 0;   }
      /// Access number of used bytes for this transaction
      virtual std::size_t  used_bytes()     const {  return length;        }
      /// Access number of free bytes for this transaction
      virtual std::size_t  free_bytes()     const {  return size-length;   }
      /// Access the data size since the last (partial) commit
      virtual std::size_t  event_size()     const {  return length-start;  }
      /// Access current buffer pointer
      virtual data_t*      pointer()        const {  return buffer+length; }
      /// Access current buffer pointer with concrete type
      template <typename T> T* get()  const;
      /// Add event data to the transaction buffer
      std::size_t add_data(const datapointer_t data, std::size_t length);
      /// Add event data to the transaction buffer
      template <typename T> std::size_t add_data(const T& data);
      /// Advance buffer pointer to reserve free space
      data_t* advance(std::size_t len);

      /// Allocate buffer to store contiguous junk of data
      virtual bool reserve(std::size_t len) = 0;
      /// Synchronize the buffered data with the medium
      virtual bool sync() = 0;
      /// Shutdown the I/O medium
      virtual bool close() = 0;
      /// Reposition output buffer to specified location
      virtual std::size_t position(std::size_t new_position) = 0;
    };
    typedef std::list<transaction_t*>   transaction_collection_t;

  public:
    /// Reference to output logger
    std::unique_ptr<RTL::Logger>               m_logger        { };
    /// Mutex to protect the transaction queue
    std::mutex                                 m_transactionLock{};
    /// Mutex for condition variable when waiting from the frame producer
    std::mutex                                 m_haveTrLock    { };
    /// Condition variable when waiting from the frame producer
    std::condition_variable                    m_haveTr        { };
    /// List of idle transactions to be (re-)used
    std::list<std::unique_ptr<transaction_t> > m_idleTr        { };
    /// List of transactions used currently by clients
    std::list<std::unique_ptr<transaction_t> > m_busyTr        { };
    /// Output burst counter
    std::atomic<std::size_t>                   m_burstCount    { 0 };
    /// To compute average event size: event counter
    std::atomic<std::size_t>                   m_eventCount    { 0 };
    /// To compute average event size: event counter
    std::atomic<std::size_t>                   m_eventAvgCount { 0 };
    /// To compute average event size: event bytes
    std::atomic<std::size_t>                   m_eventAvgBytes { 0 };

    std::atomic<std::size_t>                   m_numGetTr      { 0 };
    
    /// Flag to indicate if event request got cancelled
    bool                                       m_cancelled     { false };

    /// Base class to hold basic configuration properties
    /**
     *
     * \author  M.Frank
     * \version 1.0
     * \date    25/04/2019
     */
    struct config_t  {
      /// Default constructor
      config_t() = default;
      /// Move constructor
      config_t(config_t&& copy) = default;
      /// Copy constructor
      config_t(const config_t& copy) = default;
      /// Default destructor
      ~config_t() = default;
      /// Assignment operator
      config_t& operator=(config_t&& copy) = default;
      /// Move assignment operator
      config_t& operator=(const config_t& copy) = default;

      /// Property: default allocation size for data buffer
      std::size_t                   allocation_size    { 2*1024*1024 };
      /// Property: Print output information every eventPrintCount events
      std::size_t                   burstPrintCount    { std::numeric_limits<std::size_t>::max() };
      /// Property: timeout to wait for transaction becoming availible [milli-seconds]
      int32_t                       transitionWaitTMO  { 10 };
      /// Property: Enforce single event commits (0 -> fill space to end)
      std::size_t                   max_events         {  0 };
      /// Property: Use the event mask of the first event
      bool                          useEventMask       { false };
    } *base_config = 0;

    /// Dequeue transaction for use by client
    transaction_t* dequeueTransaction(std::size_t size);
    /// Requeue transaction for further use
    bool enqueueTransaction(transaction_t* tr);

  public:
    /// Default constructors and destructors / inhibits
    ONLINE_COMPONENT_CTORS(EventOutput);

    /// Default constructor
    EventOutput(std::unique_ptr<RTL::Logger>&& logger, config_t* config);
    /// Standard destructor
    virtual ~EventOutput();

    /// Print burst/event information
    virtual void printInfo()  const;
    /// Are there any data contained ?
    virtual bool    empty();
    /// Queue cancellation
    void queueCancel(bool value = true);
    /// Access cancellation flag
    bool isCancelled() const              { return m_cancelled;        }

    /// Cancel all pending I/O requests to the buffer manager
    virtual int cancel()  = 0;
    /// EventAccess overload: Close connection to event data source
    virtual void close();

    /// Start output transaction
    virtual transaction_t* getTransaction(std::size_t min_size);
    /// Commit output transaction. Data may stll be cached if there is sufficient space left
    virtual bool end(transaction_t*& tr);
    /// Cancel commit transaction. Resets the buffer to the event start
    virtual bool rollback(transaction_t*& tr);
    /// Forced commit output transaction. Flush data to medium
    virtual bool commit(transaction_t*& tr);
    /// Forced commit of all output transactions. Flush data to medium
    virtual bool commit_all();
  };

  template <typename T> inline
    T* EventOutput::transaction_t::get()   const {
    if ( length<size )
      return reinterpret_cast<T*>(this->pointer());
    throw std::runtime_error("EventOutput::transaction: Exceeded buffer size.");
  }

  /// Add event data to the transaction buffer
  template <typename T> inline
    std::size_t EventOutput::transaction_t::add_data(const T& data)   {
    std::size_t tot = 0;
    std::size_t pos = length;
    for(const auto* b : data)   {
      std::size_t len = add_data((const datapointer_t)b, b->totalSize());
      if ( !len )   {
	length = pos;
	return 0;
      }
      tot += len;
    }
    length = pos + tot;
    return tot;
  }

}        // End namespace Online
#endif   // ONLINE_EVENTDATA_EVENTOUTPUT_H
