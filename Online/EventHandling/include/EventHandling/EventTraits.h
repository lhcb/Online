//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Project    : Online
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_EVENTDATA_EVENTTRAITS_H
#define ONLINE_EVENTDATA_EVENTTRAITS_H

/// Framework include files
#include <PCIE40Data/pcie40.h>
#include <EventData/odin_t.h>
#include <EventData/event_header_t.h>
#include <EventData/raw_bank_offline_t.h>

/// C/C++ include files
#include <vector>

/// Online namespace declaration
namespace Online   {

  /// Online event traits namespace declaration
  namespace event_traits   {

    enum BurstType   {
      NO_DATA_TYPE = 0,
      TELL1_DATA_TYPE = 1,
      TELL40_DATA_TYPE = 2
    };

    typedef unsigned char data_t;

    union record_t {
      const void*         pointer;
      data_t*             data;
      event_header_t*     tell1_event;
      pcie40::event_t*    tell40_event;

      record_t(std::nullptr_t p)             {  pointer = p;         }
      record_t(const event_header_t* p)      {  pointer = p;         }
      record_t(const pcie40::event_t* p)     {  pointer = p;         }
      record_t(const record_t& p)            {  pointer = p.pointer; }
      record_t& operator=(const record_t& copy)  {
	pointer = copy.pointer;
	return *this;
      }
      operator bool () const                 {  return data != 0; }
      bool is_tell1()  const
      {	return data != nullptr && tell1_event->is_mdf();          }
      bool is_tell40()  const
      {	return data != nullptr && tell40_event->pattern() == pcie40::MAGIC_PATTERN && !tell1_event->is_mdf(); }
    };
    /// Event container definition for bursts: Contains only the MDFHeader banks
    typedef std::vector<record_t> event_collection_t;

    struct none   {
      static constexpr int data_type = NO_DATA_TYPE;
    };


    struct tell1  {
      typedef Online::raw_bank_offline_t bank_type;
      typedef event_header_t  event_type;
      typedef record_t        record_type;
      static constexpr int data_type = TELL1_DATA_TYPE;
      struct bank_iteration_t  {
	const bank_type* _first;
	const bank_type* _last;
	bank_iteration_t(const event_type& e, std::size_t which)    {
	  _first = pcie40::add_ptr<bank_type>(&e, e.sizeOf(e.headerVersion()));
	  _last  = which != 0 ? _first : pcie40::add_ptr<bank_type>(_first, e.size0());
	}
	const bank_type* begin()  const      {  return _first;         }
	const bank_type* end()    const      {  return _last;          }
	const bank_type* next(const bank_type* prev)  const
	{  return pcie40::add_ptr<bank_type>(prev, prev->totalSize()); }
      };
    };

    
    struct tell40   {
      typedef raw_bank_online_t bank_type;
      typedef pcie40::event_t   event_type;
      typedef record_t          record_type;
      static constexpr int data_type = TELL40_DATA_TYPE;

      struct bank_iteration_t  {
	const bank_type* _first;
	const bank_type* _last;
	bank_iteration_t(const event_type& e, std::size_t which) {
	  const pcie40::bank_collection_t* bc = e.bank_collection(which);
	  _first = bc->begin();
	  _last  = bc->end();
	}
	const bank_type* begin()  const   {  return _first;            }
	const bank_type* end()    const   {  return _last;             }
	const bank_type* next(const bank_type* prev)  const 
	{  return pcie40::add_ptr<bank_type>(prev, sizeof(bank_type)); }
      };
    };
    inline bool is_tell1_type(int typ)   {  return typ == tell1::data_type;   }
    inline bool is_tell40_type(int typ)  {  return typ == tell40::data_type;  }

  }      // End namespace event_traits
}        // End namespace Online
#endif   // ONLINE_EVENTDATA_EVENTTRAITS_H
