//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Project    : Online
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_EVENTDATA_MBMEVENTACCESS_H
#define ONLINE_EVENTDATA_MBMEVENTACCESS_H

/// Framework include files
#include <EventHandling/EventAccess.h>
#include <MBM/Requirement.h>
#include <MBM/bmdef.h>

/// Online namespace declaration
namespace Online   {

  /// Class to access events from the buffer manager
  /** @class MBMEventAccess
    *
    * @author M.Frank
    */
  class MBMEventAccess : public EventAccess  {
  public:
    /// The guard type
    template <typename T> class mbm_guard_t;

    /// Pointer to mbmplug device
    typedef std::list<BMID> consumer_collection_t;
    bool                    m_inhibit;
    consumer_collection_t   m_idlePlugs;
    consumer_collection_t   m_busyPlugs;
    std::mutex              m_consumerLock;

    /// Helper class containing the configuration parameters of this object 
    /**
     *
     * \version 1.0
     * \author  M.Frank
     */
    struct mbm_config_t : public EventAccess::config_t  {
      /// Default constructor
      mbm_config_t() = default;
      /// Move constructor
      mbm_config_t(mbm_config_t&& copy) = default;
      /// Copy constructor
      mbm_config_t(const mbm_config_t& copy) = default;
      /// Default destructor
      ~mbm_config_t() = default;
      /// Assignment operator
      mbm_config_t& operator=(mbm_config_t&& copy) = default;
      /// Move assignment operator
      mbm_config_t& operator=(const mbm_config_t& copy) = default;

      /// Property: MBM: Mapped buffer names
      std::vector<std::string>      buffers;
      /// Property: MBM: Requests in string format
      std::vector<std::string>      requests;
      /// Property: MBM: Event input buffer (Default: "Events")
      std::string                   input;
      /// Partition name
      std::string                   partitionName;
      /// Property: Number of parallel MBM connections
      std::size_t                   num_connections;
      /// Property: Number of parallel event access threads
      std::size_t                   num_event_threads;
      /// Partition ID
      int                           partitionID;
      /// Flag to discriminate buffers using the partition name
      bool                          partitionBuffers;
    }  config;

  public:
    /// Create proper buffer name depending on partitioning
    std::string bufferName(const std::string& nam)  const;
    /// Fill the event data cache if necessary
    int fillCache();
    /// Free busy consumer and re-attach to idle queue
    bool requeueConsumer(BMID consumer);

  public:
    /// Default constructors and destructors / inhibits
    ONLINE_COMPONENT_CTORS(MBMEventAccess);

    /// Default constructor
    MBMEventAccess(std::unique_ptr<RTL::Logger>&& logger);
    /// Standard destructor
    virtual ~MBMEventAccess() = default;

    /// Print burst/event information
    virtual void printInfo()  const  override;

    /// Free busy consumer and re-attach to idle queue
    void requeue(BMID consumer);

    /// EventAccess overload: Connect to event data source
    virtual int connect(const std::string& input, 
			const std::vector<std::string>& requests,
			const std::string& proc);
    /// Fill the event data cache if necessary
    virtual int fill_cache()    override;
    /// Cancel all pending I/O requests to the buffer manager
    virtual int cancel()  override;
    /// EventAccess overload: Close connection to event data source
    virtual void close()  override;
  };
}        // End namespace Online
#endif   // ONLINE_EVENTDATA_MBMEVENTACCESS_H
