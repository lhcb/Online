//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Project    : Online
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_EVENTDATA_MACROS_H
#define ONLINE_EVENTDATA_MACROS_H

/// Framework include files

/// Online namespace declaration
namespace Online   {

  enum  {
    ONLINE_ERROR = 0,
    ONLINE_OK = 1,
    ONLINE_NO_EVENT = 2,
    ONLINE_NOT_INITED = 4,
    ONLINE_CANCELLED = 6,
    ONLINE_END_OF_DATA = 8
  };
}        // End namespace Online


#define ONLINE_COMPONENT_CTORS(x)		\
  x() = delete;					\
  x(const x& copy) = delete;			\
  x(x&& copy) = delete;				\
  x& operator=(const x& copy) = delete;		\
  x& operator=(x&& copy) = delete

#endif   // ONLINE_EVENTDATA_MACROS_H
