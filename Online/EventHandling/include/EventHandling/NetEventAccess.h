//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Project    : Online
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_EVENTDATA_NETEVENTACCESS_H
#define ONLINE_EVENTDATA_NETEVENTACCESS_H

/// Framework include files
#define TRANSFER_NS        BoostAsio
#include <NET/Transfer.h>
#include <MBM/Requirement.h>
#include <EventHandling/EventAccess.h>

/// Online namespace declaration
namespace Online   {

  /** @class NetEventAccess
    *
    * @author M.Frank
    */
  class NetEventAccess : public EventAccess  {
  public:
    class net_guard_t;
    typedef TRANSFER_NS::NET NET;

    /// NET Plugin reference
    NET*                 m_netPlug      = 0;
    /// Combined request buffer (valid after initialize)
    MBM::Requirement     m_request;
    /// Time stamp of the last event received
    long                 m_lastEvent    = 0;
    /// Keep track if ream requests are active
    std::atomic<int>     m_rearmActive   {0};
    /// Keep track if the re-subscribe timer was fired or not.
    std::atomic<int>     m_timerActive   {0};
    /// Keep track about the connection status
    std::atomic<int>     m_connected     {0};
    /// Keep track of consecutive connection failures
    std::atomic<int>     m_connectFail   {0};

    /// Helper class containing the configuration parameters of this object 
    /**
     *
     * \version 1.0
     * \author  M.Frank
     */
    struct net_config_t : public EventAccess::config_t  {
      /// Default constructor
      net_config_t() = default;
      /// Move constructor
      net_config_t(net_config_t&& copy) = default;
      /// Copy constructor
      net_config_t(const net_config_t& copy) = default;
      /// Default destructor
      ~net_config_t() = default;
      /// Assignment operator
      net_config_t& operator=(net_config_t&& copy) = default;
      /// Move assignment operator
      net_config_t& operator=(const net_config_t& copy) = default;

      /// Property: NET: Requests in string format
      std::vector<std::string> requests;
      /// Network name of the event source in the form <node>::<utgid>
      std::string              source;
      /// Process name of self (UTGID)
      std::string              process;
      /// Property if a timer should be started to check the reception of events
      int                      maxConnectFail = 0;
      /// Property if a timer should be started to check the reception of events
      int                      eventTimeout = 0;
      /// Property to indicate if a DAQ_CANCEL incident shoud be generated on task death
      bool                     cancelDeath  = true;
    } config;

    /// Routine called to reset the timer.
    void resetEventTimer();
    /// Callback when event source dies
    void taskDead(const std::string& who);
    /// Callback when network data are ready
    void handleData(const std::string& src, size_t size, data_t* buff);
    /// Rearm event data request (Send data request to source)
    virtual int rearm();

  public:
    /// Default constructors and destructors / inhibits
    ONLINE_COMPONENT_CTORS(NetEventAccess);

    /// Default constructor
    NetEventAccess(std::unique_ptr<RTL::Logger>&& logger);
    /// Standard destructor
    virtual ~NetEventAccess() = default;

    /// Interactor overload: stimulus handler routine
    virtual void handle(const CPP::Event& ev)  override;

    /// EventAccess overload: Connect to event data source
    virtual int connect(const std::vector<std::string>& requests);
    /// Fill the event data cache if necessary
    virtual int fill_cache()  override;
    /// Cancel all pending I/O requests to the buffer manager
    virtual int cancel()  override;
    /// EventAccess overload: Close connection to event data source
    virtual void close()  override;
  };
}        // End namespace Online
#endif   // ONLINE_EVENTDATA_NETEVENTACCESS_H
