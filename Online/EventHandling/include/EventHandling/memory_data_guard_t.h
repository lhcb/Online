//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Project    : Online
//
//  Author     : Markus Frank
//==========================================================================
//  MemoryData.h
//==========================================================================
#ifndef EVENTHANDLING_MEMORYDATA_H
#define EVENTHANDLING_MEMORYDATA_H

/// Framework include files
#include <EventHandling/EventAccess.h>
#include <EventHandling/EventTraits.h>

/// C/C++ include files
#include <memory>

/// Online namespace declaration
namespace Online   {

  template <typename T> class memory_data_guard_t : public EventAccess::guard_t {
  public:
    std::unique_ptr<T>    events {};
    event_traits::data_t* buffer = nullptr;

  public:
    memory_data_guard_t(std::unique_ptr<T>&& e, std::vector<int>&& bx, long guard_type, event_traits::data_t* b)
    : guard_t(std::move(bx), guard_type), events(std::move(e)), buffer(b)
    {
      num_evt = bx.size();
    }
    memory_data_guard_t() = delete;
    memory_data_guard_t(memory_data_guard_t&& copy) = delete;
    memory_data_guard_t& operator=(memory_data_guard_t&& copy) = delete;
    memory_data_guard_t& operator=(const memory_data_guard_t& copy) = delete;
    /// Destructor
    virtual ~memory_data_guard_t()    {
      if ( buffer ) delete [] buffer;
    }
    virtual event_traits::record_t at(std::size_t which) const  override {
      return events->at(which);
    }
  };
}      // namespace Online
#endif // EVENTHANDLING_MEMORYDATA_H
