//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Project    : Online
//
//  Author     : M.Frank
//
//==========================================================================

/// Framework include files
#include <EventHandling/EventAccess.h>
#include <EventData/event_header_t.h>
#include <PCIE40Data/pcie40decoder.h>
#include <RTL/rtl.h>

#include <PCIE40Data/pcie40_writer.h>
#define __UNLIKELY( x ) __builtin_expect( ( x ), 0 )

using namespace Online;

namespace {
  static std::size_t MBYTE = 1024UL*1024UL;
  template <typename BANK_TYPE> inline bool checkBank(const BANK_TYPE* bank)  {
    return
      // Check for Tell1 bank magic word or PCIE40 magic word
      (bank->magic() == BANK_TYPE::MagicPattern || bank->magic() == 0xFACE) &&
      (bank->size() >= 0) &&
      (bank->type() >= 0) && 
      (bank->type() < bank_types_t::LastType);
  }

  inline bool checkHeader(const event_header_t* header)  {
    return
      header->size0() > 0 &&
      header->size0() < 100UL*MBYTE &&
      header->size0() == header->size1() &&
      header->size0() == header->size2();
  }

  inline bool match_header(const std::array<unsigned int, 4>& mask,
			   const EventAccess::config_t::header_mask_t& required, 
			   const EventAccess::config_t::header_mask_t& veto)  {
    bool tmatch = false, vmatch = false;
    for( int i=0; i < required.words(); i++ )  {
      tmatch = tmatch | ((required.word(i) & mask.at(i)) != 0);
      vmatch = vmatch | ((veto.word(i)     & mask.at(i)) != 0);
    }
    return vmatch ? false : tmatch;
  }

}

/// Debugging: return type name of the burst ("TELL1" or "PCIE40")
const char* EventAccess::guard_t::type_name()  const  {
  switch( typ )  {
  case event_traits::tell1::data_type:
    return "TELL1";
  case event_traits::tell40::data_type:
    return "PCIE40";
  default:
    return "UNKNOWN";
  }
}

/// Standard constructor
EventAccess::EventAccess(std::unique_ptr<RTL::Logger>&& logger)
  : m_logger(std::move(logger))
{
}

/// Standard destructor
EventAccess::~EventAccess()  {
  m_logger->info("+++ Event access shutdown: "
		 "Burst-in:%10ld out:%8ld req:%8ld rel:%8ld Events-in:%10ld out:%10ld MDF:%10ld buffered:%4ld",
		 monitor.burstsIn,      monitor.burstsOut,
		 monitor.burstsRequest, monitor.burstsRelease, 
		 monitor.eventsIn,      monitor.eventsOut,
		 monitor.eventsMDF,     monitor.eventsBuffered);
}

/// Configure preselection using event header: VETO mask
void EventAccess::setHeaderVetoMask(const std::vector<uint32_t>& mask)  {
  base_config->headerVetoMask.clear();
  for( std::size_t i=0; i<std::min(4UL, mask.size()); ++i )
    base_config->headerVetoMask.setWord(i, mask[i]);
  for( std::size_t i=std::min(4UL, mask.size()); i<4UL; ++i )
    base_config->headerVetoMask.setWord(i, 0x0);
  if( mask.size() != 4UL )  {
    m_logger->info("+++ VETO    Mask: Size: %ld Used %ld words",
		   mask.size(), std::min(4UL, mask.size()));
  }
}

/// Configure preselection using event header: TRIGGER mask
void EventAccess::setHeaderTriggerMask(const std::vector<uint32_t>& mask)  {
  base_config->headerTriggerMask.clear();
  for( std::size_t i=0; i<std::min(4UL, mask.size()); ++i )
    base_config->headerTriggerMask.setWord(i, mask[i]);
  for( std::size_t i=std::min(4UL, mask.size()); i<4UL; ++i )
    base_config->headerTriggerMask.setWord(i, ~0x0);
  if( mask.size() != 4UL )  {
    m_logger->info("+++ TRIGGER Mask: Size: %ld Used %ld words",
		   mask.size(), std::min(4UL, mask.size()));
  }
}

/// Print burst/event information
void EventAccess::printInfo()  const  {
  if( (monitor.burstsIn%base_config->burstPrintCount) == 0 )  {
    m_logger->info("+++ Burst-in:%10ld out:%8ld req:%8ld rel:%8ld Events-in:%10ld out:%10ld MDF:%10ld buffered:%4ld",
		   monitor.burstsIn,      monitor.burstsOut,
		   monitor.burstsRequest, monitor.burstsRelease, 
		   monitor.eventsIn,      monitor.eventsOut,
		   monitor.eventsMDF,     monitor.eventsBuffered);
  }
}

/// Insert new entry into event queue with lock protection
std::size_t EventAccess::queueBurst(shared_guard_t&& burst)  {
  if( !burst.get() )  {
    m_logger->warning("+++ queueBurst: Invaliud burst encountered -> skip.");
    return monitor.eventsBuffered;
  }
  {
    lock_t lock(m_eventLock);
    monitor.eventsIn        += burst->num_bx();
    monitor.eventsBuffered  += burst->num_bx();
  }
  {
    lock_t lock(m_burstLock);
    m_bursts.emplace_back(std::move(burst));
    ++monitor.burstsIn;
  }
  m_haveData.notify_one();
  return monitor.eventsBuffered;
}

/// Dequeue event if one is present with lock protection
EventAccess::shared_guard_t EventAccess::dequeueBurst()  {{
    lock_t lock(m_burstLock);
    if( !m_bursts.empty() )  {
      shared_guard_t burst(m_bursts.front());
      m_bursts.pop_front();
      ++monitor.burstsOut;
      return burst;
    }
  }
  return shared_guard_t();
}

/// Access thread safe number of queued bursts
std::size_t EventAccess::numQueuedBurst()  {
  lock_t lock(m_burstLock);
  return m_bursts.size();
}

/// Clear burst queue and update counters
std::size_t EventAccess::clear()  {
  std::size_t count = 0;
  pcie40::decoder_t::reset_last_good_run_number();
  for( shared_guard_t burst=dequeueBurst(); burst.get(); burst=dequeueBurst() )  {
    lock_t lock(m_eventLock);
    monitor.eventsBuffered -= burst->num_bx();
    count += burst->num_bx();
  }
  return count;
}

/// Dequeue event if present, otherwise wait for producer to emplace one.
EventAccess::shared_guard_t EventAccess::waitBurst()  {
  shared_guard_t burst(dequeueBurst());
  if( !burst.get() )  {
    while ( m_bursts.empty() )  {
    WaitBurst:
      std::unique_lock<std::mutex> lock(m_dataLock);
      std::cv_status sc = m_haveData.wait_for(lock, std::chrono::milliseconds(m_eventPollTMO));
      if( sc == std::cv_status::no_timeout )  {
	burst = dequeueBurst();
	if( !burst && !isCancelled() ) continue;
	return burst;
      }
      if( isCancelled() )  {
	return burst;
      }
    }
    burst = dequeueBurst();
    if( !burst )  {
      if( isCancelled() )  {
	m_logger->info("+++ waitBurst: Event request was cancelled.");
	return burst;
      }
      m_logger->warning("+++ waitBurst: Requeue request!");
      goto WaitBurst;
    }
  }
  return burst;
}

/// Dequeue event if one is present
EventAccess::event_t EventAccess::dequeueEvent(shared_guard_t& loop_context)  {
  if( loop_context )  {
    int32_t bxid = loop_context->next_bx();
    if( bxid >= 0 )  {
      event_t event = { bxid, loop_context };
      // If we used up the burst, reset it.
      if( loop_context->empty() ) loop_context.reset();
      // Return the event to the user
      if( event.second.get() )  {
	lock_t lock(m_eventLock);
	++monitor.eventsOut;
	--monitor.eventsBuffered;
      }
      return event;
    }
    loop_context.reset();
  }
  return event_t(0, shared_guard_t());
}

/// Dequeue event if present, otherwise wait for producer to emplace one.
EventAccess::event_t EventAccess::waitEvent(shared_guard_t& loop_context)  {
  event_t event(dequeueEvent(loop_context));
  if( !event.first )  {
    while( !loop_context )  {
      shared_guard_t tmp(waitBurst());
      if( tmp.get() )  {
	loop_context = tmp;
	event = dequeueEvent(loop_context);
	if( !event.first )  {
	  m_logger->error("+++ Encountered empty EVENT!");
	  int halt = 1;
	  while(halt)  {
	    ::lib_rtl_sleep(100); 
	  }
	}
	return event;
      }
      // If cancelled and no bursts queued, we end here and stop processing
      if( m_cancelled ) break;
      m_logger->warning("+++ waitEvent: Encountered empty MDF burst!");
    }
  }
  // This only happens when an "end-marker" is injected and we are already cancelled
  return event;
}

/// The event is a PCIE40 MEP structure with multiple events, which must be decoded
std::pair<std::vector<int>, std::unique_ptr<pcie40::event_collection_t> >
EventAccess::convertPCIE40MEP(datapointer_t start, std::size_t len)  {
  using namespace pcie40;
  struct _printer  {
    RTL::Logger& logger;
    _printer(RTL::Logger& e) : logger(e) {}
    /// Printout operator
    void operator()(int lvl, const char* source, const char* msg)  {
      logger.printmsg(lvl, source, msg);
    }
  } printer(*m_logger);
  std::pair<std::vector<int>, std::unique_ptr<pcie40::event_collection_t> > events;
  pcie40::decoder_t::logger_t logger = printer;
  pcie40::decoder_t           decoder(logger);
  datapointer_t end = start + len;
  
  if( end > start )  {
    static constexpr std::size_t max_bank    = 20000;
    static constexpr std::size_t max_source  = 2000;
    static constexpr std::size_t max_packing = 35000;
    const auto* pmep_hdr = reinterpret_cast<const pcie40::mep_header_t*>(start);
    if( 0 == pmep_hdr->size )  {
      m_logger->error("convertMEP: MEP with invalid size encountered: %d", pmep_hdr->size);
      decoder.initialize(events.second,0);
      return events;
    }
    if( 0 == pmep_hdr->num_source || pmep_hdr->num_source > max_source )  {
      m_logger->error("convertMEP: MEP with invalid number of sources encountered: %d",
		      pmep_hdr->num_source);
      decoder.initialize(events.second,0);
      return events;
    }
    uint32_t packing = pmep_hdr->multi_fragment(0)->header.packing;
    if( 0 == packing || packing > max_packing )  {
      decoder.initialize(events.second,0);
      try {
	throw std::runtime_error("convertMEP: MEP with invalid packing factor encountered");
      }
      catch(...)  {
	m_logger->error("convertMEP: MEP with invalid packing factor encountered: %d", packing);
      }
      return events;
    }
    std::size_t total_odin = 0;
    long        ev_id = 0, num_bx = 0, num_events = 0;
    /// Header checks done. Now decode the buffer
    decoder.decode(events.second, pmep_hdr);
    /// When decoding the sequence of beam-crossings we need to know about TAE crossings.....
    events.first.reserve(events.second->size());
    for( ev_id=0, num_bx=0, num_events=events.second->size(); ev_id < num_events; ++ev_id )  {
      auto* ev_wrt = reinterpret_cast<pcie40::event_collection_writer_t*>(events.second.get());
      auto* ev_ptr = ev_wrt->at(ev_id);
      std::size_t num_coll = ev_ptr->num_bank_collections();
      std::size_t num_bank = 0;
      std::size_t num_odin = 0;
      std::size_t num_bad_bank = 0;
      for( std::size_t j=0; j<num_coll; ++j )  {
	auto* coll = ev_ptr->bank_collection(j);
	for( const auto* b=coll->begin(); b != coll->end(); b=coll->next(b) )  {
	  if( b->magic() != pcie40::bank_t::MagicPattern )  {
	    m_logger->error("convertMEP: Bad magic pattern in raw bank! --> Skip");
	    ++num_bad_bank;
	  }
	  else if( b->type() == pcie40::bank_t::ODIN )  {
	    ++num_odin;
	  }
	  ++num_bank;
	}
	for( const auto* b=coll->special_begin(); b != coll->special_end(); b=coll->next(b) )  {
	  if( b->magic() != pcie40::bank_t::MagicPattern )  {
	    m_logger->error("convertMEP: Bad magic pattern in raw bank! --> Skip");
	    ++num_bad_bank;
	  }
	  ++num_bank;
	}
      }
      if( num_bank == 0 )  {
	++monitor.eventsNoBanks;
	m_logger->error("convertMEP: Event with ZERO banks encountered --> Skip");
	continue;
      }
      else if( num_bank > max_bank )  {
	++monitor.eventsMaxBanks;
	m_logger->error("convertMEP: Event with TOO MANY banks encountered --> Skip");
	continue;
      }
      else if( num_bad_bank > 0 )  {
	++monitor.eventsBadBanks;
	m_logger->error("convertMEP: Event with BAD bank(s) encountered --> Skip");
	continue;
      }
      else if( num_odin < 1 )  {
	++monitor.eventsNoODIN;
	m_logger->debug("convertMEP: Event with no ODIN bank encountered --> Skip");
	continue;
      }
      else  {
	++total_odin;
      }
      /// Normal NON-TAE events. Check if they should be ignored and only TAE should be selected
      if( !ev_ptr->is_tae() )  {
	if( !base_config->onlyTAE )  {
	  events.first.emplace_back(ev_id);
	  ++num_bx;
	}
	continue;
      }
      /// If we supposedly ignore TAEs, increase event counter and continue
      /// This switch simply handles everything as a single crossing
      if( !base_config->expandTAE )  {
	if( !base_config->ignoreTAE )  {
	  ev_ptr->flags.detail.tae_first   = 0;
	  ev_ptr->flags.detail.tae_entry   = 0;
	  ev_ptr->flags.detail.tae_central = 0;
	  events.first.emplace_back(ev_id);
	  ++num_bx;
	}
	continue;
      }
      /// TAE: Only keep the central crossing
      bool central = ev_ptr->is_tae_central();
      if( central )  {
	long window  = ev_ptr->tae_half_window();
	/// TAE mode: If ignore TAE, drop this event
	if( base_config->ignoreTAE )  {
	  continue;
	}
	/// TAE mode: Check for specialized edge handling
	bool contains_edge = (ev_id-window < 0) || (ev_id+window >= num_events);
	/// Drop edges at the start/end of a MEP if requested
	if( contains_edge && base_config->dropEdgesTAE )  {
	  continue;
	}
	/// Convert edges at the start/end of a MEP to regulare crossings
	else if( contains_edge && base_config->convertEdgesTAE )  {
	  long num_evts = std::min(ev_id+window,num_events);
	  for( long i=std::max(ev_id-window,0L); i <= num_evts; ++i )  {
	    auto* ev = ev_wrt->at(i);
	    ev->flags.detail.tae_first   = 0;
	    ev->flags.detail.tae_entry   = 0;
	    ev->flags.detail.tae_central = 0;
	    events.first.emplace_back(i);
	  }
	  continue;
	}
	/// TAE mode: regular TAE central crossing
	events.first.emplace_back(ev_id);
	++num_bx;
	continue;
      }
    }
    if( total_odin != packing )  {
      m_logger->error("convertMEP: Dropped %ld out of %ld events without ODIN bank.",
		      long(packing-total_odin), long(packing));
    }
    m_logger->debug("convertMEP: Decoded %ld events packing:%ld with %ld Bx(s).",
		    long(packing), num_events, num_bx);
    return events;
  }
  m_logger->error("convertMEP: Encountered empty MDF burst!");
  decoder.initialize(events.second,0);
  return events;
}

/// The event is a MDF with multiple events, which must be decoded
std::pair<std::vector<int>, std::unique_ptr<event_traits::event_collection_t> >
EventAccess::convertMultiMDF(datapointer_t startptr, std::size_t len)  {
  std::pair<std::vector<int>, std::unique_ptr<event_collection_t> > events;
  datapointer_t start = startptr, end = startptr + len;

  events.second = std::make_unique<event_collection_t>();
  if( end > start )  {
    int ievt = 0;
    while ( start < end )  {
      const auto* header = (const event_header_t*)start;
      try  {
	bool                 skip      = false;
	bool                 stop      = false;
	const char*          err_msg   = nullptr;
	const bank_header_t* err_bank  = nullptr;
	const bank_header_t* odin      = nullptr;
	raw_bank_offline_t*  last_bank = nullptr;
	auto [evt_start, evt_end]      = header->data_frame();
	
	/// Check bank's magic word and check on bank type and length
	if( !checkHeader(header) )  {
	  err_msg = "Event with BAD header bank encountered";
	  stop = true;
	}
	/// Check if the event header matches the required trigger/veto mask
	else if( !match_header(event_header_t::SubHeader(header).H1->triggerMask(),
				base_config->headerTriggerMask,
				base_config->headerVetoMask) )  {
	  start += header->size0();
	  continue;
	}
	else if( base_config->verifyBanks || base_config->onlyTAE )  {
	  std::size_t num_banks = 0;
	  std::size_t max_banks = 20000;
	  raw_bank_offline_t*  curr_bank = nullptr;

	  last_bank = nullptr;
	  while(evt_start < evt_end && num_banks < (max_banks+1) )  {
	    curr_bank = (raw_bank_offline_t*)evt_start;
	    if( !checkBank(curr_bank) )  {
	      err_msg  = "Event with BAD bank encountered";
	      ++monitor.eventsBadBanks;
	      err_bank = curr_bank;
	      skip     = true;
	      break;
	    }
	    if( base_config->onlyTAE )  {
	      if( curr_bank->type() == bank_types_t::ODIN && curr_bank->version() >= 7 )  {
		if( !curr_bank->begin<pcie40::sodin_t>()->is_tae() )  {
		  skip = true;
		}
	      }
	    }
	    if ( curr_bank->type() == bank_types_t::ODIN )  {
	      odin = curr_bank;
	    }
	    else if( curr_bank->type() == bank_types_t::TAEHeader )  {
	      max_banks = 2000 * curr_bank->size()/(3*sizeof(int));
	    }
	    evt_start += curr_bank->totalSize();
	    last_bank = curr_bank;
	    ++num_banks;
	  }
	  if( num_banks == 0 )  {
	    ++monitor.eventsNoBanks;
	    err_msg = "Event with ZERO banks encountered";
	    skip = true;
	  }
	  else if( num_banks > max_banks )  {
	    ++monitor.eventsMaxBanks;
	    err_msg = "Event with TOO MANY banks encountered";
	    skip = true;
	  }
	}
	if( err_msg || err_bank )  {
	  m_logger->error("SKIP EVENT: Buffer:%p Start:%p End:%p Size:(%d,%d,%d) HdrVsn:%u %s %s",
			  (void*)startptr, (void*)start, (void*)evt_end, header->size0(),
			  header->size1(), header->size2(), header->headerVersion(),
			  skip ? "---> event ignored" : "", stop ? "---> STOP decoding" : "");
	  if( err_msg )  {
	    m_logger->error("   %s", err_msg);
	  }
	  if( last_bank )  {
	    m_logger->error("   Last good Bank:   %p  -> %s",
			    (void*)last_bank, event_print::bankHeader(last_bank).c_str());
	  }
	  if( err_bank )  {
	    m_logger->error("   Current bad Bank: %p  -> %s",
			    (void*)err_bank, event_print::bankHeader(err_bank).c_str());
	  }
	  auto header_lines = event_print::headerData(header);
	  for( const auto& l : header_lines )
	    m_logger->error("  %s", l.c_str());
	  if ( odin )  {
	    auto o = std::make_pair(odin, lib_rtl_add_ptr(odin, sizeof(*odin)));
	    auto odin_lines = event_print::odin_data(o);
	    for( const auto& l : odin_lines )
	      m_logger->error("  |%s", l.c_str());    
	  }
	  if( stop )  {
	    return events;
	  }
	}
	else if( !skip )  {
	  events.second->emplace_back(header);
	  events.first.emplace_back(ievt);
	  ++ievt;
	}
	++monitor.eventsMDF;
	start += header->size0();
      }
      catch(...)  {
	m_logger->error("SKIP EVENT: Buffer:%p Start:%p End:%p Size:(%d,%d,%d) HdrVsn:%u",
			(void*)startptr, (void*)start, (void*)end, header->size0(),
			header->size1(), header->size2(), header->headerVersion());
	start = end;
      }
    }
    return events;
  }
  m_logger->error("convertMultiMDF: Encountered empty MDF burst!");
  return events;
}
