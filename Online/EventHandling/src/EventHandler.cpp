//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Project    : Online
//
//  Author     : Markus Frank
//==========================================================================

/// Framework include files
#include <EventHandling/EventHandler.h>
#include <EventHandling/EventTraits.h>
#include <EventHandling/EventOutput.h>
#include <EventData/odin_t.h>
#include <EventData/event_header_t.h>
#include <EventData/raw_bank_offline_t.h>
#include <PCIE40Data/pcie40decoder.h>
#include <CPP/Value.h>

/// C/C++ include files
#include <exception>
#include <stdexcept>
#include <vector>

using namespace Online;

namespace {

  void _update_mask(uint32_t* m, const EventHandler::mask_t& mask)
  {
    if ( mask.size() == 1 )  {
      m[3] = mask[0];
    }
    else if ( mask.size() == 2 )  {
      m[3] = mask[0];
      m[2] = mask[1];
    }
    else if ( mask.size() == 3 )  {
      m[3] = mask[2];
      m[2] = mask[1];
      m[1] = mask[0];
    }
    else if ( mask.size() >= 4 )  {
      m[3] = mask[3];
      m[2] = mask[2];
      m[1] = mask[1];
      m[0] = mask[0];
    }
  }

  void _update_mask_or(uint32_t* m, const EventHandler::mask_t& mask)
  {
    if ( mask.size() == 1 )  {
      m[3] |= mask[0];
    }
    else if ( mask.size() == 2 )  {
      m[3] |= mask[0];
      m[2] |= mask[1];
    }
    else if ( mask.size() == 3 )  {
      m[3] |= mask[2];
      m[2] |= mask[1];
      m[1] |= mask[0];
    }
    else if ( mask.size() >= 4 )  {
      m[3] |= mask[3];
      m[2] |= mask[2];
      m[1] |= mask[1];
      m[0] |= mask[0];
    }
  }

  template<typename odin_type>
  void _set_event_header(event_header_t*  ev_hdr,
			 std::size_t      len,
			 const odin_type& odin,
			 uint32_t*        mask)
  {
    ev_hdr->setHeaderVersion(event_header_t::CURRENT_VERSION);
    ev_hdr->setSpare(0);
    ev_hdr->setChecksum(0);
    ev_hdr->setCompression(0);
    ev_hdr->setSize(len);
    ev_hdr->setDataType(event_header_t::BODY_TYPE_BANKS);
    ev_hdr->setSubheaderLength(sizeof(event_header_t::Header1));

    auto* sub_hdr = ev_hdr->subHeader().H1;
    if ( !odin.first )   {
      sub_hdr->setRunNumber(0);
      sub_hdr->setOrbitNumber(0);
      sub_hdr->setBunchID(0);
    }
    else if ( odin.first->version() < 7 )   {
      const auto* o = reinterpret_cast<const run2_odin_t*>(odin.second);
      sub_hdr->setRunNumber(o->run_number());
      sub_hdr->setOrbitNumber(o->orbit_id());
      sub_hdr->setBunchID(o->bunch_id());
    }
    else    {
      const auto* o = reinterpret_cast<const run3_odin_t*>(odin.second);
      sub_hdr->setRunNumber(o->run_number());
      sub_hdr->setOrbitNumber(o->orbit_id());
      sub_hdr->setBunchID(o->bunch_id());
    }
    sub_hdr->setTriggerMask(mask);
  }
  

  std::unique_ptr<uint8_t []> _make_tmp_odin_pcie40()   {
    std::size_t len = sizeof(raw_bank_offline_t)+sizeof(run3_odin_t)+sizeof(uint32_t); // 32 bit padding
    std::unique_ptr<uint8_t[]> tmp(new uint8_t[len]);
    ::memset(tmp.get(), 0, len);
    auto* b = (raw_bank_offline_t*)tmp.get();
    b->setMagic();
    b->setVersion(0);
    b->setSourceID(0);
    b->setType(bank_types_t::ODIN);
    b->setSize(sizeof(run3_odin_t));
    b->begin<run3_odin_t>()->_run_number = pcie40::decoder_t::last_good_run_number();
    return tmp;
  }

  std::size_t _event_length(const EventHandler::evt_data_t& event)
  {
    std::size_t len = 0;
    for(const auto& bank : event) len += bank.first->totalSize();
    return len;
  }
  
  event_traits::record_t _record(const EventHandler::evt_desc_t& event)
  {
    return event_traits::record_t(event.second->at(event.first));
  }

  std::size_t output_bank(const std::unique_ptr<RTL::Logger>& logger,
			  EventOutput::transaction_t*         tr,
			  const bank_header_t*                header,
			  const void*                         data)
  {
    raw_bank_offline_t* bank  = tr->get<raw_bank_offline_t>();
    std::size_t start = tr->length;
    std::size_t len   = header->totalSize();
    ::memcpy( (void*)bank,  header, bank_header_t::hdrSize());
    ::memcpy( bank->data(), data, len-bank_header_t::hdrSize());
    bank->setMagic();
    tr->advance(len);
    if ( tr->length-start != len )   {
      logger->error("output_bank: Failed to commit data!");
    }
    return len;
  }

  std::size_t output_bank(const std::unique_ptr<RTL::Logger>& logger,
			  EventOutput::transaction_t*    tr,
			  const std::pair<const bank_header_t*, const void*>& dsc)
  {
    const bank_header_t* hdr = dsc.first;
    switch (hdr->magic())   {
    case pcie40::bank_t::MagicPattern:
    case raw_bank_offline_t::MagicPattern:
      return output_bank(logger, tr, hdr, dsc.second);
    default:
      logger->warning("Bad bank: %d %ld transaction length:%ld",
		      int(hdr->type()), hdr->totalSize(), tr->length);
      break;
    }
    return 0;
  }

  const char* _mdf_error_string(int32_t code)   {
    switch(code)    {
    case EventHandler::NO_MDF_ERROR:
      return "Success";
    case EventHandler::INVALID_MDF_FORMAT:
      return "INVALID_MDF_FORMAT";
    case EventHandler::INVALID_MAGIC_PATTERN:
      return "INVALID_MAGIC_PATTERN";
    case EventHandler::TOO_MANY_BANKS:
      return "TOO_MANY_BANKS";
    case EventHandler::INVALID_BANK_SIZE:
      return "INVALID_BANK_SIZE";
    default:
      return "Unknown error";
    }
  }
}

/// Online namespace declaration
namespace Online  {

  template <typename T> T EventHandler::mdf_error_report(T return_value, int32_t code)  const {
    m_logger->error("Bank error: %s -- action aborted.", _mdf_error_string(code));
    return return_value;
  }

  std::string EventHandler::_bx_offset(int32_t bx)   const   {
    std::string offset;
    int32_t cr = std::abs(bx);
    if ( cr > 99 ) offset += char('0'+((cr/100)%100));
    if ( cr > 9  ) offset += char('0'+((cr/10)%10));
    offset += char('0'+(cr%10));
    return offset;
  }

  /// Check integer array for the occurrence of an item
  bool EventHandler::type_found(int required_type, const std::vector<int>& good_types)  const
  {
    if ( good_types.empty() )
      return true;
    for( const int& i : good_types )  {
      if( required_type == i )
	return true;
    }
    return false;
  }

  /// Access to ODIN bank from the event record
  std::pair<const bank_header_t*, const void*> EventHandler::get_odin_bank(const event_header_t* ev)  const
  {
    if ( ev && ev->is_mdf() )   {
      auto data = ev->data_frame();
      while ( data.start < data.end )   {
	const raw_bank_offline_t* bank = (raw_bank_offline_t*)data.start;
	if ( bank->type() == bank_types_t::ODIN )
	  return {bank, bank->data()};
	std::size_t len = bank->totalSize();
	data.start += len;
	// Protect against infinite loop in case of data corruption
	if ( 0 == len ) break;
      }
    }
    return std::make_pair(nullptr, nullptr);
  }

  /// Access to ODIN bank from the event record
  std::pair<const bank_header_t*, const void*> EventHandler::get_odin_bank(const evt_desc_t& event)  const
  {
    const auto record = _record(event);
    switch(event.second->type())   {
    case event_traits::tell1::data_type:  {
      return get_odin_bank(record.tell1_event);
    }
    case event_traits::tell40::data_type:   {
      const auto* bc = record.tell40_event->bank_collection(pcie40::params::collection_id_ODIN);
      const auto* ba = (bc && bc->num_banks() > 0) ? bc->at(0) : nullptr;
      return ba ? std::make_pair(ba, ba->data()) : std::make_pair(nullptr, nullptr);
    }
    default:
      m_logger->except("EventHandler","+++ Failed to access ODIN [Invalid event type].");
    }
    return std::make_pair(nullptr, nullptr);
  }

  /// Check if the event is part of a TAE frame
  std::pair<uint32_t, bool> EventHandler::check_tae(const evt_desc_t& event)  const
  {
    auto record = _record(event);
    switch(event.second->type())   {
    case event_traits::tell1::data_type:  {
      const raw_bank_offline_t* bank = record.tell1_event->data<raw_bank_offline_t>();
      if ( bank->magic() == raw_bank_offline_t::MagicPattern &&
	   bank->type()  == bank_types_t::TAEHeader )   {
	uint32_t count_bx = bank->size()/sizeof(int)/3;
	uint32_t window   = (count_bx-1)/2;
	return {window, true};
      }
      const auto odin = this->get_odin_bank(event);
      if ( odin.first )    {
	if ( odin.first->version() < 7 )   {
	  const auto* o = reinterpret_cast<const run2_odin_t*>(odin.second);
	  return {o->tae_window(), o->tae_central()};
	}
	const auto* o = reinterpret_cast<const run3_odin_t*>(odin.second);
	return {o->tae_half_window(), o->tae_central()};
      }
      m_logger->except("EventHandler","+++ Detected event without ODIN bank! [Missing bank]");
      return std::make_pair(0,false);
    }
    case event_traits::tell40::data_type:   {
      const auto* e = record.tell40_event;
      return e ? std::make_pair(e->tae_half_window(),e->is_tae_central()) : std::make_pair(uint8_t(0),false);
    }
    default:
      m_logger->except("EventHandler","+++ Failed to check for TAE [Invalid event type].");
    }
    return std::make_pair(0,false);
  }

  /// Create raw event object from banks
  std::pair<int32_t, EventHandler::evt_data_t> 
  EventHandler::create_event_data(const evt_desc_t& event, int32_t bx)  const
  {
    switch(event.second->type())   {
    case event_traits::tell1::data_type:  {
      const auto  rec = _record(event);
      auto frame = rec.tell1_event->data_frame();
      raw_bank_offline_t* bank = (raw_bank_offline_t*)frame.start;
      if ( bank->type() == bank_types_t::TAEHeader ) {   // Is it the TAE bank?
	const int* data = bank->begin<int>();
	int32_t count_bx = bank->size()/sizeof(int)/3;
	int32_t window   = (count_bx-1)/2;
	for(int32_t i=-window; i<=window; ++i)   {
	  int32_t idx = i+window;
	  int32_t crossing = data[3*idx];
	  if ( crossing == bx )  {
	    int32_t off = data[3*idx+1];
	    int32_t len = data[3*idx+2];
	    frame.start += bank->totalSize() + off;
	    return this->extract_tell1_banks(frame.start, frame.start+len);
	  }
	}
	m_logger->except("create_event_data: Wrong BX requested: %d [%d < bx < %d]",
			 bx, -window, window);
	/// Will never get here: exception!
      }
      return this->extract_tell1_banks(frame.start, frame.end);
    }

    case event_traits::tell40::data_type:   {
      const auto  rec = _record({event.first+bx, event.second});
      const auto& evt = *rec.tell40_event;
      evt_data_t evt_data;
      evt_data.reserve(512);
      for(std::size_t i=0; i < evt.num_bank_collections(); ++i)  {
	const auto* bc = evt.bank_collection(i);
	for(const auto* b=bc->begin(); b != bc->end(); b = bc->next(b))
	  evt_data.emplace_back((bank_header_t*)b, b->data());
	for(const auto* b=bc->special_begin(); b != bc->special_end(); b = bc->next(b))
	  evt_data.emplace_back((bank_header_t*)b, b->data());
      }
      return std::make_pair(NO_MDF_ERROR, std::move(evt_data));
    }

    default:
      m_logger->except("EventHandler","+++ Failed to access event structure [Invalid event type].");
    }
    return std::pair<int,evt_data_t>(INVALID_MDF_FORMAT,{});
  }

  /// Put bank container to specified location in the TES
  void EventHandler::put_event(const std::string& loc, const evt_desc_t& event, int32_t eid)  const
  {
    auto evt (this->create_event_data({event.first+eid, event.second}, 0));
    put_banks(loc, std::move(evt.second));
  }
  
  /// Output TAE frame in MDF format
  template <>
  std::size_t EventHandler::output_tae<event_traits::tell1>(const evt_desc_t& event,
							    mask_t&           mask,
							    EventOutput&      output)       const
  {
    const event_traits::record_t record = _record(event);
    const event_traits::tell1::event_type* evt = record.tell1_event;
    const raw_bank_offline_t* bank = evt->data<raw_bank_offline_t>();
    std::size_t data_len = evt->size0();

    /// We have here directly a TAE frame. Just copy it to the output
    if ( bank->type() == bank_types_t::TAEHeader ) {   // Is it the TAE bank?
      auto*   tr       = output.getTransaction(data_len);
      auto*   ev_hdr   = tr->get<event_header_t>();
      auto    daq      = ev_hdr->subHeader().H1->triggerMask();
      uint32_t m[] = { daq[0], daq[1], daq[2], daq[3] };

      _update_mask(m, mask);
      ::memcpy(tr->pointer(), record.data, data_len);
      ev_hdr->subHeader().H1->setTriggerMask(m);
      tr->advance(data_len);
      if ( !output.end(tr) )   {
	m_logger->except("output_tae: Failed to commit event output!");
      }
      return data_len;
    }
    auto data = this->create_event_data(event, 0);
    if ( data.first != NO_MDF_ERROR )   {
      m_logger->error("Bank error: %s -- action aborted.", _mdf_error_string(data.first));
      return 0;
    }
    std::size_t num_odin = 0;
    std::pair<const bank_header_t*, const void*> *odin = nullptr, tae;
    for( auto& b : data.second )  {
      if ( b.first->type() == bank_types_t::TAEHeader )  {
	tae = b;
      }
      else if ( b.first->type() == bank_types_t::ODIN )  {
	++num_odin;
	odin = &b;
      }
    }
    const auto* o = reinterpret_cast<const run3_odin_t*>(odin->second);
    if ( odin->first && odin->first->version() >= 7 && o->tae_window() > 0 )   {
      m_logger->warning("output_tae: %s and %ld ODIN banks. TAE window: %d TAE central: %s [MEP overlap]",
			"Got TAE event without TAE bank", num_odin, o->tae_window(),
			o->tae_central() ? "YES" : " NO");
    }
    return this->output_mdf<event_traits::tell1>(data.second, mask, true, output);
  }
  
  /// Output TAE frame in MDF format
  template <>
  std::size_t EventHandler::output_tae<event_traits::tell40>(const evt_desc_t& event,
							     mask_t&           mask,
							     EventOutput&      output)       const
  {
    typedef event_traits::tell40 traits;
    if( output.isCancelled() )  {
      return 0;
    }
    else if ( mask.empty() )  {
    }
    bool          dbg = false;
    auto          eid = event.first;
    const auto& guard = event.second;
    const auto   odin = this->get_odin_bank(event);
    if ( odin.first )    {
      auto [half_window, central] = this->check_tae(event);
      if ( central )   {
	if ( eid >= half_window && event.second->num_bx()-eid > half_window )   {
	  std::size_t total_len = 0, tae_len = 0, checked_len = 0;
	  std::vector<std::size_t> subevt_len;
	  std::vector<pcie40::event_t*> subevents;
	  const std::size_t hdr_len = event_header_t::sizeOf(event_header_t::CURRENT_VERSION);
	  for(int32_t hw=half_window, iw=-hw; iw <= hw; ++iw)   {
	    auto   evt = guard->at(eid+iw);
	    std::size_t len = evt.tell40_event->total_length();
	    subevents.emplace_back(evt.tell40_event);
	    subevt_len.emplace_back(len);
	    total_len += len;
	  }
	  tae_len   = bank_header_t::hdrSize()+subevents.size()*3*sizeof(int);
	  total_len = total_len + hdr_len + tae_len;

	  auto* tr = output.getTransaction(total_len);
	  if ( !tr )   {
	    m_logger->except("Event output failed [Cannot reserve output space]");
	  }
	  std::size_t buffer_position = tr->used_bytes();
	  uint32_t m[] = { ~0x0U, ~0x0U, ~0x0U, ~0x0U };
	  _update_mask(m, mask);

	  auto* ev_hdr = tr->get<event_header_t>();
	  _set_event_header(ev_hdr, total_len-hdr_len, odin, m);
	  tr->advance(hdr_len);
	  checked_len += hdr_len;
	  
	  auto* tae_bnk = tr->get<raw_bank_offline_t>();
	  tae_bnk->setMagic();
	  tae_bnk->setVersion(0);
	  tae_bnk->setSourceID(0);
	  tae_bnk->setType(bank_types_t::TAEHeader);
	  tae_bnk->setSize(subevents.size()*3*sizeof(int));
	  off_t offset = 0;
	  auto* data   = tae_bnk->begin<int32_t>();
	  for(int32_t hw=half_window, i=-hw; i <= hw; ++i)   {
	    *data++  = i;
	    *data++  = offset;
	    *data++  = subevt_len[i+half_window];
	    offset  += subevt_len[i+half_window];
	  }
	  tr->advance(tae_bnk->totalSize());
	  checked_len += tae_bnk->totalSize();
	  if ( dbg )   {
	    if ( tae_bnk->totalSize() != int(tae_len) )   {
	      m_logger->warning("output_tae: inconsistent TAE bank length: %ld != %ld",
				tae_len, tae_bnk->totalSize());
	    }
	  }
	  for(int32_t hw=half_window, iw=-hw; iw <= hw; ++iw)   {
	    std::size_t bank_len, sub_len = 0;
	    auto evt = this->create_event_data(event, iw);
	    if ( evt.first != NO_MDF_ERROR )    {
	      m_logger->warning("output_tae: Invalid TAE subevent at position %d --> drop",iw);
	      tr->position(buffer_position);
	      return 0;
	    }
	    for( auto& dsc : evt.second )   {
	      auto* bank = dsc.first;
	      if ( bank->magic() != traits::bank_type::MagicPattern )   {
		m_logger->warning("Bad bank: %d %ld transaction length:%ld",
				  int(bank->type()), bank->totalSize(), tr->length);
		continue;
	      }
	      bank_len     = output_bank(m_logger, tr, bank, dsc.second);
	      sub_len     += bank_len;
	      checked_len += bank_len;
	      if ( dbg )   {
		if ( checked_len != tr->length )   {
		  m_logger->warning("output_tae: inconsistent transaction length: %ld != %ld",
				    checked_len, tr->length);
		}
	      }
	    }
	    if ( dbg )   {
	      if ( sub_len != subevt_len[iw+half_window] )    {
		m_logger->warning("output_tae: inconsistent subevent length: %ld != %ld",
				  sub_len, subevt_len[iw+half_window]);
	      }
	    }
	  }
	  if ( checked_len != ev_hdr->recordSize() )   {
	    m_logger->warning("output_tae: inconsistent frame length: %ld != %ld",
			      checked_len, ev_hdr->recordSize());
	  }
	  if ( !output.end(tr) )   {
	    m_logger->except("Failed to commit event output!");
	  }
	  return total_len;
	}
      }
    }
    return 0;
  }
  
  /// Output TAE frame in MDF format
  template <>
  std::size_t EventHandler::output_tae<void>(const evt_desc_t& event,
					     mask_t&           mask,
					     EventOutput&      output)       const
  {
    switch(event.second->type())   {
    case event_traits::tell1::data_type:
      return this->output_tae<event_traits::tell1>(event, mask, output);
    case event_traits::tell40::data_type:
      return this->output_tae<event_traits::tell40>(event, mask, output);
    default:
      m_logger->except("output_tae: Invalid burst type encountered: %ld",event.second->type());
    }
    return 0;
  }

  /// Output MDF frame from the event data
  template <typename traits>
  std::size_t EventHandler::output_mdf(const evt_data_t& event,
				       mask_t&           routing_mask,
				       bool              /* requireODIN */,
				       EventOutput&      output)  const
  {
    typedef typename traits::bank_type bank_type;
    std::pair<const bank_header_t*, const void*> odin, tae;
    const event_header_t* evthdr   = nullptr;
    const uint32_t*    hlt_mask = nullptr;
    const std::size_t  hdr_len  = event_header_t::sizeOf(event_header_t::CURRENT_VERSION);
    std::size_t        data_len = hdr_len;
    std::size_t        cnt_odin = 0;

    for( const auto& dsc : event )   {
      auto* bank = dsc.first;
      if ( bank->type() == bank_type::DAQ && bank->version() == DAQ_STATUS_BANK )  {
	evthdr = reinterpret_cast<const event_header_t*>(dsc.second);
	continue;
      }
      else if ( bank->type() == bank_type::ODIN )  {
	odin.first  = dsc.first;
	odin.second = dsc.second;
	++cnt_odin;
      }
      else if ( bank->type() == bank_type::TAEHeader )  {
	tae.first  = dsc.first;
	tae.second = dsc.second;
      }
      else if ( bank->type() == bank_type::HltRoutingBits )  {
	hlt_mask = reinterpret_cast<const uint32_t*>(dsc.second);
      }
      data_len += bank->totalSize();
    }

    /// Create temorary odin bank for event header if not present.
    /// Worst case: run number is 1234 (default from pcie40 decoder)
    std::unique_ptr<uint8_t []> temp_odin_buffer;
    if ( !odin.first )  {
      temp_odin_buffer = _make_tmp_odin_pcie40();
      raw_bank_offline_t* b = (raw_bank_offline_t*)temp_odin_buffer.get();
      odin = std::make_pair(b, b->data());
    }
    else if ( cnt_odin == 1 && odin.first->version() >= 7 )   {
      /// Need to correct output if the TAE frames cross MEPS and we only have 1 crossing.
      /// In this case the odin bank has the wrong values.
      const auto* o = reinterpret_cast<const run3_odin_t*>(odin.second);
      if ( o->is_tae() )   {
	temp_odin_buffer = _make_tmp_odin_pcie40();
	raw_bank_offline_t* b = (raw_bank_offline_t*)temp_odin_buffer.get();
	::memcpy(b->data(), odin.second, sizeof(run3_odin_t));
	auto* s = reinterpret_cast<run3_odin_t*>(b->data());
	s->_tae_entry    = 0;
	s->_tae_first    = 0;
	s->_tae_central  = 0;
	
	s->_orbit_id     = 0;
	s->_event_id     = 0;
	s->_nzs_mode     = 0;
	s->_calib_type   = 0;
	s->_trigger_type = 0;
	s->_event_type   = 0xFFFF;
	s->_step_number  = 0xFFFF;
	odin = std::make_pair(b, b->data());
      }
    }

    uint32_t m[] = { ~0x0U, ~0x0U, ~0x0U, ~0x0U };
    _update_mask(m, routing_mask);
    if ( hlt_mask )    {
      m[0] = hlt_mask[0];
      m[1] = hlt_mask[1];
      m[2] = hlt_mask[2];
      _update_mask_or(m, routing_mask);
    }
    else if ( evthdr )   {
      auto daq_mask = evthdr->subHeader().H1->triggerMask();
      m[0] = daq_mask[0];
      m[1] = daq_mask[1];
      m[2] = daq_mask[2];
      _update_mask_or(m, routing_mask);
    }

    EventOutput::transaction_t* tr = nullptr;
    std::size_t position = 0UL;
    try  {
      tr = output.getTransaction(data_len);
      if ( !tr )   {
	m_logger->except("Event output failed [Cannot reserve output space]");
      }
      position = tr->used_bytes();
      event_header_t* ev_hdr = tr->get<event_header_t>();
      _set_event_header(ev_hdr, data_len-hdr_len, odin, m);
      /// Patch event header if no odin bank, but original header....
      if ( temp_odin_buffer.get() && evthdr )   {
	auto* h = ev_hdr->subHeader().H1;
	const auto* dh = evthdr->subHeader().H1;
	h->setRunNumber(dh->runNumber());
	h->setOrbitNumber(dh->orbitNumber());
	h->setBunchID(dh->bunchID());
      }
      tr->advance(hdr_len);
      if ( tae.first )   {
	output_bank(m_logger, tr, tae.first, tae.second);
      }
      for(const auto& dsc : event)   {
	if ( dsc.first == odin.first )  {
	  output_bank(m_logger, tr, dsc.first, odin.second);
	}
	else if ( dsc.first == tae.first )  {
	  continue;
	}
	else if ( dsc.second != (void*)evthdr )   {
	  output_bank(m_logger, tr, dsc);
	}
      }
      if ( !output.end(tr) )   {
	m_logger->except("Failed to commit event output!");
      }
      return data_len;
    }
    catch(const std::exception& e)   {
      m_logger->error("Output transaction failed: %s -- Event lost.", e.what());
      tr->position(position);
      output.end(tr);
    }
    return data_len;
  }
      
  /// Output MDF frame from the event data
  std::size_t EventHandler::output_mdf(const evt_data_t& event,
				       int32_t           type,
				       mask_t&           mask,
				       bool              requireODIN,
				       EventOutput&      output)  const
  {
    switch(type)   {
    case event_traits::tell1::data_type:
      return this->output_mdf<event_traits::tell1> (event, mask, requireODIN, output);
    case event_traits::tell40::data_type:
      return this->output_mdf<event_traits::tell40>(event, mask, requireODIN, output);
    default:
      m_logger->except("output_mdf: Invalid burst type encountered: %ld", type);
    }
    return 0;
  }

  /// Output PCIE40 event/TAE in MDF format
  template <typename traits> std::size_t
  EventHandler::output_mdf(const evt_desc_t& event,
			   mask_t&           mask,
			   bool              requireODIN,
			   EventOutput&      output)  const
  {
    auto [half_window, central] = this->check_tae(event);
    if ( central && half_window > 0 )   {
      return this->output_tae<traits>(event, mask, output);
    }
    auto data = this->create_event_data(event, 0);
    if ( data.first != NO_MDF_ERROR )   {
      m_logger->error("Bank error: %s -- action aborted.", _mdf_error_string(data.first));
      return 0;
    }
    return this->output_mdf<traits>(data.second, mask, requireODIN, output);
  }
      
  /// Output PCIE40 event in MDF format
  template <> std::size_t
  EventHandler::output_mdf<void>(const evt_desc_t& event,
				 mask_t&           mask,
				 bool              requireODIN,
				 EventOutput&      output)  const
  {
    switch(event.second->type())   {
    case event_traits::tell1::data_type:
      return this->output_mdf<event_traits::tell1>(event, mask, requireODIN, output);
    case event_traits::tell40::data_type:
      return this->output_mdf<event_traits::tell40>(event, mask, requireODIN, output);
    default:
      m_logger->except("output_mdf: Invalid data type encountered: %ld",event.second->type());
    }
    return 0;
  }

  /// Output MDF/TAE frame from the event data
  std::size_t EventHandler::output_mdf(const std::vector<std::pair<int, const evt_data_t*> >& crossings,
				       mask_t& mask, EventOutput& output)  const
  {
    if ( crossings.size() == 1 )  {
      const auto& banks = *crossings.at(0).second;
      if ( !banks.empty() )   {
	if ( banks.at(0).first->magic() == pcie40::bank_t::MagicPattern )
	  return output_mdf<event_traits::tell40>(banks, mask, true, output);
	else if ( banks.at(0).first->magic() == raw_bank_offline_t::MagicPattern )
	  return output_mdf<event_traits::tell1>(banks, mask, true, output);
      }
      return 0;
    }
    std::pair<const bank_header_t*, const void*> odin(nullptr, nullptr);
    std::pair<const bank_header_t*, const void*> hlt_routing_bits(nullptr, nullptr);
    std::size_t hdr_len = event_header_t::sizeOf(event_header_t::CURRENT_VERSION);
    std::size_t total_len = hdr_len + bank_header_t::hdrSize()+crossings.size()*3*sizeof(int);
    for(const auto& cr : crossings)   {
      total_len += _event_length(*cr.second);
      if ( 0 == cr.first )   {
	for(const auto& b : *cr.second)   {
	  if ( b.first->type() == bank_types_t::ODIN )   {
	    odin = {b.first, b.second};
	  } else if ( b.first->type() == bank_types_t::HltRoutingBits )   {
	    hlt_routing_bits = {b.first, b.second};
	  }
	}
      }
    }
    /// Create temorary odin bank for event header if not present.
    /// Worst case: run number is 1234 (default from pcie40 decoder)
    std::unique_ptr<uint8_t []> temp_odin_buffer;
    if ( !odin.first )  {
      temp_odin_buffer = _make_tmp_odin_pcie40();
      raw_bank_offline_t* b = (raw_bank_offline_t*)temp_odin_buffer.get();
      odin = std::make_pair(b, b->data());
    }

    const uint32_t* hlt_mask = nullptr;
    if ( hlt_routing_bits.first ) {
      hlt_mask = reinterpret_cast<const uint32_t*>(hlt_routing_bits.second);
    }

    auto* tr = output.getTransaction(total_len);
    if ( !tr )   {
      m_logger->except("Event output failed [Cannot reserve output space]");
    }
    uint32_t m[] = { ~0x0U, ~0x0U, ~0x0U, ~0x0U };
    _update_mask(m, mask);
    if ( hlt_mask )    {
      m[0] = hlt_mask[0];
      m[1] = hlt_mask[1];
      m[2] = hlt_mask[2];
      _update_mask_or(m, mask);
    }

    auto* ev_hdr = tr->get<event_header_t>();
    _set_event_header(ev_hdr, total_len-hdr_len, odin, m);
    tr->advance(hdr_len);

    auto* tae_bnk = tr->get<raw_bank_offline_t>();
    tae_bnk->setMagic();
    tae_bnk->setVersion(0);
    tae_bnk->setSourceID(0);
    tae_bnk->setType(bank_types_t::TAEHeader);
    tae_bnk->setSize(crossings.size()*3*sizeof(int));
    off_t offset = 0;
    auto* data   = tae_bnk->begin<int>();
    tr->advance(tae_bnk->totalSize());
    for(const auto& cr : crossings)   {
      std::size_t len = 0;
      for( auto& dsc : *cr.second )
	len += output_bank(m_logger, tr, dsc);
      *data++  = cr.first;
      *data++  = offset;
      *data++  = len;
      offset  += len;
    }
    if ( !output.end(tr) )   {
      m_logger->except("Failed to commit event output!");
    }
    return total_len;
  }

  /// Output PCIE40 event in MDF format
  void EventHandler::output_pcie40(const EventAccess::guard_t* guard, EventOutput& output)  const
  {
    if ( guard && !guard->empty() )   {
      EventAccess::record_t  rec = guard->at(0);
      const pcie40::event_collection_t* events =
	pcie40::add_ptr<pcie40::event_collection_t>(rec.data,
						    - sizeof(pcie40::event_collection_t::capacity)
						    - sizeof(pcie40::event_collection_t::length)
						    - sizeof(pcie40::event_collection_t::mep)
						    - sizeof(pcie40::event_collection_t::magic));
      if ( nullptr != events->mep )   {
	auto* tr = output.getTransaction(events->mep->size);
	if ( !tr )   {
	  m_logger->except("+++ Event output failed [Cannot reserve output space]");
	}
	tr->add_data(EventAccess::datapointer_t(events->mep), events->mep->size);
	if ( !output.end(tr) )   {
	  m_logger->except("+++ Failed to commit event output!");
	}
      }
    }
  }

  /// Extract error banks from the event data
  EventHandler::evt_data_t
  EventHandler::get_errors(event_traits::record_t record)  const
  {
    evt_data_t banks;
    if ( record.is_tell40() )   {
      auto& event = *(record.tell40_event);
      for(std::size_t i=0; i < event.num_bank_collections(); ++i)  {
	const auto* bc = event.bank_collection(i);
	for(const auto* b=bc->begin(); b != bc->end(); b = bc->next(b))    {
	  if ( b->type() >= b->DaqErrorFragmentThrottled )
	    banks.emplace_back((event_traits::tell1::bank_type*)b, b->data());
	}
	for(const auto* b=bc->special_begin(); b != bc->special_end(); b = bc->next(b))   {
	  if ( b->type() >= b->DaqErrorFragmentThrottled )
	    banks.emplace_back((event_traits::tell1::bank_type*)b, b->data());
	}
      }
    }
    return banks;
  }

  /// Extract error banks from the event data
  EventHandler::evt_data_t EventHandler::get_errors(evt_desc_t& event)  const
  {
    const auto record(event.second->at(event.first));
    return this->get_errors(record);
  }

  /// Create raw event bank descriptors from raw buffer
  std::pair<int32_t,EventHandler::evt_data_t>
  EventHandler::extract_tell1_banks(const uint8_t* start_ptr,
				    const uint8_t* end_ptr)  const
  {
    const auto* start = start_ptr, *end = end_ptr;
    std::size_t num_banks = 0, max_banks = 3000;
    raw_bank_offline_t* last_bank = nullptr;
    int32_t     error = NO_MDF_ERROR;
    evt_data_t  evt_data;
    evt_data.reserve(1024);
    while ( start < end )  {
      auto* bank = (raw_bank_offline_t*)start;
      if ( bank->magic() != raw_bank_offline_t::MagicPattern )   {
	m_logger->error("extract_banks: %s [%s] Start:%p (%p) End: %p",
			"Invalid bank", "Bad magic pattern",
			start_ptr, start, end_ptr);
	if ( last_bank )  {
	  m_logger->error("extract_banks: Last good bank: %s",
			  event_print::bankHeader(last_bank).c_str());
	}
	m_logger->error("extract_banks: BAD bank:       %s",
			event_print::bankHeader(bank).c_str());
	error = INVALID_MAGIC_PATTERN;
	break;
      }
      evt_data.emplace_back(bank, bank->data());
      start += bank->totalSize();
      if ( bank->type() == bank_types_t::TAEHeader )   {
	max_banks = 3000 * bank->size()/(3*sizeof(int));
      }
      last_bank = bank;
      if ( ++num_banks < max_banks ) continue;
      m_logger->error("Exceeded maximum number of banks: %ld > %ld  [%s]",
		      num_banks, max_banks, "Banks were dropped");
      error = TOO_MANY_BANKS;
      break;
    }
    return std::make_pair(error,std::move(evt_data));
  }

  /// Extract banks from event descriptor
  template <> std::pair<int32_t,EventHandler::evt_data_t>
  EventHandler::extract_banks<event_traits::tell1>(const evt_desc_t& e)  const
  {
    const auto record(e.second->at(e.first));
    if ( record.tell1_event->is_mdf() )   {
      auto frame = record.tell1_event->data_frame();
      return this->extract_tell1_banks(frame.start, frame.end);
    }
    m_logger->except("Attempt to extract Tell1 banks from non-Tell1 frame.");
    return std::pair<int32_t,evt_data_t>(INVALID_MDF_FORMAT,{});
  }

  /// Extract banks from event descriptor
  template <> std::pair<int32_t, EventHandler::evt_data_t>
  EventHandler::extract_banks<event_traits::tell40>(const evt_desc_t& event)  const
  {
    const auto record(event.second->at(event.first));
    const event_traits::tell40::event_type* e = record.tell40_event;
    evt_data_t evt_data;
    evt_data.reserve(1024);
    for(std::size_t i=0; i < e->num_bank_collections(); ++i)  {
      const auto* bc = e->bank_collection(i);
      for(const auto* b=bc->begin(); b != bc->end(); b = bc->next(b))   {
	evt_data.emplace_back(b, b->data());
      }
      for(const auto* b=bc->special_begin(); b != bc->special_end(); b = bc->next(b))   {
	evt_data.emplace_back(b, b->data());
      }
    }
    return std::make_pair(NO_MDF_ERROR,std::move(evt_data));
  }

  /// Extract banks from event descriptor
  template <> std::pair<int,EventHandler::evt_data_t>
  EventHandler::extract_banks<void>(const evt_desc_t& event)  const
  {
    switch(event.second->type())   {
    case event_traits::tell1::data_type:
      return this->extract_banks<event_traits::tell1>(event);
    case event_traits::tell40::data_type:
      return this->extract_banks<event_traits::tell40>(event);
    default:
      m_logger->error("+++ Algorithm failed [Invalid event type].");
    }
    return std::pair<int,evt_data_t>(INVALID_MDF_FORMAT,{});
  }
  
  /// Dump event structure to output
  void EventHandler::dump_event(evt_desc_t& e, int32_t flags)  const
  {
    auto* log = m_logger.get();
    bool dump_events = (flags&DUMP_EVENTS) != 0;
    bool dump_bank_headers = (flags&DUM_BANK_HEADERS) != 0;
    bool dump_bank_collections = (flags& DUMP_BANK_COLLECTIONS) != 0;
    if ( dump_events )    {
      auto evt_data = extract_banks(e);
      if ( evt_data.second.empty() )  {
	return;
      }
      if ( dump_bank_collections )   {
	log->always("+=====================================================================");
      }
      std::size_t data_len = 0;
      std::map<int, std::size_t> bank_types, bank_data_len;
      for( const auto& b : evt_data.second )   {
	data_len += b.first->size();
	bank_types[b.first->type()] += 1;
	bank_data_len[b.first->type()] += b.first->size();
      }
      log->always("| ++  Event[%s]:   %ld banks  %ld bytes",
		  e.second->type_name(), evt_data.second.size(), data_len);
      if ( dump_bank_collections )   {
	for ( const auto& t : bank_types)   {
	  log->always("| ++====>  Bank collection: %3d %-15s Number: %4d  %6ld",
		      t.first, event_print::bankType(t.first).c_str(),
		      t.second, bank_data_len[t.first]);
	  if ( dump_bank_headers )   {
	    for( const auto& b : evt_data.second )   {
	      if ( b.first->type() == t.first )   {
		log->always("+++  %s", event_print::bankHeader(b.first).c_str());
	      }
	    }
	  }
	}
      }
    }
  }
  
  /// On the occurrence of a TAE frame expand data and populate TES locations
  template <>
  std::size_t EventHandler::expand_tae<event_traits::tell1>(const std::string& prefix,
							    const evt_desc_t&  event)  const
  {
    std::size_t      eid  = event.first;
    const auto* hdr = event.second->at(eid).tell1_event;
    if ( hdr && hdr->is_mdf() )   {
      const auto* tae = (const raw_bank_offline_t*)hdr->data();
      if ( tae->magic() != raw_bank_offline_t::MagicPattern )   {
	m_logger->except("expand_tae: Invalid TAE bank [Invalid magic pattern] %04X", tae->magic());
	return 0;
      }
      if ( tae->type() != bank_types_t::TAEHeader )   {
	const uint8_t* start = hdr->data();
	auto banks(this->extract_tell1_banks(start, start+hdr->size()));
	if ( banks.first != NO_MDF_ERROR )   {
	  return 0;
	}
	this->put_banks(prefix + "/Central", std::move(banks.second));
	return 1;
      }
      const int*  data  = tae->begin<int>();
      int32_t     count_bx  = tae->size()/sizeof(int)/3;
      int32_t     half_win  = (count_bx-1)/2;
      std::string pref_prev = prefix + "/Prev", pref_next = prefix + "/Next";
      const uint8_t* start = ((const uint8_t*)tae)+tae->totalSize();

      for(int32_t i=-half_win; i<=half_win; ++i)   {
	int32_t idx = i + half_win;
	int32_t bx  = data[3*idx];
	int32_t off = data[3*idx+1];
	int32_t len = data[3*idx+2];

	auto banks(this->extract_tell1_banks(start+off, start+off+len));
	if ( banks.first != NO_MDF_ERROR )    {
	  return mdf_error_report(0, banks.first);
	}
	if ( bx < 0 )
	  this->put_banks(pref_prev + _bx_offset(bx), std::move(banks.second));
	else if ( bx > 0 )
	  this->put_banks(pref_next + _bx_offset(bx), std::move(banks.second));
	else
	  this->put_banks(prefix + "/Central", std::move(banks.second));
      }
      return 2*half_win + 1;
    }
    m_logger->except("expand_tae: Invalid Tell1 TAE frame encountered. Skip event");
    return 0;
  }

  /// On the occurrence of a TAE frame expand data and populate TES locations
  template <>
  std::size_t EventHandler::expand_tae<event_traits::tell40>(const std::string& prefix,
							     const evt_desc_t&  event)  const
  {
    int64_t eid = event.first;
    auto   record = event.second->at(eid);
    if ( record.is_tell40() )   {
      int64_t nevt  = event.second->num_events();
      auto  central = record.tell40_event->is_tae_central();
      auto half_win = record.tell40_event->tae_half_window();
      if ( central )   {
	if ( eid >= half_win && event.second->num_bx()-eid > half_win )   {
	  std::string pref_prev = prefix + "/Prev", pref_next = prefix + "/Next";
	  for (int32_t bx=-half_win; bx<=half_win; ++bx )  {
	    if ( bx < 0 && eid+bx >= 0 )  // Yes: '+'
	      this->put_event(pref_prev + _bx_offset(bx), event, bx);
	    else if ( bx > 0 && eid+bx < nevt )
	      this->put_event(pref_next + _bx_offset(bx), event, bx);
	    else if ( 0 == bx )
	      this->put_event(prefix + "/Central", event, 0);
	  }
	  return 2*half_win + 1;
	}
      }
      return 1;
    }
    m_logger->except("expand_tae: Invalid Tell40 TAE frame encountered. Skip event");
    return 0;
  }

  /// On the occurrence of a TAE frame expand data and populate TES locations
  template <>
  std::size_t EventHandler::expand_tae<void>(const std::string& prefix,
					     const evt_desc_t&  event)   const
  {
    switch(event.second->type())   {
    case event_traits::tell1::data_type:
      return this->expand_tae<event_traits::tell1>(prefix, event);
    case event_traits::tell40::data_type:
      return this->expand_tae<event_traits::tell40>(prefix, event);
    default:
      m_logger->except("expand_tae: Invalid burst type encountered: %ld",event.second->type());
    }
    return 0;
  }

  /// Extract banks from a TELL1 data frame
  void EventHandler::access_raw_banks(const uint8_t* start, const uint8_t* end, const bank_handler_t& handler)  const   {
    while ( start < end )   {
      auto* bank = (raw_bank_offline_t*)start;
      if ( !handler( {bank, bank->data()} ) )
	return;
      start += bank->totalSize();
    }
  }

  /// Extract banks from a TELL1 data frame (if TAE: according to TAE offset)
  void EventHandler::access_raw_banks(const event_traits::tell1::event_type& evt, long offset, const bank_handler_t& handler)  const  {
    const auto* header = (const event_header_t*)&evt;
    auto [evt_start, evt_end] = header->data_frame();
    while(evt_start < evt_end )  {
      auto* tae = (raw_bank_offline_t*)evt_start;
      if ( tae->type() == bank_types_t::TAEHeader )   {
	long half_window = ((tae->size()/sizeof(int)/3) - 1)/2;
	if ( offset >= -half_window && offset <= half_window )   {
	  int32_t* dat = tae->begin<int>();
	  int32_t  idx = half_window + offset;
	  int32_t  off = dat[3*idx+1];
	  int32_t  len = dat[3*idx+2];
	  const uint8_t* ptr = ((const uint8_t*)tae->next());
	  access_raw_banks(ptr+off, ptr+off+len, handler);
	  return;
	}
	m_logger->except("EventHandler","+++ TAE event: no BX found with offset %ld !", offset);
      }
      access_raw_banks(evt_start, evt_end, handler);
      return;
    }
  }
    
  /// Extract banks from a TELL40 data frame (if TAE: according to TAE offset)
  void EventHandler::access_raw_banks(const event_traits::tell40::event_type& evt, const bank_handler_t& handler)  const  {
    for(std::size_t i=0; i < evt.num_bank_collections(); ++i)  {
      const auto* bc = evt.bank_collection(i);
      for(const auto* b=bc->begin(); b != bc->end(); b = bc->next(b))
	if ( !handler( {b, b->data()} ) )  return;
      for(const auto* b=bc->special_begin(); b != bc->special_end(); b = bc->next(b))
	if ( !handler( {b, b->data()} ) )  return;
    }
  }

  /// Extract raw banks from access guard
  void EventHandler::access_raw_banks(const EventAccess::guard_t& guard,
				      const bank_handler_t& handler,
				      long event_id,
				      long offset)  const  {
    event_traits::record_t rec(nullptr);
    switch ( guard.type() )   {
    case event_traits::tell1::data_type:
      rec = guard.at(event_id);
      return access_raw_banks(*rec.tell1_event, offset, handler);
    case event_traits::tell40::data_type:
      rec = guard.at(event_id+offset);
      return access_raw_banks(*rec.tell40_event, handler);
    default:
      break;
    }
    m_logger->except("EventHandler","Invalid data type [%d] in call to access_raw_banks",
		     int(guard.type()));
  }

  /// Extract banks from event descriptor. If TAE: also extract subevent banks with bx information
  void EventHandler::access_raw_banks(const evt_desc_t& evt, const bank_handler_t& handler)  const   {
    const auto& guard = evt.second;
    long eid = evt.first;
    if ( guard )    {
      auto [half_window, central] = this->check_tae(evt);
      if ( central && half_window )   {
	std::map<int32_t, evt_data_t> result;
	for(long hw=half_window, iw=-hw; iw <= hw; ++iw)   {
	  long evt_id = eid + iw;
	  if ( evt_id >= 0 && evt_id < long(guard->num_events()) )   {
	    access_raw_banks(*guard, handler, eid, iw);
	  }
	}
	return;
      }
      access_raw_banks(*guard, handler, eid, 0);
    }
  }

  /// Extract banks from a TELL1 data frame
  void EventHandler::access_raw_banks(const uint8_t* start, const uint8_t* end, bank_handler_t& handler)  const   {
    while ( start < end )   {
      auto* bank = (raw_bank_offline_t*)start;
      if ( !handler( {bank, bank->data()} ) )
	return;
      start += bank->totalSize();
    }
  }

  /// Extract banks from a TELL1 data frame (if TAE: according to TAE offset)
  void EventHandler::access_raw_banks(const event_traits::tell1::event_type& evt, long offset, bank_handler_t& handler)  const  {
    const auto* header = (const event_header_t*)&evt;
    auto [evt_start, evt_end] = header->data_frame();
    while(evt_start < evt_end )  {
      auto* tae = (raw_bank_offline_t*)evt_start;
      if ( tae->type() == bank_types_t::TAEHeader )   {
	long half_window = ((tae->size()/sizeof(int)/3) - 1)/2;
	if ( offset >= -half_window && offset <= half_window )   {
	  int32_t* dat = tae->begin<int>();
	  int32_t  idx = half_window + offset;
	  int32_t  off = dat[3*idx+1];
	  int32_t  len = dat[3*idx+2];
	  const uint8_t* ptr = ((const uint8_t*)tae->next());
	  access_raw_banks(ptr+off, ptr+off+len, handler);
	  return;
	}
	m_logger->except("EventHandler","+++ TAE event: no BX found with offset %ld !", offset);
      }
      access_raw_banks(evt_start, evt_end, handler);
      return;
    }
  }
    
  /// Extract banks from a TELL40 data frame (if TAE: according to TAE offset)
  void EventHandler::access_raw_banks(const event_traits::tell40::event_type& evt, bank_handler_t& handler)  const  {
    for(std::size_t i=0; i < evt.num_bank_collections(); ++i)  {
      const auto* bc = evt.bank_collection(i);
      for(const auto* b=bc->begin(); b != bc->end(); b = bc->next(b))
	if ( !handler( {b, b->data()} ) )  return;
      for(const auto* b=bc->special_begin(); b != bc->special_end(); b = bc->next(b))
	if ( !handler( {b, b->data()} ) )  return;
    }
  }

  /// Extract raw banks from access guard
  void EventHandler::access_raw_banks(const EventAccess::guard_t& guard,
				      bank_handler_t& handler,
				      long event_id,
				      long offset)  const  {
    event_traits::record_t rec(nullptr);
    switch ( guard.type() )   {
    case event_traits::tell1::data_type:
      rec = guard.at(event_id);
      return access_raw_banks(*rec.tell1_event, offset, handler);
    case event_traits::tell40::data_type:
      rec = guard.at(event_id+offset);
      return access_raw_banks(*rec.tell40_event, handler);
    default:
      break;
    }
    m_logger->except("EventHandler","Invalid data type [%d] in call to access_raw_banks",
		     int(guard.type()));
  }

  /// Extract banks from event descriptor. If TAE: also extract subevent banks with bx information
  void EventHandler::access_raw_banks(const evt_desc_t& evt, bank_handler_t& handler)  const   {
    const auto& guard = evt.second;
    long eid = evt.first;
    if ( guard )    {
      auto [half_window, central] = this->check_tae(evt);
      if ( central && half_window )   {
	std::map<int32_t, evt_data_t> result;
	for(long hw=half_window, iw=-hw; iw <= hw; ++iw)   {
	  long evt_id = eid + iw;
	  if ( evt_id >= 0 && evt_id < long(guard->num_events()) )   {
	    access_raw_banks(*guard, handler, eid, iw);
	  }
	}
	return;
      }
      access_raw_banks(*guard, handler, eid, 0);
    }
  }

  
  /// Extract banks from a TELL1 data frame
  EventHandler::evt_data_t
  EventHandler::extract_raw_banks(const uint8_t* start,
				  const uint8_t* end,
				  bank_types_t::BankType bank_type)  const
  {
    evt_data_t result;
    while ( start < end )   {
      auto* bank = (raw_bank_offline_t*)start;
      if ( bank->type() == bank_type )   {
	result.emplace_back(bank, bank->data());
      }
      start += bank->totalSize();
    }
    return result;
  }
  
  /// Extract banks from a TELL1 data frame (if TAE: according to TAE offset)
  EventHandler::evt_data_t
  EventHandler::extract_raw_banks(const event_traits::tell1::event_type& evt,
				  bank_types_t::BankType bank_type,
				  long offset)  const
  {
    const auto* header = (const event_header_t*)&evt;
    auto [evt_start, evt_end] = header->data_frame();
    while(evt_start < evt_end )  {
      auto* tae = (raw_bank_offline_t*)evt_start;
      if ( tae->type() == bank_types_t::TAEHeader )   {
	long half_window = ((tae->size()/sizeof(int)/3) - 1)/2;
	if ( offset >= -half_window && offset <= half_window )   {
	  int32_t* dat = tae->begin<int>();
	  int32_t  idx = half_window + offset;
	  int32_t  off = dat[3*idx+1];
	  int32_t  len = dat[3*idx+2];
	  const uint8_t* ptr = ((const uint8_t*)tae->next());
	  return extract_raw_banks(ptr+off, ptr+off+len, bank_type);
	}
	m_logger->except("EventHandler","+++ TAE event: no BX found with offset %ld !", offset);
      }
      return extract_raw_banks(evt_start, evt_end, bank_type);
    }
    return { };
  }

  /// Extract banks from a TELL40 data frame (if TAE: according to TAE offset)
  EventHandler::evt_data_t
  EventHandler::extract_raw_banks(const event_traits::tell40::event_type& evt,
				  bank_types_t::BankType bank_type)  const
  {
    evt_data_t result;
    for(std::size_t i=0; i < evt.num_bank_collections(); ++i)  {
      const auto* bc = evt.bank_collection(i);
      for(const auto* b=bc->begin(); b != bc->end(); b = bc->next(b))   {
	if ( b->type() == bank_type )  {
	  result.emplace_back((bank_header_t*)b, b->data());
	}
      }
      if ( result.empty() )  {
	for(const auto* b=bc->special_begin(); b != bc->special_end(); b = bc->next(b))  {
	  if ( b->type() == bank_type )  {
	    result.emplace_back((bank_header_t*)b, b->data());
	  }
	}
      }
    }
    return result;
  }

  /// Extract raw banks from access guard
  EventHandler::evt_data_t
  EventHandler::extract_raw_banks(const EventAccess::guard_t& guard,
				  bank_types_t::BankType bank_type,
				  long event_id,
				  long offset)  const
  {
    event_traits::record_t rec(nullptr);
    switch ( guard.type() )   {
    case event_traits::tell1::data_type:
      rec = guard.at(event_id);
      return extract_raw_banks(*rec.tell1_event, bank_type, offset);
    case event_traits::tell40::data_type:
      rec = guard.at(event_id+offset);
      return extract_raw_banks(*rec.tell40_event, bank_type);
    default:
      break;
    }
    m_logger->except("EventHandler","Invalid data type [%d] in call to extract_raw_banks",
		     int(guard.type()));
    return { }; // Will never get here!
  }

  /// Extract banks from event descriptor. If TAE: also extract subevent banks with bx information
  std::map<int32_t, EventHandler::evt_data_t>
  EventHandler::extract_raw_banks(const evt_desc_t& evt, bank_types_t::BankType bank_type)  const
  {
    const auto& guard = evt.second;
    long eid = evt.first;
    if ( guard )    {
      auto [half_window, central] = this->check_tae(evt);
      if ( central && half_window )   {
	std::map<int32_t, evt_data_t> result;
	for(long hw=half_window, iw=-hw; iw <= hw; ++iw)   {
	  result.emplace(iw, extract_raw_banks(*guard, bank_type, eid, iw));
	}
	return result;
      }
      return { {0, extract_raw_banks(*guard, bank_type, eid, 0)} };
    }
    return { };
  }

}  // End namespace Online
