//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Project    : Online
//
//  Author     : M.Frank
//
//==========================================================================

/// Framework include files
#include <EventHandling/EventOutput.h>
#include <EventData/event_header_t.h>
#include <RTL/rtl.h>

#include <cstring>

using namespace Online;

namespace {
  template <typename T> struct Increase {
    T& count;
    Increase(T& t) : count(t) {  ++count; }
    ~Increase() {  --count; }
  };
}

/// Advance buffer pointer
EventOutput::data_t* EventOutput::transaction_t::advance(std::size_t len)  {
  if ( length+len <= size )   {
    length += len;
    return buffer + length;
  }
  return 0;
}

/// Add event data to the transaction buffer
std::size_t EventOutput::transaction_t::add_data(const datapointer_t data, std::size_t len)    {
  if ( length+len <= size )   {
    ::memcpy(buffer+length, data, len);
    length += len;
    return len;
  }
  return 0;
}

/// Standard constructor
EventOutput::EventOutput(std::unique_ptr<RTL::Logger>&& logger, config_t* cfg)
  : m_logger(std::move(logger)), base_config(cfg)
{
}

/// Standard destructor
EventOutput::~EventOutput()    {
  m_logger->info("Event output shutdown.");
}

/// Print burst/event information
void EventOutput::printInfo()  const    {
  if ( (m_burstCount%base_config->burstPrintCount) == 0 )   {
    m_logger->info("+++ Burst-out:%10ld %4s%8s %4s%8s %4s%8s Events:%13ld Average event size:%10.0f",
		   (std::size_t)m_burstCount, "","","","","","", (std::size_t)m_eventCount, 
		   double(m_eventAvgBytes/m_eventAvgCount));
  }
}

/// Are there any data contained ?
bool EventOutput::empty()   {
  lock_t lock(m_transactionLock);
  for( auto i=m_idleTr.begin(); i != m_idleTr.end(); ++i )
    if ( !i->get()->empty() ) return false;
  for( auto i=m_busyTr.begin(); i != m_busyTr.end(); ++i )
    if ( !i->get()->empty() ) return false;
  return true;
}

/// Queue cancellation
void EventOutput::queueCancel(bool value)  {
  m_cancelled = value;
}

/// Dequeue transaction for use by client
EventOutput::transaction_t* EventOutput::dequeueTransaction(std::size_t size)   {
  lock_t lock(m_transactionLock);
  /// First see if there is an ongoing transaction present with sufficient free space
  for( auto i=m_idleTr.begin(); i != m_idleTr.end(); ++i )   {
    transaction_t* tr = i->get();
    if ( (0 == tr->buffer) || (tr->buffer && tr->free_bytes() >= size) )  {
      m_busyTr.emplace_back(std::move(*i));
      m_idleTr.erase(i);
      return m_busyTr.back().get();
    }
  }
  /// If not, we commit buffers and take the first free one.
  for( auto i=m_idleTr.begin(); i != m_idleTr.end(); ++i )   {
    transaction_t* tr = i->get();
    if ( tr->sync() )   {
      m_busyTr.emplace_back(std::move(*i));
      m_idleTr.erase(i);
      ++m_burstCount;
      printInfo();
      return m_busyTr.back().get();
    }
  }
  return nullptr;
}

/// Requeue transaction for further use
bool EventOutput::enqueueTransaction(transaction_t* tr)    {
  lock_t lock(m_transactionLock);
  for( auto i=m_busyTr.begin(); i != m_busyTr.end(); ++i )   {
    if ( i->get() == tr )   {
      m_idleTr.emplace_back(std::move(*i));
      m_busyTr.erase(i);
      m_haveTr.notify_one();
      return true;
    }
  }
  return false;
}

/// Close network connection
void EventOutput::close()  {
  lock_t lock(m_transactionLock);
  for( auto& tr : m_idleTr )   {
    tr->close();
  }
  m_idleTr.clear();
  if ( !m_busyTr.empty() )   {
    m_logger->warning("Closing event output dispite pending transactions. Asking for trouble ?");
  }
}

/// Start output transaction
EventOutput::transaction_t* EventOutput::getTransaction(std::size_t size)   {
  transaction_t* tr = dequeueTransaction(size);
  Increase inc(m_numGetTr);
  if ( !tr )   {
    while ( !m_cancelled && !tr )   {
      std::unique_lock<std::mutex> lock(m_haveTrLock);
      m_haveTr.wait_for(lock, std::chrono::milliseconds(base_config->transitionWaitTMO));
      tr = dequeueTransaction(size);
      if ( tr ) break;
    }
    if ( !tr && isCancelled() )   {
      m_logger->info("getTransaction: Event space request for %ld bytes was cancelled.",size);
      return nullptr;
    }
  }
  if ( tr && 0 == tr->buffer )   {
    std::size_t len = (base_config->max_events == 1) ? size : std::max(size,base_config->allocation_size);
    if ( tr->reserve(len) )    {
      return tr;
    }
    m_logger->error("getTransaction: Failed to allocate %ld bytes of space.", size);
    if ( enqueueTransaction(tr) )  {
      return 0;
    }
    m_logger->except("getTransaction: Failed to allocate. [SEVERE INTERNAL ERROR]");
    return 0;
  }
  return tr;
}

/// Commit output transaction
bool EventOutput::end(transaction_t*& tr)    {
  transaction_t* ptr = tr;
  tr = 0;
  if ( ptr )   {
    /// Update counters
    ++ptr->events;
    if ( m_eventAvgBytes + ptr->event_size() > std::numeric_limits<std::size_t>::max() )  {
      m_eventAvgBytes = m_eventAvgBytes/10;
      m_eventAvgCount = m_eventAvgCount/10;
    }
    ++m_eventCount;
    ++m_eventAvgCount;
    m_eventAvgBytes += ptr->event_size();
    std::size_t avg = m_eventAvgBytes/m_eventAvgCount;
    if ( base_config->max_events == 1 )  {
      /// Single event mode: commit every transaction
      return commit(ptr);
    }
    else if ( base_config->max_events > 0 && base_config->max_events == ptr->events )  {
      /// Event burst mode: commit transaction if max. number of events is reached
      return commit(ptr);
    }
    else if ( (base_config->max_events != 1) && (ptr->free_bytes() > 2*avg) )   {
      // Do a crude check on the remaining size to decide if the buffer can be re-used
      ptr->start += ptr->event_size();
      return enqueueTransaction(ptr);
    }
    return commit(ptr);
  }
  return false;
}

/// Cancel commit transaction. Resets the buffer to the event start
bool EventOutput::rollback(transaction_t*& tr)   {
  transaction_t* ptr = tr;
  tr = 0;
  if ( ptr )   {
    ptr->length = ptr->start;
    return enqueueTransaction(ptr);
  }
  return false;
}

/// Forced commit output transaction. Flush data to medium
bool EventOutput::commit(transaction_t*& tr)    {
  transaction_t* ptr = tr;
  tr = 0;
  if ( ptr && ptr->empty() )  {
    return true;
  }
  else if ( ptr && ptr->sync() )  {
    ++m_burstCount;
    printInfo();
    return enqueueTransaction(ptr);
  }
  return false;
}

/// Forced commit of all output transactions. Flush data to medium
bool EventOutput::commit_all()   {
  lock_t lock(m_transactionLock);
  bool ret = true;
  for( auto& tr : m_idleTr )   {
    if ( !tr->empty() )  {
      if ( !tr->sync() )
	ret = false;
    }
  }
  return ret;
}

