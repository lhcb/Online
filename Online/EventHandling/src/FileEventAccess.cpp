//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Project    : Online
//
//  Author     : Markus Frank
//==========================================================================
//  FileEventAccess.cpp
//==========================================================================
//
//  Description: The Online::FileEventAccess component is able
//               to produce a list of event references given 
//               a set of "selection criteria".
//==========================================================================

/// Framework include files
#include <EventHandling/FileEventAccess.h>
#include <EventData/event_header_t.h>
#include <RTL/Compress.h>
#include <MBM/bmdef.h>

/// ROOT include files
#include <RZip.h>

/// C/C++ include files
#include <fcntl.h>
#include <unistd.h>
#include <libgen.h>
#define O_BINARY 0

using namespace Online;

namespace {
  struct InPlaceAllocator : public RawFile::Allocator {
    uint8_t* ptr = 0;
    std::size_t len = 0;
    InPlaceAllocator() = default;
    InPlaceAllocator( InPlaceAllocator&& copy ) = delete;
    InPlaceAllocator( const InPlaceAllocator& copy ) = delete;
    virtual ~InPlaceAllocator() { if( ptr ) delete [] ptr; }
    InPlaceAllocator& operator=( const InPlaceAllocator& copy ) = delete;
    uint8_t* get()      { return ptr; }
    uint8_t* release()  { uint8_t* tmp = ptr; ptr=0; return tmp; }
    virtual uint8_t* operator()(std::size_t length)   override  final {
      if( !ptr )  {
	ptr = new uint8_t[length];
	len = length;
      }
      return (uint8_t*)((length<=len) ? ptr : 0);
    }
  };
}

template <typename T> class FileEventAccess::file_guard_t : public EventAccess::guard_t {
public:
  
  std::unique_ptr<T>  events {};
  FileEventAccess*    access = nullptr;
  data_t*             buffer = nullptr;

public:

  file_guard_t(std::unique_ptr<T>&& e, std::vector<int>&& bx, long t, FileEventAccess* a, data_t* b)
    : guard_t(std::move(bx), t), events(std::move(e)), access(a), buffer(b)
  {
    num_evt = events->size();
  }
  file_guard_t() = delete;
  file_guard_t(file_guard_t&& copy) = delete;
  file_guard_t& operator=(file_guard_t&& copy) = delete;
  file_guard_t& operator=(const file_guard_t& copy) = delete;
  /// Destructor
  virtual ~file_guard_t()  {
    if(buffer) delete [] buffer;
  }
  virtual record_t at(std::size_t which) const  override {
    return events->at(which);
  }
};

/// Initializing constructor
FileEventAccess::FileEventAccess(std::unique_ptr<RTL::Logger>&& logger)
  : EventAccess(std::move(logger)), m_scan(m_logger)
{
  base_config = &config;
}

int FileEventAccess::connect(const std::vector<std::string>& requests)  {
  config.requests = requests;
  if( !config.requests.empty() )  {
    m_request.reset();
    for(int j=0; j<BM_MASK_SIZE;++j)  {
      m_request.trmask[j] = 0;
      m_request.vetomask[j] = ~0;
    }
    for (std::size_t i=0, n=config.requests.size(); i<n; ++i )  {
      MBM::Requirement r(config.requests[i]);
      for(int k=0; k<BM_MASK_SIZE; ++k)  {
        m_request.trmask[k]   |= r.trmask[k];
        m_request.vetomask[k] &= r.vetomask[k];
      }
    }
  }
  m_cancelled = m_scan.isBrokenHost(config.brokenHosts);
  if( !m_cancelled )  {
    m_scan.scanFiles(config.sources, config.prefix, config.allowedRuns);
    ++m_num_scans;
  }
  else  {
    config.rescan = false;
  }
  return ONLINE_OK;
}

/// Close filework connection
void FileEventAccess::close()  {
  cancel();
  m_cancelled = false;
  m_current.reset();
}

/// Cancel all pending I/O requests to the buffer manager
int FileEventAccess::cancel()  {
  m_cancelled = true;
  return ONLINE_OK;
}

/// Open a new data file
std::pair<int,RawFile> FileEventAccess::openFile()  {
  std::string err;
  bool unlink_error = false;
  while ( m_scan.files.size() > 0 )  {
    auto i = m_scan.files.begin();
    auto file = (*i).second;
    m_scan.files.erase(i);
    int fd = config.mmapFiles ? file.openMapped() : file.open();
    if( -1 == fd )  {
      if( config.openFailDelete != 0 )  {
	int sc = file.unlink();
        if(sc != 0)  {
	  err = RTL::errorString();
          m_logger->error("CANNOT OPEN/UNLINK file: [ignored] %s: [%s]",
			  file.cname(), !err.empty() ? err.c_str() : "????????");
	}
      }
      else if( m_scan.badFiles.find(file.name()) == m_scan.badFiles.end() )  {  // We only want to see the warning once!
	err = RTL::errorString();
	m_logger->warning("CANNOT OPEN file %s: [%s] --> Try the next one in the list.",
			  file.cname(), !err.empty() ? err.c_str() : "????????");
	m_scan.badFiles.insert(file.name());
      }
      continue;
    }
    else  {
      if( config.deleteFiles )  {
        int sc = file.unlink();
        if(sc == 1)  {
	  continue;
	}
	else if( sc != 0 )  {
	  // Suspicion of bad disk:
	  char dir_buff[PATH_MAX];
	  ::close(fd);
	  ::strncpy(dir_buff,file.cname(),PATH_MAX-1);
	  dir_buff[PATH_MAX-1] = 0;
	  char* dir = ::dirname(dir_buff);
	  if( dir && strlen(dir) > 2 )  {
	    auto files = m_scan.files;
	    std::string dir_name = dir;
	    m_logger->info("File: %s SKIPPED for HLT processing. Remove input of device:%s",
			   file.cname(), dir);
	    for( const auto& f : files )  {
	      if( f.second.name().find(dir_name) == 0 )  {
		auto j = m_scan.files.find(f.first);
		if( j != m_scan.files.end() ) m_scan.files.erase(j);
	      }
	    }
	    std::vector<std::string> dirs;
	    for(std::size_t s=0; s<config.sources.size(); ++s)  {
	      if( config.sources[s] != dir_name ) dirs.push_back(config.sources[s]);
	    }
	    config.sources = dirs;
	  }
	  continue;
	}
      }
#if _XOPEN_SOURCE >= 600 || _POSIX_C_SOURCE >= 200112L 
      if( fd > 0 )  {
	// The specified data will be accessed only once. 
	::posix_fadvise(fd, 0, 0, POSIX_FADV_NOREUSE);
	// The application expects to access the specified data sequentially.
	::posix_fadvise(fd, 0, 0, POSIX_FADV_SEQUENTIAL);
      }
#endif
      return std::make_pair(FILEACCESS_OK,std::move(file));
    }
    err = RTL::errorString();
    m_logger->error("FAILED to open file: %s for deferred HLT processing: %s.",
		    file.cname(), !err.empty() ? err.c_str() : "????????");
  }
  bool may_scan = config.rescan || (m_num_scans == 0);
  if( m_scan.files.size() == 0 && may_scan )  {
    static int s_opening = 0;
    struct OpeningTracer {
      OpeningTracer()  { ++s_opening; }
      ~OpeningTracer() { --s_opening; }
    };
    OpeningTracer _hold;
    if( s_opening < 2 )  {
      ++m_num_scans;
      m_logger->info("Rem_scanning directory list.....");
      m_scan.scanFiles(config.sources, config.prefix, config.allowedRuns);
      if( m_scan.files.size() > 0 )  {
	return openFile();
      }
    }
  }
  // We get here if no file was found in the scanned list.
  if( unlink_error )  { 
    // Goto DAQ Error stop processing on this node after hitting a read-only disk.
    m_logger->info("Invoke transition to DAQ_ERROR after unlink-failure. Files to process:%ld", 
		   m_scan.files.size());
    return std::make_pair(FILEACCESS_ERR_UNLINK, RawFile());
  }
  return std::make_pair(FILEACCESS_NOMOREFILES,RawFile());
}

namespace  {
  static constexpr int peek_size = int(5*sizeof(int));
  typedef std::pair<long,uint8_t*> MemBuffer;
}

/// Load MDF event
long FileEventAccess::load_mdf_event(RawFile&                  file,
				     std::pair<long,uint8_t*>  output,
				     std::pair<long,uint8_t*>& decomp )
{
  auto*  mdf_hdr   = (event_header_t*)output.second;
  long   status    = 0;
  int    compress  = mdf_hdr->compression();
  long   data_size = mdf_hdr->recordSize();

  ++m_curr_event_count;
  if( compress > 0 )  {
    int  new_size  = 0;
    int  tgt_size  = 0;
    int  src_size  = data_size;
    long hdr_len   = mdf_hdr->sizeOf(mdf_hdr->headerVersion());

    if( decomp.first < data_size )  {
      decomp.first  = data_size*1.2;
      if( decomp.second ) delete [] decomp.second;
      decomp.second = new uint8_t[decomp.first];
    }
    ::memcpy(decomp.second, output.second, peek_size);
    status = file.read(decomp.second + peek_size, data_size - peek_size);
    if( status < data_size - peek_size )  {
      m_logger->info("Failed read next mdf record at position %ld [file size: %ld] event: %ld",
		     file.position(), file.data_size(), m_curr_event_count);
      return -1;
    }
    if( hdr_len-peek_size > 0 )  {
      ::memcpy(output.second, decomp.second, hdr_len);
    }
    if( 0 == ::R__unzip_header(&src_size, decomp.second + hdr_len, &tgt_size) )  {
      if( output.first < (long)tgt_size )  {
	return 0;
      }
      ::R__unzip( &src_size, decomp.second + hdr_len,
		  &tgt_size, output.second + hdr_len,
		  &new_size);
      if( new_size > 0 )  {
	event_header_t* hdr = (event_header_t*)output.second;
	hdr->setHeaderVersion( 3 );
	hdr->setDataType(event_header_t::BODY_TYPE_BANKS);
	hdr->setSubheaderLength(sizeof(event_header_t::Header1));
	hdr->setSize(new_size);
	hdr->setCompression(0);
	hdr->setChecksum(0);
	hdr->setSpare(0);
	return hdr_len + new_size;
      }
    }
    return -1;
  }
  if( output.first < data_size )  {
    return 0;
  }
  status = file.read(output.second + peek_size, data_size - peek_size);
  if( status < data_size - peek_size )  {
    return -1;
  }
  return data_size;
}

/// Read file data
FileEventAccess::LoadResult
FileEventAccess::read_file_data(int packingFactor, long bufferSize,
				std::pair<long,uint8_t*>& decompress,
				Online::RawFile::Allocator& allocator,
				Online::RawFile& file)
{
  int      size[5];
  int      status;
  uint8_t *data_ptr, *alloc_ptr;
  off_t    position = file.position();

  ::memset(size, 0, sizeof(size));
  status = file.read(size, peek_size);
  if( status != peek_size )  {
    m_logger->debug("Failed to peek next event record at position %ld [file size: %ld] event: %ld",
		    file.position(), file.data_size(), m_curr_event_count);
    return { 0, -1, RawFile::NO_INPUT_TYPE };
  }
  pcie40::mep_header_t* mep_hdr = (pcie40::mep_header_t*)size;
  if( mep_hdr->is_valid() )  {
    int data_size = mep_hdr->size*sizeof(uint32_t);
    data_ptr = allocator(data_size);
    ::memcpy(data_ptr, size, peek_size);
    status = file.read(data_ptr+peek_size, data_size-peek_size);
    if( status < int(data_size-peek_size) )  {
      file.reset(true);
      return { 0, -1, RawFile::NO_INPUT_TYPE };
    }
    return { std::size_t(data_size), 1, RawFile::MEP_INPUT_TYPE };
  }
  event_header_t* mdf_hdr = (event_header_t*)size;
  if( mdf_hdr->is_mdf() )  {
    int  num_evts   = 1;
    int  compress   = mdf_hdr->compression();
    long hdr_len    = mdf_hdr->sizeOf(mdf_hdr->headerVersion());
    long alloc_size = mdf_hdr->recordSize();
    long data_size  = 0;

    if( compress > 0 )  {
      alloc_size = hdr_len + (mdf_hdr->compression() + 1) * mdf_hdr->size();
    }
    if( packingFactor > 1 )  {
      alloc_size = std::max(alloc_size, bufferSize);
    }
    alloc_ptr = data_ptr = allocator(alloc_size);
    if( !alloc_ptr )  {
      m_logger->info("Failed read next mdf record at position %ld [file size: %ld] event: %ld",
		     file.position(), file.data_size(), m_curr_event_count);
      file.position(position);
      return { 0, -1, RawFile::NO_INPUT_TYPE };
    }
    ::memcpy(data_ptr, size, peek_size);
    long length = load_mdf_event(file, MemBuffer(alloc_size,data_ptr), decompress);
    if( length <= 0 )  {
      m_logger->info("Failed read next mdf record at position %ld [file size: %ld] event: %ld",
		     file.position(), file.data_size(), m_curr_event_count);
      length == 0 ? file.position(position) : file.reset(true);
      file.reset(true);
      return { 0, -1, RawFile::NO_INPUT_TYPE };
    }
    data_ptr  += length;
    data_size += length;
    for ( ; num_evts < packingFactor; )  {
      position = file.position();
      if( alloc_size-data_size < peek_size )  {
	return { std::size_t(data_size), num_evts, RawFile::MDF_INPUT_TYPE };
      }
      status   = file.read(data_ptr, peek_size);
      if( status < peek_size )  {
	file.reset(true);
	return { std::size_t(data_size), num_evts, RawFile::MDF_INPUT_TYPE };
      }
      mep_hdr = (pcie40::mep_header_t*)data_ptr;
      if( mep_hdr->magic == mep_hdr->MagicPattern )  {
	file.position(position);
	return { std::size_t(data_size), num_evts, RawFile::MDF_INPUT_TYPE };
      }
      mdf_hdr = (event_header_t*)data_ptr;
      if( !mdf_hdr->is_mdf() )  {
	//warning("Corrupted file found: %s at position %ld. Skip %ld bytes.",
	//        file.cname(), position, long(file.data_size())-file.position());
	file.reset(true);
	return { std::size_t(data_size), num_evts, RawFile::MDF_INPUT_TYPE };
      }
      length = load_mdf_event(file, MemBuffer(alloc_size-data_size,data_ptr), decompress);
      if( length <= 0 )  {
	length == 0 ? file.position(position) : file.reset();
	return { std::size_t(data_size), num_evts, RawFile::MDF_INPUT_TYPE };
      }
      data_ptr  += length;
      data_size += length;
      ++num_evts;
    }
    return { std::size_t(data_size), num_evts, RawFile::MDF_INPUT_TYPE };
  }
  return { 0, -1, RawFile::NO_INPUT_TYPE };
}

/// Fill the event data cache if necessary
int FileEventAccess::fill_cache()  {
  while (1)  {
    if( !m_current.isOpen() )  {
      auto f = openFile();
      m_curr_event_count = 0;
      if( f.first == FILEACCESS_OK )  {
	m_current = std::move(f.second);
	m_current.setReuse(config.reuseFile);
	if( !compress::init_data_reading(m_current) )  {
	  m_logger->error("Failed to initialize data access for %s", m_current.cname());
	  continue;
	}
      }
      else if( f.first == FILEACCESS_NOMOREFILES )  {
	cancel();
	return ONLINE_END_OF_DATA;
      }
    }
    else if( config.reuseFile && m_current.pointer() == m_current.end() )  {
      m_current.seek(0,SEEK_SET);
      m_curr_event_count = 0;
    }
    if( m_current.isOpen() )  {
      InPlaceAllocator allocator;
      off64_t    file_position = m_current.position();
      LoadResult result = read_file_data(config.packingFactor,
					 config.bufferSize,
					 m_deCompress,
					 allocator,
					 m_current);
      if( result.length <= 0 && config.reuseFile )  {
	m_logger->debug("Re-use input file: %s", m_current.cname());
	m_current.seek(0, SEEK_SET);
	m_curr_event_count = 0;
	continue;
      }
      else if( result.length <= 0 )  {
	m_current.reset();
	continue;
      }
      else if( m_cancelled )  {
	if( m_current.isOpen() && config.deleteFiles )  {
	  // Set back the file position to the beginning of the event and continue.
	  // If there was a cancel in between, the file shall be saved.
	  if( config.saveRest )  {
	    m_current.position(file_position);
	    m_current.saveRestOfFile();
	  }
	}
	m_current.reset();
	return ONLINE_CANCELLED;
      }
      shared_guard_t burst;
      if( result.type == RawFile::MEP_INPUT_TYPE )  {
	auto type = event_traits::tell40::data_type;
	auto evts = convertPCIE40MEP(allocator.get(), std::size_t(result.length));
	burst = std::make_shared<file_guard_t<pcie40::event_collection_t> >
	  (std::move(evts.second), std::move(evts.first), type, this, allocator.release());
	m_curr_event_count += evts.first.size();
      }
      else if( result.type == RawFile::MDF_INPUT_TYPE )  {
	auto type = event_traits::tell1::data_type;
	auto evts = convertMultiMDF(allocator.get(), std::size_t(result.length));
	if( evts.first.empty() )  {
	  m_logger->warning("Decoded empty event burst from input: %s position:%ld event: %ld",
			    m_current.cname(), file_position, m_curr_event_count);
	  continue;
	}
	burst = std::make_shared<file_guard_t<event_collection_t> >
	  (std::move(evts.second), std::move(evts.first), type, this, allocator.release());
      }
      else  {
	m_logger->except("Unknown event type found in %s position: %ld event: %ld. Processing failed.",
			 m_current.cname(), file_position, m_curr_event_count);
      }
      if( burst )  {
	this->queueBurst(std::move(burst));
	this->printInfo();
	if( this->monitor.eventsIn > this->config.maxEventsIn )  {
	  this->cancel();
	  return ONLINE_END_OF_DATA;
	}
	return ONLINE_OK;
      }
    }
  }
  return ONLINE_OK;
}
