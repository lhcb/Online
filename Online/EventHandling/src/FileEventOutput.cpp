//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Project    : Online
//
//  Author     : M.Frank
//
//==========================================================================

/// Framework include files
#include <EventHandling/FileEventOutput.h>
#include <EventData/RawFile.h>
#include <RTL/strdef.h>

/// C/C++ include files
#include <ctime>
#include <sstream>

using namespace Online;

/// Base class to handle one atomic write operation
/**
 *
 * @author  M.Frank
 * @version 1.0
 * @date    25/04/2019
 */
class FileEventOutput::file_transaction_t : public EventOutput::transaction_t  {
public:
  RawFile                   file;
  std::unique_ptr<data_t[]> space;
  FileEventOutput*          output {0};
  std::size_t               position_ptr {0};

public:
  /// Default constructor
  file_transaction_t() = delete;
  /// Initializing constructor
  file_transaction_t(const std::string& name, FileEventOutput* out) : file(name), output(out) {}
  /// Move constructor
  file_transaction_t(file_transaction_t&& copy) = default;
  /// Move assignment
  file_transaction_t& operator=(file_transaction_t&& copy) = default;
  /// Inhibit copy assignment
  file_transaction_t& operator=(const file_transaction_t& copy) = delete;
  /// Default destructor
  virtual ~file_transaction_t() = default;
  /// Allocate buffer to store contiguous junk of data
  virtual bool reserve(std::size_t len)   override;
  /// Synchronize the buffered data with the medium
  virtual bool sync()   override;
  /// Shutdown the I/O medium
  virtual bool close()   override;
  /// Reposition output buffer to specified location
  virtual std::size_t position(std::size_t new_position)   override;
};

/// Allocate buffer to store contiguous junk of data
bool FileEventOutput::file_transaction_t::reserve(std::size_t len)   {
  if ( size < len )  {
    space.reset(new data_t[len]);
    buffer = space.get();
    size = len;
  }
  start  = 0;
  length = 0;
  events = 0;
  return true;
}

/// Synchronize the buffered data with the medium
bool FileEventOutput::file_transaction_t::sync()   {
  if ( 0 == length )   {
    return true;
  }
  else if ( !file.isOpen() )    {
    if ( file.open() == -1 )  {
      return false;
    }
  }
  int ret = file.write(space.get(), length);
  if ( ret )   {
    position_ptr += length;
    start  = 0;
    length = 0;
    events = 0;
    return true;
  }
  return false;
}

/// Shutdown the I/O medium
bool FileEventOutput::file_transaction_t::close()   {
  if ( file.isOpen() )    {
    file.close();
  }
  return true;
}

/// Reposition output buffer to specified location
std::size_t FileEventOutput::file_transaction_t::position(std::size_t new_position)   {
  if ( file.isOpen() )    {
    auto temp = file.position();
    file.position(new_position);
    return temp;
  }
  return 0;
}

/// Standard constructor
FileEventOutput::FileEventOutput(std::unique_ptr<RTL::Logger>&& logger)
  : EventOutput(std::move(logger), &config)
{
}

/// Standard destructor
FileEventOutput::~FileEventOutput()    {
}

/// Cancel all pending I/O requests to the buffer manager
int FileEventOutput::cancel()  {
  int count = 0;
  count = int(m_busyTr.size());
  while( m_busyTr.size() > 0 )   {
    {
      lock_t lock(m_transactionLock);
      for( auto& tr : m_busyTr )  {
	file_transaction_t* t = (file_transaction_t*)tr.get();
	t->start  = 0;
	t->length = 0;
	t->events = 0;
      }
    }
    {
      lock_t lock(m_transactionLock);
      if ( m_busyTr.empty() ) break;
    }
    ::lib_rtl_sleep(10);
    {
      lock_t lock(m_transactionLock);
      if ( m_busyTr.empty() ) break;
    }
  }
  queueCancel(true);      // Set cancel flag before waiting on mutex!
  return count;
}

/// Connect to the output buffer
int FileEventOutput::connect(const std::string& name_format, std::size_t instance)  {
  struct tm tm;
  char   timestr[1024];
  std::time_t now = ::time(0);
  std::stringstream nam;
  nam << instance;
  std::string fmt = RTL::str_replace(name_format, "%INSTANCE", nam.str());
  ::gmtime_r(&now, &tm);
  ::strftime(timestr, sizeof(timestr), fmt.c_str(), &tm);
  timestr[sizeof(timestr)-1] = 0;

  /// Insert producer into idle plug container
  auto file = std::make_unique<file_transaction_t>(timestr, this);
  if ( -1 == file->file.openWrite() )  {
    return ONLINE_ERROR;
  }
  lock_t lock(m_transactionLock);
  m_idleTr.emplace_back(std::move(file));
  m_haveTr.notify_one();
  return ONLINE_OK;
}
