//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Project    : Online
//
//  Author     : Markus Frank
//==========================================================================

/// Framework include files
#include <EventHandling/MBMEventAccess.h>
#include <RTL/strdef.h>

using namespace Online;

namespace {
  std::atomic<std::size_t> s_guardCount = 0;
  void mbm_dump_buffer(std::unique_ptr<RTL::Logger>& log, int* memory, std::size_t len)   {
    if ( memory )   {
      for(std::size_t i=0; i<len; i += 8*sizeof(int))   {
	log->error("MBM[%4d] %08x %08x %08x %08x %08x %08x %08x %08x",
		   i, memory[0], memory[1], memory[2], memory[3], memory[4], memory[5], memory[6], memory[7]);
      }
      return;
    }
    log->error("Invalid MBM -- NO MEMORY!");
  }

  bool header_selection_active(const EventAccess::config_t* cfg)   {
    const auto& v = cfg->headerVetoMask;
    for(int32_t i = 0; i < v.words(); ++i)
      if ( v.word(i) ) return true;
    const auto& m = cfg->headerTriggerMask;
    for(int32_t i = 0; i < m.words(); ++i)
      if ( m.word(i) != 0xFFFFFFFF )
	return true;
    return false;
  }
}

/// Multi event guard for MBM interaction
/**
 *  \author M.Frank
 */
template<typename T> class MBMEventAccess::mbm_guard_t : public EventAccess::guard_t {
public:

  std::unique_ptr<T> events {};
  MBMEventAccess*    access = nullptr;
  BMID               bmid   = MBM_INV_DESC;
public:

  mbm_guard_t(std::unique_ptr<T>&& e, std::vector<int32_t>&& bx, int64_t t, MBMEventAccess* a, BMID bm)
    : guard_t(std::move(bx), t), events(std::move(e)), access(a), bmid(bm)
  {
    num_evt = events->size();
    ++s_guardCount;
  }
  /// No default constructor
  mbm_guard_t() = delete;
  /// No move constructor
  mbm_guard_t(mbm_guard_t&& copy) = delete;
  mbm_guard_t& operator=(mbm_guard_t&& copy) = delete;
  mbm_guard_t& operator=(const mbm_guard_t& copy) = delete;
  /// Destructor
  virtual ~mbm_guard_t()    {
    if ( access && bmid != MBM_INV_DESC )  {
      access->requeue(bmid);
    }
    --s_guardCount;
  }
  virtual EventAccess::record_t at(std::size_t which) const override {
    return events->at(which);
  }
};

MBMEventAccess::MBMEventAccess(std::unique_ptr<RTL::Logger>&& logger)
  : EventAccess(std::move(logger))
{
  this->base_config = &config;
}

/// Print burst/event information
void MBMEventAccess::printInfo()  const    {
  if ( (this->monitor.burstsIn%this->base_config->burstPrintCount) == 0 )   {
    m_logger->info("+++ Burst-in:%11ld out:%8ld req:%8ld rel:%8ld Events-in:%10ld out:%10ld buffered:%4ld BUSY:%ld IDLE:%ld GUARD:%ld",
		   this->monitor.burstsIn,      this->monitor.burstsOut,
		   this->monitor.burstsRequest, this->monitor.burstsRelease, 
		   this->monitor.eventsIn,      this->monitor.eventsOut,
		   this->monitor.eventsBuffered,
		   this->m_busyPlugs.size(), this->m_idlePlugs.size(),
		   (std::size_t)s_guardCount);
  }
}

/// Create proper buffer name depending on partitioning
std::string MBMEventAccess::bufferName(const std::string& nam)  const   {
  std::string bm_name = nam;
  if ( this->config.partitionBuffers ) {
    bm_name += "_";
    bm_name += this->config.partitionName.empty() 
      ? RTL::to_hex(this->config.partitionID)
      : this->config.partitionName;
  }
  return bm_name;
}

/// Cancel all pending I/O requests to the buffer manager
int MBMEventAccess::cancel()  {
  int count = int(this->m_busyPlugs.size());
  std::size_t buffered;
  this->m_inhibit = true;
  for(std::size_t iloop=0; this->m_busyPlugs.size() > 0 && iloop<100; ++iloop)   {
    {
      lock_t lock(this->m_consumerLock);
      for( auto bmid : this->m_busyPlugs )
	::mbm_cancel_request(bmid);
    }
    {
      lock_t lock(this->m_consumerLock);
      if ( this->m_busyPlugs.empty() ) break;
    }
    // We try to wait a bit and give the event processing thread some time
    // to work down the event queue of possibly pending entries.
    // We assume here a maximum time window of 20*100 msec
    for( std::size_t i=0; i<20; ++i )   {
      ::lib_rtl_sleep(100);
      buffered = this->eventsBuffered();
      if ( buffered > 0 && (i%5) == 0 )  {
	this->m_logger->debug("+++ MBMEventAccess: "
			      "Current event stack size: %ld events. #buffers: %ld",
			      buffered, this->m_busyPlugs.size());
      }
      if ( i > 3 && 0 == buffered ) break;
    }
    if( std::size_t dropped=clear(); dropped > 0 )  {
      this->m_logger->warning("+++ MBMEventAccess: Dropped %ld events on cancel.", dropped);
    }
    {
      lock_t lock(this->m_consumerLock);
      if ( this->m_busyPlugs.empty() ) break;
    }
    ::lib_rtl_sleep(100);
  }
  this->queueCancel(true);      // Set cancel flag before waiting on mutex!
  buffered = this->eventsBuffered();
  if ( buffered > 0 )   {
    this->m_logger->warning("+++ MBMEventAccess: Final event stack size: %ld events. Busy: %ld",
		      buffered, this->m_busyPlugs.size());
  }
  return count;
}

/// Fill the event data cache if necessary
int MBMEventAccess::fill_cache()  {
  using guard40_t = mbm_guard_t<pcie40::event_collection_t>;
  using guard1_t  = mbm_guard_t<event_collection_t>;
  BMID bmid = MBM_INV_DESC;
  {
    lock_t lock(this->m_consumerLock);
    if ( this->m_idlePlugs.empty() && this->m_busyPlugs.empty() )   {
      return ONLINE_NOT_INITED;
    }
    if ( this->m_idlePlugs.empty() )   {
      return ONLINE_NO_EVENT;
    }
    bmid = this->m_idlePlugs.back();
    this->m_idlePlugs.pop_back();
    this->m_busyPlugs.emplace_back(bmid);
  }
  while ( !m_inhibit )  {
    uint32_t trmask[BM_MASK_SIZE];
    int32_t  sc, ev_type = 0, *ev_data = 0;
    int64_t  ev_len = 0;
    {
      lock_t lock(this->m_burstLock);
      ++this->monitor.burstsRequest;
    }
    sc = ::mbm_get_event(bmid, &ev_data, &ev_len, &ev_type, trmask, this->config.partitionID);
    if ( sc == MBM_NORMAL )  {
      union _data {
	int32_t*              mbm;
	event_header_t*       tell1;
	pcie40::mep_header_t* tell40;
	datapointer_t         pointer;
	_data(int32_t* p) { mbm = p; }
      } data(ev_data);
      if ( ev_len > 0 )   {
	switch(ev_type)   {
	case EVENT_TYPE_EVENT:
	case EVENT_TYPE_BURST:
	case EVENT_TYPE_MEP:  {
	  shared_guard_t burst;
	  if( data.tell40->is_valid() )   {
	    auto type = event_traits::tell40::data_type;
	    auto evts(this->convertPCIE40MEP(data.pointer, std::size_t(ev_len)));
	    burst = std::make_shared<guard40_t>(std::move(evts.second), std::move(evts.first), type, this, bmid);
	  }
	  else if ( data.tell1->is_mdf() )   {
	    auto type = event_traits::tell1::data_type;
	    auto evts(this->convertMultiMDF(data.pointer, std::size_t(ev_len)));
	    burst = std::make_shared<guard1_t>(std::move(evts.second), std::move(evts.first), type, this, bmid);
	  }
	  else   {
	    this->m_logger->error("MBM: Unknown event type (Not Tell1 or Tell40). Drop event buffer.");
	    this->m_logger->error("MBM: Event len:%d type:%d mask:%08X %08X %08X %08X",
				  ev_len, ev_type, trmask[0], trmask[1], trmask[2], trmask[3]);
	    mbm_dump_buffer(this->m_logger, data.mbm, 0x40);
	    break;
	  }
	  if ( burst.get() && !burst->empty() )  {
	    this->queueBurst(std::move(burst));
	    this->printInfo();
	    if ( this->monitor.eventsIn > this->config.maxEventsIn )   {
	      this->cancel();
	      return ONLINE_END_OF_DATA;
	    }
	    return ONLINE_OK;
	  }
	  // This should not happen:
	  // -- No pre-selection and empty burst.
	  // -- If OnlyTAE is set, there might be MEPs without TAE entry.
	  if ( !base_config->onlyTAE && !header_selection_active(base_config) )   {
	    this->m_logger->warning("MBM: Empty burst encountered. Event len:%d type:%d mask:%08X %08X %08X %08X",
				    ev_len, ev_type, trmask[0], trmask[1], trmask[2], trmask[3]);
	    ::lib_rtl_sleep(20);
	    break;
	  }
	  /// None of the events got selected. Requeue request and try again to get something we want.
	  ::mbm_free_event(bmid);
	  {
	    lock_t lock(this->m_burstLock);
	    ++this->monitor.burstsRelease;
	  }
	  continue;
	}
	default:
	  this->m_logger->error("MBM: Unknown MBM event type. Event len:%d type:%d mask:%08X %08X %08X %08X",
				ev_len, ev_type, trmask[0], trmask[1], trmask[2], trmask[3]);
	  ::lib_rtl_sleep(200);
	  break;
	}
      }
      else   {
	this->m_logger->error("MBM: Empty MBM frame encountered. Event len:%d type:%d mask:%08X %08X %08X %08X",
			      ev_len, ev_type, trmask[0], trmask[1], trmask[2], trmask[3]);
	mbm_dump_buffer(this->m_logger, data.mbm, 0x40);
	::lib_rtl_sleep(200);
      }
      ::mbm_free_event(bmid);
      this->requeueConsumer(bmid);   {
	lock_t lock(this->m_burstLock);
	++this->monitor.burstsRelease;
      }
      return ONLINE_OK;
    }
    else if ( sc == MBM_REQ_CANCEL )  {
      this->requeueConsumer(bmid);
      return ONLINE_CANCELLED;
    }
  }
  if ( bmid != MBM_INV_DESC )
    this->requeueConsumer(bmid);
  return ONLINE_CANCELLED;
}

/// Free busy consumer and re-attach to idle queue
bool MBMEventAccess::requeueConsumer(BMID consumer)   {
  lock_t lock(this->m_consumerLock);
  for( auto i=this->m_busyPlugs.begin(); i != this->m_busyPlugs.end(); ++i )    {
    if ( *i == consumer )   {
      this->m_idlePlugs.emplace_back(*i);
      this->m_busyPlugs.erase(i);
      return true;
    }
  }
  return false;
}

/// Free busy consumer and re-attach to idle queue
void MBMEventAccess::requeue(BMID consumer)   {
  if ( consumer && consumer != MBM_INV_DESC )   {
    lock_t lock(this->m_burstLock);
    ++this->monitor.burstsRelease;
    ::mbm_free_event(consumer);
    this->requeueConsumer(consumer);
  }
}

int MBMEventAccess::connect(const std::string&              input, 
			    const std::vector<std::string>& requests,
			    const std::string&              proc)
{
  std::string bm_name = this->bufferName(input);
  this->m_logger->info("+++ MBM: Connecting to MBM Buffer %s.", bm_name.c_str());
  if ( requests.empty() || requests.size() > 6 )   {
    return this->m_logger->error("MBM: Illegal number of MBM requests: %ld [1..6]", requests.size());
  }
  {
    lock_t lock(this->m_consumerLock);
    BMID bmid = this->m_idlePlugs.empty()
      ? ::mbm_include_read(bm_name.c_str(), proc.c_str(), this->config.partitionID, BM_COM_FIFO)
      : ::mbm_connect(this->m_idlePlugs.front(), proc.c_str(), this->config.partitionID);

    if ( bmid == MBM_INV_DESC )   {
      return this->m_logger->error("MBM: Failed to connect to MBM buffer %s!", bm_name.c_str());
    }
    for( const auto& r : requests )   {
      MBM::Requirement rq(r);
      int sc = ::mbm_add_req(bmid, rq.evtype, rq.trmask, rq.vetomask, rq.maskType, 
			     rq.userType, rq.freqType, rq.freq);
      if ( sc != MBM_NORMAL )   {
	this->m_logger->error("MBM: Failed to add MBM requirement: %s",r.c_str());
      }
    }
    /// Insert consumer into idle plug container
    this->m_idlePlugs.emplace_back(bmid);
  }
  this->config.input = input;
  this->m_inhibit = false;
  this->EventAccess::clear();
  return ONLINE_OK;
}

/// Close network connection
void MBMEventAccess::close()  {
  lock_t lock(this->m_consumerLock);
  for( auto bmid : this->m_idlePlugs )   {
    if ( bmid != MBM_INV_DESC )   {
      ::mbm_exclude(bmid);
    }
  }
  this->m_idlePlugs.clear();
  if ( !this->m_busyPlugs.empty() )   {
    this->m_logger->warning("Closing event access dispite pending transactions. "
			    "Asking for trouble ?");
  }
  this->config.input = "";
}
