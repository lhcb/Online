//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Project    : Online
//
//  Author     : M.Frank
//
//==========================================================================

/// Framework include files
#include <EventHandling/MBMEventOutput.h>
#include <EventData/event_header_t.h>

/// C/C++ include files
#include <iomanip>
#include <cstring>

using namespace Online;

namespace {
  template<typename INT>
  std::string to_hex(INT i) {
    std::stringstream s;
    s << std::hex << i;
    return s.str();
  }
}

/// Base class to handle one atomic write operation
/**
 *
 * @author  M.Frank
 * @version 1.0
 * @date    25/04/2019
 */
class MBMEventOutput::mbm_transaction_t : public EventOutput::transaction_t  {
public:
  /// Id of buffer manager inclusion
  BMID bmid = MBM_INV_DESC;
  /// Reference to owner
  MBMEventOutput* output {0};

public:
  /// Default constructor
  mbm_transaction_t() = delete;
  /// Initializing constructor
  mbm_transaction_t(BMID bm, MBMEventOutput* out) : bmid(bm), output(out) {}
  /// Move constructor
  mbm_transaction_t(mbm_transaction_t&& copy) = default;
  /// Move assignment
  mbm_transaction_t& operator=(mbm_transaction_t&& copy) = default;
  /// Inhibit copy assignment
  mbm_transaction_t& operator=(const mbm_transaction_t& copy) = delete;
  /// Default destructor
  virtual ~mbm_transaction_t() = default;
  /// Allocate buffer to store contiguous junk of data
  virtual bool reserve(std::size_t len)   override;
  /// Synchronize the buffered data with the medium
  virtual bool sync()   override;
  /// Shutdown the I/O medium
  virtual bool close()   override;
  /// Reposition output buffer to specified location
  virtual std::size_t position(std::size_t new_position)   override;
  /// Callback when space is availible (not really used)
  static int spaceAst(void* /* param */) {
    return MBM_NORMAL;
  }
};

/// Allocate buffer to store contiguous junk of data
bool MBMEventOutput::mbm_transaction_t::reserve(std::size_t len)   {
  buffer = 0;
  size   = 0;
  start  = 0;
  length = 0;
  events = 0;
  if ( bmid != MBM_INV_DESC && len > 0 )   {
    int sc, *ptr = 0;
    if ( output->config.max_events != 1 )  {
      /// In single event mode allocate the *exact* length
      len = std::max(len,output->config.allocation_size);
    }
    sc = ::mbm_get_space_a(bmid, len, &ptr, spaceAst, this);
    if ( sc == MBM_NORMAL )  {
      sc = ::mbm_wait_space(bmid);
      if ( sc == MBM_NORMAL )   {
	buffer = (data_t*)ptr;
	size   = len;
        return true;
      }
    }
  }
  return false;
}

/// Synchronize the buffered data with the medium
bool MBMEventOutput::mbm_transaction_t::sync()   {
  if ( bmid != MBM_INV_DESC )   {
    void *fadd;
    long  flen;
    int   sc;
    unsigned int mask[] = { ~0x0U, ~0x0U, ~0x0U, output->config.partitionID };

    if ( output->config.useEventMask )  {
      event_header_t* eh = (event_header_t*)buffer;
      ::memcpy(mask,&eh->subHeader().H1->triggerMask()[0], sizeof(mask));
    }
    output->checkConsumers(output->config.event_type, output->config.partitionID);
    sc = ::mbm_declare_event(bmid, length, 
			     output->config.event_type,
			     mask, 0, &fadd, &flen, output->config.partitionID);
    buffer = 0;
    size   = 0;
    start  = 0;
    length = 0;
    events = 0;
    if ( sc == MBM_REQ_CANCEL ) {
      output->m_logger->error("+++ Failed to declare event for MBM buffer %s. "
			      "Request was cancelled.",
			      mbm_buffer_name(bmid));
      return false;
    }
    else if ( sc == MBM_NORMAL )  {
      sc = ::mbm_send_space(bmid);
      if ( sc == MBM_REQ_CANCEL ) {
	output->m_logger->error("+++ Failed to declare event for MBM buffer %s. "
				"Request was cancelled.",
				mbm_buffer_name(bmid));
	return false;
      }
      return true;
    }
  }
  return false;
}

/// Shutdown the I/O medium
bool MBMEventOutput::mbm_transaction_t::close()   {
  if ( bmid != MBM_INV_DESC )   {
    output->m_logger->info("+++ Exclude from buffer: %s", mbm_buffer_name(bmid));
    ::mbm_exclude(bmid);
    bmid = MBM_INV_DESC;
    return true;
  }
  return false;
}

/// Reposition output buffer to specified location
std::size_t MBMEventOutput::mbm_transaction_t::position(std::size_t new_position)   {
  if ( new_position < size )   {
    auto temp = length;
    length = new_position;
    return temp;
  }
  return length;
}

/// Standard constructor
MBMEventOutput::MBMEventOutput(std::unique_ptr<RTL::Logger>&& logger)
  : EventOutput(std::move(logger), &config)
{
}

/// Standard destructor
MBMEventOutput::~MBMEventOutput()    {
}

/// Create proper buffer name depending on partitioning
std::string MBMEventOutput::bufferName(const std::string& nam)  const   {
  std::string bm_name = nam;
  if ( config.partitionBuffers ) {
    bm_name += "_";
    bm_name += config.partitionName.empty() 
      ? to_hex(config.partitionID)
      : config.partitionName;
  }
  return bm_name;
}

/// Cancel all pending I/O requests to the buffer manager
int MBMEventOutput::cancel()  {
  int count = 0;
  count = int(m_busyTr.size());
  queueCancel(true);      // Set cancel flag before waiting on mutex!
  while( m_numGetTr.load() > 0 )   {
    m_haveTr.notify_all();
    ::lib_rtl_sleep(10);
    m_logger->info("+++ MBM: %ld clients waiting to commit output.", m_numGetTr.load());
  }
  ::lib_rtl_sleep(1000);
  while( m_busyTr.size() > 0 )   {
    {
      lock_t lock(m_transactionLock);
      for( auto& tr : m_busyTr )  {
	mbm_transaction_t* t = (mbm_transaction_t*)tr.get();
	::mbm_cancel_request(t->bmid);
      }
    }
    {
      lock_t lock(m_transactionLock);
      if ( m_busyTr.empty() ) break;
    }
    ::lib_rtl_sleep(10);
    {
      lock_t lock(m_transactionLock);
      if ( m_busyTr.empty() ) break;
    }
  }
  return count;
}

/// Connect to the output buffer
int MBMEventOutput::connect(const std::string& output, std::size_t instance)  {
  std::stringstream nam;
  std::string bm_name = bufferName(output);
  nam << RTL::processName() << '.' << instance;
  m_logger->info("+++ MBM: Connecting to MBM Buffer %s.", bm_name.c_str());
  {
    lock_t lock(m_transactionLock);
    BMID bmid   = MBM_INV_DESC;
    if ( m_idleTr.empty() )  {
      bmid = ::mbm_include_write(bm_name.c_str(), nam.str().c_str(), config.partitionID, BM_COM_FIFO);
    }
    else  {
      mbm_transaction_t* tr = (mbm_transaction_t*)m_idleTr.front().get();
      bmid = ::mbm_connect(tr->bmid, nam.str().c_str(), config.partitionID);
    }
    if ( bmid == MBM_INV_DESC )   {
      return m_logger->error("+++ MBM: Failed to connect to MBM buffer %s!",
			     bm_name.c_str());
    }
    /// Insert producer into idle plug container
    m_idleTr.emplace_back(std::make_unique<mbm_transaction_t>(bmid, this));
  }
  m_haveTr.notify_one();
  if ( config.max_consumer_wait )    {
    m_mbmInfo = std::make_unique<MBM::BufferInfo>();
    m_mbmInfo->attach(bm_name.c_str());
    m_logger->info("+++ Enabled MBM consumer checks. Max Wait time: %d seconds",
		    config.max_consumer_wait);
  }
  return ONLINE_OK;
}

/// Check for consumer presence within a given time window
void MBMEventOutput::checkConsumers(int ev_type, unsigned int pid)   {
  if ( config.max_consumer_wait > 0 && m_mbmInfo.get() )    {
    int max_cons_wait = config.max_consumer_wait;
    while ( --max_cons_wait >= 0 )   {
      if ( m_mbmInfo->num_consumers_partid_evtype(ev_type, pid) != 0 )  {
	m_logger->debug("+++ Waiting for consumers subscribed to event type: %d "
			"partition:%08X time left: %d seconds",
			ev_type, pid, max_cons_wait);
	::lib_rtl_sleep(1000);
	continue;
      }
      break;
    }
    if ( max_cons_wait < 0 )   {
      m_logger->except("+++ FAILED: No suitable consumers subscribed to event type:"
		       " %d partition:%08X", ev_type, pid, max_cons_wait);
    }
  }
}
