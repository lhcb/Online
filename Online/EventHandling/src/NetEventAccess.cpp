//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Project    : Online
//
//  Author     : Markus Frank
//==========================================================================
//  NetEventAccess.cpp
//==========================================================================
//
//  Description: The Online::NetEventAccess component is able
//               to produce a list of event references given 
//               a set of "selection criteria".
//==========================================================================

/// Framework include files
#include <EventHandling/NetEventAccess.h>
#include <CPP/TimeSensor.h>
#include <CPP/Event.h>
#include <MBM/bmdef.h>
#include <WT/wtdef.h>

using namespace Online;
using namespace TRANSFER_NS;

class NetEventAccess::net_guard_t : public EventAccess::guard_t {
public:
  std::unique_ptr<event_collection_t> events   {};
  NetEventAccess*                     access = 0;
  data_t*                             buffer = 0;

public:
  net_guard_t(std::unique_ptr<event_collection_t>&& e, std::vector<int>&& bx, NetEventAccess* a, data_t* b)
    : guard_t(std::move(bx), event_traits::tell1::data_type), events(std::move(e)), access(a), buffer(b)
  {
    num_evt = events->size();
  }
  /// No default constructor
  net_guard_t() = delete;
  /// No move constructor
  net_guard_t(net_guard_t&& copy) = delete;
  net_guard_t& operator=(net_guard_t&& copy) = delete;
  net_guard_t& operator=(const net_guard_t& copy) = delete;
  /// Destructor
  virtual ~net_guard_t()    {
    if ( buffer ) delete [] buffer;
  }
  virtual record_t at(std::size_t which) const  override {
    return events->at(which);
  }
};

static void handle_death(const netheader_t& h, void* param, netentry_t*) {
  NetEventAccess* p = (NetEventAccess*)param;
  p->taskDead(h.name);
}

static void handle_event(const netheader_t& header, void* param, netentry_t* entry)  {
  NetEventAccess* p = (NetEventAccess*)param;
  NetEventAccess::data_t* buff = new NetEventAccess::data_t[header.size];
  int sc = net_receive(p->m_netPlug, entry, (char*)buff, header.size);
  if ( sc == NET_SUCCESS ) 
    p->handleData(header.name,header.size,buff);
  else
    delete [] buff;
}

NetEventAccess::NetEventAccess(std::unique_ptr<RTL::Logger>&& logger)
  : EventAccess(std::move(logger))
{
  base_config         = &config;
  m_lastEvent         = ::time(0);
  m_connected         = 0;
  m_timerActive       = 0;
  m_connectFail       = 0;
  config.cancelDeath  = false;
  config.eventTimeout = 30;
}

void NetEventAccess::taskDead(const std::string& who)  {
  m_logger->debug("The event data source %s died ....",who.c_str());
  if ( config.cancelDeath ) m_cancelled = true;
  m_connected = 0;
}

void NetEventAccess::handleData(const std::string& src, std::size_t size, data_t* buff)  {
  m_rearmActive = 0;
  m_logger->debug("Got event data from %s: %ld bytes.", src.c_str(), size);
  auto events(convertMultiMDF(buff, size));
  m_connected = 1;
  m_connectFail = 0;
  resetEventTimer();
  if ( !events.first.empty() && events.second && !events.second->empty() )  {
    std::shared_ptr<guard_t> guard = std::make_shared<net_guard_t>(std::move(events.second), std::move(events.first), this, buff);
    queueBurst(std::move(guard));
    printInfo();
    return;
  }
  delete [] buff;
}

int NetEventAccess::rearm()  {
  try  {
    if ( !m_cancelled ) {
      ++monitor.burstsRequest;
      m_rearmActive = 1;
      int sc = net_send(m_netPlug,&m_request,sizeof(m_request),config.source,WT_FACILITY_CBMREQEVENT);
      if ( sc == NET_SUCCESS )  {
	m_connected = 1;
	m_connectFail = 0;
	resetEventTimer();
	m_logger->debug("Sent event request to %s.", config.source.c_str());
	return ONLINE_OK;
      }
      ++m_connectFail;
      m_connected = 0;
      m_rearmActive = 0;
      return m_logger->error("Failed to send event request to %s.",config.source.c_str());
    }
    m_connected = 0;
    return ONLINE_OK;
  }
  catch(const std::exception& e)  {
    m_logger->error("Failed to read next event: %s.",e.what());
  }
  catch(...)  {
    m_logger->error("Failed to read next event: Unknown exception.");
  }
  m_connected = 0;
  ++m_connectFail;
  return ONLINE_ERROR;
}

int NetEventAccess::connect(const std::vector<std::string>& requests)
{
  m_request.reset();
  if ( !requests.empty() )   {
    for(int j=0; j<BM_MASK_SIZE;++j)  {
      m_request.trmask[j] = 0;
      m_request.vetomask[j] = ~0;
    }
    for (std::size_t i=0, n=requests.size(); i<n; ++i )  {
      MBM::Requirement r(requests[i]);
      for(int k=0; k<BM_MASK_SIZE; ++k)  {
        m_request.trmask[k]   |= r.trmask[k];
        m_request.vetomask[k] &= r.vetomask[k];
      }
    }
  }
  else   {
    for(int j=0; j<BM_MASK_SIZE;++j)  {
      m_request.trmask[j] = ~0;
      m_request.vetomask[j] = 0;
    }
  }
  if ( !m_netPlug )  {
    if ( config.process.empty() )   {
      config.process = RTL::nodeNameShort() + "::" + RTL::processName();
    }
    m_netPlug = net_init(config.process, NET_CLIENT);
  }
  net_subscribe(m_netPlug,this,WT_FACILITY_CBMEVENT,handle_event,handle_death);
  monitor.burstsRequest = 1;
  m_lastEvent = ::time(0);
  m_cancelled = false;
  m_connected = 1;
  if ( config.eventTimeout > 0 )  {
    m_timerActive = 1;
    TimeSensor::instance().add(this, config.eventTimeout, this);
  }
  return ONLINE_OK;
}

/// Close network connection
void NetEventAccess::close()  {
  cancel();
  m_lastEvent = ::time(0);
  m_cancelled = false;
  config.source = "";
}

/// Cancel all pending I/O requests to the buffer manager
int NetEventAccess::cancel()   {
  if ( config.eventTimeout > 0 )  {
    TimeSensor::instance().remove(this, this);
    m_timerActive = 0;
  }
  if ( m_netPlug )   {
    net_unsubscribe(m_netPlug,this,WT_FACILITY_CBMEVENT);
    net_close(m_netPlug);
    m_netPlug = 0;
  }
  m_connected = 0;
  m_cancelled = true;
  return ONLINE_OK;
}

/// Fill the event data cache if necessary
int NetEventAccess::fill_cache()   {
  if ( !m_rearmActive )   {
    if ( m_cancelled )
      return ONLINE_CANCELLED;
    else if ( m_connectFail > config.maxConnectFail )
      return ONLINE_OK;
    if ( m_connected || !(!m_timerActive && !m_cancelled) )  {
      return rearm();
    }
  }
  return ONLINE_OK;
}

/// Interactor handler routine
void NetEventAccess::handle(const CPP::Event& ev)         {
  if ( ev.eventtype == TimeEvent )  {
    long now  = ::time(0);
    long diff = now - m_lastEvent;
    if ( diff > config.eventTimeout )  {
      m_logger->warning("Renew event requests due to timeout....no event since "
			"%d seconds [Timeout:%d].", int(diff), config.eventTimeout);
      m_rearmActive = 0;
      if ( m_netPlug && !config.source.empty() )  {
	this->m_cancelled = false;
	rearm();
      }
    }
    if ( config.eventTimeout > 0 )  {
      m_timerActive = 1;
      TimeSensor::instance().add(this,config.eventTimeout,this);
    }
  }
}

/// Routine called to reset the timer.
void NetEventAccess::resetEventTimer()    {
  m_lastEvent = ::time(0);
}
