//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Project    : Online
//
//  Author     : M.Frank
//
//==========================================================================

/// Framework include files
#include <RTL/posix.h>

/// C/C++ include files
#include <cstdio>
#include <cstring>
#include <sys/stat.h>

#include <map>
#include <cctype>
#include <iostream>

#include <unistd.h>
#define O_BINARY 0

/// ROOT include files
#include <TFile.h>
#include <TSystem.h>
#include <TUrl.h>


namespace {

  typedef std::map<int, TFile*> FileMap;

  //#define POSIX_ROOT_DEBUG

  FileMap& fileMap() {
    static FileMap s_fileMap;
    return s_fileMap;
  }
  
  int root_open( const char* filepath, int flags, ... ) {
    int mode = 0;
    TFile*  f = nullptr;
    TUrl    url( filepath );
    TString opts = "filetype=raw", proto, spec, tmp = url.GetOptions();

    if (flags & O_CREAT)    {
      va_list arg;
      va_start(arg, flags);
      mode = va_arg(arg, int);
      va_end(arg);
    }
    if ( tmp.Length() > 0 ) {
      opts += "&";
      opts += url.GetOptions();
    }
    url.SetOptions( opts );
    proto = url.GetProtocol();
    if ( proto == "file" || proto == "http" ) {
      spec = filepath;
      spec += "?filetype=raw";
    } else {
      spec = url.GetUrl();
    }
#ifdef POSIX_ROOT_DEBUG
    std::cout << "URL:" << url.GetUrl() << std::endl;
    std::cout << "   opts:    " << url.GetOptions() << std::endl;
    std::cout << "   protocol:" << url.GetProtocol() << std::endl;
    std::cout << "   specs:   " << (const char*)spec << std::endl;
#endif
    if ( ( flags & ( O_WRONLY | O_CREAT ) ) != 0 && ( ( mode & S_IWRITE ) != 0 ) ) {
      f = TFile::Open( spec, "RECREATE", "", 0 );
    } else if ( ( flags == O_RDONLY || flags == ( O_BINARY | O_RDONLY ) ) && ( mode & S_IREAD ) != 0 ) {
      f = TFile::Open( spec );
    }
    if ( f && !f->IsZombie() ) {
      static int fd = 0xFEED;
      ++fd;
      fileMap()[fd] = f;
      return fd;
    }
    return -1;
  }
  int root_close( int fd ) {
    auto i = fileMap().find( fd );
    if ( i != fileMap().end() ) {
      TFile* f = i->second;
      if ( f ) {
        if ( !f->IsZombie() ) f->Close();
        delete f;
      }
      fileMap().erase( i );
      return 0;
    }
    return -1;
  }
  int root_access( const char* nam, int mode ) {
    return kFALSE == gSystem->AccessPathName( nam, ( mode & S_IWRITE ) != 0 ? kWritePermission : kReadPermission ) ? 0
                                                                                                                   : -1;
  }
  int     root_unlink( const char* name) {
    return gSystem->Unlink(name);
  }
  int64_t root_lseek64( int fd, int64_t offset, int how ) {
    auto i = fileMap().find( fd );
    if ( i != fileMap().end() ) {
      TFile*   f = i->second;
      Long64_t off;
      switch ( how ) {
      case SEEK_SET:
        f->Seek( offset, TFile::kBeg );
        return f->GetRelOffset();
      case SEEK_CUR:
        f->Seek( offset, TFile::kCur );
        return f->GetRelOffset();
      case SEEK_END:
        off = f->GetRelOffset();
        f->Seek( offset, TFile::kEnd );
        return offset == 0 && off == f->GetSize() ? off : f->GetRelOffset();
      }
    }
    return -1;
  }
  
  long root_lseek( int s, long offset, int how ) {
    return (long)root_lseek64( s, offset, how );
  }
  
  ssize_t root_read( int fd, void* ptr, size_t size ) {
    auto i = fileMap().find( fd );
    if ( i != fileMap().end() ) {
      TFile* f = i->second;
      if ( f->GetBytesRead() + int64_t(size) > f->GetSize() ) {
#ifdef POSIX_ROOT_DEBUG
        std::cout << "TFile::Read> Bytes read:" << f->GetBytesRead() << " Size:" << f->GetSize()
                  << " Relative offset:" << (long)f->GetRelOffset() << std::endl;
#endif
        return 0;
      }
      if ( f->ReadBuffer( (char*)ptr, size ) == 0 ) return size;
    }
    return -1;
  }
  ssize_t root_write( int fd, const void* ptr, size_t size ) {
    auto i = fileMap().find( fd );
    if ( i != fileMap().end() ) {
      if ( ( *i ).second->WriteBuffer( (const char*)ptr, size ) == 0 ) return size;
    }
    return -1;
  }
  int   root_stat( const char* /* path */, struct stat* /*statbuf */ ) { return -1; }
  int   root_stat64( const char* /* path */, struct stat64* /* statbuf */ ) { return -1; }
  int   root_fstat( int /* s */, struct stat* /* statbuf */ ) { return -1; }
  int   root_fstat64( int /* s */, struct stat64* /* statbuf */ ) { return -1; }
} // namespace

extern "C" Online::posix_t* root_posix_descriptor() {
  static Online::posix_t p;
  if ( !p.open ) {
    p.unbuffered = Online::posix_t::COMPLETE;
    p.open       = root_open;
    p.close      = root_close;
    p.read       = root_read;
    p.write      = root_write;
    p.lseek      = root_lseek;
    p.lseek64    = root_lseek64;
    p.access     = root_access;
    p.unlink     = root_unlink;
    p.stat       = root_stat;
    p.stat64     = root_stat64;
    p.fstat      = root_fstat;
    p.fstat64    = root_fstat64;

    p.buffered  = Online::posix_t::NONE;
    p.fopen     = 0;
    p.fclose    = 0;
    p.fwrite    = 0;
    p.fread     = 0;
    p.fseek     = 0;
    p.ftell     = 0;

    p.directory = Online::posix_t::NONE;
    p.rmdir     = 0;
    p.mkdir     = 0;
    p.opendir   = 0;
    p.readdir   = 0;
    p.closedir  = 0;
  }
  return &p;
}
