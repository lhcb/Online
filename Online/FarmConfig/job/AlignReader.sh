#!/bin/bash
#
NODE=`hostname -s | tr a-z A-Z`;
export NODE_OPTIONS=${DYNAMIC_OPTS}/Nodes/${NODE}_TriggerInfo.opts;
export GO_SERVICE_NAME=""#${PARTITION_NAME}_${NODE}/Trigger;
if test "$NODE" = "ignore-test-HLTC1020"; then
  export LD_LIBRARY_PATH=/home/frankm/cmtuser/OnlineDev_v5r31/InstallArea/x86_64-slc6-gcc49-do0/lib:$LD_LIBRARY_PATH;
  export PATH=/home/frankm/cmtuser/OnlineDev_v5r31/InstallArea/x86_64-slc6-gcc49-do0/bin:$PATH;
  echo "[WARNING] Running TEST version of HLT2 reader";
fi;
##echo "[error] Go service name is: ${GO_SERVICE_NAME}";
act=${ACTIVITY}
export AlignDir="/localdisk/Alignment/`echo $act|cut -d "|" -f 2`"
exec -a ${UTGID} ${DATAFLOW_task} -class=Class2 -opts=../options/${TASK_TYPE}.opts
