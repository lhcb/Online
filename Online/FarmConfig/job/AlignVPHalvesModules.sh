#!/bin/bash
###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
set -euo pipefail

if test -z "${DIM_DNS_NODE-}"; then
   if [ -r /etc/sysconfig/dim ]; then
       . /etc/sysconfig/dim
   fi
fi

export UTGID=${UTGID}
if test -n "${LOGFIFO-}"; then
    export LOGFIFO=${LOGFIFO}
fi

export DIM_DNS_NODE

if test -z "${BINARY_TAG-}"; then
    # Default BINARY_TAG, used when this script is called from ECS
    export BINARY_TAG=x86_64_v2-centos7-gcc10-dbg
fi

if test -z "${SCRIPT-}"; then
    SCRIPT=`realpath $0`
fi
export BINARY_TAG=x86_64_v2-centos7-gcc11-dbg;
SCRIPT=/group/online/alignment/OnlineAlignmentStack_RTAAlign/MooreOnline/MooreScripts/job/runMoore2.sh;
SCRIPT_DIR=`dirname ${SCRIPT}`
MOORESCRIPTS_DIR=`dirname ${SCRIPT_DIR}`
MOOREONLINE_DIR=`dirname ${MOORESCRIPTS_DIR}`

# FIXME the following does not work as I don't know how to run cmsetup.py as part of the install
# because xenv is not in available. Maybe find_package(xenv) would work?
# see CMakeLists.txt in this directory.

# . ${MOOREONLINE_DIR}/InstallArea/${BINARY_TAG}/setup.vars

# FIXME the following does not work as xenv with the new cmake does not have the data packages runtime?

# setup_vars=${SCRIPT_DIR}/setup.vars
# bash -c ". /cvmfs/lhcb.cern.ch/lib/LbEnv-stable; python ${MOORESCRIPTS_DIR}/scripts/cmsetup.py ${MOOREONLINE_DIR}/InstallArea/${BINARY_TAG}" > ${setup_vars}
# . "${setup_vars}"
# rm "${setup_vars}"

# Instead, use the build runtime environment
current_args=( "$@" )
set --  # unset arguments, see https://its.cern.ch/jira/browse/LBCORE-2005
set +u
. /cvmfs/lhcb.cern.ch/lib/LbEnv-stable.sh
set -u
set -- "${current_args[@]}"  # restore arguments, used by createEnvironment.sh later
. ${MOOREONLINE_DIR}/build.${BINARY_TAG}/mooreonlineenv.sh

set +u
. ${FARMCONFIGROOT}/job/createEnvironment.sh $*
set -u

export PYTHONPATH=/group/online/dataflow/options/${PARTITION}:${PYTHONPATH}
if test "${PARTITION}" = "TEST"; then
    export PYTHONPATH=${ONLINE_PROJECT_ROOT}/TestBeam/options:${PYTHONPATH}
fi

DEBUG=
echo "+++ BINARY_TAG: ${BINARY_TAG}"
echo "+++ SCRIPT:     ${SCRIPT}"
echo "+++ SCRIPT_DIR: ${SCRIPT_DIR}"
echo "+++ PYTHONPATH  ${PYTHONPATH}"
echo "+++ Task type:  ${TASK_TYPE}"
echo "+++ Partition:  ${PARTITION}"

##export JOBOPTSDUMPFILE=$MOOREONLINECONFROOT/options/hlt2.dump

cd ${SCRIPT_DIR}
# FIXME does the following still hold, i.e. can we debug fine when using genPython.exe?
# For some reason when we use genPython.exe gdb does not load the symbols from the shared libraries.
# Instead, use python directly, which works except that messages in the log viewer are not properly
# tagged by the process name.
exec -a ${UTGID} genPython `which gaudirun.py` --application=Online::OnlineEventApp \
    $MOOREONLINECONFROOT/options/tags.py \
    $HUMBOLDTROOT/options/AlignVPHalvesModules_Analyzer.py \
    $MOOREONLINECONFROOT/options/align_input.py


##    /group/online/dataflow/cmtuser/ONLINE_v7r15/Online/FarmConfig/job/freis_align_input.py
