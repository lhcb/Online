import os
import sys
import socket
import Gaudi
import fifo_log
import Configurables
from   GaudiOnline   import Application
from   GaudiOnline   import FlowManager
from   GaudiOnline   import MSG_VERBOSE, MSG_DEBUG, MSG_INFO, MSG_WARNING, MSG_ERROR, MSG_FATAL, MSG_ALWAYS
from   Configurables import Online__FlowManager as FlowManager

global _debug_flag
_debug_flag = False
_data_dir = '/group/online/dataflow/cmtuser/OnlineRelease/Online/FarmConfig/data'

class AlignApp(Application):
  """
      Alignment worker test class.

      @author  M.Frank
      @version 1.0
      @date    24/01/2022
  """
  def __init__(self, outputLevel, partitionName='OFFLINE', partitionID=0xFFFF):
    Application.__init__(self, 
                         outputLevel=outputLevel,
                         partitionName=partitionName, 
                         partitionID=partitionID)
    utgid = os.getenv('UTGID','Worker')
    # First setup printing device:
    self.setup_online_fifolog()
    fifo_log.logger_set_utgid(utgid)
    fifo_log.logger_set_tag(partitionName)
    self.app_mgr = Gaudi.Configuration.ApplicationMgr()
    # Basic application configuration
    self.config.numEventThreads         = 0
    self.config.numStatusThreads        = 0
    self.config.autoStart               = False
    self.app_mgr.EvtSel                 = 'NONE'
    self.app_mgr.EvtMax                 = -1
    self.app_mgr.StopOnSignal           = False # = default
    self.app_mgr.StalledEventMonitoring = False # = default
    Gaudi.Configurables.EventLoopMgr().Warnings = False

class AlignWorker(AlignApp):
  """
      Alignment worker test class.

      @author  M.Frank
      @version 1.0
      @date    24/01/2022
  """
  def __init__(self, outputLevel, utgid, partitionName='OFFLINE', partitionID=0xFFFF):
    AlignApp.__init__(self, 
                      outputLevel=outputLevel,
                      partitionName=partitionName, 
                      partitionID=partitionID)
    # Setup alignment worker
    wrk = Gaudi.Configurables.Online__AlignWork('Worker')
    wrk.PartitionName    = partitionName
    wrk.ReferenceFile    = _data_dir + os.sep + 'align_reference.dat'
    self.app_mgr.Runable = wrk
    #self.config.runable = wrk
    self.config.enablePause = True
    # This is the fitter tool:
    tool = wrk.addTool(Gaudi.Configurables.Online__FitterFcn(), 'FCN')
    tool.PartitionName = partitionName
    tool.DataFile      = _data_dir + os.sep + 'align_data.dat'
    tool.ParameterFile = _data_dir + os.sep + 'align_params.dat'
    wrk.FitterFunction = tool
    if outputLevel < MSG_WARNING: print(str(wrk))
    #------------------------------------------------------------
    #self.setup_monitoring_service()
    #------------------------------------------------------------
    self.setup_monitoring(utgid)
    self.setup_hive(FlowManager("EventLoop"), 40)
    self.setup_algorithms()

    self.monSvc.DimUpdateInterval     =  5
    self.monSvc.CounterUpdateInterval =  5
    self.config.execMode              =  1
    self.config.numEventThreads       =  1
    self.config.numStatusThreads      =  1
    self.config.FILE_maxEventsIn      = 10
    print('ALWAYS  %-24s Process starting... ExecMode: %d' % ('AlignWorker', self.config.execMode, ));

  def setup_algorithms(self, acceptRate=1.0):
    import Gaudi.Configuration as Gaudi
    import Configurables
    input_files = [
      '/group/online/dataflow/cmtuser/data/mdf/file_000.mdf',
      '/group/online/dataflow/cmtuser/data/mdf/file_001.mdf'
    ]
    input_files = [
      '/group/online/dataflow/cmtuser/data/mdf/Run_0000224883_20220121-065749-312_UCEB01_5921.mdf',
      '/group/online/dataflow/cmtuser/data/mdf/Run_0000224883_20220121-065749-312_UCEB01_5921.mdf',
      '/group/online/dataflow/cmtuser/data/mdf/Run_0000224883_20220121-065749-312_UCEB01_5921.mdf',
      '/group/online/dataflow/cmtuser/data/mdf/Run_0000224883_20220121-065749-312_UCEB01_5921.mdf',
      '/group/online/dataflow/cmtuser/data/mdf/Run_0000224883_20220121-065749-312_UCEB01_5921.mdf',
      '/group/online/dataflow/cmtuser/data/mdf/Run_0000224883_20220121-065749-312_UCEB01_5921.mdf',
      '/group/online/dataflow/cmtuser/data/mdf/Run_0000224883_20220121-065749-312_UCEB01_5921.mdf',
      '/group/online/dataflow/cmtuser/data/mdf/Run_0000224883_20220121-065749-312_UCEB01_5921.mdf'
      ]
    self.setup_file_access(input_files);
    self.input                  = Configurables.Online__InputAlg('EventInput')
    self.input.DeclareData      = True
    self.input.DeclareEvent     = True
    self.input.ExpandTAE        = False
    self.input.MakeRawEvent     = False
    sequence                    = Gaudi.GaudiSequencer('EventProcessor')
    sequence.Members            = [self.input]
    if self.updateAndReset:
      sequence.Members.append(self.updateAndReset)

    passThrough                 = Configurables.Online__Passthrough('Passthrough')
    passThrough.RawGuard        = '/Event/Banks/RawDataGuard'
    passThrough.AcceptRate      = 1.0
    sequence.Members.append(passThrough)
    self.sequence               = sequence
    self.app.TopAlg             = [self.sequence]
    self.broker.DataProducers   = self.app.TopAlg
    return self
    
class AlignDriver(AlignApp):
  """
      Alignment iterator test class.

      @author  M.Frank
      @version 1.0
      @date    24/01/2022
  """
  def __init__(self, outputLevel, dns='mon01', partitionName='OFFLINE', partitionID=0xFFFF):
    AlignApp.__init__(self,
                      outputLevel=outputLevel,
                      partitionName=partitionName,
                      partitionID=partitionID)
    host = socket.gethostname()
    print('Initializing Alignment driver object. Partition: %s' % (partitionName,))
    self.setup_monitoring_service()
    self.monSvc.CounterUpdateInterval = -1
    self.config.enableStop     = True
    self.config.enablePause    = False
    self.config.enableContinue = True
    # Setup alignment worker
    drv = Gaudi.Configurables.Online__AlignDrv('Driver')
    drv.PartitionName    = partitionName
    drv.ReferenceFile    = _data_dir + os.sep + 'align_reference.dat'
    conf                 = Gaudi.Configurables.Online__Fitter()
    tool                 = drv.addTool(conf, 'iterator')
    tool.PartitionName   = partitionName
    tool.ParamFile       = _data_dir + os.sep + 'align_params.dat'
    tool.CounterTask     = partitionName + '_' + host.lower() + '_AligWrk'
    tool.CounterNames    = [ 'Worker.FCN/Chi2', 'Events/IN' ]
    tool.CounterDNS      = dns
    drv.FitterFunction   = tool
    self.app_mgr.Runable = drv
    self.config.runable  = drv
    if outputLevel < MSG_WARNING: print(str(drv))

def run_app():
  import fifo_log
  import OnlineEnvBase as OnlineEnv

  outputLevel = OnlineEnv.OutputLevel
  part  = OnlineEnv.PartitionName
  utgid = os.getenv('UTGID','Worker')
  type  = os.getenv('TASK_TYPE','Worker')
  dns   = 'hlt01'
  print('INFO    %-24s Partition: %s dns: %s Lvl: %d'%( type, part, dns, outputLevel, ))

  if type == 'AlignWorker' or type == 'AligWrk':
    app = AlignWorker(outputLevel=outputLevel, utgid=utgid, partitionName=part, )
  elif type == 'AlignDriver' or type == 'AligDrv':
    app = AlignDriver(outputLevel=outputLevel, dns=dns, partitionName=part, )

  #print('ALWAYS   attachdbg  '+utgid)
  sys.stdout.flush()
    
# Invoke the application:
run_app()
