#!/bin/bash
# =========================================================================
#
#  Default script to start the Allen on a farm node.
#
#  Author   T. Colombo
#  Version: 2.0
#  Date:    09/2024
#
# =========================================================================

set -efu
set -o pipefail

echo "$UTGID: HLT1 Stack = ${MOORE_VERSION}"
echo "$UTGID: HLT1 Architecture = ${HLT_ARCHITECTURE}"

if [[ "${HLT_ARCHITECTURE}" == *cuda* ]]; then
    gpu_num=${UTGID##*_} # UTGID is: ${partition}_${hostname}_Allen_${instance_num}
    if test -n "$gpu_num" 2>/dev/null && ! test "$gpu_num" -eq "$gpu_num" 2>/dev/null; then
        echo "$UTGID: ERROR: last field of UTGID must be Allen instance number"
        exit 1
    fi
    IFS=$'\n'
    gpus=($(nvidia-smi --query-gpu=pci.bus_id,index,uuid --format=csv,noheader | sort))
    unset IFS
    if test "$gpu_num" -ge "${#gpus[@]}"; then
        echo "$UTGID: ERROR: GPU number $gpu_num not found"
        exit 1
    fi
    IFS=', '
    gpu=(${gpus[$gpu_num]})
    unset IFS
    addr=${gpu[0]}
    index=${gpu[1]}
    uuid=${gpu[2]}
    proc=$(nvidia-smi --query-compute-apps=name --format=csv,noheader --id="$index")
    if test -n "$proc"; then
        echo "$UTGID: ERROR: GPU number $gpu_num already used by $proc"
        exit 1
    fi
    export CUDA_VISIBLE_DEVICES=$uuid
    numa_cmd="hwloc-bind --cpubind pci=$addr --membind pci=$addr"
else
    numa_cmd=''
fi

source /group/hlt/commissioning/"$MOORE_VERSION"/MooreOnline/build."$HLT_ARCHITECTURE"/mooreonlineenv.sh
export PYTHONPATH=$DYNAMIC_OPTS:$PYTHONPATH

nice_cmd="nice --adjustment=5"
allen_cmd="genPython.exe '$(command -v gaudirun.py)' '$ALLENONLINEROOT/options/AllenConfig.py' --application=AllenApplication"
$numa_cmd $nice_cmd sh -c "exec -a '$UTGID' $allen_cmd"
