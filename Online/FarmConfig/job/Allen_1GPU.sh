#!/bin/bash
# =========================================================================
#
#  Default script to start the Allen on a farm node.
#
#  Author   R. Aaij, M. Fontanta, D. vom Bruch, C. Agapopoulou, D. Campora
#  Version: 1.0
#  Date:    05/2022
#
# =========================================================================

source /cvmfs/lhcb.cern.ch/lib/LbEnv

if [ -z "${MOORE_VERSION}" ]; then
  echo "ERROR: HLT1 Stack not set"
  return 3
elif [ -z "${HLT_ARCHITECTURE}" ]; then
  echo "ERROR: HLT1 Architecture not set"
  return 4
fi

echo "HLT1 Stack = ${MOORE_VERSION}"
echo "HLT1 Architecture = ${HLT_ARCHITECTURE}"

if [[ "${PARTITION}" = "TDET" || "${PARTITION}" = "FEST" ]]; then

NUMA_DOMAIN=${UTGID: -1} # partition_hostname_allen_number

if [[ "${HLT_ARCHITECTURE}" == *"cuda"* ]]; then
  has_gpu="`lspci | grep -i NVIDIA | grep -P '(2231|A5000)'`"; # 2231 is the A5000 when the hwids are not there
  if [ -z "$has_gpu" ]; then
    echo "GPU not found"
    echo "ERROR: HLT1 Architecture contains substring cuda and GPU not found"
    return 1
  else
    echo "GPU found"
    nvidia-smi
    if [ $? -ne 0 ]; then
      echo "WARNING: nvidia-smi returned an error code"
    fi
    if [[ `nvidia-smi -i ${NUMA_DOMAIN}` != *"No running processes found"* ]]; then
      echo "ERROR: GPU is under use by another process"
      return 5
    fi
    #NUMA_DOMAIN=`nvidia-smi topo -m | grep GPU0 | tail -1 | awk '{ print $NF; }'`
    #unset CUDA_VISIBLE_DEVICES
  fi
fi

else

NUMA_DOMAIN=0

if [[ "${HOSTNAME}" == "cieb01" ]]; then
  NUMA_DOMAIN=0
  HLT_ARCHITECTURE="x86_64_v2-centos9-gcc11-opt"
elif [[ "${HLT_ARCHITECTURE}" == *"cuda"* ]]; then
  has_gpu="`lspci | grep -i NVIDIA | grep -P '(2231|A5000)'`"; # 2231 is the A5000 when the hwids are not there
  if [ -z "$has_gpu" ]; then
    echo "GPU not found"
    echo "ERROR: HLT1 Architecture contains substring cuda and GPU not found"
    return 1
  else
    echo "GPU found"
    nvidia-smi
    if [ $? -ne 0 ]; then
      echo "WARNING: nvidia-smi returned an error code"
    fi
    if [[ `nvidia-smi -i 0` != *"No running processes found"* ]]; then
      echo "ERROR: GPU is under use by another process"
      return 5
    fi
    NUMA_DOMAIN=`nvidia-smi topo -m | grep GPU0 | tail -1 | awk '{ print $NF; }'`
    unset CUDA_VISIBLE_DEVICES
  fi
fi

fi

source /group/hlt/commissioning/${MOORE_VERSION}/MooreOnline/build.${HLT_ARCHITECTURE}/mooreonlineenv.sh
export PYTHONPATH=${DYNAMIC_OPTS}:${PYTHONPATH}

CUDA_VISIBLE_DEVICES=${NUMA_DOMAIN} numactl --cpunodebind=${NUMA_DOMAIN} --membind=${NUMA_DOMAIN} sh -c "exec -a ${UTGID} genPython.exe `which gaudirun.py` ${ALLENONLINEROOT}/options/AllenConfig.py --application=AllenApplication;"
