#!/bin/bash
# =========================================================================
#
#  Default script to start the Allen on a farm node.
#
#  Author   R. Aaij, M. Fontanta, D. vom Bruch
#  Version: 1.0
#  Date:    05/2022
#
# =========================================================================
#
HOST=`hostname`;

has_gpu="`lspci | grep -i NVIDIA | grep 2231`"; # 2231 is the A5000

#if [ -z "$has_gpu" ] ; then
echo "using CPU debug build"
source /cvmfs/lhcb.cern.ch/lib/LbEnv -c x86_64_v2-centos7-gcc11-dbg
if [-z ${MOORE_VERSION} ] ; then
    echo "MOORE_VERSION not set, don't know which HLT1 stack build to choose"
else
    source /group/hlt/commissioning/${MOORE_VERSION}/MooreOnline/build.x86_64_v2-centos7-gcc11-dbg/mooreonlineenv.sh
fi;
#else
#    echo "Found GPU: $has_gpu"
#    source /cvmfs/lhcb.cern.ch/lib/LbEnv -c x86_64_v2-centos7-clang12-opt
#    nvidia-smi
#    if [ $? -ne 0 ] ; then
#	echo "ERROR: nvidia-smi is not working"
#	return -1
#    fi;
#    if [ -z ${MOORE_VERSION} ] ; then
#	echo "MOORE_VERSION not set, don't know which HLT1 stack build to choose"
#    else
#	source /group/hlt/commissioning/${MOORE_VERSION}/MooreOnline/build.x86_64_v2-centos7-clang12-opt/mooreonlineenv.sh
#    fi;
#fi;

export PYTHONPATH=${DYNAMIC_OPTS}:${PYTHONPATH}
unset CUDA_VISIBLE_DEVICES;

numactl --cpunodebind=0 sh -c "exec -a ${UTGID} genPython.exe `which gaudirun.py` ${ALLENONLINEROOT}/options/AllenConfig.py --application=AllenApplication;"


# if [ -z "$has_gpu" ] ; then # || [ "$HOST" == "hceb04" ] ; then
# #if [ -z "$has_gpu" ] || [ "$HOST" == "r1eb22" ] ; then
#     echo "Did not find GPU"
#     source /cvmfs/lhcb.cern.ch/lib/LbEnv -c x86_64_v2-centos7-gcc11-opt
#     if test "${PARTITION}" = "LHCb"; then
# 	source /group/hlt/commissioning/cosmics_stack/MooreOnline/build.x86_64_v2-centos7-gcc11-opt/mooreonlineenv.sh
# 	#source /group/hlt/commissioning/cosmics_stack/MooreOnline/setup.x86_64_v2-centos7-gcc11-opt.vars
#     elif test "${PARTITION}" = "CALO"; then
# 	source /group/hlt/commissioning/cosmics_stack/MooreOnline/build.x86_64_v2-centos7-gcc11-opt/mooreonlineenv.sh
# 	#source /group/hlt/commissioning/cosmics_stack/MooreOnline/setup.x86_64_v2-centos7-gcc11-opt.vars
#     elif test "${PARTITION}" = "FEST"; then
# 	#source /cvmfs/lhcb.cern.ch/lib/LbEnv -c x86_64_v2-centos7-gcc11-dbg
# 	source /group/hlt/commissioning/fest/MooreOnline/build.x86_64_v2-centos7-gcc11-dbg/mooreonlineenv.sh
# 	#source /group/hlt/commissioning/fest/MooreOnline/setup.x86_64_v2-centos7-gcc11-dbg.vars
#     elif test "${PARTITION}" = "TDET"; then
# 	source /group/hlt/commissioning/large_scale_eb/MooreOnline/build.x86_64_v2-centos7-gcc11-opt/mooreonlineenv.sh
# 	#source /group/hlt/commissioning/large_scale_eb/MooreOnline/setup.x86_64_v2-centos7-gcc11-opt.vars
#     else
#        echo "Unknown partition - don't know which stack to use"
#        return -1
#     fi;
# else
#     echo "Found GPU"
#     echo "$has_gpu"
#     source /cvmfs/lhcb.cern.ch/lib/LbEnv -c x86_64_v2-centos7-clang12-opt
#     nvidia-smi
#     if [ $? -ne 0 ] ; then
# 	echo "ERROR: nvidia-smi is not working"
# 	return -1
#     fi;
#     if test "${PARTITION}" = "LHCb"; then
# 	source /group/hlt/commissioning/cosmics_stack/MooreOnline/build.x86_64_v2-centos7-clang12-opt/mooreonlineenv.sh
# 	#source /group/hlt/commissioning/cosmics_stack/MooreOnline/setup.x86_64_v2-centos7-clang12-opt.vars
#     elif test "${PARTITION}" = "CALO"; then
# 	source /group/hlt/commissioning/cosmics_stack/MooreOnline/build.x86_64_v2-centos7-clang12-opt/mooreonlineenv.sh
# 	#source /group/hlt/commissioning/cosmics_stack/MooreOnline/setup.x86_64_v2-centos7-clang12-opt.vars
#     elif test "${PARTITION}" = "FEST"; then
# 	#source /cvmfs/lhcb.cern.ch/lib/LbEnv -c x86_64_v2-centos7-clang12-dbg
# 	source /group/hlt/commissioning/fest/MooreOnline/build.x86_64_v2-centos7-clang12-dbg/mooreonlineenv.sh
# 	#source /group/hlt/commissioning/fest/MooreOnline/setup.x86_64_v2-centos7-clang12-dbg.vars
#     elif test "${PARTITION}" = "TDET"; then
# 	source /group/hlt/commissioning/large_scale_eb/MooreOnline/build.x86_64_v2-centos7-clang12-opt/mooreonlineenv.sh
# 	#source /group/hlt/commissioning/large_scale_eb/MooreOnline/setup.x86_64_v2-centos7-clang12-opt.vars 
#     else
# 	echo "Unknown partition - don't know which stack to use"
# 	return -1
#     fi;
# fi;

# export PYTHONPATH=${DYNAMIC_OPTS}:${PYTHONPATH}
# unset CUDA_VISIBLE_DEVICES;

# numactl --cpunodebind=0 sh -c "exec -a ${UTGID} genPython.exe `which gaudirun.py` ${ALLENONLINEROOT}/options/AllenConfig.py --application=AllenApplication;"

# gdb --args numactl --cpunodebind=0 sh -c "exec -a ${UTGID} genPython.exe `which gaudirun.py` ${ALLENONLINEROOT}/options/Allen.py --application=AllenApplication;" <</EOF
# run
# thread apply all bt
# quit
# /EOF


#numactl --cpunodebind=0 sh -c "exec -a ${UTGID} genPython.exe `which gaudirun.py` ${ALLENONLINEROOT}/options/Allen.py --application=AllenApplication;"

#numactl --cpunodebind=0 sh -c "exec -a ${UTGID} `which genPython.exe` `which gaudirun.py` ${ALLENONLINEROOT}/options/Allen.py --application=AllenApplication;"
#numactl --cpunodebind=0 sh -c "echo $PATH" > /tmp/PATH.log
#
#
#
# cd ${SMICONTROLLERROOT}/scripts;
# exec -a ${UTGID} genPython.exe ${SMICONTROLLERROOT}/scripts/DummyTask.py -utgid ${UTGID} -partition 103 -print ${OUTPUT_LEVEL};
