#!/bin/bash
# =========================================================================
#
#  Default script to start the buffer manager on the HLT farm worker node
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    05/03/2021
#
# =========================================================================
#
rm -f /dev/shm/bm_buff_Events_0_${PARTITION};
rm -f /dev/shm/bm_comm_Events_0_${PARTITION};
rm -f /dev/shm/bm_ctrl_Events_0_${PARTITION};
rm -f /dev/shm/bm_buff_Events_1_${PARTITION};
rm -f /dev/shm/bm_comm_Events_1_${PARTITION};
rm -f /dev/shm/bm_ctrl_Events_1_${PARTITION};
#
rm -f /dev/shm/bm_comm_Output_${PARTITION};
rm -f /dev/shm/bm_buff_Output_${PARTITION};
rm -f /dev/shm/bm_ctrl_Output_${PARTITION};
#
export MBM_ACCIDENT_OPTIONS=${STATIC_OPTS}/Empty.opts;
#
if test -n "${MBMINIT_FLAGS}"; then
  export MBMINIT_FLAGS="`echo ${MBMINIT_FLAGS}`";
  echo "+++ Apply external MBM initialization flags: ${MBMINIT_FLAGS}";
elif test "${PARTITION}" = "LHCb"; then
   export MBMINIT_FLAGS="";
   export MBMINIT_FLAGS="${MBMINIT_FLAGS} -s=32000000 -e=150  -u=30 -b=14 -t=1 -n=0 -f -i=Events_0 -c";
   export MBMINIT_FLAGS="${MBMINIT_FLAGS} -s=32000000 -e=150  -u=30 -b=14 -t=1 -n=1 -f -i=Events_1 -c";
   export MBMINIT_FLAGS="${MBMINIT_FLAGS} -s=10000000 -e=3000 -u=12 -b=14 -t=1      -f -i=Output   -c";
   export MBM_ACCIDENT_OPTIONS=${STATIC_OPTS}/EBCrashHandler.opts;
elif test "${PARTITION}" = "FEST"; then
   export MBMINIT_FLAGS="";
   export MBMINIT_FLAGS="${MBMINIT_FLAGS} -s=24000000 -e=150 -u=20 -b=14 -t=1 -n=0 -f -i=Events_0 -c";
   export MBMINIT_FLAGS="${MBMINIT_FLAGS} -s=24000000 -e=150 -u=20 -b=14 -t=1 -n=1 -f -i=Events_1 -c";
   export MBMINIT_FLAGS="${MBMINIT_FLAGS} -s=5000000  -e=150 -u=10 -b=14 -t=1      -f -i=Output   -c";
   export MBM_ACCIDENT_OPTIONS=${STATIC_OPTS}/EBCrashHandler.opts;
else
   export MBMINIT_FLAGS="-s=5000000 -e=150 -u=20 -b=12 -t=1 -n=0 -f -i=Events_0 -c -s=5000000 -e=150 -u=20 -b=12 -t=1 -n=1 -f -i=Events_1 -c -s=500000 -e=150 -u=10 -b=12 -t=1 -f -i=Output -c";
fi;
execute `dataflow_task Class0` -opts=${STATIC_OPTS}/${TASK_TYPE}.opts ${AUTO_STARTUP} ${DEBUG_STARTUP};
