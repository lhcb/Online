#!/bin/bash
# =========================================================================
#
#  Default script to start the buffer manager on the HLT farm worker node
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    05/03/2021
#
# =========================================================================
#
rm -f /dev/shm/bm_buff_Events_0_${PARTITION};
rm -f /dev/shm/bm_comm_Events_0_${PARTITION};
rm -f /dev/shm/bm_ctrl_Events_0_${PARTITION};
#
export MBM_ACCIDENT_OPTIONS=${STATIC_OPTS}/Empty.opts;
#
if test -n "${MBMINIT_FLAGS}"; then
  export MBMINIT_FLAGS="`echo ${MBMINIT_FLAGS}`";
  echo "+++ Apply external MBM initialization flags: ${MBMINIT_FLAGS}";
elif test "${PARTITION}" = "LHCb" -o "${PARTITION}" = "TDET"; then
   export MBMINIT_FLAGS="-s=32000000 -e=150  -u=30 -b=14 -t=1 -n=0 -f -i=Events_0 -c";
   export MBM_ACCIDENT_OPTIONS=${STATIC_OPTS}/EBCrashHandler_IN0.opts;
elif test "${PARTITION}" = "FEST"; then
   export MBMINIT_FLAGS="-s=24000000 -e=150 -u=20 -b=14 -t=1 -n=0 -f -i=Events_0 -c";
   export MBM_ACCIDENT_OPTIONS=${STATIC_OPTS}/EBCrashHandler_IN0.opts;
else
   export MBMINIT_FLAGS="-s=5000000 -e=150 -u=20 -b=12 -t=1 -n=0 -f -i=Events_0 -c";
fi;
execute `dataflow_task Class0` -opts=${STATIC_OPTS}/EBMBM.opts ${AUTO_STARTUP} ${DEBUG_STARTUP};
