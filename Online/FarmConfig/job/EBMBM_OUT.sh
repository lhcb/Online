#!/bin/bash
# =========================================================================
#
#  Default script to start the buffer manager on the HLT farm worker node
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    05/03/2021
#
# =========================================================================
#
rm -f /dev/shm/bm_comm_Output_${PARTITION};
rm -f /dev/shm/bm_buff_Output_${PARTITION};
rm -f /dev/shm/bm_ctrl_Output_${PARTITION};
#
. /group/online/dataflow/cmtuser/OnlineRelease/setup.x86_64_v2-el9-gcc13-opt.vars;
export MBM_ACCIDENT_OPTIONS=${STATIC_OPTS}/Empty.opts;
#
if test -n "${MBMINIT_FLAGS}"; then
  export MBMINIT_FLAGS="`echo ${MBMINIT_FLAGS}`";
  echo "+++ Apply external MBM initialization flags: ${MBMINIT_FLAGS}";
elif test "${PARTITION}" = "LHCb" -o "${PARTITION}" = "TDET"; then
   export MBMINIT_FLAGS="";
   export MBMINIT_FLAGS="${MBMINIT_FLAGS} -s=10000000 -e=3000 -u=16 -b=14 -t=1 -f -i=Output   -c";
elif test "${PARTITION}" = "FEST"; then
   export MBMINIT_FLAGS="";
   export MBMINIT_FLAGS="${MBMINIT_FLAGS} -s=5000000  -e=150  -u=16 -b=14 -t=1 -f -i=Output   -c";
else
   export MBMINIT_FLAGS="-s=500000 -e=150 -u=10 -b=12 -t=1 -f -i=Output -c";
fi;
execute `dataflow_task Class0` -opts=${STATIC_OPTS}/EBMBM.opts ${AUTO_STARTUP} ${DEBUG_STARTUP};
