#!/bin/bash
# =========================================================================
#
#  Default script to start the buffer manager on the HLT farm worker node
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    05/03/2021
#
# =========================================================================
#
if [[ "$UTGID" == *"${TASK_TYPE}_0"* ]]; then
  export MBM_INPUT_BUFFER=Events_0;
elif [[ "$UTGID" == *"${TASK_TYPE}_1"* ]]; then
  export MBM_INPUT_BUFFER=Events_1;
elif [[ "$UTGID" == *"${TASK_TYPE}_2"* ]]; then
  export MBM_INPUT_BUFFER=Events_0;
elif [[ "$UTGID" == *"${TASK_TYPE}_3"* ]]; then
  export MBM_INPUT_BUFFER=Events_1;
elif [[ "$UTGID" == *"${TASK_TYPE}_4"* ]]; then
  export MBM_INPUT_BUFFER=Events_0;
else
  export MBM_INPUT_BUFFER=Events_1;
fi;
#
export MBM_OUTPUT_BUFFER=Output;
if test "${PARTITION_NAME}" = "TDET"; then
    export MBM_EVENT_EXPLORER=YES;
    export EXPAND_TAE=YES;
    # export ONLY_TAE=YES;
    export DEBUG=YES;
fi;
# TAE for SD in local uncomment to enable
if test "${PARTITION_NAME}" = "MUON"; then
   export EXPAND_TAE=YES;
fi;
if test "${PARTITION_NAME}" = "SF"; then
   export EXPAND_TAE=YES;
fi;
if test "${PARTITION_NAME}" = "VELO"; then
   export EXPAND_TAE=YES;
fi;
# if test "${PARTITION_NAME}" = "RICH"; then
# export EXPAND_TAE=YES;
# fi;
# TAE global 
if test "${PARTITION_NAME}" = "LHCb"; then
    export EXPAND_TAE=YES;
fi;
#
#
if test "${PARTITION}" = "FEST"; then
  export MBM_INCLUSION=ONE;
else
  export MBM_INCLUSION=ONE;
fi;
execute `gaudi_event_task ${FARMCONFIGROOT}/job/Passthrough.py`;
