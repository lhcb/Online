#!/bin/bash
# =========================================================================
#
#  Default script to start the buffer manager on the HLT farm worker node
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    05/03/2021
#
# =========================================================================
#
if [[ "$UTGID" == *"${TASK_TYPE}_0"* ]]; then
  export MBM_OUTPUT_BUFFER=Events_0;
elif [[ "$UTGID" == *"${TASK_TYPE}_1"* ]]; then
  export MBM_OUTPUT_BUFFER=Events_1;
elif [[ "$UTGID" == *"${TASK_TYPE}_2"* ]]; then
  export MBM_OUTPUT_BUFFER=Events_0;
elif [[ "$UTGID" == *"${TASK_TYPE}_3"* ]]; then
  export MBM_OUTPUT_BUFFER=Events_1;
elif [[ "$UTGID" == *"${TASK_TYPE}_4"* ]]; then
  export MBM_OUTPUT_BUFFER=Events_0;
else
  export MBM_OUTPUT_BUFFER=Events_1;
fi;
export RUN_NUMBER_SERVICE=${PARTITION}/RunInfo/RunNumber;
if test -n "${INPUT_OPTIONS}"; then
    export INPUT_OPTIONS;
    echo "[INFO] Input data from ${INPUT_OPTIONS}";
elif test -f /group/online/dataflow/options/${PARTITION}/FileInput.opts; then
    export INPUT_OPTIONS=/group/online/dataflow/options/${PARTITION}/FileInput.opts;
else
    export INPUT_OPTIONS=${FARMCONFIGROOT}/options/Empty.opts;
fi;
execute `dataflow_task Class2` -opts=${STATIC_OPTS}/${TASK_TYPE}.opts ${AUTO_STARTUP} ${DEBUG_STARTUP};
