#!/bin/bash
#
export PYTHONPATH=${ONLINE_ENV_DIR}:${PYTHONPATH};
#
# -----------------------------------------------------------------------------------------------------------
rfilen()
{
  export PRESCALER_REQUIREMENTS=Default.prescaler-requirements
  act1=$1
  IFS="|" read -a splt <<<"$act1"
  for ((i=${#splt[@]};i>0;i--))
  do
    fn=${splt[0]}
    for ((x=1;x<$i;x++))
    do
      echo $x
      fn+="-"
      fn+=${splt[x]}
    done
    rfn="../options/"
    rfn+=$fn".prescaler-requirements"
    echo $rfn
    if [ -f ${rfn} ];
    then
       export PRESCALER_REQUIREMENTS=${rfn};
       break
    fi
  done
}
# -----------------------------------------------------------------------------------------------------------
act=${ACTIVITY}
#
rfilen ${act}
export EVENTSELECTOR_REQ1="EvType=2;TriggerMask=0xffffffff,0xffffffff,0xffffffff,0xffffffff;VetoMask=0,0,0,0;MaskType=ANY;UserType=USER;Frequency=PERC;Perc=100.0";
export TAN_PORT=YES;
export TAN_NODE=${DATAINTERFACE};
export SENDER_TARGET="${MONITORING_DISTBOX}::${PARTITION}_${MONITORING_DISTBOX}_RCVMon";
echo [INFO] Eventselector request:      ${EVENTSELECTOR_REQ1};
echo [INFO] Prescaler Requirement File: ${PRESCALER_REQUIREMENTS};
if test -z "${OPTIONS}"; then
    export OPTIONS=${STATIC_OPTS}/${TASK_TYPE}.opts;
fi;
# -----------------------------------------------------------------------------------------------------------
execute `dataflow_task Class2` -opts=${OPTIONS} ${AUTO_STARTUP} ${DEBUG_STARTUP};
