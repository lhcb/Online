#!/bin/bash
#
export PYTHONPATH=${ONLINE_ENV_DIR}:${PYTHONPATH};
#
export EVENTSELECTOR_REQ1="EvType=2;TriggerMask=0xffffffff,0xffffffff,0xffffffff,0xffffffff;VetoMask=0,0,0,0;MaskType=ANY;UserType=USER;Frequency=PERC;Perc=100.0";
export TAN_PORT=YES;
export TAN_NODE=${DATAINTERFACE};
export SENDER_TARGET="${MONITORING_DISTBOX}::${PARTITION}_${MONITORING_DISTBOX}_RCVMon";
echo [INFO] Eventselector request:      ${EVENTSELECTOR_REQ1};
if test -z "${OPTIONS}"; then
    export OPTIONS=${STATIC_OPTS}/${TASK_TYPE}.opts;
fi;
execute `dataflow_task Class2` -opts=${OPTIONS} ${AUTO_STARTUP} ${DEBUG_STARTUP};
