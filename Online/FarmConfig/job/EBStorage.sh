#!/bin/bash
# =========================================================================
#
#  Default script to start the buffer manager on the HLT farm worker node
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    05/03/2021
#
# =========================================================================
#
if test -z "${OPTIONS}"; then
    OPTIONS=${STATIC_OPTS}/${TASK_TYPE}.opts;
fi;
if test -z "${CLASS}"; then
    CLASS=Class1;
fi;
# ----------------------------------------------------------------------------------------------
if test -n "${OUTPUT_OPTIONS}"; then
    export OUTPUT_OPTIONS;
    echo "[INFO] Output data definitions from ${OUTPUT_OPTIONS}";
else
    export OUTPUT_OPTIONS=${FARMCONFIGROOT}/options/BBMD_Write.opts;
    if test "${PARTITION_NAME}" = "FEST_xxxx"; then 
	echo "[WARNING] Enable data compression mode";
	export OUTPUT_OPTIONS=${FARMCONFIGROOT}/options/BBMD_Write_Compress.opts;
    fi;
fi;
execute `dataflow_task Class1` -opts=${OPTIONS} ${AUTO_STARTUP} ${DEBUG_STARTUP};
