#!/bin/bash
# =========================================================================
#
#  Default script to start the buffer manager on the HLT farm worker node
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    05/03/2021
#
# =========================================================================
#
if test -z "${OPTIONS}"; then
    OPTIONS=${STATIC_OPTS}/${TASK_TYPE}.opts;
fi;
# ----------------------------------------------------------------------------------------------
execute `dataflow_task Class1` -opts=${OPTIONS} ${AUTO_STARTUP} ${DEBUG_STARTUP};
