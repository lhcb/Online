#!/bin/bash
# =========================================================================
#
#  Default script to start the buffer manager on the HLT farm worker node
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    05/03/2021
#
# =========================================================================
#
if test -z "${EVENTSERVER_SOURCE}"; then
    export EVENTSERVER_SOURCE=MON0101::FEST_MON0101_EVENTSRV;
    export TAN_NODE=MON0101;
fi;
#
`dataflow_task Class2` -opts=${FARMCONFIGROOT}/options/EventReq.opts ${AUTO_STARTUP} ${DEBUG_STARTUP};
