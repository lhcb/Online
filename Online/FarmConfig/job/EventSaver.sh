#!/bin/bash
# =========================================================================
#
#  Default script to start the buffer manager on the HLT farm worker node
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    05/03/2021
#
# =========================================================================
#
##cd /group/online/dataflow/cmtuser/ONLINE/ONLINE_v7r22p1;
##. setup.x86_64_v2-el9-gcc12-do0.vars;
`dataflow_task Class1` -opts=${DYNAMIC_OPTS}/MONITORING/${UTGID}.opts ${AUTO_STARTUP} ${DEBUG_STARTUP};
