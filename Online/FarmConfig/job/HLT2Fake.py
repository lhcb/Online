"""
     Minimal Gaudi task in the online environment

     @author M.Frank
"""
from __future__ import print_function
__version__ = "1.0"
__author__  = "Markus Frank <Markus.Frank@cern.ch>"

import os
import sys
import Configurables
import Gaudi.Configuration as Gaudi

import GaudiOnline
import OnlineEnvBase as OnlineEnv

from Configurables import Online__FlowManager as FlowManager

application = None

def config_writer():
  writer = application.setup_mbm_output('EventOutput')
  writer.MBM_maxConsumerWait     = 10
  writer.MBM_allocationSize      = 1024*1024*32
  writer.MBM_burstPrintCount     = 25000
  writer.MaxEventsPerTransaction = 10
  writer.RequireODIN             = False
  writer.UseTAELeaves            = False
  writer.UseRawData              = False
  writer.UseRawGuard             = False
  if os.getenv("WRITER_USE_RAW_GUARD", False):
    writer.UseRawGuard           = True
  elif os.getenv("WRITER_USE_TAE_LEAVES", False):
    writer.UseTAELeaves          = True
  else:
    writer.UseRawData            = True

  if os.getenv("MODIFY_ACCEPT_MASK", False):
    writer.AcceptMask = [
      [ 86,    3 ],  # BEAMGAS stream fraction in %
      [ 87,   50 ],  # FULL    stream fraction in %
      [ 88,   25 ],  # TURBO   stream fraction in %
      [ 89,   16 ],  # PARK    stream fraction in %
      [ 90,    6 ],  # CALIB   stream fraction in %
     ]
    writer.AcceptMask = [
      [ 87,  100 ],  # FULL    stream fraction in %
    ]
    print(f'INFO +++ Accept mask will be changed: {str(writer.AcceptMask)}')
  else:
    print('INFO +++ Accept mask left unchanged!')
  return writer;

application = GaudiOnline.Passthrough(outputLevel=OnlineEnv.OutputLevel,
                                      partitionName=OnlineEnv.PartitionName,
                                      partitionID=OnlineEnv.PartitionID,
                                      classType=GaudiOnline.Class1)
application.setup_fifolog()
application.config.debug       = False

# Want to debug? Uncomment this line to attach to a waiting process.
# application.config.debug_wait  = 1000
buffer_name = os.getenv("MBM_INPUT_BUFFER","Events")
application.setup_mbm_access(buffer_name, True)

writer  = None
if OnlineEnv.EnableWriting:
  writer = config_writer()

processor = None
if os.getenv("EVENT_PROCESSING_ALGORITHM",None):
  processor = Configurables.Online__EventSize('EventSize')

delay = None
if os.getenv("EVENT_PROCESSING_DELAY",None):
  delay_tmo = float(os.getenv("EVENT_PROCESSING_DELAY","1.0"))
  delay = Configurables.Online__DelayAlg('Delay')
  delay.Event = delay_tmo

# -----------------------------------------------------------------------------
# Store explorer algorithm
explorer = None
if os.getenv("MBM_EVENT_EXPLORER", False):
  print_freq = 0.0000001
  try:
    print_freq = float(os.getenv("MBM_EVENT_EXPLORER"))
  except:
    pass
  explorer = GaudiOnline.store_explorer(print_freq=print_freq)
  explorer.Load = 1
#
#
part = OnlineEnv.PartitionName
have_odin = True
#
application.setup_hive(FlowManager("EventLoop"), 40)
application.setup_monitoring(have_odin=have_odin)

algos = []
if processor: algos.append(processor)
if explorer:  algos.append(explorer)
if delay:     algos.append(delay);
if writer:    algos.append(writer)
application.setup_algorithms(algos, 1.0)
application.passThrough.StartDelay = 5
application.passThrough.AcceptRate = OnlineEnv.AcceptRate
#
# Set to one to really "burn" CPU time
application.passThrough.BurnCPU   = 0
if os.getenv("BURN_CPU"):
  burn_tmo = float(os.getenv("BURN_CPU"))
  print(f'INFO +++ Passthrough CPU burner set to: {burn_tmo} milliseconds')
  application.passThrough.MicroDelayTime = 1000.0*burn_tmo

if OnlineEnv.passThroughDelay > 0:
  print(f'INFO +++ Passthrough event delay set to: {OnlineEnv.passThroughDelay} milliseconds')
  application.passThrough.MicroDelayTime = 1000.0*OnlineEnv.passThroughDelay

application.monSvc.DimUpdateInterval   = 5
#
# Do not declare LHCb::RawEvent, but only pairs: (bank-header, data) 
application.input.MakeRawEvent         = False
#
# Do not expand TAE events in the transient store
# application.config.onlyTAE             = True
application.config.expandTAE           = True
application.config.burstPrintCount     = 30000

# Mode slection::  synch: 0 async_queued: 1 sync_queued: 2
application.config.execMode            = 1
application.config.numEventThreads     = 15
application.config.MBM_numConnections  = 8
application.config.MBM_numEventThreads = 5
#
application.config.numEventThreads     = 2
application.config.MBM_numConnections  = 3
application.config.MBM_numEventThreads = 1
#
if os.getenv("numEventThreads"):
  application.config.numEventThreads     = int(os.getenv("numEventThreads"))
if os.getenv("MBM_numConnections"):
  application.config.MBM_numConnections  = int(os.getenv("MBM_numConnections"))
if os.getenv("MBM_numEventThreads"):
  application.config.MBM_numEventThreads = int(os.getenv("MBM_numEventThreads"))

application.enableUI()  
#
# Enable this for debugging
#
_dbg = 0
if os.getenv("DEBUG"):
  _dbg = str(os.getenv("DEBUG"))

if _dbg:
  application.config.execMode            = 0
  application.config.numEventThreads     = 1
  application.config.MBM_numConnections  = 1
  application.config.MBM_numEventThreads = 1
#
application.config.MBM_requests = [
    'EvType=2;TriggerMask=0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF;VetoMask=0,0,0,0;MaskType=ANY;UserType=ONE;Frequency=PERC;Perc=100.0',
    'EvType=1;TriggerMask=0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF;VetoMask=0,0,0,0;MaskType=ANY;UserType=ONE;Frequency=PERC;Perc=100.0'
]
#  
print(f'Setup done. Use ODIN: {str(have_odin)} '      + \
      f'threads: {application.config.numEventThreads} ' + \
      f'MBM (input: {buffer_name} ' + \
      f'conn: {application.config.MBM_numConnections} '  + \
      f'threads: {application.config.MBM_numEventThreads}) ' + \
      f'algs: delay: {OnlineEnv.passThroughDelay} [ms] ' + \
      f'processor: {os.getenv("EVENT_PROCESSING_ALGORITHM","NO")} ' + \
      f'explorer: {os.getenv("MBM_EVENT_EXPLORER","NO")} ' + \
      f'delay2: {os.getenv("EVENT_PROCESSING_DELAY","0")} [ms] ' + \
      f'writer: {OnlineEnv.EnableWriting} ')

sys.stdout.flush()
