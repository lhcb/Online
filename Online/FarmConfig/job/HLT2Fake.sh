#!/bin/bash
## =========================================================================
#
#  Default script to start the buffer manager on the HLT farm worker node
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    05/03/2021
#
# =========================================================================
#
#export BURN_CPU=150;
#export BURN_CPU=0.15;
unset BURN_CPU;
unset DEBUG;
#export numEventThreads=25;
#export MBM_numConnections=3;
#export MBM_numEventThreads=2;
###export BURN_CPU=100.0;
export DEBUG=1
#
export MODIFY_ACCEPT_MASK=YES;
export MBM_EVENT_EXPLORER=0.0001;
export EVENT_PROCESSING_ALGORITHM=YES;
export WRITER_USE_RAW_GUARD=True;
#
exec -a ${UTGID} genPython.exe `which gaudirun.py` ${FARMCONFIGROOT}/job/HLT2Fake.py --application=Online::OnlineEventApp;
