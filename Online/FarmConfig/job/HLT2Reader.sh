#!/bin/bash
# =========================================================================
#
#  Generic farm task startup script
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    20/05/2013
#
# =========================================================================
#`dataflow_task Class2` -opts=/group/online/dataflow/cmtuser/OnlineRelease/TestBeam/options/MDFProd.opts
if test -n "${INPUT_OPTIONS}"; then
    export INPUT_OPTIONS;
    export MBM_OUTPUT_BUFFER=Events;
    echo "[INFO] Input data from ${INPUT_OPTIONS}";
else
    export INPUT_OPTIONS=${FARMCONFIGROOT}/options/Empty.opts;
fi;
execute `dataflow_task Class2` -opts=../options/HLT2Reader.opts
