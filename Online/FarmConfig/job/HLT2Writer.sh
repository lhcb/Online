#!/bin/bash
# =========================================================================
#
#  Default script to start the buffer manager on the HLT farm worker node
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    05/03/2021
#
# =========================================================================
#
if test -n "${OUTPUT_OPTIONS}"; then
    export OUTPUT_OPTIONS;
    echo "[INFO] Output data definitions from ${OUTPUT_OPTIONS}";
else
    export OUTPUT_OPTIONS=${FARMCONFIGROOT}/options/Empty.opts;
fi;
if test "${PARTITION_NAME}" = "FEST"; then
    export OUTPUT_OPTIONS=${FARMCONFIGROOT}/options/WriterCompression.opts;
elif test "${COMPRESS_OUTPUT}" != ""; then
    export OUTPUT_OPTIONS=${FARMCONFIGROOT}/options/WriterCompression.opts;
fi;
if test -z "${OPTIONS}"; then
    OPTIONS=../options/HLT2Writer.opts;
fi;
#
#
execute `dataflow_task Class1` -opts=${OPTIONS} ${AUTO_STARTUP} ${DEBUG_STARTUP};
