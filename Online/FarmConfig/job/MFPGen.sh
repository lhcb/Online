#!/bin/bash
# =========================================================================
#
#  Default script to start the buffer manager on the HLT farm worker node
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    05/03/2021
#
# =========================================================================
#
. /group/online/dataflow/EventBuilder/EventBuilderRelease/install.ebonly.x86_64-el9-gcc13-opt/bin/thisonline.sh;
cd ${FARMCONFIGROOT}/job;
#
`dataflow_task Class1` -opts=../../EventBuilding/options/${TASK_TYPE}.opts ${AUTO_STARTUP} ${DEBUG_STARTUP};
