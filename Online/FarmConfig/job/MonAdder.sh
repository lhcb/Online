#!/bin/bash
# =========================================================================
#
#  Default script to start the event reader task on the HLT farm
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    20/05/2013
#
# =========================================================================
export ADDER_TYPE=MonitoringAdder;
exec -a ${UTGID} genPython.exe `which gaudirun.py` ./AddersFromArchitecture.py --application=Online::OnlineApplication;
