#!/bin/bash
# =========================================================================
#
#  Script to start the monitoring registrar
#
#  Author   R. Aaij
#  Version: 1.0
#  Date:    19/07/2017
#
# =========================================================================
#
export TAN_PORT=YES;
export TAN_NODE=${DATAINTERFACE};
if test "`uname -a | grep \".el6\"`" != "";   # SLC6
then
    export CMTCONFIG=x86_64-slc6-gcc62-dbg
elif test "`uname -a | grep el7`" != "";      # SLC7 (Centos 7)
then
    export CMTCONFIG=x86_64-centos7-gcc62-dbg
fi;

read COMMAND <<EOF
from Monitoring import Registrar;\
Registrar.run(auto=True)
EOF

. /group/hlt/monitoring/ONLINE/ONLINE_v6r3/setup.$CMTCONFIG.vars
export UTGID
python -c "${COMMAND}"
# exec -a ${UTGID} ${Class1_task} -opt=command="${COMMAND}"
