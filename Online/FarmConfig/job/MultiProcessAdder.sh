#!/bin/bash
# =========================================================================
#
#  Default script to start the histogram/counter adder task
#  on the MONITORING farm
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    20/05/2013
#
# =========================================================================
exec -a ${UTGID} genPython.exe `which gaudirun.py` \
     /group/online/dataflow/options/${PARTITION}/MultiProcessAdder.py \
     --application=Online::OnlineApplication;
