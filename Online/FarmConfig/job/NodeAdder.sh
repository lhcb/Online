#!/bin/bash
# =========================================================================
#
#  Default script to start the event reader task on the HLT farm
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    20/05/2013
#
# =========================================================================
#
#export victimnodes="hltb0113(.*)"
export PYTHONPATH=${ONLINE_ENV_DIR}:${PYTHONPATH};
export AdderOptsFile=/tmp/${PARTITION_NAME}_AdderOpts.opts;
export AdderStaticOpts=../options/NodeAdderStatic.opts
python ./Tasklist_from_architecture.py 1 ${AdderOptsFile};
#export RTL_SLEEP_ON_FATAL=1
if test "${PARTITION_NAME}" = "LHCb"; then
 exec -a ${UTGID} ${Class1_task} -opts=../options/genAdder.opts;
# exec -a ${UTGID} ~beat/scripts/debug --command=~beat/AdderDebugScript --args ${Class1_task} -opts=../options/genAdder.opts;
fi
if test "${PARTITION_NAME}" = "LHCbA"; then
  exec -a ${UTGID} ${Class1_task} -opts=../options/genAdder.opts;
#  exec -a ${UTGID} ~beat/scripts/debug --command=~beat/AdderDebugScript --args ${Class1_task} -opts=../options/genAdder.opts;
else
  exec -a ${UTGID} ${Class1_task} -opts=../options/genAdder.opts;
fi
#exec -a ${UTGID} ${Class0_task} -opt=../options/Daemon.opts -main=../options/genAdder.opts
