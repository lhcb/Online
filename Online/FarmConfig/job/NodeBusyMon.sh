#!/bin/bash
cd /group/online/dataflow/cmtuser
export User_release_area=`pwd`;
export CMTPROJECTPATH=`pwd`:${CMTPROJECTPATH};
source OnlineRelease/setup.x86_64-centos7-gcc62-do0.vars
cd OnlineRelease/Online/FarmConfig/job
export HOST=`hostname`
export UTGID=GEN_${HOST}_BusyMon
export LOGFIFO=/run/fmc/logGaudi.fifo
export AddTest=AddTest_Victim.opts
export vnodes="hlt[a-f](.*)"
if [ -z `echo $HOST |egrep -e $vnodes` ] ; then export AddTest=AddTest_EMPTY.opts ; fi
echo "[INFO] AddTest = $AddTest"
###exec -a ${UTGID}
#export LD_LIBRARY_PATH="/home/beat/cmtuser/OnlineDev_v5r31/InstallArea/x86_64-slc6-gcc49-do0/lib:/group/online/dataflow/cmtuser/OnlineDev_v5r31/InstallArea/x86_64-slc6-gcc49-do0/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/Boost/1.59.0_python2.7-682d6/x86_64-slc6-gcc49-dbg/lib:$LD_LIBRARY_PATH"
GaudiOnlineExe.exe \
    libGaudiOnline.so OnlineTask \
    -msgsvc=LHCb::FmcMessageSvc \
    -tasktype=LHCb::Class2Task \
    -main=/group/online/dataflow/templates/options/Main.opts \
    -opts=../options/NodeBsyMon.opts  -auto
