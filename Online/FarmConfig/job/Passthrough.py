"""
     Minimal Gaudi task in the online environment

     @author M.Frank
"""
from __future__ import print_function
__version__ = "1.0"
__author__  = "Markus Frank <Markus.Frank@cern.ch>"

import os
import sys
import GaudiOnline
import Configurables
import OnlineEnvBase as OnlineEnv
import Gaudi.Configuration as Gaudi
from   Configurables import Online__FlowManager as FlowManager
#
#
#
#
# -------------------------------------------------------------------------------
def setup_application(pre_algs=None, post_algs=None):
  #
  # Setup parameters:
  #
  utgid = os.getenv('UTGID',None);
  if not utgid: utgid = 'P%08d'%(os.getpid(), )
  #
  have_counter_test    = os.getenv("COUNTER_TEST",      False)
  have_histogram_test  = os.getenv("HISTOGRAM_TEST",    False)
  have_monitoring_sink = eval(os.getenv("MONITORING_SINK",'True'))
  event_output_buffer  = os.getenv("MBM_OUTPUT_BUFFER", False)
  event_input_buffer   = os.getenv("MBM_INPUT_BUFFER",  "Events")
  expand_tae           = os.getenv("EXPAND_TAE",        False)
  only_tae             = os.getenv("ONLY_TAE",          False)
  sweep_tae            = os.getenv("SWEEP_TAE",         False)
  #
  # -----------------------------------------------------------------------------
  application = GaudiOnline.Passthrough(outputLevel=OnlineEnv.OutputLevel,
                                        partitionName=OnlineEnv.PartitionName,
                                        partitionID=OnlineEnv.PartitionID,
                                        classType=GaudiOnline.Class1)
  application.setup_fifolog()
  application.config.debug         = False
  #
  # -----------------------------------------------------------------------------
  # Default TAE handling
  # Have TAE leaves separated in TES
  application.config.expandTAE     = False
  # Ignore TAE events? Set flag to True
  application.config.ignoreTAE     = False
  application.config.onlyTAE       = False
  #
  # -----------------------------------------------------------------------------
  if expand_tae:
    application.config.expandTAE   = True
  #
  # -----------------------------------------------------------------------------
  if only_tae:
    application.config.onlyTAE     = True
  #
  # -----------------------------------------------------------------------------
  print('INFO +++ MBM input buffer: %s'%(str(event_input_buffer), ))
  application.setup_mbm_access(event_input_buffer, True)
  application.setup_event_input()
  have_odin = True
  #application.setup_monitoring_service(task_name=utgid)
  application.setup_monitoring(task_name=utgid)
  application.updateAndReset.saveHistograms = False
  # -----------------------------------------------------------------------------
  #
  if sweep_tae:
    application.input.SweepTAE = int(sweep_tae)
  # -----------------------------------------------------------------------------
  #
  algorithms = []
  if pre_algs:
    if isinstance(pre_algs, (list, tuple)):
      for item in pre_algs:
        algorithms.append(item)
    elif pre_algs:
      algorithms.append(pre_algs)
  # -----------------------------------------------------------------------------
  # Event size algorithm
  ev_size = Configurables.Online__EventSize('EventSize')
  algorithms.append(ev_size)
  #
  # -----------------------------------------------------------------------------
  # Store explorer algorithm
  if os.getenv("MBM_EVENT_EXPLORER"):
    print_freq = 0.0001
    try:
      print_freq = float(os.getenv("MBM_EVENT_EXPLORER"))
    except:
      pass
    print("INFO +++ Instantiate store explorer with print frequency: %f"%(print_freq,))
    explorer = GaudiOnline.store_explorer(print_freq=print_freq)
    algorithms.append(explorer)
  #
  # -----------------------------------------------------------------------------
  if os.getenv("ODIN_DUMP", False):
    odin_dump = Configurables.Online__OdinBankDump('ODIN-dump')
    odin_dump.Print = 0.0001
    odin_dump.Histo = True
    odin_dump.Banks = 'Banks/RawData'
    if expand_tae:
      odin_dump.Banks = 'Banks/Central'
    algorithms.append(odin_dump)
  #
  # -----------------------------------------------------------------------------
  if have_counter_test:
    counter_test = Configurables.Online__CounterTypeTest()
    algorithms.append(counter_test)
  #
  # -----------------------------------------------------------------------------
  if have_histogram_test:
    hist_test = Configurables.Online__HistogramTypeTest()
    algorithms.append(hist_test)
    #hist_test = Configurables.Online__HistogramTypeTest('Another/Histogram/Test/For/me')
    #algorithms.append(hist_test)
  #
  # -----------------------------------------------------------------------------
  if have_monitoring_sink:
    sink = Configurables.OnlMonitorSink()
    sink.HistogramsToPublish = [(".*", ".*")]
    sink.CountersToPublish = [(".*", ".*")]
    application.app.ExtSvc.append(sink)
  #
  # -----------------------------------------------------------------------------
  # Output writer algorithm
  if event_output_buffer:
    writer = application.setup_mbm_output(event_output_buffer)
    writer.MBM_maxConsumerWait     = 10
    writer.MBM_allocationSize      = 1024*1024*10
    writer.MBM_burstPrintCount     = 25000
    writer.MaxEventsPerTransaction = 10
    writer.RequireODIN             = True
    writer.UseRawData              = False
    writer.UseTAELeaves            = False
    writer.UseRawGuard             = True
    tmo = int(os.getenv('MBM_WRITER_IDLE_TIMEOUT','-1'))
    if tmo > 0:
      application.config.eventIdleTimeout = tmo
      application.config.eventMinimalIdleTime = tmo+30
      print('INFO +++ MBM Writer idle timeout: %d'%(tmo, ))
    max_events = int(os.getenv('MBM_WRITER_MAX_EVENTS','-1'))
    if max_events > 0:
      writer.MaxEventsPerTransaction = max_events
      print('INFO +++ MBM Writer has %d events per transaction'%(max_events, ))
      
    algorithms.append(writer);
  #
  application.config.eventIdleTimeout = 200
  application.config.eventMinimalIdleTime = 250
  # -----------------------------------------------------------------------------
  application.setup_hive(FlowManager("EventLoop"), 40)
  #
  if post_algs:
    if isinstance(post_algs, (list, tuple)):
      for item in post_algs:
        algorithms.append(item)
    elif post_algs:
      algorithms.append(post_algs)
  #
  application.setup_algorithms(algorithms, 1.0)
  #
  # Only here the input algorithm is defined. Set options accordingly
  application.input.MakeRawEvent     = False  # Default
  if expand_tae and expand_tae=='FULL':
    application.input.MakeRawEvent   = True
  #
  # Enable CPU burning if requested
  if os.getenv("BURN_CPU"):
    application.passThrough.BurnCPU   = 1
    application.passThrough.MicroDelayTime = int(1000.0*float(os.getenv("BURN_CPU")))
  # DELETEME Compression storage test
  # application.passThrough.MicroDelayTime = 1500
  #
  #
  application.monSvc.DimUpdateInterval   = 5
  application.monSvc.CounterClasses = [
    "AuxCounters 10 (.*)EventInput/daqCounter.(.*)",
    "DAQErrors    3 (.*)EventInput/daqError.(.*)"
  ];
  #
  # -----------------------------------------------------------------------------
  application.config.burstPrintCount     = 30000
  #
  # Mode slection::  synch: 0 async_queued: 1 sync_queued: 2
  application.config.execMode            = 1
  application.config.numEventThreads     = 15
  application.config.MBM_numConnections  = 8
  application.config.MBM_numEventThreads = 5
  #
  application.config.numEventThreads     = 15
  application.config.MBM_numConnections  = 3
  application.config.MBM_numEventThreads = 4
  #
  application.config.numEventThreads     = 4
  application.config.MBM_numConnections  = 2
  application.config.MBM_numEventThreads = 2
  #
  if os.getenv("numEventThreads"):
    application.config.numEventThreads     = int(os.getenv("numEventThreads"))
  if os.getenv("MBM_numConnections"):
    application.config.MBM_numConnections  = int(os.getenv("MBM_numConnections"))
  if os.getenv("MBM_numEventThreads"):
    application.config.MBM_numEventThreads = int(os.getenv("MBM_numEventThreads"))

  application.enableUI()  
  #
  application.config.MBM_requests = [
    'EvType=2;TriggerMask=0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF;VetoMask=0,0,0,0;MaskType=ANY;UserType=USER;Frequency=PERC;Perc=100.0',
    'EvType=1;TriggerMask=0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF;VetoMask=0,0,0,0;MaskType=ANY;UserType=USER;Frequency=PERC;Perc=100.0'
  ]
  #
  #
  if os.getenv("MBM_INCLUSION"):
    mode = os.getenv("MBM_INCLUSION")
    mode = mode.upper()
    if mode == "ONE":
      application.config.MBM_requests = [
        'EvType=2;TriggerMask=0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF;VetoMask=0,0,0,0;MaskType=ANY;UserType=ONE;Frequency=PERC;Perc=100.0',
        'EvType=1;TriggerMask=0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF;VetoMask=0,0,0,0;MaskType=ANY;UserType=ONE;Frequency=PERC;Perc=100.0'
      ]
      if application.config.MBM_numConnections < 1:
        application.config.MBM_numConnections = 2
      application.config.MBM_numEventThreads  = application.config.MBM_numConnections + 1
    elif mode == "VIP":
      application.config.MBM_requests = [
        'EvType=2;TriggerMask=0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF;VetoMask=0,0,0,0;MaskType=ANY;UserType=VIP;Frequency=PERC;Perc=100.0',
        'EvType=1;TriggerMask=0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF;VetoMask=0,0,0,0;MaskType=ANY;UserType=VIP;Frequency=PERC;Perc=100.0'
      ]
      if application.config.MBM_numConnections < 1:
        application.config.MBM_numConnections = 2
      application.config.MBM_numEventThreads  = application.config.MBM_numConnections + 1
    elif mode == "USER":
      application.config.MBM_requests = [
        'EvType=2;TriggerMask=0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF;VetoMask=0,0,0,0;MaskType=ANY;UserType=USER;Frequency=PERC;Perc=100.0',
        'EvType=1;TriggerMask=0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF;VetoMask=0,0,0,0;MaskType=ANY;UserType=USER;Frequency=PERC;Perc=100.0'
      ]
      application.config.MBM_numConnections  = 1
  #
  # Enable this for debugging
  #
  _dbg = os.getenv("DEBUG")
  if _dbg:
    print('Setup for debugging the application....')
    application.config.execMode            = 0
    application.config.numEventThreads     = 1
    application.config.MBM_numConnections  = 1
    application.config.MBM_numEventThreads = 1
  #
  #
  print('Setup complete.... Have ODIN: '+str(have_odin))
  return application

# -------------------------------------------------------------------------------
setup_application()
