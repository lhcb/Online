#!/bin/bash
# =========================================================================
#
#  Default script to start the buffer manager on the HLT farm worker node
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    05/03/2021
#
# =========================================================================
#
if test "${PARTITION}" = "TDET"; then
	. /group/online/dataflow/EventBuilder/EventBuilderDev/install.ebonly.x86_64-el9-gcc13-dbg/bin/thisonline.sh;
elif test "${PARTITION}" = "FEST"; then
	. /group/online/dataflow/EventBuilder/EventBuilderRelease/install.ebonly.x86_64-el9-gcc13-opt/bin/thisonline.sh;
elif test "${PARTITION}" = "UT"; then
# fpisani test for UT commissioning
	. /group/online/dataflow/EventBuilder/gitlab-builds/EB_MFP_prescale-9937e46a/install.ebonly.x86_64-el9-gcc13-opt/bin/thisonline.sh;
# . /group/online/dataflow/EventBuilder/gitlab-builds/EB_MFP_prescale-36589046/install.ebonly.x86_64-el9-gcc13-opt/bin/thisonline.sh;
else
# . /group/online/dataflow/EventBuilder/EventBuilderRelease/install.ebonly.x86_64-centos9-gcc12-opt/bin/thisonline.sh;
	. /group/online/dataflow/EventBuilder/EventBuilderRelease/install.ebonly.x86_64-el9-gcc13-opt/bin/thisonline.sh;
fi;

cd ${EVENTBUILDINGROOT}/options;
`dataflow_task Class1` -opts=EB_RU.opts ${AUTO_STARTUP} ${DEBUG_STARTUP}
