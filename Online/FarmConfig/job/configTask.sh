#!/bin/bash
# =========================================================================
#
#  Default script to start any task on the HLT farm.
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    20/05/2013
#
# =========================================================================
#
#
config_task_type()
{
    part=${1};
    type=${2};
    if test "ANY" = "${part}" -o "${PARTITION}" = "${part}"; then
	if test "${TASK_TYPE}" = "${type}"; then
	    echo "config_task_type                       WARNING Configure special startup for partition: ${part} task: ${type}";
	    export PYTHONPATH=;
	    RELEASE=/group/online/dataflow/cmtuser/ONLINE/ONLINE_v7r27.testing;
	    . ${RELEASE}/setup.x86_64_v2-el9-gcc13-do0.vars;
	    cd ${FARMCONFIGROOT}/job;
	fi;
    fi;
}
#
config_mbm_type()
{
    part=${1};
    type=${2};
    if test "ANY" = "${part}" -o "${PARTITION}" = "${part}"; then
	if test "${TASK_TYPE}" = "${type}"; then
	    echo "config_task_type                       WARNING Configure special startup for partition: ${part} task: ${type}";
	    export PYTHONPATH=;
	    #if test "${type}" = "EBMBM_IN0"; then
	    #    export DEBUG_STARTUP=-debug;
	    #fi;
	    RELEASE=/group/online/dataflow/cmtuser/ONLINE/ONLINE_v7r21;
	    . ${RELEASE}/setup.x86_64_v2-el9-gcc13-opt.vars;
	    cd ${FARMCONFIGROOT}/job;
	fi;
    fi;
}
#
TEST_PARTITION=LHCb;
##
config_task_type        ANY  SNDMulti
config_task_type        ANY  MBMDistBox
##
##config_task_type     ${TEST_PARTITION}  EBAdder
##config_task_type     ${TEST_PARTITION}  EBPartAdder
##config_task_type     ${TEST_PARTITION}  EBPartSaver
##config_task_type     ${TEST_PARTITION}  EBPartPublisher
##
#  config_task_type     ${TEST_PARTITION}  MonAdder
##config_task_type     LHCb2              HLT2SFAdder
##config_task_type     LHCb2              HLT2TopAdder
##
##config_task_type       ${TEST_PARTITION}  MonPublish
##config_mbm_type        ${TEST_PARTITION} EBMBM_IN0
##config_mbm_type        ${TEST_PARTITION} EBMBM_IN1
##config_task_type     ${TEST_PARTITION}  MonAdderMulti
##config_task_type     ${TEST_PARTITION}  MonSaverMulti
##
#
