#!/bin/bash
# =========================================================================
#
#  Generic farm task startup script
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    20/05/2013
#
# =========================================================================
#
export UTGID=${UTGID}
if test -n "${LOGFIFO}"; then
    export LOGFIFO=${LOGFIFO};
fi;
if test -z "${LOGFIFO}" -a -e /dev/shm/logs.dev; then
    export LOGFIFO=/dev/shm/logs.dev;
fi;
if test -z "${DIM_DNS_NODE}"; then
    if [ -r /etc/sysconfig/dim ]; then
	. /etc/sysconfig/dim;
	if test "`echo $DIM_DNS_NODE | cut -b 1-5`" = "fmc01"; then
	    DIM_DNS_NODE=`hostname -s`;
	fi;
	export DIM_DNS_NODE;
    fi;
fi;
export DIM_DNS_NODE;
export LC_ALL=C;
#
export CLASS=Class1;
#
#  Evaluate the information in the runinfo file
#  --------------------------------------------
eval `python3 <<EOF
import os, sys, importlib.util

dbg = False

def _p(x):
  if dbg: print('echo '+os.environ['UTGID']+' '+x+';')
  print (x+';')

args = "$*".split()
runinfo = None
while len(args):
  itms  = args[0].split('=')
  type  = itms[0].lower()
  value = ''
  if len(itms)>1: value=itms[1]
  del args[0]

  if type == '-type':
    _p('export TASK_TYPE='+value)
  elif type == '-partition':
    # This is overwritten by OnlineEnv.PartitionName and hance not used
    #_p('export PARTITION='+value)
    #_p('export PARTITION_NAME='+value)
    pass
  elif type == '-runinfo':
    _p('export RUNINFO='+value)
    runinfo = value
  elif type == '-taskconfig':
    _p('export ARCH_FILE='+value)
  elif type == '-taskinfo':
    _p('export ARCH_FILE='+value)
  elif type == '-architecture':
    _p('export ARCHITECTURE='+value)
  elif type == '-application':
    _p('export APPLICATION='+value)
  elif type == '-count':
    _p('export NBOFSLAVES='+value)
  elif type == '-instances':
    _p('export NBOFSLAVES='+value)
  elif type == '-numthreads':
    _p('export NBOFTHREADS='+value)
  elif type == '-utgid':
    _p('export UTGID='+value)
  elif type == '-logfifo':
    _p('export LOGFIFO='+value)
  elif type == '-script':
    _p('export TASK_SCRIPT='+value)
  elif type == '-auto':
    _p('export AUTO_STARTUP=-auto')
  elif type == '-dimdns':
    _p('export DIM_DNS_NODE='+value)
  elif type == '-tmsdns':
    _p('export TMS_DNS='+value)
  elif type == '-smidns':
    _p('export SMI_DNS='+value)
  elif type == '-smidomain':
    _p('export SMI_DOMAIN='+value)
  elif type == '-options':
    _p('export OPTIONS='+value)
  elif type == '-class':
    _p('export CLASS='+value)
  elif type == '-environ':
    _p('export '+itms[1]+'='+itms[2])
  elif type == '-debug':
    _p('export DEBUG_STARTUP=-debug')
  elif type[:8] == '-replace':
    _p('export CONTROLLER_REPLACEMENTS="-replacements='+value+'"')
  else:
    _p('echo Unknown task argument '+type)


if runinfo is None:
  _p('echo '+os.environ['UTGID']+': [ERROR]  Failed to find RUNINFO file. Cannot start task;')
  _p('exit 11')
  sys.exit(11);
try:
  spec = importlib.util.spec_from_file_location("OnlineEnvBase", runinfo)
  Online = importlib.util.module_from_spec(spec)
  spec.loader.exec_module(Online)
  if hasattr(Online,'OnlineVersion'):
    OnlineVersion = Online.OnlineVersion
    idx = OnlineVersion.find('Online_')
    if OnlineVersion == 'Online':
      OnlineVersion = 'OnlineRelease'
    elif idx >= 0:
      OnlineVersion = OnlineVersion.replace('Online_','OnlineDev_')
  _p('export ONLINE_ENV_DIR="'+os.path.dirname(runinfo)+'"')
  if hasattr(Online,'HltArchitecture'):
    _p('export HLT_ARCHITECTURE="'+str(Online.HltArchitecture)+'"')
  if hasattr(Online,'MooreOnlineVersion'):
    _p('export MOOREONLINE_VERSION='+str(Online.MooreOnlineVersion))
  if hasattr(Online,'MooreVersion'):
    _p('export MOORE_VERSION='+str(Online.MooreVersion))
  if hasattr(Online,'OnlineVersion'):
    _p('export ONLINE_VERSION='+str(OnlineVersion))
  _p('export PARTITION='+str(Online.PartitionName))
  _p('export PARTITION_ID='+str(Online.PartitionID))
  _p('export PARTITION_NAME='+str(Online.PartitionName))
  _p('export OUTPUT_LEVEL='+str(Online.OutputLevel))
  if hasattr(Online,'TAE') and Online.TAE != 0:
    _p('export TAE_PROCESSING="TAE"')
  if hasattr(Online,'HLTType'):
    _p('export HLT_TYPE="'+str(Online.HLTType)+'"')
  if hasattr(Online,'Activity'):
    _p('export RUN_TYPE="'+str(Online.Activity)+'"')
    _p('export ACTIVITY="'+str(Online.Activity)+'"')

except Exception as X:
  _p('echo '+str(X))
  _p('exit 11')

EOF`
#
#
setup_options_path()   {
    if test -n "${1}"; then
	export GEN_OPTIONS=/group/online/dataflow/options/${PARTITION}/${1};
    else
	export GEN_OPTIONS=/group/online/dataflow/options/${PARTITION};
    fi;
    export INFO_OPTIONS=${GEN_OPTIONS}/OnlineEnv.opts;
    export PYTHONPATH=${GEN_OPTIONS}:${PYTHONPATH};
    #echo "ERROR:  PYTHONPATH=${PYTHONPATH}";
}
