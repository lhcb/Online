#!/bin/bash
#
export NODENAME=`python3 -c "print ('$HOST'.split('.')[0])"`
#
dataflow_monitoring_options()
{    echo "-opts=/group/online/dataflow/options/${PARTITION_NAME}/MONITORING/${UTGID}.opts";    }
#
dataflow_task()
{
    arg_1=${1};
    shift;
    echo  "exec -a ${UTGID} genRunner.exe libDataflow.so dataflow_run_task -mon=Dataflow_DIMMonitoring -class=${arg_1} $*";
}
#
gaudi_task()
{
    arg_1=${1};
    shift;
    echo "exec -a ${UTGID} genPython.exe `which gaudirun.py` ${arg_1} --application=Online::OnlineApplication $*";
}
#
gaudi_event_task()
{
    arg_1=${1};
    shift;
    echo "exec -a ${UTGID} genPython.exe `which gaudirun.py` ${arg_1} --application=Online::OnlineEventApp $*";
}
