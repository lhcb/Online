#!/bin/bash
# =========================================================================
#
#  Default script to start any task on the HLT farm.
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    20/05/2013
#
# =========================================================================
#
if test -z "${HOST}"; then
    export HOST="${DIM_HOST_NODE}";
fi;
if test -n "${LOGFIFO}"; then
    export LOGFIFO=${LOGFIFO};
elif test -z "${LOGFIFO}" -a -e /dev/shm/logs.dev; then
    export LOGFIFO=/dev/shm/logs.dev;
fi;
#
if test -z "${ONLINERELEASE}"; then
    if test "`uname -a | grep el9`" != "";
    then
	export CMTCONFIG=x86_64_v2-el9-gcc13-do0;
	#. /group/online/dataflow/cmtuser/OnlineRelease/install.dataflow.LCG_102.x86_64-centos9-gcc11-dbg/bin/thisonline.sh;
	. /group/online/dataflow/cmtuser/OnlineRelease/setup.${CMTCONFIG}.vars;
	export LC_ALL=C;
    else
	export CMTCONFIG=x86_64_v2-centos7-gcc11-do0;
	. /group/online/dataflow/cmtuser/OnlineRelease/setup.${CMTCONFIG}.vars;
    fi;
fi;
#
cd $FARMCONFIGROOT/job;
#
. ./configTask.sh ${PARTITION} ${TASK_TYPE} ${HOST}
. ./preamble.sh;
#
HOST=`hostname -s | tr a-z A-Z`;
export SUBFARM=`echo ${DIM_DNS_NODE} | tr a-z A-Z`;
export STATIC_OPTS=${FARMCONFIGROOT}/options;
if test -z "${DYNAMIC_OPTS}"; then
    if test "${SUBFARM}" = "MON01"; then
	export DYNAMIC_OPTS=/group/online/dataflow/options/${PARTITION_NAME};
    else
	export DYNAMIC_OPTS=/group/online/dataflow/options/${PARTITION_NAME};
    fi;
fi;
if test -z "${DATAINTERFACE}"; then
    DATAINTERFACE=`python /group/online/dataflow/scripts/getDataInterface.py`;
fi;
export DATAINTERFACE;
#
export PREAMBLE_OPTS=${FARMCONFIGROOT}/options/Empty.opts;
export ONLINETASKS=/group/online/dataflow/templates;
export INFO_OPTIONS=${DYNAMIC_OPTS}/OnlineEnv.opts;
export MBM_SETUP_OPTIONS=${DYNAMIC_OPTS}/MBM_setup.opts;
export PYTHONPATH=${DYNAMIC_OPTS}:${PYTHONPATH};
export MONITORING_DISTBOX=MON0101;
#
PRINT_COMMAND_LINE=; ####YES;
#
monitoring_options()
{
    echo "/group/online/dataflow/options/${PARTITION_NAME}/MONITORING/${UTGID}.opts";
}
#
dataflow_default_options()
{
    echo "-opts=`monitoring_options`";
}
#
enable_testing()
{
    export PATH=/group/online/dataflow/cmtuser/ONLINE_TEST/InstallArea/${CMTCONFIG}/bin:${PATH};
    export PYTHON_PATH=/group/online/dataflow/cmtuser/ONLINE_TEST/InstallArea/${CMTCONFIG}/bin:${PYTHON_PATH};
    export LD_LIBRARY_PATH=/group/online/dataflow/cmtuser/ONLINE_TEST/InstallArea/${CMTCONFIG}/lib:${LD_LIBRARY_PATH};
}
#
execute()
{
    if test -n "${PRINT_COMMAND_LINE}"; then
	echo "${UTGID} [INFO] INFO_OPTIONS:       ${INFO_OPTIONS}";
	echo "${UTGID} [INFO] MBM_SETUP_OPTIONS:  ${MBM_SETUP_OPTIONS}";
	echo "${UTGID} [INFO] $*";
    fi;
    $*;
}
#
dataflow_task()
{
    echo  "exec -a ${UTGID} genRunner.exe libDataflow.so dataflow_run_task -mon=Dataflow_DIMMonitoring -class=$1";
}
#
if test "${TASK_TYPE}" = "Controller"; then
    . ${FARMCONFIGROOT}/job/Controller.sh;
    #
elif test -f ./${TASK_TYPE}.sh; then
    execute . ./${TASK_TYPE}.sh $*;
    #
elif test -f ./tests/${TASK_TYPE}.sh; then
    execute . ./tests/${TASK_TYPE}.sh $*;
    #
elif test -f "${OPTIONS}"; then
    execute `dataflow_task ${CLASS}` -opts=${OPTIONS} ${AUTO_STARTUP} ${DEBUG_STARTUP};
    #
elif test -f ./${PARTITION_NAME}${TASK_TYPE}.sh; then
    execute . ./${PARTITION_NAME}${TASK_TYPE}.sh;
    #
elif test -f ${SMICONTROLLERROOT}/scripts/${TASK_TYPE}.sh; then
    cd ${SMICONTROLLERROOT}/scripts;
    execute . ${SMICONTROLLERROOT}/scripts/${TASK_TYPE}.sh;
    #
elif test  "${TASK_TYPE}" = "MonEvents"; then
    . /group/online/dataflow/cmtuser/OnlineRelease/setup.x86_64_v2-el9-gcc13-opt.vars;
    export PYTHONPATH=${DYNAMIC_OPTS}:${PYTHONPATH};
    execute `gaudi_event_task ${FARMCONFIGROOT}/job/${TASK_TYPE}.py`;
    #
elif test -f "${STATIC_OPTS}/${TASK_TYPE}.opts"; then
    execute `dataflow_task ${CLASS}` -opts=${STATIC_OPTS}/${TASK_TYPE}.opts ${AUTO_STARTUP} ${DEBUG_STARTUP};
    #
elif test -f "${STATIC_OPTS}/${TASK_TYPE}.py"; then
    execute `gaudi_task ${STATIC_OPTS}/${TASK_TYPE}.py`;
    #
elif test -f "${FARMCONFIGROOT}/job/${TASK_TYPE}.py"; then
    execute `gaudi_event_task ${FARMCONFIGROOT}/job/${TASK_TYPE}.py`;
    #
elif test -n "`echo ${UTGID} | grep TestEvent`"; then
    execute `gaudi_event_task ${FARMCONFIGROOT}/job/PassThrough.py`;
    #
elif test -f ./${PARTITION_NAME}DefaultTask.sh; then
    execute . ./${PARTITION_NAME}DefaultTask.sh;
    #
else
    echo "ERROR  Running default Dim enabled FSM task ";
    execute exec -a ${UTGID} genPython.exe ${DATAFLOWROOT}/python/DimTask.py -utgid ${UTGID} -partition 103 -print ${OUTPUT_LEVEL};
fi;
