#!/bin/bash
cd /group/online/dataflow/cmtuser/ONLINE/ONLINE_v7r20/build.dataflow.x86_64-centos9-gcc12-dbg;
. bin/thisonline.sh;
#
task_type=HLT2;
#
#gdb --args \
    #
gentest.exe libHistAdder.so run_hist_app \
	    -input=/hist/Savesets/ByRun/${task_type}/ToMerge \
	    -output=/group/online/dataflow/cmtuser/Savesets \
	    -temporary=/tmp/${task_type}\
	    -rundb-days-to-scan=40\
	    -rundb-scan-interval=300\
	    -procdb-file=/group/online/dataflow/cmtuser/hist_adder_${task_type}.sqlite\
	    -procdb-scan-interval=150\
	    -disk-scan-interval=300\
	    -partition=LHCb\
	    -task=${task_type}\
	    -chunk_size=50\
	    -parallel_adders=5\
	    -print=INFO
