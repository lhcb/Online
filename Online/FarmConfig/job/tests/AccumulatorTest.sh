#!/bin/bash
# =========================================================================
#
#  Default script to start the buffer manager on the HLT farm worker node
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    05/03/2021
#
# =========================================================================
#
. /group/online/dataflow/cmtuser/ONLINE/ONLINE_v7r17/setup.x86_64_v2-centos7-gcc11-do0.vars;
export TAN_PORT=YES;
export TAN_NODE=${DIM_HOST_NODE};
export PYTHONPATH=${DYNAMIC_OPTS}:${PYTHONPATH};
execute `gaudi_event_task /group/online/dataflow/cmtuser/ONLINE/ONLINE_v7r17/Online/GaudiOnlineTests/scripts/gaudionline_histtest.py`;
