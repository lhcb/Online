//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef IPUBLISHSVCARR_H_
#define IPUBLISHSVCARR_H_

/// Framework include files
#include <GaudiKernel/IPublishSvc.h>

class IPublishSvcArr: virtual public IInterface, virtual public IPublishSvc    {
  public:
    /// InterfaceID
    DeclareInterfaceID(IPublishSvcArr, 1, 0);

    /// Value publishers
    virtual void declarePubItem(const std::string& name, const bool&  var)  override = 0;
    virtual void declarePubItem(const std::string& name, const int&  var)  override = 0;
    virtual void declarePubItem(const std::string& name, const long&  var)  override = 0;
    virtual void declarePubItem(const std::string& name, const double& var)  override = 0;
    virtual void declarePubItem(const std::string& name, const std::string& var)  override = 0;

    /// Array publishers
    virtual void declarePubItem(const std::string& name, const int *value, int size) = 0;
    virtual void declarePubItem(const std::string& name, int *value, int size) = 0;
    virtual void declarePubItem(const std::string& name, const double *value, int size) = 0;
    virtual void declarePubItem(const std::string& name, double *value, int size) = 0;
    virtual void declarePubItem(const std::string& name, const long *value, int size) = 0;
    virtual void declarePubItem(const std::string& name, long *value, int size) = 0;
    virtual void declarePubItem(const std::string& name, const float *value, int size) = 0;
    virtual void declarePubItem(const std::string& name, float *value, int size) = 0;
};

#endif /* IPUBLISHSVCARR_H_ */
