/*
 * GaudiCounters.cpp
 *
 *  Created on: Nov 11, 2020
 *      Author: beat
 */
#ifndef ONLINE_GAUCHO_SRC_COMPONENTS_GAUDICOUNTERS_H_
#define ONLINE_GAUCHO_SRC_COMPONENTS_GAUDICOUNTERS_H_

#include <Gaudi/Accumulators.h>

#include <dim/dis.hxx>
#include <Gaucho/dimhist.h>
#include <Gaucho/Utilities.h>
#include <Gaucho/MonCounter.h>
#include <Gaucho/CounterSubSys.h>
#include "MonCounterAccumulators.tcc.h"

#include <GAUDI_VERSION.h>
#if GAUDI_MAJOR_VERSION < 37
namespace Gaudi::Accumulators {
  template<typename T, Gaudi::Accumulators::atomicity ATOM>
  void to_json( nlohmann::json& j, const AveragingCounter<T, ATOM>& c ) {
    j = c.toJSON();
  }
  template<typename T, Gaudi::Accumulators::atomicity ATOM>
  void to_json( nlohmann::json& j, const BinomialCounter<T, ATOM>& c ) {
    j = c.toJSON();
  }
}
#endif

/// Online namespace declaration
namespace Online  {

  /// specialization of MonRate class for AveragingCounters
  template<typename T, Gaudi::Accumulators::atomicity ATOM>
    struct MonRate<Gaudi::Accumulators::AveragingCounter<T, ATOM> > : MonRateBBase    {
  public:
    typedef T DataType;
    typedef Gaudi::Accumulators::AveragingCounter<T, ATOM> CounterType;
    const CounterType& m_data;
    double             m_last;
  public:
  MonRate(const std::string &nam, const std::string &tit, const CounterType& data, MonBase*)
    : MonRateBBase(nam, tit), m_data(data)
    {
      nlohmann::json j = m_data;
      this->m_last = double(j.at("sum").get<DataType>());
      this->rate = 0e0;
    }
    virtual void makeRate(unsigned long dt) override        {
      nlohmann::json j = m_data;
      double value = double(j.at("sum").get<DataType>());
      this->rate = value - this->m_last;
      this->rate = 1.0e9 * rate / double(dt);
      if (this->rate < 0.0)
	this->rate = 0.0;
      this->m_last = value;
    }
    virtual void zeroLast() override      {
      this->m_last = 0;
      this->rate = 0e0;
    }
  };

  /// specialization of MonRate class for AveragingCounters
  template<typename T, Gaudi::Accumulators::atomicity A>
    class MonRate<Gaudi::Accumulators::BinomialCounter<T, A> > : public MonRateBBase    {
  public:
    typedef T DataType;
    typedef Gaudi::Accumulators::BinomialCounter<T, A> CounterType;
    const CounterType& m_data;
    double m_last;

  public:
  MonRate(const std::string &nam, const std::string &tit, const CounterType& data, MonBase*)
    : MonRateBBase(nam, tit), m_data(data)
    {
      nlohmann::json j = m_data;
      this->m_last = double(j.at("nEntries").get<DataType>());
      this->rate = 0e0;
    }
    virtual void makeRate(unsigned long dt) override      {
      nlohmann::json j = m_data;
      double value = double(j.at("nEntries").get<DataType>());
      this->rate = value - this->m_last;
      this->rate = 1.0e9 * rate / double(dt);
      if (this->rate < 0.0)
	this->rate = 0.0;
      this->m_last = value;
    }
    virtual void zeroLast() override      {
      this->m_last = 0;
      this->rate = 0e0;
    }
  };
  
  template <> class MonRate<Entity> : public MonRateBBase    {
  public:
    std::unique_ptr<Entity> m_ent;
    double        m_last;

  public:
  MonRate(const std::string &nam, const std::string &tit, const Entity& data, MonBase *bse)
    : MonRateBBase(nam, tit, bse)
      {
	this->m_ent  = std::make_unique<Entity>(data);
	this->m_last = this->rate = 0e0;
      }
    virtual void makeRate(unsigned long dt) override   {
      double value = 0e0;
      nlohmann::json j = *m_ent;
      switch ( this->origCounter->type ) {
      case C_GAUDIMSGACClu:
	value = double(j.at( "nEntries" ).get<unsigned long>());
	break;

	/// ==========================================================================================
      case C_GAUDIACCCHAR:
      case C_GAUDIACCuCHAR:
      case C_GAUDIACCSHORT:
      case C_GAUDIACCuSHORT:
      case C_GAUDIACCINT:
      case C_GAUDIACCuINT:
      case C_GAUDIACCLONG:
      case C_GAUDIACCuLONG:
	value = double(j.at( "nEntries" ).get<long>());
	break;
      case C_GAUDIACCFLOAT:
      case C_GAUDIACCDOUBLE:
	value = double(j.at( "nEntries" ).get<double>());
	break;
	/// ==========================================================================================
      case C_GAUDIAVGACCc:
      case C_GAUDIAVGACCcu:
      case C_GAUDIAVGACCs:
      case C_GAUDIAVGACCsu:
      case C_GAUDIAVGACCi:
      case C_GAUDIAVGACCiu:
      case C_GAUDIAVGACCl:
      case C_GAUDIAVGACClu:
	value = double(j.at( "sum" ).get<long>());
	break;
      case C_GAUDIAVGACCf:
      case C_GAUDIAVGACCd:
	value = double(j.at( "sum" ).get<double>());
	break;
	/// ==========================================================================================
      case C_GAUDISTATACCc:
      case C_GAUDISTATACCcu:
      case C_GAUDISTATACCs:
      case C_GAUDISTATACCsu:
      case C_GAUDISTATACCi:
      case C_GAUDISTATACCiu:
      case C_GAUDISTATACCl:
      case C_GAUDISTATACClu:
	value = double(j.at( "sum" ).get<long>());
        break;
      case C_GAUDISTATACCf:
      case C_GAUDISTATACCd:
	value = double(j.at( "sum" ).get<double>());
        break;
	/// ==========================================================================================
      case C_GAUDISIGMAACCc:
      case C_GAUDISIGMAACCcu:
      case C_GAUDISIGMAACCs:
      case C_GAUDISIGMAACCsu:
      case C_GAUDISIGMAACCi:
      case C_GAUDISIGMAACCiu:
      case C_GAUDISIGMAACCl:
      case C_GAUDISIGMAACClu:
	value = double(j.at( "sum" ).get<long>());
	break;
      case C_GAUDISIGMAACCf:
      case C_GAUDISIGMAACCd:
	value = double(j.at( "sum" ).get<double>());
	break;
	/// ==========================================================================================
      case C_GAUDIBINACCc:
      case C_GAUDIBINACCcu:
      case C_GAUDIBINACCs:
      case C_GAUDIBINACCsu:
      case C_GAUDIBINACCi:
      case C_GAUDIBINACCiu:
      case C_GAUDIBINACCl:
      case C_GAUDIBINACClu:
      case C_GAUDIBINACCf:
      case C_GAUDIBINACCd:
	value = double(j.at( "nTrueEntries" ).get<long>() + j.at( "nFalseEntries" ).get<long>());
        break;
	/// ==========================================================================================
      default:
	break;
      }
      this->rate = value - this->m_last;
      this->rate = 1.0e9 * rate / double(dt);
      if (this->rate < 0.0)
	this->rate = 0.0;
      this->m_last = value;
    }
    virtual void zeroLast() override   {
      this->m_last = 0e0;
      this->rate = 0e0;
    }
  };
}
#endif
