//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
/*
 * GaudiHistograms.h
 *
 *  Created on: Nov 19, 2020
 *      Author: beat
 */
#ifndef ONLINE_GAUCHO_SRC_COMPONENTS_GAUDIHISTOGRAMS_H_
#define ONLINE_GAUCHO_SRC_COMPONENTS_GAUDIHISTOGRAMS_H_

#include <Gaucho/MonHist.h>
#include <RTL/strdef.h>
#include <atomic>
#include <string>
#include <tuple>

/// specialization of MonCounter class for AveragingCounters
using namespace std;
using namespace Gaudi::Accumulators;
using namespace Gaudi::Monitoring;
using Entity = Hub::Entity;

/// Online namespace declaration
namespace Online {

  template<>
  inline MonHist<Entity>::MonHist( const std::string& source, const std::string& desc, const Entity& hist )
      : MonBase( H_ILLEGAL, source, desc ) {
    this->object = new Entity(hist);
    // Setup is done on demand (i.e. when xmitbuffersize() is called for the first time).
    // This is needed to allow calling declareInfo in registerEntity at which point we can't yet convert to JSON and determine the full histogram type/shape.
    // In turn, this allows to create histograms after initialize (e.g. on the first event).
  }

  template <>
  inline MonHist<Entity>::~MonHist() {
    this->axes.clear();
    delete (Entity*)(this->object);
    this->object = nullptr;
  }

  MONTYPE analyzeTypeJson( MonHist<Entity>* hist, const nlohmann::json& j );
  int setupJson(MonHist<Entity>* hist, nlohmann::json const & j);

  template<>
  int MonHist<Entity>::xmitbuffersize() {
    if (this->m_xmitbuffersize > 0) return this->m_xmitbuffersize;
    nlohmann::json j = *(this->object);
    this->type = analyzeTypeJson(this, j);
    this->m_xmitbuffersize = setupJson(this, j);
    return this->m_xmitbuffersize;
  }

  MONTYPE analyzeTypeJson( MonHist<Entity>* hist, const nlohmann::json& j ) {
    string         typstr      = j.at( "type" ).get<string>();
    auto           splt        = RTL::str_split( typstr, ":" );
    hist->descriptor.clazz     = HT_Unknown;
    hist->descriptor.bintyp    = BT_Unknown;
    hist->descriptor.dimension = j.at( "dimension" ).get<unsigned int>();
    if ( splt.at( 0 ) != "histogram" ) { return hist->descriptor.type(); }
    hist->descriptor.clazz   = HT_Gaudi;
    if (splt.at(2) == "d")
      hist->descriptor.bintyp = BT_Double;
    else if(splt.at(2) == "f")
      hist->descriptor.bintyp = BT_Float;
    hist->descriptor.profile = splt.at( 1 ).find( "Profile" ) != string::npos;
    hist->descriptor.bintyp  = hist->descriptor.bin_type( splt.at( 2 ) );
    return hist->descriptor.type();
  }

  template <>
  MONTYPE MonHist<Entity>::analyzeType( const Entity& var ) = delete;

  template <>
  void MonHist<Entity>::reset() {
    auto* r = const_cast<Entity*>( this->object );
    resetEntity( *r );
  }

  int setupJson(MonHist<Entity>* hist, nlohmann::json const & j) {
    switch ( hist->descriptor.clazz ) {
    case HT_Gaudi: {
      auto           jsonAxis = j.at( "axis" );
      size_t         totbin   = 1;
      hist->title             = j.at( "title" ).get<string>();
      hist->axes.resize( hist->descriptor.dimension );
      hist->headerLen =
          DimHistbuff1::buffer_name_offset( hist->descriptor.dimension ) + hist->name_length() + 1 + hist->title_length() + 1;
      for ( int i = 0; i < hist->descriptor.dimension; i++ ) {
        hist->axes[i].nbin = jsonAxis[i].at( "nBins" ).get<int>();
        hist->axes[i].min  = jsonAxis[i].at( "minValue" ).get<double>();
        hist->axes[i].max  = jsonAxis[i].at( "maxValue" ).get<double>();
        const auto labels = hist->axes[i].labels = jsonAxis[i].value( "labels", std::vector<std::string>{} );
        if ( hist->axes[i].labels.empty() ) {
          hist->axes[i].lablen = 0;
        } else {
          hist->axes[i].lablen = std::accumulate( labels.begin(), labels.end(), labels.size() + 1,
                                                  []( int sum, const std::string& x ) { return sum + x.size(); } );
        }
        hist->headerLen += hist->axes[i].lablen;
        totbin *= ( hist->axes[i].nbin + 2 );
      }
      hist->headerLen        = ( hist->headerLen + 7 ) & ~7;
      return hist->headerLen + ( ( hist->descriptor.profile ) ? 4 : 2 ) * totbin * sizeof( double );
    }
    default:
      break;
    }
    return 0;
  }

  template <>
  int MonHist<Entity>::setup() = delete;

  namespace {
    template <typename T>
    struct Helper {
      static void cpy_bins( void* ptr, nlohmann::json& j ) {
        DimHistbuff1* pp = (DimHistbuff1*)ptr;
        if ( pp->dim == 1 )
          pp->type = H_1DIM;
        else if ( pp->dim == 2 )
          pp->type = H_2DIM;
        else if ( pp->dim == 3 )
          pp->type = H_3DIM;
        auto    bincont   = j.at( "bins" ).get<std::vector<T>>();
        auto    blocksize = bincont.size() * sizeof( double );
        double* content   = add_ptr<double>( ptr, pp->dataoff );
        double* weights   = add_ptr<double>( ptr, pp->dataoff + blocksize );
        for ( size_t i = 0; i < bincont.size(); ++i, ++content, ++weights ) {
          *content = bincont[i];
          *weights = bincont[i]; // TODO fill weights when actually available
        }
      }
    };
  } // namespace

  template <>
  int MonHist<Entity>::serialize( void* ptr ) {

    DimHistbuff1* pp = (DimHistbuff1*)ptr;
    setup_buffer( pp, *this );
    setup_labels( pp, this->axes );

    nlohmann::json j        = *this->object;
    string         typstr   = j.at( "type" ).get<string>();
    auto           splt     = RTL::str_split( typstr, ":" );
    auto           NEntries = j.at( "nEntries" ).get<unsigned long>();
    auto           btyp     = ::tolower( splt.at( 2 )[0] );
    pp->nentries            = double( NEntries );
    pp->dataoff             = this->hdrlen(); // TODO removed?
    if ( this->descriptor.profile ) {
      GaudiProfileBin* dest = static_cast<GaudiProfileBin*>( add_ptr( ptr, pp->dataoff ) );
      try {
        auto bincont = j.at( "bins" ).get<std::tuple<std::vector<unsigned int>, std::vector<double>, std::vector<double>>>();
        auto const& [nent, sumw, sumw2] = bincont;
        for ( size_t i = 0; i < nent.size(); i++ ) {
                dest[i].nent = nent[i];
                dest[i].sumw = sumw[i];
                dest[i].sumw2 = sumw2[i];
        }
      } catch (const nlohmann::json::exception&) {
        auto bincont = j.at( "bins" ).get<std::vector<std::tuple<std::tuple<unsigned int, double>, double>>>();
        for ( size_t i = 0; i < bincont.size(); i++ ) {
                std::tuple<unsigned int, double> tmp;
                std::tie( tmp, dest[i].sumw2 )         = bincont[i];
                std::tie( dest[i].nent, dest[i].sumw ) = tmp;
        }
      }
    } else if ( btyp == 'd' )
      Helper<double>::cpy_bins( ptr, j );
    else if ( btyp == 'f' )
      Helper<float>::cpy_bins( ptr, j );
    else if ( btyp == 'l' )
      Helper<long>::cpy_bins( ptr, j );
    else if ( btyp == 'm' )
      Helper<long unsigned>::cpy_bins( ptr, j );
    else if ( btyp == 'i' )
      Helper<int>::cpy_bins( ptr, j );
    else if ( btyp == 'j' )
      Helper<unsigned int>::cpy_bins( ptr, j );
    else if ( btyp == 's' )
      Helper<short>::cpy_bins( ptr, j );
    else if ( btyp == 't' )
      Helper<unsigned short>::cpy_bins( ptr, j );
    else if ( btyp == 'c' )
      Helper<char>::cpy_bins( ptr, j );
    else if ( btyp == 'h' )
      Helper<unsigned char>::cpy_bins( ptr, j );
    else
      throw std::runtime_error( "Unknown Gaudi Histogram type: " + typstr );
    return pp->reclen;
  }
} // namespace Online
#endif /* ONLINE_GAUCHO_SRC_COMPONENTS_GAUDIHISTOGRAMS_H_ */
