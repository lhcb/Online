//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================

/// Framework include files
#include <CPP/Value.h>
#include <Gaudi/Accumulators.h>
#include <Gaudi/MonitoringHub.h>
#include <RTL/strdef.h>

/// C/C++ include files
#include <map>
#include <set>
#include <typeinfo>

#include <GAUDI_VERSION.h>
#if GAUDI_MAJOR_VERSION < 37
namespace Gaudi::Monitoring {
  inline void reset( Hub::Entity& e ) { e.reset(); }
  inline void to_json( nlohmann::json& j, const Hub::Entity& c ) {
    j = c.toJSON();
  }
}
#endif

namespace {
  // trampoline code to go around the problem of not being able to call
  // hidden friend reset function on Entity as it collides with the reset
  // function of MonCounter
  // See https://fekir.info/post/when-you-cannot-call-hidden-friends-in-cpp/
  // for more details on the problem of calling hidden friend when names
  // are clashing
  void resetEntity( Gaudi::Monitoring::Hub::Entity const& e ) { reset( e ); }
}

// We can choose here the wire protocol:
// -- Either use the "true" types (Define must be enabled) or (USE_TRUE_TYPE > 0)
// -- Collapse the true types to long/double (USE_TRUE_TYPE == 0)
#define USE_TRUE_TYPE 1

//
/// specialization of MonCounter class for AveragingCounters
static std::set<char> int_set   = {'c', 's', 'i', 'l', 'x'};
static std::set<char> uint_set  = {'h', 't', 'j', 'm', 'y'};
static std::set<char> float_set = {'f', 'd', 'e'};

static std::map<std::pair<std::string, char>, MONTYPE> TypeTranslator = {
    {std::pair{"Counter", 'c'},          C_GAUDIACCCHAR},   {std::pair{"Counter", 'h'}, C_GAUDIACCuCHAR},
    {std::pair{"Counter", 's'},          C_GAUDIACCSHORT},  {std::pair{"Counter", 't'}, C_GAUDIACCuSHORT},
    {std::pair{"Counter", 'i'},          C_GAUDIACCINT},    {std::pair{"Counter", 'j'}, C_GAUDIACCuINT},
    {std::pair{"Counter", 'l'},          C_GAUDIACCLONG},   {std::pair{"Counter", 'm'}, C_GAUDIACCuLONG},
    {std::pair{"Counter", 'f'},          C_GAUDIACCFLOAT},  {std::pair{"Counter", 'd'}, C_GAUDIACCDOUBLE},
    {std::pair{"Counter", 'e'},          C_GAUDIACCDOUBLE},

    {std::pair{"AveragingCounter", 'c'}, C_GAUDIAVGACCc},   {std::pair{"AveragingCounter", 'h'}, C_GAUDIAVGACCcu},
    {std::pair{"AveragingCounter", 's'}, C_GAUDIAVGACCs},   {std::pair{"AveragingCounter", 't'}, C_GAUDIAVGACCsu},
    {std::pair{"AveragingCounter", 'i'}, C_GAUDIAVGACCi},   {std::pair{"AveragingCounter", 'j'}, C_GAUDIAVGACCiu},
    {std::pair{"AveragingCounter", 'l'}, C_GAUDIAVGACCl},   {std::pair{"AveragingCounter", 'm'}, C_GAUDIAVGACClu},
    {std::pair{"AveragingCounter", 'f'}, C_GAUDIAVGACCf},   {std::pair{"AveragingCounter", 'd'}, C_GAUDIAVGACCd},
    {std::pair{"AveragingCounter", 'e'}, C_GAUDIAVGACCd},

    {std::pair{"StatCounter", 'c'},      C_GAUDISTATACCc},  {std::pair{"StatCounter", 'h'}, C_GAUDISTATACCcu},
    {std::pair{"StatCounter", 's'},      C_GAUDISTATACCs},  {std::pair{"StatCounter", 't'}, C_GAUDISTATACCsu},
    {std::pair{"StatCounter", 'i'},      C_GAUDISTATACCi},  {std::pair{"StatCounter", 'j'}, C_GAUDISTATACCiu},
    {std::pair{"StatCounter", 'l'},      C_GAUDISTATACCl},  {std::pair{"StatCounter", 'm'}, C_GAUDISTATACClu},
    {std::pair{"StatCounter", 'f'},      C_GAUDISTATACCf},  {std::pair{"StatCounter", 'd'}, C_GAUDISTATACCd},
    {std::pair{"StatCounter", 'e'},      C_GAUDISTATACCd},

    {std::pair{"SigmaCounter", 'c'},     C_GAUDISIGMAACCc}, {std::pair{"SigmaCounter", 'h'}, C_GAUDISIGMAACCcu},
    {std::pair{"SigmaCounter", 's'},     C_GAUDISIGMAACCs}, {std::pair{"SigmaCounter", 't'}, C_GAUDISIGMAACCsu},
    {std::pair{"SigmaCounter", 'i'},     C_GAUDISIGMAACCi}, {std::pair{"SigmaCounter", 'j'}, C_GAUDISIGMAACCiu},
    {std::pair{"SigmaCounter", 'l'},     C_GAUDISIGMAACCl}, {std::pair{"SigmaCounter", 'm'}, C_GAUDISIGMAACClu},
    {std::pair{"SigmaCounter", 'f'},     C_GAUDISIGMAACCf}, {std::pair{"SigmaCounter", 'd'}, C_GAUDISIGMAACCd},
    {std::pair{"SigmaCounter", 'e'},     C_GAUDISIGMAACCd},

    {std::pair{"BinomialCounter", 'c'},  C_GAUDIBINACCc},   {std::pair{"BinomialCounter", 'h'}, C_GAUDIBINACCcu},
    {std::pair{"BinomialCounter", 's'},  C_GAUDIBINACCs},   {std::pair{"BinomialCounter", 't'}, C_GAUDIBINACCsu},
    {std::pair{"BinomialCounter", 'i'},  C_GAUDIBINACCi},   {std::pair{"BinomialCounter", 'j'}, C_GAUDIBINACCiu},
    {std::pair{"BinomialCounter", 'l'},  C_GAUDIBINACCl},   {std::pair{"BinomialCounter", 'm'}, C_GAUDIBINACClu},
    {std::pair{"BinomialCounter", 'f'},  C_GAUDIBINACCf},   {std::pair{"BinomialCounter", 'd'}, C_GAUDIBINACCd},
    {std::pair{"BinomialCounter", 'e'},  C_GAUDIBINACCd},

    {std::pair{"MsgCounter", 'm'}, C_GAUDIMSGACClu},
};
using namespace Gaudi::Monitoring;
using namespace Gaudi::Accumulators;
using Entity = Hub::Entity;


namespace Online {

  template <typename T> inline void _copy_avg(nlohmann::json& j, DimBuffBase* pp)  {
    unsigned char* data_ptr = add_ptr<unsigned char>( pp, pp->dataoff );
    CPP::_PtrItem<long>  (data_ptr ).data()                   = j.at( "nEntries" ).get<long>();
    CPP::_PtrItem<double>(data_ptr += sizeof( long ) ).data() = j.at( "mean" ).get<double>();
#if USE_TRUE_TYPE > 0
    CPP::_PtrItem<T>     (data_ptr += sizeof( double ) ).data() = j.at( "sum" ).get<T>();
#else
    pp->type = C_GAUDIAVGACCl;
    CPP::_PtrItem<long>(  data_ptr += sizeof( double ) ).data() = j.at( "sum" ).get<long>();
#endif
  }
  template <> inline void _copy_avg<float>(nlohmann::json& j, DimBuffBase* pp)  {
    unsigned char* data_ptr = add_ptr<unsigned char>( pp, pp->dataoff );
    CPP::_PtrItem<long>(  data_ptr ).data()                   = j.at( "nEntries" ).get<long>();
    CPP::_PtrItem<double>(data_ptr += sizeof( long ) ).data() = j.at( "mean" ).get<double>();
#if USE_TRUE_TYPE > 0
    CPP::_PtrItem<float>(  data_ptr += sizeof( double ) ).data()  = j.at( "sum"  ).get<float>();
#else
    pp->type = C_GAUDIAVGACCd;
    CPP::_PtrItem<double>(  data_ptr += sizeof( double ) ).data() = j.at( "sum"  ).get<double>();
#endif
  }
  template <typename T> inline void _copy_stat(nlohmann::json& j, DimBuffBase* pp)  {
    unsigned char* data_ptr = add_ptr<unsigned char>( pp, pp->dataoff );
    CPP::_PtrItem<long>(  data_ptr ).data()                   = j.at( "nEntries" ).get<long>();
    CPP::_PtrItem<double>(data_ptr += sizeof( long ) ).data() = j.at( "mean" ).get<double>();
#if USE_TRUE_TYPE > 0
    CPP::_PtrItem<T>(  data_ptr += sizeof( double ) ).data() = j.at( "sum"  ).get<T>();
    CPP::_PtrItem<T>(  data_ptr += sizeof( T ) ).data()      = j.at( "sum2" ).get<T>();
#else
    pp->type = C_GAUDISTATACCl;
    CPP::_PtrItem<long>(  data_ptr += sizeof( long ) ).data() = j.at( "sum"  ).get<long>();
    CPP::_PtrItem<long>(  data_ptr += sizeof( long ) ).data() = j.at( "sum2" ).get<long>();
#endif
  }
  template <> inline void _copy_stat<float>(nlohmann::json& j, DimBuffBase* pp)  {
    unsigned char* data_ptr = add_ptr<unsigned char>( pp, pp->dataoff );
    CPP::_PtrItem<long>(  data_ptr ).data()                   = j.at( "nEntries" ).get<long>();
    CPP::_PtrItem<double>(data_ptr += sizeof( long ) ).data() = j.at( "mean" ).get<double>();
#if USE_TRUE_TYPE > 0
    CPP::_PtrItem<float>(  data_ptr += sizeof( double ) ).data()  = j.at( "sum"  ).get<float>();
    CPP::_PtrItem<float>(  data_ptr += sizeof( float  ) ).data()  = j.at( "sum2" ).get<float>();
#else
    pp->type = C_GAUDISTATACCd;
    CPP::_PtrItem<double>(  data_ptr += sizeof( double ) ).data() = j.at( "sum"  ).get<double>();
    CPP::_PtrItem<double>(  data_ptr += sizeof( double ) ).data() = j.at( "sum2" ).get<double>();
#endif
  }
  template <typename T> static inline void _copy_sigma(nlohmann::json& j, DimBuffBase* pp)  {
    unsigned char* data_ptr = add_ptr<unsigned char>( pp, pp->dataoff );
    CPP::_PtrItem<long>(  data_ptr ).data()                   = j.at( "nEntries" ).get<long>();
    CPP::_PtrItem<double>(data_ptr += sizeof( long ) ).data() = j.at( "mean" ).get<double>();
#if USE_TRUE_TYPE > 0
    CPP::_PtrItem<T>(  data_ptr += sizeof( double ) ).data()  = j.at( "sum"  ).get<T>();
    CPP::_PtrItem<T>(  data_ptr += sizeof( T ) ).data()       = j.at( "sum2" ).get<T>();
#else
    pp->type = C_GAUDISIGMAACCl;
    CPP::_PtrItem<long>(  data_ptr += sizeof( double ) ).data() = j.at( "sum"  ).get<long>();
    CPP::_PtrItem<long>(  data_ptr += sizeof( long ) ).data() = j.at( "sum2" ).get<long>();
#endif
  }
  template <> inline void _copy_sigma<float>(nlohmann::json& j, DimBuffBase* pp)  {
    unsigned char* data_ptr = add_ptr<unsigned char>( pp, pp->dataoff );
    CPP::_PtrItem<long>(  data_ptr ).data()                   = j.at( "nEntries" ).get<long>();
    CPP::_PtrItem<double>(data_ptr += sizeof( long ) ).data() = j.at( "mean" ).get<double>();
#if USE_TRUE_TYPE > 0
    CPP::_PtrItem<float>(  data_ptr += sizeof( double ) ).data()  = j.at( "sum"  ).get<float>();
    CPP::_PtrItem<float>(  data_ptr += sizeof( float  ) ).data()  = j.at( "sum2" ).get<float>();
#else
    pp->type = C_GAUDISIGMAACCd;
    CPP::_PtrItem<double>(  data_ptr += sizeof( double ) ).data() = j.at( "sum"  ).get<double>();
    CPP::_PtrItem<double>(  data_ptr += sizeof( double ) ).data() = j.at( "sum2" ).get<double>();
#endif
  }

  template <>
  struct MonCounter<Entity> : MonCounterBase {
    std::unique_ptr<Entity> m_ent;
    MonCounter( const std::string& nam, const std::string& tit, const Entity& ent )
        : MonCounterBase{H_ILLEGAL, nam, tit, 0, 0, 0} {
      m_ent     = std::make_unique<Entity>( ent );
      auto splt = RTL::str_split( ent.type.c_str(), ":" );
      if ( splt.at( 0 ) != "counter" ) { throw std::runtime_error( "Bad Entity Type: " + splt.at( 0 ) ); }
      auto CType = TypeTranslator.end();
      if ( splt.at( 1 ) == "MsgCounter" ) {
        CType = TypeTranslator.find( std::make_pair( splt.at( 1 ), 'm' ) );
      } else {
        CType = TypeTranslator.find( std::make_pair( splt.at( 1 ), *splt.at( 2 ).c_str() ) );
      }
      if ( CType == TypeTranslator.end() ) {
        throw std::runtime_error( m_ent->component + "/" + m_ent->name + 
				  ": Unrecognized Accumulator/Precision combination: " + splt.at( 2 ) + " " +
                                  splt.at( 2 ) );
      }
#if 0
      std::cout << m_ent->component << "/"  << m_ent->name 
		<<  ": RECOGNIZED Accumulator/Precision combination: " << splt.at( 2 ) << " " << splt.at( 2 )
		<< std::endl;
      std::cout << std::flush;
#endif
      this->type       = CType->second;
      this->buffersize = std::max( 8u, serialSize() );
      if ( this->buffersize != 0 ) this->counterData.reset( new unsigned char[this->buffersize] );
      this->m_contsiz  = serialSize();
      this->m_contents = this->counterData.get();
    }

    MONTYPE      Type() const { return this->type; }
    unsigned int serialSize() const {
      switch ( this->type ) {
      case C_GAUDIACCCHAR:
      case C_GAUDIACCuCHAR:
      case C_GAUDIACCSHORT:
      case C_GAUDIACCuSHORT:
      case C_GAUDIACCINT:
      case C_GAUDIACCuINT:
      case C_GAUDIACCLONG:
      case C_GAUDIACCuLONG:
      case C_GAUDIACCFLOAT:
      case C_GAUDIACCDOUBLE:

      case C_GAUDIMSGACClu:
        return sizeof( long );

      case C_GAUDIAVGACCc:
      case C_GAUDIAVGACCcu:
      case C_GAUDIAVGACCs:
      case C_GAUDIAVGACCsu:
      case C_GAUDIAVGACCi:
      case C_GAUDIAVGACCiu:
      case C_GAUDIAVGACCl:
      case C_GAUDIAVGACClu:
        return sizeof( long ) + 2*sizeof(long);
      case C_GAUDIAVGACCf:
      case C_GAUDIAVGACCd:
        return sizeof( long ) + 2*sizeof(double);

      case C_GAUDISTATACCc:
      case C_GAUDISTATACCcu:
      case C_GAUDISTATACCs:
      case C_GAUDISTATACCsu:
      case C_GAUDISTATACCi:
      case C_GAUDISTATACCiu:
      case C_GAUDISTATACCl:
      case C_GAUDISTATACClu:
        return sizeof( long ) + 2*sizeof(long) + sizeof(double);
      case C_GAUDISTATACCf:
      case C_GAUDISTATACCd:
        return sizeof( long ) + 3*sizeof(double);

      case C_GAUDISIGMAACCc:
      case C_GAUDISIGMAACCcu:
      case C_GAUDISIGMAACCs:
      case C_GAUDISIGMAACCsu:
      case C_GAUDISIGMAACCi:
      case C_GAUDISIGMAACCiu:
      case C_GAUDISIGMAACCl:
      case C_GAUDISIGMAACClu:
        return sizeof( long ) + 2*sizeof(long) + sizeof(double);
      case C_GAUDISIGMAACCf:
      case C_GAUDISIGMAACCd:
        return sizeof( long ) + 3*sizeof(double);

      case C_GAUDIBINACCc:
      case C_GAUDIBINACCcu:
      case C_GAUDIBINACCs:
      case C_GAUDIBINACCsu:
      case C_GAUDIBINACCi:
      case C_GAUDIBINACCiu:
      case C_GAUDIBINACCl:
      case C_GAUDIBINACClu:
        return 2 * sizeof( long );
      case C_GAUDIBINACCf:
      case C_GAUDIBINACCd:
        return 2 * sizeof( long );
      default:
        return 0;
      }
    }

    /// Serialize data. We dynamically upgrade the data types: float -> double and (char, short, int) -> long
    unsigned int serializeData( DimBuffBase* pp, const void* /*src*/, int ) const override {
      unsigned char* data_ptr = add_ptr<unsigned char>( pp, pp->dataoff );
      nlohmann::json j        = *m_ent;
      switch ( this->type ) {

      case C_GAUDIMSGACClu:
	pp->type = C_GAUDIACCLONG;
        CPP::_PtrItem<long>( data_ptr ).data() = j.at( "nEntries" ).get<unsigned long>();
        break;

	/// ==========================================================================================
      case C_GAUDIACCCHAR:
      case C_GAUDIACCuCHAR:
      case C_GAUDIACCSHORT:
      case C_GAUDIACCuSHORT: 
      case C_GAUDIACCINT:
      case C_GAUDIACCuINT:
      case C_GAUDIACCLONG:
      case C_GAUDIACCuLONG:
	pp->type = C_GAUDIACCLONG;
        CPP::_PtrItem<long>( data_ptr ).data() = j.at( "nEntries" ).get<long>();
        break;
      case C_GAUDIACCFLOAT:
	pp->type = C_GAUDIACCFLOAT;
        CPP::_PtrItem<float>( data_ptr ).data() = j.at( "nEntries" ).get<float>();
        break;
      case C_GAUDIACCDOUBLE:
	pp->type = C_GAUDIACCDOUBLE;
        CPP::_PtrItem<double>( data_ptr ).data() = j.at( "nEntries" ).get<double>();
        break;

	/// ==========================================================================================
      case C_GAUDIAVGACCc:      _copy_avg<signed char>(j, pp);     break;
      case C_GAUDIAVGACCcu:     _copy_avg<unsigned char>(j, pp);   break;
      case C_GAUDIAVGACCs:      _copy_avg<short>(j, pp);           break;
      case C_GAUDIAVGACCsu:     _copy_avg<unsigned short>(j, pp);  break;
      case C_GAUDIAVGACCi:      _copy_avg<int>(j, pp);             break;
      case C_GAUDIAVGACCiu:     _copy_avg<unsigned int>(j, pp);    break;
      case C_GAUDIAVGACCl:      _copy_avg<long>(j, pp);            break;
      case C_GAUDIAVGACClu:     _copy_avg<unsigned long>(j, pp);   break;
      case C_GAUDIAVGACCf:      _copy_avg<float>(j, pp);           break;
      case C_GAUDIAVGACCd:      _copy_avg<double>(j, pp);          break;
	/// ==========================================================================================
      case C_GAUDISTATACCc:     _copy_stat<signed char>(j, pp);     break;
      case C_GAUDISTATACCcu:    _copy_stat<unsigned char>(j, pp);   break;
      case C_GAUDISTATACCs:     _copy_stat<short>(j, pp);           break;
      case C_GAUDISTATACCsu:    _copy_stat<unsigned short>(j, pp);  break;
      case C_GAUDISTATACCi:     _copy_stat<int>(j, pp);             break;
      case C_GAUDISTATACCiu:    _copy_stat<unsigned int>(j, pp);    break;
      case C_GAUDISTATACCl:     _copy_stat<long>(j, pp);            break;
      case C_GAUDISTATACClu:    _copy_stat<unsigned long>(j, pp);   break;
      case C_GAUDISTATACCf:     _copy_stat<float>(j, pp);           break;
      case C_GAUDISTATACCd:     _copy_stat<double>(j, pp);          break;

	/// ==========================================================================================
      case C_GAUDISIGMAACCc:    _copy_sigma<signed char>(j, pp);    break;
      case C_GAUDISIGMAACCcu:   _copy_sigma<unsigned char>(j, pp);  break;
      case C_GAUDISIGMAACCs:    _copy_sigma<short>(j, pp);          break;
      case C_GAUDISIGMAACCsu:   _copy_sigma<unsigned short>(j, pp); break;
      case C_GAUDISIGMAACCi:    _copy_sigma<int>(j, pp);            break;
      case C_GAUDISIGMAACCiu:   _copy_sigma<unsigned int>(j, pp);   break;
      case C_GAUDISIGMAACCl:    _copy_sigma<long>(j, pp);           break;
      case C_GAUDISIGMAACClu:   _copy_sigma<unsigned long>(j, pp);  break;
      case C_GAUDISIGMAACCf:    _copy_sigma<float>(j, pp);          break;
      case C_GAUDISIGMAACCd:    _copy_sigma<double>(j, pp);         break;
	/// ==========================================================================================

      case C_GAUDIBINACCc:
      case C_GAUDIBINACCcu:
      case C_GAUDIBINACCs:
      case C_GAUDIBINACCsu:
      case C_GAUDIBINACCi:
      case C_GAUDIBINACCiu:
      case C_GAUDIBINACCl:
      case C_GAUDIBINACClu:
	pp->type = C_GAUDIBINACCl;
        CPP::_PtrItem<long>( data_ptr ).data()                   = j.at( "nTrueEntries" ).get<long>();
        CPP::_PtrItem<long>( data_ptr += sizeof( long ) ).data() = j.at( "nFalseEntries" ).get<long>();
	break;
      case C_GAUDIBINACCf:
      case C_GAUDIBINACCd:
	pp->type = C_GAUDIBINACCd;
        CPP::_PtrItem<long>( data_ptr ).data()                  = j.at( "nTrueEntries" ).get<long>();
        CPP::_PtrItem<long>( data_ptr + sizeof( long ) ).data() = j.at( "nFalseEntries" ).get<long>();
        break;
      default:
        return 0;
      }
      ::memcpy( this->counterData.get(), add_ptr( pp, pp->dataoff ), this->serialSize() );
      return pp->reclen;
    }

    void reset() override { resetEntity( *m_ent ); }

    void create_OutputService( const std::string& infix ) override {
      std::string nam = m_srvcprefix + infix + this->name;
      if ( nam.length() <= 128 /* maximum length of a DIM service Name*/ ) {
        this->dim_service.reset( new DimService( nam.c_str(), "X", this->counterData.get(), this->serialSize() ) );
      }
    }
  };

  template <typename T, Gaudi::Accumulators::atomicity A>
  struct MonCounter<Gaudi::Accumulators::AveragingCounter<T, A>> : MonCounterBase {

    using CounterType = Gaudi::Accumulators::AveragingCounter<T, A>;
    using longtype    = Gaudi::Accumulators::AveragingCounter<long, A>;
    using floattype   = Gaudi::Accumulators::AveragingCounter<float, A>;
    using doubletype  = Gaudi::Accumulators::AveragingCounter<double, A>;
    using ulongtype   = Gaudi::Accumulators::AveragingCounter<unsigned long, A>;
    MonCounter( const std::string& nam, const std::string& tit, const CounterType& data )
        : MonCounterBase{Type(), nam, tit, std::max( 8u, serialSize() ), serialSize(), (CounterType*)&data} {
      if ( !this->counterData && this->buffersize != 0 ) {
        this->counterData.reset( new unsigned char[this->buffersize] );
      }
    }

    MONTYPE Type() const {
      const std::type_info& rt  = typeid( CounterType );
      const std::type_info& lt  = typeid( longtype );
      const std::type_info& dt  = typeid( doubletype );
      const std::type_info& ult = typeid( ulongtype );
      const std::type_info& ft  = typeid( floattype );
      if ( rt == lt )
        return C_GAUDIAVGACCl;
      else if ( rt == dt )
        return C_GAUDIAVGACCd;
      else if (rt == ult)
        return C_GAUDIAVGACClu;
      else if (rt == ft)
        return C_GAUDIAVGACCf;
      else
        return H_ILLEGAL;
    }
    unsigned int serialSize() const { return sizeof( unsigned long ) + sizeof( double ); }
    int          serialize( void* ptr ) override {
      DimBuffBase* pp = (DimBuffBase*)ptr;
      return this->serializeHeader( pp ) ? this->serializeData( pp, this->m_contents, this->m_contsiz ) : 0;
    }
    unsigned int serializeData( DimBuffBase* pp, const void* src, int ) const override {
      CounterType*   s         = (CounterType*)src;
      unsigned long* ent_dest  = add_ptr<unsigned long>( pp, pp->dataoff );
      T*             data_dest = add_ptr<T>( pp, pp->dataoff + sizeof( unsigned long ) );
      *ent_dest                = (unsigned long)( s->nEntries() );
      *data_dest               = s->sum();
      ::memcpy( this->counterData.get(), ent_dest, this->serialSize() );
      return pp->reclen;
    }
    void create_OutputService( const std::string& infix ) override {
      this->dim_service.reset();
      std::string nam = m_srvcprefix + infix + this->name;
      if ( nam.length() <= 128 /* maximum length of a DIM service Name*/ ) {
        this->dim_service.reset( new DimService( nam.c_str(), "X", this->counterData.get(), serialSize() ) );
      }
    }
  };

  /// specialization of MonCounter class for BinomialCounters
  template <typename T, Gaudi::Accumulators::atomicity A>
  struct MonCounter<Gaudi::Accumulators::BinomialCounter<T, A>> : MonCounterBase {

    using CounterType = Gaudi::Accumulators::BinomialCounter<T, A>;
    using doubletype  = Gaudi::Accumulators::BinomialCounter<double, A>;
    MonCounter( const std::string& nam, const std::string& tit, const CounterType& data )
        : MonCounterBase{Type(), nam, tit, std::max( 8u, serialSize() ), serialSize(), (CounterType*)&data} {
      if ( !this->counterData && this->buffersize != 0 ) this->counterData.reset( new unsigned char[this->buffersize] );
    }

    MONTYPE Type() const {
      const std::type_info& rt = typeid( CounterType );
      const std::type_info& dt = typeid( doubletype );
      if ( rt == dt )
        return C_GAUDIBINACCd;
      else
        return C_GAUDIBINACCl;
    }
    unsigned int serialSize() const { return 2 * sizeof( unsigned long ); }
    int          serialize( void* ptr ) override {
      DimBuffBase* pp = (DimBuffBase*)ptr;
      return this->serializeHeader( pp ) ? this->serializeData( pp, this->m_contents, this->m_contsiz ) : 0;
    }
    unsigned int serializeData( DimBuffBase* pp, const void* src, int ) const override {
      CounterType*   cntr_typ                                 = (CounterType*)src;
      unsigned char* data_ptr                                 = add_ptr<unsigned char>( pp, pp->dataoff );
      CPP::_PtrItem<long>( data_ptr ).data()                  = cntr_typ->nTrueEntries();
      CPP::_PtrItem<long>( data_ptr + sizeof( long ) ).data() = cntr_typ->nFalseEntries();
      ::memcpy( this->counterData.get(), data_ptr, this->serialSize() );
      return pp->reclen;
    }
    void create_OutputService( const std::string& infix ) override {
      this->dim_service.reset();
      std::string nam = m_srvcprefix + infix + this->name;
      if ( nam.length() <= 128 /* maximum length of a DIM service Name*/ ) {
        this->dim_service.reset( new DimService( nam.c_str(), "X", this->counterData.get(), serialSize() ) );
      }
    }
  };


  /// specialization of MonCounter class for StatCounters
  template <typename T, Gaudi::Accumulators::atomicity A>
  struct MonCounter<Gaudi::Accumulators::StatCounter<T, A>> : MonCounterBase {

    using CounterType = Gaudi::Accumulators::StatCounter<T, A>;
    using doubletype  = Gaudi::Accumulators::StatCounter<double, A>;
    using ulongtype   = Gaudi::Accumulators::StatCounter<unsigned long, A>;
    MonCounter( const std::string& nam, const std::string& tit, const CounterType& data )
        : MonCounterBase{Type(), nam, tit, std::max( 8u, serialSize() ), serialSize(), (CounterType*)&data} {
      if ( !this->counterData && this->buffersize != 0 ) this->counterData.reset( new unsigned char[this->buffersize] );
    }

    MONTYPE Type() const {
      const std::type_info& rt = typeid( CounterType );
      const std::type_info& dt = typeid( doubletype );
      const std::type_info& ult = typeid( ulongtype );
      if ( rt == dt )
        return C_GAUDISTATACCd;
      else if ( rt == ult )
        return C_GAUDISTATACClu;
      else
        return H_ILLEGAL;
    }
    unsigned int serialSize() const { return 2 * sizeof( unsigned long ); }
    int          serialize( void* ptr ) override {
      DimBuffBase* pp = (DimBuffBase*)ptr;
      return this->serializeHeader( pp ) ? this->serializeData( pp, this->m_contents, this->m_contsiz ) : 0;
    }
    unsigned int serializeData( DimBuffBase* pp, const void* src, int ) const override {
      CounterType*   cntr_typ                                 = (CounterType*)src;
      unsigned char* data_ptr                                 = add_ptr<unsigned char>( pp, pp->dataoff );
      CPP::_PtrItem<long>( data_ptr ).data()                  = cntr_typ->nEntries();
      CPP::_PtrItem<long>( data_ptr + sizeof( long ) ).data() = cntr_typ->sum();
      ::memcpy( this->counterData.get(), data_ptr, this->serialSize() );
      return pp->reclen;
    }
    void create_OutputService( const std::string& infix ) override {
      this->dim_service.reset();
      std::string nam = m_srvcprefix + infix + this->name;
      if ( nam.length() <= 128 /* maximum length of a DIM service Name*/ ) {
        this->dim_service.reset( new DimService( nam.c_str(), "X", this->counterData.get(), serialSize() ) );
      }
    }
  };



  /// specialization of MonCounter class for MsgCounters
  template<MSG::Level M, Gaudi::Accumulators::atomicity A>
  struct MonCounter<Gaudi::Accumulators::MsgCounter<M, A>> : MonCounterBase    {

    using CounterType = Gaudi::Accumulators::MsgCounter<M, A>;
    MonCounter(const std::string& nam, const std::string& tit, const CounterType& data)
    : MonCounterBase{ Type(), nam, tit, std::max(8u, serialSize()), serialSize(),(CounterType*) &data }
    {
      if ( !this->counterData && this->buffersize != 0 )
	      this->counterData.reset(new unsigned char[this->buffersize]);
    }

    MONTYPE Type() const      {
      return C_GAUDIMSGACClu;
    }
    unsigned int serialSize()  const    {
      return 2 * sizeof(long) + sizeof(std::string);
    }
    int serialize(void* ptr) override      {
      DimBuffBase *pp = (DimBuffBase*)ptr;
      return this->serializeHeader(pp) ? this->serializeData(pp, this->m_contents, this->m_contsiz) : 0;
    }
    unsigned int serializeData(DimBuffBase *pp, const void *src, int)  const override    {
      CounterType   *cntr_typ = (CounterType*)src;
      unsigned char *data_ptr = add_ptr<unsigned char>(pp, pp->dataoff);
      CPP::_PtrItem<unsigned long>(data_ptr).data() = cntr_typ->nEntries();
      ::memcpy(this->counterData.get(), data_ptr, this->serialSize());
      return pp->reclen;
    }
    void create_OutputService(const std::string & infix) override    {
      this->dim_service.reset();
      std::string nam = m_srvcprefix + infix + this->name;
      if (nam.length() <= 128 /* maximum length of a DIM service Name*/) {
	      this->dim_service.reset(new DimService(nam.c_str(), "X", this->counterData.get(), serialSize()));
      }
    }
  };


  /// specialization of MonCounter class for Counters
  template<Gaudi::Accumulators::atomicity A>
  struct MonCounter<Gaudi::Accumulators::Counter<A>> : MonCounterBase    {

    using CounterType = Gaudi::Accumulators::Counter<A>;
    MonCounter(const std::string& nam, const std::string& tit, const CounterType& data)
    : MonCounterBase{ Type(), nam, tit, std::max(8u, serialSize()), serialSize(),(CounterType*) &data }
    {
      if ( !this->counterData && this->buffersize != 0 )
	      this->counterData.reset(new unsigned char[this->buffersize]);
    }

    MONTYPE Type() const      {
      return C_GAUDIACCuLONG;
    }
    unsigned int serialSize()  const    {
      return 2 * sizeof(long) + sizeof(std::string);
    }
    int serialize(void* ptr) override      {
      DimBuffBase *pp = (DimBuffBase*)ptr;
      return this->serializeHeader(pp) ? this->serializeData(pp, this->m_contents, this->m_contsiz) : 0;
    }
    unsigned int serializeData(DimBuffBase *pp, const void *src, int)  const override    {
      CounterType   *cntr_typ = (CounterType*)src;
      unsigned char *data_ptr = add_ptr<unsigned char>(pp, pp->dataoff);
      CPP::_PtrItem<unsigned long>(data_ptr).data() = cntr_typ->nEntries();
      ::memcpy(this->counterData.get(), data_ptr, this->serialSize());
      return pp->reclen;
    }
    void create_OutputService(const std::string & infix) override    {
      this->dim_service.reset();
      std::string nam = m_srvcprefix + infix + this->name;
      if (nam.length() <= 128 /* maximum length of a DIM service Name*/) {
	      this->dim_service.reset(new DimService(nam.c_str(), "X", this->counterData.get(), serialSize()));
      }
    }
  };



  /// specialization of MonCounter class for SigmaCounters
  template <typename T, Gaudi::Accumulators::atomicity A>
  struct MonCounter<Gaudi::Accumulators::SigmaCounter<T, A>> : MonCounterBase {

    using CounterType = Gaudi::Accumulators::SigmaCounter<T, A>;
    using doubletype  = Gaudi::Accumulators::SigmaCounter<double, A>;
    using longtype    = Gaudi::Accumulators::SigmaCounter<long, A>;
    MonCounter( const std::string& nam, const std::string& tit, const CounterType& data )
        : MonCounterBase{Type(), nam, tit, std::max( 8u, serialSize() ), serialSize(), (CounterType*)&data} {
      if ( !this->counterData && this->buffersize != 0 ) this->counterData.reset( new unsigned char[this->buffersize] );
    }

    MONTYPE Type() const {
      const std::type_info& rt = typeid( CounterType );
      const std::type_info& dt = typeid( doubletype );
      const std::type_info& lt = typeid( longtype );
      if ( rt == dt )
        return C_GAUDISIGMAACCd;
      else if ( rt == lt )
        return C_GAUDISIGMAACCl;
      else
        return H_ILLEGAL;
    }
    unsigned int serialSize() const { return 2 * sizeof( unsigned long ); }
    int          serialize( void* ptr ) override {
      DimBuffBase* pp = (DimBuffBase*)ptr;
      return this->serializeHeader( pp ) ? this->serializeData( pp, this->m_contents, this->m_contsiz ) : 0;
    }
    unsigned int serializeData( DimBuffBase* pp, const void* src, int ) const override {
      CounterType*   cntr_typ                                 = (CounterType*)src;
      unsigned char* data_ptr                                 = add_ptr<unsigned char>( pp, pp->dataoff );
      CPP::_PtrItem<long>( data_ptr ).data()                  = cntr_typ->nEntries();
      CPP::_PtrItem<long>( data_ptr + sizeof( long ) ).data() = cntr_typ->sum();
      ::memcpy( this->counterData.get(), data_ptr, this->serialSize() );
      return pp->reclen;
    }
    void create_OutputService( const std::string& infix ) override {
      this->dim_service.reset();
      std::string nam = m_srvcprefix + infix + this->name;
      if ( nam.length() <= 128 /* maximum length of a DIM service Name*/ ) {
        this->dim_service.reset( new DimService( nam.c_str(), "X", this->counterData.get(), serialSize() ) );
      }
    }
  };

} // namespace Online
