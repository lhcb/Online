//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#include <GaudiKernel/Service.h>
#include <GaudiKernel/IIncidentListener.h>
#include <GaudiKernel/IIncidentSvc.h>
#include <GaudiKernel/IUpdateable.h>
#include <Gaucho/IGauchoMonitorSvc.h>
#include <Gaucho/TaskSaveTimer.h>
#include <GaudiKernel/IHistogramSvc.h>
#include <dim/dis.hxx>
#include <RTL/rtl.h>

#include <TH1D.h>
#include <TProfile.h>
#include <AIDA/IProfile1D.h>
#include <AIDA/IHistogram1D.h>

#include <typeinfo>
#include <thread>
#include <atomic>
#include <string>
#include <map>
#include <set>

#include <GAUDI_VERSION.h>

using namespace Gaudi::Accumulators;
class MonSvcTest;
class MyCommand;

void runng(MonSvcTest *tis);

// backward compatibility with Gaudi < v39 where FSMCallbackHolder does not exist
#if GAUDI_MAJOR_VERSION < 39
using BaseClass = Service;
#else
#include <Gaudi/FSMCallbackHolder.h>
using BaseClass = Gaudi::FSMCallbackHolder<Service>;
#endif

class MonSvcTest : virtual public BaseClass, virtual public IIncidentListener   {
public:
  int m_count=0;
  int m_count1=0;
  double m_double=0;
  double m_darray[100];
  int m_iarray[100];
  unsigned int m_uiarray[100];
  unsigned long m_ularray[100];
  SmartIF<IGauchoMonitorSvc> m_mons;
  SmartIF<IHistogramSvc>     m_hsvc;
  AIDA::IHistogram1D *m_h1 { nullptr };
  AIDA::IHistogram1D *m_h2 { nullptr };
  std::thread        *m_thread { nullptr };
  bool                m_stop;
  float               m_float;
  int                 m_One;
  std::string         m_SaveDir;
  bool                m_SaveHists;
  int                 m_savePeriode;
  int                 m_count2 = 0;
  std::atomic<int>    m_aint1;
  std::atomic<int>    m_aint2;
  std::atomic<long>   m_along1;
  std::atomic<long>   m_along2;
  Gaudi::Accumulators::AveragingCounter<long> *m_GaudiCounter;
  MyCommand *m_chRun=0;
  bool m_changeRun = false;
  mutable Histogram<2u, atomicity::full,double> m_G2DH1{this, "GaudiH2D", "A Gaudi 2D histogram",{{10, 0.0, 100.0}, {10, 0.0, 50.0}}};
  mutable ProfileHistogram<1u,atomicity::full,double> m_G1DP1{this, "GaudiP1D", "A Gaudi 1D Profile",{10, 0.0, 100.0}};
  AIDA::IProfile2D *m_prof2;
  AIDA::IProfile1D *m_prof1;
  std::shared_ptr<Online::TaskSaveTimer> m_saveTimer;

  MonSvcTest(const std::string& name, ISvcLocator* sl): BaseClass(name, sl)
  {
    m_count=0;
    m_double=0.0;
    m_mons=0;
    m_h1=m_h2=0;m_hsvc=0;
    m_stop = false;
    m_float=0.0;
    for (int i=0;i<100;i++)      {
      m_darray[i]= 0.0;
      m_iarray[i]= 0;
      m_uiarray[i] = 0;
      m_ularray[i] = 0;
    }
    m_One = 1;
    declareProperty("SaveDir",m_SaveDir="/home/beat/hists");
    declareProperty("Save",m_SaveHists=false);
    declareProperty("SaveIntval",m_savePeriode=60);
  }
  StatusCode initialize() override;
  StatusCode start() override    {

    m_stop =false;
    m_thread = new std::thread(runng,this);
    if (m_SaveHists)      {
      std::string part="Beat";
      std::string proc = RTL::processName();
      std::shared_ptr<DimService> svc;
      m_saveTimer = std::make_shared<Online::TaskSaveTimer>(m_savePeriode);
      m_saveTimer->rootdir    = m_SaveDir;
      m_saveTimer->partition  = part;
      m_saveTimer->taskname   = proc;
      m_saveTimer->savesetSvc = svc;
      m_mons->StartSaving(m_saveTimer);
    }
    m_thread->detach();
    StatusCode status=BaseClass::start();
    return status;
  }
  StatusCode stop() override    {
    StatusCode status = BaseClass::stop();
    m_stop =true;
    return status;
  }
  StatusCode finalize() override    {
    StatusCode status = BaseClass::finalize();
    return status;
  }
  StatusCode running ()    {
    int runno = 1234;
    m_mons->setRunNo(runno);
    while(1)
      {
        m_double++;
        m_count++;
        for (int i=0;i<100;i++)
	  {
	    m_darray[i]= m_darray[i]+i;
	    m_iarray[i]+= i;
	  }
        m_aint1++;
        m_along1++;
        if (m_GaudiCounter != 0)*m_GaudiCounter += 1;
        m_G1DP1[0.5] += 0.5;
        m_prof1->fill(-0.5,-0.5);
        for (int i = 0;i<10;i++)
	  {
	    m_G1DP1[10.0*double(i)+0.5] += double(i);
	    m_prof1->fill(10.0*double(i)+0.5,double(i),1.0);
	  }
        m_G1DP1[120.0] += 120.0;
        m_prof1->fill(120.0,120.0);
        nlohmann::json json =
#if GAUDI_MAJOR_VERSION >= 37
          m_G1DP1;
#else
          m_G1DP1.toJSON();
#endif
        usleep(500000);
        if (m_stop) return StatusCode::SUCCESS;
        if (m_changeRun)
	  {
	    m_changeRun = false;
	    m_mons->update(runno).ignore();
	    m_mons->resetHistos(this);
	    runno++;
	    m_mons->setRunNo(runno);
	  }
      }
    return StatusCode::SUCCESS;
  }
  void handle(const Incident& inc) override    {
    MsgStream log(msgSvc(),name());
    log << MSG::INFO << "MonSvcTest: Got incident from:" << inc.source() << ": " << inc.type() << endmsg;
    if ( inc.type() == "APP_RUNNING" ) {
    }
    else if ( inc.type() == "APP_STOPPED" )  {
    }
  }
};

class MyCommand : public DimCommand
{
public:
  MonSvcTest *m_Parent;
  MyCommand (const char *name,MonSvcTest *p) : DimCommand(name,"C")
  {
    m_Parent = p;
  }
  void commandHandler() override
  {
    m_Parent->m_changeRun = true;
  }
};

StatusCode MonSvcTest::initialize()
{
  m_chRun = new MyCommand("ChangeRun", this);
  SmartIF<IIncidentSvc> m_incidentSvc;
  StatusCode sc = BaseClass::initialize();
  if ( !sc.isSuccess() )  {
    return sc;
  }
  m_mons = serviceLocator()->service("MonitorSvc", false);
  m_hsvc = serviceLocator()->service("HistogramDataSvc", true);
  m_incidentSvc = serviceLocator()->service("IncidentSvc", true);
  if( !m_incidentSvc ) {
    return StatusCode::FAILURE;
  }
  m_count1 = -5;
  m_count2 = 6;
  m_incidentSvc->addListener(this,"APP_RUNNING");
  m_incidentSvc->addListener(this,"APP_STOPPED");
  m_h1 = m_hsvc->book("Hist1","a Histogram1",100,0.0,100.0);
  m_h2 = m_hsvc->book("Hist2","a Histogram2",100,0.0,100.0);
  m_prof1 = m_hsvc->bookProf("Prof1","a 1D Profile",10,0.0,100.0);
  m_prof2 = m_hsvc->bookProf("Prof2","a 2D Profile",10,0.0,100.0,10,0.0,100.0);
  m_mons->declareInfo(std::string("NumTask"), m_One,std::string("Number of Tasks"),this);
  m_mons->declareInfo(std::string("Integer1"), m_count,std::string("An Integer"),this);
  m_mons->declareInfo(std::string("Integer2"), m_count,std::string("An Integer"),this);
  m_mons->declareInfo(std::string("Integer3"), m_count,std::string("An Integer"),this);
  m_mons->declareInfo(std::string("Integer4"), m_count,std::string("An Integer"),this);
  m_mons->declareInfo("Float1", m_float,"A Float", this);
  m_mons->declareInfo("Double1", m_double,"A Double", this);
  m_mons->declareInfo("Double2", m_double,"A Double", this);
  m_mons->declareInfo("Double3", m_double,"A Double", this);
  m_mons->declareInfo("Double4", m_double,"A Double", this);
  m_mons->declareInfo("DArray", "d",m_darray,sizeof(m_darray),"A Double Array", this);
  m_mons->declareInfo("IArray", "i",m_iarray,sizeof(m_iarray),"An Integer Array ", this);
  m_mons->declareInfo("UIArray", "i",m_uiarray,sizeof(m_uiarray),"An Unsigned Integer Array ", this);
  m_mons->declareInfo("ULArray", "i",m_ularray,sizeof(m_ularray),"An Unsigned Long Array ", this);
  m_mons->declareInfo( std::string("Hist1"),(const AIDA::IBaseHistogram*)m_h1, std::string("A Histogram1"),this);
  m_mons->declareInfo( std::string("Hist2"),(const AIDA::IBaseHistogram*)m_h2, std::string("A Histogram1"),this);
  m_mons->declareInfo("Pair1",m_count1,m_count2,"A Pair1",this);
  m_mons->declareInfo("AtomicInt1",m_aint1,"A atomic int 1",this);
  m_mons->declareInfo("AtomicInt2",m_aint2,"A atomic int 2",this);
  m_mons->declareInfo("AtomicLong1",m_along1,"A atomic long 1",this);
  m_mons->declareInfo("AtomicLong",m_along2,"A atomic long 2",this);
  m_GaudiCounter = new Gaudi::Accumulators::AveragingCounter<long>(this,"GaudiAcc");
  //  m_mons->declareInfo("GaudiAcc",*m_GaudiCounter,"A Gaudi Counter",this);
  ++m_G2DH1[{10.0, 30.0}];    // prefered syntax
  //  m_mons->declareInfo("Gaudi2DHistogram",m_G2DH1,"A Gaudi Histogram",this);
  //  m_mons->declareInfo("Gaudi1DProfile",m_G1DP1,"A Gaudi 1D Profile",this);
  m_mons->declareInfo("Prof1",(const AIDA::IBaseHistogram*)m_prof1,"A 1D Profile",this);
  m_mons->declareInfo("Prof2",(const AIDA::IBaseHistogram*)m_prof2,"A 2D Profile",this);
  return StatusCode::SUCCESS;
}

void runng(MonSvcTest *tis)
{
  //  tis->m_mons->setRunNo(1234);
  StatusCode status = tis->running();
  if ( !status.isSuccess() ) {}
}

DECLARE_COMPONENT(MonSvcTest)
