//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
// Include files
#include <Gaucho/MonitorInterface.h>
#include <Gaucho/MyDimErrorHandler.h>
#include <GaudiKernel/DataObject.h>
#include <GaudiKernel/IAlgorithm.h>
#include <GaudiKernel/IIncidentSvc.h>
#include <GaudiKernel/IRegistry.h>
#include <GaudiKernel/MsgStream.h>
#include <RTL/rtl.h>
#include <RTL/strdef.h>

#include "GaudiCounters.h"
#include "GaudiHistograms.h"
#include "MonitorSvc.h"
#include <AIDA/IHistogram1D.h>
#include <AIDA/IHistogram2D.h>
#include <AIDA/IProfile1D.h>
#include <AIDA/IProfile2D.h>
#include <TEnv.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TProfile.h>
#include <TProfile2D.h>
#include <TROOT.h>
#include <dim/dis.hxx>

using namespace std;

namespace Online {
  template <typename TO>
  TO* cast_histo( const AIDA::IBaseHistogram* aidahist );
}

// Factory for instantiation of service objects
DECLARE_COMPONENT( MonitorSvc )

// Constructor
MonitorSvc::MonitorSvc( const string& name, ISvcLocator* sl ) : Service( name, sl ) {
  /// Initialize ROOT and disable the SEGV signal handler
  ROOT::GetROOT();
  gEnv->SetValue( "Root.Stacktrace", 0 );
  /// Declare all variables
  declareProperty( "UniqueServiceNames",         m_uniqueServiceNames = 0 );
  declareProperty( "disableMonRate",             m_disableMonRate = 0 );
  declareProperty( "disableDimPropServer",       m_disableDimPropServer = 0 );
  declareProperty( "disableDimCmdServer",        m_disableDimCmdServer = 0 );

  declareProperty( "disableMonObjectsForBool",   m_disableMonObjectsForBool = 1 );
  declareProperty( "disableMonObjectsForInt",    m_disableMonObjectsForInt = 0 );
  declareProperty( "disableMonObjectsForLong",   m_disableMonObjectsForLong = 0 );
  declareProperty( "disableMonObjectsForDouble", m_disableMonObjectsForDouble = 0 );
  declareProperty( "disableMonObjectsForString", m_disableMonObjectsForString = 1 );
  declareProperty( "disableMonObjectsForPairs",  m_disableMonObjectsForPairs = 1 );
  declareProperty( "disableMonObjectsForHistos", m_disableMonObjectsForHistos = 0 );

  declareProperty( "disableDeclareInfoBool",     m_disableDeclareInfoBool = 1 );
  declareProperty( "disableDeclareInfoInt",      m_disableDeclareInfoInt = 0 );
  declareProperty( "disableDeclareInfoLong",     m_disableDeclareInfoLong = 0 );
  declareProperty( "disableDeclareInfoDouble",   m_disableDeclareInfoDouble = 0 );
  declareProperty( "disableDeclareInfoString",   m_disableDeclareInfoString = 1 );
  declareProperty( "disableDeclareInfoPair",     m_disableDeclareInfoPair = 1 );
  declareProperty( "disableDeclareInfoFormat",   m_disableDeclareInfoFormat = 0 );
  declareProperty( "disableDeclareInfoHistos",   m_disableDeclareInfoHistos = 0 );
  declareProperty( "maxNumCountersMonRate",      m_maxNumCountersMonRate = 1000 );
  declareProperty( "DimUpdateInterval",          m_updateInterval = 40 );
  declareProperty( "CounterUpdateInterval",      m_CounterInterval = 0 );
  declareProperty( "ExpandCounterServices",      m_expandCounterServices = 0 );
  declareProperty( "ExpandNameInfix",            m_expandInfix = "" );
  declareProperty( "PartitionName",              m_partname = "LHCb" );
  declareProperty( "ProcessName",                m_procName = "" );
  declareProperty( "ProgramName",                m_progName = "" );
  declareProperty( "DontResetCountersonRunChange", m_DontResetCountersonRunChange = false );
  declareProperty( "UseDStoreNames",             m_useDStoreNames = false );
  declareProperty( "CounterClasses",             m_CounterClasses );
  declareProperty( "HistogramClasses",           m_HistogramClasses );
  declareProperty( "HaveRates",                  m_haveRates = true );
  declareProperty( "RunAware",                   m_runAware = false );
  declareProperty( "HistUpdateOnStop",           m_histUpdateOnStop = true );
  declareProperty( "cycleTimeService",           m_cycleTimeService = "" );
  TH1D::SetDefaultSumw2();
  TH2D::SetDefaultSumw2();
  TProfile::SetDefaultSumw2();
}

MonitorSvc::~MonitorSvc() { m_MonIntf.reset(); }

// Query interfaces of Interface
// @param riid       ID of Interface to be retrieved
// @param ppvUnknown Pointer to Location for interface pointer
StatusCode MonitorSvc::queryInterface( const InterfaceID& riid, void** ppvIF ) {
  if ( IMonitorSvc::interfaceID().versionMatch( riid ) )
    *ppvIF = (IMonitorSvc*)this;
  else if ( IGauchoMonitorSvc::interfaceID().versionMatch( riid ) )
    *ppvIF = (IGauchoMonitorSvc*)this;
  else if ( IIncidentListener::interfaceID().versionMatch( riid ) )
    *ppvIF = (IIncidentListener*)this;
  else if ( IUpdateableIF::interfaceID().versionMatch( riid ) )
    *ppvIF = (IUpdateableIF*)this;
  else
    return Service::queryInterface( riid, ppvIF );
  addRef();
  return StatusCode::SUCCESS;
}

StatusCode MonitorSvc::initialize() {
  StatusCode sc = Service::initialize();
  if ( m_procName == "" ) { m_procName = RTL::processName(); }
  if ( m_expandCounterServices ) {
    m_expandInfix = RTL::str_replace( m_expandInfix, "<part>", m_partname );
    m_expandInfix = RTL::str_replace( m_expandInfix, "<proc>", m_procName );
    m_expandInfix = RTL::str_replace( m_expandInfix, "<program>", m_progName );
  }
  m_cycleTimeService = RTL::str_replace( m_cycleTimeService, "<part>", m_partname );
  m_cycleTimeService = RTL::str_replace( m_cycleTimeService, "<proc>", m_procName );
  m_cycleTimeService = RTL::str_replace( m_cycleTimeService, "<program>", m_progName );
  if ( m_CounterInterval == 0 ) m_CounterInterval = m_updateInterval;

  if ( 0 != m_disableMonRate ) { debug() << "MonRate process is disabled." << endmsg; }
  m_incidentSvc = service( "IncidentSvc", true );
  if ( !m_incidentSvc.get() ) {
    fatal() << "Service [IncidentSvc] not found" << endmsg;
    return StatusCode::FAILURE;
  }
  m_incidentSvc->addListener( this, "APP_RUNNING" );
  m_incidentSvc->addListener( this, "APP_STOPPED" );
  Online::SubSysParams cpars;
  cpars.ratePrefix   = "R_";
  cpars.dontclear    = m_DontResetCountersonRunChange;
  cpars.expandInfix  = m_expandInfix;
  cpars.expandnames  = m_expandCounterServices;
  cpars.updatePeriod = m_CounterInterval;
  cpars.type         = Online::MONSUBSYS_Counter;
  cpars.runAware     = m_runAware;
  Online::SubSysParams hpars;
  hpars.ratePrefix   = "R_";
  hpars.dontclear    = m_DontResetCountersonRunChange;
  hpars.updatePeriod = m_updateInterval;
  hpars.type         = Online::MONSUBSYS_Histogram;
  hpars.runAware     = m_runAware;
  hpars.updateOnStop = m_histUpdateOnStop;
  m_MonIntf.reset( new Online::MonitorInterface( this->m_utgid, std::move( cpars ), std::move( hpars ) ) );
  m_MonIntf->enableRates( m_haveRates );
  m_MonIntf->applyCounterClasses( m_CounterClasses );
  m_MonIntf->applyHistogramClasses( m_HistogramClasses );
  return sc;
}

/// Incident handler implemenentation: Inform that a new incident has occured
void MonitorSvc::handle( const Incident& inc ) {
  info() << "MonitorSvc: Got incident from:" << inc.source() << ": " << inc.type() << endmsg;
  if ( inc.type() == "APP_RUNNING" ) {
    i_start().ignore();
  } else if ( inc.type() == "APP_STOPPED" ) {
    i_stop().ignore();
  }
}

void MonitorSvc::undeclareInfo( const string& name, const IInterface* owner ) {
  string oname   = infoOwnerName( owner );
  string newName = oname + "/" + name;
  if ( name.find( "COUNTER_TO_RATE" ) != string::npos ) {
    newName = extract( "COUNTER_TO_RATE", name );
    newName = oname + "/" + newName;
  }
  m_MonIntf->undeclare( newName );
}

void MonitorSvc::undeclareInfo( const std::string& name )   {
  m_MonIntf->undeclare( name );
}

/** Undeclare monitoring information
    @param owner Owner identifier of the monitoring information
*/
void MonitorSvc::undeclareAll( const IInterface* owner ) {
  string oname = owner ? infoOwnerName( owner ) : "";
  m_MonIntf->undeclareAll( oname );
}

StatusCode MonitorSvc::start() {
  if ( m_errh == 0 ) {
    m_errh = new Online::MyDimErrorHandler();
    m_errh->start();
    DimServer::addErrorHandler( m_errh );
  }
  return Service::start();
}

StatusCode MonitorSvc::i_start() {
  m_i_startState = true;
  debug() << "======== MonitorSvc start() called ============= " << endmsg;
  //  setProperties();
  //  dis_set_debug_on();
  m_MonIntf->start();
  m_MonIntf->resetHistos();
  setRunNo( this->m_runno );
  m_started = true;
  return StatusCode::SUCCESS;
}

StatusCode MonitorSvc::i_stop() {
  m_i_startState = false;
  m_MonIntf->stop();
  // m_MonIntf->resetHistos();
  return StatusCode::SUCCESS;
}

void MonitorSvc::StopUpdate() {
  m_errh->stop();
  m_MonIntf->stop();
  StopSaving();
}

StatusCode MonitorSvc::finalize() {
  debug() << "MonitorSvc Finalizer" << endmsg;
  if ( m_incidentSvc.get() ) {
    m_incidentSvc->removeListener( this );
    m_incidentSvc.reset();
  }
  dim_lock();
  if ( m_started ) {
    m_started = false;
    m_MonIntf->stop();
  }
  m_MonIntf.reset();
  dim_unlock();
  debug() << "finalized successfully" << endmsg;
  return Service::finalize();
}

void MonitorSvc::setRunNo( int runno ) {
  m_runno = runno;
  m_MonIntf->setRunNo( runno );
}

template <class T>
void MonitorSvc::i_declareCounter( const string& nam, const T& var, const string& desc, const IInterface* owner ) {
  string fnam = owner ? infoOwnerName( owner ) + '/' + nam : nam;
  debug() << "MonitorSvc: declareCounter " << nam << endmsg;
  m_MonIntf->declareCounter( fnam, var, desc );
}

template <class T>
void MonitorSvc::i_declarePair( const string& nam, const T& var1, const T& var2, const string& desc,
                                const IInterface* owner ) {
  string fnam = owner ? infoOwnerName( owner ) + '/' + nam : nam;
  m_MonIntf->declarePair( fnam, var1, var2, desc );
}

template <class T>
void MonitorSvc::i_declareAtomic( const string& nam, const T& var, const string& desc, const IInterface* owner ) {
  string fnam = owner ? infoOwnerName( owner ) + '/' + nam : nam;
  m_MonIntf->declareCounter( fnam, var, desc );
}

void MonitorSvc::i_unsupported( const string& nam, const type_info& typ, const IInterface* owner ) {
  string oname    = infoOwnerName( owner );
  string typ_name = System::typeinfoName( typ );
  error() << "declareInfo(" << typ_name << ") not implemented " << oname << "/" << nam << endmsg;
}

void MonitorSvc::declareInfo( const string& name, const Entity& var, const string& desc ) {
  if ( m_disableMonObjectsForHistos == 0 ) {
    string typ = var.type;
    if ( typ.find( "histogram:" ) != typ.npos ) {
      m_MonIntf->declareHistogram( name, var, desc );
    } else if ( typ.find( "counter:" ) != string::npos ) {
      m_MonIntf->declareCounter( name, var, desc );
    }
  }
}

template <typename T>
void MonitorSvc::i_declareHistogram( const string& name, T* var, const string& desc, const IInterface* owner ) {
  if ( 0 == m_disableMonObjectsForHistos ) {
    string oname = infoOwnerName( owner );
    string hnam  = ( name.find( oname ) == string::npos ) ? oname + "/" + name : name;
    m_MonIntf->declareHistogram( hnam, var, desc );
  }
}

void MonitorSvc::declareInfo( const string& name, const int& var, const string& desc, const IInterface* owner ) {
  if ( 0 == m_disableDeclareInfoInt ) this->i_declareCounter( name, var, desc, owner );
}

void MonitorSvc::declareInfo( const string& name, const long& var, const string& desc, const IInterface* owner ) {
  if ( 0 == m_disableDeclareInfoLong ) this->i_declareCounter( name, var, desc, owner );
}

void MonitorSvc::declareInfo( const string& name, const unsigned int& var, const string& desc,
                              const IInterface* owner ) {
  if ( 0 == m_disableDeclareInfoInt ) this->i_declareCounter( name, var, desc, owner );
}

void MonitorSvc::declareInfo( const string& name, const unsigned long& var, const string& desc,
                              const IInterface* owner ) {
  if ( 0 == m_disableDeclareInfoLong ) this->i_declareCounter( name, var, desc, owner );
}

void MonitorSvc::declareInfo( const string& name, const int& var1, const int& var2, const string& desc,
                              const IInterface* owner ) {
  if ( 0 == m_disableDeclareInfoInt ) this->i_declarePair( name, var1, var2, desc, owner );
}

void MonitorSvc::declareInfo( const string& name, const long& var1, const long& var2, const string& desc,
                              const IInterface* owner ) {
  if ( 0 == m_disableDeclareInfoLong ) this->i_declarePair( name, var1, var2, desc, owner );
}

void MonitorSvc::declareInfo( const string& name, const double& var, const string& desc, const IInterface* owner ) {
  this->i_declareCounter( name, var, desc, owner );
}
void MonitorSvc::declareInfo( const string& name, const Gaudi::Accumulators::AveragingCounter<long>& var,
                              const string& desc, const IInterface* owner ) {
  this->i_declareCounter( name, var, desc, owner );
}

void MonitorSvc::declareInfo( const string& name, const Gaudi::Accumulators::AveragingCounter<double>& var,
                              const string& desc, const IInterface* owner ) {
  this->i_declareCounter( name, var, desc, owner );
}
void MonitorSvc::declareInfo( const string& name, const Gaudi::Accumulators::BinomialCounter<bool>& var,
                              const string& desc, const IInterface* owner ) {
  this->i_declareCounter( name, var, desc, owner );
}

void MonitorSvc::declareInfo( const string& name, const float& var, const string& desc, const IInterface* owner ) {
  this->i_declareCounter( name, var, desc, owner );
}

void MonitorSvc::declareInfo( const string& name, const atomic<int>& var, const string& desc,
                              const IInterface* owner ) {
  this->i_declareCounter( name, var, desc, owner );
}

void MonitorSvc::declareInfo( const string& name, const atomic<long>& var, const string& desc,
                              const IInterface* owner ) {
  this->i_declareCounter( name, var, desc, owner );
}

void MonitorSvc::declareInfo( const string& name, const atomic<float>& var, const string& desc,
                              const IInterface* owner ) {
  this->i_declareCounter( name, var, desc, owner );
}

void MonitorSvc::declareInfo( const string& name, const atomic<double>& var, const string& desc,
                              const IInterface* owner ) {
  this->i_declareCounter( name, var, desc, owner );
}

void MonitorSvc::declareInfo( const string& name, const bool& var, const string&, const IInterface* owner ) {
  this->i_unsupported( name, typeid( var ), owner );
}

void MonitorSvc::declareInfo( const string& name, const string& var, const string&, const IInterface* owner ) {
  this->i_unsupported( name, typeid( var ), owner );
}

void MonitorSvc::declareInfo( const string& name, const pair<double, double>& var, const string&,
                              const IInterface* owner ) {
  this->i_unsupported( name, typeid( var ), owner );
}

void MonitorSvc::declareInfo( const string& name, const string& format, unsigned int* const var, int size,
                              const string& desc, const IInterface* owner ) {
  string       oname = infoOwnerName( owner );
  const string fnam  = oname + "/" + name;
  m_MonIntf->declareCounter<unsigned int*>( fnam, format, var, size, desc );
}

void MonitorSvc::declareInfo( const string& name, const string& format, unsigned long* const var, int size,
                              const string& desc, const IInterface* owner ) {
  string       oname = infoOwnerName( owner );
  const string fnam  = oname + "/" + name;
  m_MonIntf->declareCounter<unsigned long*>( fnam, format, var, size, desc );
}

void MonitorSvc::declareInfo( const string& name, const string& format, const void* var, int size, const string& desc,
                              const IInterface* owner ) {
  string oname = infoOwnerName( owner );
  if ( format.empty() || ( size == 0 ) ) return;

  if ( m_started ) {
    // printf("Declare Info called after start for Name %s\n",name.c_str());
  }
  if ( 0 != m_disableDeclareInfoFormat ) return;
  const string fnam  = oname + "/" + name;
  string       nname = "R_" + oname + "/" + name;
  char         fch   = ::tolower( format[0] );
  switch ( fch ) {
  case 'x':
    m_MonIntf->declareCounter<long int*>( fnam, format, (long int*)var, size, desc );
    break;
  case 'i':
    m_MonIntf->declareCounter<int*>( fnam, format, (int*)var, size, desc );
    break;
  case 'f':
    m_MonIntf->declareCounter<float*>( fnam, format, (float*)var, size, desc );
    break;
  case 'd':
    m_MonIntf->declareCounter<double*>( fnam, format, (double*)var, size, desc );
    break;
  default:
    return;
  }
}
//
void MonitorSvc::declareInfo( const string& name, const StatEntity&, const string&, const IInterface* owner ) {
  warning() << "=========================== DeclareInfo for StatEntity Not Supported anymore " << name << "  "
            << infoOwnerName( owner ) << endmsg;
}

void MonitorSvc::declareInfo( const string& nam, const AIDA::IBaseHistogram* var, const string& desc,
                              const IInterface* owner ) {
  if ( 0 != m_disableDeclareInfoHistos ) {
    debug() << "m_disableDeclareInfoHistos DISABLED " << endmsg;
    return;
  }

  string hnam  = nam;
  string oname = infoOwnerName( owner );
  if ( m_disableMonObjectsForHistos == 0 ) {
    if ( m_useDStoreNames ) {
      AIDA::IBaseHistogram* h    = (AIDA::IBaseHistogram*)var;
      DataObject*           dObj = dynamic_cast<DataObject*>( h );
      IRegistry*            reg  = dObj->registry();
      string                path = reg->identifier(); // /stat/bla//bla/<histo-name>
      ::lib_rtl_output( LIB_RTL_INFO, "Data Store Path: %s , Hist Name: %s\n", path.c_str(), nam.c_str() );
    }
    if ( nam.find( oname ) == string::npos ) { hnam = oname + "/" + nam; }
    if ( 0 != dynamic_cast<const AIDA::IProfile1D*>( var ) ) {
      TProfile* h = Online::cast_histo<TProfile>( var );
      m_MonIntf->declareHistogram( hnam, h, desc );
    } else if ( 0 != dynamic_cast<const AIDA::IHistogram1D*>( var ) ) {
      TH1D* h = Online::cast_histo<TH1D>( var );
      m_MonIntf->declareHistogram( hnam, h, desc );
    } else if ( 0 != dynamic_cast<const AIDA::IHistogram2D*>( var ) ) {
      TH2D* h = Online::cast_histo<TH2D>( var );
      m_MonIntf->declareHistogram( hnam, h, desc );
    } else if ( 0 != dynamic_cast<const AIDA::IProfile2D*>( var ) ) {
      TProfile2D* h = Online::cast_histo<TProfile2D>( var );
      m_MonIntf->declareHistogram( hnam, h, desc );
    } else {
      error() << "Unknown histogram type (" << typeid( var ).name() << "). Source " << nam << endmsg;
    }
    return;
  } else {
    error() << "MonitorSvc doesn't have support for declaring histograms/profiles without using MonObjects " << endmsg;
  }
}

void MonitorSvc::declareMonRateComplement( int&, unsigned int&, int&, double&, double&, double&, double& ) {
  warning() << "declareMonRateComplement not supported anymore" << endmsg;
}

string MonitorSvc::extract( const string mascara, string value ) {
  if ( value.find( mascara ) != string::npos ) {
    value.erase( 0, mascara.size() + 1 );
    value.erase( value.size() - 1, 1 );
  }
  return value;
}

string MonitorSvc::infoOwnerName( const IInterface* owner ) {
  const IAlgorithm* ownerAlg = dynamic_cast<const IAlgorithm*>( owner );
  if ( 0 != ownerAlg ) // It's an algorithm
    return ownerAlg->name();
  const IAlgTool* ownerTool = dynamic_cast<const IAlgTool*>( owner );
  if ( 0 != ownerTool ) // It's a  tool
    return ownerTool->name();
  const IService* ownerSvc = dynamic_cast<const IService*>( owner );
  if ( 0 != ownerSvc ) // It's a service
    return ownerSvc->name();
  // NULL pointer or Unknown interface:
  return "";
}

StatusCode MonitorSvc::update( int runno ) {
  info() << "Monitor Service: Issuing EOR Update for Run " << runno << endmsg;
  m_MonIntf->eorUpdate( runno );
  return StatusCode::SUCCESS;
}

void MonitorSvc::updatePerSvc( unsigned long ref ) {
  m_MonIntf->update(ref);
}

void MonitorSvc::resetHistos( const IInterface* ) {
  m_MonIntf->resetHistos();
}

void MonitorSvc::Lock( void ) {
  m_MonIntf->lock();
}

void MonitorSvc::UnLock( void ) {
  m_MonIntf->unlock();
}

void MonitorSvc::StartSaving(std::shared_ptr<Online::TaskSaveTimer>& timer) {
  if ( timer && !m_cycleTimeService.empty() )  {
    timer->publishTimeouts(m_cycleTimeService);
  }
  m_MonIntf->startSaving(timer);
}

void MonitorSvc::StopSaving() {
  if ( m_MonIntf != 0 ) { m_MonIntf->stopSaving(); }
}
