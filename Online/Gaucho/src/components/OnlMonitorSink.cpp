/***********************************************************************************\
* (c) Copyright 1998-2022 CERN for the benefit of the LHCb and ATLAS collaborations *
 *                                                                                   *
 * This software is distributed under the terms of the Apache version 2 licence,     *
 * copied verbatim in the file "LICENSE".                                            *
 *                                                                                   *
 * In applying this licence, CERN does not waive the privileges and immunities       *
 * granted to it by virtue of its status as an Intergovernmental Organization        *
 * or submit itself to any jurisdiction.                                             *
 \***********************************************************************************/

#include "OnlMonitorSink.h"

#include <RTL/strdef.h>

#include <Gaucho/IGauchoMonitorSvc.h>
#include <Gaudi/Accumulators.h>
#include <Gaudi/Accumulators/Histogram.h>
#include <Gaudi/Histograming/Sink/Base.h>
#include <Gaudi/Histograming/Sink/Utils.h>
#include <GaudiKernel/Service.h>

#include <TFile.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TH3D.h>
#include <TProfile.h>
#include <TProfile2D.h>
#include <TProfile3D.h>

#include <algorithm>
#include <nlohmann/json.hpp>
#include <regex>

using namespace std::string_literals;
using namespace Gaudi::Accumulators;
using namespace Gaudi::Monitoring;

namespace Gaudi::Parsers {
  StatusCode parse( std::vector<std::pair<std::string, std::string>>& result, const std::string& input );
}

StatusCode Gaudi::Parsers::parse( std::vector<std::pair<std::string, std::string>>& result, const std::string& input ) {
  return Gaudi::Parsers::parse_( result, input );
}

namespace {
  std::ostream& operator<<( std::ostream& os, const Hub::Entity& ent ) {
    return os << "Entity(component='" << ent.component << "', name='" << ent.name << "', type='" << ent.type
              << "', id=" << ent.id() << ")";
  }

  template <typename T>
  std::string to_string( const T& streamable ) {
    std::ostringstream oss;
    oss << streamable;
    return oss.str();
  }
} // namespace

namespace {
  using namespace std::string_literals;
  using namespace Gaudi::Histograming::Sink;
  Base::HistoRegistry const registry = {
      {{"histogram:Histogram"s, 1}, &saveRootHisto<1, false, TH1D>},
      {{"histogram:WeightedHistogram"s, 1}, &saveRootHisto<1, false, TH1D>},
      {{"histogram:Histogram"s, 2}, &saveRootHisto<2, false, TH2D>},
      {{"histogram:WeightedHistogram"s, 2}, &saveRootHisto<2, false, TH2D>},
      {{"histogram:Histogram"s, 3}, &saveRootHisto<3, false, TH3D>},
      {{"histogram:WeightedHistogram"s, 3}, &saveRootHisto<3, false, TH3D>},
      {{"histogram:ProfileHistogram"s, 1}, &saveRootHisto<1, true, TProfile>},
      {{"histogram:WeightedProfileHistogram"s, 1}, &saveRootHisto<1, true, TProfile>},
      {{"histogram:ProfileHistogram"s, 2}, &saveRootHisto<2, true, TProfile2D>},
      {{"histogram:WeightedProfileHistogram"s, 2}, &saveRootHisto<2, true, TProfile2D>},
      {{"histogram:ProfileHistogram"s, 3}, &saveRootHisto<3, true, TProfile3D>},
      {{"histogram:WeightedProfileHistogram"s, 3}, &saveRootHisto<3, true, TProfile3D>}};

  std::string publishedName( Hub::Entity const& ent ) {
    // This should match the logic in Gaudi's jsonToRootHistogramInternal (called by saveRootHisto)
    // https://gitlab.cern.ch/gaudi/Gaudi/-/blob/1078fb00f896d1da3e310d01db9a90bb6a209076/GaudiKernel/include/Gaudi/Histograming/Sink/Utils.h#L125-128
    std::string nam = ( ent.name[0] != '/' ) ? ( ent.component + "/" + ent.name ) : ent.name.substr( 1 );
    return RTL::str_replace(nam, "//", "/");
  }
} // namespace

StatusCode OnlMonitorSink::initialize() {
  StatusCode sc = Service::initialize();
  if ( !sc.isSuccess() ) {
    error() << "Failed to initialize the base class." << endmsg;
    return sc;
  }
  m_mons = serviceLocator()->service( "MonitorSvc", false );
  if ( !m_mons ) {
    error() << "Failed to access the MonitorSvc" << endmsg;
    return StatusCode::FAILURE;
  }
  serviceLocator()->monitoringHub().addSink( this );
  return sc;
}

StatusCode OnlMonitorSink::stop() { return Service::stop(); }

StatusCode OnlMonitorSink::finalize() {
  if ( m_mons ) {
    m_mons->undeclareAll( nullptr );
    m_mons->release();
    m_mons.reset();
  }
  return Service::finalize();
}

void OnlMonitorSink::registerEntity( Hub::Entity ent ) {
  std::vector<std::string> type      = RTL::str_split( ent.type, ":" );
  bool                     supported = false;
  bool                     matches   = false;

  if ( type[0] == "counter" ) {
    if ( type[1] == "BinomialCounter" || type[1] == "AveragingCounter" || type[1] == "StatCounter" ||
         type[1] == "Counter" || type[1] == "SigmaCounter" ) {
      if ( "dfchstijlm"s.find( type[2][0] ) != std::string::npos ) supported = true;
    } else if ( type[1] == "MsgCounter" ) {
      supported = true;
    }

    matches = m_CountersToPublish.empty();
    for ( const auto& p : m_CountersToPublish ) {
      std::regex regex_component( p.first );
      std::regex regex_name( p.second );
      if ( std::regex_match( ent.component, regex_component ) && std::regex_match( ent.name, regex_name ) ) {
        matches = true;
        break;
      }
    }
  } else if ( type[0] == "histogram" ) {
    supported = true;
    matches   = m_HistogramsToPublish.empty();
    for ( const auto& p : m_HistogramsToPublish ) {
      std::regex regex_component( p.first );
      std::regex regex_name( p.second );
      if ( std::regex_match( ent.component, regex_component ) && std::regex_match( ent.name, regex_name ) ) {
        matches = true;
        break;
      }
    }
  }

  if ( matches ) {
    if ( !supported ) {
      error() << to_string( ent ) << " has an unsupported type" << endmsg;
    } else {
      auto it = std::find_if( m_monitoringEntities.begin(), m_monitoringEntities.end(),
                              [&ent]( auto& x ) { return x.second == std::as_const(ent); } );
      if ( it != m_monitoringEntities.end() ) {
        throw GaudiException( "Called registerEntity() twice with an identical " + to_string( ent ),
                              __PRETTY_FUNCTION__, StatusCode::FAILURE );
      }
      auto name = publishedName( ent );
      if ( auto ite = m_monitoringEntities.find( name ); ite != m_monitoringEntities.end() ) {
        error() << "Skipping " << to_string( ent ) << " as it collides with the already registered "
                << to_string( ite->second ) << ". Both would be published under '" << name << "'" << endmsg;
      } else {
        m_mons->declareInfo( name, ent, "" );
        m_monitoringEntities.emplace( name, std::move( ent ) );
      }
    }
  }
}

void OnlMonitorSink::removeEntity( Hub::Entity const& ent ) {
  if ( m_mons ) { m_mons->undeclareInfo( ent.component + "/" + ent.name ); }
  auto it = std::find_if( m_monitoringEntities.begin(), m_monitoringEntities.begin(),
                          [&ent]( auto & x ) { return x.second == ent; } );
  if ( it != m_monitoringEntities.end() ) { m_monitoringEntities.erase( it ); }
}

void OnlMonitorSink::resetEntities() {
  for ( auto& [name, entity] : m_monitoringEntities ) { reset( entity ); }
}

void OnlMonitorSink::save( std::string const& filename, bool doreset ) {
  TFile histoFile( filename.c_str(), "RECREATE" );
  for ( auto& [name, entity] : m_monitoringEntities ) {
    if ( entity.type.find( "histogram" ) != std::string::npos ) {
      nlohmann::json j    = entity;
      auto           type = j.at( "type" ).template get<std::string>();
      auto           dim  = j.at( "dimension" ).template get<unsigned int>();
      type                = type.substr( 0, type.find_last_of( ':' ) );
      auto saver          = registry.find( {type, dim} );
      if ( saver != registry.end() ) ( saver->second )( histoFile, entity.component, entity.name, j );
    }
    if ( doreset ) reset( entity );
  }
}

DECLARE_COMPONENT( OnlMonitorSink )
