//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================

// Framework include files
#include <GaudiKernel/MsgStream.h>
#include "PublishSvc.h"

using namespace LHCb;

// Factory for instantiation of service objects
DECLARE_COMPONENT( LHCb::PublishSvc )

PublishSvc::PublishSvc(const std::string& name, ISvcLocator* svcLoc)
: Service(name, svcLoc), m_started(false)
{
  declareProperty("ServicePrefix",m_Prefix="/Publisher/");
}

StatusCode PublishSvc::queryInterface(const InterfaceID& riid, void** ppvIF) {
  if ( IPublishSvc::interfaceID() == riid )
    *ppvIF = dynamic_cast<IPublishSvc*> (this);
  else if ( IPublishSvcArr::interfaceID() == riid )
    *ppvIF = dynamic_cast<IPublishSvcArr*> (this);
  else
    return this->Service::queryInterface(riid, ppvIF);
  addRef();
  return StatusCode::SUCCESS;
}

StatusCode PublishSvc::initialize() {
  StatusCode sc = Service::initialize();
  MsgStream msg(msgSvc(),name());
  if ( !sc.isSuccess() )  {
    msg << MSG::ERROR << "Cannot initialize base class." << endmsg;
    return sc;
  }
  undeclarePubAll();
  msg << MSG::INFO << " MonitorSvc of type PublishSvc initialized." << endmsg;
  return sc;
}

StatusCode PublishSvc::finalize() {
  undeclarePubAll();
  return Service::finalize();
}

StatusCode PublishSvc::start()  {
  DimServer::autoStartOn();
  for (auto& it : this->m_items )  {
    if ( !it.second->m_srvc )    {
      it.second->make_service();
    }
  }
  m_started = true;
  return StatusCode::SUCCESS;
}

void PublishSvc::regItem(CSTR item_name,const void* ptr, IType typ)   {
  ItemMap::iterator i = m_items.find(item_name);
  if ( i == m_items.end() )  {
    ITEM *item = new ITEM();
    item->item_name = item_name;
    item->SrvName = m_Prefix+item_name;
    item->typ = typ;
    item->m_ptr = (void*)ptr;
    if (m_started)    {
      item->make_service();
    }
    m_items.insert(make_pair(item_name,item));
  }
}

void PublishSvc::regItem(CSTR item_name,const void* ptr, int s, IType typ)   {
  if ( m_items.find(item_name) == m_items.end() )  {
    ITEM *item = new ITEM();
    item->item_name = item_name;
    item->SrvName = m_Prefix+item_name;
    item->typ = typ;
    item->m_ptr = (void*)ptr;
    item->siz = s;
    if (m_started)    {
      item->make_service();
    }
    m_items.insert(make_pair(item_name,item));
  }
}

void PublishSvc::undeclarePubItem(CSTR nam)  {
  ItemMap::iterator it = m_items.find(nam);
  if ( it != m_items.end() )  {
    if (it->second->m_srvc != 0) delete (it->second->m_srvc);
    delete (it->second);
  }
  m_items.erase(it);
}

void PublishSvc::undeclarePubAll()   {
  for ( auto& i : m_items )  {
    delete i.second->m_srvc;
    delete i.second;
  }
  m_items.clear();
}

void PublishSvc::updateItem(const std::string &name)   {
  ItemMap::iterator i = m_items.find(name);
  if ( i != m_items.end() )  {
    ITEM* it = i->second;
    switch (it->typ)    {
    case IT_Bool:
    case IT_Long:
    case IT_Double:
    case IT_Int:
      it->m_srvc->updateService();
      break;
    case IT_String:      {
      std::string s = *(std::string*)it->m_ptr +'\0';
      it->m_srvc->updateService((char*)((std::string*)(it->m_ptr))->c_str());
      break;
    }
    case IT_LongStar:
    case IT_DoubleStar:
    case IT_FloatStar:
    case IT_IntStar:
      it->m_srvc->updateService(it->m_ptr,it->siz);
      break;
    default :
      break;
    }
  }
}

void PublishSvc::updateAll()  {
  for (auto& i : m_items )
    updateItem(i.second->item_name);
}
