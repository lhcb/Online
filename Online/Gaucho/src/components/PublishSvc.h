//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
/*
 * PublishSvc.h
 *
 *  Created on: May 2, 2014
 *      Author: Beat Jost
 */

#ifndef PUBLISHSVC_H_
#define PUBLISHSVC_H_
#include <GaudiKernel/Service.h>
#include <GaudiKernel/IPublishSvc.h>
#include <Gaucho/IPublishSvcArr.h>
#include <dim/dis.hxx>

#include <map>

namespace LHCb  {

  /** @class PublishSvc PublishSvc.h GaudiOnline/PublishSvc.h
    *
    *  This class implements the IMonitorSvc interface, and publishes Gaudi variables
    *  into a memory area.
    *
    *  @author Markus Frank
    */
  typedef enum
  {
    IT_None = 0,
    IT_Bool,
    IT_Int,
    IT_Long,
    IT_Double,
    IT_String,
    IT_Float,
    IT_DoubleStar,
    IT_IntStar,
    IT_LongStar,
    IT_FloatStar
  }IType;

  class PublishSvc : public Service,
    /*virtual public IPublishSvc,*/ virtual public IPublishSvcArr
  {
    typedef const std::string& CSTR;
    class ITEM
    {
      public:
      std::string item_name;
      void *m_ptr;
      IType typ;
      DimService *m_srvc;
      std::string SrvName;
      int siz;
      ITEM() :item_name(""),m_ptr(0),typ(IT_None),m_srvc(0), SrvName("") {}
      ITEM(std::string &nam, void *ptr, IType t) : item_name(nam),m_ptr(ptr),typ(t),m_srvc(0), SrvName("") {};
      void make_service()
      {
        switch(typ)
        {
          case IT_Bool:
          case IT_Int:
          {
            m_srvc = new DimService(SrvName.c_str(),*((int*)m_ptr));
            break;
          }
          case IT_Long:
          {
            m_srvc = new DimService(SrvName.c_str(),*((long long *)m_ptr));
            break;
          }
          case IT_String:
          {
            m_srvc = new DimService(SrvName.c_str(),(char*)"");
            break;
          }
          case IT_Float:
          {
            m_srvc = new DimService(SrvName.c_str(),*((float*)m_ptr));
            break;
          }
          case IT_Double:
          {
            m_srvc = new DimService(SrvName.c_str(),*((double*)m_ptr));
            break;
          }
          case IT_DoubleStar:
          {
            m_srvc = new DimService(SrvName.c_str(),"D",m_ptr,siz);
            break;
          }
          case IT_IntStar:
          {
            m_srvc = new DimService(SrvName.c_str(),"I",m_ptr,siz);
            break;
          }
          case IT_LongStar:
          {
            m_srvc = new DimService(SrvName.c_str(),"L",m_ptr,siz);
            break;
          }
          case IT_FloatStar:
          {
            m_srvc = new DimService(SrvName.c_str(),"F",m_ptr,siz);
            break;
          }

          case IT_None:
          {

          }
          break;
        };
      };

    };
    class Items : public std::map<std::string,ITEM*>
    {
      public:
      Items() : std::map<std::string,ITEM*>() {}
      Items(const Items& c) : std::map<std::string,ITEM*>(c) {}
      ITEM* getItem(std::string n){map<std::string,ITEM*>::iterator it =find(n);return (it==end()) ? 0: it->second;}
    };
    typedef std::map<std::string,ITEM*> ItemMap;
//    typedef std::pair<ClientMap::iterator,Items::iterator> PubInfo;

    /// Internal helper to declare monitoring items
    void regItem(CSTR item_name,const void* ptr, IType typ);
    void regItem(CSTR item_name,const void* ptr, int siz, IType typ);

  public:
    /// Service constructor
    PublishSvc(CSTR name, ISvcLocator* svcLoc);
    /// Standard destructor
    virtual ~PublishSvc() {}
    /// IInterface pure virtual member functions
    virtual StatusCode queryInterface(const InterfaceID& riid, void** ppvIF)  override;
    /// Initialize service
    virtual StatusCode initialize()  override;
    /// Finalize service
    virtual StatusCode finalize()  override;
    virtual StatusCode start()  override;
    std::string m_Prefix;
#define _DECL(t,typ,ovr)  virtual void declarePubItem(CSTR n,t& v)   ovr  {regItem(n,&v,typ); }
#define _DECLStar(t,typ)  virtual void declarePubItem(CSTR n,t v, int siz)   override  {regItem(n,v,siz,typ); }
//#define _DECLP(t) virtual void declarePubItem(CSTR n,t  v,CSTR d,Client c)   override  {regItem(n,v,d,c);  }
    typedef std::pair<double,double> Rate;
    _DECL(const bool,IT_Bool,override)
      _DECL(const int,IT_Int,override)
      _DECL(const long,IT_Long,override)
      _DECL(const double,IT_Double,override)
      _DECL(const std::string,IT_String,override)
      _DECL(bool,IT_Bool,)
      _DECL(int,IT_Int,)
      _DECL(long,IT_Long,)
      _DECL(double,IT_Double,)
      _DECL(float,IT_Float,)
      _DECL(std::string,IT_String,)
      _DECLStar(const int *,IT_IntStar)
      _DECLStar(int *,IT_IntStar)
      _DECLStar(const double *,IT_DoubleStar)
      _DECLStar(double *,IT_DoubleStar)
      _DECLStar(const long *,IT_LongStar)
      _DECLStar(long *,IT_LongStar)
      _DECLStar(const float *,IT_FloatStar)
      _DECLStar(float *,IT_FloatStar)

    /** Undeclare monitoring information
      * @param name Monitoring information name knwon to the external system
      * @param owner Owner identifier of the monitoring information
      */
    virtual void undeclarePubItem(CSTR nam)  override;

    /** Undeclare monitoring information
      * @param owner Owner identifier of the monitoring information
      */
    virtual void undeclarePubAll()   override;

    /** Get the names for all declared monitoring informations for a given
      * owner. If the owner is NULL, then it returns for all owners
      */
    virtual void updateItem(const std::string &name)  override;
    virtual void updateAll()  override;
    bool m_started;
  private:
    /// Map associating to each algorithm name a set with the info items
    ItemMap         m_items;
  };
}      // End namespace gaudi



#endif /* PUBLISHSVC_H_ */
