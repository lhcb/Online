//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef GAUCHO_UPDATEANDRESET_H
#define GAUCHO_UPDATEANDRESET_H 1

#include "OnlMonitorSink.h"

// Include files
#include <Gaudi/Algorithm.h>
#include <GaudiKernel/IRegistry.h>
#include <GaudiKernel/DataObjectHandle.h>
#include <Gaucho/TaskSaveTimer.h>
#include <Gaucho/IGauchoMonitorSvc.h>

#include <mutex>
#include <memory>

/// Forward declarations
class TDirectory;
class DimService;
namespace LHCb  {  class RawBank;  }

///  Online namespace declaration
namespace Online   {

  class UpdateAndReset : public Gaudi::Algorithm   {
  public:
    /// Constructor of this form must be provided
    UpdateAndReset(const std::string& name, ISvcLocator* pSvcLocator);

    StatusCode execute(EventContext const& ctxt) const override final;
    StatusCode finalize() override;
    StatusCode testeExecute();
    StatusCode start() override;
    StatusCode stop() override;
    StatusCode restart() override;

    StatusCode do_execute(int run_number);

  private:
    std::string makeFileName();
    void manageTESHistos(bool list, bool reset, bool save, bool isFromEndOfRun);
    void histogramIdentifier(const IRegistry* object,
			     std::vector<std::string> &idList,
			     bool reset, bool save, int &level,
			     TDirectory* rootdir);
    IRegistry* rootObject();

  private:
    SmartIF<IGauchoMonitorSvc> m_gauchoMonitorSvc; ///< Online Gaucho Monitoring Service
    ServiceHandle<IOnlMonitorSink> m_sinkSvc{this, "MonitorSink", "OnlMonitorSink", "Service for accessing monitoring entities to save"};

    typedef std::vector<std::pair<const LHCb::RawBank*, const void*> > lb_evt_data_t;
    DataObjectReadHandle<lb_evt_data_t> m_rawData{this,"RawData","Banks/RawData"};

    std::shared_ptr<TaskSaveTimer> m_saveTimer;
    std::shared_ptr<DimService>    m_dimSvcSaveSetLoc;
    std::mutex  m_mutex;
    std::string m_taskName;
    std::string m_partName;
    std::string m_cycleStatus;
    std::string m_runStatus;
    std::string m_infoFileStatus;

    // MonRate information
    int         m_one;
    int         m_runNumber;
    int         m_eorNumber;

    /// Properties:
    std::string m_MyName;
    std::string m_saveSetDir;
    std::string m_saveSetFilePrefix;
    int         m_desiredDeltaTCycle;// integer because dimTimer can only accept seconds
    int         m_disableReadOdin;
    int         m_saveHistograms;
    int         m_saverCycle;
    int         m_resetHistosAfterSave;
    bool        m_publishSavesetLoc;
    bool        m_resetOnStart;
  };
}
#endif  // GAUCHO_UPDATEANDRESET_H
