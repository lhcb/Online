//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#pragma once
#include <Gaucho/Serializer.h>
#include <CPP/mem_buff.h>

#include <map>

/// Online namespace declaration
namespace Online   {

  class BRTLLock;
  class DimBuffBase;

  class AddSerializer : public Serializer    {
    mem_buff                              buffer  { };
    std::map<std::string, DimBuffBase* >& objects;
    BRTLLock&                             maplock;
  public:
    typedef std::map<std::string, DimBuffBase*> MonitorItems;

  public:
    AddSerializer() = delete;
    AddSerializer(MonitorItems& objects, BRTLLock& lock);
    virtual ~AddSerializer() = default;

    /// Serialize object set to local buffer
    virtual std::pair<size_t,void*> serialize_obj(size_t siz, bool clear=false) override;
    /// Serialize object set to external buffer
    virtual std::pair<size_t,void*> serialize_obj(mem_buff& buff, size_t offset, bool clear=false)  const  override;
    /// Serialize a set of objects identified by name to local buffer
    virtual std::pair<size_t,void*> serialize_match(const std::string &match, size_t offset, bool clear=false)  override;
    /// Serialize a set of objects identified by match to name to local buffer
    virtual std::pair<size_t,void*> serialize_obj(const std::vector<std::string> &nams, size_t siz, bool clear=false)    override;
    /// Serialize directory (inventory of objects identified by name to local buffer
    virtual std::pair<size_t,void*> serialize_dir(size_t siz)   override;
    /// Access to the internal buffer
    virtual std::pair<size_t,const void*> data()   const   override;
    /// Update expansions
    virtual void updateExpansions()   override;
  };
}
