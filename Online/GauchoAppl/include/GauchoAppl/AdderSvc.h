//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef ONLINE_GAUCHO_ADDERSVC_H
#define ONLINE_GAUCHO_ADDERSVC_H

#include <GauchoAppl/MonAdder.h>
#include <GaudiKernel/Service.h>
#include <GaudiKernel/IMonitorSvc.h>
#include <GaudiKernel/IIncidentListener.h>

/// C/C++ include files
#include <memory>
#include <thread>

// Forward declarations
class DimService;
class DimServerDns;
class IIncidentSvc;

namespace Online   {
  class mem_buff;
  class MonAdder;
  class SaveTimer;
  class MyDimErrorHandler;
}

class AdderSvc : public Service, virtual public IIncidentListener    {
public:
  AdderSvc(const std::string& name, ISvcLocator* sl);
  virtual ~AdderSvc();
  StatusCode queryInterface(const InterfaceID& riid, void** ppvIF) override;
  StatusCode initialize() override;
  StatusCode start() override;
  StatusCode stop() override;
  StatusCode finalize() override;

  void handle(const Incident& incident) override;

  void gotoPause();
  StatusCode i_start();
  StatusCode i_stop();

  StatusCode setup_adder(std::shared_ptr<Online::MonAdder>& adder,
			 std::shared_ptr<DimServerDns>& dns,
			 bool is_eor);
  void setup_saver(std::unique_ptr<Online::SaveTimer>& saver,
		   std::shared_ptr<Online::MonAdder>& adder,
		   bool is_eor);

  void handleCycle(Online::mem_buff& buffer, Online::MonitorItems* items);

  /// Reference to the IncidentSvc instance
  SmartIF<IIncidentSvc>       incidentSvc;
  /// Reference to the MonitorSvc instance
  SmartIF<IMonitorSvc>        monitorSvc;

  std::unique_ptr<std::thread>       idle_thread;
  std::shared_ptr<Online::MonAdder>  intervalAdder;
  std::unique_ptr<Online::SaveTimer> intervalSaver;
  std::shared_ptr<Online::MonAdder>  eorAdder;
  std::unique_ptr<Online::SaveTimer> eorSaver;
  std::shared_ptr<DimService>        savesetSvc;
  Online::MyDimErrorHandler         *m_errh   {nullptr};

  /// Properties
  std::string m_inputDNS;
  std::string m_outputDNS;
  std::string m_saveRootDir;
  std::string m_partitionName;
  std::string m_saverTaskName;
  std::string m_myName;
  std::string m_taskPattern;
  std::string m_servicePattern;
  std::string m_adderClass;
  std::string m_myServiceName;
  std::string m_infoFileStatus;

  int  m_saveInterval  { -1 }; //in seconds
  int  m_serviceDelay  { -1 }; //in seconds
  int  m_eorTmoFactor;
  int  m_recvtmo          { false };
  int  m_haveTimer        { 0 };
  bool m_isSaver          { false };
  bool m_started          { false };
  bool m_stopping         { false }; // flag for idle_thread
  bool m_doPause          { false };
  bool m_pauseOnEOR       { false };
  bool m_debugOn          { false };
  bool m_debugSys         { false };
  bool m_runAware         { false };
  bool m_trackSources     { false };
};
#endif // ONLINE_GAUCHO_ADDERSVC_H
