//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef ONLINE_GAUCHOAPPL_GAUCHOAPPL_CLASSDESCRIPTOR_H_
#define ONLINE_GAUCHOAPPL_GAUCHOAPPL_CLASSDESCRIPTOR_H_

/// Framework include files
#include <Gaucho/MonInfo.h>
#include <Gaucho/SerialHeader.h>
#include <CPP/mem_buff.h>

/// C/C++ include files
#include <map>

/// Forward declarations
class DimServerDns;

/// Online namespace declaration
namespace Online  {

  class MonInfo;
  class MonAdder;
  class ObjService;
  class TimeoutCmd;
  class DimBuffBase;
  class INServiceDescr;

  class TimeoutCmd   {
    MonAdder    *adder   { nullptr };
    std::string service;
    int         command_id { 0 };
  public:
    TimeoutCmd(DimServerDns *dns, const std::string& nam, const std::string& svc, MonAdder *tis);
    TimeoutCmd(const std::string& nam, const std::string& svc, MonAdder *tis);
    void commandHandler();
  };

  class INServiceDescr   {
  public:
    std::string              task;
    std::string              service;
    std::unique_ptr<MonInfo> info;
    mem_buff                 buffer;
    long long                last_update {-1 };
    int                      pid         {-1 };
    int                      timeouts    { 0 };
    size_t                   fragsize    { 0 };

  public:
  INServiceDescr(const std::string& tsk, const std::string& svc, std::unique_ptr<MonInfo>&& inf) 
    : task(tsk), service(svc), info(std::move(inf))
    {
    }
    virtual ~INServiceDescr();
  };

  class ClassDescriptor : public MonInfoHandler   {
    typedef std::map<std::string, DimBuffBase*> MonitorItems;
    typedef std::map<std::string, std::unique_ptr<INServiceDescr> > INServiceMap;

  private:
    void* allocate(size_t siz);
    void* reallocate(size_t siz);

  public:
    ClassDescriptor() = delete;
    ClassDescriptor(ClassDescriptor&& copy) = delete;
    ClassDescriptor(const ClassDescriptor& copy) = delete;
    ClassDescriptor& operator = (ClassDescriptor&& copy) = delete;
    ClassDescriptor& operator = (const ClassDescriptor& copy) = delete;

    ClassDescriptor(const std::string& name, MonAdder* adder);
    virtual ~ClassDescriptor();

    bool  addService(const std::string& task, const std::string& service);
    void  adoptItems(const void* buffer, size_t length);
    void  addItem(const std::string& name, DimBuffBase* data);
    void  enableOutput(std::shared_ptr<DimServerDns>& dns, const std::string& svc, unsigned int run);

    /// Overloaded handler of the MonInfoHandler
    virtual void operator()(void* data, int size, MonInfo* info)  override;
    std::size_t expected()  const     {
      return this->inputServices.size();
    }

    std::string                     name;
    std::string                     firstSource;
    std::shared_ptr<mem_buff>       buffer;
    std::unique_ptr<ObjService>     outputservice;
    std::unique_ptr<TimeoutCmd>     tmo_command;
    INServiceMap                    inputServices;
    MonitorItems                    monitorItems;
    MonAdder*                       adder        { nullptr };

    long long                       updateStamp  { 0 };
    long long                       reference    { 0 };
    unsigned long long              time0        { 0 };
    size_t                          received     { 0 };
    bool                            updated      { 0 };
    bool                            timeout      { 0 };
    bool                            has_default { false };
  };
}
#endif /* ONLINE_GAUCHOAPPL_GAUCHOAPPL_CLASSDESCRIPTOR_H_ */
