//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef ONLINE_GAUCHO_MonAdder_H
#define ONLINE_GAUCHO_MonAdder_H

#include <Gaucho/dimhist.h>
#include <Gaucho/MonTypes.h>
#include <Gaucho/MonLockable.h>
#include <GauchoAppl/ClassDescriptor.h>

#include <map>
#include <set>
#include <string>
#include <memory>
#include <vector>
#include <regex>

class DimInfo;
class DimServerDns;

/// Online namespace declaration
namespace Online   {
  class mem_buff;
  class HistServer;
  class ObjRPC;
  class MonInfo;
  class ObjService;
  class RunManager;
  class DimBuffBase;
  class ClassDescriptor;

  class TimeoutCmd;
  class INServiceDescr;
  typedef std::map<std::string, DimBuffBase*>                      MonitorItems;
  typedef std::map<std::string, std::shared_ptr<ClassDescriptor> > ClassMap;

  enum adder_type_t  {  ADD_UNKNOWN = 0, ADD_HISTO = 1, ADD_COUNTER = 2 };
  
  class MonAdder : public MonLockable {
    friend class TimeoutCmd;
    friend class ClassDescriptor;

  public:
    typedef std::map<std::string, std::unique_ptr<INServiceDescr> >  INServiceMap;

    enum { SERVICE_NO_MATCH = 0,
	   SERVICE_PRESENT  = 1,
	   SERVICE_ADDED    = 3,
	   SERVICE_REMOVED  = 4
    };

    /// Configuration properties:

    /// Construction Property: Adder object name
    std::string                 name                { };
    /// Construction Property: Adder service name
    std::string                 serviceName         { };
    /// Construction Property: Adder type (counters/histograms)
    adder_type_t                type                { ADD_UNKNOWN };

    /// Property: Task pattern to subscribe (reular expression)
    std::string         taskPattern      { };
    /// Property: Service pattern to subscribe (reular expression)
    std::string         servicePattern   { };
    /// Property: Fragment receive timeout
    long long           rectmo           { 0 };
    /// Property: Adder is EOR adder
    bool                isEOR            { false };
    /// Property: Adder is saver
    bool                isSaver          { false };
    /// Property: Disable RPC callback handling
    bool                noRPC            { false };
    /// Property: Do not publish added data
    bool                disableOutput    { false };
    /// Property: Enable verbose debugging
    bool                debugOn          { false };
    /// Property: Enable run number aware execution
    bool                runAware         { false };
    /// Property: Enable the tracking of source information
    bool                trackSources     { false };
    /// Property: Have pause 
    bool                doPause          { false };

    /// Public monitoring properties:
    long                numUpdateCycles        { 0 };
    int                 numTasksConnected      { 0 };
    int                 numServicesOutput      { 0 };
    int                 numServicesCommand     { 0 };
    int                 numServicesConnected   { 0 };
    int                 numBuffersReceived     { 0 };
    int                 numBuffersCurrent      { 0 };

    /// Public data members 
    INServiceMap        inputServicemap        { };
    MonitorItems        monitorItems           { };
    std::shared_ptr<DimServerDns> serviceDNS   { };

  protected:
    typedef std::map<std::string, std::set<std::string> > TaskInventory;
    class AddTimer;

    TaskInventory               taskInventory       { };
    BRTLLock                    maplock             { };
    BRTLLock                    invlock             { };
    std::unique_ptr<RunManager> runManager          { };
    std::unique_ptr<AddTimer>   timer               { };
    std::unique_ptr<ObjRPC>     rpc                 { };
    std::string                 cmdName             { };
    std::regex                  task_regex          { };
    std::regex                  service_regex       { };
    DimBuffBase                *rateBuff            { nullptr };
    void                       *CycleArg            { nullptr };
    void                       *PauseArg            { nullptr };
    void                      (*CycleFn)(void*, mem_buff&, MonitorItems *, MonAdder *)  { nullptr };
    void                      (*PauseFn)(void*)     { nullptr };
    bool                        isLocked            { false   };

    int                         sourceSvc_id        { 0 };
    std::string                 source_buffer       { };
    
    static std::string getClassName(const std::string &service);
    static void    feedSources(void* tag, void** address, int* size, int* first);
    static void    load_item_map(MonitorItems& m, mem_buff& buffer);
    static unsigned long long gettime();

    void load_item_map(mem_buff& buffer);

    void process_buffer(void *buffer, long siz, MonInfo *h);
    void finish_cycle(std::shared_ptr<ClassDescriptor>& cl, bool force=false);
    void publishSources();

    virtual void handleTimeout(std::shared_ptr<ClassDescriptor>& clazz);
    void add_what_we_have();

  public:

    MonAdder(const std::string& name, const std::string& service_name, adder_type_t typ);
    virtual ~MonAdder();

    std::string taskName(const std::string& server);
    void SetCycleFn(void CycleCB(void*, mem_buff&, MonitorItems*, MonAdder *), void *tis);
    void SetPauseFn(void Pause(void*), void *tis);

    virtual void configure();
    virtual void start();
    virtual void stop();

    std::pair<std::shared_ptr<ClassMap>, unsigned int>
      get_class_map(const std::string& service, bool create);
    int  addService(const std::string& task_name, const std::string& svc_name);
    int  removeService(const std::string& task_name, const std::string& svc_name);
    bool addTask(const std::string& task);
    bool removeTask(const std::string& task);
    void update(ClassDescriptor* clazz);
    void idle_update();

    /// Callback to sum up all items of a source frame
    virtual void add_items(ClassDescriptor* clazz, mem_buff& buffer, MonInfo *info);
    /// Callback to sum up one single item
    virtual void add_item(DimBuffBase* summed, const DimBuffBase* source);
  };
}
#endif
