//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
/*
 * RunManager.h
 *
 *  Created on: Nov 8, 2019
 *      Author: beat
 */

#ifndef ONLINE_GAUCHOAPPL_GAUCHOAPPL_RUNMANAGER_H_
#define ONLINE_GAUCHOAPPL_GAUCHOAPPL_RUNMANAGER_H_

#include <map>
#include <memory>

/// Online namespace declaration
namespace Online  {

  class ClassDescriptor;
  typedef std::map<std::string, std::shared_ptr<ClassDescriptor> > ClassMap;

  class RunManager : public std::map<unsigned int, std::shared_ptr<ClassMap> >  {
  public:
    std::size_t m_MaxSize  { 1 };
    RunManager() = default;
    ~RunManager() = default;
    int cleanOldest();
    void setMaxSize(std::size_t m){ m_MaxSize = m;};
  };
}

#endif /* ONLINE_GAUCHOAPPL_GAUCHOAPPL_RUNMANAGER_H_ */
