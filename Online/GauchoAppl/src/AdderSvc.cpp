//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================

#include <GauchoAppl/AdderSvc.h>
#include <GauchoAppl/HistAdder.h>
#include <GauchoAppl/CounterAdder.h>
#include <GauchoAppl/SaveTimer.h>
#include <GauchoAppl/AdderSys.h>
#include <Gaucho/MyDimErrorHandler.h>
#include <Gaucho/SegvHandler.h>
#include <Gaucho/Utilities.h>
#include <GaudiKernel/IIncidentSvc.h>
#include <AIDA/IHistogram.h>
#include <RTL/strdef.h>
#include <CPP/Value.h>
#include <dim/dis.hxx>

using namespace Online;

namespace    {
  class DimLock {
  public:
    DimLock()   { dim_lock();   }
    ~DimLock()  { dim_unlock(); }
  };

  void EORSaver(void *arg, mem_buff& /* b */, MonitorItems* /* m */, MonAdder* /* caller */)  {
    CPP::_PtrItem<SaveTimer>(arg).ptr()->timerHandler();
  }
  void doPause(void *arg)    {
    CPP::_PtrItem<AdderSvc>(arg).ptr()->gotoPause();
  }
}

AdderSvc::AdderSvc(const std::string& nam, ISvcLocator* svc_loc) : Service(nam,svc_loc)   {
  SegvHandler::instance();
  m_myServiceName = nam;
  declareProperty("MyName",          m_myName          = "");
  declareProperty("InDNS",           m_inputDNS        = "");
  declareProperty("OutDNS",          m_outputDNS       = "<dns>");
  declareProperty("SaveRootDir",     m_saveRootDir     = "/home/beat/Hist/Savesets");
  declareProperty("IsSaver",         m_isSaver         = false);
  declareProperty("SaveInterval",    m_saveInterval    = 900);
  declareProperty("PartitionName",   m_partitionName   = "LHCb");
  declareProperty("SaveSetTaskName", m_saverTaskName   = "Moore");
  declareProperty("AdderClass",      m_adderClass      = "hists"); //Possible values are 'hists' for histograms or 'counter' for counters.
  declareProperty("TaskPattern",     m_taskPattern);
  declareProperty("ServicePattern",  m_servicePattern);
  declareProperty("ReceiveTimeout",  m_recvtmo         = 0);
  declareProperty("GotoPause",       m_doPause         = false);
  declareProperty("PauseOnEOR",      m_pauseOnEOR      = false);
  declareProperty("DebugOn",         m_debugOn         = false);
  declareProperty("DebugSys",        m_debugSys        = false);
  declareProperty("EoRTmoFactor",    m_eorTmoFactor    = 0);
  declareProperty("RunAware",        m_runAware        = false);
  declareProperty("TrackSources",    m_trackSources    = false);
  declareProperty("ServiceDelay",    m_serviceDelay    = -1);
  declareProperty("HaveTimer",       m_haveTimer       = 0);
}

AdderSvc::~AdderSvc()
{
}

StatusCode AdderSvc::queryInterface(const InterfaceID& riid, void** ppvIF)  {
  if ( IIncidentListener::interfaceID().versionMatch(riid) )
    *ppvIF = (IIncidentListener*)this;
  else
    return Service::queryInterface(riid, ppvIF);
  addRef();
  return StatusCode::SUCCESS;
}

StatusCode AdderSvc::initialize()   {
  StatusCode sc = Service::initialize();
  if( !sc.isSuccess() )  {
    return sc;
  }
  this->m_stopping = false;
  if ( m_taskPattern.empty() )  {
    ::lib_rtl_output(LIB_RTL_FATAL,"+++ The option \"TaskPattern\" MUST be set!");
    return StatusCode::FAILURE;
  }
  else if ( m_servicePattern.empty() )  {
    ::lib_rtl_output(LIB_RTL_FATAL,"+++ The option \"ServicePattern\" MUST be set!");
    return StatusCode::FAILURE;
  }
  this->incidentSvc = serviceLocator()->service("IncidentSvc",true);
  if( !this->incidentSvc.get() ) {
    return StatusCode::FAILURE;
  }
  this->monitorSvc = serviceLocator()->service("MonitorSvc",true);
  if( !this->monitorSvc.get() ) {
    return StatusCode::FAILURE;
  }
  this->incidentSvc->addListener(this,"APP_INITIALIZED");
  this->incidentSvc->addListener(this,"APP_RUNNING");
  this->incidentSvc->addListener(this,"APP_STOPPED");

  std::string nodename = RTL::str_lower(RTL::nodeNameShort());
  std::string ddns = RTL::str_lower(std::getenv("DIM_DNS_NODE") ? std::getenv("DIM_DNS_NODE") : "");

  this->m_taskPattern = RTL::str_lower(this->m_taskPattern);
  this->m_adderClass  = RTL::str_lower(this->m_adderClass);
  this->m_inputDNS    = RTL::str_lower(this->m_inputDNS);
  this->m_outputDNS   = RTL::str_lower(this->m_outputDNS);

  this->m_saveRootDir = RTL::str_replace(this->m_saveRootDir, "<node>", nodename);
  this->m_saveRootDir = RTL::str_replace(this->m_saveRootDir, "<part>", this->m_partitionName);

  this->m_taskPattern = RTL::str_replace(this->m_taskPattern, "<node>", nodename);
  this->m_taskPattern = RTL::str_replace(this->m_taskPattern, "<part>", this->m_partitionName);
  this->m_taskPattern += "(.*)";

  this->m_servicePattern = RTL::str_replace(this->m_servicePattern, "<node>", nodename);
  this->m_servicePattern = RTL::str_replace(this->m_servicePattern, "<part>", this->m_partitionName);

  this->m_inputDNS = RTL::str_replace(this->m_inputDNS,"<node>",nodename);
  this->m_inputDNS = RTL::str_replace(this->m_inputDNS,"<dns>",ddns);

  auto& sys = AdderSys::instance();
  if ( this->m_serviceDelay > 0 ) sys.service_delay = this->m_serviceDelay;

  sys.setClientDns(this->m_inputDNS);
  if ( this->m_outputDNS.empty() )  {
    this->m_outputDNS = ddns;
  }
  else  {
    this->m_outputDNS = RTL::str_replace(this->m_outputDNS,"<node>",nodename);
    this->m_outputDNS = RTL::str_replace(this->m_outputDNS,"<dns>",ddns);
  }
  std::shared_ptr<DimServerDns> service_dns = sys.getDNS(this->m_outputDNS);
  if ( service_dns.get() )   {
  }

  sys.setDebugOn(this->m_debugSys);
  if (this->m_errh == 0)   {
    this->m_errh = new MyDimErrorHandler();
  }
  return i_start();
}

StatusCode AdderSvc::start()   {
  this->m_stopping = false;
  MonInfo::setShutdownInProgress(false);
  StatusCode status = Service::start();
  if ( !status.isSuccess() )  {
    return status;
  }
  this->m_errh->start();
  if ( this->idle_thread )   {
    this->m_stopping = true;
    this->idle_thread->join();
    this->idle_thread.reset();
  }
  this->m_stopping = false;
  return status;
}

StatusCode AdderSvc::i_start()   {
  std::string servicename;
  auto& sys = AdderSys::instance();
  if ( sys.numAdders == 0 )    {
    this->monitorSvc->declareInfo("SYS_NumAdders",           sys.numAdders = 0,            "", this);
    this->monitorSvc->declareInfo("SYS_NumSources",          sys.numSources = 0,           "", this);
    this->monitorSvc->declareInfo("SYS_NumServices",         sys.numServices = 0,          "", this);
    this->monitorSvc->declareInfo("SYS_NumServicesCalls",    sys.numServicesCalls = 0,     "", this);
    this->monitorSvc->declareInfo("SYS_NumServicesFails",    sys.numServicesFails = 0,     "", this);
    this->monitorSvc->declareInfo("SYS_NumServicesProcessed",sys.numServicesProcessed = 0, "", this);
  }
  if (this->m_adderClass == "hists")   {
    servicename = "Histos";
  }
  else if(this->m_adderClass == "counter")   {
    servicename = "Counter";
  }
// Nodeadders:
// Source task names:
//  Reconstruction task structure: <Partition>_<Node>_RecBrunel_xx
//  HLT Task structure: <node>_GAUCHOJOB_xx
//  Nodeadder output name:
//   Reconstruction: <part>_<node>_RecAdder_xx
//   HLT: <node>_Adder_xx
//
// subfarm adders:
// Source task names (all Farms):
//  <node>_Adder_xx
//  subfarm adder output name:
//   Reconstruction: <part>_<node>_RecAdder_xx
//   HLT: <sfnname>_Adder_xx     sfname = hltxyy
//
  std::shared_ptr<DimServerDns> service_dns = sys.getDNS(this->m_outputDNS);
  if (this->m_started) return StatusCode::SUCCESS;
  if (this->m_errh != 0) DimClient::addErrorHandler(this->m_errh);
  if (this->m_errh != 0) DimServer::addErrorHandler(this->m_errh);
  ::lib_rtl_output(LIB_RTL_INFO,"AdderSvc Pattern: Task: %s Service: %s + Data/EOR",
		   this->m_taskPattern.c_str(), this->m_servicePattern.c_str());
  if ( this->m_trackSources ) sys.trackSources();

  if ( this->m_isSaver )   {
    this->m_infoFileStatus = "SAVESETLOCATION/......................................................";
    std::string infoName = this->m_partitionName+"/"+this->m_saverTaskName+"/SAVESETLOCATION";
    this->savesetSvc.reset(new DimService(service_dns.get(), infoName.c_str(),(char*)this->m_infoFileStatus.c_str()));
  }
  if ( !this->setup_adder(this->intervalAdder, service_dns, false).isSuccess() )   {
    ::lib_rtl_output(LIB_RTL_FATAL,"+++ Failed to configure adder....");
    return StatusCode::FAILURE;
  }
  if ( this->m_isSaver )  {
    this->setup_saver(this->intervalSaver, this->intervalAdder, false);
    this->monitorSvc->declareInfo("SaveCycles", this->intervalSaver->numSaveCycles, "", this);
    this->intervalSaver->start();
  }
  this->monitorSvc->declareInfo("UpdateCycles",        this->intervalAdder->numUpdateCycles = 0,      "", this);
  this->monitorSvc->declareInfo("NumTasksConnected",   this->intervalAdder->numTasksConnected = 0,    "", this);
  this->monitorSvc->declareInfo("NumServicesOutput",   this->intervalAdder->numServicesOutput = 0,    "", this);
  this->monitorSvc->declareInfo("NumServicesCommand",  this->intervalAdder->numServicesCommand = 0,   "", this);
  this->monitorSvc->declareInfo("NumServicesConnected",this->intervalAdder->numServicesConnected = 0, "", this);
  this->monitorSvc->declareInfo("NumBuffersReceived",  this->intervalAdder->numBuffersReceived = 0,   "", this);
  this->monitorSvc->declareInfo("NumBuffersCycle",     this->intervalAdder->numBuffersCurrent = 0,    "", this);

  if ( !this->setup_adder(this->eorAdder, service_dns, true).isSuccess() )   {
    ::lib_rtl_output(LIB_RTL_FATAL,"+++ Failed to configure adder....");
    return StatusCode::FAILURE;
  }
  if ( this->m_isSaver )  {  
    this->setup_saver(this->eorSaver, this->eorAdder, true);
    this->eorAdder->SetCycleFn(EORSaver, this->eorSaver.get());
  }
  this->m_started = true;
  return StatusCode::SUCCESS;
}

void AdderSvc::setup_saver(std::unique_ptr<Online::SaveTimer>& saver, 
			   std::shared_ptr<Online::MonAdder>&  adder, bool is_eor)   {
  saver = std::make_unique<SaveTimer>(adder, this->m_saveInterval);
  saver->savesetSvc = this->savesetSvc;
  saver->partition  = this->m_partitionName;
  saver->rootdir    = this->m_saveRootDir;
  saver->taskname   = this->m_saverTaskName;
  saver->EOR        = is_eor;
}

StatusCode AdderSvc::setup_adder(std::shared_ptr<MonAdder>& adder,
				 std::shared_ptr<DimServerDns>& dns, bool is_eor)   {
  std::string nodename = RTL::str_lower(RTL::nodeNameShort());
  std::string myservicename = this->m_myName;
  myservicename = RTL::str_replace(myservicename, "<part>", this->m_partitionName);
  myservicename = RTL::str_replace(myservicename, "<node>", nodename);
  if ( this->m_adderClass == "hists" )  {
    adder = std::make_shared<HistAdder>(myservicename, is_eor ? "EOR" : "Data", ADD_HISTO);
  }
  else if ( this->m_adderClass == "counter" )  {
    adder = std::make_shared<CounterAdder>(myservicename, is_eor ? "EOR" : "Data", ADD_COUNTER);
  }
  else   {
    ::lib_rtl_output(LIB_RTL_FATAL,"+++ Invalid adder class specified: '%s'", this->m_adderClass.c_str());
    return StatusCode::FAILURE;
  }
  adder->debugOn      = this->m_debugOn;
  adder->taskPattern  = this->m_taskPattern;
  adder->serviceDNS   = dns;
  adder->isSaver      = this->m_isSaver;
  adder->trackSources = this->m_trackSources;
  adder->isEOR        = is_eor;
  adder->doPause      = is_eor ? this->m_pauseOnEOR : this->m_doPause;
  /// Neither SAVERS nor EOR adders may have a RPC interface: Name clashes !
  adder->noRPC        = is_eor || this->m_isSaver;

  if ( !is_eor ) {
    if ( this->m_doPause ) adder->SetPauseFn(::doPause, this);
    adder->runAware       = this->m_runAware;
    adder->servicePattern = this->m_servicePattern + 
      (this->m_runAware ? "[[:digit:]]*/Data(.*)" : "Data(.*)");
    adder->rectmo         = this->m_recvtmo;
  }
  else  {
    if ( this->m_pauseOnEOR ) adder->SetPauseFn(::doPause,this);
    adder->runAware       = false;
    adder->servicePattern = this->m_servicePattern + "EOR(.*)";
    adder->rectmo = (this->m_eorTmoFactor == 0) ? 500*this->m_recvtmo : this->m_eorTmoFactor * this->m_recvtmo;
  }
  adder->configure();
  return StatusCode::SUCCESS;
}

StatusCode AdderSvc::i_stop()   {
  this->m_errh->stop();
  if ( this->monitorSvc )   {
    this->monitorSvc->undeclareAll(this);
  }
  return StatusCode::SUCCESS;
}

StatusCode AdderSvc::stop()   {
  this->m_errh->stop();
  if ( !this->m_isSaver && this->intervalAdder && this->m_haveTimer )    {
    if ( this->idle_thread )   {
      this->m_stopping = true;
      this->idle_thread->join();
      this->idle_thread.reset();
    }
    this->m_stopping = false;
    this->idle_thread = std::make_unique<std::thread>([this]{
	long cnt = 0;
	MsgStream msg( msgSvc(), name() );
	while ( !this->m_stopping )   {
	  if ( ((cnt-100)%3000) == 0 )   {
	    //msg << MSG::ALWAYS << "Calling idle_update" << endmsg;
	    this->intervalAdder->idle_update();
	  }
	  ::lib_rtl_sleep(100);
	  cnt += 100;
	}
      });
  }
  return Service::stop();
}

StatusCode AdderSvc::finalize()  {
  MsgStream msg( msgSvc(), name() );
  msg << MSG::DEBUG << "AdderSvc finalize called" << endmsg;
  this->m_stopping = true;
  if ( this->idle_thread )   {
    this->idle_thread->join();
    this->idle_thread.reset();
  }
  i_stop().ignore();
  if ( this->monitorSvc )   {
    this->monitorSvc->undeclareAll(this);
  }
  this->monitorSvc.reset();
  {
    DimLock lock;
    MonInfo::setShutdownInProgress(true);
    if ( this->incidentSvc )    {
      this->incidentSvc->removeListener(this);
      this->incidentSvc.reset();
    }
    if (this->eorSaver != 0)    {
      this->eorSaver->stop();
      this->eorSaver.reset();
    }
    AdderSys::instance().stop();
    if ( this->intervalAdder )    {
      this->intervalAdder->stop();
      this->intervalAdder.reset();
    }
    if ( this->eorAdder )    {
      this->eorAdder->stop();
      this->eorAdder.reset();
    }
  }
  return Service::finalize();
}

void AdderSvc::gotoPause()   {
  this->incidentSvc->fireIncident(Incident(name(),"DAQ_PAUSE"));
}

void AdderSvc::handle(const Incident& inc)   {
  if (inc.type() == "APP_RUNNING" )    {
    AdderSys::instance().start();
  }
  else if (inc.type() == "APP_STOPPED" )   {
  }
  else if (inc.type() == "APP_INITIALIZED" )   {
  }
}
