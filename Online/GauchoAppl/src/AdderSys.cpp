//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
/// Framework include files
#include <GauchoAppl/AdderSys.h>
#include <GauchoAppl/MonAdder.h>
#include <dim/dis.hxx>
#include <dim/dic.hxx>
#include <RTL/strdef.h>
#include <RTL/rtl.h>

/// C/C++ include files
#include <algorithm>
#include <sstream>
#include <map>

Online::AdderSys::ServiceHandler::ServiceHandler(const std::string& srv, int delay) : task(srv)   {
  (delay > 0) ? ::dtq_start_timer(delay, service_tmo_handler, this) : this->start();
}

void Online::AdderSys::ServiceHandler::start()   {
  std::string svc = this->task + "/SERVICE_LIST";
  if ( 0 == this->id )   {
    this->id = ::dic_info_service(svc.c_str(), MONITORED,
				  0, nullptr, 0, ServiceHandler::callback,
				  (long)this, (char*)"DEAD", 5);
  }
  if ( 0 == this->iv )   {
    /*
      this->iv = ::dic_info_service(svc.c_str(), MONITORED,
      10, nullptr, 0, ServiceHandler::interval,
      (long)this, (char*)"DEAD", 5);
    */
  }
}

Online::AdderSys::ServiceHandler::~ServiceHandler()   {
  ::dtq_stop_timer(this);
  if ( this->id )   {
    ::dic_release_service(this->id);
    this->id = 0;
  }
  if ( this->iv )   {
    ::dic_release_service(this->iv);
    this->iv = 0;
  }
}

/// DTQ overload to process timeout(s)
void Online::AdderSys::ServiceHandler::service_tmo_handler(void* tag)  {
  if ( tag ) {
    ServiceHandler* info = (ServiceHandler*)tag;
    info->start();
  }
}

/// DimInfo overload to process messages
void Online::AdderSys::ServiceHandler::callback(void* tag, void* address, int* size)   {
  if ( address && tag && size && *size>0 )   {
    ServiceHandler* it = *(ServiceHandler**)tag;
    it->analyze_services((char*)address, true);
  }
}

/// DimInfo overload to process messages
void Online::AdderSys::ServiceHandler::interval(void* tag, void* address, int* size)   {
  if ( address && tag && size && *size>0 )   {
    ServiceHandler* it = *(ServiceHandler**)tag;
    it->analyze_services((char*)address, false);
  }
}

void Online::AdderSys::ServiceHandler::analyze_services(const char* input, bool may_remove)   {
  auto& sys   = AdderSys::instance();
  auto& alist = sys.adderList;
  sys.debugOn = 1;
  int   level = sys.debugOn /* || sys.is_tracking() */ ? LIB_RTL_ALWAYS : LIB_RTL_VERBOSE;

  if ( !input || std::strlen(input) == 0 )  {
    ::lib_rtl_output(level,"Received empty message from %s.... Ignoring...\n",task.c_str());
    ++sys.numServicesFails;
    return;
  }
  ++sys.numServicesCalls;

  if ( std::strcmp(input, "DEAD" ) == 0 )  {
    ::lib_rtl_output(level,"Service Info Handler from %s DEAD",task.c_str());
    for ( auto* i : alist )   {
      if ( may_remove ) i->removeTask(task);
    }
    return;
  }
  int action = 0;
  std::vector<std::string> service_list;
  switch(input[0])   {
  case '+':
    service_list = RTL::str_split(input+1,"\n");
    action =  1;
    break;
  case '-':
    service_list = RTL::str_split(input+1,"\n");
    action = -1;
    break;
  case '!':
    break;
  default:
    service_list = RTL::str_split(input,"\n");
    action =  1;
    break;
  }
  sys.numServices += (action*int(service_list.size()));

  if ( !service_list.empty() && (action == 1 || action == -1) )  {
    std::size_t added = 0, removed = 0;
    std::string all;
    for ( std::string svc : service_list )    {
      svc = svc.substr(0, svc.find('|'));
      std::size_t idx = svc.find('/');
      all += svc.substr(idx == std::string::npos ? 0 : idx+1) + " ";
      if ( action == 1 )   {
	//::lib_rtl_output(level,"Selective New Service:     %s -> %s", task.c_str(), svc.c_str());
	++sys.numServicesProcessed;
	for ( auto* i : alist )   {
	  int ret = i->addService(task, svc);
	  switch(ret)   {
	  case MonAdder::SERVICE_ADDED:
	    if ( i->debugOn )  {
	      ::lib_rtl_output(LIB_RTL_ALWAYS,"ADDED new service:         %s -> %s", task.c_str(), svc.c_str());
	    }
	    ++added;
	    break;
	  case MonAdder::SERVICE_NO_MATCH:
            break;
	  case MonAdder::SERVICE_PRESENT:
	    if ( i->debugOn )  {
	      ::lib_rtl_output(LIB_RTL_ALWAYS,"ALREADY PRESENT service:   %s -> %s", task.c_str(), svc.c_str());
	    }
	    break;
	  default:
	    break;
	  }
	}
      }
      else  if ( may_remove )   {
	::lib_rtl_output(level,"Selective Removed Service: %s -> %s", task.c_str(), svc.c_str());
	for ( auto* i : alist )  {
	  int ret = i->removeService(task, svc);
	  switch(ret)   {
	  case MonAdder::SERVICE_REMOVED:  ++removed; break;
	  case MonAdder::SERVICE_NO_MATCH:            break;
	  default:                                    break;
	  }
	}
      }
    }
    if ( added + removed != service_list.size() )   {
      // needed for debugging
    }
  }
}

/// DimInfo overload to process messages
void Online::AdderSys::taskinfo_callback(void* tag, void* address, int* size)   {
  if ( address && tag && size && *size>0 )   {
    AdderSys* it = *(AdderSys**)tag;
    it->analyze_tasks((char*)address);
  }
}

void Online::AdderSys::feedSources(void* tag, void** address, int* size, int* /* first */)   {
  if ( tag && address && size )   {
    std::stringstream str;
    AdderSys* sys = *(AdderSys**)tag;
    auto& m = sys->sources;
    str << "Task-Sources:" << m.size() << '#';
    for( auto s = m.begin(); s != m.end(); )
      str << *s << (++s == m.end() ? "" : "|");
    sys->source_buffer = str.str();
    *size    = sys->source_buffer.length()+1;
    *address = (void*)sys->source_buffer.c_str();
    return;
  }
  *size = 0;
  *address = (char*)"";
}


/// DTQ overload to process timeout(s)
void Online::AdderSys::task_tmo_handler(void* tag)  {
  if ( tag ) {
    AdderSys* sys = (AdderSys*)tag;
    sys->start();
  }
}

Online::AdderSys::AdderSys() : sourceSvc_id(0)  {
}

Online::AdderSys::~AdderSys()   {
  if ( this->sourceSvc_id ) ::dis_remove_service(this->sourceSvc_id);
  this->sourceSvc_id = 0;
}

void Online::AdderSys::setClientDns(const std::string& dns)    {
  if ( !dns.empty() && dns != dns_node )   {
    dns_node = dns;
    DimClient::setDnsNode(dns_node.c_str());
  }
}

void Online::AdderSys::publishSources()   {
  if ( this->sourceSvc_id )   {
    ::dis_update_service(this->sourceSvc_id);
  }
}

void Online::AdderSys::add(MonAdder *a)   {
  for ( const auto* i : this->adderList )  {
    if (i == a)
      return;
  }
  this->adderList.push_back(a);
  this->numAdders = this->adderList.size();
}

void Online::AdderSys::remove(MonAdder *a)   {
  while( true )   {
    auto i = std::find(this->adderList.begin(), this->adderList.end(), a);
    if ( i == this->adderList.end() )   {
      this->publishSources();
      break;
    }
    this->adderList.erase(i);
  }
  this->numAdders = this->adderList.size();
}

void Online::AdderSys::trackSources()   {
  if ( 0 == this->sourceSvc_id )   {
    auto svc = RTL::processName()+"/task_sources";
    this->sourceSvc_id = ::dis_add_service(svc.c_str(),"C",0,0,feedSources,(long)this);
  }
}

void Online::AdderSys::addSource(const std::string& src)   {
  this->sources.emplace(src);
  this->numSources = this->sources.size();
  this->publishSources();
}

void Online::AdderSys::removeSource(const std::string& src)   {
  auto i = this->sources.find(src);
  if ( i != this->sources.end() )  {
    this->sources.erase(i);
  }
  this->numSources = this->sources.size();
  this->publishSources();
}

Online::AdderSys &Online::AdderSys::instance()    {
  static AdderSys s;
  return s;
}

void Online::AdderSys::setDebugOn(bool dbg)    {
  this->debugOn = dbg;
}

void Online::AdderSys::start()    {
  if ( 0 == this->taskInfo_id )    {
    //::dtq_start_timer(3, task_tmo_handler, this);
    this->taskInfo_id = ::dic_info_service("DIS_DNS/SERVER_LIST", MONITORED,
					  0, nullptr, 0, taskinfo_callback, 
					  (long)this, (char*)"DEAD", 5);
    ::lib_rtl_output(this->debugOn ? LIB_RTL_ALWAYS : LIB_RTL_VERBOSE,
		     "Starting ADDER task info handler.....");
  }
}

void Online::AdderSys::stop()   {
  ::dtq_stop_timer(this);
  if ( 0 != this->taskInfo_id )    {
    int dim_id = this->taskInfo_id;
    this->taskInfo_id = 0;
    ::dic_release_service(dim_id);
  }
  //this->numServices          = 0;
  //this->numServicesCalls     = 0;
  //this->numServicesFails     = 0;
  //this->numServicesProcessed = 0;
  //?? taskMap.clear();
  //?? servers.clear();
}

std::shared_ptr<DimServerDns> Online::AdderSys::getDNS(std::string &dns)    {
  int dns_id = ::dis_add_dns(dns.c_str(), ::dim_get_dns_port());
  std::string nam = dns_id == 0 ? RTL::processName() : "MON_"+ RTL::processName();
  return this->getDNS(dns, nam);
}

std::shared_ptr<DimServerDns> Online::AdderSys::getDNS(std::string &dns, std::string &name)  {
  auto it = this->dnsMap.find(dns);
  if (it != this->dnsMap.end())
    return it->second;

  int dns_id = ::dis_add_dns(dns.c_str(), ::dim_get_dns_port());
  std::string nam = dns_id == 0 ? RTL::processName() : "MON_"+ RTL::processName();
  if (this->servers.find(nam) != this->servers.end())    {
    nam = name;
  }
  auto srv = std::make_shared<DimServerDns>(dns.c_str(), ::dim_get_dns_port(), nam.c_str());
  srv->autoStartOn();
  this->dnsMap[dns] = srv;
  this->servers.insert(nam);
  return srv;
}

void Online::AdderSys::analyze_tasks(const char* input)   {
  int level = this->debugOn ? LIB_RTL_ALWAYS : LIB_RTL_VERBOSE;

  if ( nullptr == input || std::strlen(input)==0 )  {
    return;
  }
  if ( std::strcmp(input, "DEAD") == 0 )  {
    ::lib_rtl_output(level,"Task Info Handler callback from DNS DEAD");
    return;
  }

  int action = 0;
  std::vector<std::string> server_list;
  switch(input[0])   {
  case '+':
    server_list = RTL::str_split(input+1,"|");
    action =  1;
    break;
  case '-':
    server_list = RTL::str_split(input+1,"|");
    action = -1;
    break;
  case '!':
    break;
  default:
    server_list = RTL::str_split(input,"|");
    action =  1;
    break;
  }
  if ( server_list.empty() )   {
    return;
  }

  auto& alist = Online::AdderSys::instance().adderList;
  for (auto server : server_list )  {
    std::size_t added = 0, removed = 0, create = 0;
    server = server.substr(0,server.find("@"));

    for (auto* adder : alist )   {
      std::string tname = adder->taskName(server);
      if ( !tname.empty() )   {
	auto i = this->taskMap.find(tname);
        if ( action == 1 )   {
	  if ( i == this->taskMap.end() ) ++create;
	  if ( adder->addTask(tname) ) ++added;
	}
	else if ( action == -1 )   {
	  // Do not remove services. We keep all subscription, 
	  // because there is some suspicion they may not
	  // be re-established properly!
	  if (i != this->taskMap.end())  {
	    this->taskMap.erase(i);
	    this->removeSource(tname);
	  }
	  if ( adder->removeTask(tname) ) ++removed;
	}
      }
    }
    if ( added > 0 && create > 0 )    {
      this->addSource(server);
      this->taskMap.emplace(server, std::make_shared<ServiceHandler>(server, this->service_delay));
      ::lib_rtl_output(level,"+++ Add service list handler (%d clients) for %s", added, server.c_str());
    }
    if ( (added + removed) != server_list.size() )   {
      // needed for debugging
    }
  }
}
