//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
/*
 * COutService.cpp
 *
 *  Created on: Jul 25, 2011
 *      Author: beat
 */
#include <typeinfo>
#include <dim/dis.hxx>
#include <GauchoAppl/COutService.h>

template <typename T> COutService<T>::COutService(const std::string& nam) : COutServiceBase(nam)
{
  m_data = 0;
  m_serv.reset(new DimService(m_nam.c_str(), m_data));
  m_serv->setData(&m_data,sizeof(m_data));
}

template <typename T> COutService<T>::~COutService()   {
}

template <typename T> void COutService<T>::Update(const T &dat)
{
  m_data = dat;
  m_serv->setData(&m_data,sizeof(m_data));
  m_serv->updateService();
}

template class COutService<short>;
template class COutService<int>;
template class COutService<long long int>;
template class COutService<float>;
template class COutService<double>;
