//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#include <Gaucho/MonInfo.h>
#include <Gaucho/ObjService.h>
#include <GauchoAppl/MonAdder.h>
#include <GauchoAppl/AddSerializer.h>
#include <GauchoAppl/ClassDescriptor.h>
#include <dim/dis.hxx>
#include <RTL/strdef.h>
#include <RTL/rtl.h>

#include <sstream>

using namespace Online;

namespace  {
  static int s_empty = 0;
  void dim_command_handler(void* tag, void* addr, int* size)  {
    if ( tag && addr && size && *size )   {
      TimeoutCmd* cmd = *(TimeoutCmd**)tag;
      cmd->commandHandler();
    }
  }
}

TimeoutCmd::TimeoutCmd(DimServerDns *dns, const std::string& nam, const std::string& svc, MonAdder *tis)
  : adder(tis), service(svc)  {
  this->command_id = ::dis_add_cmnd_dns(dns->getDnsId(), nam.c_str(), "I", dim_command_handler, long(this));
}

TimeoutCmd::TimeoutCmd(const std::string& nam, const std::string& svc, MonAdder *tis)
  : adder(tis), service(svc)  {
  this->command_id = ::dis_add_cmnd(nam.c_str(), "I", dim_command_handler, long(this));
}

void TimeoutCmd::commandHandler()  {
  ::lib_rtl_output(LIB_RTL_INFO,"Received Timeout Command for service %s", this->service.c_str());
  auto [class_map, run] = this->adder->get_class_map(this->service, false);
  if ( class_map )   {
    auto clit = class_map->find(MonAdder::getClassName(this->service));
    if ( clit != class_map->end() )   {
      this->adder->handleTimeout(clit->second);
    }
  }
}

INServiceDescr::~INServiceDescr()   {
}

ClassDescriptor::ClassDescriptor(const std::string& nam, MonAdder* a)
  : name(nam), adder(a)
{
  this->buffer = std::make_shared<mem_buff>();
}

ClassDescriptor::~ClassDescriptor()   {
  //printf("ClassDescriptor: %s\n",name.c_str());
}

void ClassDescriptor::operator()(void* data, int size, MonInfo* info)    {
  this->adder->process_buffer(data, size, info);
}

bool ClassDescriptor::addService(const std::string& service, const std::string& task)    {
  auto i = this->inputServices.find(service);
  if ( i == this->inputServices.end() )    {
    auto mi = std::make_unique<MonInfo>(service, this);
    this->inputServices.emplace(service, std::make_unique<INServiceDescr>(task, service, std::move(mi)));
    return true;
  }
  return false;
}

void ClassDescriptor::enableOutput(std::shared_ptr<DimServerDns>& dns, const std::string& svc, unsigned int run)   {
  if ( !this->outputservice )   {
    std::string service = svc + (this->name.empty() ? std::string("") : "." + this->name);
    if ( run != 0 )   {
      std::stringstream run_str;
      run_str << run;
      service = RTL::str_replace(service, "<runno>", run_str.str());
    }
    this->outputservice = 
      std::make_unique<ObjService>(dns, std::make_unique<AddSerializer>(this->monitorItems, this->adder->maplock),
				   service, "C", &s_empty, 4, this->buffer);
    ::lib_rtl_output(LIB_RTL_INFO,"+++ Booked Output Service [%s] %s",
		     dns.get() ? dns->getName() : "<dim-dns>",
		     this->outputservice->getName());
  }
}

void* ClassDescriptor::allocate(size_t siz)   {
  return this->buffer->allocate(siz, true);
}

void ClassDescriptor::adoptItems(const void* data, size_t size)   {
  this->buffer->copy(data, size);
  this->monitorItems.clear();
  this->adder->load_item_map(this->monitorItems, *this->buffer);
  this->adder->load_item_map(*this->buffer);
}

void ClassDescriptor::addItem(const std::string& item_name, DimBuffBase* data)    {
  size_t used_size = this->buffer->used();
  void* oldbuffer = this->buffer->begin();
  int hsiz = data->reclen;
  this->reallocate(hsiz);
  this->buffer->append(data, hsiz);
  const char *nam = add_ptr<char>(data,data->nameoff);
  if ( std::strcmp(nam,item_name.c_str()) != 0 )   {
    std::printf("[ERROR] Name Mismatch in new adder entry (%s) and Map (%s)\n",
		nam, item_name.c_str());
  }
  void* newbuffer = this->buffer->begin();
  if ( newbuffer != oldbuffer )      {
    this->adder->load_item_map(this->monitorItems, *this->buffer);
    this->adder->load_item_map(*this->buffer);
  }
  else   {
    BRTLLock::_Lock lock(&this->adder->maplock);
    this->monitorItems.emplace(nam, this->buffer->at<DimBuffBase>(used_size));
    this->adder->monitorItems.emplace(nam, this->buffer->at<DimBuffBase>(used_size));
  }
}

void* ClassDescriptor::reallocate(size_t incsiz)    {
  size_t used = this->buffer->used();
  size_t incr = incsiz > used ? incsiz : used;
  size_t newsiz = used + incr;
  if ( this->buffer->length() < newsiz )  {
    this->buffer->reallocate(newsiz);
    this->buffer->at<SerialHeader>(0)->buffersize = this->buffer->length();
  }
  return this->buffer->begin();
}
