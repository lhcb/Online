//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#include <Gaucho/dimhist.h>
#include <GauchoAppl/CounterAdder.h>
#include <GauchoAppl/ClassDescriptor.h>

//
// We can choose here the wire protocol:
// -- Either use the "true" types (Define must be enabled) or (USE_TRUE_TYPE > 0)
// -- Collapse the true types to long/double (USE_TRUE_TYPE == 0)
//
// If we ADD, it is not advisable to use tre types: The dynamic range is likely too small!
//
#define USE_TRUE_TYPE 1

namespace  {

#define __SETUP_ITEM(sum_type, src_type, name, last, offset)		\
  sum_type       *sum_##name  = add_ptr<sum_type>(sum_##last,offset);   \
  const src_type *src_##name  = add_ptr<src_type>(src_##last,offset);

#if USE_TRUE_TYPE > 0
#define SETUP_ITEM(sum_type, src_type,  name, last, offset, sum_typ)   __SETUP_ITEM(src_type, src_type,  name, last, offset)
#define SETUP_ITEM_F(sum_type, src_type,  name, last, offset, sum_typ) __SETUP_ITEM(src_type, src_type,  name, last, offset)
#else
#define SETUP_ITEM(sum_type, src_type,  name, last, offset, sum_typ)   __SETUP_ITEM(long, src_type, name, last, offset);   summed_item->type = sum_typ
#define SETUP_ITEM_F(sum_type, src_type,  name, last, offset, sum_typ) __SETUP_ITEM(double, src_type, name, last, offset); summed_item->type = sum_typ
#endif

  template <typename SUM, typename SRC> inline void add_number(Online::DimBuffBase *sumh, const Online::DimBuffBase *srch)   {
    SUM *sum = add_ptr<SUM>(sumh,sumh->dataoff);
    const SRC *src = add_ptr<SRC>(srch,srch->dataoff);
    *sum += SUM(*src);
  }

  template <typename T> inline void add_pair(Online::DimBuffBase *sumh, const Online::DimBuffBase *srch)   {
    T *sum = add_ptr<T>(sumh,sumh->dataoff);
    const T *src = add_ptr<T>(srch,srch->dataoff);
    sum[0] += src[0];
    sum[1] += src[1];
  }

  template <typename T> inline void add_array(Online::DimBuffBase *sumh, const Online::DimBuffBase *srch)   {
    int count = (srch->reclen - srch->dataoff)/sizeof(T);
    T *sum = add_ptr<T>(sumh,sumh->dataoff);
    const T *src = add_ptr<T>(srch,srch->dataoff);
    for (int indx = 0; indx < count; indx++ )
      sum[indx] += src[indx];
  }

  // --------------------------------------------------------------------------------------------------------------
  template <typename T> inline void _add_avg(Online::DimBuffBase *summed_item, const Online::DimBuffBase *source_item)   {
    long   *sum_ents = add_ptr<long>(summed_item,summed_item->dataoff);
    double *sum_mean = add_ptr<double>(sum_ents,sizeof(long));
    const long   *src_ents = add_ptr<long>(source_item,source_item->dataoff);
    const double *src_mean = add_ptr<double>(src_ents,sizeof(long));
    SETUP_ITEM(T, T, sum, mean, sizeof(double), C_GAUDIAVGACCl);
    *sum_ents += *src_ents;
    *sum_sum  += *src_sum;
    *sum_mean  = *sum_sum / std::max(1e0, double(*sum_ents));
  }
  template <> inline void _add_avg<double>(Online::DimBuffBase *summed_item, const Online::DimBuffBase *source_item)   {
    long   *sum_ents = add_ptr<long>(summed_item,summed_item->dataoff);
    double *sum_mean = add_ptr<double>(sum_ents,sizeof(long));
    const long   *src_ents = add_ptr<long>(source_item,source_item->dataoff);
    const double *src_mean = add_ptr<double>(src_ents,sizeof(long));
    SETUP_ITEM_F(double, double, sum, mean, sizeof(double), C_GAUDIAVGACCd);
    *sum_ents += *src_ents;
    *sum_sum  += *src_sum;
    *sum_mean  = *sum_sum / std::max(1e0, double(*sum_ents));
  }
  template <> inline void _add_avg<float>(Online::DimBuffBase *summed_item, const Online::DimBuffBase *source_item)   {
    long   *sum_ents = add_ptr<long>(summed_item,summed_item->dataoff);
    double *sum_mean = add_ptr<double>(sum_ents,sizeof(long));
    const long   *src_ents = add_ptr<long>(source_item,source_item->dataoff);
    const double *src_mean = add_ptr<double>(src_ents,sizeof(long));
    SETUP_ITEM_F(double, float, sum, mean, sizeof(double), C_GAUDIAVGACCd);
    *sum_ents += *src_ents;
    *sum_sum  += *src_sum;
    *sum_mean  = *sum_sum / std::max(1e0, double(*sum_ents));
  }
  // --------------------------------------------------------------------------------------------------------------
  template <typename T> inline void _add_stat(Online::DimBuffBase *summed_item, const Online::DimBuffBase *source_item)   {
    long   *sum_ents = add_ptr<long>(summed_item,summed_item->dataoff);
    double *sum_mean = add_ptr<double>(sum_ents,sizeof(long));
    const long   *src_ents = add_ptr<long>(source_item,source_item->dataoff);
    const double *src_mean = add_ptr<double>(src_ents,sizeof(long));

    SETUP_ITEM(T, T, sum, mean, sizeof(double), C_GAUDISTATACCl);
    SETUP_ITEM(T, T, sum2, sum, sizeof(T), C_GAUDISTATACCl);
    *sum_ents += *src_ents;
    *sum_sum  += *src_sum;
    *sum_sum2 += *src_sum2;
    *sum_mean  = *sum_sum / std::max(1e0, double(*sum_ents));
  }
  template <> inline void _add_stat<double>(Online::DimBuffBase *summed_item, const Online::DimBuffBase *source_item)   {
    long   *sum_ents = add_ptr<long>(summed_item,summed_item->dataoff);
    double *sum_mean = add_ptr<double>(sum_ents,sizeof(long));
    const long   *src_ents = add_ptr<long>(source_item,source_item->dataoff);
    const double *src_mean = add_ptr<double>(src_ents,sizeof(long));

    SETUP_ITEM_F(double, double, sum, mean, sizeof(double), C_GAUDISTATACCd);
    SETUP_ITEM_F(double, double, sum2, sum, sizeof(double), C_GAUDISTATACCd);
    *sum_ents += *src_ents;
    *sum_sum  += *src_sum;
    *sum_sum2 += *src_sum2;
    *sum_mean  = *sum_sum / std::max(1e0, double(*sum_ents));
  }
  template <> inline void _add_stat<float>(Online::DimBuffBase *summed_item, const Online::DimBuffBase *source_item)   {
    long   *sum_ents = add_ptr<long>(summed_item,summed_item->dataoff);
    double *sum_mean = add_ptr<double>(sum_ents,sizeof(long));
    const long   *src_ents = add_ptr<long>(source_item,source_item->dataoff);
    const double *src_mean = add_ptr<double>(src_ents,sizeof(long));

    SETUP_ITEM_F(double, float, sum, mean, sizeof(double), C_GAUDISTATACCd);
    SETUP_ITEM_F(double, float, sum2, sum, sizeof(float),  C_GAUDISTATACCd);
    *sum_ents += *src_ents;
    *sum_sum  += *src_sum;
    *sum_sum2 += *src_sum2;
    *sum_mean  = *sum_sum / std::max(1e0, double(*sum_ents));
  }
  // --------------------------------------------------------------------------------------------------------------
  template <typename T> inline void _add_sigma(Online::DimBuffBase *summed_item, const Online::DimBuffBase *source_item)   {
    long   *sum_ents = add_ptr<long>(summed_item,summed_item->dataoff);
    double *sum_mean = add_ptr<double>(sum_ents,sizeof(long));
    const long   *src_ents = add_ptr<long>(source_item,source_item->dataoff);
    const double *src_mean = add_ptr<double>(src_ents,sizeof(long));
    SETUP_ITEM(T, T, sum, mean, sizeof(double), C_GAUDISIGMAACCl);
    SETUP_ITEM(T, T, sum2, sum, sizeof(T), C_GAUDISIGMAACCl);
    *sum_ents += *src_ents;
    *sum_sum  += *src_sum;
    *sum_sum2 += *src_sum2;
    *sum_mean  = *sum_sum / std::max(1e0, double(*sum_ents));
  }
  template <> inline void _add_sigma<double>(Online::DimBuffBase *summed_item, const Online::DimBuffBase *source_item)   {
    long   *sum_ents = add_ptr<long>(summed_item,summed_item->dataoff);
    double *sum_mean = add_ptr<double>(sum_ents,sizeof(long));
    const long   *src_ents = add_ptr<long>(source_item,source_item->dataoff);
    const double *src_mean = add_ptr<double>(src_ents,sizeof(long));

    SETUP_ITEM_F(double, double, sum, mean, sizeof(double), C_GAUDISIGMAACCd);
    SETUP_ITEM_F(double, double, sum2, sum, sizeof(double), C_GAUDISIGMAACCd);
    *sum_ents += *src_ents;
    *sum_sum  += *src_sum;
    *sum_sum2 += *src_sum2;
    *sum_mean  = *sum_sum / std::max(1e0, double(*sum_ents));
  }
  template <> inline void _add_sigma<float>(Online::DimBuffBase *summed_item, const Online::DimBuffBase *source_item)   {
    long   *sum_ents = add_ptr<long>(summed_item,summed_item->dataoff);
    double *sum_mean = add_ptr<double>(sum_ents,sizeof(long));
    const long   *src_ents = add_ptr<long>(source_item,source_item->dataoff);
    const double *src_mean = add_ptr<double>(src_ents,sizeof(long));
    SETUP_ITEM_F(double, float, sum, mean, sizeof(double), C_GAUDISIGMAACCd);
    SETUP_ITEM_F(double, float, sum2, sum, sizeof(float), C_GAUDISIGMAACCd);
    *sum_ents += *src_ents;
    *sum_sum  += *src_sum;
    *sum_sum2 += *src_sum2;
    *sum_mean  = *sum_sum / std::max(1e0, double(*sum_ents));
  }
  // --------------------------------------------------------------------------------------------------------------
}

Online::CounterAdder::~CounterAdder()  {
}

void Online::CounterAdder::add_item(DimBuffBase* summed_item, const DimBuffBase* source_item)   {
  switch(source_item->type)      {
  case C_INTPAIR:           add_pair<int32_t>(summed_item, source_item);             break;
  case C_LONGPAIR:          add_pair<int64_t>(summed_item, source_item);             break;

  case C_INT:
  case C_UINT:
  case C_ATOMICINT:
    summed_item->type = C_LONGLONG;
    add_number<int64_t,int32_t>(summed_item, source_item);
    break;
  case C_LONGLONG:
  case C_ATOMICLONG:
    summed_item->type = C_LONGLONG;
    add_number<int64_t,int64_t>(summed_item, source_item);
    break;
  case C_ULONG:
    summed_item->type = C_ULONG;
    add_number<uint64_t, uint64_t>(summed_item, source_item);
    break;
  case C_FLOAT:
  case C_RATEFLOAT:  
  case C_ATOMICFLOAT:       add_number<float,float>(summed_item, source_item);       break;
  case C_DOUBLE:
  case C_RATEDOUBLE:
  case C_ATOMICDOUBLE:      add_number<double,double>(summed_item, source_item);     break;
  case C_INTSTAR:           add_array<int32_t>(summed_item, source_item);            break;        
  case C_UINTSTAR:          add_array<uint32_t>(summed_item, source_item);           break;
  case C_LONGSTAR:          add_array<int64_t>(summed_item, source_item);            break; 
  case C_ULONGSTAR:         add_array<uint64_t>(summed_item, source_item);           break; 
  case C_FLOATSTAR:         add_array<float>(summed_item, source_item);              break; 
  case C_DOUBLESTAR:
  case C_RATEDOUBLESTAR:    add_array<double>(summed_item, source_item);             break; 

    /// ==========================================================================================
  case C_GAUDIACCCHAR:
  case C_GAUDIACCuCHAR:
  case C_GAUDIACCSHORT:
  case C_GAUDIACCuSHORT:
  case C_GAUDIACCINT:
  case C_GAUDIACCuINT:
  case C_GAUDIACCLONG:      add_number<int64_t,int64_t>(summed_item, source_item);   break;
  case C_GAUDIACCuLONG:     add_number<uint64_t,uint64_t>(summed_item, source_item); break;
  case C_GAUDIACCFLOAT:
  case C_GAUDIACCDOUBLE:    add_number<double,double>(summed_item, source_item);     break;
    /// ==========================================================================================
  case C_GAUDIAVGACCc:      _add_avg<signed char>(summed_item, source_item);         break;
  case C_GAUDIAVGACCcu:     _add_avg<uint8_t>(summed_item, source_item);             break;
  case C_GAUDIAVGACCs:      _add_avg<int16_t>(summed_item, source_item);             break;
  case C_GAUDIAVGACCsu:     _add_avg<uint16_t>(summed_item, source_item);            break;
  case C_GAUDIAVGACCi:      _add_avg<int32_t>(summed_item, source_item);             break;
  case C_GAUDIAVGACCiu:     _add_avg<uint32_t>(summed_item, source_item);            break;
  case C_GAUDIAVGACCl:      _add_avg<int64_t>(summed_item, source_item);             break;
  case C_GAUDIAVGACClu:     _add_avg<uint64_t>(summed_item, source_item);            break;
  case C_GAUDIAVGACCf:      _add_avg<float>(summed_item, source_item);               break;
  case C_GAUDIAVGACCd:      _add_avg<double>(summed_item, source_item);              break;
    /// ==========================================================================================
  case C_GAUDISTATACCc:     _add_stat<signed char>(summed_item, source_item);        break;
  case C_GAUDISTATACCcu:    _add_stat<uint8_t>(summed_item, source_item);            break;
  case C_GAUDISTATACCs:     _add_stat<int16_t>(summed_item, source_item);            break;
  case C_GAUDISTATACCsu:    _add_stat<uint16_t>(summed_item, source_item);           break;
  case C_GAUDISTATACCi:     _add_stat<int32_t>(summed_item, source_item);            break;
  case C_GAUDISTATACCiu:    _add_stat<uint32_t>(summed_item, source_item);           break;
  case C_GAUDISTATACCl:     _add_stat<int64_t>(summed_item, source_item);            break;
  case C_GAUDISTATACClu:    _add_stat<uint64_t>(summed_item, source_item);           break;
  case C_GAUDISTATACCf:     _add_stat<float>(summed_item, source_item);              break;
  case C_GAUDISTATACCd:     _add_stat<double>(summed_item, source_item);             break;

    /// ==========================================================================================
  case C_GAUDISIGMAACCc:    _add_sigma<signed char>(summed_item, source_item);       break;
  case C_GAUDISIGMAACCcu:   _add_sigma<uint8_t>(summed_item, source_item);           break;
  case C_GAUDISIGMAACCs:    _add_sigma<int16_t>(summed_item, source_item);           break;
  case C_GAUDISIGMAACCsu:   _add_sigma<uint16_t>(summed_item, source_item);          break;
  case C_GAUDISIGMAACCi:    _add_sigma<int32_t>(summed_item, source_item);           break;
  case C_GAUDISIGMAACCiu:   _add_sigma<uint32_t>(summed_item, source_item);          break;
  case C_GAUDISIGMAACCl:    _add_sigma<int64_t>(summed_item, source_item);           break;
  case C_GAUDISIGMAACClu:   _add_sigma<uint64_t>(summed_item, source_item);          break;
  case C_GAUDISIGMAACCf:    _add_sigma<float>(summed_item, source_item);             break;
  case C_GAUDISIGMAACCd:    _add_sigma<double>(summed_item, source_item);            break;

  case C_GAUDIBINACCc:
  case C_GAUDIBINACCcu:
  case C_GAUDIBINACCs:
  case C_GAUDIBINACCsu:
  case C_GAUDIBINACCi:
  case C_GAUDIBINACCiu:
  case C_GAUDIBINACCl:
  case C_GAUDIBINACClu:
  case C_GAUDIBINACCf:
  case C_GAUDIBINACCd:      add_pair<int64_t>(summed_item, source_item);            break;
  default:
    printf("%s: Unknown data type to add...%x\n",this->name.c_str(), source_item->type);
    break;
  }
}

