//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#include <GauchoAppl/HistAdder.h>
#include <GauchoAppl/ClassDescriptor.h>
#include <Gaucho/MonHist.h>

Online::HistAdder::~HistAdder()  {
}

void Online::HistAdder::add_item(DimBuffBase* summed, const DimBuffBase* source)   {

  if ( source->type == C_STATENT )   {
    DimStatBuff *s = (DimStatBuff*)source;
    DimStatBuff *d = (DimStatBuff*)summed;
    d->nentries += s->nentries;
    d->sumw     += s->sumw;
    d->sumw2    += s->sumw2;
    d->min       = std::min(d->min,s->min);
    d->min       = std::max(d->max,s->max);
    return;
  }

  auto *sumh = (DimHistbuff1*)summed;
  auto *srch = (DimHistbuff1*)source;
  auto* ps   = add_ptr<double>(summed, summed->dataoff);
  auto* ph   = add_ptr<double>(source, source->dataoff);
  sumh->nentries += srch->nentries;
  sumh->min       = std::min(sumh->min, srch->min);
  sumh->max       = std::max(sumh->max, srch->max);
  sumh->yminval   = std::min(sumh->yminval, srch->yminval);
  sumh->ymaxval   = std::max(sumh->ymaxval, srch->ymaxval);
  sumh->sumw     += srch->sumw;
  sumh->sumw2    += srch->sumw2;
  sumh->sumwx    += srch->sumwx;
  sumh->sumwx2   += srch->sumwx2;
  sumh->sumwy    += srch->sumwy;
  sumh->sumwy2   += srch->sumwy2;
  sumh->sumwz    += srch->sumwt;
  sumh->sumwz2   += srch->sumwz2;
  sumh->sumwt    += srch->sumwt;
  sumh->sumwt2   += srch->sumwt2;

  int ndble = 0;
  switch(srch->type)    {
  case H_1DIM:
  case H_GAUDIH1F:
  case H_GAUDIH1D:
    ndble = 2*(srch->x.nbin+2);
    break;
  case H_2DIM:
  case H_GAUDIH2F:
  case H_GAUDIH2D:    {
    DimHistbuff2 *src = (DimHistbuff2*)srch;
    ndble = 2*(src->x.nbin+2)*(src->y.nbin+2);
    break;
  }
  case H_3DIM:
  case H_GAUDIH3F:
  case H_GAUDIH3D:   {
    DimHistbuff3 *sum = (DimHistbuff3*)sumh;
    DimHistbuff3 *src = (DimHistbuff3*)srch;
    sum->zminval = std::min(sum->zminval, src->zminval);
    sum->zmaxval = std::max(sum->zmaxval, src->zmaxval);
    ndble = 2*(src->x.nbin+2)*(src->y.nbin+2)*(src->z.nbin+2);
    break;
  }
  case H_PROFILE:
  case H_RATE:
  case H_GAUDIPR1:
    ndble = 4*(srch->x.nbin+2);
    break;
  case H_2DPROFILE:
  case H_GAUDIPR2:    {
    DimHistbuff3 *sum = (DimHistbuff3*)sumh;
    DimHistbuff3 *src = (DimHistbuff3*)srch;
    sum->zminval = std::min(sum->zminval, src->zminval);
    sum->zmaxval = std::max(sum->zmaxval, src->zmaxval);
    ndble = 4*(src->x.nbin+2)*(src->y.nbin+2);
    break;
  }
  default:
    ndble = 0;
    break;
  }
  switch(srch->type)     {
  case H_RATE:
    this->rateBuff = sumh;
    for (int icpy=0;icpy<5;icpy++)
      ps[icpy] += ph[icpy];
    for (int icpy=8;icpy<ndble;icpy++)
      ps[icpy] += ph[icpy];
    break;
  case H_GAUDIPR1:
  case H_GAUDIPR2:  {
    auto *psrc = add_ptr<GaudiProfileBin>(sumh,sumh->dataoff);
    auto *pdst = add_ptr<GaudiProfileBin>(srch,srch->dataoff);
    for (int i = 0;i<ndble;i++)   {
      pdst[i].nent  += psrc[i].nent;
      pdst[i].sumw  += psrc[i].sumw;
      pdst[i].sumw2 += psrc[i].sumw2;
    }
    break;
  }
  default:
    for (int icpy=0; icpy < ndble; icpy++)
      ps[icpy] += ph[icpy];
    break;
  }
}
