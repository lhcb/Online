//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#include "BusyPub.h"
#include "GaudiKernel/SmartIF.h"
#include "GaudiKernel/IIncidentSvc.h"
#include "GaudiKernel/IHistogramSvc.h"

#include "AIDA/IHistogram.h"
#include "Gaucho/MonCounter.h"

DECLARE_COMPONENT( BusyPub )

using namespace Online;

StatusCode BusyPub::start()   {
  StatusCode status = PubSvc::start();
  startCycling();
  return status;
}

StatusCode BusyPub::initialize()   {
  StatusCode sc = PubSvc::initialize();
  if (m_ForcePartition.size() == 0)   {
    this->m_ForcePartition = m_PartitionName;
  }
  //  DimServer::autoStartOn();
  m_PubDescrs.push_back(new PubDescr(std::string(""),std::string(m_PartitionName+"/FarmCPULoad")));
  m_PubDescrs.push_back(new PubDescr(std::string(""),std::string(m_PartitionName+"/UsedMemoryFraction")));
  m_PubDescrs.push_back(new PubDescr(std::string(""),std::string(m_PartitionName+"/BufferMemoryFraction")));
  m_PubDescrs.push_back(new PubDescr(std::string(""),std::string(m_PartitionName+"/SwapUsageFraction")));
  m_PubDescrs.push_back(new PubDescr(std::string(""),std::string(m_PartitionName+"/NumCores")));
  m_PubDescrs.push_back(new PubDescr(std::string(""),std::string(m_PartitionName+"/AvailableMemoryFraction")));
  for (size_t i = 0;i<m_NodeClasses.size();i++)
  {
    m_PubDescrs.push_back(new PubDescr(std::string(""),std::string(m_PartitionName+"/"+m_NodeClasses[i]+"/FarmCPULoad")));
    m_PubDescrs.push_back(new PubDescr(std::string(""),std::string(m_PartitionName+"/"+m_NodeClasses[i]+"/UsedMemoryFraction")));
    m_PubDescrs.push_back(new PubDescr(std::string(""),std::string(m_PartitionName+"/"+m_NodeClasses[i]+"/BufferMemoryFraction")));
    m_PubDescrs.push_back(new PubDescr(std::string(""),std::string(m_PartitionName+"/"+m_NodeClasses[i]+"/SwapUsageFraction")));
    m_PubDescrs.push_back(new PubDescr(std::string(""),std::string(m_PartitionName+"/"+m_NodeClasses[i]+"/NumCores")));
    m_PubDescrs.push_back(new PubDescr(std::string(""),std::string(m_PartitionName+"/"+m_NodeClasses[i]+"/AvailableMemoryFraction")));
  }
  return sc;
}

double &BusyPub::globalBogos()
{
  static double m_GlobalBogos;
  return m_GlobalBogos;
}

PubDescr::PubDescr(std::string insrv, std::string outsrv)
{
  setup(insrv,outsrv);
}

void PubDescr::update()
{
  m_outserv->updateService(m_outbuff);
}

void PubDescr::setup(std::string &inserv, std::string &outname)
{
  m_inserv = inserv;
  m_outbuff = -1.0;
  m_outName = outname;
  m_outserv = 0;
}

void PubDescr::service(DimServerDns *dns)
{
  if (m_outserv == 0)
  {
    m_outserv = new DimService(dns,m_outName.c_str(),m_outbuff);
  }
}

void BusyPub::analyze(mem_buff& ,MonitorItems* mmap)   {
  for (size_t i =0;i<m_PubDescrs.size();i++)
    m_PubDescrs[i]->service(this->dns());

  auto i = mmap->find("BusySvc/BusyFraction");
  if (i == mmap->end()) return;
  double bsy = (CounterSerDes::de_serialize((*i).second))->scalars.d_data;
  i = mmap->find("BusySvc/BogoMIPS");
  if (i == mmap->end()) return;
  double bogo = (CounterSerDes::de_serialize((*i).second))->scalars.d_data;
  i = mmap->find("BusySvc/NumCores");
  if (i == mmap->end()) return;
  long nCores = (CounterSerDes::de_serialize((*i).second))->scalars.i_data;
  i = mmap->find("BusySvc/TotMemory");
  long memtot = (CounterSerDes::de_serialize((*i).second))->scalars.l_data;
  i = mmap->find("BusySvc/MemAvail");
  long memavail = (CounterSerDes::de_serialize((*i).second))->scalars.l_data;
  i = mmap->find("BusySvc/FreeMemory");
  long memfree = (CounterSerDes::de_serialize((*i).second))->scalars.l_data;
  i = mmap->find("BusySvc/SwapSpaceTot");
  long swaptot = (CounterSerDes::de_serialize((*i).second))->scalars.l_data;
  i = mmap->find("BusySvc/SwapSpaceFree");
  long swapfree = (CounterSerDes::de_serialize((*i).second))->scalars.l_data;
  i = mmap->find("BusySvc/MemBuffers");
  long buffers = (CounterSerDes::de_serialize((*i).second))->scalars.l_data;
  m_PubDescrs[0]->m_outbuff = float(bsy/bogo);m_PubDescrs[0]->update();
  m_PubDescrs[1]->m_outbuff = 1.0-(float(memfree)/float(memtot));m_PubDescrs[1]->update();
  m_PubDescrs[2]->m_outbuff = (float(buffers)/float(memtot));m_PubDescrs[2]->update();
  m_PubDescrs[3]->m_outbuff = 1.0-(float(swapfree)/float(swaptot));m_PubDescrs[3]->update();
  m_PubDescrs[4]->m_outbuff = float(nCores);m_PubDescrs[4]->update();
  m_PubDescrs[5]->m_outbuff = (float(memavail)/float(memtot));m_PubDescrs[5]->update();
  for (size_t cli= 0;cli<m_NodeClasses.size();cli++)  {
    i = mmap->find("BusySvc/"+m_NodeClasses[cli]+"/BusyFraction");
    if (i == mmap->end()) continue;
    bsy = (CounterSerDes::de_serialize((*i).second))->scalars.d_data;
    i = mmap->find("BusySvc/"+m_NodeClasses[cli]+"/BogoMIPS");
    if (i == mmap->end()) continue;
    bogo = (CounterSerDes::de_serialize((*i).second))->scalars.d_data;
    i = mmap->find("BusySvc/"+m_NodeClasses[cli]+"/NumCores");
    if (i == mmap->end()) continue;
    nCores = (CounterSerDes::de_serialize((*i).second))->scalars.i_data;
    i = mmap->find("BusySvc/"+m_NodeClasses[cli]+"/TotMemory");
    memtot = (CounterSerDes::de_serialize((*i).second))->scalars.l_data;
    i = mmap->find("BusySvc/"+m_NodeClasses[cli]+"/MemAvail");
    memavail = (CounterSerDes::de_serialize((*i).second))->scalars.l_data;
    i = mmap->find("BusySvc/"+m_NodeClasses[cli]+"/FreeMemory");
    memfree = (CounterSerDes::de_serialize((*i).second))->scalars.l_data;
    i = mmap->find("BusySvc/"+m_NodeClasses[cli]+"/SwapSpaceTot");
    swaptot = (CounterSerDes::de_serialize((*i).second))->scalars.l_data;
    i = mmap->find("BusySvc/"+m_NodeClasses[cli]+"/SwapSpaceFree");
    swapfree = (CounterSerDes::de_serialize((*i).second))->scalars.l_data;
    i = mmap->find("BusySvc/"+m_NodeClasses[cli]+"/MemBuffers");
    buffers = (CounterSerDes::de_serialize((*i).second))->scalars.l_data;
    m_PubDescrs[(cli+1)*6+0]->m_outbuff = float(bsy/bogo);m_PubDescrs[(cli+1)*6+0]->update();
    m_PubDescrs[(cli+1)*6+1]->m_outbuff = 1.0-(float(memfree)/float(memtot));m_PubDescrs[(cli+1)*6+1]->update();
    m_PubDescrs[(cli+1)*6+2]->m_outbuff = (float(buffers)/float(memtot));m_PubDescrs[(cli+1)*6+2]->update();
    m_PubDescrs[(cli+1)*6+3]->m_outbuff = 1.0-(float(swapfree)/float(swaptot));m_PubDescrs[(cli+1)*6+3]->update();
    m_PubDescrs[(cli+1)*6+4]->m_outbuff = float(nCores);m_PubDescrs[(cli+1)*6+4]->update();
    m_PubDescrs[(cli+1)*6+5]->m_outbuff = (float(memavail)/float(memtot));m_PubDescrs[(cli+1)*6+5]->update();
  }
}

BusyPub::BusyPub(const std::string& name, ISvcLocator* sl) : PubSvc(name,sl)   {
  declareProperty("ForcePartition",  m_ForcePartition="");
  declareProperty("GlobalPublisher",m_GlobalPublisher=false);
  m_FarmLoad = 0;
  m_Cores = 0;
  m_NodeClasses.push_back("Slow");
  m_NodeClasses.push_back("Medium");
  m_NodeClasses.push_back("Fast");
  m_NodeClasses.push_back("Faster");
}
