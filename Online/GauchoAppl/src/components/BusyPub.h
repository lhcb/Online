//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef ONLINE_GAUCHO_BUSYPUB_H
#define ONLINE_GAUCHO_BUSYPUB_H

#include "GaudiKernel/Service.h"
#include "GaudiKernel/IToolSvc.h"
#include "GauchoAppl/MonAdder.h"
#include "PubSvc.h"
#include <string>
#include <vector>

// Forward declarations
class DimService;
class DimServerDns;

class PubDescr  {
  public:
    PubDescr(std::string insrv, std::string outsrv);
    DimService *m_outserv;
    std::string m_outName;
    std::string m_inserv;
    float m_outbuff;
    void update();
    void setup(std::string &insrv, std::string &outsrv);
    void service(DimServerDns *dns);
};

class BusyPub: public PubSvc  {
  public:
    BusyPub(const std::string& name, ISvcLocator* sl);
    virtual ~BusyPub() = default;
    StatusCode start() override;
    StatusCode initialize() override;
    void analyze(mem_buff& buffer, Online::MonitorItems* mmap)   override;
    DimService *m_FarmLoad;
    DimService *m_Cores;
    DimService *m_Moores;
    std::string m_ForcePartition;
    bool m_GlobalPublisher;
    static double &globalBogos();
    std::vector<PubDescr*> m_PubDescrs;
    std::vector<std::string> m_NodeClasses;
};
#endif // ONLINE_GAUCHO_BUSYPUB_H
