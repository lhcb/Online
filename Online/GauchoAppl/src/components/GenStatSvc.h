//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef ONLINE_GAUCHO_GENSTATSVC_H
#define ONLINE_GAUCHO_GENSTATSVC_H

#include <GauchoAppl/COutService.h>
#include "PubSvc.h"

// Forward declarations
class GenStatSvc : public PubSvc   {
public:
  std::string    m_prefix;
  COUTServiceMap m_outmap;

public:
  GenStatSvc(const std::string& name, ISvcLocator* svcloc);
  virtual ~GenStatSvc();
  StatusCode start() override;
  void analyze(mem_buff& buffer, Online::MonitorItems* mmap)  override;
};
#endif // ONLINE_GAUCHO_GENSTATSVC_H
