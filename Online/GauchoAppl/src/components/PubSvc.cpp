//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#include "PubSvc.h"
#include <GaudiKernel/IIncidentSvc.h>
#include <GaudiKernel/IHistogramSvc.h>
#include <Gaucho/IGauchoMonitorSvc.h>
#include <GauchoAppl/CounterAdder.h>
#include <GauchoAppl/SaveTimer.h>
#include <GauchoAppl/HistAdder.h>
#include <GauchoAppl/AdderSys.h>
#include <Gaucho/MonCounter.h>
#include <AIDA/IHistogram.h>
#include <RTL/strdef.h>

DECLARE_COMPONENT( PubSvc )

using namespace Online;

class PubSvc::MyErrh : public DimErrorHandler   {
public:
  bool m_flag;
  std::string name;
  void errorHandler(int severity, int code, char *msg) override  {
    if (m_flag)
      {
        ::lib_rtl_output(LIB_RTL_INFO,"DIM(%s): Code %d.%x %s\n",name.c_str(),severity,code,msg);
      }
    return;
  }
  MyErrh (const std::string& n) : DimErrorHandler(), name(n)  {
    m_flag = true;
  }
  void start()  {
    m_flag = true;
  }
  void stop()  {
    m_flag = false;
  }
};

PubSvc::PubSvc(const std::string& name, ISvcLocator* sl) 
  : Service(name,sl)
{
  declareProperty("MyName",          m_MyName         = "");
  declareProperty("InDNS",           m_InputDNS       = "");
  declareProperty("OutDNS",          m_OutputDNS      = "");
  declareProperty("PartitionName",   m_PartitionName  = "LHCb");
  declareProperty("TaskPattern",     m_TaskPattern    = "");
  declareProperty("ServicePattern",  m_ServicePattern = "");
  declareProperty("DebugOn",         m_debugOn        = false);
  declareProperty("DebugSys",        m_debugSys       = false);
  declareProperty("isEOR",           m_isEOR          = false);
  declareProperty("TrackSources",    m_trackSources   = false);
  declareProperty("AdderClass",      m_AdderClass     = "counter"); //Possible values are 'hists' for histigrams or 'counter' for counters.
}

PubSvc::~PubSvc()   {
}

StatusCode PubSvc::queryInterface(const InterfaceID& riid, void** ppvIF)  {
  if ( IIncidentListener::interfaceID().versionMatch(riid) )
    *ppvIF = (IIncidentListener*)this;
  else
    return this->Service::queryInterface(riid, ppvIF);
  addRef();
  return StatusCode::SUCCESS;
}

StatusCode PubSvc::initialize()   {
  StatusCode sc = this->Service::initialize();
  MsgStream msg( this->msgSvc(), this->name() );
  if( !sc.isSuccess() )  {
    return sc;
  }
  if ( this->m_TaskPattern.empty() )  {
    msg << MSG::FATAL << "The option \"TaskPattern\" MUST be set!" << endmsg;
    return StatusCode::FAILURE;
  }
  if ( this->m_ServicePattern.empty() )  {
    msg << MSG::FATAL << "The option \"ServicePattern\" MUST be set!" << endmsg;
    return StatusCode::FAILURE;
  }
  this->m_incidentSvc = this->service("IncidentSvc",true);
  if( !this->m_incidentSvc ) {
    return StatusCode::FAILURE;
  }
  this->m_incidentSvc->addListener(this,"APP_INITIALIZED");
  this->m_incidentSvc->addListener(this,"APP_RUNNING");
  this->m_incidentSvc->addListener(this,"APP_STOPPED");
  // Book arrival time histograms
  return StatusCode::SUCCESS;
}

StatusCode PubSvc::start()
{
  std::string servicename;
  std::string myservicename;
  std::string nodename = RTL::nodeNameShort();
  m_prevupdate = 0;
  StatusCode sc = Service::start();
  if ( !sc.isSuccess() )    {
    return sc;
  }
  if ( !m_errh )   {
    m_errh = new MyErrh(this->name());
  }
  m_TaskPattern    = RTL::str_lower(m_TaskPattern);
  m_ServicePattern = (m_ServicePattern);
  nodename         = RTL::str_lower(nodename);
  m_AdderClass     = RTL::str_lower(m_AdderClass);
  m_InputDNS       = RTL::str_lower(m_InputDNS);
  m_OutputDNS      = RTL::str_lower(m_OutputDNS);
  //
  // Nodeadders:
  //
  // Source task names:
  //  Reconstruction task structure: <Partition>_<Node>_RecBrunel_xx
  //  HLT Task structure: <node>_GAUCHOJOB_xx
  //  Nodeadder output name:
  //   Reconstruction: <part>_<node>_RecAdder_xx
  //   HLT: <node>_Adder_xx
  //
  // subfarm adders:
  // Source task names (all Farms):
  //  <node>_Adder_xx
  //  subfarm adder output name:
  //   Reconstruction: <part>_<node>_RecAdder_xx
  //   HLT: <sfnname>_Adder_xx     sfname = hltxyy
  //
  m_TaskPattern = RTL::str_replace(m_TaskPattern, "<node>", nodename);
  m_TaskPattern = RTL::str_replace(m_TaskPattern, "<part>", m_PartitionName);
  m_TaskPattern += "(.*)";
  m_ServicePattern = RTL::str_replace(m_ServicePattern, "<node>", nodename);
  m_ServicePattern = RTL::str_replace(m_ServicePattern, "<part>", m_PartitionName);
  m_myservicename = m_MyName;
  m_myservicename = RTL::str_replace(m_myservicename, "<part>", m_PartitionName);
  m_myservicename = RTL::str_replace(m_myservicename, "<node>", nodename);
  std::string ddns = RTL::str_lower(std::getenv("DIM_DNS_NODE") ? std::getenv("DIM_DNS_NODE") : "");
  m_InputDNS = RTL::str_replace(m_InputDNS, "<node>", nodename);
  m_InputDNS = RTL::str_replace(m_InputDNS, "<dns>",  ddns);
  if ( !m_OutputDNS.empty() )  {
    m_OutputDNS = RTL::str_replace(m_OutputDNS, "<node>", nodename);
    m_OutputDNS = RTL::str_replace(m_OutputDNS, "<dns>", ddns);
  }
  else  {
    m_OutputDNS = ddns;
  }
  m_errh->start();
  if (m_started) return StatusCode::SUCCESS;
  if (m_errh != 0) DimClient::addErrorHandler(m_errh);
  ::lib_rtl_output(LIB_RTL_DEBUG,"=======>PubSvc Option Summary:\tTask Pattern %s\n",m_TaskPattern.c_str());
  ::lib_rtl_output(LIB_RTL_DEBUG,"        Service Pattern %s+Data or EOR\n",m_ServicePattern.c_str());

  DimServer::autoStartOn();
  if ( !m_InputDNS.empty() )   {
    DimClient::setDnsNode(m_InputDNS.c_str());
  }
  if ( m_AdderClass == "counter" )
    m_adder = std::make_unique<CounterAdder>(m_myservicename, "Data", ADD_COUNTER);
  else
    m_adder = std::make_unique<HistAdder>(m_myservicename, "Data", ADD_HISTO);

  auto dns = AdderSys::instance().getDNS(m_OutputDNS,m_myservicename);
  if ( m_trackSources ) AdderSys::instance().trackSources();
  m_adder->serviceDNS = dns;
  m_adder->isSaver        = false;
  m_adder->isEOR          = m_isEOR;
  m_adder->taskPattern    = m_TaskPattern;
  m_adder->servicePattern = m_ServicePattern + (m_isEOR ? "EOR" : "Data");
  m_adder->rectmo         = 0;
  m_adder->disableOutput  = true;
  m_adder->noRPC          = true;
  m_adder->debugOn        = m_debugOn;
  m_adder->trackSources   = m_trackSources;
  AdderSys::instance().debugOn = m_debugSys;
  m_adder->configure();
  dns->autoStartOn();
  m_started = true;
  return StatusCode::SUCCESS;
}

StatusCode PubSvc::stop()    {
  SmartIF<IGauchoMonitorSvc> psvc(m_pMonitorSvc);
  if (psvc) psvc->resetHistos(this);

  m_errh->stop();
  return Service::stop();
}

StatusCode PubSvc::finalize()   {
  class DimLock {
  public:
    DimLock()   { dim_lock();   }
    ~DimLock()  { dim_unlock(); }
  };
  m_arrhist = 0;
  if ( m_pMonitorSvc )  {
    m_pMonitorSvc->undeclareAll(this);
  }
  if ( m_incidentSvc )  {
    m_incidentSvc->removeListener(this);
  }
  this->m_incidentSvc.reset();
  this->m_pMonitorSvc.reset();
  if (m_SaveTimer != 0)  {
    m_SaveTimer->stop();
    m_SaveTimer.reset();
  }
  {
    DimLock lock;
    AdderSys::instance().stop();
  }
  if (m_adder != 0)  {
    m_adder.reset();
  }
  if (m_EoRadder != 0)  {
    m_EoRadder.reset();
  }
  return Service::finalize();
}

SmartIF<IToolSvc> PubSvc::tools()    {
  auto svc = service<IToolSvc>("ToolSvc");
  if ( !svc.get() )   {
    ::lib_rtl_output(LIB_RTL_FATAL,"%s: Failed to access ToolsSvc.", this->name().c_str());
    throw std::runtime_error(this->name()+": Failed to access ToolsSvc.");
  }
  return svc;
}

void PubSvc::handle(const Incident& inc)   {
  if (inc.type() == "APP_RUNNING")  {
    AdderSys::instance().start();
  }
  else if (inc.type() == "APP_STOPPED")  {
    //AdderSys::instance().stop();
  }
}

namespace   {
  void _Analyze(void *arg, mem_buff& buff, MonitorItems *mmap, MonAdder *)  {
    PubSvc *tis = (PubSvc*) arg;
    tis->analyze(buff, mmap);
  }
}

void PubSvc::startCycling()    {
  if ( this->m_adder )    {
    this->m_adder->SetCycleFn(_Analyze, this);
  }
}

void PubSvc::analyze(mem_buff& , Online::MonitorItems* )   {
}
