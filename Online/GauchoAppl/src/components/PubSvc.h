//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef ONLINE_GAUCHO_PUBSVC_H
#define ONLINE_GAUCHO_PUBSVC_H

#include <GaudiKernel/Service.h>
#include <GaudiKernel/IIncidentListener.h>
#include <GauchoAppl/MonAdder.h>
#include <dim/dis.hxx>

// Forward declarations
class IMonitorSvc;
class IIncidentSvc;
class IGauchoMonitorSvc;
class IHistogramSvc;

class DimServiceDns;
class COutServiceBase;
namespace Online   {
  class mem_buff;
  class SaveTimer;
}

class PubSvc : public Service, virtual public IIncidentListener    {
public:
  class MyErrh;
  typedef Online::mem_buff mem_buff;
  typedef std::map<std::string, COutServiceBase*> COUTServiceMap;

public:
  PubSvc(const std::string& name, ISvcLocator* sl);
  virtual ~PubSvc();
  //IInterface pure member functions
  virtual StatusCode queryInterface(const InterfaceID& riid, void** ppvIF) override;
  virtual StatusCode initialize() override;
  virtual StatusCode start() override;
  virtual StatusCode stop() override;
  virtual StatusCode finalize() override;
  virtual void handle(const Incident&) override;

  virtual void analyze(mem_buff& buff, Online::MonitorItems* mmap);
  void startCycling();

  SmartIF<IToolSvc> tools();

  DimServerDns* dns() const {  return this->m_adder->serviceDNS.get();   }
  /// Reference to the IncidentSvc instance
  SmartIF<IIncidentSvc> m_incidentSvc;
  SmartIF<IMonitorSvc>  m_pMonitorSvc;

  std::unique_ptr<Online::MonAdder>  m_adder;
  std::unique_ptr<Online::MonAdder>  m_EoRadder;
  std::unique_ptr<Online::SaveTimer> m_SaveTimer;
  std::unique_ptr<Online::SaveTimer> m_EoRSaver;

  AIDA::IHistogram1D *m_arrhist      {nullptr};
  MyErrh             *m_errh         {nullptr};

  std::string m_InputDNS;
  std::string m_OutputDNS;
  std::string m_PartitionName;
  std::string m_MyName;
  std::string m_TaskPattern;
  std::string m_ServicePattern;
  std::string m_AdderClass;
  std::string m_myservicename;
  std::string m_prefix;
  unsigned long long  m_prevupdate  {0};
  int  m_recvtmo;
  bool m_started         { false };
  bool m_isEOR           { false };
  bool m_debugOn         { false };
  bool m_debugSys        { false };
  bool m_trackSources    { false };
};
#endif // ONLINE_GAUCHO_PUBSVC_H
