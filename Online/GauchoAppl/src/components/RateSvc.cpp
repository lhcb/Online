//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#include "RateSvc.h"
#include <Gaucho/Utilities.h>
#include <Gaucho/MonCounter.h>
#include <GauchoAppl/RateService.h>
#include <RTL/strdef.h>

DECLARE_COMPONENT( RateSvc )
#define MAX_SERVICE_SIZE 128

using namespace Online;

void RateSvc::analyze(mem_buff& ,MonitorItems* mmap)    {
  makerate(mmap);
}

void RateSvc::makerate(MonitorItems* mmap)  {
  double rate;
  for (const auto& it : *mmap )  {
    const DimBuffBase *m_RateBuff = it.second;
    if (m_RateBuff != 0)    {
      std::string nams = it.first;
      if ( nams.substr(0,2) == "R_" )     {
        switch ( m_RateBuff->type )     {
          case C_RATEFLOAT:    {
            const float *a = add_ptr<float>(m_RateBuff,m_RateBuff->dataoff);
            rate = *a;
            break;
          }
          case C_RATEDOUBLE:    {
            const double *a = add_ptr<double>(m_RateBuff,m_RateBuff->dataoff);
            rate = *a;
            break;
          }
          case C_DOUBLE:      {
            const double *a = add_ptr<double>(m_RateBuff,m_RateBuff->dataoff);
            rate = *a;
            break;
          }
          default:       {
            continue;
          }
        }
        OUTServiceDescr *outs = findOUTService(nams);
        RateService *s = 0;
        if (outs != 0)    {
          s = outs->m_svc;
        }
        else     {
          DimServer::autoStartOn();
	  std::string sname = m_NamePrefix + nams;
          if (sname.size() < MAX_SERVICE_SIZE)    {
            s = new RateService(sname, "D:1");
            outs = new OUTServiceDescr(nams, new RateService(sname, "D:1"));
            this->m_outputServicemap.emplace(nams, outs);
	    s = outs->m_svc;
          }
        }
        if ( s ) s->update(sizeof(rate));
      }
    }
  }
}

template <typename T> void RateSvc::update_rate(const DimBuffBase* b)   {
  const char *n = add_ptr<char>(b, b->nameoff);
  const void *dat = add_ptr(b, b->dataoff);
  std::string snam = m_prefix+"/"+n;
  auto it = this->m_outmap.find(snam);
  COutServiceBase *ob = (it == m_outmap.end()) ? nullptr : it->second;
  COutService<T> *os = nullptr;
  if (ob == 0)      {
    ob = new COutService<T>(snam);
    m_outmap[snam] = ob;
  }
  os = (COutService<T> *)ob;
  const T *d = (const T*)dat;
  os->Update(*d);
}

void RateSvc::makecounters(MonitorItems* mmap)
{
  for (const auto& it : *mmap )  {
    const DimBuffBase *b = it.second;
    switch(b->type)    {
    case   C_INT:
      this->update_rate<int>(b);
      break;
    case   C_LONGLONG:
      this->update_rate<long long>(b);
      break;
    case   C_FLOAT:
      this->update_rate<float>(b);
      break;
    case   C_DOUBLE:
      this->update_rate<double>(b);
      break;
    default:
      break;
    }
  }
}

RateSvc::RateSvc(const std::string& name, ISvcLocator* sl) : PubSvc(name,sl)  {
  declareProperty("ServicePrefix", m_prefix         = "");
}

OUTServiceDescr *RateSvc::findOUTService(std::string servc)   {
  auto i = m_outputServicemap.find(servc);
  return (i == m_outputServicemap.end()) ? nullptr : i->second;
}

StatusCode RateSvc::start()   {
  StatusCode sc = PubSvc::start();
  m_prefix = RTL::str_replace(m_prefix,"<part>",m_PartitionName);
  if (sc.isSuccess() )  {
    startCycling();
  }
  m_started = true;
  m_NamePrefix = m_PartitionName+"_";
  return sc;
}

StatusCode RateSvc::stop()   {
  return PubSvc::stop();
}

