//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
/*
 * ReadCheckTsts.cpp
 *
 *  Created on: Aug 7, 2017
 *      Author: beat
 */


#include "stdio.h"
#include "Gaucho/SegvHandler.h"
int main(int , char**)
{
  void *ptr= (void*)6;
  int r;
  r = CheckRead(ptr);
  if (r == 0)
  {
    printf("Location %p readable\n",ptr);
  }
  else
  {
    printf("Location %p NOT readable\n", ptr);
  }
  ptr= (void*)&ptr;
  r = CheckRead(ptr);
  if (r == 0)
  {
    printf("Location %p readable\n",ptr);
  }
  else
  {
    printf("Location %p NOT readable\n", ptr);
  }
  return 0;
}


