//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
/*
 * segvtst.cpp
 *
 *  Created on: Oct 10, 2011
 *      Author: Beat Jost
 */
#include <cstdio>
#include <Gaucho/SegvHandler.h>

int main(int , char **)    {
  SegvHandler::instance();
//  M_SegvHandler->oldact = M_SegvHandler->oldact;
  int *p = (int*)7;
  int  i = *p;
  std::printf("%d\n",i);
  return 0;
}
