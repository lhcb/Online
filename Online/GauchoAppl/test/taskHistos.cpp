//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================

/// Framework include files
#include <Gaucho/HistTask.h>
#include <RTL/rtl.h>
#include <TH2D.h>
#include <TRint.h>
#include <TProfile.h>
#include <TBrowser.h>

/// C/C++ include files
#include <iostream>
#include <iomanip>

using namespace Online;

static void print_tasks(const std::set<std::string>& tasks)   {
  size_t i = 0;
  for( const auto& t : tasks )
    ::printf("Task[%3ld]  %s\n", ++i, t.c_str());
}

int main(int argc, char *argv[])   {
  RTL::CLI cli(argc, argv, [] () {
      std::cout 
	<< " Usage: taskHistos -arg [-arg]                                          \n"
	<< "  -d(ns)=<dns>         DNS name to interrogate for services.            \n"
	<< "  -t(ask)=<task-name>  Supply task name for detailed counter printout.  \n"
	<< "  -o(ne)               Get histograms 'one' by 'one'.                   \n"
	<< "  -s(how)              Display the retrieved histograms.                \n"
	<< std::endl;
    });
  std::string dns, task;
  bool show = cli.getopt("show",1);
  bool help = cli.getopt("help",1);
  bool one  = cli.getopt("one",1);
  cli.getopt("task", 1, task);
  cli.getopt("dns",  1, dns);
  if ( help )  {
    cli.call_help();
  }
  if ( dns.empty() )  {
    std::cout << "Insufficient Number of arguments" << std::endl;
    cli.call_help();
  }

  TH1D::SetDefaultSumw2();
  TH2D::SetDefaultSumw2();
  TProfile::SetDefaultSumw2();

  if ( task.empty() )   {
    std::set<std::string> tasks;
    HistTask::TaskList(dns,tasks);
    print_tasks(tasks);
  }
  else   {
    auto h = std::make_unique<HistTask>(task, dns);
    std::vector<std::string> hists;
    hists.clear();
    int status = h->Directory(hists);
    if (status == 1)  {
      printf("Task does not exist...\n");
      return 1;
    }
    for (std::size_t i=0; i < hists.size(); i++)
      std::cout << hists[i] << std::endl;

    std::cout << "Number of Histograms for Task " << task << ": " << hists.size() << std::endl;

    std::cout << "Trying to retrieve the histograms" << std::endl;
    std::size_t count = 0;
    while ( !hists.empty() )   {
      std::vector<std::string> hsts;
      std::size_t num_hist = one ? 1 : hists.size();

      hsts.insert(hsts.end(), hists.begin(), hists.begin()+num_hist);
      hists.erase(hists.begin(), hists.begin()+num_hist);

      std::vector<TObject*> robjs;
      h->Histos(hsts, robjs);
      std::cout << "Retrieved " << robjs.size() << " Histograms" << std::endl;
      for (std::size_t i=0; i < robjs.size(); i++)      {
	std::cout << "Histogram [" << count << "]: " << hsts[i]
		  << " at address " << (void*)robjs[i];
	std::cout << std::endl;
	++count;
      }
      if ( show )    {
	int one_arg = 1;
	char cmd[256];
	TRint theApp("App", &one_arg, argv);
	::lib_rtl_sleep(100);
	::snprintf(cmd,sizeof(cmd),
		   "std::vector<TH1*>& gHistos = *(std::vector<TH1*>*)%p;",
		   (void*)&robjs);
	gInterpreter->ProcessLine(cmd);
	std::cout << std::endl
		  << "***** Use the ROOT interpreter variable " << std::endl
		  << "*****      'std::vector<TH1*>& gHistos'"  << std::endl
		  << "***** to access and draw histograms!"     << std::endl;
	new TBrowser();
	theApp.Run();
      }
    }
  }
  return 0;
}
