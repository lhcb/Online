#===============================================================================
#  LHCb Online software suite
#-------------------------------------------------------------------------------
# Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
# All rights reserved.
#
# For the licensing terms see OnlineSys/LICENSE.
#
# Author     : M.Frank
#
#===============================================================================
#[=======================================================================[.rst:
Online/GauchoBase
-----------------
#]=======================================================================]

find_package(TBB REQUIRED)

file(GLOB sources src/*.cpp utils/*.cpp)

online_library(GauchoBase ${sources})
target_link_libraries(GauchoBase
  PUBLIC    nlohmann_json::nlohmann_json
	    AIDA::aida
            ROOT::Hist
            Online::dim
            Online::OnlineBase
  PRIVATE   ROOT::RIO
            ROOT::Core
)

macro(rpc_exec name call_def)
  online_executable(${name} main/gaucho_app.cpp)
  target_compile_definitions(${name} PRIVATE -DGAUCHO_APP_CALL=${call_def})
  target_link_libraries(${name} PRIVATE Online::dim Online::OnlineBase Online::GauchoBase ROOT::Core TBB::tbb -lrt)
endmacro()

rpc_exec(taskHistosJson.exe     gaucho_app_task_histos_json)
rpc_exec(taskCountersJson.exe   gaucho_app_task_counters_json)
rpc_exec(taskCounters.exe       gaucho_app_task_counters)
rpc_exec(taskGauchoExtract.exe  gaucho_app_task_extract)
rpc_exec(counterDeserialize.exe gaucho_app_counter_deserialize)
rpc_exec(histoDeserialize.exe   gaucho_app_histogram_deserialize)
