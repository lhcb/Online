//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef ONLINE_GAUCHO_BRTL_LOCK_H
#define ONLINE_GAUCHO_BRTL_LOCK_H

#include <pthread.h>
#include <string>
#include <chrono>

/// Online namespace declaration
namespace Online   {

  class BRTLLock   {
  private:
    pthread_mutex_t lid;
  public:
    std::string name;
    std::chrono::high_resolution_clock::time_point tryLockTime;
    std::chrono::high_resolution_clock::time_point succLockTime;
    std::chrono::high_resolution_clock::time_point unLockTime;

    BRTLLock(void);
    ~BRTLLock(void);
    int lockMutex();
    int unlockMutex();

  public:
    struct _Lock   {
      BRTLLock* mtx;
      _Lock(BRTLLock* m) : mtx(m) { if ( mtx ) mtx->lockMutex();   }
      ~_Lock()                    { if ( mtx ) mtx->unlockMutex(); }
    };
  };
}

#endif /* ONLINE_GAUCHO_BRTL_LOCK_H */
