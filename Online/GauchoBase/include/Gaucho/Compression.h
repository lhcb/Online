//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//
//  \author   Markus Frank
//  \date     2024-03-13
//  \version  1.0
//
//==========================================================================
#ifndef GAUCHO_COMPRESSION_H 
#define GAUCHO_COMPRESSION_H 1

#include <vector>
#include <cstdint>
#include <algorithm>

/// Online namespace declaration
namespace Online   {
  
  /// Gaucho namespace declaration
  namespace gaucho   {
    /// Compress a byte buffer
    std::pair<std::size_t, void*>
    compress(const void* data, std::size_t data_len, std::vector<uint8_t>& buffer);
    /// Inflate the response
    std::pair<std::size_t, void*>
    decompress(const void* data, std::size_t data_len, std::vector<uint8_t>& buffer);

  }    // End namespace gaucho
}      // End namespace Online
#endif // GAUCHO_COMPRESSION_H
