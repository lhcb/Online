//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef ONLINE_GAUCHO_COUNTERDESERIALIZE_H
#define ONLINE_GAUCHO_COUNTERDESERIALIZE_H

/// C/C++ include files
#include <string>
#include <memory>
#include <algorithm>

/// Online namespace declaration
namespace Online  {

  class CntrDescr  {
  public:
    std::string name      = "";
    unsigned int type     = 0;

    /// Union containing scalars. Everything smaller than int *is* int
    union _scalars  {
      int           i_data;
      unsigned int  ui_data;
      long          l_data;
      unsigned long ul_data;
      float         f_data;
      double        d_data;
      _scalars() { this->ul_data = 0; }
    } scalars;

    /// Array holder
    std::unique_ptr<unsigned char[]> ptr {};
    int nel               = 0;

    /// Stuff for pairs
    std::pair<int,int>   ip_data;
    std::pair<long,long> lp_data;

    /// Union containing gaudi accumulator structures
    union  {
      struct _avg_data {
	long   entries;
	double sum;
	double mean;
      } average;
      struct _stat_data {
	long   entries;
	double sum;
	double sum2;
	double mean;
      } stat;
      struct _sigma_data {
	long   entries;
	double sum;
	double sum2;
	double mean;
      } sigma;
      struct _binomial_data {
	long   entries;
	long   entries_true;
	long   entries_false;
      } binomial;
    } gaudi;
    CntrDescr() = default;
    ~CntrDescr() = default;
  };

  class CounterSerDes  {
  public:
    static CntrDescr *de_serialize(const void* pointer, const char* name = 0);
  };
}
#endif // ONLINE_GAUCHO_COUNTERDESERIALIZE_H
