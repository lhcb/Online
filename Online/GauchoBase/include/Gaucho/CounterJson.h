//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef ONLINE_GAUCHO_COUNTERJSON_H
#define ONLINE_GAUCHO_COUNTERJSON_H

#include <nlohmann/json.hpp>

/// Online namespace declaration
namespace Online  {
  namespace JsonCounterDeserialize   {
    using json = nlohmann::json;
    json de_serialize(const void* ptr);
  }
}
#endif // ONLINE_GAUCHO_COUNTERDESERIALIZE_H
