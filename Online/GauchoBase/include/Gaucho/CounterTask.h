//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef ONLINE_GAUCHO_COUNTERTASK_H_
#define ONLINE_GAUCHO_COUNTERTASK_H_

#include <Gaucho/TaskRPC.h>

/// Online namespace declaration
namespace Online  {

  class CntrDescr;

  class CounterTask : public TaskRPC  {
  public:
    CounterTask(const std::string& task,const std::string& dns="", int tmo=3);
    virtual ~CounterTask() = default;
    int counters(const std::vector<std::string>& names)   {  return this->items(names);  }
    int counters(const std::vector<std::string>& names, std::vector<CntrDescr*>& Cntrs);
    int counters(const std::vector<std::string>& names, std::map<std::string,CntrDescr*>& Cntrs);

    static std::pair<int, std::set<std::string> > taskList(const std::string& dns);
    template <typename CONT> static int taskList(const std::string& dns, CONT& tasks)
      {   return TaskRPC::_taskList("Counter", dns, tasks);  }
  };
}
#endif // ONLINE_GAUCHO_COUNTERTASK_H_
