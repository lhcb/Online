//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef ONLINE_GAUCHO_HISTJSON_H
#define ONLINE_GAUCHO_HISTJSON_H

/// Framework include files
#include <nlohmann/json.hpp>

/// Online namespace declaration
namespace Online  {

  namespace JsonHistDeserialize   {
    using json = nlohmann::json;
    json de_serialize(const void* ptr);
  }
}
#endif // ONLINE_GAUCHO_HISTJSON_H
