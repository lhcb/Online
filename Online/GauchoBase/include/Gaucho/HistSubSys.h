//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef HISTSUBSYS_H
#define HISTSUBSYS_H

#include "Gaucho/MonSubSys.h"
#include "Gaucho/MonHist.h"

/// Online namespace declaration
namespace Online  {

  class HistSubSys: public MonSubSys  {
  private:
    int    updatePeriod;

    void i_addEntry(std::unique_ptr<MonBase>&& histo);

  public:
    template<typename T> void add_hist(const std::string& nam, const std::string &desc, const T&  var)  {
      i_addEntry(std::make_unique<MonHist<T> >(nam,desc,var));
    }
    HistSubSys(SubSysParams& params);
    virtual ~HistSubSys() = default;
    void setup(const std::string& n, bool expand=false) override;
    void setExtLastDelta(unsigned long deltaT);
    virtual size_t remove(const std::string& name)  override;
    virtual size_t removeAll(const std::string &owner_name)   override;
  };
}
#endif
