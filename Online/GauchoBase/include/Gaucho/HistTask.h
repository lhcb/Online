//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef ONLINE_GAUCHO_HISTTASK_H_
#define ONLINE_GAUCHO_HISTTASK_H_

#include <Gaucho/TaskRPC.h>

class TObject;

/// Online namespace declaration
namespace Online  {

  class HistTask : public TaskRPC  {
  protected:
    int Histos(const std::vector<std::string>& names)    {  return this->items(names);  }
  public:
    HistTask(const std::string& task, const std::string& dns = "", int tmo = 3);
    virtual ~HistTask();
    int Directory(std::vector<std::string>& names)   {  return this->directory(names);  } 
    int Histos(const std::vector<std::string>& hists,std::vector<TObject*>& histos);
    int Histos(const std::vector<std::string>& hists,std::map<std::string,TObject*>& histos);
    template <typename CONT> static int TaskList(const std::string& dns, CONT& tasks)
      {   return TaskRPC::_taskList("Histos", dns, tasks);  }
    template <typename CONT> static int taskList(const std::string& dns, CONT& tasks)
      {   return TaskRPC::_taskList("Histos", dns, tasks);  }
  };
}
#endif /* ONLINE_GAUCHO_HISTTASK_H_ */
