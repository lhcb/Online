//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#pragma once

#include <Gaucho/MonTypes.h>
#include <Gaucho/dimhist.h>

#include <string>

/// Online namespace declaration
namespace Online  {

  template<typename...> inline constexpr bool false_v = false;

  class MonBase   {
  public:
    MONTYPE      type       { H_ILLEGAL };
    std::string  name       {   };
    std::string  title      {   };
    std::size_t  buffersize { 0 };  /* buffer size of the data if serialized*/
    std::size_t  m_contsiz  { 0 };  /* Allocated length in Bytes for the bin contents */

  public:
    MonBase() = delete;
    MonBase(MONTYPE typ, const std::string& nam, const std::string& tit,
            std::size_t bufsize=0, std::size_t contsiz=0);
    virtual ~MonBase() = default;

    void *cpyName(void *p) const    {
      ::memcpy(p, name.c_str(), name.length() + 1);
      return add_ptr(p, name.length());
    }
    void *cpytitle(void *p) const    {
      ::memcpy(p, title.c_str(), title.length() + 1);
      return add_ptr(p, title.length());
    }
    int title_length() const    {
      return title.length() + 1;
    }
    int name_length() const    {
      return this->name.length() + 1;
    }
    int datasize() const    {
      return buffersize;
    }
    virtual int hdrlen() const    {
      int s = sizeof(DimBuffBase) + title_length() + 1 + name_length() + 1;
      s = (s + 7) & ~7;   //align to 8-byte boundary...
      return s;
    }
    virtual int   xmitbuffersize() = 0;
    virtual int   serialize(void* ptr)  = 0;
    virtual void  create_OutputService(const std::string& connection) = 0;
    virtual void  delete_OutputService() {}
    virtual void  update_OutputService() {}
    virtual void  reset()                {}
  };

  inline MonBase::MonBase(MONTYPE typ, const std::string& nam, const std::string& tit,
			  std::size_t bufsize, std::size_t contsiz) :
    type(typ), name(nam), title(tit),
    buffersize(bufsize), m_contsiz(contsiz)
  {
  }
}
