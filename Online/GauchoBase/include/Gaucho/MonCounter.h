//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef ONLINE_GAUCHO_MONCOUNTER_H
#define ONLINE_GAUCHO_MONCOUNTER_H

/// Framework include files
#include <Gaucho/MonTypes.h>
#include <Gaucho/MonBase.h>
#include <Gaucho/Utilities.h>
#include <Gaucho/CounterDeserialize.h>
#include <CPP/Value.h>
#include <dim/dis.hxx>

/// C/C++ include files
#include <vector>
#include <string>
#include <atomic>
#include <memory>

class DimService;

/// Online namespace declaration
namespace Online  {

  class MonCounterBase : public MonBase {
  protected:
    const void*                      m_contents      { nullptr };
    std::unique_ptr<unsigned char[]> counterData     {   };
    std::unique_ptr<DimService>      dim_service     {   };
    std::string                      m_srvcprefix    {   };
    std::string                      m_fmt           {   };
    long                             m_atomData      { 0 };
    bool                             m_expandService { false };

  public:
    /// Inhibit default constructor
    MonCounterBase() = delete;
    /// Initializing constructor
    MonCounterBase(MONTYPE type, const std::string& name, const std::string& title,
                   std::size_t bufsize, std::size_t contsiz, const void *contents,
                   bool expandService=true, std::string fmt="");
    /// Default destructor
    virtual ~MonCounterBase() = default;
    /// Reset counter data
    virtual void reset()  override    {
    }
    /// Wire buffer size
    virtual int xmitbuffersize()  override    {
      return (this->m_contents == 0) ? 0 : this->hdrlen() + this->datasize();
    }
    void SetExpand(bool expand){
      this->m_expandService = expand;
    }
    virtual int serializeHeader(DimBuffBase* b)  const   {
      if (this->m_contents != 0)  {
        int hdrl   = this->hdrlen();
        b->type    = this->type;
        b->reclen  = hdrl + this->datasize();
        b->titlen  = this->title_length();
        b->namelen = this->name_length();
        b->nameoff = sizeof(DimBuffBase);
        b->titoff  = b->nameoff + b->namelen;
        b->dataoff = hdrl;
        cpyName(add_ptr<char>(b, b->nameoff));
        cpytitle(add_ptr<char>(b, b->titoff));
        return hdrl;
      }
      return 0;
    }

    /// Output serialization data call
    virtual unsigned int serializeData(DimBuffBase* ptr, const void *src,int len) const = 0;

    /// Input serialization data call
    virtual int serialize(void* ptr)  override  {
      DimBuffBase *tptr = (DimBuffBase*)ptr;
      if ( this->serializeHeader(tptr) )   {
        unsigned int retval = this->serializeData(tptr, m_contents, m_contsiz);
        if ( retval != 0 && this->counterData )   {
          ::memcpy(this->counterData.get(), add_ptr(tptr, tptr->dataoff), this->buffersize);
        }
        return retval;
      }
      return 0;
    }
    /// Delete output service on demand
    virtual void delete_OutputService() override;
    /// Update eoutput service on demand
    virtual void update_OutputService() override;
  };

  /// Initializing constructor
  inline MonCounterBase::MonCounterBase(MONTYPE typ, const std::string& nam, const std::string& tit,
                                        std::size_t bufsize, std::size_t contsiz, const void *contents,
                                        bool expandService, std::string fmt) :
    MonBase{typ, nam, tit, bufsize, contsiz}, m_contents(contents), m_fmt(fmt), m_expandService(expandService)
  {
    if (this->buffersize != 0) this->counterData.reset(new unsigned char[this->buffersize]);
  }

  template <typename T> struct MonCounter : MonCounterBase {
  public:
    /// Initializing constructor
    MonCounter(const std::string& name, const std::string& title, const T& data);
    /// Counter type
    constexpr static MONTYPE Type();
    /// Data size
    constexpr static unsigned int serialSize()  {  return sizeof(T);  }
    /// Output serialization data callback
    virtual unsigned int serializeData(DimBuffBase* ptr, const void *src, int len) const override;
    /// Create output service on demand
    virtual void         create_OutputService(const std::string& infix)  override;
  };
}

// Specializations of MonCounter for different types
#include <Gaucho/MonCounterScalars.tcc.h>
#include <Gaucho/MonCounterArrays.tcc.h>
#include <Gaucho/MonCounterAtomic.tcc.h>
#include <Gaucho/MonCounterPair.tcc.h>
#endif // ONLINE_GAUCHO_MONCOUNTER_H
