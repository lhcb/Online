//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================

/// Online namespace declaration
namespace Online  {

  /// specialization of MonCounter class for array types
  template <typename T> struct MonCounter<T*> : MonCounterBase {

    MonCounter(const std::string& nam, const std::string& tit,const std::string& fmt, T* const &data , std::size_t siz)
      : MonCounterBase {
          Type(), nam, tit, siz, siz, 
          // siz*std::max(sizeof(T), sizeof(int32_t)),
          // siz*std::max(sizeof(T), sizeof(int32_t)),
          data, false, fmt
        }
    {
    }

    MONTYPE Type() const {
      if constexpr (std::is_same_v<T, int8_t>) {
        return C_INTSTAR;
      } else if constexpr (std::is_same_v<T, uint8_t>) {
        return C_UINTSTAR;
      } else if constexpr (std::is_same_v<T, int16_t>) {
        return C_INTSTAR;
      } else if constexpr (std::is_same_v<T, uint16_t>) {
        return C_UINTSTAR;
      } else if constexpr (std::is_same_v<T, int32_t>) {
        return C_INTSTAR;
      } else if constexpr (std::is_same_v<T, uint32_t>) {
        return C_UINTSTAR;
      } else if constexpr (std::is_same_v<T, int64_t>) {
        return C_LONGSTAR;
      } else if constexpr (std::is_same_v<T, uint64_t>) {
        return C_ULONGSTAR;
      } else if constexpr (std::is_same_v<T, long long>) {
        return C_LONGSTAR;
      } else if constexpr (std::is_same_v<T, unsigned long long>) {
        return C_ULONGSTAR;
      } else if constexpr (std::is_same_v<T, float>) {
        return C_FLOATSTAR;
      } else if constexpr (std::is_same_v<T, double>) {
        return C_DOUBLESTAR;
      } else {
        static_assert(false_v<T>, "Unsupported type for array specialization of MonCounter");
      }
    }

    constexpr unsigned int serialSize() const {  return 0;  }

    unsigned int serializeData(DimBuffBase *pp, const void *src, int len)  const override {
      ::memcpy(add_ptr(pp, pp->dataoff), src, len);
      return pp->reclen;
    }

    void create_OutputService(const std::string & infix) override {
      this->dim_service = 0;
      std::string nam = m_srvcprefix + infix + this->name;
      if ( nam.length() <= 128 /* maximum length of a DIM service Name*/ ) {
        this->dim_service.reset(new DimService(nam.c_str(), (char*) m_fmt.c_str(), (void*) m_contents, m_contsiz));
      }
    }
  };

  template <> inline MonCounter<int8_t*>::MonCounter(const std::string& n, const std::string& t,const std::string& f, int8_t* const &d , std::size_t s)
    : MonCounterBase {Type(), n, t, s*sizeof(int32_t)/sizeof(int8_t), s*sizeof(int32_t)/sizeof(int8_t), d, false, f}  {}
  template <> inline MonCounter<uint8_t*>::MonCounter(const std::string& n, const std::string& t,const std::string& f, uint8_t* const &d , std::size_t s)
    : MonCounterBase {Type(), n, t, s*sizeof(int32_t)/sizeof(uint8_t), s*sizeof(int32_t)/sizeof(uint8_t), d, false, f}  {}
  template <> inline MonCounter<int16_t*>::MonCounter(const std::string& n, const std::string& t,const std::string& f, int16_t* const &d , std::size_t s)
    : MonCounterBase {Type(), n, t, s*sizeof(int32_t)/sizeof(int16_t), s*sizeof(int32_t)/sizeof(int16_t), d, false, f}  {}
  template <> inline MonCounter<uint16_t*>::MonCounter(const std::string& n, const std::string& t,const std::string& f, uint16_t* const &d , std::size_t s)
    : MonCounterBase {Type(), n, t, s*sizeof(int32_t)/sizeof(uint16_t), s*sizeof(int32_t)/sizeof(uint16_t), d, false, f}  {}

  template <typename T> unsigned int serial_array_int(DimBuffBase *pp, const void *psrc, int len)  {
    int32_t* dst = add_ptr<int32_t>(pp, pp->dataoff);
    T*       src = (T*)psrc;
    for(int i=0; i < len; ++i, ++dst, ++src)
      *dst = *src;
    return pp->reclen;
  }

  template <> inline unsigned int MonCounter<int8_t*>::serializeData(DimBuffBase *buf, const void *src, int len)  const
  { return serial_array_int<int8_t>(buf, src, len);    }
  template <> inline unsigned int MonCounter<int16_t*>::serializeData(DimBuffBase *buf, const void *src, int len)  const
  { return serial_array_int<int16_t>(buf, src, len);   }
  template <> inline unsigned int MonCounter<uint8_t*>::serializeData(DimBuffBase *buf, const void *src, int len)  const
  { return serial_array_int<uint8_t>(buf, src, len);   }
  template <> inline unsigned int MonCounter<uint16_t*>::serializeData(DimBuffBase *buf, const void *src, int len)  const
  { return serial_array_int<uint16_t>(buf, src, len);  }
}
