//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================

/// C/C++ include files
#include <atomic>
#include <cstdint>
#include <typeinfo>

/// Online namespace declaration
namespace Online  {

  /// specialization of MonCounter class for atomic types
  template <typename T> struct MonCounter<std::atomic<T>> : MonCounterBase {

    MonCounter(const std::string& nam, const std::string& tit, const std::atomic<T>& data) :
      MonCounterBase{Type(), nam, tit, std::max(8u, serialSize()), serialSize(), (std::atomic<T>*)&data} {}

    constexpr static MONTYPE Type() {
      if        constexpr (std::is_same_v<T, int8_t>)   {
        return C_ATOMICINT;
      } else if constexpr (std::is_same_v<T, int16_t>)  {
        return C_ATOMICINT; 
      } else if constexpr (std::is_same_v<T, int32_t>)  {
        return C_ATOMICINT;
      } else if constexpr (std::is_same_v<T, int64_t>)  {
        return C_ATOMICLONG;
      } else if constexpr (std::is_same_v<T, uint8_t>)  {
        return C_ATOMICINT;
      } else if constexpr (std::is_same_v<T, uint16_t>) {
        return C_ATOMICINT;
      } else if constexpr (std::is_same_v<T, uint32_t>) {
        return C_ATOMICINT;
      } else if constexpr (std::is_same_v<T, uint64_t>) {
        return C_ATOMICLONG;
      } else if constexpr (std::is_same_v<T, float>)    {
        return C_ATOMICFLOAT;
      } else if constexpr (std::is_same_v<T, double>)   {
        return C_ATOMICDOUBLE;
      } else {
        static_assert(false_v<T>, "Unsupported type for atomic specialization of MonCounter");
      }
    }

    static unsigned int serialSize() {
      std::atomic<T> q;
      return sizeof( q.load() );
    }

    unsigned int serializeData(DimBuffBase *pp, const void *src, int )  const override {
      long *dst = add_ptr<long>(pp, pp->dataoff);
      std::atomic<T> *s = (std::atomic<T>*)src;
      *dst = 0;
      auto q = s->load();
      decltype(q) *a = (decltype(q)*)dst;
      *a = q;
      a = (decltype(q)*)&m_atomData;
      *a = q;
      return pp->reclen;
    }

    void create_OutputService(const std::string & infix) override {
      this->dim_service.reset();
      std::string nam = this->m_srvcprefix + infix + this->name;
      if (nam.length() <= 128 /* maximum length of a DIM service Name*/) {
        this->m_atomData = 0;
        std::atomic<T> *s = (std::atomic<T>*)this->m_contents;
        auto q = s->load();
        decltype(q) *a = (decltype(q)*)&this->m_atomData;
        *a = q;
        this->dim_service.reset(new DimService(nam.c_str(),typeid(q).name(), &this->m_atomData,sizeof(*a)));
      }
    }
  };
  inline static unsigned int atomic_size_int32()  {
    std::atomic<int32_t> q;
    return sizeof( q.load() );
  }
  template <typename T> unsigned int serial_atm_int(DimBuffBase *pp, const void* atom_data, const void *src)  {
    long *ldst = add_ptr<long>(pp, pp->dataoff);
    std::atomic<T> *s = (std::atomic<T>*)src;
    int32_t* aatom = (int32_t*)atom_data;
    int32_t* adest = (int32_t*)ldst;
    auto data = s->load();
    *ldst  = 0;
    *adest = data;
    *aatom = data;
    return pp->reclen;
  }
  template <> inline unsigned int MonCounter<std::atomic<int8_t> >::serialSize()   { return atomic_size_int32(); }
  template <> inline unsigned int MonCounter<std::atomic<int16_t> >::serialSize()  { return atomic_size_int32(); }
  template <> inline unsigned int MonCounter<std::atomic<uint8_t> >::serialSize()  { return atomic_size_int32(); }
  template <> inline unsigned int MonCounter<std::atomic<uint16_t> >::serialSize() { return atomic_size_int32(); }

  template <> inline unsigned int MonCounter<std::atomic<int8_t> >::serializeData(DimBuffBase *buf, const void *src, int)  const
  { return serial_atm_int<int8_t>(buf, &m_atomData, src);  }
  template <> inline unsigned int MonCounter<std::atomic<int16_t> >::serializeData(DimBuffBase *buf, const void *src, int)  const
  { return serial_atm_int<int16_t>(buf, &m_atomData, src);  }
  template <> inline unsigned int MonCounter<std::atomic<uint8_t> >::serializeData(DimBuffBase *buf, const void *src, int)  const
  { return serial_atm_int<uint8_t>(buf, &m_atomData, src);  }
  template <> inline unsigned int MonCounter<std::atomic<uint16_t> >::serializeData(DimBuffBase *buf, const void *src, int)  const
  { return serial_atm_int<uint16_t>(buf, &m_atomData, src);  }
}
