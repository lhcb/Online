//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================

#include <utility>

/// Online namespace declaration
namespace Online  {

  template <typename IN, typename OUT> static inline
  unsigned int serial_pair(DimBuffBase *pp, const void *src) {
    std::pair<IN*, IN*>* s = (std::pair<IN*, IN*>*)src;
    OUT*                 d = add_ptr<OUT>(pp, pp->dataoff);
    *d = *s->first;
    *(d + 1) = *s->second;
    return pp->reclen;
  }
    
  /// specialization of MonCounter class for pairs
  template <typename T> struct MonCounter<std::pair<T*,T*> > : MonCounterBase {

    using pairType = std::pair<T*,T*>;
    MonCounter(const std::string& nam, const std::string& tit, const pairType& data )
      : MonCounterBase{Type(), nam, tit, std::max(8u, serialSize()), serialSize(), (pairType*)&data} {}

    MONTYPE Type() const {
      if constexpr (std::is_same_v<T, int8_t>) {
        return C_INTPAIR;
      } else if constexpr (std::is_same_v<T, uint8_t>) {
        return C_INTPAIR;
      } else if constexpr (std::is_same_v<T, int16_t>) {
        return C_INTPAIR;
      } else if constexpr (std::is_same_v<T, uint16_t>) {
        return C_INTPAIR;
      } else if constexpr (std::is_same_v<T, int32_t>) {
        return C_INTPAIR;
      } else if constexpr (std::is_same_v<T, uint32_t>) {
        return C_INTPAIR;
      } else if constexpr (std::is_same_v<T, int64_t>) {
        return C_LONGPAIR;
      } else if constexpr (std::is_same_v<T, uint64_t>) {
        return C_LONGPAIR;
      } else {
        static_assert(false_v<T>, "Unsupported type for pair specialization of MonCounter");
      }
    }
    unsigned int serialSize()  const  {
      return 2*sizeof(T); 
    }
    unsigned int serializeData(DimBuffBase *pp, const void *src, int)  const override  {
      return serial_pair<T, T>(pp, src);
    }
    void create_OutputService(const std::string &) override  {
      this->dim_service.reset();
    }
  };

  inline constexpr static unsigned int pair_size_int32()  {    return sizeof(std::pair<int32_t, int32_t>);  }
#define PAIR_COUNTER(x) MonCounter<std::pair< x , x > >
  template <> inline constexpr unsigned int PAIR_COUNTER(int8_t)::serialSize()    {  return pair_size_int32();  }
  template <> inline constexpr unsigned int PAIR_COUNTER(int16_t)::serialSize()   {  return pair_size_int32();  }
  template <> inline constexpr unsigned int PAIR_COUNTER(uint8_t)::serialSize()   {  return pair_size_int32();  }
  template <> inline constexpr unsigned int PAIR_COUNTER(uint16_t)::serialSize()  {  return pair_size_int32();  }
  template <> inline unsigned int PAIR_COUNTER(int8_t)::serializeData(DimBuffBase *buf, const void *src, int)  const
  { return serial_pair<int8_t, int32_t>(buf, src);   }
  template <> inline unsigned int PAIR_COUNTER(int16_t)::serializeData(DimBuffBase *buf, const void *src, int)  const
  { return serial_pair<int16_t, int32_t>(buf, src);  }
  template <> inline unsigned int PAIR_COUNTER(uint8_t)::serializeData(DimBuffBase *buf, const void *src, int)  const
  { return serial_pair<uint8_t, int32_t>(buf, src);  }
  template <> inline unsigned int PAIR_COUNTER(uint16_t)::serializeData(DimBuffBase *buf, const void *src, int)  const
  { return serial_pair<uint16_t, int32_t>(buf, src); }
#undef PAIR_COUNTER
}
