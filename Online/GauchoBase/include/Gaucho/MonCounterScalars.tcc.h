//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================

/// implementation of MonCounter methods for scalar types
#include <iostream>
/// Online namespace declaration
namespace Online  {

  template<typename T> constexpr MONTYPE MonCounter<T>::Type() {
    if        constexpr (std::is_same_v<T, int8_t>) {
      return C_INT;
    } else if constexpr (std::is_same_v<T, int16_t>) {
      return C_INT;
    } else if constexpr (std::is_same_v<T, int32_t>) {
      return C_INT;
    } else if constexpr (std::is_same_v<T, int64_t>) {
      return C_LONGLONG;
    } else if constexpr (std::is_same_v<T, long long>) {
      return C_LONGLONG;
    } else if constexpr (std::is_same_v<T, uint8_t>) {
      return C_UINT;
    } else if constexpr (std::is_same_v<T, uint16_t>) {
      return C_UINT;
    } else if constexpr (std::is_same_v<T, uint32_t>) {
      return C_UINT;
    } else if constexpr (std::is_same_v<T, uint64_t>) {
      return C_ULONG;
    } else if constexpr (std::is_same_v<T, unsigned long long>) {
      return C_ULONG;
    } else if constexpr (std::is_same_v<T, float>) {
      return C_FLOAT;
    } else if constexpr (std::is_same_v<T, double>) {
      return C_DOUBLE;
    } else {
      static_assert(false_v<T>, "Unsupported scalar type in MonCounter");
    }
  }

  template <typename T> MonCounter<T>::MonCounter(const std::string& nam, const std::string& tit, const T& dat)
    : MonCounterBase(Type(), nam, tit, std::max(8u, serialSize()), serialSize(), (T*)&dat, 0) {}

  template <typename T> unsigned int MonCounter<T>::serializeData(DimBuffBase *pp, const void *src, int)  const  {
    long* dst = add_ptr<long>(pp, pp->dataoff);
    *dst      = 0L;        // Zero first 8 data bytes
    *(T*)dst  = *(T*)src;  // Then copy value to lower bytes
    return pp->reclen;
  }

  template <typename T> void MonCounter<T>::create_OutputService(const std::string & infix)    {
    this->dim_service = 0;
    std::string nam = m_srvcprefix + infix + this->name;
    if (nam.length() <= 128 /* maximum length of a DIM service Name*/) {
      if constexpr (std::is_same_v<T, int8_t> || std::is_same_v<T, uint8_t>) {
        this->dim_service.reset(new DimService(nam.c_str(), *(int32_t*)m_contents));
      } else if constexpr (std::is_same_v<T, int16_t> || std::is_same_v<T, uint16_t>) {
        this->dim_service.reset(new DimService(nam.c_str(), *(int32_t*)m_contents));
      } else if constexpr (std::is_same_v<T, int32_t> || std::is_same_v<T, uint32_t>) {
        this->dim_service.reset(new DimService(nam.c_str(), *(int32_t*)m_contents));
      } else if constexpr (std::is_same_v<T, int64_t> || std::is_same_v<T, uint64_t>) {
        this->dim_service.reset(new DimService(nam.c_str(), *(long long*)m_contents));
      } else if constexpr (std::is_same_v<T, long long>) {
        this->dim_service.reset(new DimService(nam.c_str(), (long long&)*(T*)m_contents));
      } else if constexpr (std::is_same_v<T, int64_t> || std::is_same_v<T, uint64_t>) {
        this->dim_service.reset(new DimService(nam.c_str(), (long long&)(*((long long*)m_contents))));
      } else if constexpr (std::is_same_v<T, float>) {
        this->dim_service.reset(new DimService(nam.c_str(), *(float*)m_contents));
      } else if constexpr (std::is_same_v<T, double>) {
        this->dim_service.reset(new DimService(nam.c_str(), *(double*)m_contents));
      } else {
        static_assert(false_v<T>, "Unsupported scalar type in MonCounter::create_OutputService");
      }
    }
  }

  template <> inline constexpr unsigned int MonCounter<int8_t>::serialSize()    {  return sizeof(int32_t); }
  template <> inline constexpr unsigned int MonCounter<int16_t>::serialSize()   {  return sizeof(int32_t); }
  template <> inline constexpr unsigned int MonCounter<uint8_t>::serialSize()   {  return sizeof(int32_t); }
  template <> inline constexpr unsigned int MonCounter<uint16_t>::serialSize()  {  return sizeof(int32_t); }

  template <typename T> unsigned int serial_scalar_int(DimBuffBase *pp, const void *src)  {
    long* dst      = add_ptr<long>(pp, pp->dataoff);
    *dst           = 0L;        // Zero first 8 data bytes
    *(int32_t*)dst = *(T*)src;  // Then copy value to lower bytes
    return pp->reclen;
  }
  template <> inline unsigned int MonCounter<int8_t>::serializeData(DimBuffBase *buf, const void *src, int)  const
  { return serial_scalar_int<int8_t>(buf, src);  }
  template <> inline unsigned int MonCounter<int16_t>::serializeData(DimBuffBase *buf, const void *src, int)  const
  { return serial_scalar_int<int16_t>(buf, src);  }
  template <> inline unsigned int MonCounter<uint8_t>::serializeData(DimBuffBase *buf, const void *src, int)  const
  { return serial_scalar_int<uint8_t>(buf, src);  }
  template <> inline unsigned int MonCounter<uint16_t>::serializeData(DimBuffBase *buf, const void *src, int)  const
  { return serial_scalar_int<uint16_t>(buf, src);  }

}
