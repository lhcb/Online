//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef ONLINE_GAUCHO_MONHIST_H_
#define ONLINE_GAUCHO_MONHIST_H_

/// Framework include files
#include <Gaucho/MonBase.h>
#include <TProfile.h>

/// C/C++ include files
#include <typeinfo>

/// Forward declarations
class TAxis;
class StatEntity;
namespace AIDA {
  class IBaseHistogram;
}

/// Online namespace declaration
namespace Online {

  /// Forward declarations
  class DimAxis;

  class StatEntityShadow {
  public:
    /// number of calls
    unsigned long nEntries;
    /// accumulated flag
    double accumulatedFlag;
    double accumulatedFlag2;
    double minimalFlag;
    double maximalFlag;
    // DR number of calls before reset
    long          nEntriesBeforeReset;
    void          Reset();
    unsigned long GetEntries() const { return nEntries; }
  };

  class MonRateProfile : public TProfile {
  public:
    MonRateProfile( const char* name, const char* title, int nbinsx, double xlow, double xup, const char* option = "" )
        : TProfile( name, title, nbinsx, xlow, xup, option ) {}
    virtual ~MonRateProfile() {}
    const double* GetSumwArr() const { return this->fArray; }
    const double* GetEntryArr() const { return this->fBinEntries.fArray; }
    const double* GetSumw2Arr() const { return this->GetSumw2()->GetArray(); }
    const double* GetBinSumw2Arr() const { return GetBinSumw2()->GetArray(); }
    double        gfYmin() const { return fYmin; }
    double        gfYmax() const { return fYmax; }
  };

  class BinNAxis {
  public:
    std::vector<std::string> labels;
    double                   min{0e0};
    double                   max{0e0};
    int                      nbin{0};
    int                      lablen{-1};
    TAxis*                   axis{nullptr};
    explicit BinNAxis() = default;
    void fetch( TAxis* ax );
    void put( DimAxis& axis ) const;
  };

  class GaudiProfileBin {
  public:
    unsigned long nent;
    double        sumw;
    double        sumw2;
  };

  enum HistClass { HT_Unknown = -1, HT_Root, HT_Gaudi, HT_Aida };
  enum BinType { BT_Unknown = 0, BT_Float, BT_Double };

  struct HDescriptor {
    HistClass clazz{HistClass::HT_Unknown};
    BinType   bintyp{BinType::BT_Unknown};
    int       dimension{-1};
    bool      profile{false};

    HDescriptor()  = default;
    ~HDescriptor() = default;
    static BinType                    bin_type( const std::string& bin_typ );
    static std::pair<HistClass, bool> histogram_type( const std::string& type_name );

    void    set_dimension( const std::string& type_name, size_t lpos );
    void    setup( const std::type_info& type_info );
    MONTYPE type() const;
  };

  template <typename T>
  class MonHist : public MonBase {
  private:
    const T*    object{nullptr};
    mutable int m_xmitbuffersize{0};

    template <typename Q>
    Q* as() {
      return (Q*)object;
    }

    int setup();

    MONTYPE analyzeType( const T& var ) {
      this->descriptor.setup( typeid( var ) );
      return this->descriptor.type();
    }

  public:
    HDescriptor           descriptor;
    std::string           bookopts;
    std::vector<BinNAxis> axes;
    int                   headerLen{0};
    int                   dimension{0};

  public:
    MonHist( const std::string& source, const std::string& desc, const T& hist );

    virtual ~MonHist();

    virtual int hdrlen() const override { return this->headerLen; }

    virtual MONTYPE monitorType() { return this->descriptor.type(); }

    virtual int xmitbuffersize() override {
      switch ( this->type ) {
      case H_1DIM:
      case H_2DIM:
      case H_PROFILE:
      case H_3DIM:
      case H_2DPROFILE:
        return this->setup();

      case H_RATE:
      case C_INT:
      case C_LONGLONG:
      case C_FLOAT:
      case C_DOUBLE:
      case C_STATENT:
      case H_ILLEGAL:
      case C_VOIDSTAR:
      default:
        break;
      }
      return this->m_xmitbuffersize;
    }
    virtual int  serialize( void* ptr ) override;
    virtual void reset() override;
    virtual void create_OutputService( const std::string& ) override {}
  };

  template <typename T>
  inline MonHist<T>::MonHist( const std::string& source, const std::string& desc, const T& hist )
      : MonBase( H_ILLEGAL, source, desc ) {
    this->object = &hist;
    this->type   = this->analyzeType( hist );
    setup();
  }

  template <typename T>
  inline MonHist<T>::~MonHist() {
    Bool_t dirstat = TH1::AddDirectoryStatus();
    TH1::AddDirectory( kFALSE );
    this->axes.clear();
    this->object = nullptr;
    TH1::AddDirectory( dirstat );
  }

  template <typename T>
  inline void setup_buffer( DimHistbuff1* dim_hist, MonHist<T>& hist ) {
    dim_hist->type      = hist.type;
    dim_hist->flags     = 0;
    dim_hist->addoffset = 0;
    dim_hist->nentries  = 0;
    dim_hist->sumw      = 0e0;
    dim_hist->sumw2     = 0e0;
    dim_hist->sumwx     = 0e0;
    dim_hist->sumwx2    = 0e0;
    dim_hist->sumwy     = 0e0;
    dim_hist->sumwy2    = 0e0;
    dim_hist->sumwz     = 0e0;
    dim_hist->sumwz2    = 0e0;
    dim_hist->sumwt     = 0e0;
    dim_hist->sumwt2    = 0e0;
    dim_hist->yminval   = 0e0;
    dim_hist->ymaxval   = 0e0;
    dim_hist->titlen    = hist.title_length();
    dim_hist->namelen   = hist.name_length();
    dim_hist->reclen    = hist.xmitbuffersize();
    dim_hist->dim       = hist.descriptor.dimension;
    dim_hist->dataoff   = hist.hdrlen();
    dim_hist->nameoff   = dim_hist->buffer_name_offset( hist.descriptor.dimension );
    dim_hist->titoff    = dim_hist->nameoff + dim_hist->namelen;
    hist.cpyName( add_ptr<char>( dim_hist, dim_hist->nameoff ) );
    hist.cpytitle( add_ptr<char>( dim_hist, dim_hist->titoff ) );
  }
  void setup_labels( DimHistbuff1* dim_histo, const std::vector<BinNAxis>& axes );
}      // namespace Online
#endif //  ONLINE_GAUCHO_MONHIST_H_
