//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef ONLINE_GAUCHO_MONINFO_H
#define ONLINE_GAUCHO_MONINFO_H

/// Framework include files
#include <Gaucho/MonInfoHandler.h>
#include <dim/dic.hxx>

/// C/C++ include files
#include <string>

/// Online namespace declaration
namespace Online  {

  class MonInfo;
  class MonInfoHandler;

  class MonInfo /* : public DimUpdatedInfo */ {

  private:
    std::string     name     {   };
    MonInfoHandler* handler  { nullptr };
    int             id       { 0 };
    
    static void handler_callback(void* tagp, void* bufp, int* len);

  public:
    MonInfo(const std::string& target, MonInfoHandler* handler = nullptr);
    MonInfo(const std::string& target, int period, MonInfoHandler* handler = nullptr);
    virtual ~MonInfo();
    const char* getName() const  { return name.c_str();    }
    static void setShutdownInProgress(bool value);
    //virtual void infoHandler () /* override  */;
  };
}       // End namespace Online
#endif  // ONLINE_GAUCHO_MONINFO_H
