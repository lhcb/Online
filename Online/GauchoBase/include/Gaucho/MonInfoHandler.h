//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
#ifndef ONLINE_GAUCHO_MONINFOHANDLER_H
#define ONLINE_GAUCHO_MONINFOHANDLER_H

/// Online namespace declaration
namespace Online  {

  /// Forward declarations
  class MonInfo;

  class MonInfoHandler   {
  public:
    MonInfoHandler() = default;
    virtual ~MonInfoHandler() = default;
    virtual void operator()(void* data, int size, MonInfo* info) = 0;
  };
}      // End namespace Online
#endif // ONLINE_GAUCHO_MONINFOHANDLER_H
