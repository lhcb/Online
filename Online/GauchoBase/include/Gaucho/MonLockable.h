//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef ONLINE_GAUCHO_MONLOCAKBLE_H
#define ONLINE_GAUCHO_MONLOCAKBLE_H

#include <Gaucho/BRTL_Lock.h>
#include <memory>

/// Online namespace declaration
namespace Online   {

  class MonLockable  {
  public:
    struct _Lock {
      MonLockable* m_sys;
      _Lock(MonLockable* sys) : m_sys(sys)     {
	m_sys->lock();
      }
      ~_Lock()      {
	m_sys->unlock();
      }
    };

  protected:
    std::unique_ptr<BRTLLock> lockid;
    int          lockcount          { 0 };
    int          unlockcount        { 0 };

  public:
    MonLockable() = default;
    virtual ~MonLockable() = default;
    virtual int  lock(void);
    virtual int  unlock(void);
  };
}
#endif // ONLINE_GAUCHO_MONLOCAKBLE_H
