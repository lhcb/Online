//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef GAUCHO_MONRATE_H
#define GAUCHO_MONRATE_H

/// Framework include files
#include <Gaucho/MonCounter.h>
#include <Gaucho/MonBase.h>

/// C/C++ include files
#include <atomic>

/// Online namespace declaration
namespace Online  {

  class MonRateBase : public MonBase  {
  public:
    enum  {
      R_INT, R_FLOAT, R_DOUBLE, R_STATENT
    };

    std::unique_ptr<MonBase> rateCounter   { };
    MonBase*                 origCounter  { nullptr };

  public:
    MonRateBase(const std::string&, const std::string& , MonBase *bse=0);
    virtual void create_OutputService(const std::string &s) override;
    virtual void update_OutputService() override;
    virtual void makeRate(unsigned long) = 0;
    virtual void zeroLast()    {
    }
    virtual void zero()    {
      this->zeroLast();
    }
  };

  MonBase *CREATERate(const std::string &name, const std::string &title, double &Rate);

  class MonRateBBase : public MonRateBase  {
  public:
    double rate    { 0.0 };

  public:
    virtual void create_OutputService(const std::string &s) override    {
      if (this->rateCounter)
        this->rateCounter->create_OutputService(s);
    }

    MonRateBBase(const std::string &nam, const std::string &tit, MonBase *bse=0)
      :  MonRateBase(nam, tit, bse)
    {
      this->zeroLast();
      this->rateCounter.reset(CREATERate(nam, tit, this->rate));
      this->type = C_RATEDOUBLE;
    }

    virtual ~MonRateBBase()      {
    }

    virtual int xmitbuffersize() override    {
      return (this->rateCounter) ? this->rateCounter->xmitbuffersize() : 0;
    }
    
    virtual int serialize(void* buff) override   {
      return (this->rateCounter) ? this->rateCounter->serialize(buff) : 0;
    }
  };

  template<typename T> class MonRate : public MonRateBBase  {
  public:
    const T& m_data;
    T        m_lastCount;
    
  public:
    MonRate(const std::string &nam, const std::string &tit, const T &dat, MonBase *bse=0)
      : MonRateBBase(nam, tit, bse), m_data(dat)
    {
      this->m_lastCount = this->m_data;
    }
    virtual void makeRate(unsigned long dt) override      {
      this->rate = double(m_data - m_lastCount);
      this->rate = 1.0e9 * rate / double(dt);
      if (this->rate < 0.0)
        this->rate = 0.0;
      this->m_lastCount = m_data;
    }
    virtual void zeroLast() override      {
      this->m_lastCount = 0;
    }
    virtual void zero() override      {
      this->zeroLast();
      this->rate = 0e0;
    }
  };

  /// specialization of MonRate for arrays
  template<typename T> class MonRate<T*> : public MonRateBase    {
  public:
    std::unique_ptr<T[]>      m_lastCount;
    std::unique_ptr<double[]> m_ratearr;
    T*                        m_ptr { nullptr };
    int                       m_nel  { 0 };

  public:
    MonRate(const std::string &nam, const std::string &tit, const std::string, T* dat, int num_elts, MonBase *bse=0);
    virtual ~MonRate();
    virtual int  xmitbuffersize() override;
    virtual int  serialize(void* buff) override;
    void         makeRate(unsigned long dt) override;
    virtual void zeroLast() override;
  };

  /// specialization of MonRate class for atomic types
  template <typename T> class MonRate<std::atomic<T>> : public MonRateBBase    {
  public:
    const std::atomic<T>* m_ptr;
    std::atomic<T>        m_lastCount;

  public:
    MonRate(const std::string &name, const std::string &title, const std::atomic<T> &dat, MonBase *bse=0);
    virtual void makeRate(unsigned long dt) override;
    virtual void zeroLast() override;
  };

  template <typename T> inline
  MonRate<std::atomic<T> >::MonRate(const std::string &nam, const std::string &tit, const std::atomic<T> &dat, MonBase *bse)
    : MonRateBBase(nam, tit, bse), m_ptr(&dat)  {
  }
  template <typename T> inline void MonRate<std::atomic<T> >::makeRate(unsigned long dt)   {
    this->rate = double(this->m_ptr->load() - this->m_lastCount.load());
    this->rate = 1.0e9 * this->rate / double(dt);
    if (this->rate < 0.0)
      this->rate = 0.0;
    this->m_lastCount.store(this->m_ptr->load());
  }
  template <typename T> inline void MonRate<std::atomic<T> >::zeroLast()   {
    this->m_lastCount = 0;
    this->rate = 0e0;
  }

  /// specialization of MonRate class for pairs
  template<typename T> class MonRate<std::pair<T, T>> : public MonRateBBase   {
  public:
    MonRate(const std::string &nam, const std::string &tit, const std::atomic<T> &, MonBase *bse=0) 
      : MonRateBBase(nam, tit, bse)      {
    }
    virtual void makeRate(unsigned long) override     {
      assert(false);
    }
    virtual void zeroLast() override      {
    }
  };

  template<typename T> MonRate<T*>::MonRate(const std::string &nam,
                                            const std::string &tit,
                                            const std::string,
                                            T* dat, int num_elts,
                                            MonBase *bse)
    : MonRateBase(nam, tit, bse ), m_ptr(dat)
  {
    this->m_nel = num_elts;
    this->m_lastCount.reset(new T[this->m_nel]);
    this->m_ratearr.reset(new double[this->m_nel]);
    ::memset(this->m_lastCount.get(), 0, this->m_nel*sizeof(T));
    ::memset(this->m_ratearr.get(), 0, this->m_nel*sizeof(double));
    this->rateCounter = std::make_unique<MonCounter<double*> >(nam, tit, "d", this->m_ratearr.get(), this->m_nel*sizeof(double));
  }

  template<typename T> MonRate<T*>::~MonRate()    {
  }

  template<typename T> int MonRate<T*>::xmitbuffersize()   {
    return ( this->rateCounter ) ? this->rateCounter->xmitbuffersize() : 0;
  }

  template<typename T> int MonRate<T*>::serialize(void* buff)    {
    return ( this->rateCounter ) ? this->rateCounter->serialize(buff) : 0;
  }

  template<typename T> void MonRate<T*>::makeRate(unsigned long dt)    {
    for (int i = 0; i < this->m_nel; i++)   {
      long tmp = this->m_ptr[i] - this->m_lastCount[i];
      this->m_ratearr[i] = (double) (tmp);
      this->m_ratearr[i] = (this->m_ratearr[i]) / double(dt);
      this->m_ratearr[i] *= 1.0e9;
      if (this->m_ratearr[i] < 0.0)
        this->m_ratearr[i] = 0;
    }
    ::memcpy(m_lastCount.get(), m_ptr, m_nel*sizeof(T));
  }
  template<typename T> void MonRate<T*>::zeroLast()    {
    ::memset(this->m_lastCount.get(), 0, this->m_nel*sizeof(T));
  }
}      // End namespace Online
#endif // GAUCHO_MONRATE_H
