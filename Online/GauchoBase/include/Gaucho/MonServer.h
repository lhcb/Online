//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef GAUCHO_HISTSERVER_H
#define GAUCHO_HISTSERVER_H

/// Framework include files
#include <dim/dis.hxx>

/// Online namespace declaration
namespace Online   {

  class HistServer : public DimServer    {
  public:
    HistServer();
    virtual ~HistServer();
  };
}      // End namespace Online
#endif // GAUCHO_HISTSERVER_H
