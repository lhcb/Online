//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#pragma once

/// Framework include files
#include <Gaucho/MonClassMgr.h>
#include <Gaucho/MonLockable.h>

/// C/C++ include files
#include <list>

/// Online namespace declaration
namespace Online   {

  class MonitorClass;

  enum    {
    MONSUBSYS_Histogram = 1,
    MONSUBSYS_Counter,
    MONSUBSYS_String
  };

  class SubSysParams    {
  public:
    bool        expandnames = false;
    bool        dontclear = false;
    std::string expandInfix;
    std::string ratePrefix;
    int         type = 0;
    int         updatePeriod = 0;
    bool        runAware = false;
    bool        updateOnStop = true;
  };

  class MonSubSys : public MonLockable {
  public:
    MonClassMgr    classMgr;
    int            type               { 0 };
    int            runNumber          { 0 };
    bool           dontclear          { false };

  protected:
    std::string  name;
    int          lockcount          { 0 };
    int          unlockcount        { 0 };
    bool         start_done         { false };
  public:

    MonSubSys(SubSysParams & params);
    virtual ~MonSubSys();
    virtual void setup(const std::string& n, bool expand=false)=0;

    virtual void start();
    virtual void stop();
    virtual void update();
    virtual void update(unsigned long ref);

    virtual void reset();

    virtual size_t remove(const std::string &name) = 0;
    virtual size_t removeAll(const std::string &owner_name);
    virtual void clear();

    virtual void setRunNo(int runno);
    virtual void eorUpdate(int runno);
    void         addClass(std::string &nam, int intv, std::list<std::string> &sels, bool updateOnStop);
    void         addClass(std::shared_ptr<MonitorClass>&& clazz);
  };
}      // End namespace Online

