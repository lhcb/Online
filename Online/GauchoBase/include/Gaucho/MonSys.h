//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef ONLINE_GAUCHO_MONSYS_H
#define ONLINE_GAUCHO_MONSYS_H

/// C/C++ include files
#include <vector>
#include <string>
#include <memory>

/// Online namespace declaration
namespace Online   {

  /// Forward declaration
  class MonSubSys;

  class MonSys    {
  public:
    std::string name;
    std::string state;
    std::vector<std::shared_ptr<MonSubSys> > subSystems;

  public:
    MonSys();
    virtual ~MonSys() = default;
    void add(std::shared_ptr<MonSubSys> subsys);
    void remove(std::shared_ptr<MonSubSys> subsys);
    void lock();
    void unlock();
    static MonSys& instance();
    MonSys *setup(const std::string& n);
    void start();
    void reset();
    void clear();
    void eorUpdate(int runo);
    void update();
    void update(unsigned long ref);
    void stop();
    void setRunNo(int runo);
  };
}       // End namespace Online
#endif  // ONLINE_GAUCHO_MONSYS_H
