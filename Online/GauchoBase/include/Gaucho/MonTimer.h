//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef ONLINE_GAUCHO_MONTIMER_H
#define ONLINE_GAUCHO_MONTIMER_H

/// Framework include files
#include "Gaucho/GenTimer.h"

/// Online namespace declaration
namespace Online   {

  /// Forward declaration
  class MonitorClass;

  class MonTimer : public GenTimer    {
    MonitorClass *m_Hsys { nullptr };
  public:
    MonTimer(MonitorClass *tis, int period = 10);
    virtual ~MonTimer() = default;
    void timerHandler() override;
  };
}      // End namespace Online
#endif // ONLINE_GAUCHO_MONTIMER_H
