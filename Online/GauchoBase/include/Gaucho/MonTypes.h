//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef GAUCHO_MONTYPES_H
#define GAUCHO_MONTYPES_H


/// IMPORTANT:
/// -- DO NOT RE_ORDER TYPES!!!
/// -- ONLY ADD NEW TYPES TO THE VERY END!
/// 
enum MONTYPE   {
  H_ILLEGAL          = 0xFEED0000,
  H_1DIM,                  // No.1
  H_2DIM,
  H_PROFILE,
  H_RATE,
  C_INT,
  C_LONGLONG,
  C_FLOAT,
  C_DOUBLE,
  C_STATENT,
  C_VOIDSTAR,             // No.10
  C_RATEFLOAT,
  C_INTSTAR,
  C_LONGSTAR,
  C_FLOATSTAR,
  C_DOUBLESTAR,
  C_RATEDOUBLE       = 0xFEED0010,
  C_RATEDOUBLESTAR,
  H_3DIM,
  H_2DPROFILE,
  C_LONGPAIR,              // No.20
  C_INTPAIR,
  C_ATOMICINT,
  C_ATOMICLONG,
  C_ATOMICFLOAT,
  C_ATOMICDOUBLE,

  C_GAUDIACCLONG,
  C_GAUDIACCINT,
  C_GAUDIACCFLOAT,
  C_GAUDIACCDOUBLE,        // No.30
  C_GAUDIAVGACCl,
  C_GAUDIAVGACCd,
  C_GAUDIBINACC_temp = 0xFEED0020,

  C_UINT,
  C_ULONG,
  C_UINTSTAR,
  C_ULONGSTAR,

  H_GAUDIH1F,
  H_GAUDIH1D,
  H_GAUDIH2F,              // No.40
  H_GAUDIH2D,
  H_GAUDIH3F,
  H_GAUDIH3D,
  H_GAUDIPR1,
  H_GAUDIPR2,
  H_GAUDIPR3,
  C_GAUDIACCuLONG,
  C_GAUDIACCuINT,

  C_GAUDIBINACCd     = 0xFEED0030,
  C_GAUDIAVGACClu,         // No.50
  C_GAUDIAVGACCf,
  C_GAUDISTATACCd,
  C_GAUDISTATACClu,
  C_GAUDISIGMAACCl,
  C_GAUDISIGMAACCd,
  C_GAUDIMSGACClu,  // 0xFEED0037

  C_GAUDIACCCHAR     = 0xFEED0050,
  C_GAUDIACCuCHAR,
  C_GAUDIACCSHORT,
  C_GAUDIACCuSHORT,        // No.60

  C_GAUDIAVGACCc     = 0xFEED0070,
  C_GAUDIAVGACCcu,
  C_GAUDIAVGACCs,
  C_GAUDIAVGACCsu,
  C_GAUDIAVGACCi,
  C_GAUDIAVGACCiu,

  C_GAUDISIGMAACCc   = 0xFEED0090,
  C_GAUDISIGMAACCcu,
  C_GAUDISIGMAACCs,
  C_GAUDISIGMAACCsu,        // No.70
  C_GAUDISIGMAACCi,
  C_GAUDISIGMAACCiu,
  C_GAUDISIGMAACClu,
  C_GAUDISIGMAACCf,

  C_GAUDISTATACCc    = 0xFEED00B0,
  C_GAUDISTATACCcu,
  C_GAUDISTATACCs,
  C_GAUDISTATACCsu,
  C_GAUDISTATACCi,
  C_GAUDISTATACCiu,        // No.80
  C_GAUDISTATACCl,
  C_GAUDISTATACCf,

  C_GAUDIBINACCc     = 0xFEED00D0,
  C_GAUDIBINACCcu,
  C_GAUDIBINACCs,
  C_GAUDIBINACCsu,
  C_GAUDIBINACCi,
  C_GAUDIBINACCiu,
  C_GAUDIBINACCl     = C_GAUDIBINACC_temp,
  C_GAUDIBINACClu    = C_GAUDIBINACCiu+2,  // No.90
  C_GAUDIBINACCf,                          // No. 91
  //C_GAUDIBINACCd,

};

#endif
