//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef ONLINE_GAUCHOBASE_GAUCHO_MONITORCLASS_H_
#define ONLINE_GAUCHOBASE_GAUCHO_MONITORCLASS_H_

/// Framework include files
#include <Gaucho/MonLockable.h>
#include <Gaucho/MonRate.h>
#include <Gaucho/ObjMap.h>

/// C/C++ include files
#include <list>
#include <set>
#include <map>
#include <string>
#include <regex>

/// Online namespace declaration
namespace Online  {

  /// Forward declarations
  class MonBase;
  class ObjRPC;
  class RateMgr;
  class MonTimer;
  class ObjService;
  class Serializer;

  /// Definition of the MonitorClass
  /**
   *  \author  Beat Jost
   *  \version 1.0
   */
  class MonitorClass : public MonLockable {
  public:
    static  std::string DefaultClassName;
    typedef std::map<std::string,std::unique_ptr<MonBase> > MonitorObjects;
  protected:
    std::string                    expandInfix;
    std::string                    serviceFMT = "";
    std::shared_ptr<Serializer>    serializer;
    std::shared_ptr<Serializer>    eor_serializer;
    std::unique_ptr<ObjService>    eor_service;
    std::unique_ptr<MonTimer>      timer;
    int                            interval { 0 };
    bool                           updateOnStop { true };

  public:
    std::string                    ID, name;
    std::unique_ptr<RateMgr>       rateManager;
    std::unique_ptr<ObjService>    genSrv;
    std::list<std::string>         entityExpression;
    std::list<std::regex*>         class_regex;
    MonitorObjects                 entities;
    int                            type         { 0 };
    unsigned int                   runNumber    { 0 };
    bool                           expand       { false };
    bool                           runAware     { false };


    MonitorClass(const std::string &id, int intv, std::list<std::string> &sel, bool updateOnStop);
    virtual ~MonitorClass();

    void add(std::unique_ptr<MonBase>&& mon);
    std::unique_ptr<MonBase> remove(const std::string &nam);
    std::size_t clearAll(const std::string &owner_name);
    std::size_t clear();

    bool matchName(const std::string &nam);

    void reset();
    void setup(const std::string& n, bool expnd, const std::string &einfix);
    void makeRates(unsigned long);
    void start();
    void stop();
    void setRunNo(unsigned int runno);
    void eorUpdate(int runo);
    void update();
    void update(unsigned long ref);
    void configure(int typ);
  };
}      // End namespace Online
#endif /* ONLINE_GAUCHOBASE_GAUCHO_MONITORCLASS_H_ */
