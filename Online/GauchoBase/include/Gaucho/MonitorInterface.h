//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#pragma once

/// Framework include files
#include <Gaucho/CounterSubSys.h>
#include <Gaucho/TaskSaveTimer.h>
#include <Gaucho/HistSubSys.h>
#include <RTL/rtl.h>

/// C/C++ include files
#include <map>
#include <set>
#include <list>
#include <ctime>
#include <vector>
#include <memory>

class DimService;
namespace AIDA   {
  class IBaseHistogram;
}

/// Online namespace declaration
namespace Online   {

  /// Forward declarations
  class MonSys;
  class MonSubSys;
  class CounterSubSys;
  class HistSubSys;
  class RateMgr;
  class TaskSaveTimer;

  class MonitorInterface  {
  public:
    std::vector<std::string>       m_CounterClasses   { };
    std::vector<std::string>       m_HistogramClasses { };
    SubSysParams                   m_CntrPars         { };
    SubSysParams                   m_HistPars         { };
    std::string                    m_UTGID            { };
    std::string                    m_ServiceInfix     { };
    std::shared_ptr<HistSubSys>    m_HistSubSys       { };
    std::shared_ptr<CounterSubSys> m_CntrSubSys       { };
    std::shared_ptr<TaskSaveTimer> m_saveTimer        { };
    int                            m_runnr            { 0 };
    bool                           m_expandSrv        { false };
    bool                           m_started          { false };
    bool                           m_runAware         { false };
    time_t                         m_lastEOR          { 0 };
    time_t                         m_lastUpdate       { 0 };

  private:
    std::list<std::shared_ptr<MonitorClass> > makeClassList(std::vector<std::string> &option) const;

  public:
    MonitorInterface(const std::string &utgid, SubSysParams&& cntpars, SubSysParams&& histpars);
    virtual ~MonitorInterface();

    void i_unsupported(const std::string& name, const std::type_info& typ);
    template<class T>
    void declareCounter(const std::string& name, T const& var, const std::string& desc);
    template<class T>
    void declareCounter(const std::string& name, const std::string& format, T var, int size, const std::string& desc);
    template <typename T>
    void declareHistogram(const std::string& name, T*  var, const std::string& desc);
    template <typename T>
    void declareHistogram(const std::string& name, T&  var, const std::string& desc);
    template<typename T>
    void declarePair(const std::string& name, const T& var1,  const T& var2, const std::string& desc);
  
    std::size_t undeclareCounter(const std::string& entry);
    std::size_t undeclare(const std::string&);
    std::size_t undeclareAll(std::string &);

    int start();
    int stop();
    void applyCounterClasses(std::vector<std::string> &m_CounterClasses);
    void applyHistogramClasses(std::vector<std::string> &m_CounterClasses);
    void enableRates(bool enable);
    void setRunNo(int runno);
    void eorUpdate(int runno);
    void update(unsigned long ref);
    void resetHistos();
    void lock(void);
    void unlock(void);
    void startSaving(std::shared_ptr<TaskSaveTimer>& timer);
    void stopSaving();
  };

  template<class T>
  void MonitorInterface::declareCounter(const std::string& nam, T const & var, const std::string& desc)   {
    m_CntrSubSys->add_counter<T>(nam,desc,var);
  }

  template<typename T> 
  void MonitorInterface::declareCounter(const std::string& nam,
                                        const std::string& format,
                                        T var,
                                        int size,
                                        const std::string& desc)
  {
    ::lib_rtl_output(LIB_RTL_INFO,"MonitorInterface declareCounter %s %s\n",
                     nam.c_str(),typeid(var).name());
    m_CntrSubSys->add_counter<T>(nam,desc,format,var,size);
  }
  
  template <class T>
  void MonitorInterface::declareHistogram(const std::string& nam, T* var, const std::string& desc)   {
    m_HistSubSys->add_hist(nam,desc,(const T&)*var);
  }

  template <class T>
  void MonitorInterface::declareHistogram(const std::string& nam, T& var, const std::string& desc)   {
    m_HistSubSys->add_hist(nam,desc,(const T&)var);
  }

  template<typename T>
  void MonitorInterface::declarePair(const std::string& nam,  const T& var1, const T& var2, const std::string& desc)   {
    m_CntrSubSys->add_counter(nam,desc,var1,var2);
  }
}
