//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef ONLINE_GAUCHOAPPL_GAUCHOAPPL_MYDIMERRORHANDLER_H_
#define ONLINE_GAUCHOAPPL_GAUCHOAPPL_MYDIMERRORHANDLER_H_

/// Framework include files
#include "dim/dim.hxx"
#include "RTL/rtl.h"


/// Online namespace declaration
namespace Online  {

  class MyDimErrorHandler : public DimErrorHandler  {
  public:
    bool m_flag;
    void errorHandler(int /*severity*/, int code, char *msg) override    {
      int sev = LIB_RTL_INFO;
      if (m_flag)	{
	::lib_rtl_output(sev,"DIM Message Code %x %s\n",code,msg);
      }
    }
    MyDimErrorHandler(bool value=true) : DimErrorHandler()      {
      m_flag = value;
    }
    void start()    {
      m_flag = true;
    }
    void stop()    {
      m_flag = false;
    }
  };
}
#endif /* ONLINE_GAUCHOAPPL_GAUCHOAPPL_MYDIMERRORHANDLER_H_ */
