//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef ONLINE_GAUCHO_GAUCHO_OBJMAP_H
#define ONLINE_GAUCHO_GAUCHO_OBJMAP_H

/// C/C++ include files
#include <memory>
#include <string>
#include <map>

/// Online namespace declaration
namespace Online   {

  /// Forward declaration
  class MonBase;
  typedef std::map<std::string,std::unique_ptr<MonBase> >  MonitorObjects;
}

#endif /* ONLINE_GAUCHO_GAUCHO_OBJMAP_H */
