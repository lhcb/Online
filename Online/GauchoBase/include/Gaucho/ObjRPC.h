//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef GAUCHO_OBJRRPC_H
#define GAUCHO_OBJRRPC_H

/// Framework include files
#include <dim/dis.hxx>
#include <Gaucho/RPCdefs.h>
#include <Gaucho/Serializer.h>

/// C/C++ include files
#include <memory>
#include <vector>

/// Online namespace declaration
namespace Online   {

  /// Forward declaration
  class BRTLLock;

  /// Definition of the object RPC class using the DIM protocol
  /**
   *  \author  Beat Jost
   *  \version 1.0
   */
  class ObjRPC : public DimRpc    {
  protected:
    std::shared_ptr<DimServerDns>  dns;
    std::unique_ptr<Serializer>    serializer;
    std::vector<uint8_t>           compress;
    BRTLLock *maplock  { nullptr };
    BRTLLock *objlock  { nullptr };

  public:
    ObjRPC() = delete;
    ObjRPC(std::unique_ptr<Serializer>&& serial,
           const std::string& name,
           const char *f_in, const char *f_out,
           BRTLLock* = nullptr, BRTLLock * = nullptr);
    ObjRPC(std::shared_ptr<DimServerDns> dns,
           std::unique_ptr<Serializer>&& serial,
           const std::string& name,
           const char *f_in, const char *f_out,
           BRTLLock* = nullptr, BRTLLock * = nullptr);
    virtual ~ObjRPC() = default;
    void rpcHandler() override;
  };
}      // End namespace Online
#endif // GAUCHO_OBJRRPC_H
