//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#pragma once
#include <CPP/mem_buff.h>
#include <Gaucho/Serializer.h>

/// Online namespace declaration
namespace Online  {

  class MonBase;
  class MonClassMgr;
  class MonitorClass;

  class ObjSerializer : public Serializer {
  private:
    mem_buff              buffer           {  };
    const MonClassMgr*    manager          { nullptr };
    const MonitorClass*   monitorClass     { nullptr };
    bool                  expand           { false };

    std::vector<MonBase*> collect()   const;
    MonBase* find(const std::string& name)  const;
    std::pair<std::size_t,void*> i_serialize_obj(mem_buff& buff, std::vector<MonBase*>&& items, std::size_t offset, bool clear, bool dim_limit=true) const;

  public:
    ObjSerializer(MonClassMgr* mgr, bool expand);
    ObjSerializer(MonitorClass* clazz, bool expand);
    virtual ~ObjSerializer() = default;

    /// Serialize object set to local buffer
    virtual std::pair<std::size_t,void*> serialize_obj(std::size_t offset, bool clear=false)   override;
    /// Serialize object set to external buffer
    virtual std::pair<std::size_t,void*> serialize_obj(mem_buff& buff, std::size_t offset, bool clear=false)  const  override;
    /// Serialize a set of objects identified by name to local buffer
    virtual std::pair<std::size_t,void*> serialize_match(const std::string& match, std::size_t offset, bool clear=false)   override;
    /// Serialize a set of objects identified by match to name to local buffer
    virtual std::pair<std::size_t,void*> serialize_obj(const std::vector<std::string>& nams, std::size_t offset, bool clear=false)   override;
    /// Serialize directory (inventory of objects identified by name to local buffer
    virtual std::pair<std::size_t,void*> serialize_dir(std::size_t offset)   override;
    /// Access to the internal buffer
    virtual std::pair<std::size_t,const void*> data()   const   override;
    /// Update expansions
    virtual void updateExpansions()   override;
  };
}
