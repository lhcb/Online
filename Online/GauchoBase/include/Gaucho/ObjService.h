//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef GAUCHO_ObjService_h
#define GAUCHO_ObjService_h

/// Framework include files
#include <dim/dis.hxx>
#include <CPP/mem_buff.h>
#include <Gaucho/Serializer.h>
#include <Gaucho/SerialHeader.h>

/// C/C++ include files
#include <memory>

/// Online namespace declaration
namespace Online  {

  class ObjService : public DimService  {
  protected:
    std::unique_ptr<Serializer>    serializer;
    std::shared_ptr<mem_buff>      extBuffer;
    std::unique_ptr<DimService>    num_update_svc;

    bool         m_EORservice      { false };
    SerialHeader m_hdr             {   };
    void        *m_serptr          { nullptr };
    size_t       m_sersiz          { 0 };
    int          num_updates       { 0 };
    void add_updater(DimServerDns* dns=nullptr);

  public:
    /// Default constructor
    ObjService () = default;
    /// Default destructor
    virtual ~ObjService();
    /// Initializing constructor
    ObjService (std::shared_ptr<DimServerDns>& dns, std::unique_ptr<Serializer>&& s,const std::string& name, const char *format, void *buff, size_t siz);
    /// Initializing constructor
    ObjService (std::shared_ptr<DimServerDns>& dns, std::unique_ptr<Serializer>&& s,const std::string& name, const char *format, void *buff, size_t siz, std::shared_ptr<mem_buff> extbuff);
    /// Initializing constructor
    ObjService (std::unique_ptr<Serializer>&& s,const std::string& name, const char *format, void *buff, size_t siz);
    /// Initializing constructor
    ObjService (std::unique_ptr<Serializer>&& s,const std::string& name, const char *format, void *buff, size_t siz, std::shared_ptr<mem_buff> extbuff);

    void serialize(mem_buff& buffer, int updtIntv)  const;
    void serialize(int updtIntv);
    void update();
    void setRunNo(int runno)  {  long r = 0;  r=runno;  m_hdr.run_number=r;  }
    void setTime(long time)   {  m_hdr.ser_tim = time;   }
    void setEORflag(bool val);
    const void* serialized_buffer()   const;
    size_t serialized_size()   const;
  };
}        // End namespace Online
#endif   // GAUCHO_ObjService_h
