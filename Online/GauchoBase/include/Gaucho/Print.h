//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef ONLINE_GAUCHO_PRINT_H
#define ONLINE_GAUCHO_PRINT_H

/// C/C++ include files
#include <iostream>

/// Online namespace declaration
namespace Online   {

  namespace gaucho  {

    std::string to_string_histo(const void* obj, int flags=0);

    template <typename OBJECT>
      std::string to_string(const OBJECT* obj, int flags=0);

    template <typename OBJECT>
      std::string to_string(const OBJECT& obj, int flags=0)  {
      return to_string(&obj, flags);
    }
  }
}
#endif  // ONLINE_GAUCHO_PRINT_H
