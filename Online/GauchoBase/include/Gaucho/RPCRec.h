//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef ONLINE_GAUCHO_RPCREC_H
#define ONLINE_GAUCHO_RPCREC_H

/// Framework include files
#include <dim/dic.hxx>
#include <Gaucho/dimhist.h>
#include <Gaucho/RPCdefs.h>

/// C/C++ include files
#include <string>
#include <vector>
#include <map>

/// Online namespace declaration
namespace Online   {

  class RPCRec : public DimRpcInfo   {
  public:
    typedef  std::vector<const char*>              NAMEVEC;
    typedef  std::vector<std::string>              STRVEC;
    typedef  std::map<const char*, const void*>    PTRMAP;

  private:
    bool        m_synch;
    RPCCommType m_reply;
    void (*DirCB)(const NAMEVEC&)   { nullptr };
    void (*DatCB)(const PTRMAP&)    { nullptr };

  public:
    NAMEVEC names;
    PTRMAP  hists;
    void    rpcInfoHandler() override;

    RPCRec(const std::string& name, int timeout, bool synch=false);
    virtual ~RPCRec() = default;
    static void set_default_dns();

    void declareDirectoryCallback(void (*CBfn)(const NAMEVEC&))  { DirCB = CBfn;  }
    void declareDataCallback(void (*CBfn)(const PTRMAP&))        { DatCB = CBfn;  }
    int analyseReply();
    int analyseReply(std::size_t siz, const void* valin);
    int analyseReply(NAMEVEC& names);
    int analyseReply(PTRMAP&  objects);
    int analyseReply(STRVEC&  names);
  };
}
#endif  // ONLINE_GAUCHO_RPCREC_H
