//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef RPCDEFS_H
#define RPCDEFS_H

/// Framework include files
#include <RTL/Pack.h>

/// C/C++ include files
#include <string>
#include <vector>
#include <cstdint>


/// Online namespace declaration
namespace Online   {

  enum RPCCommType   {
    RPCCIllegal=0,

    RPCCRead,
    RPCCReadAll,
    RPCCReadRegex,
    RPCCClear,
    RPCCClearAll,
    RPCCDirectory,

    RPCCReadCookie,
    RPCCReadAllCookie,
    RPCCReadRegexCookie,
    RPCCClearCookie,
    RPCCClearAllCookie,
    RPCCDirectoryCookie

  };

  class RPCReply;
  class RPCCommRead;
  class RPCCommRegex;
  class RPCCommClear;

  union  RPCFlags {          // Important for padding! Do not remove.
    struct _flags  {
      unsigned compress  :  1;
    } values;
    uint32_t data;

    RPCFlags() { data = 0; }
    RPCFlags(uint32_t d) { data = d; }
  };
  
  class RPCComm   {
  public:
    RPCCommType comm;
    static char* copy_name(char* ptr, const std::string& name);
    static char* copy_names(char* ptr, const std::vector<std::string>& names);
    template <typename T> static T* make_command(std::size_t len, RPCCommType typ);
    template <typename T> static T* make_command(std::size_t len, RPCCommType typ, const void* cookie);
  };

  class RPCCommCookie : public RPCComm   {
  public:
    typedef const void* cookie_t;
    cookie_t cookie;
    int32_t  id;
    uint32_t flags;    // Important for padding! Do not remove.
  };

  class RPCReply   {
  public:
    typedef const void* cookie_t;
    int32_t     status;
    RPCCommType comm;
  };

  class RPCReplyCookie : public RPCReply   {
  public:
    cookie_t cookie;
    int32_t  id;
    uint32_t flags;    // Important for padding! Do not remove.
  };

  class RPCCommRead : public RPCComm    {
  public:
    char which[1];
  };

  class RPCCommReadCookie : public RPCCommCookie    {
  public:
    char which[1];
  };

  class RPCCommRegex : public RPCComm    {
  public:
    char which[1];
  };

  class RPCCommRegexCookie : public RPCCommCookie    {
  public:
    char which[1];
  };

  class RPCCommClear : public RPCComm   {
  public:
    char which[1];
  };

  class RPCCommClearCookie : public RPCCommCookie   {
  public:
    char which[1];
  };
}
#include <RTL/Unpack.h>
#endif
