//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef GAUCHO_RATEMGR_H
#define GAUCHO_RATEMGR_H 1

/// Framework include files
#include <Gaucho/MonitorClass.h>

/// C/C++ include files
#include <string>

/// Online namespace declaration
namespace Online  {

  /// Forward declarations
  class MonitorClass;

  /// Definition of the rate manager class
  /**
   *  \author  Beat Jost
   *  \version 1.0
   */
  class RateMgr  {
  public:
    unsigned long deltaT = 0;
  private:
    MonitorClass* monClass { nullptr };
  public:

    RateMgr(MonitorClass* cl);
    virtual ~RateMgr() = default;
    void zero();
    void makeRates(unsigned long dt);
    void CreateOutputServices(const std::string& infix);
  };
}      // End namespace Online
#endif // GAUCHO_RATEMGR_H
