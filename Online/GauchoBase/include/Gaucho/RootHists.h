//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef ONLINE_GAUCHO_ROOTHISTS_H
#define ONLINE_GAUCHO_ROOTHISTS_H

#include <TH1D.h>
#include <TH2D.h>
#include <TH3D.h>
#include <TProfile.h>
#include <TProfile2D.h>
#include <TProfile3D.h>
#include <AIDA/IProfile1D.h>
#include <AIDA/IProfile2D.h>
#include <AIDA/IHistogram1D.h>
#include <AIDA/IHistogram2D.h>
#include <AIDA/IHistogram3D.h>
#include <Gaucho/Utilities.h>
#include <Gaucho/dimhist.h>

#include <cstring>

/// Online namespace declaration
namespace Online   {

  class HistSerDes;
  template <typename HISTO> class MonHist;

  inline void copy_data(void* to, const void* from, size_t len)    {
    if ( from )
      ::memcpy(to, from, len);
    else
      ::memset(to, 0, len);
  }

  template <typename BASE> class MyTH1 : public BASE  {
    friend class HistSerDes;
  public:
    using BASE::BASE;
    void   *GetEntryArr()                 {  return this->fArray;                     }
    //double *GetSumw2Arr()                 {  return this->GetSumw2()->GetArray();     }
    void movetodimbuffer(DimHistbuff1 *b);
    void movefromdimbuffer(const void *base);
  };
  typedef MyTH1<TH1D> MyTH1D;
  typedef MyTH1<TH1F> MyTH1F;
  typedef MyTH1<TH1I> MyTH1I;
  typedef MyTH1<TH1S> MyTH1S;
  typedef MyTH1<TH1C> MyTH1C;

  class MyTProfile : public TProfile  {
    friend class HistSerDes;
  public:
    using TProfile::TProfile;
    double *GetSumwArr()                  {  return this->fArray;                     }
    double *GetEntryArr()                 {  return this->fBinEntries.fArray;         }
    double *GetSumw2Arr()                 {  return this->GetSumw2()->GetArray();     }
    double *GetBinSumw2Arr()              {  return this->GetBinSumw2()->GetArray();  }
    double get_fYmin()                    {  return this->fYmin;                      }
    double get_fYmax()                    {  return this->fYmax;                      }
    void movetodimbuffer(DimHistbuff1 *b)   {
      void *base = add_ptr(b, b->dataoff);
      size_t blocksize = this->GetNcells()*sizeof(double);
      copy_data(add_ptr(base, 0*blocksize), this->GetEntryArr(),    blocksize);
      copy_data(add_ptr(base, 1*blocksize), this->GetSumwArr(),     blocksize);
      copy_data(add_ptr(base, 2*blocksize), this->GetSumw2Arr(),    blocksize);
      copy_data(add_ptr(base, 3*blocksize), this->GetBinSumw2Arr(), blocksize);
      b->min     = this->fMinimum;
      b->max     = this->fMaximum;
      b->sumw    = this->fTsumw;
      b->sumw2   = this->fTsumw2;
      b->sumwx   = this->fTsumwx;
      b->sumwx2  = this->fTsumwx2;
      b->sumwy   = this->fTsumwy;
      b->sumwy2  = this->fTsumwy2;
      b->yminval = this->fYmin;
      b->ymaxval = this->fYmax;
    }
    void movefromdimbuffer(const void *base)    {
      int blocksize = this->GetNcells()*sizeof(double);
      ::memcpy(this->GetEntryArr(),    add_ptr(base,0*blocksize), blocksize);
      ::memcpy(this->GetSumwArr(),     add_ptr(base,1*blocksize), blocksize);
      ::memcpy(this->GetSumw2Arr(),    add_ptr(base,2*blocksize), blocksize);
      ::memcpy(this->GetBinSumw2Arr(), add_ptr(base,3*blocksize), blocksize);
    }
  };

  class MyTH2D : public TH2D  {
    friend class HistSerDes;
  public:
    using TH2D::TH2D;
    double *GetEntryArr()                 {  return this->fArray;                     } 
    double *GetSumw2Arr()                 {  return this->GetSumw2()->GetArray();     }
    void movetodimbuffer(DimHistbuff1 *b)   {
      void *base = add_ptr(b, b->dataoff);
      int blocksize = this->GetNcells()*sizeof(double);
      copy_data(add_ptr(base,0*blocksize), this->GetEntryArr(), blocksize);
      copy_data(add_ptr(base,1*blocksize), this->GetSumw2Arr(), blocksize);
      b->min    = this->fMinimum;
      b->max    = this->fMaximum;
      b->sumw   = this->fTsumw;
      b->sumw2  = this->fTsumw2;
      b->sumwx  = this->fTsumwx;
      b->sumwx2 = this->fTsumwx2;
      b->sumwy  = this->fTsumwy;
      b->sumwy2 = this->fTsumwy2;
      //b->sumwxy = this->fTsumwxy;
    }
    void movefromdimbuffer(const void *base)    {
      int blocksize = this->GetNcells()*sizeof(double);
      ::memcpy(this->GetEntryArr(), add_ptr(base,0*blocksize), blocksize);
      ::memcpy(this->GetSumw2Arr(), add_ptr(base,1*blocksize), blocksize);
    }
  };

  class MyTH3D : public TH3D  {
    friend class MonHist<const ::AIDA::IHistogram3D>;
    friend class HistSerDes;
  public:
    using TH3D::TH3D;
    double *GetEntryArr()                 {  return fArray;                           }
    double *GetSumw2Arr()                 {  return this->GetSumw2()->GetArray();     }
    void movetodimbuffer(DimHistbuff1 *b)   {
      void *base = add_ptr(b, b->dataoff);
      int blocksize = this->GetNcells()*sizeof(double);
      copy_data(add_ptr(base,0*blocksize), this->GetEntryArr(), blocksize);
      copy_data(add_ptr(base,1*blocksize), this->GetSumw2Arr(), blocksize);
      b->min    = this->fMinimum;
      b->max    = this->fMaximum;
      b->sumw   = this->fTsumw;
      b->sumw2  = this->fTsumw2;
      b->sumwx  = this->fTsumwx;
      b->sumwx2 = this->fTsumwx2;
      b->sumwy  = this->fTsumwy;
      b->sumwy2 = this->fTsumwy2;
      b->sumwz  = this->fTsumwz;
      b->sumwz2 = this->fTsumwz2;
      b->sumwt  = this->fTsumwxy;
      b->sumwt2 = this->fTsumwxz;
      //b->sumwyz = this->fTsumwyz;
    }
    void movefromdimbuffer(const void *base)    {
      int blocksize = this->GetNcells()*sizeof(double);
      ::memcpy(this->GetEntryArr(), add_ptr(base,0*blocksize), blocksize);
      ::memcpy(this->GetSumw2Arr(), add_ptr(base,1*blocksize), blocksize);
    }
  };

  class MyTProfile2D : public TProfile2D  {
    friend class MonHist<const ::AIDA::IProfile2D>;
    friend class HistSerDes;
  public:
    using TProfile2D::TProfile2D;
    double *GetSumwArr()                  {  return fArray;                           }
    double *GetEntryArr()                 {  return fBinEntries.GetArray();           }
    double *GetSumw2Arr()                 {  return this->GetSumw2()->GetArray();     }
    double *GetBinSumw2Arr()              {  return this->GetBinSumw2()->GetArray();  }
    //double gfZmin()                       {  return fZmin;                            }
    //double gfZmax()                       {  return fZmax;                            }
    //int NBinEntries()                     {  return fBinEntries.fN;                   }
    //int NSumw2()                          {  return this->GetSumw2()->fN;             }
    //int NBinSumw2()                       {  return this->GetBinSumw2()->fN;          }
    //void SetfZmin(double d)               {  fZmin =d;                                }
    //void SetfZmax(double d)               {  fZmax =d;                                }
    void movetodimbuffer(DimHistbuff1 *bb)   {
      DimHistbuff3 *b = (DimHistbuff3*)bb; 
      void *base = add_ptr(b, b->dataoff);
      int blocksize = this->GetNcells()*sizeof(double);
      copy_data(add_ptr(base,0*blocksize), this->GetEntryArr(),    blocksize);
      copy_data(add_ptr(base,1*blocksize), this->GetSumwArr(),     blocksize);
      copy_data(add_ptr(base,2*blocksize), this->GetSumw2Arr(),    blocksize);
      copy_data(add_ptr(base,3*blocksize), this->GetBinSumw2Arr(), blocksize);
      b->min     = this->fMinimum;
      b->max     = this->fMaximum;
      b->sumw    = this->fTsumw;
      b->sumw2   = this->fTsumw2;
      b->sumwx   = this->fTsumwx;
      b->sumwx2  = this->fTsumwx2;
      b->sumwy   = this->fTsumwy;
      b->sumwy2  = this->fTsumwy2;
      b->sumwz   = this->fTsumwz;
      b->sumwz2  = this->fTsumwz2;
      //b->sumwyz = this->fTsumwyz;

      b->yminval = 0e0;
      b->ymaxval = 0e0;
      b->zminval = this->fZmin;
      b->zmaxval = this->fZmax;
    }
    void movefromdimbuffer(const void *base)    {
      int blocksize = this->GetNcells()*sizeof(double);
      ::memcpy(this->GetEntryArr(),    add_ptr(base,0*blocksize), blocksize);
      ::memcpy(this->GetSumwArr(),     add_ptr(base,1*blocksize), blocksize);
      ::memcpy(this->GetSumw2Arr(),    add_ptr(base,2*blocksize), blocksize);
      ::memcpy(this->GetBinSumw2Arr(), add_ptr(base,3*blocksize), blocksize);
    }
  };

  class MyTProfile3D : public TProfile3D  {
    friend class HistSerDes;
  public:
    using TProfile3D::TProfile3D;
    double *GetSumwArr()                  {  return this->fArray;                     }
    double *GetEntryArr()                 {  return this->fBinEntries.GetArray();     }
    double *GetSumw2Arr()                 {  return this->GetSumw2()->GetArray();     }
    double *GetBinSumw2Arr()              {  return this->GetBinSumw2()->GetArray();  }
    //double TMin()  const                  {  return this->fTmin;                      }
    //double TMax()  const                  {  return this->fTmax;                      }
    //int NBinEntries()  const              {  return this->fBinEntries.fN;             }
    //int NSumw2()  const                   {  return this->GetSumw2()->fN;             }
    //int NBinSumw2() const                 {  return this->GetBinSumw2()->fN;          }
    void SetfTmin(double d)               {  this->fTmin =d;                          }
    void SetfTmax(double d)               {  this->fTmax =d;                          }
    void movetodimbuffer(DimHistbuff1 *bb)   {
      DimHistbuff3 *b = (DimHistbuff3*)bb; 
      void *base = add_ptr(b, b->dataoff);
      int blocksize = this->GetNcells()*sizeof(double);
      copy_data(add_ptr(base,0*blocksize), this->GetEntryArr(),blocksize);
      copy_data(add_ptr(base,1*blocksize), this->GetSumwArr(),blocksize);
      copy_data(add_ptr(base,2*blocksize), this->GetSumw2Arr(),blocksize);
      copy_data(add_ptr(base,3*blocksize), this->GetBinSumw2Arr(),blocksize);
      b->min     = this->fMinimum;
      b->max     = this->fMaximum;
      b->sumw    = this->fTsumw;
      b->sumw2   = this->fTsumw2;
      b->sumwx   = this->fTsumwx;
      b->sumwx2  = this->fTsumwx2;
      b->sumwy   = this->fTsumwy;
      b->sumwy2  = this->fTsumwy2;
      b->sumwz   = this->fTsumwz;
      b->sumwz2  = this->fTsumwz2;
      b->sumwt   = this->fTsumwt;
      b->sumwt2  = this->fTsumwt2;
      b->yminval = this->fTmin;    // !!!
      b->ymaxval = this->fTmax;    // !!!
      b->zminval = 0e0;
      b->zmaxval = 0e0;
    }
    void movefromdimbuffer(void *base)    {
      int blocksize = this->GetNcells()*sizeof(double);
      ::memcpy(this->GetEntryArr(),    add_ptr(base,0*blocksize),blocksize);
      ::memcpy(this->GetSumwArr(),     add_ptr(base,1*blocksize),blocksize);
      ::memcpy(this->GetSumw2Arr(),    add_ptr(base,2*blocksize),blocksize);
      ::memcpy(this->GetBinSumw2Arr(), add_ptr(base,3*blocksize),blocksize);
    }
  };
}
#endif // ONLINE_GAUCHO_ROOTHISTS_H
