//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
/*
 * SerialHeader.h
 *
 *  Created on: Aug 26, 2010
 *      Author: beat
 */

#ifndef SERIALHEADER_H_
#define SERIALHEADER_H_

#include <cstring>

#define SERHEADER_Compress 1<<0
#define SERHEADER_Version  2
#define SERIAL_MAGIC       0xfeedbabe

namespace Online   {

  class SerialHeader_V1   {
  public:
    uint32_t m_magic;
    int32_t  flags;
    int32_t  version;
    int32_t  comp_version;
    int64_t  ser_tim;
    int64_t  run_number;
    int32_t  buffersize;
  };

  class SerialHeader_V2   {
  public:
    uint32_t m_magic;
    int32_t  flags;
    int32_t  version;
    int32_t  comp_version;
    int64_t  ser_tim;
    int64_t  run_number;
    int32_t  buffersize;
    uint32_t updateInterval;
    int32_t  level;
  };

  class SerialHeader   {
  public:
    uint32_t m_magic        { SERIAL_MAGIC };
    int32_t  flags          { 0 };
    int32_t  version        { SERHEADER_Version };
    int32_t  comp_version   { 0 };
    int64_t  ser_tim        { 0 };
    int64_t  run_number     { 0 };
    int32_t  buffersize     { 0 };
    uint32_t updateInterval { 0 };
    int32_t  level          { 0 };

    SerialHeader() = default;
    SerialHeader(const SerialHeader& copy) = default;
    SerialHeader & operator = (const SerialHeader &copy);

    template <typename RETURN_TYPE=void> RETURN_TYPE* endPtr()  {
      switch(version)    {
      case 1:
      return (RETURN_TYPE*)((int8_t*)this + sizeof(SerialHeader_V1));
      case 2:
      return (RETURN_TYPE*)((int8_t*)this + sizeof(SerialHeader_V2));
      default:
      return (RETURN_TYPE*)((int8_t*)this + sizeof(SerialHeader));
      }
    }

    template <typename RETURN_TYPE=void> const RETURN_TYPE* endPtr()  const {
      switch(version)    {
      case 1:
      return (RETURN_TYPE*)((int8_t*)this + sizeof(SerialHeader_V1));
      case 2:
      return (RETURN_TYPE*)((int8_t*)this + sizeof(SerialHeader_V2));
      default:
      return (RETURN_TYPE*)((int8_t*)this + sizeof(SerialHeader));
      }
    }

    size_t SizeOf() const  {
      switch(version)   {
      case 1:
	return sizeof(SerialHeader_V1);
      case 2:
	return sizeof(SerialHeader_V2);
      default:
	return sizeof(SerialHeader);
      }
    }
  };

  inline SerialHeader & SerialHeader::operator = (const SerialHeader &copy)  {
    ::memcpy(this, &copy, copy.SizeOf());
    return *this;
  }
}
#endif /* SERIALHEADER_H_ */
