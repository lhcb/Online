//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef ONLINE_GAUCHO_TASKRPC_H_
#define ONLINE_GAUCHO_TASKRPC_H_

/// Framework include files
#include <Gaucho/RPCRec.h>

/// C/C++ include files
#include <vector>
#include <string>
#include <memory>
#include <list>
#include <set>
#include <map>

/// Online namespace declaration
namespace Online  {

  /// Forward declarations
  class RPCRec;
  class CntrDescr;


  /// RPC interactor with monitoring tasks
  /** @class TaskRPC
    *
    *   \version 1.0
    */
  class TaskRPC   {
  protected:
    /// DNS node name
    std::string m_dns;
    /// Variable for task name
    std::string m_task;
    /// RPC interactor
    std::unique_ptr<RPCRec> m_RPC;
    template <typename CONT> static int _taskList(const std::string& type, const std::string& dns, CONT& tasks);

  public:
    /// Inhibit default constructor
    TaskRPC() = delete;
    /// Initializing constructor
    TaskRPC(const std::string& type, const std::string& task, const std::string& dns="", int tmo=0);
    /// Default destructor
    virtual ~TaskRPC();
    /// Access the tasks on a given DIM DNS publishing gaucho items
    static std::vector<std::string> taskList(const std::string& type, const std::string& dns);
    /// Access data size
    std::size_t getSize()  const;
    /// Access data buffer
    const char* getData()  const;
    /// Access task name
    const std::string& taskName()  const   {  return m_task;  }
    ///
    int directory (std::vector<std::string>& counters);
    ///
    std::pair<int, std::vector<std::string> > directory(const std::string& match="");
    ///
    int items(const std::vector<std::string>& names);
    ///
    int items(const std::string& selection);
    ///
    const RPCRec::NAMEVEC& names()  const;
    ///
    const RPCRec::PTRMAP& items()  const;
  };
}
#endif // ONLINE_GAUCHO_TASKRPC_H_
