//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef DIMHIST_DEF
#define DIMHIST_DEF

/// Framework include files
#include <Gaucho/Utilities.h>

/// C/C++ include files
#include <cstddef>
#include <cstring>
#include <cstdint>

#define FLAGS_XLABELS 1<<0
#define FLAGS_YLABELS 1<<1

#if defined(__clang__) || defined(__CLING__)
/// #pragma clang diagnostic push
/// #pragma clang diagnostic ignored "-Wclass-memaccess"
#elif defined(__GNUC__) && __GNUC__ >= 9
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wclass-memaccess"
#endif

/// Online namespace declaration
namespace Online  {

  class DimBuffBase   {
  public:
    DimBuffBase() = default;
    int32_t  reclen        { sizeof(DimBuffBase) };
    uint32_t type          { 0 };
    int32_t  flags         { 0 };
    int32_t  nameoff       { 0 };
    int32_t  namelen       { 0 };
    int32_t  titoff        { 0 };
    int32_t  titlen        { 0 };
    int32_t  dataoff       { 0 };
    int32_t  addoffset     { 0 };
    template < typename TYPE > TYPE* as()   const    {
      return (TYPE*)this;
    }
    /// Only call this once the structure is filled!
    void clear()   {
      ::memset(add_ptr(this, this->dataoff), 0, this->reclen - this->dataoff);
    }
    const char* name()   const   {
      return add_ptr<char>(this, this->nameoff);
    }
    const double* data()  const  {
      return add_ptr<double>(this, this->dataoff);
    }
    const double* data(std::size_t offset)  const  {
      return add_ptr<double>(this, offset + this->dataoff);
    }
    bool is_counter()  const;
    bool is_histo()  const;
  };
}

#if defined(__clang__) || defined(__CLING__)
///#pragma clang diagnostic pop
#elif defined(__GNUC__) && __GNUC__ >= 5
#pragma GCC diagnostic pop
#endif

/// Online namespace declaration
namespace Online  {

  class DimAxis   {
  public:
    int32_t     nbin       {   0 };
    double      min        { 0.0 };
    double      max        { 0.0 };
    int32_t     lablen     {   0 };
    int32_t     laboff     {   0 };
  };

  class DimDirEnt   {
  public:
    int32_t type       { 0 };
    char    name[32];
  };

  class DimStatBuff : public DimBuffBase  {
  public:
    double nentries;
    double sumw;
    double sumw2;
    double min;
    double max;
  };

  class DimHistbuff1 : public DimBuffBase   {
  public:
    int32_t dim;
    double  nentries;
    double  min;
    double  max;
    double  sumw;
    double  sumw2;
    double  sumwx;
    double  sumwx2;
    double  sumwy;
    double  sumwy2;
    double  sumwz;
    double  sumwz2;
    double  sumwt;
    double  sumwt2;
    double  yminval;
    double  ymaxval;
    DimAxis x;
    size_t num_entries()  const  {  return (size_t)this->nentries;  }
    size_t num_cells()    const;
    double*       data()         { return add_ptr<double>(this, this->dataoff);  }
    const double* data()  const  { return add_ptr<double>(this, this->dataoff);  }
    static int32_t  buffer_name_offset(int32_t  dim);
  };

  class DimHistbuff2 : public DimHistbuff1   {
  public:
    DimAxis y;
  };

  class DimHistbuff3 : public DimHistbuff2    {
  public:
    DimAxis z;
    double  zminval;
    double  zmaxval;
  };

  inline std::size_t DimHistbuff1::num_cells()  const  {
    if ( this->dim == 1 )   {
      return this->x.nbin + 2;
    }
    else if ( dim == 2 )   {
      const auto* b = (const DimHistbuff2*)this;
      return (b->x.nbin + 2) * (b->y.nbin + 2);
    }
    else if ( dim == 3 )  {
      const auto* b = (const DimHistbuff3*)this;
      return (b->x.nbin + 2) * (b->y.nbin + 2) * (b->z.nbin + 2);
    }
    return 0;
  }

  inline int32_t DimHistbuff1::buffer_name_offset(int32_t dim)   {
    if ( dim == 1 )
      return sizeof(DimHistbuff1);
    else if ( dim == 2 )
      return sizeof(DimHistbuff2);
    else if ( dim == 3 )
      return sizeof(DimHistbuff3);
    return sizeof(DimHistbuff1);
  }

}
#endif
