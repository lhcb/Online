//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Author     : Markus Frank
//==========================================================================

extern "C" int GAUCHO_APP_CALL (int argc, char** argv);

int main (int argc, char** argv)    {
  return GAUCHO_APP_CALL (argc, argv);
}
