//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  AIDA.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : B.Jost
//==========================================================================
#if !defined(GAUCHO_NO_ROOT)

#include <TObject.h>
#include <ostream>
#pragma push_macro("Gaudi")
#ifdef Gaudi
#undef Gaudi
#endif

///  Gaudi namespace declaration
namespace Gaudi {

  /**@class HistogramBase HistogramBase.h HistogramSvc/HistogramBase.h
    *
    *   Common base class for all histograms
    *   Use is solely functional to minimize dynamic_casts inside HistogramSvc
    *
    *   @author  M.Frank
    *   @version 1.0
    */
  class HistogramBase {
  public:
    /// ROOT object implementation
    virtual TObject* representation() const = 0;
    /// Adopt ROOT histogram representation
    virtual void adoptRepresentation(TObject*rep) = 0;
    /// Print histogram to output stream
    virtual std::ostream& print(std::ostream& s) const = 0;
    /// Write (binary) histogram to output stream
    virtual std::ostream& write(std::ostream& s) const = 0;
    /// Write (binary) histogram to file
    virtual int           write(const char* file_name) const = 0;
    virtual ~HistogramBase() = default;
  }; // class

} // namespace Gaudi

using Gaudi::HistogramBase;
#pragma pop_macro("Gaudi")

#include <AIDA/IBaseHistogram.h>
#include <AIDA/IHistogram1D.h>
#include <AIDA/IHistogram2D.h>
#include <AIDA/IHistogram3D.h>
#include <AIDA/IProfile1D.h>
#include <AIDA/IProfile2D.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TH3D.h>
#include <TProfile.h>
#include <TProfile2D.h>

namespace Online  {
  template <typename TO> TO* cast_histo(const AIDA::IBaseHistogram* aidahist)  {
    AIDA::IBaseHistogram* aida = const_cast<AIDA::IBaseHistogram*>(aidahist);
    TO* to = dynamic_cast<TO*>(aida);
    if ( !to )  {
      HistogramBase* base = dynamic_cast<HistogramBase*>(aida);
      if ( base )  {
	to = dynamic_cast<TO*>(base->representation());
      }
    }
    return to;
  }
  template AIDA::IBaseHistogram* cast_histo<AIDA::IBaseHistogram>(const AIDA::IBaseHistogram* aidahist);
  template AIDA::IHistogram1D* cast_histo<AIDA::IHistogram1D>(const AIDA::IBaseHistogram* aidahist);
  template AIDA::IHistogram2D* cast_histo<AIDA::IHistogram2D>(const AIDA::IBaseHistogram* aidahist);
  template AIDA::IHistogram3D* cast_histo<AIDA::IHistogram3D>(const AIDA::IBaseHistogram* aidahist);
  template AIDA::IProfile1D*   cast_histo<AIDA::IProfile1D>(const AIDA::IBaseHistogram* aidahist);
  template AIDA::IProfile2D*   cast_histo<AIDA::IProfile2D>(const AIDA::IBaseHistogram* aidahist);
  template TH1D*       cast_histo<TH1D>(const AIDA::IBaseHistogram* aidahist);
  template TH2D*       cast_histo<TH2D>(const AIDA::IBaseHistogram* aidahist);
  template TH3D*       cast_histo<TH3D>(const AIDA::IBaseHistogram* aidahist);
  template TProfile*   cast_histo<TProfile>(const AIDA::IBaseHistogram* aidahist);
  template TProfile2D* cast_histo<TProfile2D>(const AIDA::IBaseHistogram* aidahist);
  template TObject*    cast_histo<TObject>(const AIDA::IBaseHistogram* aidahist);
}
#endif
