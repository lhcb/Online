//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#include <Gaucho/BRTL_Lock.h>
#include <RTL/rtl.h>
#include <cerrno>

using namespace Online;

BRTLLock::BRTLLock()  {
  pthread_mutex_t tmp = PTHREAD_ERRORCHECK_MUTEX_INITIALIZER_NP;
  this->name = "Lock_Name_Not_Set";
  this->lid = tmp;
}

BRTLLock::~BRTLLock()  {
  ::pthread_mutex_destroy(&this->lid);
}

int BRTLLock::lockMutex()  {
  this->tryLockTime=std::chrono::high_resolution_clock::now();
  int status = ::pthread_mutex_lock(&this->lid);
  if( status != 0 )  {
#ifndef NO_ONLINEBASE
    ::lib_rtl_output(LIB_RTL_INFO, "%s: Undesired return code  from locking mutex %s Status %s",
		     RTL::processName().c_str(), this->name.c_str(), RTL::errorString(status).c_str());
#endif
    if (status == EINVAL)  {
      ::printf("Invalid Argument... Ignoring...\n");
      return 0;
    }
  }
  this->succLockTime = std::chrono::high_resolution_clock::now();
  return status;
}

int BRTLLock::unlockMutex()  {
  int status = ::pthread_mutex_unlock(&this->lid);
  if( status != 0 )  {
#ifndef NO_ONLINEBASE
    ::lib_rtl_output(LIB_RTL_INFO,"%s: Undesired return code  from unlocking mutex %s Status %s\n",
		     RTL::processName().c_str(), this->name.c_str(), RTL::errorString(status).c_str());
#endif
    if( status == EPERM )  {
      status = 0;
    }
    if( status == EINVAL )  {
      ::printf("Invalid Argument... Ignoring...\n");
      return 0;
    }
  }
  this->unLockTime = std::chrono::high_resolution_clock::now();
  return status;
}
