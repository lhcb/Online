//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================

/// Framework include files
#include <Gaucho/Compression.h>
//#define GAUCHO_HAVE_COMPRESSION 1

#include <zlib.h>
#include <cstdint>

/// Online namespace declaration
namespace Online  {

  /// Gaucho namespace
  namespace gaucho {

#if defined(GAUCHO_HAVE_COMPRESSION) && GAUCHO_HAVE_COMPRESSION>0
    static constexpr const int32_t CHUNK =            0x4000;
    static constexpr const int32_t windowBits =           15;
    static constexpr const int32_t ENABLE_ZLIB_GZIP =     32;
    static constexpr const int32_t GZIP_ENCODING =        16;
#endif

    /* These are parameters to inflateInit2. See
       http://zlib.net/manual.html for the exact meanings. */

    /// compress a byte buffer
    std::pair<std::size_t, void*>
    compress(const void* data, std::size_t data_len, std::vector<uint8_t>& buffer)  {
      uint8_t* data_ptr = (uint8_t*)data;
#if defined(GAUCHO_HAVE_COMPRESSION) && GAUCHO_HAVE_COMPRESSION>0
      z_stream strm;
      uint8_t  out[CHUNK+1];
      int32_t  status, window = windowBits + GZIP_ENCODING;

      ::memset(&strm, 0, sizeof(strm));
      strm.zalloc = Z_NULL;
      strm.zfree  = Z_NULL;
      strm.opaque = Z_NULL;
      strm.avail_in = 0;
      strm.next_in  = data_ptr;
      status = ::deflateInit2 (&strm, 
                               Z_DEFAULT_COMPRESSION,
                               Z_DEFLATED,
                               window, 8,
                               Z_DEFAULT_STRATEGY);
      if( status < 0 )   {
        std::stringstream str;
        str << "ObjRPC: [Failed to initialize zlib/gzip " 
            << std::error_condition(EINVAL,std::system_category()).message() << "]";
        goto Default;
      }
      strm.next_in  = data_ptr;
      strm.avail_in = data_len;
      do {
        strm.avail_out = CHUNK;
        strm.next_out  = out;
        status = ::deflate(&strm, Z_FINISH);
        if( status < 0 )   {
          std::stringstream str;
          str << "XMLRPC [Failed to deflate buffer with zlib/gzip " 
              << std::error_condition(EINVAL,std::system_category()).message() << "]";
          ::deflateEnd (&strm);
          goto Default;
        }
        std::copy(out, out+CHUNK-strm.avail_out, std::back_inserter(buffer));
      }  while (strm.avail_out == 0);
      ::deflateEnd(&strm);
      return { buffer.size(), &buffer.at(0) };

    Default:
      return { data_len, data_ptr };
#else
      return { data_len+buffer.size(), data_ptr-buffer.size() };
#endif
    }

    /// Inflate the response
    std::pair<std::size_t, void*>
    decompress(const void* data, std::size_t data_len, std::vector<uint8_t>& buffer)  {
      uint8_t* data_ptr = (uint8_t*)data;
#if defined(GAUCHO_HAVE_COMPRESSION) && GAUCHO_HAVE_COMPRESSION>0
      uint8_t  out[CHUNK+1];
      z_stream strm;

      buffer.reserve(1024*1024);
      ::memset(&strm, 0, sizeof(strm));
      strm.zalloc   = Z_NULL;
      strm.zfree    = Z_NULL;
      strm.opaque   = Z_NULL; 
      strm.next_in  = data_ptr;
      strm.avail_in = 0;
      int32_t window = windowBits + ENABLE_ZLIB_GZIP;
      int32_t status = ::inflateInit2(&strm, window);
      if( status < 0 )   {
        std::stringstream str;
        str << "XMLRPC [Failed to initialize zlib/gzip "
            << std::error_condition(EINVAL,std::system_category()).message() << "]";
        goto Default;
      }
      strm.avail_in = data_len;
      strm.next_in  = data_ptr;
      do {
        strm.avail_out = CHUNK;
        strm.next_out  = out;
        status         = ::inflate (&strm, Z_NO_FLUSH);
        switch (status) {
        case Z_OK:
        case Z_STREAM_END:
          break;
        case Z_BUF_ERROR:
        default:  {
          inflateEnd(&strm);
          std::stringstream str;
          str << "XMLRPC [Failed inflate buffer with zlib/gzip : " << status << "]";
          goto Default;
        }
        }
        std::copy(out, out+CHUNK-strm.avail_out, std::back_inserter(buffer));
      }  while (strm.avail_out == 0);
      ::inflateEnd(&strm);
      return { buffer.size(), &buffer.at(0) };
    Default:
      return { 0UL, nullptr };
#else
      return { data_len+buffer.size(), data_ptr-buffer.size() };
#endif
    }
  }
}

