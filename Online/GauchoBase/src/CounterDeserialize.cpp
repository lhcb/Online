//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================

/// Framework include files
#include <Gaucho/CounterDeserialize.h>
#include <Gaucho/MonTypes.h>
#include <Gaucho/dimhist.h>

namespace  {
  template <typename T> void _copy_bin(Online::CntrDescr* desc, const void* data)    {
    desc->gaudi.binomial.entries_true  = *(add_ptr<const long>(data,0));
    desc->gaudi.binomial.entries_false = *(add_ptr<const T>(data,sizeof(long)));
    desc->gaudi.binomial.entries = 
      desc->gaudi.binomial.entries_true + 
      desc->gaudi.binomial.entries_false;
  }
  template <typename T> void _copy_avg(Online::CntrDescr* desc, const void* data)    {
    desc->gaudi.average.entries = *(add_ptr<const long>(data,0));
    desc->gaudi.average.mean    = *(add_ptr<const double>(data,sizeof(long)));
    desc->gaudi.average.sum     = *(add_ptr<const T>(data,sizeof(long)+sizeof(double)));
  }
  template <typename T> void _copy_stat(Online::CntrDescr* desc, const void* data)    {
    desc->gaudi.stat.entries = *(add_ptr<const long>(data,0));
    desc->gaudi.stat.mean    = *(add_ptr<const double>(data,sizeof(long)));
    desc->gaudi.stat.sum     = *(add_ptr<const T>(data,sizeof(long)+sizeof(double)));
    desc->gaudi.stat.sum2    = *(add_ptr<const T>(data,sizeof(long)+sizeof(double)+sizeof(T)));
  }
  template <typename T> void _copy_sigma(Online::CntrDescr* desc, const void* data)    {
    desc->gaudi.sigma.entries = *(add_ptr<const long>(data,0));
    desc->gaudi.sigma.mean    = *(add_ptr<const double>(data,sizeof(long)));
    desc->gaudi.sigma.sum     = *(add_ptr<const T>(data,sizeof(long)+sizeof(double)));
    desc->gaudi.sigma.sum2    = *(add_ptr<const T>(data,sizeof(long)+sizeof(double)+sizeof(T)));
  }
}

Online::CntrDescr* Online::CounterSerDes::de_serialize( const void* ptr, const char* nam ) {
  DimBuffBase* p = (DimBuffBase*)ptr;
  if ( nam == 0 ) { nam = add_ptr<const char>( p, p->nameoff ); }
  auto h     = std::make_unique<CntrDescr>();
  const void* dat  = add_ptr<const void>( p, p->dataoff );
  const long* plng = add_ptr<const long>( p, p->dataoff );
  int   bsiz = p->reclen - p->dataoff;

  h->name    = nam;
  h->type    = p->type;
  h->scalars.ul_data = 0;
  switch ( p->type ) {
  case C_GAUDIACCCHAR:
  case C_GAUDIACCuCHAR:

  case C_GAUDIACCSHORT:
  case C_GAUDIACCuSHORT:

  case C_INT:
  case C_ATOMICINT:
  case C_GAUDIACCINT:
    h->scalars.i_data = *(int*)dat;
    break;
  case C_UINT:
  case C_GAUDIACCuINT:
    h->scalars.ui_data = *(unsigned int*)dat;
    break;
  case C_LONGLONG:
  case C_ATOMICLONG:
  case C_GAUDIACCLONG:
    h->scalars.l_data = *(long*)dat;
    break;
  case C_ULONG:
  case C_GAUDIACCuLONG:
  case C_GAUDIMSGACClu:
    h->scalars.ul_data = *(unsigned long*)dat;
    break;
  case C_FLOAT:
  case C_RATEFLOAT:
  case C_ATOMICFLOAT:
    h->scalars.f_data = (float)( *(float*)dat );
    break;
  case C_DOUBLE:
  case C_RATEDOUBLE:
  case C_ATOMICDOUBLE:
    h->scalars.d_data = *(double*)dat;
    break;
  case C_INTSTAR:
  case C_UINTSTAR:
    h->nel = bsiz / sizeof( int );
    h->ptr.reset( new unsigned char[bsiz] );
    ::memcpy( h->ptr.get(), plng, bsiz );
    break;
  case C_LONGSTAR:
  case C_ULONGSTAR:
    h->nel = ( bsiz ) / sizeof( long );
    h->ptr.reset( new unsigned char[bsiz] );
    ::memcpy( h->ptr.get(), plng, bsiz );
    break;
  case C_FLOATSTAR:
    h->nel = ( bsiz ) / sizeof( float );
    h->ptr.reset( new unsigned char[bsiz] );
    ::memcpy( h->ptr.get(), plng, bsiz );
    break;
  case C_DOUBLESTAR:
  case C_RATEDOUBLESTAR:
    h->nel = ( bsiz ) / sizeof( double );
    h->ptr.reset( new unsigned char[bsiz] );
    ::memcpy( h->ptr.get(), plng, bsiz );
    break;
  case C_LONGPAIR:
    h->lp_data.first = *plng;
    plng++;
    h->lp_data.second = *plng;
    break;
  case C_INTPAIR:
    h->ip_data.first = (int)*plng;
    plng++;
    h->ip_data.second = (int)*plng;
    break;

  case C_GAUDIAVGACCc:     _copy_avg<char>(h.get(), dat);              break;
  case C_GAUDIAVGACCcu:    _copy_avg<unsigned char>(h.get(), dat);     break;
  case C_GAUDIAVGACCs:     _copy_avg<short>(h.get(), dat);             break;
  case C_GAUDIAVGACCsu:    _copy_avg<unsigned short>(h.get(), dat);    break;
  case C_GAUDIAVGACCi:     _copy_avg<int>(h.get(), dat);               break;
  case C_GAUDIAVGACCiu:    _copy_avg<unsigned int>(h.get(), dat);      break;
  case C_GAUDIAVGACCl:     _copy_avg<long>(h.get(), dat);              break;
  case C_GAUDIAVGACClu:    _copy_avg<unsigned long>(h.get(), dat);     break;
  case C_GAUDIAVGACCf:     _copy_avg<float>(h.get(), dat);             break;
  case C_GAUDIAVGACCd:     _copy_avg<double>(h.get(), dat);            break;

  case C_GAUDISIGMAACCc:   _copy_sigma<char>(h.get(), dat);            break;
  case C_GAUDISIGMAACCcu:  _copy_sigma<unsigned char>(h.get(), dat);   break;
  case C_GAUDISIGMAACCs:   _copy_sigma<short>(h.get(), dat);           break;
  case C_GAUDISIGMAACCsu:  _copy_sigma<unsigned short>(h.get(), dat);  break;
  case C_GAUDISIGMAACCi:   _copy_sigma<int>(h.get(), dat);             break;
  case C_GAUDISIGMAACCiu:  _copy_sigma<unsigned int>(h.get(), dat);    break;
  case C_GAUDISIGMAACCl:   _copy_sigma<long>(h.get(), dat);            break;
  case C_GAUDISIGMAACClu:  _copy_sigma<unsigned long>(h.get(), dat);   break;
  case C_GAUDISIGMAACCf:   _copy_sigma<float>(h.get(), dat);           break;
  case C_GAUDISIGMAACCd:   _copy_sigma<double>(h.get(), dat);          break;

  case C_GAUDISTATACCc:    _copy_stat<char>(h.get(), dat);             break;
  case C_GAUDISTATACCcu:   _copy_stat<unsigned char>(h.get(), dat);    break;
  case C_GAUDISTATACCs:    _copy_stat<short>(h.get(), dat);            break;
  case C_GAUDISTATACCsu:   _copy_stat<unsigned short>(h.get(), dat);   break;
  case C_GAUDISTATACCi:    _copy_stat<int>(h.get(), dat);              break;
  case C_GAUDISTATACCiu:   _copy_stat<unsigned int>(h.get(), dat);     break;
  case C_GAUDISTATACCl:    _copy_stat<long>(h.get(), dat);             break;
  case C_GAUDISTATACClu:   _copy_stat<unsigned long>(h.get(), dat);    break;
  case C_GAUDISTATACCf:    _copy_stat<float>(h.get(), dat);            break;
  case C_GAUDISTATACCd:    _copy_stat<double>(h.get(), dat);           break;

  case C_GAUDIBINACCc:     _copy_bin<char>(h.get(), dat);              break;
  case C_GAUDIBINACCcu:    _copy_bin<unsigned char>(h.get(), dat);     break;
  case C_GAUDIBINACCs:     _copy_bin<short>(h.get(), dat);             break;
  case C_GAUDIBINACCsu:    _copy_bin<unsigned short>(h.get(), dat);    break;
  case C_GAUDIBINACCi:     _copy_bin<int>(h.get(), dat);               break;
  case C_GAUDIBINACCiu:    _copy_bin<unsigned int>(h.get(), dat);      break;
  case C_GAUDIBINACCl:     _copy_bin<long>(h.get(), dat);              break;
  case C_GAUDIBINACClu:    _copy_bin<unsigned long>(h.get(), dat);     break;
  case C_GAUDIBINACCf:     _copy_bin<float>(h.get(), dat);             break;
  case C_GAUDIBINACCd:     _copy_bin<double>(h.get(), dat);            break;

  default:
    break;
  }
  return h.release();
}
