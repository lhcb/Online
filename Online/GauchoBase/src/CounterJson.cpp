//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================

/// Framework include files
#include <Gaucho/CounterJson.h>
#include <Gaucho/Utilities.h>
#include <Gaucho/MonTypes.h>
#include <Gaucho/dimhist.h>

using json = nlohmann::json;

namespace Online   {

  namespace JsonCounterDeserialize   {
    template <typename TYPE> static json _scalar(const DimBuffBase* p, const std::string& typ);
    template <typename TYPE> static json _numbers(const DimBuffBase* b, const std::string& typ);
    template <typename FIRST, typename SECOND> static json _pair(const DimBuffBase* b, const std::string& typ);
    template <typename ENTRIES, typename SUM, typename MEAN=double>  
      static json _average(const DimBuffBase* b, const std::string& typ);
    template <typename ENTRIES, typename SUM, typename MEAN=double>  
      static json _sigma(const DimBuffBase* b, const std::string& typ);
    template <typename ENTRIES, typename SUM, typename MEAN=double>  
      static json _stat(const DimBuffBase* b, const std::string& typ);
    template <typename FIRST, typename SECOND> 
      static json _binomial(const DimBuffBase* b, const std::string& typ);
  }
  
  template <typename TYPE>  
  json JsonCounterDeserialize::_scalar(const DimBuffBase* p, const std::string& typ)   {
    const auto* item   = add_ptr<TYPE>(p, p->dataoff);
    return {{"type", typ}, {"data", *item }};
  }

  template <>  
  json JsonCounterDeserialize::_scalar<float>(const DimBuffBase* p, const std::string& typ)   {
    const auto* item   = add_ptr<double>(p, p->dataoff);
    return {{"type", typ}, {"data", float(*item) }};
  }

  template <typename FIRST, typename SECOND>  
  json JsonCounterDeserialize::_pair(const DimBuffBase* p, const std::string& typ)   {
    const auto* first   = add_ptr<FIRST>(p, p->dataoff);
    const auto* second  = add_ptr<SECOND>(p, p->dataoff+sizeof(FIRST));
    return {{"type", typ}, {"data", {{"first", *first}, {"second", *second }}}};
  }

  template <typename ENTRIES, typename SUM, typename MEAN>  
  json JsonCounterDeserialize::_average(const DimBuffBase* p, const std::string& typ)   {
    const auto* entries = add_ptr<ENTRIES>(p, p->dataoff);
    const auto* mean    = add_ptr<MEAN>   (p, p->dataoff+sizeof(ENTRIES));
    const auto* sum     = add_ptr<SUM>    (p, p->dataoff+sizeof(ENTRIES)+sizeof(MEAN));
    return {{"type", typ}, {"data", {{"nEntries", *entries}, {"sum", *sum }, {"mean", *mean}}}};
  }

  template <typename ENTRIES, typename SUM, typename MEAN>
  json JsonCounterDeserialize::_sigma(const DimBuffBase* p, const std::string& typ)   {
    const auto* entries = add_ptr<ENTRIES>(p, p->dataoff);
    const auto* mean    = add_ptr<MEAN>   (p, p->dataoff+sizeof(ENTRIES));
    const auto* sum     = add_ptr<SUM>    (p, p->dataoff+sizeof(ENTRIES)+sizeof(MEAN));
    const auto* sum2    = add_ptr<SUM>    (p, p->dataoff+sizeof(ENTRIES)+sizeof(MEAN)+sizeof(SUM));
    return {{"type", typ}, {"data", {{"nEntries", *entries}, {"sum", *sum }, {"sum2", *sum2 }, {"mean", *mean}}}};
  }

  template <typename ENTRIES, typename SUM, typename MEAN>  
  json JsonCounterDeserialize::_stat(const DimBuffBase* p, const std::string& typ)   {
    const auto* entries = add_ptr<ENTRIES>(p, p->dataoff);
    const auto* mean    = add_ptr<MEAN>   (p, p->dataoff+sizeof(ENTRIES));
    const auto* sum     = add_ptr<SUM>    (p, p->dataoff+sizeof(ENTRIES)+sizeof(MEAN));
    const auto* sum2    = add_ptr<SUM>    (p, p->dataoff+sizeof(ENTRIES)+sizeof(MEAN)+sizeof(SUM));
    // Missing: min, max, std-dev
    return {{"type", typ}, {"data", {{"nEntries", *entries}, {"sum", *sum }, {"sum2", *sum2 }, {"mean", *mean}}}};
  }

  template <typename TRUE_ENT, typename FALSE_ENT>  
  json JsonCounterDeserialize::_binomial(const DimBuffBase* p, const std::string& typ)   {
    const auto* et = add_ptr<TRUE_ENT>(p, p->dataoff);
    const auto* ef = add_ptr<FALSE_ENT>(p, p->dataoff+sizeof(TRUE_ENT));
    return {{"type", typ}, {"data", {{"nTrueEntries", *et}, {"nFalseEntries", *ef }}}};
  }

  template <typename TYPE> 
  json JsonCounterDeserialize::_numbers(const DimBuffBase* p, const std::string& typ)   {
    int    bsiz = p->reclen - p->dataoff;
    size_t nel  = bsiz / sizeof(TYPE);
    const auto* data_ptr = add_ptr<TYPE>(p, p->dataoff);
    return {{"type", typ}, {"data", std::vector<TYPE>(data_ptr, data_ptr + nel)}};
  }

  json JsonCounterDeserialize::de_serialize(const void* ptr)   {
    const DimBuffBase *p = (DimBuffBase*)ptr;
    std::string nam = add_ptr<char>(p, p->nameoff);
    switch (p->type)  {
    case C_INT:
      return { {"name", nam}, {"type", p->type}, {"value", _scalar<int>(p, "INT") } };
    case C_UINT:
      return { {"name", nam}, {"type", p->type}, {"value", _scalar<unsigned int>(p, "UINT") } };
    case C_ULONG:
      return { {"name", nam}, {"type", p->type}, {"value", _scalar<unsigned long>(p, "ULONG") } };
    case C_LONGLONG:
    case C_ATOMICINT:
    case C_ATOMICLONG:
    case C_GAUDIACCLONG:
    case C_GAUDIACCINT:
      return { {"name", nam}, {"type", p->type}, {"value", _scalar<long>(p, "LONG") } };
    case C_GAUDIACCuINT:
      return { {"name", nam}, {"type", p->type}, {"value", _scalar<unsigned long>(p, "ULONG") } };
    case C_GAUDIACCuLONG:
      return { {"name", nam}, {"type", p->type}, {"value", _scalar<unsigned long>(p, "ULONG") } };
    case C_FLOAT:
      return { {"name", nam}, {"type", p->type}, {"value", _scalar<float>(p, "FLOAT") } };
    case C_RATEFLOAT:
      return { {"name", nam}, {"type", p->type}, {"value", _scalar<float>(p, "RATE_FLOAT") } };
    case C_DOUBLE:
      return { {"name", nam}, {"type", p->type}, {"value", _scalar<double>(p, "DOUBLE") } };
    case C_RATEDOUBLE:
      return { {"name", nam}, {"type", p->type}, {"value", _scalar<double>(p, "RATE_DOUBLE") } };
    case C_INTSTAR:
      return { {"name", nam}, {"type", p->type}, {"value", _numbers<int>(p, "INT*") } };
    case C_UINTSTAR:
      return { {"name", nam}, {"type", p->type}, {"value", _numbers<unsigned int>(p, "UINT*") } };
    case C_LONGSTAR:
      return { {"name", nam}, {"type", p->type}, {"value", _numbers<long>(p, "LONG*") } };
    case C_ULONGSTAR:
      return { {"name", nam}, {"type", p->type}, {"value", _numbers<unsigned long>(p, "ULONG*") } };
    case C_FLOATSTAR:
      return { {"name", nam}, {"type", p->type}, {"value", _numbers<float>(p, "FLOAT*") } };
    case C_DOUBLESTAR:
      return { {"name", nam}, {"type", p->type}, {"value", _numbers<double>(p, "DOUBLE*") } };
    case C_RATEDOUBLESTAR:
      return { {"name", nam}, {"type", p->type}, {"value", _numbers<double>(p, "RATE_DOUBLE*") } };
    case C_LONGPAIR:
      return { {"name", nam}, {"type", p->type}, {"value", _pair<long,long>(p, "LONGPAIR") } };
    case C_INTPAIR:
      return { {"name", nam}, {"type", p->type}, {"value", _pair<long,long>(p, "INTPAIR") } };

    case C_GAUDIAVGACCc: //first element: NEntries; second element: Sum
      return { {"name", nam}, {"type", p->type}, {"value", _average<long,signed char>(p, "GAUDIAVGACCc") } };
    case C_GAUDIAVGACCcu://first element: NEntries; second element: Sum
      return { {"name", nam}, {"type", p->type}, {"value", _average<long,unsigned char>(p, "GAUDIAVGACCcu") } };
    case C_GAUDIAVGACCs: //first element: NEntries; second element: Sum
      return { {"name", nam}, {"type", p->type}, {"value", _average<long,short>(p, "GAUDIAVGACCs") } };
    case C_GAUDIAVGACCsu://first element: NEntries; second element: Sum
      return { {"name", nam}, {"type", p->type}, {"value", _average<long,unsigned short>(p, "GAUDIAVGACCsu") } };
    case C_GAUDIAVGACCi: //first element: NEntries; second element: Sum
      return { {"name", nam}, {"type", p->type}, {"value", _average<long,int>(p, "GAUDIAVGACCi") } };
    case C_GAUDIAVGACCiu://first element: NEntries; second element: Sum
      return { {"name", nam}, {"type", p->type}, {"value", _average<long,unsigned int>(p, "GAUDIAVGACCiu") } };
    case C_GAUDIAVGACCl: //first element: NEntries; second element: Sum
      return { {"name", nam}, {"type", p->type}, {"value", _average<long,long>(p, "GAUDIAVGACCl") } };
    case C_GAUDIAVGACClu://first element: NEntries; second element: Sum
      return { {"name", nam}, {"type", p->type}, {"value", _average<long,unsigned long>(p, "GAUDIAVGACClu") } };
    case C_GAUDIAVGACCf: //first element: NEntries; second element: Sum
      return { {"name", nam}, {"type", p->type}, {"value", _average<long,float>(p, "GAUDIAVGACCf") } };
    case C_GAUDIAVGACCd: //first element: NEntries; second element: Sum
      return { {"name", nam}, {"type", p->type}, {"value", _average<long,double>(p, "GAUDIAVGACCd") } };

    case C_GAUDISIGMAACCc: //first element: NEntries; second element: Sum
      return { {"name", nam}, {"type", p->type}, {"value", _sigma<long,signed char>(p, "GAUDISIGMAACCc") } };
    case C_GAUDISIGMAACCcu://first element: NEntries; second element: Sum
      return { {"name", nam}, {"type", p->type}, {"value", _sigma<long,unsigned char>(p, "GAUDISIGMAACCcu") } };
    case C_GAUDISIGMAACCs: //first element: NEntries; second element: Sum
      return { {"name", nam}, {"type", p->type}, {"value", _sigma<long,short>(p, "GAUDISIGMAACCs") } };
    case C_GAUDISIGMAACCsu://first element: NEntries; second element: Sum
      return { {"name", nam}, {"type", p->type}, {"value", _sigma<long,unsigned short>(p, "GAUDISIGMAACCsu") } };
    case C_GAUDISIGMAACCi: //first element: NEntries; second element: Sum
      return { {"name", nam}, {"type", p->type}, {"value", _sigma<long,int>(p, "GAUDISIGMAACCi") } };
    case C_GAUDISIGMAACCiu://first element: NEntries; second element: Sum
      return { {"name", nam}, {"type", p->type}, {"value", _sigma<long,unsigned int>(p, "GAUDISIGMAACCiu") } };
    case C_GAUDISIGMAACCl: //first element: NEntries; second element: Sum
      return { {"name", nam}, {"type", p->type}, {"value", _sigma<long,long>(p, "GAUDISIGMAACCl") } };
    case C_GAUDISIGMAACClu://first element: NEntries; second element: Sum
      return { {"name", nam}, {"type", p->type}, {"value", _sigma<long,unsigned long>(p, "GAUDISIGMAACClu") } };
    case C_GAUDISIGMAACCf: //first element: NEntries; second element: Sum
      return { {"name", nam}, {"type", p->type}, {"value", _sigma<long,float>(p, "GAUDISIGMAACCf") } };
    case C_GAUDISIGMAACCd: //first element: NEntries; second element: Sum
      return { {"name", nam}, {"type", p->type}, {"value", _sigma<long,double>(p, "GAUDISIGMAACCd") } };

    case C_GAUDISTATACCc: //first element: NEntries; second element: Sum
      return { {"name", nam}, {"type", p->type}, {"value", _stat<long,signed char>(p, "GAUDISTATACCc") } };
    case C_GAUDISTATACCcu://first element: NEntries; second element: Sum
      return { {"name", nam}, {"type", p->type}, {"value", _stat<long,unsigned char>(p, "GAUDISTATACCcu") } };
    case C_GAUDISTATACCs: //first element: NEntries; second element: Sum
      return { {"name", nam}, {"type", p->type}, {"value", _stat<long,short>(p, "GAUDISTATACCs") } };
    case C_GAUDISTATACCsu://first element: NEntries; second element: Sum
      return { {"name", nam}, {"type", p->type}, {"value", _stat<long,unsigned short>(p, "GAUDISTATACCsu") } };
    case C_GAUDISTATACCi: //first element: NEntries; second element: Sum
      return { {"name", nam}, {"type", p->type}, {"value", _stat<long,int>(p, "GAUDISTATACCi") } };
    case C_GAUDISTATACCiu://first element: NEntries; second element: Sum
      return { {"name", nam}, {"type", p->type}, {"value", _stat<long,unsigned int>(p, "GAUDISTATACCiu") } };
    case C_GAUDISTATACCl: //first element: NEntries; second element: Sum
      return { {"name", nam}, {"type", p->type}, {"value", _stat<long,long>(p, "GAUDISTATACCl") } };
    case C_GAUDISTATACClu://first element: NEntries; second element: Sum
      return { {"name", nam}, {"type", p->type}, {"value", _stat<long,unsigned long>(p, "GAUDISTATACClu") } };
    case C_GAUDISTATACCf: //first element: NEntries; second element: Sum
      return { {"name", nam}, {"type", p->type}, {"value", _stat<long,float>(p, "GAUDISTATACCf") } };
    case C_GAUDISTATACCd: //first element: NEntries; second element: Sum
      return { {"name", nam}, {"type", p->type}, {"value", _stat<long,double>(p, "GAUDISTATACCd") } };

    case C_GAUDIBINACCc: //first element: NEntries; second element: Sum
      return { {"name", nam}, {"type", p->type}, {"value", _binomial<signed char,signed char>(p, "GAUDIBINACCc") } };
    case C_GAUDIBINACCcu://first element: NEntries; second element: Sum
      return { {"name", nam}, {"type", p->type}, {"value", _binomial<unsigned char,unsigned char>(p, "GAUDIBINACCcu") } };
    case C_GAUDIBINACCs: //first element: NEntries; second element: Sum
      return { {"name", nam}, {"type", p->type}, {"value", _binomial<short,short>(p, "GAUDIBINACCs") } };
    case C_GAUDIBINACCsu://first element: NEntries; second element: Sum
      return { {"name", nam}, {"type", p->type}, {"value", _binomial<unsigned short,unsigned short>(p, "GAUDIBINACCsu") } };
    case C_GAUDIBINACCi: //first element: NEntries; second element: Sum
      return { {"name", nam}, {"type", p->type}, {"value", _binomial<int,int>(p, "GAUDIBINACCi") } };
    case C_GAUDIBINACCiu://first element: NEntries; second element: Sum
      return { {"name", nam}, {"type", p->type}, {"value", _binomial<unsigned int,unsigned int>(p, "GAUDIBINACCiu") } };
    case C_GAUDIBINACCl: //first element: NEntries; second element: Sum
      return { {"name", nam}, {"type", p->type}, {"value", _binomial<long,long>(p, "GAUDIBINACCl") } };
    case C_GAUDIBINACClu://first element: NEntries; second element: Sum
      return { {"name", nam}, {"type", p->type}, {"value", _binomial<unsigned long,unsigned long>(p, "GAUDIBINACClu") } };
    case C_GAUDIBINACCf: //first element: NEntries; second element: Sum
      return { {"name", nam}, {"type", p->type}, {"value", _binomial<float,float>(p, "GAUDIBINACCf") } };
    case C_GAUDIBINACCd: //first element: NEntries; second element: Sum
      return { {"name", nam}, {"type", p->type}, {"value", _binomial<double,double>(p, "GAUDIBINACCd") } };

    default:
      break;
    }
    return { {"name", nam}, {"type", p->type}, {"value", {"type", int(H_ILLEGAL) } } };
  }
}
