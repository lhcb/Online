//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================

// Framework include files
#include <Gaucho/CounterTask.h>
#include <Gaucho/MonCounter.h>

// C/C++ include files
#include <numeric>

Online::CounterTask::CounterTask(const std::string& task, const std::string& dns, int tmo)
  : TaskRPC("Counter", task, dns, tmo) 
{
}

int Online::CounterTask::counters(const std::vector<std::string>& names, std::vector<CntrDescr*>& cntrs)  {
  if ( m_RPC )   {
    int cmdlen = std::accumulate(names.begin(), names.end(), names.size()+sizeof(RPCCommRead), sum_string_length);
    RPCCommRead *cmd = RPCComm::make_command<RPCCommRead>(cmdlen, RPCCRead);
    cmd->copy_names(cmd->which, names);
    m_RPC->setData(cmd, cmdlen);
    int status  = m_RPC->analyseReply();
    if ( status == 0 )  {
      for (const auto& k : m_RPC->hists )   {
        CntrDescr *o = (CntrDescr*)CounterSerDes::de_serialize(k.second);
        cntrs.push_back(o);
      }
    }
    return status;
  }
  return 1;
}

int Online::CounterTask::counters(const std::vector<std::string>& names, std::map<std::string,CntrDescr*>& cntrs)  {
  int status = this->counters(names);
  if ( status == 0 )  {
    for ( const auto& k : m_RPC->hists )    {
      CntrDescr *o = (CntrDescr*)CounterSerDes::de_serialize(k.second);
      cntrs.emplace(k.first,o);
    }
  }
  return status;
}

std::pair<int, std::set<std::string> > Online::CounterTask::taskList(const std::string& dns)  {
  std::pair<int, std::set<std::string> > result;
  result.first = TaskRPC::_taskList("Counter", dns, result.second);
  return result;
}
