//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================

// Framework include files
#include <Gaucho/CounterTaskJson.h>
#include <Gaucho/CounterJson.h>

Online::CounterTaskJson::CounterTaskJson(const std::string& task, const std::string& dns, int tmo)
  : TaskRPC("Counter", task, dns, tmo) 
{
}

Online::CounterTaskJson::json Online::CounterTaskJson::counter_directory()    {
  std::vector<std::string> items;
  int ret = this->directory(items);
  if( 0 == ret )   {
    return items;
  }
  std::stringstream str;
  str << "Invalid RPC call to CounterTaskJson::counter_directory()<br>"
      << " RPC code: " << ret;
  throw std::runtime_error(str.str());
}

Online::CounterTaskJson::json Online::CounterTaskJson::taskList(const std::string& dns)    {
  std::vector<std::string> items;
  if( 0 == CounterTaskJson::taskList(dns, items) )   {
    return items;
  }
  throw std::runtime_error("Invalid RPC call to CounterTaskJson::taskList("+dns+")");
}

int Online::CounterTaskJson::counters(const std::vector<std::string>& names, std::vector<json>& cntrs)  {
  if( m_RPC )   {
    int cmdlen = std::accumulate(names.begin(), names.end(), names.size()+sizeof(RPCCommRead), sum_string_length);
    RPCCommRead *cmd = RPCComm::make_command<RPCCommRead>(cmdlen, RPCCRead);
    cmd->copy_names(cmd->which, names);
    m_RPC->setData(cmd, cmdlen);
    int status  = m_RPC->analyseReply();
    if( status == 0 )  {
      for( const auto& k : m_RPC->hists )   {
	json o = JsonCounterDeserialize::de_serialize(k.second);
	cntrs.push_back(o);
      }
    }
    return status;
  }
  return 1;
}

int Online::CounterTaskJson::counters(const std::vector<std::string>& names, std::map<std::string,json>& cntrs)  {
  int status = this->items(names);
  if( status == 0 )  {
    for( const auto& k : m_RPC->hists )    {
      json o = JsonCounterDeserialize::de_serialize(k.second);
      cntrs.emplace(k.first,o);
    }
  }
  return status;
}

Online::CounterTaskJson::json Online::CounterTaskJson::counters(const std::string& selection)    {
  int status = this->items(selection);
  if( status == 0 )  {
    std::vector<json> cntrs;
    for( const auto& k : m_RPC->hists )    {
      json o = JsonCounterDeserialize::de_serialize(k.second);
      cntrs.emplace_back(std::move(o));
    }
    json result = { {"task", this->taskName()}, {"counters", cntrs} };
    return result;
  }
  return {};
}

Online::CounterTaskJson::json Online::CounterTaskJson::counters()    {
  return this->counters(".(.*)");
}

