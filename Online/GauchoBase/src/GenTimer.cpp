//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================

// Framework include files
#include <Gaucho/GenTimer.h>
#include <Gaucho/Utilities.h>

// C/C++ include files
#include <cstdio>
#include <cerrno>
#include <ctime>
#ifdef WIN32
#include "windows.h"
#define onesec        10000000UL
#define onemilli_nano 1000000UL
#else   // WIN32
#include <unistd.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/resource.h>
#define onesec_mu     1000000UL
#define onesec_mili   1000000UL

#include <syscall.h>
namespace {
  inline pid_t lib_rtl_gettid()  {  return ((pid_t)syscall(SYS_gettid)); }
}

#endif   // !WIN32

#define onesec_nano   1000000000UL
#include <dim/dis.hxx>

Online::GenTimer::GenTimer(int periodicity, int typ)  {
  this->period       = periodicity;
  this->type         = typ;
  this->periodic     = (typ & TIMER_TYPE_PERIODIC) != 0;
  this->synched      = (typ & TIMER_MODIFYER_SYNCHRONIZED) != 0;
}

Online::GenTimer::~GenTimer()  {
  this->stop();
}

void Online::GenTimer::start()  {
  if ( m_thread == 0 )  {
    this->forceExit = false;
    this->m_stopSeen = false;
    this->m_thread = std::make_unique<std::thread>([this]() {  this->ThreadRoutine(); this->pid = 0; } );
    this->m_threadBalance++;
  }
  else  {
    if ( !this->periodic )  {
      this->stop();
      this->forceExit = false;
      this->m_thread = std::make_unique<std::thread>([this]() {  this->ThreadRoutine(); this->pid = 0; } );
      this->m_threadBalance++;
    }
  }
  if ( !m_timeoutSvcName.empty() )  {
    m_timeoutSvc = std::make_unique<DimService>(m_timeoutSvcName.c_str(), "L", &m_dueTime, sizeof(m_dueTime));
  }
}

void Online::GenTimer::startPeriodic(int periodicity)  {
  this->period = periodicity*1000;
  this->start();
}

void Online::GenTimer::stop()  {
  if (!this->m_dontdimlock)
    dim_lock();
  if (m_thread != 0)   {
    this->m_stopSeen = false;
    this->forceExit = true;
    int ncnt=0;
    while  (!m_stopSeen  && ncnt < 5)  {
      this->forceExit = true;
      ::usleep(10000);
      ncnt++;
    }
    this->m_thread->detach();
    this->m_thread.reset();
    this->m_threadBalance--;
  }
  if (!this->m_dontdimlock)
    dim_unlock();
//  m_lock.unlockMutex();
  m_timeoutSvc.reset();
}

/// Publish task save timeouts to dim
void Online::GenTimer::publishTimeouts(const std::string& service_name)  {
  m_timeoutSvcName = service_name;
}

void Online::GenTimer::makeDeltaT()  {
//
// do all calculations in nanoseconds
// and also return nanoseconds
// Handle the precision outside...
//
#ifdef WIN32
  SYSTEMTIME tim;
  unsigned long timestamp,ntim;
  GetLocalTime(&tim);
  SystemTimeToFileTime(&tim, (LPFILETIME)&timstamp);
  timestamp *= 10;
  ntim  = (timstamp/(m_period*onesec_nano))*(m_period*onesec_nano)+(this->m_period*onesec_nano);
  this->m_dueTime = ntim;
  unsigned long dtim = (ntim-timstamp);
  return dtim;
#else
  unsigned long timstamp;
  struct timeval tv;
  struct timezone *tz;
  tz = 0;
  gettimeofday(&tv, tz);
  timstamp  = tv.tv_sec * onesec_nano + tv.tv_usec*1000UL;
  this->m_lastdelta = timstamp-this->m_lastfire;
  if (this->m_extlastdelta != 0)
  {
    *this->m_extlastdelta = this->m_lastdelta;
  }
  return;
#endif
}

unsigned long Online::GenTimer::getDeltaTime(int incr)
{
//
// do all calculations in nanoseconds
// and also return nanoseconds
// Handle the precision outside...
//
#ifdef WIN32
  SYSTEMTIME tim;
  unsigned long timstamp,ntim;
  GetLocalTime(&tim);
  SystemTimeToFileTime(&tim, (LPFILETIME)&timstamp);
  timestamp *= 100;
  ntim  = (timstamp/(m_period*onesec_nano))*(m_period*onesec_nano)+(m_period*onesec_nano);
  m_dueTime = ntim;
  unsigned long dtim = (ntim-timstamp);
  return dtim;
#else
  unsigned long timstamp,newtim;
  struct timeval tv;
  struct timezone *tz = nullptr;
  ::gettimeofday(&tv, tz);
  timstamp  = tv.tv_sec;
  timstamp *= onesec_nano;
  timstamp += tv.tv_usec*1000;
  m_lastfire = timstamp;
  if ( this->synched )  {
    newtim  = (timstamp/(this->period*onesec_mili));
    newtim *= (onesec_mili*this->period);
    newtim += (((unsigned long)incr+this->period)*onesec_mili);
  }
  else  {
    newtim = timstamp+(((unsigned long)incr+this->period)*onesec_mili);
  }
  m_dueTime = newtim;
  unsigned long dtim = (newtim-timstamp);
  if (dtim < 20000000)  {
    dtim += this->period*onesec_mili;
    m_dueTime += this->period*onesec_mili;
  }
  return dtim;
#endif
}

int Online::GenTimer::NanoSleep(struct timespec* req, struct timespec* )  {
  unsigned long slicetime = onesec_nano/20;
  unsigned long tottime   = req->tv_sec*onesec_nano+req->tv_nsec;
  unsigned long nslice    = tottime/slicetime;
  unsigned long resttime  = tottime-nslice*slicetime;
  int status;
  struct timespec req1;

  for (unsigned long i = 0; i < nslice; i++ )    {
    req1.tv_sec = slicetime/onesec_nano;
    req1.tv_nsec = slicetime % onesec_nano;
    for( status = -1; status != 0; )    {
      status = nanosleep(&req1,&req1);
      if ( this->forceExit ) return -1;
    }
  }
  if ( resttime == 0 ) return 0;
  req1.tv_sec = resttime/onesec_nano;
  req1.tv_nsec = resttime % onesec_nano;
  for( status = -1; status != 0; )   {
    status = nanosleep(&req1,&req1);
    if ( this->forceExit ) return -1;
  }
  return status;
}

void *Online::GenTimer::ThreadRoutine()  {
  this->pid = ::lib_rtl_gettid();
  this->timeout = false;
  setpriority(PRIO_PROCESS, this->pid, 0);
  std::string threadName = "GenTimer_" + std::to_string(this->period);
  pthread_setname_np(pthread_self(), threadName.c_str());

  while ( !this->forceExit )  {
    unsigned long delta = getDeltaTime(0);
#ifdef WIN32
    Sleep(delta/onemilli_nano);
#else
    unsigned long s = delta/onesec_nano;
    struct timespec req;
    req.tv_sec = (time_t)s;
    req.tv_nsec = delta % onesec_nano;
    if ( m_timeoutSvc )  {
      m_timeoutSvc->updateService( m_dueTime );
    }
    for(int status = -1; status != 0; )  {
      status = NanoSleep(&req,&req);
      if (this->forceExit)  {
        this->m_stopSeen = true;
        return 0;
      }
      if (status == 0)  {
        this->timeout = true;
        this->makeDeltaT();
        this->timerHandler();
        break;
      }
      else if (errno == EINTR)  {
        ::lib_rtl_output(LIB_RTL_WARNING,"EINTR in nanosleep \n");
        continue;
      }
      break;
    }
#endif
    if (this->forceExit)  {
      this->m_stopSeen = true;
      return 0;
    }
    if ( !this->periodic )  {
      break;
    }
  }
  return 0;
}
