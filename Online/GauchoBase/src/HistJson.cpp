//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================

/// Framework include files
#include <Gaucho/MonTypes.h>
#include <Gaucho/HistJson.h>
#include <Gaucho/dimhist.h>

using json = nlohmann::json;

namespace Online   {
  
  namespace JsonHistDeserialize   {
    json _basic_hist(const DimHistbuff1* b);
    json _get_axis(const DimHistbuff1* b, const DimAxis& axis);
    json _get_stats(const DimHistbuff1 *b);
    json _get_doubles(const DimHistbuff1 *b, size_t offset=0);
    std::pair<std::string,std::string> _get_title(const DimHistbuff1 *b);
  }
  
  json JsonHistDeserialize::_get_doubles(const DimHistbuff1 *b, size_t offset)   {
    const double* values = add_ptr<double>(b, b->dataoff + offset);
    return json(std::vector<double>(values, values + b->num_cells()));
  }

  json JsonHistDeserialize::_get_stats(const DimHistbuff1 *b)   {
    json stats = {
      {"nentries", b->nentries},
      {"min",      b->min}, 
      {"max",      b->max}, 
      {"sumw",     b->sumw},
      {"sumw2",    b->sumw2},
      {"sumwx",    b->sumwx},
      {"sumwx2",   b->sumwx2},
      {"sumwy",    b->sumwy},
      {"sumwy2",   b->sumwy2},
      {"sumwz",    b->sumwz},
      {"sumwz2",   b->sumwz2},
      {"sumwt",    b->sumwt},
      {"sumwt2",   b->sumwt2},
      {"ymin",     b->yminval},
      {"ymax",     b->ymaxval}
    };
    return stats;
  }
  json JsonHistDeserialize::_get_axis(const DimHistbuff1* b, const DimAxis& axis)   {
    if ( axis.lablen > 0 )   {
      const char* lab = add_ptr<char>(b, axis.laboff);
      std::vector<std::string> labels;
      labels.reserve(axis.nbin);
      for ( int i=0; i < axis.nbin; ++i )   {
	labels.emplace_back(lab);
	lab = add_ptr<char>(lab, ::strlen(lab)+1);
      }
      return json({ {"nBins", axis.nbin}, {"minValue", axis.min}, {"maxValue", axis.max}, {"labels", labels} });
    }
    return json({ {"nBins", axis.nbin}, {"minValue", axis.min}, {"maxValue", axis.max} });
  }

  json JsonHistDeserialize::_basic_hist(const DimHistbuff1* bb)   {
    const DimHistbuff3* b = (const DimHistbuff3*)bb;
    std::string nam = add_ptr<char>(b, b->nameoff);
    std::string title = _get_title(b).first;
    json hist = {
      { "name",       nam     }, 
      { "title",      title   },
      { "type",       b->type },
      { "dimension",  b->dim  },
      { "stats",      _get_stats(b) },
      { "entries",    _get_doubles(b) } };
    switch(b->dim)   {
    case 1:
      hist["axis"] = { {"x", _get_axis(b, b->x)} };
      break;
    case 2:
      hist["axis"] = { {"x", _get_axis(b, b->x)}, {"y", _get_axis(b, b->y)}};
      break;
    case 3:
      hist["axis"] = { {"x", _get_axis(b, b->x)}, {"y", _get_axis(b, b->y)}, {"z", _get_axis(b, b->z)} };
      break;
    }
    return hist;
  }
  std::pair<std::string,std::string> JsonHistDeserialize::_get_title(const DimHistbuff1 *b)   {
    std::string optsep = "/:/:/:";
    std::string bopt;
    std::string option = add_ptr<char>(b, b->titoff);
    size_t seppos = option.find(optsep);
    if ((seppos != std::string::npos) && (seppos < 100000))      {
      bopt = option.substr(seppos + optsep.length());
      option.erase(seppos);
    }
    return std::make_pair(option, bopt);
  }

  json JsonHistDeserialize::de_serialize(const void* ptr)   {
    const DimHistbuff1 *b = (const DimHistbuff1*)ptr;
    switch (b->type)  {
    case H_1DIM:
    case H_GAUDIH1F:
    case H_GAUDIH1D:    {
      json hist         = _basic_hist(b);
      hist["sumw2"]     = _get_doubles(b, b->num_cells()*sizeof(double));
      return hist;
    }

    case H_2DIM:
    case H_GAUDIH2F:
    case H_GAUDIH2D:    {
      const DimHistbuff2 *b2 = (const DimHistbuff2*) ptr;
      json hist         = _basic_hist(b2);
      hist["sumw2"]     = _get_doubles(b2, b2->num_cells()*sizeof(double));
      return hist;
    }

    case H_3DIM:
    case H_GAUDIH3F:
    case H_GAUDIH3D:    {
      const DimHistbuff3 *b3 = (const DimHistbuff3*) ptr;
      json hist         = _basic_hist(b3);
      hist["sumw2"]     = _get_doubles(b3, b3->num_cells()*sizeof(double));
      return hist;
    }

    case H_GAUDIPR1:
    case H_PROFILE:
    case H_RATE:   {
      json hist = _basic_hist(b);
      hist["sumw"]      = _get_doubles(b,   b->num_cells()*sizeof(double));
      hist["sumw2"]     = _get_doubles(b, 2*b->num_cells()*sizeof(double));
      hist["binsumw2"]  = _get_doubles(b, 3*b->num_cells()*sizeof(double));
      return hist;
    }

    case H_GAUDIPR2:
    case H_2DPROFILE:   {
      const DimHistbuff2 *b2 = (const DimHistbuff2*) ptr;
      json hist         = _basic_hist(b2);
      hist["sumw"]      = _get_doubles(b2,   b2->num_cells()*sizeof(double));
      hist["sumw2"]     = _get_doubles(b2, 2*b2->num_cells()*sizeof(double));
      hist["binsumw2"]  = _get_doubles(b2, 3*b2->num_cells()*sizeof(double));
      return hist;
    }

    case H_GAUDIPR3:    {
      const DimHistbuff3 *b3 = (DimHistbuff3*) ptr;
      json hist         = _basic_hist(b3);
      hist["sumw"]      = _get_doubles(b3,   b3->num_cells()*sizeof(double));
      hist["sumw2"]     = _get_doubles(b3, 2*b3->num_cells()*sizeof(double));
      hist["binsumw2"]  = _get_doubles(b3, 3*b3->num_cells()*sizeof(double));
      return hist;
    }

    case C_STATENT:
      return {};
    }
    return {};
  }
}


