//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#include <Gaucho/HistSaver.h>
#include <Gaucho/SerialHeader.h>
#include <Gaucho/HistSerDes.h>
#include <Gaucho/RootHists.h>
#include <Gaucho/MonTypes.h>
#include <Gaucho/MonLockable.h>
#include <Gaucho/dimhist.h>
#include <RTL/strdef.h>
#include <RTL/rtl.h>
#include <dim/dis.hxx>

#include <TROOT.h>
#include <TSystem.h>
#include <TThread.h>

#include <cstdio>
#include <filesystem>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/types.h>

using namespace Online;

HistSaver::HistSaver() : lockable()   {
  TH1D::SetDefaultSumw2();
  TH2D::SetDefaultSumw2();
  TProfile::SetDefaultSumw2();
  ROOT::GetROOT();
  if (!TThread::IsInitialized())  {
    TThread::Initialize();
  }
}

HistSaver::HistSaver(std::shared_ptr<MonLockable> lck) : lockable(std::move(lck))   {
  TH1D::SetDefaultSumw2();
  TH2D::SetDefaultSumw2();
  TProfile::SetDefaultSumw2();
  ROOT::GetROOT();
  if (!TThread::IsInitialized())  {
    TThread::Initialize();
  }
}

HistSaver::~HistSaver()   {
}

size_t HistSaver::savetoFile(const void *buff)    {
  const SerialHeader *hd = (SerialHeader*)buff;
  if ( !hd )  {
    ::lib_rtl_output(LIB_RTL_WARNING, "No buffer available. Not saving histograms for this cycle.");
    return 0;
  }
  else if ( hd->m_magic != SERIAL_MAGIC )  {
    ::lib_rtl_output(LIB_RTL_WARNING, "%s Bad Magic Word in buffer while saving to file\n",
	     RTL::processName().c_str());
    return 0;
  }
  if ( hd->run_number == 0 )  {
    return 0;
  }

  size_t saved = 0;
  const void *bend = add_ptr(buff, hd->buffersize);

  buff = ((SerialHeader*)buff)->endPtr();
  Bool_t dirstat = kFALSE;
  locked_execution(this->lockable, [&dirstat]() {
      dirstat = TH1::AddDirectoryStatus();
      TH1::AddDirectory(kFALSE);
    });
  DimBuffBase *prevb = 0;
  while ( buff < bend )  {
    DimBuffBase *b = (DimBuffBase*)buff;
    if ( b->reclen <= 0 )   {
      ::lib_rtl_output(LIB_RTL_WARNING, "Bad Record. Record Length <= 0!!! Previous record %p! \n",(void*)prevb);
      break;
    }
    switch (b->type)   {
      case H_RATE:
      case H_1DIM:
      case H_2DIM:
      case H_3DIM:
      case H_GAUDIH1F:
      case H_GAUDIH1D:
      case H_GAUDIH2F:
      case H_GAUDIH2D:
      case H_GAUDIH3F:
      case H_GAUDIH3D:
      case H_GAUDIPR1:
      case H_GAUDIPR2:
      case H_GAUDIPR3:
      case H_PROFILE:
      case H_2DPROFILE:     {
	std::unique_ptr<TNamed> r;
	std::string last;
	locked_execution(this->lockable, [&r, &last, buff]() {
	    r.reset((TNamed*)HistSerDes::de_serialize(buff));
	    auto hname = RTL::str_split(r->GetName(),"/");
	    gDirectory->Cd("/");
	    for ( size_t i=0; i < hname.size()-1; i++ )    {
	      std::string sub_dir = hname.at(i);
	      // Need to 'normalize' sub-directory names. Replace 'not-allowed' characters.
	      for(std::size_t j=0; j<sub_dir.size(); ++j)  {
		char c = sub_dir[j];
		if ( !(::isalnum(c) || '_' == c || '-' == c) ) sub_dir[j] = '.';
	      }
	      TDirectory *where = gDirectory->GetDirectory(sub_dir.c_str(), kFALSE, "save");
	      if ( !where )  {
		gDirectory->mkdir(sub_dir.c_str());
		where = gDirectory->GetDirectory(sub_dir.c_str(), kFALSE, "save");
	      }
	      if ( where )  {
		where->cd();
	      }
	    }
	    r->SetName(hname.at(hname.size() - 1).c_str());
	    last = hname.at(hname.size() - 1);
	  });
        locked_execution(this->lockable, [&r,last]() {  r->Write(last.c_str()); });
	++this->numSavedHisto;
	++saved;
  	locked_execution(this->lockable, [&r]() {  r.reset(); });
        prevb = b;
        buff = add_ptr(buff,b->reclen);
        continue;
      }

      default:     {
        buff = add_ptr(buff,b->reclen);
        continue;
      }
    }
  }
  locked_execution(this->lockable, [&dirstat]() { TH1::AddDirectory(dirstat); });
  ++this->numSaveCycle;
  return saved;
}

void HistSaver::makeDirs(int runno)   {
  locked_execution(this->lockable, [this,runno]() { this->i_makeDirs(runno); });
}

void HistSaver::i_makeDirs(int runno)   {
  char timestr[64], year[5], month[3], day[3];
  time_t      rawTime  = ::time(NULL);
  struct tm*  timeInfo = ::localtime(&rawTime);
  struct stat buff;

  ::strftime(timestr, sizeof(timestr), "%Y%m%dT%H%M%S", timeInfo);
  ::strftime(year,    sizeof(year),    "%Y", timeInfo);
  ::strftime(month,   sizeof(month),   "%m", timeInfo);
  ::strftime(day,     sizeof(day),     "%d", timeInfo);
  std::string dir = this->rootdir + "/" + year + "/";
  if ( !this->partition.empty() ) dir += this->partition + "/";
  dir += this->taskname + "/" + month + "/" + day;

  if ( 0 != ::stat(dir.c_str(), &buff) )   {
    if ( gSystem->mkdir(dir.c_str(), kTRUE) ) {
      ::lib_rtl_output(LIB_RTL_ERROR,
		       "Failed to create histogram directory: %s [%s]",
		       dir.c_str(), gSystem->GetError());
    }
  }
  // Add runnumber to construct saveset name
  std::stringstream str;
  str << dir << "/" << this->taskname << "-" << runno << "-"
      << timestr << (this->EOR ? "-EOR.root" : ".root");
  this->currFileName = str.str();
}

std::unique_ptr<TFile> HistSaver::openFile()  {
  std::unique_ptr<TFile> file;
  file.reset(TFile::Open(this->currFileName.c_str(), "RECREATE"));
  if ( !file )   {
    ::lib_rtl_output(LIB_RTL_WARNING, "Root File %s cannot be opened\n", this->currFileName.c_str());
  }
  else if ( file->IsZombie())  {
    ::lib_rtl_output(LIB_RTL_WARNING, "Root File %s is Zombie\n", this->currFileName.c_str());
    file.reset();
  }
  return file;
}

void HistSaver::closeFile(std::unique_ptr<TFile>&& file)   {
  if ( file )   {
    auto* f = file.release();
    this->mustUpdateSaveSet = true;
    locked_execution(this->lockable, [f]() {
	if ( f ) f->Close();
	delete f;
      });
  }
}

void HistSaver::updateSaveSet()   {
  if ( this->savesetSvc && this->mustUpdateSaveSet )    {
    this->savesetSvc->updateService((char*)this->currFileName.c_str());
  }
  this->mustUpdateSaveSet = false;
}
