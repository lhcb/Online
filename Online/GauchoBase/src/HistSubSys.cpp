//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#include <Gaucho/MonitorClass.h>
#include <Gaucho/HistSubSys.h>
#include <Gaucho/MonTimer.h>
#include <Gaucho/MonSys.h>
#include <RTL/rtl.h>

#include <climits>

using namespace Online;

HistSubSys::HistSubSys(SubSysParams &p) : MonSubSys(p)  {
  this->dontclear    = false; // p.dontclear;
  this->updatePeriod = p.updatePeriod;
}

void HistSubSys::i_addEntry(std::unique_ptr<MonBase>&& h)   {
  auto cls = this->classMgr.getClass(h->name);
  cls->add(std::move(h));
  return;
}

size_t HistSubSys::removeAll(const std::string& owner_name)  {
  size_t ret = this->MonSubSys::removeAll(owner_name);
  return ret;
}

size_t HistSubSys::remove(const std::string &nam)   {
  auto cls = this->classMgr.getClass(nam);
  std::unique_ptr<MonBase> e(cls->remove(nam));
  if ( e )   {
    return 1;
  }
  return 0;
}

void HistSubSys::setup(const std::string& n, bool )   {
  this->classMgr.setup(this->name=n, false, "", RTL::processName());
}
