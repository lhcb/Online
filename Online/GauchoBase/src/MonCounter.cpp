//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================

// Framework include files
#include <Gaucho/MonCounter.h>

void Online::MonCounterBase::delete_OutputService()  {
  this->dim_service.reset();
}

void Online::MonCounterBase::update_OutputService()  {
  if ( this->dim_service ) this->dim_service->updateService();
}
