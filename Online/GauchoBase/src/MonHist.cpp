//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================

// Framework include files
#include <Gaucho/MonHist.h>
#include <Gaucho/RootHists.h>
#include <Gaucho/HistSerDes.h>
#include <RTL/strdef.h>
#include <cxxabi.h>

std::string Online::HistSerDes::optsep = "/:/:/:";

namespace Online  {
  template <typename TO> TO* cast_histo(const AIDA::IBaseHistogram* aidahist);
}

void Online::setup_labels(DimHistbuff1* dim_histo, const std::vector<BinNAxis>& axes)   {
  if ( dim_histo->dim >= 1 && axes.size() >= 1 )   {
    DimHistbuff1 *dim_histo1 = dim_histo;
    axes[0].put(dim_histo1->x);
    dim_histo1->x.laboff = dim_histo1->titoff + dim_histo1->titlen;
    HistSerDes::cpyBinLabels(add_ptr<char>(dim_histo1, dim_histo1->x.laboff), axes[0]);
  }
  if ( dim_histo->dim >= 2 && axes.size() >= 2 )   {
    DimHistbuff2 *dim_histo2 = (DimHistbuff2*)dim_histo;
    axes[1].put(dim_histo2->y);
    dim_histo2->y.laboff = dim_histo2->x.laboff+dim_histo2->x.lablen;
    HistSerDes::cpyBinLabels(add_ptr<char>(dim_histo2, dim_histo2->y.laboff), axes[1]);
  }
  if ( dim_histo->dim >= 3 && axes.size() >= 3 )   {
    DimHistbuff3 *dim_histo3 = (DimHistbuff3*)dim_histo;
    axes[2].put(dim_histo3->z);
    dim_histo3->z.laboff = dim_histo3->y.laboff+dim_histo3->y.lablen;
    HistSerDes::cpyBinLabels(add_ptr<char>(dim_histo3, dim_histo3->z.laboff), axes[2]);
  }
}

void Online::StatEntityShadow::Reset()  {
  nEntries            =   0;
  accumulatedFlag     =   0;
  minimalFlag         =   std::numeric_limits<double>::max();
  maximalFlag         =  -std::numeric_limits<double>::max();
  accumulatedFlag2    =   0;
  nEntriesBeforeReset =  -1;
}

void Online::BinNAxis::put(DimAxis& dim_axis)  const   {
  dim_axis.nbin   = this->nbin;
  dim_axis.min    = this->min;
  dim_axis.max    = this->max;
  dim_axis.lablen = this->lablen;
}

void Online::BinNAxis::fetch(TAxis* ax)   {
  this->axis = ax;
  this->nbin = ax->GetNbins();
  this->min  = ax->GetXmin();
  this->max  = ax->GetXmax();
  if (this->lablen == -1)      {
    this->lablen = HistSerDes::GetBinLabels(ax, this->labels);
  }
}

MONTYPE Online::HDescriptor::type()  const  {
  switch ( this->clazz ) 	{
  case HistClass::HT_Gaudi:
    switch ( this->dimension )     {
    case 1:  return
	(  this->profile )                     ? MONTYPE::H_GAUDIPR1
	: (this->bintyp == BinType::BT_Double) ? MONTYPE::H_GAUDIH1D
	: (this->bintyp == BinType::BT_Float)  ? MONTYPE::H_GAUDIH1F
	: MONTYPE::H_ILLEGAL;
    case 2:	 return
	(  this->profile )                     ? MONTYPE::H_GAUDIPR2
	: (this->bintyp == BinType::BT_Double) ? MONTYPE::H_GAUDIH2D
	: (this->bintyp == BinType::BT_Float)  ? MONTYPE::H_GAUDIH2F
	: MONTYPE::H_ILLEGAL;
    case 3:	 return
	(  this->profile )                     ? MONTYPE::H_GAUDIPR3
	: (this->bintyp == BinType::BT_Double) ? MONTYPE::H_GAUDIH3D
	: (this->bintyp == BinType::BT_Float)  ? MONTYPE::H_GAUDIH3F
	: MONTYPE::H_ILLEGAL;
    default:
      return MONTYPE::H_ILLEGAL;
    }
  case HistClass::HT_Root:
    switch (this->dimension)    {
    case 1:  return
	(  this->profile )                     ? MONTYPE::H_PROFILE
	: (this->bintyp == BinType::BT_Double) ? MONTYPE::H_1DIM
	: MONTYPE::H_ILLEGAL;
    case 2:  return
	(  this->profile )                     ? MONTYPE::H_2DPROFILE
	: (this->bintyp == BinType::BT_Double) ? MONTYPE::H_2DIM
	: MONTYPE::H_ILLEGAL;
    case 3:  return 
	(  this->profile )                     ? MONTYPE::H_ILLEGAL
	: (this->bintyp == BinType::BT_Double) ? MONTYPE::H_3DIM
	: MONTYPE::H_ILLEGAL;
    default:
      return MONTYPE::H_ILLEGAL;
    }
  case HistClass::HT_Aida:
  default:
    return MONTYPE::H_ILLEGAL;
  }
  return MONTYPE::H_ILLEGAL;
}

std::pair<Online::HistClass, bool>
Online::HDescriptor::histogram_type(const std::string& type_name)     {
  bool is_profile = type_name.find("Profile") != type_name.npos;
  if      ( type_name.find("Gaudi::Accumulators::") != type_name.npos)
    return std::make_pair(HT_Gaudi, is_profile);
  else if ( type_name.find("TH")         != type_name.npos)
    return std::make_pair(HT_Root, is_profile);
  else if ( type_name.find("TProfile")   != type_name.npos)
    return std::make_pair(HT_Root, is_profile);
  else if ( type_name.find("IHistogram") != type_name.npos)
    return std::make_pair(HT_Aida, is_profile);
  else if ( type_name.find("IProfile")   != type_name.npos)
    return std::make_pair(HT_Aida, is_profile);
  return std::make_pair(HT_Unknown, is_profile);
}

Online::BinType Online::HDescriptor::bin_type(const std::string& bin_typ)     {
  if ( bin_typ == "D" || bin_typ == "d" )
    return BT_Double;
  else if ( bin_typ == "F" || bin_typ == "f" )
    return BT_Float;
  else if (bin_typ.find("double") != std::string::npos)
    return BT_Double;
  else if (bin_typ.find("float") != std::string::npos)
    return BT_Float;
  return BT_Unknown;
}

void Online::HDescriptor::set_dimension(const std::string& type_name, size_t lpos)   {
  std::string dchar = type_name.substr(lpos,1);
  this->dimension = (dchar.empty()) ? 1 : std::stoi(dchar);                 // empty: TProfile
  std::string bin_typ = (dchar.empty() ? "D" : type_name.substr(++lpos,1)); // empty: TProfile
  this->bintyp = HDescriptor::bin_type(bin_typ);
}

void Online::HDescriptor::setup(const std::type_info& type_id)   {
  int    status = 4;
  size_t lpos = 0, rpos = 0;
  std::string type_name = abi::__cxa_demangle(type_id.name(), NULL, NULL, &status);
  auto [cl, prof] = HDescriptor::histogram_type(type_name);

  this->clazz   = cl;
  this->profile = prof;
  this->bintyp = BT_Unknown;
  switch ( this->clazz )   {
  case HT_Gaudi:   {
    lpos = type_name.find("<");
    rpos = type_name.find("u",lpos);
    std::string dchar = type_name.substr(lpos+1,(rpos-lpos)-1);
    this->dimension = (dchar == "") ? 1 : std::stoi(dchar);
    auto splt = RTL::str_split(type_name,",");
    this->bintyp = HDescriptor::bin_type(splt.at(2));
    break;
  }
  case HT_Root:  {
    lpos = (this->profile) ? std::strlen("TProfile") : 2;
    this->set_dimension(type_name, lpos);
    break;
  }
  case HT_Aida:
    lpos = (this->profile)
      ? type_name.find("IProfile")   + std::strlen("IProfile")
      : type_name.find("IHistogram") + std::strlen("IHistogram");
    this->set_dimension(type_name, lpos);
    break;
  default:
    break;
  }
}

template<typename T> void Online::MonHist<T>::reset()   {
  auto* r = const_cast<T*>(this->object);
  if ( r ) r->Reset("ICES");
}

template<> void Online::MonHist<Online::StatEntityShadow>::reset()   {
  auto* r = const_cast<StatEntityShadow*>(this->object);
  if ( r ) r->Reset();
}

template <typename T> int Online::MonHist<T>::setup()  {
  for (size_t i=0; i < this->axes.size();i++)     {
    this->axes[i].labels.clear();
    this->axes[i].lablen = -1;
  }
  this->axes.clear();
  switch(this->descriptor.clazz)  {
  case HT_Root:    {
    MyTH1D* rhist  = (MyTH1D*)this->object;
    rhist->SetName(this->name.c_str());
    this->name       = rhist->GetName();
    this->bookopts   = rhist->GetOption();
    this->title      = rhist->GetTitle() + HistSerDes::optsep + this->bookopts;
    this->headerLen  = DimHistbuff1::buffer_name_offset(this->descriptor.dimension) +
      this->title_length() + 1 + this->name_length() + 1;

    this->axes.resize(this->descriptor.dimension);
    if (this->descriptor.dimension > 0 )      {
      this->axes[0].fetch(rhist->GetXaxis());
      this->headerLen += this->axes[0].lablen;
    }
    if (this->descriptor.dimension > 1 )      {
      this->axes[1].fetch(rhist->GetYaxis());
      this->headerLen += this->axes[1].lablen;
    }
    if ( this->descriptor.dimension > 2 )   {
      this->axes[2].fetch(rhist->GetZaxis());
      this->headerLen += this->axes[2].lablen;
    }
    this->headerLen = (headerLen + 7) & (~7);
    this->m_xmitbuffersize = this->headerLen +
      ((this->descriptor.profile) ? 4 : 2)*rhist->GetNcells() * sizeof(double);
    break;
  }
  default:    {
    break;
  }
  }
  return this->m_xmitbuffersize;
}

template <typename T> int Online::MonHist<T>::serialize(void* ptr)    {
  DimHistbuff1 *b = (DimHistbuff1*) ptr;
  setup_buffer(b, *this);
  setup_labels(b,  this->axes);
  b->nentries  = double(this->object->GetEntries());
  if ( this->descriptor.clazz == HistClass::HT_Root)  {
    if ( this->descriptor.profile && this->descriptor.dimension == 1 )
      this->as<MyTProfile>()->movetodimbuffer(b);
    else if ( this->descriptor.profile && this->descriptor.dimension == 2 )
      this->as<MyTProfile2D>()->movetodimbuffer(b);
    else if ( this->descriptor.profile && this->descriptor.dimension == 3 )
      this->as<MyTProfile3D>()->movetodimbuffer(b);
    else if ( this->descriptor.dimension == 1 )
      this->as<MyTH1D>()->movetodimbuffer(b);
    else if ( this->descriptor.dimension == 2 )
      this->as<MyTH2D>()->movetodimbuffer(b);
    else if ( this->descriptor.dimension == 3 )
      this->as<MyTH3D>()->movetodimbuffer(b);
  }
  ptr = add_ptr(ptr, b->reclen);
  return b->reclen;
}

int Online::HistSerDes::GetBinLabels(TAxis *ax, std::vector<std::string> &labs)   {
  int l = 0, nbin = ax->GetNbins();
  for (int i = 1; i < (nbin + 1); ++i)  {
    const char *binLab = ax->GetBinLabel(i);
    l += strlen(binLab);
  }
  if (l > 0)  {
    for (int i = 1; i < nbin + 1; i++)
      labs.push_back(ax->GetBinLabel(i));

    l += nbin + 1;
  }
  return l;
}

void Online::HistSerDes::SetBinLabels(TAxis *ax, const char *labs)   {
  const char *lab = labs;
  for (int i = 1, nbin = ax->GetNbins(); i < (nbin + 1); ++i)  {
    ax->SetBinLabel(i, lab);
    lab = add_ptr<char>(lab, std::strlen(lab)+1);
  }
}

void Online::HistSerDes::cpyBinLabels(char *dst, const BinNAxis& axis)    {
  const auto& src = axis.labels;
  if ( src.size() > 0 )   {
    for (size_t i = 0; i < src.size(); i++)    {
      int leni = src[i].length();
      strncpy(dst, src[i].c_str(), leni);
      dst[leni] = 0;
      dst += leni + 1;
    }
  }
}

void *Online::HistSerDes::de_serialize(const void *ptr, const char *nam)  {
  Bool_t dirstat = TH1::AddDirectoryStatus();
  TH1::AddDirectory(kFALSE);
  DimBuffBase *p = (DimBuffBase*) ptr;
  std::string bopt;
  if (nam == 0)  {
    nam = add_ptr<char>(p, p->nameoff);
  }
  switch (p->type)  {
  case H_1DIM:
  case H_GAUDIH1F:
  case H_GAUDIH1D:    {
    const DimHistbuff1 *b = (const DimHistbuff1*) ptr;
    std::string option = add_ptr<char>(b, b->titoff);
    size_t seppos = option.find(HistSerDes::optsep);
    if ((seppos != std::string::npos) && (seppos < 100000))      {
      bopt = option.substr(seppos + HistSerDes::optsep.length());
      option.erase(seppos);
    }
    MyTH1D *h = new MyTH1D(nam, option.c_str(), b->x.nbin, b->x.min, b->x.max);
    h->SetEntries(b->nentries);
    h->movefromdimbuffer(add_ptr(b, b->dataoff));
    if (b->x.lablen > 0)
      HistSerDes::SetBinLabels(&h->fXaxis, add_ptr<char>(b, b->x.laboff));

    TH1::AddDirectory(dirstat);
    return h;
  }
  case H_2DIM:
  case H_GAUDIH2F:
  case H_GAUDIH2D:    {
    const DimHistbuff2 *b = (const DimHistbuff2*) ptr;
    std::string option = add_ptr<char>(b, b->titoff);
    size_t seppos = option.find(HistSerDes::optsep);
    if ((seppos != std::string::npos) && (seppos < 100000))     {
      bopt = option.substr(seppos + HistSerDes::optsep.length());
      option.erase(seppos);
    }
    MyTH2D *h =  new MyTH2D(nam, option.c_str(),
			    b->x.nbin, b->x.min, b->x.max,
			    b->y.nbin, b->y.min, b->y.max);
    h->SetEntries(b->nentries);
    h->movefromdimbuffer(add_ptr(b, b->dataoff));
    if (b->x.lablen > 0)
      HistSerDes::SetBinLabels(&h->fXaxis, add_ptr<char>(b, b->x.laboff));

    if (b->y.lablen > 0)
      HistSerDes::SetBinLabels(&h->fYaxis, add_ptr<char>(b, b->y.laboff));

    TH1::AddDirectory(dirstat);
    return h;
  }
  case H_3DIM:
  case H_GAUDIH3F:
  case H_GAUDIH3D:    {
    const DimHistbuff3 *b = (const DimHistbuff3*) ptr;
    std::string option = add_ptr<char>(b, b->titoff);
    size_t seppos = option.find(HistSerDes::optsep);
    if ((seppos != std::string::npos) && (seppos < 100000))      {
      bopt = option.substr(seppos + HistSerDes::optsep.length());
      option.erase(seppos);
    }
    MyTH3D *h = new MyTH3D(nam, option.c_str(),
			   b->x.nbin, b->x.min, b->x.max,
			   b->y.nbin, b->y.min, b->y.max,
			   b->z.nbin, b->z.min, b->z.max);
    h->SetEntries(b->nentries);
    h->movefromdimbuffer(add_ptr(b, b->dataoff));
    if ( b->x.lablen > 0 )
      HistSerDes::SetBinLabels(&h->fXaxis, add_ptr<char>(b, b->x.laboff));

    if ( b->y.lablen > 0 )
      HistSerDes::SetBinLabels(&h->fYaxis, add_ptr<char>(b, b->y.laboff));

    if ( b->z.lablen > 0 )
      HistSerDes::SetBinLabels(&h->fZaxis, add_ptr<char>(b, b->z.laboff));

    TH1::AddDirectory(dirstat);
    return h;
  }
  case H_RATE:
  case H_PROFILE:
  case H_GAUDIPR1:    {
    const DimHistbuff1 *b = (const DimHistbuff1*) ptr;
    std::string option = add_ptr<char>(b, b->titoff);
    size_t seppos = option.find(HistSerDes::optsep);
    if ((seppos != std::string::npos) && (seppos < 100000))   {
      bopt = option.substr(seppos + HistSerDes::optsep.length());
      option.erase(seppos);
    }
    MyTProfile *h = new MyTProfile(nam, option.c_str(),b->x.nbin, b->x.min, b->x.max,bopt.c_str());
    if (b->type != H_GAUDIPR1)    {
      h->movefromdimbuffer(add_ptr(b, b->dataoff));
    }
    else     {
      h->Sumw2(false);
      size_t nbin = b->x.nbin+2;
      const GaudiProfileBin *sers = add_ptr<GaudiProfileBin>(b,b->dataoff);
      for ( size_t i = 0; i < nbin; i++ )     {
	h->GetEntryArr()[i] = double(sers[i].nent);
	h->GetSumwArr()[i]  = sers[i].sumw;
	h->GetSumw2Arr()[i] = sers[i].sumw2;
      }
    }
    h->SetEntries(b->nentries);
    h->fYmin = b->yminval;
    h->fYmax = b->ymaxval;
    if ( h->GetYmin() == h->GetYmax() )
      h->fYmax++;
    if (b->x.lablen > 0)
      HistSerDes::SetBinLabels(&h->fXaxis, add_ptr<char>(b, b->x.laboff));
    TH1::AddDirectory(dirstat);
    return h;
  }

  case H_2DPROFILE:
  case H_GAUDIPR2:   {
    DimHistbuff2 *b = (DimHistbuff2*) ptr;
    std::string option = add_ptr<char>(b, b->titoff);
    size_t seppos = option.find(HistSerDes::optsep);
    if ((seppos != std::string::npos) && (seppos < 100000))     {
      bopt = option.substr(seppos + HistSerDes::optsep.length());
      option.erase(seppos);
    }
    MyTProfile2D *h = (MyTProfile2D*) new TProfile2D(nam, option.c_str(),
						     b->x.nbin, b->x.min, b->x.max,
						     b->y.nbin, b->y.min, b->y.max,
						     bopt.c_str());
    if (b->type != H_GAUDIPR2)   {
      h->movefromdimbuffer(add_ptr(b, b->dataoff));
    }
    else     {
      size_t nbin = (b->x.nbin+2)*(b->y.nbin+2);
      for (size_t i=0;i<nbin;i++)      {
	const GaudiProfileBin *sers = add_ptr<GaudiProfileBin>(b,b->dataoff);
	h->GetEntryArr()[i] = double(sers[i].nent);
	h->GetSumwArr()[i]  = sers[i].sumw;
	h->GetSumw2Arr()[i] = sers[i].sumw2;
      }
    }
    h->SetEntries(b->nentries);
    if (b->x.lablen > 0)
      HistSerDes::SetBinLabels(&h->fXaxis, add_ptr<char>(b, b->x.laboff));

    if (b->y.lablen > 0)
      HistSerDes::SetBinLabels(&h->fYaxis, add_ptr<char>(b, b->y.laboff));

    TH1::AddDirectory(dirstat);
    return h;
  }

  case H_GAUDIPR3:    {
    DimHistbuff3 *b = (DimHistbuff3*) ptr;
    std::string option = add_ptr<char>(b, b->titoff);
    size_t seppos = option.find(HistSerDes::optsep);
    if ((seppos != std::string::npos) && (seppos < 100000))      {
      bopt = option.substr(seppos + HistSerDes::optsep.length());
      option.erase(seppos);
    }
    auto* sh = new TProfile3D(nam, option.c_str(),
			      b->x.nbin, b->x.min, b->x.max,
			      b->y.nbin, b->y.min, b->y.max, 
			      b->z.nbin, b->z.min, b->z.max, 
			      bopt.c_str());
    MyTProfile3D *h = (MyTProfile3D*)sh;
    if ( b->type != H_GAUDIPR3 )    {
      h->movefromdimbuffer(add_ptr(b, b->dataoff));
    }
    else   {
      size_t nbin = (b->x.nbin+2)*(b->y.nbin+2)*(b->z.nbin+2);
      for (size_t i=0; i<nbin; i++)     {
	const GaudiProfileBin *sers = add_ptr<GaudiProfileBin>(b,b->dataoff);
	h->GetEntryArr()[i] = double(sers[i].nent);
	h->GetSumwArr()[i]  = sers[i].sumw;
	h->GetSumw2Arr()[i] = sers[i].sumw2;
      }
    }
    h->SetEntries(b->nentries);
    if (b->x.lablen > 0)      {
      HistSerDes::SetBinLabels(&h->fXaxis, add_ptr<char>(b, b->x.laboff));
    }
    if (b->y.lablen > 0)      {
      HistSerDes::SetBinLabels(&h->fYaxis, add_ptr<char>(b, b->y.laboff));
    }
    if (b->z.lablen > 0)      {
      HistSerDes::SetBinLabels(&h->fZaxis, add_ptr<char>(b, b->z.laboff));
    }
    TH1::AddDirectory(dirstat);
    return h;
  }

  case C_STATENT:    {
    StatEntityShadow* e = new StatEntityShadow;
    DimStatBuff *b      = (DimStatBuff*) ptr;
    e->nEntries         = b->nentries;
    e->accumulatedFlag  = b->sumw;
    e->accumulatedFlag2 = b->sumw2;
    e->minimalFlag      = b->min;
    e->maximalFlag      = b->max;
    e->nEntriesBeforeReset = -1;
    TH1::AddDirectory(dirstat);
    return e;
  }
  }
  TH1::AddDirectory(dirstat);
  return 0;
}

namespace Online   {
  template class MonHist<TH1D>;
  template class MonHist<TH2D>;
  template class MonHist<TH3D>;
  template class MonHist<TProfile>;
  template class MonHist<TProfile2D>;
  template class MonHist<MonRateProfile>;
  template class MonHist<StatEntityShadow> ;
}
