//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================

// Framework include files
#include <Gaucho/MonInfo.h>
#include <CPP/mem_buff.h>

namespace  {
  static int  s_empty =-1;
  static bool s_shutdowninProgress = false;
}

Online::MonInfo::MonInfo(const std::string& target, MonInfoHandler* hdlr)
  : name(target), handler(hdlr)
{
  int null_period = 0;
  this->id = dic_info_service_stamped(target.c_str(), MONITORED, null_period, 0, 0,
				      handler_callback, (dim_long)this,
				      &s_empty, sizeof(int));
}

Online::MonInfo::MonInfo(const std::string& target, int period, MonInfoHandler* hdlr)
  : name(target), handler(hdlr)
{
  this->id = ::dic_info_service_stamped(target.c_str(), MONITORED, period, 0, 0,
				      handler_callback, (dim_long)this,
				      &s_empty, sizeof(int));
}

Online::MonInfo::~MonInfo()  {
  if ( this->id )  {
    ::dic_release_service(this->id);
    this->id = 0;
  }
}

void Online::MonInfo::handler_callback(void* tagp, void* bufp, int* len)   {
    int      size = *len;
    char     *valin = (char*)bufp;
    Online::MonInfo* info = *(Online::MonInfo**)tagp;
    if (size == sizeof(int) && *(int*)valin == s_empty )    {
      return;
    }
    else if ( s_shutdowninProgress )   {
      return;
    }
    else if ( info->handler != nullptr )   {
      (*info->handler)(valin, size, info);
    }
  }

void Online::MonInfo::setShutdownInProgress(bool value)   {
  s_shutdowninProgress = value;
}

#if 0
void Online::MonInfo::infoHandler()   {
  void *valin = getData();
  int   size  = getSize();
  if (size == sizeof(int) && *(int*)valin == s_empty )    {
    return;
  }
  else if ( s_shutdowninProgress )   {
    return;
  }
  else if ( this->handler != nullptr )   {
    mem_buff buffer(valin, size);
    (*this->handler)(buffer.begin(), size, this);
  }
}
#endif
