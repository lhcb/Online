//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================

// Framework include files
#include <Gaucho/MonLockable.h>

// C/C++ include files
#include <cstdio>

int Online::MonLockable::lock()    {
  if ( this->lockid )   {
    int status = 1;
    while (status != 0)  {
      status = lockid->lockMutex();
      if (status != 0)    {
	//      printf("Status from lock not success......\n");
      }
    }
    this->lockcount++;
    if (this->lockcount != 1)  {
      //    printf("!!!!!!!!!!!!!!!!!!!!!!!!!!!Lock Counter != 1 after locking %d %d %d\n",m_lockcnt,m_lockcount,m_unlockcount);
    }
    return status;
  }
  return 0;
}

int Online::MonLockable::unlock()   {
  if ( this->lockid )   {
    int status = 1;
    this->unlockcount++;
    while (status != 0)    {
      status = this->lockid->unlockMutex();
      if (status != 0)    {
	//      printf("Status from unlock not success......\n");
      }
    }
    return status;
  }
  return 0;
}

