//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================

// Framework include files
#include <Gaucho/MonRate.h>

Online::MonBase* Online::CREATERate(const std::string &name,const std::string &title, double &rate)  {
  return new MonCounter<double>(name, title, rate);
}

Online::MonRateBase::MonRateBase(const std::string& nam, const std::string& tit, MonBase *original)
  : MonBase(C_RATEDOUBLESTAR, nam, tit), origCounter(original)
{
}

void Online::MonRateBase::create_OutputService(const std::string& service)   {
  if ( this->rateCounter )
    this->rateCounter->create_OutputService(service);
}

void Online::MonRateBase::update_OutputService()   {
  if ( this->rateCounter )
    this->rateCounter->update_OutputService();
}
