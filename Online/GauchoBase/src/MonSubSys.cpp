//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================

// Framework include files
#include <Gaucho/MonSubSys.h>
#include <Gaucho/MonSys.h>
#include <Gaucho/Utilities.h>
#include <Gaucho/MonitorClass.h>

Online::MonSubSys::MonSubSys(SubSysParams &p) : classMgr(p)  {
  this->runNumber = 0;
  this->type      = p.type;
  this->lockid    = std::make_unique<BRTLLock>();
  this->classMgr.runAware = p.runAware;
}

Online::MonSubSys::~MonSubSys()    {
  locked_execution(this, [this]() { this->classMgr.clear(); });
#if 0
  locked_execution(this, [this]() {
      if ( this->type == MONSUBSYS_Counter)    {
	for (auto& i : this->objects )
	  i.second->delete_OutputService();
      }
      this->objects.clear();
    });
#endif
}

void Online::MonSubSys::start()    {
  if (!this->start_done)  {
    this->classMgr.start();
    start_done = true;
  }
}

void Online::MonSubSys::stop()  {
  if (this->start_done)  {
    this->classMgr.stop();
    this->start_done = false;
  }
}

void Online::MonSubSys::reset()  {
  this->classMgr.reset();
}

std::size_t Online::MonSubSys::removeAll(const std::string& owner_name)  {
  return this->classMgr.removeAll(owner_name);
}

void Online::MonSubSys::clear()  {
  this->classMgr.clear();
}

void Online::MonSubSys::eorUpdate(int runo)  {
  this->classMgr.eorUpdate(runo);
}

void Online::MonSubSys::update()   {
  this->classMgr.update();
}

void Online::MonSubSys::update(unsigned long ref)   {
  this->classMgr.update(ref);
}

void Online::MonSubSys::setRunNo(int runno)   {
  this->runNumber = runno;
  this->classMgr.setRunNo(runno);
}

void Online::MonSubSys::addClass(std::string &nam, int intv, std::list<std::string>&sels, bool updateOnStop)  {
  this->addClass(std::make_shared<MonitorClass>(nam, intv, sels, updateOnStop));
}

void Online::MonSubSys::addClass(std::shared_ptr<MonitorClass>&& c)  {
  this->classMgr.add(std::move(c));
}

