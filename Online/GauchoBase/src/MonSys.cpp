//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================

// Framework include files
#include "Gaucho/MonSys.h"
#include "Gaucho/MonSubSys.h"
#include <dim/dis.hxx>

// C/C++ include files
#include <algorithm>

Online::MonSys::MonSys()  {
  state = "Constructed";
}

void Online::MonSys::add(std::shared_ptr<MonSubSys> ss)   {
  for(auto& p : subSystems)  {
    if (p->type == ss->type)
      return;
  }
  subSystems.insert(subSystems.end(),std::move(ss));
}

void Online::MonSys::start()  {
  DimServer::setWriteTimeout(20);
  DimServer::autoStartOn();
  DimServer::start(name.c_str());
  for(auto& p : subSystems)
    p->start();
  state = "Started";
}

void Online::MonSys::stop()  {
  DimServer::autoStartOff();
  for(auto& p : subSystems)
    p->stop();
  state = "Stopped";
}

void Online::MonSys::remove(std::shared_ptr<MonSubSys> ss)  {
  auto it = std::find(subSystems.begin(), subSystems.end(), std::move(ss));
  if ( it != subSystems.end() )
    subSystems.erase(it);
}

void Online::MonSys::lock()  {
  for(auto& p : subSystems)
    p->lock();
}

void Online::MonSys::unlock()   {
  for(auto& p : subSystems)
    p->unlock();
}

Online::MonSys& Online::MonSys::instance()   {
  static MonSys s;
  return s;
}

Online::MonSys *Online::MonSys::setup(const std::string& n)  {
  name = n;
  state = "Set-up";
  return this;
}

void Online::MonSys::clear()   {
  for(auto& p : subSystems)
    p->clear();
}

void Online::MonSys::reset()   {
  for(auto& p : subSystems)
    p->reset();
}

void Online::MonSys::update()  {
  for(auto& p : subSystems)
    p->update();
}

void Online::MonSys::update(unsigned long ref)
{
  for(auto& p : subSystems)
    p->update(ref);
}

void Online::MonSys::eorUpdate(int runo)   {
  for(auto& p : subSystems)
    p->eorUpdate(runo);
}

void Online::MonSys::setRunNo(int runo)   {
  for(auto& p : subSystems)
    p->setRunNo(runo);
}
