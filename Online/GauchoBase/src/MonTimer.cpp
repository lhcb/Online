//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================

// Framework include files
#include <Gaucho/MonTimer.h>
#include <Gaucho/MonitorClass.h>
#include <Gaucho/ObjService.h>
#include <Gaucho/MonSubSys.h>
#include <dim/dim_common.h>

// C/C++ include files
#include <stdexcept>

Online::MonTimer::MonTimer(MonitorClass *HSys, int periodicity) : GenTimer(periodicity*1000)   {
  m_Hsys = HSys;
  m_dueTime = 0;
  m_dontdimlock = true;
}

void Online::MonTimer::timerHandler ( void )   {
  if ( this->forceExit ) return;

  if (!m_dontdimlock) dim_lock();  
  {
    bool need_return = false;

    m_Hsys->genSrv->setRunNo(m_Hsys->runNumber);
    m_Hsys->genSrv->setTime(m_dueTime);
    try    {
      m_Hsys->makeRates(m_lastdelta);
      m_Hsys->genSrv->serialize(int(this->period/1000));
    }
    catch(const std::exception& e)    {
      ::printf("MonTimer: Exception:%s\n",e.what());
      need_return = true;
    }
    catch(...)    {
      printf("MonTimer: Unknown Exception.\n");
      need_return = true;
    }
    if ( need_return )    {
      if ( !m_dontdimlock ) ::dim_unlock();
      return;
    }
  }
  m_Hsys->genSrv->update();
  if ( !m_dontdimlock ) ::dim_unlock();
}
