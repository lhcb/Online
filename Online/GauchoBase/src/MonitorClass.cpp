//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================

// Framework include files
#include <Gaucho/MonitorClass.h>
#include <Gaucho/ObjSerializer.h>
#include <Gaucho/ObjService.h>
#include <Gaucho/Utilities.h>
#include <Gaucho/MonSubSys.h>
#include <Gaucho/MonTimer.h>
#include <Gaucho/MonBase.h>
#include <Gaucho/RateMgr.h>
#include <RTL/strdef.h>

/// C/C++ include files
#include <limits>

static long s_empty = 0;
std::string Online::MonitorClass::DefaultClassName = "**DEFAULT**";


Online::MonitorClass::MonitorClass(const std::string &id, int intv, std::list<std::string> &sel, bool uos)   {
  this->ID = id;
  this->interval = intv;
  this->entityExpression = sel;
  this->expand           = false;
  this->type             = 0;
  this->runNumber        = 0;
  this->lockid           = std::make_unique<BRTLLock>();
  this->updateOnStop     = uos;
  std::regex_constants::match_flag_type matchFlags =
    std::regex_constants::match_flag_type(std::regex_constants::icase | std::regex_constants::ECMAScript
    //      | regex_constants::optimize | regex_constants::match_not_null
				     );
  for (const auto& s : sel )  {
    std::regex *r = new std::regex(s, (std::regex_constants::syntax_option_type)matchFlags);
    this->class_regex.push_back(r);
  }
}

Online::MonitorClass::~MonitorClass()    {
}

void Online::MonitorClass::configure(int typ)   {
  this->type = typ;
  if (this->type == MONSUBSYS_Counter)  {
    if ( !this->rateManager )   {
      this->rateManager = std::make_unique<RateMgr>(this);
    }
  }
}

void Online::MonitorClass::setup(const std::string& clazz_name, bool expnd, const std::string &infix)  {
  std::string nodename = RTL::nodeNameShort();
  std::string utgid    = RTL::processName();
  this->expandInfix    = infix;
  this->expand         = expnd;
  this->name           = clazz_name;
  this->timer          = std::make_unique<MonTimer>(this, this->interval);

  if ( this->expand )  {
    if (this->type == MONSUBSYS_Counter)   {
      for ( auto& h : this->entities )
        h.second->create_OutputService(expandInfix);
    }
  }
  if (runAware)
    this->serviceFMT = "MON_"+utgid+"/"+this->name+"/<runno>/Data";
  else
    this->serviceFMT = "MON_"+utgid+"/"+this->name+"/Data";

  std::string run = std::to_string(this->runNumber);
  std::string nam = RTL::str_replace(this->serviceFMT, "<runno>", run);

  if (this->ID != MonitorClass::DefaultClassName)
    nam += "."+this->ID;

  this->lockid->name = nam;
  if ( !this->genSrv )    {
    auto serial = std::make_unique<ObjSerializer>(this, this->expand);
    this->genSrv = std::make_unique<ObjService>(std::move(serial), nam, "C", &s_empty, 4);
  }
  if ( !this->eor_service )   {
    nam = "MON_" + utgid + "/" + this->name + "/EOR";
    if (this->ID != MonitorClass::DefaultClassName)
      nam = "MON_" + utgid + "/" + this->name + "/EOR." + this->ID;
    auto serial = std::make_unique<ObjSerializer>(this, false);
    this->eor_service = std::make_unique<ObjService>(std::move(serial), nam, "C", &s_empty, 4);
  }
  this->eor_service->setEORflag(true);
}

void Online::MonitorClass::start()   {
  if ( this->timer ) this->timer->start();
}

void Online::MonitorClass::stop()    {
  if ( this->timer )  {
    this->timer->stop();
  }
  if ( this->rateManager )    {
    this->genSrv->setRunNo(this->runNumber);
    this->rateManager->zero();
  }
  if ( this->updateOnStop ) {
    this->update();
  }
}

void Online::MonitorClass::reset()   {
  for(auto& i : this->entities )
    i.second->reset();
}

void Online::MonitorClass::add(std::unique_ptr<MonBase>&& m)   {
  if ( m )   {
    auto i = this->entities.find(m->name);
    if ( i != this->entities.end() )
      i->second->delete_OutputService();
    this->entities[m->name] = std::move(m);
  }
}

std::size_t Online::MonitorClass::clear()   {
  size_t ret = this->entities.size();
  for(auto& i : this->entities )
    i.second->delete_OutputService();
  this->entities.clear();
  return ret;
}

std::size_t Online::MonitorClass::clearAll(const std::string &owner_name)    {
  if ( owner_name.size() == 0 || owner_name == "*" )    {
    return this->clear();
  }
  std::size_t ret = 0;
  std::vector<std::string> items;
  items.reserve(this->entities.size());
  for ( auto& i : this->entities )   {
    if ( i.first.find(owner_name) != std::string::npos )
      items.push_back(i.first);
  }
  for ( auto& i : items )   {
    auto iter = this->entities.find(i);
    iter->second->delete_OutputService();
    this->entities.erase(iter);
    ++ret;
  }
  return ret;
}

std::unique_ptr<Online::MonBase>
Online::MonitorClass::remove(const std::string &nam)   {
  std::unique_ptr<MonBase> ret;
  auto i = this->entities.find(nam);
  if ( i != this->entities.end() )   {
    ret.reset(i->second.release());
    this->entities.erase(i);
  }
  return ret;
}

bool Online::MonitorClass::matchName(const std::string &nam)  {
//  regex_constants::match_flag_type matchFlags;
//  matchFlags = (regex_constants::match_flag_type) regex_constants::icase;
//      | (regex_constants::match_flag_type) regex_constants::optimize
//      | (regex_constants::match_flag_type) regex_constants::match_not_null;
  for (const auto* r : this->class_regex )   {
    std::smatch sm;
    bool stat = std::regex_match(nam, sm, *r);
    if ( stat )    {
      return true;
    }
  }
  return false;
}

void Online::MonitorClass::makeRates(unsigned long dt)   {
  if ( this->rateManager ) this->rateManager->makeRates(dt);
}

void Online::MonitorClass::setRunNo(unsigned int runno)  {
  if ( !runAware )  {
    this->runNumber = runno;
    return;
  }
  if ( runno != this->runNumber )   {
    if (this->genSrv == 0 )    {
      this->runNumber = runno;
      return;
    }
    std::string run = std::to_string(runno);
    std::string nam = RTL::str_replace(this->serviceFMT, "<runno>", run);
    if ( this->ID.size() != 0 )   {
      nam += "."+this->ID;
    }
    auto serial  = std::make_unique<ObjSerializer>(this, this->expand);
    this->genSrv = std::make_unique<ObjService>(std::move(serial), nam, "C", &s_empty, 4);
    this->genSrv->setRunNo(runno);
  }
  this->runNumber = runno;
}

void Online::MonitorClass::eorUpdate(int runno)   {
  this->eor_service->setRunNo(runno);
  this->setRunNo(runno);
  locked_execution(this, [this]() { this->eor_service->serialize(0); });
  this->eor_service->update();
}

void Online::MonitorClass::update()  {
  locked_execution(this, [this]() {
      this->genSrv->setRunNo(this->runNumber);
      this->genSrv->serialize(0);
    });
  this->genSrv->update();
}

void Online::MonitorClass::update(unsigned long ref)  {
  long r = ref;
  this->genSrv->setRunNo(this->runNumber);
  this->genSrv->setTime(r);
  locked_execution(this, [this]() { this->genSrv->serialize(0); });
  this->genSrv->update();
}
