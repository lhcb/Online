//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================

// Framework include files
#include <Gaucho/MonitorInterface.h>
#include <Gaucho/TaskSaveTimer.h>
#include <Gaucho/MonitorClass.h>
#include <Gaucho/ObjService.h>
#include <Gaucho/MonSys.h>
#include <RTL/strdef.h>

// C/C++ include files
#include <atomic>

Online::MonitorInterface::MonitorInterface(const std::string &utgid,
					   SubSysParams&& cntpars,
					   SubSysParams&& histpars)
  : m_CntrPars(cntpars), m_HistPars(histpars), m_UTGID(utgid)
{
  m_runAware      = m_CntrPars.runAware;
  m_CntrPars.type = MONSUBSYS_Counter;
  m_CntrSubSys    = std::make_shared<CounterSubSys>(m_CntrPars);
  MonSys::instance().add(m_CntrSubSys);

  histpars.type   = MONSUBSYS_Histogram;
  m_HistSubSys    = std::make_shared<HistSubSys>(histpars);
  MonSys::instance().add(m_HistSubSys);
}

Online::MonitorInterface::~MonitorInterface()   {
  MonSys::instance().remove(m_CntrSubSys);
  MonSys::instance().remove(m_HistSubSys);
}

void Online::MonitorInterface::i_unsupported(const std::string& /*name*/,
    const std::type_info& /*typ, const std::string &owner*/)
{
}

std::size_t Online::MonitorInterface::undeclare(const std::string& entry)   {
  std::size_t removed = 0;
  if ( m_HistSubSys )
    removed = m_HistSubSys->remove(entry);
  if ( !removed && m_CntrSubSys )
    removed = m_CntrSubSys->remove(entry);
  return removed;
}

std::size_t Online::MonitorInterface::undeclareCounter(const std::string& entry)   {
  std::size_t removed = 0;
  if ( m_CntrSubSys )
    removed = m_CntrSubSys->remove(entry);
  return removed;
}

std::size_t Online::MonitorInterface::undeclareAll(std::string &oname)  {
  std::size_t removed = 0;
  if ( m_HistSubSys )  {
    m_HistSubSys->stop();
    removed += m_HistSubSys->removeAll(oname);
  }
  if ( m_CntrSubSys )  {
    m_CntrSubSys->stop();
    removed += m_CntrSubSys->removeAll(oname);
  }
  return removed;
}

int Online::MonitorInterface::start()   {
  DimServer::autoStartOff();
  if ( m_started )    {
    MonSys::instance().start();
    DimServer::autoStartOn();
    //    DimServer::start();
    return 1;
  }
  this->declareCounter("MonitorInternals/LastEOR",    m_lastEOR, "");
  this->declareCounter("MonitorInternals/LastUpdate", m_lastUpdate, "");
  this->declareCounter("MonitorInternals/RunNumber",  m_runnr, "");
  MonSys::instance().setup(this->m_UTGID.c_str());
  if (m_CntrSubSys != 0) m_CntrSubSys->setup("Counter",this->m_CntrPars.expandnames);
  if (m_HistSubSys != 0) m_HistSubSys->setup("Histos");
  DimServer::autoStartOn();
  DimServer::start();
  MonSys::instance().start();
  m_started = true;
  return 1;
}

int Online::MonitorInterface::stop()   {
  this->undeclareCounter("MonitorInternals/LastEOR");
  this->undeclareCounter("MonitorInternals/LastUpdate");
  this->undeclareCounter("MonitorInternals/RunNumber");
  MonSys::instance().stop();
  return 1;
}

void Online::MonitorInterface::applyCounterClasses(std::vector<std::string> &options)  {
  if (options.size() > 0)  {
    auto clist = this->makeClassList(options);
    for ( auto& cl : clist )    {
      cl->configure(MONSUBSYS_Counter);
      cl->runAware = m_CntrPars.runAware;
      m_CntrSubSys->addClass(std::move(cl));
    }
  }
}

void Online::MonitorInterface::applyHistogramClasses(std::vector<std::string> &options)  {
  if (options.size() > 0)  {
    auto clist = this->makeClassList(options);
    for ( auto& cl : clist )    {
      cl->configure(MONSUBSYS_Histogram);
      cl->runAware = m_HistPars.runAware;
      m_HistSubSys->addClass(std::move(cl));
    }
  }
}

void Online::MonitorInterface::enableRates(bool enable)    {
  m_CntrSubSys->have_rates = enable;
}

std::list<std::shared_ptr<Online::MonitorClass> >
Online::MonitorInterface::makeClassList(std::vector<std::string> &option) const  {
  /*
   * Class Definition Option Syntax:
   * "<Class Name><blank><update interval><blank> list of regular expressions separated by <blanks>"
   */
  std::string name, def;
  std::list<std::shared_ptr<MonitorClass> > result;
  for (size_t i = 0; i < option.size(); i++)  {
    int intv = 0;
    std::list<std::string> l;
    auto op = RTL::str_split(option[i].c_str(), " ");
    if ( op.size() > 1 )   {
      name = op[0];
      ::sscanf(op[1].c_str(), "%d", &intv);
      for (size_t j = 2; j < op.size(); j++)
        l.insert(l.end(), op.at(j));
    }
    result.insert(result.end(), std::make_shared<MonitorClass>(name, intv, l, true));
    // NOTE the class definition option syntax does not support disabling update on save
  }
  return result;
}

void Online::MonitorInterface::setRunNo(int runno)  {
  m_runnr = runno;
  MonSys::instance().setRunNo(runno);
}

void Online::MonitorInterface::eorUpdate(int runno)   {
  m_lastEOR = ::time(0);
  MonSys::instance().eorUpdate(runno);
}

void Online::MonitorInterface::update(unsigned long ref)  {
  m_lastUpdate = ::time(0);
  MonSys::instance().update(ref);
}

void Online::MonitorInterface::resetHistos()  {
  MonSys::instance().reset();
}

void Online::MonitorInterface::lock(void)   {
  MonSys::instance().lock();
}

void Online::MonitorInterface::unlock(void)   {
  MonSys::instance().unlock();
}

void Online::MonitorInterface::startSaving(std::shared_ptr<TaskSaveTimer>& timer)   {
  timer->sub_system = m_HistSubSys;
  timer->lockable   = m_HistSubSys;
  if ( m_saveTimer.get() != timer.get() )   {
    m_saveTimer = timer;
  }
  m_saveTimer->start();
}

void Online::MonitorInterface::stopSaving()  {
  if ( m_saveTimer ) m_saveTimer->stop();
  m_saveTimer.reset();
}
