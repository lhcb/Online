//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================

// Framework include files
#include <Gaucho/ObjRPC.h>
#include <Gaucho/BRTL_Lock.h>
#include <Gaucho/Utilities.h>
#include <Gaucho/Compression.h>
#include <RTL/strdef.h>

Online::ObjRPC::ObjRPC(std::unique_ptr<Serializer>&& serial, 
                       const std::string&            name,
                       const char *f_in, const char *f_out, 
                       BRTLLock   *mlid, BRTLLock   *olid)
  : DimRpc(name.c_str(), f_in, f_out), dns(),
    serializer(std::move(serial)), maplock(mlid), objlock(olid)
{
}

Online::ObjRPC::ObjRPC(std::shared_ptr<DimServerDns> srv,
                       std::unique_ptr<Serializer>&& serial, 
                       const std::string&            name,
                       const char *f_in, const char *f_out, 
                       BRTLLock   *mlid, BRTLLock   *olid)
  : DimRpc(srv.get(), name.c_str(), f_in, f_out),
    dns(std::move(srv)), serializer(std::move(serial)),
    maplock(mlid), objlock(olid)
{
}

void Online::ObjRPC::rpcHandler()   {
  RPCCommCookie *comm  = (RPCCommCookie*)this->getData();
  std::pair<std::size_t, void*> result(0, nullptr);
  {
    BRTLLock::_Lock mlock(this->maplock);

    if ( comm->comm == RPCCRead )    {
      BRTLLock::_Lock lock(this->objlock);
      auto nams = RTL::str_split(add_ptr<char>(comm,sizeof(comm->comm)), '\n');
      result = serializer->serialize_obj(nams, sizeof(RPCReply));
    }
    else if ( comm->comm == RPCCClear )    {
      BRTLLock::_Lock lock(this->objlock);
      auto nams = RTL::str_split(add_ptr<char>(comm,sizeof(comm->comm)), '\n');
      result = serializer->serialize_obj(nams, sizeof(RPCReply), true);
    }
    else if ( comm->comm == RPCCClearAll )   {
      BRTLLock::_Lock lock(this->objlock);
      result = serializer->serialize_obj(sizeof(RPCReply), true);
    }
    else if ( comm->comm == RPCCReadAll )    {
      BRTLLock::_Lock lock(this->objlock);
      result = serializer->serialize_obj(sizeof(RPCReply));
    }
    else if ( comm->comm == RPCCReadRegex )    {
      BRTLLock::_Lock lock(this->objlock);
      std::string match = add_ptr<char>(comm,sizeof(comm->comm));
      match  = match.substr(0, match.find("\n"));
      result = serializer->serialize_match(match, sizeof(RPCReply));
    }
    else if ( comm->comm == RPCCDirectory )    {
      BRTLLock::_Lock lock(this->objlock);
      result = serializer->serialize_dir(sizeof(RPCReply));
    }
    /// RPC calls supporting client side cookies:
    else if ( comm->comm == RPCCReadCookie )    {
      BRTLLock::_Lock lock(this->objlock);
      auto nams = RTL::str_split(add_ptr<char>(comm,sizeof(RPCCommCookie)), '\n');
      result = serializer->serialize_obj(nams, sizeof(RPCReplyCookie));
    }
    else if ( comm->comm == RPCCClearCookie )    {
      BRTLLock::_Lock lock(this->objlock);
      auto nams = RTL::str_split(add_ptr<char>(comm,sizeof(RPCCommCookie)), '\n');
      result = serializer->serialize_obj(nams, sizeof(RPCReplyCookie), true);
    }
    else if ( comm->comm == RPCCClearAllCookie )   {
      BRTLLock::_Lock lock(this->objlock);
      result = serializer->serialize_obj(sizeof(RPCReplyCookie), true);
    }
    else if ( comm->comm == RPCCReadAllCookie )    {
      BRTLLock::_Lock lock(this->objlock);
      result = serializer->serialize_obj(sizeof(RPCReplyCookie));
    }
    else if ( comm->comm == RPCCReadRegexCookie )    {
      BRTLLock::_Lock lock(this->objlock);
      std::string match = add_ptr<char>(comm,sizeof(RPCCommCookie));
      match = match.substr(0, match.find("\n"));
      result = serializer->serialize_match(match, sizeof(RPCReplyCookie));
    }
    else if ( comm->comm == RPCCDirectoryCookie )    {
      BRTLLock::_Lock lock(this->objlock);
      result = serializer->serialize_dir(sizeof(RPCReplyCookie));
    }
    else   {
      RPCReply reply;
      reply.status = -2;
      reply.comm = RPCCIllegal;
      setData(&reply, sizeof(RPCReply));
      if (this->maplock !=0) this->maplock->unlockMutex();
      return;
    }
  }
  RPCReplyCookie *reply = (RPCReplyCookie*)result.second;
  reply->status = 0;
  reply->comm = comm->comm;
  switch( comm->comm )   {
  case RPCCReadCookie:
  case RPCCClearCookie:
  case RPCCClearAllCookie:
  case RPCCReadAllCookie:
  case RPCCReadRegexCookie:
  case RPCCDirectoryCookie:  {
    RPCFlags flags(comm->flags);
    if ( flags.values.compress && result.first > 1024*1024 )   {
      std::size_t off = sizeof(RPCReplyCookie);
      uint8_t*    hdr = add_ptr<uint8_t>(reply, 0);
      uint8_t*    ptr = add_ptr<uint8_t>(reply, off);
      std::size_t len = result.first - off;
      // Prepare the compressed data buffer
      this->compress.clear();
      this->compress.reserve(len);
      std::copy(hdr, hdr+len, std::back_inserter(this->compress));
      // Compress data load and swap replies buffer
      result = gaucho::compress(ptr, len, this->compress);
      reply  = (RPCReplyCookie*)result.second;
    }
    else   {
      flags.values.compress = 0;
    }
    reply->cookie = comm->cookie;
    reply->flags  = flags.data;
    reply->id     = comm->id;
  } break;
  default:
    break;
  }
#if 0
  ::printf("%s [xmit buffer] monitoring: size=%ld @ %p\n",
           this->getName(), result.first, result.second);
#endif
  this->setData(result.second, result.first);
}
