//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================

// Framework include files
#include <Gaucho/ObjSerializer.h>
#include <Gaucho/MonitorClass.h>
#include <Gaucho/MonClassMgr.h>
#include <Gaucho/MonBase.h>
#include <Gaucho/dimhist.h>
#include <dim/dis.hxx>
#include <RTL/rtl.h>

// C/C++ include files
#include <stdexcept>
#include <climits>
#include <regex>

constexpr int DIM_BUFFER_SIZE_LIMIT = (INT_MAX - 1024);
  
namespace {
  void check_serial_buffers(...)   {}
#if 0
  void check_serial_buffers(void* ptr, std::size_t total_size, std::size_t len, const MonBase* h)   {
    void* buffer_end = add_ptr(ptr, len);
    void* buffer_tot = add_ptr(ptr, total_size);
    if ( buffer_end > buffer_tot )   {
      throw std::runtime_error("!!!!!!!!!!!!!!! BUFFER OVERFLOW serializing MonObject "+h->name);
    }
  }
#endif
}

Online::ObjSerializer::ObjSerializer(MonClassMgr* m, bool expnd) : manager(m), expand(expnd)  {
}

Online::ObjSerializer::ObjSerializer(MonitorClass* cl, bool expnd) : monitorClass(cl), expand(expnd)  {
}

std::vector<Online::MonBase*> Online::ObjSerializer::collect()   const    {
  std::vector<MonBase*> result;
  result.reserve(1024);
  if ( this->monitorClass )    {
    for ( const auto& j : this->monitorClass->entities )
      result.push_back(j.second.get());
  }
  if ( this->manager )   {
    for( const auto& i : this->manager->getMap() )   {
      for ( const auto& j : i.second->entities )
        result.push_back(j.second.get());
    }
  }
  return result;
}

Online::MonBase* Online::ObjSerializer::find(const std::string& name)  const  {
  if ( monitorClass )    {
    for ( auto& j : monitorClass->entities )
      if ( j.second->name == name ) return j.second.get();
  }
  if ( manager )   {
    for( auto& i : this->manager->getMap() )   {
      for ( auto& j : i.second->entities )
	if ( j.second->name == name ) return j.second.get();
    }
  }
  return nullptr;
}

/// Serialize a set of objects identified by name to local buffer
std::pair<std::size_t,void*>
Online::ObjSerializer::serialize_match(const std::string &match, std::size_t offset, bool clear)  {
  std::size_t total_size = offset;
  std::vector<MonBase*> items;
  int flags = std::regex_constants::icase | std::regex_constants::ECMAScript;
  std::regex r(match, (std::regex_constants::syntax_option_type)flags);

  auto objects = this->collect();
  items.reserve(objects.size());
  for ( auto* i : objects )   {
    if ( i )   {
      std::smatch sm;
      bool stat = std::regex_match(i->name, sm, r);
      if ( stat )    {
        total_size += i->xmitbuffersize();
        items.emplace_back(i);
      }
    }
  }
  if ( total_size == offset )   {
    // No match found. Error ?
  }
  return i_serialize_obj(this->buffer, std::move(items), offset, clear);
}

std::pair<std::size_t,void*>
Online::ObjSerializer::serialize_obj(mem_buff& buff, std::size_t offset, bool clear)  const  {
  // this overload is only called used from TaskSaveTimer, so we will not impose a limit on the buffer size
  auto objects = this->collect();
  return i_serialize_obj(buff, std::move(objects), offset, clear, false);
}

std::pair<std::size_t,void*>
Online::ObjSerializer::serialize_obj(std::size_t offset, bool clear)   {
  auto objects = this->collect();
  return i_serialize_obj(this->buffer, std::move(objects), offset, clear);
}

std::pair<std::size_t,void*>
Online::ObjSerializer::serialize_obj(const std::vector<std::string>& nams,
				     std::size_t offset,
				     bool clear)
{
  std::vector<MonBase*> items;
  items.reserve(nams.size());
  for (const auto& name : nams )  {
    auto* h = this->find(name);
    items.emplace_back(h);
  }
  return i_serialize_obj(this->buffer, std::move(items), offset, clear);
}

std::pair<std::size_t,void*>
Online::ObjSerializer::i_serialize_obj(mem_buff& buff,
				       std::vector<MonBase*>&& items,
				       std::size_t offset,
				       bool clear, bool dim_limit) const
{
  std::size_t total_size = offset;
  for ( auto& h : items )  {
    if ( h == nullptr )  {
      total_size += sizeof(DimBuffBase);
      continue;
    }
    if ( dim_limit && ( total_size + h->xmitbuffersize() > DIM_BUFFER_SIZE_LIMIT ) ) {
      ::lib_rtl_output(LIB_RTL_WARNING,"[Exceeded xmit buffer] Drop histogram: %s (size=%d) total_size=%ld limit=%d",
		       h->name.c_str(), h->xmitbuffersize(), total_size, DIM_BUFFER_SIZE_LIMIT);
      h = nullptr;
      total_size += sizeof(DimBuffBase);
      continue;
    }
    total_size += h->xmitbuffersize();
    ::lib_rtl_output(LIB_RTL_DEBUG,"[xmit buffer] histogram: %s (size=%d) total_size=%ld limit=%d",
		     h->name.c_str(), h->xmitbuffersize(), total_size, DIM_BUFFER_SIZE_LIMIT);
  }
  void*  ptr        = buff.allocate(total_size);
  void*  buffer_ptr = add_ptr(ptr,offset);
  std::size_t len = 0;
  for ( const auto& h : items )   {
    if ( h != nullptr )    {
      std::size_t siz = h->serialize(buffer_ptr);
      check_serial_buffers(ptr, total_size, len += siz, h);
      buffer_ptr = add_ptr(buffer_ptr, siz);
      if (clear) h->reset();
    }
    else    {
      DimBuffBase b;
      b.type = (unsigned int)H_ILLEGAL;
      ::memcpy (buffer_ptr, &b, sizeof(b));
      buffer_ptr = add_ptr(buffer_ptr, sizeof(b));
      len += sizeof(DimBuffBase);
    }
  }
  buff.set_cursor(total_size);
  return std::make_pair(total_size,ptr);
}

std::pair<std::size_t,void*>
Online::ObjSerializer::serialize_dir(std::size_t offset)   {
  std::size_t total_size = offset;
  auto objects = this->collect();
  for ( auto* h : objects )
    total_size += h->name_length() + sizeof(int);

  void*      ptr = this->buffer.allocate(total_size);
  DimDirEnt* ent = add_ptr<DimDirEnt>(ptr, offset);
  for ( auto* h : objects )  {
    ent->type = h->type;
    std::strncpy(ent->name,h->name.c_str(),h->name.length()+1);
    ent = add_ptr<DimDirEnt>(ent,sizeof(ent->type)+h->name.length()+1);
  }
  this->buffer.set_cursor(total_size);
  return std::make_pair(total_size,ptr);
}

void Online::ObjSerializer::updateExpansions()   {
  if ( this->expand )    {
    if ( monitorClass )    {
      for ( auto& j : monitorClass->entities )
        j.second->update_OutputService();
    }
    if ( manager )   {
      for( auto& i : this->manager->getMap() )   {
	for ( auto& j : i.second->entities )
	  j.second->update_OutputService();
      }
    }
  }
}

std::pair<std::size_t,const void*> Online::ObjSerializer::data()   const     {
  return std::make_pair(this->buffer.used(), this->buffer.begin());
}

