//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================

/// Framework include files
#include <Gaucho/ObjService.h>
#include <Gaucho/MonBase.h>
#include <Gaucho/MonSys.h>
#include <RTL/strdef.h>
#include <RTL/rtl.h>

/// C/C++ include files
#include <vector>

Online::ObjService::ObjService (std::shared_ptr<DimServerDns>& dns,
				std::unique_ptr<Serializer>&& serial,
				const std::string& name,
				const char*        format,
				void*              buffer,
				std::size_t        size)
  : DimService(dns.get(), name.c_str(), format, buffer, size),
    serializer(std::move(serial))
{
  add_updater(dns.get());
}

Online::ObjService::ObjService (std::unique_ptr<Serializer>&& serial,
				const std::string& name,
				const char*        format,
				void*              buffer,
				std::size_t        size)
  : DimService(name.c_str(), format, buffer, size),
    serializer(std::move(serial))
{
  add_updater(nullptr);
}

Online::ObjService::ObjService (std::shared_ptr<DimServerDns>& dns,
				std::unique_ptr<Serializer>&& serial,
				const std::string& name,
				const char*        format,
				void*              buffer,
				std::size_t        size,
				std::shared_ptr<mem_buff> extbuff)
  : DimService(dns.get(), name.c_str(), format, buffer, size),
    serializer(std::move(serial)), extBuffer(extbuff)
{
  add_updater(dns.get());
}

Online::ObjService::ObjService (std::unique_ptr<Serializer>&& serial,
				const std::string& name,
				const char*        format,
				void*              buffer,
				std::size_t        size,
				std::shared_ptr<mem_buff> extbuff)
  : DimService(name.c_str(), format, buffer, size),
    serializer(std::move(serial)), extBuffer(extbuff)
{
  add_updater(nullptr);
}

Online::ObjService::~ObjService()   {
#if 0
  if ( this->getName() != 0 && ::strlen(this->getName())>0 )  {
    ::lib_rtl_output(level, "Delete dim output service for publication: %s", this->getName());
  }
#endif
}

void Online::ObjService::add_updater(DimServerDns* dns)    {
  std::string nn = RTL::processName() + "/" + RTL::str_replace(this->getName(),"MON_","");
  nn = RTL::str_replace(nn,"/Histos/","/Debug/Update/Hist/");
  nn = RTL::str_replace(nn,"/Counter/","/Debug/Update/Cntr/");
  if ( dns ) {}
  this->num_update_svc.reset(new DimService(nn.c_str(),"I", &this->num_updates, sizeof(this->num_updates)));
  /*
    if ( dns )
    this->num_update_svc.reset(new DimService(dns, nn.c_str(),"I", &this->num_updates, sizeof(this->num_updates)));
    else
    this->num_update_svc.reset(new DimService(nn.c_str(),"I", &this->num_updates, sizeof(this->num_updates)));
  */
}

const void* Online::ObjService::serialized_buffer()   const   {
  return this->extBuffer ? this->m_serptr : this->serializer->data().second;
}

size_t Online::ObjService::serialized_size()   const   {
  return this->extBuffer ? this->m_sersiz : this->serializer->data().first;
}

void Online::ObjService::serialize(int updtIntv)   {
  if ( this->extBuffer )  {
    this->m_serptr = this->extBuffer->begin();
    this->m_sersiz = this->extBuffer->used();
    return;
  }
  auto [len, ptr] = this->serializer->serialize_obj(m_hdr.SizeOf());
  this->m_serptr = ptr;
  this->m_sersiz = len;
  this->m_hdr.m_magic = SERIAL_MAGIC;
  this->m_hdr.buffersize = len - m_hdr.SizeOf();
  this->m_hdr.updateInterval = updtIntv;
  ::memcpy(this->m_serptr, &m_hdr, m_hdr.SizeOf());
}

void Online::ObjService::serialize(mem_buff& buff, int updtIntv)   const  {
  if ( this->extBuffer )  {
    buff.copy(this->extBuffer->begin(), this->extBuffer->used());
    return;
  }
  SerialHeader hdr = this->m_hdr;
  auto [len, ptr]  = this->serializer->serialize_obj(buff, hdr.SizeOf());
  hdr.m_magic      = SERIAL_MAGIC;
  hdr.buffersize   = len - hdr.SizeOf();
  hdr.updateInterval = updtIntv;
  ::memcpy(buff.begin(), &hdr, hdr.SizeOf());
}

void Online::ObjService::update()  {
  ++this->num_updates;
  ::lib_rtl_output(LIB_RTL_DEBUG,"%s [xmit buffer] monitoring: update # %d size=%ld @ %p",
		   this->getName(), this->num_updates, this->m_sersiz, (void*)this->m_serptr);
  this->updateService(this->m_serptr,this->m_sersiz);
  this->serializer->updateExpansions();
  this->num_update_svc->updateService();
}

void Online::ObjService::setEORflag(bool val)   {
  m_EORservice = val;
}
