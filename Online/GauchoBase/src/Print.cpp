//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================

/// Framework include files
#include <Gaucho/Print.h>
#include <Gaucho/dimhist.h>
#include <Gaucho/MonCounter.h>
#include <Gaucho/CounterDeserialize.h>
#include <nlohmann/json.hpp>

/// C/C++ include files
#include <iostream>
#include <iomanip>
#include <sstream>

namespace  {
  using namespace Online;

  std::pair<std::string,std::string> _get_title(const DimHistbuff1 *b)   {
    std::string optsep = "/:/:/:";
    std::string bopt;
    std::string option = add_ptr<char>(b, b->titoff);
    size_t seppos = option.find(optsep);
    if ((seppos != std::string::npos) && (seppos < 100000))      {
      bopt = option.substr(seppos + optsep.length());
      option.erase(seppos);
    }
    return std::make_pair(option, bopt);
  }
  std::vector<double> _get_doubles(const DimHistbuff1 *b, size_t offset=0) {
    const double* values = add_ptr<double>(b, b->dataoff + offset);
    return std::vector<double>(values, values + b->num_cells());
  }

  std::string strng(double value)    {
    if ( std::fabs(value) > 1e-5 )
      return std::to_string(value);
    return "0";
  }

}

namespace Online  {
  namespace gaucho   {

    template <typename OBJECT>
    std::string to_string(const OBJECT* /* obj */, int /* flags */)   {
      throw std::runtime_error("Invalid call to overloaded function!");
    }

    template <>
    std::string to_string(const nlohmann::json* json, int /* flags */)  {
      std::stringstream os;
      os << (*json);
      return os.str();
    }

    template <>
    std::string to_string(const double* vec, int /* flags */)  {
      std::stringstream str;
      str << std::scientific << strng(*vec);
      return str.str();
    }

    template <typename INT>
    std::string to_string_int(const std::vector<INT>& v, int /* flags */)  {
      std::stringstream str;
      str << "[";
      for( size_t i=0, n=v.size(); i<n; i++ )
        str << v[i] << (((i+1)<n) ? "," : "");
      str << "]";
      return str.str();
    }
    template <typename FLOAT>
    std::string to_string_float(const std::vector<FLOAT>& v, int /* flags */)  {
      std::stringstream str;
      str << "[";
      for( size_t i=0, n=v.size(); i<n; i++ )
        str << strng(v[i]) << (((i+1)<n) ? "," : "");
      str << "]";
      return str.str();
    }
    
    template <> std::string to_string(const std::vector<long>* vec, int flags)
    {  return to_string_int(*vec, flags);       }
    
    template <> std::string to_string(const std::vector<unsigned long>* vec, int flags)
    {  return to_string_int(*vec, flags);       }
    
    template <> std::string to_string(const std::vector<int>* vec, int flags)
    {  return to_string_int(*vec, flags);       }
    
    template <> std::string to_string(const std::vector<unsigned int>* vec, int flags)
    {  return to_string_int(*vec, flags);       }
    
    template <> std::string to_string(const std::vector<short>* vec, int flags)
    {  return to_string_int(*vec, flags);       }
    
    template <> std::string to_string(const std::vector<unsigned short>* vec, int flags)
    {  return to_string_int(*vec, flags);       }
    
    template <>  std::string to_string(const std::vector<double>* vec, int flags)
    {  return to_string_float(*vec, flags);     }

    template <>  std::string to_string(const std::vector<float>* vec, int flags)
    {  return to_string_float(*vec, flags);     }

    template <>
    std::string to_string(const std::vector<nlohmann::json>* json, int /* flags */)  {
      const auto& v = *json;
      std::stringstream str;
      str << "[" << std::scientific;
      for( size_t i=0, n=v.size(); i<n; i++ )   {
        str << v[i] << (((i+1)<n) ? "," : "") << std::endl;
      }
      str << "]";
      return str.str();
    }

    std::string to_string_axis(const DimHistbuff1* b, const DimAxis& axis)   {
      std::stringstream str;
      str << "Nbin:" << axis.nbin << " min:" << axis.min << " max:" << axis.max;
      if ( axis.lablen > 0 )   {
        const char* lab = add_ptr<char>(b, axis.laboff);
        str << " Labels: ";
        for ( int i=0; i < axis.nbin; ++i, lab = add_ptr<char>(lab, ::strlen(lab)+1) )
          str << "'" << lab << "' ";
      }
      return str.str();
    }

    std::string to_string_histo(const void* ptr, int flags)   {
      const DimHistbuff3 *b = (const DimHistbuff3*)ptr;
      std::stringstream str;
      char        text[512];
      std::string nam = add_ptr<char>(b, b->nameoff);
      std::string title = _get_title(b).first;

      ::snprintf(text, sizeof(text), "+ typ:%d dim:%d %-32s %s ",
                 b->type, b->dim, nam.c_str(), title.c_str());
      switch(b->dim)  {
      case 1:
        ::snprintf(text, sizeof(text),
                   "| X-axis: %s", to_string_axis(b, b->x).c_str());
        str << text << std::endl;
        ::snprintf(text, sizeof(text),
                   "| Stats:  %4ld min:%s max:%s sumw:%s sumw2:%s sumwx:%s sumwx2:%s",
                   long(b->nentries), to_string(b->min).c_str(), to_string(b->max).c_str(), 
                   to_string(b->sumw).c_str(), to_string(b->sumw2).c_str(), to_string(b->sumwx).c_str(),
                   to_string(b->sumwx2).c_str()); 
        str << text << std::endl;
        break;
      case 2:
        ::snprintf(text, sizeof(text),
                   "| X-axis: %s", to_string_axis(b, b->x).c_str());
        str << text << std::endl;
        ::snprintf(text, sizeof(text),
                   "| Y-axis: %s", to_string_axis(b, b->y).c_str());
        str << text << std::endl;
        ::snprintf(text, sizeof(text),
                   "| Stats:  %4ld min:%s max:%s sumw:%s sumw2:%s sumwx:%s sumwx2:%s "
                   "sumwy:%s sumwy2:%s",
                   long(b->nentries), strng(b->min).c_str(), strng(b->max).c_str(), 
                   strng(b->sumw).c_str(), strng(b->sumw2).c_str(), strng(b->sumwx).c_str(),
                   strng(b->sumwx2).c_str(), strng(b->sumwy).c_str(), strng(b->sumwy2).c_str());
        str << text << std::endl;
        break;
      case 3:
        ::snprintf(text, sizeof(text),
                   "| X-axis: %s", to_string_axis(b, b->x).c_str());
        str << text << std::endl;
        ::snprintf(text, sizeof(text),
                   "| Y-axis: %s", to_string_axis(b, b->y).c_str());
        str << text << std::endl;
        ::snprintf(text, sizeof(text),
                   "| Z-axis: %s", to_string_axis(b, b->z).c_str());
        str << text << std::endl;
        ::snprintf(text, sizeof(text),
                   "| Stats:  %4ld min:%s max:%s sumw:%s sumw2:%s sumwx:%s sumwx2:%s "
                   "sumwy:%s sumwy2:%s sumwz:%s sumwz2:%s sumwt:%s sumwt2:%s",
                   long(b->nentries), strng(b->min).c_str(), strng(b->max).c_str(),
                   strng(b->sumw).c_str(), strng(b->sumw2).c_str(), strng(b->sumwx).c_str(),
                   strng(b->sumwx2).c_str(), strng(b->sumwy).c_str(), strng(b->sumwy2).c_str(),
                   strng(b->sumwz).c_str(), strng(b->sumwz2).c_str(), strng(b->sumwt).c_str(),
                   strng(b->sumwt2).c_str());
        str << text << std::endl;
        break;
      }
      if ( flags > 0 )   {
        const DimHistbuff2 *b2 = (const DimHistbuff2*)ptr;
        const DimHistbuff3 *b3 = (const DimHistbuff3*)ptr;
        switch (b->type)  {
        case H_1DIM:
        case H_GAUDIH1F:
        case H_GAUDIH1D:
          str << "sumw:     " << to_string(_get_doubles(b, b->num_cells()*sizeof(double))) << std::endl;
          break;
        case H_2DIM:
        case H_GAUDIH2F:
        case H_GAUDIH2D:
          str << "sumw:     " << to_string(_get_doubles(b2, b2->num_cells()*sizeof(double))) << std::endl;
          break;
        case H_3DIM:
        case H_GAUDIH3F:
        case H_GAUDIH3D:
          str << "sumw:     " << to_string(_get_doubles(b3, b3->num_cells()*sizeof(double))) << std::endl;
          break;
        case H_GAUDIPR1:
        case H_PROFILE:
        case H_RATE:
          str << "sumw:     " << to_string(_get_doubles(b,   b->num_cells()*sizeof(double))) << std::endl;
          str << "sumw2:    " << to_string(_get_doubles(b, 2*b->num_cells()*sizeof(double))) << std::endl;
          str << "binsumw2: " << to_string(_get_doubles(b, 3*b->num_cells()*sizeof(double))) << std::endl;
          break;
        case H_GAUDIPR2:
        case H_2DPROFILE:
          str << "sumw:     " << to_string(_get_doubles(b2,   b2->num_cells()*sizeof(double))) << std::endl;
          str << "sumw2:    " << to_string(_get_doubles(b2, 2*b2->num_cells()*sizeof(double))) << std::endl;
          str << "binsumw2: " << to_string(_get_doubles(b2, 3*b2->num_cells()*sizeof(double))) << std::endl;
          break;
        case H_GAUDIPR3:
          str << "sumw:     " << to_string(_get_doubles(b3,   b3->num_cells()*sizeof(double))) << std::endl;
          str << "sumw2:    " << to_string(_get_doubles(b3, 2*b3->num_cells()*sizeof(double))) << std::endl;
          str << "binsumw2: " << to_string(_get_doubles(b3, 3*b3->num_cells()*sizeof(double))) << std::endl;
          break;
        case C_STATENT:
          break;
        default:
          break;
        }
      }
      return str.str();
    }

    template <typename TYPE>
    std::string to_string_array(const CntrDescr* cnt, const char* type)  {
      std::stringstream os;
      int   nel = cnt->nel;
      TYPE* ptr = (TYPE*)cnt->ptr.get();
      os << std::left << std::setw(48) << cnt->name
         << std::hex  << cnt->type
         << std::scientific << " @ " << (void*)cnt
         << std::left << std::setw(20) << (" data (" + std::string(type) + "*):")
         << " #elements: " << std::to_string(nel);
      for ( int j = 0; j < nel; j++ )  {
        if ( j==0 || (j%10) == 0 )   {
          os << std::endl << " " << std::setw(8) << std::left << ("["+std::to_string(j)+"]:  ");
        }
        os << std::setw(10) << std::right << std::to_string(ptr[j])  << " ";
      }
      return os.str();
    }

    template <>
    std::string to_string(const CntrDescr* cnt, int /* flags */)  {
      char text[256];
      switch ( cnt->type ) {
      case C_GAUDIACCCHAR:
      case C_GAUDIACCuCHAR:
        ::snprintf( text, sizeof( text ), "(CHAR)         %d", int((signed char)cnt->scalars.i_data) );
        break;
      case C_GAUDIACCSHORT:
      case C_GAUDIACCuSHORT:
        ::snprintf( text, sizeof( text ), "(SHORT)        %d", int((signed short)cnt->scalars.i_data) );
        break;
      case C_INT:
      case C_ATOMICINT:
      case C_GAUDIACCINT:
        ::snprintf( text, sizeof( text ), "(INT)          %d", (int)cnt->scalars.i_data );
        break;
      case C_UINT:
      case C_GAUDIACCuINT:
        ::snprintf( text, sizeof( text ), "(UINT)         %d", int((unsigned int)cnt->scalars.ui_data) );
        break;
      case C_LONGLONG:
      case C_ATOMICLONG:
      case C_GAUDIACCLONG:
        ::snprintf( text, sizeof( text ), "(LONG)         %ld", long( cnt->scalars.l_data ) );
        break;
      case C_ULONG:
      case C_GAUDIACCuLONG:
        ::snprintf( text, sizeof( text ), "(ULONG)        %ld", (unsigned long)( cnt->scalars.ul_data ) );
        break;
      case C_FLOAT:
      case C_ATOMICFLOAT:
        ::snprintf( text, sizeof( text ), "(FLOAT)        %.3f", cnt->scalars.f_data );
        break;
      case C_DOUBLE:
      case C_ATOMICDOUBLE:
        ::snprintf( text, sizeof( text ), "(DOUBLE)       %.3f", cnt->scalars.d_data );
        break;
      case C_RATEDOUBLE:
        ::snprintf( text, sizeof( text ), "(RATEDOUBLE)   %.3f Hz", cnt->scalars.d_data );
        break;
      case C_RATEFLOAT:
        ::snprintf( text, sizeof( text ), "(RATEFLOAT)    %.3f Hz", cnt->scalars.f_data );
        break;

      case C_INTSTAR:
      case C_UINTSTAR:
        return to_string_array<int32_t>(cnt, "INT");
      case C_LONGSTAR:
      case C_ULONGSTAR:
        return to_string_array<int64_t>(cnt, "LONG");
      case C_FLOATSTAR:
        // case C_RATEFLOATSTAR:
        return to_string_array<float>(cnt, "FLOAT");
      case C_DOUBLESTAR:
      case C_RATEDOUBLESTAR:
        return to_string_array<double>(cnt, "DOUBLE");

      case C_INTPAIR:
        ::snprintf( text, sizeof( text ), "(INT Pair) value  %d %d", cnt->ip_data.first, cnt->ip_data.second );
        break;
      case C_LONGPAIR:
        ::snprintf( text, sizeof( text ), "(LONG Pair) value %ld %ld", cnt->lp_data.first, cnt->lp_data.second );
        break;
      case C_GAUDIAVGACCc:
      case C_GAUDIAVGACCcu:
      case C_GAUDIAVGACCs:
      case C_GAUDIAVGACCsu:
      case C_GAUDIAVGACCi:
      case C_GAUDIAVGACCiu:
      case C_GAUDIAVGACCl:
      case C_GAUDIAVGACClu:
      case C_GAUDIAVGACCf:
      case C_GAUDIAVGACCd:
        ::snprintf( text, sizeof( text ), "(Gaudi Averaging Counter) Entries: %ld Sum: %.3f Mean: %.3f", cnt->gaudi.average.entries,
                    cnt->gaudi.average.sum, cnt->gaudi.average.mean );
        break;
      case C_GAUDISTATACCc:
      case C_GAUDISTATACCcu:
      case C_GAUDISTATACCs:
      case C_GAUDISTATACCsu:
      case C_GAUDISTATACCi:
      case C_GAUDISTATACCiu:
      case C_GAUDISTATACCl:
      case C_GAUDISTATACClu:
      case C_GAUDISTATACCf:
      case C_GAUDISTATACCd:
        ::snprintf( text, sizeof( text ), "(Gaudi Stat Counter) Entries: %ld Sum: %.3f Sum2: %.3f Mean: %.3f",
                    cnt->gaudi.stat.entries, cnt->gaudi.stat.sum, cnt->gaudi.stat.sum2, cnt->gaudi.stat.mean );
        break;
      case C_GAUDISIGMAACCc:
      case C_GAUDISIGMAACCcu:
      case C_GAUDISIGMAACCs:
      case C_GAUDISIGMAACCsu:
      case C_GAUDISIGMAACCi:
      case C_GAUDISIGMAACCiu:
      case C_GAUDISIGMAACCl:
      case C_GAUDISIGMAACClu:
      case C_GAUDISIGMAACCf:
      case C_GAUDISIGMAACCd:
        ::snprintf( text, sizeof( text ), "(Gaudi Sigma Counter) Entries: %ld Sum: %.3f Sum2: %.3f Mean: %.3f",
                    cnt->gaudi.sigma.entries, cnt->gaudi.sigma.sum, cnt->gaudi.sigma.sum2, cnt->gaudi.sigma.mean );
        break;
      case C_GAUDIBINACCc:
      case C_GAUDIBINACCcu:
      case C_GAUDIBINACCs:
      case C_GAUDIBINACCsu:
      case C_GAUDIBINACCi:
      case C_GAUDIBINACCiu:
      case C_GAUDIBINACCl:
      case C_GAUDIBINACClu:
      case C_GAUDIBINACCf:
      case C_GAUDIBINACCd:
        ::snprintf( text, sizeof( text ), "(Gaudi Binomial Counter) Entries: %ld True: %ld False: %ld",
                    cnt->gaudi.binomial.entries, cnt->gaudi.binomial.entries_true, cnt->gaudi.binomial.entries_false); 
        break;
      case C_GAUDIMSGACClu:
        ::snprintf( text, sizeof( text ), "(Gaudi Msg Counter) %lu", long( cnt->scalars.ul_data ) );
        break;
      default:
        snprintf( text, sizeof( text ), "Unknown Counter" );
        break;
      }
      std::stringstream os;
      os << std::left << std::setw(48) << cnt->name << std::hex << cnt->type
         << std::scientific << " @ " << (void*)cnt << " data " << text;
      return os.str();
    }
  }
}

