//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifdef NO_ONLINEBASE

// C/C++ include files
#include <string>
#include <vector>
#include <stdexcept>

namespace RTL  {
  /// C++: split string according to non-empty delimiter character
  std::vector<std::string> str_split(const std::string& source, char delimiter);
  /// C++: split string according to non-empty delimiter string
  std::vector<std::string> str_split(const std::string& source, const std::string& delimiter);
}

/// C++: split string according to non-empty delimiter character
std::vector<std::string> RTL::str_split(const std::string& source, char delimiter)  {
  std::string tmp;
  std::vector<std::string> result;
  size_t start = 0, idx = source.find(delimiter, start);
  result.reserve(10);
  tmp = source.substr(start, idx);
  if ( !tmp.empty() ) result.emplace_back(tmp);
  while( idx != std::string::npos )  {
    start = idx+1;
    idx = source.find(delimiter, start);
    tmp = source.substr(start, idx-start);
    if ( !tmp.empty() ) result.emplace_back(tmp);
  }
  return result;
}
/// C++: split string according to non-empty delimiter string
std::vector<std::string> RTL::str_split(const std::string& source, const std::string& delimiter)  {
  if ( !delimiter.empty() )   {
    std::string tmp;
    std::vector<std::string> result;
    size_t start = 0, idx = source.find(delimiter, start);
    result.reserve(10);
    tmp = source.substr(start, idx);
    if ( !tmp.empty() ) result.emplace_back(tmp);
    while( idx != std::string::npos )  {
      start = idx+1;
      idx = source.find(delimiter, start);
      tmp = source.substr(start, idx-start);
      if ( !tmp.empty() ) result.emplace_back(tmp);
    }
    return result;
  }
  throw std::runtime_error("RTL: Cannot split string with empty delimiter");
}
#endif
