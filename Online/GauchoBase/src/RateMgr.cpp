//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================

/// Framework include files
#include <Gaucho/MonTypes.h>
#include <Gaucho/RateMgr.h>

namespace {
  bool is_rate(MONTYPE typ)   {
    switch(typ)   {
    case C_RATEFLOAT:
    case C_RATEDOUBLE:
    case C_RATEDOUBLESTAR:
      return true;
    default:
      return false;
    }
  }
}

Online::RateMgr::RateMgr(MonitorClass* cl)
  : monClass(cl)
{
}

void Online::RateMgr::makeRates(unsigned long dt)   {
  if ( this->monClass )   {
    for ( auto& o : this->monClass->entities )    {
      if ( is_rate(o.second->type) )   {
	MonRateBase* r = dynamic_cast<MonRateBase*>(o.second.get());
	if ( r )   {
	  r->makeRate(dt);
	}
      }
    }
  }
}

void Online::RateMgr::zero()   {
  if ( this->monClass )   {
    for ( auto& o : this->monClass->entities )    {
      if ( is_rate(o.second->type) )   {
	MonRateBase* r = dynamic_cast<MonRateBase*>(o.second.get());
	if ( r )   {
	  r->zero();
	}
      }
    }
  }
}

void Online::RateMgr::CreateOutputServices(const std::string& infix)    {
  if ( this->monClass )   {
    for ( auto& o : this->monClass->entities )    {
      if ( is_rate(o.second->type) ) 
	o.second->create_OutputService(infix);
    }
  }
}
