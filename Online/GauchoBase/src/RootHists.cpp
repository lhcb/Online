//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================

// Framework include files
#include <Gaucho/RootHists.h>

/// Online namespace declaration
namespace Online   {

  template <typename Q, typename T> inline void copy_items(Q* to, const T* from, size_t len)    {
    if ( from )    { 
      for(size_t i=0; i < len; ++i, ++to, ++from)
	*to = (Q)(*from);
    }
    ::memset(to, 0, len*sizeof(Q));
  }

  template <typename T> void MyTH1<T>::movetodimbuffer(DimHistbuff1 *b)   {
    void *base = add_ptr(b, b->dataoff);
    int ncells = this->GetNcells();
    int blocksize = ncells*sizeof(double);
    double* sumw2 = this->GetSumw2()->GetArray();
    copy_items(add_ptr<double>(base,0*blocksize), this->fArray, ncells);
    copy_data (add_ptr<double>(base,1*blocksize), sumw2, blocksize);
    b->min    = this->fMinimum;
    b->max    = this->fMaximum;
    b->sumw   = this->fTsumw;
    b->sumw2  = this->fTsumw2;
    b->sumwx  = this->fTsumwx;
    b->sumwx2 = this->fTsumwx2;
  }

  template <typename T> void MyTH1<T>::movefromdimbuffer(const void *base)    {
    int ncells = this->GetNcells();
    int blocksize = ncells*sizeof(double);
    double* sumw2 = this->GetSumw2()->GetArray();
    copy_items(this->fArray, add_ptr<double>(base,0*blocksize), ncells);
    copy_data (sumw2,        add_ptr<double>(base,1*blocksize), blocksize);
  }

  template <> void MyTH1<TH1D>::movetodimbuffer(DimHistbuff1 *b)   {
    void *base = add_ptr(b, b->dataoff);
    int blocksize = this->GetNcells()*sizeof(double);
    double* sumw2 = this->GetSumw2()->GetArray();
    copy_data(add_ptr<double>(base,0*blocksize), this->fArray, blocksize);
    copy_data(add_ptr<double>(base,1*blocksize), sumw2, blocksize);
    b->min    = this->fMinimum;
    b->max    = this->fMaximum;
    b->sumw   = this->fTsumw;
    b->sumw2  = this->fTsumw2;
    b->sumwx  = this->fTsumwx;
    b->sumwx2 = this->fTsumwx2;
  }
  template <> void MyTH1<TH1D>::movefromdimbuffer(const void *base)    {
    int blocksize = this->GetNcells()*sizeof(double);
    double* sumw2 = this->GetSumw2()->GetArray();
    ::memcpy(this->fArray, add_ptr(base,0*blocksize), blocksize);
    ::memcpy(sumw2, add_ptr(base,1*blocksize), blocksize);
  }

  template void MyTH1<TH1F>::movetodimbuffer(DimHistbuff1 *b);
  template void MyTH1<TH1F>::movefromdimbuffer(const void *base);

  template void MyTH1<TH1I>::movetodimbuffer(DimHistbuff1 *b);
  template void MyTH1<TH1I>::movefromdimbuffer(const void *base);

  template void MyTH1<TH1S>::movetodimbuffer(DimHistbuff1 *b);
  template void MyTH1<TH1S>::movefromdimbuffer(const void *base);

  template void MyTH1<TH1C>::movetodimbuffer(DimHistbuff1 *b);
  template void MyTH1<TH1C>::movefromdimbuffer(const void *base);
}
