//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================

// C/C++ include files
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <setjmp.h>

// Framework include files
#include "Gaucho/SegvHandler.h"

static jmp_buf __ReadCheckEnv;
static void segvTestHandler(int        /* sig */,
			    siginfo_t* /* siginfo */, 
			    void*      /* context */)
{
  longjmp(__ReadCheckEnv,27);
}

int CheckRead(void *add)  {
  struct sigaction act;
  struct sigaction oldact;
  memset (&act,0,sizeof(act));
  act.sa_sigaction = &segvTestHandler;
  act.sa_flags = SA_SIGINFO;
  sigaction(SIGSEGV,&act,&oldact);
  int ret=setjmp(__ReadCheckEnv);
  if (ret == 27)  {
    sigaction(SIGSEGV,&oldact,0);
    return 1;
  }
  char dummy = *(char*)add;
  if ( dummy ) {}
  sigaction(SIGSEGV,&oldact,0);
  return 0;
}


static void segvhandler (int sig, siginfo_t *siginfo, void *context)   {
  char str[255];
  SegvHandler *M_SegvHandler = &SegvHandler::instance();

  size_t len = snprintf (str,255,"********SEGVHANDLER: Received signal %d, Faulting Address: %lx Access error: %s\n",sig,
			 (unsigned long )siginfo->si_addr, (siginfo->si_code == 1) ? "Address Not Mapped" : "insufficient Access Rights" );
  //  printf("%s",str);
  str[sizeof(str)-2] = '\n';
  str[sizeof(str)-1] = 0;
  ::write(STDERR_FILENO,str,len);
  if (M_SegvHandler != 0)  {
    if ((M_SegvHandler->oldact.sa_flags & SA_SIGINFO) != 0)  {
      M_SegvHandler->oldact.sa_sigaction(sig,siginfo,context);
    }
    else  {
      if (M_SegvHandler->oldact.sa_handler == SIG_DFL)  {
	_exit(0);
      }
      else if (M_SegvHandler->oldact.sa_handler == SIG_IGN)  {
	return;
      }
      else  {
	M_SegvHandler->oldact.sa_handler(sig);
      }
    }
  }
}

SegvHandler& SegvHandler::instance()  {
  static SegvHandler inst;
  return inst;
}

SegvHandler::SegvHandler()  {
  memset (&act,0,sizeof(act));
  act.sa_sigaction = &segvhandler;
  act.sa_flags = SA_SIGINFO;
  sigaction(SIGSEGV,&act,&oldact);
}

SegvHandler::~SegvHandler()  {
  sigaction(SIGSEGV,&oldact,&act);
}
