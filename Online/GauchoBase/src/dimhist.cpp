//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================

// Framework include files
#include <Gaucho/dimhist.h>
#include <Gaucho/MonTypes.h>

bool Online::DimBuffBase::is_counter()  const   {
  switch (this->type)  {
  case C_INT:
  case C_UINT:
  case C_ULONG:
  case C_LONGLONG:
  case C_ATOMICINT:
  case C_ATOMICLONG:
  case C_FLOAT:
  case C_RATEFLOAT:
  case C_DOUBLE:
  case C_RATEDOUBLE:
  case C_INTSTAR:
  case C_UINTSTAR:
  case C_LONGSTAR:
  case C_ULONGSTAR:
  case C_FLOATSTAR:
  case C_DOUBLESTAR:
  case C_RATEDOUBLESTAR:
  case C_LONGPAIR:
  case C_INTPAIR:

  case C_GAUDIACCCHAR:
  case C_GAUDIACCuCHAR:
  case C_GAUDIACCSHORT:
  case C_GAUDIACCuSHORT:
  case C_GAUDIACCuLONG:
  case C_GAUDIACCLONG:
  case C_GAUDIACCuINT:
  case C_GAUDIACCINT:
  case C_GAUDIACCFLOAT:
  case C_GAUDIACCDOUBLE:

    
  case C_GAUDIAVGACCc: 
  case C_GAUDIAVGACCcu:
  case C_GAUDIAVGACCs: 
  case C_GAUDIAVGACCsu:
  case C_GAUDIAVGACCi: 
  case C_GAUDIAVGACCiu:
  case C_GAUDIAVGACCl:
  case C_GAUDIAVGACClu:
  case C_GAUDIAVGACCf: 
  case C_GAUDIAVGACCd: 

  case C_GAUDISIGMAACCc: 
  case C_GAUDISIGMAACCcu:
  case C_GAUDISIGMAACCs: 
  case C_GAUDISIGMAACCsu:
  case C_GAUDISIGMAACCi: 
  case C_GAUDISIGMAACCiu:
  case C_GAUDISIGMAACCl:
  case C_GAUDISIGMAACClu:
  case C_GAUDISIGMAACCf: 
  case C_GAUDISIGMAACCd: 

  case C_GAUDISTATACCc:  
  case C_GAUDISTATACCcu: 
  case C_GAUDISTATACCs:  
  case C_GAUDISTATACCsu: 
  case C_GAUDISTATACCi:  
  case C_GAUDISTATACCiu:   
  case C_GAUDISTATACCl:
  case C_GAUDISTATACClu:   
  case C_GAUDISTATACCf:    
  case C_GAUDISTATACCd:    

  case C_GAUDIBINACCc:     
  case C_GAUDIBINACCcu:    
  case C_GAUDIBINACCs:     
  case C_GAUDIBINACCsu:    
  case C_GAUDIBINACCi:     
  case C_GAUDIBINACCiu:    
  case C_GAUDIBINACCl:
  case C_GAUDIBINACClu:    
  case C_GAUDIBINACCf:     
  case C_GAUDIBINACCd:
    return true;
  default:
    break;
  }
  return false;
}

bool Online::DimBuffBase::is_histo()  const   {
  switch (this->type)  {
  case H_1DIM:                 // No.1
  case H_GAUDIH1F:
  case H_GAUDIH1D:
  case H_2DIM:
  case H_GAUDIH2F:
  case H_GAUDIH2D:
  case H_3DIM:
  case H_GAUDIH3F:
  case H_GAUDIH3D:
  case H_GAUDIPR1:             // No.10
  case H_PROFILE:
  case H_RATE:
  case H_GAUDIPR2:
  case H_2DPROFILE:
  case H_GAUDIPR3:
    return true;
  case C_STATENT:              // No.17
  default:
    break;
  }
  return false;
}
