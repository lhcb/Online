//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================

/// Framework includes
#include <Gaucho/CounterDeserialize.h>
#include <Gaucho/MyDimErrorHandler.h>
#include <Gaucho/RPCRec.h>
#include <Gaucho/Print.h>
#include <CPP/mem_buff.h>
#include <dim/dic.hxx>
#include <RTL/rtl.h>

/// C/C++ includes
#include <fstream>
#include <iostream>
#include <filesystem>

extern "C" int gaucho_app_counter_deserialize( int argc, char* argv[] ) {
  namespace fs = std::filesystem;
  using namespace Online;
  std::error_code ec;
  std::string     input;
  RTL::CLI        cli( argc, argv, []() {
      std::cout << " Usage: taskCounters -arg [-arg]                 \n\n"
		<< "  -in(put)=<file-name>    Input file name.         \n"
		<< "  -data                   Dump array data content. \n"
		<< "  -help                   Show this help.          \n"
		<< std::endl;
    } );
  bool help = cli.getopt( "help", 1 );
  bool data = cli.getopt( "data", 1 );
  cli.getopt( "input",  1, input);
  DimClient::addErrorHandler(new MyDimErrorHandler(false));
  RPCRec::set_default_dns();
  if ( help ) {
    cli.call_help();
    exit(EXIT_SUCCESS);
  }
  else if ( !fs::exists(input, ec) )  {
    std::cout << "The input file " << input << " cannot be found." << std::endl;
    return ENOENT;
  }
  auto len = fs::file_size(input, ec);
  if ( ec )  {
    std::cout << "The input file " << input << " cannot be accessed." << std::endl;
    return EPERM;
  }

  std::ifstream in(input, std::ios::in|std::ios::binary);
  if ( in.good() )   {
    mem_buff buffer(len+1);
    RPCRec rec("None", -1, false);

    in.read(buffer.begin<char>(), len);
    buffer.set_cursor(len);
    in.close();
    rec.analyseReply(buffer.used(), buffer.begin());
    std::cout << std::endl
	      << ::lib_rtl_timestr("%Y-%m-%d %H:%M:%S")
	      << " Retrieved " << rec.hists.size()
	      << " Counters from " << input << std::endl;

    for (const auto& k : rec.hists )   {
      std::unique_ptr<CntrDescr> cnt(CounterSerDes::de_serialize(k.second));
      std::cout << gaucho::to_string(*cnt, data ? 1 : 0) << std::endl << std::flush;
    }
  }
  return EXIT_SUCCESS;
}

extern "C" int counterDeserialize(int argc, char *argv[])   {
  return gaucho_app_counter_deserialize (argc, argv);
}
