//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================

/// Framework includes
#include <Gaucho/MyDimErrorHandler.h>
#include <Gaucho/RPCRec.h>
#include <Gaucho/Print.h>
#include <CPP/mem_buff.h>
#include <dim/dic.hxx>
#include <RTL/rtl.h>

/// C/C++ includes
#include <regex>
#include <fstream>
#include <iostream>
#include <filesystem>

using namespace Online;

extern "C" int gaucho_app_histogram_deserialize( int argc, char* argv[] ) {
  namespace fs = std::filesystem;
  std::error_code ec;
  std::string input;
  RTL::CLI    cli( argc, argv, []()  {
      std::cout << " Usage: taskCounters -arg [-arg]                 \n\n"
		<< "  -in(put)=<file-name>    Input file name.         \n"
		<< "  -data                   Dump array data content. \n"
		<< "  -help                   Show this help.          \n"
		<< std::endl;
    });
  bool help = cli.getopt( "help", 1 );
  bool data = cli.getopt( "data", 1 );
  cli.getopt( "input", 1, input);
  DimClient::addErrorHandler(new MyDimErrorHandler(false));
  RPCRec::set_default_dns();
  if ( help ) {
    cli.call_help();
    return EXIT_SUCCESS;
  }
  fs::path path = fs::path(input).parent_path();
  if ( !fs::exists(path, ec) )  {
    std::cout << "The input directory " << path.string() << " cannot be found." << std::endl;
    return ENOENT;
  }

  std::string match = fs::path(input).filename();
  int flags = std::regex_constants::icase | std::regex_constants::ECMAScript;
  std::regex check(match, (std::regex_constants::syntax_option_type)flags);
  for(auto it = fs::directory_iterator(path); it != fs::directory_iterator(); ++it)  {
    std::smatch sm;
    std::string nam = fs::path(*it).filename();
    bool stat = std::regex_match(nam, sm, check);
    if ( !stat )    {
      continue;
    }
    fs::path file_path = *it;
    if ( fs::is_regular_file(file_path, ec) )  {
      auto len = fs::file_size(file_path, ec);
      if ( ec )  {
	std::cout << "The input file " << file_path << " cannot be accessed." << std::endl;
	return EPERM;
      }
      mem_buff buffer(len+1);
      std::ifstream in(file_path, std::ios::in|std::ios::binary);
      if ( in.good() )   {
	RPCRec rec("None", -1, false);
	in.read(buffer.begin<char>(), len);
	buffer.set_cursor(len);
	in.close();
	rec.analyseReply(buffer.used(), buffer.begin());
	std::cout << std::endl
		  << ::lib_rtl_timestr("%Y-%m-%d %H:%M:%S") << " Retrieved " << rec.hists.size() 
		  << " Histograms from " << file_path << std::endl;
	for (const auto& k : rec.hists )   {
	  std::cout << gaucho::to_string_histo(k.second, data ? 1 : 0) << std::endl;
	}
      }
    }
  }
  return EXIT_SUCCESS;
}

extern "C" int histoDeserialize(int argc, char *argv[])   {
  return gaucho_app_histogram_deserialize (argc, argv);
}
