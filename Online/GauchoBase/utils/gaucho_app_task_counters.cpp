//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================

/// Framework includes
#include <Gaucho/CounterTask.h>
#include <Gaucho/Print.h>
#include <RTL/rtl.h>

/// C/C++ includes
#include <iostream>

using namespace Online;
namespace {
  static void print_tasks( const std::set<std::string>& tasks ) {
    size_t i = 0;
    for ( const auto& t : tasks ) ::printf( "Task[%3ld]  %s\n", ++i, t.c_str() );
  }
}

extern "C" int gaucho_app_task_counters( int argc, char* argv[] ) {
  std::string dns, task, match;
  int         continuous = 0;
  RTL::CLI    cli( argc, argv, []() {
      std::cout << " Usage: taskCounters -arg [-arg]                                       \n\n"
		<< "  -d(ns)=<dns>            DNS name to interrogate for services           \n"
		<< "  -t(ask)=<task-name>     Supply task name for detailed counter printout \n"
		<< "  -m(atch)=<regex>        Only retrieve counters with a name matching    \n"
		<< "                          the supplied regular expression                \n"
		<< "  -c(continuous)=<number> Continuous mode. Give seconds between updates." << std::endl;
    } );
  bool        help = cli.getopt( "help", 1 );
  cli.getopt( "continuous", 1, continuous );
  cli.getopt( "task",  1, task );
  cli.getopt( "match", 1, match );
  cli.getopt( "dns",   1, dns );
  if ( help )  {
    cli.call_help();
    return 0;
  }
  if ( dns.empty() )  {
    std::cout << "Insufficient Number of arguments" << std::endl;
    cli.call_help();
    return EINVAL;
  }
  if ( continuous == 1 )  {
    continuous = 2;
  }

  auto [status, tasks] = CounterTask::taskList( dns );
  if ( task.empty() ) {
    print_tasks( tasks );
    return 0;
  }
  auto h = std::make_unique<CounterTask>( task, dns );
  auto [sc, requests] = h->directory( match );
  std::vector<CntrDescr*> objs;

  if ( status == 1 )  {
    std::cout << "Task " << task << " does not exist..." << std::endl;
    return 1;
  }
#if 0
  std::cout << "Number of Counters for Task " << task << " " << requests.size() << std::endl
	    << "Trying to retrieve the following Counters:" << std::endl;
  for( const auto& count : requests )
    std::cout << count << std::endl;
  std::cout << std::flush;
#endif

 Again:
  h->counters( requests, objs );
  std::cout << std::endl
	    << ::lib_rtl_timestr("%Y-%m-%d %H:%M:%S") << "  Retrieved " << objs.size()
	    << " Counters  from " << task << std::endl;
  for ( size_t i = 0; i < objs.size(); i++ ) {
    std::cout << gaucho::to_string(*objs[i]) << std::endl << std::flush;
  }
  if ( continuous > 0 ) {
    std::cout << "========================================================================================="
              << std::endl;
    ::lib_rtl_sleep( continuous * 1000 );
    objs.clear();
    goto Again;
  }
  return 0;
}

extern "C" int taskCounters(int argc, char *argv[])   {
  return gaucho_app_task_counters (argc, argv);
}
