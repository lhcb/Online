//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================

/// Framework includes
#include <Gaucho/CounterTaskJson.h>
#include <Gaucho/Print.h>
#include <RTL/rtl.h>

/// C/C++ include files
#include <sstream>

using namespace Online;

namespace  {
  static void print_tasks(const std::set<std::string>& tasks)   {
    size_t i = 0;
    char txt[128];
    for( const auto& t : tasks )   {
      std::snprintf(txt, sizeof(txt), "Task[%3ld]  %s", ++i, t.c_str());
      std::cout << txt << std::endl;
    }
  }
}

extern "C" int gaucho_app_task_counters_json(int argc, char *argv[])  {
  std::string dns, task, match;
  int continuous = 0;
  RTL::CLI cli(argc, argv, [] (int ac, char** av) {
    ::printf(
	" Usage: taskCounters -arg [-arg]  \n"
	"  -d(ns)=<dns>            DNS name to interrogate for services\n"
	"  -t(ask)=<task-name>     Supply task name for detailed counter printout \n"
	"  -json                   Output pure json (re-loadable)                 \n"
	"  -m(atch)=<regex>        Only retrieve counters with a name matching    \n"
	"                          the supplied regular expression                \n"
	"  -c(continuous)=<number> Continuous mode. Give seconds between updates. \n");
      std::stringstream str;
      for (int i=0; i<ac; ++i) str << av[i] << " ";
      ::printf("%d arguments given: %s\n", ac, str.str().c_str());
      ::fflush(stdout);
      ::exit(EINVAL);
    });
  bool help = cli.getopt("help",1);
  bool json = cli.getopt("json",1);
  cli.getopt( "continuous", 1, continuous);
  cli.getopt( "task",  1, task);
  cli.getopt( "match", 1, match );
  cli.getopt( "dns",   1, dns);
  if ( help )  {
    cli.call_help();
    return EXIT_SUCCESS;
  }
  if ( dns.empty() )  {
    ::printf("Insufficient Number of arguments.\n");
    cli.call_help();
    return EINVAL;
  }
  if ( continuous == 1 )   {
    continuous = 2;
  }
  if ( task.empty() )   {
    std::set<std::string> tasks;
    CounterTaskJson::taskList(dns, tasks);
    print_tasks(tasks);
    return EXIT_SUCCESS;
  }
  auto h = std::make_unique<CounterTaskJson>(task, dns);
  auto [status, requests] = h->directory(match);
  if ( status == 1 )    {
    if ( !json )  {
      ::printf("Task \"%s\" does not exist in DIM domain: %s...\n",
	       task.c_str(), dns.c_str());
    }
    return ENOENT;
  }
  if ( !json ) ::printf("Number of Counters for Task %s: %ld  [match: \"%s\"]\n",
			task.c_str(), requests.size(), match.c_str());
  if ( !json )  {
    ::printf("Trying to retrieve the following Counters:\n");
    for( const auto& count : requests )  {
      ::printf("%s\n", count.c_str());
    }
    ::printf("\n");
  }

  std::vector<nlohmann::json> objs;
 Again:
  objs.clear();
  h->counters(requests, objs);
  if ( !json )  {
    ::printf("\n%s  Retrieved %ld Counters from %s\n",
	     ::lib_rtl_timestr("%Y-%m-%d %H:%M:%S"), objs.size(), task.c_str());
  }
  std::cout << gaucho::to_string(objs) << std::endl;
  if ( continuous > 0 )  {
    ::printf("=========================================================================================\n");
    ::lib_rtl_sleep(continuous*1000);
    goto Again;
  }
  return EXIT_SUCCESS;
}

extern "C" int taskCountersJson(int argc, char *argv[])  {
  return gaucho_app_task_counters_json (argc, argv);
}
