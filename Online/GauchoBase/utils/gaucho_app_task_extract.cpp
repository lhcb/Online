//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================

/// Framework includes
#include <Gaucho/HistTaskJson.h>
#include <RTL/rtl.h>

/// C/C++ include files
#include <iomanip>
#include <fstream>
#include <iostream>
#include <filesystem>

using namespace Online;

namespace  {
  static void print_tasks(const std::vector<std::string>& tasks)   {
    size_t i = 0;
    for( const auto& t : tasks )
      ::printf("Task[%3ld]  %s\n", ++i, t.c_str());
  }
}

extern "C" int gaucho_app_task_extract (int argc, char *argv[]) // Taskname, DNS Node
{
  RTL::CLI cli(argc, argv, [] (){
      std::cout 
	<< " Usage: taskHistos -arg [-arg]                                          \n"
	<< "  -dns=<dns>           DNS name to interrogate for services.            \n"
	<< "  -task=<task-name>    Supply task name for detailed counter printout.  \n"
	<< "  -one                 Get histograms 'one' by 'one'.                   \n"
	<< "  -list(-only)         Only list histograms. Do not receive.            \n"
	<< "  -out(put)=<file>     Output file to dump selected histograms.         \n"
	<< "  -show                Display the retrieved histograms.                \n"
	<< "  -verb(ose)           Display the retrieved histograms.                \n"
	<< "  -help                Display this help.                               \n"
	<< std::endl;
      ::exit(EINVAL);
    });

  std::string dns, task, match, output, type;
  bool show    = cli.getopt("show",      3);
  bool help    = cli.getopt("help",      3);
  bool one     = cli.getopt("one",       3);
  bool verbose = cli.getopt("verbose",   3);
  bool list    = cli.getopt("list-only", 4);
  bool htyp    = cli.getopt("histos",    3);
  bool ctyp    = cli.getopt("counters",  3);
  int  timeout = 5;
  cli.getopt("dns",     3, dns);
  cli.getopt("task",    4, task);
  cli.getopt("match",   1, match);
  cli.getopt("output",  3, output);
  cli.getopt("timeout", 4, timeout);
  type = htyp ? "Histos" : (ctyp ? "Counter" : "Counter");

  if ( help )  {
    cli.call_help();
  }
  if ( dns.empty() )  {
    std::cout << "Insufficient Number of arguments" << std::endl;
    cli.call_help();
  }

  if ( task.empty() )   {
    print_tasks(TaskRPC::taskList(type, dns));
    return 0;
  }

  auto rpc = std::make_unique<TaskRPC>(type, task, dns, timeout);
  auto [status, requests] = rpc->directory(match);
  if ( status == 1 )  {
    printf("Task \"%s\" does not exist in DIM domain: %s...\n",
	   task.c_str(), dns.c_str());
    return 1;
  }
  if ( list || verbose )  {
    std::cout << "Number of matching Items " << match
	      << " of type " << type << " for Task " << task 
	      << ": " << requests.size() << std::endl;
    if ( list )   {
      return 0;
    }
  }
  std::cout << "Trying to retrieve the data. Output file: " << output << std::endl;

  for( std::size_t num_req=0; !requests.empty(); ++num_req )   {
    std::vector<std::string> req;
    std::size_t num_hist = one ? 1 : requests.size();

    req.insert(req.end(), requests.begin(), requests.begin()+num_hist);
    requests.erase(requests.begin(), requests.begin()+num_hist);
    status = rpc->items(req);
    if ( status != 0 )   {
      /// Error
    }
    
    std::size_t siz    = rpc->getSize();
    const char* valin  = rpc->getData();
    const char* valend = add_ptr<char>(valin,siz);

    std::cout << "Retrieved " << rpc->items().size() << " Item(s). [" << siz << " bytes]" << std::endl;
    //
    // Dump histos to output file
    if ( !output.empty() )   {
      namespace fs = std::filesystem;
      std::error_code ec;
      fs::path output_path = output + "." + std::to_string(num_req);

      std::cout << "Open output file: " << output_path.string() << std::endl;
      output_path = fs::absolute(output_path, ec);
      fs::create_directories(output_path.parent_path(), ec);
      if ( ec )   {
	std::cout << "Failed to create output directory: "
		  << output_path.parent_path().string() << std::endl;
	::exit(errno);
      }
      std::ofstream out(output_path.string(), std::ios_base::out|std::ios::binary);
      if ( !out.is_open() )   {
	std::cout << "Failed to open output file " << output_path.string() 
		  << "  [" << ::strerror(errno) << "]" << std::endl;
	::exit(errno);
      }
      out.write(valin, siz);
      out.close();
    }
    auto *ptr = add_ptr<DimHistbuff1>(valin, sizeof(RPCReply));
    for (std::size_t count=0; (char*)ptr < valend; ++count )    {
      if ( show ) std::cout << "============================================================" << std::endl;
      std::cout << "Item [" << count << "]: " << ptr->name() << std::endl;
      if ( show )   {
	//print_item(ptr);
      }
      if ( ptr->reclen <= 0 ) break;
      ptr = add_ptr<DimHistbuff1>(ptr,ptr->reclen);
    }
  }
  return 0;
}

extern "C" int taskGauchoExtract (int argc, char *argv[])  {
  return gaucho_app_task_extract(argc, argv);
}
