//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================

/// Framework includes
#include <Gaucho/HistTaskJson.h>
#include <RTL/rtl.h>

/// C/C++ include files
#include <regex>
#include <fstream>
#include <iostream>
#include <filesystem>

using namespace Online;

namespace  {
  static void print_tasks(const std::set<std::string>& tasks)   {
    size_t i = 0;
    for( const auto& t : tasks )
      ::printf("Task[%3ld]  %s\n", ++i, t.c_str());
  }
}

extern "C" int gaucho_app_task_histos_json(int argc, char *argv[])
{
  RTL::CLI cli(argc, argv, [] (){
      std::cout 
	<< " Usage: taskHistos -arg [-arg]                                          \n"
	<< "  -d(ns)=<dns>         DNS name to interrogate for services.            \n"
	<< "  -task=<task-name>    Supply task name for detailed counter printout.  \n"
	<< "  -o(ne)               Get histograms 'one' by 'one'.                   \n"
	<< "  -time(out)=<secs>    Timeout for DIM RPC in seconds.                  \n"
	<< "  -l(list-only)        Only list histograms. Do not receive.            \n"
	<< "  -o(output)=<file>    Output file to dump selected histograms.         \n"
	<< "  -s(how)              Display the retrieved histograms.                \n"
	<< std::endl;
      ::exit(EINVAL);
    });
 
  std::string dns, task, match, output;
  bool show      = cli.getopt("show", 1);
  bool help      = cli.getopt("help", 1);
  bool one       = cli.getopt("one",  3);
  bool json      = cli.getopt("json", 4);
  bool list_only = cli.getopt("list-only", 4);
  int  timeout   = 5;
  cli.getopt("dns",     3, dns);
  cli.getopt("task",    4, task);
  cli.getopt("match",   1, match);
  cli.getopt("output",  3, output);
  cli.getopt("timeout", 4, timeout);

  if ( help )  {
    cli.call_help();
  }
  if ( dns.empty() )  {
    std::cout << "Insufficient Number of arguments" << std::endl;
    cli.call_help();
  }

  if ( task.empty() )   {
    std::set<std::string> tasks;
    HistTaskJson::taskList(dns,tasks);
    print_tasks(tasks);
    return 0;
  }

  auto h = std::make_unique<HistTaskJson>(task, dns, timeout);
  auto [status, requests] = h->directory(match);
  
  if ( status == 1 )  {
    printf("Task \"%s\" does not exist in DIM domain: %s...\n",
	   task.c_str(), dns.c_str());
    return 1;
  }
  if ( list_only || !json )  {
    if ( !json )  {
      std::cout << "Number of matching Histograms " << match
		<< " for Task " << task << ": " << requests.size()
		<< std::endl;
    }
    if ( match.empty() || match == "*" )   {
      if ( json ) std::cout << "[" << std::endl;
      for ( std::size_t i=0; i < requests.size(); ++i )  {
	const auto& histo = requests[i];
	if ( json ) std::cout << "{ \"name\": \"" << histo << "\" }" << (i+1<requests.size() ? "," : "") << std::endl;
	else        std::cout << histo << std::endl;
      }
      if ( json ) std::cout << "]" << std::endl;
    }
    if ( list_only )   {
      return 0;
    }
  }
  std::cout << "Trying to retrieve the histograms. Output directory: " << output << std::endl;

  while ( !requests.empty() )   {
    std::vector<std::string> req;
    std::vector<nlohmann::json> json_histograms;
    std::size_t num_hist = one ? 1 : requests.size();
    std::time_t start = ::time(0);

    req.insert(req.end(), requests.begin(), requests.begin()+num_hist);
    requests.erase(requests.begin(), requests.begin()+num_hist);
    h->histos(req, json_histograms);
    if ( json_histograms.size() > 0 )
      std::cout << "Retrieved " << json_histograms.size() << " Histogram(s)." << std::endl;
    else
      std::cout << "Retrieved NO Histograms after " << long(::time(0)-start) << " seconds." << std::endl;

    for (std::size_t i=0; i < json_histograms.size(); i++)      {
      if ( show ) std::cout << "============================================================" << std::endl;
      std::cout << "Histogram [" << i << "]: "
		<< json_histograms[i].at("name").get<std::string>() << std::endl;
      if ( show ) std::cout << json_histograms[i] << std::endl;
    }
    //
    // Dump histos to output file
    if ( !output.empty() && !json_histograms.empty() )   {
      namespace fs = std::filesystem;
      std::error_code ec;
      fs::path output_path = output + "/" + json_histograms[0].at("name").get<std::string>() + ".json";

      std::cout << "Open output file: " << output_path.string() << std::endl;
      output_path = fs::absolute(output_path, ec);
      fs::create_directories(output_path.parent_path(), ec);
      if ( ec )   {
	std::cout << "Failed to create output directory: "
		  << output_path.parent_path().string() << std::endl;
	::exit(errno);
      }
      std::ofstream out(output_path.string(), std::ios_base::out);
      if ( !out.is_open() )   {
	std::cout << "Failed to open output file " << output_path.string() 
		  << "  [" << ::strerror(errno) << "]" << std::endl;
	::exit(errno);
      }
      out << "[" << std::endl;
      for (std::size_t i=0; i < json_histograms.size(); i++)      {
	out << json_histograms[i] << std::endl;
      }
      out << "]" << std::endl;
      out.close();
    }
  }
  return 0;
}

extern "C" int taskHistosJson(int argc, char *argv[])  {
  return gaucho_app_task_histos_json (argc, argv);
}
