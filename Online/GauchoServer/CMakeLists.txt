#===============================================================================
#  LHCb Online software suite
#-------------------------------------------------------------------------------
# Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
# All rights reserved.
#
# For the licensing terms see OnlineSys/LICENSE.
#
# Author     : M.Frank
#
#===============================================================================
#[=======================================================================[.rst:
Online/GauchoServer
----------
#]=======================================================================]

find_package(yaml-cpp REQUIRED)
#
# We need yaml-cpp > 0.6.3 to use a well defined yaml-cpp::yaml-cpp target
if(NOT TARGET yaml-cpp::yaml-cpp AND TARGET yaml-cpp)
  add_library(yaml-cpp::yaml-cpp INTERFACE IMPORTED GLOBAL)
  target_link_libraries(yaml-cpp::yaml-cpp INTERFACE yaml-cpp)
  target_include_directories(yaml-cpp::yaml-cpp SYSTEM INTERFACE ${YAML_CPP_INCLUDE_DIR})
endif()
#
online_library(GauchoServer
        src/YAML.cpp
        src/GauchoRPC.cpp
        src/rpc_relay_client.cpp
        src/rpc_relay_server.cpp
        src/gaucho_adapter.cpp
        src/gaucho_ioc_adapter.cpp
        src/gaucho_server.cpp
        src/gaucho_server_run.cpp
)
#
target_link_libraries(GauchoServer
 PUBLIC	    nlohmann_json::nlohmann_json
	    yaml-cpp::yaml-cpp
            Online::GauchoBase
            Online::RPCServer
            Online::dim
)
#
# =======================================================================
online_root_dictionary(GauchoServerDict
  INCLUDES    ${CMAKE_CURRENT_SOURCE_DIR}/dict/dictionary.h
              ${CMAKE_CURRENT_SOURCE_DIR}/include/GauchoServer/gaucho_adapter.h
              ${CMAKE_CURRENT_SOURCE_DIR}/include/GauchoServer/gaucho_server.h
  USES        Online::GauchoServer Online::OnlineBase Online::dim ROOT::Core
  MAKE_LIBRARY ON)
#
# =======================================================================
online_install_includes(include)
online_install_scripts(scripts)
online_install_python(python)
