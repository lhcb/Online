//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================

/// Framework include files
#include <GauchoServer/rpc_relay_client.h>
#include <GauchoServer/rpc_relay_server.h>
#include <GauchoServer/gaucho_adapter.h>
#include <GauchoServer/gaucho_server.h>

namespace gaucho  {
  typedef server_adapter::action_counters_t action_counters_t;
}

// -------------------------------------------------------------------------
// Regular dictionaries
// -------------------------------------------------------------------------
#if defined(__CINT__) || defined(__MAKECINT__) || defined(__CLING__) || defined(__ROOTCLING__)

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ namespace rpc_relay;
#pragma link C++ class rpc_relay::server_t;
#pragma link C++ class rpc_relay::client_t;
#pragma link C++ class rpc_relay::client_t::counter_t;
#pragma link C++ class std::vector<rpc_relay::client_t::counter_t>;

#pragma link C++ namespace gaucho;
#pragma link C++ class gaucho::gaucho_server;
#pragma link C++ class gaucho::server_adapter;
#pragma link C++ class gaucho::action_counters_t;

#endif
