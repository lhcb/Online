//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//=========================================================================
//
//  Author     : M.Frank
//  Created    : 05/4/2022
//
//=========================================================================
#ifndef GAUCHOSERVER_GAUCHO_SERVER_ADAPTER_H
#define GAUCHOSERVER_GAUCHO_SERVER_ADAPTER_H


/// Framework includes
#include <GauchoServer/rpc_relay_server.h>
#include <nlohmann/json.hpp>

/// Forward declaration
namespace rpc {  class HttpJsonRpcHandler;  }

/// gaucho namespace declaration
namespace gaucho {

  /// Definition of the server adapter class
  /**
   *
   *    \author  M.Frank
   *    \version 1.0
   */
  class server_adapter : public rpc_relay::server_t::server_adapter_t   {
  public:
    static constexpr const unsigned long HIST_CALL    = 0x1UL;
    static constexpr const unsigned long COUNTER_CALL = 0x2UL;
    static constexpr const unsigned int MAGIC_PATTERN = 0xFEEDBABE;

    using json = nlohmann::json;
    using server_t = rpc_relay::server_t;

    ///
    /**
     *
     *    \author  M.Frank
     *    \version 1.0
     */
    class action_counters_t {
    public:
      long num_request           = 0;
      long num_success           = 0;
      long num_error             = 0;
      long object_tasks          = 0;
      long task_objects          = 0;
      long task_object           = 0;
      long task_object_directory = 0;
      long task_objects_regex    = 0;
    } m_counters, m_histograms;

    class call_context_t;

    bool                       debug              { true };
    //server_t*                  _rpc_server        { nullptr };
    std::unique_ptr<server_t>  _rpc_server        { };
    std::regex                 _histogram_match   { };
    std::regex                 _counter_match     { };
    std::mutex                 _message_id_lock   { };
    std::chrono::milliseconds  _rpc_timeout       { 1000 };
    int32_t                    _last_message_id   { 0 };

    std::mutex                 _output_lock       { }; 
    std::mutex                 _context_lock      { }; 
    call_context_t*            _current_context   { nullptr };
    rpc::HttpJsonRpcHandler&   _json_handler;

    /// Helper: Generate unique message identifiers
    int _new_message_id();
    /// Helper: Decode string arrays
    std::vector<std::string>           decode_names(const void* beg, const void* end)   const;
    /// Helper: Decode object maps
    std::map<std::string, const void*> decode_objects(const void* beg, const void* end)   const;
    /// Helper: Check dns string for validity
    static bool _check_dns(server_adapter* rpc, const std::string& dns);
    /// Helper: Check dns and task string for validity
    static bool _check_dns_task(server_adapter* rpc, const std::string& dns, const std::string& task);
    /// Helper: Access RPC service for a task on a given dns
    std::shared_ptr<server_t::rpc_t> get_rpc(const std::string& dns, const std::string& task, const std::regex& match);
    /// Helper: Generic RPC call wrapper
    bool do_call(std::shared_ptr<server_t::rpc_t>& server_rpc, std::shared_ptr<call_context_t>& ctx );

  public:
    /// Initializing constructor
    server_adapter(rpc::HttpJsonRpcHandler& handler);
    /// Default destructor
    virtual ~server_adapter();
    
    /// Server adapter overload: response callback
    virtual void on_response(rpc_t* rpc_ptr, const void* data, size_t len)  override;
    /// Server adapter overload: no-link callback
    virtual void on_nolink(rpc_t* rpc_ptr)   override;
    /// Server adapter overload: callback on connection to RPC
    virtual void on_connect_rpc(rpc_t* rpc_ptr)   override;
    /// Server adapter overload: callback on disconnection of the RPC
    virtual void on_disconnect_rpc(rpc_t* /* rpc_ptr */)  override;

    /// Create unique message context
    std::shared_ptr<call_context_t> create_context(server_t::rpc_t* rpc, int typ, size_t data_length);

    /// Access matching tasks by regex of a given dns
    json _taskservice_matches(const std::string& dns, const std::regex& match);
    /// Emit the directory data of a specified task according to a given match
    json _task_directory(const std::string& dns, const std::string& task, const std::regex& match);

    /// Access counter statistics
    json _counter_statistics()  const;
    /// Access counter statistics
    json _histogram_statistics()  const;
    /// Output server statistics
    json _print_statistics()  const;
    
    /// Access matching counter tasks of a given dns
    json counter_tasks(const std::string& dns);
    /// Emit the counter directory of a specified task
    json task_counter_directory(const std::string& dns, const std::string& task);
    /// Access counter list by regular expression
    json task_counter(const std::string& dns, const std::string& task, const std::string& selection);
    /// Access all taskcounter
    json task_counters(const std::string& dns, const std::string& task);
    /// Access counter list by regular expression
    json task_counters_regex(const std::string& dns, const std::string& task, const std::string& selection);
    
    /// Access matching histogram tasks of a given dns
    json histogram_tasks(const std::string& dns);
    /// Emit the histogram directory of a specified task
    json task_histogram_directory(const std::string& dns, const std::string& task);
    /// Access all task histograms
    json task_histograms(const std::string& dns, const std::string& task);
    /// Access histogram list by regular expression
    json task_histogram(const std::string& dns, const std::string& task, const std::string& selection);
    /// Access histogram list by regular expression
    json task_histograms_regex(const std::string& dns, const std::string& task, const std::string& selection);
  };
}      // End namespace gaucho
#endif // GAUCHOSERVER_GAUCHO_SERVER_ADAPTER_H
