//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//=========================================================================
//
//  Author     : M.Frank
//  Created    : 05/4/2022
//
//=========================================================================
#ifndef GAUCHOSERVER_GAUCHO_IOC_SERVER_ADAPTER_H
#define GAUCHOSERVER_GAUCHO_IOC_SERVER_ADAPTER_H

// Framework includes
#include <GauchoServer/gaucho_adapter.h>
#include <CPP/Interactor.h>

// C++ include files
#include <iostream>

/// gaucho namespace declaration
namespace gaucho   {

  /// Definition of the server adapter class
  /**
   *  Instance can handle IOC,Time, etc. events
   *
   *    \author  M.Frank
   *    \version 1.0
   */
  class ioc_server_adapter : public CPP::Interactor, public server_adapter   {
  public:
    enum { CMD_CONNECT = 1, CMD_REMOVE = 2, CMD_GETCOUNTERS = 3 };

  public:
    using server_adapter::server_adapter;
    /// Helper: Load counter directory
    void get_counters(rpc_t* rpc_ptr);
    /// Interactor overload: interrupt handler callback
    void handle(const Event& event)  override;
  };
}      // End namespace gaucho
#endif // GAUCHOSERVER_GAUCHO_IOC_SERVER_ADAPTER_H

