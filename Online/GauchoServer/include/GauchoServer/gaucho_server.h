//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//=========================================================================
//
//  Author     : M.Frank
//  Created    : 05/4/2022
//
//=========================================================================
#ifndef GAUCHOSERVER_GAUCHO_SERVER_H
#define GAUCHOSERVER_GAUCHO_SERVER_H

/// Framework includes

/// C/C++ include files
#include <memory>
#include <string>

/// gaucho namespace declaration
namespace gaucho {

  /// Definition of the server class to be exported to python
  /**
   *
   *    \author  M.Frank
   *    \version 1.0
   */
  class gaucho_server   {
  private:
    /// Internal implementation
    class internals_t;
    std::shared_ptr<internals_t> internals;

  public:
    /// Default constructor
    gaucho_server();
    /// Copy constructor
    gaucho_server(const gaucho_server& copy) = default;
    /// Move constructor
    gaucho_server(gaucho_server&& copy) = default;
    /// Default destructor
    virtual ~gaucho_server();
    
    /// Print help
    void help();
    /// Configure the server object
    int set(const std::string& what, const std::string& value = "");
    /// Configure the server object
    void configure();
    /// Configure the server object
    void configure(int argc, char** argv);
    /// Stop the server
    int stop();
    /// Run the server
    virtual int run(bool detached = false);
  };
}      // End namespace gaucho
#endif // GAUCHOSERVER_GAUCHO_SERVER_H
