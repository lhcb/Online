#===============================================================================
#  LHCb Online software suite
#-------------------------------------------------------------------------------
# Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
# All rights reserved.
#
# For the licensing terms see OnlineSys/LICENSE.
#
# Author     : M.Frank
#
#===============================================================================
#
#  Module initialization script for dataflow
#
#  \author   M.Frank
#  \version  1.0
#  \date     27/03/2018
#
#===============================================================================
from __future__ import absolute_import, unicode_literals
from builtins import str
from builtins import object
import logging

global DEBUG_FLAG, SERVER_PORT, SERVER_MOUNT, NUM_THREADS, DNS, CACHE_TIMEOUT, CLIENT_SILENT
DNS           = 'mon01'
DEBUG_FLAG    = 0
SERVER_PORT   = 8100
SERVER_ADDR   = '0.0.0.0'
SERVER_HOST   = None
SERVER_MOUNT  = '/JSONRPC'
NUM_THREADS   = 5
CACHE_TIMEOUT = 5000
CLIENT_SILENT = 2

logger = logging.getLogger(__name__)

#===============================================================================
def output(msg):
  if not CLIENT_SILENT > 1:
    logger.info(msg)
    
#===============================================================================
def error(msg):
  if not CLIENT_SILENT:
    logger.info(msg)
    
#===============================================================================
def create_client(host=None):
  import socket
  import rpc_relay
  if host is None:
    host = socket.gethostname()
  client = rpc_relay.client_t.create(host, SERVER_MOUNT, SERVER_PORT)
  return client

#===============================================================================
def show_task_counters(client, task):
  output('+++ Show counters for ask: %s'%(task, ))

  try:
    entries = client.task_counter_directory(DNS, task)
  except KeyboardInterrupt as i:
    raise(i)
  except Exception as e:
    error('------ ERROR Failed to access counter directory of process: %s [%s]' % (task, str(e), ))
    return

  try:
    counters = client.task_counters(DNS, task)
    for counter_name in counters:
      counter = client.task_counter(DNS, task, counter_name.name)
      output('Task: %-32s : %s' % (task, counter.to_string(), ))
  except KeyboardInterrupt as i:
    raise(i)
  except Exception as e:
    error('------ ERROR Failed to access counters of process: %s [%s]' % (task, str(e), ))

  try:
    counter = client.task_counter(DNS, task, 'Events/IN')
    output('+++++ Task: %-32s : %s' % (task, counter.to_string(), ))
    counters = client.task_counters_regex(DNS, task, 'Events/IN')
    for counter in counters:
      output('ooooo Task: %-32s : %s' % (task, counter.to_string(), ))
  except KeyboardInterrupt as i:
    raise(i)
  except Exception as e:
    error(str(e))

#===============================================================================
def show_task_histograms(client, task):
  import random
  output('+++ Show histograms for task: %s'%(task, ))
  try:
    entries = client.task_histogram_directory(DNS, task)
    output('+++ Task: %s   has %d histograms:'%(task, entries.size(), ))
  except KeyboardInterrupt as i:
    raise(i)
  except Exception as e:
    error('------ ERROR Failed to access histogram directory of process: %s [%s]'%(task, str(e), ))
    return

  try:
    for hist_name in entries:
      if random.randrange(100) >= 50:
        histo = client.task_histogram('mon01', task, hist_name)
        output('task_histogram:        %-32s : %s'%(task, histo.to_string(), ))
      else:
        histo = client.task_histograms_regex('mon01', task, hist_name)
        output('task_histograms_regex: %-32s : %d histos'%(task, histo.size(), ))

  except KeyboardInterrupt as i:
    raise(i)
  except Exception as e:
    error('------ ERROR Failed to access histograns of process: %s [%s]'%(task, str(e), ))

#===============================================================================
class ClientCounters:
  def __init__(self, client):
    self.client = client
    self.count  = 0;

  def check(self):
    cnt = self.client.call_count()
    if (cnt - self.count) > 3000:
      logger.info('+++ Executed %d calls to RPC server.' % (cnt, ))
      self.client.output_server_stats()
      self.count = cnt
      return True
    return False

#===============================================================================
def loop_counters(client):
  import random
  processes = client.counter_tasks(DNS)
  counts = ClientCounters(client)
  while 1:
    try:
      index = int((random.randrange(processes.size()*100))/100)
      task  = processes[index]
      entries = client.task_counter_directory(DNS, task)
      show_task_counters(client, task)
      # Histograms are expensive. call them only for a fraction of the tasks
      if random.randrange(100) > 75:
        index = int((random.randrange(processes.size()*100))/100)
        task  = processes[index]
        entries = client.task_counter_directory(DNS, task)
        show_task_histograms(client, task)

      if counts.check():
        processes = client.counter_tasks(DNS)

    except KeyboardInterrupt:
      logger.info('Key Interrupt.....')
      break
    except Exception as e:
      output(str(e))
      processes = client.counter_tasks(DNS)

#===============================================================================
def run_client(host=None):
  """
  Usage:
  $> python -c "import rpc_relay.tests; rpc_relay.tests.run_client()"

  """
  if host is None:
    host = SERVER_HOST
  client = create_client(host)
  loop_counters(client)

#===============================================================================
def run_server():
  """
  Usage:
  $> python -c "import rpc_relay.tests; rpc_relay.tests.run_server()"

  """
  import rpc_relay
  srv = rpc_relay.rpc_server()
  srv.debug   = DEBUG_FLAG
  srv.port    = SERVER_PORT
  srv.host    = SERVER_ADDR
  srv.mount   = SERVER_MOUNT
  srv.threads = NUM_THREADS
  srv.dns     = DNS
  srv.cache   = CACHE_TIMEOUT
  logger.info("+++ Configure and execute Gaucho RPC server on %s:%d%s with DNS:%s  [%d threads, cache tmo:%d]" % 
              (SERVER_ADDR, SERVER_PORT, SERVER_MOUNT, DNS, NUM_THREADS, CACHE_TIMEOUT, ))
  srv.configure()
  srv.run()


#===============================================================================
# Start from the command line
if __name__ == '__main__':
  import optparse
  import socket
  parser = optparse.OptionParser()
  parser.description = 'Run Gaucho RPC server/client.'
  parser.formatter.width = 132
  #
  #  Debug options
  parser.add_option('-D', '--debug', 
                    dest='debug',
                    default=DEBUG_FLAG,
                    help='Server option: Enable output debug statements',
                    metavar='<integer>')
  parser.add_option('-P', '--pdb',
                    action='store_true',
                    dest='start_pdb',
                    default=False,
                    help='Start pdb for debugging',
                    metavar='<boolean>')
  parser.add_option('-c', '--cache', 
                    dest='cache',
                    default=CACHE_TIMEOUT,
                    help='Server option: Run cache server version. Timeout in milliseconds',
                    metavar='<integer>')
  parser.add_option('-t', '--threads', 
                    dest='threads',
                    default=NUM_THREADS,
                    help='Server option: Run in threaded mode. Supply number of workers.',
                    metavar='<integer>')

  parser.add_option('-p', '--port',
                    dest='port',
                    default=SERVER_PORT,
                    help='Client+Server option: Server port number',
                    metavar='<integer>')
  parser.add_option('-m', '--mount',
                    dest='mount',
                    default=SERVER_MOUNT,
                    help='Client+Server option: Server RPC mount point',
                    metavar='<integer>')
  parser.add_option('-d', '--dns',
                    dest='dns',
                    default=socket.gethostname(),
                    help='Client+Server option: DNS name',
                    metavar='<string>')

  parser.add_option('-C', '--client',
                    action='store_true',
                    dest='run_client',
                    default=False,
                    help='Client option: Run the client instance',
                    metavar='<boolean>')
  parser.add_option('-s', '--server',
                    dest='server',
                    default=socket.gethostname(),
                    help='Client option: Server name',
                    metavar='<string>')
  parser.add_option('-v', '--verbose',
                    dest='silent',
                    default=2,
                    help='Client option: Run the client instance in verbose mode',
                    metavar='<integer>')

  (opts, args) = parser.parse_args()
  DNS            = opts.dns
  DEBUG_FLAG     = opts.debug
  SERVER_PORT    = int(opts.port)
  SERVER_HOST    = opts.server
  SERVER_MOUNT   = opts.mount
  NUM_THREADS    = int(opts.threads)
  CACHE_TIMEOUT  = int(opts.cache)
  CLIENT_SILENT  = int(opts.silent)

  if opts.start_pdb:
    import pdb
    pdb.set_trace()

  if opts.run_client:
    run_client()
  else:
    run_server()

