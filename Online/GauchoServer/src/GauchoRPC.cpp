//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================
#ifndef ONLINE_RPC_GAUCHORPC_H
#define ONLINE_RPC_GAUCHORPC_H 1

// Framework include files
#include <RPC/JSONRPC.h>

// C++ include files
#include <set>
#include <mutex>
#include <string>
#include <vector>
#include <filesystem>

/*
 *   Online namespace declaration
 */
namespace Online {

  /**@class GauchoRPC GauchoRPC.h GaudiOnline/GauchoRPC.h
   *
   * Readout monitor DIM server for a single node
   *
   * @author M.Frank
   */
  class GauchoRPC {
  public:
    typedef nlohmann::json json;
    struct {
      long numRequest     = 0;
      long numSuccess     = 0;
      long numError       = 0;
      long numCacheDrop   = 0;
      long numCacheInsert = 0;
      long numCacheReuse  = 0;
    } m_counters;
    enum  request_type_t  {
      COUNTER_TASKS,
      TASK_COUNTER_DIRECTORY,
      TASK_COUNTER,
      TASK_COUNTERS,
      TASK_COUNTERS_REGEX,
      HISTOGRAM_TASKS,
      TASK_HISTOGRAM_DIRECTORY,
      TASK_HISTOGRAM,
      TASK_REGEX_HISTOGRAM,
      TASK_HISTOGRAMS_REGEX,
      LIST_HISTOGRAM_PAGES,
      LOAD_HISTOGRAM_PAGE
    };

    std::mutex            dim_rpc_lock;
    std::filesystem::path pages_location;
    int                   debug = 0;

  public:
    /// Standard constructor with initialization
    GauchoRPC() = default;
    /// Default destructor
    virtual ~GauchoRPC() = default;
    /// Access counter directory of a given task in a dns
    json counter_tasks(const std::string& dns);
    /// Access counter directory of a given task in a dns
    json task_counter_directory(const std::string& dns, const std::string& task);
    /// Access counter list by regular expression
    json task_counter(const std::string& dns, const std::string& task, const std::string& selection);
    /// Access all taskcounter
    json task_counters(const std::string& dns, const std::string& task);
    /// Access counter list by regular expression
    json task_counters_regex(const std::string& dns, const std::string& task, const std::string& selection);

    /// Access counter directory of a given task in a dns
    json histogram_tasks(const std::string& dns);
    /// Access histogram directory of a given task in a dns
    json task_histogram_directory(const std::string& dns, const std::string& task);
    /// Access histogram list by regular expression
    json task_histogram(const std::string& dns, const std::string& task, const std::string& selection);
    /// Access histogram list by regular expression
    json task_regex_histogram(const std::string& dns, const std::string& task, const std::string& selection);
    /// Access histogram list by regular expression
    json task_histograms_regex(const std::string& dns, const std::string& task, const std::string& selection);

    /// List histogram pages for implementation of the presenter
    json list_histogram_pages();
    /// Load single histogram page identified by the relative path
    json load_histogram_page(const std::string& path);
    /// Save single histogram page identified by the relative path
    json save_histogram_page(const std::string& path, const std::string& data);
    /// Update single histogram page identified by the relative path
    json update_histogram_page(const std::string& path, const std::string& data);
    /// Delete/remove single histogram page identified by the relative path
    json remove_histogram_page(const std::string& path);
  };
}      // End namespace Online
#endif /* ONLINE_RPC_GAUCHORPC_H */

//====================================================================
//  Online
//--------------------------------------------------------------------
//
//  Package    : Online
//
//  Description: Readout monitoring in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//====================================================================

// C++ include files
#include <iostream>
#include <stdexcept>
#include <fstream>
#include <memory>

// Framework includes
#include <RTL/Logger.h>
//#include <Online/GauchoRPC.h>
#include <Gaucho/HistTaskJson.h>
#include <Gaucho/CounterTaskJson.h>

#include <algorithm>
#include <memory>
#include <thread>

using namespace Online;
using namespace std;
#define protocol jsonrpc

namespace {
  struct StringCheck   {
    const string& match;
    StringCheck(const string& m) : match(m) {}
  };
  struct RegexCheck : public StringCheck {
    RegexCheck(const string& m) : StringCheck(m)  {}
    bool operator()(const std::string& n) const  { return n==match; }
  };
  struct NameCheck : public StringCheck {
    NameCheck(const string& m) : StringCheck(m)  {}
    bool operator()(const std::string& n) const  { return n==match; }
  };

  bool _check_dns_task(GauchoRPC* rpc, const std::string& dns, const std::string& task)   {
    ++rpc->m_counters.numRequest;
    if ( dns.empty() )    {
      ++rpc->m_counters.numError;
      return false;
    }
    if ( task.empty() )    {
      ++rpc->m_counters.numError;
      return false;
    }
    return true;
  }
  /// Check dns string for validity
  bool _check_dns(GauchoRPC* rpc, const std::string& dns)   {
    ++rpc->m_counters.numRequest;
    if ( dns.empty() )    {
      ++rpc->m_counters.numError;
      return false;
    }
    return true;
  }
}

GauchoRPC::json GauchoRPC::counter_tasks(const string& dns)  {
  json answer;
  if ( _check_dns(this, dns) )   {
    // std::lock_guard<std::mutex> loc(dim_rpc_lock);
    answer = CounterTaskJson::taskList(dns);
    ++m_counters.numSuccess;
  }
  return answer;
}

GauchoRPC::json GauchoRPC::task_counter_directory(const string& dns, const string& task)  {
  json answer;
  if ( _check_dns_task(this, dns, task) )   {
    // std::lock_guard<std::mutex> loc(dim_rpc_lock);
    CounterTaskJson obj(task, dns);
    answer = obj.counter_directory();
  }
  ++m_counters.numSuccess;
  return answer;
}

/// Access histogram list by regular expression
GauchoRPC::json GauchoRPC::task_counter(const std::string& dns, const std::string& task, const std::string& selection)   {
  json answer;
  if ( _check_dns_task(this, dns, task) )   {
    // std::lock_guard<std::mutex> loc(dim_rpc_lock);
    CounterTaskJson obj(task, dns);
    answer = obj.counters(selection);
    if ( answer.size() > 0 )   {
      ++m_counters.numSuccess;
      return answer.at(0);
    }
    ++m_counters.numError;
    return {{"error", EINVAL},
	{"message", "No counter found: <br>task_counter('"+dns+"','"+task+","+selection+"')"}};
  }
  return {{"error", EINVAL},
      {"message", "Invalid arguments to call <br>task_counter('"+dns+"','"+task+","+selection+"')"}};
}

/// Access all taskcounter
GauchoRPC::json GauchoRPC::task_counters(const std::string& dns, const std::string& task)   {
  if ( _check_dns_task(this, dns, task) )   {
    // std::lock_guard<std::mutex> loc(dim_rpc_lock);
    CounterTaskJson obj(task, dns);
    json answer = obj.counters();
    if ( answer.size() > 0 )   {
      ++m_counters.numSuccess;
      return answer;
    }
    ++m_counters.numError;
    return {{"error", EINVAL},
	{"message", "No counters found: <br>task_counters('"+dns+"','"+task+"')"}};
  }
  return {{"error", EINVAL},
      {"message", "Invalid arguments to call <br>task_counters('"+dns+"','"+task+"')"}};
}

/// Access counter list by regular expression
GauchoRPC::json GauchoRPC::task_counters_regex(const std::string& dns, const std::string& task, const std::string& selection)   {
  if ( _check_dns_task(this, dns, task) )   {
    // std::lock_guard<std::mutex> loc(dim_rpc_lock);
    CounterTaskJson obj(task, dns);
    json answer = obj.counters(selection);
    if ( answer.size() > 0 )   {
      ++m_counters.numSuccess;
      return answer;
    }
    ++m_counters.numError;
    return {{"error", EINVAL},
	{"message", "No counters found: <br>task_counters_regex('"+dns+"','"+task+","+selection+"')"}};
  }
  return {{"error", EINVAL},
      {"message", "Invalid arguments to call task_counters_regex("+dns+","+task+","+selection+")"}};
}

/// Access counter directory of a given task in a dns
GauchoRPC::json GauchoRPC::histogram_tasks(const std::string& dns)   {
  if ( _check_dns(this, dns) )   {
    // std::lock_guard<std::mutex> loc(dim_rpc_lock);
    json answer = HistTaskJson::taskList(dns);
    ++m_counters.numSuccess;
    return answer;
  }
  return {{"error", EINVAL},
      {"message", "Invalid arguments to call histogram_tasks("+dns+")"}};
}

/// Access histogram directory of a given task in a dns
GauchoRPC::json GauchoRPC::task_histogram_directory(const std::string& dns, const std::string& task)   {
  if ( _check_dns_task(this, dns, task) )   {
    // std::lock_guard<std::mutex> loc(dim_rpc_lock);
    HistTaskJson obj(task, dns);
    json answer = obj.histo_directory();
    ++m_counters.numSuccess;
    return answer;
  }
  return {{"error", EINVAL},
      {"message", "Invalid arguments to call task_histogram_directory("+dns+","+task+")"}};
}

/// Access histogram list by regular expression
GauchoRPC::json GauchoRPC::task_histograms_regex(const std::string& dns, const std::string& task, const std::string& selection)   {
  if ( _check_dns_task(this, dns, task) )   {
    // std::lock_guard<std::mutex> loc(dim_rpc_lock);
    HistTaskJson obj(task, dns);
    json answer = obj.histos(selection);
    ++m_counters.numSuccess;
    if ( answer.size() > 0 )   {
      return answer;
    }
    ++m_counters.numError;
    return {{"error", EINVAL},
	{"message", "No histograms found: <br>task_histograms_regex("+dns+","+task+","+selection+")"}};
  }
  return {{"error", EINVAL},
      {"message", "Invalid arguments to call <br>task_histograms_regex("+dns+","+task+","+selection+")"}};
}

/// Access histogram list by regular expression
GauchoRPC::json GauchoRPC::task_histogram(const std::string& dns, const std::string& task, const std::string& selection)   {
  if ( _check_dns_task(this, dns, task) )   {
    // std::lock_guard<std::mutex> loc(dim_rpc_lock);
    HistTaskJson obj(task, dns);
    json answer = obj.histos(selection);
    if ( answer.size() > 0 )   {
      ++m_counters.numSuccess;
      return answer.at(0);
    }
    ++m_counters.numError;
    return {{"error", EINVAL},
      {"message", "No histograms found: <br>task_histogram("+dns+","+task+","+selection+")"}};
  }
  return {{"error", EINVAL},
      {"message", "Invalid arguments to call <br>task_histogram("+dns+","+task+","+selection+")"}};
}

/// Access histogram list by regular expression
GauchoRPC::json GauchoRPC::task_regex_histogram(const std::string& dns, const std::string& task, const std::string& selection)   {
  std::vector<std::string> tasks;
  _check_dns_task(this, dns, task);
  // std::lock_guard<std::mutex> loc(dim_rpc_lock);
  HistTaskJson::taskList(dns, tasks);
  auto flags = std::regex_constants::icase;
  std::regex regex(task, flags);

  for( const auto& t : tasks )   {
    std::smatch sm;
    bool stat = std::regex_match(t, sm, regex);
    if ( stat )    {
      json answer;      {
	// std::lock_guard<std::mutex> loc(dim_rpc_lock);
	HistTaskJson obj(t, dns);
	answer = obj.histos(selection);
      }
      if ( answer.size() > 0 )   {
	json&  hist = answer.at(0);
	++m_counters.numSuccess;
	return hist;
      }
      ++m_counters.numError;
      return {{"error", EINVAL },
	  {"message", "No histograms found: <br>task_regex_histogram("+dns+","+task+","+selection+")"}};
    }
  }
  return {{"error", EINVAL}, 
      {"message", 
	  "No matching histogram to call <br>task_regex_histogram('"+dns+"','"+task+"','"+selection+"')"}};
}

/// List histogram pages for implementation of the presenter
GauchoRPC::json GauchoRPC::list_histogram_pages()    {
  struct Scanner  {
    static json scan(GauchoRPC* parent, const std::filesystem::directory_entry& entry)     {
      if ( entry.exists() )   {
	std::filesystem::path p = std::filesystem::relative(entry.path(), parent->pages_location.parent_path());
	auto name = p.filename().string();
	if ( entry.is_directory() )   {
	  json leafs = json::array();
	  for(auto const& e : std::filesystem::directory_iterator{entry.path()})
	    leafs.emplace_back(scan(parent, e));
	  return { {"name", name}, {"topic", leafs} };
	}
	else if ( entry.is_symlink() )   {
	  std::filesystem::path link = std::filesystem::read_symlink(entry.path());
	  json result = scan(parent, std::filesystem::directory_entry(link));
	  result["name"] = name;
	  return result;
	}
	else if ( entry.is_regular_file() )   {
	  return { {"name", name }, {"page", name }};
	}
      }
      return {};
    }
  };
  std::filesystem::directory_entry entry(pages_location);
  json obj = Scanner::scan(this, entry);
  //std::cout << obj << std::endl;
  return obj;
}

/// Load single histogram page identified by the relative path
GauchoRPC::json GauchoRPC::load_histogram_page(const std::string& path)    {
  std::filesystem::path loc(pages_location.string()+path.substr(path.find("/",1)));
  std::filesystem::directory_entry entry(loc);
  std::cout << loc << std::endl;
  if ( entry.exists() )   {
    auto len = entry.file_size();
    std::string   str(len, '\0');
    std::ifstream in(loc.string(), std::ios::binary|std::ios::in);
    in.read(&str[0], len);
    return json::parse(str);
  }
  return {{"error", EINVAL}, {"message", "No histogram page found: <br>"+path+")"}};
}

/// Save single histogram page identified by the relative path
GauchoRPC::json GauchoRPC::save_histogram_page(const std::string& path, const std::string& data)    {
  std::error_code ec;
  ec.assign(EINVAL, std::system_category());
  if ( path.empty() )   {
    return { {"result", ec.value() }, {"message", ec.message() }};
  }
  else if ( path.find("..") != std::string::npos )   { // Need to ensure there are no invalid sniffers
    return { {"result", ec.value() }, {"message", ec.message() }};
  }
  else   {
    std::filesystem::path loc(pages_location.string()+path.substr(path.find("/",1)));
    std::cout << loc << std::endl;
    if ( std::filesystem::exists(loc, ec) )   {
      ec.assign(EEXIST, std::system_category());
      return { {"result", ec.value() }, {"message", ec.message() }};
    }
    std::filesystem::path parent = loc.parent_path();
    if ( !std::filesystem::exists(parent, ec) )   {
      if ( !std::filesystem::create_directories(parent, ec) )   {
	return { {"result", ec.value() }, {"message", ec.message() }};
      }
    }
    std::ofstream out(loc.string(), std::ios::out);
    if ( out.is_open() )  {
      out << data;
      if ( out.good() )   {
	out.close();
	return { {"result", 0 }, {"message", "Page successfully saved" }};
      }
      ec.assign(errno, std::system_category());
      return { {"result", ec.value() }, {"message", ec.message() }};
    }
    ec.assign(EPERM, std::system_category());
    return { {"result", ec.value() }, {"message", ec.message() }};
  }
  ec.assign(EINVAL, std::system_category());
  return { {"result", ec.value() }, {"message", ec.message() }};
}

/// Update single histogram page identified by the relative path
GauchoRPC::json GauchoRPC::update_histogram_page(const std::string& path, const std::string& data)    {
  std::error_code ec;
  ec.assign(EINVAL, std::system_category());
  if ( path.empty() )   {
    return { {"result", ec.value() }, {"message", ec.message() }};
  }
  else if ( path.find("..") != std::string::npos )   { // Need to ensure there are no invalid sniffers
    return { {"result", ec.value() }, {"message", ec.message() }};
  }
  else   {
    std::filesystem::path loc(pages_location.string()+path.substr(path.find("/",1)));
    std::cout << loc << std::endl;
    if ( std::filesystem::exists(loc, ec) )   {
      std::ofstream out(loc.string(), std::ios::out);
      if ( out.is_open() )  {
	out << data;
	if ( out.good() )   {
	  out.close();
	  return { {"result", 0 }, {"message", "Page successfully updated" }};
	}
      }
      ec.assign(errno, std::system_category());
      return { {"result", ec.value() }, {"message", ec.message() }};
    }
    ec.assign(EPERM, std::system_category());
    return { {"result", ec.value() }, {"message", ec.message() }};
  }
  ec.assign(EINVAL, std::system_category());
  return { {"result", ec.value() }, {"message", ec.message() }};
}

/// Delete single histogram page identified by the relative path
GauchoRPC::json GauchoRPC::remove_histogram_page(const std::string& path)    {
  std::error_code ec;
  ec.assign(EINVAL, std::system_category());
  if ( path.empty() )   {
    return { {"result", ec.value() }, {"message", ec.message() }};
  }
  else if ( path.find("..") != std::string::npos )   { // Need to ensure there are no invalid sniffers
    return { {"result", ec.value() }, {"message", ec.message() }};
  }
  else   {
    std::filesystem::path loc(pages_location.string()+path.substr(path.find("/",1)));
    if ( !std::filesystem::exists(loc, ec) )   {
      ec.assign(ENOENT, std::system_category());
      return { {"result", ec.value() }, {"message", ec.message() }};
    }
    if ( std::filesystem::remove(loc, ec) )   {
      return { {"result", 0 }, {"message", "Histogram page definition successfule removed." }};
    }
    return { {"result", ec.value() }, {"message", ec.message() }};
  }
  return { {"result", ec.value() }, {"message", ec.message() }};
}

//=========================================================================
#include <XML/Printout.h>
#include <RPC/HttpRpcHandler.h>
#include <RPC/HttpJsonRpcHandler.h>
#include <HTTP/HttpCacheCheck.h>
#include <HTTP/HttpFileHandler.h>

//=========================================================================
static void help_server(const char* msg = 0, const char* argv = 0) {
  std::cout << "run_gaucho_rpc -opt <value> [ -opt <value> ]                                " << std::endl
	    << "     -port    <number>    Listening port (default: 8000)                    " << std::endl
	    << "     -threads <number>    Number of *additional* worker threads (default: 0)" << std::endl
	    << "     -debug               Enable debug flag (default: off)                  " << std::endl
	    << "     -cache <number>      Enable cache with timeout in seconds.             " << std::endl
	    << "     -help                Print this help.                                  " << std::endl;
  if ( msg )
    std::cout << "Error cause:    " << msg << std::endl;
  if ( argv )
    std::cout << "Argument given: " << argv << std::endl;
  std::cout << std::endl;
  ::exit(EINVAL);
}

//=========================================================================
/// Main entry point to start the application
extern "C" int run_gaucho_rpc(int argc, char** argv) {
  int cache   = 0;
  int debug   = 0;
  int threads = 0;
  int port    = 8000;
  std::string host = "0.0.0.0";
  for(int i = 1; i < argc && argv[i]; ++i)  {
    if ( 0 == std::strncmp("-port",argv[i],4) )
      port = ::atol(argv[++i]);
    else if ( 0 == std::strncmp("-host",argv[i],4) )
      host = (++i < argc) ? argv[++i] : "localhost";
    else if ( 0 == std::strncmp("-threads",argv[i],3) )
      threads = (++i < argc) ? ::atol(argv[i]) : 2;
    else if ( 0 == std::strncmp("-debug",argv[i],4) )
      debug = (++i < argc) ? ::atol(argv[i]) : 3;
    else if ( 0 == std::strncmp("-cache",argv[i],4) )
      cache = (++i < argc) ? ::atol(argv[i]) : 30;
    else if ( 0 == std::strncmp("-help",argv[i],2) )
      help_server();
    else
      help_server("Invalid argument given", argv[i]);
  }

  GauchoRPC callable;
  printout(INFO,"HTTPSERVER","TEST> Starting test JSONRPC server with URI:%s:%d  threads:%d cache:%d debug:%d %s",
	   host.c_str(), port, threads, cache, debug, debug ? "YES" : "NO");
  callable.debug = (debug&4) != 0;

  auto json_handler = std::make_unique<rpc::RpcHandlerImp<rpc::HttpJsonRpcHandler> >();
  (*json_handler)->define("counter_tasks",            jsonrpc::Call(&callable).make(&GauchoRPC::counter_tasks));
  (*json_handler)->define("task_counter_directory",   jsonrpc::Call(&callable).make(&GauchoRPC::task_counter_directory));
  (*json_handler)->define("task_counters_regex",      jsonrpc::Call(&callable).make(&GauchoRPC::task_counters_regex));
  (*json_handler)->define("task_counter",             jsonrpc::Call(&callable).make(&GauchoRPC::task_counter));

  (*json_handler)->define("histogram_tasks",          jsonrpc::Call(&callable).make(&GauchoRPC::histogram_tasks));
  (*json_handler)->define("task_histogram_directory", jsonrpc::Call(&callable).make(&GauchoRPC::task_histogram_directory));
  (*json_handler)->define("task_histograms_regex",    jsonrpc::Call(&callable).make(&GauchoRPC::task_histograms_regex));
  (*json_handler)->define("task_histogram",           jsonrpc::Call(&callable).make(&GauchoRPC::task_histogram));
  (*json_handler)->define("task_regex_histogram",     jsonrpc::Call(&callable).make(&GauchoRPC::task_regex_histogram));

  callable.pages_location = "/home/frankm/Presenter/web/Pages";
  (*json_handler)->define("list_histogram_pages",     jsonrpc::Call(&callable).make(&GauchoRPC::list_histogram_pages));
  (*json_handler)->define("load_histogram_page",      jsonrpc::Call(&callable).make(&GauchoRPC::load_histogram_page));
  (*json_handler)->define("save_histogram_page",      jsonrpc::Call(&callable).make(&GauchoRPC::save_histogram_page));
  (*json_handler)->define("update_histogram_page",    jsonrpc::Call(&callable).make(&GauchoRPC::update_histogram_page));
  (*json_handler)->define("remove_histogram_page",    jsonrpc::Call(&callable).make(&GauchoRPC::remove_histogram_page));

  (*json_handler)->cache.timeout      = { cache, 0 };
  (*json_handler)->cache.enable       = (cache != 0);
  (*json_handler)->cache.debug        = (debug&2) != 0;
  (*json_handler)->cache.name         = "Json RPC";
  (*json_handler)->compression.enable = true;
  (*json_handler)->debug              = (debug&1) != 0;

  auto file_handler = std::make_unique<rpc::RpcHandlerImp<http::HttpFileHandler> >("/monitoring/FILES", "/home/frankm/Presenter/web");
  (*file_handler)->cache.timeout      = { 10000, 0 };
  (*file_handler)->cache.enable       = (cache != 0);
  (*file_handler)->cache.debug        = (debug&2) != 0;
  (*file_handler)->cache.name         = "FileServ";
  (*file_handler)->compression.enable = true;
  (*file_handler)->debug              = (debug&1) != 0;

  auto handler    = std::make_unique<rpc::HttpServer::Handler>();
  std::shared_ptr<http::HttpCacheCheck> json_clean, file_clean;
  if ( cache != 0 )  {
    json_clean = std::make_shared<http::HttpCacheCheck>(**json_handler, handler->io_context, 20);
    file_clean = std::make_shared<http::HttpCacheCheck>(**file_handler, handler->io_context, 200);
  }
  handler->mountPoints.handlers.emplace("/monitoring/FILES",   std::move(file_handler));
  handler->mountPoints.handlers.emplace("/monitoring/JSONRPC", std::move(json_handler));

  rpc::HttpServer server(std::move(handler), host, port, rpc::HttpServer::SERVER);
  server.setDebug((debug&1) != 0);
  ::dis_start_serving(RTL::processName().c_str());
  server.start(false, threads);
  return 0;
}

