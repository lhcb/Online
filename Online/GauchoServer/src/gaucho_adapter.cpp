//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//=========================================================================
//
//  Author     : M.Frank
//  Created    : 05/4/2022
//
//=========================================================================

// Framework includes
#include <GauchoServer/gaucho_adapter.h>
#include <Gaucho/CounterJson.h>
#include <Gaucho/HistJson.h>
#include <Gaucho/dimhist.h>
#include <Gaucho/RPCdefs.h>
#include <RPC/HttpJsonRpcHandler.h>
#include <RTL/strdef.h>
#include <RTL/rtl.h>

// C++ include files
#include <iostream>
#include <stdexcept>
#include <cerrno>

using namespace rpc_relay;

/// gaucho namespace declaration
namespace gaucho   {

  typedef const std::string c_str;

  /// String conversion of json data
  static std::string _to_string(const nlohmann::json& data)   {
    return data.dump(2);
  }

  /// Call context
  /**
   *
   *    \author  M.Frank
   *    \version 1.0
   */
  class server_adapter::call_context_t {
  public:
    unsigned int             magic      { MAGIC_PATTERN };
    int                      call_id    { 0 };
    int                      ret_id     { 0 };
    int                      ready      { 0 };
    int                      debug      { 0 };
    std::mutex               lock       { };
    std::condition_variable  condition  { };
    json                     answer     { };
    server_t::rpc_t*         rpc        { nullptr };
    void*                    request    { nullptr };
    size_t                   len        { 0 };

  public:
    /// Initializing constructor
    call_context_t(server_t::rpc_t* r, void* d, size_t l);
    /// Default destructor
    ~call_context_t();
    /// Access typed data
    template <typename T=void> T* data()  {  return (T*)this->request; }
    /// Callback handler when data ready
    template <typename DATA> static void on_ready(call_context_t* context, DATA&& data);
    /// Wait for response with timeout
    int wait_for(std::chrono::milliseconds timeout);
  };


  /// Format object to text
  std::string to_string(const server_adapter::action_counters_t& c)  {
    char text[1024];
    ::snprintf(text, sizeof(text), "Req:%10ld ERR:%11ld OK:%11ld tasks:%10ld objs:%10ld obj:%10ld dir:%10ld regex:%10ld",
	       c.num_request, c.num_error, c.num_success, c.object_tasks,
	       c.task_objects, c.task_object, c.task_object_directory, c.task_objects_regex);
    return text;
  }

  /// Format object to text
  std::string to_string(const http::HttpCacheHandler::stats_type& c)  {
    char text[1024];
    ::snprintf(text, sizeof(text), "INS:%10ld DROP:%10ld HIT:%10ld MISS:%11ld Upda:%10ld",
	       c.cache_insert, c.cache_drop, c.cache_hit, c.cache_miss, c.cache_update);
    return text;
  }
  
  /// Initializing constructor
  server_adapter::call_context_t::call_context_t(server_t::rpc_t* r, void* d, size_t l)
    : rpc(r), request(d), len(l)
  {
  }

  /// Default destructor
  server_adapter::call_context_t::~call_context_t()   {
    len = 0UL;
    rpc = nullptr;
    if ( request ) ::operator delete(request);
  }

  /// Wait for response with timeout
  int server_adapter::call_context_t::wait_for(std::chrono::milliseconds timeout)    {
    std::unique_lock<std::mutex> guard(this->lock);
    auto result = this->condition.wait_for(guard, timeout, [this] { return this->ready > 0; });
    this->magic = 0;
    return result;
  }

  /// Callback handler when data ready
  template <typename DATA> 
  void server_adapter::call_context_t::on_ready(call_context_t* context, DATA&& data)   {
    if ( context && context->magic == MAGIC_PATTERN )    {
      if ( context->debug )  {
	::lib_rtl_output(LIB_RTL_ALWAYS,"+++ on_ready: data: %s", _to_string(data).c_str());
      }
      context->answer = std::move(data);
      context->ready  = 1;
      context->condition.notify_one();
      return;
    }
    else if ( context )   {
      ::lib_rtl_output(LIB_RTL_ALWAYS,"+++ on_ready: Invalid magic pattern: %08X [%08X]",
		       context->magic, MAGIC_PATTERN);
    }
    else  {
      ::lib_rtl_output(LIB_RTL_ALWAYS,"+++ on_ready: Invalid call context.");
    }
  }

  /// Initializing constructor
  server_adapter::server_adapter(rpc::HttpJsonRpcHandler& handler) : _json_handler(handler)
  {
  }

  /// Default destructor
  server_adapter::~server_adapter()
  {
  }
  
  /* ============================================================================================ */
  /// Helper: Generic RPC call wrapper
  bool server_adapter::do_call(std::shared_ptr<server_t::rpc_t>& server_rpc, std::shared_ptr<call_context_t>& ctx )   {
    this->_current_context = ctx.get();
    server_rpc->call(ctx->data(), ctx->len);
    int stat = ctx->wait_for(this->_rpc_timeout);
    this->_current_context = nullptr;
    if ( stat )   {
      ::lib_rtl_output(debug ? LIB_RTL_ALWAYS : LIB_RTL_DEBUG,
		       "+++ do_call: call-id: %8d  [return-id: %8d]", ctx->call_id, ctx->ret_id);
      return true;
    }
    ::lib_rtl_output(LIB_RTL_ERROR,
		     "+++ do_call: Call sequence mismatch. call-id: %8d  [return-id: %8d]", ctx->call_id, ctx->ret_id);
    return false;
  }

  /// Helper: Decode string arrays
  std::vector<std::string> server_adapter::decode_names(const void* beg, const void* end)  const  {
    std::vector<std::string> names;
    while ( beg < end )     {
      int titoff = 4;
      const char *tptr = add_ptr<char>(beg, titoff);
      if ( !tptr ) break;
      names.push_back(tptr);
      int recl = 4 + ::strlen(tptr)+1;
      beg = add_ptr(beg,recl);
    }
    return names;
  }

  /// Helper: Decode object maps
  std::map<std::string, const void*> server_adapter::decode_objects(const void* beg, const void* end)   const  {
    std::map<std::string, const void*> objects;
    const auto *ptr = (const Online::DimHistbuff1*)beg;
    while ( ptr < end )     {
      objects.emplace(add_ptr<char>(ptr, ptr->nameoff), ptr);
      if ( ptr->reclen <= 0 ) break;
      ptr = add_ptr<Online::DimHistbuff1>(ptr,ptr->reclen);
    }
    return objects;
  }

  /// Helper: Check dns string for validity
  bool server_adapter::_check_dns(server_adapter* server_adapter, c_str& dns)  {
    ++server_adapter->m_counters.num_request;
    if ( dns.empty() )    {
      ++server_adapter->m_counters.num_error;
      return false;
    }
    return true;
  }

  /// Helper: Check dns and task string for validity
  bool server_adapter::_check_dns_task(server_adapter* server_adapter, c_str& dns, c_str& task)  {
    ++server_adapter->m_counters.num_request;
    if ( dns.empty() )    {
      ++server_adapter->m_counters.num_error;
      return false;
    }
    if ( task.empty() )    {
      ++server_adapter->m_counters.num_error;
      return false;
    }
    return true;
  }

  /// Helper: Generate unique message identifiers
  int server_adapter::_new_message_id()  {
    std::lock_guard<std::mutex> lock(this->_message_id_lock);
    ++this->_last_message_id;
    return this->_last_message_id;
  }

  /// Helper: Access RPC service for a task on a given dns
  std::shared_ptr<server_t::rpc_t> server_adapter::get_rpc(c_str& dns, c_str& task, const std::regex& match)  {
    if ( _check_dns_task(this, dns, task) )   {
      std::string rpc_name = "MON_" + task;
      auto server_rpc = this->_rpc_server->get_server_rpc(dns, rpc_name, match);
      return server_rpc;
    }
    return {};
  }

  /// Server adapter overload: no-link callback
  void server_adapter::on_nolink(rpc_t* rpc_ptr)     {
    ::lib_rtl_output(LIB_RTL_ALWAYS,"+++ NoLink from rpc service %-12s :: %-32s",
		     rpc_ptr->server->dns->name.c_str(), rpc_ptr->service.c_str());
  }

  /// Server adapter overload: callback on connection to RPC
  void server_adapter::on_connect_rpc(rpc_t* rpc_ptr)      {
    auto* s = rpc_ptr->server;
    ::lib_rtl_output(LIB_RTL_INFO,"%p +++ Add rpc service %-12s :: %-32s -> %s",
		     rpc_ptr, s->dns->name.c_str(), s->name.c_str(), rpc_ptr->service.c_str() );
  }

  /// Server adapter overload: callback on disconnection of the RPC
  void server_adapter::on_disconnect_rpc(rpc_t* /* rpc_ptr */)    {
  }

  /// Server adapter overload: response callback
  void server_adapter::on_response(rpc_t* rpc_ptr, const void* data, size_t len)  {
    if ( data && len )   {
      typedef  std::map<const char*, const void*> PTRMAP;
      int secs = 0, milli = 0;
      rpc_ptr->get_time_stamp(secs, milli);
      auto *rep = (const Online::RPCReplyCookie*)data;
      const void* valin  = data;
      const void* valend = add_ptr(valin, len);
      PTRMAP  hists;

      switch(rep->comm)    {
      case Online::RPCCRead:
      case Online::RPCCReadAll:
      case Online::RPCCReadRegex:
      case Online::RPCCClear:
      case Online::RPCCClearAll:    {
	const auto *ptr = add_ptr<Online::DimHistbuff1>(valin, sizeof(Online::RPCReply));
	while ( ptr < valend )    {
	  hists.emplace(add_ptr<char>(ptr, ptr->nameoff), ptr);
	  if ( ptr->reclen <= 0 ) break;
	  ptr = add_ptr<Online::DimHistbuff1>(ptr,ptr->reclen);
	}
	break;
      }

      case Online::RPCCDirectory:   {
	auto names = this->decode_names(add_ptr(valin, sizeof(Online::RPCReply)), valend);
	::lib_rtl_output(debug ? LIB_RTL_ALWAYS : LIB_RTL_DEBUG,
			 "+++ Got answer %d [RPCCDirectory]       from rpc service %-12s :: %-32s  %08d:%03d  %ld objects",
			 rep->id, rpc_ptr->server->dns->name.c_str(), rpc_ptr->service.c_str(), secs, milli, names.size());
	call_context_t::on_ready(this->_current_context, std::move(names));
	break;
      }

      case Online::RPCCDirectoryCookie:   {
	auto names = this->decode_names(add_ptr(valin, sizeof(Online::RPCReplyCookie)), valend);
	call_context_t* context = (call_context_t*)rep->cookie;
	::lib_rtl_output(debug ? LIB_RTL_ALWAYS : LIB_RTL_DEBUG,
			 "+++ Got answer %d [RPCCDirectoryCookie] from rpc service %-12s :: %-32s  %08d:%03d  %ld objects",
			 rep->id, rpc_ptr->server->dns->name.c_str(), rpc_ptr->service.c_str(), secs, milli, names.size());
	context->ret_id = rep->id;
	call_context_t::on_ready(context, std::move(names));
	break;
      }

      case Online::RPCCReadRegexCookie:   {
	std::vector<json> objs;
	auto objects = this->decode_objects(add_ptr(valin, sizeof(Online::RPCReplyCookie)), valend);
	for ( const auto& k : objects )    {
	  if ( rpc_ptr->rpc_type == COUNTER_CALL )    {
	    json o = Online::JsonCounterDeserialize::de_serialize(k.second);
	    objs.emplace_back(std::move(o));
	  }
	  else if ( rpc_ptr->rpc_type == HIST_CALL )    {
	    json o = Online::JsonHistDeserialize::de_serialize(k.second);
	    o["task"] = rpc_ptr->service;
	    objs.emplace_back(std::move(o));
	  }
	}
	json result;
	if ( rpc_ptr->rpc_type == COUNTER_CALL )
	  result = { {"task", rpc_ptr->service }, {"counters", objs} };
	else if ( rpc_ptr->rpc_type == HIST_CALL )
	  result = objs;

	call_context_t* context = (call_context_t*)rep->cookie;
	context->ret_id = rep->id;
	if ( this->debug )   {
	  ::lib_rtl_output(LIB_RTL_ALWAYS,
			   "+++ Got answer %d [RPCCReadRegexCookie] from rpc service %-12s :: %-32s  %08d:%03d  %ld objects",
			   rep->id, rpc_ptr->server->dns->name.c_str(), rpc_ptr->service.c_str(), secs, milli, objs.size());
	  ::lib_rtl_output(LIB_RTL_ALWAYS,"+++ %s", result.dump(2).c_str());
	}
	call_context_t::on_ready(context, std::move(result));
	break;
      }

      case Online::RPCCIllegal:	    {
	default:                    /// ERROR
	  ::lib_rtl_output(LIB_RTL_ALWAYS,"+++ ILLEGAL rpc call %-12s :: %-32s  %08d:%03d",
			   rpc_ptr->server->dns->name.c_str(), rpc_ptr->service.c_str(), secs, milli);
	  std::lock_guard<std::mutex> context_lock(this->_context_lock);
	  if ( this->_current_context )    {
	    call_context_t* context = this->_current_context;
	    context->answer = std::string("");
	    context->ready  = 1;
	    context->condition.notify_one();
	  }
	  return;
      }
      }
    }
  }

  /* ============================================================================================ */
  /// Create unique message context
  std::shared_ptr<server_adapter::call_context_t>
  server_adapter::create_context(server_t::rpc_t* r, int comm, size_t data_length)  {
    Online::RPCCommCookie* c = (Online::RPCCommCookie*)::operator new(data_length);
    auto ctx   = std::make_shared<call_context_t>(r, c, data_length);
    ctx->debug = this->debug;
    c->comm    = (Online::RPCCommType)comm;
    switch(comm)    {
    case Online::RPCCReadCookie:
    case Online::RPCCReadAllCookie:
    case Online::RPCCReadRegexCookie:
    case Online::RPCCClearCookie:
    case Online::RPCCClearAllCookie:
    case Online::RPCCDirectoryCookie:
      c->cookie = ctx.get();
      c->id = this->_new_message_id();
      ctx->call_id = c->id;
      break;
    default:
      ctx->call_id = this->_new_message_id();
      ctx->ret_id  = ctx->call_id;
      lib_rtl_output(LIB_RTL_ERROR,"%s > Invalid call context: %d", r->service.c_str(), comm);
      break;
    }
    return ctx;
  }

  /// Access matching tasks by regex of a given dns
  server_adapter::json server_adapter::_taskservice_matches(c_str& dns, const std::regex& match)  {
    json answer;
    if ( _check_dns(this, dns) )   {
      auto servers = this->_rpc_server->get_servers(dns, match);
      std::vector<std::string> tasks;
      tasks.reserve(servers.size());
      for( const auto* s : servers )
	tasks.push_back(s->name);
      answer = tasks;
      ++m_counters.num_success;
    }
    return answer;
  }

  /// Emit the directory data of a specified task according to a given match
  server_adapter::json server_adapter::_task_directory(c_str& dns, c_str& task, const std::regex& match)  {
    ::lib_rtl_output(debug ? LIB_RTL_ALWAYS : LIB_RTL_DEBUG,
		     "+++ Calling _task_directory( '%s', '%s' )", dns.c_str(), task.c_str());
    auto server_rpc = this->get_rpc(dns, task, match);
    if ( server_rpc )   {
      auto ctx = this->create_context(server_rpc.get(), Online::RPCCDirectoryCookie, sizeof(Online::RPCCommCookie));
      if ( this->do_call(server_rpc, ctx) )   {
	++m_counters.num_success;
	return ctx->answer;
      }
    }
    ++m_counters.num_error;
    return {{"error", EINVAL},
	    {"message", "Invalid arguments to call <br>task_directory('"+dns+"','"+task+"')"}};
  }

  /* ============================================================================================ */
  /// Access counter statistics
  server_adapter::json server_adapter::_counter_statistics()  const  {
    return {
      {"num_request",           m_counters.num_request},
      {"num_success",           m_counters.num_success},
      {"num_error",             m_counters.num_error},
      {"object_tasks",          m_counters.object_tasks},
      {"task_objects",          m_counters.task_objects},
      {"task_object",           m_counters.task_object},
      {"task_object_directory", m_counters.task_object_directory},
      {"task_objects_regex",    m_counters.task_objects_regex}
    };
  }
  
  /// Access counter statistics
  server_adapter::json server_adapter::_histogram_statistics()  const  {
    return {
      {"num_request",           m_histograms.num_request},
      {"num_success",           m_histograms.num_success},
      {"num_error",             m_histograms.num_error},
      {"object_tasks",          m_histograms.object_tasks},
      {"task_objects",          m_histograms.task_objects},
      {"task_object",           m_histograms.task_object},
      {"task_object_directory", m_histograms.task_object_directory},
      {"task_objects_regex",    m_histograms.task_objects_regex}
    };
  }

  /// Output server statistics
  server_adapter::json server_adapter::_print_statistics()  const  {
    ::lib_rtl_output(LIB_RTL_ALWAYS, "+++ Stats Counters: %s", to_string(m_counters).c_str());    
    ::lib_rtl_output(LIB_RTL_ALWAYS, "+++ Stats Histos:   %s", to_string(m_histograms).c_str());
    ::lib_rtl_output(LIB_RTL_ALWAYS, "+++ Stats Cache:    %s", to_string(_json_handler.cache_counters()).c_str());
    return {};
  }

  /* ============================================================================================ */
  /// Access matching counter tasks of a given dns
  server_adapter::json server_adapter::counter_tasks(c_str& dns)  {
    ++m_counters.object_tasks;
    ::lib_rtl_output(debug ? LIB_RTL_ALWAYS : LIB_RTL_DEBUG,
		     "+++ Calling counter_tasks( '%s' )", dns.c_str());
    return this->_taskservice_matches(dns, this->_counter_match);
  }

  /// Access all task counters
  server_adapter::json server_adapter::task_counters(c_str& dns, c_str& task)  {
    ++m_counters.task_objects;
    ::lib_rtl_output(debug ? LIB_RTL_ALWAYS : LIB_RTL_DEBUG,
		     "+++ Calling task_counters( '%s', '%s' )", dns.c_str(), task.c_str());
    return this->task_counters_regex(dns, task, ".(.*)");
  }

  /// Access counter list by regular expression
  server_adapter::json server_adapter::task_counter(c_str& dns, c_str& task, c_str& selection)  {
    json result;
    ++m_counters.task_object;
    result = this->task_counters_regex(dns, task, selection);
    if ( result.find("counters") != result.end() )   {
      json& counters = result["counters"];
      if ( counters.is_array() && counters.size() > 0 )   {
	return counters.at(0);
      }
      return {{"error", EINVAL},
   	      {"message", "No counter found: <br>task_counter('"+dns+"','"+task+"','"+selection+"')"}};
    }
    return result;
  }

  /// Emit the counter directory of a specified task
  server_adapter::json server_adapter::task_counter_directory(c_str& dns, c_str& task)  {
    ++m_counters.task_object_directory;
    return this->_task_directory(dns, task, this->_counter_match);
  }

  /// Access counter list by regular expression
  server_adapter::json server_adapter::task_counters_regex(c_str& dns, c_str& task, c_str& selection)  {
    ++m_counters.task_objects_regex;
    auto server_rpc = this->get_rpc(dns, task, this->_counter_match);
    if ( server_rpc )   {
      auto  len = selection.length()+1+sizeof(Online::RPCCommRegexCookie);
      auto  ctx = this->create_context(server_rpc.get(), Online::RPCCReadRegexCookie, len);
      auto* cmd = ctx->data<Online::RPCCommRegexCookie>();
      cmd->copy_name(cmd->which, selection);
      if ( this->do_call(server_rpc, ctx) )   {
	auto result = std::move(ctx->answer);
	if ( result.is_structured() )   {
	  if ( debug )   {
	    ::lib_rtl_output(LIB_RTL_ALWAYS,
			     "+++ task_counters('%s','%s','%s') = %s",
			     dns.c_str(), task.c_str(), selection.c_str(), _to_string(result).c_str());
	  }
	  ++m_counters.num_success;
	  return result;
	}
      }
      ++m_counters.num_error;
      return {{"error", EINVAL},
	      {"message", "No counters found: <br>task_counters('"+dns+"','"+task+"','"+selection+"') [Internal-error]"}};
    }
    ++m_counters.num_error;
    return {{"error", EINVAL},
	    {"message", "Invalid arguments to call <br>task_counters('"+dns+"','"+task+"','"+selection+"')  [No-such-process]"}};
  }

  /* ============================================================================================ */
  /// Access matching histogram tasks of a given dns
  server_adapter::json server_adapter::histogram_tasks(c_str& dns)  {
    ++m_histograms.object_tasks;
    ::lib_rtl_output(debug ? LIB_RTL_ALWAYS : LIB_RTL_DEBUG,
		     "+++ Calling histogram_tasks( '%s' )", dns.c_str());
    return this->_taskservice_matches(dns, this->_histogram_match);
  }
  
  /// Emit the histogram directory of a specified task
  server_adapter::json server_adapter::task_histogram_directory(c_str& dns, c_str& task)  {
    ++m_histograms.object_tasks;
    ::lib_rtl_output(debug ? LIB_RTL_ALWAYS : LIB_RTL_DEBUG,
		     "+++ Calling task_histogram_directory( '%s', '%s' )", dns.c_str(), task.c_str());
    return this->_task_directory(dns, task, this->_histogram_match);
  }

  /// Access all task histograms
  server_adapter::json server_adapter::task_histograms(c_str& dns, c_str& task)  {
    ++m_histograms.task_objects;
    ::lib_rtl_output(debug ? LIB_RTL_ALWAYS : LIB_RTL_DEBUG,
		     "+++ Calling task_histograms( '%s', '%s' )", dns.c_str(), task.c_str());
    return this->task_histograms_regex(dns, task, ".(.*)");
  }

  /// Access histogram list by regular expression
  server_adapter::json server_adapter::task_histogram(c_str& dns, c_str& task, c_str& selection)  {
    ++m_histograms.task_object;
    auto result = this->task_histograms_regex(dns, task, selection);
    if ( result.find("objects") != result.end() )   {
      json& objects = result["objects"];
      if ( objects.is_array() && objects.size() > 0 )   {
	return objects.at(0);
      }
      return {{"error", EINVAL},
   	      {"message", "No histogram found: <br>task_histogram('"+dns+"','"+task+"','"+selection+"')"}};
    }
    return result;
  }

  /// Access histogram list by regular expression
  server_adapter::json server_adapter::task_histograms_regex(c_str& dns, c_str& task, c_str& selection)  {
    ++m_histograms.task_objects_regex;
    ::lib_rtl_output(debug ? LIB_RTL_ALWAYS : LIB_RTL_DEBUG,
		     "+++ Calling task_histogram_regex( '%s', '%s', '%s' )",
		     dns.c_str(), task.c_str(), selection.c_str());
    auto server_rpc = this->get_rpc(dns, task, this->_histogram_match);
    if ( server_rpc )   {
      int cmdlen = selection.length()+1+sizeof(Online::RPCCommRegexCookie);
      auto  ctx = this->create_context(server_rpc.get(), Online::RPCCReadRegexCookie, cmdlen);
      auto* cmd = ctx->data<Online::RPCCommRegexCookie>();
      cmd->copy_name(cmd->which, selection);
      if ( this->do_call(server_rpc, ctx) )   {
	auto result = std::move(ctx->answer);
	ctx.reset();
	if ( result.is_array() )   {
	  json ret = std::move(result);
	  if ( debug )   {
	    ::lib_rtl_output(LIB_RTL_ALWAYS,
			     "+++ task_counters('%s','%s','%s') = %s",
			     dns.c_str(), task.c_str(), selection.c_str(), _to_string(ret).c_str());
	  }
	  ++m_histograms.num_success;
	  return ret;
	}
	++m_histograms.num_error;
	return {{"error", EINVAL},
		{"message", "Received invalid object structure: <br>task_histograms_regex('"+dns+"','"+task+"','"+selection+"')"}};
      }
      ++m_histograms.num_error;
      return {{"error", EINVAL},
	      {"message", "No histograms found: <br>task_histograms_regex('"+dns+"','"+task+"','"+selection+"')"}};
    }
    ++m_histograms.num_error;
    return {{"error", EINVAL},
	    {"message", "Invalid arguments to call <br>task_histograms_regex('"+dns+"','"+task+"','"+selection+"')"}};
  }
}
