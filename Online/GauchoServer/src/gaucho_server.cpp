//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//=========================================================================
//
//  Author     : M.Frank
//  Created    : 05/4/2022
//
//=========================================================================

/// Framework includes
#include <GauchoServer/gaucho_server.h>
#include <GauchoServer/gaucho_ioc_adapter.h>
#include <RPC/HttpJsonRpcHandler.h>
#include <RPC/HttpRpcHandler.h>
#include <HTTP/HttpFileHandler.h>
#include <HTTP/HttpCacheCheck.h>
#include <CPP/IocSensor.h>
#include <RTL/strdef.h>
#include <RTL/rtl.h>

/// C/C++ include files
#include <thread>
#include <memory>
#include <iostream>

/// gaucho namespace declaration
namespace gaucho   {

  /// Definition of the server implementation class
  /**
   *
   *    \author  M.Frank
   *    \version 1.0
   */
  class gaucho_server::internals_t   {
  public:
    enum server_state { CREATED, CONFIGURED, RUNNING, };
    int cache    = 0;
    int port     = 0;
    int threads  = 0;
    int debug    = 0;
    int have_ioc = 0;
    int state    = CREATED;
    int detached = 0;
    int print    = LIB_RTL_WARNING;
    std::string host        { "0.0.0.0" };
    std::string mount_point { "/JSONRPC" };
    std::vector<std::string>                    dns_nodes     { };
    std::unique_ptr<std::thread>                ioc_handler   { };
    std::unique_ptr<std::thread>                srv_handler   { };
    std::shared_ptr<rpc::HttpServer>            http_server   { };
    std::unique_ptr<rpc::HttpServer::Handler>   http_handler  { };
    std::unique_ptr<gaucho::server_adapter>     callable      { };
    std::shared_ptr<http::HttpCacheCheck>       json_clean    { };
    std::shared_ptr<rpc::RpcHandlerImp<rpc::HttpJsonRpcHandler> > json_handler;

    /// Print help menu
    void help_server(const char* msg = 0, const char* argv = 0, bool do_exit = true);
    /// Run server
    int  run_server();

  public:
    /// Default constructor
    internals_t() = default;
    /// Default destructor
    virtual ~internals_t() = default;
    /// Configure the server object
    int set(const std::string& what, const std::string& value);
    /// Configure the server object
    void configure();
    /// Configure the server object
    void configure(int argc, char** argv);
    /// Configure RPC handler
    void configure_rpc(rpc::HttpJsonRpcHandler& handler);
    /// Configure other stuff
    void configure_options();
    /// Stop the server
    int stop();
    /// Run the server
    int run(bool detached);
  };

  /// =========================================================================
  void gaucho_server::internals_t::help_server(const char* msg, const char* argv, bool do_exit) {
    std::cout << "run_gaucho_rpc -opt <value> [ -opt <value> ]                                 " << std::endl
	      << "     -host    <string>    Host part of address (default: 0.0.0.0)            " << std::endl
	      << "     -port    <number>    Listening port (default: 8000)                     " << std::endl
	      << "     -dns     <string>    Add DNS domain to publish Gaucho information       " << std::endl
	      << "     -mount   <string>    Service URL. (default: " << this->mount_point << ")" << std::endl
	      << "     -threads <number>    Number of *additional* worker threads (default: 0) " << std::endl
	      << "     -detached            Run in detached mode. Prompt returns to client.    " << std::endl
	      << "     -debug   <number>    Enable debug mask (default: off)                   " << std::endl
	      << "     -cache   <number>    Enable cache with timeout in milli seconds.        " << std::endl
	      << "     -help                Print this help.                                   " << std::endl;
    if ( msg )
      std::cout << "Error cause:    " << msg << std::endl;
    if ( argv )
      std::cout << "Argument given: " << argv << std::endl;
    std::cout << std::endl;
    if ( do_exit )
      ::exit(EINVAL);
  }

  /// Configure the server object
  int gaucho_server::internals_t::set(const std::string& what, const std::string& value)  {
    const char* argv[2] = {(what[0] == '-') ? what.c_str()+1 : what.c_str(), value.c_str()};
    int argc = 2, i = 0;
    if ( 0 == std::strncmp("port",argv[i],4) )   {
      this->port = ::atol(argv[++i]);
      return 2;
    }
    else if ( 0 == std::strncmp("host",argv[i],4) )  {
      this->host = (++i < argc) ? argv[i] : "localhost";
      return 2;
    }
    else if ( 0 == std::strncmp("mount",argv[i],5) )  {
      this->mount_point = (++i < argc) ? argv[i] : "localhost";
      return 2;
    }
    else if ( 0 == std::strncmp("dns",argv[i],3) )  {
      this->dns_nodes.emplace_back(argv[++i]);
      return 2;
    }
    else if ( 0 == std::strncmp("ioc",argv[i],3) )  {
      this->have_ioc = 1;
      return 1;
    }
    else if ( 0 == std::strncmp("detached",argv[i],6) )  {
      this->detached = 1;
      return 1;
    }
    else if ( 0 == std::strncmp("threads",argv[i],3) )  {
      this->threads = (++i < argc) ? ::atol(argv[i]) : 2;
      return 2;
    }
    else if ( 0 == std::strncmp("debug",argv[i],4) )  {
      this->debug = (++i < argc) ? ::atol(argv[i]) : 3;
      return 2;
    }
    else if ( 0 == std::strncmp("print",argv[i],4) )  {
      this->print = (++i < argc) ? ::atol(argv[i]) : 3;
      return 2;
    }
    else if ( 0 == std::strncmp("cache",argv[i],4) )  {
      this->cache = (++i < argc) ? ::atol(argv[i]) : 30;
      return 2;
    }
    else if ( 0 == std::strncmp("help",argv[i],2) )  {
      this->help_server();
      return 1;
    }
    else  {
      this->help_server("Invalid argument given", argv[i]);
      return 1;
    }
    return 1;
  }
  
  /// Configure the server object
  void gaucho_server::internals_t::configure(int argc, char** argv)  {
    for ( int i = 1; i < argc && argv[i]; )
      i += this->set(argv[i], argc>i+1 ? argv[i+1] : "");
    return this->configure();
  }

  /// Configure the server object
  void gaucho_server::internals_t::configure()  {
    auto regex_flags = std::regex_constants::ECMAScript | std::regex_constants::icase;
    ::lib_rtl_set_log_level(this->print);
    this->json_handler = std::make_shared<rpc::RpcHandlerImp<rpc::HttpJsonRpcHandler> >();
    this->callable = std::make_unique<ioc_server_adapter>(**this->json_handler);
    this->callable->_counter_match   = std::regex("(.*)/Counter/HistCommand", regex_flags);
    this->callable->_histogram_match = std::regex("(.*)/Histos/HistCommand",  regex_flags);

    auto rpc = std::make_unique<rpc_relay::server_t>();
    rpc->service_matches.insert(std::make_pair(server_adapter::HIST_CALL,
					       this->callable->_histogram_match));
    rpc->service_matches.emplace(std::make_pair(server_adapter::COUNTER_CALL,
						this->callable->_counter_match));
    rpc->server_adapter = this->callable.get();
    this->callable->_rpc_server = std::move(rpc);
    this->callable->debug       = debug&1;

    this->configure_rpc(**this->json_handler);
    this->configure_options();
    this->state = CONFIGURED;
  }

  /// Configure RPC handler
  void gaucho_server::internals_t::configure_rpc(rpc::HttpJsonRpcHandler& handler)   {
    server_adapter* callee = this->callable.get();
    handler.define("counter_tasks",            jsonrpc::Call(callee).make(&server_adapter::counter_tasks));
    handler.define("task_counter",             jsonrpc::Call(callee).make(&server_adapter::task_counter));
    handler.define("task_counters",            jsonrpc::Call(callee).make(&server_adapter::task_counters));
    handler.define("task_counters_regex",      jsonrpc::Call(callee).make(&server_adapter::task_counters_regex));
    handler.define("task_counter_directory",   jsonrpc::Call(callee).make(&server_adapter::task_counter_directory));

    handler.define("histogram_tasks",          jsonrpc::Call(callee).make(&server_adapter::histogram_tasks));
    handler.define("task_histogram",           jsonrpc::Call(callee).make(&server_adapter::task_histogram));
    handler.define("task_histograms",          jsonrpc::Call(callee).make(&server_adapter::task_histograms));
    handler.define("task_histograms_regex",    jsonrpc::Call(callee).make(&server_adapter::task_histograms_regex));
    handler.define("task_histogram_directory", jsonrpc::Call(callee).make(&server_adapter::task_histogram_directory));

    handler.define("sys_output_stats",         jsonrpc::Call(callee).make(&server_adapter::_print_statistics));
    handler.define("sys_counter_statistics",   jsonrpc::Call(callee).make(&server_adapter::_counter_statistics));
    handler.define("sys_histogram_statistics", jsonrpc::Call(callee).make(&server_adapter::_histogram_statistics));

  
    handler.compression.enable = true;
    handler.debug              = (this->debug&2) != 0;
    if ( cache )   {
      ::lib_rtl_output(LIB_RTL_ALWAYS,"+++ Cache handling is enabled: %d debug: %d", cache, (debug&2));
    }
    handler.cache.timeout      = { this->cache/1000, this->cache%1000 };
    handler.cache.enable       = (this->cache != 0);
    handler.cache.debug        = (this->debug&4) != 0;
    handler.cache.name         = "Json RPC";
  }

  /// Configure other stuff
  void gaucho_server::internals_t::configure_options()  {
    this->http_handler = std::make_unique<rpc::HttpServer::Handler>();
    if ( cache != 0 )  {
      this->json_clean = std::make_shared<http::HttpCacheCheck>(**this->json_handler, this->http_handler->io_context, 5);
    }
    if ( this->have_ioc )   {
      this->ioc_handler = std::make_unique<std::thread>([] {  CPP::IocSensor::instance().run();  });
    }
    this->http_handler->mountPoints.handlers.emplace(this->mount_point, std::move(this->json_handler));
  }

  /// Stop the server
  int gaucho_server::internals_t::stop()  {
    if ( this->http_server )   {
      this->http_server->stop();
      return 1;
    }
    return 0;
  }

  int gaucho_server::internals_t::run_server()   {
    this->state = RUNNING;
    this->http_server->run(this->threads); 
    return 0x0;
  }
  
  /// Run the server
  int gaucho_server::internals_t::run(bool run_detached)  {
    ::lib_rtl_output(LIB_RTL_INFO,
		     "[RODomainRPC] Running HTTP xmlrpc service %s:%d with %d additional threads.",
		     this->host.c_str(), this->port, this->threads);
    this->http_server = std::make_shared<rpc::HttpServer>(std::move(this->http_handler),
							  this->host,
							  this->port,
							  rpc::HttpServer::SERVER);
    this->http_server->setDebug(this->debug);
    this->http_server->start(true);
    for ( const auto& dns : dns_nodes )    {
      this->callable->_rpc_server->add_dns(dns);
    }
    if ( this->detached || run_detached )    {
      this->srv_handler = std::make_unique<std::thread>([this]{ this->run_server(); });
      return 0x0;
    }
    return this->run_server();
  }

  /// Initializing constructor
  gaucho_server::gaucho_server()  {
    internals = std::make_unique<internals_t>();
  }

  /// Default destructor
  gaucho_server::~gaucho_server()  {
    internals.reset();
  }

  /// Print help
  void gaucho_server::help()  {
    internals->help_server(0, 0, false);
  }

  /// Configure the server object
  void gaucho_server::configure()  {
    internals->configure();
  }

  /// Configure the server object
  int gaucho_server::set(const std::string& what, const std::string& value)  {
    return internals->set(what, value);
  }

  /// Configure the server object
  void gaucho_server::configure(int argc, char** argv)  {
    internals->configure(argc, argv);
  }

  /// Stop the server
  int gaucho_server::stop()  {
    return internals->stop();
  }

  /// Run the server
  int gaucho_server::run(bool detached)  {
    return internals->run(detached);
  }
}  
