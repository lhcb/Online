//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//=========================================================================
//
//  Author     : M.Frank
//  Created    : 05/4/2022
//
//=========================================================================

// Framework includes
#include <GauchoServer/rpc_relay_server.h>
#include <RTL/strdef.h>
#include <RTL/rtl.h>
#include <dim/dis.h>
#include <dim/dic.h>

// C++ include files
#include <iostream>
#include <stdexcept>
#include <fstream>
#include <cerrno>
#include <vector>
#include <map>

using namespace rpc_relay;

#define NOLINK ((void*)"DEAD")

namespace {
  bool is_no_link(const void* buffer, size_t length)   {
    return length == sizeof(NOLINK) && *(int*)NOLINK == *(int*)buffer;
  }
}

server_t::rpc_t::rpc_t(const std::string& svc, unsigned long type, server_stub_t* serv)
  : service(svc), server(serv), rpc_type(type)
{
  auto resp = this->service + "/RpcOut";
  int  dns  = this->server->dns->dns_id;
  int  tmo  = this->server->listener->timeout;
  this->command = this->service + "/RpcIn";
  this->response_svc_id = ::dic_info_service_stamped_dns(dns, resp.c_str(), MONIT_ONLY, 
						     tmo, 0, 0, 
						     rpc_t::rpc_response_call, long(this),
						     NOLINK, sizeof(NOLINK));
}

server_t::rpc_t::~rpc_t()   {
  if ( this->response_svc_id )   {
    ::dic_release_service(this->response_svc_id);
  }
  this->magic = 0xDEADDEAD;
}

void server_t::rpc_t::call(const void* data, size_t len)   {
  ::dic_cmnd_service_dns(this->server->dns->dns_id, this->command.c_str(), (void*)data, len);
}

void server_t::rpc_t::call_with_feedback(const void* data, size_t len)     {
  ::dic_cmnd_callback_dns(this->server->dns->dns_id, this->command.c_str(), (void*)data, len, rpc_t::rpc_submission_response, dim_long(this));
}

void server_t::rpc_t::get_time_stamp(int& secs, int& milli)    {
  ::dic_get_timestamp (this->response_svc_id, &secs, &milli);
}

void server_t::rpc_t::rpc_submission_response(void* param, int* data)    {
  if ( param && data )    {
    rpc_t* rpc = (rpc_t*)param;
    rpc->on_submission_response(*data != 0);
  }
}

void server_t::rpc_t::on_submission_response(bool /* success */)    {
}

void server_t::rpc_t::rpc_response_call(void* tag, void* buffer, int* size)    {
  if ( tag )    {
    auto* rpc = *(server_t::rpc_t**)tag;
    if ( buffer && size && *size )   {
      long  length = *size;
      if ( is_no_link(buffer, length) )
	rpc->on_nolink();
      else
	rpc->on_response(buffer, length);
    }
  }
}

/// Handle no-link RPC response
void server_t::rpc_t::on_nolink()   {
  this->connected = 0;
  this->server->on_nolink(this);
}

/// Handle RPC response from server
void server_t::rpc_t::on_response(const void* data, size_t len)   {
  if ( data && len )   {
    this->connected = 1;
    this->server->on_response(this, data, len);
    return;
  }
  this->server->on_nolink(this);
}

/// Lock RPC handler
bool server_t::rpc_t::lock()    {
  this->lock_mutex.lock();
  return true;
}

/// Unlock RPC handler
bool server_t::rpc_t::unlock()   {
  this->lock_mutex.unlock();
  return true;
}

/// Default destructor
server_t::dns_t::~dns_t()   {
  this->magic = 0xDEADDEAD;
}

server_t::server_stub_t::server_stub_t(const std::string& nam,
				     const std::string& nod,
				     std::shared_ptr<dns_t>& d,
				     server_t* l)
  : name(nam), node(nod), dns(d), listener(l), info_id(0)
{
  if ( nullptr == this->listener )   {
    std::cout << "++++ Server's listener is NULL!" << std::endl;
  }
}

server_t::server_stub_t::~server_stub_t()   {
  this->clear_all_rpc();
  this->magic = 0xDEADDEAD;
}

/// Handle no-link RPC response
void server_t::server_stub_t::on_nolink(rpc_t* rpc)   {
  this->listener->on_nolink(rpc);
}

/// Handle RPC response
void server_t::server_stub_t::on_response(rpc_t* rpc, const void* data, size_t len)     {
  this->listener->on_response(rpc, data, len);
}

bool server_t::server_stub_t::add_rpc(const std::string& service, unsigned long cookie)    {
  auto it = this->connections.find(service);
  if ( it == this->connections.end() )   {
    auto  rpc = std::make_unique<rpc_t>(service, cookie, this);
    auto* ptr = rpc.get();
    this->connections.emplace(service, std::move(rpc));
    this->listener->on_connect_rpc(ptr);
    return true;
  }
  return false;
}

bool server_t::server_stub_t::remove_rpc(const std::string& service)    {
  auto it = this->connections.find(service);
  if ( it != this->connections.end() )   {
    this->listener->on_disconnect_rpc(it->second.get());
    this->connections.erase(it);
    return true;
  }
  return false;
}

void server_t::server_stub_t::clear_all_rpc()    {
  std::lock_guard<std::mutex> protection(this->lock);
  this->connections.clear();
}

/// Invoke RPC call
bool server_t::server_stub_t::call_rpc(const std::string& service, const void* data, size_t len)   {
  if ( !service.empty() && data && len )   {
    std::string cmd;  {
      std::lock_guard<std::mutex> protection(this->lock);
      auto it = this->connections.find(service);
      if ( it == this->connections.end() )   {
	return false;
      }
      cmd = it->second->command;
    }
    ::dic_cmnd_service_dns(this->dns->dns_id, cmd.c_str(), (void*)data, len);
    return true;
  }
  return false;
}

/// Default destructor
server_t::server_adapter_t::~server_adapter_t()    {
  this->magic = 0xDEADDEAD;
}

/// Default constructor
server_t::server_t()    {
}

/// Default destructor
server_t::~server_t()    {
  this->magic = 0xDEADDEAD;
}

/// Informational callback when a rpc service appears
void server_t::on_connect_rpc(rpc_t* rpc)    {
  this->server_adapter->on_connect_rpc(rpc);
}

/// Informational callback when a rpc service disappears
void server_t::on_disconnect_rpc(rpc_t* rpc)    {
  this->server_adapter->on_disconnect_rpc(rpc);
}

/// Handle no-link RPC response
void server_t::on_nolink(rpc_t* rpc)   {
  this->server_adapter->on_nolink(rpc);
}

/// Handle RPC response from server
void server_t::on_response(rpc_t* rpc, const void* data, size_t len)   {
  this->server_adapter->on_response(rpc, data, len);
}

/// DimInfo overload to process messages
void server_t::dnsinfo_callback(void* tag, void* address, int* size)   {
  if ( address && tag && size && *size>0 )   {
    server_t::dns_t* it = *(server_t::dns_t**)tag;
    for( auto& e : it->listener->known_dns_servers )   {
      if ( e.second.get() == it )   {
	it->listener->analyze_tasks((const char*)address, e.second);
      }
    }
  }
}

/// DimInfo overload to process messages
void server_t::taskinfo_callback(void* tag, void* address, int* size)   {
  if ( address && tag && size && *size>0 )   {
    server_t::server_stub_t* it = *(server_t::server_stub_t**)tag;
    it->listener->analyze_services((const char*)address, it);
  }
}

void server_t::analyze_tasks(const char* input, std::shared_ptr<dns_t>& dns)   {
  int level = LIB_RTL_INFO;
  if ( is_no_link(input, strlen(input)) )  {
    ::lib_rtl_output(level,"Task Info Handler callback from DNS DEAD");
    return;
  }
  else if ( std::strlen(input)==0 )  {
    return;
  }

  int action = 0;
  std::vector<std::string> server_list;
  switch(input[0])   {
  case '+':
    server_list = RTL::str_split(input+1,"|");
    action =  1;
    break;
  case '-':
    server_list = RTL::str_split(input+1,"|");
    action = -1;
    break;
  case '!':
    return;
  default:
    server_list = RTL::str_split(input,"|");
    action =  1;
    break;
  }
  if ( server_list.empty() )   {
    return;
  }
  for ( auto server : server_list )  {
    ::lib_rtl_output(level,"+++ %s service list handler for %s -> %s",
		     action==1 ? "Add" : "Remove",
		     dns->name.c_str(), server.c_str());
    ( action == 1 ) ? this->add_server(server, dns) : this->remove_server(server, dns);
  }
}

void server_t::analyze_services(const char* input, server_stub_t* server)   {
  int level = LIB_RTL_ALWAYS;
  if ( is_no_link(input, strlen(input)) )  {
    std::string server_node = server->name + "@" + server->node;
    ::lib_rtl_output(level,"Service Info Handler callback from DNS DEAD: %s", server->name.c_str());
    //this->remove_server(server_node);
    return;
  }
  else if ( std::strlen(input)==0 )  {
    return;
  }
  int action = 0;
  char cact = input[0];
  std::vector<std::string> service_list;
  switch(cact)   {
  case '+':
    service_list = RTL::str_split(input+1,"\n");
    action =  1;
    break;
  case '-':
    service_list = RTL::str_split(input+1,"\n");
    action = -1;
    break;
  case '!':
    break;
  default:
    service_list = RTL::str_split(input,"\n");
    action =  1;
    cact = ' ';
    break;
  }
  std::stringstream str;
  std::size_t matches = 0;
  if ( !service_list.empty() )  {
    for ( std::string svc : service_list )    {
      std::string service = svc.substr(0, svc.find("|"));
      for( const auto& match : service_matches )   {
	std::smatch sm;
	bool stat = std::regex_match(service, sm, match.second);
	if ( stat )   {
	  std::lock_guard<std::mutex> lock(server->lock);
	  (action==1) ? server->add_rpc(service, match.first) : server->remove_rpc(service);
	  str << service << " ";
	  ++matches;
	}
      }
    }
  }
  ::lib_rtl_output(LIB_RTL_DEBUG,
		   "+++ Got Service list from %s. action: '%c'  %ld entries %ld matches.  %s",
		   server->name.c_str(), cact, service_list.size(), matches, str.str().c_str());
}

/// Add new DNS server list 
bool server_t::add_dns(const std::string& dns)   {
  auto dns_name = RTL::str_upper(dns);
  if ( this->known_dns_servers.find(dns_name) == this->known_dns_servers.end() )    {
    auto dns_id = ::dic_add_dns(dns_name.c_str(), ::dim_get_dns_port());
    auto entry  = std::make_shared<dns_t>(dns_name, this, dns_id, 0);
    entry->info_id = ::dic_info_service_dns(dns_id, 
					    "DIS_DNS/SERVER_LIST", MONITORED,
					    0, nullptr, 0, dnsinfo_callback, 
					    (long)entry.get(), NOLINK, sizeof(NOLINK));
    this->known_dns_servers.emplace(dns_name, std::move(entry));
    ::lib_rtl_output(LIB_RTL_DEBUG,"+++ Add DNS: %s id: %d", dns.c_str(), dns_id);
    return true;
  }
  return false;
}

/// Add new server to list
bool server_t::add_server(const std::string& server_node, std::shared_ptr<dns_t>& dns)   {
  size_t idx = server_node.find('@');
  if ( idx != std::string::npos )    {
    std::string server  = server_node.substr(0, idx);
    if ( dns->servers.find(server) == dns->servers.end() )   {
      std::string node    = server_node.substr(idx+1);
      std::string service = server + "/SERVICE_LIST";
      auto entry = std::make_shared<server_stub_t>(server, node, dns, this);
      dns->servers.emplace(server, std::move(entry));
      auto& e = dns->servers[server];
      e->info_id = ::dic_info_service_dns(dns->dns_id,
					  service.c_str(), MONITORED,
					  0, nullptr, 0, taskinfo_callback, 
					  (long)e.get(), NOLINK, sizeof(NOLINK));
      return true;
    }
  }
  return false;
}

/// Remove server from list 
bool server_t::remove_server(const std::string& server_node, std::shared_ptr<dns_t>& dns)   {
  size_t idx = server_node.find('@');
  if ( idx != std::string::npos )    {
    std::string server  = server_node.substr(0, idx);
    auto i = dns->servers.find(server);
    if ( i != dns->servers.end() )   {
      auto& svc = i->second;
      if ( svc->info_id )    {
	::lib_rtl_output(LIB_RTL_INFO,"+++ Release SERVICE_LIST of %s", server_node.c_str());
	::dic_release_service(svc->info_id);
	svc->info_id = 0;
      }
      dns->servers.erase(i);
      return true;
    }
  }
  return false;
}

/// Access all servers from a given DNS node
std::vector<server_t::server_stub_t*> 
server_t::get_servers(const std::string& dns_node, const std::regex& match)  const
{
  std::vector<server_stub_t*> result;
  auto dns_name = RTL::str_upper(dns_node);
  auto i = this->known_dns_servers.find(dns_name);
  if ( i != this->known_dns_servers.end() )    {
    auto& dns = i->second;
    for( const auto& s : dns->servers )   {
      auto& srv = s.second;
      if ( srv->dns == dns )   {
	std::lock_guard<std::mutex> lock(srv->lock);
	for( const auto& r : srv->connections )   {
	  std::smatch sm;
	  bool stat = std::regex_match(r.first, sm, match);
	  if ( stat )   {
	    result.push_back(srv.get());
	    break;
	  }
	}
      }
    }
  }
  return result;
}

std::shared_ptr<server_t::rpc_t> 
server_t::get_server_rpc(const std::string& dns_node, const std::string& server, const std::regex& rpc_match)  const
{
  auto dns_name = RTL::str_upper(dns_node);
  auto idns = this->known_dns_servers.find(dns_name);
  if ( idns != this->known_dns_servers.end() )    {
    auto& dns = idns->second;
    for ( const auto& its : dns->servers )    {
      auto& srv = its.second;
      std::lock_guard<std::mutex> lock(srv->lock);
      for( const auto& r : srv->connections )   {
	if ( ::strncmp(r.first.c_str(), server.c_str(), server.length()) == 0 )   {
	  std::smatch sm;
	  bool stat = std::regex_match(r.first, sm, rpc_match);
	  if ( stat )   {
	    return r.second;
	  }
	}
      }
    }
  }
  return {};
}

std::shared_ptr<server_t::rpc_t> 
server_t::get_server_rpc(const std::string& dns_node, const std::regex& server_match, const std::regex& rpc_match)  const
{
  /// BAD implementation!!!
  auto dns_name = RTL::str_upper(dns_node);
  auto idns = this->known_dns_servers.find(dns_name);
  if ( idns != this->known_dns_servers.end() )    {
    auto& dns = idns->second;
    for( const auto& isrv : dns->servers )   {
      std::smatch sm_srv;
      bool stat = std::regex_match(isrv.first, sm_srv, server_match);
      if ( stat )   {
	auto& srv = isrv.second;
	std::lock_guard<std::mutex> lock(srv->lock);
	for( const auto& rpc : srv->connections )   {
	  std::smatch sm;
	  stat = std::regex_match(rpc.first, sm, rpc_match);
	  if ( stat )   {
	    return rpc.second;
	  }
	}
      }
    }
  }
  return {};
}

std::vector<server_t::rpc_t*>
server_t::get_servers_rpc(const std::string& dns_node, const std::regex& server_match, const std::regex& rpc_match)  const
{
  /// BAD implementation!!!
  std::vector<rpc_t*> result;
  auto dns_name = RTL::str_upper(dns_node);
  auto idns = this->known_dns_servers.find(dns_name);
  if ( idns != this->known_dns_servers.end() )    {
    auto& dns = idns->second;
    for( const auto& isrv : dns->servers )   {
      std::smatch sm_srv;
      bool stat = std::regex_match(isrv.first, sm_srv, server_match);
      if ( stat )   {
	auto& srv = isrv.second;
	std::lock_guard<std::mutex> lock(srv->lock);
	for( const auto& rpc : srv->connections )   {
	  std::smatch sm;
	  stat = std::regex_match(rpc.first, sm, rpc_match);
	  if ( stat )   {
	    result.emplace_back(rpc.second.get());
	  }
	}
      }
    }
  }
  return result;
}
