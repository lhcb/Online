//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Project    : Online
//
//  Author     : Markus Frank
//==========================================================================
namespace Online {}
using namespace Online;

#include <Gaudi/PluginService.h>

#include <EventHandling/MBMEventAccess.h>
DECLARE_FACTORY(MBMEventAccess, Gaudi::PluginService::Factory<EventAccess*(std::unique_ptr<RTL::Logger>&&)>)

#include <EventHandling/NetEventAccess.h>
DECLARE_FACTORY(NetEventAccess, Gaudi::PluginService::Factory<EventAccess*(std::unique_ptr<RTL::Logger>&&)>)

#include <EventHandling/FileEventAccess.h>
DECLARE_FACTORY(FileEventAccess, Gaudi::PluginService::Factory<EventAccess*(std::unique_ptr<RTL::Logger>&&)>)

#include "Sequencer.h"
DECLARE_COMPONENT( Online::Sequencer )
