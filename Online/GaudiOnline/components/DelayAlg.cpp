//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : DAQ/MDF
//
//  Author     : Markus Frank
//==========================================================================

// Include files from Gaudi
#include <Gaudi/Algorithm.h>
#include <GaudiKernel/IIncidentSvc.h>
#include <GaudiKernel/IIncidentListener.h>
#include <RTL/rtl.h>

// C/C++ include files
#include <atomic>
#include <sstream>

/// Online namespace declaration
namespace Online  {

/** @class DelayAlg DelayAlg.cpp
  *  Creates and fills dummy RawEvent
  *
  *  @author Markus Frank
  *  @date   2005-10-13
  */
  class DelayAlg : public extends<Gaudi::Algorithm, IIncidentListener>  {
    /// Properties for possible timeouts
    Gaudi::Property<int>    m_initDelay    { this, "Initialize",   0.0, "Delay at initialize"};
    Gaudi::Property<int>    m_finiDelay    { this, "Finialize",    0.0, "Delay at finalize"};
    Gaudi::Property<int>    m_startDelay   { this, "Start",        0.0, "Delay at start"};
    Gaudi::Property<int>    m_stopDelay    { this, "Stop",         0.0, "Delay at stop"};
    Gaudi::Property<int>    m_pauseDelay   { this, "Pause",        0.0, "Delay at Pause"};
    Gaudi::Property<int>    m_cancelDelay  { this, "Cancel",       0.0, "Delay at Cancel"};
    Gaudi::Property<int>    m_eventDelay   { this, "Event",        0.0, "Delay at execute()"};
    Gaudi::Property<int>    m_stopTrgDelay { this, "StopTrigger",  0.0, "Delay at StopTrigger"};
    Gaudi::Property<int>    m_startTrgDelay{ this, "StartTrigger", 0.0, "Delay at StartTrigger"};
    Gaudi::Property<long>   m_eventNumber  { this, "EventNumber",   -1, "Event number to trigger Event delay"};

    /// Current event counter
    mutable std::atomic<long> m_num_event  { 0 };
    /// Reference to the incident service
    SmartIF<IIncidentSvc>     m_incident;

  public:

    using extends::extends;

    /// Sleep function
    void delay(double timeout)  const   {
      if( timeout > 1E-6 )  {
	long useconds = long(timeout*1E6);
	::lib_rtl_usleep(useconds);
      }
    }

    /// Initialize the delay component
    StatusCode initialize()  override  {
      this->m_incident = service<IIncidentSvc>("IncidentSvc");
      if( !this->m_incident )   {
	this->error() << "+++ Failed to access Incident service." << endmsg;
	return StatusCode::FAILURE;
      }
      this->m_incident->addListener(this,"DAQ_PAUSE");
      this->m_incident->addListener(this,"DAQ_CANCEL");
      this->m_incident->addListener(this,"DAQ_START_TRIGGER");
      this->m_incident->addListener(this,"DAQ_STOP_TRIGGER");
      delay(m_initDelay.value());
      return StatusCode::SUCCESS;
    }

    /// Start the delay component
    StatusCode start() override   {
      delay(m_startDelay.value());
      m_num_event = 0;
      return StatusCode::SUCCESS;
    }

    /// Finalize the delay component
    StatusCode finalize()  override  {
      delay(m_finiDelay.value());
      this->m_incident->removeListener(this);
      this->m_incident.reset();
      return StatusCode::SUCCESS;
    }

    /// Stop the delay component
    StatusCode stop() override   {
      delay(m_stopDelay.value());
      return StatusCode::SUCCESS;
    }

    /// Data processing overload: Send data record to network client
    StatusCode execute(EventContext const& /* ctxt */) const override  {
      long evt = m_eventNumber.value();
      ++m_num_event;
      if( evt < 0 || evt == m_num_event )  {
	delay(m_eventDelay.value());
      }
      return StatusCode::SUCCESS;
    }

    /// Incident handler implemenentation: Inform that a new incident has occured
    void handle(const Incident& inc)   override   {
      debug() << "Got incident of type: " << inc.type() << endmsg;
      if( inc.type() == "DAQ_PAUSE" )  {
	delay(m_pauseDelay.value());
      }
      else if( inc.type() == "DAQ_CANCEL" )  {
	delay(m_cancelDelay.value());
      }
      else if( inc.type() == "DAQ_START_TRIGGER" )  {
	delay(m_startTrgDelay.value());
      }
      else if( inc.type() == "DAQ_STOP_TRIGGER" )  {
	delay(m_stopTrgDelay.value());
      }
    }

  };
}

DECLARE_COMPONENT( Online::DelayAlg )
