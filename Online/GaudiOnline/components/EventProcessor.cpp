//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Project    : Online
//
//  Author     : Markus Frank
//==========================================================================

/// Framework include files
#include "EventProcessor.h"
#include <Gaucho/IGauchoMonitorSvc.h>

/// C/C++ include files

/// Put bank container to specified location in the TES
void Online::EventProcessor::put_banks(const std::string& loc, evt_data_t&& event)  const  {
  lb_evt_data_t* lb_event = (lb_evt_data_t*)&event;
  auto obj = std::make_unique<AnyDataWrapper<lb_evt_data_t> >(std::move(*lb_event));
  if ( eventSvc()->registerObject(loc, obj.get()).isSuccess() )   {
    obj.release();
    return;
  }
  m_logger->except("EventHandler","+++ Failed to register TAE leaf %s",loc.c_str());
}

/// Initialize the algorithm
StatusCode Online::EventProcessor::initialize()  {
  m_logger = std::make_unique<RTL::Logger>(RTL::Logger::getGlobalDevice(),name(),msgLevel());
  SmartIF<IGauchoMonitorSvc> m(this->monitorSvc());
  this->m_numFilterPassed = 0;
  this->m_numEventsProcessed = 0;
  this->m_numGenericException = 0;
  if ( m )   {
    m->declareInfo("NumEventsProcessed", this->m_numEventsProcessed,
		   "Number of events entering process()", this);
    m->declareInfo("NumGenericExceptions", this->m_numGenericException,
		   "Number of events with exceptions", this);
    m->declareInfo("NumFilterPassed", this->m_numFilterPassed,
		   "Number of events passing processing step", this);
  }
  return StatusCode::SUCCESS;
}

/// Start the algorithm
StatusCode Online::EventProcessor::start()  {
  this->m_numFilterPassed = 0;
  this->m_numEventsProcessed = 0;
  this->m_numGenericException = 0;
  return StatusCode::SUCCESS;
}

/// Stop the algorithm
StatusCode Online::EventProcessor::stop()  {
  return StatusCode::SUCCESS;
}

/// Finalize the algorithm
StatusCode Online::EventProcessor::finalize()  {
  auto m = monitorSvc();
  if ( m )   {
    m->undeclareAll(this);
  }
  m_logger.reset();
  return StatusCode::SUCCESS;
}

/// Execute single event
StatusCode Online::EventProcessor::execute(EventContext const& ctxt)   const  {
  std::exception_ptr exception_ptr;
  try  {
    ++this->m_numEventsProcessed;
    StatusCode sc = process(ctxt);
    execState(ctxt).setFilterPassed(sc.isSuccess() ? true : false);
    if ( sc.isSuccess() ) ++this->m_numFilterPassed;
    return StatusCode::SUCCESS;
  }
  catch(const std::exception& exception)  {
    ++this->m_numGenericException;
    exception_ptr = std::current_exception();
    m_logger->error("Algorithm failed with exception: %s.",exception.what());
  }
  execState(ctxt).setFilterPassed(false);
  if ( exception_ptr )  {
    std::rethrow_exception(exception_ptr);
  }
  return StatusCode::FAILURE;
}
