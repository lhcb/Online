//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Project    : Online
//
//  Author     : M.Frank
//==========================================================================
#ifndef ONLINE_GAUDIONLINE_FLOWMANAGER_H
#define ONLINE_GAUDIONLINE_FLOWMANAGER_H

/// Framework include files
#include <GaudiKernel/MinimalEventLoopMgr.h>
#include <GaudiKernel/IDataManagerSvc.h>
#include <GaudiOnline/QueuedFlowManager.h>

/// Online namespace declaration
namespace Online   {

  /// Online algorithm flow manager based on logic of the minimal event loop
  /** 
    *
    * \author  M.Frank
    * \version 1.0
    * \date    25/10/2019
    */
  class FlowManager final : public extends<MinimalEventLoopMgr, QueuedFlowManager>    {

  public:
    /// Standard Constructor
    using extends::extends;

    /// IEventProcessor override: Implementation of IService::initialize
    StatusCode initialize() override;
    /// IEventProcessor override: Implementation of IService::reinitialize
    StatusCode reinitialize() override { return StatusCode::FAILURE; }
    /// IEventProcessor override: Implementation of IService::finalize
    StatusCode finalize() override;
    /// IEventProcessor override: Implementation of IService::start
    StatusCode start() override;
    /// IEventProcessor override: Implementation of IService::stop
    StatusCode stop() override;

    /// IEventProcessor override: Implementation of IEventProcessor::nextEvent
    StatusCode nextEvent( int ) override  { return StatusCode::FAILURE; }
    /// IEventProcessor override: Implementation of IEventProcessor::executeEvent(...)
    StatusCode executeEvent( EventContext&& evtContext ) override;
    /// IEventProcessor override: Implementation of IEventProcessor::executeRun()
    StatusCode executeRun( int ) override { return StatusCode::FAILURE; }
    /// IEventProcessor override: Implementation of IEventProcessor::createEventContext
    EventContext createEventContext() override;

    ///=================================================================
    /// Process one single event synchronously (1 per thread)
    std::tuple<StatusCode, EventContext> processEvent(EventContext&& evtContext)  override;

  public:
    /// Reference to the Event Data Service's IDataManagerSvc interface
    SmartIF<IDataManagerSvc>        m_evtDataMgrSvc = nullptr;
  };   // class FlowManager
}      // Endnamespace Online
#endif // ONLINE_GAUDIONLINE_FLOWMANAGER_H
