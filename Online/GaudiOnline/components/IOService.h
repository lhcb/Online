//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Project    : Online
//
//  Author     : Markus Frank
//==========================================================================
#ifndef GAUDIONLINE_IOSERVICE_H
#define GAUDIONLINE_IOSERVICE_H

/// Framework include files
#include <GaudiKernel/Service.h>
#include <EventHandling/EventAccess.h>

/// C/C++ include files

/// Forward declarations

/// Online namespace declaration
namespace Online  {

  /// Helper service to pass options to non-service clients
  /** @class IOService IOService.h
   *
   * 
   * \author  M.Frank
   * \version 1.0
   * \date    25/04/2019
   */
  class IOService : public Service  {
  public:
    /// Access object (Valid between start and stop)
    std::shared_ptr<EventAccess>           access;
    /// Lock to protect event queue
    std::mutex                             event_queue_lock;
    /// In burst mode the events are within the burst list
    std::list<EventAccess::shared_guard_t> burst_queue;
    /// Property to ignore TAE frames (Property used by clients only)
    bool                                   expandTAE  { true };

  public:
    /// Retrieve interface ID
    static const InterfaceID& interfaceID() {
      static const InterfaceID iid("Online::IOService", 0, 0);
      return iid;
    }
    /// Constructors
    using Service::Service;

    /// Check if IO was cancelled (should be unlikely)
    bool isCancelled()  const;
    /// Query interfaces of Interface
    virtual StatusCode queryInterface(const InterfaceID& riid, void** ppv) override;
    /// Push rawevent burst
    size_t push(EventAccess::shared_guard_t&& burst);
    /// Pop event data
    EventAccess::event_t pop();
  };   // class IOService
}      // namespace Online
#endif // GAUDIONLINE_ONLINEIOSERVICE_H
