//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Project    : Online
//
//  Author     : Markus Frank
//==========================================================================
#ifndef GAUDIONLINE_INPUTALG_H
#define GAUDIONLINE_INPUTALG_H

/// Framework include files
#include <GaudiOnline/IRawEventCreator.h>
#include <GaudiKernel/IMonitorSvc.h>
#include "EventProcessor.h"
#include "IOService.h"

/// C/C++ include files
#include <atomic>

/// Online namespace declaration
namespace Online  {

  class bank_header_t;
  class raw_bank_online_t;

  /// Online algorithm to feed the TES with the basic raw event.
  /**
    *
    * \author  M.Frank
    * \version 1.0
    * \date    25/04/2019
    */
  class InputAlg : public EventProcessor   {

  protected:
    using daq_error_t = std::vector<const raw_bank_online_t*>;
    class Counters;

    /// Property: Declare raw data to TES
    Gaudi::Property<bool>                 m_declareData{this, "DeclareData",  true,  "Declare or drop data"};
    /// Property: Declare error banks seperately to the TES
    Gaudi::Property<bool>                 m_declareErrs{this, "DeclareErrors",false, "Declare error data"};
    /// Property: Create LHCb::RawEvent and declare it to the TES
    Gaudi::Property<bool>                 m_declareEvt {this, "DeclareEvent", false, "Declare raw data"};
    /// Property: Declare raw event structures
    Gaudi::Property<bool>                 m_makeRawEvt {this, "MakeRawEvent", false, "Declare RawEvent structures"};
    /// Property: Enable halt on error
    Gaudi::Property<bool>                 m_enableHalt {this, "EnableHalt",   true,  "Enable halt the event loop"};
    /// Property: Ignore errors
    Gaudi::Property<bool>                 m_canFail    {this, "CanFail",      true,  "Return ERROR on failures"};
    /// Property: Enable TAE sweep (randomly select TAE crossings) rather than place all bx's to the TES
    Gaudi::Property<int>                  m_sweepTAE   {this, "SweepTAE",     0,     "Sweep over TAE sub-events (exclusive 'expandTAE')"};
    /// Property: Name of the I/O service to pop events
    Gaudi::Property<std::string>          m_ioService  {this, "IOServiceName","Online::IOService/IOService","Name of IO service"};
    /// Property: Directory to place for vectors of raw banks for TAE subevents
    Gaudi::Property<std::string>          m_bankDir    {this, "BankDirectory","Banks","Directory to place raw banks"};

    /// Data handle to place the raw data
    DataObjectWriteHandle<lb_evt_data_t>  m_rawData    {this, "RawData",   "Banks/RawData"};
    /// Data handle to place DAQ error banks
    DataObjectWriteHandle<evt_data_t>     m_daqError   {this, "DAQErrors", "Banks/RawDataErrors"};
    /// Data handle to place the data guard
    DataObjectWriteHandle<evt_desc_t>     m_rawGuard   {this, "RawGuard",  "Banks/RawDataGuard"};
    /// Data handle to put the TAE half window (only when the input event is TAE)
    DataObjectWriteHandle<unsigned int>   m_taeHalfWindow{this, "TAEHalfWindow", "Banks/TAEHalfWindow"};

    /// Reference to the I/O service
    SmartIF<IOService>                    m_io       { };
    /// Reference to the I/O service
    SmartIF<IMonitorSvc>                  m_monitor  { };
    /// Reference to RawEvent tool
    std::unique_ptr<IRawEventCreator>     m_evtTool  { };
    /// Reference to internal monitoring counters
    std::unique_ptr<Counters>             m_counters { };
    /// Last TAE crossing for linear TAE sweep
    mutable std::atomic<long>             m_last_bx  { 0 };
    
    /// Halt the algorithm and allow for debugging
    void halt()  const;

    /// Expand TAE record from MDF
    StatusCode expand_tae_tell1_event(const event_header_t* hdr)  const;

    /// Allow to escape halt
    mutable int m_halt;

  public:
    using EventProcessor::EventProcessor;
    /// Initialize the algorithm
    virtual StatusCode initialize()   override;
    /// Finalize the algorithm
    virtual StatusCode finalize()   override;
    /// Start the algorithm
    virtual StatusCode start()   override;
    /// Stop the algorithm
    virtual StatusCode stop()    override;
    /// Execute single event
    virtual StatusCode process(EventContext const& ctxt)  const  override;
  };    // class InputAlg
}       // Endnamespace Online
#endif  // GAUDIONLINE_INPUTALG_H
