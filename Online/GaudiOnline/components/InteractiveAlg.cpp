//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Project    : Online
//
//  Author     : Markus Frank
//==========================================================================

/// Framework include files
#include <Gaudi/Algorithm.h>

// C/C++ include files
#include <cstdio>
#include <unistd.h>
#include <signal.h>

/// Online namespace declaration
namespace Online  {

  /// Online algorithm to feed the TES with the basic raw event.
  /** 
   *
   * @author  M.Frank
   * @version 1.0
   * @date    25/04/2019
   */
  class InteractiveAlg : public Gaudi::Algorithm {
    struct sigaction m_old_handler;
    Gaudi::Property<std::string> m_prompt{this, "Prompt", {"<ENTER> to continue q to quit :"}, ""};
    mutable std::mutex m_promptLock;

  public:
    using Algorithm::Algorithm;
    /// Print message
    static void print(const char* str)   {
      std::fflush(stdout); 
      std::fflush(stderr);
      std::cout << std::flush;
      std::cerr << std::flush;
      write(STDOUT_FILENO, str, ::strlen(str));
    }
    /// Interrupt handler
    static void sigint(int /* sig_num */, siginfo_t*, void*)    {
      print("Interrupt caught. Exiting.....\n");
      _exit(0);
    }
    /// Initialize the algorithm
    StatusCode initialize()  override  {
      struct sigaction handler;
      handler.sa_handler = 0;
      handler.sa_sigaction = sigint;
      handler.sa_flags = SA_SIGINFO;
      ::sigaction(SIGINT, &handler, &m_old_handler);
      return StatusCode::SUCCESS;
    }
    /// Initialize the algorithm
    StatusCode finalize()  override  {
      ::sigaction(SIGINT, &m_old_handler, NULL);
      return StatusCode::SUCCESS;
    }
    /// Execute single event
    StatusCode execute(EventContext const& /* ctxt */)  const  override {
      std::lock_guard<std::mutex> lock(m_promptLock);
    Next:
      print(m_prompt.value().c_str());
    Next2:
      int c = std::getchar();
      if ( c == -1 )  {
	goto Next2;
      }
      if ( c == 10 )  { // <ENTER>
	return StatusCode::SUCCESS;
      }
      if ( ::toupper(c) != 'Q' )   {
	//printf("Got: %d \n",int(c));
	goto Next;
      }
      print("Exiting.....\n");
      _exit(0);
      return StatusCode::SUCCESS;
    }

  };
}

using namespace Online;
/// Factory instantiation
DECLARE_COMPONENT( InteractiveAlg )
