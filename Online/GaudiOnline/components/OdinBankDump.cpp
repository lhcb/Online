//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : DAQ/MDF
//
//  Author     : Markus Frank
//==========================================================================

/// Include files from Gaudi
#include "EventProcessor.h"
#include <EventData/odin_t.h>
#include <EventData/raw_bank_offline_t.h>

#ifdef HAVE_AIDA
#include <GaudiKernel/IHistogramSvc.h>
#include <AIDA/IHistogram1D.h>
#endif

/// C/C++ include files
#include <functional>
#include <atomic>


/// Forward declarations
namespace LHCb {  class RawBank;  }

/// Online namespace declaration
namespace Online  {

  /// Class to dump the ODIN bank from a single beam crossings
  /** Class to dump the ODIN bank from a single beam crossings
   *  Dump Odin bank from Tell1 input bank data
   *
   *  @author Markus Frank
   *  @date   2024-01-13
   */
  class OdinBankDump : public EventProcessor {

    using lb_evt_data_t = std::vector<std::pair<const LHCb::RawBank*, const void*> >;
    using evt_data_t    = std::vector<std::pair<const bank_header_t*, const void*> >;
    /// Property: Flag to print ODIN bank content
    Gaudi::Property<double>              m_print{this, "Print", 1.0,  "Printout frequency of ODIN bank content"};
    /// Property: Flag to histogram ODIN bank items
    Gaudi::Property<bool>                m_histo{this, "Histo", true, "Create histograms from ODIN values"};
    /// Property: Input data handle
    DataObjectReadHandle<lb_evt_data_t>  m_banks{this, "Banks", "Banks/RawData"};

    /// Monitoring quantity: Number of run 1/2 ODIN banks
    mutable std::atomic<long> m_num_run2_odin  { 0 };
    /// Monitoring quantity: Number of run 3 ODIN banks
    mutable std::atomic<long> m_num_run3_odin  { 0 };

#ifdef HAVE_AIDA
    /// Reference to TAE entry histogram
    AIDA::IHistogram1D*  m_tae_entries  { nullptr };
    /// Reference to ODIN bank version histogram
    AIDA::IHistogram1D*  m_bnk_version  { nullptr };
#endif
    
  public:

    /// Use base class c'tors
    using EventProcessor::EventProcessor;

#ifdef HAVE_AIDA
    void fill_histo(const run2_odin_t* b, int vsn)  const    {
      m_bnk_version->fill(vsn, 1.0);
      m_tae_entries->fill(b->tae_window(), 1.0);
    }
    void fill_histo(const run3_odin_t* b, int vsn)  const    {
      m_bnk_version->fill(vsn, 1.0);
      m_tae_entries->fill(b->tae_window(), 1.0);
    }
#else
    template <typename T> void fill_histo(const T* , int )  const    { }
#endif
    /// Initialize the algorithm
    StatusCode initialize()  override  {
      StatusCode sc = this->EventProcessor::initialize();
#ifdef HAVE_AIDA
      if ( sc.isSuccess() && m_histo.value() )   {
	m_tae_entries = histoSvc()->book(name()+"/tae_entry", "TAE entry histogram", 31, -15.5, 15.5);
	m_bnk_version = histoSvc()->book(name()+"/odin_vsn",  "ODIN bank version",    5,   4.5,  9.5);
      }
#endif
      declareInfo("Run2OdinCount", m_num_run2_odin = 0, "Number of run 1/2 ODIN bank");
      declareInfo("Run3OdinCount", m_num_run3_odin = 0, "Number of run 3   ODIN bank");
      return sc;
    }

    /// Start the algorithm
    StatusCode start()  override  {
      StatusCode sc = this->EventProcessor::start();
      m_num_run2_odin = 0;
      m_num_run3_odin = 0;
      return sc;
    }
    
    /// Main execution callback
    StatusCode process(EventContext const& /* ctxt */) const override  {
      const evt_data_t* banks = (const evt_data_t*)m_banks.get();
      if ( banks )   {
	bool do_print = !(m_print < 1.0 && m_print < (double(::rand()) / double(RAND_MAX)));
	for ( const auto& dsc : *banks )    {
	  auto* bank = dsc.first;
	  if ( bank->type() == bank_types_t::ODIN )    {
	    if ( bank->version() < 7 )
	      ++m_num_run2_odin;
	    else if ( bank->version() == 7 )
	      ++m_num_run3_odin;
	    if ( do_print )   {
	      auto lines = event_print::odin_data(dsc);
	      for(const auto& line : lines)   {
		m_logger->always(line);
	      }
	    }
	    if ( m_histo )   {
	      if ( bank->version() < 7 )  {
		const auto* o = reinterpret_cast<const run2_odin_t*>(dsc.second);
		fill_histo(o, bank->version());
	      }
	      else if ( bank->version() == 7 )  {
		const auto* o = reinterpret_cast<const run3_odin_t*>(dsc.second);
		fill_histo(o, bank->version());
	      }
	    }
	    break;
	  }
	}
      }
      return StatusCode::SUCCESS;
    }
  };
}

/// Declare factory for object creation
DECLARE_COMPONENT( Online::OdinBankDump )
