//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Project    : Online
//
//  Author     : Markus Frank
//==========================================================================

#include "OnlineEventApp.h"
#include "IOService.h"
#include <EventHandling/FileEventAccess.h>
#include <EventHandling/MBMEventAccess.h>
#include <EventHandling/NetEventAccess.h>

#include <CPP/Event.h>
#include <CPP/TimeSensor.h>
#include <Gaudi/Interfaces/IQueueingEventProcessor.h>
#include <Gaudi/PluginService.h>
#include <Gaudi/Property.h>
#include <GaudiKernel/AppReturnCode.h>
#include <GaudiKernel/IProperty.h>
#include <GaudiKernel/ISvcLocator.h>
#include <RTL/rtl.h>
#include <RTL/hash.h>
#include <RTL/strdef.h>
#include <dim/dis.hxx>

// tbb control
#include "tbb/global_control.h"

/// C/C++ include files
#include <chrono>
#include <cmath>
#include <iostream>
#include <fstream>

using namespace std;
using namespace Online;

namespace {

  template <typename T>
  void stop_thread( T& t ) {
    if ( t ) {
      t->join();
      t.reset();
    }
  }

  template <typename T>
  struct Increment {
    T& count;
    Increment( T& t ) : count( t ) { ++count; }
    ~Increment() { --count; }
  };
  constexpr static long CMD_TIMER_DAQ_IDLE = 111;
} // end anonymous namespace

/// Easier command line: only supply --application=OnlineEvents
class OnlineEvents : public Online::OnlineEventApp {
public:
  using Online::OnlineEventApp::OnlineEventApp;
  virtual ~OnlineEvents() = default;
};

/// Factory instantiation
DECLARE_COMPONENT( OnlineEventApp )
DECLARE_COMPONENT( OnlineEvents )

typedef Gaudi::Interfaces::IQueueingEventProcessor IQueued;

/** @class instance_command_t
 *
 *  @author M.Frank
 */
class OnlineEventApp::instance_command_t : public DimCommand {
  /// Reference to checkpoint service
  Online::OnlineEventApp* m_app{0};

public:
  /// Constructor
  instance_command_t( const string& nam, Online::OnlineEventApp* app ) : DimCommand( nam.c_str(), "I" ), m_app( app ) {}
  /// DimCommand overload: handle DIM commands
  void commandHandler() override {
    int value = getInt();
    m_app->setMaxEventsParallel( value );
  }
};

/// Reset counters at start
void OnlineEventApp::monitor_t::reset() {
  burstsIn          = 0;
  burstsOut         = 0;
  eventsIn          = 0;
  eventsSubmitted   = 0;
  eventsProcessed   = 0;
  eventsFailed      = 0;
  eventsBuffered    = 0;
  consecutiveErrors = 0;
}

/// Specialized constructor
OnlineEventApp::OnlineEventApp( Options opts ) : OnlineApplication( opts ) {
  SmartIF<ISvcLocator> sloc = app.as<ISvcLocator>();
  m_num_cores = std::thread::hardware_concurrency();

  /// Set print I/O buffering to line mode
  m_logger->set_io_buffering(RTL::Logger::LINE_BUFFERING);
  /// Configure the IO service
  m_ioSvc = sloc->service<IOService>( "Online::IOService/IOService" );
  if ( !m_ioSvc ) {
    m_logger->except( "+++ Failed to connect to online IOService instance." );
  }
  /// Setup DIM communication to retrieve the number of instances
  if ( !m_config->numThreadSvcName.empty() ) {
    string nam = m_name + "/" + m_config->numThreadSvcName;
    m_logger->info( "+++ Create DIM command service: %s", nam.c_str() );
    m_parallel_dim_svc = make_unique<instance_command_t>(nam, this );
  }
  int seed = RTL::hash32( (RTL::processName() + "@" + RTL::nodeName()).c_str() );
  ::srand( seed );
}

// Default destructor
OnlineEventApp::~OnlineEventApp() {
  m_ioSvc.reset();
  m_parallel_dim_svc.reset();
}

/// Set the maximum number of parallel executing events
void OnlineEventApp::setMaxEventsParallel( size_t number ) {
  m_parallel_threads = std::min( std::max(number,0UL), m_max_parallel_threads );
}

/// Interactor overload: handle Timer Sensor stimuli
void OnlineEventApp::handleTimer(long type)  {
  switch( type )  {
  case CMD_TIMER_DAQ_IDLE:
    if ( m_config->event_idle_timeout > 0 )  {
      std::time_t now = ::time(nullptr);
      if ( (now - m_event_time_last) > m_config->event_min_idle_time )  {
	fireIncident( "DAQ_IDLE_TIMEOUT" );
	m_event_time_last = now;
      }
      TimeSensor::instance().add(this, m_config->event_idle_timeout, CMD_TIMER_DAQ_IDLE);
    }
    break;
  default:
    break;
  }
}

/// Cancel the application: Cancel IO request/Event loop
int OnlineEventApp::cancel() {
  if ( m_events ) m_events->cancel();
  return 1;
}

/// Un-declare all monitoring items registered
void OnlineEventApp::undeclareInfo( const string& nam ) {
  if ( m_monSvc ) m_monSvc->undeclareInfo( nam, m_config );
}

/// Internal: Initialize the application            (NOT_READY  -> READY)
int OnlineEventApp::configureApplication() {
  int ret = this->OnlineApplication::configureApplication();
  if ( ret == ONLINE_OK )   {
    SmartIF<ISvcLocator> sloc = app.as<ISvcLocator>();
    m_whiteboard              = sloc->service<IHiveWhiteBoard>( "EventDataSvc" );
    if ( !m_whiteboard )   {
      m_logger->error( "+++ Error retrieving EventDataSvc interface IHiveWhiteBoard." );
      return ONLINE_ERROR;
    }
    string tmp;
    SmartIF<IProperty>( m_whiteboard )->getProperty( "EventSlots", tmp ).ignore();
    m_max_parallel_threads = ::strtol( tmp.c_str(), nullptr, 10 );
    if ( 0 == m_max_parallel_threads && errno == EINVAL ) {
      m_logger->error( "+++ Failed to access number of event slots from IHiveWhiteBoard [%s].", tmp.c_str() );
      return ONLINE_ERROR;
    }
    m_parallel_threads = std::min( std::max(m_config->numEventThreads,1UL), m_max_parallel_threads );
    m_monitor.reset();
    if ( !m_config->monitorType.empty() ) {
      m_monBursts.reset( new Service( "Bursts", sloc ) );
      m_monEvents.reset( new Service( "Events", sloc ) );
      m_monSvc = sloc->service<IMonitorSvc>( m_config->monitorType );
      if ( !m_monSvc.get() ) {
        m_logger->error( "Cannot access monitoring service of type %s.", m_config->monitorType.c_str() );
        return ONLINE_ERROR;
      }
      m_monSvc->declareInfo( "IN",  m_monitor.burstsIn, "Number of bursts received", m_monBursts );
      m_monSvc->declareInfo( "OUT", m_monitor.burstsOut, "Number of bursts fully processed", m_monBursts );
      m_monSvc->declareInfo( "IN",  m_monitor.eventsIn, "Number of events received", m_monEvents );
      m_monSvc->declareInfo( "OUT", m_monitor.eventsProcessed, "Number of events fully processed", m_monEvents );
      m_monSvc->declareInfo( "Buffered", m_monitor.eventsBuffered, "Number of events awaiting processing", m_monEvents );
      m_monSvc->declareInfo( "Submitted", m_monitor.eventsSubmitted, "Number of events submitted", m_monEvents );
      m_monSvc->declareInfo( "Errors", m_monitor.eventsFailed, "Number of events with processing errors", m_monEvents );
      m_monSvc->declareInfo( "ConsecutiveErrors", m_monitor.consecutiveErrors,
                             "Number of events with consecutive processing errors", m_monEvents );
      m_monSvc->declareInfo( "NumberOfProcessingThreads", *(long*)&m_parallel_threads, "Number of processing threads", m_monEvents );
      m_monSvc->declareInfo( "NumberOfAccessThreads", *(long*)&m_event_access_threads_count, "Number event access threads", m_monEvents );
    }
  }
  return ret;
}

/// Internal: Finalize the application              (READY      -> NOT_READY)
int OnlineEventApp::finalizeApplication() {
  if ( m_monSvc.get() ) {
    m_monSvc->undeclareAll( m_monBursts );
    m_monSvc->undeclareAll( m_monEvents );
    m_monSvc->undeclareAll( m_config );
    m_monSvc.reset();
  }
  m_whiteboard.reset();
  m_monBursts.reset();
  m_monEvents.reset();
  return OnlineApplication::finalizeApplication();
}

/// Internal: Start the application                 (READY      -> RUNNING)
int OnlineEventApp::startApplication() {
  if ( m_eventAccess.empty() ) {
    m_halt = EVENTLOOP_EXECUTE;
    string input_type = RTL::str_upper( m_config->inputType );
    if ( input_type == "MBM" ) {
      m_events = start_mbm_access();
    }
    else if ( input_type == "NET" ) {
      m_events = start_net_access();
    }
    else if ( input_type == "FILE" ) {
      m_events = start_file_access();
    }
    else if ( input_type == "GENERIC" ) {
      m_events = start_generic_access();
    }
    else if ( input_type == "NONE" ) {
      int status         = OnlineApplication::startApplication();
      if ( ONLINE_OK != status ) {
	m_logger->error( "+++ Failed to start the Online Application." );
      }
      return status;
    }
    else {
      m_logger->error( "+++ Event I/O access mechanism is not defined. "
                       "Check option \"inputType\" of your application: \"%s\"",
                       input_type.c_str() );
      return ONLINE_ERROR;
    }
    if ( !m_events ) {
      m_logger->error( "+++ Event I/O access mechanism not properly initialized. "
                       "Check configuration for input type: \"%s\"",
                       input_type.c_str() );
      return ONLINE_ERROR;
    }
    /// Data integrity
    m_events->setBurstPrintCount( m_config->burstPrintCount );
    m_events->setVerifyBanks( m_config->verifyBanks );

    /// Configure preselection using event header
    m_events->setHeaderVetoMask( m_config->headerVetoMask );
    m_events->setHeaderTriggerMask( m_config->headerTriggerMask );

    /// Configure TAE handling
    m_events->setExpandTAE( m_config->expandTAE );
    m_events->setOnlyTAE( m_config->onlyTAE );
    m_events->setIgnoreTAE( m_config->ignoreTAE );
    m_events->setDropEdgesTAE( m_config->dropEdgesTAE );
    m_events->setConvertEdgesTAE( m_config->convertEdgesTAE );

    m_monitor.reset();
    m_ioSvc->access    = m_events;
    m_ioSvc->expandTAE = m_config->expandTAE;
    int status         = OnlineApplication::startApplication();
    if ( ONLINE_OK != status ) {
      m_logger->error( "+++ Failed to start the Online Application." );
      return status;
    }
    m_logger->info( "+++ Concurrency information:  [Exec mode:%d]", m_config->execMode );
    m_logger->info( "+++ o TBB thread pool size:      %d / %ld  [max: %ld]", m_config->numEventThreads,
		    m_parallel_threads, m_max_parallel_threads );
    m_logger->info( "+++ o Number of event slots:     %ld  Low/High mark: %ld / %ld", m_whiteboard->getNumberOfStores(),
		    m_config->lowMark, m_config->highMark );
    SmartIF<IEventProcessor> ep{app};
    if ( m_config->execMode == 1 && SmartIF<IQueued>( ep ).get() ) {
      for ( size_t i = 0; i < m_config->numStatusThreads; ++i ) {
        m_eventStatus.emplace_back( new thread( &OnlineEventApp::queued_event_result, ref( *this ) ) );
        pthread_setname_np( m_eventStatus.back()->native_handle(),
                            ( std::string( "eventStatus_" ) + std::to_string( i ) ).c_str() );
      }
    }
    /// Configure event loop thread
    m_eventLoop.reset( new thread( &OnlineEventApp::event_loop, ref( *this ) ) );
    pthread_setname_np( m_eventLoop->native_handle(), "eventLoop" );

    /// Configure data access threads
    for ( size_t i = 0; i < m_config->mbm.num_event_threads; ++i ) {
      m_eventAccess.emplace_back( make_unique<thread>( &OnlineEventApp::event_access, ref( *this ), i ) );
      pthread_setname_np( m_eventAccess.back()->native_handle(),
                          ( std::string( "eventAccess_" ) + std::to_string( i ) ).c_str() );
    }

    /// Start timer to act on event idle timeouts
    if ( m_config->event_idle_timeout > 0 )   {
      TimeSensor::instance().add(this, m_config->event_idle_timeout, CMD_TIMER_DAQ_IDLE);
    }

#if 0
    static bool first = true;
    if ( first )   {
      static sa_sigaction new_act;
      static sa_sigaction old_act;
      first = false;
      new_act.sa_handler   = nullptr;
      new_act.sa_sigaction = segv_handler;
      new_act.sa_flags     = SA_SIGINFO;
      new_act.sa_mask      = 0;
      new_act.sa_restorer  = nullptr;
      sigemptyset(&new_act.sa_mask);
      sigaddset(&new_act.sa_mask,SIGSEGV);

      if ( 0 != ::sigaction(SIGSEGV, &new_act, &old_act) )   {
	m_logger->error("+++ Failed to install SEGV handler.");
      }
    }
#endif
    return status;
  }
  return m_logger->error( "+++ Inconsistent thread state! [FSM failure]" );
}

/// Configure MBM event access
OnlineEventApp::access_t OnlineEventApp::start_generic_access()  {
  typedef Gaudi::PluginService::Factory<EventAccess*( unique_ptr<RTL::Logger> &&, const Configuration* )> _fac;
  string type_name = m_config->generic.factory;
  auto logger = m_logger->clone( "GenInput", m_config->IO_output_level );
  unique_ptr<EventAccess> acc = _fac::create( type_name, std::move(logger), m_config);
  return acc;
}

/// Configure MBM event access
OnlineEventApp::access_t OnlineEventApp::start_mbm_access()  {
  auto mbm    = make_shared<MBMEventAccess>( m_logger->clone( "MBMInput", m_config->IO_output_level ) );
  mbm->config = m_config->mbm;
  for ( size_t i = 0; i < m_config->mbm.num_connections; ++i )  {
    stringstream nam;
    nam << m_name << '.' << i;
    int sc = mbm->connect( m_config->mbm.input, m_config->mbm.requests, nam.str() );
    if ( ONLINE_OK != sc ) {
      m_logger->error( "+++ Failed to connect MBM client %s.", nam.str().c_str() );
      return nullptr;
    }
  }
  return mbm;
}

/// Configure network based event access
OnlineEventApp::access_t OnlineEventApp::start_net_access() {
  auto net    = make_shared<NetEventAccess>( m_logger->clone( "NetworkInput", m_config->IO_output_level ) );
  net->config = m_config->net;
  int sc      = net->connect( m_config->net.requests );
  if ( ONLINE_OK != sc ) {
    m_logger->error( "+++ Failed to connect Network client %s.", m_config->net.source.c_str() );
    return nullptr;
  }
  return net;
}

/// Configure file based event access
OnlineEventApp::access_t OnlineEventApp::start_file_access() {
  auto file    = make_shared<FileEventAccess>( m_logger->clone( "FileInput", m_config->IO_output_level ) );
  file->config = m_config->file;
  int sc       = file->connect( m_config->file.requests );
  if ( ONLINE_OK != sc ) {
    m_logger->error( "+++ Failed to connect FILE reader client." );
    return nullptr;
  }
  return file;
}

/// Pause the application                            (RUNNING    -> READY)
int OnlineEventApp::pauseProcessing() {
  // Noop: we want to empty the event pipeline and run until a "stop" appears....
  m_logger->debug( "+++ Pause the application." );
  // m_halt = EVENTLOOP_PAUSE;
  return OnlineApplication::pauseProcessing();
}

/// Continue the application                        (PAUSED -> RUNNING )
int OnlineEventApp::continueProcessing() {
  m_logger->debug( "+++ Resume application processing." );
  m_halt = EVENTLOOP_EXECUTE;
  return OnlineApplication::continueProcessing();
}

/// Stop the application                             (RUNNING    -> READY)
int OnlineEventApp::stop() {
  this->fireIncident( "DAQ_CANCEL" );
  if ( m_events ) { m_events->cancel(); }
  m_halt = EVENTLOOP_STOP;
  for ( auto& t : m_eventAccess ) stop_thread( t );
  m_eventAccess.clear();
  stop_thread( m_eventLoop );
  /// Event loop finally finished. Declare it as stopped and wait until
  /// the status thread has finished.
  m_halt = EVENTLOOP_STOPPED;
  for ( auto& t : m_eventStatus ) stop_thread( t );
  m_eventStatus.clear();
  m_ioSvc->access.reset();
  if ( m_events ) {
    m_events->close();
    m_events.reset();
  }
  m_halt = EVENTLOOP_ENDED;
  return OnlineApplication::stop();
}

void OnlineEventApp::async_queued_event_loop() {
  typedef tbb::global_control control_t;
  size_t                      num_threads = m_parallel_threads;
  EventAccess::shared_guard_t event_burst;
  SmartIF<IQueued>            ev_que{app};

  // from global_control_t documentation:
  // max_allowed_parallelism -- Limits total number of worker threads that can
  // be active in the task scheduler to parameter_value - 1. That's why we pass
  // num_threads + 1
  auto tbb_control = make_unique<control_t>(control_t::max_allowed_parallelism, num_threads + 1);
  if ( !ev_que.get() ) {
    m_logger->error( "+++ async_loop: No IQueuedEventProcessor interface accessible. EXITING Event loop." );
    return;
  }
  do {
    if ( num_threads > 0 ) {
      try {
        event_burst = m_events->waitBurst();
        if ( event_burst ) {
	  size_t nevt = m_ioSvc->push( std::move( event_burst ) );
          m_monitor.burstsOut = m_events->monitor.burstsOut;
          for ( size_t i = 0; i < nevt; ++i ) {
            EventContext ctx( ev_que->createEventContext() );
            // m_logger->error("+++ Submit event %ld out of %ld", i, nevt);
            ev_que->push( std::move( ctx ) );
            ++m_monitor.eventsSubmitted;
	    m_event_time_last = ::time(0);
	    m_monitor.eventsBuffered = m_events->eventsBuffered() + (nevt-1) - i;

	    /// Check if the scenario changed and we have to adapt the number of threads executing
	    if ( num_threads != m_parallel_threads ) {
	      num_threads = m_parallel_threads;
	      m_logger->info( "+++ async_loop: Restart event processing with %ld threads.", num_threads );
	      tbb_control.reset(new control_t(control_t::max_allowed_parallelism, num_threads + 1));
	    }
          }
	  m_monitor.eventsBuffered = m_events->eventsBuffered();
        } else if ( num_threads == m_parallel_threads && !m_events->isCancelled() ) {
          continue;
        }
        this_thread::sleep_for( chrono::milliseconds( 10 ) );
      }
      catch ( const exception& e ) {
        m_logger->error( e, "+++ async_loop: Event loop failure with exception." );
      }
      catch ( ... ) {
	m_logger->error( "+++ async_loop: Event loop failure with UNKNOWN exception" );
      }
    } else {
      this_thread::sleep_for( chrono::milliseconds( 1000 ) );
    }
    /// Check if the scenario changed and we have to adapt the number of threads executing
    if ( num_threads != m_parallel_threads ) {
      // First wait until all events in the queue finished processing
      //while ( !ev_que->empty() ) this_thread::sleep_for( chrono::milliseconds( 10 ) );
      num_threads = m_parallel_threads;
      m_logger->info( "+++ async_loop: Restart event processing with %ld threads.", num_threads );
      tbb_control.reset(new control_t(control_t::max_allowed_parallelism, num_threads + 1));
    }

    if ( m_events->eventsBuffered() > 0 ) { continue; }
    this_thread::sleep_for( chrono::milliseconds( 1000 ) );
    // Check if we got some late frame(s)
    if ( m_events->eventsBuffered() > 0 ) { continue; }
    if ( m_events->isCancelled() ) {
      m_logger->debug( "+++ async_loop: Event requests were cancelled." );
      break;
    }
    if ( m_halt == EVENTLOOP_PAUSE && ev_que->empty() ) {
      m_logger->info( "+++ async_loop: PAUSE processing. "
		      "Events processed: %ld submitted: %ld Empty: %s",
		      m_monitor.eventsProcessed,
		      m_monitor.eventsSubmitted,
		      RTL::yes_no(ev_que->empty()));
      m_halt = EVENTLOOP_STOP;
    }
  } while ( !( m_halt == EVENTLOOP_STOP ) || !m_events->empty() || !ev_que->empty() );

  while ( !ev_que->empty() ) // this is our "threads.join()" alternative
    this_thread::sleep_for( chrono::milliseconds( 100 ) );
  tbb_control.reset();
  if ( m_events->isCancelled() ) {
    event_burst.reset();
    m_logger->info( "+++ async_loop: Event loop exiting [CANCELLED]." );
  } else {
    m_logger->info( "+++ async_loop: Event loop exiting." );
  }
  autoStop();
}

/// Synchronous event loop
void OnlineEventApp::sync_queued_event_loop() {
  EventAccess::shared_guard_t event_burst;
  SmartIF<IQueued>            ev_que{app};

  if ( !ev_que.get() ) {
    m_logger->error( "+++ No IEventProcessor interface accessible. EXITING Event loop." );
    return;
  }
  do {
    try {
      event_burst = m_events->waitBurst();
      if ( event_burst ) {
        m_monitor.burstsOut = m_events->monitor.burstsOut;
        for ( size_t nevt = m_ioSvc->push( std::move( event_burst ) ), i = 0; i < nevt; ++i ) {
          EventContext ctx( ev_que->createEventContext() );
          // m_logger->error("+++ Submit event %ld out of %ld", i, nevt);
          ev_que->push( std::move( ctx ) );
          ++m_monitor.eventsSubmitted;
	  m_event_time_last = ::time(0);
          m_monitor.eventsBuffered = m_events->eventsBuffered() + nevt - i;
          auto result              = ev_que->pop();
          for ( ; !result; result = ev_que->pop() ) this_thread::sleep_for( chrono::milliseconds( 2 ) );
          auto&& [sc, context] = std::move( *result );
          if ( !sc.isSuccess() ) { m_logger->error( "+++ Failed to process event." ); }
          ++m_monitor.eventsProcessed;
        }
        continue;
      }
      this_thread::sleep_for( chrono::milliseconds( 10 ) );
    }
    catch ( const exception& e ) {
      m_logger->error( e, "+++ Event loop failure with exception." );
    }
    catch ( ... ) {
      m_logger->error( "+++ Event loop failure with UNKNOWN exception" );
    }
    // this_thread::sleep_for(chrono::milliseconds(100));
  } while ( ( m_halt != EVENTLOOP_STOP ) );
  //
  if ( m_events->isCancelled() ) {
    event_burst.reset();
    m_logger->info( "+++ Event loop exiting [CANCELLED]." );
  }
  autoStop();
}

/// Synchronous event loop
void OnlineEventApp::sync_event_loop() {
  EventAccess::shared_guard_t event_burst;
  SmartIF<IEventProcessor>    ev_que{app};

  if ( !ev_que.get() ) {
    m_logger->error( "+++ No IEventProcessor interface accessible. EXITING Event loop." );
    return;
  }
  do {
    try {
      event_burst = m_events->waitBurst();
      if ( event_burst ) {
        m_monitor.burstsOut = m_events->monitor.burstsOut;
        for ( size_t nevt = m_ioSvc->push( std::move( event_burst ) ), i = 0; i < nevt; ++i ) {
          EventContext ctx( ev_que->createEventContext() );
          ctx.setSlot( m_whiteboard->allocateStore( ctx.evt() ) );
          // m_logger->error("+++ Submit event %ld out of %ld", i, nevt);
          ++m_monitor.eventsSubmitted;
	  m_event_time_last = ::time(0);
          StatusCode sc = ev_que->executeEvent( std::move( ctx ) );
          if ( !sc.isSuccess() ) {}
          m_monitor.eventsBuffered = m_events->eventsBuffered() + nevt - i;
          ++m_monitor.eventsProcessed;
        }
        continue;
      }
      this_thread::sleep_for( chrono::milliseconds( 10 ) );
    }
    catch ( const exception& e )   {
      m_logger->error( e, "+++ Event loop failure with exception." );
    }
    catch ( ... )   {
      m_logger->error( "+++ Event loop failure with UNKNOWN exception" );
    }
  } while ( ( m_halt != EVENTLOOP_STOP ) );

  m_logger->info( "Event loop exiting [CANCELLED]." );
  autoStop();
}

/// Main function running the synchronous event processing loop
void OnlineEventApp::event_loop() {
  SmartIF<IProperty> prop = app.as<IProperty>();
  Gaudi::setAppReturnCode( prop, Gaudi::ReturnCode::Success, true ).ignore();
  m_monitor.reset();
  this->m_event_time_last = ::time(0);
  this->fireIncident( "DAQ_START_TRIGGER" );
  if ( m_config->execMode == 0 ) {
    sync_event_loop();
  } else if ( m_config->execMode == 1 ) {
    async_queued_event_loop();
  } else if ( m_config->execMode == 2 ) {
    sync_queued_event_loop();
  }
  this->fireIncident( "DAQ_END_EVENT" );
  this->fireIncident( "DAQ_STOP_TRIGGER" );
}

void OnlineEventApp::queued_event_result() {
  SmartIF<IQueued> ev_que{app};
ReStart:
  try {
    do {
      std::size_t evts_buffered = this->m_events->eventsBuffered();
      if ( this->m_halt == EVENTLOOP_PAUSE && ev_que->empty() && 0 == evts_buffered )   {
	this->m_logger->debug( "+++ queued_event_result: No event queued. Application pausing.");
	this->m_halt = EVENTLOOP_STOP;
	this->fireIncident( "DAQ_PAUSE" );
	break;
      }
      else if ( auto result = ev_que->pop() ) {
        auto&& [sc, ctx] = std::move( *result );
        ++this->m_monitor.eventsProcessed;
        if ( sc.isSuccess() && this->m_monitor.consecutiveErrors > 0 ) {
          this->m_monitor.consecutiveErrors = 0;
          // Reset the application return code.
          // Isolated errors shouldn't mean that the process exit code is non-zero.
          // TODO Such problematic events should be monitored and ideally stored.
          SmartIF<IProperty> prop = app.as<IProperty>();
          Gaudi::setAppReturnCode( prop, Gaudi::ReturnCode::Success, true ).ignore();
        }
	else if ( !sc.isSuccess() ) {
          SmartIF<IProperty> prop       = app.as<IProperty>();
          const auto&        p          = prop->getProperty( "ReturnCode" );
          const auto&        returnCode = dynamic_cast<const Gaudi::Property<int>&>( p );
          if ( returnCode.value() != 0 ) {
            // Add here more sophisticated error handling based on further information
            m_logger->error( "+++ Application processing error: ApplicationMgr has a non-zero returncode: %d",
                             returnCode.value() );
          }
          ++this->m_monitor.eventsFailed;
          ++this->m_monitor.consecutiveErrors;
          if ( this->m_monitor.consecutiveErrors > this->m_config->maxConsecutiveErrors ) {
            m_logger->fatal( "+++ Application processing halted: "
                             "Too many consecutive processing errors:  %ld > %ld",
                             this->m_monitor.consecutiveErrors, this->m_config->maxConsecutiveErrors );
            m_events->cancel();
            this->m_halt = EVENTLOOP_STOPPED;
            fireIncident( "DAQ_ERROR" );
	    while ( !ev_que->empty() || (this->m_events->eventsBuffered() > 0) )   {
	      result = ev_que->pop();
	      if ( sc.isSuccess() && this->m_monitor.consecutiveErrors > 0 ) {
		this->m_monitor.consecutiveErrors = 0;
		break;
	      }
	    }
	    if ( this->m_monitor.consecutiveErrors > this->m_config->maxConsecutiveErrors ) {
	      break;
	    }
          }
        }
        continue;
      }
      this_thread::sleep_for( chrono::microseconds( 100 ) );
    } while ( !ev_que->empty() ||
	      ( this->m_events->eventsBuffered() > 0 ) ||
	      ( this->m_halt != EVENTLOOP_STOPPED ) );
  } catch ( const exception& e ) {
    this->m_logger->error( "+++ queued_event_result: Event result exception: %s", e.what() );
    goto ReStart;
  } catch ( ... ) {
    this->m_logger->error( "+++ queued_event_result: UNKNOWN Event result exception." );
    goto ReStart;
  }
  this->m_logger->debug( "+++ queued_event_result: Leaving event status loop.");
}

/// Main function running the event buffer manager
void OnlineEventApp::event_access( size_t instance_num ) {
  bool load_events = true;
  int sc = ONLINE_NO_EVENT;
  SmartIF<IQueued> ev_que{app};
  Increment inc( m_event_access_threads );
  while ( 1 ) {
    m_event_access_threads_count = m_event_access_threads;
    try {
      if ( EVENTLOOP_PAUSE == m_halt ) {
        // If end of the event loop. Must be followed by a "stop"
        ::lib_rtl_sleep( 1000 );
        continue;
      } else if ( EVENTLOOP_STOP == m_halt ) {
        // If end of the event loop. Must be followed by a "stop"
        break;
      } else if ( 0 == m_parallel_threads ) {
        // If there are no workers, there is no need to take events out of the pipeline
        ::lib_rtl_sleep( 1500 );
        continue;
      } else if ( m_events->eventsBuffered() > m_config->highMark ) {
        ::lib_rtl_usleep( 10 );
        continue;
      } else if ( m_events->eventsBuffered() > m_config->lowMark ) {
        ::lib_rtl_usleep( 1 );
        continue;
      }
      /// Fill events into the cache
      if ( load_events )  {
	sc = m_events->fill_cache();
	m_monitor.burstsIn = m_events->monitor.burstsIn;
	m_monitor.eventsIn = m_events->monitor.eventsIn;
      }

      if ( sc == ONLINE_CANCELLED ) {
        // If end of the event loop must be followed by a "stop"
        break;
      } else if ( m_config->enablePause && sc == ONLINE_END_OF_DATA ) {
        // End of the event loop: no more data
	m_logger->debug( "+++ event_access: Set PAUSE flag. "
			 "Events processed: %ld submitted: %ld Empty: %s",
			 m_monitor.eventsProcessed, m_monitor.eventsSubmitted,
			 RTL::yes_no(ev_que->empty()));
        m_halt = EVENTLOOP_PAUSE;
        break;
      } else if ( sc == ONLINE_END_OF_DATA ) {
        // End of the event loop: no more data
	std::size_t evts_buffered = this->m_events->eventsBuffered();
	load_events = false;
	if ( m_config->dropPipeline || (evts_buffered == 0 && ev_que->empty()) )   {
	  m_halt = EVENTLOOP_STOP;
	  break;
	}
      } else if ( EVENTLOOP_PAUSE == m_halt ) {
        // If end of the event loop must be followed by a "stop"
        continue;
      } else if ( EVENTLOOP_STOP == m_halt ) {
        // If end of the event loop must be followed by a "stop"
        break;
      } else if ( sc == ONLINE_NO_EVENT ) {
	// No event present in the output queue to be acknowledged
        ::lib_rtl_usleep( 100 );
        continue;
      } else if ( sc == ONLINE_NOT_INITED ) {
	/// Event access not initialized. Error condition --> break
        m_logger->error( "+++ Event access NOT initialized!" );
        this->OnlineApplication::error();
        break;
      }
    }
    catch ( const exception& e ) {
      m_logger->error( "+++ Event access exception: %s", e.what() );
    }
    catch ( ... ) {
      m_logger->error( "+++ UNKNOWN Event access exception." );
    }
  }
  m_logger->debug( "+++ Event access shutdown [%ld].", instance_num );
  m_event_access_threads_count = m_event_access_threads - 1;
}
