//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Project    : Online
//
//  Author     : Markus Frank
//==========================================================================

#ifndef GAUDIONLINE_ONLINEEVENTAPP_H
#define GAUDIONLINE_ONLINEEVENTAPP_H

/// Framework include files
#include "IOService.h"
#include <EventHandling/EventAccess.h>
#include <GaudiKernel/EventContext.h>
#include <GaudiKernel/IHiveWhiteBoard.h>
#include <GaudiKernel/IMonitorSvc.h>
#include <GaudiOnline/OnlineApplication.h>

/// C/C++ include files
#include <atomic>
#include <memory>
#include <mutex>
#include <thread>

/// Forward declarations

/// Online namespace declaration
namespace Online {

  /// Basic steering plugin for online application having an event loop
  /** @class OnlineEventApp OnlineEventApp.h
   *
   *
   * \author  M.Frank
   * \version 1.0
   * \date    25/04/2019
   */
  class OnlineEventApp : public OnlineApplication {
  public:
    typedef std::unique_ptr<std::thread> thread_t;
    typedef std::shared_ptr<EventAccess> access_t;
    typedef std::lock_guard<std::mutex>  lock_t;

    enum EventLoopState {
      EVENTLOOP_EXECUTE = 1 << 0,
      EVENTLOOP_PAUSE   = 1 << 1,
      EVENTLOOP_STOP    = 1 << 2,
      EVENTLOOP_STOPPED = 1 << 3,
      EVENTLOOP_ENDED   = 1 << 4
    };
    /// Forward declaration to the DIM command changing the number of processing threads
    class instance_command_t;

    /// Handle to the main event loop thread
    thread_t m_eventLoop;
    /// Handles to the event processing result collectors
    std::vector<thread_t> m_eventStatus;
    /// Handles to the event input data collectors
    std::vector<thread_t> m_eventAccess;
    /// Event access object
    access_t m_events;
    /// Event loop state
    EventLoopState m_halt = EVENTLOOP_STOPPED;
    /// Handles to helper service to properly name burst counters
    SmartIF<IService> m_monBursts;
    /// Handles to helper service to properly name event counters
    SmartIF<IService> m_monEvents;
    /// Reference to the IO service
    SmartIF<IOService> m_ioSvc;
    /// Reference to the monitoring service
    SmartIF<IMonitorSvc> m_monSvc;
    /// Reference to the white board
    SmartIF<IHiveWhiteBoard> m_whiteboard;
    /// DIM command service to change the maximum number of events to be processed in parallel
    std::unique_ptr<instance_command_t> m_parallel_dim_svc;
    /// Time stamp of the last event processed
    std::time_t m_event_time_last = 0;
    /// Maximum number of parallel event processing threads within the hive
    std::size_t m_max_parallel_threads = 0;
    /// Number of parallel event processing threads within the hive
    std::size_t m_parallel_threads = 0;
    /// Number of existing cores on the current system.
    std::size_t m_num_cores = 0;
    /// Monitoring quantity for 'm_event_access_threads'
    std::size_t m_event_access_threads_count {0};
    /// True thread safe counter for number of event access threads
    std::atomic<size_t> m_event_access_threads{0};

    /// Structurte containing all monitoring items
    struct monitor_t {
      long burstsIn          = 0;
      long burstsOut         = 0;
      long eventsIn          = 0;
      long eventsBuffered    = 0;
      long eventsSubmitted   = 0;
      long eventsProcessed   = 0;
      long eventsFailed      = 0;
      long consecutiveErrors = 0;
      monitor_t()            = default;
      virtual ~monitor_t()   = default;
      void reset();
    } m_monitor;

    /** Declare named monitoring information items
     * @param nam    Monitoring information name known to the external system
     * @param var    Monitoring variable (by reference!)
     * @param dsc    Description of monitoring variable
     */
    template <class T>
    void declareInfo( const std::string& nam, const T& var, const std::string& dsc ) const {
      if ( !m_monSvc.get() ) return;
      m_monSvc->declareInfo( nam, var, dsc, m_config.get() );
    }

    /** Undeclare monitoring information
     * @param nam    Monitoring information name known to the external system
     */
    virtual void undeclareInfo( const std::string& nam );

  public:
    /// Specialized constructor
    OnlineEventApp( Options opts );

    /// Default destructor
    virtual ~OnlineEventApp();

    /// Set the maximum number of parallel executing events
    void setMaxEventsParallel( size_t number );

    /// Interactor overload: handle Timer Sensor stimuli
    virtual void handleTimer(long type)  override;

    /// Cancel the application: Cancel IO request/Event loop
    virtual int cancel() override;

    /// Internal: Initialize the application            (NOT_READY  -> READY)
    virtual int configureApplication() override;
    /// Internal: Finalize the application              (READY      -> NOT_READY)
    virtual int finalizeApplication() override;

    /// Internal: Start the application                 (READY      -> RUNNING)
    virtual int startApplication() override;
    /// Stop the application                            (RUNNING    -> READY)
    virtual int stop() override;
    /// Pause the application                           (RUNNING    -> PAUSED)
    virtual int pauseProcessing() override;
    /// Continue the application                        (PAUSED -> RUNNING )
    virtual int continueProcessing() override;

    /// Configure MBM event access
    access_t start_mbm_access();
    /// Configure network based event access
    access_t start_net_access();
    /// Configure file based event access
    access_t start_file_access();
    /// Configure generic event access using factory
    access_t start_generic_access();

    /// Main function running the synchronous event processing loop
    void event_loop();
    /// Event loop for managers with IEventProcessor interface only and no IO service
    void sync_event_loop();
    /// Event loop for managers with IEventProcessor interface only using IO Service
    void sync_queued_event_loop();
    /// Event loop for managers with IQueuedEventProcessor interface using IO Service
    void async_queued_event_loop();
    /// Access event processing results for managers with IQueuedEventProcessor interface
    void queued_event_result();
    /// Main function running the event buffer manager
    void event_access( size_t instance_num );
  }; // class OnlineEventApp
} // namespace Online
#endif // GAUDIONLINE_ONLINEEVENTAPP_H
