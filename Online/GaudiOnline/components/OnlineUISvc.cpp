//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================

// Framework include files
#include <GaudiKernel/IIncidentListener.h>
#include <GaudiKernel/IIncidentSvc.h>
#include <GaudiKernel/IAlgorithm.h>
#include <GaudiKernel/IAlgManager.h>
#include <GaudiKernel/IProperty.h>
#include <GaudiKernel/Service.h>
#include <CPP/ObjectProperty.h>

/// C/C++ include files
#include <regex>

namespace rpc {
  class GaudiUI;
  /// Invoke the interpreter and interprete the command lines
  template <typename T> class PropertyUI;
}

/// Online namespace declaration
namespace Online  {
  class OnlineUISvc;

  /// Service to perform strategic sleeps during transitions.....
  /*
   *  \author Markus Frank
   *  \date   2015-10-13
   */
  class OnlineUISvc : public extends<Service,IIncidentListener>  {
    /// Friend declaration
    friend class rpc::PropertyUI<rpc::GaudiUI>;

  protected:
    /// Property: transition at which the publishing should start: "initialize" or "start"
    std::string  when{"initialize"};
    /// Property: UI RPC library name
    std::string  rpc_library_name;
    /// Property: UI RPC server type
    std::string  rpc_server_type;
    /// Property: Initialization arguments for the RPC server
    std::vector<std::string>  rpc_ui_args;
    /// Property: Blocked wild-card matches
    std::vector<std::string>  blocked;

    /// Reference to the Gaudi incident service
    SmartIF<IIncidentSvc>      incidentSvc;
    /// Data buffer for the property block
    std::vector<char>          propertyData;
    /// Blocked wild-card (regex) matches
    std::vector<std::regex>    blocked_regex;

    /// Reference to the UI interface instance
    std::unique_ptr<rpc::UI>    gaudi_ui;
    /// referebce to the RPC responsive interface facading the UI
    std::unique_ptr<rpc::RpcUI> rpc;

    /// Lock
    std::mutex   lock;
    /// RPC library handle
    void*        rpc_lib_handle { nullptr };
    /// Dim service ID for the properties publication block
    int          dimSvcID       { 0 };
    /// Dim service ID for the command service
    int          dimCmdID       { 0 };
    /// Dim buffer allocation reservation
    int          dimMemSize = 1024*1024*2;

    /// DIM callback to handle commands
    static void processRequest(void* tag, void* address, int* size);

    /// DIM callback to publish properties
    static void feedProperties(void* tag, void** address, int* size, int*);

    /// Handle command
    void handleCommand(char* data, size_t len);

    /// Handle manipulation of properties for WinCCOA
    void WinCCOASetProperties(char* address, size_t len);

  protected:
    /// Start all DIM publishing services
    void startServices();

    /// Start all DIM publishing services
    void stopServices();

  public:
    /// Cache of known properties.
    std::vector<IProperty*> knownComponents;

    /// Publish property collection to DIM in WinCCOA format
    void publishProperties();

  public:

    /// Standard constructor
    OnlineUISvc(const std::string& nam,ISvcLocator* pSvc);

    /// Destructor
    virtual ~OnlineUISvc();

    /// Initialize the service
    virtual StatusCode initialize()  override;

    /// Start the service
    virtual StatusCode start()  override;

    /// Stop the service
    virtual StatusCode stop()  override;

    /// Finalize the service
    virtual StatusCode finalize()  override;

    /// Incident handler implemenentation: Inform that a new incident has occured
    virtual void handle(const Incident& inc)  override;

    /// Update a given component property from the string representation
    virtual int _setProperty(Gaudi::Details::PropertyBase& property, const std::string& value);

    /// Update a given component property from the string representation
    virtual int _setProperty(IProperty* component, const std::string& property, const std::string& value);

    /// Update a given component property from the string representation
    virtual int _setProperty(const std::string& component, const std::string& property, const std::string& value);

    /// Collect all known components
    virtual std::vector<IProperty*> components()  const;

    /// Collect all known components, order them by name
    virtual std::map<std::string,IProperty*> ordered_components()  const;

    /// Forced update of the properties
    virtual int updateProperties();
  };
}

//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================

// Framework include files
#include <RTL/strdef.h>
#include <RTL/rtl.h>
#include <dim/dis.h>

#include <TInterpreter.h>
#include <sstream>
#include <map>

DECLARE_COMPONENT( Online::OnlineUISvc )

namespace  {

  /// Select dataflow component by name match
  class SelectIPropertyByName  {
  public:
    /// Reference to the selected component object
    IProperty*    selected = 0;
    /// Required component name
    std::string   name;

  public:
    /// Default constructor
    SelectIPropertyByName() = delete;
    /// Default copy constructor
    SelectIPropertyByName(const SelectIPropertyByName&) = delete;
    /// Default move constructor
    SelectIPropertyByName(const SelectIPropertyByName&&) = delete;
    /// Assignment operator
    SelectIPropertyByName& operator=(const SelectIPropertyByName&) = delete;
    /// Initializing constructor
    SelectIPropertyByName(const std::string& nam);
    /// Default destructor
    virtual ~SelectIPropertyByName() = default;
    /// Callback function. When component is selected, return negative value
    virtual long operator()(IProperty* p)   {
      SmartIF<INamedInterface> n(p);
      if ( n && n->name() == name )  {
	selected = p;
	return 1;
      }
      return 0;
    }
    int select(const std::vector<IProperty*>& props)   {
      int ret = 0;
      for ( auto* p : props )
	if ( (*this)(p) ) ++ret;
      return ret > 0 ? ret : -1;
    }
  };

  /// Initializing constructor
  inline SelectIPropertyByName::SelectIPropertyByName(const std::string& nam) : name(nam)
  {
  }
}

namespace rpc {

  class GaudiUI {
  public:
    Online::OnlineUISvc* service;
  };

  /// Invoke the interpreter and interprete the command lines
  template <> int PropertyUI<GaudiUI>::interpreteCommand(const std::string& cmd)  const  {
    if ( !cmd.empty() && gInterpreter != 0 )   {
      gInterpreter->ProcessLine(cmd.c_str());
      return 1;
    }
    return 0;
  }
  /// Invoke the interpreter and interprete the command lines
  template <> int PropertyUI<GaudiUI>::interpreteCommands(const std::vector<std::string>& cmds)  const  {
    int count = 0;
    if ( !cmds.empty() && gInterpreter != 0)   {
      for( const auto& c : cmds )  {
	gInterpreter->ProcessLine(c.c_str());
	++count;
      }
    }
    return count;
  }
  /// Access the hosted client services
  template <> UI::clients_t PropertyUI<GaudiUI>::clients()  const  {
    std::map<std::string,IProperty*> comp = type.service->ordered_components();
    clients_t ret;
    ret.reserve(comp.size());
    for(const auto& c : comp)
      ret.push_back(c.first);
    return ret;
  }

  /// Access all properties of an object
  template <> UI::properties_t PropertyUI<GaudiUI>::allProperties()  const   {
    properties_t props;
    const auto& blocked = type.service->blocked_regex;
    std::map<std::string,IProperty*> comp = type.service->ordered_components();
    for(const auto& c : comp)  {
      for(const auto* p : c.second->getProperties())  {
	bool block_prop = false;
	for( const auto& rg : blocked )  {
	  std::smatch sm;
	  std::string pnam = p->name();
	  bool stat = std::regex_match(pnam, sm, rg);
	  if ( stat )  {
	    block_prop = true;
	    break;
	  }
	}
	if ( !block_prop )   {
	  props.push_back(rpc::ObjectProperty(c.first,p->name(),p->toString()));
	}
      }
    }
    return props;
  }

  /// Access all properties with the same name from all clients
  template <> UI::properties_t PropertyUI<GaudiUI>::namedProperties(const std::string& nam)  const   {
    properties_t props;
    std::map<std::string,IProperty*> comp = type.service->ordered_components();
    for(const auto& c : comp)  {
      for(const auto* p : c.second->getProperties())  {
	if ( p->name() == nam)  {
	  props.push_back(rpc::ObjectProperty(c.first,p->name(),p->toString()));
	}
      }
    }
    return props;
  }

  /// Access all properties of one remote client (service, ect.)
  template <> UI::properties_t PropertyUI<GaudiUI>::clientProperties(const std::string& cl)  const   {
    SelectIPropertyByName selector(cl);
    if ( -1 != selector.select(type.service->knownComponents) )  {
      properties_t props;
      for(const auto* p : selector.selected->getProperties())  {
	props.push_back(rpc::ObjectProperty(cl,p->name(),p->toString()));
      }
      return props;
    }
    throw std::runtime_error(RTL::processName()+".clientProperties: Unknown client: "+cl);
  }

  /// Access a single property of an object
  template <> UI::property_t PropertyUI<GaudiUI>::property(const std::string& cl,
							   const std::string& nam)  const   {
    SelectIPropertyByName sel(cl);
    if ( -1 != sel.select(type.service->knownComponents) && sel.selected )  {
      for(const auto* p : sel.selected->getProperties())  {
	if ( p->name() == nam)  {
	  return rpc::ObjectProperty(cl,nam,p->toString());
	}
      }
    }
    throw std::runtime_error(RTL::processName()+".property: Unknown property: "+cl+"."+nam);
  }

  /// Modify a property
  template <> void PropertyUI<GaudiUI>::setProperty(const std::string& cl,
						    const std::string& nam,
						    const std::string& value)   {
    SelectIPropertyByName sel(cl);
    if ( -1 != sel.select(type.service->knownComponents) && sel.selected )  {
      type.service->_setProperty(sel.selected, nam, value);
      type.service->publishProperties();
      return;
    }
    throw std::runtime_error(RTL::processName()+".setProperty: Unknown property: "+cl+"."+nam);
  }

  /// Modify a property
  template <> void PropertyUI<GaudiUI>::setPropertyObject(const property_t& prop)  {
    type.service->_setProperty(prop.client, prop.name, prop.value);
  }
  
  /// Modify all properties in allclients matching the name
  template <> int  PropertyUI<GaudiUI>::setPropertiesByName(const std::string& nam,
							    const std::string& val)    {
    std::vector<IProperty*>& comp = type.service->knownComponents;
    int count = 0;
    for( auto c : comp )  {
      if ( type.service->_setProperty(c, nam, val) )   {
	++count;
      }
    }
    return count;
  }

  /// Modify a whole bunch of properties. Call returns the number of changes
  template <> int PropertyUI<GaudiUI>::setProperties(const properties_t& props)   {
    std::vector<IProperty*>&         comp = type.service->knownComponents;
    std::set<std::string>            miss_clients;
    std::map<std::string,IProperty*> update_clients;
    std::stringstream                err;
    int count = 0;
    
    err << RTL::processName() << ".setProperties: ";
    for(const auto& p : props)  {
      bool found = false;
      for(auto c : comp)  {
	SmartIF<INamedInterface> n(c);
	if ( n->name() == p.client )  {
	  update_clients.insert(make_pair(p.client,c));
	  found = true;
	  break;
	}
      }
      if ( !found ) miss_clients.insert(p.client);
    }
    for(const auto& c : miss_clients)
      err << "Options for client: " << c << " were not updated." << std::endl;
    for(const auto& c : update_clients)   {
      for(const auto& p : props)  {
	if ( p.client == c.first )  {
	  if ( type.service->_setProperty(c.second, p.name, p.value) == 1 )  {
	    ++count;
	    continue;
	  }
	  err << "Option update " << c.first << "." << p.name << " FAILED." << std::endl;
	}
      }
    }
    if ( count != (long)props.size() )   {
      throw std::runtime_error(err.str());
    }
    type.service->publishProperties();
    return count;
  }
}

  /// Standard constructor
Online::OnlineUISvc::OnlineUISvc(const std::string& nam,ISvcLocator* pSvc)
  : base_class(nam, pSvc)
{
  this->declareProperty("When",          this->when);
  this->declareProperty("DimMemSize",    this->dimMemSize);
  this->declareProperty("Blocked",       this->blocked);
  this->declareProperty("RpcServerType", this->rpc_server_type = "dimjsonrpc");
  this->declareProperty("RpcLibrary",    this->rpc_library_name = "libRPCServer.so");
  this->declareProperty("RpcUiArgs",     this->rpc_ui_args);
  this->blocked.push_back("Audit.(.*)");
  //this->SetErrorHandler(RootErrorHandler);
  auto ui = std::make_unique<rpc::PropertyUI<rpc::GaudiUI> >(rpc::GaudiUI{this});
  this->gaudi_ui.reset(ui.release());
}

/// Destructor
Online::OnlineUISvc::~OnlineUISvc()  {
  this->gaudi_ui.reset();
}

/// Incident handler implemenentation: Inform that a new incident has occured
void Online::OnlineUISvc::handle(const Incident& inc)     {
  this->info() << "Got incident: " << inc.source() << " of type " << inc.type() << endmsg;
  if ( inc.type() == "APP_RUNNING" || inc.type() == "START_UI" ) {
    this->knownComponents = this->components();
    this->startServices();
    this->publishProperties();
  }
  else if ( inc.type() == "APP_STOPPED" ) {
    this->knownComponents.clear();
    this->stopServices();
  }
  else if ( inc.type() == "DAQ_PAUSED" )  {
  }
  else if ( inc.type() == "DAQ_CANCEL" )  {
  }
}

/// Initialize the service
StatusCode Online::OnlineUISvc::initialize()   {
  StatusCode sc = this->Service::initialize();
  int regex_flags = std::regex_constants::icase | std::regex_constants::ECMAScript;

  if ( !sc.isSuccess() )  {
    this->error() << "Cannot initialize service base class." << endmsg;
    return sc;
  }
  this->incidentSvc = this->service("IncidentSvc", true);
  if ( !sc.isSuccess() )  {
    this->error() << "Cannot access incident service." << endmsg;
    return StatusCode::FAILURE;
  }
  this->knownComponents = this->components();
  if ( this->when == "initialize" )
    this->startServices();
  for( const auto& b : this->blocked )   {
    std::regex r(b, (std::regex_constants::syntax_option_type)regex_flags);
    this->blocked_regex.emplace_back(r);
  }
  this->incidentSvc->addListener(this, "APP_RUNNING");
  this->incidentSvc->addListener(this, "APP_STOPPED");
  return sc;
}

/// Start the service
StatusCode Online::OnlineUISvc::start()   {
  StatusCode sc = Service::start();
  this->knownComponents = components();
  if ( sc.isSuccess() && this->when == "start" )
    this->startServices();
  return sc;
}

/// Stop the service
StatusCode Online::OnlineUISvc::stop()   {
  if ( this->dimSvcID != 0 && this->when == "start" )   {
    this->knownComponents.clear();
    this->stopServices();
  }
  if ( incidentSvc )  {
    this->incidentSvc->removeListener(this);
    this->incidentSvc.reset();
  }
  return this->Service::stop();
}

/// Finalize the service
StatusCode Online::OnlineUISvc::finalize()   {
  this->knownComponents.clear();
  this->stopServices();
  this->incidentSvc.reset();
  return this->Service::finalize();
}
#include <dlfcn.h>
/// Start all DIM publishing services
void Online::OnlineUISvc::startServices()  {
  std::string svc;
  if ( this->dimCmdID == 0 )  {
    svc = RTL::processName() + "/properties/set";
    this->dimCmdID = ::dis_add_cmnd(svc.c_str(), "C", processRequest, (long)this);
  }
  if ( this->dimSvcID == 0 )  {
    svc = RTL::processName() + "/properties/publish";
    this->dimSvcID = ::dis_add_service(svc.c_str(), "C", 0, 0, feedProperties, (long)this);
  }
  if ( !this->rpc.get() )   {
    struct FuncPointer {
      union {
	void* ptr; 
	rpc::RpcUI* (*creator)(const std::string&, rpc::UI*, const std::vector<std::string>&); 
      } fptr;
      FuncPointer(void* _p) {  fptr.ptr = _p; }
    };
    if ( !this->rpc_lib_handle )   {
      rpc_lib_handle = ::dlopen(this->rpc_library_name.c_str(), RTLD_LAZY | RTLD_GLOBAL);
    }
    if ( rpc_lib_handle )   {
      FuncPointer fp(::dlsym(rpc_lib_handle, "create_rpc_ui"));
      if ( fp.fptr.creator )   {
	if ( this->rpc_server_type.substr(0,3) == "dim" )   {
	  if ( this->rpc_ui_args.empty() ) this->rpc_ui_args.push_back(RTL::processName());
	  if ( this->rpc_ui_args.size() < 2 ) this->rpc_ui_args.push_back(std::getenv("DIM_DNS_NODE"));
	  if ( this->rpc_ui_args.size() < 3 ) this->rpc_ui_args.push_back("/JSONRPC");
	}
	this->rpc.reset( (fp.fptr.creator)(this->rpc_server_type, this->gaudi_ui.get(), this->rpc_ui_args) );
	if ( !rpc )   {
	  warning() << "Failed to create RPC server of type: " << this->rpc_server_type << endmsg;
	}
      }
      else   {
	warning() << "Failed to access RPC creator." << endmsg;
      }
    }
    else   {
      warning() << "Failed to access RPC creator." << endmsg;
    }
  }
  //svc = RTL::processName();
  //::dis_start_serving_dns(::dis_add_dns(dns.c_str()::dim_get_dns_port()), svc.c_str());
  info() << "Succesfully initialized UI services: Cmd: " << this->dimCmdID
	 << " Svc: " << this->dimSvcID << " rpc: " << RTL::processName() << endmsg;
}

/// Start all DIM publishing services
void Online::OnlineUISvc::stopServices()  {
  info() << "Stopping UI services: Cmd: " << this->dimCmdID
	 << " Svc: " << this->dimSvcID << endmsg;
  this->rpc.reset();
  if ( this->dimCmdID != 0 )  {
    ::dis_remove_service(this->dimCmdID);
    this->dimCmdID = 0;
  }
  if ( this->dimSvcID != 0 )  {
    ::dis_remove_service(this->dimSvcID);
    this->dimSvcID = 0;
  }
}

/// Collect all known components
std::vector<IProperty*> Online::OnlineUISvc::components()  const   {
  std::set<IProperty*>    comp2;
  std::vector<IProperty*> comp;
  std::vector<IToolSvc*>  tools;
  const std::list<IService*>& svcs = this->serviceLocator()->getServices();
  for(auto* s : svcs)    {
    SmartIF<IProperty> p(s);
    SmartIF<IToolSvc>  t(s);
    if ( p ) comp.push_back(p);
    if ( t && s->name() == "ToolSvc" ) tools.push_back(t);
  }
  SmartIF<IAlgManager> mgr(this->serviceLocator());
  if ( mgr )  {
    const auto& algs = mgr->getAlgorithms();
    for(auto* a : algs)    {
      SmartIF<IProperty> p(a);
      if ( p ) comp.push_back(p);
    }
  }
  for(auto* ts : tools)    {
    const auto tv = ts->getTools();
    for(auto* t : tv)    {
      SmartIF<IProperty> p(t);
      if ( p ) comp.push_back(p);
    }
  }
  return comp;
}

/// Collect all known components, order them by name
std::map<std::string,IProperty*> Online::OnlineUISvc::ordered_components()  const   {
  std::map<std::string,IProperty*> ret;
  std::vector<IProperty*> comp = knownComponents;
  info() << "OnlineUISvc::ordered_components: Got " << comp.size() << " property clients" << endmsg;
  for(IProperty* c : comp)   {
    SmartIF<INamedInterface> n(c);
    auto r = ret.emplace(n->name(), c);
    if ( !r.second )   {
      warning() << "Component " << n->name() << " is already in the map!!!!!" << endmsg;
    }
  }
  for(const auto& c : ret)   {
    SmartIF<INamedInterface> n(c.second);
    info() << "RET: OnlineUISvc::ordered_components: " <<  c.first << " -- " << n->name() << endmsg;
  }
  return ret;
}

/// Forced update of the properties
int Online::OnlineUISvc::updateProperties()    {
  std::vector<char> data;
  std::string value;
  int num_prop = 0;
  data.reserve(dimMemSize);

  std::vector<IProperty*>& comp = knownComponents;
  for(IProperty* c : comp )   {
    SmartIF<INamedInterface> n(c);
    if ( n )  {
      const auto& props = c->getProperties();
      for(const auto* p : props)  {
	bool block_prop = false;
	for( const auto& rg : this->blocked_regex )  {
	  std::smatch sm;
	  std::string pnam = p->name();
	  bool stat = std::regex_match(pnam, sm, rg);
	  if ( stat )  {
	    block_prop = true;
	    break;
	  }
	}
	if ( !block_prop )   {
	  value = n->name() + "/" + p->name() + " " + p->toString();
	  std::copy(value.begin(),value.end(),back_inserter(data));
	  data.push_back(0);
	  ++num_prop;
	}
      }
    }
  }
  debug() << "Publishing " << num_prop << " properties with DIM" << endmsg;
  propertyData = std::move(data);
  return 1;
}

/// Publish property collection to DIM in WinCCOA format
void Online::OnlineUISvc::publishProperties()    {
  if ( 0 != this->dimSvcID )   {
    std::lock_guard<std::mutex> lck(lock);
    this->updateProperties();
    ::dis_update_service(this->dimSvcID);
  }
}

/// Update a given component property from the string representation
int Online::OnlineUISvc::_setProperty(const std::string& component, const std::string& property, const std::string& value)   {
  SelectIPropertyByName selector(component);
  if ( -1 != selector.select(knownComponents) )
    return _setProperty(selector.selected, property, value);
  return 0;
}

/// Update a given component property from the string representation
int Online::OnlineUISvc::_setProperty(Gaudi::Details::PropertyBase& property, const std::string& value)   {
  return property.fromString(value).getCode();
}

/// Update a given component property from the string representation
int Online::OnlineUISvc::_setProperty(IProperty* component, const std::string& property, const std::string& value)   {
  auto& props = component->getProperties();
  for( auto* p : props )  {
    if ( p->name() == property )
      return _setProperty(*p, value);
  }
  return 0;
}

/// DIM callback to publish properties
void Online::OnlineUISvc::feedProperties(void* tag, void** address, int* size, int*)   {
  static std::string empty = "";
  if ( tag && address && size )  {
    OnlineUISvc* ui = *(OnlineUISvc**)tag;
    if ( ui )   {
      *address = (void*)&ui->propertyData[0];
      *size    = ui->propertyData.size();
      return;
    }
  }
  *address = (void*)empty.c_str();
  *size    = 0;
}

/// DIM callback to handle commands
void Online::OnlineUISvc::processRequest(void* tag, void* address, int* size)   {
  if ( tag && address && size )  {
    int len = *size;
    OnlineUISvc* m = *(OnlineUISvc**)tag;
    if ( m && len>0 )   {
      m->handleCommand((char*)address, len);
    }
  }
}

/// Handle command
void Online::OnlineUISvc::handleCommand(char* address, size_t len)   {
  std::string cmd = address;
  if ( cmd == "setProperties" )   {
    std::size_t cmd_len = cmd.length()+1;
    this->WinCCOASetProperties(address+cmd_len, len-cmd_len);
    this->publishProperties();
  }
  else if ( cmd.substr(0,::strlen("publishProperties")) == "publishProperties" )   {
    this->publishProperties();
  }
  else  {
    this->error() << "Received unknown DIM command: " << cmd << endmsg;
  }
}

/// Handle manipulation of properties for WinCCOA
void Online::OnlineUISvc::WinCCOASetProperties(char* address, size_t len)   {
  std::lock_guard<std::mutex> lck(this->lock);
  char* p = address, *e = p+len, *n, *v, *c;
  while(p < e)  {
    c = p;
    n = ::strchr(c,'/');
    if ( n )  {
      *n = 0;
      ++n;
      v = ::strchr(n,' ');
      if ( v ) {
	*v = 0;
	++v;
	p = v + ::strlen(v) + 1;
	if ( this->_setProperty(c, n, v) == 1 )   {
	  this->info() << "Successfully updated option: " << c << "." << n << " = " << v << endmsg;
	  continue;
	}
	this->error() << "Failed to update option: " << c << "." << n << " = " << v << endmsg;
      }
    }
    else   {
      this->error() << "Failed to update option: " << c << "  [Bad formatted command. Unknwon failure]" << endmsg;
      return;
    }
  }
}
