//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Project    : Online
//
//  Author     : Markus Frank
//==========================================================================

/// Framework include files
#include "EventProcessor.h"
#include <GaudiKernel/IIncidentSvc.h>
#include <GaudiKernel/DataObjectHandle.h>
#include <GaudiKernel/IIncidentListener.h>
#include <EventHandling/FileEventOutput.h>
#include <EventHandling/MBMEventOutput.h>

/// C/C++ include files
#include <random>

/// Forward declarations
namespace LHCb { class RawBank;  }

using RawBanks = std::vector<std::pair<const LHCb::RawBank*, const void*> >;

/// Online namespace declaration
namespace Online  {

  /// Online algorithm to feed the TES with the basic raw event.
  /**
    *
    * \author  M.Frank
    * \version 1.0
    * \date    25/04/2019
    */
  class OutputAlg : public extends<EventProcessor, IIncidentListener>   {
  protected:
    /// Property: Output type MBM, FILE, NET
    Gaudi::Property<std::string>          m_outputType{this,     "OutputType",    "MBM", "Output type"};
    /// Property: Directory to search for vectors of raw banks to output TAE from TES
    Gaudi::Property<std::string>          m_bankDir   {this,     "BankDirectory", "Banks","Directory to place raw banks"};
    /// Property: Whether to use RawData for the central crossing for TAE
    Gaudi::Property<bool>                 m_centralFromRawData{this, "CentralFromRawData", false};
    /// Property: Location of TAEHalfWindow value (if not present, this is not a TAE event)
    Gaudi::Property<std::string>          m_taeHalfWindow{this,  "TAEHalfWindow", "Banks/TAEHalfWindow"};
    /// Property: Output buffer name
    Gaudi::Property<std::string>          m_mbm_buffer{this,     "MBM_buffer",    "Output", "Output buffer name"};
    /// Property: Number of output connections
    Gaudi::Property<size_t>               m_mbmConnections{this, "MBM_numConnections", 1, ""};
    /// Property: Output file naming convention
    Gaudi::Property<std::string>          m_fileNameFormat{this, "FILE_nameFormat", "Output_%CONNECTION_%Y-%m-%d_%H-%M-%S.raw", ""};
    /// Property: Number of output connections
    Gaudi::Property<size_t>               m_fileConnections{this,"FILE_numConnections", 1, ""};
    /// Property: Maximum number of events to write
    Gaudi::Property<long>                 m_maxEvents{this,"MaxEvents", -1, "Number of events to output (-1: all)"};

    /// Property: Forced output trigger mask; if empty will not be applied
    Gaudi::Property<std::vector<int> >    m_routingMask{this, "RoutingMask", {}, "Forced output trigger mask"};

    /// Property: Map <bit number, probability> to enforce routing bit distribution. Gets auto-normalized.
    Gaudi::Property<std::vector<std::vector<double> > >
                                          m_acceptMask{this, "AcceptMask", {}, "Map <bit number, probability> to enforce routing bit distribution"};

    /// Property: Maximal number of events per transaction
    size_t                                m_max_events_tr {  1000 };
    /// Property: Use raw event
    bool                                  m_useRawData    {  true };
    /// Property: Use raw data descriptor
    bool                                  m_useRawGuard   { false };
    /// Property: Use TAE leaves from TES
    bool                                  m_useTAELeaves  { false };
    /// Property: Output MDF format (instead of PCIE40 MEPs)
    bool                                  m_output_mdf    {  true };
    /// Property: Require ODIN to output MDF format
    bool                                  m_requireODIN   { false };

    /// Raw event location in TES
    DataObjectReadHandle<RawBanks>        m_rawData{ this, "RawData",  "Banks/RawData"};
    /// Data guard location in TES
    DataObjectReadHandle<evt_desc_t>      m_rawGuard{this, "RawGuard", "Banks/RawDataGuard"};

    /// Reference to output writer
    std::shared_ptr<EventOutput>          m_output;
    /// Reference to the incident service
    SmartIF<IIncidentSvc>                 m_incident;
    /// MBM output parameters
    MBMEventOutput::mbm_config_t          mbm;
    /// FILE output parameters
    FileEventOutput::file_config_t        file;
    /// Secondary transaction lock
    mutable std::mutex                    m_tr_lock;
    /// Number of events written to output
    mutable std::atomic<long>             m_numEventsWritten;
    /// Number of events which failed to be written
    mutable std::atomic<long>             m_numEventsFailed;

    std::map<size_t, double>              m_acceptBitMask;
    double                                m_acceptBitTotal { 0e0 };

    bool                                  m_cancelled { false };

    struct MaskGenerator  {
      const OutputAlg* alg { nullptr };
      mutable std::mutex                 lock;
      mutable std::default_random_engine generator;
      MaskGenerator(const OutputAlg* a) : alg(a) {}
      std::vector<int> get()  const;
    };
    MaskGenerator m_mask_generator    { this };

    void output_mdf_pcie40(const pcie40::event_t& evt,
			   const std::vector<int>& routing_mask,
			   EventOutput&            output)  const;

    /// Flush data caches if not empty
    bool flush_caches();

  public:
    /// Default constructor
    OutputAlg(const std::string& nam, ISvcLocator* loc);
    /// Default destructor
    virtual ~OutputAlg() = default;
    /// Algorithm overload: Initialize the algorithm
    virtual StatusCode initialize()   override;
    /// Algorithm overload: Finalize the algorithm
    virtual StatusCode finalize()   override;
    /// Algorithm overload: Start the algorithm
    virtual StatusCode start()   override;
    /// Algorithm overload: Stop the algorithm
    virtual StatusCode stop()    override;
    /// Algorithm overload: Execute single event
    virtual StatusCode process(EventContext const& evtCtx) const override;
    /// IIncidentListener overload: incident callback handler
    virtual void handle(const Incident& incident)  override;
  };
}

/// Framework include files
#include <GaudiKernel/SmartIF.h>
#include <GaudiKernel/AnyDataWrapper.h>
#include <GaudiKernel/IDataManagerSvc.h>
#include <Gaucho/IGauchoMonitorSvc.h>

#include <RTL/strdef.h>
#include <RTL/bits.h>

// C/C++ include files

using namespace Online;

/// Factory instantiation
DECLARE_COMPONENT( OutputAlg )

std::vector<int> OutputAlg::MaskGenerator::get()  const  {
  if ( this->alg->m_acceptBitTotal > 0e0 )   {
    Bits::BitMask<4> bits;
    double tot = 0e0, number = 0e0;
    std::uniform_real_distribution<double> distribution(0.0,this->alg->m_acceptBitTotal);
    {
      std::lock_guard<std::mutex> guard(this->lock);
      number = distribution(this->generator);
    }
    for( const auto& c : this->alg->m_acceptBitMask )  {
      tot += c.second;
      if ( number <= tot )  {
	bits.set(c.first);
	break;
      }
    }
    return {int(bits.word(0)), int(bits.word(1)), int(bits.word(2)), ~0x0};
  }
  return this->alg->m_routingMask;
}

/// Default constructor
OutputAlg::OutputAlg(const std::string& nam, ISvcLocator* loc)
: extends::extends(nam, loc)
{
  declareProperty("UseRawData",              m_useRawData);
  declareProperty("UseRawGuard",             m_useRawGuard);
  declareProperty("UseTAELeaves",            m_useTAELeaves);
  declareProperty("OutputMDF",               m_output_mdf);
  declareProperty("RequireODIN",             m_requireODIN);
  declareProperty("MaxEventsPerTransaction", m_max_events_tr);

  declareProperty("MBM_allocationSize",      mbm.allocation_size);
  declareProperty("MBM_burstPrintCount",     mbm.burstPrintCount);
  declareProperty("MBM_transactionWaitTMO",  mbm.transitionWaitTMO);
  declareProperty("MBM_maxEvents",           mbm.max_events);
  declareProperty("MBM_useEventMask",        mbm.useEventMask);

  declareProperty("MBM_partitionName",       mbm.partitionName);
  declareProperty("MBM_partitionID",         mbm.partitionID);
  declareProperty("MBM_partitionBuffers",    mbm.partitionBuffers);
  declareProperty("MBM_eventType",           mbm.event_type);
  declareProperty("MBM_maxConsumerWait",     mbm.max_consumer_wait);

  declareProperty("FILE_allocationSize",     file.allocation_size);
  declareProperty("FILE_burstPrintCount",    file.burstPrintCount);
  declareProperty("FILE_transactionWaitTMO", file.transitionWaitTMO);
  declareProperty("FILE_maxEvents",          file.max_events);
  declareProperty("FILE_useEventMask",       file.useEventMask);

  declareProperty("FILE_maxFileSize",        file.maxfileSize);
}

/// IIncidentListener overload: incident callback handler
void OutputAlg::handle(const Incident& incident)      {
  if ( incident.type() == "DAQ_STOPPED" )   {
    this->stop().ignore();
  }
  else if ( incident.type() == "DAQ_CANCEL" )   {
    this->m_cancelled = true;
    if ( this->m_output.get() )  {
      //this->m_output->cancel();
    }
  }
  else if ( incident.type() == "DAQ_END_EVENT" )   {
    if( this->flush_caches() )  {
      this->m_logger->info("+++ Flushed all pending data  [output-end-event].");
    }
  }
  else if ( incident.type() == "DAQ_IDLE_TIMEOUT" )  {
    if( this->flush_caches() )  {
      this->m_logger->info("+++ Flushed all pending data  [output-idle].");
    }
  }
}

/// Flush data caches if not empty
bool OutputAlg::flush_caches()   {
  if( this->m_output.get() )  {
    if( !this->m_output->empty() )  {
      this->m_output->commit_all();
      return true;
    }
  }
  return false;
}

/// Initialize the algorithm
StatusCode OutputAlg::initialize() {
  this->EventProcessor::initialize().ignore();
  this->m_acceptBitTotal = 0e0;
  for (auto& a : this->m_acceptMask.value() )  {
    this->m_acceptBitMask[(size_t)a[0]] = a[1];
    this->m_acceptBitTotal += a[1];
  }
  this->m_incident = service<IIncidentSvc>("IncidentSvc");
  if ( !this->m_incident )   {
    this->m_logger->error("+++ Failed to access Incident service.");
    return StatusCode::FAILURE;
  }
  SmartIF<IGauchoMonitorSvc> m(this->monitorSvc());
  this->m_numEventsWritten = 0;
  this->m_numEventsProcessed = 0;
  if ( m )   {
    m->declareInfo("NumEventsWritten",   this->m_numEventsWritten,
		   "Number of events written to output", this);
    m->declareInfo("NumEventsFailed",    this->m_numEventsFailed,
		   "Number of events failed to be written", this);
  }
  this->m_incident->addListener(this,"DAQ_END_EVENT");
  this->m_incident->addListener(this,"DAQ_STOPPED");
  this->m_incident->addListener(this,"DAQ_CANCEL");
  this->m_incident->addListener(this,"DAQ_IDLE_TIMEOUT");
  this->m_logger->info("+++ Successfully initialized output algorithm.");
  this->m_cancelled = false;
  return StatusCode::SUCCESS;
}

/// Finalize the algorithm
StatusCode OutputAlg::finalize() {
  this->m_incident->removeListener(this);
  this->m_incident.reset();
  this->m_cancelled = false;
  if ( this->m_numEventsWritten > 0 )   {
    std::size_t num_wrt = this->m_numEventsWritten;
    std::size_t num_err = this->m_numEventsFailed;
    this->m_logger->info("+++ Wrote %ld events with %ld errors.", num_wrt, num_err);
  }
  return this->EventProcessor::finalize();
}

/// Start the algorithm
StatusCode OutputAlg::start()   {
  this->EventProcessor::start().ignore();
  auto typ = RTL::str_upper(this->m_outputType);
  if ( typ == "MBM" )  {
    auto* out = new MBMEventOutput(this->m_logger->clone(name(),this->msgLevel()));
    out->config = mbm;
    out->config.max_events = this->m_max_events_tr;
    for(size_t i=0; i<m_mbmConnections; ++i)
      out->connect(m_mbm_buffer, i);
    this->m_output.reset(out);
  }
  else if ( typ == "FILE" || typ == "MDF" )  {
    auto* out = new FileEventOutput(this->m_logger->clone(this->name(),this->msgLevel()));
    out->config = file;
    out->config.max_events = this->m_max_events_tr;
    for(size_t i=0; i < this->m_fileConnections; ++i)
      out->connect(this->m_fileNameFormat, i);
    this->m_output.reset(out);
  }
  else  {
    this->m_logger->error("+++ No recognized output type specified: %s. --> Error",
			  this->m_outputType.value().c_str());
    return StatusCode::FAILURE;
  }
  this->m_cancelled = false;
  this->m_numEventsFailed = 0;
  this->m_numEventsWritten = 0;
  return StatusCode::SUCCESS;
}

/// Stop the algorithm
StatusCode OutputAlg::stop()   {
  if ( this->m_output )   {
    this->m_output->close();
  }
  this->m_output.reset();
  return this->EventProcessor::stop();
}

/// Execute single event
StatusCode OutputAlg::process(EventContext const& /* ctxt */)  const   {
  // Check if the output writing was already cancelled
  if ( this->m_cancelled )   {
    //m_logger->info("+++ Task cancelled. Skip output event.");
    //return StatusCode::SUCCESS;
  }
  // Check event limit:
  if ( m_maxEvents > 0 && m_numEventsWritten >= m_maxEvents )   {
    return StatusCode::SUCCESS;
  }
  // If enabled write data:
  if ( this->m_output_mdf )   {
    const evt_desc_t* event = this->m_rawGuard.get();
    bool        requireODIN = this->m_requireODIN;
    EventOutput&     output = *this->m_output.get();
    std::vector<int>   mask = this->m_mask_generator.get();
    if ( this->m_useTAELeaves )   {
      DataObject* pObj = nullptr;
      bool foundCentral = false;
      std::vector<std::pair<int, const evt_data_t*> > crossings;
      if ( this->eventSvc()->retrieveObject(this->m_taeHalfWindow.value() , pObj).isSuccess() ) {
	int hw  = static_cast<const AnyDataWrapper<unsigned int>*>(pObj)->getData();
	crossings.reserve(2*hw + 1);
	for ( int bx = -hw; bx <= hw; ++bx ) {
	  if ( bx == 0 && m_centralFromRawData.value() ) continue;
	  std::string suff = (bx < 0)?("/Prev" + std::to_string(-bx)):(bx > 0)?("/Next" + std::to_string(bx)):"/Central";
	  if ( this->eventSvc()->retrieveObject(this->m_bankDir.value() + suff, pObj).isSuccess() ) {
	    auto* wr = static_cast<const AnyDataWrapper<RawBanks>*>(pObj);
	    crossings.emplace_back(bx, reinterpret_cast<const evt_data_t*>(&wr->getData()));
	    foundCentral = foundCentral || (bx == 0);
	  }
	}
      }
      if ( !foundCentral )   {
        crossings.emplace_back(0, reinterpret_cast<const evt_data_t*>(m_rawData.get()));
      }
      std::lock_guard transaction_lock(m_tr_lock); // Should not be necessary
      this->output_mdf(crossings, mask, output);
      ++this->m_numEventsWritten;
      return StatusCode::SUCCESS;
    }
    else if ( m_useRawGuard )    {
      /// To be tested before use!
      const evt_desc_t* evt = m_rawGuard.get();
      std::lock_guard transaction_lock(m_tr_lock); // Should not be necessary
      this->output_mdf(*evt, mask, requireODIN, output);
      ++this->m_numEventsWritten;
      return StatusCode::SUCCESS;
    }
    else if ( m_useRawData )    {
      /// Please note: in this mode PCIE40 TAE events cannot be saved -- only the central event!
      const evt_data_t* evt = reinterpret_cast<evt_data_t*>(m_rawData.get());
      std::lock_guard transaction_lock(m_tr_lock); // Should not be necessary
      this->output_mdf(*evt, event->second->type(), mask, requireODIN, output);
      ++this->m_numEventsWritten;
      return StatusCode::SUCCESS;
    }
    else   {
      std::lock_guard transaction_lock(m_tr_lock); // Should not be necessary
      this->output_mdf(*event, mask, requireODIN, output);
      ++this->m_numEventsWritten;
      return StatusCode::SUCCESS;
    }
  }
  else   {
    const evt_desc_t* event = m_rawGuard.get();
    if ( !m_useRawData && event->second->type() == event_traits::tell40::data_type )   {
      std::lock_guard transaction_lock(m_tr_lock); // Should not be necessary
      this->output_pcie40(event->second.get(), *m_output);
      ++this->m_numEventsWritten;
      return StatusCode::SUCCESS;
    }
    else   {
      ++this->m_numEventsFailed;
      this->m_logger->except("+++ Invalid PCIE40 output conversion requested!");
    }
  }
  return StatusCode::SUCCESS;
}
