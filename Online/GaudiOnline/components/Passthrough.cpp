//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : GaudiOnline
//
//  Author     : Markus Frank
//==========================================================================

/// Framework include files
#include "EventProcessor.h"

/// Online namespace declaration
namespace Online {

  /// Online passthrough algorithm with acceptrate and basic trigger type selection
  /** 
    *
    * @author  M.Frank
    * @version 1.0
    * @date    25/04/2019
    */
  class Passthrough : public EventProcessor  {
  private:
    /// Property: Odin event type to be downscaled
    std::vector< int > m_downScale;
    /// Property: Odin trigger types to be filtered
    std::vector< int > m_triggerTypesToPass;
    /// Property: Odin calibration types to be downscaled
    std::vector< int > m_calibTypesToPass;
    /// Property: Percentage of events that should be passed
    double             m_rate;
    /// Property: Event execution delay in seconds
    long               m_delay;
    /// Property: Event execution delay in micro seconds
    long               m_muDelay;
    /// Property: Initialization delay in seconds
    long               m_initDelay;
    /// Property: Finalization delay in seconds
    long               m_finiDelay;
    /// Property: Start delay in seconds
    long               m_startDelay;
    /// Property: Stop delay in seconds
    long               m_stopDelay;
    /// Property: Flag to burn CPU rather than sleep in event delay
    long               m_burnCPU;

    /// Initial random number seed.
    unsigned int       m_seed;

    /// Input data handle
    DataObjectReadHandle<EventAccess::event_t>  m_rawGuard{this, "RawGuard", "Banks/RawDataGuard"};

  protected:
    void _halt(long micro_seconds, bool burn = false)   const;
    
  public:
    /// Standard constructor
    Passthrough( const std::string& name, ISvcLocator* pSvcLocator );
    /// Default Destructor
    virtual ~Passthrough() = default;
    /// Initialization
    virtual StatusCode initialize()  override {
      _halt(m_initDelay*1000000L);
      return this->EventProcessor::initialize();
    }
    /// Algorithm overload: Start the algorithm
    virtual StatusCode start() override;
    /// Stop
    virtual StatusCode stop()        override {
      _halt(m_stopDelay*1000000L);
      return this->EventProcessor::stop();
    }
    /// Finalization
    virtual StatusCode finalize()    override {
      _halt(m_finiDelay*1000000L);
      return this->EventProcessor::finalize();
    }
    /// Algorithm execution
    virtual StatusCode process(EventContext const& evtCtx)   const   override;
  };
}

/// Framework include files
#include <GaudiKernel/MsgStream.h>
#include <RTL/hash.h>
#include <RTL/rtl.h>

/// C/C++ header files
#include <cmath>
#include <sstream>

using namespace Online;

DECLARE_COMPONENT( Passthrough )

/// Initializing constructor
Passthrough::Passthrough( const std::string& name, ISvcLocator* pSvc) : EventProcessor(name , pSvc)
{
  declareProperty("TriggerTypesToPass", m_triggerTypesToPass,
		  "Trigger types to pass if not calibration trigger inhibit. (Run 1/2 Only)");
  declareProperty("CalibTypesToPass" ,  m_calibTypesToPass,
		  "Calibration trigger inhibits");

  declareProperty("DownscaleTriggerTypes", m_downScale,
		  "Downscaled ODIN trigger types");
  declareProperty("AcceptRate", m_rate=1.0,
		  "Fraction of the events allowed to pass the filter");

  /// Delay times during ramping up/down:
  declareProperty("InitializeDelay",  m_initDelay=0);
  declareProperty("StartDelay",       m_startDelay=0);
  declareProperty("StopDelay",        m_stopDelay=0);
  declareProperty("FinalizeDelay",    m_finiDelay=0);

  /// Delay times for event processing:
  declareProperty("BurnCPU",        m_burnCPU=0);
  declareProperty("DelayTime",      m_delay=0);
  declareProperty("MicroDelayTime", m_muDelay=0);
}

/// Pause function
void Passthrough::_halt(long mu_seconds, bool burn)   const  {
  if ( mu_seconds > 0 )   {
    if ( burn )   {
      struct timeval start, now;
      long usecs = 0;
      ::gettimeofday(&start, nullptr);
      now = start;
      do {
	::gettimeofday(&now, nullptr);
	usecs =
	  (long(now.tv_sec)*1000000L + long(now.tv_usec)) -
	  (long(start.tv_sec)*1000000L + long(start.tv_usec));
      } while ( usecs < mu_seconds );
      return;
    }
    ::lib_rtl_usleep(mu_seconds);
  }
}

/// Algorithm overload: Start the algorithm
StatusCode Passthrough::start() {
  m_seed = RTL::hash32((RTL::processName()+"@"+RTL::nodeName()).c_str());
  ::srand(m_seed);
  _halt(m_startDelay*1000000L);
  return this->EventProcessor::start();
}

/// Event callback
StatusCode Passthrough::process(EventContext const& /* ctxt */)   const {
  long delay = m_delay*1000000L+m_muDelay;
  const auto* raw = m_rawGuard.get();
  if ( !raw ) {
    std::stringstream str;
    str << " ==> No raw data at: " << m_rawGuard;
    m_logger->except(str.str());
  }

  /// If mothing is specified everything will be downscaled
  bool downscale = false;
  if ( m_downScale.empty() && m_triggerTypesToPass.empty() && m_calibTypesToPass.empty() )   {
    downscale = (m_rate < 1.0 && m_rate < (double(::rand()) / double(RAND_MAX)));
  }
  else  {
    if ( raw->second->type() == event_traits::tell1::data_type )   {
      const auto odin_bank = this->get_odin_bank(*raw);
      if ( !odin_bank.first )
	downscale = true;
      else   {
	int readoutType = 0;
	int triggerType = 0;
	int vsn = odin_bank.first->version();
	if ( vsn < 7 )    {
	  const auto* odin = reinterpret_cast<const run2_odin_t*>(odin_bank.second);
	  readoutType = odin->readout_type();
	  triggerType = odin->trigger_type();
	}
	else   {
	  const auto* odin = reinterpret_cast<const run3_odin_t*>(odin_bank.second);
	  readoutType = odin->calib_type();
	  triggerType = odin->event_type();
	}
	if ( type_found(triggerType, m_downScale) )
	  downscale = (m_rate < 1.0 && m_rate < (double(::rand()) / double(RAND_MAX)));
	if ( vsn < 7 )   {
	  /// Require a certain trigger type
	  if ( !downscale && !type_found(triggerType, m_triggerTypesToPass) )
	    downscale = true;
	  /// Require a certain calibration type
	  if ( !downscale && triggerType == OdinData::CalibrationTrigger &&
	       !type_found(readoutType, m_calibTypesToPass) )
	    downscale = true;
	}
	else   {
	  if ( type_found(triggerType, m_downScale) )
	    downscale = true;
	  /// Require certain calibration types
	  if ( !downscale && !type_found(readoutType, m_calibTypesToPass) )
	    downscale = true;
	}
      }
    }
    else if ( raw->second->type() == event_traits::tell40::data_type )   {
      const auto odin_bank = this->get_odin_bank(*raw);
      if ( !odin_bank.first )
	downscale = true;
      else   {
	const auto* odin = reinterpret_cast<const run3_odin_t*>(odin_bank.second);
	if ( type_found(odin->event_type(), m_downScale) )
	  downscale = true;
	/// Require certain calibration types
	if ( !downscale && !type_found(odin->calib_type(), m_calibTypesToPass) )
	  downscale = true;
      }
    }
  }
  _halt(delay, m_burnCPU);
  return (downscale) ? StatusCode::FAILURE : StatusCode::SUCCESS;
}
