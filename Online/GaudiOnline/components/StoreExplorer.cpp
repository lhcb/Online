//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : GaudiOnline
//
//  Author     : Markus Frank
//==========================================================================

/// Framework include files
#include <GaudiKernel/SmartIF.h>
#include <GaudiKernel/Algorithm.h>
#include <GaudiKernel/MsgStream.h>
#include <GaudiKernel/IRegistry.h>
#include <GaudiKernel/ObjectList.h>
#include <GaudiKernel/ObjectVector.h>
#include <GaudiKernel/SmartDataPtr.h>
#include <GaudiKernel/IOpaqueAddress.h>
#include <GaudiKernel/KeyedContainer.h>
#include <GaudiKernel/IDataManagerSvc.h>
#include <GaudiKernel/IDataProviderSvc.h>
#include <GaudiKernel/ObjectContainerBase.h>
#include <RTL/strdef.h>

// C/C++ include files
#include <numeric>

/// Online namespace declaration
namespace Online   {

  /**@class StoreExplorer
   *
   * Small algorith, which traverses the data store and
   * prints generic information about all leaves, which
   * can be loaded/accessed.
   *
   * @author:  M.Frank
   * @version: 1.0
   */
  class StoreExplorer : public Algorithm {

    Gaudi::Property<bool>        m_load{             this, "Load",             false, "Load non existing items" };
    Gaudi::Property<long>        m_print{            this, "PrintEvt",             1, "Limit printout to first N events" };
    Gaudi::Property<long>        m_printMissing{     this, "PrintMissing",         0, "Indicate if missing entities should be printed" };
    Gaudi::Property<double>      m_frequency{        this, "PrintFreq",          0.0, "Printout frequency" };
    Gaudi::Property<bool>        m_exploreRelations{ this, "ExploreRelations", false, "If relations should be followed" };
    Gaudi::Property<std::string> m_dataSvcName{      this, "DataSvc", "EventDataSvc", "Name of the data provider service" };
    Gaudi::Property<bool>        m_testAccess{       this, "TestAccess",       false, "Test access to objects (DataObject and ContainedObject)" };
    Gaudi::Property<bool>        m_accessForeign{    this, "AccessForeign",    false, "Indicate if foreign files should be opened" };
    Gaudi::Property<bool>        m_cleanTypeNames{   this, "CleanTypeNames",    true, "Remove unnecessary parts from the type name before printing" };

    /// Internal counter to trigger printouts
    long                         m_total = 0;
    /// Internal counter to adjust printout frequency
    long                         m_frqPrint = 0;
    /// Reference to data provider service
    SmartIF<IDataProviderSvc>    m_dataSvc;
    /// Name of the root leaf (obtained at initialize)
    std::string                  m_rootName;

  public:
    /// Inherited constructor
    using Algorithm::Algorithm;

    template <class T>
    std::string access( T* p ) {
      if ( !p ) return "Access FAILED.";
      std::string result = std::accumulate(std::begin( *p ), std::end( *p ), std::string{}, [&]( std::string s, typename T::const_reference i ) {
	return s + std::to_string( p->index( i ) ) + ":" + std::to_string( i->clID() ) + ",";
      } );
      return result.substr( 0, result.length() - 2 );
    }

    /// Print datastore leaf
    void printObj( IRegistry* pReg, std::vector<bool>& flg ) {
      auto& log = info();
      for ( size_t j = 1; j < flg.size(); j++ ) {
	if ( !flg[j - 1] && flg[j] )
	  log << "| ";
	else if ( flg[j] )
	  log << "  ";
	else
	  log << "| ";
      }
      log << "+--> " << pReg->name();
      if ( pReg->address() ) {
	log << " [Address: CLID=" << std::showbase << std::hex << pReg->address()->clID()
	    << " Type=" << (void*)pReg->address()->svcType() << "]";
      }
      else {
	log << " [No Address]";
      }
      DataObject* p = pReg->object();
      if ( p ) {
	try {
	  std::string typ = System::typeinfoName(typeid(*p));
	  if ( m_testAccess ) p->clID();
	  if ( m_cleanTypeNames )   {
	    typ = RTL::str_replace(typ, "std::", "");
	    auto idx = typ.find("AnyDataWrapper<");
	    if ( idx != std::string::npos )
	      typ = typ.substr(idx+15, typ.length()-15-1);
	    idx = typ.find(",allocator<");
	    if ( idx != std::string::npos )
	      typ = typ.substr(0, idx) + " >";
	  }
	  log << "  " << typ.substr( 0, 80 );
	}
	catch ( ... ) {
	  log << "Access test FAILED";
	}
      }
      else {
	log << "  (Unloaded) ";
      }
      ObjectContainerBase* base = dynamic_cast<ObjectContainerBase*>( p );
      if ( base ) {
	try {
	  int        numObj = base->numberOfObjects();
	  const CLID id     = p->clID();
	  log << " [" << numObj << "]";
	  if ( m_testAccess ) {
	    CLID idd = id >> 16;
	    switch ( idd ) {
	    case CLID_ObjectList >> 16: /* ObjectList    */
	      access( (ObjectList<ContainedObject>*)base );
	      break;
	    case CLID_ObjectVector >> 16: /* ObjectVector  */
	      access( (ObjectVector<ContainedObject>*)base );
	      break;
	    case ( CLID_ObjectVector + 0x00030000 ) >> 16: /* Keyed Map     */
	      access( (KeyedContainer<KeyedObject<int>, Containers::Map>*)base );
	      break;
	    case ( CLID_ObjectVector + 0x00040000 ) >> 16: /* Keyed Hashmap */
	      access( (KeyedContainer<KeyedObject<int>, Containers::HashMap>*)base );
	      break;
	    case ( CLID_ObjectVector + 0x00050000 ) >> 16: /* Keyed array   */
	      access( (KeyedContainer<KeyedObject<int>, Containers::Array>*)base );
	      break;
	    }
	  }
	} catch ( ... ) { log << "Access test FAILED"; }
      }
      log << endmsg;
    }

    /// Callback to explore single data entry in the TES
    void explore( IRegistry* pObj, std::vector<bool>& flg ) {
      printObj( pObj, flg );
      if ( pObj ) {
	auto mgr = eventSvc().as<IDataManagerSvc>();
	if ( mgr ) {
	  std::vector<IRegistry*> leaves;
	  StatusCode              sc   = mgr->objectLeaves( pObj, leaves );
	  const std::string*      par0 = nullptr;
	  if ( pObj->address() )  par0 = pObj->address()->par();
	  if ( sc.isSuccess() )  {
	    for ( auto i = leaves.begin(); i != leaves.end(); i++ ) {
	      const std::string& id = ( *i )->identifier();
	      DataObject*        p  = nullptr;
	      if ( !m_accessForeign && ( *i )->address() ) {
		if ( par0 ) {
		  const std::string* par1 = ( *i )->address()->par();
		  if ( par1 && par0[0] != par1[0] ) continue;
		}
	      }
	      if ( m_load ) {
		sc = eventSvc()->retrieveObject( id, p );
	      } else {
		sc = eventSvc()->findObject( id, p );
	      }
	      if ( sc.isSuccess() ) {
		if ( id != "/Event/Rec/Relations" || m_exploreRelations ) {
		  flg.push_back( i + 1 == leaves.end() );
		  explore( *i, flg );
		  flg.pop_back();
		}
	      }
	      else {
		flg.push_back( i + 1 == leaves.end() );
		printObj( *i, flg );
		flg.pop_back();
	      }
	    }
	  }
	}
      }
    }

    /// Initialize the algorithm
    StatusCode initialize() override {
      m_rootName.clear();
      m_dataSvc = service( m_dataSvcName, true );
      if ( !m_dataSvc ) {
	error() << "Failed to access service \"" << m_dataSvcName << "\"." << endmsg;
	return StatusCode::FAILURE;
      }
      auto mgr = m_dataSvc.as<IDataManagerSvc>();
      if ( !mgr ) {
	error() << "Failed to retrieve IDataManagerSvc interface." << endmsg;
	return StatusCode::FAILURE;
      }
      m_rootName = mgr->rootName();
      return StatusCode::SUCCESS;
    }

    /// Finalize the algorithm
    StatusCode finalize() override {
      m_dataSvc.reset();
      return StatusCode::SUCCESS;
    }

    /// Execute procedure of the algorithm
    StatusCode execute() override {
      SmartDataPtr<DataObject> root( m_dataSvc.get(), m_rootName );
      if ( ((m_print > m_total++) || (m_frequency * m_total > m_frqPrint)) && root ) {
	if ( m_frequency * m_total > m_frqPrint ) m_frqPrint++;
	std::string store_name = "Unknown";
	IRegistry*  pReg       = root->registry();
	if ( pReg ) {
	  auto isvc = SmartIF<IService>{ pReg->dataSvc() };
	  if ( isvc ) store_name = isvc->name();
	}
	info() << "+======== " << m_rootName
	       << "[" << std::showbase << std::hex << (unsigned long)root.ptr() << std::dec
	       << "@" << store_name << "]:  "
	       << " event: " << m_total << endmsg;
	std::vector<bool> flg( 2, true );
	explore( root->registry(), flg );
	return StatusCode::SUCCESS;
      }
      else if ( root ) {
	return StatusCode::SUCCESS;
      }
      error() << "Cannot retrieve \"/Event\"!" << endmsg;
      return StatusCode::FAILURE;
    }
  };
}

using namespace Online;
DECLARE_COMPONENT( StoreExplorer )
