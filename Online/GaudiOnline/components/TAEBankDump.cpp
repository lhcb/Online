//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : DAQ/MDF
//
//  Author     : Markus Frank
//==========================================================================

/// Include files from Gaudi
#include "EventProcessor.h"

/// C/C++ include files

/// Online namespace declaration
namespace Online  {

  /// Class to dump all banks of a given type for all TAE crossings
  /** Class to dump all banks of a given type for all TAE crossings
   *  Dump TAE bank from Tell1 input bank data
   *
   *  @author Markus Frank
   *  @date   2005-10-13
   */
  class TAEBankDump : public EventProcessor {

    /// Property: Type of banks to be dumped for all TAE crossings
    Gaudi::Property<int>        m_bank_type{ this, "BankType",  16,      "BankType: Type of bank to be dumped"};
    /// Property: Type of banks to be dumped for all TAE crossings
    Gaudi::Property<std::vector<int> > m_bank_types{ this, "BankTypes",  {},  "BankType: Types of bank to be dumped"};
    /// Property: Steer the dump of the data part of the banks
    Gaudi::Property<bool>       m_dump_data{ this, "DumpData",  true,    "Dump bank data"};
    /// Property: Input data handle
    DataObjectReadHandle<EventAccess::event_t>  m_rawGuard{this, "RawGuard", "Banks/RawDataGuard"};

    /// Access crossing name as string
    std::string bx_name(int32_t offset)  const  {
      std::stringstream str;
      if ( offset < 0 )
	str << "Prev" << offset;
      else if ( offset > 0 )
	str << "Next" << offset;
      else
	str << "Central";
      return str.str();
    }

  public:

    /// Use base class c'tors
    using EventProcessor::EventProcessor;
    
    /// Main execution callback
    StatusCode process(EventContext const& /* ctxt */) const override  {
      const evt_desc_t* evt = m_rawGuard.get();
      if ( evt )   {
	std::vector<int> types = m_bank_types.value();
	if ( types.empty() )   {
	  types.push_back(m_bank_type.value());
	}
	for( int btyp : types )   {
	  bank_types_t::BankType typ = (bank_types_t::BankType)btyp;
	  auto crossings = extract_raw_banks(*evt, typ);
	  if ( crossings.size() > 1 )  {
	    m_logger->always("+++ %s bank content for %ld TAE events: ",
			     event_print::bankType(typ).c_str(), crossings.size());
	  }
	  for( const auto& crossing : crossings )   {
	    const auto  bxid  = crossing.first;
	    const auto& banks = crossing.second;

	    if ( banks.size() > 0 )   {
	      m_logger->always("|   %-12s [crossing] ----> %ld %s bank%c.",
			       bx_name(bxid).c_str(), banks.size(),
			       event_print::bankType(typ).c_str(),
			       banks.size()<=1 ? ' ' : 's');

	      for( std::size_t i = 0; i < banks.size(); ++i )   {
		const auto [header, data] = banks[i];
		auto hdr    = event_print::bankHeader(header);
		auto lines  = event_print::bankData(header, data, 150);
		m_logger->always("|      Bank %ld: %s", i, hdr.c_str());
		for( const auto& l : lines )
		  m_logger->always("|      %s", l.c_str());
	      }
	    }
	  }
	}
      }
      return StatusCode::SUCCESS;
    }
  };
}

/// Declare factory for object creation
DECLARE_COMPONENT( Online::TAEBankDump )
