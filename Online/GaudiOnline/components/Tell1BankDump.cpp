//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : DAQ/MDF
//
//  Author     : Markus Frank
//==========================================================================

// Include files from Gaudi
#include <Gaudi/Algorithm.h>
#include <GaudiKernel/DataObjectHandle.h>
#include <EventData/bank_types_t.h>
#include <EventData/bank_header_t.h>
#include <Tell1Data/Tell1Decoder.h>

// C/C++ include files
#include <atomic>
#include <mutex>

/// Forward declarations
namespace LHCb { class RawBank;  }

/// Online namespace declaration
namespace Online  {

  /// Dump raw bank collection from input data in the online TES
  /** @class Tell1BankDump Tell1BankDump.cpp
   *
   *  @author Markus Frank
   *  @date   2005-10-13
   */
  class Tell1BankDump : public Gaudi::Algorithm {

    using evt_data_t  = std::vector<std::pair<const bank_header_t*, const void*> >;
    using lhcb_data_t = std::vector<std::pair<const LHCb::RawBank*, const void*> >;

    Gaudi::Property<bool>   m_full{      this, "FullDump",  false, "FullDump:  If true, full bank contents are dumped"};
    Gaudi::Property<bool>   m_dump{      this, "DumpData",  false, "DumpData:  If true, bank headers are dumped"};
    Gaudi::Property<bool>   m_summary{   this, "Summary",   true,  "Summary:   If true, print bank summary at stop"};
    Gaudi::Property<bool>   m_check{     this, "CheckData", false, "CheckData: If true, full bank contents are checked"};
    Gaudi::Property<int>    m_debug{     this, "Debug",     0,     "Number of events where all dump flags should be considered true"};
    Gaudi::Property<long>   m_maxEvents{ this, "MaxEvents", -1,    "Number of events to dump"};
    DataObjectReadHandle<lhcb_data_t>  m_rawData{this, "RawData", "DAQ/RawData"};

    /// Event counter
    mutable std::atomic<long>                      m_numEvent;             ///< Event counter
    mutable std::map<bank_types_t::BankType, long> m_bankCounters;         ///< Bank counter
    mutable std::mutex                             m_lock;                 ///< Bank counter lock

  public:
    using Algorithm::Algorithm;

    /// Algorithm initialization
    StatusCode start() override  {
      m_numEvent = 0;
      m_bankCounters.clear();
      return StatusCode::SUCCESS;
    }

    /// Algorithm initialization
    StatusCode finalize() override  {
      if ( m_summary )   {
	MsgStream logger(msgSvc(), name());
	logger << MSG::INFO;
	logger << "+----> Events total:" << std::left << std::setw(6) << long(m_numEvent)
	       << " Key: " << m_rawData.objKey() << endmsg;
	for( const auto& cnt : m_bankCounters )   {
	  auto btyp = cnt.first;
	  logger << "| Found a total of " << std::setw(8) << cnt.second 
		 << " bank(s) of type "   << std::setw(3) << btyp
		 << " (" << event_print::bankType(btyp) << ") "
		 << endmsg;
	}
	logger << std::flush;
      }
      m_numEvent = 0;
      m_bankCounters.clear();
      return StatusCode::SUCCESS;
    }

    /// Main execution callback
    StatusCode execute(EventContext const& /* ctxt */) const override  {
      MsgStream logger(msgSvc(), name());
      std::map<int, std::vector<std::pair<const bank_header_t*, const void*> > > banks;
      bool dmp = m_numEvent<m_debug || m_dump;
      bool chk = m_numEvent<m_debug || m_check;
      bool ful = m_numEvent<m_debug || m_full;
      DataObject* pObj = nullptr;
      const evt_data_t* raw = nullptr;      // = (evt_data_t*)m_rawData.get();

      if ( eventSvc()->retrieveObject(m_rawData.objKey(), pObj).isSuccess() )   {
	if ( m_maxEvents > 0 && m_numEvent >= m_maxEvents )   {
	  m_numEvent++;
	  return StatusCode::SUCCESS;
	}
	if ( typeid(*pObj) == typeid(AnyDataWrapper<evt_data_t>) )  {
	  const auto* p = (AnyDataWrapper<evt_data_t>*)pObj;
	  raw = &p->getData();
	}
	else if ( typeid(*pObj) == typeid(AnyDataWrapper<lhcb_data_t>) )  {
	  const auto* p = (AnyDataWrapper<lhcb_data_t>*)pObj;
	  raw = (evt_data_t*)&p->getData();
	}
	else   {
	  throw std::runtime_error("Wrong, unexpected datatype: "+
				   std::string(typeid(*pObj).name())+
				   " from "+m_rawData.objKey());
	}
      }
      else   {
	logger << MSG::ERROR << "Failed to access object: "
	       << m_rawData.objKey() << " from TES." << endmsg;
	return StatusCode::FAILURE;
      }
      logger << MSG::INFO;
      for ( const auto& dsc : *raw )   {
	banks[dsc.first->type()].emplace_back(dsc.first, dsc.second);
      }
      for ( const auto& dsc : banks )   {
        const auto& b = dsc.second;
	int btyp = dsc.first;
        int cnt, inc = (btyp == bank_types_t::Rich) ? 64 : 32;
        if ( b.size() > 0 )  {
          if ( dmp )  {
	    logger << MSG::INFO;
            logger << "+----> Event No:" << std::left << std::setw(6) << long(m_numEvent)
		   << " has " << std::setw(3) << b.size() << " bank(s) of type " << btyp
		   << " (" << event_print::bankType(btyp) << ") "
		   << " Key: " << m_rawData.objKey() << " "
		   << endmsg;
          }

          int k = 0;
	  logger << MSG::NIL;
	  {
	    std::lock_guard<std::mutex> guard(m_lock);
	    for(const auto& itB : b)  {
	      const bank_header_t* r = itB.first;
	      ++m_bankCounters[bank_types_t::BankType(r->type())];
	    }
	  }
          for(const auto& itB : b)  {
            const bank_header_t* r = itB.first;
            if ( dmp )   {
	      logger << MSG::INFO;
              logger << "+ Bank:  " << event_print::bankHeader(r) << endmsg;
            }
            if( ful ) {
	      bool hdr  = true;
	      auto lines = event_print::bankData(itB.second, r->size(), 10);
	      logger << MSG::INFO;
	      for( const auto& line : lines )   {
		logger << (hdr ? "| Data:  " : "|        ") << line << endmsg;
		hdr = false;
	      }
            }
            if( chk ) { // Check the patterns put in by RawEventCreator
              int kc = k;
              int ks = k+1;
	      logger << MSG::INFO;
              if ( r->type() != bank_types_t::DAQ )  {
		const int *p, *end;
                if ( r->size() != inc*ks )  {
                  logger << "!! Bad bank size:" << r->size() << " expected:" << ks*inc << endmsg;
                }
                if ( r->sourceID() != kc )  {
                  logger << "!! Bad source ID:" << r->sourceID() << " expected:" << kc << endmsg;
                }
                for(p=(int*)itB.second, end=p+(r->size()/sizeof(int)), cnt=0; p != end; ++p, ++cnt)  {
                  if ( *p != cnt )  {
                    logger << "!! Bad BANK DATA:" << *p << endmsg;
                  }
                }
                if ( cnt != (inc*ks)/int(sizeof(int)) )  {
                  logger << "!! Bad amount of data in bank:" << cnt << " word" << endmsg;
                }
              }
            }
          }
        }
      }
      logger    << std::flush;
      std::cout << std::flush;
      m_numEvent++;
      return StatusCode::SUCCESS;
    }
  };
}

DECLARE_COMPONENT( Online::Tell1BankDump )
