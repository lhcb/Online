//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : DAQ/MDF
//
//  Author     : Markus Frank
//==========================================================================

// Include files from Gaudi
#include <Gaudi/Algorithm.h>
#include <GaudiKernel/DataObjectHandle.h>
#include <EventHandling/EventAccess.h>

// C/C++ include files
#include <atomic>
#include <sstream>

/// Online namespace declaration
namespace Online  {

  /// Dump Tell1 event header from inout data
  /** @class Tell1HeaderDump Tell1HeaderDump.cpp
   *  Creates and fills dummy RawEvent
   *
   *  @author Markus Frank
   *  @date   2005-10-13
   */
  class Tell1HeaderDump : public Gaudi::Algorithm {
    typedef std::pair<size_t, EventAccess::shared_guard_t> evt_desc_t;

    DataObjectReadHandle<evt_desc_t>  m_raw{this, "RawGuard", "Banks/RawDataGuard"};
    mutable std::atomic<long> m_numEvent;  ///< Monitoring quantity: Event counter

  public:
    using Algorithm::Algorithm;

    /// Algorithm initialization
    StatusCode start() override  {
      m_numEvent = 0;
      return StatusCode::SUCCESS;
    }

    /// Main execution callback
    StatusCode execute(EventContext const& /* ctxt */) const override  {
      const auto* e = m_raw.get();
      if ( e->second )    {
	MsgStream info(msgSvc(),name());
	std::vector<std::string> lines;
	if ( e->second->type() == event_traits::tell40::data_type )
	  lines = event_print::headerData(e->second->at(e->first).tell40_event);
	else if ( e->second->type() == event_traits::tell1::data_type )
	  lines = event_print::headerData(e->second->at(e->first).tell1_event);
	else  {
	  std::stringstream str;
	  str << "Unknown Event type: " << e->second->type() << std::endl;
	  lines.emplace_back(str.str());
	}
	for( const auto& line : lines )
	  info << MSG::INFO << line << endmsg;
      }
      m_numEvent++;
      return StatusCode::SUCCESS;
    }
  };
}

DECLARE_COMPONENT( Online::Tell1HeaderDump )
