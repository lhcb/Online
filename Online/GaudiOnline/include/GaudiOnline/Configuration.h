//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Project    : Online
//
//  Author     : Markus Frank
//==========================================================================
#ifndef GAUDIONLINE_CONFIGURATION_H
#define GAUDIONLINE_CONFIGURATION_H

/// Framework include files
#include <EventHandling/MBMEventAccess.h>
#include <EventHandling/NetEventAccess.h>
#include <EventHandling/FileEventAccess.h>
#include <GaudiKernel/Service.h>


/// C/C++ include files

/// Forward declarations

/// Online namespace declaration
namespace Online  {

  /// Helper service to pass options to non-service clients
  /** @class Configuration Configuration.h
   *
   *
   * \author  M.Frank
   * \version 1.0
   * \date    25/04/2019
   */
  class Configuration : public Service  {
  public:
    /// Retrieve interface ID
    static const InterfaceID& interfaceID() {
      // Declaration of the interface ID.
      static const InterfaceID iid("Online::Configuration", 0, 0);
      return iid;
    }

  public:
    static constexpr std::size_t kByte = 1024;
    static constexpr std::size_t MByte = kByte*1024;
    static constexpr std::size_t GByte = MByte*1024;
    static constexpr std::size_t TByte = GByte*1024;

    /** Basic process steering  */
    /// Property: Debug flag
    bool                           debug;
    /// Property: Automatic startup flag
    bool                           autoStart;
    /// Property: Automatic stop flag
    bool                           autoStop;
    /// Property: Processing class (Default 1)
    int                            clazz;
    /// Property: Timeout to trigger event idle callbacks (negative: OFF)
    int                            event_idle_timeout  { -1 };
    /// Property: Timeout to trigger event idle callbacks (minimum: time to enable trigger and have events present)
    int                            event_min_idle_time { 300 };
    /// Property: Timeout before config to connect debugger (seconds)
    int                            debug_wait          { -1 };

    /// Property: Enable/disable PAUSE transition using incident    (RUNNING -> PAUSED)
    int                            enablePause;
    /// Property: Enable/disable CONTINUE transition using incident (READY -> RUNNING)
    int                            enableContinue;
    /// Property: Enable/disable STOP transition using incident     (RUNNING -> READY)
    int                            enableStop;
    /// Property: Enable/disable ERROR transition using incident    (* -> ERROR)
    int                            enableError;
    /// Property: Execution mode definition
    int                            execMode;

    /// Property: Flag to dump options in the application constructor
    bool                           dump_options;
    /// Property: Numa node binding (Default: no binding)
    int                            numaNode;
    /// Property: Event IO Logger level
    int                            IO_output_level;
    /// Property: Maximum number of consecutive event processing errors
    long                           maxConsecutiveErrors;
    /// Property: Maximum number of parallel event threads
    size_t                         numEventThreads;
    /// Property: Maximum number of processing result threads
    size_t                         numStatusThreads;
    /// Property: Burst information printed every ... events
    size_t                         burstPrintCount;
    /// Property: Enable data verification for Tell1 banks
    bool                           verifyBanks       = true;
    /// Property: Drop pipeline on 'cancel/stop'. Neede for DAQ
    bool                           dropPipeline      = true;

    /** TAE handling  */
    /// Property: Expand TAE frames, otherwise handle as normal bx (default true)
    bool                           expandTAE         = true;
    /// Property: Ignore all TAE events to be passed to the user. Requires expandTAE=true
    bool                           ignoreTAE         = false;
    /// Property: Only select TAE events. Requires expandTAE=true
    bool                           onlyTAE           = false;
    /// Property: Drop edges at start/end of a MEP in case TAE handling is requested
    bool                           dropEdgesTAE      = true;
    /// Property: Convert edges at start/end of a MEP to single collisision entries if TAE
    bool                           convertEdgesTAE   = false;

    /// Property: Enable monitoring: monitor service name/type or disabled if empty
    std::string                    monitorType;
    /// Property: Event data input type (MBM. NET, FILE)
    std::string                    inputType;
    /// Property: Call ApplicationMgr::run() (Default: false / 0)
    std::string                    runable;
    /// Property: Logger device type
    std::string                    logDeviceType;
    /// Property: Logger device output format
    std::string                    logDeviceFormat;
    /// Property: Logger device (if empty ==> environment ${LOGFIFO})
    std::string                    logDevice;
    /// Property: Type/Name of the event loop/flow manager
    std::string                    flowManager = "Online::OnlineFlowMgr/EventLoop";
    /// Property: Name of service providing max number of threads
    std::string                    numThreadSvcName  {""};

    /** Event header based pre-selection for raw events: (monitoring, mbm, network, file), not PCIE40 */
    /// Property: Raw events with header: Check TRIGGER mask if not empty
    std::vector<uint32_t>          headerTriggerMask { };
    /// Property: Raw events with header: Check VETO mask if not empty
    std::vector<uint32_t>          headerVetoMask    { };

    /// Properties for the MBM input data handling:
    MBMEventAccess::mbm_config_t   mbm               { };

    /// Properties for the MBM input data handling:
    NetEventAccess::net_config_t   net               { };

    /// Properties for the FILE input data handling:
    FileEventAccess::file_config_t file              { };

    struct generic_config_t : public EventAccess::config_t  {
      /// Factory type name
      std::string  factory      { };
      /// TAE half window size
      std::size_t  halfWindow  { 0 };
      /// Property: Burst packingfactor
      int          packingFactor  = 1;
      /// Property: Buffer allocation space for bursts
      size_t       bufferSize     = 4*MByte;

      /// Default constructor
      generic_config_t() = default;
      /// Move constructor
      generic_config_t(generic_config_t&& copy) = default;
      /// Copy constructor
      generic_config_t(const generic_config_t& copy) = default;
      /// Default destructor
      ~generic_config_t() = default;
      /// Assignment operator
      generic_config_t& operator=(generic_config_t&& copy) = default;
      /// Move assignment operator
      generic_config_t& operator=(const generic_config_t& copy) = default;
    } generic;

    /** Properties for the event queue handling:      */
    /// Property: Low water mark of pre-cached events
    size_t                         lowMark;
    /// Property: High water mark of pre-cached events
    size_t                         highMark;

    int outputLevel()  const  {  return this->Service::outputLevel();  }

  public:
    /// Specialized constructor
    Configuration(std::string name, ISvcLocator* svcloc);
    /// Default destructor
    virtual ~Configuration();
    /// Query interfaces of Interface
    virtual StatusCode queryInterface(const InterfaceID& riid, void** ppvInterface) override;
    /// Wait for debugger being attached
    void waitDebugger();
  };   // class Configuration
}      // namespace Online
#endif // GAUDIONLINE_CONFIGURATION_H
