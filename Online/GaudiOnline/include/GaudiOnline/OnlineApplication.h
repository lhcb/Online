//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Project    : Online
//
//  Author     : Markus Frank
//==========================================================================
#ifndef GAUDIONLINE_ONLINEAPPLICATION_H
#define GAUDIONLINE_ONLINEAPPLICATION_H

/// Framework include files
#include "Configuration.h"

#include <Gaudi/Application.h>
#include <GaudiKernel/SmartIF.h>
#include <GaudiKernel/IIncidentSvc.h>
#include <GaudiKernel/IIncidentListener.h>

#include <CPP/Interactor.h>
#include <RTL/Logger.h>

/// Forward declarations
class DimCommand;
class DimService;

/// Online namespace declaration
namespace Online  {

  /// Basic steering plugin for online application steered by DIM
  /** @class OnlineApplication OnlineApplication.h
   *
   * The OnlineApplication is an object to encapsulate applications,
   * which execute the basic FSM state machinery given by
   * the FSMs in the HLT farm.
   *  
   * These FSMs look like this:
   *
   *
   *                     +----------+                        
   *                     |  OFFLINE |                      
   *                     +----------+                         
   *                         |  ^                  
   *           constructor() |  | unload()
   *                         |  |        
   *                         v  |                  
   *                     +----------+       +---------+
   *                     | NOT_READY|<------+  ERROR  |<------+
   *                     +----------+       +---------+       |
   *                         |  ^                             |
   *                         |  |                             |
   *              configure()|  |reset()              error() |
   *                         |  |                             |
   *                         |  |                             |
   *                         V  |                             |
   *        start()      +----------+                         |
   *      +--------------| READY    |                         |
   *      |              +----------+                         |
   *      |                  |  ^                             |
   *      V                  |  |                             |
   *  +---------+            |  |                             |
   *  | ACTIVE  |            |  |                             |
   *  +---------+            |  |                             |
   *      |                  |  |                             |
   *      |          start() |  | stop()                      |
   *      |                  V  |                             |
   *      | go()         +-----------+                        |
   *      +------------->| RUNNING   |------------------------+
   *                     +-----------+
   *                         |  ^                             
   *                         |  |                             
   *               pause()   |  |  continue()                             
   *                         |  |                             
   *                         V  |                             
   *                     +-----------+
   *                     | PAUSED    |
   *                     +-----------+
   *
   *
   * 
   * \author  M.Frank
   * \version 1.0
   * \date    25/04/2019
   */
  class OnlineApplication : public Gaudi::Application, public CPP::Interactor  {
  public:
    enum Transitions { 
      STARTUP_DONE=1,
      CONFIGURE,
      START,
      STOP,
      RESET,
      UNLOAD, 
      PAUSE,
      CONTINUE,
      ERROR,
      CANCEL=900,
      CONNECT_DIM=1000,
      FIRE_INCIDENT=1100,
      ONLINEAPP_USER_IOC=3000
    };
    enum State  {
      ST_OFFLINE   = 'U',
      ST_NOT_READY = 'N',
      ST_READY     = 'r',
      ST_RUNNING   = 'R',
      ST_STOPPED   = 'S',
      ST_PAUSED    = 'P',
      ST_ERROR     = 'E',
      TR_ERROR     = ERROR
    };
    enum SubState  {
      ACTION_SUCCESS = 'S',
      ACTION_EXEC    = 'E',
      ACTION_FAILED  = 'F',
      ACTION_UNKNOWN = 'U'
    };

  public:
    struct FSMMonitoring {
      unsigned long lastCmd, doneCmd;
      int pid;
      char targetState, state, metaState, pad;
      int partitionID;
    } m_monitor;

  protected:
    /// Variable to contain object name (==constant)
    std::string                  m_name;
    /// Variable to contain the state name
    std::string                  m_stateName;
    /// Variable to contain the previous state name
    std::string                  m_prevStateName;
    /// Pointer to dim command to treceive transition changes
    DimCommand*                  m_command    = 0;
    /// Pointer to the dim service publishing the state
    DimService*                  m_service    = 0;
    /// Pointer to the dim service publishing the sub-state
    DimService*                  m_fsmService = 0;
    /// Handle to the output logger
    std::unique_ptr<RTL::Logger> m_logger;
    /// Handle to the logger device
    std::shared_ptr<RTL::Logger::LogDevice> m_logDev;
    /// Configuration service for parameters
    SmartIF<Configuration>       m_config;
    /// Handle to the incident service
    SmartIF<IIncidentSvc>        m_incidentSvc;
    /// Handle to the incident listener to accept incidents
    SmartIF<IIncidentListener>   m_incidentListener;
    /// Handle to the UI service
    SmartIF<IService>            m_uiSvc;

  protected:

    /// Dump all job options
    void  dumpOptions( const Gaudi::Application::Options& options )  const;

  public:

    /// Declare FSM state
    virtual int declareStateByName(const std::string& new_state);
    /// Translate integer state to string name
    static const char* stateName(int state);
    /// Access meta state name
    static const char* metaName(int state);

    /// Enable UI
    void enableUI();

    /// Clear current state name
    void clearState()  { m_stateName = ""; }

    /// Invoke automatic stop sequence
    virtual int autoStop();

    /// Access to logger object
    RTL::Logger* logger()   {  return m_logger.get();  }
    
  public:
    /// Specialized constructor
    OnlineApplication(Options opts);
    /// Default destructor
    virtual ~OnlineApplication();

    /// Interactor overload: handle Sensor stimuli
    virtual void handle(const Event& ev)   override;
    /// Interactor overload: handle Timer Sensor stimuli
    virtual void handleTimer(long type);
    /// Interactor overload: handle IOC Sensor stimuli
    virtual void handleIoc(int type, void* data);

    /// Print overload
    virtual void output(int level, const char* s);
    /// Cancel IO request/Event loop
    virtual int cancel();

    /// FSM network interface: Declare FSM state
    virtual int declareState(State state);
    /// FSM network interface: Declare FSM sub-state
    virtual int declareSubState(SubState state);
    /// FSM network interface: Set transition target state
    virtual void setTargetState(State target)
    { m_monitor.targetState = char(target); }

    /// (Re)connect DIM services
    virtual int connectDIM(DimCommand* cmd=0);
    /// Disconnect DIM services
    virtual int disconnectDIM();

    /// Access object name (=UTGID/process name)
    virtual const std::string& name() const      {    return m_name;      }
    /// Access current state name
    virtual const std::string& stateName() const {    return m_stateName; }

    /// Incident handler callback
    virtual void handleIncident(const Incident& incident);
    
    /// Fire an incident to inform clients about DAQ state etc.
    int fireIncident(const std::string& type)   const;

    /// Internal: Initialize the application            (NOT_READY  -> READY)
    virtual int configureApplication();
    /// Finalize the application                        (READY  -> NOT_READY)
    virtual int finalizeApplication();
    /// Internal: Start the application                 (READY      -> RUNNING)
    virtual int startApplication();
    /// Run application if required
    virtual int runApplication();

    /// Startup done                                    (Offline    -> NOT_READY)
    virtual int startupDone();

    /// Initialize the application                      (NOT_READY  -> READY)
    virtual int configure();
    /// Start the application                           (READY      -> RUNNING)
    virtual int start();
    /// Stop the application                            (RUNNING    -> READY)
    virtual int stop();
    /// State declaration call on Pause                 (RUNNING    -> READY)
    virtual int stopProcessing();
    /// Reset the application                           (READY      -> NOT_READY)
    virtual int reset();
    /// Disconnect process and exit                     (NOT_READY  -> UNKNOWN)
    virtual int unload();

    /// Pause the application                           (RUNNING    -> PAUSED)
    virtual int pause();
    /// State declaration call on Pause                 (RUNNING    -> PAUSED)
    virtual int pauseProcessing();
    /// Continue the application                        (PAUSED     -> RUNNING)
    virtual int continuing();
    /// Continue the application                        (PAUSED     -> RUNNING)
    virtual int continueProcessing();

    /// Invoke transition to error state                ( ****      -> ERROR)
    virtual int error();

    /// Method to implement the main application logic (prepare to run, loop over events, terminate)
    virtual int run()   override;
  };   // class OnlineApplication
}      // namespace Online
#endif // GAUDIONLINE_ONLINEAPPLICATION_H
