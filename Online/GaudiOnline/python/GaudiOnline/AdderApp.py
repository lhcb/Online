"""
     Online Adder application configuration

     @author M.Frank
"""
__version__ = "1.0"
__author__  = "Markus Frank <Markus.Frank@cern.ch>"

import os
import Gaudi
import Configurables

from  .OnlineApplication import *
from   GauchoAppl  import GauchoApplConf

EventLoopMgr      = Gaudi.Configurables.EventLoopMgr
ApplicationMgr    = Gaudi.Configuration.ApplicationMgr
SAVESET_DIRECTORY = '/hist/Savesets'


global adder_debug_flag
adder_debug_flag = False

# ===========================================================================================
#
# Class to simplify the building of adder services
#
# ===========================================================================================
class AdderService:
  # =========================================================================================
  def __init__(self, task, nodes, partition, typ, in_dns=None, out_dns=None, ext=None, alt_task_name=None, debug=False, run_aware=False):
    global adder_debug_flag
    pad = ''
    task_name = str(task)
    tname = task_name
    if ext: tname = task_name + ext
    if typ == "counter":
      adder = GauchoApplConf.AdderSvc("C_"+task_name)
      adder.ServicePattern = "MON_<part>_"+nodes+"_"+tname+"/Counter/"
    else:
      pad = ' '
      adder = GauchoApplConf.AdderSvc("H_"+task_name)
      adder.ServicePattern = "MON_<part>_"+nodes+"_"+tname+"/Histos/"

    adder.MyName         = "<part>_<node>_"+task_name
    adder.PartitionName  = partition
    if alt_task_name:
      adder.TaskPattern  = "<part>_"+nodes+"_"+str(alt_task_name)+"_(.*)"
    else:
      adder.TaskPattern  = "<part>_"+nodes+"_"+task_name

    input = ''
    if in_dns: input = 'Collect from: '+in_dns
    output = ''
    if out_dns: output = 'Outputs to: '+out_dns

    if adder_debug_flag:
      print("%-8s Service pattern: %s%s Task pattern: %s %s%s Debug: %s"\
            %("INFO", adder.ServicePattern, pad, adder.TaskPattern, input, output, str(debug), ))

    adder.AdderClass     = typ
    adder.RunAware       = run_aware
    adder.DebugOn        = debug
    if in_dns:
      adder.InDNS        = in_dns
    if out_dns:
      adder.OutDNS       = out_dns
    self.task_name = task_name
    self.obj = adder

  # =========================================================================================
  def setTaskPattern(self, pattern):
    self.obj.TaskPattern = pattern
    return self

  # =========================================================================================
  def setServicePattern(self, pattern):
    self.obj.ServicePattern = pattern
    return self

  # =========================================================================================
  def enableSaving(self, recv_tmo, interval, saveset_dir):
    self.obj.ReceiveTimeout  = recv_tmo
    self.obj.IsSaver         = True
    self.obj.SaveInterval    = interval
    self.obj.SaveRootDir     = saveset_dir
    self.obj.SaveSetTaskName = self.task_name
    return self

# ===========================================================================================
#
# ===========================================================================================
class AdderApp(Application):
  # =========================================================================================
  def __init__(self, outputLevel, partitionName='OFFLINE', partitionID=0xFFFF):
    Application.__init__(self, 
                         outputLevel=outputLevel,
                         partitionName=partitionName, 
                         partitionID=partitionID)
    self.setup_fifolog(format='%-8LEVEL %-24SOURCE', device='RTL::Logger::LogDevice')
    self.config.numEventThreads   = 0
    self.config.numStatusThreads  = 0
    self.config.autoStart         = False
    self.setup_monitoring_service()
    self.monSvc.HaveRates         = False
    self.app.ExtSvc.append(self.monSvc)
    EventLoopMgr().Warnings       = False
    self.enableUI()

  # =========================================================================================
  def defineAdders(self, tasklist, nodes, in_dns, out_dns, ext='_(.*)', alt_task_name=None, debug=False):
    adder_task_class = Class1
    tasks_str        = str(tasklist)[1:-1]
    self.config.classType = Class1
    self.log(MSG_INFO, '++ Configure TEST adder for tasks: %s', tasks_str)
    counter_adders = []
    histogram_adders = []
    for task_name in tasklist:
      adder = AdderService(task_name, nodes=nodes, partition=self.partitionName, typ='counter', \
                           in_dns=in_dns, out_dns=out_dns, ext=ext, alt_task_name=alt_task_name, debug=debug)
      adder.obj.HaveTimer = 0
      counter_adders.append(adder)
      self.app.ExtSvc.append(adder.obj)

      adder = AdderService(task_name, nodes=nodes, partition=self.partitionName, typ='hists', \
                           in_dns=in_dns, out_dns=out_dns, ext=ext, alt_task_name=alt_task_name, debug=debug)
      histogram_adders.append(adder)
      self.app.ExtSvc.append(adder.obj)
    return (counter_adders, histogram_adders)

  # =========================================================================================
  def defineMultiMonitoringAdders(self, tasklist, nodes='MON01..', debug=False):
    counter_adders, histogram_adders = self.defineAdders(tasklist, nodes=nodes, in_dns=None, out_dns='MON01', debug=debug)
    return (counter_adders, histogram_adders)

  # =========================================================================================
  def defineTestAdders(self, tasklist, debug=True):
    return self.defineAdders(tasklist, nodes='LOCAL', in_dns=None, out_dns='localhost', debug=debug)

  # =========================================================================================
  def setupAdder(partition, output_level, tasklist=[]):
    global adder_debug_flag
    #adder_debug_flag = True
    print('[INFO] Configure adder application. Partition: '+partition+' tasks: '+str(tasklist))
    app = AdderApp(outputLevel=output_level, partitionName=partition)
    app.defineMultiMonitoringAdders(tasklist, debug=adder_debug_flag)

  # =========================================================================================
  def setupSaver(partition, output_level, tasklist=[]):
    global adder_debug_flag
    #adder_debug_flag = True
    print('[INFO] Configure adder application. Partition: '+partition+' tasks: '+str(tasklist))
    app = AdderApp(outputLevel=output_level, partitionName=partition)
    counter_adders, histogram_adders = app.defineMultiMonitoringAdders(tasklist, nodes='mon01..', debug=adder_debug_flag)
    for adder in histogram_adders:
      adder.enableSaving(recv_tmo=12, interval=900, saveset_dir=SAVESET_DIRECTORY)
