"""
     Online Passthrough application configuration

     @author M.Frank
"""
__version__ = "1.0"
__author__  = "Markus Frank <Markus.Frank@cern.ch>"

from .OnlineApplication import *

class Convert2MDF(Application):
  def __init__(self, outputLevel, partitionName='OFFLINE', partitionID=0xFFFF):
    Application.__init__(self, 
                         outputLevel=outputLevel,
                         partitionName=partitionName, 
                         partitionID=partitionID,
                         classType=Class1)

  def setup_algorithms(self, output):
    import Gaudi.Configuration as Gaudi
    import Configurables
    self.config.numEventThreads  = 1
    self.config.numStatusThreads = 1
    self.config.events_HighMark  = 0
    self.config.events_LowMark   = 0
    self.config.logDeviceType    = 'RTL::Logger::LogDevice'
    self.config.logDeviceFormat  = '%TIME%LEVEL %-16SOURCE'

    input                        = Configurables.Online__InputAlg('EventInput')
    input.DeclareData            = True
    input.DeclareEvent           = True
    input.ExpandTAE              = True
    input.MakeRawEvent           = True
    """
    explorer                     = Configurables.Online__StoreExplorer('Explorer')
    explorer.Load                = 1
    explorer.PrintFreq           = 1.0
    explorer.OutputLevel         = 1
    ctrl                         = Configurables.Online__InteractiveAlg('CommandHandler')
    ctrl.Prompt                  = "Press <ENTER> to continue, q or Q to quit :"
    """
    writer                       = self.setup_file_output()
    writer.FILE_nameFormat       = output
    writer.FILE_numConnections   = 1
    writer.OutputMDF             = True
    #self.app.TopAlg              = [input, explorer, writer, ctrl]
    self.app.TopAlg              = [input, writer]
    self.broker.DataProducers    = self.app.TopAlg
    return self

def convert2MDF(file, output):
  app = Convert2MDF(outputLevel=MSG_INFO)
  if isinstance(file,list):
    app.setup_file_access(file)
  else:
    app.setup_file_access([file])
  app.config.autoStart = True
  app.config.autoStop  = True
  app.setup_hive(FlowManager( 'EventLoop' ), 1)
  app.setup_algorithms( output )
  return app

