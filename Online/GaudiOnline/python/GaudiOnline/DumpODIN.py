"""
     Online Passthrough application configuration

     @author M.Frank
"""
__version__ = "1.0"
__author__  = "Markus Frank <Markus.Frank@cern.ch>"

from .OnlineApplication import *

class DumpODIN(Application):
  def __init__(self, outputLevel, partitionName='OFFLINE', partitionID=0xFFFF):
    Application.__init__(self, 
                         outputLevel=outputLevel,
                         partitionName=partitionName, 
                         partitionID=partitionID,
                         classType=Class1)
    self.config.numEventThreads  = 1
    self.config.numStatusThreads = 1
    self.config.events_HighMark  = 0
    self.config.events_LowMark   = 0
    self.config.logDeviceType    = 'RTL::Logger::LogDevice'
    self.config.logDeviceFormat  = '%-9LEVEL%-24SOURCE'
    
  def setup_algorithms(self, opts):
    import Gaudi.Configuration as Gaudi
    import Configurables

    input                        = Configurables.Online__InputAlg('EventInput')
    input.DeclareData            = True
    input.DeclareEvent           = True
    input.MakeRawEvent           = True
    input.CanFail                = False
    self.input                   = input
    algs                         = [input]
    #
    if opts.get('have_explorer'):
      explorer                     = Configurables.Online__StoreExplorer('Explorer')
      explorer.Load                = 1
      explorer.PrintFreq           = 1.0
      explorer.OutputLevel         = 1
      self.explorer                = explorer
      algs.append(explorer)
      #
    if opts.get('dump_headers'):
      hdr                          = Configurables.Online__Tell1HeaderDump('Headers')
      hdr.RawGuard                 = 'Banks/RawDataGuard'
      self.header_dump             = hdr
      algs.append(hdr)
      #
    if opts.get('dump_odin') or opts.get('dump_banks'):
      dump                         = Configurables.Online__TAEBankDump('ODIN-Dump')
      dump.RawGuard                = 'Banks/RawDataGuard'
      dump.BankType                = 16
      dump.OutputLevel             = 1
      self.dump                    = dump
      algs.append(dump)
      #
    if opts.get('dump_banks'):
      if opts.get('interactive'):
        ctrl                       = Configurables.Online__InteractiveAlg('CommandHandler')
        ctrl.Prompt                = "Press <ENTER> to continue, q or Q to quit :"
        algs.append(ctrl)
        #
      full_dump                    = False
      if opts.get('dump_bank_data'):
        full_dump = True
      dump                         = bank_dump(name='Dump',
                                               raw_data='Banks/RawData',
                                               dump_data=True,
                                               full_dump=full_dump,
                                               summary=True,
                                               check_data=False)
      algs.append(dump)
      #
    if opts.get('interactive'):
      ctrl                         = Configurables.Online__InteractiveAlg('CommandHandler')
      ctrl.Prompt                  = "Press <ENTER> to continue, q or Q to quit :"
      algs.append(ctrl)
      #
    self.app.TopAlg              = algs
    self.broker.DataProducers    = self.app.TopAlg
    return self

def dumpODIN(file, interactive=True, odin=True, headers=True, banks=True, explorer=True, bank_data=False, silent=False):
  level = MSG_DEBUG
  if silent: level = MSG_INFO
  app = DumpODIN(outputLevel=level)
  if isinstance(file,list):
    app.setup_file_access(file)
  else:
    app.setup_file_access([file])
  app.config.autoStart = True
  app.config.autoStop  = True
  app.setup_hive(FlowManager("EventLoop"), 1)
  app.setup_algorithms(opts={'interactive':    interactive,
                             'have_explorer':  explorer,
                             'dump_headers':   headers,
                             'dump_odin':      odin,
                             'dump_banks':     banks,
                             'dump_bank_data': bank_data})
  return app
