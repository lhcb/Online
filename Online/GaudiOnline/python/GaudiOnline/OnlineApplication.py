"""
     Minimal Gaudi task in the online environment

     @author M.Frank
"""
from __future__ import print_function
from builtins import object
__version__ = "1.0"
__author__  = "Markus Frank <Markus.Frank@cern.ch>"

import os
import logging
import Gaudi
import Configurables
import GaudiKernel.ProcessJobOptions
from Online.MessageLevels import *

utgid = ''
if 'UTGID' in os.environ:
  utgid = os.environ['UTGID']+': '
else:
  utgid = 'P%06d'%(os.getpid(),)

MessageSvc     = Configurables.MessageSvc
MonitorSvc     = None
FlowManager    = Configurables.Online__FlowManager
Configuration  = Configurables.Online__Configuration
InputAlg       = Configurables.Online__InputAlg
OutputAlg      = Configurables.Online__OutputAlg
UpdateAndReset = Configurables.Online__UpdateAndReset
Sequencer      = Configurables.Online__Sequencer

ApplicationMgr = Gaudi.Configuration.ApplicationMgr
WhiteBoard     = Configurables.HiveWhiteBoard
DataBroker     = Configurables.HiveDataBrokerSvc

Class0         = 0
Class1         = 1
Class2         = 2


global minPrintLevel
minPrintLevel = MSG_INFO
GaudiKernel.ProcessJobOptions.PrintOff(100)

# ------------------------------------------------------------------------------
def log(level, fmt, *args):
  """

  \author   M.Frank
  \version  1.0
  \date     30/06/2002
  """
  import sys
  global minPrintLevel
  try:
    f = sys.stdout
    if level < minPrintLevel:
      return
    elif level == MSG_VERBOSE:
      format = 'VERBOSE ' + fmt
    elif level == MSG_DEBUG:
      format = 'DEBUG   ' + fmt
    elif level == MSG_INFO:
      format = 'INFO    ' + fmt
    elif level == MSG_WARNING:
      format = 'WARNING ' + fmt
    elif level == MSG_ERROR:
      format = 'ERROR   ' + fmt
    elif level == MSG_ALWAYS:
      format = 'SUCCESS ' + fmt
    elif level >  MSG_ALWAYS:
      format = 'INFO    ' + fmt
    print(format%args)
    f.flush()
  except Exception as X:
    print('WARNING Exception failure: %s'%(str(X),))

# ------------------------------------------------------------------------------
def output(level, source, fmt, *args):
  """

  \author   M.Frank
  \version  1.0
  \date     30/06/2002
  """
  import sys
  global minPrintLevel
  try:
    f = sys.stdout
    if level < minPrintLevel:
      return
    elif level == MSG_VERBOSE:
      format = 'VERBOSE '
    elif level == MSG_DEBUG:
      format = 'DEBUG   '
    elif level == MSG_INFO:
      format = 'INFO    '
    elif level == MSG_WARNING:
      format = 'WARNING '
    elif level == MSG_ERROR:
      format = 'ERROR   '
    elif level == MSG_ALWAYS:
      format = 'SUCCESS '
    elif level >  MSG_ALWAYS:
      format = 'INFO    '
    format = (format + ('%-25s '%(source, ))) + fmt
    print(format%args)
    f.flush()
  except Exception as X:
    print('WARNING Exception failure: %s args:%s'%(str(X), str(args),))

def store_explorer(load=1, print_freq=1.0, output_level=MSG_ALWAYS):
  from Configurables import Online__StoreExplorer as StoreExplorer
  exp                     = StoreExplorer('Explorer')
  exp.Load                = 1
  exp.PrintFreq           = print_freq
  exp.OutputLevel         = 3
  return exp

def header_dump(name='Headers', raw_data='Banks/RawDataGuard'):
  from Configurables import Online__Tell1HeaderDump as Tell1HeaderDump
  dump = Tell1HeaderDump(name)
  dump.RawGuard = raw_data
  return dump

def bank_dump(name='Dump', raw_data='Banks/RawData', bx=None, dump_data=True, full_dump=True, summary=True, check_data=False, output_level=MSG_INFO):
  from Configurables import Online__Tell1BankDump as Tell1BankDump
  if raw_data:
    location = raw_data
  else:
    if bx < 0:
      location = 'Prev'+str(-1*int(bx))
    elif bx == 0:
      location = 'Central'
    elif bx > 0:
      location = 'Next'+str(int(bx))
    name = name + '__' + location
    location = 'Banks/' + location

  dump             = Configurables.Online__Tell1BankDump(name)
  dump.RawData     = location
  dump.CheckData   = check_data
  dump.DumpData    = dump_data
  dump.FullDump    = full_dump
  dump.Summary     = summary
  dump.OutputLevel = output_level
  return dump

def command_handler(name='CommandHandler', prompt="Press <ENTER> to continue, q or Q to quit :"):
  from Configurables import Online__InteractiveAlg as InteractiveAlg
  ctrl = InteractiveAlg(name)
  ctrl.Prompt = prompt
  return ctrl
  
def delay(name='Delay', delays={}):
  from Configurables import Online__DelayAlg as Delay
  dump = Delay(name)
  for k, v in delays.items():
    setattr(dump, k, v)
  return dump

class Application(object):
  def __init__(self, outputLevel, partitionName, partitionID, classType=Class1):
    global minPrintLevel
    import fifo_log
    minPrintLevel               = outputLevel
    self.outputLevel            = outputLevel
    self.partitionID            = partitionID
    self.partitionName          = partitionName
    self.app                    = ApplicationMgr()
    self.app.MessageSvcType     = 'MessageSvc'
    self.app.EvtSel             = 'NONE'
    self.app.EvtMax             = -1
    self.app.AppName            = ''
    self.app.OutputLevel        = self.outputLevel
    self.app.HistogramPersistency = 'NONE'
    self.config                 = Configuration('Application')
    self.messageSvc             = MessageSvc('MessageSvc')
    self.messageSvc.Format      = '% F%8W%L%T %25W%L%S %0W%M'
    self.messageSvc.OutputLevel = self.outputLevel
    self.ui                     = None
    config                      = self.config
    config.debug                = False
    config.classType            = classType
    config.autoStart            = False
    config.autoStop             = False
    # Exec mode: 0 : synchronous; 1 : asynchronous  2 : synchronous pop(queue-length=1)
    config.execMode             = 0
    config.monitorType          = ''  # No monitoring counters declared if the type is empty!
    config.logDeviceType        = 'RTL::Logger::LogDevice'
    config.logDeviceFormat      = '%TIME%LEVEL%-8NODE: %-32PROCESS %-20SOURCE'
    config.OutputLevel          = self.outputLevel
    config.IOOutputLevel        = self.outputLevel
    config.maxConsecutiveErrors = 2
    config.events_LowMark       = 8000
    config.events_HighMark      = 18000
    config.numEventThreads      = 1
    config.numStatusThreads     = 1
    config.burstPrintCount      = 30000
    config.expandTAE            = False
    config.dropEdgesTAE         = True
    config.eventIdleTimeout     = 100
    config.eventMinimalIdleTime = 300
    ##config.numEventThreads      = 1  # Debugging only
    ##config.events_LowMark       = 50
    ##config.events_HighMark      = 180
    self.log                    = fifo_log.logger_print

  def enableUI(self):
    if not self.ui:
      print('ALWAYS  +++ Enable UI service')
      self.ui = Configurables.Online__OnlineUISvc()
      self.app.ExtSvc.append(self.ui)
    return self

  def setup_fifolog(self, format='%-38SOURCE %-8LEVEL', device='fifo'):
    import fifo_log
    self.config.logDeviceType   = device
    self.config.logDeviceFormat = format
    fifo_log.logger_set_tag(self.partitionName)
    if os.getenv('UTGID', False):
      fifo_log.logger_set_utgid(os.getenv('UTGID'))
    fifo_log.logger_start()
    return self

  def setup_online_fifolog(self):
    return self.setup_fifolog(format='%-38SOURCE %-8LEVEL', device='RTL::Logger::LogDevice')

  def log(self, level, fmt, *args):
    log(level, fmt, *args)

  def setup_event_input(self):
    log(MSG_INFO,'Setup event input for REGULAR processing')
    input                       = InputAlg('EventInput')
    input.DeclareData           = True
    input.DeclareEvent          = True
    input.MakeRawEvent          = True
    input.DeclareErrors         = False
    self.input = input
    return self.input

  def setup_monitoring_service(self, task_name=None):
    global MonitorSvc
    MonitorSvc                  = Configurables.MonitorSvc
    config                      = self.config
    config.monitorType          = 'MonitorSvc' # No monitoring counters declared if the type is empty!
    mon                         = Configurables.MonitorSvc(config.monitorType)
    mon.PartitionName           = self.partitionName
    mon.ExpandNameInfix         = '<proc>/'
    mon.ExpandCounterServices   = True
    mon.UniqueServiceNames      = True
    mon.UseDStoreNames          = True
    mon.DimUpdateInterval       = 5
    mon.CounterUpdateInterval   = 5
    if task_name:
      mon.ProgramName           = task_name
    self.app.ExtSvc.insert(0, mon)
    self.monSvc                 = mon
    return self.monSvc

  def setup_monitoring(self, task_name=None, have_odin=True):
    self.setup_monitoring_service(task_name)
    self.updateAndReset         = None
    update                      = Configurables.Online__UpdateAndReset('UpdateAndReset')
    update.resetOnStart         = False
    update.saverCycle           = 900
    update.saveHistograms       = 0
    update.disableReadOdin      = not have_odin
    if task_name:
      update.MyName             = task_name
    self.updateAndReset         = update
    return self

  def setup_algorithms(self,*args,**kwd):
    return self

  def setup_mbm_access(self, input, partitionBuffers=True):
    config                      = self.config
    config.inputType            = 'MBM'
    config.verifyBanks          = True
    config.MBM_input            = input
    config.MBM_partitionID      = self.partitionID
    config.MBM_partitionName    = self.partitionName
    config.MBM_partitionBuffers = partitionBuffers
    config.MBM_numConnections   = 1
    config.MBM_numEventThreads  = 1
    config.MBM_requests = [
      'EvType=2;TriggerMask=0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF;VetoMask=0,0,0,0;MaskType=ANY;UserType=ONE;Frequency=PERC;Perc=100.0'
    ]
    return self

  def setup_net_access(self, source):
    config                      = self.config
    config.inputType            = 'NET'
    config.verifyBanks          = True
    config.NET_requests         = [
      'EvType=2;TriggerMask=0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF;VetoMask=0,0,0,0;MaskType=ANY;UserType=ONE;Frequency=PERC;Perc=100.0'
    ]
    config.NET_source           = source
    config.NET_eventTimeout     = 10
    return self

  def setup_file_access(self, data_sources):
    config                     = self.config
    config.inputType           = 'FILE'
    config.verifyBanks         = True
    config.FILE_allowedRuns    = [ '*' ]
    config.FILE_brokenHosts    = ''
    config.FILE_prefix         = ''
    config.FILE_packingFactor  = 1
    config.FILE_bufferSize     = 1024*1024
    config.FILE_rescan         = False
    config.FILE_openFailDelete = False
    config.FILE_deleteFiles    = False
    config.FILE_saveRest       = False
    config.FILE_mmapFiles      = False
    config.FILE_sources        = data_sources
    config.FILE_requests       = [
      'EvType=2;TriggerMask=0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF;VetoMask=0,0,0,0;MaskType=ANY;UserType=ONE;Frequency=PERC;Perc=100.0'
    ]
    return self

  def setup_file_directory_access(self, data_sources, file_prefix='fakedata_'):
    config                     = self.config
    config.inputType           = 'FILE'
    config.verifyBanks         = True
    config.FILE_allowedRuns    = [ '*' ]
    config.FILE_brokenHosts    = ''
    config.FILE_prefix         = file_prefix
    config.FILE_packingFactor  = 3000
    config.FILE_bufferSize     = 4*1024*1024
    config.FILE_rescan         = False
    config.FILE_openFailDelete = False
    config.FILE_deleteFiles    = False
    config.FILE_saveRest       = False
    config.FILE_mmapFiles      = False
    config.FILE_sources        = data_sources
    config.FILE_requests       = [
      'EvType=2;TriggerMask=0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF;VetoMask=0,0,0,0;MaskType=ANY;UserType=ONE;Frequency=PERC;Perc=100.0'
    ]
    return self

  def setup_hive(self, flowmgr, num_slots):
    self.flowmgr                       = flowmgr
    self.flowmgr.OutputLevel           = self.outputLevel
    self.config.flowManager            = flowmgr
    self.whiteboard                    = WhiteBoard('EventDataSvc', EventSlots=num_slots)
    self.whiteboard.ForceLeaves        = True
    self.broker                        = DataBroker()
    self.broker.OutputLevel            = self.outputLevel
    self.app.EventLoop                 = flowmgr
    self.app.ExtSvc.append(self.whiteboard)
    return self

  def setup_events(self, num_slots=1):
    self.flowmgr.OutputLevel           = self.outputLevel
    self.whiteboard                    = WhiteBoard('EventDataSvc', EventSlots=num_slots)
    self.whiteboard.ForceLeaves        = True
    self.broker                        = DataBroker()
    self.broker.OutputLevel            = self.outputLevel
    self.app.EventLoop                 = self.flowmgr
    self.app.ExtSvc.append(self.whiteboard)
    return self

  def setup_mbm_output(self, type, partitionBuffers=True, buffer='Output', name='EventOutput'):
    writer                             = OutputAlg(name)
    writer.MaxEventsPerTransaction     = 100000000
    writer.MBM_allocationSize          = 1024*1024
    writer.MBM_maxEvents               = 0 # fill space!
    writer.MBM_useEventMask            = False
    writer.MBM_partitionName           = self.partitionName
    writer.MBM_partitionID             = self.partitionID
    writer.MBM_partitionBuffers        = partitionBuffers
    writer.MBM_eventType               = 2
    writer.MBM_transactionWaitTMO      = 10
    writer.MBM_buffer                  = buffer
    writer.MBM_burstPrintCount         = 1000
    writer.MBM_numConnections          = 1
    writer.MBM_maxConsumerWait         = -1
    return writer

  def setup_file_output(self, name_format, type='FILE'):
    writer                             = OutputAlg('EventOutput')
    writer.OutputType                  = type
    writer.MaxEventsPerTransaction     = 100000000
    writer.FILE_allocationSize         = 1024*1024*10
    writer.FILE_burstPrintCount        = 1000
    writer.FILE_transactionWaitTMO     = 10
    writer.FILE_maxEvents              = 0 # fill space!
    writer.FILE_useEventMask           = False
    writer.FILE_nameFormat             = ''
    writer.FILE_maxFileSize            = 1024*1024*1024
    writer.FILE_numConnections         = 1
    if name_format:
      writer.FILE_nameFormat           = name_format
    return writer

