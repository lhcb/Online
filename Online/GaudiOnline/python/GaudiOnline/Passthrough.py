"""
     Online Passthrough application configuration

     @author M.Frank
"""
__version__ = "1.0"
__author__  = "Markus Frank <Markus.Frank@cern.ch>"

from .OnlineApplication import *

class Passthrough(Application):
  def __init__(self, outputLevel, partitionName, partitionID, classType):
    Application.__init__(self,
                         outputLevel=outputLevel,
                         partitionName=partitionName, 
                         partitionID=partitionID,
                         classType=classType)

  def setup_algorithms(self, writer=None, acceptRate=1.0):
    import Gaudi.Configuration as Gaudi
    import Configurables
    if not hasattr(self,'input'):
      self.input = self.setup_event_input()

    passThrough                 = Configurables.Online__Passthrough('Passthrough')
    passThrough.RawGuard        = '/Event/Banks/RawDataGuard'
    passThrough.AcceptRate      = acceptRate
    self.passThrough            = passThrough

    sequence                    = Sequencer('Output')
    sequence.Members            = [self.input]
    if hasattr(self,'updateAndReset'):
      sequence.Members.append(self.updateAndReset)
    sequence.Members.append(self.passThrough)
    if isinstance(writer, (list, tuple)):
      for item in writer:
        sequence.Members.append(item)
    elif writer:
        sequence.Members.append(writer)
    self.sequence               = sequence

    self.app.TopAlg             = [self.sequence]
    self.broker.DataProducers   = self.app.TopAlg
    self.enableUI()
    return self
