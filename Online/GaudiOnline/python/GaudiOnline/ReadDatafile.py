"""
     Online Passthrough application configuration

     @author M.Frank
"""
__version__ = "1.0"
__author__  = "Markus Frank <Markus.Frank@cern.ch>"

from .OnlineApplication import *

class ReadDatafile(Application):
  def __init__(self, outputLevel, partitionName='OFFLINE', partitionID=0xFFFF):
    Application.__init__(self, 
                         outputLevel=outputLevel,
                         partitionName=partitionName, 
                         partitionID=partitionID,
                         classType=Class1)
    self.config.numEventThreads  = 1
    self.config.numStatusThreads = 1
    self.config.events_HighMark  = 0
    self.config.events_LowMark   = 0
    self.config.logDeviceType    = 'RTL::Logger::LogDevice'
    self.config.logDeviceFormat  = '%-9LEVEL%-24SOURCE'
    self.interactive = True
    self.summary = True
    self.full_dump = False
    self.dump = True
    
  def setup_input(self):
    import Gaudi.Configuration as Gaudi
    import Configurables

    input                        = Configurables.Online__InputAlg('EventInput')
    input.DeclareData            = True
    input.DeclareEvent           = True
    input.MakeRawEvent           = True
    self.input                   = input
    algs                         = [input]
    self.app.TopAlg              = algs
    self.broker.DataProducers    = self.app.TopAlg
    return self
    
  def setup_algorithms(self):
    import Gaudi.Configuration as Gaudi
    import Configurables

    self.setup_input()
    explorer                     = store_explorer(load=1, print_freq=1.0)
    ask                          = Configurables.Online__InteractiveAlg('DumpHandler')
    ask.Prompt                   = "Press <ENTER> to dump banks, q or Q to quit :"
    dump                         = bank_dump(name='Dump',
                                             raw_data='Banks/RawData',
                                             dump_data=self.dump,
                                             full_dump=self.full_dump,
                                             summary=self.summary,
                                             check_data=False)
    hdr                          = header_dump(name='Headers', raw_data='Banks/RawDataGuard')
    ctrl                         = Configurables.Online__InteractiveAlg('CommandHandler')
    ctrl.Prompt                  = "Press <ENTER> to continue, q or Q to quit :"
    algs                         = [explorer]
    if self.interactive:
      algs.append(ask)
    algs.append(hdr)
    algs.append(dump)
    if self.interactive:
      algs.append(ctrl)
    for alg in algs:  
      self.app.TopAlg.append(alg)
    self.broker.DataProducers = self.app.TopAlg
    return self

def readDatafile(file, interactive=True, output_level=MSG_VERBOSE, algs=None, dump=True, full_dump=False, summary=True):
  app = ReadDatafile(outputLevel=output_level)
  app.interactive = interactive
  if isinstance(file,list):
    app.setup_file_access(file)
  else:
    app.setup_file_access([file])
  app.config.autoStart = True
  app.config.autoStop  = True
  app.dump             = dump
  app.full_dump        = full_dump
  app.summary          = summary
  app.setup_hive(FlowManager("EventLoop"), 1)
  if algs:
    app.setup_input()
    for alg in algs:  
      app.app.TopAlg.append(alg)
    app.broker.DataProducers = app.app.TopAlg
  else:
    app.setup_algorithms()
  return app


def legacy_readDatafile(input):
  import Gaudi.Configuration as Gaudi
  app = Gaudi.ApplicationMgr()
  app.AppName = ''
  app.HistogramPersistency = 'NONE'
  svc = Gaudi.EventPersistencySvc()
  svc.CnvServices = [Configurables.LHCb__RawDataCnvSvc('RawDataCnvSvc')]
  Gaudi.FileCatalog().Catalogs = ['xmlcatalog_file:/tmp/qmTest.xml']
  Gaudi.IODataManager().OutputLevel = 3  
  Gaudi.IODataManager().AgeLimit    = 1
  svc = Gaudi.EventDataSvc()
  svc.RootCLID           = 1
  svc.ForceLeaves        = 1
  svc.EnableFaultHandler = True

  sel = Gaudi.EventSelector()
  sel.PrintFreq = 1
  input_files   = []
  if isinstance(input,str):
    input_files.append(input)
  elif isinstance(input,list) or isinstance(input,tuple):
    input_files = [i for i in input]
  print('legacy_readDatafile: input: %s'%(str(input_files), ))
  sel.Input = input_files

  exp                = Configurables.Online__StoreExplorer('Explorer') 
  exp.Load           = 1
  exp.PrintFreq      = 1
  exp.AccessForeign  = True
  exp.OutputLevel    = 3
  app.TopAlg        += [exp]
  return app
