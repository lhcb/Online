from __future__ import print_function
from builtins import input
from builtins import str
from builtins import range
from builtins import object
import os, sys, time, copy, pydim, threading
from Online.MessageLevels import *

# ------------------------------------------------------------------------------
def print_item(msg, r):
  if type(r) is type({}):
    for i in r.keys():
      print_item(msg+'.'+str(i), r[i])
  elif type(r) is type([]):
    for i in range(len(r)):
      print_item('  '+msg+' ['+str(i)+']',r[i])
  else:
    print('  '+msg+' : '+str(r))

#----------------------------------------------------------------------------------------
"""
from DimInterface import *
c = Client('MONA1001_R_Client/status','C')

"""
class Client(object):
  #--------------------------------------------------------------------------------------
  def __init__(self, client, format='C'):
    log(MSG_INFO,"+++ Starting control object connected to %s"%(client,))
    self.__client = client
    self.__state = ''
    self.__lock = threading.Lock()
    self.__infoID = pydim.dic_info_service(self.__client+'/status',format,self.status_callback)
    self.__cmd_format = format
    self.__callbacks = {}
    self.last = 'UNKNOWN'
    self.auto = False
  #--------------------------------------------------------------------------------------
  def name(self):
    return self.__client
  #--------------------------------------------------------------------------------------
  def state(self):
    return str(self.__state)
  #--------------------------------------------------------------------------------------
  def state2(self):
    self.__lock.acquire()
    data = None
    if self.__state:
      data = copy.deepcopy(self.__state)
    self.__lock.release()
    return data
  #--------------------------------------------------------------------------------------
  def register(self, state, call):
    self.__callbacks[state] = call

  #--------------------------------------------------------------------------------------
  def status_callback(self, *args):
    log(MSG_VERBOSE,'++ Client callback. Args are %s'%str(args))
    if len(args) > 1:
      self.__lock.acquire()
      r = args[1]
      r = r[:r.find('\0')]
      self.__state = r
      if self.__state in self.__callbacks:
        self.__callbacks[self.__state]()
      self.handleState(r)
      self.__lock.release()

  #--------------------------------------------------------------------------------------
  def cmd_callback(self, *args):
    log(MSG_VERBOSE,'++ Client callback. Args are %s'%str(args))

  def send(self, command):
      log(MSG_VERBOSE,'++ Client command: %s'%str(command))
      args = (command, )
      res = pydim.dic_cmnd_callback(self.__client, args, self.__cmd_format, self.cmd_callback, 1)
      if res:   
        log(MSG_ALWAYS,'Client: return code %d)' %res)
      else:
        log(MSG_ALWAYS,'Client: Command execution failed (return code %d)' %res)

  #--------------------------------------------------------------------------------------
  def handleState(self, state):
    log(MSG_INFO,'++ Client callback [%s]. State: %s'%(self.__client,str(state),))
    if self.auto:
      #time.sleep(2)
      if state == 'UNKNOWN':
        pass
      elif state == 'NOT_READY':
        self.send('configure')
      elif state == 'READY' and self.last == 'RUNNING':
        self.send('reset')
      elif state == 'READY':
        self.send('start')
      elif state == 'RUNNING':
        self.send('stop')
    self.last = state
    return self



if __name__ == "__main__":
  ctrl = Client("TEST")
  ctrl.auto = True
  loop = 1
  while loop:
      value = input('Give command, <ENTER> to exit: \n')
      if not value:
        loop = 0
      else:
        ctrl.send(value)
