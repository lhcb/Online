import os, sys, GaudiOnline
import Configurables

app = GaudiOnline.Passthrough(outputLevel=1,
                              partitionName='SF',
                              partitionID=0xFFFF,
                              classType=GaudiOnline.Class1)
#
app.setup_net_access('pluscc04::SF_PLUSCC04_EventSrv_000')
app.config.autoStart = True
app.config.taeHalfWindow = 7
app.config.numEventThreads = 1

writer = None #app.setup_mbm_output('EventOutput')
#writer.MBM_buffer = 'Remote'
app.setup_hive(GaudiOnline.FlowManager("EventLoop"), 40)
app.setup_algorithms(writer, 1.0, TAE=True)

expander                     = Configurables.Online__ExpandTAE('ExpandTAE')
app.sequence.Members.append(expander)

explorer                     = Configurables.StoreExplorerAlg('Explorer')
explorer.Load                = 1
explorer.PrintFreq           = 1.0
explorer.OutputLevel         = 1
app.sequence.Members.append(explorer)

ask                          = Configurables.Online__InteractiveAlg('DumpHandler')
ask.Prompt                   = "Press <ENTER> to dump banks, q or Q to quit :"
app.sequence.Members.append(ask)

dump                         = Configurables.Online__Tell1BankDump('Dump')
dump.RawLocation             = '/Event/DAQ/RawEvent'
dump.CheckData               = 0
dump.DumpData                = 1
dump.FullDump                = 1
dump.OutputLevel             = 1
app.sequence.Members.append(dump)

ctrl                         = Configurables.Online__InteractiveAlg('CommandHandler')
ctrl.Prompt                  = "Press <ENTER> to continue, q or Q to quit :"
app.sequence.Members.append(ctrl)

