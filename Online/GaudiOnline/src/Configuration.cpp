//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Project    : Online
//
//  Author     : Markus Frank
//==========================================================================

/// Framework include files
#include <GaudiOnline/Configuration.h>
#include <RTL/rtl.h>

/// C/C++ include files
#include <thread>

using namespace Online;

namespace   {
  /// Information to start the debugger
  void start_debugger()  {
    char text[1024];
    ::snprintf(text, sizeof(text), "[ERROR] Debug command:  gdb --pid %d\n\n", ::lib_rtl_pid());
    ::write(STDERR_FILENO, text, ::strlen(text)+1);
  }
}

/// Specialized constructor
Configuration::Configuration(std::string nam, ISvcLocator* svcloc)
  : Service(nam, svcloc)
{
  /** Basic process steering  */
  declareProperty("debug",                debug                 = false);
  declareProperty("eventIdleTimeout",     event_idle_timeout    = 100);
  declareProperty("eventMinimalIdleTime", event_min_idle_time   = 300);
  declareProperty("debug_wait",           debug_wait            = -1);
  declareProperty("runable",              runable               = "");
  declareProperty("classType",            clazz                 = 1);
  declareProperty("autoStart",            autoStart             = false);
  declareProperty("autoStop",             autoStop              = false);
  declareProperty("enablePause",          enablePause           = 0);
  declareProperty("enableContinue",       enableContinue        = 0);
  declareProperty("enableStop",           enableStop            = 0);
  declareProperty("enableError",          enableError           = 1);
  declareProperty("execMode",             execMode              = 1);
  declareProperty("dropPipeline",         dropPipeline          = true);

  declareProperty("dump_options",         dump_options          = false);
  declareProperty("numaNode",             numaNode              = -1);
  declareProperty("monitorType",          monitorType           = "");
  declareProperty("inputType",            inputType             = "MBM");
  declareProperty("flowManager",          flowManager);
  declareProperty("logDevice",            logDevice);
  declareProperty("logDeviceType",        logDeviceType         = "RTL::Logger::LogDevice");
  declareProperty("logDeviceFormat",      logDeviceFormat       = "%-16s %5s %s");
  declareProperty("IOOutputLevel",        IO_output_level       = LIB_RTL_WARNING);
  declareProperty("maxConsecutiveErrors", maxConsecutiveErrors  = 2);
  declareProperty("numEventThreads",      numEventThreads       = 1);
  declareProperty("numStatusThreads",     numStatusThreads      = 1);
  declareProperty("events_LowMark",       lowMark               = 1);
  declareProperty("events_HighMark",      highMark              = 100);
  declareProperty("burstPrintCount",      burstPrintCount       = std::numeric_limits<size_t>::max());
  declareProperty("verifyBanks",          verifyBanks           = true);
  declareProperty("numThreadSvcName",     numThreadSvcName      = "");

  /** Event header based pre-selection for raw events: (monitoring, mbm, network, file), not PCIE40 */
  /// Event header: trigger mask
  declareProperty("headerTriggerMask",    headerTriggerMask);
  /// Event header event: veto mask
  declareProperty("headerVetoMask",       headerVetoMask);

  /// TAE event handling: enhance/suppress TAE events, etc
  declareProperty("expandTAE",            expandTAE             = true);
  declareProperty("onlyTAE",              onlyTAE               = false);
  declareProperty("ignoreTAE",            ignoreTAE             = false);
  declareProperty("dropEdgesTAE",         dropEdgesTAE          = true);
  declareProperty("convertEdgesTAE",      convertEdgesTAE       = false);
  
  /// MBM event access options
  declareProperty("MBM_input",            mbm.input             = "Events");
  declareProperty("MBM_buffers",          mbm.buffers);
  declareProperty("MBM_requests",         mbm.requests);
  declareProperty("MBM_partitionID",      mbm.partitionID       = 0xFFFF);
  declareProperty("MBM_partitionName",    mbm.partitionName     = "NONE");
  declareProperty("MBM_partitionBuffers", mbm.partitionBuffers  = true);
  declareProperty("MBM_numConnections",   mbm.num_connections   = 1);
  declareProperty("MBM_numEventThreads",  mbm.num_event_threads = 1);
  declareProperty("MBM_maxEventsIn",      mbm.maxEventsIn       = std::numeric_limits<size_t>::max());

  /// Network event access options
  declareProperty("NET_requests",         net.requests);
  declareProperty("NET_source",           net.source);
  declareProperty("NET_eventTimeout",     net.eventTimeout      = 0);
  declareProperty("NET_cancelDeath",      net.cancelDeath       = true);
  declareProperty("NET_maxConnectFail",   net.maxConnectFail    = 1);

  /// File event access options
  declareProperty("FILE_requests",        file.requests);
  declareProperty("FILE_sources",         file.sources);
  declareProperty("FILE_allowedRuns",     file.allowedRuns);
  declareProperty("FILE_brokenHosts",     file.brokenHosts);
  declareProperty("FILE_prefix",          file.prefix           = "Run_");
  declareProperty("FILE_packingFactor",   file.packingFactor    = 1);
  declareProperty("FILE_bufferSize",      file.bufferSize       = 4*MByte);
  declareProperty("FILE_maxEventsIn",     file.maxEventsIn      = std::numeric_limits<size_t>::max());
  declareProperty("FILE_rescan",          file.rescan           = false);
  declareProperty("FILE_openFailDelete",  file.openFailDelete   = false);
  declareProperty("FILE_deleteFiles",     file.deleteFiles      = false);
  declareProperty("FILE_saveRest",        file.saveRest         = false);
  declareProperty("FILE_mmapFiles",       file.mmapFiles        = false);
  file.allowedRuns.push_back("*");

  declareProperty("GENERIC_factory",      generic.factory);
  declareProperty("GENERIC_bufferSize",   generic.bufferSize    = 4*MByte);
  declareProperty("GENERIC_packingFactor",generic.packingFactor = 1);
  declareProperty("GENERIC_halfWindow",   generic.halfWindow    = 0);
  declareProperty("GENERIC_maxEventsIn",  generic.maxEventsIn   = std::numeric_limits<size_t>::max());
}


/// Default destructor
Configuration::~Configuration()   {
}

/// Query interfaces of Interface
StatusCode Configuration::queryInterface(const InterfaceID& riid, void** ppv)  {
  if ( Configuration::interfaceID().versionMatch(riid) )   {
    *ppv = this;
    addRef();
    return StatusCode::SUCCESS;
  }
  return Service::queryInterface(riid,ppv);
}

/// Wait for debugger being attached
void Configuration::waitDebugger()    {
  std::thread* debug_thread = 0;
  if ( debug )  {
    bool wait_for_debugger = true;
    debug_thread = new std::thread([]{ start_debugger(); });
    while(wait_for_debugger)  {
      ::lib_rtl_sleep(100);
    }
    if ( !debug_thread )  {
      ::lib_rtl_output(LIB_RTL_ERROR,"Failed to start debugger thread....");
    }
  }
}
