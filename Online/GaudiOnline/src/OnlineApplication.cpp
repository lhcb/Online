//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Project    : Online
//
//  Author     : Markus Frank
//==========================================================================

#define NO_LONGLONG_TYPEDEF
#include <dim/dis.hxx>

#include <GaudiOnline/OnlineApplication.h>

#include <GaudiKernel/IMessageSvc.h>
#include <GaudiKernel/AppReturnCode.h>
#include <GaudiKernel/ISvcLocator.h>
#include <GaudiKernel/IProperty.h>
#include <GaudiKernel/IRunable.h>
#include <GaudiKernel/Incident.h>
#include <GaudiKernel/SmartIF.h>

#include <CPP/Event.h>
#include <CPP/IocSensor.h>
#include <RTL/FmcLogDevice.h>
#include <RTL/rtl.h>
#include <TError.h>

#define ST_NAME_OFFLINE     "OFFLINE"
#define ST_NAME_ERROR       "ERROR"
#define ST_NAME_NOT_READY   "NOT_READY"
#define ST_NAME_READY       "READY"
//#define ST_NAME_STOPPED     "STOPPED"
#define ST_NAME_STOPPED     "READY"
#define ST_NAME_RUNNING     "RUNNING"
#define ST_NAME_PAUSED      "PAUSED"

extern "C" void dimlog_initialize_logger();
extern "C" void dimlog_finalize_logger();
//extern "C" void fifolog_initialize_logger();
//extern "C" void fifolog_finalize_logger();

namespace   {

  /// DIM command handler class for FSM transions
  /**
   *
   *  @author M.Frank
   */
  class Command : public DimCommand  {
    typedef Online::OnlineApplication Target;
    /// Command target
    Target* m_target;
  public:
    /// Constructor
    Command(const std::string& nam, Target* target)
      : DimCommand(nam.c_str(), "C"), m_target(target) { }

    /// DimCommand overload: handle DIM commands
    void commandHandler()   override {
      // Decouple as quickly as possible from the DIM command loop !
      std::string cmd = getString();
      //m_target->m_logger->debug("Received DIM command: %s",cmd.c_str());
      if      ( cmd == "load"  ) {  // Ignore!
        return;
      }
      else if ( cmd == "configure"  ) {
        m_target->setTargetState(Target::ST_READY);
        IOCSENSOR.send(m_target, Target::CONFIGURE);
      }
      else if ( cmd == "start"      ) {
        m_target->setTargetState(Target::ST_RUNNING);
        IOCSENSOR.send(m_target, Target::START);
      }
      else if ( cmd == "stop"       ) {
        m_target->setTargetState(Target::ST_STOPPED);
        IOCSENSOR.send(m_target, Target::STOP);
      }
      else if ( cmd == "reset"      ) {
        m_target->setTargetState(Target::ST_OFFLINE);
        IOCSENSOR.send(m_target, Target::RESET);
      }
      else if ( cmd == "pause"      ) {
        m_target->setTargetState(Target::ST_PAUSED);
        IOCSENSOR.send(m_target, Target::PAUSE);
      }
      else if ( cmd == "continue"   ) {
        m_target->setTargetState(Target::ST_RUNNING);
        IOCSENSOR.send(m_target, Target::CONTINUE);
      }
      else if ( cmd == "unload"     ) {
        m_target->setTargetState(Target::ST_OFFLINE);
        IOCSENSOR.send(m_target, Target::CANCEL);
        IOCSENSOR.send(m_target, Target::UNLOAD);
      }
      else if ( cmd == "recover"    ) {
        m_target->setTargetState(Target::ST_OFFLINE);
        IOCSENSOR.send(m_target, Target::CANCEL);
        IOCSENSOR.send(m_target, Target::UNLOAD);
      }
      else if ( cmd == "cancel"     ) {
        IOCSENSOR.send(m_target, Target::CANCEL);
      }
      else if ( cmd == "RESET"      ) {
        m_target->setTargetState(Target::ST_OFFLINE);
        IOCSENSOR.send(m_target, Target::CANCEL);
        IOCSENSOR.send(m_target, Target::UNLOAD);
      }
      else if ( cmd == "error" )  {
        m_target->setTargetState(Target::ST_ERROR);
        m_target->cancel();
        m_target->declareState(Target::ST_ERROR);
        return;
      }
      else if ( cmd == "stop_trigger" )   {
        std::string* value = new std::string("DAQ_STOP_TRIGGER");
        IOCSENSOR.send(m_target, Target::FIRE_INCIDENT, value);
        return;
      }
      else if ( cmd == "start_trigger" )   {
        std::string* value = new std::string("DAQ_START_TRIGGER");
        IOCSENSOR.send(m_target, Target::FIRE_INCIDENT, value);
        return;
      }
      else if ( cmd == "set_state" )  {
        std::string old_state = m_target->stateName();
        m_target->clearState();
        m_target->declareStateByName(old_state);
        return;
      }
      else if ( cmd == "get_state" || cmd == "!state" )  {
	m_target->declareStateByName(m_target->stateName());
        return;
      }
      else if ( cmd == "set_ui" )  {
	m_target->enableUI();
        return;
      }
      else if ( ::strncasecmp(cmd.c_str(),"incident__",10) == 0 )  {
        IOCSENSOR.send(m_target, Target::FIRE_INCIDENT, new std::string(cmd.c_str()+10));
        return;
      }
      else   {
        m_target->declareState(Target::ST_ERROR);
        m_target->declareSubState(Target::ACTION_UNKNOWN);
        return;
      }
      m_target->declareSubState(Target::ACTION_EXEC);
    }
  };

  /// Incident handler implementation to propagate incidents
  class _IncidentHandler : public extends<Service, IIncidentListener>    {
  public:
    /// Reference to the application object
    Online::OnlineApplication* app  {nullptr};

  public:
    /// Use default constructors
    using extends::extends;

    /// Inform that a new incident has occurred
    virtual void handle( const Incident& incident )   override   {
      app->handleIncident(incident);
    }
  };

  /// Static printer (RTL overload)
  static size_t printout(void* context, int level, const char* fmt, va_list& args)  {
    RTL::Logger* logger = (RTL::Logger*)context;
    if ( logger )   {
      return logger->printout(level, fmt, args);
    }
    return 0;
  }

  static ErrorHandlerFunc_t         g_rootErrorHandler = nullptr;
  static Online::OnlineApplication* g_gaudiApplication = nullptr;

  /// A very simple error handler that is usually replaced by the TROOT default error handler.
  static void rootErrorHandler(Int_t level, Bool_t abort_bool, const char *location, const char *msg)    {
    auto* logger = g_gaudiApplication->logger();
    if ( level == kInfo )
      level = LIB_RTL_DEBUG;
    else if ( level == kWarning )
      level = LIB_RTL_WARNING;
    else if ( level == kError || level == kSysError )
      level = LIB_RTL_ERROR;
    else if ( level == kBreak || level == kFatal )
      level = LIB_RTL_FATAL;
    else if ( level == kPrint )
      level = LIB_RTL_VERBOSE;
    else
      level = LIB_RTL_VERBOSE;
    if ( logger )   {
      if ( logger->do_print(level) )   {
	logger->printmsg(level, location ? location : "<unspecified>", msg ? msg : "");
	logger->printmsg(level, location ? location : "<unspecified>", "Ignore ROOT abort.");
      }
    }
    else   {
      ::fprintf(stderr, "<%s>: %s\n", location ? location : "<unspecified>", msg ? msg : "");
      ::fflush(stderr);
      if (abort_bool) {
	::fprintf(stderr, "<%s>: Ignore abort\n", location ? location : "<unspecified>");
	::fflush(stderr);
      }
    }
  }

#define GAUDI_ASSERT_THROW_NAME( cond, msg, name )                                                                     \
  if ( !cond ) throw GaudiException{msg, name, StatusCode::FAILURE};

  void consumeOptions( SmartIF<IProperty> prop, Gaudi::Application::Options& options ) {
    GAUDI_ASSERT_THROW_NAME( prop, "invalid IProperty", "Gaudi::Application" );
    std::string prefix      = SmartIF<INamedInterface>( prop )->name() + '.';
    const auto  prefix_size = prefix.size();

    auto opt = options.upper_bound( prefix );
    while ( opt != end( options ) && std::string_view( opt->first ).substr( 0, prefix_size ) == prefix ) {
      GAUDI_ASSERT_THROW_NAME( prop->setProperty( opt->first.substr( prefix_size ), opt->second ),
                               "failure setting " + opt->first + " to " + opt->second, "Gaudi::Application" );
      // drop processed option and increase iterator
      opt = options.erase( opt );
    }
  }
  using namespace Online;
}

extern "C" void* DimTaskFSM_instance() {
  return g_gaudiApplication;
}

// Specialized constructor
OnlineApplication::OnlineApplication(Options opts)
  : Application(opts),
    m_name(), m_stateName(ST_NAME_OFFLINE), m_prevStateName(ST_NAME_OFFLINE),
    m_command(0), m_service(0), m_fsmService(0)
{
  auto sloc = app.as<ISvcLocator>();
  g_gaudiApplication = this;
  g_rootErrorHandler = ::SetErrorHandler(rootErrorHandler);
  gPrintViaErrorHandler = kTRUE;

  //my_init_hooks();
  m_name    = RTL::processName();
  m_logDev  = std::make_shared<RTL::Logger::LogDevice>();
  m_logger.reset(new RTL::Logger(m_logDev, "Application", LIB_RTL_INFO));
  ::lib_rtl_install_printer(printout, m_logger.get());
  m_config  = sloc->service<Configuration>("Online::Configuration/Application");
  if ( !m_config.get() )  {
    m_logger->throwError("Failed to acces the online configuration service");
  }
  consumeOptions(m_config.as<IProperty>(),opts);
  /// Start debugger if requested
  if ( m_config->debug ) m_config->waitDebugger();
  /// Configure output
  if ( m_config->logDeviceType == "RTL::Logger::LogDevice" )
    m_logDev = std::make_shared<RTL::Logger::LogDevice>();
  else if ( m_config->logDeviceType == "RTL::FmcLogDevice" && ::getenv("LOGFIFO") != 0 )
    m_logDev = std::make_shared<RTL::FmcLogDevice>(::getenv("LOGFIFO"));
  else if ( m_config->logDeviceType == "RTL::FmcLogDevice" )
    m_logDev = std::make_shared<RTL::FmcLogDevice>();
  else if ( m_config->logDeviceType == "fmc" && ::getenv("LOGFIFO") != 0 )
    m_logDev = std::make_shared<RTL::FmcLogDevice>(::getenv("LOGFIFO"));
  else if ( m_config->logDeviceType == "fmc" )
    m_logDev = std::make_shared<RTL::FmcLogDevice>();
  else if ( m_config->logDeviceType == "fifo" )   {
    m_logDev = std::make_shared<RTL::Logger::LogDevice>();
    //::fifolog_initialize_logger();
  }
  else if ( m_config->logDeviceType == "dim" )   {
    m_logDev = std::make_shared<RTL::Logger::LogDevice>();
    //::dimlog_initialize_logger();
  }
  RTL::Logger::setGlobalDevice(m_logDev, m_config->outputLevel());
  m_logDev->compileFormat(m_config->logDeviceFormat);
  m_logger->outputLevel = m_config->outputLevel();
  m_logger->device = m_logDev;
  m_logger->info("Successfully configured output logger device of type %s Level:%d.",
		 m_config->logDeviceType.c_str(), m_config->outputLevel());

  m_monitor.targetState = m_monitor.state = ST_NOT_READY;
  m_monitor.lastCmd     = m_monitor.doneCmd = (unsigned long)::time(0);
  m_monitor.metaState   = ACTION_EXEC;
  m_monitor.pid         = ::lib_rtl_pid();
  m_monitor.partitionID = -1;
  m_monitor.pad         = 0;
  if ( m_config->dump_options )    {
    dumpOptions(opts);
  }
  connectDIM();
  if ( this->m_config->debug_wait > 0 )  {
    long dbg_wait = 1000 * this->m_config->debug_wait; // milli seconds -> seconds
    m_logger->always("Wait for %ld seconds. [ gdb --pid %d ]", dbg_wait, ::lib_rtl_pid());
    while ( dbg_wait > 0 )  {
      ::lib_rtl_sleep(10);
      dbg_wait -= 10;
    }
  }
}

// Default destructor
OnlineApplication::~OnlineApplication()   {
  disconnectDIM();
  ::lib_rtl_install_printer(0,0);
  ::SetErrorHandler(g_rootErrorHandler);
  m_config.reset();
  m_logger.reset();
  g_gaudiApplication = 0;
}

/// Dump all job options
void OnlineApplication::dumpOptions( const Gaudi::Application::Options& options )   const  {
  for( const auto& opt : options )
    m_logger->always("%-40s  =  %s", opt.first.c_str(), opt.second.c_str());
}

/// Print overload
void OnlineApplication::output(int level, const char* s)  {
  return m_logger->output(level, s);
}

int OnlineApplication::connectDIM(DimCommand* cmd) {
  if ( !m_command )  {
    std::string svcname;
    if ( m_name.empty() ) m_name = RTL::processName();
    m_monitor.pid = ::lib_rtl_pid();
    svcname       = m_name+"/status";
    ::dim_init();
    DimServer::autoStartOff();
    m_command = cmd ? cmd : new Command(m_name, this);
    m_service = new DimService(svcname.c_str(),(char*)m_stateName.c_str());
    svcname   = m_name+"/fsm_status";
    m_fsmService = new DimService(svcname.c_str(),(char*)"L:2;I:1;C",&m_monitor,sizeof(m_monitor));
    DimServer::start(m_name.c_str());
  }
  return 1;
}

int OnlineApplication::disconnectDIM() {
  if ( m_fsmService ) delete m_fsmService;
  if ( m_service    ) delete m_service;
  if ( m_command    ) delete m_command;
  m_fsmService = 0;
  m_service    = 0;
  m_command    = 0;
  DimServer::stop();
  return 1;
}

/// Enable UI
void OnlineApplication::enableUI()   {
  if ( !this->m_uiSvc )    {
    SmartIF<ISvcLocator> sloc = app.as<ISvcLocator>();
    this->m_uiSvc = sloc->service<IService>("Online::OnlineUISvc/UIService");
    if ( !this->m_uiSvc.get() )
      m_logger->error("Failed to access the UI service");
    else
      this->fireIncident("START_UI");
  }
}

/// Translate integer state to string name
const char* OnlineApplication::stateName(int state) {
  switch(state) {
  case ST_NOT_READY:
    return ST_NAME_NOT_READY;
  case ST_READY:
    return ST_NAME_READY;
  case ST_RUNNING:
    return ST_NAME_RUNNING;
  case ST_STOPPED:
    return ST_NAME_STOPPED;
  case ST_PAUSED:
    return ST_NAME_PAUSED;
  case ST_ERROR:
    return ST_NAME_ERROR;
  case ST_OFFLINE:
  default:
    return ST_NAME_OFFLINE;
  }
}

/// Translate integer state to string name
const char* OnlineApplication::metaName(int state) {
  switch(state) {
  case ACTION_SUCCESS:
    return "SUCCESS";
  case ACTION_EXEC:
    return "EXEC";
  case ACTION_FAILED:
    return "FAILED";
  case ACTION_UNKNOWN:
  default:
    return "UNKNOWN";
  }
}

/// Declare process state to DIM service
int OnlineApplication::declareStateByName(const std::string& new_state)  {
  m_prevStateName = m_stateName;
  m_stateName = new_state;
  m_service->updateService((char*)m_stateName.c_str());
  m_logger->info("PUBLISH(FSM): Previous:%12s Current:%12s Target:%12s Meta-state: %s",
		 m_prevStateName.c_str(), m_stateName.c_str(),
		 stateName(m_monitor.targetState),
		 metaName(m_monitor.metaState));
  if ( new_state == ST_NAME_ERROR )
    return 0;
  else if ( m_prevStateName == new_state )
    return 0;
  return 1;
}

/// Declare process state to DIM service
int OnlineApplication::declareState(State new_state)  {
  m_monitor.state = char(new_state);
  switch(new_state)   {
  case TR_ERROR:
  case ST_ERROR:
    return declareStateByName(ST_NAME_ERROR);
  case ST_NOT_READY:
    return declareStateByName(ST_NAME_NOT_READY);
  case ST_READY:
    return declareStateByName(ST_NAME_READY);
  case ST_STOPPED:
    return declareStateByName(ST_NAME_STOPPED);
  case ST_RUNNING:
    return declareStateByName(ST_NAME_RUNNING);
  case ST_PAUSED:
    return declareStateByName(ST_NAME_PAUSED);
  case ST_OFFLINE:
  default:
    m_monitor.state = ST_OFFLINE;
    return declareStateByName(ST_NAME_OFFLINE);
  }
}

/// Declare FSM sub-state
int OnlineApplication::declareSubState(SubState new_state)  {
  char last_meta = m_monitor.metaState;
  m_monitor.metaState = char(new_state);
  switch(new_state)   {
  case ACTION_SUCCESS:
    m_monitor.doneCmd = ::time(0);
    break;
  case ACTION_EXEC:
    m_monitor.lastCmd = (unsigned long)::time(0);
    break;
  case ACTION_FAILED:
    m_monitor.doneCmd = (unsigned long)::time(0);
    break;
  case ACTION_UNKNOWN:
  default:
    m_monitor.doneCmd = (unsigned long)::time(0);
    m_monitor.metaState = ACTION_UNKNOWN;
    break;
  }
  m_logger->info("PUBLISH(SUB): Previous:%12s Current:%12s Target:%12s Meta-state: %s -> %s",
		 m_prevStateName.c_str(), m_stateName.c_str(),
		 stateName(m_monitor.targetState),
		 metaName(last_meta), metaName(m_monitor.metaState));
  m_fsmService->updateService(&m_monitor,sizeof(m_monitor));
  return 1;
}

/// Incident handler callback. May be overriden by sub-classes
void OnlineApplication::handleIncident(const Incident& incident )     {
  if ( incident.type() == "DAQ_PAUSE" )   {
    this->pause();
  }
  else if ( incident.type() == "DAQ_STOP" )   {
    IOCSENSOR.send(this, STOP);
    //this->stop();
  }
  else if ( incident.type() == "DAQ_CONTINUE" )   {
    this->continuing();
  }
  else if ( incident.type() == "DAQ_ERROR" )   {
    this->setTargetState(ST_ERROR);
    IOCSENSOR.send(this, ERROR);
  }
}

/// Fire an incident to inform clients about DAQ state etc.
int OnlineApplication::fireIncident(const std::string& type)   const  {
  if ( m_incidentSvc )  {
    Incident incident(m_name, type);
    m_incidentSvc->fireIncident(incident);
    m_logger->debug("fireIncident: Fire incident of type: %s",type.c_str());
  }
  return ONLINE_OK;
}

/// Cancel the application
int OnlineApplication::cancel()  {
  return 1;
}

/// Interactor overload: handle IOC Sensor stimuli
void OnlineApplication::handleIoc(int type, void* data)  {
#define _CASE(x)  case x: action = #x;
  const char* action = "UNKNOWN";
  int sc = 0;
  try  {
    switch(type) {
      _CASE(UNLOAD)       sc=unload();                              break;
      _CASE(CONFIGURE)    sc=configure();                           break;
      _CASE(START)        sc=start();                               break;
      _CASE(CANCEL)       sc=cancel();                              break;
      _CASE(STOP)         sc=stop();                                break;
      _CASE(RESET)        sc=reset();                               break;
      _CASE(PAUSE)        sc=pause();                               break;
      _CASE(CONTINUE)     sc=continuing();                          break;
      _CASE(ERROR)        sc=declareState(ST_ERROR);                break;
      _CASE(STARTUP_DONE) sc = startupDone();                       break;
      _CASE(CONNECT_DIM)  sc = connectDIM();                        break;
      _CASE(FIRE_INCIDENT)
	sc = fireIncident(*(std::string*)data);
      delete (std::string*)data;                                 break;
    default:  m_logger->warning("Got Unknown IOC action request: %d",type);   break;
    }
    (sc == 1) ? declareSubState(ACTION_SUCCESS) : declareSubState(ACTION_FAILED);
    return;
  }
  catch(const std::exception& e)  {
    std::string err="Exception executing action:";
    err += action;
    err += " [";
    err += e.what();
    err += "]";
    m_logger->error("Action handling exception: %s",err.c_str());
    declareSubState(ACTION_FAILED);
  }
  catch(...)  {
    std::string err="Unknown exception executing action:";
    err += action;
    m_logger->error("Action handling exception: %s",err.c_str());
    declareSubState(ACTION_FAILED);
  }
}

/// Interactor overload: handle Timer Sensor stimuli
void OnlineApplication::handleTimer(long type)  {
  m_logger->error("Got Unknown TIMER event request of type %ld [%lX]", type, type);
}

/// Interactor overload: handle Sensor stimuli
void OnlineApplication::handle(const Event& ev)  {
  if( ev.eventtype == IocEvent )  {
    this->handleIoc(ev.type, ev.data);
    return;
  }
  else if( ev.eventtype == TimeEvent )  {
    this->handleTimer((long)ev.timer_data);
    return;
  }
  m_logger->error("Got Unknown event request.");
}

/// Invoke automatic stop sequence
int OnlineApplication::autoStop()   {
  if ( m_config->autoStop )    {
    m_logger->info("Invoke auto shutdown sequence.");
    IOCSENSOR.send(this, STOP);
    IOCSENSOR.send(this, RESET);
    IOCSENSOR.send(this, UNLOAD);
  }
  return 1;
}

/// Startup done                                    (Offline    -> NOT_READY)
int OnlineApplication::startupDone()  {
  m_stateName = ST_NAME_NOT_READY;
  if ( !m_command )  {
    connectDIM(0);
  }
  if ( m_config->clazz == 0 )  {
    StatusCode sc;
#if 0
    sc = app->configure();
    if ( !sc.isSuccess() ) {
      m_logger->error("Failed (Gaudi) configure the Class 0 application.");
      return ONLINE_ERROR;
    }
#endif
    sc = app->initialize();
    if ( !sc.isSuccess() ) {
      m_logger->error("Failed to (Gaudi) initialize the Class 0 application.");
      declareState(ST_ERROR);
      return 0;
    }
    sc = app->start();
    if ( !sc.isSuccess() ) {
      m_logger->error("Failed to (Gaudi) start the Class 0 application.");
      declareState(ST_ERROR);
      return 0;
    }
    declareState(ST_NOT_READY);
    return 1;
  }
  declareState(ST_NOT_READY);
  if ( m_config->autoStart )   {
    m_logger->info("Invoke auto startup sequence.");
    IOCSENSOR.send(this, CONFIGURE);
    IOCSENSOR.send(this, START);
  }
  return 1;
}

#define MAKE_TRANSITION(cl, action, success, failure)			\
  if ( m_config->clazz == cl ) {					\
    StatusCode sc = action;						\
    if ( !sc.isSuccess() )   {						\
      m_logger->error("%s Class %d application: Failed to execute the "	\
		      "transition to state %s",				\
		      #action , cl , stateName(success));		\
      fireIncident("DAQ_ERROR");					\
      return declareState(failure);					\
    }									\
    return declareState(success);					\
  }

/// Initialize the application                      (NOT_READY  -> READY)
int OnlineApplication::configureApplication()  {
  SmartIF<ISvcLocator> sloc = app.as<ISvcLocator>();
  if ( this->m_config->clazz == 1 )  {
    StatusCode sc;
#if 0
    sc = app->configure();
    if ( !sc.isSuccess() ) {
      m_logger->error("Failed to configure Gaudi.");
      return ONLINE_ERROR;
    }
#endif
    sc = app->initialize();
    if ( !sc.isSuccess() ) {
      m_logger->error("Failed to initialize Gaudi.");
      return ONLINE_ERROR;
    }
  }
  this->m_incidentSvc = sloc->service<IIncidentSvc>("IncidentSvc");
  if ( !this->m_incidentSvc.get() )  {
    m_logger->error("Failed to access the incident service");
    return ONLINE_ERROR;
  }
  if ( this->m_config->enableStop  ||
       this->m_config->enablePause ||
       this->m_config->enableContinue ||
       this->m_config->enableError)   {
    auto* h = new _IncidentHandler("InternalIncidentHandler", sloc.get());
    h->app = this;
    this->m_incidentListener.reset(h);
    if ( this->m_config->enableStop )
      this->m_incidentSvc->addListener(h, "DAQ_STOP");
    if ( this->m_config->enablePause )
      this->m_incidentSvc->addListener(h, "DAQ_PAUSE");
    if ( this->m_config->enableContinue )
      this->m_incidentSvc->addListener(h, "DAQ_CONTINUE");
    if ( this->m_config->enableError )
      this->m_incidentSvc->addListener(h, "DAQ_ERROR");
  }
  return ONLINE_OK;
}

/// Finalize the application                        (READY  -> NOT_READY)
int OnlineApplication::finalizeApplication()  {
  m_uiSvc.reset();
  m_incidentSvc.reset();
  m_incidentListener.reset();
  if ( m_config->clazz == 1 )  {
    app->finalize().ignore();
    app->terminate().ignore();
  }
  return ONLINE_OK;
}

/// Initialize the application                      (NOT_READY  -> READY)
int OnlineApplication::configure()  {
  int sc = configureApplication();
  if ( sc != ONLINE_OK )   {
    m_logger->error("configure: Class %d application: Failed to execute the "
		    "transition to state %s",m_config->clazz,stateName(ST_READY));
    return declareState(ST_ERROR);
  }
  if ( m_config->clazz == 1 )  {
    fireIncident("DAQ_INITIALIZE");
  }
  return declareState(ST_READY);
}

/// Start the application                           (READY      -> RUNNING)
int OnlineApplication::startApplication()   {
  if ( m_config->clazz == 1 )  {
    StatusCode sc = app->start();
    if ( !sc.isSuccess() ) {
      return ONLINE_ERROR;
    }
  }
  else if ( m_config->clazz == 2 )  {
    StatusCode sc;
#if 0
    sc = app->configure();
    if ( !sc.isSuccess() ) {
      m_logger->error("Failed to configure Gaudi.");
      return ONLINE_ERROR;
    }
#endif
    sc = app->initialize();
    if ( !sc.isSuccess() ) {
      m_logger->error("Failed to (Gaudi) initialize the Class 2 application.");
      return ONLINE_ERROR;
    }
    this->fireIncident("DAQ_INITIALIZE");
    sc = app->start();
    if ( !sc.isSuccess() ) {
      m_logger->error("Failed to (Gaudi) start the Class 2 application.");
      return ONLINE_ERROR;
    }
  }
  this->fireIncident("DAQ_RUNNING");
  this->fireIncident("APP_RUNNING");
  return ONLINE_OK;
}

/// Run application if required
int OnlineApplication::runApplication()   {
  if ( !m_config->runable.empty() )    {
    SmartIF<ISvcLocator> sloc = app.as<ISvcLocator>();
    SmartIF<IRunable>    runable = sloc->service<IRunable>( m_config->runable );
    if ( !runable )    {
      m_logger->error("Failed to access runable %s.", m_config->runable.c_str());
      return ONLINE_ERROR;
    }
    auto sc = runable->run();
    if ( !sc.isSuccess() )    {
      m_logger->error("Failed to run the application manager.");
      return ONLINE_ERROR;
    }
  }
  return ONLINE_OK;
}

/// Start the application                           (READY      -> RUNNING)
int OnlineApplication::start()   {
  int sc = this->startApplication();
  if ( sc != ONLINE_OK )   {
    m_logger->error("start: Class %d application: Failed to execute the "
		    "transition to state %s",m_config->clazz,stateName(ST_RUNNING));
    return this->declareState(ST_ERROR);
  }
  sc = this->runApplication();
  if ( sc != ONLINE_OK )   {
    m_logger->error("Failed to run the application manager.");
    return this->declareState(ST_ERROR);
  }
  return this->declareState(ST_RUNNING);
}

/// Stop the application                            (RUNNING    -> READY)
int OnlineApplication::stop()   {
  if ( m_config->clazz == 1 )   {
    StatusCode sc = app->stop();
    if ( !sc.isSuccess() )   {
      m_logger->error("stop: Class %d application: Failed to execute the "
		      "transition to state %s", m_config->clazz, stateName(ST_READY));
      fireIncident("DAQ_ERROR");
      return declareState(ST_ERROR);
    }
  }
  this->fireIncident("DAQ_STOPPED");
  this->fireIncident("APP_STOPPED");
  return this->stopProcessing();
}

/// Stop the application                            (RUNNING    -> READY)
int OnlineApplication::stopProcessing()   {
  int ret = this->declareState(ST_STOPPED);
  return ret;
}

/// Reset the application                           (READY      -> NOT_READY)
int OnlineApplication::reset()  {
  this->fireIncident("DAQ_FINALIZE");
  int sc = this->finalizeApplication();
  if ( sc != ONLINE_OK )   {
    m_logger->error("reset: Class %d application: Failed to execute the "
		    "transition to state %s",m_config->clazz,stateName(ST_READY));
  }
  return this->declareState(ST_NOT_READY);
}

/// Disconnect process and exit                     (NOT_READY  -> OFFLINE )
int OnlineApplication::unload()  {
  this->setTargetState(ST_OFFLINE);
  this->declareState(ST_OFFLINE);
  ::lib_rtl_sleep(100);
  ::_exit(Gaudi::getAppReturnCode( app.as<IProperty>() ));
  return 1;
}

/// Pause the application                           (RUNNING -> PAUSED )
int OnlineApplication::pause()  {
  this->fireIncident("DAQ_PAUSED");
  return this->pauseProcessing();
}

/// Pause the application                           (RUNNING -> PAUSED )
int OnlineApplication::pauseProcessing()  {
  return this->declareState(ST_PAUSED);
}

/// Continue the application                        (PAUSED -> RUNNING )
int OnlineApplication::continuing()  {
  return continueProcessing();
}

/// Continue the application                        (PAUSED -> RUNNING )
int OnlineApplication::continueProcessing() {
  return this->declareState(ST_RUNNING);
}

/// Invoke transition to error state                ( ****      -> Error   )
int OnlineApplication::error()  {
  cancel();
  IOCSENSOR.send(this, ERROR);
  return 1;
}

/// this method is used to implement the main application logic (prepare to run, loop over events, terminate)
int OnlineApplication::run()   {
  this->setTargetState(ST_NOT_READY);
  IOCSENSOR.send(this, STARTUP_DONE);
  IOCSENSOR.run();
  // NOTE: as of 14/10/2022 this point is never reached, instead _exit is called in unload()
  // - get and propagate the return code the ApplicationMgr whishes to expose
  return Gaudi::getAppReturnCode( app.as<IProperty>() );
}
