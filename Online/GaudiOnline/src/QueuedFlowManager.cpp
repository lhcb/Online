//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Project    : Online
//
//  Author     : Markus Frank
//==========================================================================

/// Framework include files
#include <GaudiOnline/QueuedFlowManager.h>

#include <thread>
#include <tbb/task_arena.h>

using namespace std;
using namespace Online;

namespace {
  template <typename fun> struct EventTask {
    mutable EventContext evtctx;
    fun                  f;
    void                 operator()() const { f( std::move(evtctx)); }
  };
  template <typename fun> EventTask( EventContext&&, fun && )->EventTask<fun>;

  template <typename fun> void enqueue(fun&& f )   {
    static auto arena = std::make_unique<tbb::task_arena>(std::thread::hardware_concurrency());
    arena->enqueue(std::forward<fun>( f ));
  }
} // namespace

/// MinimalEventLoopMgr override: Implementation of IService::start
StatusCode QueuedFlowManager::start(SmartIF<IHiveWhiteBoard> wb)   {
  m_whiteboard = wb;
  m_createEventCond.notify_all();
  return StatusCode::SUCCESS;
}

/// IQueueingEventProcessor override: Schedule the processing of an event.
void QueuedFlowManager::push(EventContext&& ctx)  {
  using namespace std::chrono_literals;
  auto okToStartNewEvt = [&] { return m_whiteboard->freeSlots() > 0;  };  
  while ( 1 )  {
    std::unique_lock<std::mutex> lock{m_createEventMutex};

    if ( m_createEventCond.wait_for( lock, 2ms, okToStartNewEvt ) ) {
      ++m_inFlight;
      ctx.setSlot(m_whiteboard->allocateStore(ctx.evt()));

      EventTask event_task { std::move(ctx), [this](EventContext&& ctx)  {
	pthread_setname_np(pthread_self(), "worker");
	auto ret(processEvent(std::move(ctx)));
	m_createEventCond.notify_all();
	m_done.emplace(ret);
	--m_inFlight;
	return nullptr;
	}};
      enqueue(std::move(event_task));
      return;
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(1));
  }
}
	
/// IQueueingEventProcessor override: Tell if the processor has events in the queues.
bool QueuedFlowManager::empty() const  {
  std::size_t evt_in_flight = m_inFlight.load();
  std::size_t evt_in_done   = m_done.size();
  return evt_in_flight == 0 && evt_in_done == 0;
}

/// IQueueingEventProcessor override: Get the next available result.
std::optional<Gaudi::Interfaces::IQueueingEventProcessor::ResultType> QueuedFlowManager::pop()   {
  ResultType out;
  if ( m_done.try_pop(out) )
    return out;
  return std::nullopt;
}
