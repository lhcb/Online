//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================

#include "GauchoTestProcess.h"

namespace  Online  {

  struct CounterTypeTest : public GauchoTestProcess  {

    typedef unsigned long  ulong;
    typedef unsigned int   uint;
    typedef unsigned short ushort;
    typedef unsigned char  uchar;

    using atomicity = Gaudi::Accumulators::atomicity;

    // declare all sorts of counters with default options (double values, atomicity full)
    mutable Gaudi::Accumulators::Counter<atomicity::full, char>              basic_char_atomic        {   this, "Basic_char_atomic" };
    mutable Gaudi::Accumulators::Counter<atomicity::none, char>              basic_char_non_atomic    {   this, "Basic_char_non_atomic" };
    mutable Gaudi::Accumulators::Counter<atomicity::full, uchar>             basic_uchar_atomic       {   this, "Basic_uchar_atomic" };
    mutable Gaudi::Accumulators::Counter<atomicity::none, uchar>             basic_uchar_non_atomic   {   this, "Basic_uchar_non_atomic" };
    mutable Gaudi::Accumulators::Counter<atomicity::full, short>             basic_short_atomic       {   this, "Basic_short_atomic" };
    mutable Gaudi::Accumulators::Counter<atomicity::none, short>             basic_short_non_atomic   {   this, "Basic_short_non_atomic" };
    mutable Gaudi::Accumulators::Counter<atomicity::full, ushort>            basic_ushort_atomic      {   this, "Basic_ushort_atomic" };
    mutable Gaudi::Accumulators::Counter<atomicity::none, ushort>            basic_ushort_non_atomic  {   this, "Basic_ushort_non_atomic" };
    mutable Gaudi::Accumulators::Counter<atomicity::full, int>               basic_int_atomic         {   this, "Basic_int_atomic" };
    mutable Gaudi::Accumulators::Counter<atomicity::none, int>               basic_int_non_atomic     {   this, "Basic_int_non_atomic" };
    mutable Gaudi::Accumulators::Counter<atomicity::full, uint>              basic_uint_atomic        {   this, "Basic_uint_atomic" };
    mutable Gaudi::Accumulators::Counter<atomicity::none, uint>              basic_uint_non_atomic    {   this, "Basic_uint_non_atomic" };
    mutable Gaudi::Accumulators::Counter<atomicity::full, long>              basic_long_atomic        {   this, "Basic_long_atomic" };
    mutable Gaudi::Accumulators::Counter<atomicity::none, long>              basic_long_non_atomic    {   this, "Basic_long_non_atomic" };
    mutable Gaudi::Accumulators::Counter<atomicity::full, ulong>             basic_ulong_atomic       {   this, "Basic_ulong_atomic" };
    mutable Gaudi::Accumulators::Counter<atomicity::none, ulong>             basic_ulong_non_atomic   {   this, "Basic_ulong_non_atomic" };
#if 0
    mutable Gaudi::Accumulators::Counter<atomicity::full, float>             basic_float_atomic {   this, "Basic_float_atomic" };
    mutable Gaudi::Accumulators::Counter<atomicity::none, float>             basic_float_non_atomic {   this, "Basic_float_non_atomic" };
    mutable Gaudi::Accumulators::Counter<atomicity::full, double>            basic_double_atomic {   this, "Basic_double_atomic" };
    mutable Gaudi::Accumulators::Counter<atomicity::none, double>            basic_double_non_atomic {   this, "Basic_double_non_atomic" };
#endif
    mutable Gaudi::Accumulators::AveragingCounter<char, atomicity::full>     avg_char_atomic{      this, "Average_char_atomic" };
    mutable Gaudi::Accumulators::AveragingCounter<char, atomicity::none>     avg_char_non_atomic{      this, "Average_char_non_atomic" };
    mutable Gaudi::Accumulators::AveragingCounter<uchar, atomicity::full>    avg_uchar_atomic{      this, "Average_uchar_atomic" };
    mutable Gaudi::Accumulators::AveragingCounter<uchar, atomicity::none>    avg_uchar_non_atomic{      this, "Average_uchar_non_atomic" };
    mutable Gaudi::Accumulators::AveragingCounter<short, atomicity::full>    avg_short_atomic{      this, "Average_short_atomic" };
    mutable Gaudi::Accumulators::AveragingCounter<short, atomicity::none>    avg_short_non_atomic{      this, "Average_short_non_atomic" };
    mutable Gaudi::Accumulators::AveragingCounter<ushort, atomicity::full>   avg_ushort_atomic{      this, "Average_ushort_atomic" };
    mutable Gaudi::Accumulators::AveragingCounter<ushort, atomicity::none>   avg_ushort_non_atomic{      this, "Average_ushort_non_atomic" };
    mutable Gaudi::Accumulators::AveragingCounter<int, atomicity::full>      avg_int_atomic{      this, "Average_int_atomic" };
    mutable Gaudi::Accumulators::AveragingCounter<int, atomicity::none>      avg_int_non_atomic{      this, "Average_int_non_atomic" };
    mutable Gaudi::Accumulators::AveragingCounter<uint, atomicity::full>     avg_uint_atomic{      this, "Average_uint_atomic" };
    mutable Gaudi::Accumulators::AveragingCounter<uint, atomicity::none>     avg_uint_non_atomic{      this, "Average_uint_non_atomic" };
    mutable Gaudi::Accumulators::AveragingCounter<long, atomicity::full>     avg_long_atomic{      this, "Average_long_atomic" };
    mutable Gaudi::Accumulators::AveragingCounter<long, atomicity::none>     avg_long_non_atomic{      this, "Average_long_non_atomic" };
    mutable Gaudi::Accumulators::AveragingCounter<ulong, atomicity::full>    avg_ulong_atomic{      this, "Average_ulong_atomic" };
    mutable Gaudi::Accumulators::AveragingCounter<ulong, atomicity::none>    avg_ulong_non_atomic{      this, "Average_ulong_non_atomic" };
    mutable Gaudi::Accumulators::AveragingCounter<double, atomicity::full>   avg_double_atomic{      this, "Average_double_atomic" };
    mutable Gaudi::Accumulators::AveragingCounter<double, atomicity::none>   avg_double_non_atomic{      this, "Average_double_non_atomic" };
    mutable Gaudi::Accumulators::AveragingCounter<float, atomicity::full>    avg_float_atomic{      this, "Average_float_atomic" };
    mutable Gaudi::Accumulators::AveragingCounter<float, atomicity::none>    avg_float_non_atomic{      this, "Average_float_non_atomic" };

    mutable Gaudi::Accumulators::SigmaCounter<char, atomicity::full>         sig_char_atomic{      this, "Sigma_char_atomic" };
    mutable Gaudi::Accumulators::SigmaCounter<char, atomicity::none>         sig_char_non_atomic{      this, "Sigma_char_non_atomic" };
    mutable Gaudi::Accumulators::SigmaCounter<uchar, atomicity::full>        sig_uchar_atomic{      this, "Sigma_uchar_atomic" };
    mutable Gaudi::Accumulators::SigmaCounter<uchar, atomicity::none>        sig_uchar_non_atomic{      this, "Sigma_uchar_non_atomic" };
    mutable Gaudi::Accumulators::SigmaCounter<short, atomicity::full>        sig_short_atomic{      this, "Sigma_short_atomic" };
    mutable Gaudi::Accumulators::SigmaCounter<short, atomicity::none>        sig_short_non_atomic{      this, "Sigma_short_non_atomic" };
    mutable Gaudi::Accumulators::SigmaCounter<ushort, atomicity::full>       sig_ushort_atomic{      this, "Sigma_ushort_atomic" };
    mutable Gaudi::Accumulators::SigmaCounter<ushort, atomicity::none>       sig_ushort_non_atomic{      this, "Sigma_ushort_non_atomic" };
    mutable Gaudi::Accumulators::SigmaCounter<int, atomicity::full>          sig_int_atomic{      this, "Sigma_int_atomic" };
    mutable Gaudi::Accumulators::SigmaCounter<int, atomicity::none>          sig_int_non_atomic{      this, "Sigma_int_non_atomic" };
    mutable Gaudi::Accumulators::SigmaCounter<uint, atomicity::full>         sig_uint_atomic{      this, "Sigma_uint_atomic" };
    mutable Gaudi::Accumulators::SigmaCounter<uint, atomicity::none>         sig_uint_non_atomic{      this, "Sigma_uint_non_atomic" };
    mutable Gaudi::Accumulators::SigmaCounter<long, atomicity::full>         sig_long_atomic{      this, "Sigma_long_atomic" };
    mutable Gaudi::Accumulators::SigmaCounter<long, atomicity::none>         sig_long_non_atomic{      this, "Sigma_long_non_atomic" };
    mutable Gaudi::Accumulators::SigmaCounter<ulong, atomicity::full>        sig_ulong_atomic{      this, "Sigma_ulong_atomic" };
    mutable Gaudi::Accumulators::SigmaCounter<ulong, atomicity::none>        sig_ulong_non_atomic{      this, "Sigma_ulong_non_atomic" };
    mutable Gaudi::Accumulators::SigmaCounter<float, atomicity::full>        sig_float_atomic{      this, "Sigma_float_atomic" };
    mutable Gaudi::Accumulators::SigmaCounter<float, atomicity::none>        sig_float_non_atomic{      this, "Sigma_float_non_atomic" };
    mutable Gaudi::Accumulators::SigmaCounter<double, atomicity::full>       sig_double_atomic{      this, "Sigma_double_atomic" };
    mutable Gaudi::Accumulators::SigmaCounter<double, atomicity::none>       sig_double_non_atomic{      this, "Sigma_double_non_atomic" };

    mutable Gaudi::Accumulators::StatCounter<char, atomicity::full>          stat_char_atomic{     this, "Stat_char_atomic" };
    mutable Gaudi::Accumulators::StatCounter<char, atomicity::none>          stat_char_non_atomic{     this, "Stat_char_non_atomic" };
    mutable Gaudi::Accumulators::StatCounter<uchar, atomicity::full>         stat_uchar_atomic{     this, "Stat_uchar_atomic" };
    mutable Gaudi::Accumulators::StatCounter<uchar, atomicity::none>         stat_uchar_non_atomic{     this, "Stat_uchar_non_atomic" };
    mutable Gaudi::Accumulators::StatCounter<short, atomicity::full>         stat_short_atomic{     this, "Stat_short_atomic" };
    mutable Gaudi::Accumulators::StatCounter<short, atomicity::none>         stat_short_non_atomic{     this, "Stat_short_non_atomic" };
    mutable Gaudi::Accumulators::StatCounter<ushort, atomicity::full>        stat_ushort_atomic{     this, "Stat_ushort_atomic" };
    mutable Gaudi::Accumulators::StatCounter<ushort, atomicity::none>        stat_ushort_non_atomic{     this, "Stat_ushort_non_atomic" };
    mutable Gaudi::Accumulators::StatCounter<int, atomicity::full>           stat_int_atomic{     this, "Stat_int_atomic" };
    mutable Gaudi::Accumulators::StatCounter<int, atomicity::none>           stat_int_non_atomic{     this, "Stat_int_non_atomic" };
    mutable Gaudi::Accumulators::StatCounter<uint, atomicity::full>          stat_uint_atomic{     this, "Stat_uint_atomic" };
    mutable Gaudi::Accumulators::StatCounter<uint, atomicity::none>          stat_uint_non_atomic{     this, "Stat_uint_non_atomic" };
    mutable Gaudi::Accumulators::StatCounter<long, atomicity::full>          stat_long_atomic{     this, "Stat_long_atomic" };
    mutable Gaudi::Accumulators::StatCounter<long, atomicity::none>          stat_long_non_atomic{     this, "Stat_long_non_atomic" };
    mutable Gaudi::Accumulators::StatCounter<ulong, atomicity::full>         stat_ulong_atomic{     this, "Stat_ulong_atomic" };
    mutable Gaudi::Accumulators::StatCounter<ulong, atomicity::none>         stat_ulong_non_atomic{     this, "Stat_ulong_non_atomic" };
    mutable Gaudi::Accumulators::StatCounter<float, atomicity::full>         stat_float_atomic{     this, "Stat_float_atomic" };
    mutable Gaudi::Accumulators::StatCounter<float, atomicity::none>         stat_float_non_atomic{     this, "Stat_float_non_atomic" };
    mutable Gaudi::Accumulators::StatCounter<double, atomicity::full>        stat_double_atomic{     this, "Stat_double_atomic" };
    mutable Gaudi::Accumulators::StatCounter<double, atomicity::none>        stat_double_non_atomic{     this, "Stat_double_non_atomic" };

    mutable Gaudi::Accumulators::BinomialCounter<char, atomicity::full>      binary_char_atomic{ this, "Binary_char_atomic" };
    mutable Gaudi::Accumulators::BinomialCounter<char, atomicity::none>      binary_char_non_atomic{ this, "Binary_char_non_atomic" };
    mutable Gaudi::Accumulators::BinomialCounter<uchar, atomicity::full>     binary_uchar_atomic{ this, "Binary_uchar_atomic" };
    mutable Gaudi::Accumulators::BinomialCounter<uchar, atomicity::none>     binary_uchar_non_atomic{ this, "Binary_uchar_non_atomic" };
    mutable Gaudi::Accumulators::BinomialCounter<short, atomicity::full>     binary_short_atomic{ this, "Binary_short_atomic" };
    mutable Gaudi::Accumulators::BinomialCounter<short, atomicity::none>     binary_short_non_atomic{ this, "Binary_short_non_atomic" };
    mutable Gaudi::Accumulators::BinomialCounter<ushort, atomicity::full>    binary_ushort_atomic{ this, "Binary_ushort_atomic" };
    mutable Gaudi::Accumulators::BinomialCounter<ushort, atomicity::none>    binary_ushort_non_atomic{ this, "Binary_ushort_non_atomic" };
    mutable Gaudi::Accumulators::BinomialCounter<int, atomicity::full>       binary_int_atomic{ this, "Binary_int_atomic" };
    mutable Gaudi::Accumulators::BinomialCounter<int, atomicity::none>       binary_int_non_atomic{ this, "Binary_int_non_atomic" };
    mutable Gaudi::Accumulators::BinomialCounter<uint, atomicity::full>      binary_uint_atomic{ this, "Binary_uint_atomic" };
    mutable Gaudi::Accumulators::BinomialCounter<uint, atomicity::none>      binary_uint_non_atomic{ this, "Binary_uint_non_atomic" };
    mutable Gaudi::Accumulators::BinomialCounter<long, atomicity::full>      binary_long_atomic{ this, "Binary_long_atomic" };
    mutable Gaudi::Accumulators::BinomialCounter<long, atomicity::none>      binary_long_non_atomic{ this, "Binary_long_non_atomic" };
    mutable Gaudi::Accumulators::BinomialCounter<ulong, atomicity::full>     binary_ulong_atomic{ this, "Binary_ulong_atomic" };
    mutable Gaudi::Accumulators::BinomialCounter<ulong, atomicity::none>     binary_ulong_non_atomic{ this, "Binary_ulong_non_atomic" };
    mutable Gaudi::Accumulators::BinomialCounter<float, atomicity::full>     binary_float_atomic{ this, "Binary_float_atomic" };
    mutable Gaudi::Accumulators::BinomialCounter<float, atomicity::none>     binary_float_non_atomic{ this, "Binary_float_non_atomic" };
    mutable Gaudi::Accumulators::BinomialCounter<double, atomicity::full>    binary_double_atomic{ this, "Binary_double_atomic" };
    mutable Gaudi::Accumulators::BinomialCounter<double, atomicity::none>    binary_double_non_atomic{ this, "Binary_double_non_atomic" };

  public:

#define UN_SUPPORTED(x)

    void fill()  const  override {
      basic_char_atomic          += 1;
      basic_char_non_atomic      += 1;
      basic_uchar_atomic         += 1;
      basic_uchar_non_atomic     += 1;
      basic_short_atomic         += 2;
      basic_short_non_atomic     += 2;
      basic_ushort_atomic        += 2;
      basic_ushort_non_atomic    += 2;
      basic_int_atomic           += 3;
      basic_int_non_atomic       += 3;
      basic_uint_atomic          += 3;
      basic_uint_non_atomic      += 3;
      basic_long_atomic          += 5;
      basic_long_non_atomic      += 5;
      basic_ulong_atomic         += 5;
      basic_ulong_non_atomic     += 5;

      avg_char_atomic            += 1;
      avg_char_non_atomic        += 1;
      avg_uchar_atomic           += 1;
      avg_uchar_non_atomic       += 1;
      avg_short_atomic           += 2;
      avg_short_non_atomic       += 2;
      avg_ushort_atomic          += 2;
      avg_ushort_non_atomic      += 2;
      avg_int_atomic             += 3;
      avg_int_non_atomic         += 3;
      avg_uint_atomic            += 3;
      avg_uint_non_atomic        += 3;
      avg_long_atomic            += 5;
      avg_long_non_atomic        += 5;
      avg_ulong_atomic           += 5;
      avg_ulong_non_atomic       += 5;
      avg_float_atomic           += 10e0;
      avg_float_non_atomic       += 10e0;
      avg_double_atomic          += 20e0;
      avg_double_non_atomic      += 20e0;

      sig_char_atomic            += 1;
      sig_char_non_atomic        += 1;
      sig_uchar_atomic           += 1;
      sig_uchar_non_atomic       += 1;
      sig_short_atomic           += 2;
      sig_short_non_atomic       += 2;
      sig_ushort_atomic          += 2;
      sig_ushort_non_atomic      += 2;
      sig_int_atomic             += 3;
      sig_int_non_atomic         += 3;
      sig_uint_atomic            += 3;
      sig_uint_non_atomic        += 3;
      sig_long_atomic            += 5;
      sig_long_non_atomic        += 5;
      sig_ulong_atomic           += 5;
      sig_ulong_non_atomic       += 5;
      sig_float_atomic           += 10e0;
      sig_float_non_atomic       += 10e0;
      sig_double_atomic          += 20e0;
      sig_double_non_atomic      += 20e0;

      stat_char_atomic           += 1;
      stat_char_non_atomic       += 1;
      stat_uchar_atomic          += 1;
      stat_uchar_non_atomic      += 1;
      stat_short_atomic          += 2;
      stat_short_non_atomic      += 2;
      stat_ushort_atomic         += 2;
      stat_ushort_non_atomic     += 2;
      stat_int_atomic            += 3;
      stat_int_non_atomic        += 3;
      stat_uint_atomic           += 3;
      stat_uint_non_atomic       += 3;
      stat_long_atomic           += 5;
      stat_long_non_atomic       += 5;
      stat_ulong_atomic          += 5;
      stat_ulong_non_atomic      += 5;
      stat_float_atomic          += 10e0;
      stat_float_non_atomic      += 10e0;
      stat_double_atomic         += 20e0;
      stat_double_non_atomic     += 20e0;

      binary_char_atomic         += 1;
      binary_char_non_atomic     += 1;
      binary_uchar_atomic        += 1;
      binary_uchar_non_atomic    += 1;
      binary_short_atomic        += 2;
      binary_short_non_atomic    += 2;
      binary_ushort_atomic       += 2;
      binary_ushort_non_atomic   += 2;
      binary_int_atomic          += 3;
      binary_int_non_atomic      += 3;
      binary_uint_atomic         += 3;
      binary_uint_non_atomic     += 3;
      binary_long_atomic         += 5;
      binary_long_non_atomic     += 5;
      binary_ulong_atomic        += 5;
      binary_ulong_non_atomic    += 5;
      binary_float_atomic        += true;
      binary_float_non_atomic    += false;
      binary_double_atomic       += true;
      binary_double_non_atomic   += false;
#if 0
      basic_float_atomic         += 10e0;
      basic_float_non_atomic     += 10e0;
      basic_double_atomic        += 10e0;
      basic_double_non_atomic    += 10e0;
#endif
    }

    using GauchoTestProcess::GauchoTestProcess;
  };
}
/// Factory instantiation
DECLARE_COMPONENT( Online::CounterTypeTest )
