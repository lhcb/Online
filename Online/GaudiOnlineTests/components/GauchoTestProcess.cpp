//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================

/// Framework include files
#include "GauchoTestProcess.h"
#include <RTL/rtl.h>
#include <CPP/Event.h>
#include <CPP/IocSensor.h>
#include <CPP/TimeSensor.h>

/// Online namespace declaration
namespace  Online  {

  /// Initializing constructor
  GauchoTestProcess::GauchoTestProcess(const std::string& name, ISvcLocator* svc)
    : Interactor(), extends::extends(name, svc)
  {
    ::lib_rtl_output(LIB_RTL_ALWAYS, "GauchoTestProcess: Construction finished successfully.");
  }

  /// Default destructor
  GauchoTestProcess::~GauchoTestProcess()  {
    ::lib_rtl_output(LIB_RTL_ALWAYS, "GauchoTestProcess: Destruction finished.");
  }

  /// Dummy fill method
  void GauchoTestProcess::i_fill()  const  {
    ++m_nCalls;
    this->fill();
  }
    
  /// Dummy fill method. To be overloaded by concrete implementations
  void GauchoTestProcess::fill()  const  {
  }
    
  /// Execute single event
  StatusCode GauchoTestProcess::execute(EventContext const& /* ctxt */)  const  {
    i_fill();
    return StatusCode::SUCCESS;
  }

  /// Execute single event
  StatusCode GauchoTestProcess::initialize()  {
    ::lib_rtl_enable_backtrace();
    m_incident = service<IIncidentSvc>("IncidentSvc");
    if( !m_incident )   {
      error() << "+++ Failed to access Incident service." << endmsg;
      return StatusCode::FAILURE;
    }
    m_incident->addListener(this, "APP_RUNNING");
    m_incident->addListener(this, "DAQ_RUNNING");
    m_incident->addListener(this, "DAQ_STOPPED");
    m_incident->addListener(this, "DAQ_CANCEL");
    if( m_handle_daq_idle.value() > 0 )  {
      m_incident->addListener(this, "DAQ_IDLE_TIMEOUT");
    }
    return StatusCode::SUCCESS;
  }

  /// Start the algorithm
  StatusCode GauchoTestProcess::start()  {
    if( m_auto_update > 0 )  {
      info() << "Start auto update every " << m_auto_update.value() << " seconds." << endmsg;
      TimeSensor::instance().add(this, int(m_auto_update.value()), CMD_FILL);
    }
    return StatusCode::SUCCESS;
  }
  
  /// Start the algorithm
  StatusCode GauchoTestProcess::stop()  {
    if( m_auto_update > 0 )  {
      TimeSensor::instance().remove(this);
    }
    return StatusCode::SUCCESS;
  }
  
  /// Handle interaction event
  void GauchoTestProcess::handle(const CPP::Event& ev)  {
    try {
      switch(ev.eventtype)  {
      case TimeEvent:  {
	long cmd = (long)ev.timer_data;
	switch( cmd )  {
	case CMD_PAUSE:
	  IocSensor::instance().send(this, cmd, nullptr);
	  break;
	case CMD_FILL:
	  IocSensor::instance().send(this, cmd, nullptr);
	  break;
	default:
	  break;
	}
	break;
      }
      case IocEvent:
	switch( ev.type )   {
	case CMD_PAUSE:
	  info() << "Invoke transition to PAUSED now." << endmsg;
	  m_incident->fireIncident(Incident(name(), "DAQ_PAUSE"));
	  break;
	case CMD_FILL:
	  i_fill();
	  TimeSensor::instance().add(this, int(m_auto_update.value()), CMD_FILL);
	  break;
	default:
	  break;
	}
	break;
      default:
	break;
      }
    }
    catch(const std::exception& e)  {
      error() << "Exception in callback processing: " << e.what() << endmsg;
    }
    catch(...)  {
      error() << "Exception in callback processing... " << endmsg;
    }
  }

  /// IIncidentListener overload: incident callback handler
  void GauchoTestProcess::handle(const Incident& incident)  {
    info() << "Incidient received: " << incident.type() << endmsg;
    if( incident.type() == "APP_RUNNING" )   {
      long prefill = (m_num_pre_fill >= 0) ? m_num_pre_fill.value() : 1L;
      info() << "Prefilling counter entries with " << prefill << " turns." << endmsg;
      for( long i = 0; i < prefill; ++i )  {
	i_fill();
      }
    }
    else if ( incident.type() == "DAQ_RUNNING" )  {
      if( m_goto_pause > 0 )  {
	info() << "Rearmed transition to PAUSED in " << m_goto_pause << " seconds." << endmsg;
	TimeSensor::instance().add(this, m_goto_pause.value(), CMD_PAUSE);
      }
    }
    else if( incident.type() == "DAQ_STOPPED" )  {
    }
    else if( incident.type() == "DAQ_CANCEL" )  {
      TimeSensor::instance().remove(this);
    }
    else if( incident.type() == "DAQ_IDLE_TIMEOUT" )  {
      this->always() << "+++ DAQ_IDLE_TIMEOUT: Flush pending data." << endmsg;
    }
  }
}

/// Factory instantiation
DECLARE_COMPONENT( Online::GauchoTestProcess )
