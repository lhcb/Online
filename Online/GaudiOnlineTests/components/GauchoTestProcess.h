//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef GAUDIONLINETESTS_GAUDITESTPROCESS_H
#define GAUDIONLINETESTS_GAUDITESTPROCESS_H

/// Framework include files
#include <Gaudi/Algorithm.h>
#include <Gaudi/Accumulators.h>
#include <GaudiKernel/IIncidentSvc.h>
#include <GaudiKernel/RndmGenerators.h>
#include <GaudiKernel/IIncidentListener.h>

#include <RTL/rtl.h>
#include <CPP/Event.h>
#include <CPP/TimeSensor.h>
#include <CPP/Interactor.h>


/// Online namespace declaration
namespace  Online  {

  /// Gaucho test brocess base class
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   */
  struct GauchoTestProcess : public Interactor, public extends<Gaudi::Algorithm, IIncidentListener>  {

    enum  _commands {
      CMD_FILL = 1,
      CMD_PAUSE = 2
    };

    /// Property: Prefill counters 'm_num_pre_fill' times at initialize  (for testing)
    Gaudi::Property<long> m_num_pre_fill {this, "num_prefill", -1, "Prefill counters N times at initialize  (for testing)"};
    Gaudi::Property<int>  m_auto_update  {this, "auto_update", -1, "Auto update with time sensor"};
    Gaudi::Property<int>  m_goto_pause   {this, "goto_pause",  -1, "Goto pause after timeout when in state RUNNING"};

    Gaudi::Property<int>  m_handle_daq_idle{this, "handle_daq_idle", 0, "Handle DAQ_IDLE_TIMEOUT incident."};

    /// Reference to the incident service
    SmartIF<IIncidentSvc> m_incident;

    mutable Gaudi::Accumulators::Counter<>             m_nCalls{ this, "calls" };

    /// Dummy fill method
    void i_fill()  const;

  public:

    /// Initializing constructor
    GauchoTestProcess(const std::string& name, ISvcLocator* svc);

    /// Default destructor
    virtual ~GauchoTestProcess();
    
    /// Dummy fill method. To be overloaded by concrete implementations
    virtual void fill()  const;
    
    /// Execute single event
    virtual StatusCode execute(EventContext const& ctxt)  const  override;

    /// Initialize the algorithm
    virtual StatusCode initialize() override;

    /// Start the algorithm
    virtual StatusCode start() override;

    /// Start the algorithm
    virtual StatusCode stop() override;

    /// Handle interaction event
    virtual void handle(const CPP::Event& ev)  override;

    /// IIncidentListener overload: incident callback handler
    virtual void handle(const Incident& incident)   override;
  };

}      // End namespace Online
#endif // GAUDIONLINETESTS_GAUDITESTPROCESS_H

