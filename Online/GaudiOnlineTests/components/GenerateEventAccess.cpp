//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : Online/GaudiOnlineTests
//
//  Author     : Markus Frank
//==========================================================================

// Include files from the framework
#include <Gaudi/PluginService.h>
#include <EventData/mdf_generator_t.h>
#include <EventHandling/memory_data_guard_t.h>
#include <GaudiOnline/Configuration.h>

/// Online namespace declaration
namespace Online    {

  /// Forward declarations
  class Configuration;
  
  /// Generic MDF File I/O class to be used by the OnlineEvtApp
  /**
   *
   * \version 1.0
   * \author  M.Frank
   */
  class GenerateEventAccess : public EventAccess  {
  public:

    using Factory = Gaudi::PluginService::Factory<EventAccess*(std::unique_ptr<RTL::Logger>&& logger,
							       const Configuration* cfg)>;
    const Online::Configuration* online_config { nullptr };
    Configuration::generic_config_t config     { };
    mdf_generator_t generator                  { };
    int             run_id                     { 123 };
    int             event_id                   { 0 };

  public:
    /// Default constructors and destructors / inhibits
    ONLINE_COMPONENT_CTORS(GenerateEventAccess);

    /// Default constructor
    GenerateEventAccess(std::unique_ptr<RTL::Logger>&& logger, const Configuration* cfg);
    /// Standard destructor
    virtual ~GenerateEventAccess() = default;
    /// Fill the event data cache if necessary
    virtual int fill_cache()  override;
    /// Cancel all pending I/O requests to the buffer manager
    virtual int cancel()  override;
    /// EventAccess overload: Close connection to event data source
    virtual void close()  override;
  };
}    // End namespace Online

#include <EventData/mdf_generator_t.h>

using namespace Online;

/// Initializing constructor
GenerateEventAccess::GenerateEventAccess(std::unique_ptr<RTL::Logger>&& logger,
					 const Configuration* cfg)
  : EventAccess(std::move(logger)), online_config(cfg), config(cfg->generic)
{
  base_config = &config;
}

/// Close filework connection
void GenerateEventAccess::close()  {
  cancel();
  m_cancelled = false;
}

/// Cancel all pending I/O requests to the buffer manager
int GenerateEventAccess::cancel()   {
  m_cancelled = true;
  return ONLINE_OK;
}

/// Fill the event data cache if necessary
int GenerateEventAccess::fill_cache()   {
  generator.packing_factor = config.packingFactor;
  generator.half_window    = config.halfWindow;
  generator.buffer_size    = config.bufferSize;
  generator.have_odin      = true;

  generator.bank_types.clear();
  generator.bank_types.push_back({5, 2, 1, true, bank_types_t::VP,   run3::RO_BIT_VELOA});
  generator.bank_types.push_back({5, 2, 1, true, bank_types_t::VP,   run3::RO_BIT_VELOC});
  generator.bank_types.push_back({4, 2, 1, true, bank_types_t::UT,   run3::RO_BIT_UTA});
  generator.bank_types.push_back({4, 2, 1, true, bank_types_t::UT,   run3::RO_BIT_UTC});
  generator.bank_types.push_back({3, 2, 1, true, bank_types_t::Calo, run3::RO_BIT_ECAL});
  generator.bank_types.push_back({2, 2, 1, true, bank_types_t::Calo, run3::RO_BIT_HCAL});

  while (1)   {
    mem_buff buffer = generator.generate(this->run_id, this->event_id);
    auto  length = buffer.used();
    auto* data   = buffer.release().second;
    auto  type   = event_traits::tell1::data_type;
    auto  evts   = convertMultiMDF(data, length);
    auto  nevt   = evts.first.size();
    std::shared_ptr<memory_data_guard_t<event_collection_t> > burst;
    if ( nevt > config.maxEventsIn )   {
      m_logger->warning("+++ Event limit of %ld events reached. "
			"Drop %ld evenst and exit event loop",
			config.maxEventsIn, nevt-config.maxEventsIn);
      while ( evts.first.size() > config.maxEventsIn )
	evts.first.pop_back();
    }
    config.maxEventsIn -= evts.first.size();
    m_logger->debug("+++ Loading %ld events. Events left: %ld",
		    evts.first.size(), config.maxEventsIn);
    burst = std::make_shared<memory_data_guard_t<event_collection_t> >
      (std::move(evts.second), std::move(evts.first), type, data);
    this->queueBurst(std::move(burst));
    this->printInfo();
    if ( config.maxEventsIn <= 0 )   {
      this->cancel();
      return ONLINE_END_OF_DATA;
    }
    return ONLINE_OK;
  }
};

DECLARE_COMPONENT( GenerateEventAccess )
