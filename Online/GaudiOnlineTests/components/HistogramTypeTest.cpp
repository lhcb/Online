//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#include "GauchoTestProcess.h"
#include <Gaudi/Accumulators/Histogram.h>
#include <GaudiKernel/RndmGenerators.h>
#include <Gaucho/IGauchoMonitorSvc.h>

#include <GAUDI_VERSION.h>
#if GAUDI_MAJOR_VERSION < 37
namespace Gaudi::Monitoring {
  inline void reset( Hub::Entity& e ) { e.reset(); }
  inline void to_json( nlohmann::json& j, const Hub::Entity& c ) {
    j = c.toJSON();
  }
}
#endif

// backward compatibility with Gaudi < v39 where FSMCallbackHolder does not exist
#if GAUDI_MAJOR_VERSION < 39
using BaseClass = Online::GauchoTestProcess;
namespace Gaudi::Accumulators {
  template <unsigned int ND, atomicity Atomicity = atomicity::full, typename Arithmetic = double>
  using StaticHistogram = Histogram<ND, Atomicity, Arithmetic>;
} // namespace Gaudi::Accumulators
#else
#include <Gaudi/FSMCallbackHolder.h>
using BaseClass = Gaudi::FSMCallbackHolder<Online::GauchoTestProcess>;
#endif

/// Online namespace declaration
namespace  Online  {

  /// Histogram test class
  struct HistogramTypeTest : public BaseClass {
    typedef unsigned long  ulong;
    typedef unsigned int   uint;
    typedef unsigned short ushort;
    typedef unsigned char  uchar;

    // non atomic double histogram
    mutable Gaudi::Accumulators::Histogram<1, Gaudi::Accumulators::atomicity::none> m_gauss_1D_noatomic_double{
      this, "Gauss_NA1D_double", "Gaussian mean=0, sigma=1, non atomic double", { 100, -5, 5 } };
    mutable Gaudi::Accumulators::Histogram<2, Gaudi::Accumulators::atomicity::none> m_gauss_2D_noatomic_double{
      this, "Gauss_NA2D_double", "Gaussian V Flat, non atomic double", { { 50, -5, 5 }, { 50, -5, 5 } } };
    mutable Gaudi::Accumulators::Histogram<3, Gaudi::Accumulators::atomicity::none> m_gauss_3D_noatomic_double{
      this, "Gauss_NA3D_double", "Gaussian V Flat V Gaussian, non atomic double",
	  { { 10, -5, 5 }, { 10, -5, 5 }, { 10, -5, 5 } } };

    // non atomic float histogram
    mutable Gaudi::Accumulators::Histogram<1, Gaudi::Accumulators::atomicity::none> m_gauss_1D_noatomic_float{
      this, "Gauss_NA1D_float", "Gaussian mean=0, sigma=1, non atomic float", { 100, -5, 5 } };
    mutable Gaudi::Accumulators::Histogram<2, Gaudi::Accumulators::atomicity::none> m_gauss_2D_noatomic_float{
      this, "Gauss_NA2D_float", "Gaussian V Flat, non atomic float", { { 50, -5, 5 }, { 50, -5, 5 } } };
    mutable Gaudi::Accumulators::Histogram<3, Gaudi::Accumulators::atomicity::none> m_gauss_3D_noatomic_float{
      this, "Gauss_NA3D_float", "Gaussian V Flat V Gaussian, non atomic float",
	  { { 10, -5, 5 }, { 10, -5, 5 }, { 10, -5, 5 } } };

    // non atomic int histogram
    mutable Gaudi::Accumulators::Histogram<1, Gaudi::Accumulators::atomicity::none, int> m_gauss_1D_noatomic_int{
      this, "Gauss_NA1D_int", "Gaussian mean=0, sigma=1, non atomic int", { 100, -5, 5 } };
    mutable Gaudi::Accumulators::Histogram<2, Gaudi::Accumulators::atomicity::none, int> m_gauss_2D_noatomic_int{
      this, "Gauss_NA2D_int", "Gaussian V Flat, non atomic int", { { 50, -5, 5 }, { 50, -5, 5 } } };
    mutable Gaudi::Accumulators::Histogram<3, Gaudi::Accumulators::atomicity::none, int> m_gauss_3D_noatomic_int{
      this, "Gauss_NA3D_int", "Gaussian V Flat V Gaussian, non atomic int",
	  { { 10, -5, 5 }, { 10, -5, 5 }, { 10, -5, 5 } } };

    // non atomic uint histogram
    mutable Gaudi::Accumulators::Histogram<1, Gaudi::Accumulators::atomicity::none, uint> m_gauss_1D_noatomic_uint{
      this, "Gauss_NA1D_uint", "Gaussian mean=0, sigma=1, non atomic uint", { 100, 0, 10 } };
    mutable Gaudi::Accumulators::Histogram<2, Gaudi::Accumulators::atomicity::none, uint> m_gauss_2D_noatomic_uint{
      this, "Gauss_NA2D_uint", "Gaussian V Flat, non atomic uint", { { 50, 0, 10 }, { 50, 0, 10 } } };
    mutable Gaudi::Accumulators::Histogram<3, Gaudi::Accumulators::atomicity::none, uint> m_gauss_3D_noatomic_uint{
      this, "Gauss_NA3D_uint", "Gaussian V Flat V Gaussian, non atomic uint",
	  { { 10, 0, 10 }, { 10, 0, 10 }, { 10, 0, 10 } } };

    // non atomic long histogram
    mutable Gaudi::Accumulators::Histogram<1, Gaudi::Accumulators::atomicity::none, long> m_gauss_1D_noatomic_long{
      this, "Gauss_NA1D_long", "Gaussian mean=0, sigma=1, non atomic long", { 100, -5, 5 } };
    mutable Gaudi::Accumulators::Histogram<2, Gaudi::Accumulators::atomicity::none, long> m_gauss_2D_noatomic_long{
      this, "Gauss_NA2D_long", "Gaussian V Flat, non atomic long", { { 50, -5, 5 }, { 50, -5, 5 } } };
    mutable Gaudi::Accumulators::Histogram<3, Gaudi::Accumulators::atomicity::none, long> m_gauss_3D_noatomic_long{
      this, "Gauss_NA3D_long", "Gaussian V Flat V Gaussian, non atomic long",
	  { { 10, -5, 5 }, { 10, -5, 5 }, { 10, -5, 5 } } };

    // non atomic ulong histogram
    mutable Gaudi::Accumulators::Histogram<1, Gaudi::Accumulators::atomicity::none, ulong> m_gauss_1D_noatomic_ulong{
      this, "Gauss_NA1D_ulong", "Gaussian mean=0, sigma=1, non atomic ulong", { 100, 0, 10 } };
    mutable Gaudi::Accumulators::Histogram<2, Gaudi::Accumulators::atomicity::none, ulong> m_gauss_2D_noatomic_ulong{
      this, "Gauss_NA2D_ulong", "Gaussian V Flat, non atomic ulong", { { 50, 0, 10 }, { 50, 0, 10 } } };
    mutable Gaudi::Accumulators::Histogram<3, Gaudi::Accumulators::atomicity::none, ulong> m_gauss_3D_noatomic_ulong{
      this, "Gauss_NA3D_ulong", "Gaussian V Flat V Gaussian, non atomic ulong",
	  { { 10, 0, 10 }, { 10, 0, 10 }, { 10, 0, 10 } } };

    // "default" case, that is bins containing doubles and atomic
    mutable Gaudi::Accumulators::Histogram<1, Gaudi::Accumulators::atomicity::full, double> m_gauss_1D_atomic_double{
      this, "Gauss_ATO1D_double", "Gaussian mean=0, sigma=1, atomic double", { 100, -5, 5, "X" } };
    mutable Gaudi::Accumulators::Histogram<2, Gaudi::Accumulators::atomicity::full, double> m_gauss_2D_atomic_double{
      this, "Gauss_ATO2D_double", "Gaussian V Flat, atomic double", { { 50, -5, 5, "X" }, { 50, -5, 5, "Y" } } };
    mutable Gaudi::Accumulators::Histogram<3, Gaudi::Accumulators::atomicity::full, double> m_gauss_3D_atomic_double{
      this, "Gauss_ATO3D_double", "Gaussian V Flat V Gaussian, atomic double",
	  { { 10, -5, 5, "X" }, { 10, -5, 5, "Y" }, { 10, -5, 5, "Z" } } };

    // using float
    mutable Gaudi::Accumulators::Histogram<1, Gaudi::Accumulators::atomicity::full, float> m_gauss_1D_atomic_float{
      this, "Gauss_ATO1D_float", "Gaussian mean=0, sigma=1, float values", { 10, 0, 10 } };
    mutable Gaudi::Accumulators::Histogram<2, Gaudi::Accumulators::atomicity::full, float> m_gauss_2D_atomic_float{
      this, "Gauss_ATO2D_float", "Gaussian V Flat, float values", { { 10, 0, 10 }, { 10, 0, 10 } } };
    mutable Gaudi::Accumulators::Histogram<3, Gaudi::Accumulators::atomicity::full, float> m_gauss_3D_atomic_float{
      this, "Gauss_ATO3D_float", "Gaussian V Flat V Gaussian, float values",
	  { { 10, 0, 10 }, { 10, 0, 10 }, { 10, 0, 10 } } };

    // using char
    mutable Gaudi::Accumulators::StaticHistogram<1, Gaudi::Accumulators::atomicity::full, char> m_gauss_1D_atomic_char{
      this, "Gauss_ATO1D_char", "Gaussian mean=0, sigma=1, char values", { 10, 0, 10 } };
    mutable Gaudi::Accumulators::StaticHistogram<2, Gaudi::Accumulators::atomicity::full, char> m_gauss_2D_atomic_char{
      this, "Gauss_ATO2D_char", "Gaussian V Flat, char values", { { 10, 0, 10 }, { 10, 0, 10 } } };
    mutable Gaudi::Accumulators::StaticHistogram<3, Gaudi::Accumulators::atomicity::full, char> m_gauss_3D_atomic_char{
      this, "Gauss_ATO3D_char", "Gaussian V Flat V Gaussian, char values",
	  { { 10, 0, 10 }, { 10, 0, 10 }, { 10, 0, 10 } } };

    // using short
    mutable Gaudi::Accumulators::Histogram<1, Gaudi::Accumulators::atomicity::full, short> m_gauss_1D_atomic_short{
      this, "Gauss_ATO1D_short", "Gaussian mean=0, sigma=1, short values", { 10, 0, 10 } };
    mutable Gaudi::Accumulators::Histogram<2, Gaudi::Accumulators::atomicity::full, short> m_gauss_2D_atomic_short{
      this, "Gauss_ATO2D_short", "Gaussian V Flat, short values", { { 10, 0, 10 }, { 10, 0, 10 } } };
    mutable Gaudi::Accumulators::Histogram<3, Gaudi::Accumulators::atomicity::full, short> m_gauss_3D_atomic_short{
      this, "Gauss_ATO3D_short", "Gaussian V Flat V Gaussian, short values",
	  { { 10, 0, 10 }, { 10, 0, 10 }, { 10, 0, 10 } } };

    // using integers
    mutable Gaudi::Accumulators::Histogram<1, Gaudi::Accumulators::atomicity::full, int> m_gauss_1D_atomic_int{
      this, "Gauss_ATO1D_int", "Gaussian mean=0, sigma=1, integer values", { 10, -5, 5 } };
    mutable Gaudi::Accumulators::Histogram<2, Gaudi::Accumulators::atomicity::full, int> m_gauss_2D_atomic_int{
      this, "Gauss_ATO2D_int", "Gaussian V Flat, integer values", { { 10, -5, 5 }, { 10, -5, 5 } } };
    mutable Gaudi::Accumulators::Histogram<3, Gaudi::Accumulators::atomicity::full, int> m_gauss_3D_atomic_int{
      this, "Gauss_ATO3D_int", "Gaussian V Flat V Gaussian, integer values",
	  { { 10, -5, 5 }, { 10, -5, 5 }, { 10, -5, 5 } } };

    // using longs
    mutable Gaudi::Accumulators::Histogram<1, Gaudi::Accumulators::atomicity::full, long> m_gauss_1D_atomic_long{
      this, "Gauss_ATO1D_long", "Gaussian mean=0, sigma=1, long integer values", { 10, -5, 5 } };
    mutable Gaudi::Accumulators::Histogram<2, Gaudi::Accumulators::atomicity::full, long> m_gauss_2D_atomic_long{
      this, "Gauss_ATO2D_long", "Gaussian V Flat, long integer values", { { 10, -5, 5 }, { 10, -5, 5 } } };
    mutable Gaudi::Accumulators::Histogram<3, Gaudi::Accumulators::atomicity::full, long> m_gauss_3D_atomic_long{
      this, "Gauss_ATO3D_long", "Gaussian V Flat V Gaussian, long integer values",
	  { { 10, -5, 5 }, { 10, -5, 5 }, { 10, -5, 5 } } };

    // using unsigned char
    mutable Gaudi::Accumulators::StaticHistogram<1, Gaudi::Accumulators::atomicity::full, uchar> m_gauss_1D_atomic_uchar{
      this, "Gauss_ATO1D_uchar", "Gaussian mean=0, sigma=1, uchar values", { 10, 0, 10 } };
    mutable Gaudi::Accumulators::StaticHistogram<2, Gaudi::Accumulators::atomicity::full, uchar> m_gauss_2D_atomic_uchar{
      this, "Gauss_ATO2D_uchar", "Gaussian V Flat, uchar values", { { 10, 0, 10 }, { 10, 0, 10 } } };
    mutable Gaudi::Accumulators::StaticHistogram<3, Gaudi::Accumulators::atomicity::full, uchar> m_gauss_3D_atomic_uchar{
      this, "Gauss_ATO3D_uchar", "Gaussian V Flat V Gaussian, uchar values",
	  { { 10, 0, 10 }, { 10, 0, 10 }, { 10, 0, 10 } } };

    // using ushort
    mutable Gaudi::Accumulators::Histogram<1, Gaudi::Accumulators::atomicity::full, ushort> m_gauss_1D_atomic_ushort{
      this, "Gauss_ATO1D_ushort", "Gaussian mean=0, sigma=1, ushort values", { 10, 0, 10 } };
    mutable Gaudi::Accumulators::Histogram<2, Gaudi::Accumulators::atomicity::full, ushort> m_gauss_2D_atomic_ushort{
      this, "Gauss_ATO2D_ushort", "Gaussian V Flat, ushort values", { { 10, 0, 10 }, { 10, 0, 10 } } };
    mutable Gaudi::Accumulators::Histogram<3, Gaudi::Accumulators::atomicity::full, ushort> m_gauss_3D_atomic_ushort{
      this, "Gauss_ATO3D_ushort", "Gaussian V Flat V Gaussian, ushort values",
	  { { 10, 0, 10 }, { 10, 0, 10 }, { 10, 0, 10 } } };

    // using uintegers
    mutable Gaudi::Accumulators::Histogram<1, Gaudi::Accumulators::atomicity::full, uint> m_gauss_1D_atomic_uint{
      this, "Gauss_ATO1D_uint", "Gaussian mean=0, sigma=1, uinteger values", { 10, 0, 10 } };
    mutable Gaudi::Accumulators::Histogram<2, Gaudi::Accumulators::atomicity::full, uint> m_gauss_2D_atomic_uint{
      this, "Gauss_ATO2D_uint", "Gaussian V Flat, uinteger values", { { 10, 0, 10 }, { 10, 0, 10 } } };
    mutable Gaudi::Accumulators::Histogram<3, Gaudi::Accumulators::atomicity::full, uint> m_gauss_3D_atomic_uint{
      this, "Gauss_ATO3D_uint", "Gaussian V Flat V Gaussian, uinteger values",
	  { { 10, 0, 10 }, { 10, 0, 10 }, { 10, 0, 10 } } };

    // using ulongs
    mutable Gaudi::Accumulators::Histogram<1, Gaudi::Accumulators::atomicity::full, ulong> m_gauss_1D_atomic_ulong{
      this, "Gauss_ATO1D_ulong", "Gaussian mean=0, sigma=1, long uinteger values", { 10, 0, 10 } };
    mutable Gaudi::Accumulators::Histogram<2, Gaudi::Accumulators::atomicity::full, ulong> m_gauss_2D_atomic_ulong{
      this, "Gauss_ATO2D_ulong", "Gaussian V Flat, long uinteger values", { { 10, 0, 10 }, { 10, 0, 10 } } };
    mutable Gaudi::Accumulators::Histogram<3, Gaudi::Accumulators::atomicity::full, ulong> m_gauss_3D_atomic_ulong{
      this, "Gauss_ATO3D_ulong", "Gaussian V Flat V Gaussian, long uinteger values",
	  { { 10, 0, 10 }, { 10, 0, 10 }, { 10, 0, 10 } } };

    // weighted version, "default" case
    mutable Gaudi::Accumulators::WeightedHistogram<1> m_gauss_1D_weighted_double{
      this, "Gauss_W1D_double", "Gaussian mean=0, sigma=1, weighted, double", { 100, -5, 5 } };
    mutable Gaudi::Accumulators::WeightedHistogram<2> m_gauss_2D_weighted_double{
      this, "Gauss_W2D_double", "Gaussian V Flat, weighted, double", { { 50, -5, 5 }, { 50, -5, 5 } } };
    mutable Gaudi::Accumulators::WeightedHistogram<3> m_gauss_3D_weighted_double{
      this, "Gauss_W3D_double", "Gaussian V Flat V Gaussian, weighted, double",
	  { { 10, -5, 5 }, { 10, -5, 5 }, { 10, -5, 5 } } };

    // "default" case, dedicated to testing buffers
    mutable Gaudi::Accumulators::Histogram<1> m_gauss_1D_buf_double{
      this, "Gauss_Buf1D_double", "Gaussian mean=0, sigma=1, buffered, double", { 100, -5, 5 } };
    mutable Gaudi::Accumulators::Histogram<2> m_gauss_2D_buf_double{
      this, "Gauss_Buf2D_double", "Gaussian V Flat, buffered, double", { { 50, -5, 5 }, { 50, -5, 5 } } };
    mutable Gaudi::Accumulators::Histogram<3> m_gauss_3D_buf_double{
      this, "Gauss_Buf3D_double", "Gaussian V Flat V Gaussian, buffered, double",
	  { { 10, -5, 5 }, { 10, -5, 5 }, { 10, -5, 5 } } };

    // Testing profiling histograms in all dimensions and features

    // non atomic versions: double values
    mutable Gaudi::Accumulators::ProfileHistogram<1, Gaudi::Accumulators::atomicity::none> m_prof_gauss_1D_noatomic_double{
      this, "Gauss_Prof_NA1D_double", "1D Profile, Gaussian mean=0, sigma=1, non atomic, double", { 100, -5, 5 } };
    mutable Gaudi::Accumulators::ProfileHistogram<2, Gaudi::Accumulators::atomicity::none> m_prof_gauss_2D_noatomic_double{
      this, "Gauss_Prof_NA2D_double", "2D Profile, Gaussian, non atomic, double", { { 50, -5, 5 }, { 50, -5, 5 } } };
    mutable Gaudi::Accumulators::ProfileHistogram<3, Gaudi::Accumulators::atomicity::none> m_prof_gauss_3D_noatomic_double{
      this, "Gauss_Prof_NA3D_double", "3D Profile, Gaussian, non atomic, double",
	  { { 10, -5, 5 }, { 10, -5, 5 }, { 10, -5, 5 } } };

    // non atomic versions: long values
    mutable Gaudi::Accumulators::ProfileHistogram<1, Gaudi::Accumulators::atomicity::none> m_prof_gauss_1D_noatomic_long{
      this, "Gauss_Prof_NA1D_long", "1D Profile, Gaussian mean=0, sigma=1, non atomic, long", { 100, -5, 5 } };
    mutable Gaudi::Accumulators::ProfileHistogram<2, Gaudi::Accumulators::atomicity::none> m_prof_gauss_2D_noatomic_long{
      this, "Gauss_Prof_NA2D_long", "2D Profile, Gaussian, non atomic, long", { { 50, -5, 5 }, { 50, -5, 5 } } };
    mutable Gaudi::Accumulators::ProfileHistogram<3, Gaudi::Accumulators::atomicity::none> m_prof_gauss_3D_noatomic_long{
      this, "Gauss_Prof_NA3D_long", "3D Profile, Gaussian, non atomic, long",
	  { { 10, -5, 5 }, { 10, -5, 5 }, { 10, -5, 5 } } };

    // "default" case, that is bins containing doubles and atomic
    mutable Gaudi::Accumulators::ProfileHistogram<1> m_prof_gauss_1D_atomic_double{
      this, "Gauss_Prof_ATO1D_double", "Profile, Gaussian mean=0, sigma=1, atomic", { 100, -5, 5 } };
    mutable Gaudi::Accumulators::ProfileHistogram<2> m_prof_gauss_2D_atomic_double {
      this, "Gauss_Prof_ATO2D_double", "Profile, Gaussian V Flat, atomic", { { 50, -5, 5 }, { 50, -5, 5 } } };
    mutable Gaudi::Accumulators::ProfileHistogram<3> m_prof_gauss_3D_atomic_double {
      this, "Gauss_Prof_ATO3D_double", "Profile, Gaussian V Flat V Gaussian, atomic",
	  { { 10, -5, 5 }, { 10, -5, 5 }, { 10, -5, 5 } } };

    // using ints internally
    mutable Gaudi::Accumulators::ProfileHistogram<1, Gaudi::Accumulators::atomicity::full, int> m_prof_gauss_1D_atomic_int{
      this, "Gauss_Prof_ATO1D_int", "1D Profile, Gaussian mean=0, sigma=1, int values", { 10, -5, 5 } };
    mutable Gaudi::Accumulators::ProfileHistogram<2, Gaudi::Accumulators::atomicity::full, int> m_prof_gauss_2D_atomic_int{
      this, "Gauss_Prof_ATO2D_int", "2D Profile, Gaussian, int values",  { { 10, -5, 5 }, { 10, -5, 5 } } };
    mutable Gaudi::Accumulators::ProfileHistogram<3, Gaudi::Accumulators::atomicity::full, int> m_prof_gauss_3D_atomic_int{
      this, "Gauss_Prof_ATO3D_int", "3D Profile, Gaussian, int values",
	  { { 10, -5, 5 }, { 10, -5, 5 }, { 10, -5, 5 } } };

    // using uints internally
    mutable Gaudi::Accumulators::ProfileHistogram<1, Gaudi::Accumulators::atomicity::full, uint>  m_prof_gauss_1D_atomic_uint{
      this, "Gauss_Prof_ATO1D_uint", "1D Profile, Gaussian mean=0, sigma=1, atomic, uint values", { 10, 0, 10 } };
    mutable Gaudi::Accumulators::ProfileHistogram<2, Gaudi::Accumulators::atomicity::full, uint> m_prof_gauss_2D_atomic_uint{
      this, "Gauss_Prof_ATO2D_uint", "2D Profile, Gaussian, atomic, uint values",  { { 10, 0, 10 }, { 10, 0, 10 } } };
    mutable Gaudi::Accumulators::ProfileHistogram<3, Gaudi::Accumulators::atomicity::full, uint> m_prof_gauss_3D_atomic_uint{
      this, "Gauss_Prof_ATO3D_uint", "3D Profile, Gaussian, atomic uint values",
	{ { 10, 0, 10 }, { 10, 0, 10 }, { 10, 0, 10 } } };

    // weighted version, "default" case
    mutable Gaudi::Accumulators::WeightedProfileHistogram<1> m_prof_gauss_1D_weighted_double{
      this, "Gauss_Prof_WATO1D_double", "1D Profile, Gaussian mean=0, sigma=1, weighted, double", { 100, -5, 5 } };
    mutable Gaudi::Accumulators::WeightedProfileHistogram<2> m_prof_gauss_2D_weighted_double{
      this, "Gauss_Prof_WATO2D_double", "2D Profile, Gaussian, weighted, double", { { 50, -5, 5 }, { 50, -5, 5 } } };
    mutable Gaudi::Accumulators::WeightedProfileHistogram<3> m_prof_gauss_3D_weighted_double{
      this, "Gauss_Prof_WATO3D_double", "3D Profile, Gaussian, weighted, double",
	  { { 10, -5, 5 }, { 10, -5, 5 }, { 10, -5, 5 } } };

    // weighted version, uint atomic case
    mutable Gaudi::Accumulators::WeightedProfileHistogram<1, Gaudi::Accumulators::atomicity::full, uint> m_prof_gauss_1D_weighted_uint{
      this, "Gauss_Prof_WATO1D_uint", "1D Profile, Gaussian mean=0, sigma=1, atomic, weighted, uint", { 100, 0, 10 } };
    mutable Gaudi::Accumulators::WeightedProfileHistogram<2, Gaudi::Accumulators::atomicity::full, uint> m_prof_gauss_2D_weighted_uint{
      this, "Gauss_Prof_WATO2D_uint", "2D Profile, Gaussian, atomic, weighted, uint", { { 50, 0, 10 }, { 50, 0, 10 } } };
    mutable Gaudi::Accumulators::WeightedProfileHistogram<3, Gaudi::Accumulators::atomicity::full, uint> m_prof_gauss_3D_weighted_uint{
      this, "Gauss_Prof_WATO3D_uint", "3D Profile, Gaussian, atomic, weighted, uint",
	  { { 10, 0, 10 }, { 10, 0, 10 }, { 10, 0, 10 } } };

    // "default" case, dedicated to testing buffers
    mutable Gaudi::Accumulators::ProfileHistogram<1> m_prof_gauss_1D_buf_double{
      this, "Gauss_Prof_WBuf1D_double", "1D Profile, Gaussian mean=0, sigma=1, buffered, double", { 100, -5, 5 } };
    mutable Gaudi::Accumulators::ProfileHistogram<2> m_prof_gauss_2D_buf_double{
      this, "Gauss_Prof_WBuf2D_double", "2D Profile, Gaussian, buffered, double", { { 50, -5, 5 }, { 50, -5, 5 } } };
    mutable Gaudi::Accumulators::ProfileHistogram<3> m_prof_gauss_3D_buf_double{
      this, "Gauss_Prof_WBuf3D_double", "3D Profile, Gaussian, buffered, double",
	  { { 10, -5, 5 }, { 10, -5, 5 }, { 10, -5, 5 } } };

    // uint case, dedicated to testing buffers
    mutable Gaudi::Accumulators::ProfileHistogram<1, Gaudi::Accumulators::atomicity::full, uint> m_prof_gauss_1D_buf_uint{
      this, "Gauss_Prof_WBuf1D_uint", "1D Profile, Gaussian mean=0, sigma=1, buffered, uint", { 100, 0, 10 } };
    mutable Gaudi::Accumulators::ProfileHistogram<2, Gaudi::Accumulators::atomicity::full, uint> m_prof_gauss_2D_buf_uint{
      this, "Gauss_Prof_WBuf2D_uint", "2D Profile, Gaussian, buffered, uint", { { 50, 0, 10 }, { 50, 0, 10 } } };
    mutable Gaudi::Accumulators::ProfileHistogram<3, Gaudi::Accumulators::atomicity::full, uint> m_prof_gauss_3D_buf_uint{
      this, "Gauss_Prof_WBuf3D_uint", "3D Profile, Gaussian, buffered, uint",
	  { { 10, 0, 10 }, { 10, 0, 10 }, { 10, 0, 10 } } };

    // histogram that is created on the first event
    mutable std::mutex               m_emplace_mutex;
    mutable std::optional<Gaudi::Accumulators::Histogram<3, Gaudi::Accumulators::atomicity::full, double>> m_delayed_gauss_3D_atomic_double;

  public:
    using BaseClass::BaseClass;

    template <typename T> void convert(T& hist)   {
      Gaudi::Monitoring::Hub::Entity entity("Component", "name", "type", hist);
      nlohmann::json js = entity;
      std::cout << js << std::endl;
    }

    void fill()  const override  {
      // some random number generators, just to provide numbers
      static Rndm::Numbers Gauss( randSvc(), Rndm::Gauss( 0.0, 1.0 ) );
      static Rndm::Numbers Flat( randSvc(), Rndm::Flat( -10.0, 10.0 ) );
      static Rndm::Numbers Gauss3( randSvc(), Rndm::Gauss( 5.0, 1.0 ) );

      // cache some numbers
      const double gauss( Gauss() );
      const double gauss2( Gauss() );
      const double flat( Flat() );
      const double gauss3( Gauss3() );
      // updating histograms

      /// Non atomic histogram tests
      ++m_gauss_1D_noatomic_double[gauss];
      ++m_gauss_2D_noatomic_double[{ flat, gauss }];
      ++m_gauss_3D_noatomic_double[{ flat, gauss, gauss2 }];
      ++m_gauss_1D_noatomic_float[gauss];
      ++m_gauss_2D_noatomic_float[{ flat, gauss }];
      ++m_gauss_3D_noatomic_float[{ flat, gauss, gauss2 }];

      ++m_gauss_1D_noatomic_int[gauss];
      ++m_gauss_2D_noatomic_int[{ flat, gauss }];
      ++m_gauss_3D_noatomic_int[{ flat, gauss, gauss2 }];
      ++m_gauss_1D_noatomic_uint[gauss+5.0];
      ++m_gauss_2D_noatomic_uint[{ flat+5.0, gauss+5.0 }];
      ++m_gauss_3D_noatomic_uint[{ flat+5.0, gauss+5.0, gauss2+5.0 }];

      ++m_gauss_1D_noatomic_long[gauss];
      ++m_gauss_2D_noatomic_long[{ flat, gauss }];
      ++m_gauss_3D_noatomic_long[{ flat, gauss, gauss2 }];
      ++m_gauss_1D_noatomic_ulong[gauss+5.0];
      ++m_gauss_2D_noatomic_ulong[{ flat+5.0, gauss+5.0 }];
      ++m_gauss_3D_noatomic_ulong[{ flat+5.0, gauss+5.0, gauss2+5.0 }];

      /// Atomic histogram tests
      ++m_gauss_1D_atomic_double[gauss];
      ++m_gauss_2D_atomic_double[{ flat, gauss }];
      ++m_gauss_3D_atomic_double[{ flat, gauss, gauss2 }];
      ++m_gauss_1D_atomic_float[gauss];
      ++m_gauss_2D_atomic_float[{ flat, gauss }];
      ++m_gauss_3D_atomic_float[{ flat, gauss, gauss2 }];

      ++m_gauss_1D_atomic_char[gauss];
      ++m_gauss_2D_atomic_char[{ flat, gauss }];
      ++m_gauss_3D_atomic_char[{ flat, gauss, gauss2 }];
      ++m_gauss_1D_atomic_uchar[gauss+5.0];
      ++m_gauss_2D_atomic_uchar[{ flat+5.0, gauss+5.0 }];
      ++m_gauss_3D_atomic_uchar[{ flat+5.0, gauss+5.0, gauss2+5.0 }];

      ++m_gauss_1D_atomic_short[gauss];
      ++m_gauss_2D_atomic_short[{ flat, gauss }];
      ++m_gauss_3D_atomic_short[{ flat, gauss, gauss2 }];
      ++m_gauss_1D_atomic_ushort[gauss+5.0];
      ++m_gauss_2D_atomic_ushort[{ flat+5.0, gauss+5.0 }];
      ++m_gauss_3D_atomic_ushort[{ flat+5.0, gauss+5.0, gauss2+5.0 }];

      ++m_gauss_1D_atomic_int[gauss];
      ++m_gauss_2D_atomic_int[{ flat, gauss }];
      ++m_gauss_3D_atomic_int[{ flat, gauss, gauss2 }];
      ++m_gauss_1D_atomic_uint[gauss+5.0];
      ++m_gauss_2D_atomic_uint[{ flat+5.0, gauss+5.0 }];
      ++m_gauss_3D_atomic_uint[{ flat+5.0, gauss+5.0, gauss2+5.0 }];

      ++m_gauss_1D_atomic_long[gauss];
      ++m_gauss_2D_atomic_long[{ flat, gauss }];
      ++m_gauss_3D_atomic_long[{ flat, gauss, gauss2 }];
      ++m_gauss_1D_atomic_ulong[gauss+5.0];
      ++m_gauss_2D_atomic_ulong[{ flat+5.0, gauss+5.0 }];
      ++m_gauss_3D_atomic_ulong[{ flat+5.0, gauss+5.0, gauss2+5.0 }];


      // weighted cases
      m_gauss_1D_weighted_double[gauss] += .5;
      m_gauss_2D_weighted_double[{ flat, gauss }] += .5;
      m_gauss_3D_weighted_double[{ flat, gauss, gauss2 }] += .5;

      // using buffers
      auto gauss_1D_buf = m_gauss_1D_buf_double.buffer();
      auto gauss_2D_buf = m_gauss_2D_buf_double.buffer();
      auto gauss_3D_buf = m_gauss_3D_buf_double.buffer();
      for ( uint i = 0; i < 10; i++ ) {
	++gauss_1D_buf[gauss];
	++gauss_2D_buf[{ flat, gauss }];
	++gauss_3D_buf[{ flat, gauss, gauss2 }];
      }

      // updating profile histograms
      m_prof_gauss_1D_noatomic_double[gauss] += gauss3;
      m_prof_gauss_2D_noatomic_double[{ flat, gauss }] += gauss3;
      m_prof_gauss_3D_noatomic_double[{ flat, gauss, gauss2 }] += gauss3;

      m_prof_gauss_1D_noatomic_long[gauss] += gauss3;
      m_prof_gauss_2D_noatomic_long[{ flat, gauss }] += gauss3;
      m_prof_gauss_3D_noatomic_long[{ flat, gauss, gauss2 }] += gauss3;

      m_prof_gauss_1D_atomic_double[gauss] += gauss3;
      m_prof_gauss_2D_atomic_double[{ flat, gauss }] += gauss3;
      m_prof_gauss_3D_atomic_double[{ flat, gauss, gauss2 }] += gauss3;

      m_prof_gauss_1D_atomic_int[gauss] += gauss3;
      m_prof_gauss_2D_atomic_int[{ flat, gauss }] += gauss3;
      m_prof_gauss_3D_atomic_int[{ flat, gauss, gauss2 }] += gauss3;

      m_prof_gauss_1D_atomic_uint[gauss+5.0] += gauss3+5.0;
      m_prof_gauss_2D_atomic_uint[{ flat+5.0, gauss+5.0 }] += gauss3+5.0;
      m_prof_gauss_3D_atomic_uint[{ flat+5.0, gauss+5.0, gauss2+5.0 }] += gauss3+5.0;

      // weighted profile histograms
      m_prof_gauss_1D_weighted_double[gauss] += { gauss3, .5 };
      m_prof_gauss_2D_weighted_double[{ flat, gauss }] += { gauss3, .5 };
      m_prof_gauss_3D_weighted_double[{ flat, gauss, gauss2 }] += { gauss3, .5 };
      m_prof_gauss_1D_weighted_uint[  (gauss+5.0)] += { (uint)gauss3+5.0, 0.5 };
      m_prof_gauss_2D_weighted_uint[{ (flat+5.0), (gauss+5.0) }] += { (uint)gauss3+5.0, 0.5 };
      m_prof_gauss_3D_weighted_uint[{ (flat+5.0), (gauss+5.0), (gauss2+5.0) }] += { (uint)gauss3+5.0, 0.5 };

      // using buffers on profile histograms
      auto prof_gauss_1D_buf_double = m_prof_gauss_1D_buf_double.buffer();
      auto prof_gauss_2D_buf_double = m_prof_gauss_2D_buf_double.buffer();
      auto prof_gauss_3D_buf_double = m_prof_gauss_3D_buf_double.buffer();
      auto prof_gauss_1D_buf_uint   = m_prof_gauss_1D_buf_uint.buffer();
      auto prof_gauss_2D_buf_uint   = m_prof_gauss_2D_buf_uint.buffer();
      auto prof_gauss_3D_buf_uint   = m_prof_gauss_3D_buf_uint.buffer();

      for ( unsigned int i = 0; i < 10; i++ ) {
	prof_gauss_1D_buf_double[gauss] += gauss3;
	prof_gauss_2D_buf_double[{ flat, gauss }] += gauss3;
	prof_gauss_3D_buf_double[{ flat, gauss, gauss2 }] += gauss3;
	prof_gauss_1D_buf_uint  [gauss+5.0] += gauss3+5.0;
	prof_gauss_2D_buf_uint  [{ flat+5.0, gauss+5.0 }] += gauss3+5.0;
	prof_gauss_3D_buf_uint  [{ flat+5.0, gauss+5.0, gauss2+5.0 }] += gauss3+5.0;
      }

      // histogram that is created on the first event
      {
        auto lock = std::scoped_lock{m_emplace_mutex};
        if ( !m_delayed_gauss_3D_atomic_double.has_value() ) {
          using Axis1D = Gaudi::Accumulators::Axis<double>;
          m_delayed_gauss_3D_atomic_double.emplace(this, "delayed_Gauss_ATO3D_double", "Gaussian V Flat V Gaussian, atomic double", Axis1D{ 10, -5, 5, "X" }, Axis1D{ 10, -5, 5, "Y" }, Axis1D{ 10, -5, 5, "Z" });
        }
      }
      ++m_delayed_gauss_3D_atomic_double.value()[{ flat, gauss, gauss2 }];
    }
  };
}

/// Factory instantiation
DECLARE_COMPONENT( Online::HistogramTypeTest )
