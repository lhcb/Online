//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : Online/GaudiOnlineTests
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include <Dataflow/Delay.h>
#include <Dataflow/Incidents.h>
#include <CPP/TimeSensor.h>
#include <CPP/IocSensor.h>
#include <CPP/Event.h>
#include <RTL/rtl.h>
#include <dim/dis.h>

/// Online namespace declaration
namespace Online  {

  /// Generic output test class
  /** Generic output test class
   *
   *   \version 1.0
   *   \author  M.Frank
   */
  class AlignWriterTest : public Delay  {
  protected:
    typedef std::vector<std::pair<std::string, std::pair<int,int> > > Steps_t;
    std::string       m_dns            {   };
    std::string       m_stream         {   };
    std::string       m_partition      {   };
    Steps_t           m_steps          {   };
    Steps_t           m_running_steps  {   };
    Steps_t::iterator m_current        {   };
    int               m_wait_pre_step  { 0 };
    int               m_wait_post_step { 0 };

    long              m_dnsID          { 0 };
    int               m_serviceID      { 0 };
    int               m_serviceData    { 0 };
    int               m_step_event     { 0 };

  public:
    /// Initializing constructor
    AlignWriterTest(const std::string& nam, Context& ctxt)
      : Delay(nam, ctxt)
    {
      declareProperty("DNS",          m_dns = "localhost");
      declareProperty("Steps",        m_steps);
      declareProperty("Stream",       m_stream = "ALIGN");
      declareProperty("Partition",    m_partition = "LHCb");  
      declareProperty("WaitPreStep",  m_wait_pre_step = 0);
      declareProperty("WaitPostStep", m_wait_post_step = 0);
    }

    /// Default destructor
    virtual ~AlignWriterTest() = default;

    /// Initialize the delay component
    virtual int initialize()  override  {
      int sc = this->Delay::initialize();
      std::string svc = m_partition + "/" + m_stream + "/Control";
      if ( m_dnsID == 0 )  {
	m_dnsID     = ::dis_add_dns(m_dns.c_str(), ::dim_get_dns_port());
      }
      m_serviceData = 0;
      m_serviceID = ::dis_add_service_dns(m_dnsID, svc.c_str(), "I", &m_serviceData, sizeof(m_serviceData), nullptr, 0L);
      always("+++ Created DIM steering service: %s value:%d", svc.c_str(), m_serviceData);
      ::dis_update_service(m_serviceID);
      return sc;
    }

    /// Start the delay component
    virtual int start()  override  {
      int sc = this->Delay::start();
      m_running_steps = m_steps;
      m_current = m_running_steps.begin();
      if ( m_current != m_running_steps.end() )    {
	info("+++ Starting alignment test with step: %s #events: %d steering: %d",
	     m_current->first.c_str(), m_current->second.first, m_current->second.second);
      }
      m_step_event = 0;
      return sc;
    }

    /// Finalize the delay component
    virtual int finalize()  override  {
#if 0
      if ( m_dnsID > 0 )  {
	::dis_release_dns(m_dnsID);
	m_dnsID = 0;
      }
#endif
      if ( m_serviceID > 0 )  {
	::dis_remove_service(m_serviceID);
	m_serviceID = 0;
      }
      return this->Delay::finalize();
    }

    /// Data processing overload: process event
    virtual int execute(const Context::EventData& event)  override  {
      this->Delay::execute(event);
      if ( m_current != m_running_steps.end() )    {
	auto& step = m_current->second;
	std::string nam = m_current->first;
	if ( ++m_step_event == 1 )  {
	  info("+++ Executing alignment test step: %s #events: %d steering: %d",
	       nam.c_str(), step.first, step.second);
	  this->delay(nam.c_str(), m_wait_pre_step/2e0);
	  m_serviceData = step.second;
	  ::dis_update_service(m_serviceID);
	  this->delay(nam.c_str(), m_wait_pre_step/2e0);
	}
	if ( --step.first <= 0 )   {
	  this->delay(nam.c_str(), m_wait_post_step/2e0);
	  this->delay(nam.c_str(), m_wait_post_step/2e0);
	  m_step_event = 0;
	  ++m_current;
	}
      }
      return DF_SUCCESS;
    }
  };
}    // End namespace Online

// Include files from the framework
#include <Dataflow/Plugins.h>
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_AlignWriterTest,AlignWriterTest)
