//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : Online/GaudiOnlineTests
//
//  Author     : Markus Frank
//==========================================================================

// Include files from the framework
#include <Dataflow/EventRunable.h>
#include <EventData/mdf_generator_t.h>

/// Online namespace declaration
namespace Online    {

  /// Generic MDF File I/O class to be used by the OnlineEvtApp
  /**
   *
   * \version 1.0
   * \author  M.Frank
   */
  class MDFGenerator : public EventRunable  {
  public:
    std::map<Context::EventData*,mem_buff> buffers;
    mdf_generator_t generator;
    int             run_number    { 123 };
    int             event_number  {   0 };
    int             burst_count   {   0 };
    int             max_bursts    {  -1 };
    
  public:
    /// Default constructor
    MDFGenerator(const std::string& name, Context& ctxt);

    /// Standard destructor
    virtual ~MDFGenerator() = default;
    /// Initialize the component
    virtual int initialize()  override;
    /// Finalize the component
    virtual int finalize()  override;

    /** Event access interface for sub-classes */
    /// Event access interface: Get next event from wherever
    virtual int getEvent(Context::EventData& event)  override;
    /// Event access interface: Release event. Default implementation is empty
    virtual int freeEvent(Context::EventData& event)  override;
  };
}    // End namespace Online

// Include files from the framework
#include <Dataflow/Plugins.h>
#include <MBM/Consumer.h>

DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_MDFGenerator,MDFGenerator)

using bank_descr_t = Online::mdf_generator_t::bank_descr_t;
#include <iostream>
std::ostream& operator << (std::ostream& os, const bank_descr_t& o);
#include <Parsers/spirit/Parsers.h>
#include <Parsers/spirit/ToStream.h>
PARSERS_DECL_FOR_SINGLE(bank_descr_t)
PARSERS_DECL_FOR_LIST(bank_descr_t)

#include <DD4hep/ComponentProperties.h>
#include <DD4hep/detail/BasicGrammar_inl.h>
#include <DD4hep/detail/ComponentProperties_inl.h>

DD4HEP_DEFINE_PARSER_GRAMMAR_CONT_VL(bank_descr_t,dd4hep::eval_none)
DD4HEP_DEFINE_PROPERTY_TYPE(std::vector<bank_descr_t>)

using namespace Online;

/// Initializing constructor
MDFGenerator::MDFGenerator(const std::string& nam, Context& ctxt)
  : EventRunable(nam, ctxt)
{
  declareProperty("HalfWindow",    generator.half_window = 0);
  declareProperty("BufferSize",    generator.buffer_size = 1000000 );
  declareProperty("PackingFactor", generator.packing_factor = 1);
  declareProperty("BankTypes",     generator.bank_types);
  declareProperty("HaveOdin",      generator.have_odin);
  declareProperty("RunNumber",     this->run_number);
  declareProperty("MaxBursts",     this->max_bursts);
}

/// Initialize the component
int MDFGenerator::initialize()  {
  int sc = EventRunable::initialize();
  if ( generator.bank_types.empty() )   {
    generator.bank_types.push_back({5, 2, 1, true, bank_types_t::VP,   run3::RO_BIT_VELOA});
    generator.bank_types.push_back({5, 2, 1, true, bank_types_t::VP,   run3::RO_BIT_VELOC});
    generator.bank_types.push_back({4, 2, 1, true, bank_types_t::UT,   run3::RO_BIT_UTA});
    generator.bank_types.push_back({4, 2, 1, true, bank_types_t::UT,   run3::RO_BIT_UTC});
    generator.bank_types.push_back({3, 2, 1, true, bank_types_t::Calo, run3::RO_BIT_ECAL});
    generator.bank_types.push_back({2, 2, 1, true, bank_types_t::Calo, run3::RO_BIT_HCAL});
  }
  info("+++ run:%d Generating %ld bursts. packing:%ld half-window:%ld odin:%s",
       this->run_number, this->max_bursts, generator.packing_factor, generator.half_window,
       generator.have_odin ? "YES" : "NO");
  info("+++ Bank types list:  typ              det         count length increment");
  for(const auto& b : generator.bank_types)    {
    info("+++                   %3d/%-12s %3d/%-7s %5d %6d %9d",
	 b.type, event_print::bankType(b.type).c_str(),
	 b.detector, event_print::detectorName(b.detector).c_str(),
	 b.count, b.length, b.increment);
  }
  return sc;
}

/// Finalize the component
int MDFGenerator::finalize()  {
  info("+----------------------------------------------------------------");
  info("+++ Successfully generated %d events in %d bursts",
       this->event_number, this->burst_count);
  info("+++ Run: %d Packing: %d TAE half window: %d",
       this->run_number, generator.packing_factor, generator.half_window);
  info("+----------------------------------------------------------------");
  return EventRunable::finalize();
}

/// Get next event from wherever
int MDFGenerator::getEvent(Context::EventData& ev)   {
  ++this->burst_count;
  if ( (this->max_bursts < 0) || (this->burst_count <= this->max_bursts) )    {
    mem_buff buffer = generator.generate(this->run_number, this->event_number);
    auto  length = buffer.used();
    auto* data   = buffer.begin();
    ev.data    = (char*)data;
    ev.header  = (event_header_t*)data;
    ev.length  = length;
    ev.type    = EVENT_TYPE_EVENT;
    ev.mask[0] = ~0x0;
    ev.mask[1] = ~0x0;
    ev.mask[2] = ~0x0;
    ev.mask[3] = ~0x0;
    this->buffers.emplace(&ev, std::move(buffer));
    return DF_SUCCESS;
  }
  info("+++ Maximum number of events reached [%ld / %ld].",
       this->event_number, this->max_bursts);
  fireIncident("DAQ_STOP_TRIGGER");
  fireIncident("DAQ_PAUSE");
  ev = Context::EventData();
  return DF_CANCELLED;
}

/// Release event
int MDFGenerator::freeEvent(Context::EventData& event)   {
  auto iter = this->buffers.find(&event);
  if ( iter != this->buffers.end() )
    this->buffers.erase(iter);
  return DF_SUCCESS;
}

