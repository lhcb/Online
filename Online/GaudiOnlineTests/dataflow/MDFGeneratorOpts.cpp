//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : Online/GaudiOnlineTests
//
//  Author     : Markus Frank
//==========================================================================

// Include files from the framework
#include <map>

#include <Parsers/spirit/Parsers.h>
#include <Parsers/spirit/ParsersFactory.h>
#include <EventData/mdf_generator_t.h>

using bank_descr_t = Online::mdf_generator_t::bank_descr_t;

// ============================================================================
namespace dd4hep   {
  namespace Utils  {
    std::ostream& toStream(const bank_descr_t& o, std::ostream& s);
  }
}
std::ostream& operator << (std::ostream& s, const bank_descr_t& o)   {
  return dd4hep::Utils::toStream(o, s);
}

namespace dd4hep   {
  namespace Utils  {
    std::ostream& toStream(const bank_descr_t& o, std::ostream& s)   {
      return s << "( "
	       << o.count  << " , "
	       << o.length << " , "
	       << o.increment << " , "
	       << o.set_pattern << " , "
	       << (int)o.type << " , "
	       << (int)o.detector
	       << " )" ;
    }
  }
}

// ============================================================================
namespace dd4hep   {
  // ==========================================================================
  namespace Parsers  {
    // ========================================================================
    template< typename Iterator, typename Skipper>
    class mdf_generator_grammar
      : public qi::grammar<Iterator, bank_descr_t(), Skipper>  {
    public:
      // ======================================================================
      typedef bank_descr_t ResultT;
      // ======================================================================
    public:
      struct tag_count{};
      struct tag_length{};
      struct tag_increment{};
      struct tag_set_pattern{};
      struct tag_type{};
      struct Operations {
        // Some magic:
        template <typename A, typename B = boost::fusion::unused_type,
		  typename C = boost::fusion::unused_type,
		  typename D = boost::fusion::unused_type>
        struct result { typedef void type; };
        // Actions:
        void operator()(bank_descr_t& val, const int count, tag_count) const {
	  val.count = std::size_t(count);
        }
        void operator()(bank_descr_t& val, const int length, tag_length) const {
	  val.length = std::size_t(length);
        }
        void operator()(bank_descr_t& val, const int increment, tag_increment) const {
	  val.increment = std::size_t(increment);
        }
        void operator()(bank_descr_t& val, const int set_pattern, tag_set_pattern) const {
	  val.set_pattern = set_pattern;
        }
        void operator()(bank_descr_t& val, const int type, tag_type) const {
	  val.type = bank_types_t::BankType(type);
        }
        void operator()(bank_descr_t& val, const int detector) const {
	  val.detector = run3::run3_detector_bits(detector);
        }
      };
    public:
      mdf_generator_grammar() : mdf_generator_grammar::base_type(para)      {
	para = qi::lit('(')
	  >> qi::int_[op(qi::_val, qi::_1, tag_count())]
	  >> ','
	  >> qi::int_[op(qi::_val, qi::_1, tag_length())]
	  >> ','
	  >> qi::int_[op(qi::_val, qi::_1, tag_increment())]
	  >> ','
	  >> qi::int_[op(qi::_val, qi::_1, tag_set_pattern())]
	  >> ','
	  >> qi::int_[op(qi::_val, qi::_1, tag_type())]
	  >> ','
	  >> qi::int_[op(qi::_val, qi::_1)]
	  >> ')' ;
        }
        qi::rule<Iterator, bank_descr_t(), Skipper> para;
        StringGrammar<Iterator, Skipper> det;
        ph::function<Operations> op;
      // ======================================================================
    };
    REGISTER_GRAMMAR(bank_descr_t, mdf_generator_grammar);

    int parse(bank_descr_t& res, const std::string& in)  {
      return parse_(res, in);
    }
  } //                                         end of namespace dd4hep::Parsers
} //                                                    end of namespace dd4hep

#include <Parsers/spirit/ParsersStandardListCommon.h>
namespace dd4hep   {
  namespace Parsers  {
    IMPLEMENT_STL_PARSER(std::list,bank_descr_t)
    IMPLEMENT_STL_PARSER(std::vector,bank_descr_t)
  } //                                         end of namespace dd4hep::Parsers
} //                                                    end of namespace dd4hep
