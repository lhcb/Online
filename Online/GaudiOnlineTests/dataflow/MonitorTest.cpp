//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : Online/GaudiOnlineTests
//
//  Author     : Markus Frank
//==========================================================================

/// Include files from the framework
#include <Dataflow/DataflowComponent.h>

/// C/C++ include files
#include <array>
#include <vector>
#include <atomic>
#include <cstdint>

// Forward declarations
class TH1D;
class TH2D;
class TProfile;
class TProfile2D;

/// Online namespace declaration
namespace Online    {

  /// Generic output test class
  /** Generic output test class
   *
   *   \version 1.0
   *   \author  M.Frank
   */
  class MonitorTest : public DataflowComponent  {
  public:
    std::string                   used_monitors                {       };
    int                           init_increment               {     0 };
    int                           nbins_x                      {   100 };
    double                        low_x                        { -10.0 };
    double                        high_x                       {  10.0 };
    int                           nbins_y                      {   100 };
    double                        low_y                        { -10.0 };
    double                        high_y                       {  10.0 };

    /// Scalar monitors
    int8_t                        mon_scal_int8                {     0 };
    uint8_t                       mon_scal_uint8               {     0 };
    int16_t                       mon_scal_int16               {     0 };
    uint16_t                      mon_scal_uint16              {     0 };
    int32_t                       mon_scal_int32               {     0 };
    uint32_t                      mon_scal_uint32              {     0 };
    int64_t                       mon_scal_int64               {     0 };
    uint64_t                      mon_scal_uint64              {     0 };
    float                         mon_scal_float               {   0e0 };
    double                        mon_scal_double              {   0e0 };

    /// Atomic scalar monitors
    std::atomic<int8_t>           mon_atm_int8                 {     0 };
    std::atomic<uint8_t>          mon_atm_uint8                {     0 };
    std::atomic<int16_t>          mon_atm_int16                {     0 };
    std::atomic<uint16_t>         mon_atm_uint16               {     0 };
    std::atomic<int32_t>          mon_atm_int32                {     0 };
    std::atomic<uint32_t>         mon_atm_uint32               {     0 };
    std::atomic<int64_t>          mon_atm_int64                {     0 };
    std::atomic<uint64_t>         mon_atm_uint64               {     0 };
    std::atomic<float>            mon_atm_float                {   0e0 };
    std::atomic<double>           mon_atm_double               {   0e0 };


    /// Pait/Ratio monitors
    std::pair<int32_t, int32_t>   mon_pair_int32               {  0, 0 };
    std::pair<int64_t, int64_t>   mon_pair_int64               {  0, 0 };
    
    /// Array monitors
    int8_t                        mon_ptr_int8  [6]  {1, 2, 3, 4, 5, 6 };
    uint8_t                       mon_ptr_uint8 [6]  {1, 2, 3, 4, 5, 6 };
    int16_t                       mon_ptr_int16 [6]  {1, 2, 3, 4, 5, 6 };
    uint16_t                      mon_ptr_uint16[6]  {1, 2, 3, 4, 5, 6 };
    int32_t                       mon_ptr_int32 [6]  {1, 2, 3, 4, 5, 6 };
    uint32_t                      mon_ptr_uint32[6]  {1, 2, 3, 4, 5, 6 };
    int64_t                       mon_ptr_int64 [6]  {1, 2, 3, 4, 5, 6 };
    uint64_t                      mon_ptr_uint64[6]  {1, 2, 3, 4, 5, 6 };
    float                         mon_ptr_float [6]  {0.11, 0.22, 0.33 };
    double                        mon_ptr_double[6]  {0.11, 0.22, 0.33 };

    /// Array monitors
    std::array<int8_t,10>        mon_arr_int8    { 0,0,0,0,0,0,0,0,0,0 };
    std::array<uint8_t,10>       mon_arr_uint8   { 0,0,0,0,0,0,0,0,0,0 };
    std::array<int16_t,10>       mon_arr_int16   { 0,0,0,0,0,0,0,0,0,0 };
    std::array<uint16_t,10>      mon_arr_uint16  { 0,0,0,0,0,0,0,0,0,0 };
    std::array<int32_t,10>       mon_arr_int32   { 0,0,0,0,0,0,0,0,0,0 };
    std::array<uint32_t,10>      mon_arr_uint32  { 0,0,0,0,0,0,0,0,0,0 };
    std::array<int64_t,10>       mon_arr_int64   { 0,0,0,0,0,0,0,0,0,0 };
    std::array<uint64_t,10>      mon_arr_uint64  { 0,0,0,0,0,0,0,0,0,0 };
    std::array<float,10>         mon_arr_float   { 0,0,0,0,0,0,0,0,0,0 };
    std::array<double,10>        mon_arr_double  { 0,0,0,0,0,0,0,0,0,0 };

    /// Vector monitors
    std::vector<int8_t>           mon_vec_int8    { 0,0,0,0,0,0,0,0,0,0 };
    std::vector<uint8_t>          mon_vec_uint8   { 0,0,0,0,0,0,0,0,0,0 };
    std::vector<int16_t>          mon_vec_int16   { 0,0,0,0,0,0,0,0,0,0 };
    std::vector<uint16_t>         mon_vec_uint16  { 0,0,0,0,0,0,0,0,0,0 };
    std::vector<int32_t>          mon_vec_int32   { 0,0,0,0,0,0,0,0,0,0 };
    std::vector<uint32_t>         mon_vec_uint32  { 0,0,0,0,0,0,0,0,0,0 };
    std::vector<int64_t>          mon_vec_int64   { 0,0,0,0,0,0,0,0,0,0 };
    std::vector<uint64_t>         mon_vec_uint64  { 0,0,0,0,0,0,0,0,0,0 };
    std::vector<float>            mon_vec_float   { 0,0,0,0,0,0,0,0,0,0 };
    std::vector<double>           mon_vec_double  { 0,0,0,0,0,0,0,0,0,0 };


    TH1D*                         mon_hist_th1D    { nullptr };
    TH2D*                         mon_hist_th2D    { nullptr };
    TProfile*                     mon_hist_tprof1D { nullptr };
    TProfile2D*                   mon_hist_tprof2D { nullptr };
    
    /// Dump properties to screen
    void dumpMonitors()  const;
    /// Declare monitoring entities
    void declareMonitors();
    /// Increment counters
    void incrementMonitors();
    
  public:
    /// Intializing constructor
    MonitorTest(const std::string& name, Context& ctxt);
    /// Standard destructor
    virtual ~MonitorTest() = default;
    /// Initialize the component
    virtual int initialize()  override;
    /// Start the component
    virtual int start()  override;
    /// Execute event
    virtual int execute(const Context::EventData& /* event */)  override;
  };
}   // End namespace Online

// Include files from the framework
#include <Dataflow/Plugins.h>
#include <Dataflow/Printout.h>

#include <TH1D.h>
#include <TH2D.h>
#include <TProfile.h>
#include <TProfile2D.h>

#include <random>


DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_MonitorTest,MonitorTest)

#include "printers.h"
namespace {  
  template <typename T> int dump_cont(const std::string& , const T&, size_t)    {
    return 1;
  }
  /// Increment monitors
  template <typename T> void increment_item(const std::string& /* tag */, T& value)    {
    value = value + 1;
  }
  /// Increment container monitors
  template <typename T> void increment_cont(const std::string& /* tag */, T& value)    {
    for(auto& v : value) v += 1;
  }
  template <typename T> void increment_cont(const std::string& /* tag */, T& value, size_t len)    {
    for(size_t i=0, n = len/sizeof(T); i<n; ++i)
      value[i] = value[i] + 1;
  }
}

/// Intializing constructor
Online::MonitorTest::MonitorTest(const std::string& nam, Context& ctxt)
  : DataflowComponent(nam, ctxt)
{
  declareProperty("use",               used_monitors);
  declareProperty("initial_increment", init_increment);
  declareProperty("nbins_x",           nbins_x);
  declareProperty("low_x",             low_x);
  declareProperty("high_x",            high_x);  
  declareProperty("nbins_y",           nbins_y);
  declareProperty("low_y",             low_y);
  declareProperty("high_y",            high_y);  
}

/// Declare monitoring entities
void Online::MonitorTest::declareMonitors()   {
  if ( used_monitors.find("SCAL") != std::string::npos )   {
    declareMonitor("mon_scal_int8",   mon_scal_int8);
    declareMonitor("mon_scal_uint8",  mon_scal_uint8);
    declareMonitor("mon_scal_int16",  mon_scal_int16);
    declareMonitor("mon_scal_uint16", mon_scal_uint16);
    declareMonitor("mon_scal_int32",  mon_scal_int32);
    declareMonitor("mon_scal_uint32", mon_scal_uint32);
    declareMonitor("mon_scal_int64",  mon_scal_int64);
    declareMonitor("mon_scal_uint64", mon_scal_uint64);
    declareMonitor("mon_scal_float",  mon_scal_float);           
    declareMonitor("mon_scal_double", mon_scal_double);          
  }
  if ( used_monitors.find("ATOM") != std::string::npos )   {
    declareMonitor("mon_atm_int8",    mon_atm_int8);
    declareMonitor("mon_atm_uint8",   mon_atm_uint8);
    declareMonitor("mon_atm_int16",   mon_atm_int16);
    declareMonitor("mon_atm_uint16",  mon_atm_uint16);
    declareMonitor("mon_atm_int32",   mon_atm_int32);
    declareMonitor("mon_atm_uint32",  mon_atm_uint32);
    declareMonitor("mon_atm_int64",   mon_atm_int64);
    declareMonitor("mon_atm_uint64",  mon_atm_uint64);
    declareMonitor("mon_atm_float",   mon_atm_float);           
    declareMonitor("mon_atm_double",  mon_atm_double);          
  }
  if ( used_monitors.find("POINTER") != std::string::npos )   {
    declareMonitor("mon_ptr_int8",    &mon_ptr_int8[0],   sizeof(mon_ptr_int8),   std::string());
    declareMonitor("mon_ptr_uint8",   &mon_ptr_uint8[0],  sizeof(mon_ptr_uint8),  std::string());
    declareMonitor("mon_ptr_int16",   &mon_ptr_int16[0],  sizeof(mon_ptr_int16),  std::string());
    declareMonitor("mon_ptr_uint16",  &mon_ptr_uint16[0], sizeof(mon_ptr_uint16), std::string());
    declareMonitor("mon_ptr_int32",   &mon_ptr_int32[0],  sizeof(mon_ptr_int32),  std::string());
    declareMonitor("mon_ptr_uint32",  &mon_ptr_uint32[0], sizeof(mon_ptr_uint32), std::string());
    declareMonitor("mon_ptr_int64",   &mon_ptr_int64[0],  sizeof(mon_ptr_int64),  std::string());
    declareMonitor("mon_ptr_uint64",  &mon_ptr_uint64[0], sizeof(mon_ptr_uint64), std::string());
    declareMonitor("mon_ptr_float",   &mon_ptr_float[0],  sizeof(mon_ptr_float),  std::string());
    declareMonitor("mon_ptr_double",  &mon_ptr_double[0], sizeof(mon_ptr_double), std::string());
  }
  if ( used_monitors.find("ARRAY") != std::string::npos )   {
    declareMonitorArray("mon_arr_int8",    mon_arr_int8);
    declareMonitorArray("mon_arr_uint8",   mon_arr_uint8);
    declareMonitorArray("mon_arr_int16",   mon_arr_int16);
    declareMonitorArray("mon_arr_uint16",  mon_arr_uint16);
    declareMonitorArray("mon_arr_int32",   mon_arr_int32);
    declareMonitorArray("mon_arr_uint32",  mon_arr_uint32);
    declareMonitorArray("mon_arr_int64",   mon_arr_int64);
    declareMonitorArray("mon_arr_uint64",  mon_arr_uint64);
    declareMonitorArray("mon_arr_float",   mon_arr_float);
    declareMonitorArray("mon_arr_double",  mon_arr_double);
    
  }
  if ( used_monitors.find("VECTOR") != std::string::npos )   {
    declareMonitor("mon_vec_int8",    mon_vec_int8);
    declareMonitor("mon_vec_uint8",   mon_vec_uint8);
    declareMonitor("mon_vec_int16",   mon_vec_int16);
    declareMonitor("mon_vec_uint16",  mon_vec_uint16);
    declareMonitor("mon_vec_int32",   mon_vec_int32);
    declareMonitor("mon_vec_uint32",  mon_vec_uint32);
    declareMonitor("mon_vec_int64",   mon_vec_int64);
    declareMonitor("mon_vec_uint64",  mon_vec_uint64);
    declareMonitor("mon_vec_float",   mon_vec_float);
    declareMonitor("mon_vec_double",  mon_vec_double);
  }
  if ( used_monitors.find("HISTO") != std::string::npos )   {
    mon_hist_th1D    = new TH1D((this->name+"/TH1D").c_str(),
                                "1-dimesional histogram", nbins_x, low_x, high_x);
    mon_hist_tprof1D = new TProfile((this->name+"/TProfile").c_str(),
                                    "1-dimesional profile histogram", nbins_x, -low_x, high_x);
    mon_hist_th2D    = new TH2D((this->name+"/TH2D").c_str(),
                                "2-dimesional histogram",
                                nbins_x, low_x, high_x, nbins_y, low_y, high_y);
    mon_hist_tprof2D = new TProfile2D((this->name+"/TProfile2D").c_str(),
                                      "1-dimesional profile histogram",
                                      nbins_x, low_x, high_x, nbins_y, low_y, high_y);

    declareMonitor("mon_hist_bhist1D", *mon_hist_th1D);
    declareMonitor("mon_hist_bprof1D", *mon_hist_tprof1D);
    declareMonitor("mon_hist_ahist2D", *mon_hist_th2D);
    declareMonitor("mon_hist_aprof2D", *mon_hist_tprof2D);
  }
}

/// Dump properties to screen
void Online::MonitorTest::dumpMonitors()  const    {
  int result = 0;
  const char* line = "+----------------------------------------------------------------------------------------------------------";
  std::cout << line << std::endl;

  if ( used_monitors.find("SCAL") != std::string::npos )   {
    result += dump_item("mon_scal_int8",   mon_scal_int8);
    result += dump_item("mon_scal_uint8",  mon_scal_uint8);
    result += dump_item("mon_scal_int16",  mon_scal_int16);
    result += dump_item("mon_scal_uint16", mon_scal_uint16);
    result += dump_item("mon_scal_int32",  mon_scal_int32);
    result += dump_item("mon_scal_uint32", mon_scal_uint32);
    result += dump_item("mon_scal_int64",  mon_scal_int64);
    result += dump_item("mon_scal_uint64", mon_scal_uint64);
    result += dump_item("mon_scal_float",  mon_scal_float);           
    result += dump_item("mon_scal_double", mon_scal_double);          
    std::cout << line << std::endl;
  }
  if ( used_monitors.find("ATOM") != std::string::npos )   {
    result += dump_item("mon_atm_int8",    mon_atm_int8);
    result += dump_item("mon_atm_uint8",   mon_atm_uint8);
    result += dump_item("mon_atm_int16",   mon_atm_int16);
    result += dump_item("mon_atm_uint16",  mon_atm_uint16);
    result += dump_item("mon_atm_int32",   mon_atm_int32);
    result += dump_item("mon_atm_uint32",  mon_atm_uint32);
    result += dump_item("mon_atm_int64",   mon_atm_int64);
    result += dump_item("mon_atm_uint64",  mon_atm_uint64);
    result += dump_item("mon_atm_float",   mon_atm_float);           
    result += dump_item("mon_atm_double",  mon_atm_double);          
    std::cout << line << std::endl;
  }
  if ( used_monitors.find("POINTER") != std::string::npos )   {
    result += dump_cont("mon_ptr_int8",    mon_ptr_int8,   sizeof(mon_ptr_int8));
    result += dump_cont("mon_ptr_uint8",   mon_ptr_uint8,  sizeof(mon_ptr_uint8));
    result += dump_cont("mon_ptr_int16",   mon_ptr_int16,  sizeof(mon_ptr_int16));
    result += dump_cont("mon_ptr_uint16",  mon_ptr_uint16, sizeof(mon_ptr_uint16));
    result += dump_cont("mon_ptr_int32",   mon_ptr_int32,  sizeof(mon_ptr_int32));
    result += dump_cont("mon_ptr_uint32",  mon_ptr_uint32, sizeof(mon_ptr_uint32));
    result += dump_cont("mon_ptr_int64",   mon_ptr_int64,  sizeof(mon_ptr_int64));
    result += dump_cont("mon_ptr_uint64",  mon_ptr_uint64, sizeof(mon_ptr_uint64));
    result += dump_cont("mon_ptr_float",   mon_ptr_float,  sizeof(mon_ptr_float));
    result += dump_cont("mon_ptr_double",  mon_ptr_double, sizeof(mon_ptr_double));
    std::cout << line << std::endl;
  }
  if ( used_monitors.find("ARRAY") != std::string::npos )   {
    result += dump_cont("mon_arr_int8",    mon_arr_int8);
    result += dump_cont("mon_arr_uint8",   mon_arr_uint8);
    result += dump_cont("mon_arr_int16",   mon_arr_int16);
    result += dump_cont("mon_arr_uint16",  mon_arr_uint16);
    result += dump_cont("mon_arr_int32",   mon_arr_int32);
    result += dump_cont("mon_arr_uint32",  mon_arr_uint32);
    result += dump_cont("mon_arr_int64",   mon_arr_int64);
    result += dump_cont("mon_arr_uint64",  mon_arr_uint64);
    result += dump_cont("mon_arr_float",   mon_arr_float);
    result += dump_cont("mon_arr_double",  mon_arr_double);
    std::cout << line << std::endl;
  }
  if ( used_monitors.find("VECTOR") != std::string::npos )   {
    result += dump_cont("mon_vec_int8",    mon_vec_int8);
    result += dump_cont("mon_vec_uint8",   mon_vec_uint8);
    result += dump_cont("mon_vec_int16",   mon_vec_int16);
    result += dump_cont("mon_vec_uint16",  mon_vec_uint16);
    result += dump_cont("mon_vec_int32",   mon_vec_int32);
    result += dump_cont("mon_vec_uint32",  mon_vec_uint32);
    result += dump_cont("mon_vec_int64",   mon_vec_int64);
    result += dump_cont("mon_vec_uint64",  mon_vec_uint64);
    result += dump_cont("mon_vec_float",   mon_vec_float);
    result += dump_cont("mon_vec_double",  mon_vec_double);
    std::cout << line << std::endl;
  }
  if ( 0 == result )
    std::cout << "| Test PASSED" << std::endl;
  else
    std::cout << "| Test FAILED" << std::endl
	      << "| ===> " << result << " Subtests FAILED" << std::endl;
  std::cout << line << std::endl;
}

/// Increment counters
void Online::MonitorTest::incrementMonitors()   {
  if ( used_monitors.find("SCAL") != std::string::npos )   {
    increment_item("mon_scal_int8",   mon_scal_int8);
    increment_item("mon_scal_uint8",  mon_scal_uint8);
    increment_item("mon_scal_int16",  mon_scal_int16);
    increment_item("mon_scal_uint16", mon_scal_uint16);
    increment_item("mon_scal_int32",  mon_scal_int32);
    increment_item("mon_scal_uint32", mon_scal_uint32);
    increment_item("mon_scal_int64",  mon_scal_int64);
    increment_item("mon_scal_uint64", mon_scal_uint64);
    increment_item("mon_scal_float",  mon_scal_float);           
    increment_item("mon_scal_double", mon_scal_double);          
  }
  if ( used_monitors.find("ATOM") != std::string::npos )   {
    increment_item("mon_atm_int8",    mon_atm_int8);
    increment_item("mon_atm_uint8",   mon_atm_uint8);
    increment_item("mon_atm_int16",   mon_atm_int16);
    increment_item("mon_atm_uint16",  mon_atm_uint16);
    increment_item("mon_atm_int32",   mon_atm_int32);
    increment_item("mon_atm_uint32",  mon_atm_uint32);
    increment_item("mon_atm_int64",   mon_atm_int64);
    increment_item("mon_atm_uint64",  mon_atm_uint64);
    increment_item("mon_atm_float",   mon_atm_float);           
    increment_item("mon_atm_double",  mon_atm_double);          
  }
  if ( used_monitors.find("POINTER") != std::string::npos )   {
    increment_cont("mon_ptr_int8",    mon_ptr_int8);
    increment_cont("mon_ptr_uint8",   mon_ptr_uint8);
    increment_cont("mon_ptr_int16",   mon_ptr_int16);
    increment_cont("mon_ptr_uint16",  mon_ptr_uint16);
    increment_cont("mon_ptr_int32",   mon_ptr_int32);
    increment_cont("mon_ptr_uint32",  mon_ptr_uint32);
    increment_cont("mon_ptr_int64",   mon_ptr_int64);
    increment_cont("mon_ptr_uint64",  mon_ptr_uint64);
    increment_cont("mon_ptr_float",   mon_ptr_float);
    increment_cont("mon_ptr_double",  mon_ptr_double);
  }
  if ( used_monitors.find("ARRAY") != std::string::npos )   {
    increment_cont("mon_arr_int8",    mon_arr_int8);
    increment_cont("mon_arr_uint8",   mon_arr_uint8);
    increment_cont("mon_arr_int16",   mon_arr_int16);
    increment_cont("mon_arr_uint16",  mon_arr_uint16);
    increment_cont("mon_arr_int32",   mon_arr_int32);
    increment_cont("mon_arr_uint32",  mon_arr_uint32);
    increment_cont("mon_arr_int64",   mon_arr_int64);
    increment_cont("mon_arr_uint64",  mon_arr_uint64);
    increment_cont("mon_arr_float",   mon_arr_float);
    increment_cont("mon_arr_double",  mon_arr_double);
  }
  if ( used_monitors.find("VECTOR") != std::string::npos )   {
    increment_cont("mon_vec_int8",    mon_vec_int8);
    increment_cont("mon_vec_uint8",   mon_vec_uint8);
    increment_cont("mon_vec_int16",   mon_vec_int16);
    increment_cont("mon_vec_uint16",  mon_vec_uint16);
    increment_cont("mon_vec_int32",   mon_vec_int32);
    increment_cont("mon_vec_uint32",  mon_vec_uint32);
    increment_cont("mon_vec_int64",   mon_vec_int64);
    increment_cont("mon_vec_uint64",  mon_vec_uint64);
    increment_cont("mon_vec_float",   mon_vec_float);
    increment_cont("mon_vec_double",  mon_vec_double);
  }
  if ( used_monitors.find("HISTO") != std::string::npos )   {
    std::random_device rd {};
    std::mt19937       gen {rd()};
    std::normal_distribution dist_x((high_x + low_x)/2e0, (high_x - low_x)/10e0);
    std::normal_distribution dist_y((high_y + low_y)/2e0, (high_y - low_y)/10e0);
    
    double val_x = dist_x(gen);
    double val_y = dist_y(gen);

    mon_hist_th1D->Fill(val_x, 1.0);
    mon_hist_th2D->Fill(val_x, val_y, 1.0);
    mon_hist_tprof1D->Fill(val_x, 1.0);
    mon_hist_tprof2D->Fill(val_x, val_y, 1.0);
  }
}

/// Initialize the component
int Online::MonitorTest::initialize()    {
  int sc = this->DataflowComponent::initialize();
  if ( sc == DF_SUCCESS )   {
    used_monitors = RTL::str_upper( used_monitors );
    this->declareMonitors();
    for( int i=0; i<this->init_increment; ++i )
      this->incrementMonitors();
  }
  return sc;
}

/// Start the component
int Online::MonitorTest::start()    {
  int sc = this->DataflowComponent::start();
  return sc;
}

/// Execute event
int Online::MonitorTest::execute(const Context::EventData& /* event */)    {
  this->incrementMonitors();
  return DF_SUCCESS;
}

