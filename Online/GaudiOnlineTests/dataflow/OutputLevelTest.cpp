//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : Online/GaudiOnlineTests
//
//  Author     : Markus Frank
//==========================================================================

/// Include files from the framework
#include <Dataflow/DataflowComponent.h>

/// Online namespace declaration
namespace Online    {

  /// Generic output test class
  /** Generic output test class
   *
   *   \version 1.0
   *   \author  M.Frank
   */
  class OutputLevelTest : public DataflowComponent  {
  private:
    /// Convert printout level to string
    const char* _level(int level)   const;
    /// Do test printout
    int         _printout()  const;
    
  public:
    /// Default constructor
    using DataflowComponent::DataflowComponent;

    /// Start the component
    virtual int start()  override;
    /// Stop the component
    virtual int stop()  override;
  };
}    // End namespace Online

// Include files from the framework
#include <Dataflow/Plugins.h>
#include <Dataflow/Printout.h>
DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_OutputLevelTest,OutputLevelTest)

/// Convert printout level to string
const char* Online::OutputLevelTest::_level(int level)   const   {
  if      ( level < NOLOG ) level = NOLOG;
  else if ( level > ALWAYS ) level = ALWAYS;
  switch ( level )   {
  case NOLOG:     return "[NOLOG]  ";
  case VERBOSE:   return "[VERBOSE]";
  case DEBUG:     return "[DEBUG]  ";
  case INFO:      return "[INFO]   ";
  case WARNING:   return "[WARNING]";
  case ERROR:     return "[ERROR]  ";
  case FATAL:     return "[FATAL]  ";
  case ALWAYS:    return "[ALWAYS] ";
  default:        return "[UNKNOWN]";
  }
}

/// Do test printout
int Online::OutputLevelTest::_printout()   const  {
  for( int i = NOLOG; i <= ALWAYS; ++i )   {
    this->output(i, "+++ Printout test of %-20s  component output level: %d %-s  printout level: %d %s",
		 this->name.c_str(), this->outputLevel, _level(this->outputLevel), i, _level(i));
  }
  return DF_SUCCESS;
}

/// Start the component
int Online::OutputLevelTest::start()    {
  int sc = DataflowComponent::start();
  if ( sc == DF_SUCCESS )  {
    this->_printout();
  }
  return sc;
}

/// Stop the component
int Online::OutputLevelTest::stop()  {
  this->_printout();
  return DataflowComponent::stop();
}

