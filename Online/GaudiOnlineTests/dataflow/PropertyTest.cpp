//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : Online/GaudiOnlineTests
//
//  Author     : Markus Frank
//==========================================================================

/// Include files from the framework
#include <Dataflow/DataflowComponent.h>
#include <RTL/strdef.h>

/// ROOT include files
#include <Math/Point3D.h>
#include <Math/Vector4D.h>
#include <Math/Vector3D.h>

/// C/C++ include files
#include <map>
#include <set>
#include <list>
#include <vector>

/// Online namespace declaration
namespace Online    {

  /// Generic output test class
  /** Generic output test class
   *
   *   \version 1.0
   *   \author  M.Frank
   */
  class PropertyTest : public DataflowComponent  {
  public:
    using XYZPoint  = ROOT::Math::XYZPoint;
    using XYZVector = ROOT::Math::XYZVector;
    using PxPyPzE   = ROOT::Math::PxPyPzEVector;
    typedef unsigned int  uint;
    typedef unsigned long ulong;

    std::string                   used_properties          {       };

    std::string                   prop_str     {       };     // 'a'
    bool                          prop_bool    { false };     // true
    int                           prop_int     {     0 };     // 11
    uint                          prop_uint    {     0 };     // 11
    long                          prop_long    {     0 };     // 11
    ulong                         prop_ulong   {     0 };     // 11
    float                         prop_float   {   0e0 };     // 1.11
    double                        prop_double  {   0e0 };     // 1.11
    XYZPoint                      prop_XYZPoint;              // (1, 2, 3)
    XYZVector                     prop_XYZVector;             // (1, 2, 3)
    PxPyPzE                       prop_PxPyPzEVector;         // (1, 2, 3, 4)
    
    std::set<std::string>         set_str;                    // {'a',                    'bb',              'ccc'}
    std::set<bool>                set_bool;                   // {true,                   false,              true}
    std::set<int>                 set_int;                    // {11,                     222,               3333}
    std::set<uint>                set_uint;                   // {11,                     222,               3333}
    std::set<long>                set_long;                   // {11,                     222,               3333}
    std::set<ulong>               set_ulong;                  // {11,                     222,               3333}
    std::set<float>               set_float;                  // {1.11,                   22.22,             333.33}
    std::set<double>              set_double;                 // {1.11,                   22.22,             333.33}
    std::set<XYZPoint>            set_XYZPoint;               // {(1, 2, 3),              (10,20,30),       (100,200,300)}
    std::set<XYZVector>           set_XYZVector;              // {(1, 2, 3),              (10,20,30),       (100,200,300)}
    std::set<PxPyPzE>             set_PxPyPzEVector;          // {(1, 2, 3,4),            (10,20,30,40),    (100,200,300,400)}

    std::vector<std::string>      vector_str;                 // {'a',                    'bb',              'ccc'}
    std::vector<bool>             vector_bool;                // {true,                   false,              true}
    std::vector<int>              vector_int;                 // {11,                     222,               3333}
    std::vector<uint>             vector_uint;                // {11,                     222,               3333}
    std::vector<long>             vector_long;                // {11,                     222,               3333}
    std::vector<ulong>            vector_ulong;               // {11,                     222,               3333}
    std::vector<float>            vector_float;               // {1.11,                   22.22,             333.33}
    std::vector<double>           vector_double;              // {1.11,                   22.22,             333.33}
    std::vector<XYZPoint>         vector_XYZPoint;            // {(1, 2, 3),              (10,20,30), (100,200,300)}
    std::vector<XYZVector>        vector_XYZVector;           // {(1, 2, 3),              (10,20,30), (100,200,300)}
    std::vector<PxPyPzE>          vector_PxPyPzEVector;       // {(1, 2, 3,4),            (10,20,30,40),    (100,200,300,400)}

    std::list<std::string>        list_str;                   // {'a',                    'bb',              'ccc'}
    std::list<bool>               list_bool;                  // {true,                   false,              true}
    std::list<int>                list_int;                   // {11,                     222,               3333}
    std::list<uint>               list_uint;                  // {11,                     222,               3333}
    std::list<long>               list_long;                  // {11,                     222,               3333}
    std::list<ulong>              list_ulong;                 // {11,                     222,               3333}
    std::list<float>              list_float;                 // {1.11,                   22.22,             333.33}
    std::list<double>             list_double;                // {1.11,                   22.22,             333.33}
    std::list<XYZPoint>           list_XYZPoint;              // {(1, 2, 3),              (10,20,30), (100,200,300)}
    std::list<XYZVector>          list_XYZVector;             // {(1, 2, 3),              (10,20,30), (100,200,300)}
    std::list<PxPyPzE>            list_PxPyPzEVector;         // {(1, 2, 3,4),            (10,20,30,40),    (100,200,300,400)}

    std::map<int, std::string>    map_int_str;                // { 10:   'a',        200:   'bb',        3000: '    ccc'}
    std::map<int, bool>           map_int_bool;               // { 10:   true,       200:  false,        3000:      true}
    std::map<int, int>            map_int_int;                // { 10:   11,         200:  200,          3000:      3000}
    std::map<int, uint>           map_int_uint;               // { 10:   11,         200:  200,          3000:      3000}
    std::map<int, long>           map_int_long;               // { 10:   11,         200:  200,          3000:      3000}
    std::map<int, ulong>          map_int_ulong;              // { 10:   11,         200:  200,          3000:      3000}
    std::map<int, float>          map_int_float;              // { 10:   1.11,       200:   22.22,       3000:    333.33}
    std::map<int, double>         map_int_double;             // { 10:   1.11,       200:   22.22,       3000:    333.33}

    std::map<std::string, std::string> map_str_str;           // {'a':   'a',      'b': 'bb',        'c': 'ccc'}
    std::map<std::string, bool>        map_str_bool;          // {'a':   true,     'b':  false,      'c':  true}
    std::map<std::string, int>         map_str_int;           // {'a':   11,       'b':  200,        'c':  3000}
    std::map<std::string, uint>        map_str_uint;          // {'a':   11,       'b':  200,        'c':  3000}
    std::map<std::string, long>        map_str_long;          // {'a':   11,       'b':  200,        'c':  3000}
    std::map<std::string, ulong>       map_str_ulong;         // {'a':   11,       'b':  200,        'c':  3000}
    std::map<std::string, float>       map_str_float;         // {'a':   1.11,     'b': 22.22,       'c': 333.33}
    std::map<std::string, double>      map_str_double;        // {'a':   1.11,     'b': 22.22,       'c': 333.33}
    std::map<std::string, std::vector<std::string> > map_str_vec_str;
    std::map<std::string, std::map<std::string, std::string> > map_str_map_str_str;

    /// Dump properties to screen
    void dumpProperties()  const;
    
  public:
    PropertyTest(const std::string& name, Context& ctxt);

    /// Standard destructor
    virtual ~PropertyTest() = default;
    /// Start the component
    virtual int initialize()  override;
  };
}   // End namespace Online

// Include files from the framework
#include <Dataflow/Plugins.h>
#include <Dataflow/Printout.h>

DECLARE_DATAFLOW_NAMED_COMPONENT_NS(Online,Dataflow_PropertyTest,PropertyTest)

#define DATAFLOW_TEST_OS_OPERS
#include "printers.h"

Online::PropertyTest::PropertyTest(const std::string& nam, Context& ctxt)
  : DataflowComponent(nam, ctxt)
{
  declareProperty("use", used_properties);

  declareProperty("prop_str",       prop_str); 
  declareProperty("prop_bool",      prop_bool);
  declareProperty("prop_int",       prop_int);
  declareProperty("prop_uint",      prop_uint);
  declareProperty("prop_long",      prop_long);
  declareProperty("prop_ulong",     prop_ulong);
  declareProperty("prop_float",     prop_float);           
  declareProperty("prop_double",    prop_double);          

  declareProperty("map_str_str",    map_str_str);
  declareProperty("map_str_bool",   map_str_bool);
  declareProperty("map_str_int",    map_str_int);
  declareProperty("map_str_uint",   map_str_uint);
  declareProperty("map_str_long",   map_str_long);
  declareProperty("map_str_ulong",  map_str_ulong);
  declareProperty("map_str_float",  map_str_float);
  declareProperty("map_str_double", map_str_double);
  declareProperty("map_str_vec_str", map_str_vec_str);
  declareProperty("map_str_map_str_str", map_str_map_str_str);
  
  declareProperty("map_int_str",    map_int_str);
  declareProperty("map_int_bool",   map_int_bool);
  declareProperty("map_int_int",    map_int_int);
  declareProperty("map_int_uint",   map_int_uint);
  declareProperty("map_int_long",   map_int_long);
  declareProperty("map_int_ulong",  map_int_ulong);
  declareProperty("map_int_float",  map_int_float);
  declareProperty("map_int_double", map_int_double);
  
  declareProperty("set_str",        set_str);
  declareProperty("set_bool",       set_bool);
  declareProperty("set_int",        set_int);
  declareProperty("set_uint",       set_uint);
  declareProperty("set_long",       set_long);
  declareProperty("set_ulong",      set_ulong);
  declareProperty("set_float",      set_float);
  declareProperty("set_double",     set_double);
  
  declareProperty("vector_str",     vector_str);
  declareProperty("vector_bool",    vector_bool);
  declareProperty("vector_int",     vector_int);
  declareProperty("vector_uint",    vector_uint);
  declareProperty("vector_long",    vector_long);
  declareProperty("vector_ulong",   vector_ulong);
  declareProperty("vector_float",   vector_float);
  declareProperty("vector_double",  vector_double);
  
  declareProperty("list_str",      list_str);
  declareProperty("list_bool",     list_bool);
  declareProperty("list_int",      list_int);
  declareProperty("list_uint",     list_uint);
  declareProperty("list_long",     list_long);
  declareProperty("list_ulong",    list_ulong);
  declareProperty("list_float",    list_float);
  declareProperty("list_double",   list_double);
}

/// Dump properties to screen
void Online::PropertyTest::dumpProperties()  const    {
  int result = 0;
  const char* line = "+----------------------------------------------------------------------------------------------------------";
  std::cout << line << std::endl;

  if ( used_properties.find("PRIM") != std::string::npos )   {
    result += dump_item("prop_str",      prop_str); 
    result += dump_item("prop_bool",     prop_bool);
    result += dump_item("prop_int",      prop_int);
    result += dump_item("prop_uint",     prop_uint);
    result += dump_item("prop_long",     prop_long);
    result += dump_item("prop_ulong",    prop_ulong);
    result += dump_item("prop_float",    prop_float);           
    result += dump_item("prop_double",   prop_double);          
    std::cout << line << std::endl;
  }
  
  if ( used_properties.find("MAP") != std::string::npos )   {
    result += dump_map("map_str_str",    map_str_str);
    result += dump_map("map_str_bool",   map_str_bool);
    result += dump_map("map_str_int",    map_str_int);
    result += dump_map("map_str_uint",   map_str_uint);
    result += dump_map("map_str_long",   map_str_long);
    result += dump_map("map_str_ulong",  map_str_ulong);
    result += dump_map("map_str_float",  map_str_float);
    result += dump_map("map_str_double", map_str_double);
    result += dump_map("map_str_vec_str", map_str_vec_str);
    result += dump_map("map_str_map_str_str", map_str_map_str_str);
    std::cout << line << std::endl;

    result += dump_map("map_int_str",    map_int_str);
    result += dump_map("map_int_bool",   map_int_bool);
    result += dump_map("map_int_int",    map_int_int);
    result += dump_map("map_int_uint",   map_int_uint);
    result += dump_map("map_int_long",   map_int_long);
    result += dump_map("map_int_ulong",  map_int_ulong);
    result += dump_map("map_int_float",  map_int_float);
    result += dump_map("map_int_double", map_int_double);
    std::cout << line << std::endl;
  }
  
  if ( used_properties.find("SET") != std::string::npos )   {
    result += dump_cont("set_str",       set_str);
    result += dump_cont("set_bool",      set_bool);
    result += dump_cont("set_int",       set_int);
    result += dump_cont("set_uint",      set_uint);
    result += dump_cont("set_long",      set_long);
    result += dump_cont("set_ulong",     set_ulong);
    result += dump_cont("set_float",     set_float);
    result += dump_cont("set_double",    set_double);
    std::cout << line << std::endl;
  }
  
  if ( used_properties.find("VECTOR") != std::string::npos )   {
    result += dump_cont("vector_str",    vector_str);
    result += dump_cont("vector_bool",   vector_bool);
    result += dump_cont("vector_int",    vector_int);
    result += dump_cont("vector_uint",   vector_uint);
    result += dump_cont("vector_long",   vector_long);
    result += dump_cont("vector_ulong",  vector_ulong);
    result += dump_cont("vector_float",  vector_float);
    result += dump_cont("vector_double", vector_double);
    std::cout << line << std::endl;
  }
 
  if ( used_properties.find("LIST") != std::string::npos )   {
    result += dump_cont("list_str",      list_str);
    result += dump_cont("list_bool",     list_bool);
    result += dump_cont("list_int",      list_int);
    result += dump_cont("list_uint",     list_uint);
    result += dump_cont("list_long",     list_long);
    result += dump_cont("list_ulong",    list_ulong);
    result += dump_cont("list_float",    list_float);
    result += dump_cont("list_double",   list_double);
    std::cout << line << std::endl;
  }
  if ( 0 == result )
    std::cout << "| Test PASSED" << std::endl;
  else
    std::cout << "| Test FAILED" << std::endl
	      << "| ===> " << result << " Subtests FAILED" << std::endl;
  std::cout << line << std::endl;
}

/// Initialize the component
int Online::PropertyTest::initialize()    {
  int sc = DataflowComponent::initialize();
  if ( sc == DF_SUCCESS )  {
    used_properties = RTL::str_upper(used_properties);
    this->dumpProperties();
  }
  return sc;
}

