//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : Online/GaudiOnlineTests
//
//  Author     : Markus Frank
//==========================================================================

/// C/C++ include files
#include <map>
#include <set>
#include <list>
#include <vector>
#include <iostream>

/// Include files from the framework
#include <RTL/strdef.h>

namespace {
#if defined(DATAFLOW_TEST_OS_OPERS)
  std::ostream& operator<<(std::ostream& os, const std::vector<std::string>& value) {
    std::stringstream log;
    log << "[";
    for(const auto& p : value)
      log << p << " ";
    log << "] ";
    return os << log.str();
  }
  std::ostream& operator<<(std::ostream& os, const std::map<std::string,std::string>& value) {
    std::stringstream log;
    log << "{";
    for(const auto& p : value)
      log << std::left << p.first << ": " << std::left << p.second << " ";
    log << "} ";
    return os << log.str();
  }
#endif
  void _print(std::stringstream& log)   {
    auto res = RTL::str_replace(log.str(), "( ", "(");
    std::cout << res << std::endl;
  }
  /// Dump properties to screen
  template <typename T> int dump_item(const std::string& tag, const T& value)    {
    try  {
      std::stringstream log;
      log << "| " << std::setw(32) << std::left << tag << " " << std::setw(14) << std::left << value;
      _print(log);
    }
    catch(const std::exception& e)   {
      std::cout << "Test FAILED: " << tag << " Exception: " << e.what() << std::endl;
      return 1;
    }
    return 0;
  }
  template <typename T> int dump_cont(const std::string& tag, const T& value)    {
    try  {
      std::stringstream log;
      log << "| " << std::setw(32) << std::left << tag << " ";
      for(const auto& val : value)
        log << std::setw(6) << std::left << val << "  ";
      _print(log);
    }
    catch(const std::exception& e)   {
      std::cout << "Test FAILED: " << tag << " Exception: " << e.what() << std::endl;
      return 1;
    }
    return 0;
  }
  template <typename T> int dump_map(const std::string& tag, const T& value)    {
    try  {
      std::stringstream log;
      log << "| " << std::setw(32) << std::left << tag << " ";
      for(const auto& p : value)
        log << std::setw(6) << std::left << p.first << " = " << std::setw(10) << std::left << p.second << "  ";
      _print(log);
    }
    catch(const std::exception& e)   {
      std::cout << "Test FAILED: " << tag << " Exception: " << e.what() << std::endl;
      return 1;
    }
    return 0;
  }
}
