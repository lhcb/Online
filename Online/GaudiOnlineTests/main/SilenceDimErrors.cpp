//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
#include <dim/dic.hxx>
#include <cstdio>

namespace   {

  class Errh : public DimErrorHandler   {
  public:
    bool m_flag;
    void errorHandler(int severity, int code, char *msg) override  {
      if (m_flag)  {
	printf("DIM: Code %d.%x %s\n", severity, code, msg);
      }
    }
    Errh (bool flg) : DimErrorHandler()  {
      m_flag = flg;
    }
    ~Errh()   {
    }
  };
  __attribute__((constructor)) void s___initialize_error_handler()    {
    DimClient::addErrorHandler(new Errh(0));
    ::printf("Switching off dim error halder messages.\n");
  }
}
