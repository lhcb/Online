#==========================================================================
#  LHCb Online software suite
#--------------------------------------------------------------------------
# Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
# All rights reserved.
#
# For the licensing terms see OnlineSys/LICENSE.
#
# Author     : M.Frank
#
#==========================================================================
#
# Gaudi online tests
#
#==========================================================================
__version__ = "1.0"
__author__  = "Markus Frank <Markus.Frank@cern.ch>"

import os, sys
import GaudiOnlineTests.Environ as _test_env
from dataflow import *
import dataflow.task

from dataflow.task import (
  get_online_env,
  set_online_env,
  mbm_flags,
  mbm_default_requirement)

from dataflow.task import (
  basic_task,
  MDFGen,
  TAEGen,
  DatafileReader)

# -------------------------------------------------------------------------------------
def df_set_online_env():
  import OnlineEnvBase as env
  return set_online_env(env)

# -------------------------------------------------------------------------------------
def get_free_port():
  import socket
  port = -1
  try:
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(('localhost',0))
    addr, port = s.getsockname()
    s.close()
    if port < 8080:
      return get_free_port()
  except:
    pass
  return port

#--------------------------------------------------------------------------
def _run_task(task, execute):
  if execute:
    return task.run()
  return task

#--------------------------------------------------------------------------
#
#  Process setup class for dataflow tests
#
#
#--------------------------------------------------------------------------
class dataflow_task(dataflow.task.basic_task):

  #------------------------------------------------------------------------
  def __init__(self, clazz, output_level=1):
    basic_task.__init__(self, clazz=clazz, output_level=output_level)

  #------------------------------------------------------------------------
  def get_test_tag_number(self):
    return _test_env.get_test_tag_number()

  #------------------------------------------------------------------------
  def get_test_port_number(self,offset=0x2000):
    port = (0x7FFF&self.get_test_tag_number()) + offset
    while port > 60000:
      port -= 1000;
    while port < 8000:
      port += 200
    return port

  #------------------------------------------------------------------------
  def setup_cpp_exe_wrapper_component(self, name='Wrapper', start='initialize', stop='finalize', prt=False, ensure=0):
    exec_wrapper    = self.create_component(type='Dataflow_ExecutableWrapper', name=name)
    exec_wrapper.Start = start
    exec_wrapper.Stop  = stop
    exec_wrapper.PrintCommand = prt
    if ensure > 0:
      exec_wrapper.Ensure = ensure
    self.setup.append(exec_wrapper)
    return exec_wrapper

  #------------------------------------------------------------------------
  def setup_cpp_exe_wrapper_algorithm(self, name='Wrapper', start='initialize', stop='finalize', prt=False, ensure=0):
    exec_wrapper = self.create_component(type='Dataflow_ExecutableWrapper', name=name)
    exec_wrapper.Start = start
    exec_wrapper.Stop  = stop
    exec_wrapper.PrintCommand = prt
    if ensure > 0:
      exec_wrapper.Ensure = ensure
    self.algorithms.append(exec_wrapper)
    return exec_wrapper

  #------------------------------------------------------------------------
  def setup_cpp_exe_wrapper_task(self, name='Wrapper', start='initialize', stop='finalize', prt=False, ensure=0):
    exec_wrapper = self.setup_cpp_exe_wrapper_component(name=name, start=start, stop=stop, prt=prt, ensure=ensure)
    return exec_wrapper

  #------------------------------------------------------------------------
  def setup_cpp_lib_wrapper_component(self, name='Wrapper', start='initialize', stop='finalize', prt=False):
    exec_wrapper    = self.create_component(type='Dataflow_LibraryCallWrapper', name=name)
    exec_wrapper.Start = start
    exec_wrapper.Stop  = stop
    exec_wrapper.PrintCommand = prt
    self.setup.append(exec_wrapper)
    return exec_wrapper

  #------------------------------------------------------------------------
  def setup_cpp_lib_wrapper_algorithm(self, name='Wrapper', start='initialize', stop='finalize', prt=False):
    exec_wrapper = self.create_component(type='Dataflow_LibraryCallWrapper', name=name)
    exec_wrapper.Start = start
    exec_wrapper.Stop  = stop
    exec_wrapper.PrintCommand = prt
    self.algorithms.append(exec_wrapper)
    return exec_wrapper

  #------------------------------------------------------------------------
  def setup_cpp_lib_wrapper_task(self, name='Wrapper', start='initialize', stop='finalize', prt=False):
    exec_wrapper = self.setup_cpp_lib_wrapper_component(name=name, start=start, stop=stop, prt=prt)
    return exec_wrapper

  #------------------------------------------------------------------------
  def setup_hlt1_storage(self, files=None, database=None, print_level=None):
    import socket
    env     = get_online_env()
    port    = self.get_test_port_number()
    test    = '0x%04X'%(port,)
    db_url  = '127.0.0.1:%d'%(port, )
    fs_url  = '127.0.0.1:%d'%(port+8000, )

    if not database:
      database = 'fdb_0x%08X.db'%(port,)
    dbase = '-database='+database

    printout = '-print='+str(env.OutputLevel)
    if print_level:
      printout = '-print=%d'%(print_level, )

    filebuff = '-files=.'
    if files:
      filebuff = '-files='+str(files)

    os.environ['DIM_DNS_NODE'] = socket.gethostname()

    processes = {}
    name = 'FDB_DB_STOP_'+test
    proc = self.setup_cpp_exe_wrapper_task(name='DB_STOP', start='stop', stop='finalize', prt=True)
    proc.Process         = name
    proc.Executable      = 'wget'
    proc.Sleep_Prestart  = 1
    proc.Sleep_Stop      = 2
    proc.Arguments       = ['http://'+db_url+'/control=exit', '--output-document='+name+'.data', '--output-file='+name+'.log']
    processes['DB_STOP'] = proc

    name = 'FDB_FS_STOP_'+test
    proc = self.setup_cpp_exe_wrapper_task(name='FS_STOP', start='stop', stop='finalize', prt=True)
    proc.Process         = name
    proc.Executable      = 'wget'
    proc.Sleep_Prestart  = 2
    proc.Sleep_Stop      = 2
    proc.Arguments       = ['http://'+fs_url+'/control=exit', '--output-document='+name+'.data', '--output-file='+name+'.log']
    processes['FS_STOP'] = proc

    proc = self.setup_cpp_exe_wrapper_task(name='DB_SERV', start='initialize', stop='finalize', prt=True, ensure=2)
    proc.Process         = 'FDB_DB_SERVER_'+test
    proc.Executable      = 'gentest'
    proc.LogFile         = 'DB_SERVER.log'
    proc.Sleep_Start     = 8
    proc.Arguments       = ['libStorageServer.so', 'fdb_sqlite_server',
                         '-threads=1', printout, '-local='+db_url, '-server='+fs_url, dbase]
    processes['DB_SERV'] = proc

    proc = self.setup_cpp_exe_wrapper_task(name='FS_SERV', start='initialize', stop='finalize', prt=True, ensure=2)
    proc.Process         = 'FDB_FS_SERVER_'+test
    proc.Executable      = 'gentest'
    proc.Sleep_Start     = 4
    proc.LogFile         = 'FS_SERVER.log'
    proc.Arguments       = ['libStorageServer.so', 'fdb_fs_file_server',
                         '-threads=1', '-buffersize=16777216', printout, '-local='+fs_url, '-server='+db_url, filebuff]
    processes['FS_SERV'] = proc

    name = 'FDB_DB_LIST_'+test
    proc = self.setup_cpp_exe_wrapper_task(name='DB_LIST1', start='finalize', stop='destruction', prt=True)
    proc.Process         = name
    proc.Executable      = 'gentest'
    proc.Sleep_Start     = 5
    proc.Arguments       = ['libStorageServer.so', 'fdb_cli_dumpdb', dbase]
    processes['DB_LIST'] = proc
    return processes

#--------------------------------------------------------------------------
def df_check_numa():
  env  = get_online_env()
  task = dataflow_task('Class1', output_level=env.OutputLevel)
  task.setup_basics(auto_startup=True, auto_finalize=True, ui=None, monitoring=None, exit=True).setup_numa()
  return task.run()

#--------------------------------------------------------------------------
def df_check_output_levels(output_level):
  env  = get_online_env()
  task = dataflow_task('Class1', output_level=output_level)
  task.setup_basics(auto_startup=True, auto_finalize=True, ui=None, monitoring=None, exit=True)
  print('INFO:   +++ OutputLevelTest: level = %d'%(output_level, ))
  levels = { 'VERBOSE':  OutputLevel.VERBOSE,
             'DEBUG':    OutputLevel.DEBUG,
             'INFO':     OutputLevel.INFO,
             'WARNING':  OutputLevel.WARNING,
             'ERROR':    OutputLevel.ERROR,
             'FATAL':    OutputLevel.FATAL,
             'ALWAYS':   OutputLevel.ALWAYS }
  items = levels.items()
  for level_name, level in items:
    test = task.create_component(type='Dataflow_OutputLevelTest', name='OutputTest_'+level_name, output_level=level)
    task.services.append(test)
  del(levels)
  del(items)
  return task.run()

#--------------------------------------------------------------------------
def MBM(auto_startup=False, auto_finalize=False):
  env   = df_set_online_env()
  task  = dataflow_task('Class0', output_level=env.OutputLevel)
  flags = mbm_flags('Events') + ' ' + mbm_flags('Output')
  task.setup_basics(auto_startup=auto_startup, auto_finalize=auto_finalize, exit=False)
  task.setup_delay_service(name='Delay', when={'Finalize': 5, 'Stop': 15})
  task.setup_mbm_server_component(flags=flags) \
      .setup_mbm_summary(regex='.(.*)_'+env.PartitionName, when=['(start)'])
  return task.run()

#--------------------------------------------------------------------------
def Reader(directory, prefix='*', rescan=0, delay=0.0, wait=10, pause=15, checksum=None, execute=True):
  env = df_set_online_env()
  print('INFO:   +++ Reader directory: %s file prefix: \'%s\' Wait: start: %d sec pause: %d sec'%(str(directory), prefix, wait, pause, ))
  task = dataflow_task('Class2', output_level=env.OutputLevel)
  task.setup_basics()
  task.HavePause = True
  buffer = 'Events'
  client = task.setup_mbm_client_component(name='MBM', buffers=[buffer], connect='initialize')
  sleep  = task.setup_delay_service(name='Delay', when={'Start': wait, 'Cancel': 5})
  reader = task.setup_file_reader_component(name='Reader', buffer=buffer, delay=delay)
  if isinstance(directory, str):
    if (directory[0] == '[') or (directory[0] == '('):
      reader.Directories = [d for d in eval(directory)]
    else:
      reader.Directories = [directory]
  elif isinstance(directory, list):
    reader.Directories = directory
  elif isinstance(directory, tuple):
    reader.Directories = [d for d in directory]
  reader.FilePrefix  = prefix
  reader.PauseSleep  = pause
  reader.Rescan      = rescan
  if checksum and isinstance(checksum, str):
    checksum = checksum.upper()
    if checksum == 'ADLER32':
      reader.ChecksumType = CHECKSUM_ADLER32
    elif checksum == 'CRC32':
      reader.ChecksumType = CHECKSUM_CRC32
    elif checksum == 'CRC16':
      reader.ChecksumType = CHECKSUM_CRC16
    elif checksum == 'CRC82':
      reader.ChecksumType = CHECKSUM_CRC8
    elif checksum == 'HASH32':
      reader.ChecksumType = CHECKSUM_HASH32
    elif checksum == 'XOR':
      reader.ChecksumType = CHECKSUM_XOR
    reader.PrintChecksum = True
  else:
    reader.ChecksumType = CHECKSUM_NONE
  task.services.append(reader)
  task.setup_eventloop_wrapper(reader)
  task.setup_mbm_summary_algorithm(regex='(.*)_'+env.PartitionName, when=['(pause)'])
  return task.run()

MDFReader=Reader
TAEReader=Reader

#--------------------------------------------------------------------------
def StorageFileReader(directory, prefix='*', rescan=0, delay=0.0, wait=10, pause=40, checksum=None, execute=True):
  env = df_set_online_env()
  print('INFO:   +++ Reader directory: %s file prefix: \'%s\' Wait: start: %d sec pause: %d sec'%(str(directory), prefix, wait, pause, ))
  task = dataflow_task('Class2', output_level=env.OutputLevel)
  task.setup_basics()
  task.HavePause = True
  buffer = 'Events'
  client = task.setup_mbm_client_component(name='MBM', buffers=[buffer], connect='initialize')
  sleep  = task.setup_delay_service(name='Delay', when={ 'Start': wait, 'Cancel': 5 })
  sleep.AutoPause = pause
  reader = task.setup_hlt2_reader_component(name='Reader', buffer=buffer, delay=delay)
  reader.Directories = directory
  reader.FilePrefix  = prefix
  reader.PauseSleep  = 15
  reader.Rescan      = rescan
  if checksum and isinstance(checksum, str):
    checksum = checksum.upper()
    if checksum == 'ADLER32':
      reader.ChecksumType = CHECKSUM_ADLER32
    elif checksum == 'CRC32':
      reader.ChecksumType = CHECKSUM_CRC32
    elif checksum == 'CRC16':
      reader.ChecksumType = CHECKSUM_CRC16
    elif checksum == 'CRC82':
      reader.ChecksumType = CHECKSUM_CRC8
    elif checksum == 'HASH32':
      reader.ChecksumType = CHECKSUM_HASH32
    elif checksum == 'XOR':
      reader.ChecksumType = CHECKSUM_XOR
    reader.PrintChecksum = True
  else:
    reader.ChecksumType = CHECKSUM_NONE
  task.services.append(reader)
  task.setup_eventloop_wrapper(reader)
  task.setup_mbm_summary_algorithm(regex='(.*)_'+env.PartitionName, when=['(pause)'])
  return task.run()

#--------------------------------------------------------------------------
def ReaderEx(directory, prefix='*', rescan=0, delay=0.0, wait=20, checksum=None):
  env = df_set_online_env()
  print('INFO:   +++ Reader directory: %s file prefix: \'%s\''%(str(directory), prefix, ))
  task = dataflow_task('Class2', output_level=env.OutputLevel)
  task.setup_basics()
  task.HavePause = True
  buffer = 'Events'
  client = task.setup_mbm_client_component(name='MBM', buffers=[buffer], connect='initialize')
  sleep  = task.setup_delay_service(name='Delay', when={'Start': wait, 'Cancel': 5})
  reader = task.setup_file_reader_component(name='Reader', buffer=buffer, delay=delay)
  reader.Directories = directory
  reader.FilePrefix  = prefix
  reader.PauseSleep  = 15
  reader.Rescan      = rescan
  if checksum and isinstance(checksum, str):
    checksum = checksum.upper()
    if checksum == 'ADLER32':
      reader.ChecksumType = CHECKSUM_ADLER32
    elif checksum == 'CRC32':
      reader.ChecksumType = CHECKSUM_CRC32
    elif checksum == 'CRC16':
      reader.ChecksumType = CHECKSUM_CRC16
    elif checksum == 'CRC82':
      reader.ChecksumType = CHECKSUM_CRC8
    elif checksum == 'HASH32':
      reader.ChecksumType = CHECKSUM_HASH32
    elif checksum == 'XOR':
      reader.ChecksumType = CHECKSUM_XOR
    reader.PrintChecksum = True
  else:
    reader.ChecksumType = CHECKSUM_NONE
  task.services.append(reader)
  task.setup_eventloop_wrapper(reader)
  task.setup_mbm_summary_algorithm(regex='(.*)_'+env.PartitionName, when=['(pause)'])
  return task.run()

#--------------------------------------------------------------------------
def Writer(output, max_events=100000, buffer='Events', compression_type=None, compression=None, file_compression=None, checksum=None, idle_timeout=None, execute=True):
  env = df_set_online_env()
  print('INFO:   +++ Writer output specs: %s [max. %d events per file]'%(output, max_events, ))
  task = dataflow_task('Class1', output_level=OutputLevel.WARNING)
  task.setup_basics()
  cl, sel, wr = task.setup_file_writer_task(name='Writer', buffer=buffer, output=output)
  wr.MaxEvents = max_events
  if idle_timeout:
    wr.IdleTimeout = int(idle_timeout)
    print('INFO:   +++ Writer timeout set to %d seconds'%(int(idle_timeout), ))
  if compression:
    wr.CompressionType  = compression[0]
    wr.CompressionLevel = compression[1]
  if file_compression:
    wr.FileCompression  = 1
  if checksum and isinstance(checksum, str):
    checksum = checksum.upper()
    if checksum == 'ADLER32':
      wr.ChecksumType = CHECKSUM_ADLER32
    elif checksum == 'CRC32':
      wr.ChecksumType = CHECKSUM_CRC32
    elif checksum == 'CRC16':
      wr.ChecksumType = CHECKSUM_CRC16
    elif checksum == 'CRC82':
      wr.ChecksumType = CHECKSUM_CRC8
    elif checksum == 'HASH32':
      wr.ChecksumType = CHECKSUM_HASH32
    elif checksum == 'XOR':
      wr.ChecksumType = CHECKSUM_XOR
    wr.PrintChecksum = True
  else:
    wr.ChecksumType = CHECKSUM_NONE
  return _run_task(task=task, execute=execute)

MDFWriter = Writer
TAEWriter = Writer

#--------------------------------------------------------------------------
def Passthrough():
  env = df_set_online_env()
  task = dataflow_task('Class1', output_level=env.OutputLevel)
  task.setup_basics()
  task.setup_mbm_summary_algorithm(regex='.(.*)_'+env.PartitionName, when=['(cancel)'])
  task.setup_mbm_writer_task(name='Passthrough', inbuffer='Events', outbuffer='Output')
  return task.run()

#--------------------------------------------------------------------------
def TanWrapper():
  env = df_set_online_env()
  task = dataflow_task('Class0', output_level=env.OutputLevel)
  task.setup_basics()
  pnum = '%d'%(task.get_test_port_number(offset=0x1000),)
  phex = 'ID%04X'%(task.get_test_port_number(offset=0x1000),)
  gbl  = 'TAN_PUBAREA_'+phex
  print('INFO:   +++ TanWrapper: gbl: %s port:%s '%(gbl, pnum,))
  tan_serv = task.setup_cpp_exe_wrapper_task(name='TanWrapper', start='initialize', stop='destruction', prt=False, ensure=3)
  offset   = '%d'%((task.get_test_tag_number()&0xF) * 1000, );
  tan_serv.Executable = 'online_test'
  tan_serv.Arguments  = ['tan_nameserver', '-tcp', '-a', '-q', pnum, '-p', gbl, "-o", offset, '-x']
  tan_serv.Process    = 'tan_service_'+phex
  tan_serv.LogFile    = 'TanServ.log'
  tan_serv.KillOnly   = True
  tan_stop = task.setup_cpp_exe_wrapper_component(name='TanShutdown', start='finalize', stop='destruction', prt=True)
  tan_stop.Executable = 'gentest'
  tan_stop.Arguments  = ['libOnlineBase.so', 'tan_nameserver_shutdown', '-quiet', '-nowait', '-port='+pnum, '-node=localhost']
  tan_stop.Process    = 'tan_shutdown_'+phex
  tan_stop.LogFile    = 'TanStop.log'
  return task.run()

#--------------------------------------------------------------------------
def DefaultTask(seconds=10, output_level=None, execute=True):
  env = df_set_online_env()
  if output_level:
    env.OutputLevel = output_level
  task = dataflow_task('Class1', output_level=env.OutputLevel)
  task.setup_basics()
  task.manager.OutputLevel = MSG_DEBUG
  if seconds:
    if seconds > 0:
      delay = task.setup_delay_algorithm(name='AutoPause', when={'Start': 5})
      delay.AutoPause = seconds
  return _run_task(task=task, execute=execute)

Task = DefaultTask
AutoPause = DefaultTask
CounterExtract = DefaultTask
HistogramExtract = DefaultTask

#--------------------------------------------------------------------------
def DelayTask(output_level=MSG_DEBUG, execute=True):
  global online_environment
  task = dataflow_task('Class0', output_level=output_level)
  task.setup_basics(auto_startup=False, auto_finalize=False, monitoring=False)
  # optional wait to attach debuggers/wait for slow startup
  delays = {
    'Initialize':   1.0,
    'Start':        1.1,
    'Enable':       1.2,
    'Stop':         1.3,
    'Finalize':     1.4,
    'Terminate':    1.5,
    'Pause':        1.6,
    'Continue':     1.7,
    'Cancel':       1.8,
    'AutoPause':    2.0,
    'Event':        2.0,
  }
  for k,v in delays.items():
    delay = task.setup_delay_service(name='Delay_'+k, when={k: v})
    delay.OutputLevel = output_level
  return _run_task(task=task, execute=execute)

#--------------------------------------------------------------------------
def Sender(target, buffer='Events', type='Dataflow_AsioSender', execute=True):
  env  = df_set_online_env()
  task = dataflow_task('Class2', output_level=env.OutputLevel)
  task.setup_basics()
  task.setup_net_sender_task(target=target, name='Sender', type=type, buffer=buffer)
  task.setup_mbm_summary_algorithm(regex=buffer+'_'+env.PartitionName, when=['(stop)'])
  return _run_task(task=task, execute=execute)

#--------------------------------------------------------------------------
def Receiver(buffer='Events', threads=3, type='Dataflow_AsioReceiver', execute=True):
  env  = df_set_online_env()
  task = dataflow_task('Class1', output_level=env.OutputLevel)
  task.setup_basics()
  task.setup_net_receiver_task(name='Receiver', type=type, buffer=buffer, threads=threads)
  return _run_task(task=task, execute=execute)

#--------------------------------------------------------------------------
def Receiver(buffer='Events', threads=3, type='Dataflow_AsioReceiver', execute=True):
  env  = df_set_online_env()
  task = dataflow_task('Class1', output_level=env.OutputLevel)
  task.setup_basics()
  task.setup_net_receiver_task(name='Receiver', type=type, buffer=buffer, threads=threads)
  return _run_task(task=task, execute=execute)

#--------------------------------------------------------------------------
def EventServer(buffer='Events', type='Dataflow_AsioEventRequestServer', execute=True):
  env  = df_set_online_env()
  task = dataflow_task('Class1', output_level=env.OutputLevel)
  task.setup_basics()
  cl, sel, wrap = task.setup_net_eventserver_task(name='Client', type=type, buffer=buffer)
  sel.REQ = mbm_default_requirement(event_type=2, user_type='USER')
  return _run_task(task=task, execute=execute)

#--------------------------------------------------------------------------
def EventClient(target, buffer='Output', max_events=-1, type='Dataflow_AsioSelector', execute=True):
  env  = df_set_online_env()
  task = dataflow_task('Class2', output_level=env.OutputLevel)
  task.setup_basics()
  cl, sel, wrap = task.setup_net_eventclient_task(name='EventProc', type=type, buffer=buffer, target=target)
  sel.MaxEvents = max_events
  sel.RearmRetries = 5
  sel.RearmSleep   = 1000
  task.setup_mbm_summary(regex=buffer+'_'+env.PartitionName, when=['(pause)'], delay=2)
  return _run_task(task=task, execute=execute)

#--------------------------------------------------------------------------
def HLT1Storage(files=None, database=None, execute=True):
  env   = df_set_online_env()
  level = str(env.OutputLevel)
  task  = dataflow_task('Class0', output_level=env.OutputLevel)
  task.setup_basics()
  task.setup_hlt1_storage(database=database, files=files, print_level=None)
  # optional wait to attach debuggers/wait for slow startup
  task.setup_delay_algorithm(name='StorageDelay', when={'Start': 15})
  return _run_task(task=task, execute=execute)

#--------------------------------------------------------------------------
def HLT1Reader(server=None, name='Reader', buffer='Events', runs=None, execute=True):
  env  = df_set_online_env()
  task = dataflow_task('Class2', output_level=OutputLevel.WARNING)
  task.setup_basics()
  task.HavePause = True
  client  = task.setup_mbm_client_component(name='MBM', buffers=[buffer], connect='initialize')

  if not server:
    port   = task.get_test_port_number()
    server = '127.0.0.1:%d'%(port, )
  run_list = []
  if runs:
    for r in runs:
      tag = '/objects/data/ID%08X/Full/%010d'%(task.get_test_tag_number(), r,)
      run_list.append(tag)
  print('INFO:   +++ HLT1 Reader specs: PartitionID: %s http://%s [tag: 0x%04X buffer: %s, runs: %s]'%\
        (env.PartitionName,
         server,
         task.get_test_tag_number(),
         buffer,
         str(run_list), ))
  reader = task.setup_hlt1_reader_component(name=name, server=server, buffer=buffer, runs=run_list)
  task.services.append(reader)
  task.setup_eventloop_wrapper(reader)
  task.setup_mbm_summary_algorithm(regex='(.*)_'+env.PartitionName, when=['(stop)'])
  task.setup_delay_algorithm(name='Delay', when={'Start': 5, 
                                                 'Stop':  5,
                                                 'StopTrigger': 5})
  return _run_task(task=task, execute=execute)

#--------------------------------------------------------------------------
def HLT1Writer(server=None, max_events=100000, buffer='Events', execute=True):
  env  = df_set_online_env()
  task = dataflow_task('Class1', output_level=env.OutputLevel)
  task.setup_basics()
  if not server:
    port   = task.get_test_port_number()
    server = '127.0.0.1:%d'%(port,)
  print('INFO:   +++ HLT1 Writer specs: http://%s [max. %d events per file]'%(server, max_events, ))
  cl, sel, wr = task.setup_hlt1_writer_task(name='Writer', buffer=buffer, server=server)
  wr.MaxEvents = max_events
  return _run_task(task=task, execute=execute)

#--------------------------------------------------------------------------
def AlignDataGen(buffer='Events', run=12345, bursts=10000, packing=1, stream='ALIGN'):
  # -----------------------------------------------------------------------
  env  = df_set_online_env()
  print('INFO:   +++ Generating run: %d  bursts: %d packing: %d half_window: %s'%(run, bursts, 0, 1))
  task = dataflow_task('Class2', output_level=env.OutputLevel)
  task.setup_basics()
  #
  client = task.setup_mbm_client_component(name='MBM', buffers=[buffer], connect='initialize')
  gene   = task.setup_event_generator_component(name='Generator', half_window=0, packing=1)
  gene.RunNumber = run
  gene.MaxBursts = bursts
  #
  steer  = task.create_component(name="Steering", type="Dataflow_AlignWriterTest")
  steer.DNS = os.getenv('DIM_DNS_NODE', 'localhost')
  steer.Stream = stream
  steer.WaitPreStep  = 5
  steer.WaitPostStep = 10
  steer.OutputLevel  = OutputLevel.DEBUG
  steer.Partition    = env.PartitionName
  steer.Steps = [('align-1', (10, 1)), ('wait', (10, 0)), \
                 ('align-2', (10, 1)), ('wait', (10, 0)), \
                 ('align-3', (10, 1)), ('wait', (10, 0))]
  task.algorithms.append(steer)
  #
  writer = task.setup_mbm_writer_component(name='Writer', buffer=buffer)
  task.HavePause = True
  task.setup_delay_algorithm(name='Delay', when={'Start':10, 'Pause': 15})
  task.setup_mbm_summary_algorithm(regex=buffer+'_'+env.PartitionName, when=['(stop)'])
  #
  return _run_task(task=task, execute=True)

#--------------------------------------------------------------------------
def AlignWriter(buffer='Events', stream='ALIGN', max_events=100, requirement=None, steering=None, execute=True):
  env  = df_set_online_env()
  task = dataflow_task('Class2', output_level=env.OutputLevel)
  task.setup_basics()
  print(f'INFO:   +++ AlignWriter[{stream}].requirement = {requirement}')
  print(f'INFO:   +++ AlignWriter[{stream}].max_events  = {max_events}')
  print(f'INFO:   +++ AlignWriter[{stream}].input       = {buffer}')
  print(f'INFO:   +++ AlignWriter[{stream}].steering    = {str(steering)}')
  task.setup_align_writer_task(name=stream, buffer=buffer, max_events=max_events, requirement=requirement, steering=steering)
  task.setup_mbm_summary_algorithm(regex=buffer+'_'+env.PartitionName, when=['(stop)'])
  return _run_task(task=task, execute=execute)

#--------------------------------------------------------------------------
def CounterDataGen(run=12345, bursts=10000, monitors='SCALAR|ATOMIC|VECTOR', execute=True, auto_startup=False, auto_finalize=False):
  # -----------------------------------------------------------------------
  env  = df_set_online_env()
  print('INFO:   +++ Generating run: %d  bursts: %d packing: %d half_window: %s'%(run, bursts, 0, 1))
  task = dataflow_task('Class2', output_level=env.OutputLevel)
  task.setup_basics()
  task.HavePause = True
  gene = task.setup_event_generator_component(name='Generator', half_window=0, packing=1)
  gene.RunNumber = run
  gene.MaxBursts = bursts
  task.setup_delay_algorithm(name='Delay', when={'Start':10, 'Pause': 15})
  # -----------------------------------------------------------------------
  test = task.create_component(type='Dataflow_MonitorTest', name='Monitor1')
  test.initial_increment = 10
  test.use = monitors
  task.algorithms.append(test)
  #
  test = task.create_component(type='Dataflow_MonitorTest', name='Monitor2')
  test.initial_increment = 1000
  test.use = monitors
  task.algorithms.append(test)
  #
  task.set_automatic(auto_startup=auto_startup, auto_finalize=auto_finalize)
  #
  return _run_task(task=task, execute=execute)

MonitorDataGen = CounterDataGen

#--------------------------------------------------------------------------
def HistSerialize(run=12345, bursts=10000, monitors='SCALAR|ATOMIC|VECTOR', bins=1000, execute=True, auto_startup=False, auto_finalize=False):
  env  = df_set_online_env()
  task = dataflow_task('Class2', output_level=env.OutputLevel)
  task.setup_basics()
  task.HavePause = True
  gene   = task.setup_event_generator_component(name='Generator', half_window=0, packing=1)
  gene.RunNumber = run
  gene.MaxBursts = bursts
  task.setup_delay_algorithm(name='Delay', when={'Start':10, 'Pause': 15})
  # -----------------------------------------------------------------------
  incr = 100
  for i in range(5):
    test = task.create_component(type='Dataflow_MonitorTest', name='Monitor'+str(i))
    test.initial_increment = incr
    test.use = monitors
    test.nbins_x = bins
    test.nbins_y = bins
    incr = incr * 2
    task.algorithms.append(test)
  task.set_automatic(auto_startup=auto_startup, auto_finalize=auto_finalize)
  return _run_task(task=task, execute=execute)

#--------------------------------------------------------------------------
def extract_gaucho_info(exe=None, dns='localhost', task='CounterTask', match='(.*)', pause=10.0, opt=[]):
  env = df_set_online_env()
  env.OutputLevel = MSG_INFO
  task_name = env.PartitionName+'_'+task
  app = dataflow_task('Class2', output_level=env.OutputLevel)
  app.setup_basics()
  args = ['-dns='+dns, '-task='+task_name]

  if opt and len(opt):
    if isinstance(opt,str): opt = [opt]
    for i in opt: args.append(i)

  if isinstance(match,str) and 'list-only' in match:
    args.append('-list-only')
  elif isinstance(match,str):
    test_name = match.replace('(','_').replace(')','_').replace('*','_').replace('.','_')
    args.append('-match='+match)

  try:
    output_dir = '../gaucho'
    os.mkdir(output_dir)
  except FileExistsError as exc:
    pass

  str_arg = str(args)[1:-1].replace(",","").replace("'","")
  str_log = exe + '.log'
  print(f'extract_gaucho_info: {exe}.exe {str_arg} > {str_log}')

  """
  counters = app.setup_cpp_exe_wrapper_algorithm(name=exe, start='pause', stop='finalize', prt=False, ensure=3)
  counters.KillOnly     = True
  counters.Executable   = exe + '.exe'
  counters.Process      = exe
  """
  counters = app.setup_cpp_lib_wrapper_algorithm(name=exe, start='pause', stop='finalize', prt=False)
  counters.Synchronous  = True
  counters.Library      = 'libGauchoBase.so'
  counters.Function     = exe

  counters.PrintCommand = True
  counters.Arguments    = args
  counters.LogFile      = str_log
  counters.Sleep_Start  = 5
  counters.OutputLevel  = MSG_DEBUG

  delay = app.setup_delay_service(name='Delay', when={'Start': 1})
  delay.AutoPause       = pause
  return app.run()

#--------------------------------------------------------------------------
def ExtractCounters(dns='localhost', task='CounterTask', match='(.*)', pause=10.0, format=None):
  if isinstance(format,str) and format == 'json':
    return extract_gaucho_info(exe='taskCountersJson', dns=dns, task=task, match=match, pause=pause, opt=['-json'])
  return extract_gaucho_info(exe='taskCounters', dns=dns, task=task, match=match, pause=pause)

#--------------------------------------------------------------------------
def ExtractHistograms(dns='localhost', task='GauchoHistoProcess', match='(.*)', pause=10.0, format=None, option=None, timeout=None):
  print(f'+++ Task: {task} dns: {dns} match: {match} format: {format} option: {option} [{option.__class__}]')
  none_var = None
  opt = option
  if timeout: opt.append('-timeout='+str(timeout))
  if isinstance(opt, none_var.__class__): opt = ['-one']
  if isinstance(format,str) and 'json' in format.lower(): opt.append('-json')
  return extract_gaucho_info(exe='taskHistosJson', dns=dns, task=task, match=match, pause=pause, opt=opt)

#--------------------------------------------------------------------------
def ExtractGaucho(dns='localhost', task='GauchoProcess', type=None, match='(.*)', pause=10.0, output=None, format=None):
  opt = ['-verbose']
  print(f'+++ Task: {task} type: {str(type)} match: {match} format: {format}')
  if type: opt.append('-'+type.lower())
  if 'histo' in task.lower(): opt.append('-one')
  if isinstance(output,str): opt.append('-output='+output)
  if isinstance(format,str) and 'bin' in format: opt.append('-binary')
  return extract_gaucho_info(exe='taskGauchoExtract', dns=dns, task=task, match=match, pause=pause, opt=opt)

GauchoExtractionProcess = ExtractGaucho

#--------------------------------------------------------------------------
def Timeout(timeouts, execute=True):
  import time
  env = df_set_online_env()
  print_level = str(env.OutputLevel)
  task = dataflow_task('Class0', output_level=env.OutputLevel)
  task.setup_basics()
  print(f'+++ Setup timeout process with values: {str(timeouts)}')
  if timeouts and timeouts.get('Load'):
    time.sleep(int(timeouts.get('Load')))
    del timeouts['Load']
  task.setup_delay_algorithm(name='StorageDelay', when=timeouts)
  return _run_task(task=task, execute=execute)

