#==========================================================================
#  LHCb Online software suite
#--------------------------------------------------------------------------
# Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
# All rights reserved.
#
# For the licensing terms see OnlineSys/LICENSE.
#
# Author     : M.Frank
#
#==========================================================================
__version__ = "1.0"
__author__  = "Markus Frank <Markus.Frank@cern.ch>"

import os, sys, time
from   Online.MessageLevels import *

s_time_start = time.time()
# -------------------------------------------------------------------------------------
def generate_identifier_32(offset=0):
  import hashlib
  if os.getenv('BUILD_ID',None):
    text = '%s---%d'%(os.getenv('BUILD_ID',None), os.getpid(), )
  else:
    text = '%f---%d'%(s_time_start, os.getpid(), )
  identifier = hashlib.blake2s(bytes(text,'utf-8'),digest_size=4).hexdigest()
  value = int(identifier,16) + int(offset)
  return value
# -------------------------------------------------------------------------------------
def generate_identifier(offset=0):
  identifier = (0x7FFF&generate_identifier_32()) + int(offset)
  while identifier < 1024:
    identifier += 0x100
  while identifier > 62000:
    identifier -= 0x1000
  return identifier

#------------------------------------------------------------------------
def get_test_tag_number():
  default = str(generate_identifier())
  value   = os.getenv("TEST_TAG", default)
  return 0xffffffff&int(value,10)

# --------------------------------------------------------------------------
def print_trace(exception):
  import traceback
  log(MSG_WARNING, '++ Exception in user code: %s', str(exception))
  log(MSG_WARNING, '++ '+('-'*132))
  formatted_lines = traceback.format_exc().splitlines()
  for l in formatted_lines:
    log(MSG_WARNING, '++ %s'%(l, ))
  log(MSG_WARNING, '++ '+('-'*132))

# --------------------------------------------------------------------------
class DimActor:
  # ------------------------------------------------------------------------
  def __init__(self, dns=None):
    import pydim
    self.utgid = utgid
    if dns:
      node = dns
      if dns.find(':'):
        node, port = dns.split(':')
        pydim.dim_set_dns_port(int(port))
      pydim.dim_set_dns_node(node)
    for name in dir(pydim):
      setattr(self, name, getattr(pydim, name))
    log(MSG_INFO, str(self))

  # ------------------------------------------------------------------------
  def setup_fifolog(self, format='%-38SOURCE %-8LEVEL', device='RTL::Logger::LogDevice'):
    import fifo_log
    self.log = fifo_log.logger_print
    self.logDeviceType   = device
    self.logDeviceFormat = format
    fifo_log.logger_set_tag(utgid)
    fifo_log.logger_start()
    return self

  # ------------------------------------------------------------------------
  def shutdown(self, server):
    log(MSG_ALWAYS, '++ Stop server: %s service: %s/EXIT port:%d'%(server, server, self.dim_get_dns_port(), ))
    return self.dic_cmnd_service(server + '/EXIT', (1,), 'I')

  # ------------------------------------------------------------------------
  def start_serving(self, name=utgid):
    return self.dis_start_serving(name)

