#==========================================================================
#  LHCb Online software suite
#--------------------------------------------------------------------------
# Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
# All rights reserved.
#
# For the licensing terms see OnlineSys/LICENSE.
#
# Author     : M.Frank
#
#==========================================================================
#
# Gaudi online tests
#
#==========================================================================
__version__ = "1.0"
__author__  = "Markus Frank <Markus.Frank@cern.ch>"

import os
import sys
from GaudiOnline.OnlineApplication import *

#--------------------------------------------------------------------------
def setup_app_basic(self):
  self.config.numEventThreads  = 1
  self.config.numStatusThreads = 0
  self.config.events_HighMark  = 0
  self.config.events_LowMark   = 0
  self.config.logDeviceType    = 'RTL::Logger::LogDevice'
  self.config.logDeviceFormat  = '%-9LEVEL%-24SOURCE'
  self.config.dropPipeline     = False
  self.config.expandTAE        = True
  self.config.autoStart        = True
  self.config.autoStop         = True
  self.config.execMode         = 2
  return self

#--------------------------------------------------------------------------
class TestApplication(Application):
  #------------------------------------------------------------------------
  def __init__(self, outputLevel, partitionName='OFFLINE', partitionID=0xFFFF, interactive=False, dump=False, full_dump=False):
    Application.__init__(self, 
                         outputLevel=outputLevel,
                         partitionName=partitionName, 
                         partitionID=partitionID,
                         classType=Class1)
    self.interactive = interactive
    self.full_dump   = full_dump
    self.dump        = dump
    setup_app_basic(self)
    if os.getenv('GO_DEBUG'):
      self.setup_debugging()

  #------------------------------------------------------------------------
  def setup_debugging(self):
    self.config.debug               = True
    self.config.execMode            = 0
    self.config.numEventThreads     = 1
    self.config.MBM_numConnections  = 1
    self.config.MBM_numEventThreads = 1
    return self
    
  #------------------------------------------------------------------------
  def setup_algorithms(self, algorithms=None):
    import Gaudi.Configuration as Gaudi
    import Configurables

    members = []
    if not hasattr(self,'input'):
      self.input = self.setup_event_input()
    members = [self.input]
    if hasattr(self,'updateAndReset'):
      members.append(self.updateAndReset)
    if isinstance(algorithms, (list, tuple)):
      for item in algorithms:
        members.append(item)
    elif algorithms:
      members.append(algorithms)

    sequence = Sequencer('Algorithms')
    sequence.Members          = members
    self.config.autoStart     = False
    self.config.autoStop      = False
    self.app.TopAlg           = [ sequence ]
    if hasattr(self,'broker'):
      self.broker.DataProducers = self.app.TopAlg
    self.enableUI()
    return self

  #------------------------------------------------------------------------
  def setup_generator(self, type, factory, buffer_size=10000000, half_window=0, packing=1):
    self.config.inputType             = type
    self.config.GENERIC_factory       = factory
    self.config.GENERIC_bufferSize    = buffer_size
    self.config.GENERIC_halfWindow    = half_window
    self.config.GENERIC_packingFactor = packing
    self.config.GENERIC_maxEventsIn   = 0
    return self

  #------------------------------------------------------------------------
  def exec_events(self, algs, num_events):
    self.setup_hive(FlowManager("EventLoop"), 1)
    self.config.GENERIC_maxEventsIn = num_events
    self.app.TopAlg                 = algs
    self.broker.DataProducers       = self.app.TopAlg
    return self

#--------------------------------------------------------------------------
def test_setup(app, algs=[], algorithm=None):
  # Add algorithms if required
  if isinstance(algorithm, (list, tuple)):
    for item in algorithm:
      algs.append(item)
  elif algorithm:
      algs.append(algorithm)
  # Add command prompt if requested
  if app.interactive:
    algs.append(command_handler())
  return app

#--------------------------------------------------------------------------
def test_generator(app, half_window=0, packing=1, num_events=10, algorithm=None):
  app.setup_generator(type='Generic', factory='Online__GenerateEventAccess', half_window=half_window, packing=packing)
  input = app.setup_event_input()
  algs  = [input]
  if app.dump:
    algs.append(store_explorer(print_freq=1.0))
    algs.append(header_dump('Headers', 'Banks/RawDataGuard'))
    if half_window == 0:
      alg = bank_dump('Dump', raw_data='Banks/RawData', dump_data=app.dump, full_dump=app.full_dump);
      alg.MaxEvents = num_events
      algs.append(alg)
    else:
      for i in range(2*half_window+1):
        alg = bank_dump('Dump', raw_data=None, bx=i-half_window, dump_data=app.dump, full_dump=app.full_dump)
        alg.MaxEvents = num_events
        algs.append(alg)
  test_setup(app, algs, algorithm)
  app.exec_events(algs, num_events=num_events)
  return app

#--------------------------------------------------------------------------
def test_create_banks(num_events, output_level=MSG_INFO, dump=True, full_dump=True, half_window=0):
  app = TestApplication(outputLevel=output_level, dump=dump, full_dump=full_dump)
  alg = delay('Delay', {'Cancel': 3, 'StopTrigger': 3})
  return test_generator(app, packing=1, half_window=half_window, num_events=num_events, algorithm=alg)

#--------------------------------------------------------------------------
def test_create_mdf(num_events, output_level=MSG_INFO, dump=True, full_dump=True, half_window=0, output='Output.mdf'):
  app  = TestApplication(outputLevel=output_level, dump=dump, full_dump=full_dump)
  app.config.GENERIC_maxEventsIn = num_events
  alg1 = app.setup_file_output(type='MDF', name_format=output)
  alg1.MaxEvents = num_events
  alg2 = delay('Delay', {'Cancel': 3, 'StopTrigger': 3})
  return test_generator(app, packing=1, half_window=half_window, num_events=num_events, algorithm=[alg1, alg2])

#--------------------------------------------------------------------------
def test_create_mdf_tae(num_events, output_level=MSG_INFO, dump=True, full_dump=False, half_window=7, output='Output.tae'):
  app = TestApplication(outputLevel=output_level, dump=dump, full_dump=full_dump)
  app.config.GENERIC_maxEventsIn = num_events
  alg = app.setup_file_output(type='MDF', name_format=output)
  alg.MaxEvents = num_events
  alg.UseRawGuard = True
  return test_generator(app, packing=1, half_window=half_window, num_events=num_events, algorithm=alg)

#--------------------------------------------------------------------------
def test_read_mdf(num_events=9999999999, output_level=MSG_INFO, dump=True, full_dump=True, summary=True, input='Input.mdf'):
  import GaudiOnline
  app = GaudiOnline.readDatafile(input, interactive=False, output_level=output_level, dump=dump, full_dump=full_dump, summary=summary)
  return setup_app_basic(app)

#--------------------------------------------------------------------------
def test_legacy_read_mdf(num_events=9999999999, output_level=MSG_INFO, dump=True, full_dump=True, input='Input.mdf'):
  import GaudiOnline
  return GaudiOnline.legacy_readDatafile(input=input)

#--------------------------------------------------------------------------
def test_sweep_tae(num_events=9999999999, output_level=MSG_INFO, dump=True, sweep=1, input='Input.tae'):
  import GaudiOnline
  import Configurables
  hdr = header_dump(name='Headers', raw_data='Banks/Central')
  dmp = Configurables.Online__OdinBankDump('ODIN-dump')
  dmp.Print = 1.0
  dmp.Histo = False
  #dmp.Banks = 'Banks/RawData'
  app = GaudiOnline.readDatafile(input, interactive=False, output_level=output_level, dump=dump, summary=False, algs=[dmp])
  #app.input.RawData  = 'Banks/RawData'
  app.input.SweepTAE = sweep  # 1: real sweep, 2: random sweek
  return setup_app_basic(app)

#--------------------------------------------------------------------------
def test_bank_dump_tae(num_events, output_level=MSG_INFO, dump=True, input='Input.mdf'):
  import GaudiOnline
  import Configurables
  hdr = header_dump(name='Headers', raw_data='Banks/RawDataGuard')
  dmp = Configurables.Online__TAEBankDump('TAE-dump')
  dmp.DumpData = dump
  dmp.BankType = 16
  app = GaudiOnline.readDatafile(input, interactive=False, output_level=output_level, algs=[hdr, dmp])
  return setup_app_basic(app)

#--------------------------------------------------------------------------
def test_daq_idle(num_events=9999999999, output_level=MSG_INFO, input='Input.mdf'):
  import GaudiOnline
  import Configurables
  test  = Configurables.Online__GauchoTestProcess('Test')
  test.handle_daq_idle = 1
  pause = delay('Delay', {'Event': 12})
  pause.EventNumber = 9
  app   = GaudiOnline.readDatafile(input, interactive=False, output_level=output_level, dump=False, summary=False, algs=[pause,test])
  app.config.eventIdleTimeout = 5
  app.config.eventMinimalIdleTime = 5
  return setup_app_basic(app)

#--------------------------------------------------------------------------
def algorithm_sequence_task(output_level=None, algorithms=None):
  import Configurables
  import GaudiOnline
  import Gaudi.Configuration as Gaudi
  import OnlineEnv as online_env

  app = TestApplication(outputLevel=output_level,
                        partitionName=online_env.PartitionName,
                        partitionID=online_env.PartitionID)
  Configurables.EventLoopMgr().Warnings = False
  app.setup_monitoring_service()
  #app.setup_hive(GaudiOnline.FlowManager("EventLoop"), 40)
  app.config.inputType     = 'None'
  app.config.execMode      = 4
  app.enableUI()
  sink = Configurables.OnlMonitorSink()
  sink.HistogramsToPublish = [(".*", ".*")]
  sink.CountersToPublish = [(".*", ".*")]
  app.app.ExtSvc.append(sink)
  app.setup_algorithms(algorithms)
  print('Setup complete....')
  return app

#--------------------------------------------------------------------------
def gaucho_test_task(output_level=None, counters=None, histograms=None, pause=None, timer=None, prefill=10000):
  import Configurables
  import OnlineEnv

  if output_level is None:
    output_level = OnlineEnv.OutputLevel

  have_pause = pause
  print(f'+++ Execute gaucho_test_task(output_level={output_level}, counters={counters}, histograms={histograms}, pause={pause})')
  algs = []
  if counters:
    test = Configurables.Online__CounterTypeTest('Counters')
    test.num_prefill = prefill
    if pause:
      test.goto_pause = int(pause)
    pause = None
    print('Adding Counter test algorithm')
    algs.append(test)
  if histograms:
    test = Configurables.Online__HistogramTypeTest('Histos')
    test.num_prefill = prefill
    if pause:
      test.goto_pause = int(pause)
    pause = None
    print('Adding Histogram test algorithm')
    algs.append(test)
  # Set auto-update if requested
  if timer and timer > 0:
    for test in algs:
      test.auto_update = timer
  app = algorithm_sequence_task(output_level=output_level, algorithms=algs)
  if have_pause:
    app.config.enablePause = True
  app.monSvc.HaveRates = False
  app.monSvc.CounterUpdateInterval   = 3
  app.monSvc.DimUpdateInterval       = 5
  app.monSvc.UniqueServiceNames      = True
  app.monSvc.ExpandCounterServices   = True
  app.monSvc.ExpandNameInfix         = '<proc>/'
  print(f'app.app.TopAlg : {str(app.app.TopAlg)}')
  sys.stdout.flush()
  return app

GauchoTestAlg        = gaucho_test_task
GauchoTestProcess    = gaucho_test_task
GauchoHistoProcess   = gaucho_test_task
GauchoCounterProcess = gaucho_test_task
#--------------------------------------------------------------------------
def Adder(architecture=None, match=None):
  import importlib.util
  if not os.getenv('ADDER_TYPE',None):
    os.environ['ADDER_TYPE'] = 'TestAdderBasic'
  fname = os.environ['FARMCONFIGROOT'] + '/job/AddersFromArchitecture.py'
  spec  = importlib.util.spec_from_file_location('AddersFromArchitecture', fname)
  adder = importlib.util.module_from_spec(spec)
  sys.modules['Adder'] = adder
  spec.loader.exec_module(adder)
  print('Adder module:  '+str(dir(adder)))
  getattr(adder, 'run_adder')()
  return (spec, adder)

#--------------------------------------------------------------------------
def Passthrough(max_events=None, idle_timeout=None, pre_algs=None, post_algs=None):
  import importlib.util
  fname = os.environ['FARMCONFIGROOT'] + '/job/Passthrough.py'
  spec  = importlib.util.spec_from_file_location('Passthrough', fname)
  passthrough = importlib.util.module_from_spec(spec)
  sys.modules['Passthrough'] = passthrough
  spec.loader.exec_module(passthrough)
  app = getattr(passthrough, 'setup_application')(pre_algs=pre_algs, post_algs=post_algs)
  return (spec, passthrough, app)

#--------------------------------------------------------------------------
def DAQIdle(max_events=None, idle_timeout=None):
  test  = Configurables.Online__GauchoTestProcess('Test')
  test.handle_daq_idle = 1
  test.OutputLevel = MSG_INFO
  spec, passthrough, app = Passthrough(max_events=max_events, idle_timeout=idle_timeout, post_algs=[test])
  app.config.eventIdleTimeout = 5
  app.config.eventMinimalIdleTime = 5
  return (spec, passthrough, app)
