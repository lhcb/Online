#==========================================================================
#  LHCb Online software suite
#--------------------------------------------------------------------------
# Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
# All rights reserved.
#
# For the licensing terms see OnlineSys/LICENSE.
#
# Author     : M.Frank
#
#==========================================================================
#
# Gaudi online tests
#
#==========================================================================
__version__ = "1.0"
__author__  = "Markus Frank <Markus.Frank@cern.ch>"

import dataflow.task as DF

# --------------------------------------------------------------------------
def setup_test():
  online_environment = DF.get_online_env()
  task = DF.basic_task('Class1', output_level=DF.MSG_INFO)
  task.setup_basics(auto_startup=True, auto_finalize=True, ui=None, monitoring=None, exit=True)
  test = task.create_component(type='Dataflow_PropertyTest', name='PropertyTest')
  task.services.append(test)
  test.use = ''
  return (task, test)
    
# --------------------------------------------------------------------------
def set_primitives(test):
  test.use = test.use + '|PRIMITIVES'
  test.prop_str    = 'Hallo Markus!'
  test.prop_bool   = True;
  test.prop_int    = 123456
  test.prop_uint   = 987654321
  test.prop_long   = 123456789012345
  test.prop_ulong  = 987654321098765
  test.prop_float  = 123.4567890
  test.prop_double = 12345.678901234

# --------------------------------------------------------------------------
def set_maps(test):
  test.use = test.use + '|MAPS'
  test.map_str_str      = {'a': 'AA', 'b': 'BB', 'c': 'CC'}
  test.map_str_bool     = {'a': 1, 'b': 0, 'c': 1}
  test.map_str_int      = {'a': 11, 'b': 22, 'c': 33}
  test.map_str_uint     = {'a': 99, 'b': 88, 'c': 77}
  test.map_str_long     = {'a': 111, 'b': 222, 'c': 333}
  test.map_str_ulong    = {'a': 9999, 'b': 999, 'c': 99}
  test.map_str_float    = {'a': 11.11, 'b': 22.22, 'c': 33.33}
  test.map_str_double   = {'a': 11.111, 'b': 22.222, 'c': 33.333}
  test.map_str_vec_str  = {'a': ['1','2'], 'b': ['3','4'], 'c': ['5','6']}
  test.map_str_map_str_str  = {'a': {'A':'Za', 'B':'Zb'}, 'b': {'C':'Zc', 'D':'Zd'}}
  
  test.map_int_str      = {100 * 10: 'AA', 200 * 10: 'BB', 300 * 10: 'CC'}
  test.map_int_bool     = {100 * 10: 1, 200 * 10: 0, 300 * 10: 1}
  test.map_int_int      = {100 * 10: 11, 200 * 10: 22, 300 * 10: 33}
  test.map_int_uint     = {100 * 10: 11, 200 * 10: 22, 300 * 10: 33}
  test.map_int_long     = {100 * 10: 111, 200 * 10: 222, 300 * 10: 333}
  test.map_int_ulong    = {100 * 10: 1111, 200 * 10: 2222, 300 * 10: 3333}
  test.map_int_float    = {100 * 10: 11.11, 200 * 10: 22.22, 300 * 10: 33.33}
  test.map_int_double   = {100 * 10: 11.111, 200 * 10: 22.222, 300 * 10: 33.333}

# --------------------------------------------------------------------------
def set_sets(test):
  test.use = test.use + '|SETS'
  test.set_str          = ['aa', 'bbb', 'cccc', 'ddddd', 'eeeeee', 'fffffff', 'gggggggg']
  test.set_bool         = [0, 0, 0, 1, 1, 1]
  test.set_int          = [0, 1, 2, 3, 4, 5, 6, 7, 8, 8, 8]
  test.set_uint         = [0, 1, 2, 3, 4, 5, 6, 7, 8, 8, 8]
  test.set_long         = [0, 11100, 22200, 33300, 44400, 55500, 66600, 77700, 88800, 99900, 111000, 111100]
  test.set_ulong        = [0, 11100, 22200, 33300, 44400, 55500, 66600, 77700, 88800, 99900, 111000, 111100]
  test.set_float        = [0, 1.111, 2.222, 3.333, 4.444, 5.555, 6.666, 7.777, 8.888, 9.999, 10.1010]
  test.set_double       = [0, 100.01, 200.02, 300.03, 400.04, 500.05, 600.06, 700.07, 800.08, 900.09, 1000.10, 1100.11]
  
# --------------------------------------------------------------------------
def set_vectors(test):
  test.use = test.use + '|VECTORS'
  test.vector_str       = ['aa', 'bbb', 'cccc', 'ddddd', 'eeeeee', 'fffffff', 'gggggggg']
  test.vector_bool      = [0, 0, 0, 1, 1, 1]
  test.vector_int       = [0, 1, 2, 3, 4, 5, 6, 7, 8, 8, 8]
  test.vector_uint      = [0, 1, 2, 3, 4, 5, 6, 7, 8, 8, 8]
  test.vector_long      = [0, 11100, 22200, 33300, 44400, 55500, 66600, 77700, 88800, 99900, 111000, 111100]
  test.vector_ulong     = [0, 11100, 22200, 33300, 44400, 55500, 66600, 77700, 88800, 99900, 111000, 111100]
  test.vector_float     = [0, 1.111, 2.222, 3.333, 4.444, 5.555, 6.666, 7.777, 8.888, 9.999, 10.1010]
  test.vector_double    = [0, 100.01, 200.02, 300.03, 400.04, 500.05, 600.06, 700.07, 800.08, 900.09, 1000.10, 1100.11]

# --------------------------------------------------------------------------
def set_lists(test):
  test.use = test.use + '|LISTS'
  test.list_str         = ['aa', 'bbb', 'cccc', 'ddddd', 'eeeeee', 'fffffff', 'gggggggg']
  test.list_bool        = [0, 0, 0, 1, 1, 1]
  test.list_int         = [0, 1, 2, 3, 4, 5, 6, 7, 8, 8, 8]
  test.list_uint        = [0, 1, 2, 3, 4, 5, 6, 7, 8, 8, 8]
  test.list_long        = [0, 11100, 22200, 33300, 44400, 55500, 66600, 77700, 88800, 99900, 111000, 111100]
  test.list_ulong       = [0, 11100, 22200, 33300, 44400, 55500, 66600, 77700, 88800, 99900, 111000, 111100]
  test.list_float       = [0, 1.111, 2.222, 3.333, 4.444, 5.555, 6.666, 7.777, 8.888, 9.999, 10.1010]
  test.list_double      = [0, 100.01, 200.02, 300.03, 400.04, 500.05, 600.06, 700.07, 800.08, 900.09, 1000.10, 1100.11]

# --------------------------------------------------------------------------
def test_primitives():
  task, test = setup_test()
  set_primitives(test)
  return task.run()

# --------------------------------------------------------------------------
def test_maps():
  task, test = setup_test()
  set_maps(test)
  return task.run()

# --------------------------------------------------------------------------
def test_sets():
  task, test = setup_test()
  set_sets(test)
  return task.run()

# --------------------------------------------------------------------------
def test_vectors():
  task, test = setup_test()
  set_vectors(test)
  return task.run()

# --------------------------------------------------------------------------
def test_lists():
  task, test = setup_test()
  set_lists(test)
  return task.run()

# --------------------------------------------------------------------------
def test_all():
  task, test = setup_test()
  set_primitives(test)
  set_maps(test)
  set_sets(test)
  set_vectors(test)
  set_lists(test)
  return task.run()
