#==========================================================================
#  LHCb Online software suite
#--------------------------------------------------------------------------
# Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
# All rights reserved.
#
# For the licensing terms see OnlineSys/LICENSE.
#
# Author     : M.Frank
#
#==========================================================================
__version__ = "1.0"
__author__  = "Markus Frank <Markus.Frank@cern.ch>"

import os, sys, time, errno, logging
import GaudiOnlineTests.Environ as Environ
from   GaudiOnlineTests.Environ import log
from   Online.MessageLevels import *

# --------------------------------------------------------------------------
class NodeLogger(Environ.DimActor):
  # ------------------------------------------------------------------------
  def __init__(self, server, dns=None):
    Environ.DimActor.__init__(self, dns=dns)
    self.server = server
    self.fifo = None

  # ------------------------------------------------------------------------
  def shutdown(self):
    return Environ.DimActor.shutdown(self, self.server)

# --------------------------------------------------------------------------
def NodeLoggerBasicSetup(tag=None, dns=None):
  log(MSG_ALWAYS,"++ NodeLoggerBasicSetup with pid %d starting up... log-fifo: %s"%(os.getpid(), os.getenv('LOGFIFO','????'),))
  os.environ['LOGFIFO'] = '/dev/shm/logs_ID%04X.dev'%(tag,)
  os.putenv('LOGFIFO', os.environ['LOGFIFO'])
  
  if not tag:
    raise Exception("Test argument 'tag' is not supplied!")

  if not dns:
    node = os.getenv('DIM_DNS_NODE','localhost')
    port = os.getenv('DIM_DNS_PORT','2505')
    dns = node+':'+port

  try:
    import fifo_log
    logger = NodeLogger(dns=dns, server='/ID%04X/LOGS'%(tag,))
    fifo_log.logger_stop()
    logger.setup_fifolog()
    logger.start_serving(name=utgid)
    time.sleep(5)
    log(MSG_ALWAYS, '++ NodeloggerBasicSetup with pid %d fully initialized'%(os.getpid(), ))
    for count in range(0, 100):
      log(MSG_ALWAYS, '++ This is message number %d', count)
    log(MSG_ALWAYS, '++ NodeLoggerBasicSetup with pid %d finished.'%(os.getpid(), ))

    sys.stdout.flush()
    time.sleep(15)
    return 0

  except Exception as X:
    Environ.print_trace(X)
    return errno.EINVAL
