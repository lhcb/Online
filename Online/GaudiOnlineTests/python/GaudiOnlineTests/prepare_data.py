#==========================================================================
#  LHCb Online software suite
#--------------------------------------------------------------------------
# Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
# All rights reserved.
#
# For the licensing terms see OnlineSys/LICENSE.
#
# Author     : M.Frank
#
#==========================================================================
__version__ = "1.0"
__author__  = "Markus Frank <Markus.Frank@cern.ch>"

import os, sys

def _exec(command):
  print(f'Executing {command}')
  os.system(command)

def do_unzip(zip_file):
  import gzip, stat
  input = os.getenv('GAUDIONLINETESTSROOT', '????')+os.sep+'tests'+os.sep+zip_file
  output = os.getcwd() + os.sep + os.path.splitext(zip_file)[0]
  out_dir = os.path.dirname(output)
  print(f'Input  file: "{input}"')
  print(f'Output file: "{output}"')
  prot = stat.S_IRWXU|stat.S_IRWXG|stat.S_IROTH|stat.S_IXOTH
  try:
    if os.path.exists(out_dir):
      os.chmod(path=out_dir, mode=prot)
    else:
      os.mkdir(path=out_dir, mode=prot)
  except Exception as x:
    print('os.mkdir: '+str(x))
  compressed_file = gzip.open(input, 'rb')
  decompressed_file = open(output, 'wb')
  length = 1
  while length > 0:
    buff = compressed_file.read(1024*10)
    length = len(buff)
    if length > 0:
      decompressed_file.write(buff)

  compressed_file.close()
  decompressed_file.close()
  print(f'Successfully unzipped file {zip_file}')

def prepare_eventdata():
  print('Preparing input data for tests section <eventdata.qms>')
  print(f'Working directory: {os.getcwd()}')
  do_unzip('data/mep_data.raw.gz')
