#==========================================================================
#  LHCb Online software suite
#--------------------------------------------------------------------------
# Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
# All rights reserved.
#
# For the licensing terms see OnlineSys/LICENSE.
#
# Author     : M.Frank
#
#==========================================================================
__version__ = "1.0"
__author__  = "Markus Frank <Markus.Frank@cern.ch>"

import os, sys
import TaskDB

global total_api_calls
total_api_calls = 0

def _printObject(type, key, object):
    print ('%-16s:      %s'%(type, object[key],))
    for k in object.keys():
      if k != key:
        print ('%-16s:      %-16s %s'%('|',k+':',object[k],))

#==========================================================================
def _get_connection(server):
  """ Open server connection and verify the functioning

      \author   M.Frank
      \version  1.0
  """
  try:
    db = TaskDB.connect(address=server)
    if db.test():
      print('+++ Connected to TaskDB server: '+server)
      return db
    print('+++ FAILED to connect to TaskDB server: '+server)
  except Exception as e:
    print('+++ FAILED to connect to TaskDB server: '+server)
  raise Exception('FAILED to connect to TaskDB server: '+server)

#==========================================================================
def print_db_collections(server):
  """ Test to Access all collections from the TASKDB using the python API

      \author   M.Frank
      \version  1.0
  """
  db = _get_connection(server)

  tasks  = db.getTasksByNode('ECS03')
  print('+++ Tasks running on ECS03: [%d entries]'%(len(tasks), ))
  TaskDB.printTasks(tasks)

  tasks  = db.getTask('*')
  print('+++ Tasks matching *: [%d entries]'%(len(tasks), ))
  TaskDB.printTasks(tasks)

  tasks  = db.getTask('NodeLogger')
  print('+++ Tasks matching NodeLogger: [%d entries]'%(len(tasks), ))
  TaskDB.printTasks(tasks)

  sets   = db.getSet('*')
  print('+++ Task sets matching *: [%d entries]'%(len(sets), ))
  TaskDB.printTaskSets(sets)

  classes = db.getClass('*')
  print('+++ Node classes matching *: [%d entries]'%(len(classes), ))
  TaskDB.printClasses(classes)

  nodes = db.getNode('*')
  print('+++ Node names matching *: [%d entries]'%(len(nodes), ))
  TaskDB.printNodeTypes(nodes)

#==========================================================================
def print_node_types(server, match):
  """ Test to effectively test the full read-only interface of the
      TaskDB API.

      \author   M.Frank
      \version  1.0
  """
  import time
  db = None
  for i in range(10):
    try:
      db = _get_connection(server)
    except:
      time.sleep(10)
      continue
    break
  if not db:
    print('ERROR: Failed to access taskdb connection after 10 retries!')
    return

  node_types = db.getNode(match)
  print('+++ Node names matching *: [%d entries]'%(len(node_types), ))
  for typ in node_types:
    _printObject('+- Node-type', 'regex', typ)
    classes = db.classesInNode(typ['regex'])
    for cls in classes:
      _printObject('|-- Class ', 'node_class', cls)
      sets = db.taskSetsInClass(cls['node_class'])
      for s in sets:
        _printObject('|---- Set ', 'task_set', s)
        tasks = db.tasksInSet(s['task_set'])
        for task in tasks:
          tsk = db.getTask(task['task'])
          for t in tsk:
            _printObject('|------ Task ', 'task', t)

def inc_calls():
  global total_api_calls
  import time
  total_api_calls = total_api_calls + 1
  #time.sleep(0.02)
  return total_api_calls

#==========================================================================
def explore_match(db, node_match, log=None):
  node_types = db.getNode(node_match)
  start = inc_calls()
  end = start
  for typ in node_types:
    classes = db.classesInNode(typ['regex'])
    end = inc_calls()
    for cls in classes:
      sets = db.taskSetsInClass(cls['node_class'])
      end = inc_calls()
      for s in sets:
        tasks = db.tasksInSet(s['task_set'])
        end = inc_calls()
        for task in tasks:
          tsk = db.getTask(task['task'])
          end = inc_calls()
  if log:
    print(f'+++ Node names matching "{node_match}": [{len(node_types)} entries, {end-start} calls]')

#==========================================================================
def make_matches():
  matches = ['dataflow01','dataflow02','ecs03', 'fmc01', 'hlt02', 'mon0101']
  detectors = ['va', 'vc', 'ua', 'uc', 'sa', 'sc', 'ec', 'hc', 'ma', 'mc']
  for d in detectors:
    for eb in range(60):
      matches.append(f'{d}eb{eb+1:02d}')
  for sf in range(2):
    matches.append(f'hlt{sf+1:02d}')
    for node in range(40):
      matches.append(f'hlt{sf+1:02d}{node+1:02d}')
  return matches

#==========================================================================
def stress(server):
  import time
  global total_api_calls
  matches = make_matches()

  db = None
  for i in range(10):
    try:
      db = _get_connection(server)
      total_api_calls = total_api_calls + 1
    except:
      time.sleep(10)
      continue
    break
  if not db:
    print('ERROR: Failed to access taskdb connection after 10 retries!')
    return
  try:
    for match in matches:
      explore_match(db, match)
  except Exception as X:
    print(f'Exception during test execution: [{str(X.__class__)}] {str(X)}')
  print(f'+++ TaskDB stress (0):  Invoked {total_api_calls}  RPC api calls.')
