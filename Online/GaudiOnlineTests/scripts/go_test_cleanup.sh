#!/bin/bash
#==========================================================================
#  LHCb Online software suite
#--------------------------------------------------------------------------
# Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
# All rights reserved.
#
# For the licensing terms see OnlineSys/LICENSE.
#
# Author     : M.Frank
#
#==========================================================================
echo "....starting cleanup.....";
#
#
echo "+++ Cleanup outputs from qmtest/dataflow.qms ...";
/bin/rm -rf df_*;
#
echo "+++ Cleanup outputs from qmtest/controller.qms ...";
/bin/rm -rf ctrl_*;
#
echo "+++ Cleanup outputs from qmtest/nodelogger.qms ...";
/bin/rm -rf node_logger_*;
#
echo "+++ Cleanup outputs from qmtest/pydim.qms ...";
rm -rf pydim_*;
#
echo "+++ Cleanup outputs from qmtest/gaucho.qms ...";
rm -rf  gaucho_*;
rm -rf  gaucho;
mkdir   gaucho;
#
echo "+++ Cleanup outputs from qmtest/adders.qms ...";
rm -rf  add_*;
#
echo "+++ Cleanup outputs from qmtest/gaudionline.qms ...";
rm -rf  go_*;
/bin/rm -f *.joboptsdump;
/bin/rm -f Output*.tae;
/bin/rm -f Output*.mdf;
#
/bin/rm -f ID*;
/bin/rm -f DIM_DNS.log;
# ---------------------------------------------------------------
echo "....cleanup finished.....";
