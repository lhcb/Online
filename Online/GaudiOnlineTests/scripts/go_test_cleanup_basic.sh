#!/bin/bash
#==========================================================================
#  LHCb Online software suite
#--------------------------------------------------------------------------
# Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
# All rights reserved.
#
# For the licensing terms see OnlineSys/LICENSE.
#
# Author     : M.Frank
#
#==========================================================================
echo "....starting cleanup.....";
echo "....Check bm_buffers size.....";
#
/bin/rm -f *.dat;
/bin/rm -f Cnew.xml;
/bin/rm -f histadder.sqlite;
#
python <<EOF
import os

num_bytes = 8192
if os.path.exists('/dev/shm/bm_buffers'):
  stat = os.stat('/dev/shm/bm_buffers')
  if stat.st_size < num_bytes:
    try:
      os.truncate('/dev/shm/bm_buffers', num_bytes)
      print(f'Successfully truncated MBM inventory to {num_bytes}')
    except:
      print(f'FAILED to truncate MBM inventory to {num_bytes}')
EOF
#
#
echo "+++ Cleanup shared memory stuff ...";
if test -n "${TEST_TAG}"; then
  # rm -f /dev/shm/TAN_
  if test -e /dev/shm/logs_${TEST_TAG}.dev; then
      rm -f /dev/shm/logs_${TEST_TAG}.dev;
  fi;
  /bin/rm -f /dev/shm/GBL_${TEST_TAG}.dev >/dev/null 2>&1;
  /bin/rm -f /dev/shm/TAN_PUBAREA_${TEST_TAG}.dev >/dev/null 2>&1;
  /bin/rm -f /dev/shm/sem.TAN_PUBAREA_${TEST_TAG}.dev >/dev/null 2>&1;
fi;
#
echo "+++ Cleanup outputs from qmtest/onlinekernel.qms ...";
/bin/rm -rf rtl_*;
/bin/rm -rf this_is_test_directory_created_by_datafile;
#
echo "+++ Cleanup temporary data directory ...";
/bin/rm -rf data;
#
echo "+++ Cleanup outputs from qmtest/farmconfig.qms ...";
/bin/rm -rf farmconfig_*;
#
echo "+++ Cleanup outputs from qmtest/taskdb.qms ...";
/bin/rm -rf taskdb_*;
#
echo "+++ Cleanup outputs from qmtest/storage.qms ...";
/bin/rm -rf storage_*;
/bin/rm -rf db_server_*;
/bin/rm -rf fs_server_*;
/bin/rm -rf fdb_*;
/bin/rm -rf hlt1_data;
#
# ---------------------------------------------------------------
rmdir this_is_test_directory_created_by_datafile;
echo "....cleanup finished.....";
