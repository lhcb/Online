#!/bin/bash
#==========================================================================
#  LHCb Online software suite
#--------------------------------------------------------------------------
# Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
# All rights reserved.
#
# For the licensing terms see OnlineSys/LICENSE.
#
# Author     : M.Frank
#
#==========================================================================
shift;

setup_rootsys()   {
    if test -z "${ROOTSYS}"; then
	ROOTSYS=`which root`;
	if test -n "${ROOTSYS}"; then
	    ROOTSYS=`dirname ${ROOTSYS}`;
	    ROOTSYS=`dirname ${ROOTSYS}`;
	fi;
	# echo "ROOTSYS=${ROOTSYS}";
	export ROOTSYS;
    #else
	#echo "+++ ROOTSYS=${ROOTSYS}";
    fi;
}
if test -z "${APPLICATION}"; then
    export APPLICATION=Online::OnlineEventApp;
fi;
#
#
#
setup_rootsys;
#
# echo "Execute:  $*";
export DIM_DNS_NODE=127.0.0.1;
# export ASAN_OPTIONS=detect_odr_violation=0
temp_file=/tmp/0xfeed$$_commands.py;
echo $* >${temp_file}
#
#  Start the Gaudi task
#
exec -a ${UTGID} `which python` `which gaudirun.py` ${temp_file} --application=${APPLICATION} 2>&1
# gdb --args `which python` `which gaudirun.py` ${temp_file} --application=${APPLICATION} 2>&1
rm -f ${temp_file};
