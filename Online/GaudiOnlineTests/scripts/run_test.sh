#!/bin/bash
# ===============================================================================
#  (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration
#
#  This software is distributed under the terms of the GNU General Public
#  Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".
#
#  In applying this licence, CERN does not waive the privileges and immunities
#  granted to it by virtue of its status as an Intergovernmental Organization
#  or submit itself to any jurisdiction.
#
# ===============================================================================
#
#
if test -n "${DF_DEBUG}"; then
    echo "+++ Executing run_test.sh";
    echo "+++ Working directory: `pwd`";
    echo "+++ Command: ${COMMAND}";
fi;
. ./setup.vars;
. ${FARMCONFIGROOT}/job/createEnvironment.sh $*;

print()   {
    severity="[$1]";
    shift;
    printf "%-24s %9s %s\n" "${UTGID}" "$severity" "$*";
}

setup_rootsys()   {
    if test -z "${ROOTSYS}"; then
	ROOTSYS=`which root`;
	if test -n "${ROOTSYS}"; then
	    ROOTSYS=`dirname ${ROOTSYS}`;
	    ROOTSYS=`dirname ${ROOTSYS}`;
            export ROOTSYS;
        else
  	    print ERROR "+++ Cannot locate root executable. ROOTSYS cannot be defined!";
        fi;
    fi;
}

print_setup()   {
  #set GAUDIONLINETESTS_DEBUG=1;
  print INFO "+++ UTGID:          ${UTGID}";
  print INFO "+++ DIM_DNS    port:${DIM_DNS_PORT}";
  print INFO "+++ DIM_DNS    node:${DIM_DNS_NODE}";
  print INFO "+++ OPTIONS:        ${OPTIONS}";
  print INFO "+++ TASK_TYPE:      ${TASK_TYPE}";
  if test "${TASK_TYPE}" = "Controller"; then
    print INFO "+++ ARCH_FILE:      ${ARCH_FILE}";
    print INFO "+++ ARCHITECTURE:   ${ARCHITECTURE}";
    print INFO "+++ RUNINFO:        ${RUNINFO}";
    print INFO "+++ DIM_DNS    TMS: ${TMS_DNS}";
    print INFO "+++ DIM_DNS    SMI: ${SMI_DNS}";
    print INFO "+++ WORKING_DIR:    ${WORKING_DIR}";
  else
    # print INFO "+++ WORKING_DIR:    ${WORKING_DIR}";
    # print INFO "+++ WORKING_DIR:    `pwd`";
    if test -n "${CONTROLLER_PARENT}"; then
      print INFO "+++ CONTROLLER_PARENT: ${CONTROLLER_PARENT}";
    fi;
    if test -n "${GAUDIONLINETESTS_DEBUG}"; then
      print INFO "+++ LOGFIFO:        ${LOGFIFO}";
      print INFO "+++ ARCH_FILE:      ${ARCH_FILE}";
      print INFO "+++ ARCHITECTURE:   ${ARCHITECTURE}";
      print INFO "+++ RUNINFO:        ${RUNINFO}";
      print INFO "+++ WORKING_DIR:    ${WORKING_DIR}";
    fi;
    if test -n "${TAN_PORT}"; then
      print INFO "+++ TAN host:${TAN_NODE}   port:${TAN_PORT}";
    fi;
  fi;
}

print_process()  {
  exec=$1;
  shift;
  print_setup;
  print INFO "execute bash command: `basename ${exec}` $*";
}

execute_process()  {
  print_process $*;
  exec=$1;
  shift;
  if test -n "${STDIN}"; then
    eval "export THE_STDIN=\"${STDIN}\"";
    exec -a ${UTGID} ${exec} $*  < ${THE_STDIN};
  else
    exec -a ${UTGID} ${exec} $*;
  fi;
  print ERROR "+++ Failed to start process [Exit code: $?]: ${exec} $*";
  exit $?;
}

setup_rootsys;

export PYTHONPATH=`pwd`:${PYTHONPATH};
# export ASAN_OPTIONS=detect_odr_violation=0
if test "${TASK_TYPE}" = "Controller"; then
  print INFO "Starting controller for batchbench....";
  export TMS_DNS=${DIM_DNS_NODE};
  export SMI_DNS=${DIM_DNS_NODE};
  export SMI_FILE=${SMICONTROLLERROOT}/options/StdNode.VIP;
  export SMI_DOMAIN="${PARTITION_NAME}-host:local-pid:$$_SMI";
  SMI_DEBUG=0;
  export CONTROLLER_PARENT=${UTGID};
  #
  INVOKE_DEBUGGER=;
  if test "`hostname -d`" = "lbdaq.cern.ch"; then
      INVOKE_DEBUGGER="/usr/bin/gdb --args";
  fi;
  #
  execute_process  \
      `which gentest` libSmiController.so smi_controller         \
      -service=none -print=4 -standalone=1 -bindcpus=0 -forking  \
      -dns=${DIM_DNS_NODE}           \
      -tmsdns=${TMS_DNS}             \
      -smidns=${SMI_DNS}             \
      -part=${PARTITION_NAME}        \
      -smidomain=${SMI_DOMAIN}       \
      -smidebug=${SMI_DEBUG}         \
      -smifile=${SMI_FILE}           \
      -count=${NBOFSLAVES}           \
      -runinfo=${RUNINFO}            \
      -taskconfig=${ARCH_FILE}       \
      "${CONTROLLER_REPLACEMENTS}" < ${GAUDIONLINETESTSROOT}/scripts/gdb.cmds
else
  print INFO "+++ Running $*";
  ALL_ARGUMENTS="${ARGUMENTS}${ARG0}${ARG1}${ARG2}${ARG3}${ARG4}${ARG5}${ARG6}${ARG7}${ARG8}${ARG9}";
  export EXECUTABLE;
  export DIM_DNS_PORT;
  export DIM_DNS_NODE;

  if test "${EXECUTABLE}" = "gentest"; then
    execute_process `which gentest` ${ALL_ARGUMENTS};
  elif test "${EXECUTABLE}" = "genRunner"; then
    execute_process `which genRunner` ${ALL_ARGUMENTS};
  elif test "${EXECUTABLE}" = "python"; then
    eval "export THE_ARGS=\"${ALL_ARGUMENTS}\"";
    execute_process `which python` "${THE_ARGS}";
  fi;

  COMMAND="import os,sys; import ${OPTIONS} as module; module.${TASK_TYPE}(${ALL_ARGUMENTS})";
  EXE="`which python`";
  if test "${EXECUTABLE}" = "run_online_gaudi_python.sh"; then
      echo "Executing: ${EXECUTABLE} ${COMMAND}";
      . ${EXECUTABLE} run "${COMMAND}";
  elif test "${EXECUTABLE}" = "genPython"; then
      EXE="`which genPython`";
      if test -z "${LOGFIFO}"; then
	  LOGFIFO="/dev/shm/logs_${PARTITION}.dev";
      fi;
      export LOGFIFO;
      while true; do
	  if test -e ${LOGFIFO}; then
	      break;
	  fi;
	  sleep 1;
      done;
  fi;
  print_process    ${EXE} -c "${COMMAND}";
  if test "${CONTROLLER_PARENT}" != "" -a "$TASK_TYPE}" != ""; then
    exec -a ${UTGID} ${EXE} -c "${COMMAND}";
  elif test "$TASK_TYPE}" != ""; then
    exec -a ${UTGID} ${EXE} -c "${COMMAND}";
  fi;
fi;
