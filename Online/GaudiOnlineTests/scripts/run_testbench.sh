#!/bin/bash
#==========================================================================
#  LHCb Online software suite
#--------------------------------------------------------------------------
# Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
# All rights reserved.
#
# For the licensing terms see OnlineSys/LICENSE.
#
# Author     : M.Frank
#
#==========================================================================
#
#
usage()  {
    `which python` ${ONLINETESTBENCH}/testbench.py --show-help;
}
#
#
arguments=;
workdir=;
datadir=;
partition=;
producer=;
architecture=;
controller=;
write_unique_id=;
taskscript=;
needtan=;
writeid=;
readid=;
runtime=;
scenario="--scenario=${ONLINETESTBENCH}/dataflow_scenario.py";
runinfo="--runinfo=${ONLINETESTBENCH}/online_env.py";
BENCH_NAME=TEST;
#
parse_arguments()   {
  #
  PARSED_ARGUMENTS=$(getopt -a -n run_testbench -o a:c:dD:l:m:D:P:p:r:R:s:t:Tw:W:X: --longoptions architecture:,controller:,debug,data-dir:,log-file:,measure-throughput,dim-dns-node:,dim-dns-port:,producer:,read-unique-id:,scenario:,runinfo:,task-script:,need-tan,working-dir:,write-unique-id:,runtime: -- "[run_testbench]" $*)
  VALID_ARGUMENTS=$?
  if [ "$VALID_ARGUMENTS" != "0" ]; then
    do_print "Invalid arguments to cmake step";
    usage;
  fi;
  #
  #
  eval set -- "$PARSED_ARGUMENTS";
  while :
  do
    case "${1}" in
	-a | --architecture)    architecture="`echo --architecture=${2}`";  shift 2 ;;
	-c | --controller)      controller="`echo --controller=${2}`";      shift 2 ;;
	-d | --debug)           debug="--debug";                            shift ;;
	-D | --data-dir)        datadir="`echo --data-dir=${2}`";           shift 2 ;;
	-l | --log-file)        logfile="`echo --log-file=${2}`";           shift 2 ;;
	-m | --measure-throughput)  measure_throughput="`echo --measure-throughput=${2}`"; shift 2 ;;
        -D | --dim_dns_node)    dns_node="`echo --dim-dns-node=${2}`";      shift 2 ;;
        -P | --dim_dns_port)    dns_port="`echo --dim-dns-port=${2}`";      shift 2 ;;
	-P | --partition)       partition="`echo --partition=${2}`";        shift 2 ;;
	-p | --producer)        producer="`echo --producer=${2}`";          shift 2 ;;
	-r | --runinfo)         runinfo="`echo --runinfo=${2}`";            shift 2 ;;
	-R | --read-unique-id)  readid="`echo --read-unique-id=${2}`";      shift 2 ;;
	-s | --scenario)        scenario="`echo --scenario=${2}`";          shift 2 ;;
	-t | --task-script)     taskscript="`echo --task-script=${2}`";     shift 2 ;;
	-T | --need-tan)        needtan="--need-tan";                       shift;;
	-w | --working-dir)     workdir="`echo --working-dir=${2}`";
				BENCH_NAME="`echo ${2}`";                   shift 2 ;;
	-W | --write-unique-id) writeid="`echo --write-unique-id=${2}`";    shift 2 ;;
	-R | --read-unique-id)  readid="`echo --read-unique-id=${2}`";      shift 2 ;;
	-X | --runtime)         runtime="`echo --runtime=${2}`";            shift 2 ;;
	-h | --help)            usage;                                      shift ;;
	# -- means the end of the arguments; drop this, and break out of the while loop
	--) shift;  break;;
	# If invalid options were passed, then getopt should have reported an error,
	# which we checked as VALID_ARGUMENTS when getopt was called...
	*) do_print " Unexpected option: $1 - this should not happen."
            usage; shift; break;;
    esac;
  done;
}
#
#
setup_rootsys()   {
    if test -z "${ROOTSYS}"; then
	ROOTSYS=`which root`;
	if test -n "${ROOTSYS}"; then
	    ROOTSYS=`dirname ${ROOTSYS}`;
	    ROOTSYS=`dirname ${ROOTSYS}`;
	fi;
	# echo "ROOTSYS=${ROOTSYS}";
	export ROOTSYS;
    fi;
}
#
parse_arguments $*;
setup_rootsys;
#
#
#echo "exec -a testbench_${workdir} `which python` ${ONLINETESTBENCH}/testbench.py $*";
#  #`which gdb` --args
exec -a testbench_${BENCH_NAME}  \
     `which python` ${ONLINETESTBENCH}/testbench.py \
     ${architecture} \
     ${controller}   \
     ${datadir}      \
     ${runinfo}      \
     ${scenario}     \
     ${taskscript}   \
     ${workdir}      \
     ${logfile}      \
     ${needtan}      \
     ${partition}    \
     ${producer}     \
     ${dns_port}     \
     ${dns_node}     \
     ${runtime}      \
     ${readid}       \
     ${writeid}      \
     ${measure_throughput}  < ${GAUDIONLINETESTSROOT}/scripts/gdb.cmds;
