//
// blocking_tcp_echo_client.cpp
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//
// Copyright (c) 2003-2015 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

// System / Boost include files
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <boost/asio.hpp>

extern "C" int boost_asio_echo_client(int argc, char** argv)   {
  enum { max_length = 1024 };
  using boost::asio::ip::tcp;

  try  {
    if (argc != 3)    {
      std::cerr << "Usage: blocking_tcp_echo_client <host> <port>\n";
      return 1;
    }
    boost::asio::io_context io_context;
    tcp::resolver resolver(io_context);
#if BOOST_ASIO_VERSION < 103400    
    auto conns = resolver.resolve({tcp::v4(), argv[1], argv[2]});
#else
    auto conns = resolver.resolve(tcp::v4(), argv[1], argv[2]);
#endif
    tcp::socket s(io_context);
    boost::asio::connect(s, conns);

    using namespace std; // For strlen.
    while(1)  {
      std::cout << "Enter message: ";
      char request[max_length];
      std::cin.getline(request, max_length);
      size_t request_length = strlen(request);
      if ( !request_length ) break;
      boost::asio::write(s, boost::asio::buffer(request, request_length));
      
      char reply[max_length];
      size_t reply_length = boost::asio::read(s,boost::asio::buffer(reply,request_length));
      std::cout << "Reply is: ";
      std::cout.write(reply, reply_length);
      std::cout << "\n";
    }
  }
  catch (std::exception& e)  {
    std::cerr << "Exception: " << e.what() << "\n";
  }
  return 0;
}
