//
// blocking_tcp_echo_client.cpp
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//
// Copyright (c) 2003-2015 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

// System / Boost include files

#include <iostream>
#include <istream>
#include <ostream>
#include <string>
#include <boost/asio.hpp>
#include <boost/bind/bind.hpp>

using boost::asio::ip::tcp;
using namespace boost;
using namespace std;

namespace  {
  
  class client  {
  public:
    client(asio::io_context& io_context,
	   const string& server, const string& port, const string& path)
      : resolver_(io_context), socket_(io_context)
    {
      // Form the request. We specify the "Connection: close" header so that the
      // server will close the socket after transmitting the response. This will
      // allow us to treat all data up until the EOF as the content.
      ostream request_stream(&request_);
      request_stream << "GET " << path << " HTTP/1.0\r\n";
      request_stream << "Host: " << server << "\r\n";
      request_stream << "Accept: */*\r\n";
      request_stream << "Connection: close\r\n\r\n";

      // Start an asynchronous resolve to translate the server and service names
      // into a list of endpoints.
      resolver_.async_resolve(server, port,
			      boost::bind(&client::handle_resolve, this,
					  asio::placeholders::error,
					  asio::placeholders::results));
    }

  private:
    void handle_resolve(const boost::system::error_code& err,
			const tcp::resolver::results_type& endpoints)  {
      if (!err)      {
	// Attempt a connection to each endpoint in the list until we
	// successfully establish a connection.
	asio::async_connect(socket_, endpoints,
				   boost::bind(&client::handle_connect, this,
					       asio::placeholders::error));
      }
      else      {
	cout << "Error: " << err.message() << "\n";
      }
    }

    void handle_connect(const boost::system::error_code& err)  {
      if (!err)      {
	// The connection was successful. Send the request.
	asio::async_write(socket_, request_,
				 boost::bind(&client::handle_write_request, this,
					     asio::placeholders::error));
      }
      else      {
	cout << "Error: " << err.message() << "\n";
      }
    }

    void handle_write_request(const boost::system::error_code& err)  {
      if (!err)      {
	// Read the response status line. The response_ streambuf will
	// automatically grow to accommodate the entire line. The growth may be
	// limited by passing a maximum size to the streambuf constructor.
	asio::async_read_until(socket_, response_, "\r\n",
				      boost::bind(&client::handle_read_status_line, this,
						  asio::placeholders::error));
      }
      else      {
	cout << "Error: " << err.message() << "\n";
      }
    }

    void handle_read_status_line(const boost::system::error_code& err)  {
      if (!err)      {
	// Check that response is OK.
	istream response_stream(&response_);
	string http_version;
	response_stream >> http_version;
	unsigned int status_code;
	response_stream >> status_code;
	string status_message;
	getline(response_stream, status_message);
	if (!response_stream || http_version.substr(0, 5) != "HTTP/")	  {
	  cout << "Invalid response\n";
	  return;
	}
	if (status_code != 200)	  {
	  cout << "Response returned with status code ";
	  cout << status_code << "\n";
	  return;
	}

	// Read the response headers, which are terminated by a blank line.
	asio::async_read_until(socket_, response_, "\r\n\r\n",
				      boost::bind(&client::handle_read_headers, this,
						  asio::placeholders::error));
      }
      else      {
	cout << "Error: " << err << "\n";
      }
    }

    void handle_read_headers(const boost::system::error_code& err)  {
      if (!err)      {
	// Process the response headers.
	istream response_stream(&response_);
	string header;
	while (getline(response_stream, header) && header != "\r")
	  cout << header << "\n";
	cout << "\n";

	// Write whatever content we already have to output.
	if (response_.size() > 0)
	  cout << &response_;

	// Start reading remaining data until EOF.
	asio::async_read(socket_, response_,
				asio::transfer_at_least(1),
				boost::bind(&client::handle_read_content, this,
					    asio::placeholders::error));
      }
      else      {
	cout << "Error: " << err << "\n";
      }
    }

    void handle_read_content(const boost::system::error_code& err)  {
      if (!err)      {
	// Write all of the data that has been read so far.
	cout << &response_;

	// Continue reading remaining data until EOF.
	asio::async_read(socket_, response_,
				asio::transfer_at_least(1),
				boost::bind(&client::handle_read_content, this,
					    asio::placeholders::error));
      }
      else if (err != asio::error::eof)      {
	cout << "Error: " << err << "\n";
      }
    }

    tcp::resolver   resolver_;
    tcp::socket     socket_;
    asio::streambuf request_;
    asio::streambuf response_;
  };
}

/// Invocation routine
extern "C" int asio_http_async_client(int argc, char* argv[])   {
  try    {
    if ( argc != 4 )   {
      cout << "Usage: async_client <server> <port> <path>\n";
      cout << "Example:\n";
      cout << "  async_client www.boost.org 80 /LICENSE_1_0.txt\n";
      return 1;
    }
    asio::io_context io_context;
    client c(io_context, argv[1], argv[2], argv[3]);
    io_context.run();
  }
  catch ( std::exception& e )    {
    cout << "Exception: " << e.what() << "\n";
  }
  return 0;
}
