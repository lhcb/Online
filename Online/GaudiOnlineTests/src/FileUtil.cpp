//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================

#include <RTL/rtl.h>
#include "FileUtil.h"

#include <cstring>

#define CHUNK_SIZE 8192

/// Online namespace declaration
namespace Online {

  /// util namespace declaration
  namespace util  {

    /// Compare two named memory buffers
    int compare_buffers(const std::string& f1, const std::vector<uint8_t>& b1,
			const std::string& f2, const std::vector<uint8_t>& b2)   {
      if ( b1.size() != b2.size() )   {
	::lib_rtl_output(LIB_RTL_ERROR,
			 "Decompressed file and source have different size: %s: %ld %s: %ld",
			 f1.c_str(), b1.size(), f2.c_str(), b2.size());
	return EINVAL;
      }
      std::size_t err_byte = 0;
      for( const uint8_t* p1 = &b1.at(0), *p2 = &b2.at(0), *end=p1+b1.size(); p1 < end; )  {
	long len = std::min(end - p1, 1024L);
	if ( ::memcmp(p1, p2, len) == 0 )  {
	  p1 += len;
	  p2 += len;
	  continue;
	}
	++err_byte;
      }
      if ( err_byte > 0 )  {
	::lib_rtl_output(LIB_RTL_ERROR,
			 "Content check differs by %ld frames. %s: %s",
			 err_byte, f1.c_str(), f2.c_str());
	return EINVAL;
      }
      ::lib_rtl_output(LIB_RTL_ALWAYS,
		       "Content check was successful. Buffer %s identical to buffer %s",
		       f1.c_str(), f2.c_str());
      return 0;
    }

    /// Compare two named file buffers (data are read from the files)
    int compare_buffers(Online::Datafile& f1, Online::Datafile& f2)   {
      std::vector<uint8_t> b1 = load_file(f1);
      std::vector<uint8_t> b2 = load_file(f2);

      if ( b1.empty() )   {
	::lib_rtl_output(LIB_RTL_ERROR,
			 "Failed to open file for content check: %s: %d", f1.cname(), f1.fileno());
	return EINVAL;
      }
      else if ( b2.empty() )   {
	::lib_rtl_output(LIB_RTL_ERROR,
			 "Failed to open file for content check: %s: %d", f2.cname(), f2.fileno());
	return EINVAL;
      }
      return compare_buffers(f1.filename(), b1, f2.filename(), b2);
    }

    /// Open datafile and read the data
    std::vector<uint8_t> load_file(Online::Datafile& file)  {
      if ( file.isOpen() || file.open() > 0 )   {
	int iret = 0;
	char chunk[CHUNK_SIZE];
	std::vector<uint8_t> buffer;
	do  {
	  iret = file.read(chunk, sizeof(chunk));
	  if ( iret > 0 )  {
	    std::copy(chunk, chunk+iret, std::back_inserter(buffer));
	  }
	} while ( iret > 0 );
	file.close();
	return buffer;
      }
      ::lib_rtl_output(LIB_RTL_ERROR,"Failed to open file: %s", file.cname());
      return { };
    }
  }    // End namespace util
}      // End namespace Online
