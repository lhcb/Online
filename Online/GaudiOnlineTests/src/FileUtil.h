//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
#ifndef GAUDIONLINETESTS_FILEUTIL_H
#define GAUDIONLINETESTS_FILEUTIL_H

/// Framework include files
#include <RTL/Datafile.h>

/// C/C++ include files
#include <vector>
#include <string>

/// Online namespace declaration
namespace Online {

  /// util namespace declaration
  namespace util  {

    /// Compare two named memory buffers
    int compare_buffers(const std::string& f1, const std::vector<uint8_t>& b1,
			const std::string& f2, const std::vector<uint8_t>& b2);

    /// Compare two named file buffers (data are read from the files)
    int compare_buffers(Online::Datafile& f1, Online::Datafile& f2);

    /// Open datafile and read the data
    std::vector<uint8_t> load_file(Online::Datafile& file);
    
  }    // End namespace util
}      // End namespace Online
#endif // GAUDIONLINETESTS_FILEUTIL_H
