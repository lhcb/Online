//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================

/// Framework include files
#include <RTL/rtl.h>
#include <SCR/scr.h>
#include "KeyboardEmulator.h"

/// C/C++ include files
#include <cstring>
#include <csignal>

Online::KeyboardEmulator::KeyboardEmulator(const char* command)
{
  ::signal(SIGPIPE, SIG_IGN);
  input = ::popen(command, "w");
}

Online::KeyboardEmulator::~KeyboardEmulator()   {
  // if ( input ) ::pclose(input);
}

Online::KeyboardEmulator& Online::KeyboardEmulator::put_chars(const char* data )   {
  int fd = ::fileno(this->input);
  ::write(fd, data, ::strlen(data)+1);
  ::fsync(fd);
  ::lib_rtl_sleep(50*scale);
  return *this;
}

Online::KeyboardEmulator& Online::KeyboardEmulator::put_chars(const std::vector<char>& data )   {
  return put_chars(&data.at(0));
}

Online::KeyboardEmulator& Online::KeyboardEmulator::put_chars(const std::string& data )   {
  return put_chars(data.c_str());
}

/// Hit a single specified key once
Online::KeyboardEmulator& Online::KeyboardEmulator::hit_key(int key)  {
  std::vector<char> v = { char(key), 0 };
  return put_chars(v);
}

/// Hit a single specified key multiple times
Online::KeyboardEmulator& Online::KeyboardEmulator::hit_key(std::size_t n, int key)  {
  for( std::size_t i=0; i < n; ++i ) hit_key(key);
  return *this;
}

Online::KeyboardEmulator& Online::KeyboardEmulator::move_right(std::size_t n)  {
  return hit_key(n, SCR::MOVE_RIGHT);
}

Online::KeyboardEmulator& Online::KeyboardEmulator::move_left(std::size_t n)  {
  return hit_key(n, SCR::MOVE_LEFT);
}

Online::KeyboardEmulator& Online::KeyboardEmulator::move_down(std::size_t n)  {
  return hit_key(n, SCR::MOVE_DOWN);
}

Online::KeyboardEmulator& Online::KeyboardEmulator::move_up(std::size_t n)  {
  return hit_key(n, SCR::MOVE_UP);
}

/// Hit key pad PERIOD key once
Online::KeyboardEmulator& Online::KeyboardEmulator::hit_period()  {
  return hit_key(SCR::KPD_PERIOD);
}

/// Hit key pad ENTER key once
Online::KeyboardEmulator& Online::KeyboardEmulator::hit_enter()  {
  return hit_enter(1);
}

/// Hit key pad ENTER key multiple times
Online::KeyboardEmulator& Online::KeyboardEmulator::hit_enter(std::size_t count)   {
  return hit_key(count, SCR::KPD_ENTER);
}

/// Hit the cursor key pad CARRIAGE RETURN once
Online::KeyboardEmulator& Online::KeyboardEmulator::hit_return()  {
  return hit_return(1);
}

/// Hit the cursor key pad CARRIAGE RETURN repeatedly multiple times
Online::KeyboardEmulator& Online::KeyboardEmulator::hit_return(std::size_t count)   {
  return hit_key(count, '\n');
}

/// Wait tfor a specified number of seconds
Online::KeyboardEmulator& Online::KeyboardEmulator::wait(int seconds)   {
  ::lib_rtl_sleep(seconds*1000);
  return *this;
}

int Online::KeyboardEmulator::done()  {
  hit_return(2);
  ::lib_rtl_sleep(2500);
  return 0;
}
