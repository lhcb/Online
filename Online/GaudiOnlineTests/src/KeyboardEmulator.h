//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
#ifndef GAUDIONLINETESTS_KEYBOARDEMULATOR_H
#define GAUDIONLINETESTS_KEYBOARDEMULATOR_H

/// C/C++ include files
#include <cstdio>
#include <vector>
#include <string>

/// Online namespace declaration
namespace Online {

  /// UPI test class
  class KeyboardEmulator   {
  private:
    FILE* input { nullptr };
    int scale   { 2 };
    
  public:
    /// Initializing constructor: open input pipe executing command
    KeyboardEmulator(const char* command);
    /// Default destructor
    ~KeyboardEmulator();
    /// Emit characters to terminal
    KeyboardEmulator& put_chars(const char* data);
    /// Emit characters to terminal
    KeyboardEmulator& put_chars(const std::vector<char>& data);
    /// Emit characters to terminal
    KeyboardEmulator& put_chars(const std::string& data);
    /// Hit an arbitrary key
    KeyboardEmulator& hit_key(int key);
    /// Hit an arbitrary key multiple times
    KeyboardEmulator& hit_key(std::size_t n, int key);
    /// Move the cursor DOWN repeatedly
    KeyboardEmulator& move_down(std::size_t n=1);
    /// Move the cursor UP repeatedly
    KeyboardEmulator& move_up(std::size_t n=1);
    /// Move the cursor LEFT repeatedly
    KeyboardEmulator& move_left(std::size_t n=1);
    /// Move the cursor RIGHT repeatedly
    KeyboardEmulator& move_right(std::size_t n=1);
    /// Hit key pad PERIOD repeatedly
    KeyboardEmulator& hit_period();
    /// Hit key pad ENTER key
    KeyboardEmulator& hit_enter();
    /// Hit key pad ENTER key multiple times
    KeyboardEmulator& hit_enter(std::size_t count);
    /// Move the cursor key pad CARRIAGE RETURN repeatedly
    KeyboardEmulator& hit_return();
    /// Move the cursor key pad CARRIAGE RETURN repeatedly multiple times
    KeyboardEmulator& hit_return(std::size_t count);
    /// Finalize test and close pipe
    int done();
    /// Wait tfor a specified number of seconds
    KeyboardEmulator& wait(int seconds);    
  };
}       // End namespace Online
#endif  // GAUDIONLINETESTS_KEYBOARDEMULATOR_H
