//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#include <RTL/raiseDebug.h>
#include <cstdio>

extern "C" int lib_rtl_signal_test(int, char**)
{
  ::printf("Starting...\n");
  RTL::raiseDebug();
  return 0;
}
