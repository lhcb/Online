//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================

#include <RTL/rtl.h>
#include <boost/asio.hpp>
#include <csignal>

using namespace std;
namespace asio = boost::asio;

extern "C" int asio_test_signals(int /* argc */, char** /* argv */)     {
  struct handler_t   {
    /// Timer handler
    void timer_handler(const boost::system::error_code& /*e*/)  {
      ::lib_rtl_output(LIB_RTL_ALWAYS,
		       "%p: +++ pid:%d Timer handler fired!",
		       (void*)this, ::lib_rtl_pid());
      ::kill(::lib_rtl_pid(), SIGTERM);
    }
    /// Signal handler
    void signal_handler(const boost::system::error_code&, int signum)  {
      ::lib_rtl_output(LIB_RTL_ALWAYS,
		       "%p: +++ Termination request received. Handling signal %s.",
		       (void*)this, RTL::signalName(signum).c_str());
    }
  } handler;

  asio::io_context        io_context;
  boost::asio::signal_set signals(io_context);

  signals.add(SIGINT);
  signals.add(SIGTERM);
#if defined(SIGQUIT)
  signals.add(SIGQUIT);
#endif // defined(SIGQUIT)
  boost::asio::steady_timer timer(io_context, boost::asio::chrono::seconds(5));

  signals.async_wait(std::bind(&handler_t::signal_handler, &handler, std::placeholders::_1, std::placeholders::_2));
  timer.async_wait(  std::bind(&handler_t::timer_handler,  &handler, std::placeholders::_1));
  io_context.run();
  return 0;
}
