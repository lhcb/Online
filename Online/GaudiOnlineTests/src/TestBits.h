//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================

/// Framework include files
#include <RTL/rtl.h>
#include <RTL/bits.h>

/// C/C++ include files
#include <cstdio>

extern "C" int BIT_TEST(int,char **) {
  int32_t  pos = 0, size = 15;
  int8_t   txt[32*sizeof(int32_t)];
  int32_t  bf_len = 8*sizeof(txt);

  ::memset(txt, 0, sizeof(txt));
  ::printf("Filled size: %ld bits\n", long(sizeof(txt)*8));
  BF_SET(txt, 20, 98);
  ::printf("BF_set(txt, 20, %d);\n", 96);
  BF_SET(txt, 835, 35);
  ::printf("BF_set(txt, 20, %d);\n", 96);

  BF_print(txt, sizeof(txt));

  for( std::size_t i=0; i<120; i+=4 )   {
    BF_COUNT(txt+i, bf_len-i * Bits::BITS_PER_BYTE, &pos, &size);
    ::printf("BFCount: start:%4ld pos:%4d size:%4d  %4ld\n",
	     i*Bits::BITS_PER_BYTE, pos, size, (i*Bits::BITS_PER_BYTE) + pos+size);
  }

  BF_FREE(txt, 21, 94);
  ::printf("BF_free(txt, 21, 94);\n");
  BF_FREE(txt, 835, 35);
  ::printf("BF_free(txt, 135, 35);\n");
  BF_print(txt, sizeof(txt));

  BF_ALLOC(txt, bf_len, 256, &pos);
  ::printf("BF_alloc(txt, ..., 256, %d);\n", pos);
  BF_print(txt, sizeof(txt));

  BF_ALLOC(txt, bf_len, 11, &pos);
  ::printf("BF_alloc(txt, bf_len, 11, %d);\n", pos);
  BF_print(txt, sizeof(txt));
  BF_FREE(txt, pos, 11);
  ::printf("BF_free(txt, %d, 11);\n", pos);
  BF_print(txt, sizeof(txt));

  BF_ALLOC(txt, bf_len, 11+32, &pos);
  ::printf("BF_alloc(txt, ..., 11+32, %d);\n", pos);
  BF_print(txt, sizeof(txt));

  BF_ALLOC(txt, bf_len, 32, &pos);
  ::printf("BF_alloc(txt, ..., 32, %d);\n", pos);
  BF_print(txt, sizeof(txt));
  return 0;
}
