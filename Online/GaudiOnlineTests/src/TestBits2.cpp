//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <RTL/rtl.h>
#include <RTL/bits.h>

namespace {
  struct Test {
    char* txt;
    int txt_len, bf_len;
    Test(char* buff, int txlen, int len) : txt(buff), txt_len(txlen), bf_len(len) {}
    int alloc(int len)  {
      int pos = 0;
      ::BF_alloc2(txt,bf_len,len,&pos);
      printf("BF_alloc(txt,%d,%d,%d);\n",bf_len,len,pos);
      ::BF_print(txt,txt_len);
      return pos;
    }
    void free(int pos, int len)  {
      ::BF_free2(&txt[0], pos, len);
      printf("BF_free(txt,%d,%d);\n",pos,len);
      ::BF_print(txt,txt_len);
    }
  };
}

extern "C" int rtl_testbits3(int,char **) {
  char txt[32*sizeof(int)];
  int  bf_len = 8*sizeof(txt);
  Test test(txt, sizeof(txt), bf_len);  

  ::memset(txt,0,sizeof(txt));
  ::printf("Filled size: %ld bits\n",long(sizeof(txt)*8));

  test.alloc(512);
  test.free(60,16);
  test.alloc(15);
  test.free(120,122);
  test.alloc(108);
  test.alloc(13);
#if 0
  test.free(test.alloc(11), 11);
  test.alloc(11+32);
  test.alloc(32);
  test.alloc(20);
  test.alloc(19);
  test.alloc(5*4*32);
#endif
  return 0;
}

#define BF_SET   BF_set2
#define BF_FREE  BF_free2
#define BF_ALLOC BF_alloc2
#define BF_COUNT BF_count2
#define BIT_TEST rtl_testbits2
#include "TestBits.h"
