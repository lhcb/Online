//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================

/// Framework include files
#include <EventData/RawFile.h>
#include <RTL/Compress.h>
#include <RTL/strdef.h>
#include <RTL/rtl.h>
#include "FileUtil.h"

/// C/C++ include files
#include <filesystem>
#include <sys/stat.h>
#include <fcntl.h>

#define CHUNK_SIZE 8192
using namespace Online;

namespace {

  /// Test routine to compress input file (args) to output file (args)
  int rtl_test_buffer_compression(int argc, char** argv, std::vector<uint8_t> (*compress)(const uint8_t*, std::size_t, int, int)) {
    RTL::CLI cli(argc, argv, []() {
      ::printf("rtl_test_<type>_buffer_compression -option [-option]    \n\n"
	       "       -input=<file-name>          Input file name        \n"
	       "       -output=<file-name>         Output file name       \n"
	       "       -strategy=<strategy>        Compression stratgegy  \n"
	       "                                   Technology dependent   \n"
	       "       -compression=<leve>         Compression level      \n"
	       "                                   Technology dependent   \n"
	       "\n" );
    });
    std::string input, output, debug;
    int strategy, compression;
    cli.getopt("debug",       5, debug);
    cli.getopt("input",       3, input);
    cli.getopt("output",      3, output);
    cli.getopt("strategy",    3, strategy = Online::compress::COMPRESSION_STRATEGY_DEFAULT);
    cli.getopt("compression", 3, compression = Online::compress::COMPRESSION_LEVEL_DEFAULT);
    if ( input.empty() || output.empty() )  {
      cli.call_help();
      ::exit(EINVAL);
    }
    Online::RawFile out(RTL::str_expand_env(output));
    if ( out.openWrite() > 0 )  {
      Online::RawFile in(RTL::str_expand_env(input));
      std::vector<uint8_t> buffer = util::load_file(in);
      if ( buffer.size() > 0 )   {
	std::vector<uint8_t> result = (*compress)(&buffer.at(0), buffer.size(), compression, strategy);
	if ( result.size() > 0 )   {
	  ::lib_rtl_output(LIB_RTL_ALWAYS,
			   "Compressed input file %s [0x%08lX bytes] to %s [0x%08lX compressed bytes]",
			   in.filename().c_str(), buffer.size(), out.filename().c_str(), result.size());
	  int iret = out.write(&result.at(0), result.size());
	  if ( iret < 0 )  {
	    ::lib_rtl_output(LIB_RTL_ALWAYS,"Failed to write output file %s iret=%d",
			     out.filename().c_str(), iret);
	  }
	  out.close();
	}
	else  {
	  ::lib_rtl_output(LIB_RTL_ALWAYS,
			   "FAILED to compress input buffer %s [0x%08lX bytes, no data]",
			   in.filename().c_str(), buffer.size());
	}
      }
      return 0;
    }
    ::lib_rtl_output(LIB_RTL_ERROR,"Failed to open output file: %s", output.c_str());
    return EINVAL;
  }
  
  /// Test routine to decompress input file (args) to output file (args)
  int rtl_test_buffer_decompression(int argc, char** argv, std::vector<uint8_t> (*decompress)(const uint8_t*, std::size_t)) {
    RTL::CLI cli(argc, argv, []() {
      ::printf("rtl_test_<type>_decompression -option [-option]         \n\n"
	       "       -input=<file-name>          Input file name        \n"
	       "       -output=<file-name>         Output file name       \n"
	       "       -source=<file-name>         Source file name       \n"
	       "             Source will be checked against deflated file.\n"
	       "\n" );
    });
    std::string input, output, source;
    cli.getopt("input",    3, input);
    cli.getopt("output",   3, output);
    cli.getopt("source",   3, source);
    if ( input.empty() || output.empty() )  {
      cli.call_help();
      ::exit(EINVAL);
    }
    Online::RawFile out(RTL::str_expand_env(output));
    if ( out.openWrite() > 0 )  {
      Online::RawFile in(RTL::str_expand_env(input));
      std::vector<uint8_t> buffer = util::load_file(in);
      if ( buffer.size() > 0 )   {
	std::vector<uint8_t> result = (*decompress)(&buffer.at(0), buffer.size());
	if ( result.size() > 0 )   {
	  ::lib_rtl_output(LIB_RTL_ALWAYS,
			   "Decompressed input file %s [0x%08lX bytes] to %s [0x%08lX compressed bytes]",
			   in.filename().c_str(), buffer.size(), out.filename().c_str(), result.size());
	  int iret = out.write(&result.at(0), result.size());
	  if ( iret < 0 )  {
	    ::lib_rtl_output(LIB_RTL_ALWAYS,"Failed to write output file %s iret=%d",
			     out.filename().c_str(), iret);
	  }
	  out.close();
	  if ( !source.empty() )   {
	    Online::RawFile src(RTL::str_expand_env(source));
	    return util::compare_buffers(out, src) == 0 ? 0 : EINVAL;
	  }
	  return 0;
	}
	else  {
	  ::lib_rtl_output(LIB_RTL_ALWAYS,
			   "FAILED to compressed input buffer %s [0x%08lX bytes]",
			   in.filename().c_str(), buffer.size());
	}
      }
    }
    ::lib_rtl_output(LIB_RTL_ERROR,"Failed to open output file: %s", out.cname());
    return EINVAL;
  }

  /// Test routine to compress input file (args) to output file (args)
  int rtl_test_compression(int argc, char** argv, Online::posix_t* descriptor) {
    RTL::CLI cli(argc, argv, []() {
      ::printf("rtl_test_<type>_compression -option [-option]           \n\n"
	       "       -input=<file-name>          Input file name        \n"
	       "       -output=<file-name>         Output file name       \n"
	       "       -strategy=<strategy>        Compression stratgegy  \n"
	       "                                   Technology dependent   \n"
	       "       -compression=<leve>         Compression level      \n"
	       "                                   Technology dependent   \n"
	       "\n" );
    });
    std::string input, output, strategy, compression = "3", debug;
    cli.getopt("debug",       5, debug);
    cli.getopt("input",       3, input);
    cli.getopt("output",      3, output);
    cli.getopt("strategy",    3, strategy);
    cli.getopt("compression", 3, compression);
    if ( input.empty() || output.empty() )  {
      cli.call_help();
      ::exit(EINVAL);
    }
    debug  = RTL::str_lower(debug);
    input  = RTL::str_expand_env(input);
    output = RTL::str_expand_env(output);
    Online::RawFile in(input);
    Online::RawFile out(output, descriptor);
    if ( in.open() > 0 )   {
      if ( out.openWrite() > 0 )  {
	int  iret, oret;
	std::size_t in_bytes = 0;
	std::size_t out_bytes = 0;
	char chunk[CHUNK_SIZE];
	in.set_option("READ_SIZE", std::to_string(4*1024*1024));
	out.set_option("WRITE_SIZE", std::to_string(4*1024*1024));
	if ( !strategy.empty() )    out.set_option("STRATEGY", strategy);
	if ( !compression.empty() ) out.set_option("COMPRESSION_LEVEL", compression);
	if ( debug.find("in")  != std::string::npos ) in.set_option("GLOBAL_DEBUG",  "YES");
	if ( debug.find("out") != std::string::npos ) out.set_option("GLOBAL_DEBUG", "YES");
	out.set_option("APPLY", "");
	do  {
	  iret = in.read(chunk, sizeof(chunk));
	  if ( iret > 0 )  {
	    in_bytes += iret;
	    oret = out.write(chunk, iret);
	    if ( oret > 0 )  {
	      out_bytes += oret;
	      continue;
	    }
	    ::lib_rtl_output(LIB_RTL_ALWAYS,"Failed to write output file %s iret=%d",
			     out.filename().c_str(), oret);
	    break;
	  }
	  
	  ::lib_rtl_output(LIB_RTL_ALWAYS,
			   "Failed to read input file %s (iret=%d) after a total of 0x%08X bytes read.",
			   in.filename().c_str(), iret, in_bytes);
	  break;
	} while ( iret > 0 );
	out.close();
	in.close();
	struct stat stat_buf;
	::stat(output.c_str(), &stat_buf);
	::lib_rtl_output(LIB_RTL_ALWAYS,
			 "Compressed input file %s [0x%08X bytes] to %s [ 0x%08X uncompressed / 0x%08X compressed bytes]",
			 in.filename().c_str(),  in_bytes,
			 out.filename().c_str(), out_bytes,
			 stat_buf.st_size);
	return 0;
      }
      ::lib_rtl_output(LIB_RTL_ERROR,"Failed to open output file: %s", output.c_str());
      return EINVAL;
    }
    ::lib_rtl_output(LIB_RTL_ERROR,"Failed to open input file: %s", input.c_str());
    return EINVAL;
  }
  
  /// Test routine to decompress input file (args) to output file (args)
  int rtl_test_decompression(int argc, char** argv, Online::posix_t* descriptor) {
    RTL::CLI cli(argc, argv, []() {
      ::printf("rtl_test_<type>_decompression -option [-option]         \n\n"
	       "       -input=<file-name>          Input file name        \n"
	       "       -output=<file-name>         Output file name       \n"
	       "       -source=<file-name>         Source file name       \n"
	       "             Source will be checked against deflated file.\n"
	       "\n" );
    });
    std::string input, output, source;
    cli.getopt("input",    3, input);
    cli.getopt("output",   3, output);
    cli.getopt("source",   3, source);
    if ( input.empty() || output.empty() )  {
      cli.call_help();
      ::exit(EINVAL);
    }
    input  = RTL::str_expand_env(input);
    output = RTL::str_expand_env(output);
    Online::RawFile in(input, descriptor);
    Online::RawFile out(output);
    if ( in.open() > 0 )   {
      if ( out.openWrite() > 0 )  {
	int  iret, oret;
	std::size_t in_bytes = 0;
	std::size_t out_bytes = 0;
	char chunk[CHUNK_SIZE];
	do  {
	  iret = in.read(chunk, sizeof(chunk));
	  if ( iret > 0 )  {
	    in_bytes += iret;
	    oret = out.write(chunk, iret);
	    if ( oret > 0 )  {
	      out_bytes += oret;
	      continue;
	    }
	    ::lib_rtl_output(LIB_RTL_ALWAYS,"Failed to write output file %s iret=%d after 0x%08X bytes total",
			     out.filename().c_str(), oret, out_bytes);
	    break;
	  }
	  ::lib_rtl_output(LIB_RTL_ALWAYS,
			   "Failed to read input file %s (iret=%d) after a total of 0x%08X bytes read.",
			   in.filename().c_str(), iret, in_bytes);
	  break;
	} while ( iret > 0 );
	out.close();
	in.close();
	struct stat stat_buf;
	::stat(input.c_str(), &stat_buf);
	::lib_rtl_output(LIB_RTL_ALWAYS,
			 "Decompressed input file %s [0x%08X uncompressed / 0x%08X compressed bytes] to %s with 0x%08X bytes.",
			 in.filename().c_str(),  in_bytes, stat_buf.st_size,
			 out.filename().c_str(), out_bytes);
	if ( !source.empty() )   {
	  Online::RawFile src(RTL::str_expand_env(source));
	  return util::compare_buffers(out, src) == 0 ? 0 : EINVAL;
	}
	return 0;
      }
      ::lib_rtl_output(LIB_RTL_ERROR,"Failed to open output file: %s", output.c_str());
      return EPERM;
    }
    ::lib_rtl_output(LIB_RTL_ERROR,"Failed to open input file: %s", input.c_str());
    return ENOENT;
  }
}

#define TEST_COMPRESSION(x)  extern "C"   {				\
  /* Compress file in buffer mode using x algorithm  */			\
  int rtl_test_buffer_compression_##x (int argc, char** argv) {		\
    return rtl_test_buffer_compression( argc, argv, Online::compress::compress_##x );    \
  }									\
  /* Decompress file in buffer mode using x algorithm  */		\
  int rtl_test_buffer_decompression_##x (int argc, char** argv) {	\
    return rtl_test_buffer_decompression(argc, argv, Online::compress::decompress_##x ); \
  }									\
  /* Compress file in stream mode using x algorithm  */			\
  int rtl_test_compression_##x (int argc, char** argv) {		\
    return rtl_test_compression(argc, argv, Online::compress::descriptor_##x ());        \
  }									\
  /* Decompress file in stream mode using x algorithm  */		\
  int rtl_test_decompression_##x (int argc, char** argv) {		\
    return rtl_test_decompression(argc, argv, Online::compress::descriptor_##x ());      \
  }									\
  }

#ifdef ONLINE_HAVE_ZLIB
TEST_COMPRESSION(zlib)
#endif
#ifdef ONLINE_HAVE_ZSTD
TEST_COMPRESSION(zstd)
#endif
#ifdef ONLINE_HAVE_LZMA
TEST_COMPRESSION(lzma)
#endif
#ifdef ONLINE_HAVE_LZ4
TEST_COMPRESSION(lz4)
#endif
