//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================

// Framework inclde files
#include "Util.h"
#include <RTL/rtl.h>
#include <RTL/strdef.h>

// C/C++ include files
#include <iostream>
//  
//
//==========================================================================
namespace   {

  /// Class to test the basi cfunctionality for the data transfer libraries
  /**
   *   Tests include the following technologies:
   *   -- Standard transfers with homegrown sockets
   *   -- Transfers using asio + tcpip
   *   -- Transfers using asio + unix sockets (IPC)
   *   -- Transfers using ZeroMQ
   *
   *   \author  M.Frank
   *   \version 1.0
   */
  class TestDataTransfer : public Online::util::MultiProcessTest  {
  public:
    std::string host       { "localhost" };
    std::string mount      { "/TDBDATA/XMLRPC" };
    int         port       { 0 };
    long        count      { 1000000 };
    long        print_freq { 100000 };
    bool        need_tan   { false };
    int         child_debug { 0 };
    RTL::Process* server   { nullptr };

  public:
    //==========================================================================
    TestDataTransfer(int argc, char** argv, int p=-1) : MultiProcessTest(argc, argv), port(p)   {
      RTL::CLI cli(argc, argv, [](){});
      this->port     = Online::util::generate_tan_port();
      this->need_tan = cli.getopt("tan", 3) != nullptr;
      this->lib_name = "libOnlineBase.so";
      this->command  = this->gentest_exe;
      cli.getopt("host",  4, this->host);
      cli.getopt("port",  4, this->port);
      cli.getopt("count", 4, this->count);
      cli.getopt("freq",  4, this->print_freq);
      cli.getopt("child_debug", 10, this->child_debug);
      ::setenv("DIM_DNS_NODE", this->host.c_str(), 1);
    }
    //==========================================================================
    virtual int exec_preamble() override  {
      if ( this->need_tan )   {
	auto [port, srv] = this->start_tan(this->logs.c_str());
	this->server = srv;
	this->server->wait_for(2000);
	if ( this->server->is_running() )  {
	  this->port = ::atol(std::getenv("TAN_PORT"));
	}
      }
      return this->do_wait(this->need_tan ? 3 : 0);
    }
    //==========================================================================
    virtual int exit_server(int)  override  {
      if ( this->server )   {
	auto* end = this->start_proc(this->test_name+"_TAN_shutdown",
				     {"tan_nameserver_shutdown", "-nowait",
				      "-node=" + this->host,
				      "-port=" + std::to_string(this->port)}, "stdout");
	return this->do_wait({end, this->server}, 0);
      }
      return 0;
    }
    //==========================================================================
    int execute(const std::string& receiver, const std::string& sender)   {
      std::string recv_utgid = this->test_name+"_Recv";
      std::string send_utgid = this->test_name+"_Send";
      auto* recv = this->start_proc(recv_utgid,
				    { receiver,
					"-print_freq=" + std::to_string(this->print_freq),
					"-name="       + recv_utgid,
					"-debug="      + std::to_string(this->child_debug) },
				    this->logs.c_str());
      this->do_wait(this->wait);
      recv->ensure(2,2000);
      auto* send = this->start_proc(send_utgid,
				    { sender, "-batch",
					"-print_freq=" + std::to_string(this->print_freq),
					"-turns="  + std::to_string(this->count),
					"-name="   + send_utgid,
					"-target=" + recv_utgid,
					"-debug="  + std::to_string(this->child_debug) },
				    this->logs.c_str() );
      this->wait = 1;
      if ( this->child_debug )  {
	::lib_rtl_output(LIB_RTL_ALWAYS,"Recv: %s", recv->bash_command().c_str());
	::lib_rtl_output(LIB_RTL_ALWAYS,"Send: %s", send->bash_command().c_str());
	this->do_wait(20000); // Wait a loooong time to attach debuggers
      }
      return this->do_wait({ send, recv }, 0);
    }
  };
}
//
//==========================================================================
extern "C" int rtl__test_transfer_asio_unix_ipc(int argc, char** argv)    {
  return complex_test<TestDataTransfer>(argc, argv, [](TestDataTransfer& test)   {
    return test.execute("test_asio_unix_ipc_recv", "test_asio_unix_ipc_send");
  });
}
//
//==========================================================================
extern "C" int rtl__test_transfer_asio_net(int argc, char** argv)    {
  return complex_test<TestDataTransfer>(argc, argv, [](TestDataTransfer& test) {
    return test.execute("test_asio_net_recv", "test_asio_net_send");
  });
}
//
//==========================================================================
extern "C" int rtl__test_transfer_socket(int argc, char** argv)    {
  return complex_test<TestDataTransfer>(argc, argv, [](TestDataTransfer& test) {
    return test.execute("test_socket_net_recv", "test_socket_net_send");
  });
}
//
//==========================================================================
extern "C" int rtl__test_transfer_ams(int argc, char** argv)    {
  return complex_test<TestDataTransfer>(argc, argv, [](TestDataTransfer& test) {
    return test.execute("test_ams_transfer_recv", "test_ams_transfer_send");
  });
}
//
//==========================================================================
extern "C" int rtl__test_transfer_zeromq(int argc, char** argv)    {
  return complex_test<TestDataTransfer>(argc, argv, [](TestDataTransfer& test) {
    std::string recv_utgid = "localhost::" + test.test_name + "_Recv";
    std::string send_utgid = "localhost::" + test.test_name + "_Send";
    auto* recv = test.start_proc(recv_utgid, test.gentest_exe, "libDataflowZMQ.so",
				 { "test_zmq_net_recv",
				   "-print_freq="+std::to_string(test.print_freq),
				   "-turns="+std::to_string(test.count),
				   "-name="+recv_utgid  }, "");
    test.do_wait(test.wait);
    auto* send = test.start_proc(send_utgid, test.gentest_exe, "libDataflowZMQ.so",
				 { "test_zmq_net_send", "-batch",
				   "-print_freq="+std::to_string(test.print_freq),
				   "-turns="+std::to_string(test.count),
				   "-name="+send_utgid,
				   "-target="+recv_utgid }, "");
    return test.do_wait({ send, recv }, 0);
  });
}
