//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================

// Framework inclde files
#include "Util.h"
#include <RTL/rtl.h>
#include <RTL/strdef.h>

// C/C++ include files
#include <iostream>
//  
//
//==========================================================================
namespace   {

  /// Class to test the GauchoBase functionality
  /**
   *   \author  M.Frank
   *   \version 1.0
   */
  class GauchoBaseTest : public Online::util::MultiProcessTest  {
  public:

  public:
    //==========================================================================
    GauchoBaseTest(int argc, char** argv) : MultiProcessTest(argc, argv)  {
      RTL::CLI cli(argc, argv, [](){});
      this->lib_name  = "libGaudiOnlineTests.so";
      this->test_name = "ID"+RTL::to_hex(dns_port, 4);
      ::setenv("DIM_DNS_NODE", this->dns_node.c_str(), 1);
    }
    //==========================================================================
    virtual int exit_server(int wait_seconds)  override  {
      this->do_wait(wait_seconds);
      return 1;
    }
    //==========================================================================
    RTL::Process* start_server()    {
      return nullptr;
    }
    //==========================================================================
    RTL::Process* start_client(const std::string& cmd,
			       const std::vector<std::string>& args)
    {
      if ( cmd.empty() || args.empty() )  {
	return nullptr;
      }
      return nullptr;
    }
    //==========================================================================
    virtual int exec_preamble() override  {
      return 0;
    }
  };
}
//
//==========================================================================
extern "C" int gauchobase_start_server_qmtest(int argc, char** argv)    {
  return complex_test<GauchoBaseTest>(argc, argv, [](GauchoBaseTest&) { return 0; });
}
