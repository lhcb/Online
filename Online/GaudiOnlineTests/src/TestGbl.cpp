//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
#include <fcntl.h>
#include <RTL/rtl.h>
#include <RTL/strdef.h>
#include <RTL/GlobalSection.h>

extern "C" int rtl_test_gbl(int argc, char** argv) {
  int msecs = 300;
  lib_rtl_lock_t id;
  char txt[132];
  int turns = 20;
  std::string proc = argc>1 ? std::string(argv[1]) : RTL::processName();
  ::snprintf(txt,sizeof(txt),"GBL_test_0x%08X",lib_rtl_pid());
  std::string name = txt;
  ::lib_rtl_unlink(("/dev/shm/sem."+name).c_str());
  ::lib_rtl_unlink(("/dev/shm/"+name).c_str());   {
    int status = ::lib_rtl_create_lock(name.c_str(), &id);
    if ( lib_rtl_is_success(status) )  {
      RTL::GlobalSection gbl(name,512, true);
      if ( gbl )  {
	char* buff = gbl.buffer();
	for( int i=0; i <turns; ++i )  {
	  ::lib_rtl_sleep(msecs);
	  ::lib_rtl_lock(id);
	  if ( i != 0 )   {
	    ::printf("%4d[%s] >> Read from shared memory: %s\n",i,proc.c_str(),buff);
	    if ( strcmp(buff,txt) != 0 )   {
	      ::printf("Found changed shared memory.....\n");
	    }
	  }
	  ::snprintf(txt,sizeof(txt),"%s (%s):%3d",name.c_str(),proc.c_str(),i);
	  ::strncpy(buff,txt,512);
	  {
	    ::printf("%4d[%s] >> Wrote to shared memory:  %s\n",i,proc.c_str(),buff);
	  }
	  ::lib_rtl_unlock(id);
	}
      }
      ::lib_rtl_delete_lock(id);
    }
  }
  return 0;
}
