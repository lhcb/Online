//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
#define MBM_IMPLEMENTATION

/// Framework include files
#include <RTL/rtl.h>
#include <MBM/bmdef.h>
#include <MBM/bmstruct.h>
#include <MBM/bmserver.h>
#include <RTL/netdef.h>
#include <RTL/strdef.h>
#include <RTL/DllAccess.h>
#include <RTL/ProcessGroup.h>

/// C/C++ incloude files
#include <ctime>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <stdexcept>

#include "Util.h"

using OnlineBase::currentCommand;

using namespace RTL;

extern "C" int mbm_remove(int argc, char** argv);
extern "C" int mbm_summary(int argc, char** argv);

namespace {

  /// Get random buffer names to allow for parallel tests
  std::string _bm_name_rndm()   {
    auto [sc, prt] = RTL::get_free_server_port();
    return "bm_0x" + RTL::to_hex(prt, 8);
  }

  /// Remove buffer manager
  void _bm_remove(const std::string& /* bm */)   {
    //std::string i_bm = "-i="+bm;
    //const char *a1[] = {"mbm_remove", i_bm.c_str(), 0};
    //::mbm_remove(2, (char**)a1);
  }
}

extern "C" int mbm_install_test_bm(int argc , char** argv) {
  ::lib_rtl_set_log_level(LIB_RTL_INFO);
  try  {
    std::vector<char*> opts;
    std::vector<ServerBMID> bmids;
    char type[64] = "mbm_install";
    ServerBMID bmid;
    for( std::size_t i=0; i<size_t(argc); ++i )  {
      char c0 = argv[i][0];
      char c1 = ::toupper(argv[i][1]);
      opts.push_back(argv[i]);
      if ( (c0 == '-' || c0 == '/') && (c1 == 'C' || c1 == 'A') ) {
        bmid = mbm_install_server(opts.size(), &opts[0]);
        if ( !bmid )  {
          ::lib_rtl_output(LIB_RTL_ERROR,"Unable to install MBM buffers...\n");
          throw std::runtime_error("Unable to install MBM buffers...");
        }
        bmids.push_back(bmid);
        opts.clear();
        opts.push_back(type);
      }
    }
    if ( !opts.empty() )  {
      //opts.push_front("dummy");
      RTL::CLI cli(opts.size(),&opts[0],0);
      int sleep = 10000;
      bool summary = cli.getopt("summary",3);
      cli.getopt("sleep",3,sleep);
      ::lib_rtl_sleep(sleep*1000);
      if ( summary )   {
	return mbm_summary(opts.size(),&opts[0]);
      }
    }
  }
  catch (std::exception& e)  {
    ::lib_rtl_output(LIB_RTL_ERROR,"++mbm_install++ MBM initialization failed: %s\n",e.what());
  }
  return MBM_ERROR;
}

extern "C" int rtl_mbm_install_qmtest(int argc, char** argv) {
  RTL::CLI cli(argc, argv, [] { ::exit(0); });
  std::string nam = _bm_name_rndm();
  cli.getopt("name", 4, nam);
  std::string id  = "-i="+nam;
  const char* av[] = {"mbm_install_test", "-s=8096", "-e=64", "-u=64", id.c_str(), "-c", 0};
  int         sleep_secs = 0;
  ServerBMID  bm = ::mbm_install_server(6, (char**)av);
  int status =  bm ? 0 : 1;

  cli.getopt("sleep", 5, sleep_secs);
  if ( 0 == status && sleep_secs > 0 )   {
    std::cout << "Sleeping for " << sleep_secs << " seconds...." << std::flush << std::endl;
    ::lib_rtl_sleep(1000*sleep_secs);
    std::cout << "....done" << std::endl;
  }
  _bm_remove(nam);
  return status;
}

extern "C" int rtl_mbm_simple_qmtest(int argc, char** /* argv */)  {
  ProcessGroup pg;
  std::string bm = _bm_name_rndm();
  std::string i_bm = "-i="+bm;
  std::string b_bm = "-b="+bm;
  std::string m_bm = "-match="+bm;
  Process* p[10] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
  const char* logs = argc>1 ? "" : "/dev/null";
  const char *a1[] = {"mbm_install_test_bm", "-s=8096", "-e=128", "-u=12", i_bm.c_str(), "-f", "-c", "-sleep=60",0};
  const char *a2[] = {"mbm_cons", "-name=cons_s_0", b_bm.c_str(), 0};
  const char *a4[] = {"mbm_cons", "-name=cons_o_0", "-one", b_bm.c_str(), 0};
  const char *a8[] = {"mbm_prod", "-name=prod_0", "-m=50000", "-s=3500", b_bm.c_str(), 0};

  std::string cmd(currentCommand());
  ::lib_rtl_set_log_level(LIB_RTL_INFO);
  //Process::setDebug(true);
  p[0] = pg.add(new Process("Installer",cmd,a1,logs));
  pg.start();
  ::lib_rtl_sleep(1500);
  std::cout << "Starting processes ..... " << std::endl;
  p[1] = pg.add(new Process("Cons_s_0",cmd,a2,logs));
  p[2] = pg.add(new Process("Cons_o_0",cmd,a4,logs));
  pg.start();
  ::lib_rtl_sleep(6000);
  Online::util::run_mbm_producer("Prod_0",cmd,a8,logs);
  Online::util::run_mbm_summary("Summary_0",cmd,{"mbm_summary", m_bm.c_str()});
  ::mbm_qmtest_check_start();
  ::lib_rtl_sleep(2000);
  pg.stop(2, 1);
  ::lib_rtl_sleep(1000);
  p[0]->stop();
  pg.wait(Process::WAIT_BLOCK);
  std::cout << "All processes finished work.. " << std::endl;
  pg.removeAll();
  _bm_remove(bm);
  return 0;
}

extern "C" int rtl_mbm_full_qmtest(int argc, char** /* argv */)  {
  ProcessGroup pg;
  std::vector<Process*> procs;
  std::string bm = _bm_name_rndm();
  std::string i_bm = "-i="+bm;
  std::string b_bm = "-b="+bm;
  std::string m_bm = "-match="+bm;
  const char* logs = argc>1 ? "" : "/dev/null";
  const char *a1[] = {"mbm_install_test_bm", "-s=8096", "-e=128", "-u=12", i_bm.c_str(), "-c", "-sleep=60",0};
  const char *a2[] = {"mbm_cons", "-name=cons_s_0",0};
  const char *a3[] = {"mbm_cons", "-name=cons_s_1",0};
  const char *a4[] = {"mbm_cons", "-name=cons_o_0", "-one", b_bm.c_str(), 0};
  const char *a5[] = {"mbm_cons", "-name=cons_o_1", "-one", b_bm.c_str(), 0};
  const char *a6[] = {"mbm_cons", "-name=cons_u_0", "-s=1", b_bm.c_str(), 0};
  const char *a7[] = {"mbm_cons", "-name=cons_u_1", "-s=1", b_bm.c_str(), 0};
  const char *a8[] = {"mbm_prod", "-name=prod_0", "-m=5000", "-s=3500", b_bm.c_str(), 0};
  const char *a9[] = {"mbm_summary", m_bm.c_str(), 0};

  std::string cmd(currentCommand());

  ::lib_rtl_set_log_level(LIB_RTL_INFO);
  //Process::setDebug(true);
  procs.reserve(10);
  auto* srv = new Process("Install", cmd, a1, logs);
  pg.add(srv);
  pg.start();
  ::lib_rtl_sleep(1500);
  procs.emplace_back(pg.add(new Process("Cons_s_0", cmd, a2, logs)));
  procs.emplace_back(pg.add(new Process("Cons_s_1", cmd, a3, logs)));
  procs.emplace_back(pg.add(new Process("Cons_o_0", cmd, a4, logs)));
  procs.emplace_back(pg.add(new Process("Cons_o_1", cmd, a5, logs)));
  procs.emplace_back(pg.add(new Process("Cons_u_0", cmd, a6, logs)));
  procs.emplace_back(pg.add(new Process("Cons_u_1", cmd, a7, logs)));
  std::cout << "Starting processes ..... " << std::endl;
  pg.start();

  ::lib_rtl_sleep(12000);
  Online::util::run_mbm_producer("Prod_0", cmd, a8, logs);
  pg.wait_for(procs, 12000);

  Online::util::run_mbm_summary("Summary_0", cmd, a9);
  ::mbm_qmtest_check_start();

  ::lib_rtl_sleep(3000);
  pg.stop(6, 1);
  ::lib_rtl_sleep(1000);
  srv->stop();
  pg.wait(Process::WAIT_BLOCK);
  std::cout << "All processes finished work.. " << std::endl;
  pg.removeAll();
  return 0;
}


extern "C" int rtl_mbm_one_qmtest(int argc, char** argv)  {
  ProcessGroup pg;
  RTL::CLI cli(argc, argv, []() {});
  std::vector<Process*> procs;
  std::string bm = _bm_name_rndm();
  std::string i_bm  = "-i="+bm;
  std::string b_bm  = "-b="+bm;
  std::string m_bm  = "-match="+bm;
  const char* logs  = argc>1 ? "" : "/dev/null";
  const char *a1[]  = {"mbm_install_test_bm", "-s=8096", "-e=128", "-u=20", i_bm.c_str(), "-c", "-sleep=60",0};
  const char *a2[]  = {"mbm_cons", "-name=cons_v_0",  b_bm.c_str(), 0};
  const char *a3[]  = {"mbm_cons", "-name=cons_v_1",  b_bm.c_str(), 0};
  const char *a4[]  = {"mbm_cons", "-name=cons_uo_0", b_bm.c_str(), "-user_one", "-inc=0", "-s=10", 0};
  const char *a5[]  = {"mbm_cons", "-name=cons_uo_1", b_bm.c_str(), "-user_one", "-inc=0", "-s=3", 0};
  const char *a6[]  = {"mbm_cons", "-name=cons_UO_0", b_bm.c_str(), "-user_one", "-inc=1", 0};
  const char *a7[]  = {"mbm_cons", "-name=cons_UO_1", b_bm.c_str(), "-user_one", "-inc=1", 0};
  const char *a8[]  = {"mbm_cons", "-name=cons_o_0",  b_bm.c_str(), "-one",      "-inc=2", 0};
  const char *a9[]  = {"mbm_cons", "-name=cons_o_1",  b_bm.c_str(), "-one",      "-inc=2", 0};
  const char *a10[] = {"mbm_cons", "-name=cons_u_0",  b_bm.c_str(), "-usermode", 0};
  const char *a11[] = {"mbm_cons", "-name=cons_u_1",  b_bm.c_str(), "-usermode", 0};
  const char *a12[] = {"mbm_cons", "-name=cons_u_2",  b_bm.c_str(), "-usermode", "-s=2", 0};
  const char *a13[] = {"mbm_cons", "-name=cons_u_3",  b_bm.c_str(), "-usermode", "-s=2", 0};
  const char *a14[] = {"mbm_prod", "-name=prod_0",    b_bm.c_str(), "-m=50000",  "-s=3500", 0};
  const char *a15[] = {"mbm_summary", m_bm.c_str(), 0};

  std::string cmd(currentCommand());

  ::lib_rtl_set_log_level(LIB_RTL_INFO);
  //Process::setDebug(true);
  procs.reserve(10);
  auto* srv = new Process("Install", cmd, a1, logs);
  pg.add(srv);
  pg.start();
  ::lib_rtl_sleep(1500);
  procs.emplace_back(pg.add(new Process("Cons_v_0",  cmd,  a2, logs)));
  procs.emplace_back(pg.add(new Process("Cons_v_1",  cmd,  a3, logs)));
  procs.emplace_back(pg.add(new Process("Cons_uo_0", cmd,  a4, logs)));
  procs.emplace_back(pg.add(new Process("Cons_uo_1", cmd,  a5, logs)));
  procs.emplace_back(pg.add(new Process("Cons_UO_0", cmd,  a6, logs)));
  procs.emplace_back(pg.add(new Process("Cons_UO_1", cmd,  a7, logs)));
  procs.emplace_back(pg.add(new Process("Cons_o_0",  cmd,  a8, logs)));
  procs.emplace_back(pg.add(new Process("Cons_o_1",  cmd,  a9, logs)));
  procs.emplace_back(pg.add(new Process("Cons_u_0",  cmd, a10, logs)));
  procs.emplace_back(pg.add(new Process("Cons_u_1",  cmd, a11, logs)));
  procs.emplace_back(pg.add(new Process("Cons_u_2",  cmd, a12, logs)));
  procs.emplace_back(pg.add(new Process("Cons_u_3",  cmd, a13, logs)));
  std::cout << "Starting processes ..... " << std::endl;
  pg.start();

  ::lib_rtl_sleep(5000);
  procs.emplace_back(pg.add(new Process("Prod_0", cmd, a14, logs)));
  pg.start();
  while(1) ::sleep(1);

  pg.wait_for(procs, 5000);

  Online::util::run_mbm_summary("Summary_0", cmd, a15);
  ::mbm_qmtest_check_start();

  ::lib_rtl_sleep(3000);
  pg.stop(6, 1);
  ::lib_rtl_sleep(1000);
  srv->stop();
  pg.wait(Process::WAIT_BLOCK);
  std::cout << "All processes finished work.. " << std::endl;
  pg.removeAll();
  return 0;
}

int mbm_qmtest_check_no_active_buffers(int, char**)   {
  lib_rtl_gbl_t handle = 0;
  int res = ::mbmsrv_map_global_buffer_info(&handle, true);
  if ( lib_rtl_is_success(res) ) {
    BUFFERS* buffers = (BUFFERS*)handle->address;
    std::time_t start = ::time(0), now;
    int bad = 0;
  rescan:
    bad = 0;
    now = ::time(0);
    for (int i = 0; i < buffers->p_bmax; ++i)  {
      if ( buffers->buffers[i].used == 1 )  {
	if ( now - start > 400 ) {
	  ::printf("MBM: Buffer %s is still in use!\n",buffers->buffers[i].name);
	  ++bad;
	}
      }
    }
    if ( (bad > 0) && (now - start > 400) ) {
      ::printf("MBM: Buffers are still not released after 400 seconds!\n");
      ::mbmsrv_unmap_global_buffer_info(handle,false);
      ::exit(0);
    }
    else if ( bad > 0 )  {
      ::lib_rtl_sleep(1000);
      goto rescan;
    }
    res = 1;
    ::mbmsrv_unmap_global_buffer_info(handle,false);
  }
  else   {
    ::printf("MBM: Failed to map global buffer: ret=%d!\n", res);
  }
  return res;
}

extern "C" int mbm_qmtest_check_start() {
  std::string cmd = currentCommand() + " mbm_qmtest_check_no_active_buffers";
  pid_t pid = ::fork();
  if ( pid == 0 )  {  // Child
    std::string curr_cmd = currentCommand();
    const char* args[] = {curr_cmd.c_str(), 
			  "mbm_qmtest_check_no_active_buffers",
			  nullptr};
    ::execvpe(currentCommand().c_str(),(char**)args, ::environ);
    ::printf("MBM: Failed to execve check process: %s",RTL::errorString(errno).c_str());
    ::exit(errno);
  }
  else if ( pid < 0 )   {
    ::printf("MBM: Failed to check process: %s",RTL::errorString(errno).c_str());
  }
  return 0;
}
