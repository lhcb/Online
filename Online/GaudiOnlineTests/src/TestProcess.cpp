//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : OnlineKernel
//
//  Author     : Markus Frank
//==========================================================================
// Include files
#include <RTL/ProcessGroup.h>

#include <cmath>
#include <ctime>
#include <cstdio>
#include <signal.h>
#include "RTL/rtl.h"
#include "RTL/Logger.h"
#ifndef _WIN32
#include <sys/wait.h>
#endif
#include <iostream>
#include <sstream>
#include <csignal>
#include <cerrno>

#include "RTL/DllAccess.h"

extern "C" int rtl_test_process_group(int, char** ) {
  RTL::Process *p2;
  std::stringstream str;
  const char *a1[] = {"5",0}, *a2[]={"10",0}, *a3[]={"15",0};
  ::lib_rtl_set_log_level(LIB_RTL_INFO);
  RTL::ProcessGroup pg;
  pg.add(   new RTL::Process("SLEEPER_1","/bin/sleep",a1));
  pg.add(p2=new RTL::Process("SLEEPER_2","/bin/sleep",a2));
  pg.add(   new RTL::Process("SLEEPER_3","/bin/sleep",a3));
  time_t start = ::time(0);
  pg.start();
  RTL::Process::setDebug(true);
  ::lib_rtl_sleep(200);
  str << "Stop process " << p2->name() << std::endl;
  p2->stop();
  ::lib_rtl_sleep(200);
  pg.wait();
  pg.removeAll();
  time_t stop = ::time(0);
  if ( ::fabs(float(stop-start)) > 14E0 ) {
    str << "All processes ended" << std::endl;
  }
  else {
    str << "Subprocess execution failed." << std::endl;
  }
  ::lib_rtl_output(LIB_RTL_INFO,str.str().c_str());
  return 0;
}

// Dummy task executed by next test (rather than the stupid "sleep"
extern "C" int rtl_test_process_sleep(int argc, char** argv) {
  const char* nam = ::lib_rtl_getenv("UTGID");
  class sleep_handler   {
  public:
    static void _handler(int num)   {
      if ( num == SIGTERM ) ::_exit(0);
      if ( num == SIGINT  ) ::_exit(0);
      if ( num == SIGQUIT ) ::_exit(0);
    }
  };

  ::signal(SIGTERM, sleep_handler::_handler);
  if ( 0 == nam )
    nam = "Unknown";
  if ( argc > 1 ) {
    int msec;
    ::sscanf(argv[1], "%d", &msec);
    ::lib_rtl_sleep(1000*msec);
    ::printf("%-12s Process starting...\n", nam);
    ::printf("%-12s arg0:%s\n", nam, argv[0]);
    ::printf("%-12s arg1:%s\n", nam, argv[1]);
    ::printf("%-12s Process sleeping for %d seconds.\n", nam, msec);
    ::printf("%-12s Process exiting....\n", nam);
    return 0;
  }
  ::printf("%-12s Process exiting....  [WRONG ARGUMENTS]\n", nam);
  return 0;
}

extern "C" int rtl_test_sub_processes(int, char** ) {
  RTL::Process *p2, *p4, *p6;
  std::stringstream str;
  const char *a1[] = {"rtl_test_process_sleep", "4", 0};
  const char *a2[] = {"rtl_test_process_sleep", "5", 0};
  const char *a3[] = {"rtl_test_process_sleep", "6", 0};
  const char *a4[] = {"rtl_test_process_sleep", "7", 0};
  const char *a5[] = {"rtl_test_process_sleep", "8", 0};
  const char *a6[] = {"rtl_test_process_sleep", "9", 0};

  RTL::ProcessGroup pg;
  ::lib_rtl_set_log_level(LIB_RTL_INFO);
  std::string cmd(OnlineBase::currentCommand());
  ::lib_rtl_signal_log(false);
  RTL::Process::setDebug(true);
  pg.add(   new RTL::Process("SLEEPER_0xFEED0001",cmd.c_str(),a1,"/dev/null"));
  pg.add(p2=new RTL::Process("SLEEPER_0xFEED0002",cmd.c_str(),a2));
  pg.start();
  pg.add(   new RTL::Process("SLEEPER_0xFEED0003",cmd.c_str(),a3,"/dev/null"));
  pg.add(p4=new RTL::Process("SLEEPER_0xFEED0004",cmd.c_str(),a4));
  pg.start();
  pg.add(   new RTL::Process("SLEEPER_0xFEED0005",cmd.c_str(),a5,"/dev/null"));
  pg.add(p6=new RTL::Process("SLEEPER_0xFEED0006",cmd.c_str(),a6));
  pg.start();
  time_t start = ::time(0);
  ::lib_rtl_sleep(4000);
  str.str("");
  str << "Stop process " << p2->name() << std::endl;
  ::lib_rtl_output(LIB_RTL_INFO,str.str().c_str());
  p2->stop();
  ::lib_rtl_sleep(100);
  str.str("");
  str << "Stop process " << p4->name() << std::endl;
  ::lib_rtl_output(LIB_RTL_INFO,str.str().c_str());
  p4->stop();
  ::lib_rtl_sleep(100);
  str.str("");
  str << "Stop process " << p6->name() << std::endl;
  ::lib_rtl_output(LIB_RTL_INFO,str.str().c_str());
  p6->stop();
  ::lib_rtl_sleep(15000);
  pg.wait();
  pg.removeAll();
  time_t stop = ::time(0);
  str.str("");
  if ( ::fabs(float(stop-start)) < 30E0 )
    str << "All processes ended" << std::endl;
  else
    str << "Subprocess execution failed:" << ::fabs(float(stop-start)) << "seconds." << std::endl;
  ::lib_rtl_output(LIB_RTL_INFO,str.str().c_str());
  return 0;
}
