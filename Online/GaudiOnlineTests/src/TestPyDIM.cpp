//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================

// Framework inclde files
#include "Util.h"
#include <RTL/rtl.h>
#include <RTL/strdef.h>

// C/C++ include files
#include <iostream>
//  
//
//==========================================================================
namespace   {

  /// Class to test the PyDIM functionality
  /**
   *   \author  M.Frank
   *   \version 1.0
   */
  class PyDIMTest : public Online::util::MultiProcessTest  {
  public:
    std::string option, counter_arg;
    int count = {-1};
    RTL::Process* server { nullptr };

  public:
    //======================================================================
    PyDIMTest(int argc, char** argv) : MultiProcessTest(argc, argv)   {
      RTL::CLI cli(argc, argv, [](){});
      cli.getopt("option", 6, this->option);
      cli.getopt("count",  5, this->count);
      this->counter_arg = "--counter=" + std::to_string(count);
      this->wait = 5;
    }
    //======================================================================
    std::string utgid(const std::string process_name)  const   {
      return "PyDIM_" + this->test_name + process_name;
    }
    //======================================================================
    virtual int exit_server(int wait_seconds)  override  {
      this->stop_dns(this->server, wait_seconds);
      this->server = nullptr;
      return 1;
    }
    //======================================================================
    RTL::Process* run_python(const std::string& process_name,
			     const std::vector<std::string>& args,
			     const std::string& log="",
			     const std::string& input="")   {
      auto call_args = args;
      auto file = "${GAUDIONLINETESTSROOT}/../PyDIM/tests/"+args[0];
      call_args[0] = RTL::str_expand_env(file);
      return start_exe(this->utgid(process_name), "python", call_args, log.c_str(), input.c_str());
    }
    //======================================================================
    virtual int exec_preamble() override  {
      this->server = this->start_dns("/dev/null");
      return 0;
    }
    //======================================================================
  };
}
//
//==========================================================================
extern "C" int pydim_test_start_dns(int argc, char** argv)    {
  return complex_test<PyDIMTest>(argc, argv, [](PyDIMTest&) -> int { return 0; });
}
//
//==========================================================================
extern "C" int pydim_test_dis_interface(int argc, char** argv)    {
  return complex_test<PyDIMTest>(argc, argv, [](PyDIMTest& test) -> int {
    test.do_wait(8);
    test.check_dns(test.server);
    auto client = test.run_python("test", { "test_dis_interface.py" }, "dis_interface.log");
    return test.do_wait(client, 20);
  });
}
//
//==========================================================================
extern "C" int pydim_test_dimc_cmnd(int argc, char** argv)    {
  return complex_test<PyDIMTest>(argc, argv, [](PyDIMTest& test) -> int {
    test.do_wait(8);
    test.check_dns(test.server);
    auto client = test.run_python("cmnd_test", { "test_dimc_cmnd.py", test.counter_arg }, "dimc_cmnd.log");
    return test.do_wait(client, 40);
  });
}
//
//==========================================================================
extern "C" int pydim_test_dimc_service(int argc, char** argv)    {
  return complex_test<PyDIMTest>(argc, argv, [](PyDIMTest& test) -> int {
    test.do_wait(12);
    test.check_dns(test.server);
    auto service = test.run_python("server", { "test_dimc_service.py", "Server" }, "Server.log");
    auto client  = test.run_python("client", { "test_dimc_service_client.py", test.counter_arg }, "Client.log");
    auto ret = test.do_wait({ client, service }, 40);
    service->stop();
    return ret;
  });
}
//
//==========================================================================
extern "C" int pydim_test_pydim_rpc(int argc, char** argv)    {
  return complex_test<PyDIMTest>(argc, argv, [](PyDIMTest& test) -> int {
    test.do_wait(12);
    test.check_dns(test.server);
    auto client = test.run_python("rpc_test", { "test_dimRpc.py", test.counter_arg });
    return test.do_wait(client, 30);
  });
}
//
//==========================================================================
extern "C" int pydim_test_pydim_rpcinfo(int argc, char** argv)    {
  return complex_test<PyDIMTest>(argc, argv, [](PyDIMTest& test) -> int {
    test.do_wait(12);
    test.check_dns(test.server);
    auto client = test.run_python("rpcinfo_test", { "test_dimRpcInfo.py" });
    return test.do_wait(client,30);
  });
}
//
//==========================================================================
extern "C" int pydim_test_services(int argc, char** argv)    {
  return complex_test<PyDIMTest>(argc, argv, [](PyDIMTest& test) -> int {
    test.do_wait(15);
    test.check_dns(test.server);
    auto client = test.run_python("Client", { "services-client.py",
					     "--name=Server",
					     "--wait=2"}, "Client.log");
    if ( !client )  {
      ::lib_rtl_output(LIB_RTL_ERROR,"++ FAILED to create PyDIM client.");
    }
    test.do_wait(client, 10);
    auto server = test.run_python("Server", { "services-server.py",
					     "--name=Server",
					     "--count="+std::to_string(test.count),
					     "--wait=10"}, "Server.log");
    if ( !server )  {
      ::lib_rtl_output(LIB_RTL_ERROR,"++ FAILED to create PyDIM server.");
    }
    if ( !server->is_running() )   {
      ::lib_rtl_output(LIB_RTL_ERROR,"++ FAILED to start PYDIM server %s.",
		       server->name().c_str());
    }
    return test.do_wait({client, server}, 30);
  });
}
//
//==========================================================================
extern "C" int pydim_test_commands(int argc, char** argv)    {
  return complex_test<PyDIMTest>(argc, argv, [](PyDIMTest& test) -> int {
    test.do_wait(15);
    test.check_dns(test.server);
    auto server = test.run_python("Server",
				  { "commands-server.py","--name=Server"},
				  "Server.log");
    auto client =
      test.run_python("Client",
		      { "commands-client.py","--name=Server","--wait=20"},
		      "Client.log",
		      "${GAUDIONLINETESTSROOT}/tests/options/pydim_commands.stdin");
    test.do_wait(server, 15);
    if ( !server )  {
      ::lib_rtl_output(LIB_RTL_ERROR,"++ FAILED to create PyDIM server.");
    }
    if ( !server->is_running() )   {
      ::lib_rtl_output(LIB_RTL_ERROR,"++ FAILED to start PYDIM server %s.",
		       server->name().c_str());
    }
    if ( !client )  {
      ::lib_rtl_output(LIB_RTL_ERROR,"++ FAILED to create PyDIM client.");
    }
    return test.do_wait({client, server}, 30);
  });
}
