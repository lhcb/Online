//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : GaudiOnlineTests
//
//  Author     : Markus Frank
//==========================================================================

// Framework inclde files

// C/C++ include files
#include <iostream>

extern "C" int qmtest_filter_stderr_output(int /* argc */, char** /* argv */)    {
  const char msg[] = 
    "../Online/OnlineBase/src/WT/wtlib.cpp:495:46: runtime error: pointer index expression with base 0xffffffffffffffc0 overflowed to 0x00000217ac10\n"
    "    #0 0x7fedb629bd2a in _wtc_find_facility ../Online/OnlineBase/src/WT/wtlib.cpp:495\n"
    "    #1 0x7fedb629e4e5 in wtc_wait_with_mask ../Online/OnlineBase/src/WT/wtlib.cpp:319\n"
    "    #2 0x7fedb629ec24 in wtc_wait ../Online/OnlineBase/src/WT/wtlib.cpp:450\n"
    "    #3 0x7fedb6047208 in CPP::Sensor::run() ../Online/OnlineBase/src/CPP/Sensor.cpp:103\n"
    "    #4 0x7fedb789b49a in taskdb_cache_server ../Online/PcSrv/src/CacheDBSrv.cpp:535\n"
    "    #5 0x7fedb789b601 in run_pcsrv_cachedb ../Online/PcSrv/src/CacheDBSrv.cpp:541\n"
    "    #6 0x401360 in main ../Online/OnlineBase/main/gentest.cpp:32\n"
    "    #7 0x7fedb7a8d554 in __libc_start_main ../csu/libc-start.c:266\n"
    "    #8 0x4011b8  (/workspace/build/Online/InstallArea/x86_64_v2-centos7-gcc12-dbg+ubsan/bin/gentest+0x4011b8)\n";
  const char lin[]  = "===============================================================================================\n";
  const char hdr[]  = "====      Testing stderr filtering in Online qmtest                                        ====\n";
  const char end[]  = "====      Test  finished  successfully.                                                    ====\n";
  const char out[]  = "====      Output written to stdout:                                                        ====\n";
  const char err[]  = "====      Output written to stderr:                                                        ====\n";

  std::cout << lin << hdr << lin;
  for( std::size_t i=0; i < 10; ++i )   {
    std::cout << lin << out << msg;
    std::cout << err;
    std::cerr << msg;
    std::cout << lin;
  }
  std::cout << end << lin;
  return 0;
}
