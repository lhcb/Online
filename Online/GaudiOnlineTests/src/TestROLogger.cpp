//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================

// Framework inclde files
#include "Util.h"
#include <dim/dis.h>
#include <RTL/rtl.h>
#include <RTL/strdef.h>

// C/C++ include files
#include <stdexcept>
#include <iostream>
#include <vector>
#include <dlfcn.h>
//  
//
//==========================================================================
namespace   {

  /// Class to test the ROLogger functionality
  /**
   *   \author  M.Frank
   *   \version 1.0
   */
  class ROLoggerTest : public Online::util::MultiProcessTest  {
  public:
    std::string   logger   { };
    std::string   host     { };
    int           tag      { 0 };
    int           count    { 0 };
    RTL::Process* server   { nullptr };

  public:
    //==========================================================================
    ROLoggerTest(int argc, char** argv) : MultiProcessTest(argc, argv)   {
      RTL::CLI cli(argc, argv, [](){});
      this->lib_name  = "libROLogger.so";
      cli.getopt("logger",   4, this->logger);
      cli.getopt("tag",      2, this->tag);
      cli.getopt("host",     4, this->host);
      cli.getopt("count",    4, this->count);
      if ( this->tag == 0 )
	this->tag = Online::util::get_unique_port_number(0x0);

      this->test_name = "ID"+RTL::to_HEX(this->tag, 4);
      if ( !host.empty() )  {
	::setenv("DIM_DNS_NODE", this->host.c_str(), 1);
	::setenv("DIM_HOST_NODE", this->host.c_str(), 1);
	RTL::RTL_reset();
      }
    }
    //==========================================================================
    int reopen_fifo_logs()   {
      struct fifolog  {
	/// Initialize the logger unit
	typedef void (*initialize_logger_t)();
	initialize_logger_t initialize_logger  { nullptr };
	/// Set fifo value (Only possible BEFORE startup (return=1), otherwise returns NULL)
	typedef int  (*set_fifo_t)(const char* fifo);
        set_fifo_t set_fifo  { nullptr };
	/// Set new utgid value
	typedef void (*set_utgid_t)(const char* new_utgid);
	set_utgid_t set_utgid  { nullptr };
	/// Set new tag value
	typedef void (*set_tag_t)(const char* new_tag);
	set_tag_t set_tag  { nullptr };
	/// Print a message
	typedef void (*print_t)(int level, const char* source, const char* message);
	print_t print  { nullptr };
	/// Shutdown the logger unit
	typedef void (*finalize_logger_t)();
	finalize_logger_t finalize_logger  { nullptr };
	
	fifolog()  {
	  void *handle = ::dlopen (NULL, RTLD_LAZY);
	  if ( !handle )   {
	    throw std::runtime_error(std::string("dlopen failed: ")+dlerror());
	  }
	  initialize_logger = (initialize_logger_t)::dlsym(handle, "fifolog_initialize_logger");
	  if ( nullptr == this->initialize_logger ) throw std::runtime_error("Missing function: fifolog_initialize_logger");
	  this->set_fifo          = (set_fifo_t)         ::dlsym(handle, "fifolog_set_fifo");
	  if ( nullptr == this->set_fifo ) throw std::runtime_error("Missing function: fifolog_set_fifo");
	  this->set_utgid         = (set_utgid_t)        ::dlsym(handle, "fifolog_set_utgid");
	  if ( nullptr == this->set_utgid ) throw std::runtime_error("Missing function: fifolog_set_utgid");
	  this->set_tag           = (set_tag_t)          ::dlsym(handle, "fifolog_set_tag");
	  if ( nullptr ==this-> set_tag ) throw std::runtime_error("Missing function: fifolog_set_tag");
	  this->finalize_logger   = (finalize_logger_t)  ::dlsym(handle, "fifolog_finalize_logger");
	  if ( nullptr == this->finalize_logger ) throw std::runtime_error("Missing function: fifolog_finalize_logger");
	}
      } fifo;
      ::setenv("LOGFIFO", this->logger.c_str(), 1);
      fifo.finalize_logger();
      fifo.set_tag(this->test_name.c_str());
      fifo.set_fifo(this->logger.c_str());
      int max_retries = 5;
      int retry = 0;
      do {
	++retry;
	try {
	  fifo.initialize_logger();
	}
	catch(const std::exception& e)   {
	  ::lib_rtl_sleep(1500);
	  if ( retry <= max_retries )  {
	    ::lib_rtl_output(LIB_RTL_ALWAYS,
			     "%s [pid:%d] ++ NodeLoggerFeed: retry %d to open fifo logger.",
			     this->test_name.c_str(), ::lib_rtl_pid(), retry);
	  }
	  continue;
	}
	break;
      } while (retry <= max_retries);
      return 1;
    }
    //==========================================================================
    void feed_log_messages()   {
      ::lib_rtl_output(LIB_RTL_ALWAYS,
		       "%s ++ NodeLoggerFeed with pid:%d starting up... log-fifo: %s",
		       this->test_name.c_str(), ::lib_rtl_pid(), this->logger.c_str());
      for (int i = 0; i < this->count; ++i )
	::lib_rtl_output(LIB_RTL_ALWAYS, "%s ++ NodeLoggerFeed: This is message number %d",
			 this->test_name.c_str(), i);
      ::lib_rtl_output(LIB_RTL_ALWAYS,
		       "%s ++ NodeLoggerFeed with pid:%d finished [%d messages produced].",
		       this->test_name.c_str(), ::lib_rtl_pid(), this->count);
      ::fflush(stdout);
    }
    //==========================================================================
    virtual int exit_server(int wait_seconds)  override  {
      if ( this->server )   {
	this->server->stop();
	this->server = nullptr;
      }
      this->do_wait(wait_seconds);
      return 1;
    }
    //==========================================================================
    RTL::Process* start_server()    {
      std::string utgid = "/" + this->test_name + "/LOGS";
      this->server = 
	this->start_proc(utgid,
			 {"run_dim_publish_logger",
			  "-I",
			  "-broker", "/dev/shm/logs_"+this->test_name+".dev",
			  "-create", "-p",
			  "-i", "2000"
			  "-DNS", "localhost",
			  "-name", this->test_name.c_str(),
			  "-Q", "LOGS",
			  "-echo"
			 });
      return this->server;
    }
    //==========================================================================
    virtual int exec_preamble() override  {
      //this->start_server();
      return this->do_wait(1);
    }
  };
}
//
//
//==========================================================================
extern "C" int rologger_logfeed_qmtest(int argc, char** argv)    {
  return complex_test<ROLoggerTest>(argc, argv, [](ROLoggerTest& test) {
    ::dis_start_serving(RTL::processName().c_str());
    test.reopen_fifo_logs();
    ::lib_rtl_output(LIB_RTL_ALWAYS,
		     "%s ++ NodeloggerFeed with pid:%d fully initialized",
		     test.test_name.c_str(), ::lib_rtl_pid());
    ::lib_rtl_sleep(2000);
    test.feed_log_messages();
    ::lib_rtl_sleep(8000);
    return 0;
  });
}
//
//
//==========================================================================
extern "C" int rologger_basic_server_qmtest(int argc, char** argv)    {
  return complex_test<ROLoggerTest>(argc, argv, [](ROLoggerTest& test) {
    // First start server
    test.start_server();
    test.do_wait(1);
    // No execute the feeder:
    test.reopen_fifo_logs();
    ::lib_rtl_output(LIB_RTL_ALWAYS,
		     "%s ++ NodeloggerFeed with pid:%d fully initialized",
		     test.test_name.c_str(), ::lib_rtl_pid());
    ::lib_rtl_sleep(2000);
    test.feed_log_messages();
    ::lib_rtl_sleep(8000);
    return 0;
  });
}
#if 0
//
//==========================================================================
extern "C" int rologger_connection_qmtest(int argc, char** argv)    {
  return complex_test<ROLoggerTest>(argc, argv, [](ROLoggerTest& test)  {
    auto* client = test.start_client("rologger_test_connection", { "-show", "-loop="+test.option });
    return test.do_wait(client, 20);
  });
}
//
//==========================================================================
extern "C" int rologger_tasks_qmtest(int argc, char** argv)    {
  return complex_test<ROLoggerTest>(argc, argv, [](ROLoggerTest& test)  {
    auto* client = test.start_client("rologger_task", { "-show", "-match="+test.match });
    return test.do_wait(client, 20);
  });
}
#endif
