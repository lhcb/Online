//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================

// Framework inclde files
#include "Util.h"
#include <RTL/rtl.h>
#include <RTL/netdef.h>

extern "C" int test_rpc_server_qmtest(int argc, char** argv)   {
  int loop  = 1, port  = -1;
  std::string host = "localhost", type;
  RTL::CLI cli(argc, argv, []{});
  cli.getopt("host",  4, host);
  cli.getopt("port",  4, port);
  cli.getopt("type",  4, type);
  cli.getopt("loop",  4, loop);

  if ( port < 0 )  {
    port = Online::util::get_unique_port_number(0x4000);
  }
  Online::util::MultiProcessTest test(argc, argv);
  test.test_name   = std::to_string(port);
  test.lib_name    = "libRPCTests.so";
  std::string dbg  = test.debug ? "-debug" : "-nodebug";
  std::string hstr = "-host=" + host;
  std::string pstr = "-port=" + test.test_name;
  std::string lstr = "-loop=" + std::to_string(loop);
  std::string proc = "test_"  + type + "_server";

  auto* server = test.start_proc(proc, {proc, hstr, pstr, dbg}, "stdout");
  test.do_wait(server, 8); // Server must be up before the client starts
  if ( !server->is_running() )  {
    ::lib_rtl_output(LIB_RTL_ALWAYS, "+++ FAILED to start RPC server.");
    return ECHILD;
  }
  proc = "test_"+type+"_client";
  auto* client = test.start_proc(proc, {proc, hstr, pstr, lstr, dbg, "-exit"});
  test.do_wait(client, 2);
  ::lib_rtl_output(LIB_RTL_ALWAYS, "+++ Kill RPC server and end test.");
  test.end(0);
  return 0;
}
