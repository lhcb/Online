//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================

/// Framework include files
#include <RTL/rtl.h>
#include <RTL/hash.h>
#include <RTL/strdef.h>
#include <EventData/RawFile.h>

/// C/C++ include files
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <vector>

namespace {

  int rtl_hash_file(const std::vector<const char*>& args)  {
    
    std::string input, hash_algorithm;
    RTL::CLI cli(args.size(), (char**)&args[0], [](int, char**)  {
      ::printf("Usage : rtl_hashes -opt <opts>\n"
	       "    Verify the consistency of various hash algorithms across platforms \n"
	       "    -input=<file>           Input file name to run hash algorithms on  \n"
	       "    -hash=<name>            Name of the hash algorithm                 \n"
	       "                            crc8, crc16, crc32, fletcher16, fletcher32,\n"
	       "                            hash32, murmur3_32, adler32                \n"
	       "                            (case insensitive)                         \n\n");
    });
    cli.getopt("input", 5, input);
    cli.getopt("hash",  4, hash_algorithm);
    if ( input.empty() )   {
      ::lib_rtl_output(LIB_RTL_INFO, "+++ %s: '%s'",
		       "No input file given to compute checksum/hash of type", hash_algorithm.c_str());
      cli.call_help();
      return EINVAL;
    }
    if ( hash_algorithm.empty() )  {
      ::lib_rtl_output(LIB_RTL_INFO, "+++ %s: '%s'",
		       "No hash algorithm supplied to compute checksum/hash of input", input.c_str());
      cli.call_help();
      return EINVAL;
    }
    hash_algorithm = RTL::str_lower(hash_algorithm);
    Online::RawFile file(input);
    if ( file.openMapped() )   {
      uint32_t hash_value = 0;
      if ( hash_algorithm == "adler32" )
	hash_value = RTL::adler32(    file.begin(), file.mapped_size());
      else if ( hash_algorithm == "hash32" )
	hash_value = RTL::hash32(     file.begin(), file.mapped_size());
      else if ( hash_algorithm == "fletcher16" )
	hash_value = RTL::fletcher16( file.begin(), file.mapped_size());
      else if ( hash_algorithm == "fletcher32" )
	hash_value = RTL::fletcher32( file.begin(), file.mapped_size());
      else if ( hash_algorithm == "murmur3_32" )
	hash_value = RTL::murmur3_32( file.begin(), file.mapped_size());
      else if ( hash_algorithm == "crc8" )
	hash_value = RTL::crc8(       file.begin(), file.mapped_size());
      else if ( hash_algorithm == "crc16" )
	hash_value = RTL::crc16(      file.begin(), file.mapped_size());
      else if ( hash_algorithm == "crc32" )
	hash_value = RTL::crc32(      file.begin(), file.mapped_size());
      else if ( hash_algorithm == "bsd16" )
	hash_value = RTL::bsd16(      file.begin(), file.mapped_size());
      else   {
	::lib_rtl_output(LIB_RTL_ERROR, "+++ Unknown checksum algorithm '%s' requested for input file '%s'",
			 hash_algorithm.c_str(), input.c_str());
	return EINVAL;
      }
      ::lib_rtl_output(LIB_RTL_ALWAYS,
		       "+++ Generated checksum for input file '%s' with algorithm %-16s  Value: %08X",
		       input.c_str(), hash_algorithm.c_str(), hash_value);
      return 0;
    }
    ::lib_rtl_output(LIB_RTL_ERROR, "+++ Failed to map input file: '%s': %s",
		     hash_algorithm.c_str(), input.c_str(), RTL::errorString(errno).c_str());
    return EINVAL;
  }

}

extern "C" int rtl_test_hash_file(int argc, char **argv)  {
  std::vector<const char*> args(&argv[0], &argv[argc]);
  return rtl_hash_file(args);
}

extern "C" int rtl_test_hashes(int argc, char **argv)  {
  std::string input, in, hash;
  RTL::CLI cli(argc, argv, [](int, char**)  {
    ::printf("Usage : rtl_hashes -opt <opts>\n"
	     "    Verify the consistency of various hash algorithms across platforms \n"
	     "    -input=<file>           Input file name to run hash algorithms on  \n"
	     "                                                                       \n");
  });
  cli.getopt("input", 5, input);
  in = "-input="+input;
  //
  hash = "-hash=crc8";
  rtl_hash_file( {argv[0], in.c_str(), hash.c_str()} );
  //
  hash = "-hash=crc16";
  rtl_hash_file( {argv[0], in.c_str(), hash.c_str()} );
  //
  hash = "-hash=crc32";
  rtl_hash_file( {argv[0], in.c_str(), hash.c_str()} );
  //
  hash = "-hash=bsd16";
  rtl_hash_file( {argv[0], in.c_str(), hash.c_str()} );
  //
  hash = "-hash=hash32";
  rtl_hash_file( {argv[0], in.c_str(), hash.c_str()} );
  //
  hash = "-hash=fletcher16";
  rtl_hash_file( {argv[0], in.c_str(), hash.c_str()} );
  //
  hash = "-hash=fletcher32";
  rtl_hash_file( {argv[0], in.c_str(), hash.c_str()} );
  //
  hash = "-hash=adler32";
  rtl_hash_file( {argv[0], in.c_str(), hash.c_str()} );
  //
  hash = "-hash=murmur3_32";
  rtl_hash_file( {argv[0], in.c_str(), hash.c_str()} );
  //
  return 0;
}
