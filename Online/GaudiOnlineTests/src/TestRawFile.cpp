//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================

/// Framework include files
#include "FileUtil.h"
#include <RTL/rtl.h>
#include <RTL/strdef.h>
#include <RTL/Datafile.h>
#include <EventData/RawFile.h>
#include <EventData/event_header_t.h>
#include <Tell1Data/Tell1Decoder.h>

/// C/C++ include files
#include <string>
#include <vector>
#include <cstdint>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/sysmacros.h>

namespace fs = std::filesystem;

namespace {

  struct InPlaceAllocator : public Online::RawFile::Allocator {
    uint8_t* ptr = 0;
    std::size_t len = 0;
    InPlaceAllocator() = default;
    InPlaceAllocator( InPlaceAllocator&& copy ) = delete;
    InPlaceAllocator( const InPlaceAllocator& copy ) = delete;
    virtual ~InPlaceAllocator() { if ( ptr ) delete [] ptr; }
    InPlaceAllocator& operator=( const InPlaceAllocator& copy ) = delete;
    uint8_t* get()      { return ptr; }
    uint8_t* release()  { uint8_t* tmp = ptr; ptr=0; return tmp; }
    virtual uint8_t* operator()(std::size_t length)   override  final {
      if ( !ptr )  {
	ptr = new uint8_t[length];
	len = length;
      }
      return (uint8_t*)((length<=len) ? ptr : 0);
    }
  };
}

extern "C" int rtl_test_datafile_properties(int argc, char** argv) {
  std::string fname;
  RTL::CLI cli( argc, argv, [] {  } );
  cli.getopt("file", 1, fname);
  if ( fname.empty() ) fname = "../../../../Online/ROMon/python/TaskSupervisor.py";
  Online::Datafile file( RTL::str_expand_env(fname) );
  std::error_code ec;
  ::lib_rtl_output( LIB_RTL_ALWAYS, "File path:              %s",  fname.c_str());
  ::lib_rtl_output( LIB_RTL_ALWAYS, "File filesystem path:   %s",  file.fspath().c_str());
  ::lib_rtl_output( LIB_RTL_ALWAYS, "File name:              %s",  file.filename().c_str());
  ::lib_rtl_output( LIB_RTL_ALWAYS, "File directory:         %s",  file.directory().c_str());
  ::lib_rtl_output( LIB_RTL_ALWAYS, "File exists:            %s",  file.exists() ? "YES" : "NO");
  ::lib_rtl_output( LIB_RTL_ALWAYS, "File is empty:          %s",  file.is_empty() ? "YES" : "NO");
  ::lib_rtl_output( LIB_RTL_ALWAYS, "File type regular:      %s",  file.type()==fs::file_type::regular ? "YES" : "NO");
  ::lib_rtl_output( LIB_RTL_ALWAYS, "File type symlink:      %s",  file.type()==fs::file_type::symlink ? "YES" : "NO");
  ::lib_rtl_output( LIB_RTL_ALWAYS, "File type directory:    %s",  file.type()==fs::file_type::directory ? "YES" : "NO");
  // ::lib_rtl_output( LIB_RTL_ALWAYS, "File last written:      %ld", file.last_write_time().time_since_epoch());
  // ::lib_rtl_output( LIB_RTL_ALWAYS, "Current working dir:    %s",  fs::current_path(ec).c_str());
  return 0;
}

extern "C" int rtl_test_datafile_mkdir(int argc, char** argv) {
  std::string dir;
  struct stat stat;
  RTL::CLI cli( argc, argv, [] {  } );
  cli.getopt("directory", 1, dir);
  if ( dir.empty() )  cli.call_help();

  if ( 0 == ::stat( dir.c_str(), &stat ) )   {
    ::lib_rtl_output(LIB_RTL_ALWAYS,"FAILED to create directory: %s [directory exists]",dir.c_str());
    return EINVAL;
  }  
  int ret = Online::Datafile::mkdir( dir.c_str(), 0777, false );
  if ( ret )   {
    ::lib_rtl_output(LIB_RTL_ALWAYS,"FAILED to create directory: %s [%s]",
		     dir.c_str(), RTL::errorString(errno).c_str());
    return EINVAL;
  }
  if ( 0 == ::stat( dir.c_str(), &stat ) )   {
    if ( (stat.st_mode&S_IFMT) == S_IFDIR )  {
      ::lib_rtl_output(LIB_RTL_ALWAYS,"PASSED: Created directory: %s",dir.c_str());
      return 0;
    }
  }
  return EINVAL;
}

extern "C" int rtl_test_datafile_rmdir(int argc, char** argv) {
  std::string dir;
  struct stat stat;
  RTL::CLI cli( argc, argv, [] {    } );
  cli.getopt( "directory", 1, dir );
  if ( dir.empty() )  cli.call_help();
 
  if ( -1 == ::stat( dir.c_str(), &stat ) )   {
    ::lib_rtl_output( LIB_RTL_ALWAYS,
		      "FAILED: Directory %s does not exist. Cannot delete.", dir.c_str());
    return EINVAL;
  }  
  int ret = Online::Datafile::rmdir( dir.c_str(), true, false );
  if ( 0 == ret && -1 == ::stat( dir.c_str(), &stat ) )   {
    ::lib_rtl_output( LIB_RTL_ALWAYS, "PASSED: removed directory: %s", dir.c_str());
    return 0;
  }
  ::lib_rtl_output( LIB_RTL_ALWAYS, "FAILED to remove directory: %s [%s]",
		    dir.c_str(), RTL::errorString(errno).c_str());
  return EINVAL;
}

extern "C" int rtl_test_datafile_mapmemory( int argc, char** argv ) {
  RTL::CLI cli( argc, argv, [] { } );
  std::size_t bufflen = 1024;
  std::vector<int32_t> buffer;

  cli.getopt( "size", 1, bufflen );
  for( std::size_t i=0; i<bufflen; ++i ) buffer.emplace_back(i);

  Online::Datafile file( "Mapped-memory" );
  int ret = file.map_memory( &buffer.at(0), bufflen*sizeof(int32_t) );
  if ( ret )  {
    std::size_t length = file.data_size();
    
    ::lib_rtl_output( LIB_RTL_ALWAYS,
		      "Successfully mapped memory buffer of %ld bytes", length);
    for(int32_t data = 0, count = 0; file.read( &data, sizeof(data) ) > 0; ++count )  {
      length -= sizeof(data);
      if ( data != count )  {
	::lib_rtl_output( LIB_RTL_ERROR, "Data error: %d != %d", data, count);
      }
    }
    ::lib_rtl_output( LIB_RTL_ALWAYS,
		      "Successfully read mapped memory buffer. %ld bytes left.", length);
  }
  return 0;
}

extern "C" int rtl_test_datafile_mapfile_read(int argc, char** argv) {
  std::string name, fname;
  RTL::CLI cli( argc, argv, [] { } );
  cli.getopt( "file", 1, fname );

  fname = RTL::str_expand_env(fname);
  name  = fs::path(fname).filename();

  Online::Datafile in( fname );
  std::vector<uint8_t> buffer = Online::util::load_file( in );
  ::lib_rtl_output( LIB_RTL_ALWAYS,
		    "Successfully read verification data from %s [0x%08lX bytes]",
		    name.c_str(), buffer.size());

  Online::Datafile file( fname );
  if ( file.openMapped() )  {
    int32_t* in_buff = (int32_t*)&buffer.at(0);
    int64_t  length = 0;

    ::lib_rtl_output( LIB_RTL_ALWAYS,
		      "Successfully mapped file %s of 0x%08lX bytes",
		      name.c_str(), length);
    for(int32_t len=1, count=0, data=0; len > 0; ++count ) {
      len = file.read( &data, sizeof(data) );
      if ( data != in_buff[count] )  {
	::lib_rtl_output( LIB_RTL_ERROR,
			  "Data error: %d != %d", data, in_buff[count]);
      }
      length += len;
      if ( (len == sizeof(data)) && (file.position() != length) )  {
	::lib_rtl_output( LIB_RTL_ERROR,
			  "File position error: %ld != %ld",
			  length, file.position());
      }
    }
    if ( (length-file.data_size()) < 4 ) length = 0;
    ::lib_rtl_output( LIB_RTL_ALWAYS,
		      "PASSED: Successfully read and verified mapped file %s. [0x%08lX bytes]",
		      name.c_str(), length);
  }
  return 0;
}

extern "C" int rtl_test_datafile_read_events( int argc, char** argv )  {
  std::string fname;
  RTL::CLI cli( argc, argv, [] { } );
  cli.getopt( "file", 1, fname );
  Online::RawFile file( RTL::str_expand_env( fname ) );

  if ( file.open() )  {
    int size[3];
    InPlaceAllocator allocator;
    std::size_t      num_events = 0;
    while ( 1 )  {
      int64_t peek_size = sizeof(size);
      int64_t status = file.read( size, peek_size );
      if ( status == sizeof(size) )  {
	auto* hdr = (Online::event_header_t*)size;
	if ( hdr->is_mdf() )   {
	  int64_t  data_size = hdr->recordSize();
	  uint8_t* data_ptr  = allocator( data_size );
	  status = file.read( data_ptr+peek_size, data_size-peek_size );
	  if ( status == (data_size - peek_size) )   {
	    hdr = (Online::event_header_t*)data_ptr;
	    ::memcpy( data_ptr, size, peek_size );
	    Online::checkRecord( hdr, ~0x0, true, true );
	    ++num_events;
	    ::lib_rtl_output( LIB_RTL_ERROR,
			      "Successfully read event %ld [%ld bytes] %ld banks",
			      num_events, data_size, Online::numberOfBanks(hdr) );
	    continue;
	  }
	}
      }
      break;
    }
    ::lib_rtl_output( LIB_RTL_ERROR,
		      "PASSED: Successfully read %ld events", num_events);
    return 0;
  }
  std::error_code ec = std::make_error_code(std::errc::no_such_file_or_directory);
  ::lib_rtl_output( LIB_RTL_ERROR,
		    "FAILED: Cannot open input file: %s",
		    ec.message().c_str() );
  return ENOENT;
}

