//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
#include <cstdio>
#include <cstring>
#include <RTL/rtl.h>
#include <SCR/scr.h>

extern "C" int rtl_testscreen(int argc,char **) {
  char caption[255];
  ::snprintf(caption, sizeof(caption), "This is just a message!");
  using namespace SCR;
  Pasteboard* pasteboard = nullptr;
  Display*    display = nullptr;
  int height = 0, width = 0;
  ::scrc_create_pasteboard (&pasteboard, 0, &height, &width);
  ::scrc_create_display (&display, height-2, width-2, NORMAL, ON, "SCR test program");
  ::scrc_paste_display  (display, pasteboard, 2, 2);
  ::scrc_put_chars(display,caption, NORMAL, 2, 2, 0);
  ::scrc_end_pasteboard_update(pasteboard);
  ::scrc_fflush(pasteboard);
  ::scrc_set_cursor(display, 2, 10);
  ::scrc_cursor_off(pasteboard);

  if ( argc == 0 ) lib_rtl_sleep(1000*20);
  ::scrc_clear_screen(pasteboard);
  ::scrc_delete_display(display);
  ::scrc_delete_pasteboard(pasteboard);
  ::scrc_end_screen();
  ::printf("\n\n\n  +---------------------------------------+\n");
  ::printf("  |    It looks like SCR seems to work    |\n");
  ::printf("  +---------------------------------------+\n\n\n");
  ::lib_rtl_sleep(1000);
  return 0;
}
