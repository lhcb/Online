//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
#include <CPP/Interactor.h>
#include <CPP/TimeSensor.h>
#include <CPP/IocSensor.h>
#include <CPP/KeySensor.h>
#include <CPP/Event.h>
#include <WT/wtdef.h>
#include "KeyboardEmulator.h"

#include <iostream>
#include <cstdlib>
#include <vector>
using namespace std;

namespace {

  class SensorTester : public Interactor {
    typedef vector<SensorTester*> Probes;
    SensorTester* m_parent;
    int    m_turns=0, m_sleep=0, m_id=0;
    std::string m_name;
    Probes m_probes;

  public:
    int count = 0;

  public:
    SensorTester(const std::string name, SensorTester* parent, int turns)
      : m_parent(parent), m_turns(turns), m_name(name), count(turns)
    {
      m_sleep = 100%(turns>0 ? turns : 1);
    }

    virtual ~SensorTester() {
      for( auto* p : m_probes )
        delete p;
      m_probes.clear();
      cout << "[" << m_name << "]  Sensor test finished after " << count << " calls." << endl;
    }

    void add(SensorTester* test) {
      count  += test->m_turns;
      m_turns  += test->m_turns+1;
      test->m_id = m_probes.size()+1;
      m_probes.push_back(test);
    }

    void check_finish()  {
      --m_turns;
      if ( 0 == m_turns )   {
	cout << "[" << m_name << "] Finished processing after " << count << " turns." << endl;
	for(size_t i=0; i<m_probes.size(); ++i) {
	  cout << "[" << m_name << "] ---> Probe " << i << " had " << m_probes[i]->count << " turns." << endl;
	}
	::wtc_insert(WT_FACILITY_EXIT,0);
      }
    }

    void handleTimer(long value) {
      switch(value)   {
      case 0:
        --m_parent->m_turns;
	//cout << "[" << m_name << "] " << m_turns << " turns." << m_parent->m_turns << endl;
	TimeSensor::instance().add(this,m_sleep,(void*)long((--m_turns)>0 ? 0 : 1));
	break;
      case 1:
	TimeSensor::instance().add(m_parent,m_sleep,(void*)2);
	break;
      case 2:
	check_finish();
	break;
      }
    }

    void handleIoc(int typ) {
      switch(typ) {
      case 0:
        --m_parent->m_turns;
	//cout << "[" << m_name << "] " << m_turns << " turns. Parent:" << m_parent->m_turns << endl;
	IocSensor::instance().send(this,(--m_turns)>0 ? 0 : 1,this);
	break;
      case 1:
	IocSensor::instance().send(m_parent,2,this);
        break;
      case 2:
	check_finish();
	break;
      }
    }

    void handleKey(int key) {
      m_parent = this;
      if ( --m_turns > 1 )  {
	cout << "[" << m_name << "] " << m_turns << " turns. Got key:" << key << endl;
	return;
      }
      check_finish();
      CPP::KeySensor::instance().shutdown();
    }

    void handleMouse(const CPP::KeyEvent& ev) {
      if ( --m_turns > 1 )  {
	cout << "[" << m_name << "] " << m_turns << " turns. Got mouse event:"
	     << " button:" << ev.button << " mod:" << ev.modifier
	     << " x:" << ev.x << " y:" << ev.y << endl;
	return;
      }
      m_parent = this;
      IocSensor::instance().send(this,1,this);
    }

    void handle(const Event& ev) override {
      switch(ev.eventtype) {
      case IocEvent:
        handleIoc(ev.type);
        break;
      case TimeEvent:
        handleTimer((long)ev.timer_data);
        break;
      case CPP::KeyboardEvent:
	handleKey(ev.get<CPP::KeyEvent>()->stroke);
	break;
      case CPP::ScrMouseEvent:
	handleMouse(*ev.get<CPP::KeyEvent>());
	break;
      default:
        break;
      }
    }
    
    static int start_ioc() {
      SensorTester test("IOCTEST", 0, 0);
      for (size_t i=0; i<10; ++i) {
        SensorTester* probe = new SensorTester("IOCTEST", &test, 1000);
        test.add(probe);
        IocSensor::instance().send(probe,0,probe);
      }
      IocSensor::instance().run();
      IocSensor::instance().shutdown();
      return 0;
    }
    
    static int start_timer() {
      SensorTester test("TIMERTEST", 0, 0);
      for (int i=0; i<10; ++i) {
        SensorTester* probe = new SensorTester("TIMERTEST", &test, i+1);
        test.add(probe);
        TimeSensor::instance().add(probe,i+1);
      }
      TimeSensor::instance().run();
      TimeSensor::instance().shutdown();
      return 0;
    }
    
    static int start_key_sensor()  {
      SensorTester test("KEYTEST", 0, 15);
      CPP::KeySensor::instance().add(&test);
      CPP::KeySensor::instance().run();
      return 0;
    }
  };
}

extern "C" int cpp_test_iocsensor(int, char**) {
  return SensorTester::start_ioc();
}

extern "C" int cpp_test_timesensor(int, char**) {
  return SensorTester::start_timer();
}

extern "C" int cpp_test_keysensor_internal(int, char**)  {
  return SensorTester::start_key_sensor();
}

/// Testing UPI detached menu
extern "C" int cpp_test_keysensor(int /* argc */, char** /* argv */)  {
  Online::KeyboardEmulator test("online_test cpp_test_keysensor_internal");
  for(int i=0; i<15; ++i)
    test.hit_key('a'+i);
  return test.done();
}
