//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================

/// Framework include files
#include <RTL/rtl.h>
#include <RTL/strdef.h>

/// C/C++ include files
#include <csignal>
#include <unistd.h>

namespace {
  int signal_handler(void* user_context, int signal, siginfo_t* , void* )   {
    ::lib_rtl_output(LIB_RTL_ALWAYS,"+++ Signal handler called on %d context: %p", signal, user_context);
    return LIB_RTL_SIGNAL_HANDLED;
  }
  void test_signal(int sig_num)   {
    long ctxt = 0xfeedbabe + sig_num;
    ::lib_rtl_subscribe_signal(sig_num, (void*)ctxt, signal_handler);
    ::kill(lib_rtl_pid(), sig_num);
    ::lib_rtl_sleep(50);
    ::fflush(stdout);
  }
}

extern "C" int rtl_test_signals(int /* argc */, char** /* argv */)  {
  ::lib_rtl_reset();
  ::setenv("UTGID", "SIGTEST", 1);
  ::lib_rtl_output(LIB_RTL_ALWAYS,"+++ Starting signal handler test.");
  /// Trigger loading of ROOT signal handler
  ::kill(lib_rtl_pid(), SIGALRM);
  /// Now overwrite the signal handlers
  ::lib_rtl_apply_signal_handlers();

  /// And off we go!
  test_signal(SIGHUP);
  test_signal(SIGINT);
  test_signal(SIGQUIT);
  test_signal(SIGILL);
  test_signal(SIGTRAP);

  test_signal(SIGABRT);
  test_signal(SIGBUS);
  test_signal(SIGFPE);
  test_signal(SIGUSR1);

  test_signal(SIGSEGV);
  test_signal(SIGUSR2);
  test_signal(SIGPIPE);
  test_signal(SIGALRM);
  test_signal(SIGTERM);

  test_signal(SIGSTKFLT);
  test_signal(SIGCHLD);
  test_signal(SIGCONT);
  //This one not! test_signal(SIGSTOP);
  test_signal(SIGTSTP);
  
  test_signal(SIGTTIN);
  test_signal(SIGTTOU);
  test_signal(SIGURG);
  test_signal(SIGXCPU);
  test_signal(SIGXFSZ);

  test_signal(SIGVTALRM);
  test_signal(SIGPROF);
  test_signal(SIGWINCH);
  test_signal(SIGIO);
  test_signal(SIGPWR);

  test_signal(SIGSYS);
  test_signal(SIGRTMIN);

  test_signal(SIGRTMAX);

  //test_signal();

  ::lib_rtl_output(LIB_RTL_ALWAYS,"+++ Signal handler test ended.");
  return 0;
}
