//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================

// Framework inclde files
#include "Util.h"
#include <RTL/rtl.h>
#include <RTL/strdef.h>
#include <RTL/Logger.h>
#include <SmiController/Tasklist.h>
#include <SmiController/TasklistHandlers.h>
#include <Parsers/Printout.h>
#include <XML/XML.h>

// C/C++ include files
#include <regex>
#include <iostream>
#include <filesystem>
//
namespace xml = dd4hep::xml;
//  
//
//==========================================================================
namespace   {

  /// Class to test the SmiController functionality
  /**
   *   \author  M.Frank
   *   \version 1.0
   */
  class SmiControllerTest : public Online::util::MultiProcessTest  {
  public:
    std::string snapshot;
    std::string option;
    int         port   { 0 };
    std::string host   { "localhost" };
    std::string match  { "(.*)/df_(.*).xml" };
    std::vector<std::string> replacements { };
    RTL::Process* server { nullptr };
    std::shared_ptr<RTL::Logger::LogDevice> logger;

    static size_t print_func(void* p, dd4hep::PrintLevel lvl, const char* src, const char* fmt, va_list& args) {
      SmiControllerTest* test = (SmiControllerTest*)p;
      return test->logger->printout(lvl, src, fmt, args);
    }

  public:
    //==========================================================================
    SmiControllerTest(int argc, char** argv, int p=-1) : MultiProcessTest(argc, argv), port(p)   {
      RTL::CLI cli(argc, argv, [](){});
      this->port      = get_unique_port(0x3000);
      this->lib_name  = "libPcSrvLib.so";
      this->logger    = RTL::Logger::getGlobalDevice();
      Online::setPrinter2(this, print_func);
      xml::enableEnvironResolution(false);
      cli.getopt("host",     4, this->host);
      cli.getopt("match",    4, this->match);
    }
    //==========================================================================
    std::map<std::string, std::filesystem::path> architectures()  const {
      std::regex reg(this->match);
      std::map<std::string, std::filesystem::path> files;
      const char* env  = std::getenv("GAUDIONLINETESTSROOT");
      std::string path = (env ? env : ".") + std::string("/tests/architectures/dataflow");

      for (auto const& entry : std::filesystem::directory_iterator{path})  {
	std::error_code ec;
	if ( std::filesystem::is_regular_file(entry.path(), ec) )  {
	  std::smatch sm;
	  std::string nam = entry.path().string();
	  if ( std::regex_match(nam, sm, reg) )    {
	    //std::cout << "Match: " << nam << std::endl;
	    files.emplace(entry.path().string(), entry.path());
	  }
	}
      }
      return files;
    }
    //==========================================================================
    xml::Document open_architecture(const std::filesystem::path& path)  const  {
      const auto& fname = path.filename();
      std::cout 
	<< "+---------------------------------------------------------------" << std::endl
	<< "|   Loading architecture file: " << fname << std::endl
	<< "+---------------------------------------------------------------" << std::endl;
      return xml::DocumentHandler().load(path);
    }
  };
}
//
//==========================================================================
extern "C" int controller_read_architecture_xml(int argc, char** argv)    {
  return complex_test<SmiControllerTest>(argc, argv, [](SmiControllerTest& test) {
      using namespace Online;
      auto files = test.architectures();
      for(const auto& file : files)   {
	FiniteStateMachine::TasklistPrinter  printer { file.second.filename() };
	xml::DocumentHolder doc(test.open_architecture(file.second));
	xml_coll_t(doc.root(),_Unicode(include)).for_each(printer);
	xml_coll_t(doc.root(),_Unicode(task)).for_each(printer);
      }
      return 0;
    });
}
//
//==========================================================================
extern "C" int controller_eval_architecture(int argc, char** argv)    {
  return complex_test<SmiControllerTest>(argc, argv, [](SmiControllerTest& test) {
      using namespace Online;
      auto files = test.architectures();
      xml::enableEnvironResolution(false);
      for(const auto& file : files)   {
	const auto& fname = file.second.filename();
	FiniteStateMachine::Tasklist         tasks;
	FiniteStateMachine::TasklistAnalyzer analyzer(tasks);
	FiniteStateMachine::TasklistPrinter  printer {fname};
	xml::DocumentHolder doc(test.open_architecture(file.second));
	xml_coll_t(doc.root(),_Unicode(include)).for_each(analyzer);
	xml_coll_t(doc.root(),_Unicode(task)).for_each(analyzer);
	printer(tasks);
      }
      return 0;
    });
}
