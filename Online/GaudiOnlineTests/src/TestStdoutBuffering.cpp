//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
#include "RTL/rtl.h"

static void help()   {
}

extern "C" int rtl_test_stdout_buffering(int argc, char** argv) {
  RTL::CLI cli(argc, argv, help);

  return 0;
}
