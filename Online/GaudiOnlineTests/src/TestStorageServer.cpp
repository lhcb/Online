//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================

// Framework inclde files
#include "Util.h"
#include <RTL/rtl.h>
#include <RTL/strdef.h>
#include <RTL/netdef.h>

// C/C++ include files
#include <iostream>
//  
//
//==========================================================================
namespace   {

  /// Class to test the HLT1 storage client with the small stanalone server
  /**
   *   \author  M.Frank
   *   \version 1.0
   */  
  class StorageTest : public Online::util::MultiProcessTest  {
  public:
    std::string   db_url;
    std::string   fs_url;
    std::string   dbase;
    std::string   host   { "127.0.0.1" };
    int           count  { 0 };
    int           length { 0 };
    int           turns  { 0 };
    int           port   { 0 };
    int           file_seq { 0 };
    int           retries  { 3 };
    std::unique_ptr<RTL::Process> db_server;
    std::unique_ptr<RTL::Process> fs_server;
    std::unique_ptr<RTL::Process> dns_server;

  public:
    //==========================================================================
    StorageTest(int argc, char** argv, int p=-1) : MultiProcessTest(argc, argv), port(p)   {
      RTL::CLI cli(argc, argv, [](){});
      this->port     = get_unique_port(0x2000);
      this->lib_name = "libStorageServer.so";

      cli.getopt("host",    4, this->host);
      cli.getopt("count",   4, this->count);
      cli.getopt("turns",   5, this->turns);
      cli.getopt("length",  5, this->length);
      cli.getopt("port",    4, this->port);
      cli.getopt("retries", 4, this->retries);
      ::setenv("DIM_DNS_NODE", RTL::nodeNameShort().c_str(), 1);
    }
    //==========================================================================
    virtual ~StorageTest()  {
      this->db_server.release();
      this->fs_server.release();
      dns_server.release();
    }
    //==========================================================================
    std::string new_file()   {
      return "/objects/fdb_test/"+this->test_name+"/Run0000123.0x"+RTL::to_hex(++this->file_seq)+".raw";
    }    
    //==========================================================================
    int exit_http_server(const std::string& url, int wait_seconds)   {
      int ret;
      std::string cmd = "wget http://"+url+"/control=exit -O /dev/null > /dev/null 2>&1";
      ::lib_rtl_output(LIB_RTL_VERBOSE, "+++ Executing: %s", cmd.c_str());
      ret = system(cmd.c_str());
      this->do_wait(wait_seconds);
      return ret;
    }
    //==========================================================================
    RTL::Process* start_db_server()    {
      std::string log = this->logs.empty() ? "stdout" : "DB_SERVER.log";
      if ( this->db_server )   {
	this->db_server->killall();
	this->remove_process(db_server.release());
      }
      this->db_server.reset(this->start_proc("FDB_DB_SERVER_" + this->test_name,
					     {"fdb_sqlite_server",
						 "-threads=1",
						 "-print="  + std::to_string(this->print_level),
						 "-local="  + this->db_url,
						 "-server=" + this->fs_url,
						 this->dbase}, log));
      return this->db_server.get();
    }
    //==========================================================================
    RTL::Process* start_fs_server()    {
      std::string log = this->logs.empty() ? "stdout" : "FS_SERVER.log";
      if ( this->fs_server )  {
	this->fs_server->killall();
	this->remove_process(fs_server.release());
      }
      this->fs_server.reset(this->start_proc("FDB_FS_SERVER_" + this->test_name,
					     { "fdb_fs_file_server",
						 "-threads=1",
						 "-buffersize=16777216",
						 "-print="  + std::to_string(this->print_level),
						 "-local="  + this->fs_url,
						 "-server=" + this->db_url,
						 "-files=."}, log));
      return this->fs_server.get();
    }
    //==========================================================================
    RTL::Process* setup_generator_writer(const std::string& id,
					 const std::string& fname,
					 std::size_t length)
    {
      std::string log = this->logs.empty() ? std::string("stdout") : "FDB_WRITE_"+id+".log";
      return setup_proc("FDB_WRITE_" + /* test_name + "_" + */ id,
			this->command,
			"libStorageCli.so",
			{ "fdb_test_client_put",
			  "-server=" + this->db_url,
			  "-file="   + fname,
			  "-length=" + std::to_string(length) },
			log, true);
    }
    //==========================================================================
    RTL::Process* setup_generic_reader(const std::string& id)  {
      std::string log = this->logs.empty() ? std::string("stdout") : "FDB_READ_"+id+".log";
      return setup_proc("FDB_READ_" + /* test_name + "_" + */ id,
			this->command,
			"libStorageCli.so",
			{ "fdb_test_client_next_delete", "-server=" + this->db_url,
			  "-url=/objects/fdb_test/" + this->test_name },
			log, true);
    }
    //==========================================================================
    std::vector<RTL::Process*> parallel_writers(std::size_t count)   {
      std::vector<RTL::Process*> procs;
      for( std::size_t i=1; i <= count; ++i )    {
	auto* p = this->setup_generator_writer(std::to_string(i), this->new_file(), i*10*1024);
	procs.emplace_back(p);
      }
      return procs;
    }
    //==========================================================================
    std::vector<RTL::Process*> parallel_readers(std::size_t count)   {
      std::vector<RTL::Process*> procs;
      for( std::size_t i=1; i <= count; ++i )
	procs.emplace_back(this->setup_generic_reader(std::to_string(i)));
      return procs;
    }
    //==========================================================================
    std::vector<RTL::Process*> parallel_db_stress(std::size_t count)   {
      std::vector<RTL::Process*> procs;
      for( std::size_t i=1; i <= count; ++i )  {
	auto id = std::to_string(i);
	auto* p =
	  setup_proc("FDB_DBSTRESS_" + id, this->command, "libStorageCli.so",
		     { "fdb_test_db_server_stress",
		       "-server="+this->db_url,
		       "-run="+this->test_name+id,
		       "-count="  + std::to_string(this->turns),
		       "-length=" + std::to_string(this->length) },
		     nullptr, true);
	procs.emplace_back(p);
      }
      return procs;
    }
    //==========================================================================
    void start_unlink()    {
      auto* proc =
	this->start_proc("FDB_DB_UNLINK_"+this->test_name,
			 {"storage_sqlite_unlink_database", "-print=3", this->dbase}, "DB_UNLINK.log");
      this->do_wait(proc, 0);
    }
    //==========================================================================
    int exit_db_server(int wait_seconds)   {
      int status = this->exit_http_server(db_url, 0);
      if ( this->db_server )   {
	this->do_wait(this->db_server, wait_seconds);
	this->start_unlink();
	this->db_server->killall();
	this->db_server.release();
      }
      else  {
	this->do_wait(wait_seconds);
      }
      return status;
    }
    //==========================================================================
    int exit_fs_server(int wait_seconds)   {
      int status = this->exit_http_server(fs_url, wait_seconds);
      if ( this->fs_server )  {
	this->do_wait(this->fs_server, wait_seconds);
	this->fs_server->killall();
	this->fs_server.release();
      }
      else  {
	this->do_wait(wait_seconds);
      }
      return status;
    }
    //==========================================================================
    std::vector<RTL::Process*> start_servers(bool db, bool fs)   {
      this->dns_server.reset(this->start_dns());
      for(int i=0; i < retries; ++i)   {
	struct sockaddr_in db_addr { AF_INET, 0, { INADDR_ANY }, {0}};
	struct sockaddr_in fs_addr { AF_INET, 0, { INADDR_ANY }, {0}};
	int fs_sock = ::net_bind_free_server_port(&fs_addr);
	int db_sock = ::net_bind_free_server_port(&db_addr);

	this->test_name = "ID" + RTL::to_HEX(db_addr.sin_port, 8);
	this->dbase     = "-database=fdb_" + this->test_name + ".db";
	this->db_url    = this->host + ":" + std::to_string(db_addr.sin_port);
	this->fs_url    = this->host + ":" + std::to_string(fs_addr.sin_port);
	::close(db_sock);
	if ( db ) this->start_db_server();
	::close(fs_sock);
	if ( fs ) this->start_fs_server();
	if ( db && fs )   {
	  this->do_wait(this->db_server, 5);
	  this->do_wait(this->fs_server, 1);
	  if ( this->db_server->is_running() && this->fs_server->is_running() )   {
	    return { this->db_server.get(), this->fs_server.get() };
	  }
	}
	else if ( fs )   {
	  this->do_wait(this->fs_server, 5);
	  if ( this->fs_server->is_running() )   {
	    return { this->db_server.get(), this->fs_server.get() };
	  }
	}
	else if ( db )   {
	  this->do_wait(this->db_server, 5);
	  if ( this->db_server->is_running() )   {
	    return { this->db_server.get(), this->db_server.get() };
	  }
	}
      }

      ::lib_rtl_output(LIB_RTL_ERROR,
		       "FAILED to start storage file servers: %s %s %s %s.",
		       db ? "DB:" : "",
		       db ? this->db_server->is_running() ? "OK" : "FAILED" : "",
		       fs ? "FS:" : "",
		       fs ? this->fs_server->is_running() ? "OK" : "FAILED" : "");
      return { this->dns_server.get(), this->db_server.get(), this->db_server.get() };
    }
    //==========================================================================
    virtual int exec_preamble()  override {
      start_servers(1, 1);
      this->do_wait(8);
      return 0;
    }
    //==========================================================================
    virtual int exit_server(int sleep=3) override  {
      this->exit_db_server(3);
      this->exit_fs_server(3);
      if ( this->dns_server ) this->stop_dns(this->dns_server.release(), 1);
      return this->end(sleep);
    }
  };
}
//  
//
//==========================================================================
extern "C" int storage_start_db_server(int argc, char** argv)    {
  StorageTest test(argc, argv);
  test.start_servers(1, 0);
  test.do_wait(test.wait+5);
  test.exit_db_server(3);
  ::lib_rtl_output(LIB_RTL_ALWAYS,"++ Storage Database Server: terminated.....");
  return test.end(3);
}
//  
//
//==========================================================================
extern "C" int storage_stress_db_server(int argc, char** argv)    {
  StorageTest test(argc, argv);
  test.start_servers(1, 0);
  test.do_wait(10);
  auto procs = test.parallel_db_stress(test.count);
  test.process_group.start();
  test.do_wait(procs, test.wait);
  ::lib_rtl_output(LIB_RTL_ALWAYS,"+++ Client group has finished processing.");
  test.exit_db_server(3);
  return test.end(0);
}
//
//
//==========================================================================
extern "C" int storage_start_fs_server(int argc, char** argv)    {
  StorageTest test(argc, argv);
  test.start_servers(0, 1);
  test.do_wait(test.wait+5);
  test.exit_fs_server(3);
  return test.end(3);
}
//  
//
//==========================================================================
extern "C" int storage_start_storage(int argc, char** argv)    {
  return complex_test<StorageTest>(argc, argv, [](StorageTest& test) {  test.do_wait(8);  return 0; });
}
//  
//
//==========================================================================
extern "C" int storage_write_storage(int argc, char** argv)    {
  return complex_test<StorageTest>(argc, argv, [](StorageTest& test) {
    test.do_wait(5); // Optional wait to ensure servers are running and listening
    auto procs = test.parallel_writers(test.count);
    test.process_group.start();
    test.do_wait(test.wait);
    return test.do_wait(procs, 3);
  });
}
//  
//
//==========================================================================
extern "C" int storage_write_read_storage(int argc, char** argv)    {
  return complex_test<StorageTest>(argc, argv, [](StorageTest& test) {
    test.do_wait(5); // Optional wait to ensure servers are running and listening
    auto procs = test.parallel_writers(test.count);
    test.process_group.start();
    test.do_wait(procs, test.wait+2);
    // One reader more: test if empty storage is properly detected.
    procs = test.parallel_readers(test.count+1);
    test.process_group.start();
    return test.do_wait(procs, 2);
  });
}
//
//
//==========================================================================
extern "C" int storage_interactive_storage(int argc, char** argv)    {
  return complex_test<StorageTest>(argc, argv, [](StorageTest& test) {
    test.do_wait(test.wait+1);
    return 0;
  });
}
