//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================

// Framework include files
#include "Util.h"
#include <RTL/strdef.h>

// C/C++ include files
#include <ctime>

namespace {
  int tan_test(Online::util::MultiProcessTest& test,
	       RTL::Process* tan_serv,
	       const char* port,
	       const char* aa[],
	       const char* bb[],
	       const char* cc[] )
  {
    RTL::ProcessGroup pg;
    std::vector<RTL::Process*> procs;
    const char* logs = test.logs.c_str();
    std::string node = "-node=" + RTL::nodeName();
    const char *end[] = {"tan_nameserver_shutdown", "-nowait", node.c_str(), port, 0};
    std::string cmd(OnlineBase::currentCommand());
    ::lib_rtl_set_log_level(LIB_RTL_INFO);
    RTL::Process::setDebug(true);
    procs.emplace_back(pg.add(new RTL::Process("AAAAAA", cmd, aa, logs)))->start();
    ::lib_rtl_sleep(100);
    procs.emplace_back(pg.add(new RTL::Process("BBBBBB", cmd, bb, logs)))->start();
    ::lib_rtl_sleep(100);
    procs.emplace_back(pg.add(new RTL::Process("CCCCCC", cmd, cc, logs)))->start();
    std::cout << "Wait for processes ..... " << std::endl;
    pg.wait(procs, RTL::Process::WAIT_BLOCK);
    ::lib_rtl_sleep(1000);
    procs.clear();
    auto* shutdown = pg.add(new RTL::Process("TANShutdown", cmd, end, logs));
    shutdown->start();
    shutdown->wait(RTL::Process::WAIT_BLOCK);
    tan_serv->wait(RTL::Process::WAIT_BLOCK);
    ::lib_rtl_sleep(1000);
    pg.kill();
    pg.wait(RTL::Process::WAIT_BLOCK);
    std::cout << "All processes finished work.. " << std::endl;
    ::lib_rtl_sleep(1000);
    pg.removeAll();
    return 0;
  }
}

extern "C" int rtl_socket_tan_qmtest(int argc, char** argv)  {
  Online::util::MultiProcessTest test(argc, argv);
  auto [prt, srv]  = test.start_tan("stdout");
  std::string port = "-port=" + std::to_string(prt);
  const char *aa[] = {"rtl_tan_alloc_test", "-task=AAAAAA", "-maxtask=1", "-inquire", "-alias=20", port.c_str(), 0};
  const char *bb[] = {"rtl_tan_alloc_test", "-task=BBBBBB", "-maxtask=1", "-inquire", "-alias=20", port.c_str(), 0};
  const char *cc[] = {"rtl_tan_alloc_test", "-task=CCCCCC", "-maxtask=1", "-inquire", "-alias=20", port.c_str(), 0};
  return tan_test(test, srv, port.c_str(), aa, bb, cc);
}

extern "C" int rtl_socket_asio_tan_qmtest(int argc, char** argv)  {
  Online::util::MultiProcessTest test(argc, argv);
  auto [prt, srv]  = test.start_tan("stdout");
  std::string port = "-port=" + std::to_string(prt);
  const char *aa[] = {"boost_asio_tan_alloc_test", "-task=AAAAAA", "-maxtask=1", "-inquire", "-alias=20", port.c_str(), 0};
  const char *bb[] = {"boost_asio_tan_alloc_test", "-task=BBBBBB", "-maxtask=1", "-inquire", "-alias=20", port.c_str(), 0};
  const char *cc[] = {"boost_asio_tan_alloc_test", "-task=CCCCCC", "-maxtask=1", "-inquire", "-alias=20", port.c_str(), 0};
  return tan_test(test, srv, port.c_str(), aa, bb, cc);
}

extern "C" int rtl_asio_socket_tan_qmtest(int argc, char** argv)  {
  Online::util::MultiProcessTest test(argc, argv);
  auto [prt, srv]  = test.start_asio_tan("stdout");
  std::string port = "-port=" + std::to_string(prt);
  const char *aa[] = {"rtl_tan_alloc_test", "-task=AAAAAA", "-maxtask=15", "-inquire", "-alias=15", port.c_str(), 0};
  const char *bb[] = {"rtl_tan_alloc_test", "-task=BBBBBB", "-maxtask=15", "-inquire", "-alias=15", port.c_str(), 0};
  const char *cc[] = {"rtl_tan_alloc_test", "-task=CCCCCC", "-maxtask=15", "-inquire", "-alias=15", port.c_str(), 0};
  return tan_test(test, srv, port.c_str(), aa, bb, cc);
}

extern "C" int rtl_boost_asio_tan_qmtest(int argc, char** argv)  {
  Online::util::MultiProcessTest test(argc, argv);
  auto [prt, srv]  = test.start_asio_tan("stdout");
  std::string port = "-port=" + std::to_string(prt);
  const char *aa[] = {"boost_asio_tan_alloc_test", "-task=AAAAAA", "-maxtask=15", "-inquire", "-alias=15", port.c_str(), 0};
  const char *bb[] = {"boost_asio_tan_alloc_test", "-task=BBBBBB", "-maxtask=15", "-inquire", "-alias=15", port.c_str(), 0};
  const char *cc[] = {"boost_asio_tan_alloc_test", "-task=CCCCCC", "-maxtask=15", "-inquire", "-alias=15", port.c_str(), 0};
  return tan_test(test, srv, port.c_str(), aa, bb, cc);
}

extern "C" int rtl_socket_tan_shutdown(int argc, char** argv)  {
  Online::util::MultiProcessTest test(argc, argv);
  ::lib_rtl_set_log_level(LIB_RTL_INFO);
  RTL::Process::setDebug(true);

  auto [pnum, srv] = test.start_tan();
  std::string port = "-port="+std::to_string(pnum);
  std::string node = "-node=" + RTL::nodeName();

  if ( srv->wait_for(3000) == RTL::Process::ENDED )  {
    int exit_status = srv->exit_status();
    std::cout << "+++ TAN nameserver ended prematurely: "
	      << RTL::errorString(exit_status) << std::endl;
    test.end();
    return exit_status;
  }
  auto* end = test.start_proc("TANShutdown", "online_test",
			      { "tan_nameserver_shutdown", "-nowait", "-quiet", node.c_str(), port.c_str() },
			      argc>1 ? "" : "/dev/null");
  test.do_wait({end, srv}, 2);
  std::cout << "Process TANServer " 
	    << (srv->is_running() ? "NOT ended [FAILURE]. " : "ended. ")
	    << "Status=" << srv->exit_status() << std::endl;
  std::cout << "Process TANShutdown " 
	    << (end->is_running() ? "NOT ended [FAILURE]. " : "ended. ")
	    << "Status=" << end->exit_status() << std::endl;
  std::cout << "All processes finished work.. " << std::endl;
  return test.end();
}

static std::string now()  {
  std::time_t now;
  now = ::time(0);
  std::string str = ::ctime(&now);
  return str.substr(0,str.length()-1) + " ";
}

extern "C" int rtl_amsc_qmtest(int argc, char** argv)  {
  Online::util::MultiProcessTest test(argc, argv);
  const char* logs   = test.logs.c_str();
  auto [pnum, srv]   = test.start_tan("stdout");
  std::string node   = "-node=" + RTL::nodeName();
  std::string port   = "-port=" + std::to_string(pnum);
  std::string rdrnam = "-name=READER_0x" + RTL::to_hex(pnum);
  std::string sndnam = "-name=SENDER_0x" + RTL::to_hex(pnum);
  std::string sendto = "-ssendto=READER_0x" + RTL::to_hex(pnum);

  std::cout << now() << "TAN nameserver on port:" << pnum << std::endl;
  RTL::Process::setDebug(true);
  auto* r = test.start_proc("READER_0", {"amsc_bounce", "-len=555", rdrnam, "-turns=5000", port}, logs);
  test.do_wait(r, 5);
  if ( r->is_running() )   {
    auto* s = test.start_proc("SENDER_0", {"amsc_bounce", "-len=555", sendto, sndnam, "-turns=5000", port}, logs);
    std::cout << now() << "Wait for processes ..... " << std::endl;
    s->wait(RTL::Process::WAIT_BLOCK);
    ::lib_rtl_sleep(5000);
    std::cout << now() << "SENDER_0: exit code: " << s->exit_status() << std::endl;
  }
  r->wait_for(RTL::Process::WAIT_BLOCK);
  std::cout << now() << "SENDER_0: exit code: " << r->exit_status() << std::endl;
  auto* p = test.start_proc("SHUTDOWN", {"tan_nameserver_shutdown", "-nowait", node, port}, nullptr);
  p->wait(RTL::Process::WAIT_BLOCK);
  srv->wait(RTL::Process::WAIT_BLOCK);
  test.process_group.kill();
  test.process_group.wait_for(3000);
  std::cout << now() << "All processes finished work.. " << std::endl << std::flush;
  return test.end(1);
}
