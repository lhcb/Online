//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================

// Framework inclde files
#include "Util.h"
#include <RTL/rtl.h>
#include <RTL/strdef.h>
#include <RTL/netdef.h>

// C/C++ include files
#include <iostream>
//  
//
//==========================================================================
namespace   {

  /// Class to test the TaskDB functionality
  /**
   *   \author  M.Frank
   *   \version 1.0
   */
  class TaskDBTest : public Online::util::MultiProcessTest  {
  public:
    std::string   snapshot   { };
    std::string   match      { };
    std::string   option     { };
    std::string   host       { "localhost" };
    std::string   mount      { "/TDBDATA/XMLRPC" };
    int           port       { 0 };
    int           timeout    { 20 };
    int           num_client { 0 };
    RTL::Process* server     { nullptr };
    void*         dllHandle  { nullptr };
    int (*test_taskdb_server)(int, const char**)   { nullptr };
  public:
    //==========================================================================
    TaskDBTest(int argc, char** argv, int p=-1) : MultiProcessTest(argc, argv), port(p)   {
      RTL::CLI cli(argc, argv, [](){});
      this->lib_name  = "libPcSrvLib.so";
      cli.getopt("host",     4, this->host);
      cli.getopt("port",     4, this->port);
      cli.getopt("match",    4, this->match);
      cli.getopt("mount",    4, this->mount);
      cli.getopt("option",   4, this->option);
      cli.getopt("snapshot", 4, this->snapshot);
      this->snapshot  = RTL::str_expand_env(this->snapshot);
      this->test_name = "0x"+RTL::to_hex(port, 4);
      ::setenv("DIM_DNS_NODE", this->host.c_str(), 1);
      dllHandle = ::dlopen("libPcSrvLib.so", RTLD_LAZY);
      test_taskdb_server = (int (*)(int, const char**)) ::dlsym(dllHandle, "rtl_check_taskdb_server");
      if ( nullptr == test_taskdb_server )   {
	throw std::runtime_error("Failed to load TaskDB server check: rtl_check_taskdb_server");
      }
    }
    //==========================================================================
    virtual ~TaskDBTest()  {
      if ( dllHandle ) ::dlclose(dllHandle);
    }
    //==========================================================================
    virtual int exit_server(int wait_seconds)  override  {
      if ( this->server )   {
	this->server->stop();
	this->server = nullptr;
      }
      this->do_wait(wait_seconds);
      return 1;
    }
    //==========================================================================
    int get_taskdb_port()  {
      if ( this->port <= 0 )   {
	for(int i=0; i<3; ++i)   {
	  auto [sc, prt] = RTL::get_free_server_port();
	  if ( 0 == sc )  {
	    this->port = prt;
	    this->test_name = "0x"+RTL::to_hex(port, 4);
	    return this->port;
	  }
	}
	throw std::runtime_error("Failed to allocate free port number!");
      }
      return this->port;
    }
    //==========================================================================
    RTL::Process* start_server()    {
      int port = this->get_taskdb_port();
      std::string utgid = "TDBSRV_" + this->test_name;
      std::string nod = "-node="+this->host;
      std::string mnt = "-mount="+this->mount;
      std::string prt = "-port="+std::to_string(port);
      auto* srv = this->start_proc(utgid,
				   {"run_pcsrv_cachedb",
				    "-threads=0",
				    mnt, nod, prt,
				    "-server=" + this->snapshot,
				    "-utgid="  + utgid }, "TaskDBServ.log");
      srv->wait_for(3000);
      if ( !srv->is_running() )   {
	//::lib_rtl_output(LIB_RTL_ALWAYS,"+++ Failed to start TaskDB server: %d",
	//		 this->server->exit_status());
	this->remove_process(srv);
	return nullptr;
      }
      std::vector<const char*> args  {"gentest", "libPcSrvLib.so",
				      "rtl_check_taskdb_server",
				      nod.c_str(), mnt.c_str(), prt.c_str()};
      this->server = srv;
      for(int i=0; i<10; ++i)   {
	int ret = (*test_taskdb_server)(args.size(), &args.at(0));
	// ::lib_rtl_output(LIB_RTL_ALWAYS,"+++ Called test_taskdb_server() result = %d.", ret);
	if ( ret == 0 )   {
	  ::lib_rtl_output(LIB_RTL_ALWAYS,"+++ Successfully tested TaskDB server. ret = %d", ret);
	  return this->server;
	}
	this->server->wait_for(2000);
      }
      return this->server;
    }
    //==========================================================================
    RTL::Process* start_client(const std::string& cmd,
			       const std::vector<std::string>& args)
    {
      std::string log   = "TDB_Client_"+std::to_string(num_client)+".log";
      std::string utgid = "TDB_Client_"+this->test_name+"_"+std::to_string(num_client);
      std::vector<std::string> all_args { cmd,
	"-host=localhost",
	"-port="   + std::to_string(this->port) };
      all_args.insert(all_args.end(), args.begin(), args.end());
      ++num_client;
      return start_proc(utgid, all_args, log.c_str());
    }
    //==========================================================================
    virtual int exec_preamble() override  {
      int retries = 3;
      for( int i=0; i < retries; ++i )   {
	auto* srv = this->start_server();
	if ( srv && srv->is_running() )   {
	  ::lib_rtl_output(LIB_RTL_INFO,
			   "+++ Started TaskDB server %s on host:%s port:%d",
			   srv->name().c_str(), "localhost", this->port);
	  return 1;
	}
	if ( !srv )   {
	  ::lib_rtl_output( (i == retries-1) ? LIB_RTL_ERROR : LIB_RTL_INFO,
			    "+++ TaskDB server port:%d not running. %s",
			    this->port, (i < retries-1) ? "--> restarting" : "");
	}
	else  {
	  ::lib_rtl_output( (i == retries-1) ? LIB_RTL_ERROR : LIB_RTL_INFO,
			    "+++ %s server port:%d not running: [%d: %s] %s",
			    srv->name().c_str(), this->port,
			    srv->exit_status(),
			    RTL::errorString(srv->exit_status()).c_str(),
			    (i < retries-1) ? "--> restarting" : "");
	}
	this->port = -1; // Reset port number to get new unique port.
	this->do_wait(5);
      }
      ::lib_rtl_output(LIB_RTL_INFO,
		       "+++ Cannot start server %s: [%s]",
		       this->server->name().c_str(),
		       RTL::errorString(this->server->exit_status()).c_str());
      return 0;
    }
  };
}
//
//==========================================================================
extern "C" int taskdb_start_server_qmtest(int argc, char** argv)    {
  return complex_test<TaskDBTest>(argc, argv, [](TaskDBTest&) { return 0; });
}
//
//==========================================================================
extern "C" int taskdb_connection_qmtest(int argc, char** argv)    {
  return complex_test<TaskDBTest>(argc, argv, [](TaskDBTest& test)  {
    auto* client = test.start_client("taskdb_test_connection", { "-show", "-loop="+test.option });
    return test.do_wait(client, test.timeout);
  });
}
//
//==========================================================================
extern "C" int taskdb_tasks_qmtest(int argc, char** argv)    {
  return complex_test<TaskDBTest>(argc, argv, [](TaskDBTest& test)  {
    auto* client = test.start_client("taskdb_task", { "-show", "-match="+test.match });
    return test.do_wait(client, test.timeout);
  });
}
//
//==========================================================================
extern "C" int taskdb_tasksets_qmtest(int argc, char** argv)    {
  return complex_test<TaskDBTest>(argc, argv, [](TaskDBTest& test)  {
    auto* client = test.start_client("taskdb_set", { "-show", "-match="+test.match });
    return test.do_wait(client, test.timeout);
  });
}
//  
//==========================================================================
extern "C" int taskdb_taskclasses_qmtest(int argc, char** argv)    {
  return complex_test<TaskDBTest>(argc, argv, [](TaskDBTest& test)  {
    auto* client = test.start_client("taskdb_class", { "-show", "-match="+test.match });
    return test.do_wait(client, test.timeout);
  });
}
//
//==========================================================================
extern "C" int taskdb_tasknodes_qmtest(int argc, char** argv)    {
  return complex_test<TaskDBTest>(argc, argv, [](TaskDBTest& test)  {
    auto* client = test.start_client("taskdb_node", { "-show", "-match="+test.match });
    return test.do_wait(client, test.timeout);
  });
}
//  
//==========================================================================
extern "C" int taskdb_tasks_in_set_qmtest(int argc, char** argv)    {
  return complex_test<TaskDBTest>(argc, argv, [](TaskDBTest& test)  {
    auto* client = test.start_client("taskdb_tasks_in_set", { "-show", "-match="+test.match });
    return test.do_wait(client, test.timeout);
  });
}
//
//==========================================================================
extern "C" int taskdb_tasksets_in_class_qmtest(int argc, char** argv)    {
  return complex_test<TaskDBTest>(argc, argv, [](TaskDBTest& test)  {
    auto* client = test.start_client("taskdb_tasksets_in_class", { "-show", "-match="+test.match });
    return test.do_wait(client, test.timeout);
  });
}
//
//==========================================================================
extern "C" int taskdb_classes_in_node_qmtest(int argc, char** argv)    {
  return complex_test<TaskDBTest>(argc, argv, [](TaskDBTest& test)  {
    auto* client = test.start_client("taskdb_classes_in_node", { "-show", "-match="+test.match });
    return test.do_wait(client, test.timeout);
  });
}
//
//==========================================================================
extern "C" int taskdb_tasks_in_class_qmtest(int argc, char** argv)    {
  return complex_test<TaskDBTest>(argc, argv, [](TaskDBTest& test)  {
    auto* client = test.start_client("taskdb_tasks_in_class", { "-show", "-match="+test.match });
    return test.do_wait(client, test.timeout);
  });
}
//
//==========================================================================
extern "C" int taskdb_tasks_in_node_qmtest(int argc, char** argv)    {
  return complex_test<TaskDBTest>(argc, argv, [](TaskDBTest& test)  {
    auto* client = test.start_client("taskdb_tasks_in_node", { "-show", "-match="+test.match });
    return test.do_wait(client, test.timeout);
  });
}
//
//==========================================================================
extern "C" int taskdb_tasks_match_node_qmtest(int argc, char** argv)    {
  return complex_test<TaskDBTest>(argc, argv, [](TaskDBTest& test)  {
    auto* client = test.start_client("taskdb_tasks_by_node", { "-show", "-match="+test.match });
    return test.do_wait(client, test.timeout);
  });
}
//
//==========================================================================
extern "C" int taskdb_python_api_qmtest(int argc, char** argv)    {
  return complex_test<TaskDBTest>(argc, argv, [](TaskDBTest& test)  {
    std::string log   = "TDB_Client_"+std::to_string(test.num_client)+".log";
    std::string utgid = "TDB_PYTHON_"+test.test_name+"_"+std::to_string(test.num_client);
    std::string con   = "http://"+test.host+":"+std::to_string(test.port)+test.mount;
    std::string cmd   = RTL::str_replace(test.option, "${CONNECTION}", con);

    ++test.num_client;
    ::lib_rtl_output(LIB_RTL_ALWAYS,
		     "+++ Execute taskdb_python_api_qmtest with command: \"%s\"", cmd.c_str());
    test.do_wait(5);
    auto* client = test.start_proc(utgid, test.command, "libgenPythonLib.so",
				   { "genPython", "-c", cmd }, log.c_str()); 
    return test.do_wait(client, test.timeout);
  });
}
//
//==========================================================================
