//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================

/// Framework include files
#include <SCR/scr.h>
#include "KeyboardEmulator.h"

/// Basic UPI test
extern "C" int qmtest_upi_basic(int /* argc */, char** /* argv */)  {
  Online::KeyboardEmulator test("gentest libUPI.so upi_test");
  test.hit_key(SCR::MOVE_DOWN);
  test.hit_return();
  test.hit_key(SCR::MOVE_DOWN);
  test.hit_return();
  test.move_up(2);
  return test.done();
}

/// Testing UPI detached menu
extern "C" int qmtest_upi_detached_menu(int /* argc */, char** /* argv */)  {
  Online::KeyboardEmulator test("gentest libUPI.so upi_test");
  test.move_down(8);
  test.hit_return().hit_return();
  test.hit_key(SCR::MOVE_DOWN).hit_return();
  test.hit_key(SCR::BACK_SPACE);
  test.move_up(9);
  return test.done();
}

/// Testing UPI detached menu
extern "C" int qmtest_upi_son_menu(int /* argc */, char** /* argv */)  {
  Online::KeyboardEmulator test("gentest libUPI.so upi_test");
  test.move_down(9);
  test.hit_return().hit_return();
  test.hit_key(SCR::BACK_SPACE);
  test.move_up(10);
  return test.done();
}

/// Testing UPI detached menu
extern "C" int qmtest_upi_move_menus(int /* argc */, char** /* argv */)  {
  Online::KeyboardEmulator test("gentest libUPI.so upi_test");
  test.move_right(5);
  test.move_down(2);
  test.hit_return(2);
  return test.done();
}

/// Testing UPI param menu 
extern "C" int qmtest_upi_param_menu(int /* argc */, char** /* argv */)  {
  Online::KeyboardEmulator test("gentest libUPI.so upi_test");
  test.move_down(20).hit_return();
  test.hit_key('2').hit_return();
  test.move_right().hit_return();
  test.move_up(20);
  return test.done();
}

/// Testing UPI delete menu 
extern "C" int qmtest_upi_delete_menu(int /* argc */, char** /* argv */)  {
  Online::KeyboardEmulator test("gentest libUPI.so upi_test");
  test.move_down(18);
  test.hit_key('2').hit_return();
  test.hit_key('3').hit_return();
  test.hit_key('4').hit_return();
  test.hit_key('5').hit_return();
  test.hit_key('6').hit_return();
  test.move_up(18);
  return test.done();
}

/// Testing UPI param menu 
extern "C" int qmtest_upi_delete_items(int /* argc */, char** /* argv */)  {
  Online::KeyboardEmulator test("gentest libUPI.so upi_test");
  test.move_down(10);
  test.hit_key(SCR::MOVE_RIGHT).hit_key('1').hit_return();
  test.hit_key(SCR::MOVE_LEFT);
  for( int i=0; i < 8; ++i )  {
    test.hit_key('1'+i).hit_return();
  }
  test.move_up(2);
  return test.done();
}

/// Testing UPI param menu 
extern "C" int qmtest_upi_insert_item(int /* argc */, char** /* argv */)  {
  Online::KeyboardEmulator test("gentest libUPI.so upi_test");
  test.move_down(20);
  for(int i=0; i<9; ++i)   {
    test.hit_return();
    test.hit_key(SCR::PAGE_UP);
    test.hit_key('1').hit_enter();
    test.move_down();
    test.hit_key('1').hit_enter();
    test.put_chars(std::to_string(i+70)).hit_enter();
    test.hit_enter();
    test.put_chars("Inserted text "+std::to_string(i)).hit_enter();
    test.put_chars("Inserted help "+std::to_string(i)).hit_enter();
    test.hit_return();
  }
  test.wait(5);
  test.move_up(30);
  return test.done();
}

