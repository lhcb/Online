//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================

// Framework include files
#include <TAN/TanInterface.h>
#include <RTL/Logger.h>
#include <RTL/strdef.h>
#include <RTL/netdef.h>
#include <RTL/hash.h>
#include "Util.h"

#include <random>
#include <filesystem>

extern "C" void online_tan_set_nameserver_port(int num);
namespace fs = std::filesystem;

/// Online nmespace declaration
namespace Online  {

  /// util namespace declaration
  namespace util  {

    /// Generate unique identifier
    long generate_unique_long_id()  {
      char text[128];
      const char* build_id = std::getenv("BUILD_ID");
      if ( build_id )  {
	::snprintf(text, sizeof(text), "%s--%d", build_id, ::lib_rtl_pid());
      }
      else  {
	::snprintf(text, sizeof(text), "%ld--%d", long(::clock()), ::lib_rtl_pid());
      }
      unsigned int val = RTL::hash32(text);
      long res = (long(val) << 31) + long(val);
      return res;
    }
    /// Generate unique identifier
    int generate_unique_int_id()  {
      return (int)(generate_unique_long_id()&0x7FFFFFFFUL);
    }
    /// Generate unique identifier
    short int generate_unique_short_id()  {
      return (short int)(generate_unique_long_id()&0x7FFFUL);      
    }
    /// Generate unique identifier
    std::string generate_unique_long_str()  {
      char text[64];
      ::snprintf(text, sizeof(text), "0x%016lX", generate_unique_long_id());
      return text;
    }
    /// Generate unique identifier
    std::string generate_unique_int_str()  {
      char text[64];
      ::snprintf(text, sizeof(text), "0x%08X", int(0x7FFFFFFFUL&generate_unique_int_id()));
      return text;
    }
    /// Generate unique identifier
    std::string generate_unique_short_str()  {
      char text[64];
      ::snprintf(text, sizeof(text), "0x%04X", (short int)(0x7FFFUL&generate_unique_short_id()));
      return text;
    }

    /// Get unique port number with offset generated from the PID
    int get_unique_port_number(int offset)  {
      int port = int(generate_unique_long_id()&0x7FFFUL) + offset;
      /// Only allow port range from ]800, 60000]
      if ( port < 1024 )  {
	return get_unique_port_number(offset+0x1000);
      }
      else if ( port > 60000 )  { // max is 65535
	return get_unique_port_number(offset-0x1000);
      }
      return port;
    }

    /// Generate unique TAN server port
    int generate_tan_port()  {
      static int port = -1;
      if ( -1 == port )  {
	while ( true )  {
	  auto [sc, prt] = RTL::get_free_server_port();
	  if ( 0 != sc )  {
	    prt = get_unique_port_number(0x1000);
	  }
	  else if ( prt < 8080 )   {
	    continue;
	  }
	  port = prt;
	  ::online_tan_set_nameserver_port(port);
	  ::setenv("TAN_PORT", std::to_string(port).c_str(), 1);
	  ::setenv("TAN_NODE", RTL::nodeNameShort().c_str(), 1);
	  return prt;
	}
      }
      return port;
    }
    
    /// Generate unique TAN server port
    std::string generate_tan_port_str()  {
      return std::to_string(generate_tan_port());
    }

    /// Run simple default MBM producer
    std::unique_ptr<RTL::Process> run_mbm_producer(const std::string& nam,
						   const std::string& cmd,
						   const char* args[],
						   const char* logs)  {
      ::lib_rtl_output(LIB_RTL_INFO, "Starting producer ...... ");
      auto prod = std::make_unique<RTL::Process>(nam, cmd, args, logs);
      prod->start();
      prod->wait(RTL::Process::WAIT_BLOCK);
      ::lib_rtl_output(LIB_RTL_INFO, "Producer finished work.. ");
      ::lib_rtl_sleep(1500);
      return prod;
    }

    /// Run MBM Summary process
    std::unique_ptr<RTL::Process> run_mbm_summary(const std::string& nam,
						  const std::string& cmd,
						  const char* args[])  {
      auto summary = std::make_unique<RTL::Process>(nam, cmd, args,"");
      summary->start();
      summary->wait(RTL::Process::WAIT_BLOCK);
      ::lib_rtl_output(LIB_RTL_INFO, "Summary task finished work.. ");
      return summary;
    }
    
    /// Run MBM Summary process
    std::unique_ptr<RTL::Process>
    run_mbm_summary(const std::string& nam,
		    const std::string& cmd,
		    const std::vector<std::string>& args)
    {
      auto summary = std::make_unique<RTL::Process>(nam, cmd, args,"");
      summary->start();
      summary->wait(RTL::Process::WAIT_BLOCK);
      ::lib_rtl_output(LIB_RTL_INFO, "Summary task finished work.. ");
      return summary;
    }
    
    /// Default constructor
    MultiProcessTest::MultiProcessTest(int argc, char** argv)  {
      RTL::CLI cli(argc, argv, [](){});
      std::string work_dir;
      this->logs  = (argc > 1) ? "" : "/dev/null";
      this->debug = cli.getopt("debug", 5);
      this->dns_port = this->get_unique_port(0);
      this->test_name = "ID" + RTL::to_HEX(generate_unique_int_id(), 8);
      cli.getopt("logs",          4, this->logs);
      cli.getopt("name",          4, this->test_name);
      cli.getopt("wait",          4, this->wait);
      cli.getopt("print",         5, this->print_level);
      cli.getopt("library",       7, this->lib_name);
      cli.getopt("gentest",       7, this->gentest_exe);
      cli.getopt("dns_exe",       7, this->dns_exe);
      cli.getopt("dns_node",      8, this->dns_node);
      cli.getopt("dns_port",      8, this->dns_port);
      cli.getopt("test_exe",      8, this->test_exe);
      cli.getopt("work_dir",      8, work_dir);
      cli.getopt("dns_wait_time", 8, this->dns_wait_time);
      RTL::Logger::set_io_buffering(RTL::Logger::LINE_BUFFERING);
      RTL::Logger::install_log(RTL::Logger::log_args(this->print_level));
      ::lib_rtl_enable("RTL_GDB_ON_FATAL", 1);
      this->command = OnlineBase::currentCommand();
      if ( this->debug )  {
	bool wait=1;
	while (wait)
	  ::lib_rtl_sleep(100);
      }
      if ( !work_dir.empty() )  {
	int ret = move_to_working_dir(work_dir);
	if ( ret ) ::exit(ret);
      }
    }

    /// Default destructor
    MultiProcessTest::~MultiProcessTest()  {
      this->process_group.removeAll();
    }

    /// Print process status of a single process
    void MultiProcessTest::print_process_status(const RTL::Process* proc)  const  {
      if ( proc->is_running() )  {
	::lib_rtl_output(LIB_RTL_DEBUG,
			 "(%s) +++ Process %s [pid:%d] is running.",
			 this->test_name.c_str(), proc->name().c_str(), proc->pid());
	return;
      }
      ::lib_rtl_output(LIB_RTL_DEBUG,
		       "(%s) +++ Process %s [pid:%d] finished. Exit code: %d",
		       this->test_name.c_str(), proc->name().c_str(), proc->pid(),
		       proc->exit_status());
    }
    
    /// Print process status of multiple processes
    void MultiProcessTest::print_process_status(const std::vector<RTL::Process*>& procs)  const  {
      for(const auto* p : procs)  {
	if ( p )  {
	  this->print_process_status(p);
	}
      }
    }

    /// Change working directory (create directory if not existent)
    int MultiProcessTest::move_to_working_dir(const std::string& work_dir)  const  {
      std::error_code ec;
      fs::path dir = fs::absolute(work_dir, ec);
      if ( !fs::exists(dir, ec) )  {
	if ( !fs::create_directories(dir, ec) )  {
	  ::lib_rtl_output(LIB_RTL_DEBUG,"+++ FAILED to create working directory: %s [%s]",
			   dir.c_str(), ec.message().c_str());
	  return ec.value();
	}
      }
      ::chdir(dir.c_str());
      fs::current_path(dir, ec);
      if ( ec )  {
	::lib_rtl_output(LIB_RTL_DEBUG,"+++ FAILED to change to working directory: %s [%s]",
			 dir.c_str(), ec.message().c_str());
	return ec.value();
      }
      return 0;
    }

    /// Get unique port number with offset generated from the PID
    int MultiProcessTest::get_unique_port(int offset)   const  {
      return get_unique_port_number(offset);
    }

    /// Start process with default executable, default library and specified name, args
    std::thread* MultiProcessTest::exec_call(const std::string& library,
					     const std::string& function,
					     const std::vector<std::string>& args)  const  {
      typedef int (*function_t)(int, char**);
      void *dll = LOAD_LIB(library.c_str());
      function_t func = nullptr;
      if ( dll )   {
	func = (function_t)GETPROC(dll, function.c_str());
	if ( func )   {
	  return new std::thread([func,args]  {
	      std::vector<char*> argv;
	      int argc = int(args.size());
	      for(const auto& a : args) argv.emplace_back((char*)a.c_str());
	      argv.push_back(0);
	      return (*func)(argc, &argv[0]);
	    });
	}
	throw std::runtime_error("Failed to access function "+function+" in library "+library);
      }
      throw std::runtime_error("Failed to load library "+library);
    }

    /// Start process with default executable, default library and specified name, args
    std::thread* MultiProcessTest::exec_call(const std::string& function,
					     const std::vector<std::string>& args)  const {
      return this->exec_call(this->lib_name, function, args);
    }

    /// Check if a process is really running
    int MultiProcessTest::check_process_running(RTL::Process* proc, bool print)  const  {
      if ( proc )  {
	if ( !proc->is_running() )  {
	  if ( print )  {
	    ::lib_rtl_output(LIB_RTL_ERROR,"+++ Process DIM_DNS [%s] is not running!",
			     proc->name().c_str());
	  }
	  return 0;
	}
      }
      return 1;
    }

    /// Prepare process with default executable, default library and specified name, args
    RTL::Process*
    MultiProcessTest::setup_proc(const std::string& proc_name,
				 const std::string& executable,
				 const std::string& library,
				 const std::vector<std::string>& args,
				 const char* output,
				 bool add_to_pg)
    {
      return this->setup_proc(proc_name, executable, library, args, output ? std::string(output) : this->logs, add_to_pg);
    }

    /// Prepare process with default executable, default library and specified name, args
    RTL::Process*
    MultiProcessTest::setup_proc(const std::string& proc_name,
				 const std::string& executable,
				 const std::string& library,
				 const std::vector<std::string>& args,
				 const std::string& output,
				 bool add_to_pg)
    {
      std::vector<std::string> arguments;
      if ( !library.empty() ) arguments.emplace_back(library);
      arguments.insert(arguments.end(), args.begin(), args.end());
      auto* proc = new RTL::Process(proc_name, executable, arguments, output);
      if ( add_to_pg )  {
	this->process_group.add(proc);
      }
      ::lib_rtl_output(LIB_RTL_DEBUG,"(%s) +++ Setup process: %s %",
		       this->test_name.c_str(), proc->bash_command().c_str(),
		       (add_to_pg) ? "and added to process group" : "");
      return proc;
    }

    /// Prepare process with default executable, default library and specified name, args
    RTL::Process*
    MultiProcessTest::setup_proc(const std::string& proc_name,
				 const std::string& executable,
				 const std::vector<std::string>& args,
				 const char* output,
				 bool add_to_pg)
    {
      return this->setup_proc(proc_name, executable, this->lib_name, args, output, add_to_pg);
    }

    /// Prepare process with default executable, default library and specified name, args
    RTL::Process*
    MultiProcessTest::setup_proc(const std::string& proc_name,
				 const std::string& executable,
				 const std::vector<std::string>& args,
				 const std::string& output,
				 bool add_to_pg)
    {
      return this->setup_proc(proc_name, executable, this->lib_name, args, output, add_to_pg);
    }

    /// Prepare process with default executable, default library and specified name, args
    RTL::Process*
    MultiProcessTest::setup_proc(const std::string& proc_name,
				 const std::vector<std::string>& args,
				 const char* output,
				 bool add_to_pg)
    {
      return setup_proc(proc_name, this->command, args, output, add_to_pg);
    }

    /// Prepare process with default executable, default library and specified name, args
    RTL::Process*
    MultiProcessTest::setup_proc(const std::string& proc_name,
				 const std::vector<std::string>& args,
				 const std::string& output,
				 bool add_to_pg)
    {
      return setup_proc(proc_name, this->command, args, output, add_to_pg);
    }

    /// Prepare process with default executable, default library and specified name, args
    RTL::Process*
    MultiProcessTest::setup_proc(const std::string& proc_name,
				 const std::vector<std::string>& args,
				 bool add_to_pg)  {
      return setup_proc(proc_name, args, nullptr, add_to_pg);
    }

    /// Prepare process with default executable, default library and specified name, args
    RTL::Process*
    MultiProcessTest::setup_proc(const std::string& proc_name,
				 const std::vector<std::string>& args)  {
      return this->setup_proc(proc_name, args, nullptr, true);
    }

    /// Start process with default executable, default library and specified name, args
    RTL::Process*
    MultiProcessTest::start_exe(const std::string& proc_name,
				const std::string& executable,
				const std::vector<std::string>& args,
				const char* output,
				const char* input)  {
      auto* proc = this->setup_proc(proc_name, executable, "", args, output, true);
      proc->setInput(input);
      this->process_group.start();
      return proc;
    }

    /// Start process with default executable, default library and specified name, args
    RTL::Process*
    MultiProcessTest::start_exe(const std::string& proc_name,
				const std::string& executable,
				const std::vector<std::string>& args)  {
      auto* proc = this->setup_proc(proc_name, executable, "", args, nullptr, true);
      this->process_group.start();
      return proc;
    }

    /// Start process with default executable, default library and specified name, args
    RTL::Process*
    MultiProcessTest::start_exe(const std::string& proc_name,
				const std::vector<std::string>& args,
				const char* output,
				const char* input)  {
      auto* proc = this->setup_proc(proc_name, this->command, "", args, output, true);
      proc->setInput(input);
      this->process_group.start();
      return proc;
    }

    /// Start process with default executable, default library and specified name, args
    RTL::Process*
    MultiProcessTest::start_exe(const std::string& proc_name,
				const std::vector<std::string>& args)  {
      auto* proc = this->setup_proc(proc_name, this->command, "", args, nullptr, true);
      this->process_group.start();
      return proc;
    }

    /// Start process with default executable, default library and specified name, args
    RTL::Process* 
    MultiProcessTest::start_proc(const std::string& proc_name,
				 const std::string& executable,
				 const std::string& library,
				 const std::vector<std::string>& args,
				 const std::string& output)  {
      return this->start_proc(proc_name, executable, library, args, output.c_str());
    }
    
    /// Start process with default executable, default library and specified name, args
    RTL::Process* 
    MultiProcessTest::start_proc(const std::string& proc_name,
				 const std::string& executable,
				 const std::string& library,
				 const std::vector<std::string>& args,
				 const char* output)  {
      auto* proc = this->setup_proc(proc_name, executable, library, args, output, true);
      this->process_group.start();
      return proc;
    }
    
    /// Start process with default executable, default library and specified name, args
    RTL::Process* 
    MultiProcessTest::start_proc(const std::string& proc_name,
				 const std::string& executable,
				 const std::vector<std::string>& args,
				 const std::string& output)  {
      return this->start_proc(proc_name, executable, this->lib_name, args, output.c_str());
    }

    /// Start process with default executable, default library and specified name, args
    RTL::Process* 
    MultiProcessTest::start_proc(const std::string& proc_name,
				 const std::string& executable,
				 const std::vector<std::string>& args,
				 const char* output)  {
      return this->start_proc(proc_name, executable, this->lib_name, args, output);
    }

    /// Start process with default executable, default library and specified name, args
    RTL::Process*
    MultiProcessTest::start_proc(const std::string& proc_name,
				 const std::vector<std::string>& args,
				 const std::string& output)  {
      return this->start_proc(proc_name, this->command, this->lib_name, args, output.c_str());
    }

    /// Start process with default executable, default library and specified name, args
    RTL::Process*
    MultiProcessTest::start_proc(const std::string& proc_name,
				 const std::vector<std::string>& args,
				 const char* output)  {
      return this->start_proc(proc_name, this->command, this->lib_name, args, output);
    }

    /// Start process with default executable, default library and specified name, args
    RTL::Process*
    MultiProcessTest::start_proc(const std::string& proc_name,
				 const std::vector<std::string>& args)  {
      return this->start_proc(proc_name, args, nullptr);
    }

    /// Remove a process from the process group and delete it
    bool MultiProcessTest::remove_process(RTL::Process* process)  {
      return this->process_group.remove(process);
    }
 
    /// Start target nameserver
    std::pair<unsigned short int, RTL::Process*>
    MultiProcessTest::start_tan(const char* output)  {
      int pid = -1;
      unsigned short port = 0;
      std::string    node = RTL::nodeName();
      std::string    name = "TANServer_"+test_name;
      for( int i=0; i < 5; ++i )  {
	std::unique_ptr<RTL::Process> proc;
	struct sockaddr_in addr { AF_INET, 0, { INADDR_ANY }, {0}};
	int socket = ::net_bind_free_server_port(&addr);
	port = addr.sin_port;
	::online_tan_set_nameserver_port(port);
	::setenv("TAN_PORT", std::to_string(port).c_str(), 1);
	::setenv("TAN_NODE", "localhost", 1);
	::close(socket);
	proc.reset(this->start_proc(name,
				    gentest_exe,
				    "libOnlineBase.so",
				    {"tan_nameserver","-tcp", "-a",
					"-q", std::to_string(port),
					"-p", "TAN_PUBAREA_ID" + RTL::to_HEX(port) },
				    output));
	pid = proc->pid();
	if ( proc->wait_for(this->dns_wait_time) != RTL::Process::ENDED )  {
	  if ( proc->is_running() )  {
	    return { port, proc.release() };
	  }
	}
	this->remove_process(proc.release());
      }
      ::lib_rtl_output(LIB_RTL_ERROR,"Failed to start TAN server %s (pid:%d port:%d) on %s",		       
		       name.c_str(), pid, port, node.c_str());
      throw std::runtime_error("Failed to start TAN server on %s" + node);
    }

    /// Start ASIO based target nameserver
    std::pair<unsigned short int, RTL::Process*>
    MultiProcessTest::start_asio_tan(const char* output)  {
      int pid = -1;
      unsigned short prt = 0;
      std::string    node = RTL::nodeName();
      std::string    name = "TANServer_"+test_name;
      for( int i=0; i < 5; ++i )  {
	std::unique_ptr<RTL::Process> proc;
	struct sockaddr_in addr { AF_INET, 0, { INADDR_ANY }, {0}};
	int socket = ::net_bind_free_server_port(&addr);
	std::string pnum  = std::to_string(addr.sin_port);
	std::string port  = "-port=" + pnum;
	std::string gbl   = "-pubarea=TAN_PUBAREA_=" + pnum;
	prt = addr.sin_port;
	::online_tan_set_nameserver_port(prt);
	::setenv("TAN_PORT", pnum.c_str(), 1);
	::setenv("TAN_NODE", "localhost", 1);
	::close(socket);
	proc.reset(this->start_proc(name,
				    gentest_exe,
				    "libOnlineBase.so",
				    {"boost_asio_tan_server", "-address=0.0.0.0", "-threads=10", port, gbl},
				    output));
	pid = proc->pid();
	if ( proc->wait_for(this->dns_wait_time) != RTL::Process::ENDED )  {
	  if ( proc->is_running() )  {
	    return { prt, proc.release() };
	  }
	}
	this->remove_process(proc.release());
      }
      ::lib_rtl_output(LIB_RTL_ERROR,"Failed to start TAN server %s (pid:%d port:%d) on %s",		       
		       name.c_str(), pid, prt, node.c_str());
      throw std::runtime_error("Failed to start TAN server on %s" + node);
    }

    /// Stop TAN name server
    int MultiProcessTest::stop_tan(RTL::Process* srv, const std::string& host, int port, const char* output)  {
      if ( srv )  {
	int ret = 1;
	if ( srv->is_running() )  {
	  auto* end = this->start_proc(this->test_name+"_TAN_shutdown",
				       {"tan_nameserver_shutdown",
					   "-nowait",
					   "-node=" + host,
					   "-port=" + std::to_string(port)}, output);
	  this->do_wait(end, 1500);
	  if ( srv->is_running() )  {
	    srv->stop();
	    ret = srv->wait_for(1000);
	    if ( !(ret == RTL::Process::ENDED || ret == RTL::Process::INVALID) )  {
	      srv->kill();
	      ret = srv->wait_for(1000);
	      if ( !(ret == RTL::Process::ENDED || ret == RTL::Process::INVALID) )  {
		this->remove_process(end);
		return 0;
	      }
	    }
	  }
	  this->remove_process(end);
	}
      }
      return 1;
    }

    /// Start DIM-DNS server
    RTL::Process*
    MultiProcessTest::start_dns(const char* output)  {
      for( int i=0; i < 5; ++i )   {
	struct sockaddr_in addr { AF_INET, 0, { INADDR_ANY }, {0}};
	int sock = ::net_bind_free_server_port(&addr);
	std::string pnum = std::to_string(addr.sin_port);
	std::string name = "DNS_"+test_name;
	::setenv("DIM_DNS_PORT", pnum.c_str(), 1);
	::setenv("DIM_DNS_NODE", dns_node.c_str(), 1);
	::close(sock);
	std::unique_ptr<RTL::Process> proc(this->start_proc(name, dns_exe, "", {}, output));
	if ( proc->wait_for(this->dns_wait_time) != RTL::Process::ENDED )  {
	  if ( proc->is_running() )  {
	    return proc.release();
	  }
	}
	this->remove_process(proc.release());
	::lib_rtl_output(LIB_RTL_DEBUG,
			 "+++ FAILED to start DIM_DNS on host:%s port:%s", dns_node.c_str(), pnum.c_str());
      }
      throw std::runtime_error("Failed to start DIM_DNS on "+dns_node+" after 5 attempts.");
    }

    /// Stop DIM DNS server
    int MultiProcessTest::stop_dns(RTL::Process* srv, int wait_seconds)  const  {
      if ( srv )  {
	if ( srv->is_running() )  {
	  srv->stop();
	}
	int status = srv->wait_for(1000);
	if ( !(status == RTL::Process::ENDED || status == RTL::Process::INVALID) )  {
	  srv->kill();
	}
	status = srv->wait_for(1000);
	if ( !(status == RTL::Process::ENDED || status == RTL::Process::INVALID) )  {
	}
      }
      return this->do_wait(wait_seconds);
    }

    /// Check if the DIM DNS is running
    int MultiProcessTest::check_dns(RTL::Process* dns, bool print)  const  {
      return check_process_running(dns, print);
    }

    /// Execute a number of processes and wait for them finishing
    int MultiProcessTest::do_execute(std::vector<RTL::Process*>&& procs, int secs_to_wait)  {
      std::size_t count = procs.size();
      for( std::size_t i=0; i < count; ++i )
	if ( procs[i] ) procs[i]->start();

      do_wait(3);
      this->process_group.wait(procs, RTL::Process::WAIT_BLOCK);
      this->print_process_status(procs);
      for( std::size_t i=0; i < count; ++i )  {
        if ( procs[i] )  {
	  delete procs[i];
	}
      }
      procs.clear();
      return this->do_wait(secs_to_wait);
    }

    /// Wait on specified processes to finish + optional sleep
    int MultiProcessTest::do_wait(RTL::Process* proc, int secs_to_wait)  {
      auto wait_msec = std::min(secs_to_wait*1000UL, this->process_group.get_max_wait_time());
      proc->wait_for(wait_msec);
      if ( proc->is_running() ) proc->killall();
      this->print_process_status(proc);
      return this->do_wait(1);
    }

    /// Wait on specified processes to finish + optional sleep
    int MultiProcessTest::do_wait(std::unique_ptr<RTL::Process>& proc, int secs_to_wait)  {
      return this->do_wait(proc.get(), secs_to_wait);
    }

    /// Wait on specified processes to finish + optional sleep
    int MultiProcessTest::do_wait(const std::vector<RTL::Process*>& procs, int secs_to_wait)  {
      auto wait_msec = std::min(secs_to_wait*1000UL, this->process_group.get_max_wait_time());
      this->process_group.wait(procs, wait_msec);
      for( auto* p : procs )
	p->stopall();
      this->print_process_status(procs);
      return this->do_wait(1);
    }

    /// Wait specified number of seconds before invoking shutdown.
    int MultiProcessTest::do_wait(int wait_seconds)  const  {
      if ( wait_seconds > 0 )  {
	for(wait_seconds *= 10; wait_seconds > 0; --wait_seconds )
	  ::lib_rtl_sleep(100);
      }
      return 0;
    }

    /// End test procedure. Wait specified number of seconds before invoking shutdown.
    int MultiProcessTest::end(int wait_seconds)  {
      this->do_wait(wait_seconds);
      this->process_group.kill();
      this->process_group.wait_for(this->process_group.get_max_wait_time());
      ::lib_rtl_output(LIB_RTL_INFO, "+++ All processes finished work...");
      this->do_wait(1);
      this->print_process_status(this->process_group.processes());
      this->process_group.removeAll();
      return 0;
    }
    
  }    // End namespace util
}      // End namespace Online
