//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
#ifndef GAUDIONLINETESTS_UTIL_H
#define GAUDIONLINETESTS_UTIL_H 1

// C/C++ include files
#include <iostream>
#include <memory>
#include <thread>

// Framework include files
#include <RTL/rtl.h>
#include <RTL/DllAccess.h>
#include <RTL/ProcessGroup.h>

/// Online nmespace declaration
namespace Online   {

  /// util namespace declaration
  namespace util  {

    /// Generate unique TAN server port
    int generate_tan_port();
    /// Generate unique TAN server port
    std::string generate_tan_port_str();

    /// Generate unique identifier
    short int generate_unique_short_id();
    /// Generate unique identifier
    std::string generate_unique_short_str();
    /// Generate unique identifier
    int generate_unique_int_id();
    /// Generate unique identifier
    std::string generate_unique_int_str();
    /// Generate unique identifier
    long int generate_unique_long_id();
    /// Generate unique identifier
    std::string generate_unique_long_str();

    /// Get unique port number with offset generated from the PID
    int get_unique_port_number(int offset);

    /// Run simple default MBM producer
    std::unique_ptr<RTL::Process>
      run_mbm_producer(const std::string& nam,
		       const std::string& cmd,
		       const char* args[],
		       const char* logs);

    /// Run MBM Summary process
    std::unique_ptr<RTL::Process>
      run_mbm_summary(const std::string& nam,
		      const std::string& cmd,
		      const char* args[]);
    
    /// Run MBM Summary process
    std::unique_ptr<RTL::Process>
      run_mbm_summary(const std::string& nam,
		      const std::string& cmd,
		      const std::vector<std::string>& args);
    
    /// Object to perform multi-process tests.
    /**
     *   \author  M.Frank
     *   \version 1.0
     */  
    class MultiProcessTest   {
    public:
      RTL::ProcessGroup process_group;
      std::string test_name     { "Test" };
      std::string lib_name      { };
      std::string dns_node      { "127.0.0.1" };
      std::string dns_exe       { "dns.exe" };
      int         dns_port      { -1 };
      std::string gentest_exe   { "gentest" };
      std::string test_exe      { "onlinetest" };
      std::string logs;
      std::string command;
      int         dns_wait_time { 7000 };
      int         wait          { 0 };
      int         print_level   { LIB_RTL_INFO };
      bool        debug         { false };

    public:
      /// Default constructor
      MultiProcessTest(int argc, char** argv);

      /// Default destructor
      virtual ~MultiProcessTest();
      
      /// Change working directory (create directory if not existent)
      int move_to_working_dir(const std::string& work_dir)  const;

      /// Print process status of a single process
      void print_process_status(const RTL::Process* proc)  const;
    
      /// Print process status of multiple processes
      void print_process_status(const std::vector<RTL::Process*>& procs)  const;

      /// Check if a process is really running
      int check_process_running(RTL::Process* process, bool print=true)  const;

      /// Prepare process with default executable, default library and specified name, args
      RTL::Process* setup_proc(const std::string& proc_name,
			       const std::string& executable,
			       const std::string& library,
			       const std::vector<std::string>& args,
			       const char* output,
			       bool add_to_pg);

      /// Prepare process with default executable, default library and specified name, args
      RTL::Process* setup_proc(const std::string& proc_name,
			       const std::string& executable,
			       const std::string& library,
			       const std::vector<std::string>& args,
			       const std::string& output,
			       bool add_to_pg);

      /// Prepare process with default executable, default library and specified name, args
      RTL::Process* setup_proc(const std::string& proc_name,
			       const std::string& executable,
			       const std::vector<std::string>& args,
			       const char* output,
			       bool add_to_pg);

      /// Prepare process with default executable, default library and specified name, args
      RTL::Process* setup_proc(const std::string& proc_name,
			       const std::string& executable,
			       const std::vector<std::string>& args,
			       const std::string& output,
			       bool add_to_pg);

      /// Prepare process with default executable, default library and specified name, args
      RTL::Process* setup_proc(const std::string& proc_name,
			       const std::vector<std::string>& args,
			       const std::string& output,
			       bool add_to_pg);

      /// Prepare process with default executable, default library and specified name, args
      RTL::Process* setup_proc(const std::string& proc_name,
			       const std::vector<std::string>& args,
			       const char* output,
			       bool add_to_pg);

      /// Prepare process with default executable, default library and specified name, args
      RTL::Process* setup_proc(const std::string& proc_name,
			       const std::vector<std::string>& args,
			       bool add_to_pg);

      /// Prepare process with default executable, default library and specified name, args
      RTL::Process* setup_proc(const std::string& proc_name,
			       const std::vector<std::string>& args);

      /// Start process with default executable, default library and specified name, args
      RTL::Process* start_exe(const std::string& proc_name,
			      const std::string& executable,
			      const std::vector<std::string>& args,
			      const char* output,
			      const char* input="");

      /// Start process with default executable, default library and specified name, args
      RTL::Process* start_exe(const std::string& proc_name,
			      const std::string& executable,
			      const std::vector<std::string>& args);

      /// Start process with default executable, default library and specified name, args
      RTL::Process* start_exe(const std::string& proc_name,
			      const std::vector<std::string>& args,
			      const char* output,
			      const char* input="");

      /// Start process with default executable, default library and specified name, args
      RTL::Process* start_exe(const std::string& proc_name,
			      const std::vector<std::string>& args);

      /// Start process with default executable, default library and specified name, args
      RTL::Process* start_proc(const std::string& proc_name,
			       const std::string& executable,
			       const std::string& library,
			       const std::vector<std::string>& args,
			       const char* output);

      /// Start process with default executable, default library and specified name, args
      RTL::Process* start_proc(const std::string& proc_name,
			       const std::string& executable,
			       const std::vector<std::string>& args,
			       const char* output);

      /// Start process with default executable, default library and specified name, args
      RTL::Process* start_proc(const std::string& proc_name,
			       const std::vector<std::string>& args,
			       const char* output);

      /// Start process with default executable, default library and specified name, args
      RTL::Process* start_proc(const std::string& proc_name,
			       const std::string& executable,
			       const std::string& library,
			       const std::vector<std::string>& args,
			       const std::string& output);

      /// Start process with default executable, default library and specified name, args
      RTL::Process* start_proc(const std::string& proc_name,
			       const std::string& executable,
			       const std::vector<std::string>& args,
			       const std::string& output);

      /// Start process with default executable, default library and specified name, args
      RTL::Process* start_proc(const std::string& proc_name,
			       const std::vector<std::string>& args,
			       const std::string& output);

      /// Start process with default executable, default library and specified name, args
      RTL::Process* start_proc(const std::string& proc_name,
			       const std::vector<std::string>& args);

      /// Remove a process from the process group and delete it
      bool remove_process(RTL::Process* process);

      /// Start process with default executable, default library and specified name, args
      std::thread* exec_call(const std::string& library,
			     const std::string& function,
			     const std::vector<std::string>& args)  const;

      /// Start process with default executable, default library and specified name, args
      std::thread* exec_call(const std::string& function,
			     const std::vector<std::string>& args)  const;

      /// Get unique port number with offset generated from the PID
      int get_unique_port(int offset)   const;

      /// Start target nameserver
      std::pair<unsigned short int, RTL::Process*> start_tan(const char* output="");

      /// Start ASIO based target nameserver
      std::pair<unsigned short int, RTL::Process*> start_asio_tan(const char* output="");

      /// Stop TAN name server
      int stop_tan(RTL::Process* srv, const std::string& host, int port, const char* output="");

      /// Start DIM-DNS server
      RTL::Process* start_dns(const char* output = nullptr);

      /// Stop DIM DNS server
      int stop_dns(RTL::Process* srv, int wait_seconds=0)  const;

      /// Check if the DIM DNS is running
      int check_dns(RTL::Process* dns, bool print=true)  const;

      /// Execute a number of processes and wait for them finishing
      int do_execute(std::vector<RTL::Process*>&& procs, int secs_to_wait=0);

      /// Wait specified number of seconds before invoking shutdown.
      int do_wait(int wait_seconds)  const;

      /// Wait on specified processes to finish + optional sleep
      int do_wait(RTL::Process* proc, int secs_to_wait);

      /// Wait on specified processes to finish + optional sleep
      int do_wait(std::unique_ptr<RTL::Process>& proc, int secs_to_wait);

      /// Wait on specified processes to finish + optional sleep
      int do_wait(const std::vector<RTL::Process*>& procs, int secs_to_wait);

      /// End test procedure. Wait specified number of seconds before invoking shutdown.
      int end(int wait_seconds=0);

      //==========================================================================
      virtual int exec_preamble()   {
	return 0;
      }
      //==========================================================================
      virtual int exit_server(int sleep=3)   {
	return this->end(sleep);
      }
    };
  }    // End namespace util
}      // End namespace Online

#include <functional>

namespace  {
  template <typename TEST_TYPE> inline
  int complex_test(int argc, char** argv, int (*function)(TEST_TYPE&))  {
    TEST_TYPE test(argc, argv);
    test.exec_preamble();
    auto ret = function(test);
    if ( test.wait > 0 )   {
      ::lib_rtl_output(LIB_RTL_INFO,
		       "+++ Wait %d seconds before stopping processes.", test.wait);
      test.do_wait(test.wait);
    }
    test.exit_server(1);
    test.end();
    return ret;
  }

  template <typename TEST_TYPE> inline
  int complex_test_func(int argc, char** argv, const std::function<int(TEST_TYPE&)>& function)  {
    TEST_TYPE test(argc, argv);
    test.exec_preamble();
    auto ret = function(test);
    if ( test.wait > 0 )   {
      ::lib_rtl_output(LIB_RTL_INFO,
		       "+++ Wait %d seconds before stopping processes.", test.wait);
      test.do_wait(test.wait);
    }
    test.exit_server(1);
    test.end();
    return ret;
  }
}

#endif // GAUDIONLINETESTS_UTIL_H
