//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
#include <boost/asio/io_context.hpp>
#include <boost/asio/write.hpp>
#include <boost/asio/buffer.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/placeholders.hpp>
#include <boost/asio/version.hpp>
#include <boost/bind/bind.hpp>
#include <array>
#include <string>
#include <iostream>
#include <stdexcept>
#include <RTL/rtl.h>

#if BOOST_ASIO_VERSION < 103400
#define ASIO_RESOLVER_QUERY(x,y)          {x,y}
#define ASIO_RESOLVER_RESULT              tcp::resolver::iterator
#define ASIO_GET_RESOLVER_RESULT(x)       (x)->endpoint()
#else
#define ASIO_RESOLVER_QUERY(x,y)          x,y
#define ASIO_GET_RESOLVER_RESULT(x)       *(x).begin()
#define ASIO_RESOLVER_RESULT              const tcp::resolver::results_type&
#endif

using namespace std;
namespace asio = boost::asio;
using namespace boost::asio::ip;

namespace   {

  // ===============================================================================
  class Async  {
  public:
    typedef boost::system::error_code error_code;
    tcp::socket tcp_socket;

  public:
    Async(asio::io_context& s) : tcp_socket(s) {}
    void resolve_handler(const error_code& ec, ASIO_RESOLVER_RESULT result);
    void connect_handler(const error_code &ec);
  };

  // ===============================================================================
  void Async::connect_handler(const error_code &ec) {
    if (!ec)  {
      ::printf("SUCCESS: Connected!\n");
      return;
    }
    printf("ERROR: Connection failed: %s\n",ec.message().c_str());
  }

  // ===============================================================================
  void Async::resolve_handler(const error_code &ec, ASIO_RESOLVER_RESULT result)  {
    if ( !ec )  {
      tcp::endpoint ep = ASIO_GET_RESOLVER_RESULT(result);
      ::printf("Try endpoint: Address: [%2s] %-32s Port: %4d\n",
	       ep.address().is_v4() ? "v4" : "v6",
	       ep.address().to_string().c_str(), ep.port());
      tcp_socket.async_connect(ep,
			       std::bind(&Async::connect_handler,
					 this,
					 std::placeholders::_1));
    }
    else   {
      ::printf("Endpoint resulution failed: %s\n",ec.message().c_str());
    }
  }
  void help()  {
    ::printf("asio_test_async_connect -opt [-opt]\n"
	     "   -node=<node>    Node alias to test\n"
	     "   -port=<number>  Port number for connection endpoint\n");
  }
}

// =================================================================================
extern "C" int asio_test_async_connect(int argc, char** argv)     {
  string node = "slavetasks.service.consul.lhcb.cern.ch";
  string port = "8000";
  RTL::CLI cli(argc, argv, help);
  cli.getopt("node", 1, node);
  cli.getopt("port", 1, port);

  asio::io_context ioservice;
  asio::ip::tcp::resolver resolv{ioservice};
  Async handler(ioservice);

  resolv.async_resolve(ASIO_RESOLVER_QUERY(node, port),
		       std::bind(&Async::resolve_handler,
				 &handler,
				 std::placeholders::_1,
				 std::placeholders::_2));
  ioservice.run();
  return 0;
}
