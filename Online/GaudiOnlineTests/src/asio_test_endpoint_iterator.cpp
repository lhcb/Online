//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
#include <boost/asio.hpp>
#include <boost/asio/version.hpp>
#include <iostream>
#include <RTL/rtl.h>

using namespace std;
namespace asio = boost::asio;

namespace {
  void help()  {
    ::printf("asio_test_endpoint_iterator -opt [-opt]\n"
	     "   -node=<node>    Node alias to test\n"
	     "   -port=<number>  Port number for connection endpoint\n");
  }
}

extern "C" int asio_test_endpoint_iterator(int argc, char** argv)     {
  string node = "slavetasks.service.consul.lhcb.cern.ch";
  string port = "8000";
  RTL::CLI cli(argc, argv, help);
  cli.getopt("node", 1, node);
  cli.getopt("port", 1, port);

  /// Step 1. Creating IO service
  asio::io_context               ios;
  /// Step 2. Creating a resolver. 
  asio::ip::tcp::resolver        resolver(ios);
  /// Used to store information about error that happens during the resolution process.
  boost::system::error_code      ec;
  /// Step 3. 
#if BOOST_ASIO_VERSION < 103400    
  auto conns = resolver.resolve({node, port}, ec);
#else
  auto conns = resolver.resolve(node, port, ec);
#endif
  /// Handling errors if any. 
  if ( ec ) { // Failed to resolve the DNS name. Breaking execution.
    cout << "Failed to resolve a DNS name. " << "Error code = " << ec.value() 
	 << ". Message = " << ec.message() << endl; 
    return ec.value();
  }
  int count = 0;
  for (auto it=conns.begin(), it_end=conns.end(); it != it_end; ++it)  {
    // Here we can access the endpoint like this.
    asio::ip::tcp::endpoint ep = it->endpoint();
    ::printf("Endpoint: Address: [%2s] %-32s Port: %4d\n",
	     ep.address().is_v4() ? "v4" : "v6",
	     ep.address().to_string().c_str(), ep.port());
    ++count;
  } 
  if ( count > 0 )   {
    ::printf("asio_test_endpoint_iterator: Successfully resolved node address.\n");
  }
  return 0;
}
