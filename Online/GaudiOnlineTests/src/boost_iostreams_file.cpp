//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================

/// Framework include files
#include <RTL/rtl.h>
#include <RTL/strdef.h>
#include "FileUtil.h"

/// C/C++ include files
#include <fstream>
#include <iostream>
#include <boost/iostreams/copy.hpp>
#include <boost/iostreams/filter/gzip.hpp>
#include <boost/iostreams/filter/zstd.hpp>
#include <boost/iostreams/filter/lzma.hpp>
#include <boost/iostreams/filter/bzip2.hpp>
#include <boost/iostreams/device/file.hpp>
#include <boost/iostreams/filtering_streambuf.hpp>

extern "C" int rtl_test_boost_iostream_write(int argc, char** argv) {
  RTL::CLI cli(argc, argv, []()  {
    ::printf("boost_iostreams_write_file -option [-option]    \n\n"
	     "       -input=<file-name>          Input file name        \n"
	     "       -output=<file-name>         Output file name       \n"
	     "       -strategy=<strategy>        Compression stratgegy  \n"
	     "                                   Technology dependent   \n"
	     "       -compression=<leve>         Compression level      \n"
	     "                                   Technology dependent   \n"
	     "\n" );
  });
  std::string input, output, debug;
  int strategy = 0, compression = -1;
  cli.getopt("debug",       5, debug);
  cli.getopt("input",       3, input);
  cli.getopt("output",      3, output);
  cli.getopt("compression", 3, compression);
  cli.getopt("strategy",    3, strategy);
  if ( input.empty() || output.empty() )  {
    cli.call_help();
    ::exit(EINVAL);
  }
  namespace bio = boost::iostreams;
  
  input = RTL::str_expand_env(input);
  output = RTL::str_expand_env(output);
  /// Read filename from the first command line argument
  std::ifstream  in_file(input,  std::ios_base::in  | std::ios_base::binary);
  std::ofstream out_file(output, std::ios_base::out | std::ios_base::binary);
  bio::filtering_streambuf<bio::output> out_ios;
  if ( cli.getopt("gzip", 4) )  {
    bio::gzip_params pars(compression<0 ? bio::zlib::default_compression : compression);
    out_ios.push(bio::gzip_compressor(pars));
  }
  else if ( cli.getopt("zstd", 4) )  {
    bio::zstd_params pars(compression<0 ? bio::zstd::default_compression : compression);
    out_ios.push(bio::zstd_compressor(pars));
  }
  else if ( cli.getopt("lzma", 4) )  {
    bio::lzma_params pars(compression<0 ? bio::lzma::default_compression : compression);
    out_ios.push(bio::lzma_compressor(pars));
  }
  else if ( cli.getopt("bzip2", 4) )  {
    bio::bzip2_params pars;
    out_ios.push(bio::bzip2_compressor(pars));
  }
  out_ios.push(out_file);

  /// Convert streambuf to ostream
  std::ostream writer(&out_ios);
  std::size_t count = 0;
  char buffer[4096];
  while ( in_file.good() )   {
    in_file.read(buffer, sizeof(buffer));
    int ret = in_file.gcount();
    if ( ret > 0 )   {
      writer.write(buffer, ret);
      count += ret;
    }
  }

  /// Cleanup
  bio::close(out_ios); // Don't forget this!
  out_file.close();
  lib_rtl_output(LIB_RTL_ALWAYS, "Write test successful. Wrote 0x%X bytes", count);
  return 0;
}

extern "C" int rtl_test_boost_iostream_read(int argc, char** argv) {
  RTL::CLI cli(argc, argv, []()  {
    ::printf("boost_iostreams_read_file -option [-option]    \n\n"
	     "       -input=<file-name>          Input file name        \n"
	     "       -output=<file-name>         Output file name       \n"
	     "       -source=<file-name>         Source file name       \n"
	     "\n" );
  });
  std::string input, output, source, debug;
  cli.getopt("debug",       5, debug);
  cli.getopt("input",       3, input);
  cli.getopt("output",      3, output);
  cli.getopt("source",      3, source);
  if ( input.empty() || output.empty() )  {
    cli.call_help();
    ::exit(EINVAL);
  }
  namespace bio = boost::iostreams;
  input = RTL::str_expand_env(input);
  output = RTL::str_expand_env(output);
  /// Read filename from the first command line argument
  std::ifstream  in_file(input,  std::ios_base::in  | std::ios_base::binary);
  std::ofstream out_file(output, std::ios_base::out | std::ios_base::binary);
  bio::filtering_streambuf<bio::input> in_ios;
  if ( cli.getopt("gzip", 4) )
    in_ios.push(bio::gzip_decompressor());
  else if ( cli.getopt("zstd", 4) )
    in_ios.push(bio::zstd_decompressor());
  else if ( cli.getopt("lzma", 4) )
    in_ios.push(bio::lzma_decompressor());
  else if ( cli.getopt("bzip2", 4) )
    in_ios.push(bio::bzip2_decompressor());
  in_ios.push(in_file);

  /// Convert streambuf to ostream
  std::size_t len = 0;
  std::istream reader(&in_ios);
  char buffer[4096];
  std::vector<uint8_t> result;
  while ( 1 )  {
    reader.read(buffer, sizeof(buffer));
    int ret = reader.gcount();
    if ( ret > 0 )  {
      out_file.write(buffer, ret);
    }
    if ( ret <= 0 )  {
      lib_rtl_output(LIB_RTL_ALWAYS, "Input file size: 0x%X [0x%X] bytes", len, result.size());
      break;
    }
    len += ret;
    std::copy(buffer, buffer+ret, std::back_inserter(result));
  }
  /// Cleanup
  out_file.close();
  if ( !source.empty() )   {
    Online::Datafile     src(RTL::str_expand_env(source));
    std::vector<uint8_t> src_buf = Online::util::load_file(src);
    return Online::util::compare_buffers(output, result,
					 src.filename(), src_buf);
  }
  return 0;
}
