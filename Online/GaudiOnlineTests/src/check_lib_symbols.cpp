//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
// -------------------------------------------------------------------------
//
// Usage: gentest libGaudiOnlineTests.so check_libc_symbols
//
//==========================================================================

#include <dlfcn.h>
#include <signal.h>
#include <cstdio>
#include <string>
#include <fstream>

using namespace std;

namespace {
  void check_sym(const char* hn, void* h, const char* name)    {
    void *sym = ::dlsym(h,name);
    ::printf("%-24s %32s: 0x%016lX %s\n",hn,name,
	     (unsigned long)sym,
	     (sym?"\t  FOUND" : "\t  NOT_FOUND"));
  }

  string get_lib(const char* match)   {
    ifstream in("/proc/self/maps");
    char text[1024];
    while(in.good())   {
      in.getline(text,sizeof(text));
      text[sizeof(text)-1] = 0;
      string line = text;
      if ( in.good() )   {
	size_t idx = line.find(match);
	if ( idx != string::npos )   {
	  while (line[idx] != ' ') --idx;
	  ++idx;
	  size_t idq = line.find(".so",idx);
	  string lib = line.substr(idx,idq+3-idx);
	  //::printf("Lib: %s\n",lib.c_str());
	  return lib;
	}
      }
    }
    return "";
  }
}

#define check(h,n) check_sym(#h,h,#n)

extern "C" int check_libc_symbols(int,char**)  {

  string LIBC = get_lib("/libc-");
  string LIBPTHREAD = get_lib("/libpthread-");

  void* libc_handle    = ::dlopen(LIBC.c_str(),RTLD_LAZY|RTLD_GLOBAL|RTLD_LOCAL);
  void* pthread_handle = ::dlopen(LIBPTHREAD.c_str(),RTLD_LAZY|RTLD_GLOBAL|RTLD_LOCAL);

  sigaction(1,0,0);

  ::printf("LIBC:\t%s\n", "" /* LIBC */);
  check(0,     __sigaction);
  check(libc_handle,     __GI___libc_sigaction);
  check(libc_handle,     __the_real_sigaction);
  check(libc_handle,     __sigaction);
  ::printf("LIBPTHREAD:\t%s\n", "" /* LIBPTHREAD */);
  check(pthread_handle,  pthread_create);
  check(pthread_handle,  __pthread_create_2_1);
  check(pthread_handle,  pthread_create@@GLIBC_2.2.5);
  check(pthread_handle,  __sigaction);
  check(pthread_handle,  pthread_sigmask);
  check(pthread_handle,  sigcancel_handler);
  check(pthread_handle,  __GI___pthread_register_cancel);
  check(pthread_handle,  __pthread_register_cancel);
  return 0;
}
