//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
//
// To run run-number service by hand:
// ./build.x86_64-centos7-gcc8-do0/run gentest libGaudiOnlineTests.so 
// dim_integer_service -server=DAQ -service=RunNumber -command=setRunNumber -inter
//
//==========================================================================

// C/C++ include files
#include <iostream>

// Framework include files
#include "RTL/rtl.h"
#include "dim/dis.h"
#include "dim/dic.h"

extern "C" int dim_integer_service(int argc, char** argv)  {

  using namespace std;
  struct Actor  {
    string server_name, service_name, command_name;
    int svc_id = 0;
    int cmd_id = 0;
    int runno = 0;
    static void help() {}
    static void feed_runno(void* tag, void* addr, int* size)   {
      if ( tag && addr && size )   {
	Actor* a = *(Actor**)tag;
	a->runno = *(int*)addr;
	cout << "New Run: " << a->runno << "!\n";
	::dis_update_service(a->svc_id);
      }
    }
  } actor;
  RTL::CLI cli(argc,argv,Actor::help);
  bool interactive = cli.getopt("interactive", 1);

  cli.getopt("server",  6, actor.server_name);
  cli.getopt("service", 6, actor.service_name);
  cli.getopt("command", 6, actor.command_name);

  actor.svc_id = ::dis_add_service((actor.server_name+"/"+actor.service_name).c_str(), "I", &actor.runno,sizeof(int),0,0);
  actor.cmd_id = ::dis_add_cmnd   ((actor.server_name+"/"+actor.command_name).c_str(), "I", Actor::feed_runno, long(&actor));
  ::dis_start_serving(actor.server_name.c_str());
  if ( interactive )   {
    while ( 1 )   {
      string param;
      cout << "Please, enter the new integer parameter: ";
      getline (cin, param);
      if ( param.empty() )  break;
      actor.runno = ::atol(param.c_str());
      cout << "New Run: " << actor.runno << "!\n";
      ::dis_update_service(actor.svc_id);
    }
    cout << "Exiting.....\n";
  }
  else   {
    while( 1 ) ::lib_rtl_sleep(100);
  }
  ::dis_remove_service(actor.svc_id);
  ::dis_remove_service(actor.cmd_id);
  return 0;
}
