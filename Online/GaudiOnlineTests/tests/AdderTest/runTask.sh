#!/bin/bash
# =========================================================================
#
#  Default script to start any task on the HLT farm.
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    20/05/2013
#
# =========================================================================
#
export UTGID=${UTGID}
if test -n "${LOGFIFO}"; then
    export LOGFIFO;
fi;
if test -n "${DIM_DNS_NODE}"; then
    export DIM_DNS_NODE;
fi;
if test -n "${DIM_DNS_HOST}"; then  # THIS NEEDS FIXING!!!!!
    export DIM_HOST_NODE=${DIM_DNS_HOST};
fi;
if test -z "${HOST}"; then
    export HOST="${DIM_HOST_NODE}";
fi;
#
if test -z "${ONLINERELEASE}"; then
    if test "`uname -a | grep el9`" != "";
    then
	export CMTCONFIG=x86_64_v2-el9-gcc12-do0;
    else
	export CMTCONFIG=x86_64_v2-centos7-gcc11-do0;
    fi;
    cd /group/online/dataflow/cmtuser/ONLINE/ONLINE_v7r21;
    . setup.${CMTCONFIG}.vars;
    export LC_ALL=C;
fi;
#
. ${FARMCONFIGROOT}/job/createEnvironment.sh  $*;
#
if test -p /dev/shm/logs.dev; then
    export LOGFIFO=/dev/shm/logs.dev;
fi;
#
export STATIC_OPTS=${FARMCONFIGROOT}/options;
if test -z "${DYNAMIC_OPTS}"; then
    export DYNAMIC_OPTS=/group/online/dataflow/options/${PARTITION_NAME};
fi;
if test -z "${DATAINTERFACE}"; then
    DATAINTERFACE=`python /group/online/dataflow/scripts/getDataInterface.py`;
fi;
export DATAINTERFACE;
#
export PREAMBLE_OPTS=${FARMCONFIGROOT}/options/Empty.opts;
export ONLINETASKS=/group/online/dataflow/templates;
export INFO_OPTIONS=${DYNAMIC_OPTS}/OnlineEnv.opts;
export MBM_SETUP_OPTIONS=${DYNAMIC_OPTS}/MBM_setup.opts;
export PYTHONPATH=${DYNAMIC_OPTS}:${PYTHONPATH};
export MONITORING_DISTBOX=MON0101;
# ls -laF ${DYNAMIC_OPTS};
#
echo "${UTGID} [INFO] TASK_TYPE:          ${TASK_TYPE}";
echo "${UTGID} [INFO] FARMCONFIGROOT:     ${FARMCONFIGROOT}";
echo "${UTGID} [INFO] INFO_OPTIONS:       ${INFO_OPTIONS}";
echo "${UTGID} [INFO] MBM_SETUP_OPTIONS:  ${MBM_SETUP_OPTIONS}";
echo "${UTGID} [INFO] $*";
export LOGFIFO="/dev/shm/logs.dev";
#
if test "${TASK_TYPE}" = "Controller"; then
    echo "${UTGID} [INFO] Starting controller ${FARMCONFIGROOT}/job/Controller.sh";
    cd ${FARMCONFIGROOT};
    . ${FARMCONFIGROOT}/job/Controller.sh $*;
else
    cd ${GAUDIONLINETESTSROOT}/tests/AdderTest;
    export ARCHITECTURE=`pwd`/Architecture.xml;
    exec -a ${UTGID} genPython.exe `which gaudirun.py`   \
	${GAUDIONLINETESTSROOT}/tests/AdderTest/test.py  \
	--application=Online::OnlineApplication;
fi;
