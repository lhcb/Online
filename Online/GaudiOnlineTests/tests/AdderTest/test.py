#==========================================================================
#  LHCb Online software suite
#--------------------------------------------------------------------------
# Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
# All rights reserved.
#
# For the licensing terms see OnlineSys/LICENSE.
#
# Author     : M.Frank
#
#==========================================================================
import os

#--------------------------------------------------------------------------
def start_test_process():
  from GaudiOnlineTests import GO as go
  app = go.GauchoHistoProcess(counters=True, histograms=True, pause=None, timer=1, prefill=10)
  return app

#--------------------------------------------------------------------------
def Writer():
  return start_test_process()

#--------------------------------------------------------------------------
def Reader():
  return start_test_process()

#--------------------------------------------------------------------------
def Histograms():
  return start_test_process()

#--------------------------------------------------------------------------
def _Adder():
  return start_test_process()

#--------------------------------------------------------------------------
def Adder():
  from GaudiOnlineTests import GO as go
  os.environ['ADDER_TYPE'] = 'AdderTest'
  arch = os.getenv('ARCHITECTURE', None)
  app = go.Adder(architecture=arch)
  return app

#--------------------------------------------------------------------------
def setup_application():
  print('Starting adder test application')
  args=''
  for i in range(10):
    val = os.getenv('ARG'+str(i), None)
    if val:
      args = ',' + val
  task = os.getenv('TASK_TYPE', print) + '('+args[1:]+')'
  print('Starting adder test application: '+task)
  eval(task)

#--------------------------------------------------------------------------
setup_application()
