//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================
#ifndef HTTP_HTTP_ASIO_H
#define HTTP_HTTP_ASIO_H

// C/C++ include files
#include <boost/asio.hpp>

// Helpers for backwards compatibility
namespace boost   {
  namespace asio  {
#if BOOST_ASIO_VERSION < 103400 // 1.34.0
    using io_context = io_service;
#endif
    namespace detail  {
#if BOOST_VERSION >= 107200
      inline std::size_t buffer_size(const mutable_buffer& b) {  return b.size();      }
      inline std::size_t buffer_size(const const_buffer& b)   {  return b.size();      }
#elif BOOST_ASIO_VERSION >= 101200
      inline void* buffer_cast(const mutable_buffer& b)       {  return b.data();      }
      inline const void* buffer_cast(const const_buffer& b)   {  return b.data();      }
      inline std::size_t buffer_size(const mutable_buffer& b) {  return b.size();      }
      inline std::size_t buffer_size(const const_buffer& b)   {  return b.size();      }
#else
      inline void* buffer_cast(const mutable_buffer& b)       {  return buffer_cast_helper(b); }
      inline const void* buffer_cast(const const_buffer& b)   {  return buffer_cast_helper(b); }
      inline std::size_t buffer_size(const mutable_buffer& b) {  return buffer_size_helper(b); }
      inline std::size_t buffer_size(const const_buffer& b)   {  return buffer_size_helper(b); }
#endif
    }
  }
}

/// Namespace for the http server and client apps
namespace http   {
  /// Forward declarations
  class HttpReply;

  /// Convert the reply into a vector of buffers. The buffers do not own the
  /// underlying memory blocks, therefore the reply object must remain valid and
  /// not be changed until the write operation has completed.
  std::vector<boost::asio::const_buffer> to_buffers(const HttpReply& rep);
}      // End namespace http

#endif  // HTTP_HTTP_ASIO_H

