//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================
// Please note:
// This implementation was directly deduced (with some changes) from 
// one of the boost::asio examples. 
// boost::asio: one of the best I/O libraries ever !
//
// Hence:
//
//==========================================================================
#ifndef HTTP_HTTP_CACHE_H
#define HTTP_HTTP_CACHE_H

/// Framework include files
#include <HTTP/HttpReply.h>

/// C/C++ include files
#include <ctime>
#include <mutex>
#include <string>
#include <memory>
#include <cstdint>
#include <climits>
#include <unordered_map>
#include <sys/time.h>

/// Namespace for the http server and client apps
namespace http   {

  /// A abstract data cache to speed up HTTP requests
  /**
   *  \author  M.Frank
   *  \version 1.0
   *  \date    10.01.2021
   */
  template <typename DATA> class Cache   {
  public:
    /// Internal class representing a single cache entry
    class entry_type   {
    public:
      struct timeval timeout   { std::numeric_limits<time_t>::max(), 0 };
      std::int64_t length      { 0  };
      std::size_t  hash        { 0u };
      std::string  path        { };
      std::string  encoding    { };
      DATA         data        { };

    public:
      /// Default constructor
      entry_type() = default;
      /// Move constructor
      entry_type(entry_type&& copy) = default;
      /// Inhibit copy constructor
      entry_type(const entry_type& copy) = delete;
      /// Default destructor
      ~entry_type() = default;
      /// Move assignment operator
      entry_type& operator=(entry_type&& copy) = default;
      /// Inhibit assignment operator
      entry_type& operator=(const entry_type& copy) = delete;

      /// Check validity
      bool is_valid(const struct timeval& test)  const;
    };

  public:
    typedef size_t key_type;
    typedef std::unordered_map<key_type, std::unique_ptr<entry_type> > cache_type;

  public:
    cache_type             cache;
    std::mutex             lock;
    std::hash<std::string> hash;

    class stats_type   {
    public:
      std::size_t  cache_drop   { 0 };
      std::size_t  cache_update { 0 };
      std::size_t  cache_insert { 0 };
      std::size_t  cache_hit    { 0 };
      std::size_t  cache_miss   { 0 };
    };
    mutable stats_type counters;
    
  protected:
    void _set(entry_type*           e,
	      key_type              key,
	      const struct timeval& timeout,
	      const std::string&    path,
	      const std::string&    encoding,
	      const DATA&           reply);

  public:
    /// Default constructor
    Cache();
    /// Inhibit move constructor
    Cache(Cache&& copy) = delete;
    /// Inhibit copy constructor
    Cache(const Cache& copy) = delete;
    /// Inhibit move assignment operator
    Cache& operator=(Cache&& copy) = delete;
    /// Inhibit assignment operator
    Cache& operator=(const Cache& copy) = delete;
    /// Default destructor
    virtual ~Cache();
    /// Access number of entries in the cache
    std::size_t size()  const;
    /// Find a cache entry by path. [Not thread safe, unlocked]
    const entry_type* find(const std::string& key,
			   const std::string& encoding)  const;
    /// Find a cache entry by hash key [Not thread safe, unlocked]
    const entry_type* find(key_type key,
			   const std::string& encoding)  const;
    /// Insert new entry into the cache [Not thread safe, unlocked]
    bool insert(key_type key,
		const struct timeval&  timeout,
		const std::string&     path,
		const std::string&     encoding,
		const http::HttpReply& reply);
    /// Drop a cache entry by key  [Not thread safe, unlocked]
    std::size_t drop(key_type key);
    /// Drop a cache entry by key, encoding  [Not thread safe, unlocked]
    std::size_t drop(key_type key, const std::string& encoding);

    /// Clean "old" entries from cache [Thread safe, locked]
    std::size_t drop_expired();
    /// Drop cache entries by key  [Thread safe, locked]
    std::size_t drop_key(key_type key);
    /// Drop a cache entries by key  [Thread safe, locked]
    std::size_t drop_keys(const std::vector<key_type>& key);
  };
}      // End namespace http
#endif // HTTP_HTTP_CACHE_H
