//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include <HTTP/Cache.h>

// C/C++ include files
#include <cstring>

namespace http   {

  /// Check validity
  template <typename DATA> inline 
    bool Cache<DATA>::entry_type::is_valid(const struct timeval& now)  const  {
    if ( now.tv_sec  <  this->timeout.tv_sec )
      return true;
    if ( now.tv_sec  == this->timeout.tv_sec &&
	 now.tv_usec <=  this->timeout.tv_usec )
      return true;
    return false;
  }

  /// Initializing constructor
  template <typename DATA> Cache<DATA>::Cache()   {
  }

  /// Default destructor
  template <typename DATA> Cache<DATA>::~Cache()    {
  }

  /// Access number of entries in the cache
  template <typename DATA> std::size_t Cache<DATA>::size()  const    {
    return this->cache.size();
  }

  /// Find a cache entry by path
  template <typename DATA> const typename Cache<DATA>::entry_type*
    Cache<DATA>::find(const std::string& key_name, const std::string& encoding)  const  {
    return this->find(this->hash(key_name), encoding);
  }

  /// Find a cache entry by hash key
  template <typename DATA> const typename Cache<DATA>::entry_type*
    Cache<DATA>::find(key_type key, const std::string& encoding)  const  {
    auto range = this->cache.equal_range(key);
    for( auto it = range.first; it != range.second; ++it )   {
      if( encoding.find(it->second->encoding) != std::string::npos )   {
	++this->counters.cache_hit;
	return it->second.get();
      }
    }
    ++this->counters.cache_miss;
    return nullptr;
  }

  /// Drop a cache entry by key
  template <typename DATA> std::size_t Cache<DATA>::drop(key_type key)    {
    auto& c = this->cache;
    std::size_t removed = 0;
    for( auto it = c.find(key); it != c.end(); it = c.find(key) )   {
      c.erase(it);
      ++this->counters.cache_drop;
      ++removed;
    }
    return removed;
  }

  /// Drop a cache entry by key
  template <typename DATA> std::size_t Cache<DATA>::drop(key_type key, const std::string& encoding)    {
    auto range = this->cache.equal_range(key);
    for( auto it = range.first; it != range.second; ++it )   {
      if( encoding.find(it->second->encoding) != std::string::npos )   {
	this->cache.erase(it);
	++this->counters.cache_drop;
	return 1;
      }
    }
    return 0;
  }

  /// Insert new entry into the cache
  template <typename DATA>
    bool Cache<DATA>::insert(key_type               key,
			     const struct timeval&  tmo,
			     const std::string&     path,
			     const std::string&     encoding,
			     const HttpReply& reply)
    {
      auto range = cache.equal_range(key);
      for( auto it = range.first; it != range.second; ++it )   {
	if( encoding == it->second->encoding )   {
	  _set(it->second.get(), key, tmo, path, encoding, reply);
	  ++this->counters.cache_update;
	  return false;
	}
      }
      auto e = std::make_unique<entry_type>();
      _set(e.get(), key, tmo, path, encoding, reply);
      this->cache.emplace(key, std::move(e));
      ++this->counters.cache_insert;
      return true;
    }

  /// Clean "old" entries from cache [Thread safe, locked]
  template <typename DATA>
    std::size_t Cache<DATA>::drop_key(key_type key)    {
    std::lock_guard<std::mutex> lck(this->lock);
    return this->drop(key);
  }

  /// Drop a cache entries by key  [Thread safe, locked]
  template <typename DATA>
    std::size_t Cache<DATA>::drop_keys(const std::vector<key_type>& keys)    {
    std::size_t cnt = 0;
    Cache<DATA>::cache_type& c = this->cache;
    std::lock_guard<std::mutex> lck(this->lock);
    for( auto key : keys )   {
      for( auto it = c.find(key); it != c.end(); it = c.find(key) ) {
	++this->counters.cache_drop;
	c.erase(it);
	++cnt;
      }
    }
    return cnt;
  }

  /// Clean "old" entries from cache [Thread safe, locked]
  template <typename DATA>
    size_t Cache<DATA>::drop_expired()    {
    key_type    last = 0;
    std::size_t cnt  = 0;
    struct timeval now;
    Cache<DATA>::cache_type& c = this->cache;
    ::gettimeofday(&now, nullptr);
    std::lock_guard<std::mutex> lck(this->lock);
    for( auto it = c.begin(); it != c.end(); ++it )   {
      auto* e = it->second.get();
      if ( !e->is_valid(now) )   {
	++cnt;
	c.erase(it);
	++this->counters.cache_drop;
	it = (last != 0) ? c.find(last) : c.begin();
	if ( it == c.end() ) break;
      }
      last = it->first;
    }
    return cnt;
  }
}

