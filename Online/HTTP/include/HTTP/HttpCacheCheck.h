//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================
#ifndef HTTP_HTTP_HTTPCACHECHECK_H
#define HTTP_HTTP_HTTPCACHECHECK_H

/// Framework include caches
#include <HTTP/Asio.h>

/// Namespace for the http server and client apps
namespace http   {

  class HttpCacheHandler;
  
  /// A cachecheck to cache http requests
  /**
   *  \author  M.Frank
   *  \version 1.0
   *  \date    10.01.2021
   */
  class HttpCacheCheck   {
  protected:    
    boost::asio::deadline_timer timer;
    http::HttpCacheHandler&     handler;
    int                         timeout  { 100 };
  public:
    /// Initializing constructor. Start timer
    HttpCacheCheck(http::HttpCacheHandler& hdlr, boost::asio::io_context& io, int tmo);
    /// Initializing constructor. Does not start timer.
    HttpCacheCheck(http::HttpCacheHandler& hdlr, boost::asio::io_context& io);
    /// Inhibit move construction
    HttpCacheCheck(HttpCacheCheck&& copy) = delete;
    /// Inhibit copy construction
    HttpCacheCheck(const HttpCacheCheck& copy) = delete;
    /// Inhibit move assignment
    HttpCacheCheck& operator=(HttpCacheCheck&& copy) = delete;
    /// Inhibit copy assignment
    HttpCacheCheck& operator=(const HttpCacheCheck& copy) = delete;
    /// Default destructor
    virtual ~HttpCacheCheck();
    /// Rearm timer after single shot
    void start();
    /// Rearm timer after single shot with new timeout
    void start(int tmo);
    /// Default cleanup handler: drop expired items
    virtual void check();
  };
}      // End namespace http
#endif // HTTP_HTTP_HTTPCACHECHECK_H
