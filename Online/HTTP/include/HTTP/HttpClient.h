//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================
#ifndef HTTP_HTTPCLIENT_H
#define HTTP_HTTPCLIENT_H

// Framework include files
#include <HTTP/HttpHeader.h>

/// Namespace for the http based implementation
namespace http  {

  ///  Client class based on the HTTP protocol
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.05.2017
   */
  class HttpClient  {
  public:

    typedef http::HttpHeader Header;
    /// Connection parameters
    std::string         host;
    std::string         port    = "8000";
    std::string         mount   = "/RPC2";
    std::string         contentType;

    /// Useful HTTP Header information
    //mutable std::string contentEncoding;
    //mutable int         contentLength = 0;

    int                 timeout = 10000;
    int                 debugFlag   = 0;
    std::vector<Header> userRequestHeaders;
    std::vector<Header> userReplyHeaders;

  public:
    /// Default constructor
    HttpClient() = default;
    /// Move constructor
    HttpClient(HttpClient&& copy) = default;
    /// Copy constructor
    HttpClient(const HttpClient& copy) = default;
    /// Assignment operator
    HttpClient& operator=(HttpClient&& copy) = default;
    /// Assignment operator
    HttpClient& operator=(const HttpClient& copy) = default;
    /// Initializing constructor: No need to call open if this constructor is issued
    HttpClient(const std::string& host, int port = 8000, int tmo=10000);
    /// Initializing constructor: No need to call open if this constructor is issued
    HttpClient(const std::string& host, const std::string& port = "8000", int tmo=10000);
    /// Initializing constructor: No need to call open if this constructor is issued
    HttpClient(const std::string& host, const std::string& mount, int port, int tmo=10000);
    /// Initializing constructor: No need to call open if this constructor is issued
    HttpClient(const std::string& host, const std::string& mount, const std::string& port, int tmo=10000);
    /// Default destructor
    virtual ~HttpClient();

    /// Modify debug flag
    virtual int setDebug(int value);
    /// Access debug flag
    virtual int debug()  const;
    /// Access name
    virtual std::string name()  const;
    /// Set the caller properties
    void open(const std::string& host, int port);
    /// Set the caller properties
    void open(const std::string& host, int port, int tmo);
    /// Connect client to given URI and execute command (Bridge call)
    virtual std::vector<unsigned char> request(const std::string& request_data)  const;
    /// Connect client to given URI and execute RPC call
    virtual std::vector<unsigned char> request(const void* request_data, size_t len)  const;
    /// Connect client to given URI and execute RPC call
    virtual std::vector<unsigned char> request(const void* request_data, size_t len, 
					       const std::string& acceptedEncoding,
					       std::string& dataEncoding)  const;
  };
}
#endif  // RPC_HTTPRPCCLIENT_H

