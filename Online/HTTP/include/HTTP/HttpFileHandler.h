//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================
// Please note:
// This implementation was directly deduced (with some changes) from 
// one of the boost::asio examples. 
// boost::asio: one of the best I/O libraries ever !
//
// Hence:
//
//==========================================================================
#ifndef HTTP_HTTP_HTTPFILEHANDLER_H
#define HTTP_HTTP_HTTPFILEHANDLER_H

/// Framework include files
#include <HTTP/HttpServer.h>
#include <HTTP/HttpCacheHandler.h>
#include <HTTP/Cache.h>

/// C/C++ include files
#include <cstdint>

/// Namespace for the http server and client apps
namespace http   {

  namespace mime_types {
    /// Convert a file extension into a MIME type.
    std::string extension_to_type(const std::string& extension);
  } // namespace mime_types

  /// A filehandler to submit files to clients
  /**
   *  \author  M.Frank
   *  \version 1.0
   *  \date    10.01.2021
   */
  class HttpFileHandler : public HttpRequestHandler, public HttpCacheHandler
  {
  public:
    /// Structure steering data compression if enabled
    compression_params_t compression;
    
  protected:
    std::string mount;
    std::string file_root;

    /// Read file data and stuff them into the reply structure
    http::Reply load_file(const std::string& path, off_t length)   const;

    /// Read directory data data and stuff them into the reply structure
    http::Reply load_dir(const std::string& path)   const;

  public:
    /// Initializing constructor
    HttpFileHandler(const std::string& mount, const std::string& file_root);

    /// Inhibit default construction
    HttpFileHandler() = delete;
    /// Inhibit move construction
    HttpFileHandler(HttpFileHandler&& copy) = delete;
    /// Inhibit copy construction
    HttpFileHandler(const HttpFileHandler& copy) = delete;
    /// Inhibit move assignment
    HttpFileHandler& operator=(HttpFileHandler&& copy) = delete;
    /// Inhibit copy assignment
    HttpFileHandler& operator=(const HttpFileHandler& copy) = delete;
    
    /// Handle a request and produce a reply.
    virtual continue_action handle_request(const http::Request& req, http::Reply& rep) override;
  };
}      // End namespace http
#endif // HTTP_HTTP_HTTPFILEHANDLER_H
