//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================
#ifndef HTTP_HTTP_HEADER_H
#define HTTP_HTTP_HEADER_H

// C/C++ include files
#include <string>
#include <vector>

/// Namespace for the http based RPC implementation
namespace http  {

  /// HttpHeader is a very basic header and included by all apps.
  /// Define the constants here:
  static constexpr const std::size_t BYTE      = 1;
  static constexpr const std::size_t kBYTE     = 1024;
  static constexpr const std::size_t kiloBYTE  = kBYTE;
  static constexpr const std::size_t MBYTE     = kBYTE*1024;
  static constexpr const std::size_t MegaBYTE  = kBYTE*1024;
  static constexpr const std::size_t GBYTE     = MBYTE*1024;
  static constexpr const std::size_t GigaBYTE  = MBYTE*1024;
  static constexpr const std::size_t TBYTE     = GBYTE*1024;
  static constexpr const std::size_t TeraBYTE  = GBYTE*1024;
  static constexpr const std::size_t PBYTE     = TBYTE*1024;
  static constexpr const std::size_t PetaBYTE  = TBYTE*1024;

  /// Basic string constants for interpreting HTTP headers, etc.
  /**
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.05.2017
   */
  class constants  {
  public:
    static constexpr const char* DATE_TIME_FORMAT = "%a, %d %b %Y %T %Z";
    static constexpr const char* direct           = "Direct";
    static constexpr const char* location         = "Location";
    static constexpr const char* date             = "Date";
    static constexpr const char* error_cause      = "Error-Cause";
    static constexpr const char* content_type     = "Content-Type";
    static constexpr const char* content_length   = "Content-Length";
    static constexpr const char* content_location = "Content-Location";
    static constexpr const char* data_length      = "Data-Length";
    static constexpr const char* state            = "Object-State";
    static constexpr const char* request_uri      = "Request-Uri";
    static constexpr const char* expect           = "Expect";
    static constexpr const char* del              = "DELETE";
    static constexpr const char* get              = "GET";
    static constexpr const char* put              = "PUT";
    static constexpr const char* post             = "POST";
    static constexpr const char* update           = "UPDATE";
  };
  
  /// HTTP header object definition
  /**
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.05.2017
   */
  class HttpHeader  final  {
  public:
    /// Header name
    std::string name;
    /// Header value
    std::string value;

    /// Initializing constructor
    HttpHeader(const char* n, char* v);
    /// Initializing constructor
    HttpHeader(const char* n, const char* v);
    /// Initializing constructor
    HttpHeader(const std::string& n, const std::string& v);
    /// Initializing constructor
    template <typename T> HttpHeader(const std::string& n, const T& v);
    /// Initializing constructor
    template <typename T> HttpHeader(const std::string& n, const T* v);
    /// Copy construction is allowed here
    HttpHeader(const HttpHeader&) = default;
    /// Assignment is allowed here
    HttpHeader& operator=(const HttpHeader&) = default;
    /// Move construction is allowed here
    HttpHeader(HttpHeader&&) = default;
    /// Move assignment is allowed here
    HttpHeader& operator=(HttpHeader&&) = default;
    /// Construct ready to parse the request method.
    HttpHeader() = default;

    /// Convert header value to time counter (time_t)
    time_t as_time()  const;
    /// Convert header value to generic primitive type
    template<typename T> T as()   const;

    /// Formatted printout with prefix
    std::ostream& print(std::ostream& os, const std::string& prefix)  const;    
    /// Access date-time string
    static std::string now();
    /// Access date-time string
    static std::string date_time(time_t time);
  };

  /// Initializing constructor
  inline HttpHeader::HttpHeader(const std::string& n, const std::string& v)
    : name(n), value(v)   {}

  /// Initializing constructor
  inline HttpHeader::HttpHeader(const char* n, const char* v)
    : name(n), value(v)   {}

  /// Initializing constructor
  inline HttpHeader::HttpHeader(const char* n, char* v)
    : name(n), value(v)   {}

  std::vector<unsigned char> to_vector(const char* data);
  std::vector<unsigned char> to_vector(const std::string& data);
  std::string                to_string(const unsigned char* data, size_t len);
}      // End namespace http
#endif // HTTP_HTTP_HEADER_H

