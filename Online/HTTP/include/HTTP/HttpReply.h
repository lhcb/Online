//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================
// Please note:
// This implementation was directly deduced (with some changes) from 
// one of the boost::asio examples. 
// boost::asio: one of the best I/O libraries ever !
//
// Hence:
//
//==========================================================================
//
// Copyright (c) 2003-2015 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//
//==========================================================================
#ifndef HTTP_HTTP_HTTPREPLY_H
#define HTTP_HTTP_HTTPREPLY_H

// Framework include files
#include <HTTP/HttpHeader.h>

// C/C++ include files
#include <vector>
#include <chrono>
#include <memory>

/// Namespace for the http server and client apps
namespace http   {

  /// Forward declarations
  std::vector<unsigned char> to_vector(const std::string& data);
  std::vector<unsigned char> to_vector(const char* data);

  /// A reply to be sent to a client.
  /**
   *  \author  M.Frank
   *  \version 1.0
   *  \date    10.01.2021
   */
  class HttpReply  final  {
  public:
    /// The status of the reply.
    enum status_type     {
	continuing = 100,
	switching_protocols = 101,
	processing = 102,
	early_hints = 103,
	
	ok = 200,
	created = 201,
	accepted = 202,
	no_content = 204,
	reset_content = 205,
	partial_content = 206,
	multi_status = 207,
	already_reported = 208,
	
	multiple_choices = 300,
	moved_permanently = 301,
	moved_temporarily = 302,
	see_other = 303,
	not_modified = 304,
	use_proxy = 305,
	switch_proxy = 306,
	temp_redirect = 307,
	permanent_redirect = 308,
	
	bad_request = 400,
	unauthorized = 401,
	payment_required = 402,
	forbidden = 403,
	not_found = 404,
	method_not_allowed = 405,
	not_acceptable = 406,
	proxy_authentication_required = 407,
	request_timeout = 408,
	conflict = 409,
	gone = 410,
	length_required = 411,
	precondition_failed = 412,
	payload_too_large = 413,
	uri_too_long = 414,
	unsupported_media_type = 415,
	range_not_satisfiable = 416,
	expectation_failed = 417,
	i_am_a_teapot = 418,
	misdirected_request = 421,
	unprocessable_entity = 422,
	locked = 423,
	failed_dependency = 424,
	too_early = 425,
	upgrade_required = 426,
	precondition_required = 427,
	too_many_requests = 429,
	request_header_fields_too_large = 431,
	unavailable_for_legal_reasons = 451,
	
	internal_server_error = 500,
	not_implemented = 501,
	bad_gateway = 502,
	service_unavailable = 503,
	gateway_timeout = 504,
	http_version_not_supported = 505,
	variant_also_negotiates = 506,
	insufficient_storage = 507,
	loop_detected = 508,
	not_extended = 510,
	network_authentication_required = 511
    } status;

    /// Optional reply context
    /**
     *  \author  M.Frank
     *  \version 1.0
     *  \date    10.01.2021
     */
    class Context  {
    public:
      virtual ~Context() = default;
    };
    typedef std::unique_ptr<Context> context_t;
    
    /// The headers to be included in the reply.
    std::vector<HttpHeader>    headers;

    /// The optional user defined headers to be included in the reply.
    std::vector<HttpHeader>    userHeaders;

    /// The content to be sent in the reply.
    std::vector<unsigned char> content;

    /// Reply context for server extensions
    context_t                  context;
    
    size_t bytes_total {0};
    size_t bytes_sent  {0};
    size_t bytes_data  {0};

  public:
    /// Get a stock reply.
    static HttpReply stock_reply(status_type status);
    /// Access HTTP header string
    static std::string stock_status(status_type status);

    /// Move construction is allowed here
    HttpReply(HttpReply&&)  = default;
    /// Move assignment is allowed here
    HttpReply& operator = (HttpReply&&) = default;
    /// Copy construction is not allowed
    HttpReply(const HttpReply&) = delete;
    /// Assignment is not allowed
    HttpReply& operator = (const HttpReply&) = delete;
    /// Construct ready to parse the request method.
    HttpReply() = default;
    /// Default destructor
    ~HttpReply() = default;
    
    /// Access header by name
    const HttpHeader* header(const std::string& name)    const;

    /// Clone reply
    HttpReply clone()   const;
    
    /// Formatted header information (all except content) printout with prefix
    std::ostream& print_header(std::ostream& os, const std::string& prefix)  const;
    /// Formatted header information (all except content) printout with prefix
    std::ostream& print_content(std::ostream& os, const std::string& prefix)  const;
    /// Formatted printout with prefix
    std::ostream& print(std::ostream& os, const std::string& prefix)  const;
  };
  typedef HttpReply   Reply;

  std::string to_string(const HttpReply& status);
  std::vector<unsigned char> to_vector(HttpReply::status_type status);
}      // End namespace http
#endif // HTTP_HTTP_HTTPREPLY_H
