//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================
// Please note:
// This implementation was directly deduced (with small changes) from 
// one of the boost::asio examples. 
// boost::asio: one of the best I/O libraries ever !
//
// Hence:
//
//==========================================================================
//
// Copyright (c) 2003-2015 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//
//==========================================================================
#ifndef HTTP_HTTP_HTTPSERVER_H
#define HTTP_HTTP_HTTPSERVER_H

// Framework include files
#include <HTTP/HttpHeader.h>
#include <HTTP/HttpRequest.h>
#include <HTTP/HttpReply.h>
#include <HTTP/Asio.h>

// C/C++ include files
#include <set>
#include <mutex>
#include <string>
#include <memory>
#include <thread>

// Boost inlcude files
#include <boost/noncopyable.hpp>

/// Namespace for the http server and client apps
namespace http   {

  /// Forward declarations
  class HttpConnection;
  class HttpConnectionManager;
  class HttpRequestParser;
  class HttpRequestHandler;

  /// Parser for incoming requests.
  /**
   *  \author  M.Frank
   *  \version 1.0
   *  \date    10.01.2021
   */
  class HttpRequestParser : private boost::noncopyable    {
  public:
    size_t consumed = 0;
    /// The current state of the parser.
    enum state_types      {
      method_start,
      method,
      uri,
      http_version_h,
      http_version_t_1,
      http_version_t_2,
      http_version_p,
      http_version_slash,
      http_version_major_start,
      http_version_major,
      http_version_minor_start,
      http_version_minor,
      expecting_newline_1,
      header_line_start,
      header_lws,
      header_name,
      space_before_header_value,
      header_value,
      expecting_newline_2,
      expecting_newline_3
    } state = method_start;

    /// Result of parse.
    enum result_type { good = 1, bad = 2, indeterminate = 4 };

    /// NO copy construction is allowed here
    HttpRequestParser(const HttpRequestParser&) = delete;
    /// NO assignment is allowed here
    HttpRequestParser& operator=(const HttpRequestParser&) = delete;
    /// NO move construction is allowed here
    HttpRequestParser(HttpRequestParser&&) = delete;
    /// NO move assignment is allowed here
    HttpRequestParser& operator=(HttpRequestParser&&) = delete;
    /// Construct ready to parse the request method.
    HttpRequestParser() = default;

    /// Parse some data. The enum return value is good when a complete request has
    /// been parsed, bad if the data is invalid, indeterminate when more data is
    /// required. The InputIterator return value indicates how much of the input
    /// has been consumed.
    template <typename InputIterator>
    std::tuple<result_type, InputIterator> parse(HttpRequest& req,
						 InputIterator begin,
						 InputIterator end)    {
      size_t len = end-begin;
      //consumed = 0;
      while (begin != end)	{
	++consumed;
	result_type result = consume(req, *begin++);
	if (result == good || result == bad)     {
	  if ( len > consumed )   {
	    req.content.reserve(len);
	    std::copy(begin, end, std::back_inserter(req.content));
	  }
	  return std::make_tuple(result, begin);
	}
      }
      return std::make_tuple(indeterminate, begin);
    }

  private:
    /// Handle the next character of input.
    result_type consume(HttpRequest& req, char input);
    /// Check if a byte is an HTTP character.
    static bool is_char(int c)      {  return c >= 0 && c <= 127;  }
    /// Check if a byte is an HTTP control character.
    static bool is_ctl(int c)       {  return (c >= 0 && c <= 31) || (c == 127); }
    /// Check if a byte is defined as an HTTP tspecial character.
    static bool is_tspecial(int c);
    /// Check if a byte is a digit.
    static bool is_digit(int c)     {  return c >= '0' && c <= '9';  }
  };

  /// Represents a single connection from a client.
  /**
   *  \author  M.Frank
   *  \version 1.0
   *  \date    10.01.2021
   */
  class HttpConnection :
    public std::enable_shared_from_this<HttpConnection>,
    private boost::noncopyable
  {
  public:
    /// NO copy construction is allowed here
    HttpConnection(const HttpConnection&) = delete;
    /// NO assignment is allowed here
    HttpConnection& operator=(const HttpConnection&) = delete;

    /// NO move construction is allowed here
    HttpConnection(HttpConnection&&) = delete;
    /// NO move assignment is allowed here
    HttpConnection& operator=(HttpConnection&&) = delete;

    /// Construct a connection with the given socket.
    explicit HttpConnection(boost::asio::ip::tcp::socket socket,
			    HttpConnectionManager& manager,
			    HttpRequestHandler& handler,
			    size_t buffer_size);
    /// Default destructor
    virtual ~HttpConnection();

    /// Start the first asynchronous operation for the connection.
    virtual void start()    {      do_read();         }
    /// Stop all asynchronous operations associated with the connection.
    virtual void stop();

    /// Send synchronously direct reply
    virtual void send(const HttpReply& reply, boost::system::error_code& ec);
    /// Send synchronously asio buffer via socket
    virtual void send(const boost::asio::const_buffer& buffer, boost::system::error_code& ec);
    /// Send synchronously multiple asio buffers via socket
    virtual void send(const std::vector<boost::asio::const_buffer>& buffers, boost::system::error_code& ec);
    
    /// The handler used to process the incoming request.
    HttpRequestHandler& get_handler()   { return handler;   }

  private:
    /// Execute follow-up action 
    void do_next(int next_action);
    
    /// Perform an graceful shutdown of the connection
    void do_shutdown();
    
    /// Perform an close operation
    void do_close();
    
    /// Perform an asynchronous read operation.
    void do_read();

    /// Perform an asynchronous read operation with exact read length
    void do_read_exact(size_t len);

    /// Perform an asynchronous read operation without parsing the header
    void do_read_data();

    /// Perform an asynchronous write operation.
    void do_write();

    /// Helper routine
    void print_next(int next_action)  const;

    /// Socket for the connection.
    boost::asio::ip::tcp::socket socket;

    /// The manager for this connection.
    HttpConnectionManager& manager;

    /// The handler used to process the incoming request.
    HttpRequestHandler& handler;

    /// Buffer for incoming data.
    std::vector<unsigned char> buffer;

    /// Request parser
    HttpRequestParser parser;

    /// The incoming request.
    HttpRequest request;

    /// The reply to be sent back to the client.
    HttpReply reply;
  };
  typedef std::shared_ptr<HttpConnection> HttpConnectionPtr;

  /// Manages open connections so that they may be cleanly stopped when the server needs to shut down.
  /**
   *  \author  M.Frank
   *  \version 1.0
   *  \date    10.01.2021
   */
  class HttpConnectionManager : private boost::noncopyable   {
  public:
    /// NO copy construction is allowed here
    HttpConnectionManager(const HttpConnectionManager&) = delete;
    /// NO assignment is allowed here
    HttpConnectionManager& operator=(const HttpConnectionManager&) = delete;

    /// NO move construction is allowed here
    HttpConnectionManager(HttpConnectionManager&&) = delete;
    /// NO move assignment is allowed here
    HttpConnectionManager& operator=(HttpConnectionManager&&) = delete;

    /// Construct the default connection manager.
    HttpConnectionManager();

    /// Add the specified connection to the manager and start it.
    void start(HttpConnectionPtr c);

    /// Stop the specified connection.
    void stop(HttpConnectionPtr c);

    /// Stop all connections.
    void stop_all();

    /// Number of active connections
    size_t numConnections()  const {   return connections.size();  }
    
  private:
    /// The managed connections.
    std::set<HttpConnectionPtr> connections;
    /// Protection lock for the connection set
    std::mutex connection_lock;
  };

  /// The common handler for all incoming requests.
  /**
   *  Note:
   *  Depending on the concrete functionality of the server
   *  this base class must be sub-classed. 
   *
   *  One knownconcrete implementation is e.g. the xmlrpc hander.
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    10.01.2021
   */
  class HttpRequestHandler : private boost::noncopyable    {
  protected:
  public:
    typedef std::vector<std::shared_ptr<std::thread> > Threads;

    enum continue_action { none, write, read, close_read, close, close_write, invalid };

    typedef std::atomic<long> counter_t;
    typedef HttpRequest       request_t;
    typedef HttpReply         reply_t;

    /// Object Lock to ensure we have no race conditions when editing the call-map
    std::mutex                        lock;
    /// Thread pool if running in multi-threaded mode
    Threads                           threads;
    /// The io_context used to perform asynchronous operations.
    boost::asio::io_context           io_context;
    /// The signal_set is used to register for process termination notifications.
    boost::asio::signal_set           signals;
    /// Acceptor used to listen for incoming connections.
    boost::asio::ip::tcp::acceptor    acceptor;
    /// The connection manager which owns all live connections.
    HttpConnectionManager             manager;
    /// The next socket to be accepted.
    boost::asio::ip::tcp::socket      socket;
    /// Number of currently active connections
    counter_t                         num_connections         {0};
    /// Number of ever opened connections
    counter_t                         num_connections_opened  {0};
    /// Number of ever closed connections
    counter_t                         num_connections_closed  {0};
    /// Number of ever stopped connections
    counter_t                         num_connections_stopped {0};
    /// Number of active threads
    size_t                            num_threads             {0};
    /// Default buffer size of the connection
    size_t                            buffer_size             {16*kiloBYTE};
    /// Enable debugging if necessary
    int                               debug                   {0};

    /// Structure holding data compression properties
    struct compression_params_t   {
      /// Minimal content size in bytes for keeping entries in the cache
      size_t minContentLength   { 1*kiloBYTE };
      /// Flag to enable compression
      bool   enable             { true };
    };
    
  protected:
    /// Perform an asynchronous accept operation.
    void do_accept();      
    /// Wait for a request to stop the server.
    void do_await_stop();
    /// If we can gzip the output, here it is done
    bool compress_reply(const HttpRequest& req, HttpReply& reply)  const;
    /// If we can gzip the output, here it is done. Returns used encoding
    bool compress_reply(const HttpRequest& req, HttpReply& reply, std::string& encoding)  const;
    /// Get date and time string for use in headers
    std::string get_date_string()  const;

  public:
    /// NO copy construction is allowed here
    HttpRequestHandler(const HttpRequestHandler&) = delete;
    /// NO assignment is allowed here
    HttpRequestHandler& operator=(const HttpRequestHandler&) = delete;
    /// NO move construction is allowed here
    HttpRequestHandler(HttpRequestHandler&&) = delete;
    /// NO move assignment is allowed here
    HttpRequestHandler& operator=(HttpRequestHandler&&) = delete;

    /// Construct with a directory containing files to be served.
    explicit HttpRequestHandler();
    /// Default destructor
    virtual ~HttpRequestHandler();

    /// Wait until all threads finished to exit server cleanly
    virtual void join();
    /// Run the server's io_context loop. If specified additional workers may be used
    virtual void run(int num_additional_threads);
    /// Open the server connection and enable client interactions
    virtual void open(const std::string& address, const std::string& port);

    /// Specific handler for GET requests
    virtual continue_action handle_get(const HttpRequest& req, HttpReply& rep);
    /// Specific handler for PUT requests
    virtual continue_action handle_put(const HttpRequest& req, HttpReply& rep);
    /// Specific handler for POST requests
    virtual continue_action handle_post(const HttpRequest& req, HttpReply& rep);
    /// Specific handler for DELETE requests
    virtual continue_action handle_delete(const HttpRequest& req, HttpReply& rep);
    /// Specific handler for UPDATE requests
    virtual continue_action handle_update(const HttpRequest& req, HttpReply& rep);
    /// Handle a request and produce a reply.
    virtual continue_action handle_request(const HttpRequest& req, HttpReply& rep);
    /// Callback to eventually handle actions on connection shutdown
    virtual void            handle_shutdown(const HttpRequest& req, const HttpReply& rep);
    /// Handle possible statistics summaries.
    virtual void handle_stat(const HttpRequest& req, HttpReply& rep);
  };

  ///  Basic HTTP server interface implementation
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.02.2021
   */
  template <typename T> class basic_http_server   {
  public:
    /// Handler class specification
    class handler_t;
    /// Reference to the implementation interface
    std::unique_ptr<handler_t> implementation;
    /// Server bind address
    std::string     host;
    /// Server bind port
    std::string     port;

  public:
    /// Default constructor
    basic_http_server(const std::string& host, const std::string& port);
    /// Move constructor
    basic_http_server(basic_http_server&& copy) = default;
    /// Move operator
    basic_http_server& operator=(basic_http_server&& copy) = default;
    /// NO copy constructor
    basic_http_server(const basic_http_server& copy) = delete;
    /// NO copy operator
    basic_http_server& operator=(const basic_http_server& copy) = delete;
    /// Default destructor
    virtual ~basic_http_server();
    /// Access handler
    handler_t& handler()               {  return *this->implementation;    }
    /// Access handler
    const handler_t& handler()  const  {  return *this->implementation;    }
    /// Set connection buffer size
    virtual size_t set_buffer_size(size_t length);
    /// Modify debug flag
    virtual int    set_debug(int value);
    /// Start the xmlrpc service
    virtual void   start(bool detached = false, int num_additional_threads = 0);
    /// Run the xmlrpc service (only use in conjunction with start(false); ) 
    /** Use only to execute the callback loop in a seperate thread.
     *  Run the server's io_context loop. If specified additional workers may be used
     */
    virtual void   run(int num_additional_threads = 0);
    /// Stop the xmlrpc service
    virtual void   stop();
  };

  /// Initializing constructor
  template <typename T> basic_http_server<T>::basic_http_server(const std::string& h, const std::string& p)
    : implementation(new handler_t()), host(h), port(p)
  {
  }

  /// Default destructor
  template <typename T> basic_http_server<T>::~basic_http_server() {
    implementation.reset();
  }

  /// Modify debug flag
  template <typename T> int basic_http_server<T>::set_debug(int value)  {
    int tmp = implementation->debug;
    implementation->debug = value;
    return tmp;
  }

  /// Set connection buffer size
  template <typename T> size_t basic_http_server<T>::set_buffer_size(size_t value)  {
    size_t tmp = implementation->buffer_size;
    implementation->buffer_size = value;
    return tmp;
  }

  /// Start the xmlrpc service
  template <typename T> void basic_http_server<T>::start(bool detached, int num_additional_threads)   {
    implementation->open(host, port);
    implementation->num_threads = num_additional_threads;
    if ( detached )   {
      return;
    }
    implementation->run(num_additional_threads);
  }

  /// Run the server's io_context loop. If specified additional workers may be used
  template <typename T> void basic_http_server<T>::run(int num_additional_threads)   {
    implementation->num_threads = num_additional_threads;
    implementation->run(num_additional_threads);
  }

  /// Stop the xmlrpc service
  template <typename T> void basic_http_server<T>::stop()   {
  }
}      // End namespace http
#endif // HTTP_HTTP_HTTPSERVER_H

