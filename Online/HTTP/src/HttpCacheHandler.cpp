//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : HTTP
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include <HTTP/HttpCacheHandler.h>
#include <HTTP/HttpRequest.h>

// C/C++ include files
#include <cstring>
#include <iostream>

/// Default destructor
http::HttpCacheHandler::~HttpCacheHandler()    {
  this->reply_cache.reset();
}

/// Access number of entries in the cache
std::size_t http::HttpCacheHandler::size()  const    {
  if ( reply_cache.get() )
    return this->reply_cache->size();
  return 0;
}

/// Access monitoring counters
http::Cache<http::HttpReply>::stats_type
http::HttpCacheHandler::cache_counters()  const   {
  if ( reply_cache.get() )
    return this->reply_cache->counters;
  return { };
}

/// Auto enable or disable cache functionality
bool http::HttpCacheHandler::use_cache(std::mutex& handler_lock)   {
  std::lock_guard<std::mutex> lock(handler_lock);
  if ( this->cache.enable )   {
    if ( !this->reply_cache.get() )
      this->reply_cache = std::make_unique<Cache<HttpReply> >();
    return true;
  }
  this->reply_cache.reset();
  return false;
}

/// Check cache for the existence of a given entry
std::pair<bool, http::Reply>
http::HttpCacheHandler::check_cache(const std::string& path,
				    const http::HttpRequest& req)
{
  struct timeval now;
  std::size_t len = ::strlen("Accept-Encoding");
  ::gettimeofday(&now, nullptr);
  for(const auto& h : req.headers)  {
    if ( ::strncasecmp(h.name.c_str(),"Accept-Encoding",len) == 0 )  {
      return this->check_cache(now, path, h.value);
    }
  }
  return this->check_cache(now, path, "");
}

/// Check cache for the existence of a given entry
std::pair<bool, http::Reply>
http::HttpCacheHandler::check_cache(const struct timeval& now,
				    const std::string&    path,
				    const std::string&    encoding)
{
  if ( this->reply_cache )    {
    auto key = this->reply_cache->hash(path);
    if ( this->cache.debug )   {
      std::cout << "Cache-check  [" << (void*)key << "]  " << std::endl;
    }
    std::lock_guard<std::mutex> lock(this->reply_cache->lock);
    const auto* e = this->reply_cache->find(key, encoding);
    if ( e )   {
      if ( !e->is_valid(now) )  {
	this->reply_cache->drop(key, encoding);
	return std::make_pair(false,http::Reply());
      }
      const auto& rep = e->data;
      if ( this->cache.debug )   {
	std::cout << "Cache-fetch  [" << (void*)key << "]: " << e->path
		  << " "  << e->encoding
		  << " [" << rep.content.size() << " bytes]"
		  << std::endl;
      }
      return std::make_pair(true, rep.clone());
    }
  }
  return std::make_pair(false,http::Reply());
}

/// Add or update an entry in the data cache
http::Reply
http::HttpCacheHandler::add_cache(const std::string& path,
				  const std::string& encoding,
				  http::HttpReply&&  reply)
{
  if ( this->reply_cache )    {
    auto key = this->reply_cache->hash(path);
    std::lock_guard<std::mutex> lock(this->reply_cache->lock);
    if ( this->cache.debug )   {
      std::cout << "Cache-insert [" << (void*)key << "]: " << path
		<< " "  << encoding
		<< " [" << reply.content.size() << " bytes]"
		<< std::endl;
    }
    this->reply_cache->insert(key, cache.timeout, path, encoding, reply);
  }
  return std::move(reply);
}

/// Clean "old" entries from cache [Thread safe, locked]
std::size_t http::HttpCacheHandler::drop_expired()    {
  if ( this->reply_cache )    {
    std::size_t count = this->reply_cache->drop_expired();
    if ( this->cache.debug )   {
      std::cout << "Cache-clean:  " << this->cache.name << " Dropped " << count
		<< " entries from cache."
		<< " Now: " << this->reply_cache->size() << " entries."
		<< std::endl;
    }
    return count;
  }
  return 0;
}

/// Drop cache entries by key  [Thread safe, locked]
std::size_t http::HttpCacheHandler::drop_key(cache_type::key_type key)    {
  if ( this->reply_cache )    {
    std::size_t count = this->reply_cache->drop_key(key);
    if ( this->cache.debug )   {
      std::cout << "Cache-clean:  " << this->cache.name << "Dropped " << count
		<< " entries from cache."
		<< " Now: " << this->reply_cache->size() << " entries."
		<< std::endl;
    }
    return count;
  }
  return 0;
}

/// Drop a cache entries by key  [Thread safe, locked]
std::size_t http::HttpCacheHandler::drop_keys(const std::vector<cache_type::key_type>& keys)   {
  if ( this->reply_cache )    {
    std::size_t count = this->reply_cache->drop_keys(keys);
    if ( this->cache.debug )   {
      std::cout << "Cache-clean:  " << this->cache.name << "Dropped " << count
		<< " entries from cache."
		<< " Now: " << this->reply_cache->size() << " entries."
		<< std::endl;
    }
    return count;
  }
  return 0;
}
