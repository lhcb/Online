//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include <HTTP/Asio.h>
#include <HTTP/HttpReply.h>
#include <HTTP/HttpClient.h>
#include <RTL/Compress.h>

// C/C++ include files
#include <set>
#include <cstring>
#include <iostream>
#include <stdexcept>

using namespace std;
using namespace boost;
using namespace http;

/// Initializing constructor
HttpClient::HttpClient(const string& h, int p, int tmo)
  : host(h), timeout(tmo)
{
  stringstream str;
  str << p;
  port = str.str();
}

/// Initializing constructor
HttpClient::HttpClient(const string& h, const string& p, int tmo)
  : host(h), port(p), contentType("text/xml"), timeout(tmo)
{
}

/// Initializing constructor: No need to call open if this constructor is issued
HttpClient::HttpClient(const string& h, const string& m, int p, int tmo)
  : host(h), mount(m), contentType("text/xml"), timeout(tmo)
{
  stringstream str;
  str << p;
  port = str.str();
}

/// Initializing constructor: No need to call open if this constructor is issued
HttpClient::HttpClient(const string& h, const string& m, const string& p, int tmo)
  : host(h), port(p), mount(m), contentType("text/xml"), timeout(tmo)
{
}

/// Default destructor
HttpClient::~HttpClient() {
}

/// Modify debug flag
int HttpClient::setDebug(int value)  {
  int tmp = debugFlag;
  debugFlag = value;
  return tmp;
}

/// Access debug flag
int HttpClient::debug()  const  {
  return debugFlag;
}

/// Initializing constructor
void HttpClient::open(const string& h, int p, int tmo)  {
  timeout = tmo;
  open(h, p);
}

/// Initializing constructor
void HttpClient::open(const string& h, int p)  {
  stringstream str;
  str << p;
  host = h;
  port = str.str();
}

/// Access name
string HttpClient::name()  const   {
  return host+':'+port;
}

/// Connect client to given URI and execute RPC call
vector<unsigned char> HttpClient::request(const string& req)   const  {
  return this->request(req.c_str(), req.length());
}

/// Connect client to given URI and execute RPC call
vector<unsigned char> HttpClient::request(const void* req, size_t len)  const   {
  string dataEncoding;
  return this->request(req, len, "", dataEncoding);
}

/// Connect client to given URI and execute RPC call
vector<unsigned char> HttpClient::request(const void* req, size_t len,
					  const string& acceptedEncoding,
					  string&  dataEncoding)  const
{
  using namespace chrono;
  size_t                         req_len;
  asio::io_context               io;
  system::error_code             ec;
  asio::ip::tcp::socket          socket{io};
  asio::ip::tcp::resolver        resolver{io};
  stringstream                   str;
  string                         hdr;
  str << "POST "            << mount << " HTTP/1.0\r\n"
      << "User-Agent: "     << "XMLRPC-HttpClient\r\n"
      << "Host: "           << "XMLRPC-Host\r\n"
      << "Content-Type: "   << contentType << "\r\n"
      << "Content-Length: " << len << "\r\n";
  for(const auto& h : userRequestHeaders)
    str << h.name << ": " << h.value << "\r\n";
  str << "\r\n";
  hdr = str.str();
  vector<asio::const_buffer> request = {asio::const_buffer(hdr.c_str(), hdr.length()),
					asio::const_buffer(req, len)  };
  std::int64_t contentLength = 0;
  std::string  contentEncoding = "";
  req_len = asio::detail::buffer_size(request[0]) + asio::detail::buffer_size(request[1]);

  try  {
#if BOOST_ASIO_VERSION < 103400
    asio::ip::tcp::resolver::iterator it, it_end;
    it = resolver.resolve({host, port});
    // Try all connection end-points until one succeeds!
    auto conn_iter = asio::connect(socket, it, it_end);
    if ( conn_iter != it_end )  {
      //printout(INFO,"RPCClient","Connected to endpoint: %s",conn_iter->endpoint().address().to_string().c_str());
    }
#else    // version >= 1.34.0
    auto conns = resolver.resolve(asio::ip::tcp::v4(), host, port);
    asio::connect(socket, conns, ec);
    if ( !ec )  {
      //printout(INFO,"RPCClient","Connected to endpoint: %s",conn_iter->endpoint().address().to_string().c_str());
    }
#endif
    socket.set_option(asio::socket_base::reuse_address(true));
    socket.set_option(asio::socket_base::linger(true,0));
    size_t wr_len = asio::write(socket, request, ec);
    if ( wr_len != req_len )  {
      system::error_code errcode(errno,system::system_category());
      socket.shutdown(asio::ip::tcp::socket::shutdown_both);
      socket.close();
      errno = ENOTCONN;
      str << "XMLRPC [ENOTCONN Failed to write to RPC server] " << errcode.message();
      throw runtime_error(str.str());
    }
    asio::streambuf response;
    asio::read_until(socket, response, "\r\n");

    // Check that response is OK.
    istream response_stream(&response);
    string http_version;
    response_stream >> http_version;
    unsigned int status_code;
    response_stream >> status_code;
    string status_message;
    getline(response_stream, status_message);
    if (!response_stream || http_version.substr(0, 5) != "HTTP/")    {
      system::error_code errcode(errno=EINVAL,system::system_category());
      str << "XMLRPC [EINVAL Invalid XML-RPC response] " << errcode.message();
      errno = errcode.value();
      throw runtime_error(str.str());
    }
    if (status_code != 200)    {
      system::error_code errcode(errno=EINVAL,system::system_category());
      str << "XMLRPC [EINVAL Invalid XML-RPC bad HTTP status code] " << errcode.message();
      errno = errcode.value();
      throw runtime_error(str.str());
    }
    // Read the response headers, which are terminated by a blank line.
    asio::read_until(socket, response, "\r\n\r\n");
    // Process the response headers.
    size_t idx;
    while (getline(response_stream, hdr) && hdr != "\r")   {
      if ( 0 == contentLength && hdr.substr(0,14) == "Content-Length" )  {
	if ( (idx=hdr.rfind(' ')) != string::npos )  {
	  const char* p = hdr.c_str()+idx+1;
	  contentLength = ::atol(p);
	}
      }
      else if ( contentEncoding.empty() && hdr.substr(0,16) == "Content-Encoding" )  {
	if ( (idx=hdr.rfind(' ')) != string::npos )  {
	  contentEncoding = hdr.substr(idx+1,hdr.find('\r')-idx-1);
	}
      }
      if ( debugFlag>0 )  {
	cout << "RPCClient: +++HTTP Header: " << hdr << endl;
      }
    }
    vector<unsigned char> resp;
    int resp_len = 0, got_bytes = 0, num_bytes = 0;
    if ( contentLength > 0 )   {
      resp.reserve(contentLength+sizeof(int));
    }
    do {
      char c = (char)response_stream.get();
      if ( !response_stream.good() ) break;
      ++got_bytes;
      resp.push_back(c);
    }  while ( true );
    if ( contentLength > 0 )   {
      if ( got_bytes < contentLength )   {
	unsigned char chunk[0x4000];
	do {
	  size_t nb = std::min(size_t(contentLength-got_bytes), sizeof(chunk));
	  if ( nb > 0 )   {
	    num_bytes = asio::read(socket, asio::buffer(chunk, nb), asio::transfer_exactly(nb), ec);
	    if ( num_bytes < 0 ) break;
	    copy(chunk, chunk+num_bytes, back_inserter(resp));
	    got_bytes += num_bytes;
	    if ( num_bytes == 0 && ec ) break;
	  }
	} while ( !ec && got_bytes < contentLength );
      }
      if ( got_bytes < contentLength )   {
        resp.resize(resp_len=contentLength);
      }
    }
    else   {
      resp_len = asio::read(socket, response, asio::transfer_all(), ec);
      istream resp_stream(&response);
      if ( 0 == contentLength )  {
	contentLength = resp_len + got_bytes;
      }
      do {
	char c = (char)resp_stream.get();
	if ( !resp_stream.good() ) break;
	++got_bytes;
	resp.push_back(c);
      }  while ( true );
    }

    if ( int(resp_len) != contentLength && int(got_bytes) != contentLength )   {
      std::cout << "RPCClient: +++ Handling response[" << contentLength 
		<< " bytes]: got " << resp_len << " bytes. "
		<< "[error:" << ec.value() << " " << ec.message() << "]" << std::endl;
    }
    else  {
      resp_len = got_bytes;
    }
    socket.shutdown(asio::ip::tcp::socket::shutdown_both,ec);
    socket.close(ec);

    std::vector<unsigned char> dcom_buffer;
    /// If necessary decompress the buffer (depends on http headers)
    if ( !contentEncoding.empty() )   {
      dcom_buffer = Online::compress::decompress(contentEncoding, resp);
      if ( acceptedEncoding.find(contentEncoding) != std::string::npos )
	resp = Online::compress::decompress(contentEncoding, resp);
      else
	dataEncoding = contentEncoding;
    }
    if ( !dcom_buffer.empty() )    {
      std::cout << "RPCClient: response[" << resp.size() << " / " << resp_len << " bytes]." << std::endl;
    }
    if ( debugFlag > 1 )  {
      std::cout << "RPCClient: response[" << resp.size() << " / " << resp_len << " bytes]." << std::endl;
      if ( debugFlag > 2 )  {
	std::vector<unsigned char> resp_tmp = resp;
	resp_tmp.push_back(0);
	std::cout << "Message:" << std::endl << (char*)&resp_tmp[0] << "|end-data|" << std::endl;
      }
    }
    return resp;
  }
  catch(const system::error_code& errcode)   {
    str.str("");
    str << "XMLRPC error: [errno=" << errcode.value() << ": " << errcode.message() << "]";
  }
  catch(const system::system_error& e)   {
    str.str("");
    str << "XMLRPC error: [errno=" << e.code().value() << ": " << e.what() << "]";
  }
  catch(const std::exception& e)   {
    str.str("");
    str << e.what();
  }
  catch( ... )   {
    system::error_code errcode(errno,system::system_category());
    str.str("");
    str << "XMLRPC fault [" << errcode.value() << "] (UNKOWN Exception): " << errcode.message();
  }
  try  {
    if ( socket.is_open() )   {
      socket.shutdown(asio::ip::tcp::socket::shutdown_both,ec);
      socket.close(ec);
    }
  }
  catch( ... )   {
  }
  throw runtime_error(str.str());
}
