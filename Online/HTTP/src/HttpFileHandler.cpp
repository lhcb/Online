//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================
// Please note:
// This implementation was directly deduced (with small changes) from 
// one of the boost::asio examples. 
// boost::asio: one of the best I/O libraries ever !
//
// Hence:
//
//==========================================================================

// Framework include files
#include <HTTP/HttpFileHandler.h>

// C/C++ include files
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <iostream>
#include <sstream>

namespace http   {
  namespace mime_types {

    struct mapping  {
      const char* extension;
      const char* mime_type;
    } mappings[] =
      {
	{ "gif",  "image/gif"   },
	{ "htm",  "text/html"   },
	{ "html", "text/html"   },
	{ "xml",  "text/xml"    },
	{ "css",  "text/css"    },
	{ "csv",  "text/csv"    },
	{ "txt",  "text/plain"  },
	{ "js",   "text/javascript" },
	{ "jsp",  "text/javascript" },
	{ "jsonp","text/javascript" },
	{ "jpg",  "image/jpeg" },
	{ "jpeg", "image/jpeg" },
	{ "gif",  "image/gif"  },
	{ "png",  "image/png"  },
	{ "bmp",  "image/bmp"  },
	{ "svg",  "image/svg+xml"  },
	{ "bin",  "application/octet-stream" },
	{ "sh",   "application/x-sh"     },
	{ "csh",  "application/x-csh"    },
	{ "epub", "application/epub+zip" },
	{ "gz",   "application/gz"       },
	{ "json", "application/json"     },
	{ "pdf",  "application/pdf"      },
	{ "rtf",  "application/rtf"      },
	{ "tar",  "application/x-tar"    },
	{ "mp3",  "audio/mpeg"           },
	{ "weba", "audeo/weba"           },
	{ "mp4",  "video/mp4"            },
	{ "mpeg", "video/mpeg"           },
	{ "webm", "video/webm"           },
      };
  } // namespace mime_types
}

std::string http::mime_types::extension_to_type(const std::string& extension)  {
  for (mapping m: mappings)    {
    if (m.extension == extension)  {
      return m.mime_type;
    }
  }
  return "text/plain";
}

/// Initializing constructor
http::HttpFileHandler::HttpFileHandler(const std::string& m, const std::string& fr)
  : mount(m), file_root(fr+'/')
{
}

/// Handle a request and produce a reply.
http::HttpRequestHandler::continue_action 
http::HttpFileHandler::handle_request(const http::Request& req, http::Reply& rep)  {
  if ( req.content_length < req.content.size() )  {
    return read;
  }

  if ( rep.bytes_total > 0 && rep.bytes_total == rep.bytes_sent )   {
    if ( this->debug )   {
      std::cout << "Close request:" << req.uri << " [" << rep.bytes_sent << " / "
		<< rep.bytes_total << " bytes sent]" << std::endl;
    }
    return close_write;
    //return none;
  }

  // std::cout << "Request:" << req.method << " -> " << req.uri << std::endl;
  std::size_t idstart = req.uri.find(this->mount);
  if ( idstart == std::string::npos )    {
    rep = http::Reply::stock_reply(http::Reply::bad_request);
    return write;
  }

  // Strip arguments from URI
  std::string path  = req.uri.substr(this->mount.length());
  path = path.substr(0, path.find("?"));

  // Basic security checks
  if ( path.empty() || path[0] != '/' || path.find("..") != std::string::npos )   {
    rep = http::Reply::stock_reply(http::Reply::bad_request);
    return HttpRequestHandler::write;
  }

  // If path ends in slash (i.e. is a directory) then add "index.html".
  if (path[path.size() - 1] == '/')  {
    path += "index.html";
  }

  struct stat buff;
  std::string fname = this->file_root + path.substr(1);
 Again:
  if ( ::stat(fname.c_str(), &buff) != 0 )   {
    if ( errno == EACCES )
      rep = http::Reply::stock_reply(http::Reply::unauthorized);
    else if ( errno == EBADF )
      rep = http::Reply::stock_reply(http::Reply::not_found);
    else if ( errno == ENOENT )
      rep = http::Reply::stock_reply(http::Reply::not_found);
    else if ( errno == EINVAL )
      rep = http::Reply::stock_reply(http::Reply::bad_request);
    else if ( errno == ENOMEM )
      rep = http::Reply::stock_reply(http::Reply::bad_request);
    else
      rep = http::Reply::stock_reply(http::Reply::bad_request);
    //rep.headers.emplace_back(http::HttpHeader("Connection", "close"));
    return HttpRequestHandler::write;
  }

  if ( S_ISLNK(buff.st_mode) )   {
    char p[PATH_MAX];
    if ( ::readlink(path.c_str(), p, sizeof(p)) > 0 )  {
      fname = p;
      goto Again;
    }
  }

  /// First check if the requested entry is already in the cache:
  if ( this->use_cache(this->lock) )    {
    auto [use,reply] = this->check_cache(path, req);
    if ( use )   {
      rep = std::move(reply);
      return HttpRequestHandler::write;
    }
  }

  if ( S_ISREG(buff.st_mode) || S_ISLNK(buff.st_mode) )   {
    rep = this->load_file(fname, buff.st_size);
    // Determine the file extension.
    std::size_t last_slash_pos = fname.find_last_of("/");
    std::size_t last_dot_pos = fname.find_last_of(".");
    std::string extension;
    if (last_dot_pos != std::string::npos && last_dot_pos > last_slash_pos)   {
      extension = fname.substr(last_dot_pos + 1);
    }
    rep.headers.resize(3);
    rep.headers[0].name = "Content-Length";
    rep.headers[0].value = std::to_string(rep.content.size());
    rep.headers[1].name = "Content-Type";
    rep.headers[1].value = mime_types::extension_to_type(extension);
    rep.headers[2].name  = "Connection";
    rep.headers[2].value = "close";
  }
  else if ( S_ISDIR(buff.st_mode) )   {
    rep.headers.clear();
    rep.headers.reserve(2);
    if ( ::stat((fname + "/index.html").c_str(), &buff) == 0 )   {
      rep = load_file(fname + "/index.html", buff.st_size);
      rep.headers.emplace_back(HttpHeader("Content-Type",   mime_types::extension_to_type("html")));
    }
    else if ( ::stat((fname + "/index.htm").c_str(), &buff) == 0 )   {
      rep = load_file(fname + "/index.htm", buff.st_size);
      rep.headers.emplace_back(HttpHeader("Content-Type",   mime_types::extension_to_type("html")));
    }
    else   {
      rep = this->load_dir(fname);
      rep.headers.emplace_back(HttpHeader("Content-Type",   mime_types::extension_to_type("txt")));
    }
    rep.headers.emplace_back(HttpHeader("Content-Length", std::to_string(rep.content.size())));
    //rep.headers.emplace_back(http::HttpHeader("Connection", "close"));
  }
  else   {
    rep = http::Reply::stock_reply(http::Reply::not_found);
    //rep.headers.emplace_back(http::HttpHeader("Connection", "close"));
  }
  std::string encoding;
  if ( this->compression.enable )   {
    if ( rep.content.size() > this->compression.minContentLength )  {
      this->compress_reply(req, rep, encoding);
    }
  }
  if ( this->reply_cache )    {
    rep = this->add_cache(path, encoding, std::move(rep));
  }
  return HttpRequestHandler::write;
}

/// Read directory data data and stuff them into the reply structure
http::Reply http::HttpFileHandler::load_dir(const std::string& path)   const    {
  DIR* dir = ::opendir(path.c_str());
  if ( !dir )   {
    if ( errno == EACCES )
      return http::Reply::stock_reply(http::Reply::unauthorized);
    else if ( errno == EBADF )
      return http::Reply::stock_reply(http::Reply::not_found);
    else if ( errno == ENOENT )
      return http::Reply::stock_reply(http::Reply::not_found);
    else if ( errno == EINVAL )
      return http::Reply::stock_reply(http::Reply::bad_request);
    else if ( errno == ENOMEM )
      return http::Reply::stock_reply(http::Reply::bad_request);
    else
      return http::Reply::stock_reply(http::Reply::bad_request);
  }
  std::stringstream str;
  str << "<PRE>" << std::endl;
  while( auto* e = ::readdir(dir) )   {
    if ( e->d_type == DT_REG || e->d_type == DT_LNK || e->d_type == DT_FIFO )
      str << e->d_name << std::endl;
    else if ( e->d_type == DT_DIR )
      str << e->d_name << std::endl;
  }
  str << "</PRE>" << std::endl;
  ::closedir(dir);
  auto rep = http::Reply::stock_reply(http::Reply::ok);
  rep.content = to_vector(str.str());
  return rep;
}

/// Read file data and stuff them into the reply structure
http::Reply http::HttpFileHandler::load_file(const std::string& path, off_t len)  const  {
  int fd = ::open64(path.c_str(), O_RDONLY);
  // std::cout << "Load: " << path << std::endl;
  if ( fd != -1 )   {
    auto length = len;
    auto rep = http::Reply::stock_reply(http::Reply::ok);
    rep.content.resize(len);
    while(len > 0)    {
      int nb = ::read(fd, &rep.content[0] + rep.content.size() - len, len);
      if ( nb < 0 )   {
	break;
      }
      len -= nb;
    }
    ::close(fd);
    if ( 0 == len )    {
      if ( this->cache.debug || this->debug )   {
	std::cout << "Served:       " << path
		  << " [" << rep.content.size() << "/" << length << " bytes]" << std::endl;
      }
      return rep;
    }
  }
  std::error_code ec(errno, std::system_category());
  std::cout << "FAILED serving: " << path << " [" << ec.message() << "]" << std::endl;
  return http::Reply::stock_reply(http::Reply::not_found);
}
