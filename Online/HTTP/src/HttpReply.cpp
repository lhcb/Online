//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================
// Please note:
// This implementation was directly deduced (with small changes) from 
// one of the boost::asio examples. 
// boost::asio: one of the best I/O libraries ever !
//
// Hence:
//
//==========================================================================
//
// Copyright (c) 2003-2015 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//
//==========================================================================

// Framework include files
#include "HTTP/HttpReply.h"
#include "HTTP/Asio.h"

// C/C++ include files
#include <iostream>

// Boost inlcude files
#include <boost/asio.hpp>

using namespace http;
using namespace boost;

/// Namespace for the http server and client apps
namespace http   {

  namespace http_status_strings {
    const std::string continuing =                      "HTTP/1.0 100 Continue\r\n";
    const std::string switching_protocols =             "HTTP/1.0 101 Switching Protocols\r\n";
    const std::string processing =                      "HTTP/1.0 102 Processing (WebDAV)\r\n";
    const std::string early_hints =                     "HTTP/1.0 103 Early Hints\r\n";

    const std::string ok =                              "HTTP/1.0 200 OK\r\n";
    const std::string created =                         "HTTP/1.0 201 Created\r\n";
    const std::string accepted =                        "HTTP/1.0 202 Accepted\r\n";
    const std::string no_content =                      "HTTP/1.0 204 No Content\r\n";
    const std::string reset_content =                   "HTTP/1.0 205 Reset Content\r\n";
    const std::string partial_content =                 "HTTP/1.0 206 Partial Content\r\n";
    const std::string multi_status =                    "HTTP/1.0 207 Multi-Status (WebDAV)\r\n";
    const std::string already_reported =                "HTTP/1.0 208 Already Reported (WebDAV)\r\n";

    const std::string multiple_choices =                "HTTP/1.0 300 Multiple Choices\r\n";
    const std::string moved_permanently =               "HTTP/1.0 301 Moved Permanently\r\n";
    const std::string moved_temporarily =               "HTTP/1.0 302 Moved Temporarily\r\n";
    const std::string see_other =                       "HTTP/1.0 303 See other\r\n";
    const std::string not_modified =                    "HTTP/1.0 304 Not Modified\r\n";
    const std::string use_proxy =                       "HTTP/1.0 305 Use Proxy\r\n";
    const std::string switch_proxy =                    "HTTP/1.0 306 Switch Proxy\r\n";
    const std::string temp_redirect =                   "HTTP/1.0 307 Temporary redirect\r\n";
    const std::string permanent_redirect =              "HTTP/1.0 308 Permanent redirect\r\n";

    
    const std::string bad_request =                     "HTTP/1.0 400 Bad Request\r\n";
    const std::string unauthorized =                    "HTTP/1.0 401 Unauthorized\r\n";
    const std::string payment_required =                "HTTP/1.0 402 Payment Required\r\n";
    const std::string forbidden =                       "HTTP/1.0 403 Forbidden\r\n";
    const std::string not_found =                       "HTTP/1.0 404 Not Found\r\n";
    const std::string method_not_allowed =              "HTTP/1.0 405 Method Not Allowed\r\n";
    const std::string not_acceptable =                  "HTTP/1.0 406 Not Acceptable\r\n";
    const std::string proxy_authentication_required =    "HTTP/1.0 407 Proxy Authentication Required\r\n";
    const std::string request_timeout =                 "HTTP/1.0 408 Request Timeout\r\n";
    const std::string conflict =                        "HTTP/1.0 409 Conflict\r\n";
    const std::string gone =                            "HTTP/1.0 410 Gone\r\n";
    const std::string length_required =                 "HTTP/1.0 411 Length Required\r\n";
    const std::string precondition_failed =             "HTTP/1.0 412 Precondition Failed\r\n";
    const std::string payload_too_large =               "HTTP/1.0 413 Payload Too Large\r\n";
    const std::string uri_too_long =                    "HTTP/1.0 414 URI Too Long\r\n";
    const std::string unsupported_media_type =          "HTTP/1.0 415 Unsupported Media Type\r\n";
    const std::string range_not_satisfiable =           "HTTP/1.0 416 Range Not Satisfiable\r\n";
    const std::string expectation_failed =              "HTTP/1.0 417 Expectation Failed\r\n";
    const std::string i_am_a_teapot =                   "HTTP/1.0 418 I'm a teapot\r\n";
    const std::string misdirected_request =             "HTTP/1.0 421 Misdirected Request\r\n";
    const std::string unprocessable_entity =            "HTTP/1.0 422 Unprocessable Entity (WebDAV)\r\n";
    const std::string locked =                          "HTTP/1.0 423 Locked (WebDAV)\r\n";
    const std::string failed_dependency =               "HTTP/1.0 424 Failed Dependency (WebDAV)\r\n";
    const std::string too_early =                       "HTTP/1.0 425 Too Early\r\n";
    const std::string upgrade_required =                "HTTP/1.0 426 Upgrade Required\r\n";
    const std::string precondition_required =           "HTTP/1.0 428 Precondition Required\r\n";
    const std::string too_many_requests =               "HTTP/1.0 429 Too Many Requests\r\n";
    const std::string request_header_fields_too_large = "HTTP/1.0 431 Request Header Fields Too Large\r\n";
    const std::string unavailable_for_legal_reasons   = "HTTP/1.0 451 Unavailable For Legal Reasons\r\n";

    const std::string internal_server_error =           "HTTP/1.0 500 Internal Server Error\r\n";
    const std::string not_implemented =                 "HTTP/1.0 501 Not Implemented\r\n";
    const std::string bad_gateway =                     "HTTP/1.0 502 Bad Gateway\r\n";
    const std::string service_unavailable =             "HTTP/1.0 503 Service Unavailable\r\n";
    const std::string gateway_timeout =                 "HTTP/1.0 504 Gateway Timeout\r\n";
    const std::string http_version_not_supported =      "HTTP/1.0 505 HTTP Version Not Supported\r\n";
    const std::string variant_also_negotiates =         "HTTP/1.0 506 Variant Also Negotiates\r\n";
    const std::string insufficient_storage =            "HTTP/1.0 507 Insufficient Storage (WebDAV)\r\n";
    const std::string loop_detected =                   "HTTP/1.0 508 Loop Detected (WebDAV)\r\n";
    const std::string not_extended =                    "HTTP/1.0 510 Not Extended\r\n";
    const std::string network_authentication_required = "HTTP/1.0 511 Network Authentication Required\r\n";

    const std::string& header_string(HttpReply::status_type status)    {
      switch (status)	{
      case HttpReply::continuing:               	return continuing;
      case HttpReply::ok:                             	return ok;
      case HttpReply::created:                    	return created;
      case HttpReply::accepted:                   	return accepted;
      case HttpReply::no_content:                	return no_content;
      case HttpReply::reset_content:              	return reset_content;
      case HttpReply::partial_content:                	return partial_content;
      case HttpReply::multi_status:                    	return multi_status;
      case HttpReply::already_reported:          	return already_reported;

      case HttpReply::multiple_choices:         	return multiple_choices;
      case HttpReply::moved_permanently:        	return moved_permanently;
      case HttpReply::moved_temporarily:         	return moved_temporarily;
      case HttpReply::see_other:                      	return see_other;
      case HttpReply::not_modified:                  	return not_modified;
      case HttpReply::use_proxy:                    	return use_proxy;
      case HttpReply::switch_proxy:             	return switch_proxy;
      case HttpReply::temp_redirect:            	return temp_redirect;
      case HttpReply::permanent_redirect:       	return permanent_redirect;

      case HttpReply::bad_request:               	return bad_request;
      case HttpReply::unauthorized:                 	return unauthorized;
      case HttpReply::payment_required:         	return payment_required;
      case HttpReply::forbidden:                	return forbidden;
      case HttpReply::not_found:                    	return not_found;
      case HttpReply::method_not_allowed:          	return method_not_allowed;
      case HttpReply::not_acceptable:                 	return not_acceptable;
      case HttpReply::proxy_authentication_required:	return proxy_authentication_required;
      case HttpReply::request_timeout:              	return request_timeout;
      case HttpReply::conflict:                      	return conflict;	
      case HttpReply::gone:                     	return gone;
      case HttpReply::length_required:                 	return length_required;
      case HttpReply::precondition_failed:        	return precondition_failed;
      case HttpReply::payload_too_large:           	return payload_too_large;
      case HttpReply::uri_too_long:             	return uri_too_long;
      case HttpReply::unsupported_media_type:      	return unsupported_media_type;
      case HttpReply::range_not_satisfiable:         	return range_not_satisfiable;
      case HttpReply::expectation_failed:       	return expectation_failed;
      case HttpReply::i_am_a_teapot:               	return i_am_a_teapot;
      case HttpReply::misdirected_request:        	return misdirected_request;
      case HttpReply::unprocessable_entity:         	return unprocessable_entity;
      case HttpReply::locked:                   	return locked;
      case HttpReply::failed_dependency:          	return failed_dependency;
      case HttpReply::too_early:                	return too_early;
      case HttpReply::upgrade_required:         	return upgrade_required;
      case HttpReply::precondition_required:    	return precondition_required;
      case HttpReply::too_many_requests:        	return too_many_requests;
      case HttpReply::request_header_fields_too_large:	return request_header_fields_too_large;
      case HttpReply::unavailable_for_legal_reasons:	return unavailable_for_legal_reasons;

      case HttpReply::internal_server_error:     	return internal_server_error;
      case HttpReply::not_implemented:          	return not_implemented;
      case HttpReply::bad_gateway:               	return bad_gateway;
      case HttpReply::service_unavailable:      	return service_unavailable;	
      case HttpReply::gateway_timeout:          	return gateway_timeout;
      case HttpReply::http_version_not_supported:	return http_version_not_supported;
      case HttpReply::variant_also_negotiates:  	return variant_also_negotiates;
      case HttpReply::insufficient_storage:       	return insufficient_storage;
      case HttpReply::loop_detected:                  	return loop_detected;
      case HttpReply::not_extended:                	return not_extended;
      case HttpReply::network_authentication_required:	return network_authentication_required;

      default:                                       	return internal_server_error;
      }
    }

    boost::asio::const_buffer to_buffer(HttpReply::status_type status)    {
      return boost::asio::buffer(header_string(status));
    }
  } // namespace http_status_strings

  namespace http_misc_strings {
    const char name_value_separator[] = { ':', ' ' };
    const char crlf[] = { '\r', '\n' };
  } // namespace http_misc_strings

  namespace http_stock_replies {
    const char ok[] = "";
    const char continuing[] =
      "<html>"
      "<head><title>Continue</title></head>"
      "<body><h1>100 Continue</h1></body>"
      "</html>";
    const char created[] =
      "<html>"
      "<head><title>Created</title></head>"
      "<body><h1>201 Created</h1></body>"
      "</html>";
    const char accepted[] =
      "<html>"
      "<head><title>Accepted</title></head>"
      "<body><h1>202 Accepted</h1></body>"
      "</html>";
    const char no_content[] =
      "<html>"
      "<head><title>No Content</title></head>"
      "<body><h1>204 Content</h1></body>"
      "</html>";
    const char reset_content[] =
      "<html>"
      "<head><title>Reset Content</title></head>"
      "<body><h1>205 Reset Content</h1></body>"
      "</html>";
    const char partial_content[] =
      "<html>"
      "<head><title>Partial Content</title></head>"
      "<body><h1>206 Partial Content</h1></body>"
      "</html>";
    const char multi_status[] =
      "<html>"
      "<head><title>Multi Status</title></head>"
      "<body><h1>207 Multi Status</h1></body>"
      "</html>";
    const char already_reported[] =
      "<html>"
      "<head><title>Already Reported</title></head>"
      "<body><h1>208 Already Reported (WebDAV)</h1></body>"
      "</html>";

    const char multiple_choices[] =
      "<html>"
      "<head><title>Multiple Choices</title></head>"
      "<body><h1>300 Multiple Choices</h1></body>"
      "</html>";
    const char moved_permanently[] =
      "<html>"
      "<head><title>Moved Permanently</title></head>"
      "<body><h1>301 Moved Permanently</h1></body>"
      "</html>";
    const char moved_temporarily[] =
      "<html>"
      "<head><title>Moved Temporarily</title></head>"
      "<body><h1>302 Moved Temporarily</h1></body>"
      "</html>";
    const char see_other[] =
      "<html>"
      "<head><title>See other</title></head>"
      "<body><h1>303 See other</h1></body>"
      "</html>";
    const char not_modified[] =
      "<html>"
      "<head><title>Not Modified</title></head>"
      "<body><h1>304 Not Modified</h1></body>"
      "</html>";
    const char use_proxy[] =
      "<html>"
      "<head><title>Use Proxy</title></head>"
      "<body><h1>305 Use Proxy</h1></body>"
      "</html>";
    const char switch_proxy[] =
      "<html>"
      "<head><title>Switch Proxy</title></head>"
      "<body><h1>306 Switch Proxy</h1></body>"
      "</html>";
    const char temp_redirect[] =
      "<html>"
      "<head><title>Temporary Redirect</title></head>"
      "<body><h1>307 Temporary Redirect</h1></body>"
      "</html>";
    const char permanent_redirect[] =
      "<html>"
      "<head><title>Permanent redirect</title></head>"
      "<body><h1>308 Permanent redirect</h1></body>"
      "</html>";

    const char bad_request[] =
      "<html>"
      "<head><title>Bad Request</title></head>"
      "<body><h1>400 Bad Request</h1></body>"
      "</html>";
    const char unauthorized[] =
      "<html>"
      "<head><title>Unauthorized</title></head>"
      "<body><h1>401 Unauthorized</h1></body>"
      "</html>";
    const char payment_required[] =
      "<html>"
      "<head><title>Payment Required</title></head>"
      "<body><h1>402 Payment Required</h1></body>"
      "</html>";
    const char forbidden[] =
      "<html>"
      "<head><title>Forbidden</title></head>"
      "<body><h1>403 Forbidden</h1></body>"
      "</html>";
    const char not_found[] =
      "<html>"
      "<head><title>Not Found</title></head>"
      "<body><h1>404 Not Found</h1></body>"
      "</html>";
    const char method_not_allowed[] =
      "<html>"
      "<head><title>Method Not Allowed</title></head>"
      "<body><h1>405 Method Not Allowed</h1></body>"
      "</html>";
    const char not_acceptable[] =
      "<html>"
      "<head><title>Not Acceptable</title></head>"
      "<body><h1>406 Not Acceptable</h1></body>"
      "</html>";
    const char proxy_authentication_required[] =
      "<html>"
      "<head><title>Proxy Authentication Required </title></head>"
      "<body><h1>407 Proxy Authentication Required </h1></body>"
      "</html>";
    const char request_timeout[] =
      "<html>"
      "<head><title>Request Timeout</title></head>"
      "<body><h1>408 Request Timeout</h1></body>"
      "</html>";
    const char conflict[] =
      "<html>"
      "<head><title>Conflict</title></head>"
      "<body><h1>409 Conflict</h1></body>"
      "</html>";
    const char gone[] =
      "<html>"
      "<head><title>Gone</title></head>"
      "<body><h1>410 Gone</h1></body>"
      "</html>";
    const char length_required[] =
      "<html>"
      "<head><title>Length Required</title></head>"
      "<body><h1>411 Length Required</h1></body>"
      "</html>";
    const char precondition_failed[] =
      "<html>"
      "<head><title>Precondition Failed</title></head>"
      "<body><h1>412 Precondition Failed</h1></body>"
      "</html>";
    const char payload_too_large[] =
      "<html>"
      "<head><title>Payload Too Large</title></head>"
      "<body><h1>413 Payload Too Large</h1></body>"
      "</html>";
    const char uri_too_long[] =
      "<html>"
      "<head><title>URI Too Long</title></head>"
      "<body><h1>414 URI Too Long</h1></body>"
      "</html>";
    const char unsupported_media_type[] =
      "<html>"
      "<head><title>Unsupported Media Type</title></head>"
      "<body><h1>415 Unsupported Media Type</h1></body>"
      "</html>";
    const char range_not_satisfiable[] =
      "<html>"
      "<head><title>Range Not Satisfiable</title></head>"
      "<body><h1>416 Range Not Satisfiable</h1></body>"
      "</html>";
    const char expectation_failed[] =
      "<html>"
      "<head><title>Expectation Failed</title></head>"
      "<body><h1>417 Expectation Failed</h1></body>"
      "</html>";
    const char i_am_a_teapot[] =
      "<html>"
      "<head><title>I'm a teapot</title></head>"
      "<body><h1>418 I'm a teapot</h1></body>"
      "</html>";
    const char misdirected_request[] =
      "<html>"
      "<head><title>Misdirected Request</title></head>"
      "<body><h1>421 Misdirected Request</h1></body>"
      "</html>";
    const char unprocessable_entity[] =
      "<html>"
      "<head><title>Unprocessable Entity (WebDAV)</title></head>"
      "<body><h1>422 Unprocessable Entity (WebDAV)</h1></body>"
      "</html>";
    const char locked[] =
      "<html>"
      "<head><title>Locked (WebDAV)</title></head>"
      "<body><h1>423 Locked (WebDAV)</h1></body>"
      "</html>";
    const char failed_dependency[] =
      "<html>"
      "<head><title>Failed Dependency (WebDAV)</title></head>"
      "<body><h1>424 Failed Dependency (WebDAV)</h1></body>"
      "</html>";
    const char too_early[] =
      "<html>"
      "<head><title>Too Early</title></head>"
      "<body><h1>425 Too Early</h1></body>"
      "</html>";
    const char upgrade_required[] =
      "<html>"
      "<head><title>Upgrade Required</title></head>"
      "<body><h1>426 Upgrade Required</h1></body>"
      "</html>";
    const char precondition_required[] =
      "<html>"
      "<head><title>Precondition Required</title></head>"
      "<body><h1>428 Precondition Required</h1></body>"
      "</html>";
    const char too_many_requests[] =
      "<html>"
      "<head><title>Too Many Requests</title></head>"
      "<body><h1>429 Too Many Requests</h1></body>"
      "</html>";
    const char request_header_fields_too_large[] =
      "<html>"
      "<head><title>Request Header Fields Too Large</title></head>"
      "<body><h1>431 Request Header Fields Too Large</h1></body>"
      "</html>";
    const char unavailable_for_legal_reasons[] =
      "<html>"
      "<head><title>Unavailable For Legal Reasons</title></head>"
      "<body><h1>451 Unavailable For Legal Reasons</h1></body>"
      "</html>";

    const char internal_server_error[] =
      "<html>"
      "<head><title>Internal Server Error</title></head>"
      "<body><h1>500 Internal Server Error</h1></body>"
      "</html>";
    const char not_implemented[] =
      "<html>"
      "<head><title>Not Implemented</title></head>"
      "<body><h1>501 Not Implemented</h1></body>"
      "</html>";
    const char bad_gateway[] =
      "<html>"
      "<head><title>Bad Gateway</title></head>"
      "<body><h1>502 Bad Gateway</h1></body>"
      "</html>";
    const char service_unavailable[] =
      "<html>"
      "<head><title>Service Unavailable</title></head>"
      "<body><h1>503 Service Unavailable</h1></body>"
      "</html>";
    const char gateway_timeout[] =
      "<html>"
      "<head><title>Gateway Timeout</title></head>"
      "<body><h1>504 Gateway Timeout</h1></body>"
      "</html>";
    const char http_version_not_supported[] =
      "<html>"
      "<head><title>HTTP Version Not Supported</title></head>"
      "<body><h1>505 HTTP Version Not Supported</h1></body>"
      "</html>";
    const char variant_also_negotiates[] =
      "<html>"
      "<head><title>Variant Also Negotiates</title></head>"
      "<body><h1>506 Variant Also Negotiates</h1></body>"
      "</html>";
    const char insufficient_storage[] =
      "<html>"
      "<head><title>Insufficient Storage (WebDAV)</title></head>"
      "<body><h1>507 Insufficient Storage (WebDAV)</h1></body>"
      "</html>";
    const char loop_detected[] =
      "<html>"
      "<head><title>Loop Detected (WebDAV)</title></head>"
      "<body><h1>508 Loop Detected (WebDAV)</h1></body>"
      "</html>";
    const char not_extended[] =
      "<html>"
      "<head><title>Not Extended</title></head>"
      "<body><h1>510 Not Extended</h1></body>"
      "</html>";
    const char network_authentication_required[] =
      "<html>"
      "<head><title>Network Authentication Required</title></head>"
      "<body><h1>511 Network Authentication Required</h1></body>"
      "</html>";

    std::string to_string(HttpReply::status_type status)  {
      switch (status)   {
      case HttpReply::ok:                 	       return ok;
      case HttpReply::continuing:         	       return continuing;
      case HttpReply::created:            	       return created;
      case HttpReply::accepted:           	       return accepted;
      case HttpReply::no_content:         	       return no_content;
      case HttpReply::reset_content:                   return reset_content;
      case HttpReply::partial_content:                 return partial_content;
      case HttpReply::multi_status:                    return multi_status;
      case HttpReply::already_reported:                return already_reported;

      case HttpReply::multiple_choices:    	       return multiple_choices;
      case HttpReply::moved_permanently:  	       return moved_permanently;
      case HttpReply::moved_temporarily:   	       return moved_temporarily;
      case HttpReply::see_other:                       return see_other;
      case HttpReply::not_modified:       	       return not_modified;
      case HttpReply::use_proxy:                       return use_proxy;
      case HttpReply::switch_proxy:                    return switch_proxy;
      case HttpReply::temp_redirect:      	       return temp_redirect;
      case HttpReply::permanent_redirect:              return permanent_redirect;

      case HttpReply::bad_request:        	       return bad_request;
      case HttpReply::unauthorized:       	       return unauthorized;
      case HttpReply::payment_required:       	       return payment_required;
      case HttpReply::forbidden:          	       return forbidden;
      case HttpReply::not_found:          	       return not_found;
      case HttpReply::method_not_allowed:              return method_not_allowed;
      case HttpReply::not_acceptable:          	       return not_acceptable;
      case HttpReply::proxy_authentication_required:   return proxy_authentication_required;
      case HttpReply::request_timeout:                 return request_timeout;
      case HttpReply::conflict:                        return conflict;
      case HttpReply::gone:                 	       return gone;
      case HttpReply::length_required:         	       return length_required;
      case HttpReply::precondition_failed:     	       return precondition_failed;
      case HttpReply::payload_too_large:               return payload_too_large;
      case HttpReply::uri_too_long:                    return uri_too_long;
      case HttpReply::unsupported_media_type:          return unsupported_media_type;
      case HttpReply::range_not_satisfiable:           return range_not_satisfiable;
      case HttpReply::expectation_failed:              return expectation_failed;
      case HttpReply::i_am_a_teapot:                   return i_am_a_teapot;
      case HttpReply::misdirected_request:             return misdirected_request;
      case HttpReply::unprocessable_entity:            return unprocessable_entity;
      case HttpReply::locked:                 	       return locked;
      case HttpReply::failed_dependency:               return failed_dependency;
      case HttpReply::too_early:                       return too_early;
      case HttpReply::upgrade_required:                return upgrade_required;
      case HttpReply::precondition_required:           return precondition_required;
      case HttpReply::too_many_requests:               return too_many_requests;
      case HttpReply::request_header_fields_too_large: return request_header_fields_too_large;
      case HttpReply::unavailable_for_legal_reasons:   return unavailable_for_legal_reasons;
	
      case HttpReply::internal_server_error:	       return internal_server_error;
      case HttpReply::not_implemented:    	       return not_implemented;
      case HttpReply::bad_gateway:        	       return bad_gateway;
      case HttpReply::service_unavailable:             return service_unavailable;
      case HttpReply::gateway_timeout:                 return gateway_timeout;
      case HttpReply::http_version_not_supported:      return http_version_not_supported;
      case HttpReply::variant_also_negotiates:         return variant_also_negotiates;
      case HttpReply::insufficient_storage:            return insufficient_storage;
      case HttpReply::loop_detected:                   return loop_detected;
      case HttpReply::not_extended:                    return not_extended;
      case HttpReply::network_authentication_required: return network_authentication_required;
      default:                              	       return internal_server_error;
      }
    }

    std::vector<unsigned char> to_vector(HttpReply::status_type status)   {
      return http::to_vector(http_stock_replies::to_string(status));
    }
  } // namespace http_stock_replies
  std::string to_string(const HttpReply& reply)  {
    return http_stock_replies::to_string(reply.status);
  }
}   // namespace http


HttpReply HttpReply::stock_reply(HttpReply::status_type status)   {
  HttpReply rep;
  rep.status  = status;
  rep.content = http_stock_replies::to_vector(status);
  rep.headers.resize(2);
  rep.headers[0].name  = "Content-Length";
  rep.headers[0].value = std::to_string(rep.content.size());
  rep.headers[1].name  = "Content-Type";
  rep.headers[1].value = "text/html";
  return rep;
}

/// Access HTTP header string
std::string HttpReply::stock_status(HttpReply::status_type status)   {
  return http_status_strings::header_string(status);
}

/// Access header by name
const HttpHeader* HttpReply::header(const std::string& name)    const   {
  for ( const auto& h : headers )   {
    if ( 0 == ::strcasecmp(h.name.c_str(), name.c_str()) )
      return &h;
  }
  return 0;
}

/// Formatted header information (all except content) printout with prefix
std::ostream& HttpReply::print_header(std::ostream& os, const std::string& prefix)  const   {
  std::string status_str = http::to_string(*this);
  if ( status_str.empty() )   {
    status_str = stock_status(this->status);
    status_str = status_str.substr(0, status_str.length()-2);
  }
  os   << prefix << "HTTP status:  " << this->status << std::endl;
  os   << prefix << "HTTP status:  " << status_str << std::endl;
  for ( const auto& h : this->headers     ) h.print(os, prefix);
  for ( const auto& h : this->userHeaders ) h.print(os, prefix);
  return os;
}

/// Formatted header information (all except content) printout with prefix
std::ostream& HttpReply::print_content(std::ostream& os, const std::string& prefix)  const   {
  os   << prefix << "HTTP content: '" << (char*)&this->content[0] << "'" << std::endl;
  return os;
}

/// Formatted printout with prefix
std::ostream& HttpReply::print(std::ostream& os, const std::string& prefix)  const   {
  print_header(os, prefix);
  return print_content(os, prefix);
}

/// Clone reply
HttpReply HttpReply::clone()   const    {
  HttpReply reply;
  reply.status      = this->status;
  reply.headers     = this->headers;
  reply.userHeaders = this->userHeaders;
  reply.content     = this->content;
  reply.bytes_total = this->bytes_total;
  reply.bytes_sent  = this->bytes_sent;
  reply.bytes_data  = this->bytes_data;
  return reply;
}

/// Convert the reply into a vector of buffers. The buffers do not own the
std::vector<boost::asio::const_buffer> http::to_buffers(const HttpReply& rep)  {
  std::vector<boost::asio::const_buffer> buffers;
  buffers.push_back(http_status_strings::to_buffer(rep.status));
  for(const auto& h : rep.headers)   {
    buffers.emplace_back(boost::asio::buffer(h.name));
    buffers.emplace_back(boost::asio::buffer(http_misc_strings::name_value_separator));
    buffers.emplace_back(boost::asio::buffer(h.value));
    buffers.emplace_back(boost::asio::buffer(http_misc_strings::crlf));
  }
  for(const auto& h : rep.userHeaders)  {
    buffers.emplace_back(boost::asio::buffer(h.name));
    buffers.emplace_back(boost::asio::buffer(http_misc_strings::name_value_separator));
    buffers.emplace_back(boost::asio::buffer(h.value));
    buffers.emplace_back(boost::asio::buffer(http_misc_strings::crlf));
  }
  buffers.emplace_back(boost::asio::buffer(http_misc_strings::crlf));
  if ( !rep.content.empty() )   {
    buffers.emplace_back(boost::asio::buffer(rep.content));
  }
  return buffers;
}
