//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================
// Please note:
// This implementation was directly deduced (with small changes) from 
// one of the boost::asio examples. 
// boost::asio: one of the best I/O libraries ever !
//
// Hence:
//
//==========================================================================
//
// Copyright (c) 2003-2015 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//
//==========================================================================

/// Framework include files
#include <HTTP/HttpServer.h>
#include <HTTP/Asio.h>
#include <RTL/Compress.h>
#include <RTL/rtl.h>

/// C/C++ include files
#include <iostream>
#include <cstring>
#include <chrono>
#include <mutex>
#include <list>

using namespace http;
using namespace boost;
using std::chrono::system_clock;
using std::chrono::milliseconds;

/// Initializing constructor for a connection with the given socket.
HttpConnection::HttpConnection(asio::ip::tcp::socket s,
			       HttpConnectionManager& m,
			       HttpRequestHandler& h,
			       size_t buffer_size)
  : socket(std::move(s)), manager(m), handler(h)
{
  if ( buffer_size > 0 ) buffer.resize(buffer_size);
  else buffer.resize(16*1024);
  request.socket   = &this->socket;
  request.state    = HttpRequestParser::indeterminate;
  request.start    = system_clock::now();
  request.netio    = std::chrono::time_point<system_clock>(system_clock::duration::zero());
  request.handling = std::chrono::time_point<system_clock>(system_clock::duration::zero());
  ++handler.num_connections_opened;
  ++handler.num_connections;
}

/// Default destructor
HttpConnection::~HttpConnection()   {
  ++handler.num_connections_closed;
  --handler.num_connections;
  if ( handler.debug )   {
    std::cout << "HttpConnection: DESTRUCT." << std::endl << std::flush;
  }
}

/// Helper routine
void HttpConnection::print_next(int next_action)  const   {
  if ( handler.debug )   {
    std::cout << "HttpConnection: next action: ";      
    switch(next_action)   {
    case HttpRequestHandler::read:
      std::cout << "READ.";
      break;
    case HttpRequestHandler::write:
      std::cout << "WRITE.";
      break;
    case HttpRequestHandler::close:
      std::cout << "CLOSE.";
      break;
    case HttpRequestHandler::none:
      std::cout << "NONE.";
      break;
    default:
      std::cout << "UNKNOWN: " << next_action;
      break;
    }
    std::cout << std::endl << std::flush;
  }
}

/// Stop all asynchronous operations associated with the connection.
void HttpConnection::stop()     {
  long hdl = socket.native_handle();
  if ( socket.is_open() )   {
    system::error_code ec;
    socket.close(ec);
    ++handler.num_connections_stopped;
    if ( ec )   {
      std::cout << "HttpConnection: STOP: " << ec.message() << std::endl << std::flush;
    }
    auto now = system_clock::now();
    request.netio += std::chrono::duration_cast<milliseconds>(now-request.start);
    request.start = now;
    handler.handle_stat(request, reply);
  }
  if ( handler.debug )   {
    std::cout << "HttpConnection: STOP: socket closed: " << hdl << std::endl << std::flush;
  }
}

/// Send synchronously direct reply
void HttpConnection::send(const HttpReply& rep, system::error_code& ec)   {
  this->send(to_buffers(rep), ec);
}

/// Send synchronously asio buffer via socket
void HttpConnection::send(const boost::asio::const_buffer& buff, system::error_code& ec)   {
  this->send(std::vector<boost::asio::const_buffer>(1,buff), ec);
}

/// Send synchronously multiple asio buffers via socket
void HttpConnection::send(const std::vector<boost::asio::const_buffer>& buffers, system::error_code& ec)   {
  size_t snd_len = 0, snd_tot = 0;
  for( const auto& b : buffers )
    snd_len += boost::asio::detail::buffer_size(b);
  snd_len = asio::write(this->socket, buffers, ec);
  if ( !ec && (snd_len != snd_tot) )  {
    ec = system::error_code(errno, system::system_category());
  }
}

/// Execute follow-up action 
void HttpConnection::do_next(int next_action)   {
  print_next(next_action);
  if ( next_action == HttpRequestHandler::read )   {
    if ( request.content_length - request.content_received > request.content.size() )
      do_read();
    else
      do_read_exact(request.content_length - request.content_received);
  }
  else if ( next_action == HttpRequestHandler::write )   {
    do_write();
  }
  else if ( next_action == HttpRequestHandler::close )   {
    // We do not shurdown immediately.
    // When the connection gets closed by the client,
    // shutdown will be Invoked automatically.
    handler.handle_shutdown(request, reply);
    do_shutdown();
    auto self(shared_from_this());
    if ( this->buffer.size() < 128 ) this->buffer.resize(128);
    socket.async_read_some
      (asio::buffer(this->buffer.data(), this->buffer.size()),
       [this, self](system::error_code ec, std::size_t bytes)  {
	 if ( handler.debug )   {
	   std::cout << "HttpConnection: close: STOP "
		     << bytes << " bytes. Error code[" 
		     << ec.value() << "]: " << ec.message()
		     << std::endl << std::flush;
	 }
	 system::error_code ignored_ec;
	 socket.shutdown(asio::ip::tcp::socket::shutdown_both, ignored_ec);
	 if ( ec != asio::error::operation_aborted )
	   {
	     manager.stop(shared_from_this());
	     if ( self.get() ) {}
	   }
       });
  }
  else if ( next_action == HttpRequestHandler::close_read )   {
    // We do not shutdown immediately.
    // When the connection gets closed by the client,
    // shutdown will be invoked automatically.
    auto self(shared_from_this());
    if ( this->buffer.size() < 128 ) this->buffer.resize(128);
    socket.async_read_some
      (asio::buffer(this->buffer.data(), this->buffer.size()),
       [this, self](system::error_code ec, std::size_t bytes)  {
	if ( handler.debug )   {
	   std::cout << "HttpConnection: close_read: STOP "
		     << bytes << " bytes. Error code: " 
		     << ec.value() << ": [" << ec.message() << "]"
		     << std::endl << std::flush;
	}
	system::error_code ignored_ec;
	socket.shutdown(asio::ip::tcp::socket::shutdown_both, ignored_ec);
	manager.stop(shared_from_this());
	if ( self.get() ) {}
      });
  }
  else if ( next_action == HttpRequestHandler::close_write )   {
    static std::mutex lock;
    static std::list<HttpConnection*> prev;
    std::list<HttpConnection*> to_close;
    this->do_shutdown();
    {
      std::lock_guard<std::mutex> _lck(lock);
      if ( prev.size() > 10 /* handler.num_threads */ )   {
	to_close.push_back(prev.back());
	prev.pop_back();
      }
      prev.push_front(this);
    }
    for(auto* c : to_close)
      c->do_close();
#if 0
    auto self(shared_from_this());
    asio::async_write
      (socket, to_buffers(reply),
       [this, self](system::error_code ec, std::size_t)    {
	 if ( !ec )   {
	   system::error_code ignored_ec;
	   socket.shutdown(asio::ip::tcp::socket::shutdown_send, ignored_ec);
	 }
	 if ( ec != asio::error::operation_aborted )  {
	   manager.stop(shared_from_this());
	   if ( self.get() ) {}
	 }
       });
#endif
  }
  else   {
    do_shutdown();
  }
}

/// Perform an graceful shutdown of the connection
void HttpConnection::do_shutdown()   {
  system::error_code ignored_ec;
  socket.shutdown(asio::ip::tcp::socket::shutdown_send, ignored_ec);
  //socket.shutdown(asio::ip::tcp::socket::shutdown_both,ignored_ec);
  if ( handler.debug )   {
    std::cout << "HttpConnection: SHUTDOWN." << std::endl << std::flush;
  }
}

/// Perform an graceful shutdown of the connection
void HttpConnection::do_close()   {
  manager.stop(shared_from_this());
}

/// Perform an asynchronous read operation.
void HttpConnection::do_read()   {
  auto self(shared_from_this());
  auto handler_function =
    [this, self](system::error_code ec, std::size_t len)   {
      auto& content = request.content;
      if ( !ec )    {
	HttpRequestParser::result_type result = (HttpRequestParser::result_type)request.state;
	/// Parse transport header if not yet done
	if ( result == HttpRequestParser::indeterminate )  {
	  content.clear();
	  std::tie(result, std::ignore) = parser.parse(request, buffer.data(), buffer.data() + len);
	  request.state = result;
	}
	/// Transport header successfully parsed. Look at the content
	if ( result == HttpRequestParser::good )  {
	  auto  now      = system_clock::now();
	  request.netio += std::chrono::duration_cast<milliseconds>(now-request.start);
	  request.start  = now;
	  /// First extract the HTTP header information
	  if ( request.content_length == HttpRequest::invalid_content_length )  {
	    buffer.clear();
	    request.content_received = content.size();
	    for(const auto& h : request.headers )  {
	      if ( h.name == http::constants::content_length )  {
		request.content_length = ::atol(h.value.c_str());
	      }
	    }
	    auto next = handler.handle_request(request, reply);
	    now = system_clock::now();
	    request.handling += std::chrono::duration_cast<milliseconds>(now-request.start);
	    request.start = now;
	    do_next(next);
	    return;
	  }
	  else   {
	    request.content_received += len;
	  }
	  /// All data were received. Call the handler
	  if ( request.content_received >= request.content_length )  {
	    now = system_clock::now();
	    auto next = handler.handle_request(request, reply);
	    request.handling += std::chrono::duration_cast<milliseconds>(now-request.start);
	    request.start = now;
	    do_next(next);
	    return;
	  }
	  /// Restart reading cycle to fill the data buffer
	  request.start = now;
	  do_read_exact(request.content_length - request.content_received);
	}
	else if ( result == HttpRequestParser::bad )	  {
	  if ( handler.debug )   {
	    std::cout << "HttpConnection: read: parsing FAILED " << std::endl << std::flush;
	  }
	  reply = HttpReply::stock_reply(HttpReply::bad_request);
	  request.content_length = 0;
	  content.clear();
	  parser.consumed = 0;
	  do_write();
	}
	else  {
	  do_read();
	}
      }
      else if ( ec != asio::error::operation_aborted )     {
	system::error_code ignored_ec;
	request.content_length = 0;
	content.clear();
	parser.consumed = 0;
	std::cout << "HttpConnection: SHUTDOWN (read)." << std::endl << std::flush;
	socket.shutdown(asio::ip::tcp::socket::shutdown_both, ignored_ec);
	manager.stop(shared_from_this());
	if ( self.get() ) {}
      }
    };
  if ( request.state == HttpRequestParser::indeterminate )  {
    socket.async_read_some(asio::buffer(buffer.data(), buffer.size()), handler_function);
  }
  else   {
    size_t cont_len = request.content.size();
    size_t buff_len = request.content_length-cont_len;
    if ( request.content_length > cont_len )   {
      request.content.resize(request.content_length);
    }
    // socket.async_read_some(asio::buffer(request.content.data()+cont_len, buff_len), handler_function);
    asio::async_read(socket, asio::buffer(request.content.data()+cont_len, buff_len),
		     asio::transfer_exactly(buff_len),
		     handler_function);
  }
}

/// Perform an asynchronous read operation with exact read length
void HttpConnection::do_read_exact(size_t record_length)   {
  auto self(shared_from_this());
  auto handler_function = 
    [this, self](system::error_code ec, std::size_t len) {
      auto& content = request.content;
      if ( !ec )  {
	HttpRequestParser::result_type result = (HttpRequestParser::result_type)request.state;
	/// Parse transport header if not yet done
	if ( result == HttpRequestParser::indeterminate )  {
	  content.clear();
	  std::tie(result, std::ignore) = parser.parse(request, buffer.data(), buffer.data() + len);
	  request.state = result;
	  buffer.clear();
	}
	/// Transport header successfully parsed. Look at the content
	if ( result == HttpRequestParser::good )  {
	  auto  now          = system_clock::now();
	  auto& req_content  = request.content;
	  request.netio += std::chrono::duration_cast<milliseconds>(now-request.start);
	  request.start  = now;
	  /// First extract the HTTP header information
	  if ( request.content_length == HttpRequest::invalid_content_length )  {
	    buffer.clear();
	    for(const auto& h : request.headers )  {
	      if ( h.name == http::constants::content_length )  {
		request.content_length = ::atol(h.value.c_str());
	      }
	    }
	    request.content_received = req_content.size();
	    auto next = handler.handle_request(request, reply);
	    now = system_clock::now();
	    request.handling += std::chrono::duration_cast<milliseconds>(now-request.start);
	    request.start = now;
	    do_next(next);
	    return;
	  }
	  else   {
	    request.content_received += len;
	  }
	  /// All data were received. Call the handler
	  if ( request.content_received >= request.content_length )   {
	    now = system_clock::now();
	    auto next = handler.handle_request(request, reply);
	    request.handling += std::chrono::duration_cast<milliseconds>(now-request.start);
	    request.start = now;
	    do_next(next);
	    return;
	  }
	  /// Restart reading cycle to fill the data buffer
	  request.start = now;
	  do_read_exact(request.content_length - request.content_received);
	}
	else if ( result == HttpRequestParser::bad )	  {
	  reply = HttpReply::stock_reply(HttpReply::bad_request);
	  request.content_length = 0;
	  content.clear();
	  parser.consumed = 0;
	  do_write();
	}
	else  {
	  do_read();
	}
      }
      else if (ec != asio::error::operation_aborted)     {
	system::error_code ignored_ec;
	request.content_length = 0;
	content.clear();
	parser.consumed = 0;
	if ( handler.debug )   {
	  std::cout << "HttpConnection: SHUTDOWN (read_exact)." << std::endl << std::flush;
	}
	socket.shutdown(asio::ip::tcp::socket::shutdown_both,ignored_ec);
	manager.stop(shared_from_this());
	if ( self.get() ) {}
      }
    };
  if ( request.state == HttpRequestParser::indeterminate )  {
    asio::async_read(socket,asio::buffer(buffer.data(), buffer.size()),
		     asio::transfer_exactly(record_length),
		     handler_function);
  }
  else   {
    size_t cont_len = request.content.size();
    size_t buff_len = request.content_length-cont_len;
    request.content.resize(request.content_length);
    asio::async_read(socket, asio::buffer(request.content.data()+cont_len, buff_len),
		     asio::transfer_exactly(record_length),
		     handler_function);
  }
}

/// Perform an asynchronous write operation.
void HttpConnection::do_write()   {
  auto self(shared_from_this());
  std::vector<boost::asio::const_buffer> buffers;
  if ( 0 == reply.bytes_sent )
    buffers = to_buffers(reply);
  else if ( !reply.content.empty() )
    buffers.emplace_back(boost::asio::buffer(reply.content));
  
  for(const boost::asio::const_buffer& b : buffers)
    reply.bytes_total += boost::asio::detail::buffer_size(b);

  asio::async_write
    (socket, buffers,
     [this, self](system::error_code ec, std::size_t len)    {
       if ( handler.debug )   {
	 std::cout << "HttpConnection: do_write: Wrote " << len << " bytes to target." 
		   << " ec:" << (!ec ? "OK " : "BAD ")   << ec.value() << ": "
		   << ec.message().c_str() << std::endl << std::flush;
       }
       int next = HttpRequestHandler::none;
       if (!ec)    {
	 auto now = system_clock::now();
	 request.netio    += std::chrono::duration_cast<milliseconds>(now-request.start);
	 request.start     = now;
	 reply.bytes_sent += len;
	 next              = handler.handle_request(request, reply);
	 now               = system_clock::now();
	 request.handling += std::chrono::duration_cast<milliseconds>(now-request.start);
	 request.start     = now;
	 do_next(next);
       }
       else /* if ( ec != asio::error::operation_aborted )  */  {
	 // We know that the transaction is over. Trigger an additional receive
	 // which will fail, but which is used to close the connection.
	 socket.async_read_some(asio::buffer(buffer),
				[this, self](system::error_code erc, std::size_t bytes)  {
				  if ( handler.debug )   {
				    std::cout << "HttpConnection: do_write: STOP "
					      << bytes << " bytes.  error code[" 
					      << erc.value() << "]: " << erc.message().c_str()
					      << std::endl << std::flush;
				  }
				  manager.stop(shared_from_this());
				  if ( self.get() ) {}
				});
	 if ( self.get() ) {}
	 return;
       }
     });
}

/// Handle the next character of input.
HttpRequestParser::result_type HttpRequestParser::consume(HttpRequest& req, char input)   {
  switch (state)  {
  case method_start:
    if (!is_char(input) || is_ctl(input) || is_tspecial(input))    {
      return bad;
    }
    else    {
      state = method;
      req.method.push_back(input);
      return indeterminate;
    }
  case method:
    if (input == ' ')    {
      state = uri;
      return indeterminate;
    }
    else if (!is_char(input) || is_ctl(input) || is_tspecial(input))    {
      return bad;
    }
    else    {
      req.method.push_back(input);
      return indeterminate;
    }
  case uri:
    if (input == ' ')    {
      state = http_version_h;
      return indeterminate;
    }
    else if (is_ctl(input))    {
      return bad;
    }
    else    {
      req.uri.push_back(input);
      return indeterminate;
    }
  case http_version_h:
    if (input == 'H')    {
      state = http_version_t_1;
      return indeterminate;
    }
    return bad;
  case http_version_t_1:
    if (input == 'T')    {
      state = http_version_t_2;
      return indeterminate;
    }
    return bad;
  case http_version_t_2:
    if (input == 'T')    {
      state = http_version_p;
      return indeterminate;
    }
    return bad;
  case http_version_p:
    if (input == 'P')    {
      state = http_version_slash;
      return indeterminate;
    }
    return bad;
  case http_version_slash:
    if (input == '/')    {
      req.version_major = 0;
      req.version_minor = 0;
      state = http_version_major_start;
      return indeterminate;
    }
    return bad;
  case http_version_major_start:
    if (is_digit(input))    {
      req.version_major = req.version_major * 10 + input - '0';
      state = http_version_major;
      return indeterminate;
    }
    return bad;
  case http_version_major:
    if (input == '.')    {
      state = http_version_minor_start;
      return indeterminate;
    }
    else if (is_digit(input))    {
      req.version_major = req.version_major * 10 + input - '0';
      return indeterminate;
    }
    return bad;
  case http_version_minor_start:
    if (is_digit(input))    {
      req.version_minor = req.version_minor * 10 + input - '0';
      state = http_version_minor;
      return indeterminate;
    }
    return bad;
  case http_version_minor:
    if (input == '\r')    {
      state = expecting_newline_1;
      return indeterminate;
    }
    else if (is_digit(input))    {
      req.version_minor = req.version_minor * 10 + input - '0';
      return indeterminate;
    }
    return bad;
  case expecting_newline_1:
    if (input == '\n')    {
      state = header_line_start;
      return indeterminate;
    }
    return bad;
  case header_line_start:
    if (input == '\r')    {
      state = expecting_newline_3;
      return indeterminate;
    }
    else if (!req.headers.empty() && (input == ' ' || input == '\t'))    {
      state = header_lws;
      return indeterminate;
    }
    else if (!is_char(input) || is_ctl(input) || is_tspecial(input))    {
      return bad;
    }
    else    {
      req.headers.push_back(HttpHeader());
      req.headers.back().name.push_back(input);
      state = header_name;
      return indeterminate;
    }
  case header_lws:
    if (input == '\r')    {
      state = expecting_newline_2;
      return indeterminate;
    }
    else if (input == ' ' || input == '\t')    {
      return indeterminate;
    }
    else if (is_ctl(input))    {
      return bad;
    }
    else    {
      state = header_value;
      req.headers.back().value.push_back(input);
      return indeterminate;
    }
  case header_name:
    if (input == ':')    {
      state = space_before_header_value;
      return indeterminate;
    }
    else if (!is_char(input) || is_ctl(input) || is_tspecial(input))    {
      return bad;
    }
    else    {
      req.headers.back().name.push_back(input);
      return indeterminate;
    }
  case space_before_header_value:
    if (input == ' ')    {
      state = header_value;
      return indeterminate;
    }
    return bad;
  case header_value:
    if (input == '\r')    {
      state = expecting_newline_2;
      return indeterminate;
    }
    else if (is_ctl(input))    {
      return bad;
    }
    else    {
      req.headers.back().value.push_back(input);
      return indeterminate;
    }
  case expecting_newline_2:
    if (input == '\n')    {
      state = header_line_start;
      return indeterminate;
    }
    return bad;
  case expecting_newline_3:
    return (input == '\n') ? good : bad;
  default:
    return bad;
  }
}

/// Check if a byte is defined as an HTTP tspecial character.
bool HttpRequestParser::is_tspecial(int c)    {
  switch (c)
    {
    case '(': case ')': case '<': case '>': case '@':
    case ',': case ';': case ':': case '\\': case '"':
    case '/': case '[': case ']': case '?': case '=':
    case '{': case '}': case ' ': case '\t':
      return true;
    default:
      return false;
    }
}

/// Construct with a directory containing files to be served.
HttpRequestHandler::HttpRequestHandler()
  : io_context(),
    signals(io_context),
    acceptor(io_context),
    manager(),
    socket(io_context)
{
  signals.add(SIGINT);
  signals.add(SIGTERM);
#if defined(SIGQUIT)
  signals.add(SIGQUIT);
#endif // defined(SIGQUIT)
  do_await_stop();
}

/// Default destructor
HttpRequestHandler::~HttpRequestHandler()    {
}

/// Get date and time string for use in headers
std::string HttpRequestHandler::get_date_string()  const   {
  char        str[64];
  std::time_t now = ::time(0);
  std::string tim = ::ctime_r(&now, str);
  tim = tim.substr(0,tim.find('\n'));
  return tim;
}

// If we can gzip the output, here it is done
bool HttpRequestHandler::compress_reply(const HttpRequest& req, HttpReply& reply, std::string& encoding)  const   {
  std::size_t len = ::strlen("Accept-Encoding");
  encoding.clear();
  for(const auto& h : req.headers)  {
    if ( ::strncasecmp(h.name.c_str(),"Accept-Encoding",len) == 0 )  {
      std::string used;
      auto data = std::move(reply.content);
      reply.content = Online::compress::compress(h.value, data, used);
      if ( !used.empty() )  {
	std::string size = std::to_string(reply.content.size());
	reply.userHeaders.emplace_back(HttpHeader("Content-Encoding",used));
	len = ::strlen("Content-Length");
	encoding = used;
	for(auto& rh : reply.headers)  {
	  if ( ::strncasecmp(rh.name.c_str(),"Content-Length",len) == 0 )  {
	    rh.value = size;
	    return true;
	  }
	}
	reply.headers.emplace_back(HttpHeader("Content-Length",size));
	return true;
      }
      reply.content = std::move(data);
    }
  }
  return false;
}

// If we can gzip the output, here it is done
bool HttpRequestHandler::compress_reply(const HttpRequest& req, HttpReply& reply)  const   {
  std::string encoding;
  return this->compress_reply(req, reply, encoding);
}

void HttpRequestHandler::open(const std::string& address,const std::string&  port)  {
  // Open the acceptor with the option to reuse the address (i.e. SO_REUSEADDR).
  asio::ip::tcp::resolver resolver(io_context);
  asio::ip::tcp::endpoint endpoint =
#if BOOST_ASIO_VERSION < 103400
    *resolver.resolve({asio::ip::tcp::v4(), address, port});
#else    // version >= 1.34.0
    *resolver.resolve(asio::ip::tcp::v4(), address, port).begin();
#endif
  acceptor.open(endpoint.protocol());
  acceptor.set_option(asio::ip::tcp::acceptor::reuse_address(true));
  acceptor.set_option(asio::socket_base::linger(true,0));
  acceptor.bind(endpoint);
  acceptor.listen();
  do_accept();
}

/// Wait until all threads finished to exit server cleanly
void HttpRequestHandler::join()  {
  // Wait for all threads in the pool to exit.
  for (auto& t : threads)
    t->join();
}

/// Run the server's io_context loop. If specified additional workers may be used
void HttpRequestHandler::run(int num_additional_threads)   {
  // The io_context::run() call will block until all asynchronous operations
  // have finished. While the server is running, there is always at least one
  // asynchronous operation outstanding: the asynchronous accept call waiting
  // for new incoming connections.
  try   {
    for (int i = 0; i < num_additional_threads; ++i)   {
      std::shared_ptr<std::thread> thread(new std::thread([this]{ this->io_context.run(); }));
      threads.push_back(thread);
    }
    io_context.run();
    join();
  }
  catch(const std::exception& e)  {
    lib_rtl_output(LIB_RTL_ERROR,"xmlrpc::HttpServer",
		   "XMLRPC: Failed to start RpcServer message pump. [%s]",e.what());
  }
}

void HttpRequestHandler::do_accept()   {
  acceptor.async_accept
    (socket, [this](system::error_code ec)	 {
      // Check whether the server was stopped by a signal before this
      // completion handler had a chance to run.
      if (!acceptor.is_open())     {
        return;
      }
      if (!ec)   {
        manager.start(std::make_shared<HttpConnection>(std::move(socket), manager, *this, buffer_size));
      }
      do_accept();
    });
}

void HttpRequestHandler::do_await_stop()    {
  signals.async_wait
    ([this](system::error_code /*ec*/, int /*signo*/)     {
      // The server is stopped by cancelling all outstanding asynchronous
      // operations. Once all operations have finished the io_context::run()
      // call will exit.
      acceptor.close();
      manager.stop_all();
    });
}

/// Specific handler for GET requests
HttpRequestHandler::continue_action
HttpRequestHandler::handle_get(const HttpRequest& /* req */, HttpReply& rep)   {
  rep = HttpReply::stock_reply(HttpReply::bad_request);
  return write;
}

/// Specific handler for PUT requests
HttpRequestHandler::continue_action
HttpRequestHandler::handle_put(const HttpRequest& /* req */, HttpReply& rep)   {
  rep = HttpReply::stock_reply(HttpReply::bad_request);
  return write;
}

/// Specific handler for POST requests
HttpRequestHandler::continue_action
HttpRequestHandler::handle_post(const HttpRequest& /* req */, HttpReply& rep)   {
  rep = HttpReply::stock_reply(HttpReply::bad_request);
  return write;
}

/// Specific handler for DELETE requests
HttpRequestHandler::continue_action
HttpRequestHandler::handle_delete(const HttpRequest& /* req */, HttpReply& rep)   {
  rep = HttpReply::stock_reply(HttpReply::bad_request);
  return write;
}

/// Specific handler for UPDATE requests
HttpRequestHandler::continue_action
HttpRequestHandler::handle_update(const HttpRequest& /* req */, HttpReply& rep)   {
  rep = HttpReply::stock_reply(HttpReply::bad_request);
  return write;
}

/// Handle possible statistics summaries.
void HttpRequestHandler::handle_stat(const HttpRequest& /* req */, HttpReply& /* rep */)   {
}

/// Handle a request and produce a reply.
HttpRequestHandler::continue_action
HttpRequestHandler::handle_request(const HttpRequest& req, HttpReply& rep)   {
  if ( req.uri.empty() )   {
    rep = HttpReply::stock_reply(HttpReply::bad_request);
    rep.headers.emplace_back(http::constants::error_cause,"Empty request uri");
    return write;
  }
  else if ( req.method == http::constants::get )
    return handle_get(req, rep);
  else if ( req.method == http::constants::put )
    return handle_put(req, rep);
  else if ( req.method == http::constants::post )
    return handle_post(req, rep);
  else if ( req.method == http::constants::del )
    return handle_delete(req, rep);
  else if ( req.method == http::constants::update )
    return handle_update(req, rep);
  else
    rep = HttpReply::stock_reply(HttpReply::not_acceptable);
  return write;
}

/// Callback to eventually handle actions on connection shutdown
void HttpRequestHandler::handle_shutdown(const HttpRequest& /* req */, const HttpReply& /* rep */)   {
}

/// Construct the default connection manager.
HttpConnectionManager::HttpConnectionManager()    {
}

/// Add the specified connection to the manager and start it.
void HttpConnectionManager::start(HttpConnectionPtr c)  {
  {
    std::lock_guard<std::mutex> guard(connection_lock);
    connections.insert(c);
  }
  c->start();
}

/// Stop the specified connection.
void HttpConnectionManager::stop(HttpConnectionPtr c)   {
  { std::lock_guard<std::mutex> guard(connection_lock);
    connections.erase(c);
  }
  //c->stop();
#if BOOST_ASIO_VERSION < 103400
  c->get_handler().io_context.post([c](){ c->stop(); });
#else
  asio::post(c->get_handler().io_context.get_executor(), [c](){ c->stop(); });
#endif
}

/// Stop all connections.
void HttpConnectionManager::stop_all()   {
  {
    std::lock_guard<std::mutex> guard(connection_lock);
    for (auto c: connections)
      c->stop();
    connections.clear();
  }
}
