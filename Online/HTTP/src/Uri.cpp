//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include "HTTP/Uri.h"

// C/C++ include files
#include <iostream>
#include <algorithm>

using namespace std;

/// Namespace for the http server and client apps
namespace    {
  static void parse_uri(http::Uri& uri, const string& url_s)   {
    const string prot_end("://");
    string::const_iterator prot_i = search(url_s.begin(),url_s.end(),prot_end.begin(),prot_end.end());
    if ( prot_i != url_s.end() )   {
      uri.protocol.reserve(distance(url_s.begin(), prot_i));
      transform(url_s.begin(), prot_i, back_inserter(uri.protocol), ::tolower); // protocol is icase
      advance(prot_i, prot_end.length());
    }
    else   {
      prot_i = url_s.begin();
    }
    string::const_iterator path_i = find(prot_i, url_s.end(), '/');
    uri.host.reserve(distance(prot_i, path_i));
    transform(prot_i, path_i, back_inserter(uri.host), ::tolower); // host is icase
    string::const_iterator query_i = find(path_i, url_s.end(), '?');
    uri.path.assign(path_i, query_i);
    if( query_i != url_s.end() )
      ++query_i;
    uri.query.assign(query_i, url_s.end());
    size_t idx = uri.host.find(':');
    if ( idx != string::npos )  {
      uri.port = uri.host.substr(idx+1);
      uri.host = uri.host.substr(0,idx);
    }
  }
}

http::Uri::Uri(const string& url)   {
  parse_uri(*this, url);
}
