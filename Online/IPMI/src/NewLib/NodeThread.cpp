//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
/*
 * NodeThread.cpp
 *
 *  Created on: Feb 11, 2016
 *      Author: beat
 */

#include <string>
#include <mutex>
//#include <vector>
#include "string.h"
#include "stdio.h"
#include "unistd.h"
#include "stdlib.h"
#include "ctype.h"
#include "dim/dis.hxx"
#include "log.h"

#include "IPMINode.h"
#include "ipmi_intf.h"
#include "NodeThread.h"
#include "ipmi_constants.h"
#include "lan.h"
#include "LANPlus.h"

using namespace std;

template <class T> void NodeThread(NodeConfiguration *config,string *setup_fail, mutex *setup_fail_mtx )
{
  int status;
  string uname;
  string pword;
  uname = config->UserName;
  pword = config->Password;
  string nam = config->NodeName;
  string *ServicePrefix = config->ServicePrefix;
  IPMINode<T> *nod;
  while (1)
  {
//    m.lock();

    nod = new IPMINode<T>(nam, uname, pword);
    status = nod->setup((char*)ServicePrefix->c_str(),*config);
    if (status == 0)
    {
//      m.unlock();
//      Printf("Setup of node %s successful\n", nam.c_str());
      setup_fail_mtx->lock();
      size_t fpos = setup_fail->find(nam);
      if (fpos != string::npos)
      {
        setup_fail->replace(fpos-1,(nam+" ").size(),"");
      }
      setup_fail_mtx->unlock();
      break;
    }
    setup_fail_mtx->lock();
    size_t fpos = setup_fail->find(nam);
    if (fpos == string::npos)
    {
      setup_fail->append(" "+nam);
    }
    setup_fail_mtx->unlock();

//    Printf("node setup failed for %s\n", nam.c_str());
    int session_setup_code = nod->m_ipmiInterface->get_session_setup_code();
    delete nod;
    if (session_setup_code ==SessSup_PingFail)
    {
      sleep(10*config->setupRetryDelay);
    }
    else if (session_setup_code ==SessSup_BadUserName)
    {
      sleep(100*config->setupRetryDelay);
    }
    else if (session_setup_code ==SessSup_NEXNode)
    {
      sleep(100*config->setupRetryDelay);
    }
    else
    {
      sleep(config->setupRetryDelay);
    }
  }
  sleep(1);
  while (1)
  {
    status = nod->getPowerState();
    if (status < 0)
    {
      Printf(
          "Failed to get power status from node %s\nDeleting and restarting node...\n",
          nam.c_str());
      delete (nod);
      sleep(2);
      while (1)
      {
        nod = new IPMINode<T>(nam, uname, pword);
        status = nod->setup((char*)ServicePrefix->c_str(),*config);
        if (status == 0)
        {
//          Printf("Setup of node %s successful\n", nam.c_str());
          setup_fail_mtx->lock();
          size_t fpos = setup_fail->find(nam);
          if (fpos != string::npos)
          {
            setup_fail->replace(fpos-1,(nam+" ").size(),"");
          }
          setup_fail_mtx->unlock();
          break;
        }
//        Printf("node setup failed for %s\n", nam.c_str());
        int session_setup_code = nod->m_ipmiInterface->get_session_setup_code();
        delete nod;
        setup_fail_mtx->lock();
        size_t fpos = setup_fail->find(nam);
        if (fpos == string::npos)
        {
          setup_fail->append(" "+nam);
        }
        setup_fail_mtx->unlock();
        if (session_setup_code ==SessSup_PingFail)
        {
          sleep(10*config->setupRetryDelay);
        }
        else if (session_setup_code ==SessSup_BadUserName)
        {
          sleep(100*config->setupRetryDelay);
        }
        else
        {
          sleep(config->setupRetryDelay);
        }
      }
      sleep(1);
    }
    if (nod->m_cmd->getNext() == 1)
    {
      char *cmd = nod->m_cmd->getString();
      Printf("received command %s for node %s\n", cmd, nod->m_Name.c_str());
      if (strcmp(cmd, "on") == 0)
      {
        nod->turnOn();
      }
      else if (strcmp(cmd, "off") == 0)
      {
        nod->turnOff();
      }
      else if (strcmp(cmd, "cycle") == 0)
      {
        nod->cycle();
      }
      else if (strcmp(cmd, "reset") == 0)
      {
        nod->reset();
      }
      else if (strcmp(cmd, "soft") == 0)
      {
        nod->softOff();
      }
      else
      {
        Printf("unknown command %s for node %s\n", cmd, nod->m_Name.c_str());
      }
//      status = nod->getPowerState();
//      if (status <0)
//      {
//        delete (nod);
//        nod = new IPMINode(nam,uname,pword);
//        while (1)
//        {
//          status = nod->setup(ServerName);
//          if (status == 0)
//          {
//            break;
//          }
//          Printf ("node setup failed for %s\n",nam.c_str());
//          sleep(10);
//        }
//      }
    }
    sleep(config->statePollDelay);
  }
}
template void NodeThread<lan>(NodeConfiguration *config,string *setup_fail, mutex *setup_fail_mtx );
template void NodeThread<LANPlus>(NodeConfiguration *config,string *setup_fail, mutex *setup_fail_mtx );

