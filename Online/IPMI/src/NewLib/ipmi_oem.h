//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
/*
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * Redistribution of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * Redistribution in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * Neither the name of Sun Microsystems, Inc. or the names of
 * contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * This software is provided "AS IS," without a warranty of any kind.
 * ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES,
 * INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE OR NON-INFRINGEMENT, ARE HEREBY EXCLUDED.
 * SUN MICROSYSTEMS, INC. ("SUN") AND ITS LICENSORS SHALL NOT BE LIABLE
 * FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING
 * OR DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.  IN NO EVENT WILL
 * SUN OR ITS LICENSORS BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA,
 * OR FOR DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR
 * PUNITIVE DAMAGES, HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF
 * LIABILITY, ARISING OUT OF THE USE OF OR INABILITY TO USE THIS SOFTWARE,
 * EVEN IF SUN HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 */

#ifndef IPMI_OEM_H
#define IPMI_OEM_H

//#include "ipmi.h"
template <class  G> class ipmi_intf;
class LANPlus;
class lan;
/* oem handler, see lib/ipmi_oem.c */
template <class T> class ipmi_oem_handle {
  public:
    const char * name;
    const char * desc;
    int (*setup)(ipmi_intf<T> * intf);
};

template <class T> class ipmi_oem
{
  public:
    ipmi_intf<T> *intf;
    ipmi_oem(ipmi_intf<T> *i)
    {
      intf = i;
    }
    void ipmi_oem_print(void);
    int ipmi_oem_setup( char * oemtype);
    int ipmi_oem_active( const char * oemtype);

};
//template <class T> void ipmi_oem_print(void);
//template <class T> int ipmi_oem_setup( ipmi_intf<T> * intf, char * oemtype);
//template <class T> int ipmi_oem_active( ipmi_intf<T> * intf, const char * oemtype);
//
//
#endif /*IPMI_OEM_H*/
