//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
/*
 * Copyright (c) 2003 Sun Microsystems, Inc.  All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * Redistribution of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * Redistribution in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * Neither the name of Sun Microsystems, Inc. or the names of
 * contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * This software is provided "AS IS," without a warranty of any kind.
 * ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES,
 * INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE OR NON-INFRINGEMENT, ARE HEREBY EXCLUDED.
 * SUN MICROSYSTEMS, INC. ("SUN") AND ITS LICENSORS SHALL NOT BE LIABLE
 * FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING
 * OR DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.  IN NO EVENT WILL
 * SUN OR ITS LICENSORS BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA,
 * OR FOR DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR
 * PUNITIVE DAMAGES, HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF
 * LIABILITY, ARISING OUT OF THE USE OF OR INABILITY TO USE THIS SOFTWARE,
 * EVEN IF SUN HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <unistd.h>
#include <signal.h>
#include "log.h"
#include "lan.h"
#include "LANPlus.h"
//#include <ipmitool/ipmi.h>
//#include <ipmitool/ipmi_intf.h>
//#include <ipmitool/helper.h>
//#include <ipmitool/log.h>
//#include <ipmitool/ipmi_lanp.h>
//#include <ipmitool/ipmi_session.h>
//#include <ipmitool/ipmi_strings.h>
//#include <ipmitool/bswap.h>
#include "ipmi.h"
#include "ipmi_intf.h"
#include "ipmi_session.h"
#include "ipmi_strings.h"



/*
 * print_session_info_csv
 */
template<typename T>  ipmi_session<T>::ipmi_session(ipmi_intf<T> *i)
    {
      iface = i;
      hostname = 0;
    };

template<typename T> void ipmi_session<T>::print_session_info_csv(const struct  get_session_info_rsp * session_info, int data_len)
{
  char     buffer[18];
  uint16_t console_port_tmp;

  Printf("%d", session_info->session_handle);
  Printf(",%d", session_info->session_slot_count);
  Printf(",%d", session_info->active_session_count);

  if (data_len == 3)
  {
    /* There is no session data here*/
    Printf("\n");
    return;
  }

  Printf(",%d", session_info->user_id);
  Printf(",%s", iface->helper->val2str(session_info->privilege_level, ipmi_privlvl_vals));

  Printf(",%s", session_info->auxiliary_data?
      "IPMIv2/RMCP+" : "IPMIv1.5");

  Printf(",0x%02x", session_info->channel_number);

  if (data_len == 18)
  {
    /* We have 802.3 LAN data */
    Printf(",%s", inet_ntop(AF_INET,
            &(session_info->channel_data.lan_data.console_ip),
            buffer,
            16));

    Printf(",%02x:%02x:%02x:%02x:%02x:%02x",
        session_info->channel_data.lan_data.console_mac[0],
        session_info->channel_data.lan_data.console_mac[1],
        session_info->channel_data.lan_data.console_mac[2],
        session_info->channel_data.lan_data.console_mac[3],
        session_info->channel_data.lan_data.console_mac[4],
        session_info->channel_data.lan_data.console_mac[5]);

    console_port_tmp = session_info->channel_data.lan_data.console_port;
    #if WORDS_BIGENDIAN
    console_port_tmp = BSWAP_16(console_port_tmp);
    #endif
    Printf(",%d", console_port_tmp);
  }


  else if ((data_len == 12) || (data_len == 14))
  {
    /* Channel async serial/modem */
    Printf(",%s",
        iface->helper->val2str(session_info->channel_data.modem_data.session_channel_activity_type,
            ipmi_channel_activity_type_vals));

    Printf(",%d",
        session_info->channel_data.modem_data.destination_selector);

    Printf(",%s",
        inet_ntop(AF_INET,
            &(session_info->channel_data.modem_data.console_ip),
            buffer,
            16));

    if (data_len == 14)
    {
      /* Connection is PPP */
      console_port_tmp = session_info->channel_data.lan_data.console_port;
      #if WORDS_BIGENDIAN
      console_port_tmp = BSWAP_16(console_port_tmp);
      #endif
      Printf(",%d", console_port_tmp);
    }
  }

  Printf("\n");
}



/*
 * print_session_info_verbose
 */
template <typename T> void ipmi_session<T>::print_session_info_verbose(const struct  get_session_info_rsp * session_info, int data_len)
{
  char     buffer[18];
  uint16_t console_port_tmp;

  Printf("session handle                : %d\n", session_info->session_handle);
  Printf("slot count                    : %d\n", session_info->session_slot_count);
  Printf("active sessions               : %d\n", session_info->active_session_count);

  if (data_len == 3)
  {
    /* There is no session data here */
    Printf("\n");
    return;
  }

  Printf("user id                       : %d\n", session_info->user_id);
  Printf("privilege level               : %s\n",
      iface->helper->val2str(session_info->privilege_level, ipmi_privlvl_vals));

  Printf("session type                  : %s\n", session_info->auxiliary_data?
      "IPMIv2/RMCP+" : "IPMIv1.5");

  Printf("channel number                : 0x%02x\n", session_info->channel_number);


  if (data_len == 18)
  {
    /* We have 802.3 LAN data */
    Printf("console ip                    : %s\n",
        inet_ntop(AF_INET,
            &(session_info->channel_data.lan_data.console_ip),
            buffer,
            16));

    Printf("console mac                   : %02x:%02x:%02x:%02x:%02x:%02x\n",
        session_info->channel_data.lan_data.console_mac[0],
        session_info->channel_data.lan_data.console_mac[1],
        session_info->channel_data.lan_data.console_mac[2],
        session_info->channel_data.lan_data.console_mac[3],
        session_info->channel_data.lan_data.console_mac[4],
        session_info->channel_data.lan_data.console_mac[5]);

    console_port_tmp = session_info->channel_data.lan_data.console_port;
    #if WORDS_BIGENDIAN
    console_port_tmp = BSWAP_16(console_port_tmp);
    #endif
    Printf("console port                  : %d\n", console_port_tmp);
  }


  else if ((data_len == 12) || (data_len == 14))
  {
    /* Channel async serial/modem */
    Printf("Session/Channel Activity Type : %s\n",
        iface->helper->val2str(session_info->channel_data.modem_data.session_channel_activity_type,
            ipmi_channel_activity_type_vals));

    Printf("Destination selector          : %d\n",
        session_info->channel_data.modem_data.destination_selector);

    Printf("console ip                    : %s\n",
        inet_ntop(AF_INET,
            &(session_info->channel_data.modem_data.console_ip),
            buffer,
            16));

    if (data_len == 14)
    {
      /* Connection is PPP */
      console_port_tmp = session_info->channel_data.lan_data.console_port;
      #if WORDS_BIGENDIAN
      console_port_tmp = BSWAP_16(console_port_tmp);
      #endif
      Printf("console port                  : %d\n", console_port_tmp);
    }
  }

  Printf("\n");
}


template <typename T> void ipmi_session<T>::print_session_info(const struct  get_session_info_rsp * session_info,
                int data_len)
{
  if (csv_output)
    print_session_info_csv(session_info, data_len);
  else
    print_session_info_verbose(session_info, data_len);
}


/*
 * ipmi_get_session_info
 *
 * returns 0 on success
 *         -1 on error
 */
template <typename T> int ipmi_session<T>::ipmi_get_session_info( Ipmi_Session_Request_Type  session_request_type,
            uint32_t id_or_handle)
{
  int i, retval = 0;

  struct ipmi_rs * rsp;
  struct ipmi_rq req;
  uint8_t rqdata[5]; //  max length of the variable length request
  struct get_session_info_rsp   session_info;
  req.zero();
//  memset(&req, 0, sizeof(req));
  memset(&session_info, 0, sizeof(session_info));
  req.msg.netfn = IPMI_NETFN_APP;        // 0x06
  req.msg.cmd   = IPMI_GET_SESSION_INFO; // 0x3D
  req.msg.data = rqdata;

  switch (session_request_type)
  {

  case IPMI_SESSION_REQUEST_CURRENT:
  case IPMI_SESSION_REQUEST_BY_ID:
  case IPMI_SESSION_REQUEST_BY_HANDLE:
    switch (session_request_type)
    {
    case IPMI_SESSION_REQUEST_CURRENT:
      rqdata[0]        = 0x00;
      req.msg.data_len = 1;
      break;
    case IPMI_SESSION_REQUEST_BY_ID:
      rqdata[0]        = 0xFF;
      rqdata[1]        = id_or_handle         & 0x000000FF;
      rqdata[2]        = (id_or_handle >> 8)  & 0x000000FF;
      rqdata[3]        = (id_or_handle >> 16) & 0x000000FF;
      rqdata[4]        = (id_or_handle >> 24) & 0x000000FF;
      req.msg.data_len = 5;
      break;
    case IPMI_SESSION_REQUEST_BY_HANDLE:
      rqdata[0]        = 0xFE;
      rqdata[1]        = (uint8_t)id_or_handle;
      req.msg.data_len = 2;
      break;
    case IPMI_SESSION_REQUEST_ALL:
      break;
    }

    rsp = iface->sendrecv( &req);
    if (rsp == NULL)
    {
      lPrintf(LOG_ERR, "Get Session Info command failed");
      retval = -1;
    }
    else if (rsp->ccode > 0)
    {
      lPrintf(LOG_ERR, "Get Session Info command failed: %s",
          iface->helper->val2str(rsp->ccode, completion_code_vals));
      retval = -1;
    }

    if (retval < 0)
    {
      if ((session_request_type == IPMI_SESSION_REQUEST_CURRENT) &&
          (strncmp(iface->name, "lan", 3) != 0))
        lPrintf(LOG_ERR, "It is likely that the channel in use "
          "does not support sessions");
    }
    else
    {
      memcpy(&session_info,  rsp->data, rsp->data_len);
      print_session_info(&session_info, rsp->data_len);
    }
    break;

  case IPMI_SESSION_REQUEST_ALL:
    req.msg.data_len = 1;
    i = 1;
    do
    {
      rqdata[0] = i++;
      rsp = iface->sendrecv( &req);

      if (rsp == NULL)
      {
        lPrintf(LOG_ERR, "Get Session Info command failed");
        retval = -1;
        break;
      }
      else if (rsp->ccode > 0 && rsp->ccode != 0xCC && rsp->ccode != 0xCB)
      {
        lPrintf(LOG_ERR, "Get Session Info command failed: %s",
            iface->helper->val2str(rsp->ccode, completion_code_vals));
        retval = -1;
        break;
      }
      else if (rsp->data_len < 3)
      {
        retval = -1;
        break;
      }

      memcpy(&session_info,  rsp->data, rsp->data_len);
      print_session_info(&session_info, rsp->data_len);

    } while (i <= session_info.session_slot_count);
    break;
  }

  return retval;
}



template <typename T> void ipmi_session<T>::Printf_session_usage(void)
{
  lPrintf(LOG_NOTICE, "Session Commands: info <active | all | id 0xnnnnnnnn | handle 0xnn>");
}


template <typename T> int ipmi_session<T>::ipmi_session_main( int argc, char ** argv)
{
  int retval = 0;

  if (argc == 0 || strncmp(argv[0], "help", 4) == 0)
  {
    Printf_session_usage();
  }
  else if (strncmp(argv[0], "info", 4) == 0)
  {

    if ((argc < 2) || strncmp(argv[1], "help", 4) == 0)
    {
        Printf_session_usage();
    }
    else
    {
      Ipmi_Session_Request_Type session_request_type = IPMI_SESSION_REQUEST_CURRENT;
      uint32_t                  id_or_handle = 0;

      if (strncmp(argv[1], "active", 6) == 0)
        session_request_type = IPMI_SESSION_REQUEST_CURRENT;
      else if (strncmp(argv[1], "all", 3) == 0)
        session_request_type = IPMI_SESSION_REQUEST_ALL;
      else if (strncmp(argv[1], "id", 2) == 0)
      {
        if (argc >= 3)
        {
          session_request_type = IPMI_SESSION_REQUEST_BY_ID;
          if (iface->helper->str2uint(argv[2], &id_or_handle) != 0) {
            lPrintf(LOG_ERR, "HEX number expected, but '%s' given.",
                argv[2]);
            Printf_session_usage();
            retval = -1;
          }
        }
        else
        {
          lPrintf(LOG_ERR, "Missing id argument");
          Printf_session_usage();
          retval = -1;
        }
      }
      else if (strncmp(argv[1], "handle", 6) == 0)
      {
        if (argc >= 3)
        {
          session_request_type = IPMI_SESSION_REQUEST_BY_HANDLE;
          if (iface->helper->str2uint(argv[2], &id_or_handle) != 0) {
            lPrintf(LOG_ERR, "HEX number expected, but '%s' given.",
                argv[2]);
            Printf_session_usage();
            retval = -1;
          }
        }
        else
        {
          lPrintf(LOG_ERR, "Missing handle argument");
          Printf_session_usage();
          retval = -1;
        }
      }
      else
      {
        lPrintf(LOG_ERR, "Invalid SESSION info parameter: %s", argv[1]);
        Printf_session_usage();
        retval = -1;
      }


      if (retval == 0)
        retval = ipmi_get_session_info(
                        session_request_type,
                        id_or_handle);
    }
  }
  else
  {
    lPrintf(LOG_ERR, "Invalid SESSION command: %s", argv[0]);
    Printf_session_usage();
    retval = -1;
  }

  return retval;
}

template class ipmi_session<LANPlus>;
template class ipmi_session<lan>;
