//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
/*
 * lan.h
 *
 *  Created on: Jan 29, 2016
 *      Author: beat
 */
#ifndef ONLINE_IPMI_SRC_NEWLIB_LAN_H_
#define ONLINE_IPMI_SRC_NEWLIB_LAN_H_

#define IPMI_LAN_TIMEOUT 2
#define IPMI_LAN_RETRY  4
#define IPMI_LAN_PORT  0x26f
#define IPMI_LAN_CHANNEL_E 0x0e

#define IPMI_LAN_MAX_REQUEST_SIZE 38 /* 45 - 7 */
#define IPMI_LAN_MAX_RESPONSE_SIZE 34 /* 42 - 8 */

#include <string>
#include "ipmi_intf.h"
#include "ipmi_oem.h"
#include "auth.h"
#include "lanplus_crypt.h"
//using namespace std;
/*
 * LAN interface is required to support 45 byte request transactions and
 * 42 byte response transactions.
 */
//template <typename T> class ipmi_intf<T>;
//class ipmi_auth;
class lan
{
//using ipmi_Intf = ipmi_intf<lan>;
  public:
#include "newnew.h"
    ipmi_rq_entry<lan> * ipmi_req_entries;
    ipmi_rq_entry<lan> * ipmi_req_entries_tail;
    ipmi_oem<lan> *oemO=0;
    ipmi_intf<lan> *intf;
    uint8_t bridge_possible;
    char name[16];//: "lan"
    char desc[128];//("IPMI v1.5 LAN Interface");
    LANPlusCrypt<lan> *Crypt=0;
    lan(ipmi_intf<lan> *Intf)
    {
      this->intf = Intf;
      strcpy(name, "lan");
      strcpy(desc, "IPMI v1.5 LAN Interface");
      ipmi_req_entries = 0;
      ipmi_req_entries_tail = 0;
      bridge_possible = 0;
      auth = new ipmi_auth<lan>();
      sessionsup_fail = false;
      oemO = new ipmi_oem<lan>(intf);
      Crypt = new LANPlusCrypt<lan>(oemO);

    }
    ~lan();
    ipmi_auth<lan> *auth;
    int sessionsup_fail;
    int get_session_setup_code(){return sessionsup_fail;};
    ipmi_rq_entry<lan> *ipmi_req_add_entry(  struct ipmi_rq * req, uint8_t req_seq);
    ipmi_rq_entry<lan> *ipmi_req_lookup_entry(uint8_t seq, uint8_t cmd);
    void ipmi_req_remove_entry(uint8_t seq, uint8_t cmd);
    void ipmi_req_clear_entries(void);
    int get_random(void *data, int len);
    int ipmi_lan_send_packet(  uint8_t * data,
        int data_len);
    struct ipmi_rs * ipmi_lan_recv_packet( struct ipmi_rs *);
    int ipmi_handle_pong(  struct ipmi_rs * rsp);
    int ipmi_lan_ping( );
    void ipmi_lan_thump_first();
    void ipmi_lan_thump( );
    struct ipmi_rs * ipmi_lan_poll_recv( struct ipmi_rs *);
    ipmi_rq_entry<lan> * ipmi_lan_build_cmd( struct ipmi_rq * req, int isRetry);
    struct ipmi_rs * ipmi_send_cmd( struct ipmi_rq * req,struct ipmi_rs *);
    uint8_t * ipmi_lan_build_rsp(  struct ipmi_rs * rsp,
        int * llen);
    int ipmi_lan_send_rsp(  struct ipmi_rs * rsp);
    uint8_t * ipmi_lan_build_sol_msg( struct ipmi_v2_payload * payload, int * llen);
    int is_sol_packet(struct ipmi_rs * rsp);
    int sol_response_acks_packet( ipmi_rs * rsp,
        struct ipmi_v2_payload * payload);
    struct ipmi_rs * ipmi_lan_send_sol_payload( struct ipmi_v2_payload * payload,struct ipmi_rs *);
    int is_sol_partial_ack( ipmi_v2_payload * v2_payload,
        struct ipmi_rs * rsp);
    void set_sol_packet_sequence_number( struct ipmi_v2_payload * v2_payload);
    struct ipmi_rs * ipmi_lan_send_sol( struct ipmi_v2_payload * v2_payload,struct ipmi_rs *);
    int check_sol_packet_for_new_data( struct ipmi_rs *rsp);
    void ack_sol_packet( struct ipmi_rs * rsp);
    struct ipmi_rs * ipmi_lan_recv_sol( struct ipmi_rs *);
    int ipmi_lan_keepalive( );
    int ipmi_get_auth_capabilities_cmd( );
    int ipmi_get_session_challenge_cmd( );
    int ipmi_activate_session_cmd( );
    int ipmi_set_session_privlvl_cmd( );
    int ipmi_close_session_cmd( );
    int ipmi_lan_activate_session( );
    void ipmi_close( );
    int ipmi_open( );
    int ipmi_setup( );
    void ipmi_lan_set_max_rq_data_size(  uint16_t size);
    void ipmi_lan_set_max_rp_data_size(  uint16_t size);
    int setup(){return ipmi_setup();};
//    int open(){return ipmi_lan_open();};
//    void ipmi_close(){return ipmi_lan_close();};
    struct ipmi_rs rs;
    struct ipmi_rs *sendrecv(struct ipmi_rq * req){return ipmi_send_cmd(req,&rs);};
    int sendrsp(struct ipmi_rs *rsp){return ipmi_lan_send_rsp(rsp);};
    struct ipmi_rs *recv_sol(){return  ipmi_lan_recv_sol(&rs);};
    struct ipmi_rs *send_sol(struct ipmi_v2_payload * payload){return ipmi_lan_send_sol(payload,&rs);};
    int keepalive(){return ipmi_lan_keepalive();};
    void set_max_request_data_size(uint16_t size){return  ipmi_lan_set_max_rq_data_size(size);};
    void set_max_response_data_size(uint16_t size){return ipmi_lan_set_max_rp_data_size(size);};
};

#endif /* ONLINE_IPMI_SRC_NEWLIB_LAN_H_ */
