//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifdef IPMI_MEM_DEBUG

void *operator new(size_t len)   {
  void *ptr = ::malloc(len);
  if (ptr == 0)  {
    Printf("New Operator... Cannot allocate memory of size %ld\n", len);
  }
  ::memset(ptr, 0, len);
  return ptr;
}

void operator delete(void* ptr, size_t /* len */)   {
  if ( ptr )  {
    ::free(ptr);
  }
}
#endif
