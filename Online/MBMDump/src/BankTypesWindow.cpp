//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  BankListWindow.cpp
//  ------------------------------------------------------------------------
//
//  Package   : MBMDump: Dump utility for MBM buffer events
//
//  Author    : Markus Frank
//
//  ========================================================================
//
// C++ include files
#include <string>

// Framework include files
#include "MBMDump/MBMDump.h"

using namespace Online;

BankTypesWindow::BankTypesWindow(BaseMenu* par,int cmd_id, const Format& fmt, Banks&& b)
  : BankListWindow(par, cmd_id, fmt, std::move(b), false),
    m_bankWindow2(0)
{
  char txt[256];
  setParent(par);
  m_parentCmd = cmd_id;
  openDetached(0,0,"Display window"," List of bank types ",procName());
  addCommand(C_DISMISS,"Dismiss");
  addComment(C_COM2,"");
  addComment(C_COM3," Hit return on family to see banks list");
  addComment(C_COM4,"");
  addComment(C_COM5,"+------------------------------------------+");
  addComment(C_COM6,"| Type         Type #      Vsn    No.Banks |");
  addComment(C_COM7,"+------------------------------------------+");
  for(Banks::iterator k=m_banks.begin(); k!=m_banks.end();++k)  {
    const auto* bank = (*k).second;
    std::pair<int,int> typ(bank->type(),bank->version());
    BankMap::iterator i = m_map.find(typ);
    if ( i == m_map.end() ) m_map.insert(std::make_pair(typ,std::make_pair(bank,0)));
    m_map[typ].second++;
  }
  int cnt = 0;
  for(BankMap::iterator i=m_map.begin(); i != m_map.end(); ++i, ++cnt)  {
    const auto* bank = (*i).second.first;
    ::snprintf(txt,sizeof(txt),"  %-16s %2d %8d %11d",
      event_print::bankType(bank->type()).c_str(),bank->type(),
      (*i).first.second,(*i).second.second);
    addCommand(C_TYPES+cnt,txt);
  }
  addCommand(C_DISMISS2,"Dismiss");
  closeMenu();
  setCursor(C_DISMISS,1);
}

BankTypesWindow::~BankTypesWindow()  {
  drop(m_bankWindow);
  deleteMenu();
}

void BankTypesWindow::handleMenu(int cmd_id)    {
  if ( cmd_id != C_DISMISS && cmd_id >= C_TYPES ) {
    int typ = cmd_id-C_TYPES;
    int cnt = 0;
    for(BankMap::iterator i=m_map.begin(); i != m_map.end(); ++i, ++cnt)  {
      if ( cnt == typ )  {
	Banks banks;
        for(Banks::iterator j=m_banks.begin(); j!=m_banks.end(); ++j)  {
          if ( (*i).first.first  == (*j).second->type() &&
               (*i).first.second == (*j).second->version() )  {
              banks.push_back(*j);
          }
        }
        replace(m_bankWindow2,new BankListWindow(this, cmd_id, m_fmt, std::move(banks)));
        return;
      }
    }
  }
  return this->BankListWindow::handleMenu(cmd_id);
}

