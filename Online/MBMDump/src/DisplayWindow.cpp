//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//  Author     : Markus Frank
//==========================================================================
//  BankListWindow.cpp
//  ------------------------------------------------------------------------
//
//  Package   : MBMDump: Dump utility for MBM buffer events
//
//  Author    : Markus Frank
//
//  ========================================================================
//
// C++ include files
#include "MBMDump/MBMDump.h"

using namespace Online;

DisplayWindow::DisplayWindow(BaseMenu* par,int /* cmd_id */) : BaseMenu(par) {
  openWindow();
  openMenu(0,0,"Display window",
    "Offset  ------------ Hex Dump ------------- ---- Ascii dump ----  ",procName());
  for(int i=0; i < Constants::N_LINES; i++)
    addComment(i+1,"");
  closeMenu();
}

DisplayWindow::~DisplayWindow()  {
  deleteMenu();
}

void DisplayWindow::handleMenu(int /* cmd_id */ )    {
}
