#===============================================================================
#  LHCb Online software suite
#-------------------------------------------------------------------------------
# Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
# All rights reserved.
#
# For the licensing terms see OnlineSys/LICENSE.
#
# Author     : M.Frank
#
#===============================================================================
#[=======================================================================[.rst:
Online/OnlineAlign
------------------
#]=======================================================================]

online_interface_library(OnlineAlignLib)
target_link_libraries(OnlineAlignLib INTERFACE Gaudi::GaudiKernel)

online_gaudi_module(OnlineAlign
        src/AlignDrv.cpp
        src/AlignWork.cpp
        src/Fitter.cpp
        src/FitterFcn.cpp)

target_link_libraries(OnlineAlign
 PUBLIC Online::GauchoBase
        Online::GauchoLib
        Online::OnlineAlignLib
        Online::OnlineBase
        ROOT::Minuit
        Boost::thread
        Boost::filesystem
)
#
online_install_includes(include)
