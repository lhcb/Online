//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#ifndef ONLINE_ONLIGNALIGN_IALIGNUSER_H_
#define ONLINE_ONLIGNALIGN_IALIGNUSER_H_

#include "GaudiKernel/IAlgTool.h"

/// Online namespace declaration
namespace Online  {

  /// Alignment fitter interface definition
  class IAlignFcn : virtual public IAlgTool   {
  public:
    DeclareInterfaceID(IAlignFcn,1,0);
  public:
    virtual void publishResult(long reference) = 0;
    virtual StatusCode run() = 0;
  };

  /// Alignment fitter iterator definition
  class IAlignIterator : virtual public IAlgTool  {
  public:
    DeclareInterfaceID(IAlignIterator,1,0);
  public:
    virtual StatusCode begin() = 0;
    virtual StatusCode end() = 0;
    virtual StatusCode run() = 0;
  };
}
#endif /* ONLINE_ONLIGNALIGN_IALIGNUSER_H_ */
