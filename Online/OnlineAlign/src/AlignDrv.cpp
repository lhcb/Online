//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================

#include "AlignDrv.h"

namespace LHCb   {
  class AlignDrv : public Online::AlignDrv  {
  public:
    using Online::AlignDrv::AlignDrv;
  };
}
DECLARE_COMPONENT( LHCb::AlignDrv )
DECLARE_COMPONENT( Online::AlignDrv )

using namespace Online;

namespace   {
  long rdRef(FILE *f)   {
    long ref = 0;
    int stat = std::fscanf(f, "%ld", &ref);
    if ( stat == 0 )  {
      ref = 0;
    }
    return ref;
  }
}

AlignDrv::AlignDrv(const std::string& name, ISvcLocator* sl)
  : base_class(name,sl)
{
  this->declareProperty("PartitionName",  m_partitionName = "LHCbA");
  this->declareProperty("FitterFunction", m_fitterName    = "");
  this->declareProperty("ReferenceFile",  m_refFileName   = "");
  this->m_runonce  = false;
  this->m_firstRef = true;
  this->m_refBase  = 0;
}

AlignDrv::~AlignDrv()
{
}

void AlignDrv::waitRunOnce()  {
  while (1)  {
    if (this->m_runonce)    {
      this->m_runonce = false;
      break;
    }
    usleep(500000);
  }
  this->debug() << "Single run-cycle finished." << endmsg;
}

void AlignDrv::setRunOnce()   {
  this->m_runonce = true;
}

StatusCode AlignDrv::pause()  {
  this->debug() << "Pause: Rearm alignment driver for next round." << endmsg;
  this->setRunOnce();
  return StatusCode::SUCCESS;
}

StatusCode AlignDrv::start()   {
  StatusCode sc = this->Service::start();
  this->m_fitter->begin().ignore();
  return sc;
}

StatusCode AlignDrv::stop()   {
  //this->m_fitter->end().ignore();
  return Service::stop();
}

StatusCode AlignDrv::initialize()  {
  StatusCode sc = this->Service::initialize().ignore();
  if ( !sc.isSuccess() )   {
    return sc;
  }
  this->m_monitoringSvc = this->service("MonitorSvc", true);
  if ( !this->m_monitoringSvc.get() )  {
    error() << "Cannot access monitoring service of type MonitorSvc." << endmsg;
    return StatusCode::FAILURE;
  }
  this->m_incidentSvc = this->service("IncidentSvc", true);
  if ( !this->m_incidentSvc.get() )  {
    error() << "Cannot access incident service of type IncidentSvc." << endmsg;
    return StatusCode::FAILURE;
  }
  this->m_toolSvc = this->service("ToolSvc", true);
  if ( !this->m_toolSvc.get() )  {
    error() << "Cannot access tools service of type ToolSvc." << endmsg;
    return StatusCode::FAILURE;
  }
  sc = this->m_toolSvc->retrieveTool(this->m_fitterName, this->m_fitter, this, true);
  if ( !sc.isSuccess() )  {
    error() << "Failed to retriev Fitter Tool: " << this->m_fitterName << endmsg;
    return StatusCode::FAILURE;
  }
  this->m_incidentSvc->addListener(this, "DAQ_PAUSED");
  if ( this->m_refFileName.empty() )  {
    this->m_refFileName = "/group/online/dataflow/options/" +
      this->m_partitionName+"/Alignment_Reference_File.txt";
  }
  return sc;
}

StatusCode AlignDrv::finalize()  {
  this->m_fitter = nullptr; // Private tool: will be released by toolssvc/algorithm
  this->m_toolSvc.reset();
  this->m_incidentSvc.reset();
  this->m_monitoringSvc.reset();
  return this->Service::finalize();
}

void AlignDrv::handle(const Incident& inc)   {
  if (inc.type() == "DAQ_PAUSED")  {
    this->pause().ignore();
  }
}

StatusCode AlignDrv::run()   {
  return StatusCode::SUCCESS;
}

IGauchoMonitorSvc *AlignDrv::getMonSvc()   {
  return this->m_monitoringSvc.get();
}

void AlignDrv::setReferenceBase(long ref)   {
  if (this->m_refBase == 0)  {
    this->m_refBase = new long;
  }
  *this->m_refBase = ref;
}

void AlignDrv::writeReference()  {
  long ref=0;
  FILE *f = std::fopen(this->m_refFileName.c_str(), "r+");
  if ( f == 0 )  {
    if ( m_firstRef )   {
      if ( m_refBase != 0 )   {
        ref = *m_refBase;
      }
      else    {
        ref = 0;
      }
      m_firstRef = false;
    }
    f = std::fopen(m_refFileName.c_str(), "w");
  }
  else    {
    if ( m_firstRef )    {
      ref = (m_refBase != 0) ? *m_refBase : rdRef(f);
      if ( ref > 50 ) ref = 0;
      m_firstRef = false;
    }
    else    {
      ref = rdRef(f);
    }
    std::rewind(f);
  }
  ref++;
  std::fprintf(f, "%ld", ref);
  std::fflush(f);
  std::fclose(f);
  std::fputc('\n', stdout);
  std::fflush(::stdout);
  info() << "==================> Wrote Reference " << ref
	 << " to file " << m_refFileName << endmsg;
}

void AlignDrv::doContinue()   {
  this->m_incidentSvc->fireIncident(Incident(this->name(),"DAQ_CONTINUE"));
}

void AlignDrv::doStop()   {
  this->m_incidentSvc->fireIncident(Incident(this->name(),"DAQ_STOP"));
}
