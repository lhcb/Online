//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================

#include "AlignWork.h"
#include <RTL/rtl.h>
#include <Gaucho/Utilities.h>
#include <Gaucho/IGauchoMonitorSvc.h>
#include <GaudiKernel/IIncidentSvc.h>

namespace LHCb   {
  class AlignWork : public Online::AlignWork  {
  public:
    using Online::AlignWork::AlignWork;
  };
}
DECLARE_COMPONENT( LHCb::AlignWork )
DECLARE_COMPONENT( Online::AlignWork )

using namespace Online;

Online::AlignWork::AlignWork(const std::string& name, ISvcLocator* sl)
  : base_class(name,sl)
{
  declareProperty("PartitionName",  m_partitionName);
  declareProperty("ReferenceFile",  m_refFileName);
  declareProperty("FitterFunction", m_fitFcnName);
}

Online::AlignWork::~AlignWork()   {
}

StatusCode Online::AlignWork::stop()   {
  return StatusCode::SUCCESS;
}

StatusCode Online::AlignWork::continuing()   {
  return StatusCode::SUCCESS;
}

StatusCode Online::AlignWork::start()   {
  return Service::start();
}

StatusCode Online::AlignWork::initialize()   {
  StatusCode sc = this->Service::initialize();
  if ( !sc.isSuccess() )   {
    return sc;
  }
  this->m_toolSvc = this->service("ToolSvc",true);
  if ( !this->m_toolSvc.get() )  {
    error() << "Cannot access tools service of type ToolSvc." << endmsg;
    return StatusCode::FAILURE;
  }
  this->m_monitoringSvc = this->service("MonitorSvc",true);
  if ( ! this->m_monitoringSvc.get() )  {
    error() << "Cannot access monitoring service of type MonitorSvc." << endmsg;
    return StatusCode::FAILURE;
  }
  this->m_incidentSvc = this->service("IncidentSvc", true);
  if ( ! this->m_incidentSvc.get() )  {
    error() << "Cannot access incident service of type IncidentSvc." << endmsg;
    return StatusCode::FAILURE;
  }
  sc = this->m_toolSvc->retrieveTool(this->m_fitFcnName, this->m_fitterFcn, this, true);
  if ( !sc.isSuccess() )  {
    error() << "Cannot access fitter tool " << this->m_fitFcnName << "." << endmsg;
    return sc;
  }
  this->m_incidentSvc->addListener(this, "DAQ_CONTINUE");
  this->m_incidentSvc->addListener(this, "APP_RUNNING");
  this->m_incidentSvc->addListener(this, "DAQ_PAUSED");
  if ( this->m_refFileName.empty() )  {
    this->m_refFileName = "/group/online/dataflow/options/"+this->m_partitionName+"/Alignment_Reference_File.txt";
  }
  return sc;
}

void Online::AlignWork::handle(const Incident& inc)    {
  if (inc.type() == "APP_RUNNING")  {
  }
  else if (inc.type() == "DAQ_CONTINUE")  {
    this->continuing().ignore();
  }
  else if (inc.type() == "DAQ_PAUSED")  {
    this->pause().ignore();
  }
}

StatusCode Online::AlignWork::finalize()   {
  this->m_fitterFcn = 0;
  this->m_toolSvc.reset();
  this->m_monitoringSvc.reset();
  return this->Service::finalize();
}

/// Override from IRunable
StatusCode Online::AlignWork::run()    {
  return StatusCode::SUCCESS;
}

StatusCode Online::AlignWork::pause()   {
  this->info() << "Reading reference file: " << this->m_refFileName << endmsg;
  if ( this->readReference().isSuccess() )   {
    this->m_fitterFcn->run().ignore();
    this->m_fitterFcn->publishResult(this->m_reference);
  }
  this->debug() << "Action finished " << endmsg;
  return StatusCode::SUCCESS;
}

StatusCode Online::AlignWork::readReference()   {
  FILE *f = std::fopen(this->m_refFileName.c_str(),"r");
  if ( f )    {
    std::fscanf(f,"%lu",&this->m_reference);
    std::fclose(f);
    return StatusCode::SUCCESS;
  }
  this->error() << "Failed to open reference file: " << this->m_refFileName
		<< RTL::errorString(errno).c_str()
		<< endmsg;
  return StatusCode::FAILURE;
}
