//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================
#include "FitterFcn.h"
#include <OnlineAlign/IAlignSys.h>
#include <Gaucho/IGauchoMonitorSvc.h>

namespace LHCb   {
  class FitterFcn : public Online::FitterFcn  {
  public:
    using Online::FitterFcn::FitterFcn;
  };
}
DECLARE_COMPONENT( LHCb::FitterFcn )
DECLARE_COMPONENT( Online::FitterFcn )

using namespace Online;

/// Tool constructor
FitterFcn::FitterFcn(const std::string &  type, const std::string &  name, const IInterface *  parent  )
:  base_class(type,name,parent)
{
  declareProperty("ParameterFile", m_paramFileName);
  declareProperty("DataFile",      m_dataFileName);
  declareProperty("PartitionName", m_partitionName);
  this->m_result = 1.0;
}

/// Default destructor
FitterFcn::~FitterFcn()   {
}

StatusCode FitterFcn::initialize()   {
  StatusCode sc= this->AlgTool::initialize();
  if ( !sc.isSuccess() )    {
    return sc;
  }
  unsigned long ndat = 0;
  info() << "Parameter File Name: " << this->m_paramFileName << endmsg;
  info() << "Data File Name:      " << this->m_dataFileName << endmsg;
  this->m_monitorSvc = this->service("MonitorSvc", true);
  if ( !m_monitorSvc.get() )  {
    error() << "Failed to access monitoring service of type MonitorSvc." << endmsg;
    return StatusCode::FAILURE;
  }
  this->m_monitorSvc->declareInfo("Chi2", this->m_result, "Chi Square", this);
  FILE *f = std::fopen(this->m_dataFileName.c_str(),"r");
  if ( f )    {
    char text[256];
    std::fscanf(f, "%lu", &ndat);
    this->m_dat_x.reserve(ndat);
    this->m_dat_y.reserve(ndat);
    this->m_dat_dy.reserve(ndat);
    for ( size_t i=0; i < ndat; i++ )     {
      double x, y, dy;
      std::fscanf(f, "%lf %lf %lf\n", &x, &y, &dy);
      this->m_dat_x.insert(this->m_dat_x.end(), x);
      this->m_dat_y.insert(this->m_dat_y.end(), y);
      this->m_dat_dy.insert(this->m_dat_dy.end(), dy);
      std::snprintf(text, sizeof(text), "%15f %15f %15f",
		    this->m_dat_x[i], this->m_dat_y[i], this->m_dat_y[i]);
      debug() << text << endmsg;
    }
    std::fclose(f);
    std::fflush(stdout);
    return StatusCode::SUCCESS;
  }
  error() << "Failed to open input data file: " << '"' << this->m_dataFileName << '"' << endmsg;
  return StatusCode::FAILURE;
}

StatusCode FitterFcn::finalize()   {
  this->m_monitorSvc.reset();
  return AlgTool::finalize();
}

void FitterFcn::analyze()    {
  double result = 0.0;
  for (size_t i=0; i < this->m_dat_x.size(); i++)   {
    double x = this->m_dat_x[i];
    double y = 0.0;
    for ( size_t j=0; j < this->m_params.size(); j++ )
      y = x*y + this->m_params[j];

    double chi = (y-m_dat_y[i])/m_dat_dy[i];
    result += chi*chi;
  }
  this->m_result = result;
  debug() << "Function Result: " << this->m_result << endmsg;
}

void FitterFcn::readParams()   {
  FILE *f = std::fopen(this->m_paramFileName.c_str(),"r");
  char text[256];

  this->m_params.clear();
  while ( !std::feof(f) )  {
    double p;
    std::fscanf(f,"%lf",&p);
    if ( std::feof(f) ) break;
    this->m_params.insert(m_params.end(),p);
  }
  std::fclose(f);
  info() << "Number of Parameters " << m_params.size() << endmsg;
  for ( size_t i=0; i < this->m_params.size(); i++ )  {
    std::snprintf (text, sizeof(text), "Parameter %4ld %15g", i, this->m_params[i]);
    info() << text << endmsg;
  }
  std::printf("\n");
  std::fflush(stdout);
}

void FitterFcn::publishResult(long reference)   {
  SmartIF<IGauchoMonitorSvc> mon(this->m_monitorSvc);
  mon->updatePerSvc(reference);
}

void FitterFcn::setPartitionName(const std::string& partition)     {
  m_partitionName = partition;
}

StatusCode FitterFcn::run()   {
  this->readParams();
  this->analyze();
  return StatusCode::SUCCESS;
}

