//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================

#ifndef ONLINEBASE_AMS_TRANSFER_H
#define ONLINEBASE_AMS_TRANSFER_H

#define TRANSFER_NS AMS
#include "NET/Transfer.h"
#undef  TRANSFER_NS

#endif  /* ONLINEBASE_AMS_TRANSFER_H */
