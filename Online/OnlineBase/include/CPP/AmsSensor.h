//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
#ifndef CPP_AMSSENSOR_H
#define CPP_AMSSENSOR_H

/// C/C++ include files

/// Framework include files
#include <CPP/Sensor.h>
#include <CPP/Message.h>

/// CPP namespace declaration
namespace CPP  {


  /** @class AmsSensor AmsSensor.h  CPP/AmsSensor.h
   *
   *    The AmsSensor class implements the object that is in charge
   *    of sending/receiving network AMS messages. It also in charge
   *    (for that reason inherits from Sensor) of receiving asynchronous
   *    messages coming from the network addressed to one of the
   *    objects (Interactor). An Interactor is any object that is able
   *    to receive any kind of external stimuli (I/O, messages, time,
   *    etc.). The AmsSensor must dispatch the unrequested incoming
   *    messages to the corresponding interactor.
   *
   *    @author P.Mato
   *    @author M.Frank
   */
  class AmsSensor : public Sensor {
    friend class std::unique_ptr<AmsSensor>;
  protected:
    AmsSensor();
    /// Standard destructor
    virtual ~AmsSensor();
  public:
    static AmsSensor& instance();
    //------As a Sensor------//
    void add(Interactor*, void* ) override;
    virtual void add(Interactor*, const Address& );
    virtual void add(Interactor*, const Address&, bool );
    virtual void add(Interactor*, const std::string& addr, bool = false );
    void remove(Interactor*, void* ) override;
    virtual void remove(Interactor*, const Address* );
    void dispatch(void*) override;
    void rearm() override {}
    //------Application interface-----//
    virtual int subscribe( int, bool = false );
    virtual int send( const Message* msg, const Address& dest );
    virtual int receive( Message**, Address* src, int timeout = 0 );
    virtual int dispatchBroadCast();
  };
}
using CPP::AmsSensor;

#endif // CPP_AMSSENSOR_H
