//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
/*-----------------------------------------------------------------------*/
/*                                                                       */
/*                  ASCII GRAPHICS DISPLAY IMPLEMEMENTATION              */
/*                                                                       */
/*-----------------------------------------------------------------------*/
#ifndef ONLINEKERNEL_CPP_ASCIIDISPLAY_H
#define ONLINEKERNEL_CPP_ASCIIDISPLAY_H

/// Framework include files
#include <SCR/scr.h>
#include <CPP/MonitorDisplay.h>

/// System include files
#include <map>
#include <string>
#include <vector>
#include <cstdlib>
#include <cstdarg>

/// Simple SCR based ASCII display
/**@class AsciiDisplay AsciiDisplay.h CPP/AsciiDisplay.h
 *
 * Simple ASCII display implementation for monitoring applications
 *
 * @author Markus Frank
 */
class AsciiDisplay : public MonitorDisplay  {
 public:
  typedef int (*key_callback)(SCR::Pasteboard* pb, void* par, int key);
 protected:
  /// Helper to store callback structure
  struct handler_t  {
    key_callback call;
    void*        param;
  };

  /// Pointer to SCR pasteboard structure
  SCR::Pasteboard*        m_pasteboard;
  /// Pointer to SCR display structure
  SCR::Display*           m_display;
  /// Pointer to WINDOW structure
  //WINDOW*               m_window;
  /// Color setup
  int                     m_color;
  /// Display width
  int                     m_width;
  /// Display height
  int                     m_height;
  /// Display title
  std::string             m_title;
  /// Map of user defined key handlers attached to individual keys
  std::map<int,handler_t> m_handlers;
  /// Generic key handlers allways called
  std::vector<handler_t>  m_genHandlers;

 public:
  /// Access display directly
  SCR::Display*    display()    const { return m_display; }
  /// Access pasteboard directly
  SCR::Pasteboard* pasteboard() const { return m_pasteboard; }
  /// Update window title/header
  virtual void set_header(int flags, const char* format,...)  override;
  /// Setup display window
  virtual void setup_window() override;
  /// Reset display window
  virtual void reset_window() override;
  /// Start update cycle
  virtual void begin_update() override;
  /// Finish update cycle
  virtual void end_update() override;
  /// Access to the display width in fixed size characters
  virtual size_t width() const override;
  /// Access to the display height in fixed size characters
  virtual size_t height() const override;
  /// Draw empty line to the display
  virtual size_t draw_line() override;
  /// Draw text line with attributes
  virtual size_t draw_line(int flags, const char* format,...)  override;
  /// Draw formatted line in normal rendering
  virtual size_t draw_line_normal(const char* format,...) override;
  /// Draw formatted line in reverse rendering
  virtual size_t draw_line_reverse(const char* format,...) override;
  /// Draw formatted line in bold rendering
  virtual size_t draw_line_bold(const char* format,...) override;
  /// Draw a line bar
  virtual size_t draw_bar() override;
  /// Draw a progress bar
  virtual size_t draw_bar(int x, int y,float ratio,int full_scale) override;

  /// Install default handlers CTRL-E for exit and CTRL-W for re-paint
  void install_keyboard_handler();
  /// Install specific key handler for all keys
  void install_keyboard_handler(void* par, key_callback handler);
  /// Install specific key handler for all keys
  void remove_keyboard_handler(void* par, key_callback handler);
  /// Install specific key handler for one single key.
  void install_keyboard_handler(int key, void* par, key_callback handler);

  /// Handle single keyboard interrupt
  virtual bool handleKeyboard(int key);

  /// Default Constructor
  AsciiDisplay();
  /// Default Constructor with title
  explicit AsciiDisplay(const std::string& title);
  /// Default destructor
  virtual ~AsciiDisplay();
};
#endif // ONLINEKERNEL_CPP_ASCIIDISPLAY_H
