//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
#ifndef ONLINEBASE_RTL_FCMLOGDEVICE_H
#define ONLINEBASE_RTL_FCMLOGDEVICE_H

/// Framework include files
#include <RTL/Logger.h>

/// RTL namespace declaration
namespace CPP   {

  /// Forward declarations
  class Interactor;

  /// Basic output logger device. Prints to stdout
  /** @class Logger::LogDevice
   *
   * @author M.Frank
   */
  class IocLogDevice : public RTL::Logger::LogDevice  {

  public:
    /// Property: Target reference
    Interactor*    target = 0;
    /// Property: Target command id
    int            command = 0;
    /// Property: Output level
    int            outputLevel;

    /// Write output .....
    virtual size_t print_record(bool exc, int severity, const char* source, const char* msg)  const;

  public:
    /// Default constructor attaching explicitly to a fifo path
    IocLogDevice(Interactor* target, int command);

    /// Default destructor
    virtual ~IocLogDevice() = default;

    /// Calls the display action with a given severity level
    /**
     *  @arg severity   [int,read-only]      Display severity flag (see enum)
     *  @arg msg        [string,read-only]   Formatted message string
     *  @return Status code indicating success or failure
     */
    virtual size_t printmsg(int severity, const char* source, const char* msg)  const  override;

    /// Calls the display action with ERROR and throws an std::runtime_error exception
    /**
     *  @return Status code indicating success or failure
     *  @arg msg        [string,read-only]   Formatted message string
     */
    virtual void exceptmsg(const char* source, const char* msg)  const  override;
  };
}       // End namespace RTL
#endif  // ONLINEBASE_RTL_FCMLOGDEVICE_H
