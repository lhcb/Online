//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
//  KeySensor
// -------------------------------------------------------------------------
//
//	Author    : Markus Frank
//
//==========================================================================
#ifndef ONLINEBASE_SCR_KEYSENSOR_H
#define ONLINEBASE_SCR_KEYSENSOR_H 1

// Framework include files
#include "CPP/Sensor.h"

// System includes
#include <memory>

namespace CPP {
  
  /// Communication object describing the Key click events.
  /**
   * After the key interaction with a given SCR display
   * Clients retrieve an instance of this object.
   *
   * @author M.Frank
   */
  struct KeyEvent {
    int           stroke;
    unsigned char button;
    unsigned char modifier;
    unsigned char x;
    unsigned char y;
    unsigned int  msec;
    void*         data;
    /// Standard constructor
    KeyEvent(int key, int x=0, int y=0, unsigned int us=~0x0);
  };

  /// Key sensor. Display clients may subscribe to interrupts of a given display.
  /**
   * It is up to the clients to define the appropriate action on the callback of
   * a given display.
   *
   * @author M.Frank
   */
  class KeySensor : public CPP::Sensor {
    class internals_t;
    std::unique_ptr<internals_t> imp;

  protected:
    /// Sensor overload: Dispatch interrupts and deliver callbacks to clients
    void dispatch( void* arg ) override;
    /// Standard constructor
    KeySensor();
    /// Standard destructor
    virtual ~KeySensor();

  public:
    /// The KeySensor is a singleton: Static instantiator
    static KeySensor& instance();
    /// Enable debug flag
    void setDebug(bool yes_no);
    /// Subscribe Interactor target to display key-events
    virtual void add(CPP::Interactor* actor, long data);
    /// Subscribe Interactor target to display key-events
    virtual void add(CPP::Interactor* actor, void* data=nullptr) override;
    /// Unsubscribe Interactor target from display key-events
    virtual void remove(CPP::Interactor* actor, long data);
    /// Unsubscribe Interactor target from display key-events
    virtual void remove(CPP::Interactor* actor, void* data=nullptr) override;
    /// Rearm keyboard sensor
    virtual void rearm() override;
  };
}
#endif   // ONLINEBASE_SCR_KEYSENSOR_H
