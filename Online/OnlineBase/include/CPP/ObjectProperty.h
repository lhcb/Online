//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_ONLINEBASE_OBJECTPROPERTY_H
#define ONLINE_ONLINEBASE_OBJECTPROPERTY_H

// C/C++ include files
#include <vector>
#include <string>
#include <memory>

/// Namespace for the dimrpc based implementation
namespace rpc {

  ///  Structure to represent a single component property
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.05.2017
   */
  class ObjectProperty  final {
  public:
    /// Option's user flag for processing. Not passed with XML-RPC
    long flag = 0;
    /// Client name
    std::string client;
    /// Property name
    std::string name;
    /// Property value in its string representation
    std::string value;
    /// Default constructor
    ObjectProperty() = default;
    /// Initializing constructor
    ObjectProperty(const std::string& cl, const std::string& nam, const std::string& val);
    /// Copy constructor
    ObjectProperty(const ObjectProperty& copy) = default;
    /// Move constructor
    ObjectProperty(ObjectProperty&& copy) = default;
    /// Default destructor
    ~ObjectProperty() = default;
    /// Copy assignment
    ObjectProperty& operator= (const ObjectProperty& copy) = default;
    /// Move assignment
    ObjectProperty& operator= (ObjectProperty&& copy) = default;
    /// operator less
    bool operator<(const ObjectProperty& copy) const  {
      if ( client < copy.client ) return true;
      if ( name   < copy.name   ) return true;
      if ( value  < copy.value  ) return true;
      return false;
    }
  };

  /// Initializing constructor
  inline ObjectProperty::ObjectProperty(const std::string& cl, const std::string& nam, const std::string& val)
    : client(cl), name(nam), value(val) {}
  
  ///  Trampolin interface to real implementation of this virtual class
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.05.2017
   */
  class UI   {
  public:
    /// Definition of the clients collection type
    typedef std::vector<std::string>     clients_t;
    /// Definition of the property type
    typedef ObjectProperty               property_t;
    /// Definition of the property collection type
    typedef std::vector<ObjectProperty>  properties_t;

  public:
    /// Default constructor
    UI() = default;
    /// Inhibit move constructor
    UI(UI&& copy) = delete;
    /// Inhibit copy constructor
    UI(const UI& copy) = delete;
    /// Inhibit move assignment operator
    UI& operator=(UI&& copy) = delete;
    /// Inhibit assignment operator
    UI& operator=(const UI& copy) = delete;
    /// Default destructor
    virtual ~UI() = default;

    /// Access the hosted client services
    virtual clients_t clients()  const = 0;
    /// Access all properties of an object
    virtual properties_t allProperties()  const = 0;
    /// Access all properties with the same name from all clients
    virtual properties_t namedProperties(const std::string& name)  const = 0;
    /// Access all properties of one remote client (service, ect.)
    virtual properties_t clientProperties(const std::string& client)  const = 0;
    /// Access a single property of an object
    virtual property_t property(const std::string& client,
				const std::string& name)  const = 0;
    /// Modify one single property
    virtual void setProperty(const std::string& client,
			     const std::string& name,
			     const std::string& value) = 0;
    /// Modify one single property
    virtual void setPropertyObject(const property_t& property) = 0;
    /// Modify a whole bunch of properties. Call returns the number of changes
    virtual int  setProperties(const properties_t& props) = 0;
    /// Modify all properties in allclients matching the name
    virtual int  setPropertiesByName(const std::string& name,
				     const std::string& value) = 0;
    /// Invoke the interpreter and interprete the command line
    virtual int interpreteCommand(const std::string& cmd)  const = 0;
    /// Invoke the interpreter and interprete the command lines
    virtual int interpreteCommands(const std::vector<std::string>& cmds)  const = 0;
  };

  ///  Concrete interface implementation to be implemente3d by the client
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.05.2017
   */
  template <typename TYPE> class PropertyUI : public UI  {
    TYPE type;
  public:
    /// Default constructor
    PropertyUI() = default;
    /// Initializing constructor
  PropertyUI(TYPE&& arg) : type(std::move(arg)) {}
    /// Inhibit move constructor
    PropertyUI(PropertyUI&& copy) = delete;
    /// Inhibit copy constructor
    PropertyUI(const PropertyUI& copy) = delete;
    /// Inhibit move assignment operator
    PropertyUI& operator=(PropertyUI&& copy) = delete;
    /// Inhibit assignment operator
    PropertyUI& operator=(const PropertyUI& copy) = delete;
    /// Default destructor
    virtual ~PropertyUI() = default;

    /// Access the hosted client services
    virtual clients_t clients()  const override;
    /// Access all properties of an object
    virtual properties_t allProperties()  const override;
    /// Access all properties with the same name from all clients
    virtual properties_t namedProperties(const std::string& name)  const override;
    /// Access all properties of one remote client (service, ect.)
    virtual properties_t clientProperties(const std::string& client)  const override;
    /// Access a single property of an object
    virtual property_t property(const std::string& client,
				const std::string& name)  const override;
    /// Modify one single property
    virtual void setProperty(const std::string& client,
			     const std::string& name,
			     const std::string& value) override;
    /// Modify one single property
    virtual void setPropertyObject(const property_t& property) override;
    /// Modify a whole bunch of properties. Call returns the number of changes
    virtual int  setProperties(const properties_t& props) override;
    /// Modify all properties in allclients matching the name
    virtual int  setPropertiesByName(const std::string& name,
				     const std::string& value) override;
    /// Invoke the interpreter and interprete the command line
    virtual int interpreteCommand(const std::string& cmd)  const override;
    /// Invoke the interpreter and interprete the command lines
    virtual int interpreteCommands(const std::vector<std::string>& cmds)  const override;
  };

  ///  Client RPC server implementation
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.05.2017
   */
  class RpcUI     {
  public:
    /// Default constructor
    RpcUI() = default;
    /// Inhibit move constructor
    RpcUI(RpcUI&& copy) = delete;
    /// Inhibit copy constructor
    RpcUI(const RpcUI& copy) = delete;
    /// Inhibit move assignment operator
    RpcUI& operator=(RpcUI&& copy) = delete;
    /// Inhibit assignment operator
    RpcUI& operator=(const RpcUI& copy) = delete;
    /// Default destructor
    virtual ~RpcUI() = default;
    /// Start the server instance
    virtual void start_server() = 0;
    /// Stop the server instance
    virtual void stop_server() = 0;
  };
  std::unique_ptr<RpcUI> create_rpc_ui(const std::string& type, UI* ui, const std::vector<std::string>& args);
}       // End namespace xmlrpc
#endif  // ONLINE_ONLINEBASE_OBJECTPROPERTY_H
