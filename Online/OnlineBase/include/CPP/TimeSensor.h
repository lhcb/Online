/*
**++
**  FACILITY:  DEXPERT  (TimeSensor.h)
**
**  MODULE DESCRIPTION:
**
**      TimeSensor class definition.
**
**  AUTHORS:
**
**      P. Mato
**
**  CREATION DATE:  16-Nov-1990
**
**  DESIGN ISSUES:
**
**      The TimeSensor behaves as similar way that the AmsSensor or
**      UpiSensor all of them are derivated from a unique Sensor class.
**
**
**  MODIFICATION HISTORY:
**
**      16-Nov-1990 Derivated from the existing Timer.h
**--
*/

#ifndef CPP_TIMESENSOR_H
#define CPP_TIMESENSOR_H

/// Framework include files
#include <CPP/Sensor.h>

/// C/C++ include files
#include <memory>

/// CPP namespace declaration
namespace CPP  {


  class TimeSensor : public Sensor {
  public:
    /// Forward declaration
    class Period;

  private:
    /// Add interactor
    void add( Interactor* client, std::unique_ptr<Period>&& period, void* data );
    
    /// Standard constructor
    TimeSensor();
  public:

    /// Standard destructor
    virtual ~TimeSensor();
    /// Add interactor
    virtual void add( Interactor* client, void* param ) override final;
    /// Add interactor
    virtual void  add(Interactor* client, const char* period, void* param);
    /// Add interactor
    virtual void  add(Interactor* client, int seconds, void* param = 0 );
    /// Add interactor
    virtual void  add(Interactor* client, int seconds, long param );
    /// Add interactor
    virtual void  add(Interactor* client, double seconds, void* param = 0 );
    /// Add interactor
    virtual void  add(Interactor* client, double seconds, long param );
    /// Remove interactor
    virtual void  remove(Interactor* client, void* data = 0) override final;
    /// Remove interactor
    virtual void  remove(Interactor* client, long data);
    /// Timer event dispatching routine
    virtual void  dispatch(void*) override;
    /// Timer rearm callback
    virtual void  rearm() override;
    /// Singleton instantiation
    static TimeSensor& instance();
  };
}
using CPP::TimeSensor;

#define TIMESENSOR (::CPP::TimeSensor::instance())

#endif // CPP_TIMESENSOR_H
