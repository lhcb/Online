//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Project    : Online
//
//  Author     : M.Frank
//
//==========================================================================
#ifndef ONLINEBASE_CPP_MEM_BUF_H
#define ONLINEBASE_CPP_MEM_BUF_H

// C/C++ include files
#include <memory>
#include <cstring>


/// Online namespace declaration
namespace Online  {

  /// Memory buffer class
  class mem_buff  {
  private:
    std::unique_ptr<unsigned char[]> data  {   };
    unsigned char* pointer                 { nullptr };
    std::size_t size                       { 0 };
  public:
    /// Default constructor
    mem_buff() = default;
    /// Move constructor
    mem_buff(mem_buff&& copy);
    /// Initializing constructor with memory allocation
    mem_buff(std::size_t len);
    /// Initializing constructor with memory allocation and cursor at offset
    mem_buff(std::size_t len, std::size_t offset);
    /// Initializing constructor with memory copy
    mem_buff(const void* pointer, std::size_t len);
    /// Default destructor
    ~mem_buff() = default;

    /// Move assignment
    mem_buff& operator=(mem_buff&& copy);

    /// Access the start-pointer of the buffer memory.
    unsigned char*       begin()        {   return this->data.get();               }
    /// Access the start-pointer of the buffer memory.
    const unsigned char* begin() const  {   return this->data.get();               }
    /// Access the end-pointer of the buffer memory.
    unsigned char*       end()          {   return this->data.get() + this->size;  }
    /// Access the end-pointer of the buffer memory.
    const unsigned char* end() const    {   return this->data.get() + this->size;  }
    /// Access the current cursor of the buffer object
    unsigned char* ptr()                {   return this->pointer;                  }
    /// Access the current cursor of the buffer object
    const unsigned char* ptr() const    {   return this->pointer;                  }
    /// Access the total buffer length
    std::size_t length()  const         {   return this->size;                     }
    /// Access the number of used bytes in the buffer
    std::size_t used()    const         {   return this->pointer - this->begin();  }
    /// Number of bytes left in the buffer before the next allocation is necessary
    std::size_t space_left()  const     {   return this->end() - this->pointer;    }
    
    /// Swap two buffers
    void swap(mem_buff& other);

    /// Copy data to the buffer at a specified offset. If insufficient memory reallocation takes place
    unsigned char* copy(std::size_t offset, const void* source, std::size_t len);
    /// Copy data to the buffer at a specified offset. If insufficient memory reallocation takes place
    unsigned char* copy(std::size_t offset, const mem_buff& copy);

    /// Copy data to the buffer at the start. If insufficient memory reallocation takes place
    unsigned char* copy(const void* pointer, std::size_t len);
    /// Copy data to the buffer at the start. If insufficient memory reallocation takes place
    unsigned char* copy(const mem_buff& copy);

    /// Append data to the buffer at the cursor. If insufficient memory reallocation takes place
    unsigned char* append(const void* pointer, std::size_t len);
    /// Append data to the buffer at the cursor. If insufficient memory reallocation takes place
    unsigned char* append(const mem_buff& buffer);

    /// Allocate a new chunk of memory of the specified size.
    unsigned char* allocate(std::size_t len, bool reset=false);
    /// Reallocate a new chunk of memory of the specified size. Old data are preserved.
    unsigned char* reallocate(std::size_t len);

    /// Release the buffer memory and give ownership to caller
    std::pair<std::size_t, unsigned char*> release();
    /// Reset the buffer memory (aka memset)
    void reset(unsigned char val = 0);
    /// Set the buffer cursor to a specified location
    void set_cursor(std::size_t offset);

    /// Allocate an object of the requested size. Internal pointer advances, returned is original pointer
    template <typename T = unsigned char> T* get(std::size_t length);
    /// Access the beginning of the buffer as object
    template <typename T = unsigned char> const T* as()  const;
    /// Access the beginning of the buffer as object
    template <typename T = unsigned char> T* begin();
    /// Access the beginning of the buffer as object
    template <typename T = unsigned char> const T* begin()  const;
    /// Access the buffer at a given location as object
    template <typename T = unsigned char> T* at(std::size_t offset=0);
    /// Access the buffer at a given location as object
    template <typename T = unsigned char> const T* at(std::size_t offset=0)  const;
    //template <typename T = unsigned char> T* advance(std::size_t offset);
  };


  /// Initializing constructor with memory allocation
  inline mem_buff::mem_buff(std::size_t len) : size(len)  {
    this->data.reset(pointer = new unsigned char[this->size]);
  }

  /// Initializing constructor with memory allocation and cursor at offset
  inline mem_buff::mem_buff(std::size_t len, std::size_t offset) : size(len)  {
    this->data.reset(this->pointer = new unsigned char[this->size]);
    this->pointer += offset;
  }

  /// Initializing constructor with memory copy
  inline mem_buff::mem_buff(const void* source, std::size_t len) : size(len)  {
    this->data.reset(this->pointer = new unsigned char[this->size]);
    ::memcpy(this->pointer, source, this->size);
  }

  /// Move constructor
  inline mem_buff::mem_buff(mem_buff&& copy)
    : data(std::move(copy.data)), pointer(copy.pointer), size(copy.size)
  {
    copy.data.reset();
    copy.pointer = nullptr;
    copy.size = 0;
  }
  
  /// Move assignment
  inline mem_buff& mem_buff::operator=(mem_buff&& copy)   {
    data = std::move(copy.data);
    copy.data.reset();

    pointer = copy.pointer;
    copy.pointer = nullptr;

    size = copy.size;
    copy.size = 0;
    return *this;
  }
  
  /// Swap two buffers
  inline void mem_buff::swap(mem_buff& other)    {
    auto tmp_data     = std::move(this->data);
    auto tmp_pointer  = this->pointer;
    auto tmp_size     = this->size;
    this->data    = std::move(other.data);
    this->pointer = other.pointer;
    this->size    = other.size;
    other.data    = std::move(tmp_data);
    other.pointer = tmp_pointer;
    other.size    = tmp_size;
  }

  /// Allocate a new chunk of memory of the specified size.
  inline unsigned char* mem_buff::allocate(std::size_t len, bool reset)    {
    if ( len > this->size )   {
      this->data.reset(this->pointer = new unsigned char[this->size = len]);
    }
    if ( reset )   {
      ::memset(this->data.get(), 0, this->size);
    }
    return this->pointer = this->data.get();
  }

  /// Reallocate a new chunk of memory of the specified size. Old data are preserved.
  inline unsigned char* mem_buff::reallocate(std::size_t len)    {
    if ( len > this->size )   {
      std::size_t old_len = this->size;
      std::size_t offset  = this->pointer - this->data.get();
      std::unique_ptr<unsigned char[]> old(this->data.release());
      this->data.reset(this->pointer = new unsigned char[this->size = (old_len+len)]);
      if ( offset > 0 )   {
	::memcpy(this->pointer, old.get(), offset);
	this->pointer += offset;
      }
    }
    // Advance pointer to old position or not?
    return this->pointer;
  }

  /// Reset the buffer memory (aka memset)
  inline void mem_buff::reset(unsigned char val)   {
    if ( this->data.get() )   {
      ::memset(this->data.get(), val, this->size);
      this->pointer = this->data.get();
    }
  }

  /// Release the buffer memory and give ownership to caller
  inline std::pair<std::size_t, unsigned char*> mem_buff::release()   {
    if ( this->data.get() )   {
      std::pair<std::size_t, unsigned char*> ret(this->used(), this->data.release());
      this->pointer = nullptr;
      this->size = 0;
      return ret;
    }
    return std::make_pair(0UL, nullptr);
  }

  /// Copy data to the buffer at a specified offset. If insufficient memory reallocation takes place
  inline unsigned char* mem_buff::copy(std::size_t offset, const void* source, std::size_t len)   {
    if ( !this->data.get() || (offset+len) > this->size )   {
      if ( this->used() > 0 )   {
	std::size_t old_len = this->size;
	std::size_t off_set  = this->pointer - this->data.get();
	std::unique_ptr<unsigned char[]> old(this->data.release());
	this->data.reset(this->pointer = new unsigned char[this->size = (old_len+len)]);
	::memcpy(this->pointer, old.get(), off_set);
	this->pointer += off_set;
      }
      else  {
	this->allocate(offset+len);
      }
    }
    this->pointer = this->begin()+offset;
    ::memcpy(this->pointer, source, len);
    this->pointer += len;
    return this->pointer;
  }

  /// Copy data to the buffer at a specified offset. If insufficient memory reallocation takes place
  inline unsigned char* mem_buff::copy(std::size_t offset, const mem_buff& copy)   {
    return this->copy(offset, copy.data.get(), copy.used());
  }

  /// Copy data to the buffer at the start. If insufficient memory reallocation takes place
  inline unsigned char* mem_buff::copy(const void* source, std::size_t len)   {
    return this->copy(0, source, len);
  }

  /// Copy data to the buffer at the start. If insufficient memory reallocation takes place
  inline unsigned char* mem_buff::copy(const mem_buff& copy)   {
    return this->copy(0, copy.data.get(), copy.used());
  }

  /// Append data to the buffer at the cursor. If insufficient memory reallocation takes place
  inline unsigned char* mem_buff::append(const void* source, std::size_t len)   {
    if ( !this->data.get() )
      this->allocate(len);
    else if ( this->pointer + len > this->data.get() + this->size )
      this->reallocate(1.5*(this->size + len));
    if ( source )
      ::memcpy(this->pointer, source, len);
    this->pointer += len;
    return this->pointer;
  }

  /// Append data to the buffer at the cursor. If insufficient memory reallocation takes place
  inline unsigned char* mem_buff::append(const mem_buff& buff)   {
    return this->append(buff.data.get(), buff.used());
  }

  /// Set the buffer cursor to a specified location
  inline void mem_buff::set_cursor(std::size_t offset)   {
    this->pointer = this->data.get() + offset;
  }

#if 0
  template <typename T> inline T* mem_buff::advance(std::size_t offset)   {
    auto* p = (T*)this->pointer;
    this->pointer += offset;
    return p;
  }
#endif
  /// Allocate an object of the requested size. Internal pointer advances, returned is original pointer
  template <typename T> inline T* mem_buff::get(std::size_t len)   {
    unsigned char* p = this->append(nullptr, len);
    p -= len;
    return (T*)p;
  }

  /// Access the beginning of the buffer as object
  template <typename T> inline const T* mem_buff::as()  const {
    return (const T*)this->data.get();
  }

  /// Access the beginning of the buffer as object
  template <typename T> inline T* mem_buff::begin()   {
    return (T*)this->data.get();
  }

  /// Access the beginning of the buffer as object
  template <typename T> inline const T* mem_buff::begin()  const {
    return (const T*)this->data.get();
  }

  /// Access the buffer at a given location as object
  template <typename T> inline T* mem_buff::at(std::size_t offset)   {
    return (T*)(this->data.get() + offset);
  }

  /// Access the buffer at a given location as object
  template <typename T> inline const T* mem_buff::at(std::size_t offset)  const {
    return (const T*)(this->data.get() + offset);
  }
}

#endif // ONLINEBASE_CPP_MEM_BUF_H
