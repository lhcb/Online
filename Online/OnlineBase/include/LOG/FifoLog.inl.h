//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  Fifo Logger implementation
//--------------------------------------------------------------------------
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================


#include "LOG/FifoLog.h"
#include <cstdarg>
#include <atomic>
#include <thread>
#include <mutex>

#define LOGGER_VA_LIST va_list

class fifolog::Logger::Implementation  {
public:
  enum { STDOUT = 0, STDERR = 1 };
  typedef std::pair<void*, void (*)(const char* msg, size_t len)> hook_t;
  typedef std::pair<size_t,char*>  buffer_t;
  static const size_t OUTPUT_MAX_LEN = 8192;
  static std::string utgid;
  static std::string systag;
  static std::string fifo_name;
  static std::string logfile_name;
  std::string        real_host       {};
  std::string        physical_host   {};
  buffer_t           output_trailer  {0UL, nullptr};
  buffer_t           output[2]       {{0UL, nullptr},{0UL, nullptr}};
  hook_t             output_hook[2]  {{nullptr, nullptr},{nullptr, nullptr}};
  std::mutex         output_mutex    {};
  std::atomic<unsigned long> msg_count;
  int                fifo_fd = -1;
  FILE*              logfile_stream  { nullptr };

  /// Escape JSON special characters if necessary
  std::size_t escape_json(char* output, std::size_t max_out, const char* input, std::size_t in_len)  const;

  /// Dump data buffer to file descriptor
  void dump_buffer(int fd, const char* buf, int len);

  /// Build output record and dump record to output device
  void flush_line(int stream_id);

  /// Append (unlocked) single character to output buffer
  void append(int stream_id, char c)   {
    switch(c)  {
    case '\0':
    case '\r':
      break;
    case '\n':
      flush_line(stream_id);
      break;
    default:  {
      auto& o = output[stream_id];
      if ( o.first >= OUTPUT_MAX_LEN )  {
	flush_line(stream_id);
      }
      o.second[o.first] = c;
      ++o.first;
      break;
    }
    }
  }
  
  /// Append (locked) single character to output buffer
  int push(int stream_id, char c)  {
    std::lock_guard<std::mutex> lock(output_mutex);
    append(stream_id,c);
    return c;
  }

  /// Append (locked) opaque buffer to output buffer
  int push(int stream_id, const void* text, size_t len)  {
    const char* p = (const char*)text;
    std::lock_guard<std::mutex> lock(output_mutex);
    for(const char* end=p+len; p<end; ++p)
      this->append(stream_id, *p);
    return len;
  }

  /// Append (locked) string to output buffer
  int push(int stream_id, const char* text)  {
    const char* p = text;
    std::lock_guard<std::mutex> lock(output_mutex);
    for(; *p; ++p)
      this->append(stream_id, *p);
    return p-text;
  }
  /// Append (locked) formatted string to output buffer
  int push(int stream_id, const char* format, LOGGER_VA_LIST& args);
#if 0
  /// Append (locked) formatted string to output buffer
  int push(int stream_id, const char* format, LOGGER_VA_LIST& args)  {
    char text[4096];
    int len = ::vsnprintf(text, sizeof(text), format, args);
    text[sizeof(text)-1] = 0;
    va_end(args);
    this->push(stream_id, text);
    return len;
  }
#endif
  /// Append (locked) formatted string to output buffer
  int push(int stream_id, const wchar_t* format, LOGGER_VA_LIST& args)  {
    wchar_t text[4096];
    int len = ::vswprintf(text, sizeof(text), format, args);
    text[sizeof(text)/sizeof(wchar_t)-1] = 0;
    va_end(args);
    this->push(stream_id, text, len*sizeof(wchar_t));
    return len;
  }

public:

  /// Default constructor
  Implementation();

  /// Default destructor
  virtual ~Implementation();

  /// Object initialization
  virtual void init(const std::string& fifo, const std::string& utgid, const std::string& tag);

  /// Rebuild trailer 
  virtual void rebuild_trailer();

  /// Stop processing and shutdown
  virtual void stop();

  /// Run the instance
  virtual void run();
};

namespace fifolog   {

  /// Class to capture all libc output routines to overload stdout/stderr
  /**
   *  \author  M.Frank
   *  \version 1.0
   */
  class FuncPtr   {
  public:
    typedef ssize_t (*write_t)    (int fd, const void *buf, size_t count);
    typedef size_t  (*putchar_t)  (int c);
    typedef int     (*putc_t)     (int c, FILE* stream);
    typedef int     (*puts_t)     (const char *format);

    typedef size_t  (*fputc_t)    (int c, FILE *stream);
    typedef size_t  (*fputs_t)    (const char *ptr, FILE *stream);
    typedef size_t  (*fwrite_t)   (const void *ptr, size_t size, size_t nmemb, FILE *stream);

    typedef int     (*printf_t)   (const char *format, ...);
    typedef int     (*vprintf_t)  (const char *format, LOGGER_VA_LIST& args);

    typedef int     (*wprintf_t)  (const wchar_t *format, ...);
    typedef int     (*vwprintf_t) (const wchar_t *format, LOGGER_VA_LIST& args);

    typedef int     (*dprintf_t)  (int fd, const char *format, ...);
    typedef int     (*vdprintf_t) (int fd, const char *format, LOGGER_VA_LIST& args);

    typedef int     (*fprintf_t)  (FILE* stream, const char *format, ...);
    typedef int     (*vfprintf_t) (FILE* stream, const char *format, LOGGER_VA_LIST& args);
    typedef int     (*vfwprintf_t)(FILE* stream, const wchar_t *format, LOGGER_VA_LIST& args);

    typedef int   (*printf_chk_t) (int flag, const char *format, ...);
    typedef int   (*vprintf_chk_t)(int flag, const char *format, LOGGER_VA_LIST& args);

    typedef int  (*fprintf_chk_t) (FILE *stream, int flag, const char *format, ...);
    typedef int  (*vfprintf_chk_t)(FILE *stream, int flag, const char *format, LOGGER_VA_LIST& args);

 
  public:
    write_t     real_write    { nullptr };
    putchar_t   real_putchar  { nullptr };
    putc_t      real_putc     { nullptr };
    puts_t      real_puts     { nullptr };

    fputc_t     real_fputc    { nullptr };
    fputs_t     real_fputs    { nullptr };
    fwrite_t    real_fwrite   { nullptr };

    printf_t    real_printf   { nullptr };
    vprintf_t   real_vprintf  { nullptr };

    wprintf_t   real_wprintf  { nullptr };
    vwprintf_t  real_vwprintf { nullptr };
  
    dprintf_t   real_dprintf  { nullptr };
    vdprintf_t  real_vdprintf { nullptr };

    fprintf_t   real_fprintf  { nullptr };
    vfprintf_t  real_vfprintf { nullptr };
    vfwprintf_t real_vfwprintf{ nullptr };
  
    printf_chk_t   real_printf_chk   { nullptr };
    vprintf_chk_t  real_vprintf_chk  { nullptr };
    fprintf_chk_t  real_fprintf_chk  { nullptr };
    vfprintf_chk_t real_vfprintf_chk { nullptr };
  
  public:
    /// Default constructor
    FuncPtr(int);
    /// Default destructor
    ~FuncPtr() = default;
  };

  /// Logger class uniquely overloading output routines from libc
  /**
   *  \author  M.Frank
   *  \version 1.0
   */
  class LibCLogger : public fifolog::Logger::Implementation  {
  public:
    /// Object initialization
    virtual void init(const std::string& fifo, const std::string& utgid, const std::string& tag)  override;

    /// Default constructor
    LibCLogger();

    /// Default destructor
    virtual ~LibCLogger();
  };

  /// Logger class overloading output routines from libc and polling on stdout/stderr
  /**
   *  \author  M.Frank
   *  \version 1.0
   */
  class PollLogger : public fifolog::Logger::Implementation  {
  protected:
    typedef std::unique_ptr<std::thread> thread_t;
    int         epoll_fd     = -1;
    int         std_fd[2]    = {-1, -1};
    thread_t    runner;
    bool        done = false;

  public:
    /// Object initialization
    virtual void init(const std::string& fifo, const std::string& utgid, const std::string& tag)  override;
    /// Poll data from stdout/stderr pipes
    void poll();

    /// Default constructor
    PollLogger();

    /// Default destructor
    virtual ~PollLogger();
    /// Run the logger in a separate thread
    void run()  override;
    /// Stop processing and shutdown
    void stop()  override;
  };
}

#include <system_error>
#include <stdexcept>
#include <iostream>
#include <sstream>
#include <cstring>
#include <cstdarg>
#include <cstdio>
#include <iomanip>

#include <dlfcn.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/epoll.h>

typedef fifolog::Logger::Implementation logger_imp;

std::string logger_imp::fifo_name;
std::string logger_imp::logfile_name;
std::string logger_imp::systag = "SYSTEM";
std::string logger_imp::utgid;

/// Anonymous namespace for helpers
namespace fifolog  {

  /// Translate errno into error string
  inline const std::string cerror(int err)   {
    return std::make_error_code(std::errc(err)).message();
  }

  template <typename FUNC_TYPE> inline FUNC_TYPE get_sym(const char* name)   {
    FUNC_TYPE fun = FUNC_TYPE( ::dlsym(RTLD_NEXT, name) );
    if ( nullptr == fun )   {
      char text[256];
      ::snprintf(text,sizeof(text),"Failed to initialize logger function %s",name);
      throw std::runtime_error(text);
    }
    return fun;
  }

  inline logger_imp& log()  {
    static fifolog::LibCLogger s_log;
    //static fifolog::PollLogger s_log;
    return s_log;
  }

  inline fifolog::FuncPtr& funcs()  {
    static fifolog::FuncPtr s_funcs(0);
    return s_funcs;
  }

  inline std::unique_ptr<fifolog::Logger>& logger(bool create=true)   {
    static std::unique_ptr<fifolog::Logger> s_log;
    if ( s_log.get() )   {
      return s_log;
    }
    else if ( create && !s_log.get() )    {
      std::string tag   = logger_imp::systag;
      std::string utgid = logger_imp::utgid;
      std::string fifo  = logger_imp::fifo_name;
      if ( utgid.empty() )    {
	utgid = std::getenv("UTGID") ? std::getenv("UTGID") : "";
      }
      if ( utgid.empty() )    {
	utgid = std::getenv("PROCESS_NAME") ? std::getenv("PROCESS_NAME") : "";
      }
      if ( utgid.empty() )    {
	char text[256];
	::snprintf(text,sizeof(text),"P%06d",::getpid());
        utgid = text;
      }
      if ( fifo.empty() )   {
	fifo = std::getenv("LOGFIFO") ? std::getenv("LOGFIFO") : "";
      }
      if ( tag.empty() || tag == "SYSTEM" )   {
	tag = std::getenv("PARTITION_NAME") ? std::getenv("PARTITION_NAME") : tag;
      }
      if ( tag.empty() || tag == "SYSTEM" )   {
	tag = std::getenv("SYSTEM_TAG") ? std::getenv("SYSTEM_TAG") : tag;
      }
      if ( tag.empty() )   {
	tag = "SYSTEM";
      }
      if ( std::getenv("FIFOLOG_DBG") )   {
	bool run = false;
	while(!run) ::sleep(1);
      }
      s_log.reset(new fifolog::Logger(fifo, utgid, tag));
    }
    return s_log;
  }
}

/// Default constructor
fifolog::FuncPtr::FuncPtr(int)   {
#define SYM(x)   if ( FuncPtr::real_##x == nullptr )	\
    FuncPtr::real_##x = get_sym< FuncPtr::x##_t > ( #x )
  SYM(write);
  SYM(putchar);
  SYM(putc);
  SYM(puts);

  SYM(fputc);
  SYM(fputs);
  SYM(fwrite);

  SYM(printf);
  SYM(vprintf);

  SYM(wprintf);
  SYM(vwprintf);

  SYM(dprintf);
  SYM(vdprintf);

  SYM(fprintf);
  SYM(vfprintf);
  SYM(vfwprintf);
#undef SYM
#define SYM(x)   if ( FuncPtr::real_##x == nullptr )	\
    FuncPtr::real_##x = get_sym< FuncPtr::x##_t > ( "__" #x )
  SYM(printf_chk);
  SYM(vprintf_chk);
  SYM(fprintf_chk);
  SYM(vfprintf_chk);
#undef SYM
}

/// Default constructor
logger_imp::Implementation()   {
  output_trailer.first = 0;
  output_trailer.second = new char[OUTPUT_MAX_LEN+256];
  output[0].first = 0;
  output[0].second = new char[OUTPUT_MAX_LEN+256];
  output[1].first = 0;
  output[1].second = new char[OUTPUT_MAX_LEN+256];
  funcs();
}

/// Default destructor
logger_imp::~Implementation()   {
  if ( output_trailer.second ) delete [] output_trailer.second;
  output_trailer.second = nullptr;
  if ( output[0].second ) delete [] output[0].second;
  if ( output[1].second ) delete [] output[1].second;
  output[0].second = nullptr;
  output[1].second = nullptr;
}

/// Object initialization
void logger_imp::init(const std::string& f, const std::string& u, const std::string& t)   {
  char   text[1024];
  struct stat statBuf;

  if ( nullptr == this->logfile_stream )   {
    const char* log_file = std::getenv("LOGFILE_APPEND");
    if ( log_file && ::strlen(log_file) > 0 )   {
      Logger::Implementation::logfile_name = log_file;
      this->logfile_stream = ::fopen(log_file, "a");
    }
    else if ( (log_file=std::getenv("LOGFILE_CREATE"))  && ::strlen(log_file) > 0 )   {
      Logger::Implementation::logfile_name = log_file;
      this->logfile_stream = ::fopen(log_file, "w");
    }
  }
  if ( this->fifo_fd < 0 )   {
    std::string& fifo  = Logger::Implementation::fifo_name;
    if ( !f.empty() ) fifo = f;

    if ( !fifo.empty() )   {
      std::string& my_utgid = Logger::Implementation::utgid;
      std::string& my_tag   = Logger::Implementation::systag;

      if ( !u.empty() ) my_utgid = u;
      if ( !t.empty() ) my_tag = t;

      this->msg_count = 0;

      // check if fifo is writable 
      if( ::access(fifo.c_str(),W_OK) == -1) {       // write access to fifo OK
	::snprintf(text,sizeof(text),"+++ Failed to access output: %s [%s]\n",
		   fifo.c_str(), cerror(errno).c_str());
	throw std::runtime_error(text);
      }
      // get fifo information
      if( ::stat(fifo.c_str(),&statBuf) == -1) {     // fifo information got
	::snprintf(text,sizeof(text),"+++ Output: Cannot stat device %s! [%s]\n",
		   fifo.c_str(), cerror(errno).c_str());
	throw std::runtime_error(text);
      }
      // check if fifo is a FIFO
      if( !S_ISFIFO(statBuf.st_mode)) {            // fifo is a FIFO
	::snprintf(text,sizeof(text),"+++ Output: %s is no fifo!\n", fifo.c_str());
	throw std::runtime_error(text);
      }
      // open fifo
      this->fifo_fd = ::open(fifo.c_str(), O_RDWR|O_NONBLOCK|O_APPEND);
      if( this->fifo_fd == -1 )   {        /* fifo open() failed      */
	::snprintf(text,sizeof(text),"+++ Failed to open output: %s! [%s]\n",
		   fifo.c_str(), cerror(errno).c_str());
	throw std::runtime_error(text);
      }
      // Now we are sure that another process has the FIFO open for reading.
      // We unset now the O_NONBLOCK bit to have blocking write (no-drop behaviour).
      int status = ::fcntl(this->fifo_fd, F_GETFL);
      if ( status < 0 )    {
	::close(this->fifo_fd);
	::snprintf(text,sizeof(text),"+++ Cannot fcntl fifo path %s. [%s]",
		   fifo.c_str(), cerror(errno).c_str());
	throw std::runtime_error(text);
      }
      status &= ~O_NONBLOCK;                         /* unset O_NONBLOCK bit */
      if ( ::fcntl(this->fifo_fd, F_SETFL,status) == -1 )    {
	::close(this->fifo_fd);
	::snprintf(text,sizeof(text),"+++ Cannot set O_NONBLOCK bit of fifo %s. [%s]",
		   fifo.c_str(), cerror(errno).c_str());
	throw std::runtime_error(text);
      }
      int fd  = ::fileno(stdout);
      if ( ::dup2(this->fifo_fd, fd) == -1 )   {
	::snprintf(text,sizeof(text),"+++ Failed to dup stdout for fifo: %s. [%s]\n",
		   fifo.c_str(), cerror(errno).c_str());
	::close(this->fifo_fd);
	throw std::runtime_error(text);
      }
      fd  = ::fileno(stderr);
      if ( ::dup2(this->fifo_fd, fd) == -1 )   {
	::snprintf(text,sizeof(text),"+++ Failed to dup stderr for fifo: %s. [%s]\n",
		   fifo.c_str(), cerror(errno).c_str());
	::close(this->fifo_fd);
	throw std::runtime_error(text);
      }
    }
  }
  ///
  char host[128];
  ::gethostname(host,sizeof(host));
  this->physical_host = host;
  const char* dim_host  = std::getenv("DIM_HOST_NODE");
  this->real_host = dim_host ? dim_host : host;
  for(size_t i=0; i<this->physical_host.length(); ++i)
    this->physical_host[i] = ::tolower(this->physical_host[i]);
  for(size_t i=0; i<this->real_host.length(); ++i)
    this->real_host[i] = ::tolower(this->real_host[i]);
  ::setvbuf(stdout, nullptr, _IOLBF, 0);
  ::setvbuf(stderr, nullptr, _IOLBF, 0);
  this->rebuild_trailer();
}

/// Run the instance
void logger_imp::run() {
}

/// Stop processing and shutdown
void logger_imp::stop()    {
  ::setvbuf(stdout, nullptr, _IONBF, 0);
  ::setvbuf(stderr, nullptr, _IONBF, 0);
  this->flush_line(STDOUT);
  this->flush_line(STDERR);
  if ( this->fifo_fd >= 0 )  {
    ::syncfs(this->fifo_fd);
    ::close(this->fifo_fd);
  }
  this->fifo_fd = -1;
  if ( this->logfile_stream )  {
    ::fsync(::fileno(this->logfile_stream));
    ::fclose(this->logfile_stream);
  }
  this->logfile_stream = nullptr;
}

/// Rebuild trailer 
void logger_imp::rebuild_trailer()   {
  std::lock_guard<std::mutex> lock(this->output_mutex);
  size_t len = ::snprintf(output_trailer.second,OUTPUT_MAX_LEN,
			  "\",\"physical_host\":\"%s\",\"host\":\"%s\",\"pid\":\"%d\",\"utgid\":\"%s\",\"systag\":\"%s\"}\n",
			  this->physical_host.c_str(),
			  this->real_host.c_str(),
			  ::getpid(),
			  logger_imp::utgid.c_str(),
			  logger_imp::systag.c_str());
  output_trailer.first = len;
}

/// Dump data buffer to file descriptor
void logger_imp::dump_buffer(int fd, const char* buf, int len)   {
  struct timespec delay = {0,1000000};  /* 0.001 s */
  int tryC = 0, numRetry = 100, done = 0;
  for ( tryC=0; tryC < numRetry; tryC++ )    {
    int written = funcs().real_write(fd, buf+done, len-done);
    if ( written > 0 )
      done += written;
    else if ( written == -1 )
      break;
    else if ( errno != EAGAIN )
      break;

    if ( done == len ) break;
    nanosleep(&delay,NULL);
    delay.tv_sec  *= 2;
    delay.tv_nsec *= 2;
    if(delay.tv_nsec>999999999){
      delay.tv_sec+=1;
      delay.tv_nsec-=1000000000;
    }
  }
}

/// Escape JSON special characters if necessary
std::size_t logger_imp::escape_json(char* outstr, std::size_t max_out, const char* instr, std::size_t in_len)  const   {
  char* o = outstr;
  const char* p = instr;
  const char* stop = outstr + max_out;
  for( const char* e = instr + in_len; p < e; ++p)  {
    switch (*p) {
      /// Escape Quote and slash
    case '"':    o[0] = '\\'; o[1] = '"';                                                 o += 2; break;
    case '\\':   o[0] = '\\'; o[1] = '\\';                                                o += 2; break;
      /// Escape x00 through x1f
    case '\x00': o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '0'; o[5] = '0'; o += 6; break;
    case '\x01': o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '0'; o[5] = '1'; o += 6; break;
    case '\x02': o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '0'; o[5] = '2'; o += 6; break;
    case '\x03': o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '0'; o[5] = '3'; o += 6; break;
    case '\x04': o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '0'; o[5] = '4'; o += 6; break;
    case '\x05': o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '0'; o[5] = '5'; o += 6; break;
    case '\x06': o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '0'; o[5] = '6'; o += 6; break;
    case '\x07': o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '0'; o[5] = '7'; o += 6; break;
    case '\b':   o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '0'; o[5] = '8'; o += 6; break;
    case '\t':   o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '0'; o[5] = '9'; o += 6; break;
    case '\n':   o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '0'; o[5] = 'a'; o += 6; break;
    case '\x0b': o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '0'; o[5] = 'b'; o += 6; break;
    case '\f':   o[0] = '\\'; o[1] = 'f';                                                 o += 2; break;
    case '\r':   o[0] = '\\'; o[1] = 'r';                                                 o += 2; break;
    case '\x0e': o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '0'; o[5] = 'e'; o += 6; break;
    case '\x0f': o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '0'; o[5] = 'f'; o += 6; break;

    case '\x10': o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '1'; o[5] = '0'; o += 6; break;
    case '\x11': o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '1'; o[5] = '1'; o += 6; break;
    case '\x12': o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '1'; o[5] = '2'; o += 6; break;
    case '\x13': o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '1'; o[5] = '3'; o += 6; break;
    case '\x14': o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '1'; o[5] = '4'; o += 6; break;
    case '\x15': o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '1'; o[5] = '5'; o += 6; break;
    case '\x16': o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '1'; o[5] = '6'; o += 6; break;
    case '\x17': o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '1'; o[5] = '7'; o += 6; break;
    case '\x18': o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '1'; o[5] = '8'; o += 6; break;
    case '\x19': o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '1'; o[5] = '9'; o += 6; break;
    case '\x1a': o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '1'; o[5] = 'a'; o += 6; break;
    case '\x1b': o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '1'; o[5] = 'b'; o += 6; break;
    case '\x1c': o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '1'; o[5] = 'c'; o += 6; break;
    case '\x1d': o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '1'; o[5] = 'd'; o += 6; break;
    case '\x1e': o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '1'; o[5] = 'e'; o += 6; break;
    case '\x1f': o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '1'; o[5] = 'f'; o += 6; break;
      /// Rest stays as is
    default:
      *o = *p; ++o;
    }
    if ( o+6 > stop )   {
      break;
    }
  }
  /// This is the number of characters used in the output buffer
  return o-outstr;
}

/// Build output record and dump record to output device
void logger_imp::flush_line(int stream_id)   {
  auto& data = output[stream_id];
  data.second[data.first] = 0;
  if ( data.first > 0 )   {
    if ( this->logfile_stream )   {
      struct tm tim;
      struct timeval now;
      char format[256];
      char output_buffer[OUTPUT_MAX_LEN];
      ::gettimeofday(&now, nullptr);
      ::localtime_r(&now.tv_sec, &tim);
      ::snprintf(format, sizeof(format),"%%Y-%%m-%%dT%%H:%%M:%%S.%03ld",
		 long(now.tv_usec/1000));
      ::strftime(output_buffer,sizeof(output_buffer), format, &tim);
      if ( ::strchr(data.second,'\n') )
	funcs().real_fprintf(this->logfile_stream, "%s %s", output_buffer, data.second);
      else
	funcs().real_fprintf(this->logfile_stream, "%s %s\n", output_buffer, data.second);
      ::fflush(this->logfile_stream);
    }
    if ( this->fifo_fd >= 0 )   {
      struct tm tim;
      struct timeval now;
      size_t len, mx;
      char output_buffer[2*OUTPUT_MAX_LEN];
      char format[256];
      ::gettimeofday(&now, nullptr);
      ::localtime_r(&now.tv_sec, &tim);
      ::snprintf(format, sizeof(format), 
		 "{\"@timestamp\":\"%%Y-%%m-%%dT%%H:%%M:%%S.%03ld%%z\",\"message\":\"",
		 long(now.tv_usec/1000));
      len = ::strftime(output_buffer,sizeof(output_buffer), format, &tim);
      if ( len+data.first < sizeof(output_buffer) )   {
	//mx = std::min(sizeof(output_buffer)-len, data.first);
	len += escape_json(output_buffer+len, sizeof(output_buffer)-len, data.second, data.first);
	//::memcpy(output_buffer+len, data.second, mx);
	//len += mx;
	if ( len < sizeof(output_buffer) )   {
	  mx = std::min(sizeof(output_buffer)-len, this->output_trailer.first);
	  ::memcpy(output_buffer+len, this->output_trailer.second, mx);
	  len += mx;
	  output_buffer[sizeof(output_buffer)-1] = 0;
	  this->dump_buffer(this->fifo_fd, output_buffer, len);
	  ++this->msg_count;
	}
      }
      data.first = 0;
      data.second[0] = 0;
    }
    else   {
      data.second[++data.first] = '\n';
      data.second[++data.first] = 0;
      if ( stream_id == STDOUT )
	this->dump_buffer(STDOUT_FILENO, data.second, data.first);
      else
	this->dump_buffer(STDERR_FILENO, data.second, data.first);
    }
  }
  data.first = 0;
  data.second[0] = 0;
  //::memset(data.second,0,OUTPUT_MAX_LEN+255);
}

/// Append (locked) formatted string to output buffer
int logger_imp::push(int stream_id, const char* format, LOGGER_VA_LIST& args)  {
  auto& data = output[stream_id];
  std::lock_guard<std::mutex> lock(this->output_mutex);
  if ( data.first > 0 )   {
    this->flush_line(stream_id);
  }
  std::size_t len = 0;
  if ( this->logfile_stream )   {
    struct tm tim;
    struct timeval now;
    time_t tim_now;
    char output_buffer[OUTPUT_MAX_LEN];
    ::gettimeofday(&now, nullptr);
    tim_now = now.tv_sec;
    ::localtime_r(&tim_now, &tim);
    len = ::strftime(output_buffer,sizeof(output_buffer),"%Y-%m-%dT%H:%M:%S", &tim);
    if ( len ) {} // Need this for debugging!
    len = funcs().real_fprintf(this->logfile_stream, "%s.%03ld ",output_buffer, long(now.tv_usec/1000));
    if ( len ) {} // Need this for debugging!
    len = funcs().real_vfprintf(this->logfile_stream, format, args);
    if ( len ) {} // Need this for debugging!
    funcs().real_fputs("\n", this->logfile_stream);
    ::fflush(this->logfile_stream);
  }
  if ( this->fifo_fd >= 0 )   {
    struct tm tim;
    struct timeval now;
    char output_buffer[OUTPUT_MAX_LEN];
    char fmt[256];
    ::gettimeofday(&now, nullptr);
    ::localtime_r(&now.tv_sec, &tim);
    ::snprintf(fmt, sizeof(fmt), 
	       "{\"@timestamp\":\"%%Y-%%m-%%dT%%H:%%M:%%S.%03ld%%z\",\"message\":\"",
	       long(now.tv_usec/1000));
    len = ::strftime(output_buffer,sizeof(output_buffer), fmt, &tim);
    if ( stream_id == STDOUT )   {
      funcs().real_write(STDOUT_FILENO, output_buffer, len);
      len = funcs().real_vfprintf(stdout, format, args);
      if ( len ) {} // Need this for debugging!
      funcs().real_fputs(this->output_trailer.second, stdout);
      ::fflush(stdout);
    }
    else   {
      funcs().real_write(STDERR_FILENO, output_buffer, len);
      len = funcs().real_vfprintf(stderr, format, args);
      if ( len ) {} // Need this for debugging!
      funcs().real_fputs(this->output_trailer.second, stderr);
      ::fflush(stderr);
    }
    ++this->msg_count;
    return len;
  }
  if ( stream_id == STDOUT )   {
    len = funcs().real_vfprintf(stdout, format, args);
    funcs().real_fputs("\n", stdout);
  }
  else   {
    len = funcs().real_vfprintf(stderr, format, args);
    funcs().real_fputs("\n", stderr);
  }
  return len;
}

/// Default constructor
fifolog::LibCLogger::LibCLogger()   {
}

/// Default destructor
fifolog::LibCLogger::~LibCLogger()   {
}

/// Object initialization
void fifolog::LibCLogger::init(const std::string& f, const std::string& u, const std::string& t)   {
  this->logger_imp::init(f, u, t);
  if ( this->fifo_fd < 0 )   {
    char text[1024];
    std::string& fifo  = Logger::Implementation::fifo_name;
    if ( !fifo.empty() )   {
      int fd  = ::fileno(stdout);
      int ret = ::dup2(this->fifo_fd, fd);
      if ( ret == -1 )   {
	::snprintf(text,sizeof(text),"+++ Failed to dup stdout for fifo: %s. [%s]\n",
		   fifo.c_str(), cerror(errno).c_str());
	::close(this->fifo_fd);
	throw std::runtime_error(text);
      }
      fd  = ::fileno(stderr);
      ret = ::dup2(this->fifo_fd, fd);
      if ( ret == -1 )   {
	::snprintf(text,sizeof(text),"+++ Failed to dup stderr for fifo: %s. [%s]\n",
		   fifo.c_str(), cerror(errno).c_str());
	::close(this->fifo_fd);
	throw std::runtime_error(text);
      }
      //this->push(STDOUT,"LIBC logger initialized!!!\n");
    }
  }
}

/// Default constructor
fifolog::PollLogger::PollLogger()   {
}

/// Default destructor
fifolog::PollLogger::~PollLogger()   {
  this->done = true;
  if ( this->runner.get() )   {
    try  {
      this->runner->join();
      this->runner.reset();
    }
    catch(...)   {
    }
  }
}

/// Object initialization
void fifolog::PollLogger::init(const std::string& f, const std::string& u, const std::string& t)   {
  if ( this->fifo_fd < 0 )   {
    this->logger_imp::init(f, u, t);
    std::string& fifo  = Logger::Implementation::fifo_name;
    char text[1024];
    int fd, ret;
    ///
    ret = ::pipe(this->std_fd);
    if ( ret != 0 )   {
      ::snprintf(text,sizeof(text),"+++ Failed to create stdout pipe for fifo: %s. [%s]\n",
		 fifo.c_str(), cerror(errno).c_str());
      ::close(this->fifo_fd);
      this->fifo_fd = -1;
      throw std::runtime_error(text);
    }
    fd  = ::fileno(stdout);
    ret = ::dup2(this->std_fd[1],fd);
    if ( ret == -1 )   {
      ::snprintf(text,sizeof(text),"+++ Failed to dup stdout for fifo: %s. [%s]\n",
		 fifo.c_str(), cerror(errno).c_str());
      ::close(this->fifo_fd);
      this->fifo_fd = -1;
      throw std::runtime_error(text);
    }
    fd  = ::fileno(stderr);
    ret = ::dup2(this->std_fd[1],fd);
    if ( ret == -1 )   {
      ::snprintf(text,sizeof(text),"+++ Failed to dup stderr for fifo: %s. [%s]\n",
		 fifo.c_str(), cerror(errno).c_str());
      ::close(this->fifo_fd);
      this->fifo_fd = -1;
      throw std::runtime_error(text);
    }
    ///
    struct epoll_event ev;
    this->epoll_fd  = ::epoll_create(10);
    ev.events   = EPOLLIN | EPOLLRDHUP | EPOLLERR | EPOLLHUP | EPOLLET;
    ///
    ev.data.fd  = this->std_fd[0];
    ::fcntl(this->std_fd[0], F_SETFL, O_NONBLOCK);
    ret = ::epoll_ctl(this->epoll_fd, EPOLL_CTL_ADD, this->std_fd[0], &ev);
    if ( ret != 0 )    {
      ::snprintf(text,sizeof(text),"+++ Failed to add FD %d to epoll structure for fifo: %s. [%s]\n",
		 this->std_fd[0], fifo.c_str(), cerror(errno).c_str());
      ::close(this->epoll_fd);
      ::close(this->fifo_fd);
      throw std::runtime_error(text);
    }
    //this->push(STDOUT,"POLL logger initialized!!!\n");
  }
}

/// Run the logger in a separate thread
void fifolog::PollLogger::run()   {
  if ( !this->runner.get() )   {
    this->runner = std::make_unique<std::thread>([this]()  {  this->poll(); });
  }
}

/// Stop processing and shutdown
void fifolog::PollLogger::stop()  {
  this->done = true;
  this->runner->join();
  this->Logger::Implementation::stop();
}

/// Poll data from stdout/stderr pipes
void fifolog::PollLogger::poll()    {
  int count = 0;
  // this->push(STDOUT, "POLL thread is starting now....\n");
  while ( 1 )    {
    struct epoll_event ep;
    int nc = ::epoll_wait(this->epoll_fd, &ep, 1, 100);
    if ( nc > 0 )    {
      if ( ep.events&EPOLLIN )   {
	int fd = ep.data.fd;
	char    text[512];
	ssize_t nbyt = ::read(fd, text, sizeof(text)-2);
	if ( nbyt <= 0 )    {
	  return;
	}
	text[nbyt] = 0;
	this->push(STDOUT, text, nbyt);
      }
    }
    if ( count > 5 )
      break;
    else if ( this->done )
      ++count;
  }
  // this->push(STDOUT, "POLL thread is exiting now....\n");
}

/// Initializing constructor
fifolog::Logger::Logger(const std::string& fifo_name, const std::string& utgid, const std::string& tag)   {
  if ( !this->imp.get() ) this->imp.reset(&log());
  this->imp->init(fifo_name, utgid, tag);
}

/// Default destructor
fifolog::Logger::~Logger()   {
  if ( this->imp.get() ) this->imp.release();
}

/// Run the logger in a separate thread
void fifolog::Logger::run()    {
  if ( this->imp.get() ) this->imp->run();
}

/// Stop processing and shutdown
void fifolog::Logger::stop()    {
  if ( this->imp.get() ) this->imp->stop();
}

/// Initialize the logger unit
extern "C" void fifolog_initialize_logger()    {
  fifolog::logger(true)->run();
}

/// Set new utgid value
extern "C" void fifolog_set_utgid(const char* new_utgid)    {
  char text[1024];
  auto& log = fifolog::logger(false);
  const char* utgid = std::getenv("UTGID");
  if ( !utgid )    {
    utgid = std::getenv("PROCESS_NAME");
  }
  if ( !utgid )    {
    ::snprintf(text,sizeof(text),"P%06d",::getpid());
    utgid = text;
  }
  logger_imp::utgid = new_utgid ? new_utgid : utgid;
  if ( log.get() ) log->imp->rebuild_trailer();
}

/// Set new tag value
extern "C" void fifolog_set_tag(const char* new_tag)    {
  auto& log = fifolog::logger(false);
  logger_imp::systag = new_tag && ::strlen(new_tag)>0 ? new_tag : "SYSTEM";
  if ( log.get() ) log->imp->rebuild_trailer();
}

/// Set fifo value (Only valid BEFORE startup)
extern "C" int fifolog_set_fifo(const char* fifo)    {
  auto& log = fifolog::logger(false);
  if ( fifo && !log.get() )   {
    logger_imp::fifo_name = fifo;
    return 1;
  }
  else if ( log.get() )    {
    log->imp->rebuild_trailer();
    return 1;
  }
  ::fprintf(stderr,"Failed to set output fifo: %s [%s] logger %s initialized.\n",
	    fifo ? fifo : "???", fifolog::cerror(errno).c_str(), log.get() ? "already" : "NOT");
  errno = ENOENT;
  return 0;
}

/// Shutdown the loggetr unit
extern "C" void fifolog_finalize_logger()    {
  auto& log = fifolog::logger(false);
  if ( log.get() )    {
    log->stop();
    log.reset();
    ::fprintf(stderr, "\nFinalizing output logger....\n");  
  }
}

extern "C" int local__fputc(int c, FILE *stream)   {
  if ( fifolog::logger(false).get() )   {
    if ( stream == stdout )
      return fifolog::log().push(logger_imp::STDOUT, c);
    else if ( stream == stderr )
      return fifolog::log().push(logger_imp::STDERR, c);
  }
  return fifolog::funcs().real_fputc(c, stream);
}

#ifdef __clang__
extern "C" int local__putc(int c, FILE *stream)   {
  return ::local__fputc(c, stream);
}

extern "C" int local__putchar(int c)   {
  return ::local__fputc(c, stdout);
}
#else
extern "C" int putc(int c, FILE *stream)   {
  return ::local__fputc(c, stream);
}

extern "C" int putchar(int c)   {
  return ::local__fputc(c, stdout);
}
#endif

extern "C" int local__fputs(const char *str, FILE *stream)   {
  if ( fifolog::logger(false).get() )   {
    if ( stream == stdout )
      return fifolog::log().push(logger_imp::STDOUT, str);
    else if ( stream == stderr )
      return fifolog::log().push(logger_imp::STDERR, str);
  }
  return fifolog::funcs().real_fputs(str, stream);
}

extern "C" int local__puts(const char* str)    {
  return ::fputs(str, stdout);
}

extern "C" ssize_t local__write(int fd, const void *buf, size_t count)    {
  if ( fifolog::logger(false).get() )   {
    switch(fd)   {
    case STDOUT_FILENO: return fifolog::log().push(logger_imp::STDOUT, buf, count);
    case STDERR_FILENO: return fifolog::log().push(logger_imp::STDERR, buf, count);
    default:            break;
    }
  }
  return fifolog::funcs().real_write(fd, buf, count);
}

extern "C" size_t local__fwrite(const void *str, size_t size, size_t nmemb, FILE *stream)   {
  if ( fifolog::logger(false).get() )   {
    if ( stream == stdout )
      return fifolog::log().push(logger_imp::STDOUT, str, size*nmemb);
    else if ( stream == stderr )
      return fifolog::log().push(logger_imp::STDERR, str, size*nmemb);
  }
  return fifolog::funcs().real_fwrite(str, size, nmemb, stream);
}

extern "C" int local__vprintf(const char *format, LOGGER_VA_LIST& args)   {
  return fifolog::logger(false).get()
    ? fifolog::log().push(logger_imp::STDOUT, format, args)
    : fifolog::funcs().real_vprintf(format, args);
}

extern "C" int local__vwprintf(const wchar_t *format, LOGGER_VA_LIST& args)   {
  return fifolog::logger(false).get()
    ? fifolog::log().push(logger_imp::STDOUT, format, args)
    : fifolog::funcs().real_vwprintf(format, args);
}

extern "C" int local__vdprintf(int fd, const char *format, LOGGER_VA_LIST& args)   {
  if ( fifolog::logger(false).get() )  {
    switch(fd)   {
    case STDOUT_FILENO: return fifolog::log().push(logger_imp::STDOUT, format, args);
    case STDERR_FILENO: return fifolog::log().push(logger_imp::STDERR, format, args);
    default:            break;
    }
  }
  return fifolog::funcs().real_vdprintf(fd, format, args);
}

extern "C" int local__vfprintf(FILE* stream, const char *format, LOGGER_VA_LIST& args)   {
  if ( fifolog::logger(false).get() )  {
    if ( stream == stdout )
      return fifolog::log().push(logger_imp::STDOUT, format, args);
    else if ( stream == stderr )
      return fifolog::log().push(logger_imp::STDERR, format, args);
  }
  return fifolog::funcs().real_vfprintf(stream, format, args);
}

extern "C" int local__vfprintf_chk(FILE* stream, int flag, const char *format, LOGGER_VA_LIST& args)   {
  if ( fifolog::logger(false).get() )  {
    if ( stream == stdout )
      return fifolog::log().push(logger_imp::STDOUT, format, args);
    else if ( stream == stderr )
      return fifolog::log().push(logger_imp::STDERR, format, args);
  }
  return fifolog::funcs().real_vfprintf_chk(stream, flag, format, args);
}

extern "C" int local__vprintf_chk(int flag, const char *format, LOGGER_VA_LIST& args)   {
  return ::local__vfprintf_chk(stdout, flag, format, args);
}

extern "C" int local__printf_chk(int flag, const char *format, ...)   {
  LOGGER_VA_LIST args;
  va_start(args, format);
  int ret = ::local__vprintf_chk(flag, format, args);
  va_end(args);
  return ret;
}

extern "C" int local__fprintf_chk(FILE* stream, int flag, const char *format, ...)   {
  LOGGER_VA_LIST args;
  va_start(args, format);
  int ret = ::local__vfprintf_chk(stream, flag, format, args);
  va_end(args);
  return ret;  
}

extern "C" int local__printf(const char *format, ...)  {
  LOGGER_VA_LIST args;
  va_start(args, format);
  int ret = ::local__vprintf(format, args);
  va_end(args);
  return ret;
}

extern "C" int local__wprintf(const wchar_t *format, ...)  {
  LOGGER_VA_LIST args;
  va_start(args, format);
  int ret = ::vwprintf(format, args);
  va_end(args);
  return ret;
}

extern "C" int local__dprintf(int fd, const char *format, ...)   {
  LOGGER_VA_LIST args;
  va_start(args, format);
  int ret = ::local__vdprintf(fd, format, args);
  va_end(args);
  return ret;  
}

extern "C" int local__fprintf(FILE* stream, const char *format, ...)   {
  LOGGER_VA_LIST args;
  va_start(args, format);
  int ret = ::local__vfprintf(stream, format, args);
  va_end(args);
  return ret;  
}

/// Print a message. Don't know why this does not work from python...
extern "C" void fifolog_print(int level, const char* source, const char* message)    {
  const char* src = source ? source : "RTL";
  const char* msg = message ? message : "";
  const char* lvl = "ALWAYS";
  switch(level)   {
  case 0:
  case 1:  lvl = "VERBOSE"; break;
  case 2:  lvl = "DEBUG";   break;
  case 3:  lvl = "INFO";    break;
  case 4:  lvl = "WARNING"; break;
  case 5:  lvl = "ERROR";   break;
  case 6:  lvl = "FATAL";   break;
  case 7:
  default: lvl = "ALWAYS";  break;
  }
  ::local__fprintf(stderr, "%-8s %-24s %s\n", lvl, src, msg);
}

__asm__ (".global fputc;          .type fputc,@function;           fputc = local__fputc");
#ifdef __clang__
__asm__ (".global putc;           .type putc,@function;            putc = local__putc");
__asm__ (".global putchar;        .type putchar,@function;         putchar = local__putchar");
#endif
__asm__ (".global fputs;          .type fputs,@function;           fputs = local__fputs");
__asm__ (".global write;          .type write,@function;           write = local__write");
__asm__ (".global fputs;          .type fputs,@function;           fputs = local__fputs");
__asm__ (".global puts;           .type puts,@function;            puts = local__puts");
__asm__ (".global fwrite;         .type fwrite,@function;          fwrite = local__fwrite");

__asm__ (".global vprintf;        .type vprintf,@function;         vprintf = local__vprintf");
__asm__ (".global vwprintf;       .type vwprintf,@function;        vwprintf = local__vwprintf");
__asm__ (".global vdprintf;       .type vdprintf,@function;        vdprintf = local__vdprintf");
__asm__ (".global vfprintf;       .type vfprintf,@function;        vfprintf = local__vfprintf");
__asm__ (".global printf;         .type printf,@function;          printf = local__printf");
__asm__ (".global wprintf;        .type wprintf,@function;         wprintf = local__wprintf");
__asm__ (".global dprintf;        .type dprintf,@function;         dprintf = local__dprintf");
__asm__ (".global fprintf;        .type fprintf,@function;         fprintf = local__fprintf");
__asm__ (".global vfprintf_chk;   .type vfprintf_chk,@function;    vfprintf_chk = local__vfprintf_chk");
__asm__ (".global __vfprintf_chk; .type __vfprintf_chk,@function;  __vfprintf_chk = local__vfprintf_chk");
__asm__ (".global __printf_chk;   .type __printf_chk,@function;    __printf_chk = local__printf_chk");
__asm__ (".global __fprintf_chk;  .type __fprintf_chk,@function;   __fprintf_chk = local__fprintf_chk");
__asm__ (".global __vfprintf_chk; .type __vfprintf_chk,@function;  __vfprintf_chk = local__vfprintf_chk");

#undef LOGGER_VA_LIST
