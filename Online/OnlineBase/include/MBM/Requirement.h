//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
#ifndef _MBM_REQUIREMENT_H
#define _MBM_REQUIREMENT_H

// C/C++ include files
#include <string>

/*
 *  MBM namespace declaration
 */
namespace MBM {

  /** @class Requirement Requirement.h MBM/Reqirement.h
    *
    *   @author  B.Jost
    *   @version 1.0
    *   @date    12/1/2006
    */
  class Requirement   {
  public:
    int evtype;
    unsigned int trmask[4];
    unsigned int vetomask[4];
    int userType;
    int freqType;
    int maskType;
    float freq    { 1.0 };
  public:
    /// Default constructor
    Requirement();
    /// Initializing constructor: call internally parse
    Requirement(const std::string& reqstring);
    /// Parse requirement from string
    void parse(const std::string& reqstring);
    /// Reset requirement to default values
    void reset();
  };
}
#endif  // _MBM_REQUIREMENT_H
