//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
//  MBM Monitor
//==========================================================================
#ifndef ONLINE_ONLINEKERNEL_MBM_MONITOR_H
#define ONLINE_ONLINEKERNEL_MBM_MONITOR_H

/// Framework include files
#include <MBM/bmdef.h>

/// C/C++ include files
#include <vector>
#include <string>
#include <memory>

/// MBM namespace declaration
namespace MBM {

  /// MBM Monitoring class
  /**
   *  @author  M.Frank
   *  @version 1.0
   */
  class Summary   {
  public:
    class internals_t;
  protected:
    std::unique_ptr<internals_t> imp;

  public:
    /// Initializing constructor
    Summary(const std::string& regular_expression, bool print_errors=true);
    /// Default constructor
    Summary();
    /// Default destructor
    virtual ~Summary();
    /// Show MBM status information for selected buffers
    virtual int show(bool show_states);
    /// Show MBM status information for single buffers
    virtual int show(BufferMemory* dsc, bool show_states);
    /// Retrieve MBM status information for selected buffers
    virtual std::pair<int, std::vector<std::string> >
    get_info(bool show_states);
    /// Retrieve MBM status information for single buffers
    virtual std::pair<int, std::vector<std::string> >
    get_info(BufferMemory* dsc, bool show_states);
  };
}      // End namespace MBM
#endif // ONLINE_ONLINEKERNEL_MBM_MONITOR_H
