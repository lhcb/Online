//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
#ifndef _MBM_MBMDEF_H 
#define _MBM_MBMDEF_H

#include <cstddef>

/// MBM error codes
enum MBM_Status   {
  MBM_NO_CONS    =  0X9CA807C,
  MBM_NO_FREE_SL =  0X9CA8072,
  MBM_NO_FREE_US =  0X9CA806A,
  MBM_NO_FREE_EV =  0X9CA806C,
  MBM_INTERNAL   =  0X9CA8062,
  MBM_EV_TOO_BIG =  0X9CA805A,
  MBM_ZERO_LEN   =  0X9CA805C,
  MBM_REQ_CANCEL =  0X9CA8052,
  MBM_TOO_MANY   =  0X9CA804A,
  MBM_INCOMPLETE =  0X9CA804C,

  MBM_ILL_REQ    =  0X9CA8042,
  MBM_ILL_DEST   =  0X9CA803A,
  MBM_ILL_NAME   =  0X9CA803C,
  MBM_ILL_CONS   =  0X9CA8032,
  MBM_ILL_LEN    =  0X9CA802A,
  MBM_NO_EVENT   =  0X9CA802C,
  MBM_NO_ROOM    =  0X9CA8022,
  MBM_NO_PRIV    =  0X9CA801A,
  MBM_ILL_CALL   =  0X9CA801C,
  MBM_INACTIVE   =  0X9CA8012,

  MBM_NO_REPLY   =  0X9CA8001,
  MBM_NORMAL     =  0x1,
  MBM_ERROR      =  0x0
};

enum MBM_EvTypes  {
  EVENT_TYPE_OK      = 0,
  EVENT_TYPE_MEP     = 1,
  EVENT_TYPE_EVENT   = 2,
  EVENT_TYPE_BURST   = 3,
  EVENT_TYPE_ERROR   = 4,
  EVENT_TYPE_MISC    = 5,
  EVENT_TYPE_BADPROC = 6
};

enum MBM_Communications  {
  BM_COM_NONE = 0,
  BM_COM_FIFO = 1,
  BM_COM_ASIO = 2,
  BM_COM_UNIX = 4,
  BM_COM_SHM1 = 8,
  BM_COM_SHM2 = 16
};

enum MBM_Dimensions  {
  BM_MASK_SIZE     =  4,
  BM_BUFF_NAME_LEN = 32,
  BM_USER_NAME_LEN = 64
};

enum MBM_Internals  {
  BM_MAX_REQS   = 8,
  BM_MAX_EVTYPE = 10,
  BM_MAX_REQONE = 12,   // This must be a multiple of 4!!!
  BM_MAX_THREAD = 32
};

enum MBM_MaskTypes  {
  BM_MASK_ANY      = 0,
  BM_MASK_ALL      = 1,
  BM_MASK_REQ      = 0xFF  // Only used if Request type is BM_REQ_ONE
};

enum MBM_RequestTypes  {
  BM_NOTALL       = 0,
  BM_REQ_USER     = 0,
  BM_REQ_VIP      = 1,
  BM_REQ_ALL      = 1,
  BM_REQ_ONE      = 2,
  BM_REQ_ONE_USER = 3
};

enum MBM_FrequencyTypes  {
  BM_FREQ_MANY = 0,
  BM_FREQ_PERC = 1
};

enum MBM_IncludeModes  {
  BM_INC_READ  = 1<<0,
  BM_INC_WRITE = 1<<1,
  BM_INC_RDWR  = BM_INC_READ|BM_INC_WRITE
};

struct BufferMemory;
struct BMDESCRIPT;
struct CONTROL;
struct USER;
struct EVENT;

typedef int (*RTL_ast_t)(void*);
typedef struct lib_rtl_gbl*  lib_rtl_gbl_t;
typedef struct BMDESCRIPT*   BMID;

#define MBM_INV_DESC   ((BMID)-1)
#define MBM_INV_MEMORY ((BufferMemory*)-1)

#ifdef __cplusplus
#define __MBM_CONST const
extern "C"  {
#endif

  int  mbm_qmtest_check_no_active_buffers(int, char**);
  int  mbm_qmtest_check_start();

  int  mbm_deinstall(int argc , char** argv);
  int  mbm_dump(int argc , char** argv);
  int  mbm_mon(int argc , char** argv);

  /// Include as client in buffer manager.
  BMID mbm_include (const char* bm_name, const char* name, int partid, int com_type, int flags);
#ifdef __cplusplus
  /// Include as client in buffer manager in read-only mode
  BMID mbm_include_read (const char* bm_name, const char* name, int partid, int com_type=BM_COM_FIFO);
  /// Include as client in buffer manager in write-only mode
  BMID mbm_include_write (const char* bm_name, const char* name, int partid, int com_type=BM_COM_FIFO);
#else
  /// Include as client in buffer manager in read-only mode
  BMID mbm_include_read (const char* bm_name, const char* name, int partid, int com_type=BM_COM_FIFO);
  /// Include as client in buffer manager in write-only mode
  BMID mbm_include_write (const char* bm_name, const char* name, int partid, int com_type=BM_COM_FIFO);
#endif
  /// Include as client in buffer manager reusing the masters global section
  BMID mbm_connect(BMID master, const char* name, int partid);
  /// Exclude as client from the buffer manager and release resources
  int  mbm_exclude (BMID bm);

  /// Access the shared buffer's start address
  __MBM_CONST char* mbm_buffer_address(BMID);
  /// Access the shared buffer's size
  int mbm_buffer_size(BMID bm, size_t* size);
  /// Access the shared buffer's start address and it's length in bytes
  int mbm_buffer_memory(BMID bm, const char** addr, size_t* size);

  int  mbm_clear(BMID bm);
  /// Access buffer name from BMID. Returns NULL on error/invalid BMID
  const char* mbm_buffer_name(BMID bm);

  int  mbm_add_req (BMID bm, int evtype, __MBM_CONST unsigned int* trmask, __MBM_CONST unsigned int* veto, int masktype, 
                    int usertype, int freqmode, float freq);
  int  mbm_del_req      (BMID bm, int evtype, __MBM_CONST unsigned int* trmask, __MBM_CONST unsigned int* veto, int masktype, int usertype);
  int  mbm_get_event_a  (BMID bm, int** ptr, long* size, int* evtype, unsigned int* trmask, int part_id, RTL_ast_t astadd, void* ast_par);
  int  mbm_get_event_try(BMID bm, int** ptr, long* size, int* evtype, unsigned int* trmask, int part_id);
  int  mbm_get_event    (BMID bm, int** ptr, long* size, int* evtype, unsigned int* trmask, int part_id);
  int  mbm_free_event   (BMID bm);
  int  mbm_pause        (BMID bm);

  // Not implemented: int  mbm_get_space  (BMID bm, long len, int** ptr);
  int  mbm_get_space_a(BMID bm, long len, int** ptr, RTL_ast_t astadd, void* ast_par);
  // Returns MBM_NO_ROOM if space is not availible
  int  mbm_get_space_try(BMID bm, long len, int** ptr, RTL_ast_t astadd, void* ast_par);
  int  mbm_declare_event(BMID bm, long len, int evtype, __MBM_CONST unsigned int* trmask, const char* dest,
                         void** free_add, long* free_size, int part_id);
  // Returns MBM_NO_ROOM if no event slot is not availible
  int  mbm_declare_event_try(BMID bm, long len, int evtype, __MBM_CONST unsigned int* trmask, const char* dest,
                             void** free_add, long* free_size, int part_id);
  int  mbm_declare_event_and_send (BMID bm, long len, int evtype, __MBM_CONST unsigned int* trmask,
                                   const char* dest, void** free_add, long* free_size, int part_id);
  int  mbm_free_space  (BMID bm);
  int  mbm_send_space  (BMID bm);
  int  mbm_wait_space  (BMID bm);
  int  mbm_wait_space_a(BMID bm);

  int  mbm_cancel_request   (BMID bm);
  int  mbm_set_cancelled    (BMID bm, int value);
  int  mbm_is_cancelled     (BMID bm);

  int  mbm_stop_consumer    (BMID bm);
  int  mbm_stop_producer    (BMID bm);
  int  mbm_grant_update     (BMID bm);
  int  mbm_events_actual    (BMID bm, int *events);
  int  mbm_events_produced  (BMID bm, int *events);
  int  mbm_events_seen      (BMID bm, int *events);
  int  mbm_reset_statistics (BMID bm);
  /// Access to minimal buffer allocation size
  int  mbm_min_alloc        (BMID bm, long* size);
  int  mbm_events_in_buffer (BMID bm, int* events);
  int  mbm_space_in_buffer  (BMID bm, long* total, long* large);
  int  mbm_process_exists   (BMID bm, const char* name, int* exists);

  int  mbm_wait_event(BMID bm);
  int  mbm_wait_event_a(BMID bm);

  /// Map monitoring shared memory section
  BufferMemory* mbm_map_mon_memory(__MBM_CONST char* bm_name);
  /// Map monitoring shared memory section
  BufferMemory* mbm_map_mon_memory_quiet(__MBM_CONST char* bm_name);
  /// Unmap monitoring shared memory section
  int           mbm_unmap_mon_memory(BufferMemory* bmid);
  /// Access control table in monitoring section
  CONTROL*      mbm_get_control_table(BufferMemory* bmid);
  /// Access user table in monitoring section
  USER*         mbm_get_user_table(BufferMemory* bmid);
  /// Access event table in monitoring section
  EVENT*        mbm_get_event_table(BufferMemory* bmid);
  /// Map global buffer information on this machine
  int           mbm_map_global_buffer_info(lib_rtl_gbl_t* handle, bool create=true);
  /// Unmap global buffer information on this machine
  int           mbm_unmap_global_buffer_info(lib_rtl_gbl_t handle, bool remove=false);

#ifdef __cplusplus
#undef __MBM_CONST
}
#endif

#endif // _MBM_MBMDEF_H
