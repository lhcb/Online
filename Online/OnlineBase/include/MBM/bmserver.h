//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
#ifndef _MBM_MBMSERVER_H 
#define _MBM_MBMSERVER_H

/// Framework includes
#include <MBM/bmdef.h>

/// Forward declarations
struct MBMMessage;
typedef struct ServerBMID_t* ServerBMID;

#ifdef __cplusplus
#include <map>
#include <string>

std::map<std::string,ServerBMID> mbm_multi_install(int argc , char** argv);
#endif

extern "C" {

  ServerBMID  mbm_install_server(int argc , char** argv);
  /// Access buffer name from BMID
  int  mbmsrv_buffer_name(ServerBMID bm, char* buff, size_t buff_len);
  /// Access buffer address from offset
  int  mbmsrv_event_address(ServerBMID bm, long offset, void** address);
  /// Access server communication type
  int  mbmsrv_communication_type(ServerBMID bm);

  /// Add consumer requirement to server
  int mbmsrv_require_consumer(ServerBMID bm, const char* name, int partid, int evtype, const unsigned int mask[]);
  /// Deregister consumer requirement from server
  int mbmsrv_unrequire_consumer(ServerBMID bm, const char* name, int partid, int evtype, const unsigned int mask[]);

  /// Map global buffer information on this machine
  int  mbmsrv_map_global_buffer_info(lib_rtl_gbl_t* handle, bool create=true);
  /// Unmap global buffer information on this machine
  int  mbmsrv_unmap_global_buffer_info(lib_rtl_gbl_t handle, bool remove=false);
  /// Map buffer manager memory
  int  mbmsrv_map_memory(const char* buffer_name, BufferMemory* bmid);
  /// Map buffer manager's monitoring memory sections
  int  mbmsrv_map_mon_memory(const char* buffer_name, BufferMemory* bmid);
  /// Map buffer manager's monitoring memory sections
  int  mbmsrv_map_mon_memory_quiet(const char* buffer_name, BufferMemory* bmid);
  /// Re-map buffer manager's monitoring memory sections
  int  mbmsrv_remap_mon_memory(BufferMemory* bmid);
  /// Un-map buffer manager's monitoring memory sections
  int  mbmsrv_unmap_memory(BufferMemory* bmid);

  /// Unlink and unmap managers memory sections
  int  mbmsrv_unlink_memory(ServerBMID bmid);
  /// Server interface: Disconnect server process from clients and shut it down
  int mbmsrv_disconnect(ServerBMID bm);
  /// Destroy connection handle
  int  mbmsrv_destroy(ServerBMID bmid);
  /// Initialize communication mechanism
  int  mbmsrv_init_comm(ServerBMID bmid);
  /// Request dispatching in blocking mode
  int  mbmsrv_dispatch_blocking(ServerBMID bmid);
  /// Request dispatching in non-blocking mode (seperate thread)
  int  mbmsrv_dispatch_nonblocking(ServerBMID bmid);
  /// Stop dispatching
  int  mbmsrv_stop_dispatch(ServerBMID bmid);
  /// Wait for dispatch thread having finished.
  int  mbmsrv_wait_dispatch(ServerBMID bmid);
  /// Handle single server request
  int  mbmsrv_handle_request(ServerBMID bm, void* connection, MBMMessage& msg);
  /// Check pending tasks
  int  mbmsrv_check_pending_tasks(ServerBMID bm);
  /// Check existing clients with cleanup 
  void mbmsrv_check_clients(ServerBMID bm);
  /// Call mbmsrv_check_pending_tasks and mbmsrv_check_clients if necessary
  int  mbmsrv_client_watch_cycle(ServerBMID bm);

  typedef void (*mbmsrv_accident_callback_t)(void* param, 
					     ServerBMID bmid, 
					     long offset, 
					     size_t len, 
					     int typ,
					     const unsigned int mask[],
					     const char* user);
  /// Subscribe to 'accident' events (client crashes etc.)
  int  mbmsrv_subscribe_accidents(ServerBMID bm, void* param, mbmsrv_accident_callback_t callback);
  /// Unsubscribe from 'accident' events (client crashes etc.)
  int  mbmsrv_unsubscribe_accidents(ServerBMID bm);
}
#endif // _MBM_MBMSERVER_H
