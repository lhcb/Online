//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
#ifndef _MBM_MBM_STRUCTS_H
#define _MBM_MBM_STRUCTS_H

#define MBM_IMPLEMENTATION

/// Framework includes
#include <RTL/bits.h>
#include <RTL/que.h>
#include <MBM/bmdef.h>
#include <MBM/bmconnection.h>

struct SHMCOMM_gbl;
struct MBMMessage;
struct ServerBMID_t;
struct BufferMemory;
struct MBMCommunication;

typedef struct lib_rtl_gbl* lib_rtl_gbl_t;
typedef Bits::BitMask<BM_MASK_SIZE> TriggerMask;
typedef Bits::BitMask<BM_MASK_SIZE> UserMask;
typedef Bits::BitMask<BM_MASK_SIZE> VetoMask;

namespace MBM  {
  enum BufferStates   {
    S_nil    = -1,
    S_cancelled = 13,
    S_active = 11,
    S_pause  = 10,
    S_wspace =  9,
    S_wevent =  8,
    S_weslot =  7,
    S_wspace_ast_ready   = 12,
    S_wspace_ast_queued  =  6,
    S_wevent_ast_queued  =  5,
    S_weslot_ast_queued  =  4,
    S_wspace_ast_handled =  3,
    S_wevent_ast_handled =  2,
    S_weslot_ast_handled =  1
  };

  enum EVTIDs {
    EVTID_NONE = -1
  };

  enum BIDs {
    BID_USER  = 0xfeed0001,
    BID_EVENT = 0xfeed0002
  };

}

#include <RTL/Pack.h>

struct REQ  {     //requirements structures xx bytes each   
  TriggerMask   tr_mask PACKED_DATA;     // event Mask
  TriggerMask   vt_mask;                 // veto mask
  int           ev_type;                 // event type
  int           masktype;                // requirement type for trigger mask   
  int           user_type;               // Requirement Type (VIP/normal User)   
  int           user_type_one;           // Requirement ONE Type
  int           freqmode;                // requirement mode       
  float         freq;                    // Frequency limit       
};

template <class T> struct bm_iterator;

struct USER : public qentry_t  {
  qentry_t wsnext;
  qentry_t wenext;
  qentry_t wesnext;
  unsigned int magic;              // Magic word to check consistency
  unsigned int block_id;           // Slot magic word  
  int   busy;                      // slot busy flag       
  int   uid;                       // BM user id       
  int   serverid;                  // Server thread this clients communicates with
  int   state;                     // consumer/producer state (Active,Pause) - (Active,Wspace)
  int   partid;                    // user partition ID
  int   pid;                       // process id
  int   held_eid;                  // held event index
  int   ev_produced;               // events produced counter
  int   ev_actual;                 // events matching req
  int   ev_seen;                   // # of events seen
  int   ev_freed;                  // # of events freed
  int   n_req;                     // number of requierements
  int   get_ev_calls;
  int   get_sp_calls;
  int   free_calls;
  int   alloc_calls;               // 18th. int

  TriggerMask ev_trmask;           // trmask of waiting exent
  MBMConnection connection;        // File descriptor of the answer connection

  char  name[BM_USER_NAME_LEN];    // user name
  char  ev_dest[BM_USER_NAME_LEN]; // If event was sent with destination, this is the target user
  int   ev_type;                   // evtype of waiting event
  char  _spare[4];                 // _padding
  long  ev_ptr;                    // pointer of waiting space/event
  long  ev_size;                   // size of waiting event
  long  space_add;                 // address of allocated space    
  long  space_size;                // size in bytes

  REQ   req[BM_MAX_REQS];          // BM_MAX_REQS requirement maximum     
  enum { BID = MBM::BID_USER };
  typedef bm_iterator<USER> iterator;
};

struct USERDesc : public qentry_t  { // active consumers
  qentry_t wev_head;      // consumers waiting events
  qentry_t wsp_head;      // producers waiting space
  qentry_t wes_head;      // producers waiting event slots
  USER   users[1];
};

struct EVENT : public qentry_t {
  unsigned int block_id;  //  0: Block identifier
  int      busy;          //  4: event busy flag
  int      eid;           //  8: event ID
  int      partid;        // 12: Partition ID of the event
  int      ev_type;       // 16: Event type
  long     ev_add;        // 20: Event pointer
  long     count;         // 28: Event identifier
  long     ev_size;       // 36: Event size
  UserMask umask0;        // Mask of privilidged consumers
  UserMask umask1;        // Mask of non-privilidged consumers
  UserMask held_mask;     // Mask of users holding the event
  UserMask one_mask[BM_MAX_REQONE];  // Masks of privilidged one-only consumers
  UserMask one_user[BM_MAX_REQONE];  // Masks of non-privilidged one-only consumers
  TriggerMask tr_mask;    // Trigger Mask
  char spare[12];         // Padding to get to next 32 byte boundary
  enum { BID = MBM::BID_EVENT };
  typedef bm_iterator<EVENT> iterator;
};

struct EVENTDesc : public qentry_t  {    // general event queue
  char     _pad[8];
  EVENT    events[1];
};

struct CONTROL    {
  typedef long long int i_huge;
  int    version;            // Buffer manager version
  int    p_umax;             // maximum users
  int    p_emax;             // maximum events
  int    p_tmax;             // maximum threads
  int    p_base;             // Memory base address
  int    shift_p_Bit;        // Shifts per Bit to obtain size in bytes
  int    bytes_p_Bit;        // Number of bytes per bit in bitmap.
  int    last_bit;           // last bit on the bitmap
  int    wait_event_count;   // Number of clients in wait event
  int    wait_space_count;   // Number of clients in wait space
  int    wait_slot_count;    // Number of clients in wait slot
  long   last_alloc;         // Byte Offset of last allocation
  long   buff_size;          // Event Buffer size
  long   bm_size;            // size of bit map in bytes
  long   i_events;           // instantaneous event number
  long   i_space;            // instantaneous free  space
  long   i_users;            // instantaneous sctive users
  i_huge tot_produced;       // events produced counter
  i_huge tot_actual;         // events matching req
  i_huge tot_seen;           // events seen
  int    reqone_val[BM_MAX_REQONE];
  int    reqone_count[BM_MAX_REQONE];
} PACKED_DATA;

struct BUFFERS  {
  struct BUFF {
    char name[BM_BUFF_NAME_LEN];
    long size;
    int  used;
    int  spare;
  } PACKED_DATA;
  int   p_bmax;           // Maximum number of buffers
  int   nbuffer;          // Current number of buffers
  BUFF  buffers[1];       // Buffer descriptors
};

struct BufferMemory : public qentry_t   {
  unsigned int     magic          { 0xFEEDBABE };
  char*            gbl            { nullptr };
  CONTROL*         ctrl           { nullptr };
  USER*            user           { nullptr };
  USERDesc*        usDesc         { nullptr };
  EVENT*           event          { nullptr };
  EVENTDesc*       evDesc         { nullptr };
  char*            bitmap         { nullptr };
  unsigned long    bitmap_size    { 0 };
  char*            buffer_add     { nullptr };
  unsigned long    buffer_size    { 0 };
  char             bm_name[BM_BUFF_NAME_LEN]  { 0 };
  lib_rtl_gbl_t    gbl_add        { nullptr };
  lib_rtl_gbl_t    buff_add       { nullptr };
  BufferMemory() = default;
  BufferMemory(BufferMemory&& copy) = delete;
  BufferMemory(const BufferMemory& copy) = delete;
  BufferMemory& operator=(BufferMemory&& copy) = delete;
  BufferMemory& operator=(const BufferMemory& copy) = delete;
};

#include <RTL/Unpack.h>

#endif // _MBM_MBM_STRUCTS_H
