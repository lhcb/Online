//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================

#ifndef ONLINEKERNEL_NO_TESTS

// Framework includes
#include <RTL/rtl.h>
#include <RTL/time.h>
#include <NET/defs.h>
#include <CPP/Sensor.h>

// C/C++ include files
#include <vector>
#include <cstring>
#include <cerrno>
#include <string>
#include <system_error>

namespace {

  void help_send() {
    ::printf("test_<type>_net_send -opt [-opt]\n");
    ::printf("    -n<ame>=<process-name>     Sender/own process name     \n");
    ::printf("    -ta<rget>=<process-name>   Receiver's process name     \n");
    ::printf("    -tu<rns>=<number>          Number of receive/send turns\n");
    ::printf("    -l<ength>=<number>         Message length in bytes     \n");
    ::printf("    -b<ounce>                  Run in message bounce mode  \n");
    ::printf("    -th<reeads>=<number>       Number of threads for underlying implementation\n");
  }

  void help_recv() {
    ::printf("test_<type>_net_recv -opt [-opt]\n");
    ::printf("    -b<ounce>                  Run in message bounce mode) \n");
    ::printf("    -n<ame>=<process-name>     Receiver's process name     \n");
    ::printf("    -th<reeads>=<number>       Number of threads for underlying implementation\n");
  }

  struct NetSensor  {
  protected:
    CPP::Sensor m_sensor;
    bool  m_bounce      { false };
    long  m_bytes       { 0 };

  public:
    std::vector<unsigned char> buffer;
    NET*  net           { nullptr };
    long  printFreq     { -1 };
    long  max_message   { -1 };
    long  num_message   {  0 };
    bool  do_run        { true };

    static void handle_data(const netheader_t& hdr, void* param, netentry_t* entry)   {
      NetSensor* s = (NetSensor*)param;
      s->handleMessage(hdr,entry);
    }
    void handleMessage(const netheader_t& hdr, netentry_t* entry)   {
      static struct timeval start;
      int print_freq = (this->printFreq) > 0 ? this->printFreq : (num_message > 50000 ? 10000 : 1000);
      if( 0 == num_message )  {
	::gettimeofday(&start,0);
      }
      if( buffer.size() < hdr.size ) buffer.resize(hdr.size+1024*sizeof(int));
      net_receive(net, entry, buffer.data(), hdr.size);
      m_bytes += hdr.size;
      ++num_message;
      if ( (num_message%print_freq) == 0 )  {
	struct timeval now;
	::gettimeofday(&now,0);
	double diff = double(now.tv_sec-start.tv_sec)+double(now.tv_usec-start.tv_usec)/1e6;
	if ( diff == 0 ) diff = 1;
	double diff2 = double(m_bytes)/diff;
	printf ("%-6d %-4ld %s %6ld messages rate:%9.3f kHz %9.3f MB/sec  total:%7.3f [sec]. [%s]\n",
		*(int*)buffer.data(), time(0)-start.tv_sec, m_bounce ? "Bounced" : "Received",
		num_message, double(num_message)/diff/1000.0, diff2/1024.0/1024.0, diff, hdr.name);
      }
      if( m_bounce )    {
        int sc = net_send(net,buffer.data(),hdr.size,hdr.name,hdr.facility);
        if ( sc != NET_SUCCESS )  {
          ::printf("Failed to send message. ret=%d\n",sc);
        }
      }
      // Somehow we have to exit the ZMQ event loop as well:
      if( max_message > 0 && num_message >= max_message )  {
	handle_stop();
      }
    }
    static void handle_death(const netheader_t& hdr, void* param, netentry_t* /* data */){
      NetSensor* s = (NetSensor*)param;
      ::printf("Task died: %s\n",hdr.name);
      s->handle_stop();
    }
    void handle_stop()   {
      do_run = false;
      ::printf("End of processing reached.\n");
      m_sensor.shutdown();
      m_sensor.exit();
    }
    void run()   {
      m_sensor.run();
    }
    explicit NetSensor(const std::string& proc, int nthreads, bool bounce=false)
      : m_sensor(0, "TRANSFER", false), m_bounce(bounce)
    {
      net_subscribe(net=net_init(proc,nthreads),this,1,handle_data,handle_death);
    }
    virtual ~NetSensor()    {
      net_unsubscribe(net,this,1);
    }
  };

#define Offseto
  template <typename A, typename B> int offset_of(const A* obj, const B* member)  {
    const unsigned char* m = (const unsigned char*)member;
    const unsigned char* o = (const unsigned char*)obj;
    return int(m-o);
  }
}

extern "C" int TRANSFERTEST(print_net_header) (int, char**)   {
  netheader_t * hdr = new netheader_t();
  ::printf("+-------------------------------------------------------------------------+\n");
  ::printf("|        %-60s     |\n",__func__);
  ::printf("| netheader_t: size:%d bytes.\n",int(sizeof(netheader_t)));
  ::printf("| netheader_t: offset 'size':      %-32d\n", offset_of(hdr,&hdr->size) );
  ::printf("| netheader_t: offset 'msg_type':  %-32d\n", offset_of(hdr,&hdr->msg_type) );
  ::printf("| netheader_t: offset 'facility':  %-32d\n", offset_of(hdr,&hdr->facility) );
  ::printf("| netheader_t: offset 'magic':     %-32d\n", offset_of(hdr,&hdr->magic) );
  ::printf("| netheader_t: offset 'hash':      %-32d\n", offset_of(hdr,&hdr->hash) );
  ::printf("| netheader_t: offset 'name':      %-32d\n", offset_of(hdr,hdr->name) );
  ::printf("+-------------------------------------------------------------------------+\n");
  ::lib_rtl_sleep(999999);
  return 0;
}

extern "C" int TRANSFERTEST(send) (int argc, char **argv)  {
  RTL::CLI cli(argc, argv, help_send);
  std::string target, name, proc, to;
  int max_restart = 5;

 RestartFromScratch:
  int pfreq = -1, batch=0, count=1, length=256, loop=100000, num_thread=1;
  int bounce = 0, debug = 0;
  cli.getopt("print_freq", 1, pfreq);
  cli.getopt("count",   1, count);
  cli.getopt("length",  1, length);
  cli.getopt("turns",   2, loop);
  cli.getopt("target",  2, target);
  cli.getopt("name",    1, name);
  cli.getopt("debug",   5, debug);
  cli.getopt("threads", 2, num_thread);
  batch = cli.getopt("batch",3) != 0;
  bounce = cli.getopt("bounce",1) != 0 ? 1 : 0;
  
  if ( length <= 0 ) length = 10;
  count = bounce ? 1 : count;
  proc = name.empty() ? RTL::nodeName()+"::SND_0" : name;
  to   = target.empty() ? RTL::nodeName()+"::RCV_0" : target;
  if ( (debug&2) )  {
    ::lib_rtl_wait_debugger(true);
  }
  ::printf ("Test: %s\n",__FILE__);
  ::printf ("+-------------------------------------------------------------------------+\n");
  ::printf ("|        %-60s     |\n",__func__);
  ::printf ("+-------------------------------------------------------------------------+\n");
  ::printf (" Starting net sender: msg-len:%d turns:%d name:%s target:%s PID:%d PrintFreq:%d\n",
	    length, loop, proc.c_str(), to.c_str(), ::lib_rtl_pid(), pfreq);

  try   {
    long num_bytes = 0;
    int print_freq = pfreq>0 ? pfreq : (loop*count > 100000 ? 10000 : 1000);
    while(count-- > 0)  {
      NetSensor cl(proc,num_thread,bounce);
      cl.printFreq = print_freq;
      cl.buffer.resize(length);
      unsigned long num_consecutive_errors = 0;
      unsigned char *wmessage = cl.buffer.data();
      for (std::size_t k = 0; k < length-sizeof(int); k++)
	wmessage[k+sizeof(int)] = char((length + k) & 0xFF);

      // receive some messages and bounce them
      struct timeval start;
      ::gettimeofday(&start,0);
      for (int i=0, mx=loop; mx > 0; --mx, ++i)  {
	*(int*)wmessage = i;
	int sc = net_send(cl.net, wmessage, length, to, 1);
	if (sc != NET_SUCCESS)   {
	  std::error_code err = std::make_error_code(std::errc(errno));
	  ::printf("Client::send Failed: Error=%d errno=%d [%s]\n",
		   sc, errno, err.message().c_str());
	  switch(err.value())  {
	  case EHOSTDOWN:
	  case EHOSTUNREACH:
	  case ENETDOWN:
	  case ENETUNREACH:
	  case ENETRESET:
	  case ECONNABORTED:
	  case ECONNRESET:
	  case ENOTCONN:
	  case ESHUTDOWN:
	    if ( max_restart-- > 0 )  {
	      ::lib_rtl_sleep(3000);
	      goto RestartFromScratch;
	    }
	    ::printf("Client::send FATAL error. Test exiting.");
	    return errno;
	  }
	  ++num_consecutive_errors;
	  ::lib_rtl_sleep(500);
	}
	else   {
	  num_consecutive_errors = 0;
	}
	if ( num_consecutive_errors > 20 )   {
	  ::printf("[FATAL] Client::send FATAL error. Too many consecutive errors.");
	  return ENOTCONN;
	}
	num_bytes += length;
	if ( ((i+1) % print_freq) == 0 )  {
	  struct timeval now;
	  ::gettimeofday(&now,0);
	  double diff = double(now.tv_sec-start.tv_sec)+double(now.tv_usec-start.tv_usec)/1e6;
	  if ( diff == 0 ) diff = 1;
	  double diff2 = double(num_bytes)/diff;
	  ::printf("%-4ld Sent %8d messages rate:%8.3f kHz %7.2f MB/sec  total:%6.2f sec\n",
		   time(0)-start.tv_sec,i+1,double(i+1)/diff/1000.0,diff2/1024.0/1024.0,diff);
	}
      }
      if ( !batch && bounce )  {
	while(1)  {
	  ::lib_rtl_sleep(100);
	}
      }
    }
    if ( batch )   {
      ::lib_rtl_sleep(1000);
    }
    else {
      printf("Hit key+Enter to exit ...");
      getchar();
    }
  }
  catch ( const std::exception& e )   {
    ::printf("[FATAL] Exception thrown by application %s: %s\n", __func__, e.what());
  }
  return 0x0;
}

extern "C" int TRANSFERTEST(recv) (int argc, char **argv)  {
  RTL::CLI cli(argc, argv, help_recv);
  std::string proc;
  int  print_freq = -1, turns = -1;
  int  debug=0, num_thread = 3, bounce = cli.getopt("bounce",1) != 0;
  cli.getopt("print_freq",  8, print_freq);
  cli.getopt("threads", 2, num_thread);
  cli.getopt("turns",   1, turns);
  cli.getopt("name",    1, proc);
  cli.getopt("debug",   5, debug);
  if (proc.empty() ) proc = RTL::nodeName()+"::RCV_0";
  ::printf ("Test: %s\n",__FILE__);

  ::printf ("+-------------------------------------------------------------------------+\n");
  ::printf ("|        %-60s     |\n",__func__);
  ::printf ("+-------------------------------------------------------------------------+\n");
  ::printf (" Starting transfer receiver:%s. Bounce:%s name:%s PID:%d PrintFreq:%d\n",
	    proc.c_str(), bounce ? "true" : "false", proc.c_str(), ::lib_rtl_pid(), print_freq);

  if ( (debug&1) )  {
    ::lib_rtl_wait_debugger(true);
  }
  
  try   {
    NetSensor cl(proc, num_thread, bounce);
    cl.printFreq = print_freq;
    cl.max_message = turns;
    cl.run();
  }
  catch ( const std::exception& e )   {
    ::printf("[FATAL] Exception thrown by application %s: %s\n", __func__, e.what());
  }
  return 0x0;
}
#endif
