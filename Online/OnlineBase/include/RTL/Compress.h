//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : Storage
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_STORAGE_COMPRESS_H
#define ONLINE_STORAGE_COMPRESS_H

// Framework include files

// C/C++ include files
#include <vector>
#include <string>

/// Online namespace declaration
namespace Online   {

  /// Forward declarations
  class Datafile;
  class posix_t;
  
  /// Namespace for the compression utilities
  namespace compress   {

    /// Available posix data access descriptor for zlib encoding
    posix_t* descriptor_zlib();
    /// Available posix data access descriptor for zstd encoding
    posix_t* descriptor_zstd();
    /// Available posix data access descriptor for lzma encoding
    posix_t* descriptor_lzma();
    /// Available posix data access descriptor for lz4 encoding
    posix_t* descriptor_lz4();

    std::string lzma_error_message(unsigned long code);
    
    enum compression_levels  {
      COMPRESSION_LEVEL_DEFAULT = 100,
      COMPRESSION_LEVEL_FAST    = 200,
      COMPRESSION_LEVEL_MEDIUM  = 300,
      COMPRESSION_LEVEL_BEST    = 400
    };
    enum compression_strategies  {
      COMPRESSION_STRATEGY_DEFAULT = 101
    };

    /// Convert string data buffer to vector
    std::vector<unsigned char> to_vector(const char* data);

    /// Convert string data buffer to vector
    std::vector<unsigned char> to_vector(const std::string& data);

    /// Inflate the data byte buffer using zstd
    std::vector<unsigned char> decompress_zstd(const unsigned char* data, std::size_t len);

    /// Inflate the data byte buffer using zstd
    std::vector<unsigned char> decompress_zstd(const std::vector<unsigned char>& data);

    /// Inflate the data byte buffer using zstd
    std::vector<unsigned char> decompress_zstd(std::vector<unsigned char>&& data);

    /// Inflate the data byte buffer using lzma
    std::vector<unsigned char> decompress_lzma(const unsigned char* data, std::size_t len);

    /// Inflate the data byte buffer using lzma
    std::vector<unsigned char> decompress_lzma(const std::vector<unsigned char>& data);

    /// Inflate the data byte buffer using lzma
    std::vector<unsigned char> decompress_lzma(std::vector<unsigned char>&& data);

    /// Inflate the data byte buffer using lz4
    std::vector<unsigned char> decompress_lz4(const unsigned char* data, std::size_t len);

    /// Inflate the data byte buffer using lz4
    std::vector<unsigned char> decompress_lz4(const std::vector<unsigned char>& data);

    /// Inflate the data byte buffer using lz4
    std::vector<unsigned char> decompress_lz4(std::vector<unsigned char>&& data);

    /// Inflate the data byte buffer using lz4
    std::vector<unsigned char> decompress_zlib(const unsigned char* data, std::size_t len);

    /// Inflate the data byte buffer using gzip
    std::vector<unsigned char> decompress_zlib(const std::vector<unsigned char>& data);

    /// Inflate the data byte buffer using gzip
    std::vector<unsigned char> decompress_zlib(std::vector<unsigned char>&& data);

    /// decompress a data byte buffer according to encoding specified
    std::vector<unsigned char> decompress(const std::string& encoding, 
					  const std::vector<unsigned char>& data);
    
    /// Inflate the data byte buffer according to encoding specified
    std::vector<unsigned char> decompress(const std::string&           encoding, 
					  std::vector<unsigned char>&& data);
    
    /// Inflate the data byte buffer according to encoding specified
    std::vector<unsigned char> decompress(const std::string& encoding, 
					  const void*        data,
					  std::size_t        data_len);
    
    /// Inflate the data byte buffer according to encoding specified
    std::vector<unsigned char> decompress(const std::string&   encoding, 
					  const unsigned char* data,
					  std::size_t          data_len);

    /// decompress a byte buffer: Encoding is interpreted from the payload data
    std::vector<unsigned char> decompress(std::vector<unsigned char>&& data);

    /// decompress a byte buffer: Encoding is interpreted from the payload data
    std::vector<unsigned char> decompress(const unsigned char* data, std::size_t len);

    /// Deflate the data byte buffer using zstd
    std::vector<unsigned char> compress_zstd(const unsigned char* data,
					     std::size_t len,
					     int level    = COMPRESSION_LEVEL_DEFAULT,
					     int strategy = COMPRESSION_STRATEGY_DEFAULT);

    /// Deflate the data byte buffer using lzma
    std::vector<unsigned char> compress_lzma(const unsigned char* data,
					     std::size_t len,
					     int level    = COMPRESSION_LEVEL_DEFAULT,
					     int strategy = COMPRESSION_STRATEGY_DEFAULT);

    /// Deflate the data byte buffer using lzma
    std::vector<unsigned char> compress_lz4(const unsigned char* data,
					    std::size_t len,
					    int level    = COMPRESSION_LEVEL_DEFAULT,
					    int strategy = COMPRESSION_STRATEGY_DEFAULT);

    /// Deflate the data byte buffer using gzip
    std::vector<unsigned char> compress_zlib(const unsigned char* data,
					     std::size_t len,
					     int level    = COMPRESSION_LEVEL_DEFAULT,
					     int strategy = COMPRESSION_STRATEGY_DEFAULT);
    /// gzip compress a byte buffer with default settings
    std::vector<unsigned char> compress_zlib(const std::vector<unsigned char>& data);
    
    /// compress a byte buffer
    std::vector<unsigned char> compress(const std::string& accepted_encoding, 
					const std::vector<unsigned char>& data,
					std::string& used_encoding);

    /// Encode byte array using base64 encoding
    std::string base64_encode(const unsigned char* bytes_to_encode, unsigned int in_len);

    /// Decode base64 encoded string to byte array
    std::vector<unsigned char> base64_decode(const std::string& encoded_string);
#define compress_gzip compress_zlib
#define decompress_gzip decompress_zlib

    int init_data_reading(Datafile& file);
    
  }     // End namespace compress
}       // End namespace Online
//
//
//
extern "C"  {
  /// Available posix data access descriptor for zlib encoding
  Online::posix_t* posix_descriptor_zlib();
  /// Available posix data access descriptor for zstd encoding
  Online::posix_t* posix_descriptor_zstd();
  /// Available posix data access descriptor for lzma encoding
  Online::posix_t* posix_descriptor_lzma();
  /// Available posix data access descriptor for lz4 encoding
  Online::posix_t* posix_descriptor_lz4();
}
#endif  // ONLINE_STORAGE_COMPRESS_H
