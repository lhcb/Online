//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Project    : Online
//
//  Author     : M.Frank
//
//==========================================================================
#ifndef ONLINE_ONLINEBASE_RTL_DATACOMPRESSOR_H
#define ONLINE_ONLINEBASE_RTL_DATACOMPRESSOR_H 1

/// Framework include files
#include <RTL/Datafile.h>
#include <CPP/mem_buff.h>

/// C/C++ include files
#include <fcntl.h>

/// Online namespace declaration
namespace Online   {

  /// Forward declarations
  class posix_t;
  
  /// Namespace for the compression utilities
  namespace compress   {

    constexpr static std::size_t kBYTE = 1024;
    constexpr static std::size_t MBYTE = 1024*1024;
    constexpr static std::size_t DEFAULT_BUFFER_SIZE = 3*MBYTE;
    constexpr static std::size_t DEFAULT_WRITE_SIZE  = 5*MBYTE;

    /// Generic class to read/write files using posix like interface
    /** Generic posix like data (de-)compressor
     *
     *  Please not: 
     *  - seek calls are difficult to implement if the go beyond the
     *    currently stored memory buffer. They are not reliable.
     *  - file access is by nature ALWAYS buffered.
     *    The buffer size can be steered by the option 
     *    - "write_size" to set the intermediate buffer before flushing to disk
     *    - "read_size" to set the input read size from file to feed the
     *      decompressed data buffer.
     *
     *  \author  M.Frank
     *  \version 1.0
     */
    class DataCompressor {
    public:
      enum { NONE=0, WRITING=1, READING=2 };
      using mem_buff_t = Online::mem_buff;
      using file_t     = Online::Datafile;
      
      /// Datafile instance used
      file_t file                   {   };
      /// Name of stream type for debugging output
      std::string    stream_type    {   };
      /// Property: size of disk-read buffer
      uint64_t       in_size        { DEFAULT_BUFFER_SIZE };
      /// Property: size of disk write buffer
      uint64_t       out_limit      { DEFAULT_WRITE_SIZE  };
      /// Property: size of work buffer
      uint64_t       tmp_size       { DEFAULT_BUFFER_SIZE };

      /// Current write/read pointer in I/O buffer
      uint64_t       position       { 0 };
      
      /// Number of uncompressed bytes loaded
      uint64_t       in_total       { 0 };
      /// Number of uncompressed bytes at last disk-read action
      uint64_t       in_last_read   { 0 };
      /// Number of compressed bytes read from file
      uint64_t       in_disk        { 0 };

      /// Number of uncompressed bytes processed (not necessarily flushed)
      uint64_t       out_total      { 0 };
      /// Number of uncompressed bytes written/flushed to disk
      uint64_t       out_last_write { 0 };
      /// Number of compressed bytes flushed to disk
      uint64_t       out_disk       { 0 };

      /// Input buffer for data before compression
      mem_buff_t     in_buffer      {  };
      /// Output buffer: on write holds compressed data, in read holds uncompressed data
      mem_buff_t     out_buffer     {  };
      /// Temporary conversion buffer for (de-)compresssion
      mem_buff_t     tmp_buffer     {  };

      /// Flag set to ON during close
      unsigned       finished:1     { 0 };
      /// Flag set to ON after specific options got applied
      unsigned       applied:1      { 0 };
      /// Spare flags
      unsigned       spare:31       { 0 };
    
    public:
      /// Default constructor
      DataCompressor(const std::string& typ);
      /// Move constructor
      DataCompressor(DataCompressor&& copy) = delete;
      /// Move assignemtn
      DataCompressor& operator=(DataCompressor&& copy) = delete;
      /// Disable copy constructor
      DataCompressor(const DataCompressor& copy) = delete;
      /// Disable copy assignment
      DataCompressor& operator=(const DataCompressor& copy) = delete;
      /// Default destructor
      virtual ~DataCompressor() = default;
      /// Access decompressor based on file descriptor
      static DataCompressor* get(int fd);

      /// Access compression algorithm name
      const char* type()  const  {  return stream_type.c_str(); }
      /// Access compression stream name (file-name)
      const char* name()  const  {  return this->file.cname();  }

      /// Helper: parse digit from string
      std::pair<bool, int> parse_digit( const std::string& val );
      /// Helper: printouts
      std::size_t     print( const char* format, ... );
      /// Helper: printouts
      static  std::size_t print (int debug, const char* format, ...);
      /// Helper: error printouts
      static  std::size_t error (const char* format, ...);

      /** Generic posix-file like entries */
      /// Open a new compressor stream (read/write)
      virtual int     open( const char* path, int flags, va_list& args );
      /// Attach objects and initializ specifics
      virtual int     attach( );
      /// Close and finalize compressor stream (read/write)
      virtual int     close ( );
      /// Seek interface: limited functionality, only valid on current memory buffer
      virtual off64_t seek  ( off64_t offset, int whence );

      /// Read data from disk (on the fly decompress, if decompression buffer is empty)
      virtual ssize_t read  (  void* ptr, size_t size );

      /// Write data to disk (on the fly compress if out_limit is reached)
      virtual ssize_t write ( const void* ptr, size_t size );
      /// Append compressed data to output buffer before being flushed to disk
      void            append_output( const uint8_t* buff, std::size_t len );
      /// Flush compressed data to disk
      virtual int     flush ( std::size_t minimal_buffer_length );

      /// Technology specific error printouts
      virtual int     handle_error(uint64_t code) = 0;
      /// Set technology specific options to decompressor
      virtual int     set_option(const std::string& option, const std::string& value);

      /** File reading entries  */
      /// File reading interface: initialize reading
      virtual int     begin_reading() = 0;
      /// File reading interface: finalize reading
      virtual void    end_reading()  { }
      /// File reading interface: decompress data chunk
      virtual int64_t decompress(uint8_t* buff, int64_t len) = 0;

      /** File writing entries  */
      /// File writing interface: initialize writing
      virtual int     begin_writing() = 0;
      /// File writing interface: finalize writing
      virtual void    end_writing()  { }
      /// File writing interface: compress data chunk
      virtual int64_t compress  (const uint8_t* buffer, std::size_t len) = 0;

      static  int         open  ( std::unique_ptr<DataCompressor>&& stream,
			          const char* path, int flags, va_list& args );
      static  int         close ( int fd );
      static  ssize_t     read  ( int fd, void* ptr, std::size_t size );
      static  ssize_t     write ( int fd, const void* ptr, std::size_t size );
      static  off64_t     seek  ( int fd, off64_t offset, int whence );
      static  int         set_option(int fd, const char* option, const char* value );

      /** POSIX signature function calls  */
      /// Posix interface: close
      static  int     posix_close  ( int fd );
      /// Posix interface: access
      static  int     posix_access ( const char* nam, int mode );
      /// Posix interface: unlink
      static  int     posix_unlink ( const char* name );
      /// Posix interface: write data to file
      static  ssize_t posix_write  ( int fd, const void* ptr, size_t size );
      /// Posix interface: read data from file
      static  ssize_t posix_read   ( int fd, void* ptr, size_t size );
      /// Posix interface: lseek64 (limited)
      static  int64_t posix_lseek64( int fd, int64_t offset, int how );
      /// Posix interface: lseek (limited)
      static  long    posix_lseek  ( int fd, long offset, int how );
      /// Posix interface: fstat   (limited: returns values of disk based file)
      static  int     posix_fstat  ( int fd, struct stat* statbuf );
      /// Posix interface: fstat64 (limited: returns values of disk based file)
      static  int     posix_fstat64( int fd, struct stat64* statbuf );
      /// FILE interface: fopen
      static  FILE*   posix_fopen  ( const char* path, const char* mode);
      /// FILE interface: fclose
      static  int     posix_fclose ( FILE* file );
      /// FILE interface: fileno
      static  int     posix_fileno ( FILE* file );
      /// FILE interface: ftell
      static  long    posix_ftell  ( FILE* stream );
      /// FILE interface: fseek (limited)
      static  int     posix_fseek  ( FILE* file, int64_t offset, int how );
      /// FILE interface: fread (on the fly decompress, if decompression buffer is empty)
      static  size_t  posix_fread  ( void* ptr, size_t size, size_t nitems, FILE* file );
      /// FILE interface: fwrite (on the fly compress if out_limit is reached)
      static  size_t  posix_fwrite ( const void* ptr, size_t size, size_t nitems, FILE* file );
      /// FILE interface: stat     (limited: returns values of disk based file)
      static  int     posix_stat   ( const char* path, struct stat* statbuf );
      /// FILE interface: stat64   (limited: returns values of disk based file)
      static  int     posix_stat64 ( const char* path, struct stat64* statbuf );

      static  int     init_descriptor(Online::posix_t& p);
    };

    template <typename STREAM> class concrete_compressor : public DataCompressor {
    public:
      /// Technology specific data
      STREAM stream  { };

    public:
      /// Constructors & Co.
      using DataCompressor::DataCompressor;

      /// Specific overlay to open concrete decompressor
      static  int posix_open( const char* path, int flags, ... );
      /// Set technology specific options to decompressor
      int     set_option(const std::string& option, const std::string& value)  override;
      /// Technology specific error printouts
      int     handle_error(uint64_t code)  override;
      
      /// File reading interface: initialize reading
      int     begin_reading()  override;
      /// File reading interface: finalize reading
      void    end_reading()  override;
      /// File reading interface: decompress data chunk
      int64_t decompress(uint8_t* buff, int64_t len)  override;
      
      /// File writing interface: initialize writing
      int     begin_writing()  override;
      /// File writing interface: finalize writing
      void    end_writing()  override;
      /// File writing interface: compress data chunk
      int64_t compress  (const uint8_t* buffer, std::size_t len)  override;
    };
  }     // End namespace compress
}       // End namespace Online
#endif  // ONLINE_ONLINEBASE_RTL_DATACOMPRESSOR_H

