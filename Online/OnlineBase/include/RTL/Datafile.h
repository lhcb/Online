//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  Datafile.h
//--------------------------------------------------------------------------
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINEBASE_CPP_DATAFILE_H
#define ONLINEBASE_CPP_DATAFILE_H

/// C/C++ include files
#include <string>
#include <cstdint>
#include <filesystem>

/// Forward declarations

/// Online namespace declaration
namespace Online  {

  /// Forward declarations
  class posix_t;
  
  /// Wrapper class to manager data files
  /**
   *  \author  M.Frank
   *  \version 1.0
   */
  class Datafile   {

  public:
    enum { NONE=0, WRITING=1, READING=2, MAPPED=4 };
    /// Reuse-flag
    unsigned       reuse:1        {   false };
    /// Debug flag
    unsigned       debug:1        {   false };
    /// Mode: NONE, WRITING or READING
    unsigned       mode:4         {   NONE  };
    /// Open flags
    unsigned       flags:16       {       0 };
    /// User flags
    unsigned       user:8         {       0 };
    /// Spare flags
    unsigned       spare:2        {       0 };
    /// File handle
    int            fd             {      -1 };
    /// File name
    std::string    path           {       0 };

  protected:
    /// Reference to implementing descriptor
    const posix_t* m_descriptor   { nullptr };

    /// Memory mapping layout (if used)
    struct memory_mapping_t   {
      /// Mapping address (if file is memory mapped)
      unsigned char* begin = nullptr;
      /// End of mapped memory region (if file is memory mapped)
      unsigned char* end   = nullptr;
      /// Current memory pointer (if file is memory mapped)
      unsigned char* ptr   = nullptr;
    };
    memory_mapping_t m_mem_map;    

  public:
    /// Default constructor
    Datafile();
    /// Initializing constructor
    Datafile(const posix_t* posix_descriptor);
    /// Copy constructor
    Datafile(Datafile&& copy) = default;
    /// Copy constructor
    Datafile(const Datafile& copy) = default;
    /// Initializing constructor
    Datafile(const std::string& nam);
    /// Initializing constructor
    Datafile(const std::string& nam, const posix_t* posix_descriptor);
    /// Initializing constructor
    Datafile(int descriptor);
    /// Initializing constructor
    Datafile(int descriptor, bool);
    /// Initializing constructor
    Datafile(int descriptor, const posix_t* posix_descriptor);
    /// Initializing constructor
    Datafile(const std::string& nam, int descriptor, const posix_t* posix_descriptor);

    /// Default destructor
    virtual ~Datafile() = default;
    /// Assignment
    Datafile& operator=(Datafile&& copy) = default;
    /// Assignment
    Datafile& operator=(const Datafile& copy) = default;

    /// Reset file properties and adopt properties of another file
    int reset(Datafile& other);
    /// Set option to specific implemetation of the descriptor
    int set_option(const std::string& what, const std::string& value);
    /// Access file name
    const std::string& name() const          {  return this->path;            }
    /// Access file name
    const char* cname()  const               {  return this->path.c_str();    }
    /// Set file name. Only allowed before the file is opened
    void setName(const std::string& value)   {  this->path = value;           }

    /// Access file path as filesystem entity
    std::filesystem::path fspath() const;
    /// Access filename part of file path
    std::filesystem::path filename() const;
    /// Access filename extension part of file path
    std::filesystem::path extension() const;
    /// Access parent part of file path
    std::filesystem::path directory() const;
    /// Last modification/write time
    std::filesystem::file_time_type last_write_time()  const;
    /// File system entry status
    std::filesystem::file_status status() const;
    /// File system entry type (regular file, symlink, etc)
    std::filesystem::file_type type() const;
    /// Check if file exists
    bool exists() const;
    /// Check if file is empty
    bool is_empty() const;

    /// Access file descriptor
    int fileno()  const                      {  return this->fd;              }
    /// Check if file is connected
    bool isOpen()  const                     {  return this->fd > 0;          }
    /// Check if the file is mapped
    bool isMapped()  const                   {  return m_mem_map.begin != m_mem_map.end;  }
    /// Acces the mapped file size
    std::size_t mapped_size()  const         {  return m_mem_map.end - m_mem_map.begin;   }
    /// Access the data size: file size of mapped size
    std::size_t data_size()  const;

    /// Start of the mapped data size
    const unsigned char* begin()  const      {  return m_mem_map.begin;           }
    /// End of the mapped data size
    const unsigned char* end()  const        {  return m_mem_map.end;             }
    /// Current pointer in the mapped data space
    const unsigned char* pointer()  const    {  return m_mem_map.ptr;             }
    /// Advance the pointer in the mapped size
    const unsigned char* advance(long bytes) {  return (m_mem_map.ptr += bytes);  }
    /// Set re-use flag
    void setReuse(bool value)                {  this->reuse = value ? 1 : 0;      }
    /// Return error message in case of failure
    static std::string error_message(int error);
    /// Return error message in case of failure
    std::string error_message() const;
    /// Seek to required 
    long seek(long offset, int whence);
    /// Read chunk of data
    long read(void* buffer, long len);
    /// Write chunk of data
    long write(const void* data, int len);
    /// Write chunk of data
    long write(const void* data, long len);
    /// Write chunk of data
    long write(const void* data, std::size_t len);
    /// Map memory of the file
    int  map_memory(const void* data, std::size_t len);
    /// Unmap memory of the file
    int  unmap_memory();
    /// Open file in read mode
    int  open(bool silent=true, int user_flags=0);
    /// Open file in read or write mode
    int  open(bool silent, const char* path, int flags, int mode);
    /// Open file in read mode with mapping
    int  openMapped(bool silent=true, int user_flags=0);
    /// Open file in write mode
    int  openWrite(bool silent=true, int user_flags=0, int user_mode=0);
    /// Check file access
    int  access(int mode);
    /// Unlink file
    int  unlink();
    /// Return file position
    long position()  const;
    /// Set absolute file position
    long position(long offset, bool absolute=true);
    // Close file descriptor if open
    int  close();
    /// Reset information (close and reset name)
    int  reset(bool close=true);
    /// Create a directory structure
    static int  mkdir(const char* dir_name, int protection, bool silent=true);
    /// Create a directory structure
    static int  mkdir(const std::string& dir_name, int protection, bool silent=true);
    /// Delete a directory structure
    static int  rmdir(const char* dir_name, bool recursive, bool silent=true);
    /// Delete a directory structure
    static int  rmdir(const std::string& dir_name, bool recursive, bool silent=true);
  };
}
#endif /* ONLINEBASE_CPP_DATAFILE_H  */
