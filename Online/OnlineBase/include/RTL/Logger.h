//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
#ifndef ONLINEBASE_RTL_LOGGER_H
#define ONLINEBASE_RTL_LOGGER_H

/// Framework include files
#include "RTL/rtl.h"

/// C/C++ include files
#include <map>
#include <mutex>
#include <string>
#include <memory>
#include <cstdarg>

/// RTL namespace declaration
namespace RTL   {

  /// Basic output logger. Clonable
  /** @class Logger
   *
   * @author M.Frank
   */
  class Logger   {
  public:

    /// Logger initialization helper
    /**
     * @author M.Frank
     */
    struct log_args  {
    public:
      std::string fmt {"%-8LEVEL %-20SOURCE"};
      int  level      {-1};
      bool debug      {false};
      log_args() = default;
      log_args(int lvl) : level(lvl) {}
      log_args(const char* f) : fmt(f) {}
    };

    /// Buffering mode enumeration
    enum buffering_modes  {
      NO_BUFFERING   = 1,
      LINE_BUFFERING = 2,
      FULL_BUFFERING = 4
    };

    /// Basic output logger device. Prints to stdout
    /** @class Logger::LogDevice
     *
     * @author M.Frank
     */
    class LogDevice   {
    public:
      enum format_items {
	TIME_FORMAT    = 1,
	NODE_FORMAT    = 2,
	LEVEL_FORMAT   = 3,
	PROCESS_FORMAT = 4,
	SOURCE_FORMAT  = 5,
	EXEC_FORMAT    = 99
      };

    public:
      /// Device lock to protect against interleaved printouts
      std::mutex* device_lock {nullptr};
      /// Output format
      std::string format {"%TIME %LEVEL %-16SOURCE"};
      /// The processes executable name
      std::string executable;
      /// Format compilation flags
      std::map<size_t,std::pair<int, std::string> > formatItems;

      /// Extract format item during compilation
      std::pair<size_t,std::string> extract_format(const std::string& tag);

      /// Compile the format string
      virtual void compileFormat(const std::string& fmt);

      /// Format header string according to precompiled format
      virtual size_t formatHeader(char* buffer, size_t buff_len, int severity, const char* source)   const;


    public:
      /// Default constructor
      LogDevice();
      /// Default destructor
      virtual ~LogDevice();

      /// Check if a global output device exists
      static bool existsGlobalDevice();
      /// Set global output device
      static void setGlobalDevice(std::shared_ptr<LogDevice> dev, int level);
      /// Set global output device
      static void setGlobalDevice(std::shared_ptr<LogDevice> dev, const std::string& level);
      /// Access global output device
      static std::shared_ptr<LogDevice> getGlobalDevice();
      /// Install as global logger for RTL
      static void install_rtl_printer(int level);

      /// Set IO buffering mode for stdout, stderr. See "man setvbuf" for details
      /** 
       *  @arg mode       [int, read-only]     Buffering mode. See enum buffering_modes.
       *                                       If > FULL_BUFFERING, size of the internal
       *                                       stream buffer.
       */
      virtual void set_io_buffering(size_t mode);

      /// Calls the display action with a given severity level
      /**
       *  @arg severity   [int,read-only]      Display severity flag (see enum)
       *  @arg source     [string,read-only]   Originator name of the message
       *  @arg msg        [string,read-only]   Formatted message string
       *  @return Status code indicating success or failure
       */
      virtual size_t printmsg(int severity, const char* source, const char* msg)  const;

      /// Calls the display action with ERROR and throws an std::runtime_error exception
      /**
       *  @arg source     [string,read-only]   Originator name of the message
       *  @arg msg        [string,read-only]   Formatted message string
       */
      virtual void exceptmsg(const char* source, const char* msg)  const;

      /// Calls the display action with a given severity level
      /**
       *  @arg severity   [int,read-only]      Display severity flag (see enum)
       *  @arg fmt        [string,read-only]   Format string for ellipsis args
       *  @arg args       [ap_list,read-only]  List with variable number of arguments to fill format string.
       *  @return Status code indicating success or failure
       */
      virtual size_t printout(int severity, const char* source, const char* fmt, va_list& args)  const;

      /// Calls the display action with a given severity level
      /**
       *  @arg severity   [int,read-only]      Display severity flag (see enum)
       *  @arg source     [string,read-only]   Originator name of the message
       *  @arg fmt        [string,read-only]   Format string for arguments
       *  @return Status code indicating success or failure
       */
      virtual size_t printout(int severity, const char* source, const char* fmt, ...)  const;

      /// Calls the display action with ERROR and throws an std::runtime_error exception
      /**
       *  @arg source     [string,read-only]   Originator name of the message
       *  @arg fmt        [string,read-only]   Format string for ellipsis args
       *  @arg args       [ap_list,read-only]  List with variable number of arguments to fill format string.
       *  @return Status code indicating success or failure
       */
      virtual void except(const char* source, const char* fmt, va_list& args)  const;

      /// Calls the display action with ERROR and throws an std::runtime_error exception
      /**
       *  @arg source     [string,read-only]   Originator name of the message
       *  @arg fmt        [string,read-only]   Format string for ellipsis args
       *  @return Status code indicating success or failure
       */
      virtual void except(const char* source, const char* fmt, ...)  const;
    };

    /// Calls the display action with a given severity level
    /**
     *  @arg severity   [int,read-only]      Display severity flag (see enum)
     *  @arg fmt        [string,read-only]   Format string for ellipsis args
     *  @arg args       [ap_list,read-only]  List with variable number of arguments to fill format string.
     *  @return Status code indicating success or failure
     */
    virtual size_t printout(int severity, const char* fmt, va_list& args)  const;

    /// Calls the display action with ERROR and throws an std::runtime_error exception
    /**
     *  @arg fmt        [string,read-only]   Format string for ellipsis args
     *  @arg args       [ap_list,read-only]  List with variable number of arguments to fill format string.
     *  @return Status code indicating success or failure
     */
    virtual void except(const char* fmt, va_list& args)  const;

  public:
    /// Weak reference to the logging device
    std::shared_ptr<LogDevice> device;
    /// Data source name
    std::string source;
    /// Output level for this instance
    int outputLevel = LIB_RTL_WARNING;

  public:

    /// Default constructor
    Logger() = delete;
    /// Initializing constructor
    Logger(std::shared_ptr<LogDevice>& dev, const std::string& source, int level);
    /// Initializing constructor
    Logger(std::shared_ptr<LogDevice>&& dev, const std::string& source, int level);
    /// Standard destructor
    virtual ~Logger() = default;

    /// Clone new instance. Inherits output device
    std::unique_ptr<Logger> clone(const std::string& source, int level);

    /// Set global output device
    static void setGlobalDevice(std::shared_ptr<LogDevice> dev, int level);
    /// Access global output device
    static std::shared_ptr<LogDevice> getGlobalDevice();
    /// Install as global logger for RTL
    static void install_rtl_printer(int level);
    /// Print startup message
    static void print_startup(const char* fmt, ...);
    /// Set IO buffering mode for stdout, stderr. See LogDevice for details
    static void set_io_buffering(size_t mode);
    /// All in one initialization if fifo string is not empty
    //static void install_fifolog(const char* fifo=0);
    /// All in one initialization if fifo string is not empty
    //static void install_fifolog(const char* fifo, const log_args& args);
    /// All in one initialization if output logger
    static void install_log(const log_args& args);

    /** Output logging helpers.                                 */

    /// Helper function to steer printing. Returns true if logging is enabled for level
    bool do_print(int level)  const;

    /// Generic output logging
    void output(int level, const char* msg,...)   const;

    /// Format output string
    static std::string format(const char* format, ...);

    /// Format output string
    static std::string format(const char* format, va_list& args);

    /// Calls the display action with a given severity level
    /**
     *  @arg severity   [int,read-only]      Display severity flag (see enum)
     *  @arg source     [string,read-only]   Originator name of the message
     *  @arg msg        [string,read-only]   Formatted message string
     *  @return Status code indicating success or failure
     */
    size_t printmsg(int severity, const char* source, const char* msg)  const;
    /// Always printout handling
    void always(const char* format, ...) const;
    /// Always printout handling
    void always(const std::string& msg) const    {  always(msg.c_str());   }
    /// Always printout handling
    void always(const char* format, va_list& args) const;
    /// Debug printout handling
    void debug(const char* format, ...) const;
    /// Debug printout handling
    void debug(const std::string& msg) const    {  debug(msg.c_str());   }
    /// Debug printout handling
    void debug(const char* format, va_list& args) const;
    /// Info printout handling
    void info(const char* format, ...) const;
    /// Info printout handling
    void info(const std::string& msg) const     {  info(msg.c_str());    }
    //void info(const std::string& format, ...) const;
    /// Info printout handling
    void info(const char* format, va_list& args) const;
    /// Warning printout handling
    void warning(const char* format, ...) const;
    /// Warning printout handling
    void warning(const std::string& msg) const  {  warning(msg.c_str()); }
    //void warning(const std::string& format, ...) const;
    /// Warning printout handling
    void warning(const char* format, va_list& args) const;
    /// Error handling. Returns error code
    int error(const std::string& msg) const     {  return error(msg.c_str());   }
    /// Error handling. Returns error code
    int error(const char* format, ...) const;
    /// Error handling. Returns error code
    int error(const char* format, va_list& args) const;
    /// Error handling: print exception. Technically returns error code.
    int error(const std::exception& e, const char* format, ...) const;
    /// Error handling: print exception. Technically returns error code.
    int error(const std::exception& e, const std::string& format, ...) const;
    /// Warning printout handling
    int fatal(const char* format, ...) const;
    /// Warning printout handling
    int fatal(const std::string& msg) const     {  return fatal(msg.c_str());   }
    /// Warning printout handling
    int fatal(const char* format, va_list& args) const;
    /// Exception throwing. Technically returns error code. Throws internally exception
    void except(const char* format, ...) const;
    /// Exception throwing. Technically returns error code. Throws internally exception
    void except(const std::string& msg) const     {  except(msg.c_str());   }
    /// Error handling. Technically returns error code. Throws internally exception
    int throwError(const std::string& msg) const;
    /// Error handling. Technically returns error code. Throws internally exception
    int throwError(const char* format, ...) const;
    /// Error handling. Technically returns error code. Throws internally exception
    int throwError(const char* format, va_list& args) const;
  };
}       // End namespace RTL
#endif  // ONLINEBASE_RTL_LOGGER_H
