//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
#ifndef _RTL_NUMA_H
#define _RTL_NUMA_H

// C/C++ include files
#include <vector>
#include <string>

/// RTL namespace declaration
namespace RTL   {

  /// Numa interface control component
  /** Numa interface control component. Used by GaudiOnline and Dataflow
   *  Binds the process to a given CPU mask for CPU and/or memory usage.
   *
   * @author  Markus Frank
   * @version 1.0
   */
  class Numa   {
  public:
    /// Property: String representation of the key to be added to the event context
    std::vector<int> cpuSlots {};
    std::vector<int> cpuMask  {};
    bool             bindCPU    = true;
    bool             bindMemory = true;

  public:
    /// Initializing constructor
    Numa() = default;
    /// Default destructor
    virtual ~Numa() = default;
    /// Check availability of NUMA
    static int available();
    /// Short summary of process status
    static std::pair<int, std::vector<std::string> > printStatus(bool print_pid=true);
    /// Print the current NUMA environment the process is executing on
    static std::pair<int, std::vector<std::string> > printSettings(bool print_pid=true);

    /// Apply NUMA configuration settings according to job options
    std::pair<int, std::vector<std::string> > apply_config();
  };
}

#endif // _RTL_NUMA_H
