//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
#ifndef ONLINEKERNEL_RTL_PROCESS_H
#define ONLINEKERNEL_RTL_PROCESS_H

// C++ include files
#include <csignal>
#include <vector>
#include <string>
#include <map>

///  RTL (Run-Time-Library) namespace declaration
namespace RTL {

  /// Class, which allows to manipulate a single process
  /** 
   *
   *  \author  M.Frank
   *  \version 1.0
   */
  class Process {
  public:
    typedef std::vector<std::string>           Arguments;
    typedef std::map<std::string, std::string> Environment;
    enum { INVALID = 0,
	   RUNNING = 1 << 0,
	   ENDED   = 1 << 1,
	   STOPPED = 1 << 2
    };
    enum { WAIT_BLOCK   = 1 << 0,
	   WAIT_NOBLOCK = 1 << 1
    };
  protected:
    /// Process identifier
    int m_pid              { -1 };
    /// Exit status of this process
    int m_status           { -1 };
    /// Process state
    int m_state            { INVALID };
    /// Flag to receive parent death signal
    int m_parent_death_sig { SIGKILL };
    /// Process name (=executable name if not set)
    std::string m_name     { };
    /// Processes executable
    std::string m_exec     { };
    /// Input device name
    std::string m_input    { };
    /// Input device name
    std::string m_output   { };
    /// Startup arguments
    Arguments   m_args     { };
    /// Startup environment
    Environment m_environ  { };

    /// Send signal to process specified by m_pid
    virtual int sendSignal(int sig);
    /// Send signal to process specified by -m_pid
    virtual int sendSignalAll(int sig);
    /// Attach input device to process
    virtual void attach_input(const std::string& input);
    /// Attach output device to process
    virtual void attach_output(const std::string& output);
    /// Setup the processes arguments ready for calling execve
    char** setup_arguments();
    /// Setup the processes environment ready for calling execve
    char** setup_environment();
    /// Set working directory
    void set_working_directory(const std::string& workdir);

  public:
    /// Processes working directory
    std::string working_dir { };
    /// Processes affinity
    std::string affinity { };
    /// Processes priority
    std::string priority { };
    /// Processes nice-value
    std::string nice     { };

    /// Standard constructor
    Process();
    /// Constructor to explore existing process
    explicit Process(int pid);
    /// Constructor to start new process
    Process(const std::string& name, const std::string& exe, const char* a[]);
    /// Constructor to start new process
    Process(const std::string& name, const std::string& exe, const char* a[], const std::string& o);
    /// Constructor to start new process
    Process(const std::string& name, const std::string& exe, const char* a[], const std::string& o, const std::string& in);
    /// Constructor to start new process
    Process(const std::string& name, const std::string& exe, const Arguments& a);
    /// Constructor to start new process
    Process(const std::string& name, const std::string& exe, const Arguments& a, const std::string& o);
    /// Constructor to start new process
    Process(const std::string& name, const std::string& exe, const Arguments& a, const std::string& o, const std::string& in);

    /// Standard destructor
    virtual ~Process();

    /// Debugging flag
    static void setDebug(bool val);
    /// Access to debugging flag
    static bool debug();

    /// Access process identifier
    int pid()  const                            { return m_pid;                }
    /// Access process name
    const std::string& name() const             { return m_name;               }
    /// Access startup arguments
    const Arguments& args() const               { return m_args;               }
    /// Access startup environment
    const Environment& environment() const      { return m_environ;            }
    /// Access process name
    void setName(const std::string& val)        { m_name = val;                }
    /// Access startup arguments
    void setArgs(Arguments&& val)               { m_args = std::move(val);     }
    /// Access startup arguments
    void setArgs(const Arguments& val)          { m_args = val;                }
    /// Access startup environment
    void setEnvironment(const Environment& env) { m_environ = env;             }
    /// Access output device
    const std::string& output() const           { return m_output;             }
    /// Access process output device
    void setOutput(const std::string& val)      { m_output = val;              }
    /// Access input device
    const std::string& input() const            { return m_input;              }
    /// Access process input device
    void setInput(const std::string& val)       { m_input = val;               }
    /// Flag to receive parent death signal
    void enable_parent_death(int signum)        { m_parent_death_sig = signum; }
    /// Flag to receive parent death signal
    void disable_parent_death()                 { m_parent_death_sig = 0;      }

    /// Access the exit status
    int  exit_status()  const                   { return m_status;             }
    /// Check if the task was started
    bool is_limbo() const                       { return m_state == INVALID;   }
    /// Update environment variables
    void updateEnvironment(const Environment& env);
    /// Update environment variables
    void updateEnvironment(const std::string& key, const std::string& value);
    
    /// String representation of process
    std::string bash_command()  const;
    /// String representation of process
    virtual std::string to_string()  const;
    /// Start process. Will NOT add new processes to new process group.
    virtual int start();
    /// Start process
    virtual int start(bool new_process_group);
    /// Check if the process is running. If not restart 'restarts' times.
    virtual int ensure(int restarts, int millis_between);
    /// Send a signal to the process
    virtual int signal(int signum);
    /// Send a signal to the process and all its children (SIGTERM)
    virtual int signalall(int signum);
    /// Terminate the process (SIGTERM)
    virtual int stop();
    /// Terminate the process and all its children (SIGTERM)
    virtual int stopall();
    /// Kill the process (SIGKILL)
    virtual int kill();
    /// Kill the process and its children (SIGKILL)
    virtual int killall();
    /// Interrupt the process (SIGINT)
    virtual int interrupt();
    /// Check if process is running
    virtual int is_running() const;
    /// Wait for process to terminate
    virtual int wait(int flag=WAIT_NOBLOCK);
    /// Wait BLOCKED secs seconds for process to terminate
    virtual int wait_for(unsigned long millisecs);
    /// Wait BLOCKED secs seconds for process to terminate
    virtual int wait_for(unsigned long millisecs, int flag);
  };
#if 0
  class ProcessHandler  {
  public:
    ProcessHandler(Process* process, 
    /// Build valid command line including replacements
    std::string RTL::ProcesdHandler::command_line(Process* process)   const   {
    std::unique_ptr<std::thread> start_threaded(Process* process)  const;
  };
#endif
} // End namespace RTL
#endif  // ONLINEKERNEL_RTL_PROCESS_H
