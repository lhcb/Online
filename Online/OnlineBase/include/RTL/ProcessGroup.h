//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
#ifndef ONLINEKERNEL_RTL_PROCESSGROUP_H
#define ONLINEKERNEL_RTL_PROCESSGROUP_H

// Framework include files
#include "RTL/Process.h"

///  RTL (Run-Time-Library) namespace declaration
namespace RTL {

  // Forward declarations
  class Process;

  /// Class, which allows to manipulate entire groups of processes
  /**
   *  Mainly used for unit testing multi-process applications
   *
   *  \author  M.Frank
   *  \version 1.0
   */
  class ProcessGroup : public Process {
  public:
    /// Definition of process container
    using Processes = std::vector<Process*>;
    
  protected:
    /// Container of managed processes
    Processes     m_procs     { };
    /// Maximum wait time for pg.wait(WAIT_BLOCKED) in milliseconds
    unsigned long m_max_wait  { 300*1000 };

  public:
    /// Standard constructor
    ProcessGroup();
    /// Standard destructor
    virtual ~ProcessGroup();
    /// Get a process by PID
    Process* get(int pid) const;
    /// Add a new process. Returns the added process
    Process* add(Process* p);
    /// Add multiple processes
    ProcessGroup& add(std::vector<RTL::Process*>&& procs);
    /// Merge two process groups
    ProcessGroup& merge(ProcessGroup&& pg);
    /// Get all processes
    std::vector<Process*> processes() const {  return m_procs; }
    /// Get maximum wait time
    unsigned long get_max_wait_time()   const  { return m_max_wait; }
    /// Set maximum wait time
    void set_max_wait_time(unsigned long time_in_milliseconds)
    { m_max_wait = time_in_milliseconds; }
    /// Remove a process from the list
    int remove(const std::string& name, Process** proc=0);
    /// Remove a process from the process group and give back control to caller
    int remove(RTL::Process* process);
    /// Remove all processes
    int removeAll();
    /// String representation of process
    virtual std::string to_string()  const  override;
    /// Start process group
    virtual int start() override;
    /// Start process
    virtual int start(bool new_process_group)  override;
    /// Check if the process is running. If not restart 'restarts' times.
    virtual int ensure(int restarts, int millis_between)  override;
    /// Stop process group
    virtual int stop()  override;
    /// Stop process group
    virtual int stop(std::size_t first, std::size_t last);
    /// Kill the processes (SIGKILL)
    virtual int kill()  override;
    /// Kill the processes and their children (SIGKILL)
    virtual int killall()  override;
    /// Check if process is running
    virtual int is_running() const  override;
    /// Wait for process group.end
    virtual int wait(int flag=WAIT_NOBLOCK)  override;
    /// Wait for process group.end
    virtual int wait(const std::vector<RTL::Process*>& procs, int flag=WAIT_NOBLOCK);
    /// Wait BLOCKED secs seconds for processes to terminate
    virtual int wait_for(unsigned long millisecs)  override;
    /// Wait BLOCKED secs seconds for processes to terminate
    virtual int wait_for(unsigned long millisecs, int flag)  override;
    /// Wait BLOCKED secs seconds for processes to terminate
    virtual int wait_for(const std::vector<RTL::Process*>& procs, unsigned long millisecs);
    /// Wait BLOCKED secs seconds for processes to terminate
    virtual int wait_for(const std::vector<RTL::Process*>& procs, unsigned long millisecs, int flag);
  };
} // End namespace RTL
#endif  // ONLINEKERNEL_RTL_PROCESSGROUP_H
