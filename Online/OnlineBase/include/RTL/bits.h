//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
#ifndef BUFFERMANAGER_BITS_H
#define BUFFERMANAGER_BITS_H

/// C/C++ include files
#ifdef __cplusplus
#include <cstdlib>
#include <cstring>
#include <climits>
#include <cstdint>

extern "C" {

#else

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <limits.h>
#endif

  inline int32_t bit_set(uint32_t* mask,int32_t pos)  {
    mask[pos/32] |= (1<<(pos%32));
    return 0;
  }

  inline int32_t bit_clear(uint32_t* mask,int32_t pos)  {
    mask[pos/32] &= ~(1<<(pos%32));
    return 0;
  }

  inline int32_t bit_test (const uint32_t *mask,int32_t pos)  {
    return (mask[pos/32] & (1<<(pos%32)));
  }
  int32_t mask_and2   (const uint32_t* mask1, const uint32_t* mask2, uint32_t* mask3, int32_t mask_size);
  int32_t mask_and3   (const uint32_t* mask1, const uint32_t* mask2, const uint32_t* mask3, uint32_t* mask4, int32_t mask_size);
  int32_t mask_and4   (const uint32_t* mask1, const uint32_t* mask2, const uint32_t* mask3, const uint32_t* mask4, uint32_t* mask5, int32_t mask_size);

  int32_t mask_or2    (const uint32_t* mask1, const uint32_t* mask2,uint32_t* mask3, const int32_t mask_size);
  int32_t mask_or3    (const uint32_t* mask1, const uint32_t* mask2, const uint32_t* mask3,uint32_t* mask4,uint32_t* mask5, const int32_t mask_size);
  int32_t mask_or4    (const uint32_t* mask1, const uint32_t* mask2, const uint32_t* mask3, const uint32_t* mask4, const int32_t mask_size);
  int32_t mask_and_ro2(const uint32_t* mask1, const uint32_t* mask2, const int32_t mask_size);
  int32_t mask_and_ro3(const uint32_t* mask1, const uint32_t* mask2, const uint32_t* mask3, const int32_t mask_size);
  int32_t mask_or_ro2 (const uint32_t* mask1, const uint32_t* mask2, const int32_t mask_size);
  int32_t mask_or_ro3 (const uint32_t* mask1, const uint32_t* mask2, const uint32_t* mask3, const int32_t mask_size);
  int32_t mask_or_ro4 (const uint32_t* mask1, const uint32_t* mask2, const uint32_t* mask3, const uint32_t* mask4, const int32_t mask_size);
  int32_t mask_summ   (const uint32_t* mask, int32_t mask_size);
  int32_t BF_alloc    (void* base, int32_t bf_size, int32_t size_wanted, int32_t* pos_found);
  int32_t BF_alloc2   (void* base, int32_t bf_size, int32_t size_wanted, int32_t* pos_found);
  int32_t BF_count    (const void* base, int32_t bf_size, int32_t* pos, int32_t* size);
  int32_t BF_count2   (const void* base, int32_t bf_size, int32_t* pos, int32_t* size);
  int32_t BF_set      (void* base, int32_t start, int32_t end);
  int32_t BF_set2     (void* base, int32_t start, int32_t end);
  int32_t BF_free     (void* base, int32_t pos, int32_t size);
  int32_t BF_free2    (void* base, int32_t pos, int32_t size);
  void    BF_print    (const void* base, int32_t bf_size, size_t ncols=4, bool prt_hdr=true);

#ifdef __cplusplus
}

/// C/C++ include files
#include <vector>
#include <string>

/// Framework include files
#include "RTL/Pack.h"
namespace Bits  {


  static constexpr const int BITS_PER_BYTE = 8;
  
  /// Dump bit fiels to strings.
  void dumpWords(const void* field, int32_t len, std::vector<std::string>& words);

  /// Class to manager bit-fileds in C++
  /** Class to manager bit-fileds in C++ using the C interface
   *
   *  \author   M.Frank
   *  \version  1.9
   */
  template <int32_t i> struct BitMask  {
    static constexpr std::size_t mask_bytes = i*sizeof(uint32_t);
    uint32_t m_mask[i];
    /// Default bitfield constructor
    BitMask() { ::memset(m_mask, 0, i * sizeof(m_mask[0]));  }
    /// Default move constructor
    BitMask(BitMask&& copy) = default;
    /// Default copy constructor
    BitMask(const BitMask& copy) = default;
    /// Initializing bitfield constructor
    explicit BitMask(int32_t def) {
      for(int32_t j=0; j<i;++j) m_mask[j]=def;
    }
    /// Initializing bitfield constructor
    explicit BitMask(const uint32_t vals[]) {
      for(int32_t j=0; j<i;++j) m_mask[j]=vals[j];
    }
    BitMask& operator=(const BitMask<i>& val) = default;
    bool operator==(const BitMask<i>& val)  const {
      return ::memcmp(val.m_mask,this->m_mask,mask_bytes) == 0;
    }
    bool operator!=(const BitMask<i>& val)  const {
      return !(*this == val);
    }
    
    BitMask& operator=(const uint32_t val[i]) {
      for(int32_t j=0; j<i;++j) m_mask[j]=val[j];
      return *this;
    }
    bool operator==(const uint32_t val[i]) const   {
      return ::memcmp(val,m_mask,mask_bytes) == 0;
    }
    bool operator!=(const uint32_t val[i])   const  {
      return ::memcmp(val,m_mask,mask_bytes) != 0;
    }
    
    /// Number of digits for the bitfield
    static uint32_t digits() {
      return i*sizeof(uint32_t)*CHAR_BIT;
    }
    /// Number of 32 bit words in the mask
    int32_t words()  const {
      return i;
    }
    /// Clear bitfield
    void clear()  {
      ::memset(m_mask, 0, i * sizeof(m_mask[0]));
    }
    /// Apply OR operator  of one other mask to the current mask
    inline int32_t apply_or(const BitMask<i>& mask)  {
      return ::mask_or2(mask.m_mask, m_mask, m_mask, i);
    }
    /// Apply OR operator  of bitarray to the current mask
    int32_t apply_or(const uint32_t mask[])  {
      return ::mask_or2(mask, m_mask, m_mask, i);
    }
    /// Apply OR operator  of two other masks to the current mask
    int32_t apply_or(const BitMask<i>& mask1, const BitMask<i>& mask2)  {
      return ::mask_or3(mask1.m_mask, mask2.m_mask, m_mask, m_mask, i);
    }
    /// Apply OR operator  of three other masks to the current mask
    int32_t apply_or(const BitMask<i>& mask1, const BitMask<i>& mask2, const BitMask<i>& mask3)  {
      return ::mask_or4(mask1.m_mask, mask2.m_mask, mask3.m_mask, m_mask, m_mask, i);
    }

    /// Calculate the OR operator  with another mask and return result.
    int32_t mask_or(const BitMask<i>& mask)  const  {
      return ::mask_or_ro2(m_mask, mask.m_mask, i);
    }
    /// Calculate the OR operator  with a bitfield and return result.
    int32_t mask_or(const uint32_t mask[])  const {
      return ::mask_or_ro2(m_mask, mask, i);
    } 
    /// Calculate the OR operator  with two other masks and return result.
    int32_t mask_or(const BitMask<i>& mask1, const BitMask<i>& mask2)  const {
      return ::mask_or_ro3(m_mask, mask1.m_mask, mask2.m_mask, i);
    }
    /// Calculate the OR operator  with three other masks and return result.
    int32_t mask_or(const BitMask<i>& mask1, const BitMask<i>& mask2, const BitMask<i>& mask3)  const {
      return ::mask_or_ro4(m_mask, mask1.m_mask, mask2.m_mask, mask3.m_mask, i);
    }

    /// Apply AND operator  of one other mask to the current mask
    int32_t apply_and(const BitMask<i>& mask)  {
      return ::mask_and2(mask.m_mask, m_mask, m_mask, i);
    }
    /// Apply AND operator  of bitarray to the current mask
    int32_t apply_and(const uint32_t mask[])     {
      return ::mask_and2(mask, m_mask, m_mask, i);
    }
    /// Apply AND operator  of two other masks to the current mask
    int32_t apply_and(const BitMask<i>& mask1, const BitMask<i>& mask2)  {
      return ::mask_and3(mask1.m_mask, mask2.m_mask, m_mask, m_mask, i);
    }
    /// Apply AND operator  of three other masks to the current mask
    int32_t apply_and(const BitMask<i>& mask1, const BitMask<i>& mask2, const BitMask<i>& mask3)  {
      return ::mask_and4(mask1.m_mask, mask2.m_mask, mask3.m_mask, m_mask, m_mask, i);
    }
    int32_t mask_and(const BitMask<i>& mask1, const BitMask<i>& mask2)  {
      return ::mask_and2(mask1.m_mask, mask2.m_mask, m_mask, i);
    }
    int32_t mask_and(const BitMask<i>& mask1, const BitMask<i>& mask2, const BitMask<i>& mask3)  {
      return ::mask_and3(mask1.m_mask, mask2.m_mask, mask3.m_mask, m_mask, i);
    }

    int32_t mask_summ()  const    {
      return ::mask_summ(m_mask,i);
    }
    void set(int32_t which)  {
      ::bit_set(m_mask,which);
    }
    void clear(int32_t which)  {
      ::bit_clear(m_mask,which);
    }
    int32_t test(int32_t which) const  {
      return ::bit_test(m_mask,which);
    }
    const uint32_t* bits()  const  {
      return m_mask;
    }
    uint32_t word(int32_t which)  const  {
      return m_mask[which];
    }
    void setWord(int32_t which, int32_t value)  {
      m_mask[which] = value;
    }
  };
}
#include "RTL/Unpack.h"
#endif

#endif // BUFFERMANAGER_BITS_H
