//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
#ifndef ONLINE_ONLINEBASE_HASH_H
#define ONLINE_ONLINEBASE_HASH_H

/// C/C++ include files
#include <string>
#include <cstdint>

/// RTL namespace declaration
namespace RTL {

  /// We need it so often: one-at-time 32 bit hash function from string
  uint32_t hash32 (const char* key);
  /// Compute HASH32 checksum from string
  uint32_t hash32 (const std::string& key);
  /// Compute HASH32 checksum from buffer
  uint32_t hash32 (const void* ptr, std::size_t len);

  /// Compute 16 bit fletcher checksum from buffer
  uint16_t fletcher16(const void* buffer, std::size_t len);
  /// Compute 32 bit fletcher checksum from buffer
  uint32_t fletcher32(const void* data, std::size_t len);

  /// Compute 32 bit Murmur3 hash from checksum with update possibility
  uint32_t murmur3_32(uint32_t seed, const void* key, std::size_t len);
  /// Compute 32 bit Murmur3 hash from checksum
  uint32_t murmur3_32(const void* key, std::size_t len);
  
  /// Compute ADLER32 checksum from buffer with update possibility
  uint32_t adler32(uint32_t adler, const void* ptr, std::size_t len);
  /// Compute ADLER32 checksum from buffer
  uint32_t adler32(const void* ptr, std::size_t len);

  /// Compute CRC8 checksum from buffer
  uint8_t  crc8   (const void *ptr, std::size_t len);
  /// Compute CRC8 checksum from buffer with update possibility
  uint8_t  crc8   (uint8_t c, const void* ptr, std::size_t len);

  /// Compute CRC16 checksum from buffer
  uint16_t crc16  (const void *ptr, std::size_t len);
  /// Compute CRC16 checksum from buffer with update possibility
  uint16_t crc16(uint16_t checksum, const void* data, size_t len);

  /// Compute CRC32 checksum from buffer
  uint32_t crc32  (const void *ptr, std::size_t len);
  /// Compute CRC32 checksum from buffer with update possibility
  //uint32_t crc32  (uint32_t crc, const void *ptr, std::size_t len);

  /// Compute 16 bit BSD checksum from buffer
  uint16_t bsd16(const void* ptr, std::size_t len);  
  /// Compute 16 bit BSD checksum from buffer
  uint16_t bsd16(uint16_t checksum, const void* ptr, std::size_t len);  
}      // End namespace RTL

#endif // ONLINE_ONLINEBASE_HASH_H
