//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
#ifndef _RTL_NETDEF_H
#define _RTL_NETDEF_H

#ifdef __cplusplus
#include <string>
#include <vector>
#include <map>
#include <netinet/in.h>

extern "C"  {
  /// Bind the socket address to the first free port availible
  int net_bind_free_server_port(struct sockaddr_in* addr);
}

/// Run-Time-Library namespace declaration
namespace RTL {

  /// Get first free port number in the system. Socket is closed after aquiring the port
  std::pair<int, unsigned short> get_free_server_port();

  /// Get first free port numbers in the system. Sockets are closed after aquiring the port
  std::pair<int, std::vector<unsigned short> > get_free_server_ports(std::size_t count);

}

#endif   /* __cplusplus    */
#endif   /* _RTL_NETDEF_H  */
