//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  odin_t.h
//--------------------------------------------------------------------------
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINEBASE_RTL_POSIX_H
#define ONLINEBASE_RTL_POSIX_H

/// C/C++ include files
#include <cstdio>
#include <cstdint>
#include <cstdarg>
#include <utility>
#include <sys/stat.h>

struct stat;
struct stat64;
struct dirent;
#include <dirent.h>

/// Online namespace declaration
namespace Online {

  /// Generic posix I/O support
  /** @class posix_t
   *
   * @author:  M.Frank
   * @version: 1.0
   */
  class posix_t {
  public:
    enum { NONE, PARTIAL, COMPLETE };
    posix_t();
    posix_t(posix_t&& copy) = delete;
    posix_t(const posix_t& copy) = delete;
    posix_t& operator=(posix_t&& copy) = delete;
    posix_t& operator=(const posix_t& copy) = delete;

    static std::pair<int,int> fopen_disk(const char* path, const char* mode);
    static int  open_disk     ( const char* path, int flags, va_list& args );
    static long read_disk     ( int fd, void* ptr, std::size_t len );
    static long write_disk    ( int fd, const void* ptr, std::size_t len );
    static int  access_disk   ( const char* nam, int mode );
    static int  unlink_disk   ( const char* nam ); 
    static int  truncate_disk ( int fd, std::size_t len );
    static int  lseek_disk    ( int fd, long offset, int how );
    static int  lseek64_disk  ( int fd, int64_t offset, int how );
    static int  fstat_disk    ( int fd, struct stat* statbuf );
    static int  fstat64_disk  ( int fd, struct stat64* statbuf );
    static int  stat_disk     ( const char* path, struct stat* statbuf );
    static int  stat64_disk   ( const char* path, struct stat64* statbuf );
    static int  fileno_specific ( const FILE* stream );
    static int  new_fd        ( );
    /// Set option to specific implemetation of the descriptor
    int     ( *set_option )   (int fd, const char* what, const char* value) = nullptr;

    int unbuffered;
    int     ( *open    )( const char* filepath, int flags, ...) = nullptr;
    int     ( *close   )( int fd ) = nullptr;
    int     ( *access  )( const char* filepath, int mode ) = nullptr;
    int     ( *unlink  )( const char* filepath ) = nullptr;
    ssize_t ( *read    )( int fd, void* ptr, size_t size ) = nullptr;
    ssize_t ( *write   )( int fd, const void* ptr, size_t size ) = nullptr;
    long    ( *lseek   )( int fd, long offset, int how ) = nullptr;
    int64_t ( *lseek64 )( int fd, int64_t offset, int how ) = nullptr;
    int     ( *stat    )( const char* path, struct stat* statbuf ) = nullptr;
    int     ( *stat64  )( const char* path, struct stat64* statbuf ) = nullptr;
    int     ( *fstat   )( int fd, struct stat* statbuf ) = nullptr;
    int     ( *fstat64 )( int fd, struct stat64* statbuf ) = nullptr;

    int buffered;
    FILE*   ( *fopen   )( const char*, const char* ) = nullptr;
    int     ( *fclose  )( FILE* ) = nullptr;
    size_t  ( *fwrite  )( const void*, size_t, size_t, FILE* ) = nullptr;
    size_t  ( *fread   )( void*, size_t, size_t, FILE* ) = nullptr;
    long    ( *ftell   )( FILE* ) = nullptr;
    int     ( *fseek   )( FILE*, long, int ) = nullptr;
    int     ( *fileno  )( FILE* ) = nullptr;

    int directory;
    int     ( *mkdir   )( const char* path, unsigned int mode ) = nullptr;
    int     ( *rmdir   )( const char* path ) = nullptr;
    DIR*    ( *opendir )( const char* dirpath ) = nullptr;
    int     ( *closedir)( DIR* dirp ) = nullptr;
    dirent* ( *readdir )( DIR* dirp ) = nullptr;
  };

  /// ------------------------------------------------------------------------------
  std::size_t posix_print(int debug, const char* format, ...);
} // End namespace Online

/// Descriptor for the POSIX file system
const Online::posix_t* native_posix_descriptor();

#endif /// ONLINEBASE_RTL_POSIX_H
