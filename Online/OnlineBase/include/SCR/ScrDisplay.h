//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
/*-----------------------------------------------------------------------*/
/*                                                                       */
/*                  ASCII GRAPHICS DISPLAY IMPLEMEMENTATION              */
/*                                                                       */
/*-----------------------------------------------------------------------*/
#ifndef ONLINEKERNEL_SCR_SCRDISPLAY_H
#define ONLINEKERNEL_SCR_SCRDISPLAY_H

/// C/C++ include files
#include <cstdlib>
#include <cstdarg>
#include <string>

/// Framework include files
#include <SCR/scrdef.h>
#include <CPP/MonitorDisplay.h>

namespace SCR {
  struct Pasteboard;
  struct Display;
}

/**@class ScrDisplay ScrDisplay.h SCR/ScrDisplay.h
 *
 * Simple ASCII display implementation for monitoring applications
 *
 * @author Markus Frank
 */
class ScrDisplay : public MonitorDisplay     {
protected:
  SCR::Pasteboard* m_pb;
  SCR::Display*    m_display;
  Area             m_area;
  void print_char(int x, int y, int attr, int val);
public:
  /// Set global pasteborard area
  static void setPasteboard(SCR::Pasteboard* pasteboard);
  /// Set global border types
  static void setBorder(int border);
  /// Access to SCR Pasteboard
  SCR::Pasteboard* pasteboard() const;
  /// Access to SCR display structure
  SCR::Display* display() const               { return m_display;                    }
  /// Display Area
  const Area& area() const                    { return m_area;                       }
  /// Access to the display width in fixed size characters
  size_t width() const                override { return m_area.width;                 }
  /// Access to the display height in fixed size characters
  size_t height() const               override { return m_area.height;                }
  /// Access to next line pointer
  size_t nextLine()                           { return ++m_currLine;                 }
  /// Access in write mode to display position
  Position& position()                        { return m_position;                   }

public:
  /// Setup display window
  void setup_window() override;
  /// Default Constructor with display sizes
  virtual void setup_window(const Position& pos, const Area& a, const std::string& title="");
  /// Create subdisplay
  MonitorDisplay* createSubDisplay(const Position& pos, const Area& a, const std::string& title="");
  /// Update window title/header
  void set_header(int flags, const char* format,...)  override;
  /// Reset display window
  void reset_window() override;
  /// Start update cycle
  void begin_update() override;
  /// Finish update cycle
  void end_update() override;
  /// Draw empty line to the display
  size_t draw_line() override;
  /// Draw text line with attributes
  size_t draw_line(int flags, const char* format,...)  override;
  /// Draw formatted line in normal rendering
  size_t draw_line_normal(const char* format,...) override;
  /// Draw formatted line in reverse rendering
  size_t draw_line_reverse(const char* format,...) override;
  /// Draw a line bar
  size_t draw_bar() override;
  /// Draw formatted line in bold rendering
  size_t draw_line_bold(const char* format,...) override;
  /// Draw a progress bar
  size_t draw_bar(int x, int y,float ratio,int full_scale) override;

  /// Default Constructor: will open in full window mode
  ScrDisplay();
  /// Default Constructor with display sizes
  ScrDisplay(int width, int height);
  /// Default destructor
  virtual ~ScrDisplay();
};
#endif // ONLINEKERNEL_SCR_SCRDISPLAY_H
