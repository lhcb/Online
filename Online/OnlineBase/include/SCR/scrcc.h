//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================

#ifndef ONLINE_SCR_SCRCC_H
#define ONLINE_SCR_SCRCC_H

/// Framework include files
#include <SCR/scr.h>

extern "C" SCR::PasteboardContext scrcc_create_pasteboard(const char* device);
extern "C" SCR::Display* scrcc_create_display(int rows, int cols, int attr, SCR::flag border,const char* title);

#endif /* ONLINE_SCR_SCRCC_H */
