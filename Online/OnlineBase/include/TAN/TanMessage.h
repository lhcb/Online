//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
/*
  Definition of the TANMESSAGE structure
  C interface
  C++ usage

*/
#ifndef ONLINEBASE_TAN_TANMESSAGE_H
#define ONLINEBASE_TAN_TANMESSAGE_H

#include "TAN/TanErrno.h"
#include "NET/defs.h"

#define NAME_SERVICE_NAME    "TAN_NAME"

#ifdef __cplusplus
#include "NET/NetworkChannel.h"
#include <cstring>
#include <ctype.h>

/// Definition of a TAN communication message
/**
   {\Large{\bf Class TanMessage}}

   Message structure to communicate with the nameserver task
*/
class TanMessage  {
public:
  //@Man: Public local structure declarations
  /// Definition of the Port structure
  typedef __NetworkPort__       Port;
  /// Definition of the Family structure
  typedef __NetworkFamily__     Family;
  /// Definition of the Channle structure
  typedef __NetworkChannel__    Channel;
  /// Definition of the Address structure 
  typedef __NetworkAddress__    Address;
  /// Definition of the Subaddress structure
  typedef __NetworkSubAddress__ SubAddress;
public:
#else
  typedef struct  {
#endif
    unsigned int       m_length = sizeof(TanMessage);
    unsigned int       m_error  = TAN_SS_SUCCESS;
    unsigned int       m_function = 0;
    char               m_name[128];
    __NetworkAddress__ m_sin;
#ifndef __cplusplus
  }  TAN_MSG, TanMessage;
#else                                            /*   C++ Only!!!   */
public:
  /// Enumeration: Request codes
  enum Type {
    ALLOCATE,
    DEALLOCATE,
    INQUIRE,
    ALIAS,
    DEALIAS,
    DISCONNECTED,
    DUMP,
    SHUTDOWN,
    DECLARE_PORT
  };
public:
  //@Man: Public interface: 
  /// Constructor to build request message
  explicit TanMessage ( u_int func, const char* proc = "");
  /// Move constructor
  TanMessage (TanMessage&& copy) = default;
  /// Copy constructor
  TanMessage (const TanMessage& copy) = default;
  /// Standard constructor
  TanMessage ();
  /// Standard destructor
  ~TanMessage () = default;
  /// equal operator
  TanMessage& operator = (const TanMessage& cp) = default;
  /// Retrieve pointer to name
  char* _Name() {
    return m_name;
  }
  /// Retrieve pointer to name
  const char* _Name()  const {
    return m_name;
  }
  /// Retrieve reference to network address
  Address& Addr()  {
    return m_sin;
  }
  /// Retrieve reference to network sub address
  SubAddress& address() {
    return m_sin.sin_addr;
  }
  /// Retrieve reference to network sub address
  const SubAddress& address()  const  {
    return m_sin.sin_addr;
  }
  /// Retrieve port number
  Port    port() const {
    return m_sin.sin_port;
  }
  /// Retrieve network family type
  Family  family() const {
    return m_sin.sin_family;
  }
  /// Retrieve error code
  unsigned int error() const {
    return m_error;
  }
  /// Retrieve function code (of type Type)
  unsigned int function() const {
    return m_function;
  }
  /// Retrive length
  unsigned int _Length() const {
    return m_length;
  }
  /// Swap to network structure
  void Convert();
};

inline TanMessage::TanMessage () {
  ::memset(m_name,0,sizeof(m_name));
  ::memset(&m_sin, 0, sizeof (m_sin));
}

inline TanMessage::TanMessage (u_int func, const char* proc)  {
  m_function  = func;
  for (size_t i=0, n=::strlen(proc); i<n; i++)
    m_name[i] = char(::tolower(proc[i]));
  m_name[sizeof(m_name)-1] = 0;
  ::memset(&m_sin, 0, sizeof (m_sin));
}

inline void TanMessage::Convert()  {
  //m_length   = htonl(m_length);
  //m_error    = htonl(m_error);
  //m_function = htonl(m_function);
  //m_sin.sin_family = htons (m_sin.sin_family);
}

#endif   /* __cplusplus                  */
#endif   /* ONLINEBASE_TAN_TANMESSAGE_H  */
