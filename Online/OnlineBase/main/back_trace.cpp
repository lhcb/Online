//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Author     : Markus Frank
//==========================================================================

/// C/C++ include files
#include <unistd.h>
#include <cstring>

/// Framework include files
extern "C" int lib_rtl_enable_backtrace();

// Initialize the logger unit
__attribute__((constructor)) void s__initialize_back_trace()    {
  const char* msg = "++ GDB backtrace is now enabled.\n";
  ::lib_rtl_enable_backtrace();
  ::write(STDOUT_FILENO, msg, strlen(msg));
}

/// Shutdown the loggetr unit
__attribute__((destructor))  void s__finalize_back_trace()    {
}
