//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Author     : Markus Frank
//==========================================================================
#if defined(__clang__) || defined(__CLING__)
#pragma clang diagnostic ignored "-Wregister"
#elif defined(__GNUC__)
#pragma GCC diagnostic ignored   "-Wregister"
#endif

/// Framework include files
#include <LOG/FifoLog.inl.h>

// Initialize the logger unit
__attribute__((constructor)) void s__fifolog_initialize_logger()    {
  fifolog_initialize_logger();
}

/// Shutdown the loggetr unit
__attribute__((destructor))  void s__fifolog_finalize_logger()    {
  //::usleep(3000);
  // fifolog_finalize_logger();
}

// The actual code to start python
#include "genPythonLib.cpp"
extern "C" int genPython(int argc, char** argv);

/// Start the interpreter in normal mode without hacks like 'python.exe' does.
int main( int argc, char** argv )   {
  return genPython(argc, argv);
}
