//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Author     : Markus Frank
//==========================================================================
#if defined(__clang__) || defined(__CLING__)
#pragma clang diagnostic ignored "-Wregister"
#elif defined(__GNUC__)
#pragma GCC diagnostic ignored   "-Wregister"
#endif

/// C/C++ include files
#include <iostream>
#include <cerrno>
#include <vector>

// -----------------------------------------------------------------------------
// Python hacks to avoid warnings if outside dictionaries .....
// -----------------------------------------------------------------------------
// --> /usr/include/python2.7/pyconfig.h:1161:0: warning: "_POSIX_C_SOURCE" redefined [enabled by default]
#ifdef _POSIX_C_SOURCE
#undef _POSIX_C_SOURCE
#endif /* _POSIX_C_SOURCE */
// --> /usr/include/python2.7/pyconfig.h:1183:0: warning: "_XOPEN_SOURCE" redefined [enabled by default]
#ifdef _XOPEN_SOURCE
#undef _XOPEN_SOURCE
#endif /* _XOPEN_SOURCE */
#include "Python.h"

/// Start the interpreter in normal mode without hacks like 'python.exe' does.
extern "C" int genPython( int argc, char** argv )   {
#if PY_VERSION_HEX < 0x03000000
  return ::Py_Main(argc, argv);
#else
  std::vector<std::wstring> wargs;
  std::vector<const wchar_t*> wargv;
  for(int i=0; i<argc;++i)  {
    std::wstring wstr;
    if ( argv[i] )  {
      const size_t size = ::strlen(argv[i]);
      if (size > 0) {
        wstr.resize(size+1);
        std::mbstowcs(&wstr[0], argv[i], size);
        wstr[size] = 0;
      }
    }
    wargs.push_back(wstr);
  }
  for( auto& s : wargs ) wargv.push_back(s.c_str());
  return ::Py_Main(argc, (wchar_t**)&wargv[0]);
#endif
}
