//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Author     : Markus Frank
//==========================================================================

/// Framework include files
#include <LOG/FifoLog.inl.h>

/// C/C++ include files
#include <unistd.h>
#include <iostream>
#include <cerrno>

// Iniitalize the logger unit
__attribute__((constructor)) void s__fifolog_initialize_logger()    {
  fifolog_initialize_logger();
}

/// Shutdown the loggetr unit
__attribute__((destructor))  void s__fifolog_finalize_logger()    {
  // ::usleep(3000);
  // fifolog_finalize_logger();
}


/// Framework include files
#include <RTL/DllAccess.h>

/// Generic main program to start any process
/**
 *   Generic main program to start any process
 *   by loading a library and executing a 
 *   function within
 *
 *   \author  M.Frank
 *   \version 1.0
 */
int main (int argc, char** argv)  {
  if ( argc >= 3 )  {
    void* handle = LOAD_LIB( argv[1] );
    if ( handle )  {
      Function fun(GETPROC(handle, argv[2]));
      if ( fun.function ) {
	return (*fun.function)(argc-2, &argv[2]);
      }
      // The required function is not in the loaded image.
      // Try to load it directly from the executable
      Function f1(GETPROC(0, argv[2]));
      if ( f1.function ) {
	std::cout << "[WARNING] Executing function " << argv[2] 
		  << " from exe rather than image " << std::endl;
	return (*f1.function)(argc-2, &argv[2]);
      }
      std::cout << "[ERROR] Failed to access test procedure: '" << argv[2] << "'" << std::endl;
      std::cout << "[ERROR] Error: " << DLERROR << std::endl;
      return EINVAL;
    }
    std::cout << "[ERROR] Failed to load test library: '" << argv[1] << "'" << std::endl;
    std::cout << "[ERROR] Error: " << DLERROR << std::endl;
    return EINVAL;
  }
  std::cout << "[ERROR] usage: genRunner <library-name>  <function-name>  -arg [-arg]" << std::endl;
  return EINVAL;
}
