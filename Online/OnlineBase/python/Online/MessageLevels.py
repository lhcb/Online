#==========================================================================
#  LHCb Online software suite
#--------------------------------------------------------------------------
# Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
# All rights reserved.
#
# For the licensing terms see OnlineSys/LICENSE.
#
# Author     : M.Frank
#
#==========================================================================
__version__ = "1.0"
__author__  = "Markus Frank <Markus.Frank@cern.ch>"


import os

# ------------------------------------------------------------------------------
MSG_VERBOSE = 1
MSG_DEBUG   = 2
MSG_INFO    = 3
MSG_WARNING = 4
MSG_ERROR   = 5
MSG_FATAL   = 6
MSG_ALWAYS  = 7

# ------------------------------------------------------------------------------
global log_format
log_format = '%-24s %s'

global minPrintLevel
minPrintLevel = MSG_INFO

global utgid
utgid = ''
if 'UTGID' in os.environ:
  utgid = os.environ['UTGID']
else:
  utgid = 'P%06d'%(os.getpid(),)

# ------------------------------------------------------------------------------
class output_level:
  def __init__(self):
    self.VERBOSE = MSG_VERBOSE
    self.DEBUG   = MSG_DEBUG
    self.INFO    = MSG_INFO
    self.WARNING = MSG_WARNING
    self.ERROR   = MSG_ERROR
    self.FATAL   = MSG_FATAL
    self.ALWAYS  = MSG_ALWAYS

  def name(self, value=None):
    global minPrintLevel
    if not value:
      return self.name(minPrintLevel)
    value = int(value)
    if value == self.VERBOSE:
      return 'VERBOSE'
    elif value == self.DEBUG:
      return 'DEBUG'
    elif value == self.INFO:
      return 'INFO'
    elif value == self.WARNING:
      return 'WARNING'
    elif value == self.ERROR:
      return 'ERROR'
    elif value == self.FATAL:
      return 'FATAL'
    elif value == self.ALWAYS:
      return 'ALWAYS'
    return 'UNKNOWN'
  
OutputLevel = output_level()

# ------------------------------------------------------------------------------
def log(level, fmt, *args):
  """
  
  \author   M.Frank
  \version  1.0
  \date     30/06/2002
  """
  import sys
  global minPrintLevel
  global log_format
  global utgid
  
  try:
    f = sys.stdout
    if level < minPrintLevel:
      return
    elif level == MSG_VERBOSE:
      format = 'VERBOSE ' + log_format
    elif level == MSG_DEBUG:
      format = 'DEBUG   ' + log_format
    elif level == MSG_INFO:
      format = 'INFO    ' + log_format
    elif level == MSG_WARNING:
      format = 'WARNING ' + log_format
    elif level == MSG_ERROR:
      format = 'ERROR   ' + log_format
    elif level == MSG_ALWAYS:
      format = 'SUCCESS ' + log_format
    elif level >  MSG_ALWAYS:
      format = 'INFO    ' + log_format
    msg = fmt%args
    print(format%(utgid, msg, ), file=f)
    f.flush()
  except Exception as X:
    print('WARNING Exception failure: '+str(X))

