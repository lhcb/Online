//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank, B.Jost
//
//==========================================================================

// Framework include files
#include <AMS/Transfer.h>
#include <AMS/amsdef.h>
#include <RTL/rtl.h>

// C/C++ include files
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <mutex>
#include <memory>

namespace AMS  {

  struct netentry_t : public netheader_t {
    std::unique_ptr<char[]> data;
  };

  struct  NET  {
    struct callback_param   {
      void* call_param;
      unsigned int facility;
      net_handler_t call_data;
      net_handler_t call_death;
      callback_param(void* param, unsigned int fac, net_handler_t data, net_handler_t death)
	: call_param(param), facility(fac), call_data(data), call_death(death) {}
    };
    unsigned int      m_refCount;
    int               m_count;
    NetConnectionType m_type;
    std::mutex        m_lock;
    std::map<std::pair<const void*, unsigned int>, std::unique_ptr<callback_param> > m_callbacks;

    explicit NET() = default;
    ~NET()  {
      ::amsc_close();
    }
    static int handle_data(const amsuc_info* info, void* par)   {
      callback_param* p = (callback_param*)par;
      netentry_t entry;
      entry.size = info->length;
      entry.facility = info->facility;
      if ( p->call_data )   {
	::strncpy(entry.name, info->source, sizeof(entry.name)-1);
	entry.name[sizeof(entry.name)-1] = 0;
	(p->call_data)(entry, p->call_param, &entry);
	return AMS_SUCCESS;
      }
      std::size_t len = entry.size;
      entry.data.reset(new char[entry.size+1]);
      ::amsc_read_message(entry.data.get(), &len, nullptr, nullptr, nullptr);
      return AMS_SUCCESS;
    }
    static int handle_death(const amsuc_info* info, void* par)   {
      callback_param* p = (callback_param*)par;
      if ( p->call_death )   {
	netentry_t entry;
	entry.size = info->length;
	entry.facility = info->facility;
	::strncpy(entry.name, info->source, sizeof(entry.name)-1);
	entry.name[sizeof(entry.name)-1] = 0;
	(p->call_death)(entry, p->call_param, &entry);
	return AMS_SUCCESS;
      }
      return AMS_SUCCESS;
    }
    NetConnectionType type() const {
      return m_type;
    }
    void cancel()  {
    }
    NetErrorCode init(const std::string& proc, NetConnectionType /* type */, int /* nthreads */)   {
      int status = ::amsc_init(proc.c_str());
      if ( status == AMS_SUCCESS ) status = ::amsuc_init();
      return status==AMS_SUCCESS ? NET_SUCCESS : NET_ERROR;
    }
    NetErrorCode receive(netentry_t* entry, void* data, std::size_t /* len */)   {
      size_t len = entry->size;
      if ( !data )  {
	entry->data.reset(new char[len+1]);
	data = entry->data.get();
      }
      int status = ::amsc_read_message(data, &len, nullptr, nullptr, nullptr);
      return status==AMS_SUCCESS ? NET_SUCCESS : NET_ERROR;
    }
    NetErrorCode send(const void* buff, std::size_t size, const std::string& dest, int fac)  {
      int status = ::amsc_send_message(buff, size, dest.c_str(), fac, nullptr);
      return status==AMS_SUCCESS ? NET_SUCCESS : NET_ERROR;
    }
    NetErrorCode subscribe(void* param, unsigned int fac, net_handler_t data, net_handler_t death)  {
      auto par = std::make_unique<callback_param>(param, fac, data, death);
      int status = ::amsuc_subscribe(fac, handle_data, handle_death, par.get());
      if ( status == AMS_SUCCESS ) m_callbacks[{param, fac}] = std::move(par);
      return status==AMS_SUCCESS ? NET_SUCCESS : NET_ERROR;
    }
    NetErrorCode unsubscribe(void* , unsigned int fac)   {
      int status = ::amsuc_remove(fac);
      return status==AMS_SUCCESS ? NET_SUCCESS : NET_ERROR;
    }
    unsigned int release()   {
      delete this;
      return 0;
    }
    static NET*  instance(const std::string& proc, int nthreads, NetConnectionType type)   {
      static NET* s_ams = 0;
      if ( 0 == s_ams )  {
	s_ams = new NET();
	s_ams->init(proc, type, nthreads);
      }
      return s_ams;
    }
  };
}

//----------------------------------------------------------------------------------
AMS::NET* AMS::net_init(const std::string& proc, int nthreads, NetConnectionType type)
{ return NET::instance(proc, nthreads, type);                     }
void AMS::net_close(NET* net)
{ if ( net ) net->release();                                      }
int AMS::net_receive(NET* net, netentry_t* e, void* buff, std::size_t len) 
{ return net ? net->receive(e, buff, len) : NET_ERROR;            }
int AMS::net_send(NET* net, const void* buff, std::size_t size, const std::string& dest, unsigned int fac)
{ return net ? net->send(buff, size, dest, fac) : NET_ERROR;      }
int AMS::net_subscribe(NET* net, void* param, unsigned int fac, net_handler_t data, net_handler_t death)
{ return net ? net->subscribe(param,fac,data,death) : NET_ERROR;  }
int AMS::net_unsubscribe(NET* net, void* param, unsigned int fac)
{ return net ? net->unsubscribe(param,fac) : NET_SUCCESS;         }
void* AMS::net_lock(NET* net)
{ if(net) net->m_lock.lock();  return net ? &net->m_lock : 0;     }
void AMS::net_unlock(NET* net, void* lock)
{ if(net && lock == &net->m_lock ) net->m_lock.unlock();          }
void AMS::net_cancel(NET* net)
{ if ( net ) net->cancel();                                       }


using namespace AMS;
#define TRANSFER_USE_WT 1
#include <WT/wtdef.h>
#define TRANSFERTEST(x) test_ams_transfer_##x
#include <NET/TransferTest.h>
