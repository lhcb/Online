//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank, B.Jost
//
//==========================================================================

/// C/C++ include files
#include <cstdlib>
#include <cstring>
#include <cstdio>

/// Framework include files
#include <RTL/que.h>
#include <WT/wtdef.h>
#include <AMS/amsdef.h>

/// Local stuff in anonymous namespace
namespace  {

  //  #define _amsuc_printf printf
  static inline void _amsuc_printf(const char*, ...) {}

  struct amsu_fac_entry : public qentry_t {
    unsigned int     facility;
    amsuc_callback_t broadcast;
    amsuc_callback_t action;
    void*            param;
  }; 

  struct amsu_deadfac_entry : public qentry_t {
    char source[32];
    int  srclen;
    int  conv_fac;
  }; 

  struct _amsu_data  {
    amsu_fac_entry *scan_entry = 0;
    qentry_t *fac_list = 0;
    qentry_t *deadfac_list = 0;
    int inited = 0;
    
    void start_fac_scan()   {
      scan_entry = (amsu_fac_entry*)fac_list;
    }

    amsu_fac_entry *next_fac()    {
      amsu_fac_entry *entry = (amsu_fac_entry*)((long)scan_entry->next+(long)scan_entry);
      scan_entry = entry;
      return entry;
    }

    amsu_fac_entry *find_facility(unsigned int facility) {
      amsu_fac_entry *fac;
      for (fac = (amsu_fac_entry*)((long)fac_list->next+(long)fac_list);
	   fac != (amsu_fac_entry*)fac_list;
	   fac = (amsu_fac_entry*)((long)fac->next+(long)fac))
	if( fac->facility == facility ) return fac;
      return fac;
    }

    amsu_deadfac_entry *find_deadfacility(const char* source) {
      amsu_deadfac_entry *fac;
      int srclen = ::strlen(source);
      for (fac = (amsu_deadfac_entry*)((long)deadfac_list->next+(long)deadfac_list);
	   fac != (amsu_deadfac_entry*)deadfac_list ; 
	   fac = (amsu_deadfac_entry*)((long)fac->next+(long)fac))    {
	if (srclen==fac->srclen)      {
	  if (strncmp (fac->source, source, srclen) == 0)        {
	    break;
	  }
	}
      }
      return fac;
    }
  }  _amsu;
}

int amsuc_init( )   {
  if ( _amsu.inited ) return AMS_SUCCESS;
  _amsu.fac_list = new qentry_t(0,0);
  _amsu.deadfac_list = new qentry_t(0,0);
  _amsu.inited = 1;
  int status = ::wtc_init();
  if ( status != WT_SUCCESS )  {
    return(status);
  }
  status = ::wtc_subscribe(WT_FACILITY_AMS,0,amsuc_dispatch);
  return status != WT_SUCCESS ? status : AMS_SUCCESS;    
}

int amsuc_subscribe(unsigned int facility, amsuc_callback_t action, amsuc_callback_t broadcast, void* param)  {
  if ( !_amsu.inited ) return AMS_NOTINITED;
  amsu_fac_entry *fac = _amsu.find_facility(facility);
  if ( fac == (void*)_amsu.fac_list )   {
    fac = new amsu_fac_entry;
    if ( !fac ) return AMS_NOMEMORY;
    ::insqti( fac, _amsu.fac_list );        
  }
  fac->facility = facility;
  fac->action   = action;
  fac->broadcast= broadcast;
  fac->param    = param;
  return AMS_SUCCESS;    
}

int amsuc_subscribe_death(const char* source, unsigned int facility, void* /* param */)  {
  if ( !_amsu.inited ) return AMS_NOTINITED;
  amsu_deadfac_entry   *fac;
  int srclen = ::strlen(source);
  fac = _amsu.find_deadfacility(source);
  if ( fac == (void*)_amsu.deadfac_list )   {
    fac = new amsu_deadfac_entry;
    if ( !fac ) return AMS_NOMEMORY;
    ::insqti( fac, _amsu.deadfac_list );        
  }
  fac->conv_fac = facility;
  ::memset(fac->source,0,sizeof(fac->source));
  ::strncpy (fac->source, source,sizeof(fac->source));
  fac->source[sizeof(fac->source)-1] = 0;
  fac->srclen	= srclen;
  return AMS_SUCCESS;
}

int amsuc_remove(unsigned int facility) {
  if ( !_amsu.inited ) return AMS_NOTINITED;
  amsu_fac_entry *fac	= _amsu.find_facility(facility);
  if ( fac == _amsu.fac_list ) return AMS_ERROR;
  remqent( fac );
  delete fac;
  return AMS_SUCCESS;
}

int amsuc_remove_death( const char* source)   {
  if ( !_amsu.inited ) return AMS_NOTINITED;
  amsu_deadfac_entry* fac = _amsu.find_deadfacility(source);
  if ( fac == _amsu.deadfac_list )   {
    return AMS_NODATA;
  }
  remqent(fac);
  delete fac;
  return AMS_SUCCESS;    
}

int amsuc_dispatch( unsigned int, void* )  {
  amsuc_info info;
  size_t tlen;
  _amsuc_printf("%s: amsuc_dispatch: dispatch callback\n", amsc_name());
  info.length = sizeof(info.message);
  info.status = ::amsc_spy_next_message(info.message,
					&info.length,
					info.source,
					&info.facility,
					&tlen);
  _amsuc_printf("%s: amsuc_dispatch: status:%d source:%s facility:%d len:%d %s\n",
		amsc_name(), info.status, info.source, info.facility, tlen, info.message);
  if (info.status == AMS_SUCCESS)    {
    amsu_fac_entry* entry = _amsu.find_facility(info.facility);
    if (entry != _amsu.fac_list)    {
      if (entry->action)  {
	_amsuc_printf("%s: amsuc_dispatch: handle source: %s facility:%d len:%d\n",
		      amsc_name(), info.source, info.facility, tlen);
        /* int status = */ (*entry->action)(&info, entry->param);
	return AMS_SUCCESS;
      }
    }
    return AMS_ERROR;
  }
  else if (info.status == AMS_NOPEND) {
    return AMS_SUCCESS;
  }
  else if (info.facility == 0)    {
    amsu_deadfac_entry *deadentry = _amsu.find_deadfacility(info.source);
    info.status = AMS_SUCCESS;
    if (deadentry == (amsu_deadfac_entry*)_amsu.deadfac_list)  {
      _amsu.start_fac_scan();
      amsu_fac_entry* entry = _amsu.next_fac();
      while (entry != _amsu.fac_list)  {
        if (entry->broadcast)   {
          /* info.status = */ (*entry->broadcast)(&info, entry->param);
        }
        entry = _amsu.next_fac();
      }
    }
    else  {
      amsu_fac_entry* entry = _amsu.find_facility(deadentry->conv_fac);
      if (entry != _amsu.fac_list)  {
        if (entry->broadcast)  {
          unsigned int fac = info.facility;
          info.facility = deadentry->conv_fac;
          /* info.status = */ (*entry->broadcast)(&info,entry->param);
          info.facility = fac;
        }
      }
    }
    char dest[AMS_NAME_LENGTH];
    info.length = sizeof(info.message);
    ::amsc_read_message(info.message,&info.length,info.source,&info.facility,dest);
    return AMS_SUCCESS;
  }
  return info.status;
}

