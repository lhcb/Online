//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
#define DATATRANSFER_INTERNALS

#ifndef HAVE_ASIO_IPC
#define TRANSFER_NS BoostAsio

namespace BoostAsio  {
  typedef class TcpServer           TransferServer;
  typedef class TcpConnection       TransferConnection;
  typedef class AsyncTcpConnection  AsyncTransferConnection;
  typedef class TcpRequestHandler   TransferRequestHandler;
  typedef class TcpServerConfig     TransferServerConfig;
}
#endif

/// Framework include files
#include <NET/Transfer.h>
#include <RTL/netdef.h>

/// C/C++ include files
#include <string>
#include <thread>
#include <mutex>

#ifdef HAVE_ASIO_IPC
#include <ASIO/UnixServer.h>
#else
#include <ASIO/TcpServer.h>
#endif

namespace TRANSFER_NS  {
  class NetConnection;

  struct netentry_t   {
    NetConnection* connection { nullptr };
    NET*           sys        { nullptr };
    void*          data       { nullptr };
    std::string    name       { };
    netheader_t    header     { };
    sockaddr_in    addr;
    unsigned long  hash;
    unsigned long  _padding[16];
    netentry_t();
    netentry_t(NetConnection* c);
    netentry_t(netentry_t&& copy) = delete;
    netentry_t(const netentry_t& copy) = delete;
    ~netentry_t();
    netentry_t& operator=(netentry_t&& copy) = delete;
    netentry_t& operator=(const netentry_t& copy) = delete;
    NetErrorCode close();

    void _set_padding();
    void _check_padding()  const;
  };

  struct  NET  {
    
    struct Client {
      net_handler_t  data     { nullptr };
      net_handler_t  death    { nullptr };
      void*          param    { nullptr };
      unsigned int   fac      { 0 };
    };
    typedef std::lock_guard<std::mutex> netlock_t;
    typedef std::vector<Client> Clients;
    typedef std::map<unsigned long,netentry_t*> netdb_t;
    explicit NET(const std::string& proc);
    virtual ~NET();

    struct {
      std::string     name    {   };
      unsigned long   hash    { 0 };
      sockaddr_in     addr    {   };
      NET*            sys     { nullptr };
      netheader_t     header  {   };
    } m_me;
    NetConnectionType m_type       { NET_SERVER };
    netdb_t           m_db         { };
    std::mutex        m_lock       { };
    std::mutex        m_clientLock { };
    Clients           m_clients    { };
    TransferServer*   m_server     { nullptr };
    std::thread*      m_mgr        { nullptr };
    int               m_refCount   { 0 };
    int               m_cancelled  { 0 };
    int               m_stop       { 0 };
    int               m_async_read { 0 };

    void setSockOpts(TransferConnection::socket_t& chan);
    NetConnectionType type() const { return m_type; }
    netentry_t* connect(const std::string& dest);
    netentry_t* accept(NetConnection* connection, const netheader_t& header);
    NetErrorCode remove(netentry_t *e);
    void cancel();
    void sendShutdown(netentry_t *e);
    NetErrorCode disconnect(netentry_t *e);
    NetErrorCode init(NetConnectionType type, int nthreads);
    NetErrorCode handleMessage(netentry_t* entry, unsigned char* data);
    NetErrorCode receive(netentry_t* entry, void* data, std::size_t buffer_length);
    NetErrorCode send(const void* buff, size_t size, const std::string& dest, int fac);
    NetErrorCode subscribe(void* param, unsigned int fac, net_handler_t data, net_handler_t death);
    NetErrorCode unsubscribe(void* param, unsigned int fac);
    unsigned int release();
    void         handle_close(netentry_t* entry);
    netentry_t*  handle_data(NetConnection* conn, const netheader_t* header, unsigned char* data);
    static NET*  instance(const std::string& proc, int nthreads, NetConnectionType type);
  };
}

/// Framework include files
#include <RTL/rtl.h>
#include <RTL/hash.h>
#include <NET/defs.h>
#include <ASIO/Boost.h>
#include <ASIO/TanInterface.h>
#include <boost/bind/bind.hpp>

/// C/C++ include files
#include <map>
#include <cstdio>
#include <cerrno>
#include <memory>
#include <vector>
#include <stdexcept>

namespace TRANSFER_NS  {

  namespace asio = boost::asio;

  //static const int RECV_BUFFER_SIZE = 100*1024;
  //static const int SEND_BUFFER_SIZE = 100*1024;

  /// Network connection class using Boost::asio
  class NetConnection : public AsyncTransferConnection  {
    NET*        net    { nullptr };
  public:
    netentry_t* entry  { nullptr };
    std::string name   { };

    /// Construct a connection with the given io_context.
    explicit NetConnection(asio::io_context& io_context,
                           TransferRequestHandler::pointer_t& handler,
                           NET* network)
      : AsyncTransferConnection(io_context, handler, sizeof(netheader_t)), net(network)
    {
    }

    /// Default destructor
    virtual ~NetConnection()  {
      try  {
	auto* e = this->entry;
	this->entry = nullptr;
	if( e ) net->handle_close(e);
      }
      catch(const std::exception& e)  {
        ::lib_rtl_output(LIB_RTL_WARNING,"NetConnection(ASIO): %s %s: %s",
			 name.c_str(), "Error while closing connection", e.what());
      }
      catch(...)  {
        ::lib_rtl_output(LIB_RTL_WARNING,"NetConnection(ASIO): %s %s",
			 name.c_str(), "UNKNOWN error while closing connection");
      }
    }

    /// Handle completion of a asynchonous unlocked read/write operation.
    void handle_read(const error_t& error, std::size_t bytes_transferred)  override {
      if( !error )  {
        if( bytes_transferred > 0 )  {
          unsigned char* data = const_cast<unsigned char*>(m_recv_buffer.data());
          netheader_t*   head = reinterpret_cast<netheader_t*>(data);
          if( bytes_transferred < sizeof(netheader_t) )  {
            error_t ec;
            std::size_t len = asio::read(socket(),
                                    asio::buffer(data+bytes_transferred,sizeof(netheader_t)-bytes_transferred),
                                    ec);
            bytes_transferred += len;
            if( bytes_transferred != sizeof(netheader_t) )  {
              if( ec == boost::system::errc::success )  {
              }
              else if( ec == asio::error::eof )  {
                ::lib_rtl_output(net->m_cancelled ? LIB_RTL_DEBUG : LIB_RTL_ERROR,
                                 "ASIO: Recv EOF. Incomplete header. %d < %d bytes. %d [%s] Cancelled:%s %s",
                                 len, sizeof(netheader_t), ec.value(), ec.message().c_str(),
                                 RTL::yes_no(net->m_cancelled), this->entry ? this->entry->header.name : "");
                if( net->m_cancelled )  {
                  if( this->entry )  {
                    entry->header.msg_type = NET_CONNCLOSED;
                    net->handle_data(this, &entry->header, 0);
                  }
                  return;
                }
              }
              else if( ec )  {
                ::lib_rtl_output(net->m_cancelled ? LIB_RTL_DEBUG : LIB_RTL_ERROR,
                                 "ASIO: Recv ERROR. Incomplete header. %d < %d bytes. %d [%s] Cancelled:%s",
                                 len, sizeof(netheader_t), ec.value(), ec.message().c_str(),
                                 RTL::yes_no(net->m_cancelled));
              }
              return;
            }
          }
          if( net->m_async_read && head->size > 0 )  {
            if( m_reply_buffer.size() < head->size )  {
              m_reply_buffer.resize(head->size);
            }
            m_socket.async_read_some(asio::buffer(m_reply_buffer.data(),head->size),
                                     boost::bind(&TransferConnection::handle_additional_read,
                                                 shared_from_this(),
                                                 asio::placeholders::error,
                                                 asio::placeholders::bytes_transferred));
            return;
          }
          else  {
            netentry_t* e = nullptr;
            if( head->magic == NET_MAGIC )  {
              e = net->handle_data(this, head, nullptr);
              if( e ) this->entry = e;
            }
            else if( net->m_cancelled )  {
              goto Read_More;
            }
            else  {
              unsigned int buf;
	      std::size_t i = 0;
              int handled = 0;
              error_t ec;

              ::lib_rtl_output(LIB_RTL_ERROR,
                               "Transfer(ASIO): Retrieved unsolicited message magic: %08X",head->magic);
              const unsigned char* p = (const unsigned char*)(m_recv_buffer.data());
              for( ; i<sizeof(netheader_t); ++i, ++p)  {
                if( *(unsigned int*)p == NET_MAGIC )  {
                  break;
                }
              }
              if( *(unsigned int*)p == NET_MAGIC )  {
                *(unsigned int*)data = *(unsigned int*)p;
                std::size_t got_len  = sizeof(netheader_t) - i;
                ::memmove(data, p, got_len);
                std::size_t len = asio::read(socket(), asio::buffer(data+got_len, sizeof(netheader_t)-got_len), ec);
                if( !ec && len == sizeof(netheader_t) - got_len )  {
                  ::lib_rtl_output(LIB_RTL_ERROR,"Transfer(ASIO)(1): Late magic pattern found [%ld]", i);
                  handled = 1;
                  e = net->handle_data(this, head, nullptr);
                  if( e )  {
                    this->entry = e;
                  }
                }
              }
              /// We assume here data is at least 4 byte aligned.....
              std::size_t skipped = 0;
              for( ; 0 == handled; ++i )  {
                std::size_t len = asio::read(socket(), asio::buffer(&buf,sizeof(buf)), ec);
                skipped += sizeof(buf);
                if( !ec && len == sizeof(int) && buf == NET_MAGIC )  {
                  *(unsigned int*)data = buf;
                  std::size_t recv_len = sizeof(netheader_t)-sizeof(buf);
                  len = asio::read(socket(), asio::buffer(data+sizeof(buf), recv_len), ec);
                  if( len == sizeof(netheader_t) - sizeof(buf) )  {
                    ::lib_rtl_output(LIB_RTL_ERROR,"Transfer(ASIO)(2): "
                                     "Late magic pattern found [%ld %ld bytes skipped]", i, skipped);
                    handled = 1;
                    e = net->handle_data(this, head, 0);
                    if( e )  {
                      this->entry = e;
                    }
                  }
                  break;
                }
              }
            }
          }
        }
      Read_More:
        m_socket.async_read_some(asio::buffer(m_recv_buffer.data(),m_recvSize),
                                 boost::bind(&TransferConnection::handle_read,
                                             shared_from_this(),
                                             asio::placeholders::error,
                                             asio::placeholders::bytes_transferred));
      }
      else if( error == boost::system::errc::success )  {
      }
      else if( error == asio::error::eof )  {
        ::lib_rtl_output(net->m_cancelled ? LIB_RTL_DEBUG : LIB_RTL_VERBOSE,
                         "ASIO: Error (2) during receive %d [%s] Cancelled:%s %s",
                         error.value(), error.message().c_str(), RTL::yes_no(net->m_cancelled),
                         this->entry ? this->entry->header.name : "");
        if( this->entry )  {
          entry->header.msg_type = NET_CONNCLOSED;
          net->handle_data(this, &entry->header, 0);
        }
      }
      else  {
        // All error cases where the partner can be assumed dead.
        switch(error.value())  {
        case asio::error::connection_aborted:
        case asio::error::connection_reset:
        case asio::error::connection_refused:
        case asio::error::network_unreachable:
        case asio::error::network_down:
        case asio::error::network_reset:
        case asio::error::host_unreachable:
        case asio::error::not_connected:
        case asio::error::operation_aborted:
        case asio::error::shut_down:
        case asio::error::bad_descriptor:
          if( this->entry )  {
            net->sendShutdown(this->entry);
          }
          break;
        default:
          ::lib_rtl_output(LIB_RTL_ERROR,
                           "ASIO: Error (2) during receive %d [%s]",
			   error.value(), error.message().c_str());
        }
      }
    }

    /// Handle completion of a asynchonous unlocked read/write operation.
    void handle_additional_read(const error_t& error, std::size_t bytes_transferred)  override  {
      if( !error )  {
        if( bytes_transferred > 0 )  {
          unsigned char* data = const_cast<unsigned char*>(m_recv_buffer.data());
          netheader_t*   head = reinterpret_cast<netheader_t*>(data);
          data = m_reply_buffer.data();
          if( bytes_transferred < head->size )  {
            error_t ec;
            std::size_t len = asio::read(socket(),
                                    asio::buffer(data+bytes_transferred,
                                                 head->size-bytes_transferred),
                                    ec);
            if( len != head->size-bytes_transferred )  {
            }
          }
          net->handle_data(this, head, data);
        }
        m_socket.async_read_some(asio::buffer(m_recv_buffer.data(),m_recvSize),
                                 boost::bind(&TransferConnection::handle_read,
                                             shared_from_this(),
                                             asio::placeholders::error,
                                             asio::placeholders::bytes_transferred));
      }
    }
  };

  class NetServerConfig : public TransferServerConfig  {
    NET* net;
  public:
    NetServerConfig(TransferServer* s, NET* n) : TransferServerConfig(s), net(n) {}
    virtual ~NetServerConfig() {}
    /// Create a new blank server connection
    TransferConnection* newConnection()   override {
      return new NetConnection(io_context(), handler, net);
    }
  };
}

using namespace TRANSFER_NS;

namespace {
  inline std::pair<NET*,std::mutex>& locked_instance()  {
    static std::pair<NET*,std::mutex> inst;
    return inst;
  }
  //#define _net_printf printf
  //inline void _net_printf(const char*, ...) {}
}

//----------------------------------------------------------------------------------
netentry_t::netentry_t() : connection(nullptr)  {
  _set_padding();
}

//----------------------------------------------------------------------------------
netentry_t::netentry_t(NetConnection* c) : connection(c)  {
  _set_padding();
}

//----------------------------------------------------------------------------------
netentry_t::~netentry_t()   {
  _check_padding();
}

//----------------------------------------------------------------------------------
void netentry_t::_set_padding()  {
  unsigned long* p = this->_padding;
  std::size_t words = sizeof(this->_padding[0]);
  for( std::size_t i=0; i<words; ++i )  {
    p[i] = 0xFEEDBABE00000000UL + i;
  }
}

//----------------------------------------------------------------------------------
void netentry_t::_check_padding()  const  {
  const unsigned long* p = this->_padding;
  std::size_t words = sizeof(this->_padding[0]);
  for( std::size_t i=0; i<words; ++i )  {
    if( p[i] != (0xFEEDBABE00000000UL + i) )   {
      constexpr static const char* text = "Memory corruption detected in netentry destructor!\n";
      ::write(STDOUT_FILENO, text, sizeof(text));
    }
  }
}

//----------------------------------------------------------------------------------
NetErrorCode netentry_t::close()  {
  boost::system::error_code ec;
  try {
    _check_padding();
    if( this->connection )  {
      auto* con = this->connection;
      if( con )  {
	con->entry = nullptr;
	this->connection = nullptr;
	TransferConnection::socket_t& chan = con->socket();
	if( chan.is_open() )  {
	  chan.set_option(asio::socket_base::linger(true, 0), ec);
	  chan.shutdown(TransferConnection::socket_t::shutdown_both, ec);
	  chan.close(ec);
	}
      }
    }
  }
  catch(const std::exception& e)  {
    ::lib_rtl_output(LIB_RTL_ERROR,
                     "Transfer(ASIO): Exception while closing netentry: %s",
                     e.what());
  }
  catch(...)  {
    ::lib_rtl_output(LIB_RTL_ERROR,
                     "Transfer(ASIO): Exception while closing netentry.");
  }
  return NET_SUCCESS;
}

//----------------------------------------------------------------------------------
NET::NET(const std::string& proc)
{
  m_me.sys  = this;
  m_me.name = proc;
  m_me.hash = RTL::hash32(m_me.name.c_str());
  m_me.header.hash = m_me.hash;
  std::strncpy(m_me.header.name, proc.c_str(), sizeof(m_me.header.name)-1);
  m_me.header.name[sizeof(m_me.header.name)-1] = 0;
  m_me.addr.sin_port = 0;
  m_me.addr.sin_family = AF_INET;
  m_me.addr.sin_addr.s_addr = INADDR_ANY; //IN_CLASSA_HOST; //
}

//----------------------------------------------------------------------------------
NET::~NET() {
  m_stop = 1;
  netlock_t client_lock(m_clientLock);
  {
    netlock_t lock(m_lock);
    boost::system::error_code ec;
    for( const auto& i : m_db )  {
      netentry_t* e = i.second;
      if( e->connection )  {
        TransferConnection::socket_t& chan = e->connection->socket();
        e->connection->entry = nullptr;
        if( chan.is_open() )  {
	  asio::write(chan, asio::buffer(&m_me.header, sizeof(netheader_t)), ec);
        }
      }
    }
  }
  if( m_server )  {
    m_server->handleStop();
  }
  if( m_mgr )  {
    m_mgr->join();
    delete m_mgr;
    m_mgr = 0;
  }
  if( m_server )  {
    delete m_server;
    m_server = 0;
  }
  m_clients.clear();
  {
    netlock_t lock(m_lock);
    for( auto& i : m_db )  {
      netentry_t* e = i.second;
      i.second = nullptr;
      if( e )  {
        e->close();
        delete e;
      }
    }
    m_db.clear();
  }
#ifdef HAVE_ASIO_IPC
  std::string unix_socket = "/tmp/asio_ipc_" + m_me.name;
  std::remove(unix_socket.c_str());
#else
  if( m_me.addr.sin_port )  {
    ::tan_deallocate_port_number(m_me.name.c_str());
    m_me.addr.sin_addr.s_addr = 0;
    m_me.addr.sin_port = 0;
  }
#endif
  m_me.header.size = 0;
  m_me.header.facility = 0;
  m_me.header.msg_type = NET_CONNCLOSED;
  m_cancelled = false;
}

//----------------------------------------------------------------------------------
NET* NET::instance(const std::string& proc, int nthreads, NetConnectionType type)  {
  auto& inst = locked_instance();
  netlock_t instance_lock(inst.second);
  NET*& ptr = inst.first;
  if( 0 == ptr )  {
    auto conn = std::make_unique<NET>(proc);
    NetErrorCode status = conn->init(type,nthreads);
    if( status == NET_ADDRINUSE )  {
      throw std::runtime_error("Transfer: Cannot initialize network worker [Address already in use]");
    }
    else if( status != NET_SUCCESS )  {
      throw std::runtime_error("Transfer: Cannot initialize network worker.");
    }
    ptr = conn.release();
  }
  else if( ptr->type() == NET_CLIENT && type == NET_SERVER )  {
    NetErrorCode status = ptr->init(type,nthreads);
    if( status != NET_SUCCESS )  {
      throw std::runtime_error("Transfer: Cannot initialize network server.");
    }
  }
  ptr->m_refCount++;
  return ptr;
}

//----------------------------------------------------------------------------------
unsigned int NET::release()  {
  auto& inst = locked_instance();
  netlock_t  instance_lock(inst.second);
  if( inst.first )  {
    unsigned int cnt = --inst.first->m_refCount;
    if( cnt == 0 )  {
      delete inst.first;
      inst.first = 0;
      return cnt;
    }
    return cnt;
  }
  return 0;
}

//----------------------------------------------------------------------------------
NetErrorCode NET::init(NetConnectionType type, int nthreads)  {
  // Only initialize if needed!
#ifdef HAVE_ASIO_IPC
  type = NET_SERVER;
#endif
  if( m_me.addr.sin_port == 0 )  {
    m_type = type;
    char port[1024];
    if( type == NET_SERVER )  {
      m_cancelled = false;
      try {
#ifdef HAVE_ASIO_IPC
        std::snprintf(port,sizeof(port),"/tmp/asio_ipc_%s",m_me.name.c_str());
        std::remove(port);
        auto srv = std::make_unique<TransferServer>(port, nthreads);
#else
        char conn[64];
	int  sock = ::net_bind_free_server_port(&m_me.addr);
        std::snprintf(conn, sizeof(conn), "0.0.0.0");
        std::snprintf(port, sizeof(port), "%d", htons(m_me.addr.sin_port));

	if( sock < 0 )  {
          ::lib_rtl_output(LIB_RTL_ERROR, "FAILED to bind port. Status %d", errno);
	  ::close(sock);
          errno = ENOTCONN;
          return NET_ERROR;
	}
	int status = ::tan_declare_port_number(m_me.name.c_str(), m_me.addr.sin_port);
        if( status != TAN_SS_SUCCESS )  {
          ::lib_rtl_output(LIB_RTL_ERROR, "FAILED to allocate port number. Status %d", status);
	  ::close(sock);
          errno = ENOTCONN;
          return NET_ERROR;
        }
	::close(sock);
        auto srv = std::make_unique<TransferServer>(conn, port, nthreads);
#endif
        srv->config = TransferServer::config_t(new NetServerConfig(srv.get(), this));
        srv->config->setHandler(0);
        srv->config->recvSize = sizeof(netheader_t);
        srv->start();
        m_server = srv.release();
        m_mgr = new std::thread([this]{ this->m_server->join(); });
      }
      catch(const std::exception& e)  {
        ::lib_rtl_output(LIB_RTL_WARNING,"Transfer(ASIO): Failed to start NET "
                         "message pump. [%s] Port:%s\n",e.what(), port);
        if( ::strstr(e.what(), "Address already in use") )  {
          errno = EADDRINUSE;
	  return NET_ADDRINUSE;
        }
        return NET_ERROR;
      }
      return NET_SUCCESS;
    }
    else  {
#ifndef HAVE_ASIO_IPC
      auto srv = std::make_unique<TransferServer>("","",1);
      srv->config = TransferServer::config_t(new NetServerConfig(srv.get(),this));
      srv->config->setHandler(0);
      srv->config->recvSize = sizeof(netheader_t);
      m_server = srv.release();
      m_mgr = nullptr;
      m_mgr = new std::thread([this]{ this->m_server->join(); });
#endif
    }
  }
  return NET_SUCCESS;
}

//----------------------------------------------------------------------------------
void NET::cancel()  {
  m_cancelled = 1;
  if( m_server )  {
    //::lib_rtl_output(LIB_RTL_ERROR,"Transfer(ASIO): Stop server.....\n");
    //m_server->handleStop();
  }
}

//----------------------------------------------------------------------------------
NetErrorCode NET::remove(netentry_t *e)  {
  netdb_t::iterator i = m_db.find(e->hash);
  if( i != m_db.end() )  {
    m_db.erase(i);
    return NET_SUCCESS;
  }
  return NET_TASKNOTFOUND;
}

//----------------------------------------------------------------------------------
void NET::setSockOpts(TransferConnection::socket_t& chan)  {
  if( chan.is_open() )  {
#ifdef HAVE_ASIO_IPC
    //boost::system::error_code ec;
    //chan.set_option(asio::socket_base::send_buffer_size(SEND_BUFFER_SIZE), ec);
#else
    boost::system::error_code ec;
    chan.set_option(asio::socket_base::reuse_address(true), ec);
    chan.set_option(asio::socket_base::linger(true, 0), ec);
    //chan.set_option(asio::socket_base::receive_buffer_size(RECV_BUFFER_SIZE), ec);
    //chan.set_option(asio::socket_base::send_buffer_size(SEND_BUFFER_SIZE), ec);
#endif
  }
}

//----------------------------------------------------------------------------------
netentry_t* NET::accept(NetConnection* connection, const netheader_t& header)  {
  netentry_t* e = nullptr;  {
    netlock_t lock(m_lock);
    netdb_t::iterator i = m_db.find(header.hash);
    e = (i == m_db.end()) ? nullptr : i->second;
  }
  if( e == nullptr )  {
    if( header.magic != NET_MAGIC && !m_cancelled )  {
      ::lib_rtl_output(LIB_RTL_ERROR,
                       "Transfer(ASIO): Retrieved unsolicited message magic: %08X",
		       header.magic);
      return nullptr;
    }
    e = new netentry_t(connection);
#ifndef HAVE_ASIO_IPC
    int       fd = connection->socket().native_handle();
    socklen_t addr_len = sizeof(e->addr);
    bool      cancelled = m_cancelled;
    m_cancelled = false;
    int sc = ::getpeername(fd, (struct sockaddr*)&e->addr, &addr_len);
    if( sc == -1 || cancelled )  {
      return nullptr;
    }
#if 0
    int sc = ::tan_get_address_by_name(header.name, &e->addr);
    if( cancelled && sc == TAN_SS_TASKNOTFOUND )  {
      return nullptr;
    }
    else if( sc != TAN_SS_SUCCESS )  {
      if( !cancelled )  {
        ::lib_rtl_output(LIB_RTL_ERROR,
                         "Transfer(ASIO): Retrieved unsolicited message from %s.",
                         header.name);
      }
      return nullptr;
    }
#endif
#endif // HAVE_ASIO_IPC
    
    setSockOpts(connection->socket());
    e->sys    = this;
    e->name   = header.name;
    e->hash   = header.hash;
    e->header = header;   {
      netlock_t lock(m_lock);
      m_db[e->hash] = e;
    }
    return e;
  }
  else  {
    if( e->connection != connection )  {
      connection->entry    = e;
      connection->name     = e->name;
      e->connection->entry = 0;
      e->connection = connection;
      setSockOpts(e->connection->socket());
    }
  }
  return e;
}

//----------------------------------------------------------------------------------
netentry_t *NET::connect(const std::string& dest)  {
  sockaddr_in   addr;
  ::memset(&addr,0,sizeof(addr));
#ifdef HAVE_ASIO_IPC
  std::string destination = "/tmp/asio_ipc_" + dest;
  int sc = TAN_SS_SUCCESS;
#else
  int sc = ::tan_get_address_by_name(dest.c_str(),&addr);
#endif
  if( sc == TAN_SS_SUCCESS )  {
    try  {
      int retry = 4;
      TransferConnection::pointer_t c(m_server
                                      ->config
                                      ->newConnection());
      NetConnection* conn = (NetConnection*)c.get();
      TransferConnection::socket_t& chan = c->socket();
      for(retry=4; retry > 0; --retry )  {
        try  {
#ifdef HAVE_ASIO_IPC
          conn->connect(destination);
#else
          conn->connect(inet_ntoa(addr.sin_addr), addr.sin_port);
#endif
          break;
        }
        catch(const std::exception& e)  {
          ::lib_rtl_sleep(1000);
        }
      }
      if( retry <= 0 )  {
        ::lib_rtl_output(LIB_RTL_OS,"Transfer(ASIO): Failed to connect to %s.",dest.c_str());
        return nullptr;
      }
      setSockOpts(chan);
      auto entry = std::make_unique<netentry_t>(conn);
      entry->sys  = this;
      entry->name = dest;
      entry->addr = addr;
      entry->hash = RTL::hash32(entry->name.c_str());
      entry->header.hash = entry->hash;
      entry->header.size = 0;
      entry->header.facility = 0;
      ::strncpy(entry->header.name, entry->name.c_str(), sizeof(entry->header.name)-1);
      entry->header.name[sizeof(entry->header.name)-1] = 0;
      conn->name = entry->name;
      conn->start();
      netheader_t h = m_me.header;
      h.size = 0;
      h.facility = 0;
      h.msg_type = NET_MSG_HELLO;
      std::size_t len = asio::write(chan, asio::buffer(&h, sizeof(netheader_t)));
      if( len == sizeof(netheader_t) )  {
        m_db[entry->hash] = entry.get();
        return entry.release();
      }
    }
    catch(const std::exception& e)  {
      ::lib_rtl_output(LIB_RTL_WARNING,
                       "Transfer(ASIO): Failed to connect to destination: %s. [%s]",
                       dest.c_str(), e.what());
    }
  }
  return nullptr;
}

//----------------------------------------------------------------------------------
NetErrorCode NET::disconnect(netentry_t *e)  {
  if( e )   {
    std::unique_ptr<netentry_t> entry {e};
    netdb_t::iterator i=m_db.find(e->hash);
    if( i != m_db.end() )  {
      m_db.erase(i);
    }
    entry->close();
  }
  return NET_SUCCESS;
}

//----------------------------------------------------------------------------------
void NET::sendShutdown(netentry_t *entry)  {
  unsigned long hash = entry->hash;
  try {
    netlock_t lock(m_clientLock);
    entry->header.size     = 0;
    entry->header.facility = 0;
    entry->header.msg_type = NET_TASKDIED;
    for(const auto& cl : m_clients )
      if( cl.death ) cl.death(entry->header, cl.param, entry);
  }
  catch(const std::exception& e)  {
    ::lib_rtl_output(LIB_RTL_ERROR,
                     "Transfer(ASIO): Exception during death handling: %s",
                     e.what());
  }
  catch(...)  {
    ::lib_rtl_output(LIB_RTL_ERROR,
                     "Transfer(ASIO): Exception during death handling.");
  }
  try {
    netlock_t lock(m_lock);
    netdb_t::iterator i = m_db.find(hash);
    if( i != m_db.end() )  {
      this->disconnect(i->second);
    }
  }
  catch(...)  {
    ::lib_rtl_output(LIB_RTL_ERROR,
                     "Transfer(ASIO): Exception during disconnection handling.");
  }
}

//----------------------------------------------------------------------------------
netentry_t* NET::handle_data(NetConnection* connection, const netheader_t* header, unsigned char* data)  {
  switch(header->msg_type)  {
  case NET_MSG_DATA:  {
    netentry_t* entry = this->accept(connection, *header);
    if( entry )  {
      entry->header.size     = header->size;
      entry->header.facility = header->facility;
      entry->header.msg_type = header->msg_type;
      connection->name  = header->name;
      connection->entry = entry;
      handleMessage(entry, data);
    }
    return entry;
  }
  case NET_CONNCLOSED:  {
    netdb_t::iterator i = m_db.find(header->hash);
    if( i != m_db.end() )  {
      this->sendShutdown((*i).second);
    }
    connection->entry = 0;
    return nullptr;
  }
  case NET_MSG_HELLO:  {
    netentry_t* entry = this->accept(connection, *header);
    connection->name  = header->name;
    connection->entry = entry;
    return entry;
  }
  case NET_MSG_TERMINATE:
    m_stop = 1;
    return nullptr;

  default:
    return nullptr;
  }
}

//----------------------------------------------------------------------------------
NetErrorCode NET::handleMessage(netentry_t* entry, unsigned char* ptr) {
  netlock_t cllock(m_clientLock);
  try  {
    entry->data = ptr;
    for( const auto& cl : m_clients )  {
      if( cl.data && cl.fac == entry->header.facility ) {
	cl.data(entry->header, cl.param, entry);
      }
    }
    return NET_SUCCESS;
  }
  catch(const std::exception& e)  {
    ::lib_rtl_output(LIB_RTL_WARNING,"handleMessage(ASIO): Error handling message: %s", e.what());
  }
  catch(...)  {
    ::lib_rtl_output(LIB_RTL_WARNING,"handleMessage(ASIO): UNKNOWN error handling message.");
  }
  return NET_SUCCESS; // Show must go on....
}

//----------------------------------------------------------------------------------
void NET::handle_close(netentry_t* entry)  {
  try  {
    if( entry )  {
      ::lib_rtl_output(LIB_RTL_DEBUG,"Transfer(ASIO): Closing connection to: %s",
		       entry->name.c_str());
      NET::netlock_t lock(this->m_lock);
      this->disconnect(entry);
    }
  }
  catch(const std::exception& e)  {
    ::lib_rtl_output(LIB_RTL_ERROR,"handle_close(ASIO): Error handling close: %s", e.what());
  }
  catch(...)  {
    ::lib_rtl_output(LIB_RTL_ERROR,"handle_close(ASIO): UNKNOWN error handling close.");
  }
}

//----------------------------------------------------------------------------------
NetErrorCode NET::send(const void* buff, std::size_t size, const std::string& dest, int facility)  {
  if(size > 0)  {
    unsigned int hash = RTL::hash32(dest.c_str());
    netentry_t *entry = nullptr;
    try {
      boost::system::error_code ec;
      netheader_t h;  {
        netlock_t lock(m_lock);
        netdb_t::iterator i = m_db.find(hash);
        entry = (i != m_db.end()) ? i->second : nullptr;
        if( !entry ) {
          if( !(entry=this->connect(dest)) ) {
            return NET_TASKNOTFOUND;
          }
        }
      }
      h = m_me.header;
      h.size = size;
      h.facility = facility;
      h.msg_type = NET_MSG_DATA;
      std::vector<asio::const_buffer> msg;
      TransferConnection::socket_t& chan = entry->connection->socket();
      msg.push_back(asio::buffer(&h, sizeof(netheader_t)));
      msg.push_back(asio::buffer(buff, size));
      std::size_t nbytes = asio::write(chan, msg, ec);
      if( nbytes == sizeof(h) + size )  {
        return NET_SUCCESS;
      }
      if( ec )  {
        ::lib_rtl_output(LIB_RTL_INFO,
                         "Transfer(ASIO): Failed to send message to %s. [%d: %s]",
                         dest.c_str(), ec.value(), ec.message().c_str());
        {
          netlock_t lock(m_lock);
          netdb_t::iterator i = m_db.find(hash);
          if( i != m_db.end() )  {
            entry = i->second;
            if( entry->connection )  {
              entry->connection->entry = nullptr;
              entry->connection = nullptr;
            }
            m_db.erase(i);
            delete entry;
          }
        }
      }
    }
    catch(const std::exception& e)  {
      ::lib_rtl_output(LIB_RTL_INFO  /* LIB_RTL_OS */,
                       "Transfer(ASIO): Failed to send message to %s. [%s]",
                       dest.c_str(), e.what());
    }
    try  {
      netlock_t lock(m_lock);
      netdb_t::iterator i = m_db.find(hash);
      entry = (i != m_db.end()) ? i->second : nullptr;
      if( entry ) this->disconnect(entry);
    }
    catch(const std::exception& e)  {
      ::lib_rtl_output(LIB_RTL_INFO,
                       "Transfer(ASIO): %s Failed to close connection. [%s]",
                       dest.c_str(), e.what());
    }
    return NET_CONNCLOSED;
  }
  return NET_ILLEGAL_LENGTH;
}

//----------------------------------------------------------------------------------
NetErrorCode NET::receive(netentry_t* entry, void* buffer, std::size_t len)  {
  if( !entry )  {
    return NET_ERROR;
  }
  if( (buffer || len) && (len < entry->header.size) )   {
    ::lib_rtl_output(LIB_RTL_OS,
		     "Transfer(ASIO): %s Inconsistent buffer [%p] length [%ld <> %ld]. Throw data away.",
		     entry->name.c_str(), buffer, len, entry->header.size);
    return NET_ERROR;
  }
  if( !entry->data && (entry->header.size > 0) )  {
    try  {
      const netheader_t& header = entry->header;
      TransferConnection::socket_t& chan = entry->connection->socket();
      if( chan.is_open() )  {
        boost::system::error_code ec;
        if( buffer && len >= header.size )  {
          std::size_t read_len = asio::read(chan, asio::buffer(buffer, header.size), ec);
          return (read_len == header.size) ? NET_SUCCESS : NET_ERROR;
        }
        std::vector<unsigned char> buff;
        buff.resize(header.size);
        // asio::read is used to read a certain number of bytes of data from a stream.
        // The call will block until one of the following conditions is true:
        // -- The supplied buffers are full. That is, the bytes transferred is equal to
        //    the sum of the buffer sizes.
        // -- An error occurred.
        // Hence: No need to loop until all bytes are received.
        std::size_t read_len = asio::read(chan, asio::buffer(buff.data(), header.size), ec);
        return (read_len == header.size) ? NET_SUCCESS : NET_ERROR;
      }
    }
    catch(const std::exception& e)  {
      ::lib_rtl_output(LIB_RTL_OS,
		       "Transfer(ASIO): %s Exception receiving data [%s]",
                       entry->name.c_str(), e.what());
    }
    catch(...)  {
      ::lib_rtl_output(LIB_RTL_OS,
		       "Transfer(ASIO): %s UNKNOWN exception receiving data",
                       entry->name.c_str());
    }
    return NET_ERROR;
  }
  ::memcpy(buffer, entry->data, len);
  if( len != entry->header.size )  {
    ::lib_rtl_output(LIB_RTL_ERROR,
		     "Transfer(ASIO): %s Inconsistent data buffer length: %ld <-> %ld bytes (header)",
		     entry->name.c_str(), len, entry->header.size);
  }
  return NET_SUCCESS;
}
//----------------------------------------------------------------------------------
NetErrorCode NET::subscribe(void* param, unsigned int fac, net_handler_t data, net_handler_t death) {
  netlock_t cllock(m_clientLock);
  for( auto& cl : m_clients )  {
    if( cl.fac == fac && cl.param == param ) {
      cl.data  = data;
      cl.death = death;
      return NET_SUCCESS;
    }
  }
  m_clients.push_back(Client());
  m_clients.back().death = death;
  m_clients.back().data  = data;
  m_clients.back().param = param;
  m_clients.back().fac   = fac;
  return NET_SUCCESS;
}

//----------------------------------------------------------------------------------
NetErrorCode NET::unsubscribe(void* param, unsigned int fac) {
  netlock_t cllock(m_clientLock);
  for(Clients::iterator i=m_clients.begin(); i != m_clients.end(); ++i)  {
    if( i->fac == fac && i->param == param ) {
      m_clients.erase(i);
      return NET_SUCCESS;
    }
  }
  return NET_SUCCESS;
}

//----------------------------------------------------------------------------------
NET*  TRANSFER_NS::net_init(const std::string& proc, int nthreads, NetConnectionType type)
{ return NET::instance(proc, nthreads, type);                          }
void  TRANSFER_NS::net_close(NET* net)
{ if( net ) net->release();                                            }
int   TRANSFER_NS::net_send(NET* net, const void* buff, std::size_t size, const std::string& dest, unsigned int fac)
{ return net ? net->send(buff,size,dest,fac) : NET_ERROR;              }
int   TRANSFER_NS::net_receive(NET* net, netentry_t* entry, void* buff, std::size_t len)
{ return net ? net->receive(entry, buff, len) : NET_ERROR;             }
int   TRANSFER_NS::net_subscribe(NET* net, void* param, unsigned int fac, net_handler_t data, net_handler_t death)
{ return net ? net->subscribe(param, fac, data, death) : NET_ERROR;    }
int   TRANSFER_NS::net_unsubscribe(NET* net, void* param, unsigned int fac)
{ return net ? net->unsubscribe(param, fac) : NET_SUCCESS;             }
void* TRANSFER_NS::net_lock(NET* net)
{ if( net ) net->m_lock.lock(); return net ? net : 0;                  }
void  TRANSFER_NS::net_unlock(NET* net, void* lock)
{ if( net && lock ) net->m_lock.unlock();                              }
void  TRANSFER_NS::net_cancel(NET* net)
{ if( net ) net->cancel();                                             }

#ifdef HAVE_ASIO_IPC
#define TRANSFERTEST(x) test_asio_unix_ipc_##x
std::string TRANSFER_NS::net_self(const std::string&, const std::string& process) { return process; }
#else
#define TRANSFERTEST(x) test_asio_net_##x
std::string TRANSFER_NS::net_self(const std::string& node, const std::string& process)
{ return node + "::" + process;                                    }
#endif
#include "NET/TransferTest.h"
