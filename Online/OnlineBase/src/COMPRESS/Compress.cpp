//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : Storage
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include <RTL/Compress.h>
#include <RTL/Datafile.h>
#include <RTL/rtl.h>

// C/C++ include files
#include <system_error>
#include <sstream>
#include <cstring>
#include <cstdint>
#include <cerrno>
#include <zlib.h>

#define CHUNK 0x4000
#define ASSUMED_COMPRESSION  3

namespace {
  static const std::string base64_chars = 
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    "abcdefghijklmnopqrstuvwxyz"
    "0123456789+/";
  
  static inline bool is_base64(uint8_t c) {
    return (isalnum(c) || (c == '+') || (c == '/'));
  }

  int _get_compression_level(int level)  {
    if ( level == Online::compress::COMPRESSION_LEVEL_DEFAULT )
      level = 4;
    else if ( level == Online::compress::COMPRESSION_LEVEL_FAST )
      level = 1;
    else if ( level == Online::compress::COMPRESSION_LEVEL_MEDIUM )
      level = 4;
    else if ( level == Online::compress::COMPRESSION_LEVEL_BEST )
      level = 9;
    return level;
  }

  /// ZLIB compression: Check first 3 bytes as described in RFC 1950
  /// If YES: on the fly uncompress the stream
  bool is_gzip_data_buffer(const uint8_t* hdr)   {
    return ( hdr[0] == 0x1f && hdr[1] == 0x8b && hdr[2] == Z_DEFLATED  );
  }
    
  /// Check ZSTD magic word (see RFC 8878 for details)
  bool is_zstd_data_buffer(const uint8_t* hdr)   {
    // return ( *(uint32_t*)hdr == 0xFD2FB528 );
    return ( hdr[0] == 0x28 && hdr[1] == 0xB5 && hdr[2] == 0x2f && hdr[3] == 0xfd );
  }
    
  /// Check 7-zip/XZ file header: lzma compression
  /// (see https://py7zr.readthedocs.io/en/latest/archive_format.html for details)
  bool is_xz_data_buffer(const uint8_t* hdr)   {
    return ( hdr[0] == 0xFD && hdr[1] == '7' && hdr[2] == 'z' &&
	     hdr[3] == 'X'  && hdr[4] == 'Z' && hdr[5] == 0 );
  }
    
  /// Check LZ4 magic word in first 4 bytes
  /// (See https://github.com/lz4/lz4/blob/dev/doc/lz4_Frame_format.md)
  bool is_lz4_data_buffer(const uint8_t* hdr)   {
    // return ( *(uint32_t*)hdr == 0x184D2204 );
    return ( hdr[0] == 0x04 && hdr[1] == 0x22 && hdr[2] == 0x4d && hdr[3] == 0x18 );
  }

}

#if defined(ONLINE_HAVE_LZ4)
#include <lz4.h>
#include <lz4frame.h>

/// Namespace for the Online utilities
namespace Online  {
  /// Namespace for the Online compression utilities
  namespace compress  {

    namespace  {
      std::size_t posix_lz4_block_size(const LZ4F_frameInfo_t* info) {
	switch (info->blockSizeID) {
	case LZ4F_default:
	case LZ4F_max64KB:  return 1 << 16;
	case LZ4F_max256KB: return 1 << 18;
	case LZ4F_max1MB:   return 1 << 20;
	case LZ4F_max4MB:   return 1 << 22;
	default:
	  ::lib_rtl_output(LIB_RTL_ERROR,"Impossible with expected frame specification (<=v1.6.1)");
	  exit(1);
	}
      }
    }
    std::vector<uint8_t> decompress_lz4(const uint8_t* buff, std::size_t len) {
      std::vector<uint8_t> buffer, result;
      ::LZ4F_dctx* decompress = nullptr;
      uint64_t status = ::LZ4F_createDecompressionContext(&decompress, LZ4F_VERSION);
      if ( ::LZ4F_isError(status) ) {
	lib_rtl_output(LIB_RTL_ERROR, "LZ4: LZ4F_dctx creation error: %s", ::LZ4F_getErrorName(status));
	return {};
      }
      LZ4F_frameInfo_t info;
      std::size_t consumed = len;
      status = ::LZ4F_getFrameInfo(decompress, &info, buff, &consumed);
      if ( ::LZ4F_isError(status) )  {
	lib_rtl_output(LIB_RTL_ERROR, "LZ4: LZ4F_getFrameInfo error: %s", ::LZ4F_getErrorName(status));
	return {};
      }
      std::size_t block_size = posix_lz4_block_size(&info);
      buffer.resize(block_size);
      for( const uint8_t *in_buf=buff+consumed, *end=(in_buf+len-consumed); in_buf < end; )  {
	uint64_t in_size  = end - in_buf;
	uint64_t out_size = buffer.size();
	uint64_t ret = ::LZ4F_decompress(decompress,
					 &buffer.at(0),
					 &out_size,
					 in_buf,
					 &in_size,
					 /* LZ4F_decompressOptions_t */ nullptr);
	if ( ::LZ4F_isError(ret) )  {
	  lib_rtl_output(LIB_RTL_ERROR, "LZ4: Decompression error: %s", ::LZ4F_getErrorName(ret));
	  return {};
	}
	in_buf += in_size;
	if ( out_size > 0 )  {
	  std::copy(&buffer.at(0), &buffer.at(0)+out_size, std::back_inserter(result));
	}
      }
      ::LZ4F_freeDecompressionContext(decompress);
      return result;
    }
    std::vector<uint8_t> compress_lz4(const uint8_t* buff, std::size_t len, int level, int /* strategy */)  {
      static LZ4F_preferences_t prefs = {
	{ LZ4F_max256KB,
	  LZ4F_blockLinked,
	  LZ4F_noContentChecksum,
	  LZ4F_frame,
	  0 /* unknown content size */,
	  0 /* no dictID            */ ,
	  LZ4F_noBlockChecksum },
	level,      /* compression level; 0 == default */
	0,          /* autoflush */
	0,                       /* favor decompression speed */
	{ 0, 0, 0 },             /* reserved, must be set to 0 */
      };
      ::LZ4F_cctx* stream = nullptr;
      std::vector<uint8_t> result, buffer;
      std::size_t status = ::LZ4F_createCompressionContext(&stream, LZ4F_VERSION);
      if ( ::LZ4F_isError(status) ) {
	lib_rtl_output(LIB_RTL_ERROR, "LZ4: FAILED to initialize compressor: %ld [%s]",
		       status, ::LZ4F_getErrorName(status));
	return {};
      }
      status = ::LZ4F_compressBound(16*1024, &prefs);  // large enough for any input <= this->in_size
      buffer.resize(status);
      // write frame header
      status = ::LZ4F_compressBegin(stream, &buffer.at(0), buffer.size(), &prefs);
      if ( ::LZ4F_isError(status) ) {
	lib_rtl_output(LIB_RTL_ERROR, "LZ4: Failed to start compression: error %ld [%s]",
		       status, ::LZ4F_getErrorName(status));
	::LZ4F_freeCompressionContext(stream);
	return {};
      }
      std::copy(&buffer.at(0), &buffer.at(0)+status, std::back_inserter(result));
      for(long done=0, out_now=0, todo=len; done < todo; done += out_now )  {
	out_now = std::min(buffer.size(), len-done);
	status  = ::LZ4F_compressUpdate(stream,
					&buffer.at(0), buffer.size(), 
					buff + done,
					out_now,
					nullptr);
	if ( ::LZ4F_isError(status) )  {
	  lib_rtl_output(LIB_RTL_ERROR, "LZ4: Compression FAILED: error %ld [%s]",
			 status, ::LZ4F_getErrorName(status));
	  ::LZ4F_freeCompressionContext(stream);
	  return {};
	}
	std::copy(&buffer.at(0), &buffer.at(0)+status, std::back_inserter(result));
      }
      // write frame end
      status = ::LZ4F_compressEnd(stream, &buffer.at(0), buffer.size(), NULL);
      if ( ::LZ4F_isError(status) )  {
	lib_rtl_output(LIB_RTL_ERROR, "LZ4: Failed to end compression: error %ld [%s]",
		       status, ::LZ4F_getErrorName(status));
	::LZ4F_freeCompressionContext(stream);
	return {};
      }
      std::copy(&buffer.at(0), &buffer.at(0)+status, std::back_inserter(result));
      ::LZ4F_freeCompressionContext(stream);
      return result;
    }
  }   // End namespace compress
}     // End namespace Online
#else
namespace Online { namespace compress {
    std::vector<uint8_t> decompress_lz4(const uint8_t*, std::size_t)
   { throw std::runtime_error("LZ4 compression unfortunately not availible!"); }
    std::vector<uint8_t> compress_lz4(const uint8_t*, std::size_t)
   { throw std::runtime_error("LZ4 compression unfortunately not availible!"); }
  }}
#endif


#if defined(ONLINE_HAVE_LZMA)
#include <lzma.h>

/// Namespace for the Online utilities
namespace Online  {
  /// Namespace for the Online compression utilities
  namespace compress  {

    std::string lzma_error_message(uint64_t code)   {
      lzma_ret ret = (lzma_ret)code;
      switch (ret) {
      case LZMA_MEMLIMIT_ERROR:
	errno = ENOMEM;
	return "Memory usage limit reached";
      case LZMA_MEM_ERROR:
	errno = ENOMEM;
	return "Memory allocation failed";
      case LZMA_FORMAT_ERROR:
	errno = EINVAL;
	return "Data format error: The input is not in the .xz format";
      case LZMA_OPTIONS_ERROR:
	errno = EINVAL;
	return "Options error: Unsupported compression options";
      case LZMA_DATA_ERROR:
	errno = EINVAL;
	return "Data error: Compressed file is corrupt";
      case LZMA_BUF_ERROR:
	errno = ENOSPC;
	return "Compressed file is truncated or otherwise corrupt. No progress is possible";
      case LZMA_UNSUPPORTED_CHECK:
	errno = EINVAL;
	return "Specified integrity check is not supported";
      case LZMA_PROG_ERROR:
	errno = EINVAL;
	return "Programming error";
      case LZMA_OK:
	return "Success";
      default:
	return "Unknown error, possibly a bug";
      }
    }
    
    /// Inflate the response using lzma
    std::vector<uint8_t> decompress_lzma(const uint8_t* data, std::size_t len)  {
      std::vector<uint8_t> buffer, result;
      // Initialize the encoder using the custom filter chain.
      ::lzma_stream strm = LZMA_STREAM_INIT;
      ::lzma_ret    ret = ::lzma_stream_decoder(&strm, UINT64_MAX, LZMA_CONCATENATED);
      if ( ret != LZMA_OK )  {
	::lib_rtl_output(LIB_RTL_ERROR, "LZMA: Failed to initialize decoder. [%08X]", ret);
	return {};
      }
      result.reserve(ASSUMED_COMPRESSION*len); // Assume minimum compression of factor ASSUMED_COMPRESSION
      buffer.resize(2*1024*1024);
      uint8_t* ptr = &buffer.at(0);
      for( strm.avail_in=len, strm.next_in=data; strm.avail_in > 0; )  {
	int64_t out_now = strm.total_out;
	strm.next_out  = ptr;
	strm.avail_out = buffer.size();
	ret = ::lzma_code(&strm, LZMA_RUN);
	if ( ret != LZMA_OK )  {
	  std::string msg = std::make_error_code(std::errc(errno=EINVAL)).message();
	  ::lib_rtl_output(LIB_RTL_ERROR, "LZMA: Failed to decode frame with %ld bytes [%s]",
			   len, msg.c_str());
	  return {};
	}
	std::copy(ptr, ptr+strm.total_out-out_now, std::back_inserter(result));
      }
      ::lzma_end(&strm);
      return result;
    }
    
    /// Deflate the response using lzma
    std::vector<uint8_t> compress_lzma(const uint8_t* buff, std::size_t len, int /* level */, int /* strategy */)  {
      ::lzma_options_lzma options;
      std::vector<uint8_t> buffer, result;
      ::lzma_stream strm = LZMA_STREAM_INIT;

      if ( ::lzma_lzma_preset(&options, LZMA_PRESET_DEFAULT) ) {
	::lib_rtl_output(LIB_RTL_ERROR, "LZMA: Unsupported preset %d, possibly a bug.", LZMA_PRESET_DEFAULT);
	return {};
      }
      ::lzma_filter filters[3];
      filters[0].id = LZMA_FILTER_X86;
      filters[0].options = NULL;
      filters[1].id = LZMA_FILTER_LZMA2;
      filters[1].options = &options;
      filters[2].id = LZMA_VLI_UNKNOWN;
      filters[2].options = NULL;
      // Initialize the encoder using the custom filter chain.
      ::lzma_ret ret = ::lzma_stream_encoder(&strm, filters, LZMA_CHECK_CRC64);
      if ( ret != LZMA_OK )  {
	::lib_rtl_output(LIB_RTL_ERROR, "LZMA: FAILED to create stream encoder.");
	return {};
      }
      buffer.resize(1024*1024);
      result.reserve(std::max(std::size_t(1024*1024), len/ASSUMED_COMPRESSION));
      strm.avail_out = buffer.size();
      ::lzma_action action = LZMA_RUN; // LZMA_FINISH or LZMA_RUN;
      for( strm.avail_in = len, strm.next_in = buff; strm.avail_in > 0; )  {
	strm.next_out   = &buffer.at(0);
	strm.avail_out  = buffer.size();
	ret = ::lzma_code(&strm, action);
	if ( !(ret == LZMA_OK || ret == LZMA_STREAM_END) )  {
	  ::lib_rtl_output(LIB_RTL_ERROR, "LZMA: FAILED to compress data chunk.");
	  ::lzma_end(&strm);
	  return {};
	}
	std::copy(&buffer.at(0), &buffer.at(0)+buffer.size()-strm.avail_out, std::back_inserter(result));
      }
      if ( action == LZMA_RUN )  {
	strm.next_out   = &buffer.at(0);
	strm.avail_out  = buffer.size();
	ret = ::lzma_code(&strm, LZMA_FINISH);
	if ( !(ret == LZMA_OK || ret == LZMA_STREAM_END) )  {
	  ::lib_rtl_output(LIB_RTL_ERROR, "LZMA: FAILED to finalize data chunk.");
	  ::lzma_end(&strm);
	  return {};
	}
	std::copy(&buffer.at(0), &buffer.at(0)+buffer.size()-strm.avail_out, std::back_inserter(result));
      }
      ::lzma_end(&strm);
      return result;
    }
    
  }   // End namespace compress
}     // End namespace Online
#else
namespace Online { namespace compress {
    std::vector<uint8_t> decompress_lzma(const uint8_t*, std::size_t)
   { throw std::runtime_error("LZMA compression unfortunately not availible!"); }
    std::vector<uint8_t> compress_lzma(const uint8_t*, std::size_t)
   { throw std::runtime_error("LZMA compression unfortunately not availible!"); }
  }}
#endif

#if defined(ONLINE_HAVE_ZSTD)
#include <zstd.h>

/// Namespace for the Online utilities
namespace Online  {
  /// Namespace for the Online compression utilities
  namespace compress  {

    /// Inflate the response using zstd
    std::vector<uint8_t> decompress_zstd(const uint8_t* data, std::size_t len)  {
      ZSTD_DCtx*  decompress = ::ZSTD_createDCtx();
      std::size_t out_size   = ::ZSTD_DStreamOutSize();
      std::vector<uint8_t> buffer, result;

      result.reserve(ASSUMED_COMPRESSION*len); // Assume minimum compression of factor 3
      buffer.resize(out_size);
      ::ZSTD_inBuffer input = { data, len, 0 };
      while (input.pos < input.size)  {
	ZSTD_outBuffer output = { &buffer.at(0), buffer.size(), 0 };
	std::size_t ret = ::ZSTD_decompressStream(decompress, &output , &input);
	if ( ZSTD_isError(ret) )  {
	  ::lib_rtl_output(LIB_RTL_ERROR, "ZSTD FAILED to decompress %ld bytes.", len);
	  return {};
	}
	std::copy(&buffer.at(0), &buffer.at(0)+output.pos, std::back_inserter(result));
      }
      ::ZSTD_freeDCtx( decompress );
      return result;
    }

    /// Deflate the response using zstd
    std::vector<uint8_t> compress_zstd(const uint8_t* buff, std::size_t len, int level, int /* strategy */)  {
      std::size_t ret;
      std::vector<uint8_t> result, temp;
      auto stream = ::ZSTD_createCCtx();
      ::ZSTD_inBuffer  in = { buff, len, 0 };
      ::ZSTD_outBuffer out;

      ::ZSTD_CCtx_setParameter(stream, ZSTD_c_compressionLevel,
			       level == COMPRESSION_LEVEL_DEFAULT
			       ? ZSTD_defaultCLevel()
			       : _get_compression_level(level));
      result.reserve(len/ASSUMED_COMPRESSION);
      temp.resize(::ZSTD_CStreamOutSize());
      do {
	out = { &temp.at(0), temp.size(), 0 };
	ret = ::ZSTD_compressStream2(stream, &out, &in, ZSTD_e_continue);
	if ( ::ZSTD_isError(ret) )  {
	  ::lib_rtl_output(LIB_RTL_ERROR, "ZSTD FAILED to compress %ld bytes.", len);
	  return {};
	}
	std::copy(&temp.at(0), &temp.at(0)+out.pos, std::back_inserter(result));
      } while ( in.pos != in.size );
      in = { nullptr, 0, 0 };
      out = { &temp.at(0), temp.size(), 0 };
      ret = ::ZSTD_compressStream2(stream, &out, &in, ZSTD_e_end);
      if ( ::ZSTD_isError(ret) )
	return {};
      std::copy(&temp.at(0), &temp.at(0)+out.pos, std::back_inserter(result));
      ::ZSTD_freeCCtx( stream );
      return result;
    }
  }   // End namespace compress
}     // End namespace Online
#else
namespace Online { namespace compress {
    std::vector<uint8_t> decompress_zstd(const uint8_t*, std::size_t)
   { throw std::runtime_error("ZSTD compression unfortunately not availible!"); }
    std::vector<uint8_t> compress_zstd(const uint8_t*, std::size_t)
   { throw std::runtime_error("ZSTD compression unfortunately not availible!"); }
  }}
#endif

/// Namespace for the HTTP utilities
namespace Online  {

  /// Namespace for the HTTP compression utilities
  namespace compress  {

    /* These are parameters to inflateInit2. See
       http://zlib.net/manual.html for the exact meanings. */

#define WINDOW_BITS      15
#define ENABLE_ZLIB_GZIP 32
#define GZIP_ENCODING    16

    /// compress a byte buffer
    std::vector<uint8_t> compress_zstream(bool have_gzip,
					  const uint8_t* data,
					  const std::size_t len,
					  int level,
					  int strategy)  {
      int status, window = WINDOW_BITS + (have_gzip ? GZIP_ENCODING : 0);
      uint8_t out[CHUNK+1];
      z_stream strm;
      if ( level == COMPRESSION_LEVEL_DEFAULT )
	level = Z_DEFAULT_COMPRESSION;
      else
	level = _get_compression_level(level);
      if ( strategy == COMPRESSION_STRATEGY_DEFAULT )
	strategy = Z_DEFAULT_STRATEGY;
      
      strm.zalloc = Z_NULL;
      strm.zfree  = Z_NULL;
      strm.opaque = Z_NULL;
      strm.avail_in = 0;
      strm.next_in  = (uint8_t*)data;
      status = ::deflateInit2 (&strm, 
			       level,
			       Z_DEFLATED,
			       window, 8,
			       strategy);
      if ( status < 0 )  {
	errno = ENOSPC;
	return {};
      }
      std::vector<uint8_t> result;
      result.reserve(std::max(std::size_t(128*1024), len/ASSUMED_COMPRESSION));
      strm.next_in  = (uint8_t*)data;
      strm.avail_in = len;
      do {
	strm.avail_out = CHUNK;
	strm.next_out  = out;
	status = ::deflate(&strm, Z_FINISH);
	if ( status < 0 )  {
	  errno = ENOSPC;
	  ::deflateEnd (&strm);
	  return {};
	}
	std::copy(out, out+CHUNK-strm.avail_out, std::back_inserter(result));
      }  while (strm.avail_out == 0);
      ::deflateEnd(&strm);
      return result;
    }
    static std::vector<uint8_t>
    decompress_zstream(bool have_gzip, const uint8_t* data, std::size_t len)  {
      uint8_t out[CHUNK+1];
      std::vector<uint8_t> result;
      z_stream strm;

      result.reserve(std::max(std::size_t(256*1024), len*ASSUMED_COMPRESSION));
      ::memset(&strm,0,sizeof(strm));
      strm.zalloc   = Z_NULL;
      strm.zfree    = Z_NULL;
      strm.opaque   = Z_NULL; 
      strm.next_in  = (uint8_t*)data;
      strm.avail_in = 0;
      int window = WINDOW_BITS + (have_gzip ? ENABLE_ZLIB_GZIP : 0);
      int status = ::inflateInit2(&strm, window);
      if ( status < 0 )  {
	errno = EINVAL;
	goto Default;
      }
      strm.avail_in = len;
      strm.next_in  = (uint8_t*)data;
      do {
	strm.avail_out = CHUNK;
	strm.next_out  = out;
	status         = ::inflate (&strm, Z_NO_FLUSH);
	switch (status) {
	case Z_OK:
	case Z_STREAM_END:
	  break;
	case Z_BUF_ERROR:
	  errno = ENOSPC;
	  inflateEnd(&strm);
	  goto Default;
	default:
	  errno = EINVAL;
	  inflateEnd(&strm);
	  goto Default;
	}
	std::copy(out, out+CHUNK-strm.avail_out, back_inserter(result));
      }  while (strm.avail_out == 0);
      ::inflateEnd(&strm);
      return result;

    Default:
      result.reserve(len);
      std::copy(data, data+len, back_inserter(result));      
      return result;
    }

  }   // End namespace compress
}     // End namespace Online

/// Namespace for the HTTP utilities
namespace Online  {

  /// Namespace for the HTTP compression utilities
  namespace compress  {

    /// Inflate the response using zstd
    std::vector<uint8_t> decompress_zstd(const std::vector<uint8_t>& data)  {
      return decompress_zstd(&data[0], data.size());
    }

    /// Inflate the response using zstd
    std::vector<uint8_t> decompress_zstd(std::vector<uint8_t>&& data)  {
      return decompress_zstd(&data[0], data.size());
    }
    
    /// Inflate the response using lzma
    std::vector<uint8_t> decompress_lzma(const std::vector<uint8_t>& data)  {
      return decompress_lzma(&data[0], data.size());
    }

    /// Inflate the response using lzma
    std::vector<uint8_t> decompress_lzma(std::vector<uint8_t>&& data)  {
      return decompress_lzma(&data[0], data.size());
    }
    
    /// Inflate the response using lz4
    std::vector<uint8_t> decompress_lz4(const std::vector<uint8_t>& data)  {
      return decompress_lz4(&data[0], data.size());
    }

    /// Inflate the response using lz4
    std::vector<uint8_t> decompress_lz4(std::vector<uint8_t>&& data)  {
      return decompress_lz4(&data[0], data.size());
    }    

    /// gzip compress a byte buffer
    std::vector<uint8_t> compress_zlib(const uint8_t* data, std::size_t len,
				       int compression, int strategy)  {
      return compress_zstream(true, data, len, compression, strategy);
    }
    /// gzip compress a byte buffer
    std::vector<uint8_t> compress_zlib(const std::vector<uint8_t>& data)  {
      return compress_zlib(&data.at(0), data.size());
    }

    /// compress a byte buffer
    std::vector<uint8_t> compress(const std::string& encoding, 
				  const std::vector<uint8_t>& data, 
				  std::string& used_encoding)  {
      used_encoding = "";
      if ( !encoding.empty() )  {
	if ( encoding.find("gzip")    != std::string::npos )  {
	  auto result = compress_zstream(true, &data.at(0), data.size(), Z_DEFAULT_COMPRESSION, Z_DEFAULT_STRATEGY);
	  used_encoding = "gzip";
	  return result;
	}
	if ( encoding.find("deflate")    != std::string::npos )  {
	  auto result = compress_zstream(false, &data.at(0), data.size(), Z_DEFAULT_COMPRESSION, Z_DEFAULT_STRATEGY);
	  used_encoding = "deflate";
	  return result;
	}
#if defined(ONLINE_HAVE_ZSTD)
	if ( encoding.find("zstd") != std::string::npos )  {
	  auto result = compress_zstd(&data.at(0), data.size());
	  used_encoding = "zstd";
	  return result;
	}
#endif
#if defined(ONLINE_HAVE_LZMA)
	if ( encoding.find("lzma") != std::string::npos )  {
	  auto result = compress_lzma(&data.at(0), data.size());
	  used_encoding = "lzma";
	  return result;
	}
#endif
#if defined(ONLINE_HAVE_LZ4)
	if ( encoding.find("lz4") != std::string::npos )  {
	  auto result = compress_lz4(&data.at(0), data.size());
	  used_encoding = "lz4";
	  return result;
	}
#endif
      }
      used_encoding = "";
      return data;
    }
    
    /// Inflate the response using gzip
    std::vector<uint8_t> decompress_zlib(const uint8_t* data, std::size_t len)  {
      return decompress_zstream(true, data, len);
    }

    /// Inflate the response using gzip
    std::vector<uint8_t> decompress_zlib(const std::vector<uint8_t>& data)  {
      return decompress_zstream(true, &data[0], data.size());
    }

    /// Inflate the response using gzip
    std::vector<uint8_t> decompress_zlib(std::vector<uint8_t>&& data)  {
      return decompress_zstream(true, &data[0], data.size());
    }
    
    /// Inflate the response
    std::vector<uint8_t> decompress(const std::string&          encoding, 
				    const std::vector<uint8_t>& data)  {
      return decompress(encoding, &data[0], data.size());
    }

    /// Inflate the response
    std::vector<uint8_t> decompress(const std::string&     encoding, 
				    std::vector<uint8_t>&& data)  {
      return decompress(encoding, &data[0], data.size());
    }
    
    /// Inflate the response
    std::vector<uint8_t> decompress(const std::string& encoding, 
				    const void*        data,
				    std::size_t        len)  {
      return decompress(encoding, (const uint8_t*)data, len);
    }
    
    /// Inflate the response
    std::vector<uint8_t> decompress(const std::string&   encoding, 
				    const uint8_t*       data,
				    std::size_t          len)  {
      std::vector<uint8_t> result;
      if ( !encoding.empty() )  {
	if ( encoding.find("gzip")    != std::string::npos )
	  return decompress_zstream(true, data, len);
	else if ( encoding.find("deflate") != std::string::npos )
	  return decompress_zstream(false, data, len);
	else if ( encoding.find("zstd") != std::string::npos )
	  return decompress_zstd(data, len);
	else if ( encoding.find("lz4") != std::string::npos )
	  return decompress_lz4(data, len);
	else if ( encoding.find("identity") != std::string::npos )
	  {}
      }
      result.reserve(len);
      std::copy(data, data+len, back_inserter(result));      
      return result;
    }
    
    /// decompress a byte buffer: Encoding is interpreted from the payload data
    std::vector<unsigned char> decompress(const unsigned char* data, std::size_t len)   {
      const uint8_t* hdr = data;
      if ( is_gzip_data_buffer( data ) )
	return compress::decompress_gzip(data, len);
      else if ( is_zstd_data_buffer(data ) )
	return compress::decompress_zstd(data, len);
      else if ( is_xz_data_buffer(hdr) )
	return compress::decompress_lzma(data, len);
      else if ( is_lz4_data_buffer(hdr) )
        return compress::decompress_lz4(data, len);
      return { data, data+len };
    }

    /// decompress a byte buffer: Encoding is interpreted from the payload data
    std::vector<unsigned char> decompress(std::vector<unsigned char>&& data)   {
      const uint8_t* hdr = &data.at(0);
      if ( is_gzip_data_buffer( &data.at(0) ) )
	return compress::decompress_gzip(&data.at(0), data.size());
      else if ( is_zstd_data_buffer(&data.at(0) ) )
	return compress::decompress_zstd(&data.at(0), data.size());
      else if ( is_xz_data_buffer(hdr) )
	return compress::decompress_lzma(&data.at(0), data.size());
      else if ( is_lz4_data_buffer(hdr) )
        return compress::decompress_lz4(&data.at(0), data.size());
      return data;
    }

    /// Convert string data buffer to vector
    std::vector<uint8_t> to_vector(const char* data)  {
      std::vector<uint8_t> r;
      if ( data )  {
	size_t len = ::strlen(data);
	r.reserve(len+1);
	for(const char *c=data; *c; ++c) r.push_back((uint8_t)(*c));
      }
      return r;
    }

    /// Convert string data buffer to vector
    std::vector<uint8_t> to_vector(const std::string& data)  {
      std::vector<uint8_t> r;
      if ( !data.empty() )  {
	r.reserve(data.length()+1);
	for(const char *c=data.c_str(); *c; ++c) r.push_back((uint8_t)(*c));
      }
      return r;
    }

    int init_data_reading(Datafile& file)  {
      struct reopen {
	int operator()(Datafile& file, posix_t* p)  {
	  std::string fname = file.name();
	  file.close();
	  file = Datafile(fname, p);
	  return (file.open(true) == -1) ? 0 : 1;
	}
      };
      if ( file.isOpen() )  {
	uint8_t hdr[6];
	if ( file.read(hdr, sizeof(hdr)) == sizeof(hdr) )  {
	  if ( is_gzip_data_buffer( hdr ) )
	    return reopen()(file, posix_descriptor_zlib());
	  else if ( is_zstd_data_buffer( hdr ) )
	    return reopen()(file, posix_descriptor_zstd());
	  else if ( is_xz_data_buffer(hdr) )
	    return reopen()(file, posix_descriptor_lzma());
	  else if ( is_lz4_data_buffer(hdr) )
	    return reopen()(file, posix_descriptor_lz4());
	  else
	    file.position(0);
	}
	return 1;
      }
      return 0;
    }

    std::string base64_encode(const uint8_t* bytes_to_encode, unsigned int in_len) {
      std::string ret;
      int i = 0;
      int j = 0;
      uint8_t char_array_3[3];
      uint8_t char_array_4[4];

      while (in_len--) {
	char_array_3[i++] = *(bytes_to_encode++);
	if (i == 3) {
	  char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
	  char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
	  char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
	  char_array_4[3] = char_array_3[2] & 0x3f;

	  for(i = 0; (i <4) ; i++)
	    ret += base64_chars[char_array_4[i]];
	  i = 0;
	}
      }

      if (i)   {
	for(j = i; j < 3; j++)
	  char_array_3[j] = '\0';

	char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
	char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
	char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
	char_array_4[3] = char_array_3[2] & 0x3f;

	for (j = 0; (j < i + 1); j++)
	  ret += base64_chars[char_array_4[j]];

	while((i++ < 3))
	  ret += '=';
      }
      return ret;
    }

    std::vector<uint8_t> base64_decode(const std::string& encoded_string) {
      size_t in_len = encoded_string.size();
      size_t i = 0;
      size_t j = 0;
      int in_ = 0;
      uint8_t char_array_4[4], char_array_3[3];
      std::vector<uint8_t> ret;

      while (in_len-- && ( encoded_string[in_] != '=') && is_base64(encoded_string[in_])) {
	char_array_4[i++] = encoded_string[in_]; in_++;
	if (i ==4) {
	  for (i = 0; i <4; i++)
	    char_array_4[i] = static_cast<uint8_t>(base64_chars.find(char_array_4[i]));

	  char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
	  char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
	  char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

	  for (i = 0; (i < 3); i++)
	    ret.push_back(char_array_3[i]);
	  i = 0;
	}
      }

      if (i) {
	for (j = i; j <4; j++)
	  char_array_4[j] = 0;
	for (j = 0; j <4; j++)
	  char_array_4[j] = static_cast<uint8_t>(base64_chars.find(char_array_4[j]));

	char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
	char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
	char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

	for (j = 0; (j < i - 1); j++) ret.push_back(char_array_3[j]);
      }
      return ret;
    }

  }   // End namespace compress
}     // End namespace Online

