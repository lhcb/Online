//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Project    : Online
//
//  Author     : M.Frank
//
//==========================================================================

/// Framework include files
#include <RTL/DataCompressor.h>
#include <RTL/rtl.h>
#include <RTL/posix.h>
#include <RTL/strdef.h>

/// C/C++ include files
#include <map>
#include <memory>
#include <cstdarg>
#include <stdexcept>
#define O_BINARY 0

/// Online namespace declaration
namespace Online   {

  /// Namespace for the compression utilities
  namespace compress   {

    using posix_t = Online::posix_t;
    using FileMap = std::map<int, std::unique_ptr<DataCompressor> >;

    static int s_debug = 0;

    /// ------------------------------------------------------------------------------
    FileMap& fileMap() {
      static std::unique_ptr<FileMap> s_fileMap = std::make_unique<FileMap>();
      return *(s_fileMap.get());
    }

    /// ------------------------------------------------------------------------------
    DataCompressor::DataCompressor(const std::string& typ) : stream_type(typ)  {
    }
    /// ------------------------------------------------------------------------------
    DataCompressor* DataCompressor::get(int fd)   {
      auto i = fileMap().find( fd );
      if ( i != fileMap().end() ) {
	return i->second.get();
      }
      errno = EBADF;
      return nullptr;
    }
    /// ------------------------------------------------------------------------------
    std::size_t DataCompressor::print(int debug, const char* format, ...)  {
      if ( debug )   {
	va_list args;
	va_start( args, format );
	std::size_t len = ::lib_rtl_log(debug > 1 ? debug : LIB_RTL_ALWAYS, format, args);
	va_end(args);
	return len;
      }
      return 0;
    }
    /// ------------------------------------------------------------------------------
    std::size_t DataCompressor::print(const char* format, ...)  {
      int debug = this->file.debug;
      if ( debug )   {
	va_list args;
	va_start( args, format );
	std::size_t len = ::lib_rtl_log(debug > 1 ? debug : LIB_RTL_ALWAYS, format, args);
	va_end(args);
	return len;
      }
      return 0;
    }
    /// ------------------------------------------------------------------------------
    std::size_t DataCompressor::error(const char* format, ...)  {
      va_list args;
      va_start( args, format );
      ::lib_rtl_log(LIB_RTL_ERROR, format, args);
      va_end(args);
      return -1;
    }
    /// ------------------------------------------------------------------------------
    std::pair<bool, int> DataCompressor::parse_digit(const std::string& val)   {
      if ( !::isdigit(val[0]) )  {
	errno = EINVAL;
	return { false, 0 };
      }
      return { true, int(val[0]) - int('0') };
    }
    /// ------------------------------------------------------------------------------
    int DataCompressor::open( const char* path, int flags, va_list& args )  {
      int fd = -1;
      if ( flags&O_CREAT )  {
	int mode = va_arg(args, int);
	fd = this->file.open(this->file.debug==0, path, flags, mode);
      }
      else {
	fd = this->file.open(this->file.debug==0, path, flags, -1);
      }
      va_end(args);
      if ( fd < 0 )
	return fd;
      print("%s: %s: Opened file fd: %d", this->name(), this->type(), fd);
      return fd;
    }
    /// ------------------------------------------------------------------------------
    int DataCompressor::open( std::unique_ptr<DataCompressor>&& stream,
			      const char* path, int flags, va_list& args )  {
      int fd = stream->open(path, flags, args);
      if ( fd > 0 )  {
	fileMap().emplace(fd, std::move(stream));
      }
      return fd;
    }
    /// ------------------------------------------------------------------------------
    int DataCompressor::close()   {
      print("%s: %s: Closing file", this->file.path.c_str(), this->type());
      /// 
      if ( this->file.mode == DataCompressor::WRITING )   {
	this->finished = 1;
	this->in_total += this->position;
	this->compress(this->in_buffer.begin(), this->position);
	if ( this->position > 0 )  {
	  this->flush(0);
	}
	this->end_writing();
	this->flush(0);
      }
      else if ( this->file.mode == DataCompressor::READING )   {
	this->end_reading();
      }
      this->file.close();
      this->position = 0;
      return 0;
    }
    /// ------------------------------------------------------------------------------
    int DataCompressor::close( int fd )   {
      auto i = fileMap().find( fd );
      if ( i != fileMap().end() )  {
	int ret = i->second->close();
	fileMap().erase(i);
	return ret;
      }
      print(s_debug, "%05X: Compressor: FAILED to close file [%s]", fd, file_t::error_message(EBADF).c_str());
      return -1;
    }
    /// ------------------------------------------------------------------------------
    int DataCompressor::attach()   {
      int flags = this->file.flags;
      if ( (flags & (O_WRONLY|O_CREAT)) != 0 )  {
	this->file.mode = DataCompressor::WRITING;
	if ( 0 != this->begin_writing() )   {
	  print("%s: %s: FAILED to initialize compression stream.",
		this->name(), this->type());
	  this->file.close();
	  return -1;
	}
      }
      else if ( flags == O_RDONLY || flags == ( O_BINARY | O_RDONLY ) )  {
	this->file.mode = DataCompressor::READING;
	if ( 0 != this->begin_reading() )   {
	  print("%s: %s: FAILED to initialize decompression stream.",
		this->name(), this->type());
	  this->file.close();
	  return -1;
	}
      }
      else  {
	// Unknown opening mode. Cannot be handled
	print("%s: %s: Unknown stream opening flags: %04X. Cannot open stream.",
	      this->name(), this->type(), flags);
	return -1;
      }
      if ( this->in_buffer.length() < this->in_size )
	this->in_buffer.allocate(this->in_size);
      if ( this->tmp_buffer.length() < this->tmp_size )
	this->tmp_buffer.allocate(this->tmp_size);
      if ( this->out_buffer.length() < this->out_limit )
	this->out_buffer.allocate(this->out_limit);
      if ( (flags & (O_WRONLY|O_CREAT)) != 0 )   {
	if ( DataCompressor::posix_access(this->name(), O_WRONLY) )  {
	  posix_t::truncate_disk(this->file.fd, 0);
	}
      }
      return 0;
    }
    /// ------------------------------------------------------------------------------
    void DataCompressor::append_output(const uint8_t* buff, std::size_t len)  {
      if ( len > 0 )   {
	if ( (len+this->out_buffer.used()) > this->out_limit )
	  this->flush(0);
	if ( len > 0 )  {
	  this->out_total += len;
	  this->out_buffer.append(buff, len);
	}
      }
    }
    /// ------------------------------------------------------------------------------
    int DataCompressor::flush( std::size_t minimal_buffer_length )   {
      if ( this->out_buffer.used() >= minimal_buffer_length )   {
	int64_t ret = this->file.write(this->out_buffer.begin(), this->out_buffer.used());
	if ( ret < 0 )  {
	  print("%s: %s: FAILED to write %ld bytes to file [%s]",
		this->name(), this->type(), this->out_buffer.used(),
		this->file.error_message().c_str());
	  return -1;
	}
	print("%s: %s: Wrote %ld bytes to disk. [Total: %ld bytes]",
	      this->name(), this->type(), this->out_buffer.used(), this->out_disk);
	this->out_disk      += this->out_buffer.used();
	this->out_last_write = this->out_total;
	this->out_buffer.set_cursor(0);
	this->position = 0;
      }
      return 0;
    }
    /// ------------------------------------------------------------------------------
    ssize_t DataCompressor::write( int fd, const void* ptr, size_t size )  {
      DataCompressor* desc = DataCompressor::get(fd);
      if ( desc ) return desc->write( ptr, size );
      print(s_debug, "%05X: FAILED to write %ld bytes [%s]",
	    fd, size, file_t::error_message(EBADF).c_str());
      return -1;
    }
    /// ------------------------------------------------------------------------------
    ssize_t DataCompressor::write( const void* ptr, size_t size )  {
      if ( !this->applied )  {
	if ( this->attach() < 0 )  {
	  this->file.close();
	  print("%s: %s: FAILED to open file", this->name(), this->type());
	  return -1;
	}
	this->applied = 1;
      }
      uint8_t *pointer   = (uint8_t*)ptr;
      uint8_t *start     = this->in_buffer.begin()+this->position;
      uint64_t in_length = this->in_buffer.length();
      uint64_t in_space  = in_length - this->position;
      uint64_t in_miss   = 0;
      if ( in_space >= size )  {
	::memcpy(start, pointer, size);
	this->position += size;
	return size;
      }
      else if ( in_space > 0 )  {
	::memcpy(start, pointer, in_space);
	in_miss = size - in_space;
	this->position += in_space;
	pointer += in_space;
      }
      else  {
	in_miss = size;
      }
      int64_t written = this->compress(this->in_buffer.begin(), in_length);
      if ( written < 0 )  {
	error("%s: %s: Failed to encode frame with %ld bytes [%s]",
	      this->name(), this->type(), in_length, this->file.error_message().c_str());
	return -1;
      }
      else if ( written == 0 )  {
	print("%s: %s: Compression finished. Stop coding after %ld bytes.",
	      this->name(), this->type(), in_length);
      }
      this->in_total += in_length;
      if ( int64_t(in_length) != written )  {
	this->position = 0;
	return -1;
      }
      this->in_buffer.set_cursor(0);
      this->position = 0;
      print("%s: %s: Wrote buffer of %8ld bytes. [raw: %9ld encoded: %9ld buffered: %9ld disk: %9ld bytes]",
	    this->name(), this->type(), in_length, this->in_total, this->out_total,
	    this->out_buffer.used(), this->out_disk);
      return in_space + this->write(pointer, in_miss);
    }
    /// ------------------------------------------------------------------------------
    ssize_t DataCompressor::read( void* ptr, size_t size )  {
      const char* path = this->name();
      const char* typ  = this->type();
      if ( !this->applied )  {
	if ( this->attach() < 0 )  {
	  this->file.close();
	  print("%s: %s: FAILED to open file", path, typ);
	  return -1;
	}
	this->applied = 1;
      }
      uint8_t *pointer  = (uint8_t*)ptr;
      uint8_t *start    = this->out_buffer.begin() + this->position;
      uint64_t out_left = this->out_buffer.used()  - this->position;
      int64_t  out_miss = 0;
      if ( out_left >= size )  {
	::memcpy(pointer, start, size);
	this->position += size;
	this->in_total += size;
	return size;
      }
      else if ( out_left > 0 )  {
	::memcpy(pointer, start, out_left);
	out_miss = size - out_left;
	this->position += out_left;
	this->in_total += out_left;
	pointer += out_left;
      }
      else  {
	out_miss = size;
      }
      // We have to freshly populate the output buffer from zero position
      if ( this->out_buffer.length() < size )   {
	this->out_buffer.allocate(size*1.5);
      }
      ///
      /// Now fill new decompressed buffer:
      //print("%s: %s: reload data %ld bytes from disk. pos: %ld left: %ld length: %ld",
      //      path, typ, size, this->position, out_left, this->out_buffer.used());
      this->position = 0;
      this->out_buffer.set_cursor(0);
      for (int64_t len=0; len < out_miss;  )   {
	this->in_last_read = this->in_total;
	int64_t total = this->file.read(this->in_buffer.begin(), this->in_size);
	if ( total < 0 )  {
	  int64_t ret = (out_left > 0) ? out_left : -1;
	  std::string msg = std::make_error_code(std::errc(errno)).message();
	  print("%s: %s: FAILED to read %ld / %ld bytes. ret: %ld pos: %ld [Read error: %s]",
		path, typ, total, size, ret, this->position, msg.c_str());
	  return ret;
	}
	else if ( total == 0 )  {
	  int64_t ret = ((out_left+len) > 0) ? (out_left+len) : -1;
	  print("%s: %s: ZERO    read %ld / %ld bytes. ret: %ld pos: %ld [End-of-file]",
		path, typ, total, size, ret, this->position);
	  return ret;
	}
	else if ( total < int64_t(this->in_size) )   {
	  print("%s: %s: LIMITED read %ld / %ld bytes. pos: %ld [End-of-file]",
		path, typ, total, size, this->position);
	}
	len = this->decompress(this->in_buffer.begin(), total);
	//print("%s: %s: read %ld / %ld bytes from disk. decompressed: %ld buffered: %ld",
	//      path, typ, total, size, len, this->out_buffer.used());
	if ( len < 0 )  {
	  return -1;
	}
	this->in_disk += total;
	len = this->out_buffer.used();
      }
      return out_left + this->read(pointer, out_miss);
    }
    /// ------------------------------------------------------------------------------
    ssize_t DataCompressor::read( int fd, void* ptr, size_t size )  {
      if ( DataCompressor* desc = DataCompressor::get(fd) )  {
	return desc->read( ptr, size );
      }
      print(s_debug, "%05X: FAILED to read %ld bytes. [%s]",
	    fd, size, file_t::error_message(EBADF).c_str());
      return -1;
    }
    /// ------------------------------------------------------------------------------
    off64_t DataCompressor::seek( off64_t offset, int how )  {
      // We cover only the file range we have decompressed in memory.
      // Everything else is a FAILURE!
      off64_t pos = off64_t(this->position);
      if ( this->file.mode == DataCompressor::READING )  {
	off64_t begin = off64_t(this->in_last_read);
	off64_t end   = begin + off64_t(this->out_buffer.used());
	if ( how == SEEK_CUR )  {
	  if ( (pos + offset) >= 0 && (pos + offset) <= end )  {
	    pos += offset;
	    return begin + pos;
	  }
	}
	else if ( how == SEEK_SET )  {
	  if ( offset >= begin && offset <= end )  {
	    pos = offset - begin;
	    return begin + pos;
	  }
	}
	else if ( how == SEEK_END )  {
	}
      }
      if ( this->file.mode == DataCompressor::WRITING )  {
	off64_t begin = off64_t(this->out_last_write);
	off64_t end   = begin + off64_t(this->out_buffer.used());
	if ( how == SEEK_CUR )  {
	  off64_t diff = pos + offset;
	  if ( diff >= 0 && diff <= end )  {
	    pos += offset;
	    return begin + pos;
	  }
	}
	else if ( how == SEEK_SET )  {
	  if ( offset >= begin && offset <= end )  {
	    pos = offset - begin;
	    return begin + pos;
	  }
	}
	else if ( how == SEEK_END )  {
	  if ( offset <= 0 && offset >= -pos )  {
	    pos += offset;
	    return begin + pos;
	  }
	}
      }
      return -1;
      // return this->file.seek( offset, how );
    }
    /// ------------------------------------------------------------------------------
    off64_t DataCompressor::seek( int fd, off64_t offset, int how )  {
      auto* dsc = DataCompressor::get(fd);
      return dsc ? dsc->seek(offset, how) : -1;
      print(s_debug, "%05X: Compressor: FAILED to seek file [%s]",
	    fd, file_t::error_message(EBADF).c_str());
      return -1;
    }
    /// ------------------------------------------------------------------------------
    int DataCompressor::set_option(int fd, const char* option, const char* value)  {
      std::string val = RTL::str_lower(value);
      std::string opt = RTL::str_lower(option);
      errno = EINVAL;
      if ( opt == "global_debug" )   {
	if      ( val[0] == 'y'    ) s_debug = 1; // yes
	else if ( val[0] == 't'    ) s_debug = 1; // true
	else if ( val[0] == 'n'    ) s_debug = 0; // no
	else if ( val[0] == 'f'    ) s_debug = 0; // false
	return 0;
      }
      if ( DataCompressor* desc = DataCompressor::get(fd) )  {
	if ( 0 == desc->set_option(opt, val) )
	  return 0;
	desc->error("%s: %s: FAILED to set option %s = %s",
		    desc->name(), desc->type(), opt.c_str(), val.c_str());
      }
      errno = EBADF;
      return -1;
    }
    /// ------------------------------------------------------------------------------
    int DataCompressor::set_option(const std::string& opt, const std::string& val)  {
      if ( opt.substr(0, 5) == "debug" )   {
	if      ( val[0] == 'y'    ) this->file.debug = 1; // yes
	else if ( val[0] == 't'    ) this->file.debug = 1; // true
	else if ( val[0] == 'n'    ) this->file.debug = 0; // no
	else if ( val[0] == 'f'    ) this->file.debug = 0; // false
	return 0;
      }
      else if ( opt.substr(0, 9) == "read_size" )   {
	int64_t len = ::atol(val.c_str());
	if ( 0 == len ) goto Error;
	this->tmp_size = len;
	this->in_size  = len;
	return 0;
      }
      else if ( opt.substr(0, 10) == "write_size" )   {
	this->out_limit = ::atol(val.c_str());
	if ( 0 == this->out_limit ) goto Error;
	return 0;
      }
      else if ( opt.substr(0, 8) == "strategy" )   {
	return 0;
      }
      else if ( opt.substr(0, 5) == "apply" )   {
	return 0;
      }
    Error:
      error("%s: %s: FAILED to set option %s = %s",
	    this->name(), this->type(), opt.c_str(), val.c_str());
      return -1;
    }
    /// ------------------------------------------------------------------------------
    ssize_t DataCompressor::posix_write( int fd, const void* ptr, size_t size )  {
      return DataCompressor::write( fd, ptr, size );
    }
    /// ------------------------------------------------------------------------------
    ssize_t DataCompressor::posix_read( int fd, void* ptr, size_t size ) {
      return DataCompressor::read( fd, ptr, size );
    }
    /// ------------------------------------------------------------------------------
    int DataCompressor::posix_close( int fd )  {
      auto* dsc = DataCompressor::get(fd);
      return dsc ? dsc->close( fd ) : -1;
    }
    /// ------------------------------------------------------------------------------
    int DataCompressor::posix_access( const char* nam, int mode ) {
      return posix_t::access_disk(nam, mode);
    }
    /// ------------------------------------------------------------------------------
    int DataCompressor::posix_unlink( const char* name) {
      return posix_t::unlink_disk(name);
    }
    /// ------------------------------------------------------------------------------
    int64_t DataCompressor::posix_lseek64( int fd, int64_t offset, int how ) {
      return DataCompressor::seek( fd, offset, how );
    }
    /// ------------------------------------------------------------------------------
    long DataCompressor::posix_lseek( int fd, long offset, int how ) {
      return (long)DataCompressor::posix_lseek64( fd, offset, how );
    }
    /// ------------------------------------------------------------------------------
    int DataCompressor::posix_fstat( int fd, struct stat* statbuf ) {
      auto* dsc = DataCompressor::get(fd);
      return dsc ? posix_t::fstat_disk(dsc->file.fd, statbuf) : -1;
    }
    /// ------------------------------------------------------------------------------
    int   DataCompressor::posix_fstat64( int fd, struct stat64* statbuf ) {
      auto* dsc = DataCompressor::get(fd);
      return dsc ? posix_t::fstat64_disk(dsc->file.fd, statbuf) : -1;
    }
    /// ------------------------------------------------------------------------------
    FILE* DataCompressor::posix_fopen(const char* path, const char* mode) {
      if ( !path || !mode )   {
	errno = EINVAL;
	return nullptr;
      }
      // TODO: Fix this wrong logic!
      auto [fd, flags] = posix_t::fopen_disk(path, mode);
      auto* dsc = DataCompressor::get(fd);
      if ( dsc )   {
	if ( dsc->attach() < 0 )  {
	  dsc->file.close();
	  print(dsc->file.debug, "%s: %s: FAILED to open file", path, dsc->type());
	  return nullptr;
	}
	dsc->applied = 1;
        return (FILE*)long(dsc->file.fd);
      }
      return nullptr;
    }
    /// ------------------------------------------------------------------------------
    int DataCompressor::posix_fclose( FILE* file )   {
      return DataCompressor::posix_close(posix_t::fileno_specific(file));
    }
    /// ------------------------------------------------------------------------------
    int DataCompressor::posix_fileno( FILE* file )   {
      return posix_t::fileno_specific(file);
    }
    /// ------------------------------------------------------------------------------
    long DataCompressor::posix_ftell( FILE* stream )   {
      return DataCompressor::posix_lseek(posix_t::fileno_specific(stream), 0, SEEK_CUR);
    }
    /// ------------------------------------------------------------------------------
    int DataCompressor::posix_fseek( FILE* file, int64_t offset, int how ) {
      return DataCompressor::posix_lseek(posix_t::fileno_specific(file), offset, how);
    }
    /// ------------------------------------------------------------------------------
    size_t DataCompressor::posix_fread( void* ptr, size_t size, size_t nitems, FILE* file ) {
      return DataCompressor::posix_read(posix_t::fileno_specific(file), ptr, nitems*size);
    }
    /// ------------------------------------------------------------------------------
    size_t DataCompressor::posix_fwrite( const void* ptr, size_t size, size_t nitems, FILE* file ) {
      return DataCompressor::posix_write(posix_t::fileno_specific(file), ptr, nitems*size);
    }
    /// ------------------------------------------------------------------------------
    int   DataCompressor::posix_stat( const char* path, struct stat* statbuf ) {
      return posix_t::stat_disk(path, statbuf);
    }
    /// ------------------------------------------------------------------------------
    int   DataCompressor::posix_stat64( const char* path, struct stat64* statbuf ) {
      return posix_t::stat64_disk(path, statbuf);
    }
    /// --------------------------------------------------------------------------------
    int DataCompressor::init_descriptor(Online::posix_t& p) {
      if ( !p.open ) {
	p.set_option = DataCompressor::set_option;
	p.unbuffered = Online::posix_t::COMPLETE;
	p.open       = nullptr;
	p.close      = DataCompressor::posix_close;
	p.read       = DataCompressor::posix_read;
	p.write      = DataCompressor::posix_write;
	p.lseek      = DataCompressor::posix_lseek;
	p.lseek64    = DataCompressor::posix_lseek64;
	p.access     = DataCompressor::posix_access;
	p.unlink     = DataCompressor::posix_unlink;
	p.stat       = DataCompressor::posix_stat;
	p.stat64     = DataCompressor::posix_stat64;
	p.fstat      = DataCompressor::posix_fstat;
	p.fstat64    = DataCompressor::posix_fstat64;

	p.buffered  = Online::posix_t::COMPLETE;
	p.fopen     = DataCompressor::posix_fopen;
	p.fclose    = DataCompressor::posix_fclose;
	p.fwrite    = DataCompressor::posix_fwrite;
	p.fread     = DataCompressor::posix_fread;
	p.fseek     = DataCompressor::posix_fseek;
	p.ftell     = DataCompressor::posix_ftell;
	p.fileno    = DataCompressor::posix_fileno;

	p.directory = Online::posix_t::COMPLETE;
	p.rmdir     = ::rmdir;
	p.mkdir     = ::mkdir;
	p.opendir   = ::opendir;
	p.readdir   = ::readdir;
	p.closedir  = ::closedir;
      }
      return 0;
    }
  }     // End namespace compress
}       // End namespace Online

/// --------------------------------------------------------------------------------
