//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Project    : Online
//
//  Author     : M.Frank
//
//==========================================================================

/// Framework include files
#include <RTL/DataCompressor.h>

#if defined(ONLINE_HAVE_LZ4)

/// Framework include files
#include <RTL/posix.h>

/// C/C++ include files
#include <algorithm>

#include <lz4frame.h>

/// Online namespace declaration
namespace Online   {

  /// Namespace for the compression utilities
  namespace compress   {

    struct lz4_stream_t {
      LZ4F_cctx*     compress     { nullptr };
      LZ4F_dctx*     decompress   { nullptr };
      int32_t        level        { 0 };
      uint32_t       autoflush    { 0 };
      ~lz4_stream_t() = default;
    };
    /// ------------------------------------------------------------------------------
    std::size_t posix_lz4_block_size(const LZ4F_frameInfo_t* info) {
      switch (info->blockSizeID) {
      case LZ4F_default:
      case LZ4F_max64KB:  return 1 << 16;
      case LZ4F_max256KB: return 1 << 18;
      case LZ4F_max1MB:   return 1 << 20;
      case LZ4F_max4MB:   return 1 << 22;
      default:
	DataCompressor::error("Impossible with expected frame specification (<=v1.6.1)");
	exit(1);
      }
    }
    /// ------------------------------------------------------------------------------
    template <> int concrete_compressor<lz4_stream_t>::handle_error(uint64_t code)   {
      print("%s: LZ4: FAILURE error %X [%s]", this->name(), code, ::LZ4F_getErrorName(code));
      return 0;
    }
    /// ------------------------------------------------------------------------------
    template <> int
    concrete_compressor<lz4_stream_t>::set_option(const std::string& opt, const std::string& val)  {
      if( opt.substr(0, 8) == "compress" )   {
	std::string value = val;
	if     ( val == "none"    ) value = "0";
	else if( val == "default" ) value = "5";
	else if( val == "high"    ) value = "9e";
	auto comp = parse_digit(value);
	if( !comp.first ) goto Error;
	this->stream.level = comp.second;
	return 0;
      }
      return this->DataCompressor::set_option(opt, val);
    Error:
      error("%s: LZ4: FAILED to set option %s = %s", this->name(), opt.c_str(), val.c_str());
      return -1;
    }
    /// ------------------------------------------------------------------------------
    template <> int64_t
    concrete_compressor<lz4_stream_t>::compress(const uint8_t* buff, std::size_t length)   {
      for(long done=0, out_now=0, todo=length; done < todo; done += out_now )   {
	out_now = std::min(this->tmp_buffer.length(), length-done);
	std::size_t status  = ::LZ4F_compressUpdate(this->stream.compress,
						    this->tmp_buffer.begin(),
						    this->tmp_buffer.length(),
						    buff + done,
						    out_now,
						    nullptr);
	if( ::LZ4F_isError(status) )  {
	  this->handle_error(status);
	  return -1;
	}
	append_output(this->tmp_buffer.begin(), status);
      }
      return int64_t(length);
    }
    /// ------------------------------------------------------------------------------
    template <> int64_t
    concrete_compressor<lz4_stream_t>::decompress(uint8_t* in_buf, int64_t len)  {
      for( const uint8_t *end=in_buf + len; in_buf < end; )  {
	uint64_t input_size  = end - in_buf;
	uint64_t output_size = this->tmp_buffer.length();
	uint64_t ret = ::LZ4F_decompress(this->stream.decompress,
					 this->tmp_buffer.begin(),
					 &output_size,
					 in_buf,
					 &input_size,
					 /* LZ4F_decompressOptions_t */ nullptr);
	if( ::LZ4F_isError(ret) )  {
	  print(this->file.debug, "%s: %s: Decompression error: %s",
		this->name(), this->type(), ::LZ4F_getErrorName(ret));
	  return -1;
	}
	if( output_size > 0 )  {
	  this->out_total += len;
	  this->out_buffer.append(this->tmp_buffer.begin(), output_size);
	}
	in_buf += input_size;
      }
      return len;
    }
    /// ------------------------------------------------------------------------------
    template <> void concrete_compressor<lz4_stream_t>::end_reading()  {
      ::LZ4F_freeDecompressionContext(this->stream.decompress);
      this->stream.decompress = nullptr;
    }
    /// ------------------------------------------------------------------------------    
    template <> void concrete_compressor<lz4_stream_t>::end_writing()  {
      std::size_t status = ::LZ4F_compressEnd(this->stream.compress,
					      this->tmp_buffer.begin(),
					      this->tmp_buffer.length(),
					      NULL);
      append_output(this->tmp_buffer.begin(), status);
      ::LZ4F_freeCompressionContext(this->stream.compress);
      this->stream.compress = nullptr;
    }
    /// ------------------------------------------------------------------------------
    template <> int concrete_compressor<lz4_stream_t>::begin_writing()  {
      static LZ4F_preferences_t prefs = {
	{ LZ4F_max256KB,
	  LZ4F_blockLinked,
	  LZ4F_noContentChecksum,
	  LZ4F_frame,
	  0 /* unknown content size */,
	  0 /* no dictID            */ ,
	  LZ4F_noBlockChecksum },
	this->stream.level,      /* compression level; 0 == default */
	this->stream.autoflush,  /* autoflush */
	0,                       /* favor decompression speed */
	{ 0, 0, 0 },             /* reserved, must be set to 0 */
      };

      this->in_size   = std::max(this->in_size, uint64_t(LZ4F_HEADER_SIZE_MAX));
      this->tmp_size  = ::LZ4F_compressBound(this->in_size, &prefs);  // large enough for any input <= this->in_size
      std::size_t ctx = ::LZ4F_createCompressionContext(&this->stream.compress, LZ4F_VERSION);
      if( ::LZ4F_isError(ctx) ) {
	print("%s: LZ4: FAILED to initialize compressor: %ld", this->name(), ctx);
	return -1;
      }
      this->in_buffer.allocate(this->in_size);
      this->tmp_buffer.allocate(this->tmp_size);
      this->out_buffer.allocate(this->out_limit);
      // write frame header
      uint64_t status = ::LZ4F_compressBegin(this->stream.compress,
					     this->tmp_buffer.begin(),
					     this->tmp_buffer.length(),
					     &prefs);
      if( ::LZ4F_isError(status) ) {
	error("%s: %s: Failed to start compression: error %ld [%s]",
	      this->name(), this->type(), status, ::LZ4F_getErrorName(status));
	return -1;
      }
      print("%s: %s: Buffer size is %ld bytes, header size %u bytes",
	    this->name(), this->type(), this->tmp_buffer.length(), status);
      this->out_buffer.append(this->tmp_buffer.begin(), status);
      return 0;
    }
    /// ------------------------------------------------------------------------------
    template <> int concrete_compressor<lz4_stream_t>::begin_reading()  {
      char buff[16*kBYTE];
      LZ4F_frameInfo_t info;
      uint64_t status = ::LZ4F_createDecompressionContext(&this->stream.decompress, LZ4F_VERSION);
      if( ::LZ4F_isError(status) ) {
	print("%s: LZ4: LZ4F_dctx creation error: %s", this->name(), ::LZ4F_getErrorName(status));
      }
      this->tmp_buffer.allocate(this->tmp_size);
      this->out_buffer.allocate(this->out_limit);
      int64_t total = this->file.read(buff, sizeof(buff));
      if( total <= 0 )  {
	print("%s: LZ4 FAILED to open compressed file. read %ld bytes. [Read error: %s]",
	      this->name(), this->in_buffer.length(), this->file.error_message().c_str());
	return -1;
      }
      std::size_t consumed_size = sizeof(buff);
      status = ::LZ4F_getFrameInfo(this->stream.decompress, &info, buff, &consumed_size);
      if( ::LZ4F_isError(status) )  {
	print("%s: LZ4: frame info error: %s", this->name(), ::LZ4F_getErrorName(status));
	return -1;
      }
      std::size_t rest = total-consumed_size;
      std::size_t block_size = posix_lz4_block_size(&info);
      this->position = 0;
      this->out_buffer.set_cursor(0);
      this->in_buffer.allocate(this->in_size = block_size);
      this->in_buffer.append(buff+consumed_size, rest);
      total = this->file.read(this->in_buffer.ptr(), this->in_size-rest);
      if( total == 0 )  {
	// A priori no error. It may be a small file after all.
      }
      else if( total < 0 )  {
	print("%s: LZ4 FAILED to load compressed file. read %ld bytes. [Read error: %s]",
	      this->name(), this->in_buffer.length(), this->file.error_message().c_str());
	return -1;
      }
      this->in_buffer.set_cursor(total+rest);
      if( !this->decompress(this->in_buffer.begin(), total+rest) )   {
	return -1;
      }
      return 0;
    }
    /// ------------------------------------------------------------------------------
    template <> int concrete_compressor<lz4_stream_t>::posix_open( const char* path, int flags, ... ) {
      va_list args;
      va_start(args, flags);
      auto strm = std::make_unique<concrete_compressor<lz4_stream_t> >("LZ4");
      return DataCompressor::open(std::move(strm), path, flags, args);
    }
    /// --------------------------------------------------------------------------------
    posix_t* descriptor_lz4() {
      static posix_t p;
      if( !p.open )  {
	compress::DataCompressor::init_descriptor(p);
	p.open = compress::concrete_compressor<compress::lz4_stream_t>::posix_open;
      }
      return &p;
    }
  }     // End namespace compress
}       // End namespace Online
/// --------------------------------------------------------------------------------
#else
namespace Online { namespace compress {
Online::posix_t* descriptor_lz4() {
  throw std::runtime_error("No LZ4 compression availible!");
}}}
#endif
/// --------------------------------------------------------------------------------
extern "C" Online::posix_t* posix_descriptor_lz4() {
  return Online::compress::descriptor_lz4();
}
