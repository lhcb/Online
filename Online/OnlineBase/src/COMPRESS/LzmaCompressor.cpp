//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Project    : Online
//
//  Author     : M.Frank
//
//==========================================================================

/// Framework include files
#include <RTL/DataCompressor.h>
#include <RTL/Compress.h>

#if defined(ONLINE_HAVE_LZMA)

/// Framework include files
#include <RTL/posix.h>

/// C/C++ include files
#include <lzma.h>

/// Online namespace declaration
namespace Online  {

  /// Namespace for the compression utilities
  namespace compress  {

    struct lzma_stream_t {
      lzma_stream imp = LZMA_STREAM_INIT;
      int preset      { LZMA_PRESET_DEFAULT };
      ~lzma_stream_t() = default;
    };
    /// ------------------------------------------------------------------------------
    template <> int concrete_compressor<lzma_stream_t>::handle_error(uint64_t code)  {
      lzma_ret ret = (lzma_ret)code;
      switch (ret) {
      case LZMA_OK:
	return 1;
      default:
	error("%s: LZMA: %s", this->name(), lzma_error_message(code).c_str());
	return 0;
      }
      return 0;
    }
    /// ------------------------------------------------------------------------------
    template <> int
    concrete_compressor<lzma_stream_t>::set_option(const std::string& opt, const std::string& val)  {
      if ( opt.substr(0, 8) == "compress" )  {
	std::string value = val;
	if      ( val == "none"    ) value = "0";
	else if ( val == "default" ) value = "2";
	else if ( val == "fast"    ) value = "1";
	else if ( val == "high"    ) value = "9e";
	auto comp = parse_digit(value);
	if ( comp.first )  {
	  this->stream.preset = comp.second;
	  if ( value.length()>1 && value[1] == 'e')  {
	    this->stream.preset |= LZMA_PRESET_EXTREME;
	  }
	  return 0;
	}
      }
      return this->DataCompressor::set_option(opt, val);
    }
    /// ------------------------------------------------------------------------------
    template <> int concrete_compressor<lzma_stream_t>::begin_writing()  {
      ::lzma_options_lzma opt_lzma;
      this->stream.imp = LZMA_STREAM_INIT;
      if ( ::lzma_lzma_preset(&opt_lzma, this->stream.preset) ) {
	error("%s: LZMA: Unsupported preset, possibly a bug.", this->name());
	return -1;
      }
      ::lzma_filter filters[3];
      filters[0].id = LZMA_FILTER_X86;
      filters[0].options = NULL;
      filters[1].id = LZMA_FILTER_LZMA2;
      filters[1].options = &opt_lzma;
      filters[2].id = LZMA_VLI_UNKNOWN;
      filters[2].options = NULL;

      // Initialize the encoder using the custom filter chain.
      ::lzma_ret ret = ::lzma_stream_encoder(&this->stream.imp, filters, LZMA_CHECK_CRC64);
      if ( ret == LZMA_OK ) return 0;
      this->handle_error(ret);
      return -1;
    }
    /// ------------------------------------------------------------------------------    
    template <> void concrete_compressor<lzma_stream_t>::end_writing()  {
      ::lzma_end(&this->stream.imp);
    }
    /// ------------------------------------------------------------------------------
    template <> int concrete_compressor<lzma_stream_t>::begin_reading()  {
      // Initialize the encoder using the custom filter chain.
      ::lzma_ret ret = ::lzma_stream_decoder(&this->stream.imp, UINT64_MAX, LZMA_CONCATENATED);
      if ( ret == LZMA_OK ) return 0;
      this->handle_error(ret);
      return -1;
    }
    /// ------------------------------------------------------------------------------
    template <> void concrete_compressor<lzma_stream_t>::end_reading()  {
      ::lzma_end(&this->stream.imp);
    }
    /// ------------------------------------------------------------------------------
    template <> int64_t
    concrete_compressor<lzma_stream_t>::compress(const uint8_t* buffer, std::size_t len)  {
      auto& strm = this->stream.imp;
      this->in_total += len;
      ::lzma_action action = this->finished ? LZMA_FINISH : LZMA_RUN;
      for( strm.avail_in = len, strm.next_in = buffer; strm.avail_in > 0; )  {
	strm.next_out   = this->tmp_buffer.begin();
	strm.avail_out  = this->tmp_buffer.length();
	::lzma_ret ret  = ::lzma_code(&strm, action);
	if ( !(ret == LZMA_OK || ret == LZMA_STREAM_END) )  {
	  this->handle_error(ret);
	  return -1;
	}
	this->append_output(this->tmp_buffer.begin(), this->tmp_buffer.length() - strm.avail_out);
      }
      return len;
    }
    /// ------------------------------------------------------------------------------
    template <> int64_t
    concrete_compressor<lzma_stream_t>::decompress(uint8_t* buff, int64_t total)  {
      auto& strm = this->stream.imp;
      strm.next_in  = buff;
      for( strm.avail_in = total; strm.avail_in > 0; )  {
	int64_t out_now = strm.total_out;
	strm.next_out   = this->tmp_buffer.begin();
	strm.avail_out  = this->tmp_buffer.length();
	::lzma_ret  ret = ::lzma_code(&strm, LZMA_RUN);
	int64_t    done = strm.total_out - out_now;
	if ( ret != LZMA_OK )  {
	  this->handle_error(ret);
	  return -1;
	}
	if ( done > 0 )  {
	  this->out_total += done;
	  this->out_buffer.append(this->tmp_buffer.begin(), done);
	}
      }
      return total;
    }
    /// ------------------------------------------------------------------------------
    template <> int
    concrete_compressor<lzma_stream_t>::posix_open( const char* path, int flags, ... ) {
      va_list args;
      va_start(args, flags);
      auto strm = std::make_unique<concrete_compressor<lzma_stream_t> >("LZMA");
      return DataCompressor::open(std::move(strm), path, flags, args);
    }
    /// --------------------------------------------------------------------------------
    Online::posix_t* descriptor_lzma() {
      using namespace Online;
      static posix_t p;
      if ( !p.open )  {
	compress::DataCompressor::init_descriptor(p);
	p.open = compress::concrete_compressor<compress::lzma_stream_t>::posix_open;
      }
      return &p;
    }
  }     // End namespace compress
}       // End namespace Online
/// --------------------------------------------------------------------------------
#else
namespace Online { namespace compress {
Online::posix_t* descriptor_lzma() {
  throw std::runtime_error("No LZMA compression availible!");
}}}
#endif
/// --------------------------------------------------------------------------------
extern "C" Online::posix_t* posix_descriptor_lzma() {
  return Online::compress::descriptor_lzma();
}
