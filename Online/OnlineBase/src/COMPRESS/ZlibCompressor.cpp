//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Project    : Online
//
//  Author     : M.Frank
//
//==========================================================================

/// Framework include files
#include <RTL/DataCompressor.h>

#if defined(ONLINE_HAVE_ZLIB)

/// Framework include files
#include <RTL/posix.h>
#include <RTL/strdef.h>

/// C/C++ include files
#include <algorithm>
#include <vector>
#include <zlib.h>

#define MEM_LEVEL         8
#define WINDOW_BITS      15
#define ENABLE_ZLIB_GZIP 32
#define GZIP_ENCODING    16

/// Online namespace declaration
namespace Online   {

  /// Namespace for the compression utilities
  namespace compress   {

    struct zlib_stream_t {
      ::z_stream     strm         { };
      int32_t        strategy     { Z_DEFAULT_STRATEGY };
      int32_t        compression  { Z_DEFAULT_COMPRESSION };
      int32_t        window_bits  { WINDOW_BITS };
      int32_t        mem_level    { MEM_LEVEL };
      ~zlib_stream_t() = default;
    };
    /// ------------------------------------------------------------------------------
    template <> int concrete_compressor<zlib_stream_t>::handle_error(uint64_t code)   {
      int status = (int)code;
      switch(status)   {
      case Z_OK:
	return 0;
      case Z_STREAM_END:
	return 0;
      case Z_NEED_DICT:
	return 0;
      case Z_ERRNO:
	error("%s: %s: Error code: [%ld Z_ERRNO]", this->name(), this->type(), code);
        return -1;
      case Z_STREAM_ERROR:
	error("%s: %s: Error code: [%ld Z_STREAM_ERROR]", this->name(), this->type(), code);
        return -1;
      case Z_DATA_ERROR:
	error("%s: %s: Error code: [%ld Z_DATA_ERROR]", this->name(), this->type(), code);
        return -1;
      case Z_MEM_ERROR:
	error("%s: %s: Error code: [%ld Z_MEM_ERROR]", this->name(), this->type(), code);
        return -1;
      case Z_BUF_ERROR:
	error("%s: %s: Error code: [%ld Z_BUF_ERROR]", this->name(), this->type(), code);
        return -1;
      case Z_VERSION_ERROR:
	error("%s: %s: Error code: [%ld Z_VERSION_ERROR]", this->name(), this->type(), code);
        return -1;
      default:
	break;
      }
      error("%s: %s: Unknown error code: %ld", this->name(), this->type(), code);
      return 0;
    }
    /// ------------------------------------------------------------------------------
    template <> int
    concrete_compressor<zlib_stream_t>::set_option(const std::string& opt, const std::string& val)  {
      if ( opt.substr(0, 8) == "strategy" )   {
	if ( val == "default" )
	  this->stream.strategy = Z_DEFAULT_STRATEGY;
	else if ( val == "filtered" )
	  this->stream.strategy = Z_FILTERED;
	else if ( val == "fixed" )
	  this->stream.strategy = Z_FIXED;
	else if ( val == "rle" )
	  this->stream.strategy = Z_RLE;
	else if ( val.substr(0,4) == "huff" )
	  this->stream.strategy = Z_HUFFMAN_ONLY;
	else
	  goto Error;
	return 0;
      }
      else if ( opt.substr(0, 8) == "compress" )   {
	if ( val == "default" )
	  this->stream.compression = Z_DEFAULT_COMPRESSION;
	else if ( val == "fast" )
	  this->stream.compression = Z_BEST_SPEED;
	else if ( val == "best" )
	  this->stream.compression = Z_BEST_COMPRESSION;
	else if ( val == "none" )
	  this->stream.compression = Z_NO_COMPRESSION;
	else  {
	  char* end = 0;
	  int level = ::strtol(val.c_str(), &end, 10);
	  if ( level >= 0 && level <= 9 )
	    this->stream.compression = level;
	  else
	    goto Error;
	}
	return 0;
      }
      else if ( opt == "window_bits" )   {
	this->stream.window_bits = (val == "default") ? WINDOW_BITS : ::atol(val.c_str());
      }
      else if ( opt == "mem_level" )   {
	this->stream.mem_level = (val == "default") ? MEM_LEVEL : ::atol(val.c_str());
      }
    Error:
      return this->DataCompressor::set_option(opt, val);
    }
    /// ------------------------------------------------------------------------------
    template <> int concrete_compressor<zlib_stream_t>::begin_reading()  {
      uint8_t buff[128];
      ::z_stream& strm = this->stream.strm;
      ::memset(&strm, 0, sizeof(strm));
      strm.zalloc   = Z_NULL;
      strm.zfree    = Z_NULL;
      strm.opaque   = Z_NULL; 
      strm.next_in  = buff;
      strm.avail_in = 0;
      int status = ::inflateInit2(&strm, this->stream.window_bits + ENABLE_ZLIB_GZIP);
      if ( status < 0 )   {
	errno = EINVAL;
	return -1;
      }
      return 0;
    }
    /// ------------------------------------------------------------------------------
    template <> void concrete_compressor<zlib_stream_t>::end_reading()  {
      ::inflateEnd(&this->stream.strm);
    }
    /// ------------------------------------------------------------------------------
    template <> int64_t
    concrete_compressor<zlib_stream_t>::decompress(uint8_t* in_buf, int64_t len)  {
      ::z_stream& strm = this->stream.strm;
      int64_t  buf_len = this->tmp_buffer.length();
      int64_t  out_size = 0;
      strm.avail_in = len;
      strm.next_in  = (uint8_t*)in_buf;
      do {
	strm.avail_out = buf_len;
	strm.next_out  = this->tmp_buffer.begin();
	out_size = -1;
	int status = ::inflate (&strm, Z_NO_FLUSH);
	switch (status) {
	case Z_OK:
	case Z_STREAM_END:
	  out_size = buf_len - strm.avail_out;
	  break;
	case Z_BUF_ERROR:
	  errno = ENOSPC;
	  return -1;
	default:
	  errno = EINVAL;
	  return -1;
	}
	if ( out_size > 0 )  {
	  this->out_total += len;
	  this->out_buffer.append(this->tmp_buffer.begin(), out_size);
	}
      }  while (strm.avail_out == 0);
      return len;
    }
    /// ------------------------------------------------------------------------------
    template <> int concrete_compressor<zlib_stream_t>::begin_writing()  {
      uint8_t buff[128];
      ::z_stream& strm = this->stream.strm;
      strm.zalloc = Z_NULL;
      strm.zfree  = Z_NULL;
      strm.opaque = Z_NULL;
      strm.avail_in = 0;
      strm.next_in  = buff;
      int status = ::deflateInit2( &strm, 
				   this->stream.compression,
				   Z_DEFLATED,
				   this->stream.window_bits + GZIP_ENCODING,
				   this->stream.mem_level,
				   this->stream.strategy);
      if ( status < 0 )   {
	errno = ENOSPC;
	return -1;
      }
      return 0;
    }
    /// ------------------------------------------------------------------------------    
    template <> void concrete_compressor<zlib_stream_t>::end_writing()  {
      ::deflateEnd(&this->stream.strm);
    }
    /// ------------------------------------------------------------------------------
    template <> int64_t
    concrete_compressor<zlib_stream_t>::compress( const uint8_t* buff, std::size_t length )  {
      ::z_stream& strm = this->stream.strm;
      int64_t  buf_len = this->tmp_buffer.length();
      int64_t  total_out, done;
      strm.next_in  = (uint8_t*)buff;
      strm.avail_in = length;
      do  {
	total_out      = strm.total_out;
	strm.avail_out = buf_len;
	strm.next_out  = this->tmp_buffer.begin();
	int status = ::deflate( &strm, this->finished ? Z_FINISH : Z_NO_FLUSH );
	if ( status < 0 )  {
	  errno = ENOSPC;
	  return -1;
	}
	if ( strm.avail_out == 0 )   {
	  this->tmp_buffer.allocate(buf_len = buf_len*2);
	  this->tmp_buffer.set_cursor(0);
	  continue;
	}
	done = strm.total_out - total_out;
	append_output( this->tmp_buffer.begin(), done );
      }  while ( strm.avail_in > 0 );
      this->in_total += length;
      return int64_t(length);
    }
    /// ------------------------------------------------------------------------------
    template <> int concrete_compressor<zlib_stream_t>::posix_open( const char* path, int flags, ... ) {
      va_list args;
      va_start(args, flags);
      auto strm = std::make_unique<concrete_compressor<zlib_stream_t> >("ZLIB");
      return DataCompressor::open(std::move(strm), path, flags, args);
    }
    /// --------------------------------------------------------------------------------
    Online::posix_t* descriptor_zlib() {
      using namespace Online;
      static posix_t p;
      if ( !p.open )  {
	compress::DataCompressor::init_descriptor(p);
	p.open = compress::concrete_compressor<compress::zlib_stream_t>::posix_open;
      }
      return &p;
    }
  }     // End namespace compress
}       // End namespace Online
/// --------------------------------------------------------------------------------
#else
namespace Online { namespace compress {
    Online::posix_t* descriptor_zlib() {
      throw std::runtime_error("No ZLIB compression availible!");
    }}}
#endif
/// --------------------------------------------------------------------------------
/// Online namespace declaration
namespace Online   {
  /// Namespace for the compression utilities
  namespace compress   {
    Online::posix_t* descriptor_gzip() {
      return descriptor_zlib();
    }
  }     // End namespace compress
}       // End namespace Online
/// --------------------------------------------------------------------------------
extern "C"  {
  Online::posix_t* posix_descriptor_zlib() {
    return Online::compress::descriptor_zlib();
  }
  Online::posix_t* posix_descriptor_gzip() {
    return Online::compress::descriptor_gzip();
  }
}
/// --------------------------------------------------------------------------------
