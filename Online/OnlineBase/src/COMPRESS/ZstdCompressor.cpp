//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//
//  Project    : Online
//
//  Author     : M.Frank
//
//==========================================================================
/// Framework include files
#include <RTL/DataCompressor.h>

#if defined(ONLINE_HAVE_ZSTD)

/// Framework include files
#include <RTL/posix.h>
#include <RTL/strdef.h>

/// C/C++ include files
#include <algorithm>
#include <vector>
#include <zstd.h>

/// Online namespace declaration
namespace Online   {

  /// Namespace for the compression utilities
  namespace compress   {

    struct zstd_stream_t {
      std::vector<std::pair<ZSTD_cParameter,int> > cparams;
      std::vector<std::pair<ZSTD_dParameter,int> > dparams;
      ZSTD_CCtx*     compress     { nullptr };
      ZSTD_DCtx*     decompress   { nullptr };
      int32_t        level        { 0 };
      uint32_t       autoflush    { 0 };
      ~zstd_stream_t() = default;
    };
    /// ------------------------------------------------------------------------------
    template <> int concrete_compressor<zstd_stream_t>::handle_error(uint64_t code)   {
      error("%s: %s: Unknown error code: %ld", this->name(), this->type(), code);
      return 0;
    }
    /// ------------------------------------------------------------------------------
    template <> int
    concrete_compressor<zstd_stream_t>::set_option(const std::string& opt, const std::string& val)  {
      if ( opt.substr(0, 8) == "strategy" )   {
	if ( val == "default" )
	  this->stream.cparams.emplace_back(std::make_pair( ZSTD_c_strategy, ZSTD_fast ));
	else if ( val == "fast" )
	  this->stream.cparams.emplace_back(std::make_pair( ZSTD_c_strategy, ZSTD_fast ));
	else if ( val == "dfast" )
	  this->stream.cparams.emplace_back(std::make_pair( ZSTD_c_strategy, ZSTD_dfast ));
	else if ( val == "greedy" )
	  this->stream.cparams.emplace_back(std::make_pair( ZSTD_c_strategy, ZSTD_greedy ));
	else if ( val == "lazy" )
	  this->stream.cparams.emplace_back(std::make_pair( ZSTD_c_strategy, ZSTD_lazy ));
	else if ( val == "lazy2" )
	  this->stream.cparams.emplace_back(std::make_pair( ZSTD_c_strategy, ZSTD_lazy2 ));
	else if ( val == "btlazy2" )
	  this->stream.cparams.emplace_back(std::make_pair( ZSTD_c_strategy, ZSTD_btlazy2 ));
	else if ( val == "btopt" )
	  this->stream.cparams.emplace_back(std::make_pair( ZSTD_c_strategy, ZSTD_btopt ));
	else if ( val == "btultra" )
	  this->stream.cparams.emplace_back(std::make_pair( ZSTD_c_strategy, ZSTD_btultra ));
	else if ( val == "btultra2" )
	  this->stream.cparams.emplace_back(std::make_pair( ZSTD_c_strategy, ZSTD_btultra2 ));
	else
	  goto Error;
	return 0;
      }
      else if ( opt.substr(0, 8) == "compress" )   {
	if ( val == "default" )
	  this->stream.cparams.emplace_back(std::make_pair( ZSTD_c_compressionLevel, ZSTD_defaultCLevel()));
	else if ( val == "fast" )
	  this->stream.cparams.emplace_back(std::make_pair( ZSTD_c_compressionLevel, 1 ));
	else if ( val == "best" )
	  this->stream.cparams.emplace_back(std::make_pair( ZSTD_c_compressionLevel, 19 ));
	else  {
	  int level = ::atol(val.c_str());
	  if ( level >= ZSTD_minCLevel() && level <= ZSTD_maxCLevel() )
	    this->stream.cparams.emplace_back(std::make_pair( ZSTD_c_compressionLevel, level ));
	  else
	    goto Error;
	}
	return 0;
      }
    Error:
      return this->DataCompressor::set_option(opt, val);
    }
    /// ------------------------------------------------------------------------------
    template <> int64_t
    concrete_compressor<zstd_stream_t>::compress( const uint8_t* buff, std::size_t length )   {
      ::ZSTD_inBuffer in = { buff, length, 0 };
      while ( in.pos != in.size )  {
	::ZSTD_outBuffer out = { this->tmp_buffer.begin(), this->tmp_buffer.length(), 0 };
	std::size_t      ret = ::ZSTD_compressStream2(this->stream.compress, &out, &in,
						      this->finished ? ZSTD_e_end : ZSTD_e_continue);
	if ( ::ZSTD_isError(ret) )  {
	  print("%s: %s: FAILED to compress %ld bytes [compression error: %s]",
		this->name(), this->type(), length, ::ZSTD_getErrorName(ret));
	  return -1;
	}
	append_output( this->tmp_buffer.begin(), out.pos);
      }
      return int64_t(length);
    }
    /// ------------------------------------------------------------------------------
    template <> int64_t
    concrete_compressor<zstd_stream_t>::decompress( uint8_t* in_buf, int64_t len )  {
      int64_t done = 0;
      ::ZSTD_inBuffer in = { in_buf, uint64_t(len), 0 };
      while ( in.pos < in.size )  {
	::ZSTD_outBuffer out = { this->tmp_buffer.begin(), this->tmp_buffer.length(), 0 };
	std::size_t      ret = ::ZSTD_decompressStream(this->stream.decompress, &out , &in);
	if ( ZSTD_isError(ret) )   {
	  print("%s: %s: FAILED to decompress %ld bytes [compression error: %s]",
		this->name(), this->type(), len, ::ZSTD_getErrorName(ret));
	  return done > 0 ? done : -1;
	}
	this->out_buffer.append(this->tmp_buffer.begin(), out.pos);
	done += out.pos;
      }
      return done;
    }
    /// ------------------------------------------------------------------------------
    template <> void concrete_compressor<zstd_stream_t>::end_reading()  {
      ::ZSTD_freeDCtx( this->stream.decompress );
      this->stream.decompress = nullptr;
    }
    /// ------------------------------------------------------------------------------    
    template <> void concrete_compressor<zstd_stream_t>::end_writing()  {
      ::ZSTD_freeCCtx( this->stream.compress );
      this->stream.compress = nullptr;
    }
    /// ------------------------------------------------------------------------------
    template <> int concrete_compressor<zstd_stream_t>::begin_writing()  {
      this->stream.compress = ::ZSTD_createCCtx();
      for( const auto& p : this->stream.cparams )
	::ZSTD_CCtx_setParameter(this->stream.compress, p.first, p.second);
      this->in_size   = ::ZSTD_CStreamInSize();
      this->out_total = ::ZSTD_CStreamOutSize();
      return 0;
    }
    /// ------------------------------------------------------------------------------
    template <> int concrete_compressor<zstd_stream_t>::begin_reading()  {
      this->stream.decompress = ::ZSTD_createDCtx();
      for( const auto& p : this->stream.dparams )
	::ZSTD_DCtx_setParameter(this->stream.decompress, p.first, p.second);
      this->in_size   = ::ZSTD_DStreamInSize();
      this->out_total = ::ZSTD_DStreamOutSize();
      this->out_limit = std::max(this->out_limit, this->out_total);
      this->tmp_size  = this->out_total;
      return 0;
    }
    /// ------------------------------------------------------------------------------
    template <> int concrete_compressor<zstd_stream_t>::posix_open( const char* path, int flags, ... ) {
      va_list args;
      va_start(args, flags);
      auto strm = std::make_unique<concrete_compressor<zstd_stream_t> >("ZSTD");
      return DataCompressor::open(std::move(strm), path, flags, args);
    }
    /// --------------------------------------------------------------------------------
    Online::posix_t* descriptor_zstd() {
      using namespace Online;
      static posix_t p;
      if ( !p.open )  {
	compress::DataCompressor::init_descriptor(p);
	p.open = compress::concrete_compressor<compress::zstd_stream_t>::posix_open;
      }
      return &p;
    }
  }     // End namespace compress
}       // End namespace Online

/// --------------------------------------------------------------------------------
#else
namespace Online { namespace compress {
Online::posix_t* descriptor_zstd() {
  throw std::runtime_error("No ZSTD compression availible!");
}}}
#endif
/// --------------------------------------------------------------------------------
extern "C" Online::posix_t* posix_descriptor_zstd() {
  return Online::compress::descriptor_zstd();
}
/// --------------------------------------------------------------------------------
