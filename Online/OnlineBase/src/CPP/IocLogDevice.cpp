//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================

/// Framework include files
#include "CPP/IocLogDevice.h"
#include "CPP/IocSensor.h"

/// C/C++ include files
#include <stdexcept>
#include <cstring>

using namespace CPP;

/// Default constructor
IocLogDevice::IocLogDevice(Interactor* t, int c) 
  : RTL::Logger::LogDevice(), target(t), command(c)
{
  outputLevel = LIB_RTL_INFO;
  compileFormat("%TIME%LEVEL%-8NODE: %-32PROCESS %-20SOURCE");
}

/// Calls the display action with a given severity level
size_t IocLogDevice::printmsg(int severity, const char* source, const char* msg)  const  {
  return print_record(false, severity, source, msg);
}

/// Error handling. Throws exception
void IocLogDevice::exceptmsg(const char* source, const char* msg) const    {
  print_record(true, LIB_RTL_ERROR, source, msg);
}
#define BUF_SZ  512
/// Write output .....
size_t IocLogDevice::print_record(bool exc, int severity, const char* source, const char* text)  const {
  char header[BUF_SZ];
  if ( severity > LIB_RTL_ALWAYS   ) severity = LIB_RTL_ALWAYS;
  if ( severity < LIB_RTL_VERBOSE  ) severity = LIB_RTL_VERBOSE;
  if ( severity < outputLevel )  {
    return 0;
  }
  size_t hdr_len = formatHeader(header, BUF_SZ-1, severity, source);
  size_t txt_len = ::strlen(text);
  char* msg = new char[hdr_len+txt_len+1];
  ::strncpy(msg, header, hdr_len);
  ::strncpy(msg+hdr_len, text, txt_len);
  msg[hdr_len+txt_len] = 0;
  IocSensor::instance().send(target,command,msg);
  if ( exc )   {
    throw std::runtime_error(msg);
  }
  return hdr_len+txt_len;
}
