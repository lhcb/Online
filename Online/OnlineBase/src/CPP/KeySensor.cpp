//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================

/// Framework include files
#include <NET/IOPortManager.h>
#include <CPP/Interactor.h>
#include <CPP/KeySensor.h>
#include <RTL/SysTime.h>
#include <CPP/Event.h>
#include <WT/wtdef.h>
#include <RTL/rtl.h>

/// C/C++ include files
#include <map>
#include <cerrno>
#include <vector>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <sys/ioctl.h>
#include <unistd.h>
#include <termios.h>

extern "C" int scrc_check_key_buffer (char *buffer);

namespace  {
  typedef unsigned char uc8;
}


class CPP::KeySensor::internals_t  {
public:
  typedef std::vector<std::pair<CPP::Interactor*, void*> > Clients;
  constexpr static int KEY_BUF_SIZE = 80;
  /// Terminal settings
  struct termios neu, alt; 
  /// Buffer with clients
  Clients clients;
  int     num_key_call = 0;
  int     num_mouse_call = 0;
  int     num_stroke_call = 0;
  char    mouse_data[6] = {0, 0, 0, 0, 0, 0 };
  char    armed = 0;
  bool    debug = false;

  static int ast_keyboard (void* par)   {
    ::wtc_insert (WT_FACILITY_KEYBOARD, par);
    return WT_SUCCESS;
  }

  /// Default constructor
  internals_t()  {
    int fd = fileno(stdin);
    ::tcgetattr(fd, &this->alt);
    this->neu = this->alt;
    this->neu.c_lflag &= ~(ICANON|ECHO); 
    this->neu.c_cc[VMIN] = 1;
    this->neu.c_cc[VTIME] = 0;
    ::tcsetattr(fd, TCSANOW, &this->neu); 
    ::fflush (stdout);
    ::fprintf(stdout, "\033=");
    ::fflush (stdout);
    ::fprintf(stdout, "%c[?9h", char(27));
    ::fflush (stdout);
  }
  /// Default destructor
  ~internals_t()  {
    int fd = fileno(stdin); 
    ::tcsetattr(fd, TCSANOW, &this->alt); 
  }
  /// Mouse handler
  bool handle_mouse(char key)  {
    char* data = this->mouse_data;
    data[0] = data[1];
    data[1] = data[2];
    data[2] = data[3];
    data[3] = data[4];
    data[4] = data[5];
    data[5] = key;
    if ( data[0] == 0x1B && data[1] == '[' && data[2] == 'M' ) {
      return true;
    }
    return false;
  }
  /// Interrupt dispatcher
  void dispatch( void* /* arg */ )  {
    char Key_buffer[internals_t::KEY_BUF_SIZE+2] = "";
    int  status, stroke, Key_ptr = 0;
    int  fd = ::fileno( stdin );
    while( (status=IOPortManager::getAvailBytes(fd)) > 0 )  {
      char key = 0;
      IOPortManager::getChar(fd, &key);
      if ( Key_ptr >= internals_t::KEY_BUF_SIZE )  {
	status = 0;
      }
      else if ( key )  {
	++this->num_key_call;
	if ( this->handle_mouse(key) )   {
	  const auto* data = this->mouse_data;
	  CPP::Event ev(0, ScrMouseEvent);
	  new(ev.get<void>()) KeyEvent(0, data[3], data[4], data[5]);
	  this->handle(ev);
	}
	Key_buffer[Key_ptr] = key;
	Key_ptr++;
	Key_buffer[Key_ptr] = 0;
	if ( (stroke=::scrc_check_key_buffer(Key_buffer)) > 0 )  {
	  Key_ptr = 0;
	  Key_buffer[Key_ptr] = 0;
	  CPP::Event ev(0, KeyboardEvent);
	  new(ev.get<void>()) KeyEvent(stroke);
	  this->handle(ev);
	}
      }
    }
    this->armed = 0;
  }
  /// Client calling
  void handle(Event& ev)  {
    KeyEvent* event = ev.get<KeyEvent>();
    if ( this->debug )   {
      char* data = this->mouse_data;
      if ( ev.type == CPP::KeyboardEvent )  {
	++num_stroke_call;
	::printf("KeySensor::dispatch[%d]: Got stroke: %d %8X\n",
		 num_key_call, num_stroke_call, event->stroke, event->stroke);
      }
      else  {
	++num_mouse_call;
	::printf("KeySensor::mouse[%d, %d]: Got stroke: %d x:%d y:%d\n",
		 num_key_call, num_mouse_call, data[3], data[4], data[5] );
      }
    }
    for( auto& cl : clients )  {
      event->data = cl.second;
      cl.first->handle(ev);
    }
  }
};

/// Standard constructor
CPP::KeyEvent::KeyEvent(int key, int x_val, int y_val, unsigned int ms) 
  : stroke(key), button(uc8(key&0x3)), modifier(uc8(key>>2)), x(uc8(x_val-0x20)), y(uc8(y_val-0x20)), msec(ms)
{
}

/// Standard constructor
CPP::KeySensor::KeySensor()
  : CPP::Sensor(WT_FACILITY_KEYBOARD, "KeyboardSensor", true)
{
  imp = std::make_unique<internals_t>();
}

/// Standard destructor
CPP::KeySensor::~KeySensor()  {
}

/// The KeySensor is a singleton: Static instantiator
CPP::KeySensor& CPP::KeySensor::instance()  {
  static KeySensor* sensor = new KeySensor;
  return *sensor;
}

/// Enable debug flag
void CPP::KeySensor::setDebug(bool yes_no)   {
  imp->debug = yes_no;
}

/// Rearm keyboard sensor
void CPP::KeySensor::rearm() {
  if ( 0 == imp->armed )  {
    imp->armed = 1;
    IOPortManager(0).addEx(0, ::fileno(stdin), internals_t::ast_keyboard, this);
  }
}

/// Subscribe Interactor target to display key-events
void CPP::KeySensor::add(CPP::Interactor* actor, void* data) {
  for( const auto& cl : imp->clients )
    if ( cl.first == actor && cl.second == data ) return;
  imp->clients.emplace_back( std::make_pair(actor, data) );
}

/// Unsubscribe Interactor target from display key-events
void CPP::KeySensor::remove(CPP::Interactor* actor, void* data) {
  internals_t::Clients::iterator i = std::begin(imp->clients);
  for( ; i != std::end(imp->clients); ++i)  {
    if ( i->first == actor && (data == nullptr || data == i->second) )  {
      imp->clients.erase(i);
      i = imp->clients.begin();
    }
  }
}

/// Subscribe Interactor target to display key-events
void CPP::KeySensor::add(CPP::Interactor* actor, long data) {
  this->add(actor, (void*)data);
}

/// Unsubscribe Interactor target from display key-events
void CPP::KeySensor::remove(CPP::Interactor* actor, long data) {
  this->remove(actor, (void*)data);
}

/// Sensor overload: Dispatch interrupts and deliver callbacks to clients
void CPP::KeySensor::dispatch( void* arg )  {
  imp->dispatch(arg);
}
