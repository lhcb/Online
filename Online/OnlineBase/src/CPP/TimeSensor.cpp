//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================

/// Framework include files
#include "CPP/Event.h"
#include "CPP/TimeSensor.h"   
#include "CPP/Interactor.h"
#include "WT/wtdef.h"
#include "RTL/rtl.h"

/// C/C++ include files
#include <map>
#include <cmath>
#include <cstdio>
#include <cstdlib>

namespace {
  class InteractorTarget;
  
  class InteractorTarget {
    friend      class CPP::TimeSensor;
  private:
    Interactor* Inter;
    void*       Data;
  public:
    /// Initializing constructor
    explicit InteractorTarget( Interactor* i, void* d = 0 ) { Inter = i; Data = d;}
  };

  typedef std::map<TimeSensor::Period*, InteractorTarget*>  InteractorTable;
  static TimeSensor::Period*         s_periodHead      = 0;
  static InteractorTable s_interactorTable;
}

class TimeSensor::Period {
  friend class CPP::TimeSensor;
private:
  long          Time; 
  unsigned long m_alrmID;
  std::string   m_ascTime;
  bool          m_rearmPending;
  bool          m_cyclic;
  Period*       m_next;
public:
  /// Initializing constructor
  explicit Period( const char* time_spec );
  /// Initializing constructor
  explicit Period( double seconds );
  /// Default destructor
  ~Period();
  void set();
  void cancel();
  // unsigned long& id()      {  return m_alrmID;   }
  Period* next()  const    {  return m_next;     }
  void setNext(Period* n)  {  m_next = n;        }
  static int ast( void* param );
};

///----------------------------------------------------------------------------
TimeSensor::Period::Period(const char* period) : m_alrmID(0) {
  int day, hour, minute, sec, msec = 0;
  if( *period == '+' )  period++, m_cyclic = true;
  else                  m_cyclic = false;
  m_ascTime = period;
  if ( 4 == ::sscanf(m_ascTime.c_str(), "%d %d:%d:%d.0", &day, &hour, &minute, &sec, &msec) )  {
    msec = 0;
  }
  else  {
    ::lib_rtl_output(LIB_RTL_ERROR,"CPP:Period: Bad time format: %s [TimeSensor will not work properly]", period);
  }
  Time = (sec + day*86400 + hour*3600 + minute*60)*1000 + msec;
  m_rearmPending = true;
  m_next = 0;
}
/// Initializing constructor
TimeSensor::Period::Period(double seconds) : m_alrmID(0), m_cyclic(false) {
  char times[64];
  long timesec = long(seconds);
  int day      = (timesec)/86400;
  int hour     = (timesec - day*86400)/3600;
  int minute   = (timesec - day*86400 - hour*3600)/60;
  int sec      = (timesec - day*86400 - hour*3600 - minute*60);
  int msec     = int((seconds - std::floor(seconds))*1000);
  ::snprintf(times,sizeof(times),"%d %d:%d:%d.%d", day, hour, minute, sec, msec);
  m_ascTime = times;
  Time = (sec + day*86400 + hour*3600 + minute*60)*1000 + msec;
  m_rearmPending = true;
  m_next = 0;
}

///----------------------------------------------------------------------------
TimeSensor::Period::~Period() {
}
///----------------------------------------------------------------------------
void TimeSensor::Period::cancel() {
  if ( m_alrmID )  {
    int status = ::lib_rtl_kill_timer(m_alrmID);
    if ( !::lib_rtl_is_success(status) )  {
      ::lib_rtl_signal_message(status,"Failed to cancel period timer.");
    }
  }
}

///----------------------------------------------------------------------------
void TimeSensor::Period::set() {
  int status = ::lib_rtl_set_timer(Time, ast, this, &m_alrmID);
  if ( !::lib_rtl_is_success(status) )  {
    ::lib_rtl_signal_message(status,"Failed to set period timer.");
  }
}

///----------------------------------------------------------------------------
int TimeSensor::Period::ast( void* param )   {
  Period *period = (Period*)param;
  period->m_alrmID = 0;
  return wtc_insert(WT_FACILITY_SENSOR1, param);
}

///----------------------------------------------------------------------------
TimeSensor::TimeSensor() : Sensor( WT_FACILITY_SENSOR1, "TimeSensor" )  {
}

///----------------------------------------------------------------------------
TimeSensor::~TimeSensor()   {
  s_interactorTable.clear();
}

///----------------------------------------------------------------------------
void TimeSensor::add( Interactor* interactor, std::unique_ptr<Period>&& period, void* data ) {
  Period* temperiod = period.release();
  InteractorTarget *inttar = new InteractorTarget(interactor,data);
  temperiod->setNext(s_periodHead); 
  s_periodHead = temperiod;  
  s_interactorTable.insert( std::make_pair(temperiod,inttar));
  //---If the TimeSensor is the only active sensor, then it is necessary to
  // rearm it here and not delay it.
  rearm();
  m_rearmPending = false;
}

///----------------------------------------------------------------------------
void TimeSensor::add( Interactor* interactor, void* newperiod)    {
  return this->add(interactor,(const char*)newperiod,0);
}

/// Add interactor
void TimeSensor::add( Interactor* interactor, const char* newperiod, void* data)    {
  this->add( interactor, std::make_unique<Period>(newperiod), (void*)long(data) );
}

/// Add interactor
void TimeSensor::add( Interactor* interactor, int timesec, void* data ) {
  this->add( interactor, std::make_unique<Period>(timesec), (void*)long(data) );
}

/// Add interactor
void TimeSensor::add(Interactor* interactor, int timesec, long data )   {
  this->add( interactor, std::make_unique<Period>(timesec), (void*)data );
}

/// Add interactor
void TimeSensor::add(Interactor* interactor, double seconds, void* param )   {
  this->add( interactor, std::make_unique<Period>(seconds), param );
}

/// Add interactor
void TimeSensor::add(Interactor* interactor, double seconds, long param )   {
  this->add( interactor, std::make_unique<Period>(seconds), (void*)param );
}

//----------------------------------------------------------------------------
void TimeSensor::remove( Interactor* interactor, long data ) {
  this->remove(interactor, (void*)data);
}

//----------------------------------------------------------------------------
void TimeSensor::remove( Interactor* interactor, void* data ) {
  for( Period* pd = s_periodHead, *last = 0; pd; last = pd, pd = pd->next())  {
    InteractorTable::iterator i = s_interactorTable.find(pd);
    if ( i != s_interactorTable.end() )  {
      InteractorTarget *it = (*i).second;
      if( it->Inter == interactor && (data == 0 || it->Data == data) )  {
        s_interactorTable.erase(i);
        pd->cancel();
        if( !last )
          s_periodHead = pd->next();
        else
          last->setNext(pd->next());
        delete pd; 
        delete it;
        break;
      } 
    } 
  }
}

//----------------------------------------------------------------------------
void TimeSensor::dispatch( void* id ) {
  Period *period = (Period*)id;
  InteractorTable::iterator i = s_interactorTable.find(period);
  if ( i != s_interactorTable.end() )  {
    InteractorTarget  *it = (*i).second;
    if( period->m_cyclic )    {
      period->m_rearmPending = true;
      Event ev(it->Inter,TimeEvent);
      ev.timer_id   = id;
      ev.timer_data = it->Data;
      ev.target->handle(ev);       
    }
    else    { 
      if ( period == s_periodHead ) {
        s_periodHead = period->m_next;
      }
      else  {
        for(Period* pd = s_periodHead; pd; pd = pd->m_next)  {
          if( pd->m_next == period ) { 
            pd->m_next = period->m_next; break; 
          }
        }
      }
      s_interactorTable.erase(i);
      Event ev(it->Inter, TimeEvent);
      ev.timer_id   = id;
      ev.timer_data = it->Data;
      delete period; 
      delete it;
      ev.target->handle(ev);       
    }
  }
}
//----------------------------------------------------------------------------
void TimeSensor::rearm()  {
  for(Period * pd = s_periodHead; pd; pd = pd->m_next)  {
    if ( pd->m_rearmPending )  {
      pd->set();
      pd->m_rearmPending = false;
    }
  }
}
//----------------------------------------------------------------------------
TimeSensor& TimeSensor::instance()    {
  static TimeSensor sensor;
  return sensor;
}
