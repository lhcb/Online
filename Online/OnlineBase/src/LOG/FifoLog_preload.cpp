//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  Fifo logger in pre-load mode
//--------------------------------------------------------------------------
//
//  Author     : M.Frank
//  Created    : 29/5/2020
//
//==========================================================================

/// Framework include files
#include "LOG/FifoLog.h"
#include <unistd.h>

/// Iniitalize the logger unit
__attribute__((constructor)) void s__fifolog_initialize_logger()    {
  fifolog_initialize_logger();
}

/// Shutdown the loggetr unit
__attribute__((destructor))  void s__fifolog_finalize_logger()    {
  ::usleep(3000);
  fifolog_finalize_logger();
}
