//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
#ifndef _MBM_INSTALLER_H
#define _MBM_INSTALLER_H

#include <MBM/Manager.h>

/*
 *    MBM Namespace declaration
 */
namespace MBM {
  /* @class MBM::Installer  Installer.h  MBM/Installer.h
   *
   * @author  M.Frank
   * @version 1.0
   * @date    10/01/2006
   */
  struct Installer : public Manager  {
  private:
    int   p_continue = 0;       // Do not keep process alive; continue execution
    int   p_moni = 0;           // Start monitor
    int   p_emax = 32;          // maximum events allowed
    int   p_umax = 5;           // maximun users
    int   p_tmax = 1;           // maximun server threads
    int   p_size = 10;          // buffer size
    int   p_force = 0;          // force deinstall
    int   p_bits = 10;          // Block size
    int   p_numa_mem = -1;      // NUMA slot for memory binding
    char  buff_id[128];         // Buffer identifier
  public:
    /// Standard constructor
    Installer(int argc, char **argv);
    /// Standard destructor
    virtual ~Installer();
    /// Flag to indicate the start of the monitor after installation
    bool startMonitor() const { return p_moni > 0; }
    /// Continue with installation by processing further options
    int  continueInstallation()  const { return p_continue==1; }
    /// Start the MBM server threads asynchronously
    int  startBlocking()  const { return p_continue==0; }
    /// Parse options
    int  optparse (const char* c) override;
    /// Deinstall buffers
    int  deinstall();
    /// Install buffers
    int  install();
  };
}
#endif // _MBM_INSTALLER_H
