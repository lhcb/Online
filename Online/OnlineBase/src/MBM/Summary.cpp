//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
#define MBM_IMPLEMENTATION

#include <MBM/bmstruct.h>
#include <MBM/Manager.h>
#include <MBM/bmserver.h>
#include <MBM/Summary.h>

#include <map>
#include <regex>

#include "mbmlib_server.h"

using namespace MBM;

namespace {
  static const char *sstat[17] = {" nl", "   ", "*SL","*EV","*SP","WSL","WEV","WSP","wsl","wev","wsp"," ps"," ac", "SPR", "WER", "   "};

  struct ManagerImp : public Manager {
    int  optparse (const char*) override { return 1; }
    ManagerImp()                        {           }
    virtual ~ManagerImp()               {           }
  };

  int print_lines(int status, const std::vector<std::string>& lines)   {
    if ( status != 1 )   {
      ::printf("Failed to access MBM information! [status=%d]\n", status);
    }
    for( const auto& line : lines )
      ::printf("%s\n",line.c_str());
    return status;
  }
}

class Summary::internals_t  {
public:
  typedef std::map<std::string,USER*> Users;
  Users m_oneTasks;
  std::string match;
  bool print_errors = true;
public:
  /// Retrieve MBM status information for selected buffers
  std::pair<int, std::vector<std::string> > get_info(bool show_states);
  /// Retrieve MBM status information for single buffers
  std::pair<int, std::vector<std::string> > get_info(BufferMemory* dsc, bool show_states);
  /// Default constructor
  internals_t()                      {           }
  /// Default destructor
  virtual ~internals_t()             {           }
};

/// Default constructor
Summary::Summary(const std::string& regular_expresseion, bool print_errors)
{
  imp = std::make_unique<internals_t>();
  imp->print_errors = print_errors;
  if ( !regular_expresseion.empty() ) imp->match = regular_expresseion;
}

/// Default destructor
Summary::~Summary()   {
  imp.reset();
}

/// Show MBM status information for selected buffers
int Summary::show(bool show_states)   {
  auto [status, lines] = imp->get_info(show_states);
  return print_lines(status, lines);
}

/// Show MBM status information for single buffers
int Summary::show(BufferMemory* dsc, bool show_states)   {
  auto [status, lines] = imp->get_info(dsc, show_states);
  return print_lines(status, lines);
}

/// Retrieve MBM status information for selected buffers
std::pair<int, std::vector<std::string> >
Summary::get_info(bool show_states)   {
  return imp->get_info(show_states);
}

/// Retrieve MBM status information for single buffers
std::pair<int, std::vector<std::string> >
Summary::get_info(BufferMemory* dsc, bool show_states)   {
  return imp->get_info(dsc, show_states);
}

std::pair<int, std::vector<std::string> >
Summary::internals_t::get_info(bool show_states) {
  lib_rtl_gbl_t bm_all;
  std::pair<int, std::vector<std::string> > result { 1, {} };
  int status = ::mbmsrv_map_global_buffer_info(&bm_all,false);
  if(!lib_rtl_is_success(status))   {
    ::lib_rtl_output(LIB_RTL_ERROR,"Cannot map global buffer information....\n");
    ::exit(status);
  }
  result.second.emplace_back("+++++ MBM buffer section sucessfully mapped.");
  BUFFERS* buffs = (BUFFERS*)bm_all->address;
  try {
    typedef std::map<std::string,int> Buffs;
    Buffs bs;
    for (int i = 0; i < buffs->p_bmax; ++i)  {
      if ( buffs->buffers[i].used == 1 )  {
        bs[buffs->buffers[i].name] = i;
      }
    }
    std::regex buffer_match;
    if ( !match.empty() )   {
      auto regex_flags = std::regex_constants::ECMAScript | std::regex_constants::icase;
      buffer_match = std::regex(match, regex_flags);
    }
    for( Buffs::const_iterator j=bs.begin(); j != bs.end(); ++j )   {
      bool use_buffer = true;
      if ( !match.empty() )   {
	std::smatch sm;
	std::string buff_name = buffs->buffers[(*j).second].name;
	use_buffer = std::regex_match(buff_name, sm, buffer_match);
      }
      if ( use_buffer )   {
	ManagerImp bms;
	bms.setup(buffs->buffers[(*j).second].name);
	int sc = bms.mapMonitorSectionsQuiet();
	if ( !lib_rtl_is_success(sc) ) continue;
	auto [ret, lines] = get_info(bms.m_bm,show_states);
	if ( ret != 1 )  {
	  char txt[256];
	  ::snprintf(txt, sizeof(txt), "Error accessing buffer: %s [status=%d]",
		     buffs->buffers[(*j).second].name, ret);
	  result.second.emplace_back(txt);
	}
	result.second.insert(result.second.end(), lines.begin(), lines.end());
	bms.unmapSections();
      }
    }
  }
  catch(...) {
    result.second.emplace_back(" Exception during buffer monitoring.");
  }
  ::mbmsrv_unmap_global_buffer_info(bm_all,false);
  result.second.emplace_back("");
  result.second.emplace_back("+++++ MBM summary finished.");
  return result;
}

std::pair<int, std::vector<std::string> >
Summary::internals_t::get_info(BufferMemory* dsc, bool show_states)   {
  int j, k;
  static const char* fmt_def  = " %-35s%5x%5s          %30s%5s%7s";
  static const char* fmt_prod = " %-35s%5x%5s%6s%11d   %3.0f %-21s%15s";
  static const char* fmt_cons = " %-35s%5x%5s%6s               %12d   %3.0f%5s %7s";
  static const char* head=" Name                           Partition Type State   Produced %prod     #seen %seen Reqs Buffer";
  char txt[256], text[512];
  std::pair<int, std::vector<std::string> > result { 1, {} };
  USER*    us, *utst=(USER*)~0x0;
  CONTROL* ctr = dsc->ctrl;

  result.second.emplace_back("");
  ::snprintf(text, sizeof(text), "======================== MBM Bufer summary for buffer \"%s\" ========================",dsc->bm_name);
  result.second.emplace_back(text);
  result.second.emplace_back("");
  ::snprintf(txt,sizeof(txt)," Buffer \"%s\"",dsc->bm_name);
  ::snprintf(text, sizeof(text), "%-26s  Events: Produced:%ld Seen:%ld Pending:%ld Max:%ld",
           txt, long(ctr->tot_produced), long(ctr->tot_seen), long(ctr->i_events), long(ctr->p_emax));
  result.second.emplace_back(text);
  ::snprintf(text, sizeof(text), "%-26s  Space(kB):[Tot:%ld Free:%ld] Users:[Tot:%ld Max:%ld]",
           "",long((ctr->bm_size*ctr->bytes_p_Bit)/1024),
           long((ctr->i_space*ctr->bytes_p_Bit)/1024),
           long(ctr->i_users), long(ctr->p_umax));
  result.second.emplace_back(text);
  result.second.emplace_back("");

  bool first = true;
  Users users;
  for (j=0,us=dsc->user;j<ctr->p_umax;j++,us++)    {
    if ( us == utst || us == 0    ) break;
    if ( us->block_id != BID_USER ) continue;
    if ( us->busy     == 0        ) continue;
    users[us->name] = us;
  }
  USER cons_one, prod_one;
  ::memset(&cons_one,0,sizeof(cons_one));
  ::memset(&prod_one,0,sizeof(prod_one));
  for(Users::const_iterator ui=users.begin(); ui!=users.end();++ui) {
    char spy_val[5] = {' ',' ',' ',' ',0};
    us = (*ui).second;
    for (k=0; k<us->n_req; ++k )  {
      if      ( us->req[k].user_type == BM_REQ_ONE  ) spy_val[1] = '1';
      else if ( us->req[k].user_type == BM_REQ_USER ) spy_val[2] = 'U';
      else if ( us->req[k].user_type == BM_REQ_ALL  ) spy_val[3] = 'A';
      else if ( us->req[k].freq < 100.0             ) spy_val[0] = 'S';
    }
    if ( first )    {
      first = false;
      result.second.emplace_back(head);
    }
    if ( us->ev_produced>0 || us->get_sp_calls>0 )   {
      float perc = 0;
      if ( ctr->tot_produced>0 ) perc = ((float)us->ev_produced/(float)ctr->tot_produced)*100;
      if ( m_oneTasks.find(us->name) != m_oneTasks.end() ) {
        prod_one.ev_produced += us->ev_produced;
        prod_one.state  = 0;
        prod_one.partid = us->partid;
        ::strncpy(prod_one.name,"PROD_ONE",sizeof(prod_one.name)-1);
        continue;
      }
      const char* st = show_states ? sstat[us->state+1] : "";
      ::snprintf(text, sizeof(text), fmt_prod, us->name,us->partid,"P",st,us->ev_produced,
		 perc+0.1, spy_val, "", dsc->bm_name);
      result.second.emplace_back(text);
    }
    else if ( us->ev_actual>0 || us->get_ev_calls>0 || us->n_req>0 ) {
      float perc = 0;
      if ( spy_val[1]=='1' )   {
        m_oneTasks[us->name] = 0;
        cons_one.ev_seen += us->ev_seen;
        cons_one.state    = 0;
        cons_one.partid   = us->partid;
        ::strncpy(cons_one.name,"CONS_ONE",sizeof(cons_one.name)-1);
        continue;
      }
      if ( ctr->tot_produced>0 ) {
        perc = ((float)us->ev_seen/(float)ctr->tot_produced)*100;
      }
      const char* st = show_states ? sstat[us->state+1] : "";
      ::snprintf(text, sizeof(text), fmt_cons,us->name,us->partid,"C",st,
               us->ev_seen, perc+0.1, spy_val, dsc->bm_name);
      result.second.emplace_back(text);
    }
    else        {
      ::snprintf(text, sizeof(text), fmt_def,us->name,us->partid,"?","",spy_val, dsc->bm_name);
      result.second.emplace_back(text);
    }
  }
  if ( prod_one.name[0] ) {
    us = &prod_one;
    float perc = 0;
    if ( ctr->tot_produced>0 ) perc = ((float)us->ev_produced/(float)ctr->tot_produced)*100;
    ::snprintf(text, sizeof(text), fmt_prod,us->name,us->partid,"P","",us->ev_produced,
	       perc+0.1, "", dsc->bm_name);
    result.second.emplace_back(text);
  }
  if ( cons_one.name[0] ) {
    us = &cons_one;
    float perc = 0;
    if ( ctr->tot_produced>0 ) perc = ((float)us->ev_seen/(float)ctr->tot_produced)*100;
    ::snprintf(text, sizeof(text), fmt_cons,us->name,us->partid,"C","",us->ev_seen,perc+0.1," 1 ",dsc->bm_name);
    result.second.emplace_back(text);
  }
  return result;
}

namespace {
  void help()  {
    ::printf("mbm_summary -opt [-opt]\n");
    ::printf("    -nostates      Do cnot show client states \n");
  }
}

extern "C" int mbm_summary(int argc, char** argv) {
  bool errs = true;
  std::string match;
  RTL::CLI cli(argc, argv, help);
  bool show_states = cli.getopt("nostates",3) == 0;
  if ( cli.getopt("noerrors",3) ) errs = false;
  cli.getopt("match", 3, match);
  Summary sum(match, errs);
  return sum.show(show_states);
}
