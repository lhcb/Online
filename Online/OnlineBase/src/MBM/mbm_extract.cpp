//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================

// Framework include files
#include <RTL/rtl.h>
#include <RTL/Logger.h>
#include <MBM/bmdef.h>

// C/C++ include files
#include <cerrno>
#include <cstring>
#include <system_error>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

namespace  {
  bool do_write(int fid, const void* buff, size_t len)   {
    const unsigned char* ptr = (const unsigned char*)buff;
    size_t tlen = 0;
    errno = 0;
    while(tlen < len)   {
      int sc = ::write(fid, ptr + tlen, len-tlen);
      if ( sc >= 0 ) tlen += sc;
      else if ( sc < 0 && errno == EINTR ) continue;
      else return false;
    }
    return true;
  }
  int event_ast(void* /* param */)  {
    return MBM_NORMAL;
  }
}

extern "C" int mbm_extract(int argc,char **argv) {
  RTL::CLI cli(argc, argv, [] {
    ::lib_rtl_output(LIB_RTL_ALWAYS,"mbm_extract -opt [-opt]                        \n");
      ::lib_rtl_output(LIB_RTL_ALWAYS,"    -n(ame=<name>          Buffer member name\n");
      ::lib_rtl_output(LIB_RTL_ALWAYS,"    -b(uffer=<name>        Buffer identifier \n");
      ::lib_rtl_output(LIB_RTL_ALWAYS,"    -p(artition)=<number>  Partition ID                                     \n");
      ::lib_rtl_output(LIB_RTL_ALWAYS,"    -one                   ONE consumer (REQ type ONE)                      \n");
      ::lib_rtl_output(LIB_RTL_ALWAYS,"    -u(sermode)            USER mode (SPY) consumer                         \n");
      ::lib_rtl_output(LIB_RTL_ALWAYS,"    -f(fifo)               Use fifos connections for communication          \n");
      ::lib_rtl_output(LIB_RTL_ALWAYS,"    -t(cp)                 Use boost::asio TCP connections for communication\n");
      ::lib_rtl_output(LIB_RTL_ALWAYS,"    -ipc                   Use boost::asio ICP connections for communication\n");
      ::lib_rtl_output(LIB_RTL_ALWAYS,"    -out(put)=<file-name>  File name to dump event data                     \n");
      ::lib_rtl_output(LIB_RTL_ALWAYS,"    -event                 Request event type EVENT                         \n");
      ::lib_rtl_output(LIB_RTL_ALWAYS,"    -mep                   Request event type MEP                           \n");
      ::lib_rtl_output(LIB_RTL_ALWAYS,"    -burst                 Request event type Burst                         \n");
      exit(0);
  });
  std::string name = "consumer", buffer="0", output = "mbm.dat";
  int  partID = 0x103, inc_opt = 0, comm_type = BM_COM_FIFO, count = 10;
  int  prt = LIB_RTL_INFO;
  unsigned int trmask[4]   = {~0x0u,~0x0u,~0x0u,~0x0u};
  unsigned int vetomask[4] = {0u,0u,0u,0u};

  bool mep  = cli.getopt("mep",3) != 0;
  bool evt  = cli.getopt("event",3) != 0;
  bool brst = cli.getopt("burst",3) != 0;
  bool one  = cli.getopt("one",3) != 0;
  bool user = cli.getopt("usermode",3) != 0;
  cli.getopt("name",        3, name);
  cli.getopt("count",       3, count);
  cli.getopt("buffer",      3, buffer);
  cli.getopt("output",      3, output);
  cli.getopt("inc_opt",     3, inc_opt);
  cli.getopt("partitionid", 3, partID);
  if ( cli.getopt("fifo",1) )
    comm_type = BM_COM_FIFO;
  else if ( cli.getopt("tcp",1) )
    comm_type = BM_COM_ASIO;
  else if ( cli.getopt("icp",3) )
    comm_type = BM_COM_UNIX;
  
  RTL::Logger::install_log(RTL::Logger::log_args(prt));
  ::lib_rtl_output(LIB_RTL_ALWAYS,"Consumer \"%s\" (pid:%d) running in buffer:\"%s\"",
		   name.c_str(), partID, buffer.c_str());

  int fid = ::open(output.c_str(), O_WRONLY|O_CREAT|O_LARGEFILE|O_TRUNC, S_IRWXG|S_IRWXU);
  if ( fid == -1 )   {
    int err = errno;
    std::string error = std::make_error_code(std::errc(err)).message();
    ::lib_rtl_output(LIB_RTL_ALWAYS,"+++ Failed to open output file: %s", output.c_str());
    ::lib_rtl_output(LIB_RTL_ALWAYS,"+++ Error[%d]: %s", err, error.c_str());
    ::exit(err);
  }
  BMID bmid = ::mbm_include(buffer.c_str(),name.c_str(),partID,comm_type,BM_INC_READ);
  if ( bmid == MBM_INV_DESC )   {    
    std::string error = std::make_error_code(std::errc(errno)).message();
    ::lib_rtl_output(LIB_RTL_ALWAYS,"  Failed to include in MBM buffer %s [%s].",
		     buffer.c_str(), error.c_str());
    ::exit(errno);
  }
  int event_type = EVENT_TYPE_EVENT;

  if ( one ) {
    if ( mep )
      ::mbm_add_req(bmid,EVENT_TYPE_MEP,trmask,vetomask,BM_MASK_ANY,(inc_opt<<8)+BM_REQ_ONE,BM_FREQ_PERC,100.);
    if ( brst )
      ::mbm_add_req(bmid,EVENT_TYPE_BURST,trmask,vetomask,BM_MASK_ANY,(inc_opt<<8)+BM_REQ_ONE,BM_FREQ_PERC,100.);
    if ( evt )
      ::mbm_add_req(bmid,EVENT_TYPE_EVENT,trmask,vetomask,BM_MASK_ANY,(inc_opt<<8)+BM_REQ_ONE,BM_FREQ_PERC,100.);
    ::lib_rtl_output(LIB_RTL_ALWAYS,"+++ Event request type is BM_REQ_ONE [%d].",inc_opt);
  }
  else if ( user ) {
    if ( mep )
      ::mbm_add_req(bmid,EVENT_TYPE_MEP,trmask,vetomask,BM_MASK_ANY,BM_REQ_USER,BM_FREQ_PERC,100.);
    if ( brst )
      ::mbm_add_req(bmid,EVENT_TYPE_BURST,trmask,vetomask,BM_MASK_ANY,BM_REQ_USER,BM_FREQ_PERC,100.);
    if ( evt )
      ::mbm_add_req(bmid,EVENT_TYPE_EVENT,trmask,vetomask,BM_MASK_ANY,BM_REQ_USER,BM_FREQ_PERC,100.);
    ::lib_rtl_output(LIB_RTL_ALWAYS,"+++ Event request type is BM_REQ_USER.");
  }
  else {
    if ( mep )
      ::mbm_add_req(bmid,EVENT_TYPE_MEP,trmask,vetomask,BM_MASK_ANY,BM_REQ_ALL,BM_FREQ_PERC,100.);
    if ( brst )
      ::mbm_add_req(bmid,EVENT_TYPE_BURST,trmask,vetomask,BM_MASK_ANY,BM_REQ_ALL,BM_FREQ_PERC,100.);
    if ( evt )
      ::mbm_add_req(bmid,EVENT_TYPE_EVENT,trmask,vetomask,BM_MASK_ANY,BM_REQ_ALL,BM_FREQ_PERC,100.);
    ::lib_rtl_output(LIB_RTL_ALWAYS,"+++ Event request type is BM_REQ_ALL.");
  }

  long tr_number, len;
  for(tr_number = 1; tr_number <= count; ++tr_number)    {
    unsigned int mask[4];
    int  *data = 0, type = event_type;
    int  sc = ::mbm_get_event_a(bmid,&data,&len,&type,mask,partID,event_ast,nullptr);
    if ( sc == MBM_NORMAL )   {
      sc = ::mbm_wait_event(bmid);
      if ( sc == MBM_NORMAL )   {
	if ( !do_write(fid, &len, sizeof(len)) )
	  break;
	if ( !do_write(fid, data, len) )
	  break;
	::mbm_free_event(bmid);
	if ( errno != 0 )  {
	  std::string error = std::make_error_code(std::errc(errno)).message();
	  ::lib_rtl_output(LIB_RTL_ALWAYS,"+++ Error writing: %s", error.c_str());
	  break;
	}
	if ( tr_number+1 == count )   {
	  ::lib_rtl_output(LIB_RTL_ALWAYS,"+++ Stop writing after: %ld events.", tr_number);
	  break;
	}
	::lib_rtl_output(LIB_RTL_ALWAYS,"+++ Wrote buffer %ld", tr_number);
      }
      else   {
	std::string error = std::make_error_code(std::errc(errno)).message();
	::lib_rtl_output(LIB_RTL_ALWAYS,"+++ Error mbm_wait_event: %s", error.c_str());
      }
    }
    else   {
      std::string error = std::make_error_code(std::errc(errno)).message();
      ::lib_rtl_output(LIB_RTL_ALWAYS,"+++ Error mbm_get_event_a: %s", error.c_str());
    }
  }
  if ( tr_number < count )  {
    std::string error = std::make_error_code(std::errc(errno)).message();
    ::lib_rtl_output(LIB_RTL_ALWAYS,"+++ Error writing: %s  %ld %ld",
		     error.c_str(), tr_number, count);
  }
  ::mbm_exclude(bmid);
  ::close(fid);
  exit(0);
  return 0;
}
