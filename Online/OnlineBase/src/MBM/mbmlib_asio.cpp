//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================

// Framework include files
#include <MBM/bmstruct.h>
#include <MBM/bmserver.h>
#include <ASIO/TcpServer.h>
#include <ASIO/TanInterface.h>

#include "mbmlib_client.h"
#include "mbmlib_server.h"
#include "mbmlib_message.h"

// C/C++ include files
#include <iostream>
#include <sstream>
#include <memory>

namespace BoostAsio  {

  /// The MBM server request handler for all incoming requests.
  /**
   *  Handle MBM messages/requests using TCP/IP as transport protocol
   *
   *  \author   M.Frank
   *  \version  1.0
   *  \defgroup LHCB_ONLINE
   */
  class TcpMBMRequestHandler : public TcpRequestHandler  {
  protected:
    ServerBMID m_bm;
  public:
    /// Construct request handler
    explicit TcpMBMRequestHandler(ServerBMID bm) : m_bm(bm) {}
    /// Default destructor
    virtual ~TcpMBMRequestHandler()  {}
    /// RequestHandler overload: Handle a request and produce a reply.
    void handle(TcpConnection& connection, const Request& request, Reply& reply) override;
    /// RequestHandler overload: Handle connection finalization for cleanups
    void handleClose(TcpConnection& connection) override;
  };
}

using namespace BoostAsio;
using namespace std;
namespace asio = boost::asio;

/// Handle a request and produce a reply.
void TcpMBMRequestHandler::handle(TcpConnection& connection, const Request& req, Reply& response) {
  typedef  MBMMessage MSG;
  const MSG* const_req = reinterpret_cast<const MSG*>(asio::detail::buffer_cast_helper(req));
  MSG* msg = const_cast<MSG*>(const_req);
  if ( msg->magic != MBMMessage::MAGIC ) {
    ::lib_rtl_output(LIB_RTL_ERROR,"Wrong magic word in MBM message:%X instead of %X.",
		     msg->magic,MBMMessage::MAGIC);
    return;
  }
  mbmsrv_handle_request(m_bm,&connection,*msg);
  if ( msg->status != MBM_NO_REPLY ) {
    if ( connection.reply().size() < sizeof(MSG) )  {
      connection.reply().resize(sizeof(MSG));
    }
    ::memcpy(connection.reply().data(),msg,sizeof(MSG));
    response.push_back(asio::buffer(connection.reply().data(), sizeof(MSG)));
  }
}

/// RequestHandler overload: Handle connection finalization for cleanups
void TcpMBMRequestHandler::handleClose(TcpConnection& connection)   {
  NetworkChannel::Channel fd = connection.socket().native_handle();
  if ( fd > 0 )  {
    ::lib_rtl_output(LIB_RTL_DEBUG,"++asio_bm_server++ Closing connection: %d\n",fd);
  }
}

namespace {
  int _open(ServerBMID, MBMConnection& /* connection */, const char* /* bm_name */, const char* /* name */)  {
    return MBM_NORMAL;
  }

  int _close(ServerBMID, MBMConnection& connection)  {
    connection.init();
    return MBM_NORMAL;
  }

  int _poll(ServerBMID, MBMConnection& /* connection */, int& events, int tmo)  {
    if ( tmo > 0 ) ::lib_rtl_sleep(tmo);
    events = 0;
    return 0;
  }

  struct ServConn {
    asio::io_context io_context;
    asio::ip::tcp::socket socket;
    ServConn() : io_context(), socket(io_context) {}
  };

  int _open_server_ex(MBMConnection& connection, const char* bm_name, const char* name, int id)  {
    TanMessage::Address addr;
    stringstream port;
    string server;
    int status;
    connection.any.channel = 0;
    port << "BM_" << bm_name << "_SERVER_" << id << ends;
    server = port.str();
    status = ::tan_get_address_by_name(server.c_str(), &addr);
    port.str("");
    if ( status == TAN_SS_SUCCESS )  {
      port << addr.sin_port << ends;
      try {
	using asio::ip::tcp;
	auto conn = std::make_unique<ServConn>();
	tcp::resolver resolver(conn->io_context);
#if BOOST_ASIO_VERSION < 103400
	tcp::resolver::query query(tcp::v4(), "127.0.0.1", port.str());
	tcp::resolver::iterator iterator = resolver.resolve(query);
	asio::connect(conn->socket, iterator);
#else
	asio::connect(conn->socket, resolver.resolve(tcp::v4(), "127.0.0.1", port.str()));
#endif
	connection.any.channel = conn.release();
	return MBM_NORMAL;
      }
      catch(exception& e)   {
	::lib_rtl_output(LIB_RTL_ALWAYS,"MBM _open_server(%s, %s, %s) port:%s [%s]\n",
			 server.c_str(),bm_name,name,port.str().c_str(),e.what());
      }
    }
    return MBM_ERROR;
  }

  int _write(MBMConnection& connection, MBMMessage& msg) {
    ServConn* c = reinterpret_cast<ServConn*>(connection.any.channel);
    asio::write(c->socket, asio::buffer(&msg, sizeof(MBMMessage)));
    return msg.status=MBM_NORMAL;
  }

  int _read(MBMConnection& connection, MBMMessage& msg, int* cancel_flag) {
    ServConn* c = reinterpret_cast<ServConn*>(connection.any.channel);
    size_t reply_length;
    struct pollfd fds;

    fds.events  = POLLIN;
    fds.fd = c->socket.native_handle();
    while(1) {
      fds.revents = 0;
      ::poll(&fds,1,100);
      if ( fds.revents&POLLIN )
	break;
      else if ( cancel_flag && *cancel_flag )
	return MBM_REQ_CANCEL;
    }
    reply_length = asio::read(c->socket,asio::buffer(&msg,sizeof(MBMMessage)));
    if ( reply_length != sizeof(MBMMessage) )  {
      return msg.status=MBM_ERROR;
    }
    if ( msg.magic != MBMMessage::MAGIC ) {
      ::lib_rtl_output(LIB_RTL_ERROR,
		       "MBM: Wrong magic word in MBM message:%X instead of %X.\n",
		       msg.magic,MBMMessage::MAGIC);
      return msg.status=MBM_ERROR;
    }
    return msg.status=MBM_NORMAL;
  }

  int _send_response_client(ServerBMID, MBMConnection& connection, const MBMMessage& msg)   {
    TcpConnection* c = reinterpret_cast<TcpConnection*>(connection.any.channel);
    if ( c )  {
      asio::write(c->socket(), asio::buffer((void*)&msg, sizeof(msg)));
      return MBM_NORMAL;
    }
    return MBM_ERROR;
  }

  int _dispatch_clients(ServerBMID bm, int which)  {
    string srv_name;
    bm->server[which].running = 1;
    ::lib_rtl_output(LIB_RTL_DEBUG,"BM: %s Starting dispatch thread %d\n",bm->bm_name,which);
    try  {
      NetworkChannel::Port port_number = -1;
      int status, num_threads = 2;
      stringstream port;
      port << "BM_" << bm->bm_name << "_SERVER_" << which << ends;
      srv_name = port.str();
      port.str("");
      status = ::tan_allocate_new_port_number(srv_name.c_str(), &port_number);
      port << port_number << ends;
      if ( status == TAN_SS_SUCCESS )  {
	TcpServer server("127.0.0.1", port.str(), num_threads);
	server.config->synchronous = -1;
	server.setRecvSize(sizeof(MBMMessage));
	server.config->setHandler(new TcpMBMRequestHandler(bm));
	::lib_rtl_output(LIB_RTL_INFO,"Dispatching boost::asio requests..%s..Port:%d\n",
			 srv_name.c_str(), int(port_number));
	// Run the server until stopped.
	server.run();
	::tan_deallocate_port_number(srv_name.c_str());
	bm->server[which].running = 0;
	return 1;
      }
      bm->server[which].running = 0;
      return ENOENT;
    }
    catch (const exception& e)  {
      ::tan_deallocate_port_number(srv_name.c_str());
      ::lib_rtl_output(LIB_RTL_ERROR,"mbmsrv_asio: exception[%s]: %s\n",
		       srv_name.c_str(), e.what());
      throw;
    }
    bm->server[which].running = 0;
    return 1;
  }

  int _open_server(BMID bm, const char* bm_name, const char* name)  {
    return _open_server_ex(bm->connection,bm_name,name,0);
  }

  int _close_server(BMID bm)  {
    bm->connection.init();
    return MBM_NORMAL;
  }

  /// Exchange MBM message with server
  int _communicate_server(BMID bm, MBMMessage& msg, int* cancelled)   {
    if ( MBM_NORMAL == _write(bm->connection,msg) )
      return _read(bm->connection,msg,cancelled);
    return msg.status;
  }

  /// Send request to server
  int _send_request_server(BMID bm, MBMMessage& msg, bool /* clear_before */)   {
    return msg.status=_write(bm->connection,msg);
  }

  /// Read server response
  int _read_response_server(BMID bm, MBMMessage& msg, int* cancelled)  {
    return _read(bm->connection, msg, cancelled);
  }

  int _move_server(BMID bm, const char* bm_name, USER* user_cookie, int serverid)   {
    if ( serverid != 0 )  {
      std::unique_ptr<ServConn> conn{reinterpret_cast<ServConn*>(bm->connection.any.channel)};
      int status = _open_server_ex(bm->connection,bm_name,"reconnect",serverid);
      if ( status == MBM_NORMAL )  {
	MBMMessage msg(MBMMessage::RECONNECT,user_cookie);
	return _communicate_server(bm, msg, 0);
      }
      return status;
    }
    return MBM_NORMAL;
  }
}

int _mbm_connections_use_asio(MBMServerCommunication& com)   {
  com.type            = BM_COM_ASIO;
  /** MBM Server communication functions   */
  com.m_open          = _open;
  com.m_close         = _close;
  com.m_accept        = 0;
  com.m_bind          = 0;
  com.m_poll          = _poll;
  com.m_poll_del      = 0;
  com.m_poll_add      = 0;
  com.m_poll_create   = 0;
  com.m_send_response = _send_response_client;
  com.m_dispatch      = _dispatch_clients;
  com.m_stop          = 0;
  return MBM_NORMAL;
}

int _mbm_connections_use_asio(MBMClientCommunication& com)   {
  com.type                   = BM_COM_ASIO;
  /** MBM Client communication functions   */
  com.m_open_server          = _open_server;
  com.m_close_server         = _close_server;
  com.m_move_server          = _move_server;
  com.m_communicate_server   = _communicate_server;
  com.m_clear_server         = 0;
  com.m_send_request_server  = _send_request_server;
  com.m_read_response_server = _read_response_server;
  return MBM_NORMAL;
}
