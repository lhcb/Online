//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
#ifndef _MBMLIB_CLIENT_H
#define _MBMLIB_CLIENT_H

#ifdef MBM_IMPLEMENTATION
#include <RTL/rtl.h>
#include <RTL/que.h>
#include <RTL/bits.h>
#include <MBM/bmdef.h>
#include <MBM/bmconnection.h>

#include <mutex>

// Forward declarations
struct SHMCOMM_gbl;
struct MBMMessage;
struct MBMClientCommunication;

struct MBMClientCommunication  {
  int type { BM_COM_NONE };
  BMID bm  { 0 };

  /** MBM Client communication functions   */
  /// Open connection to server process
  int (*m_open_server)(BMID bm, const char* bm_name, const char* name) = 0;
  /// Close connection to server process
  int (*m_close_server)(BMID bm) = 0;
  /// Move server connection to worker thread
  int (*m_move_server)(BMID bm, const char* bm_name, USER* u, int serverid) = 0;
  /// Exchange MBM message with server
  int (*m_communicate_server)(BMID bm, MBMMessage& msg, int* cancelation_flag) = 0;
  /// Interrupt MBM message exchange with server
  int (*m_interrupt_server)(BMID bm) = 0;
  /// Clear possibly pending data from communication channel
  int (*m_clear_server)(BMID bm) = 0;
  /// Send request to server
  int (*m_send_request_server)(BMID bm, MBMMessage& msg, bool clear_before) = 0;
  /// Read server response
  int (*m_read_response_server)(BMID bm, MBMMessage& msg, int* cancelled) = 0;

  /** MBM Client communication functions   */
  /// Open connection to server process
  int open_server(const char* bm_name, const char* name)  const;
  /// Close connection to server process
  int close_server()  const;
  /// Move server connection to worker thread
  int move_server(const char* bm_name, USER* u, int serverid)  const;
  /// Exchange MBM message with server
  int communicate_server(MBMMessage& msg, int* cancelation_flag)  const;
  /// Interrupt MBM message exchange with server
  int interrupt_server()  const;
  /// Clear possibly pending data from communication channel
  int clear_server()  const;
  /// Send request to server
  int send_request_server(MBMMessage& msg, bool clear_before)  const;
  /// Read server response
  int read_response_server(MBMMessage& msg, int* cancelled)  const;

  MBMClientCommunication() = default;
  MBMClientCommunication(MBMClientCommunication&& copy) = delete;
  MBMClientCommunication(const MBMClientCommunication& copy) = delete;
  MBMClientCommunication& operator=(MBMClientCommunication&& copy) = delete;
  MBMClientCommunication& operator=(const MBMClientCommunication& copy) = delete;
};

struct BMDESCRIPT : public qentry_t  {
  int              pid         { -1 };
  int              partID      { -1 };
  int**            evt_ptr     { nullptr };
  long*            evt_size    { nullptr };
  int*             evt_type    { nullptr };
  unsigned int*    trmask      { nullptr };
  RTL_ast_t        ast_addr    { nullptr };
  void*            ast_param   { nullptr };
  lib_rtl_gbl_t    buff_add    { nullptr };
  char*            buffer_add  { nullptr };
  lib_rtl_gbl_t    comm_add    { nullptr };
  SHMCOMM_gbl*     comm        { nullptr };
  lib_rtl_lock_t   comm_lock   { nullptr };
  int              cancelled   {  0 };
  int              own_buffer  {  1 };
  int              comm_type   { -1 };
  MBMClientCommunication communication  { };
  MBMConnection    connection  {    };
  std::mutex       lock        {    };
  char             connectionName[BM_USER_NAME_LEN+BM_BUFF_NAME_LEN+16];
  char             name[BM_USER_NAME_LEN];     // user name
  char             bm_name[BM_BUFF_NAME_LEN];  // buffer name
  USER*            user        { nullptr };
  BMDESCRIPT() : qentry_t(0,0) {
    name[0]    = 0;
    bm_name[0] = 0;
    connection.init();
  }
  ~BMDESCRIPT() {}
};

int _mbm_connections_use_fifos (MBMClientCommunication& com);
int _mbm_connections_use_asio  (MBMClientCommunication& com);
int _mbm_connections_use_unix  (MBMClientCommunication& com);
int _mbm_connections_use_shm1  (MBMClientCommunication& com);
int _mbm_connections_use_shm2  (MBMClientCommunication& com);

#endif // MBM_IMPLEMENTATION
#endif // _MBMLIB_CLIENT_H
