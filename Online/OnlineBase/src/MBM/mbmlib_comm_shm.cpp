//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================

// Framework include files
#include <MBM/bmstruct.h>
#include <MBM/bmserver.h>

#include "mbmlib_client.h"
#include "mbmlib_server.h"
#include "mbmlib_message.h"
#include "mbmlib_comm_shm.h"

// C/C++ include files
#include <cerrno>
#include <poll.h>
#include <fcntl.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/epoll.h>
#include <sys/mman.h>


namespace {

  int _wait_semaphore1(sem_t& sem, long timeout, const int& cancel)  {
    const struct timespec poll {0, timeout*1000000};
    do {
      int ret = ::sem_timedwait(&sem, &poll);
      if ( cancel ) 
	return 0;
      else if ( ret == 0 )
	return 1;
    } while ( --timeout > 0 );
    return 0;
  }

  int _wait_semaphore2(sem_t& sem, long timeout, const int& cancel)  {
    const struct timespec poll {0, 100000};
    int count = 5;
    do {
      int ret = (--count >= 0)
	? ::sem_timedwait(&sem, &poll)
	: ::sem_wait(&sem);
      if ( cancel ) 
	return 0;
      else if ( ret == 0 )
	return 1;
    } while ( --timeout > 0 );
#if 0
    timeout *= 1000;
    const struct timespec tmo {0, 1};
    do {
      int ret = ::sem_trywait(&sem);
      if ( cancel ) 
	return 0;
      else if ( ret == 0 )
	return 1;
      ::nanosleep(&tmo, 0);
      //::usleep(1);
      timeout -= 10;
    } while ( timeout > 0 );
#endif
    return 0;
  }

  int _send_response(ServerBMID bm, MBMConnection& connection, const MBMMessage& msg)   {
    SHMCOMM_gbl* gbl = bm->comm;
    SHMCOMM& comm = gbl->user[connection.shm.slot];

    ::memcpy(&comm.answer, &msg, sizeof(MBMMessage));
    comm.server_answer = 1;
    ::sem_post(&comm.server_answer_ready);
    return MBM_NORMAL;
  }

  int _close(ServerBMID bm, MBMConnection& connection)  {
    SHMCOMM_gbl* gbl = bm->comm;
    if ( connection.shm.slot >= 0 && connection.shm.slot < gbl->num_user )   {
      ::sem_wait(&gbl->table_lock);
      ::lib_rtl_output(LIB_RTL_DEBUG,"++bm_server++ Closing client connection "
		       "%s with slot: %d\n", connection.shm.name, connection.shm.slot);
      SHMCOMM& comm = gbl->user[connection.shm.slot];
      ::sem_init(&comm.server_answer_ready, 1, 0);
      comm.server_answer  = 0;
      comm.client_request = 0;
      comm.user = 0;
      comm.pid =  0;
      connection.init();
      ::sem_post(&gbl->table_lock);
    }
    return MBM_NORMAL;
  }

  /// Poll on connections delivering data
  int _poll(ServerBMID bm, MBMConnection& /* connection */, int& events, int tmo_milli_seconds)  {
    events = 0;
    if ( tmo_milli_seconds > 0 ) ::lib_rtl_sleep(tmo_milli_seconds);
    if ( bm->communication.type == BM_COM_SHM2 )
      ::sem_post(&bm->comm->client_data_ready);
    return -1;
  }

  int _stop(ServerBMID bm)   {
    ::sem_post(&bm->comm->client_data_ready);
    return MBM_NORMAL;
  }

  int _num_pending_req(SHMCOMM_gbl* com)   {
    int count = 0;
    for(int i=0; i < com->num_user; ++i)   {
      SHMCOMM& c = com->user[i];
      if ( c.pid > 0 && c.client_request )
	++count;
    }
    return count;
  }

  void _handle_client(ServerBMID bm, SHMCOMM_gbl* com, int i)   {
    SHMCOMM& c = com->user[i];
    MBMMessage msg(0);
    ::memcpy(&msg, &c.request, sizeof(MBMMessage));
    if ( msg.magic != MBMMessage::MAGIC ) {
      ::lib_rtl_output(LIB_RTL_ERROR,
		       "++bm_server++ Wrong magic word in MBM message:%X instead of %X.",
		       msg.magic, MBMMessage::MAGIC);
      return;
    }
    int cmd = msg.type;
    mbmsrv_handle_request(bm,0,msg);
    if ( cmd == MBMMessage::INCLUDE && msg.user )    {
      MBMMessage::include_t& inc = msg.data.include;
      inc.serverid = 0;           // Do NOT move to another thread -- does not make sense
      c.user = msg.user;          // Server cookie
      msg.user->connection.shm.slot = i;
      msg.user->connection.shm.response = 1;
    }
    if ( msg.status != MBM_NO_REPLY ) {
      // Do not send a message if the user is already free'ed
      if ( msg.user && msg.user->uid != -1 )  {
	::memcpy(&c.answer, &msg, sizeof(MBMMessage));
	c.server_answer = 1;
	::sem_post(&c.server_answer_ready);
      }
    }
  }

  void handle_clients_single_threaded(ServerBMID bm, SHMCOMM_gbl* com)   {
    for ( int i=0; i < com->num_user; ++i )   {
      SHMCOMM& c = com->user[i];
      if ( c.pid > 0 && c.client_request )   {
	c.client_request = 0;
	_handle_client(bm, com, i);
      }
    }
  }

  void handle_clients_multi_threaded(ServerBMID bm, SHMCOMM_gbl* com)   {
    std::vector<int> slots;
    slots.reserve(bm->ctrl->p_umax);
    ::sem_wait(&com->table_lock);
    for ( int i=0; i < com->num_user; ++i )   {
      SHMCOMM& c = com->user[i];
      if ( c.pid > 0 && c.client_request )   {
	c.client_request = 0;
	slots.push_back(i);
      }
    }
    ::sem_post(&com->table_lock);
    for ( int i : slots)   {
      _handle_client(bm, com, i);
    }
  }

  int _dispatch(ServerBMID bm, int which)  {
    long loop_count = 0, check_count = 0, pending_count = 0;
    SHMCOMM_gbl* com = bm->comm;
    MBMDispatchTimer timer;

    bm->server[which].running = 1;
    ::lib_rtl_output(LIB_RTL_DEBUG,"++bm_server++ Thread %3d Dispatching SHM connections....\n",which);

    while ( !bm->stop )   {
      ++loop_count;
      if ( bm->stop )  {
	break;
      }
      (bm->communication.type == BM_COM_SHM1)
	? _wait_semaphore1(com->client_data_ready, 200, bm->stop)
	: _wait_semaphore2(com->client_data_ready, 200, bm->stop);

      if ( bm->stop )   {
	break;
      }
      int client_request = _num_pending_req(com);
      if ( !bm->client_thread )   {
	if ( 0 == (loop_count%10) && client_request == 0 )
	  timer.update();
	if ( client_request == 0 && timer.difference > 2500000L )  {
	  int ret = mbmsrv_client_watch_cycle(bm);
	  if ( (ret&1) ) ++check_count;
	  if ( (ret&2) ) ++pending_count;
	  timer.reset();
	}
      }
      if ( client_request > 0 ) {
	(bm->num_threads > 1)
	  ? handle_clients_multi_threaded(bm, com)
	  : handle_clients_single_threaded(bm, com);
      }
    }
    if ( check_count || pending_count )   {
    }
    bm->server[which].running = 0;
    return 1;
  }

  /// Open the connection to the server
  int _open_server(BMID bm, const char* bm_name, const char* name)  {
    SHMCOMM_gbl* gbl = bm->comm;
    SHMCOMM& comm = gbl->user[bm->connection.shm.slot];
    // Check consistency
    if ( 0 != strncmp(gbl->name, bm_name, BM_BUFF_NAME_LEN) )   {
      return MBM_INTERNAL;
    }
    // Add user name to slot
    ::strncpy(comm.name,name,sizeof(comm.name)-1);
    comm.name[sizeof(comm.name)-1] = 0;
    bm->connection.shm.response = 1;
    return MBM_NORMAL;
  }

  /// Close the connection to the server
  int _close_server(BMID bm)  {
    SHMCOMM_gbl* gbl = bm->comm;
    ::sem_wait(&gbl->table_lock);
    SHMCOMM& comm = gbl->user[bm->connection.shm.slot];
    ::sem_init(&comm.server_answer_ready, 1, 0);
    comm.server_answer  = 0;
    comm.client_request = 0;
    comm.name[0] = 0;
    comm.user = 0;
    comm.pid =  0;
    bm->connection.init();
    ::sem_post(&gbl->table_lock);
    return MBM_NORMAL;
  }

  /// Exchange MBM message with server
  int _communicate_server(BMID bm, MBMMessage& msg, int* cancelled)   {
    SHMCOMM_gbl* gbl = bm->comm;
    SHMCOMM& comm = gbl->user[bm->connection.shm.slot];
    const struct timespec poll {0, 2000000};

    msg.status = MBM_NORMAL;
    ::memcpy(&comm.request, &msg, sizeof(MBMMessage));
    comm.client_request = 1;
    comm.server_answer = 0;
    ::sem_post(&gbl->client_data_ready);

    // Read the response
    while (1)  {
      errno = 0;
      int ret = (bm->communication.type == BM_COM_SHM1)
	? ::sem_timedwait(&comm.server_answer_ready, &poll)
	: ::sem_wait(&comm.server_answer_ready);
      if ( ret == 0 && comm.server_answer )  {
	break;
      }
      else if ( cancelled && *cancelled )
	break;
    }
    if ( cancelled && *cancelled )   {
      comm.server_answer = 0;
      return msg.status=MBM_REQ_CANCEL;
    }
    ::memcpy(&msg, &comm.answer, sizeof(MBMMessage));
    comm.server_answer = 0;
    return msg.status;
  }

  /// Interrupt MBM message exchange with server
  int _interrupt_server(BMID bm)   {
    SHMCOMM_gbl* gbl = bm->comm;
    SHMCOMM& comm = gbl->user[bm->connection.shm.slot];
    comm.server_answer  = 0;
    ::sem_post(&comm.server_answer_ready);
    return MBM_NORMAL;
  }

  /// Send request to server
  int _send_request_server(BMID bm, MBMMessage& msg, bool /* clear_before */)   {
    SHMCOMM_gbl* gbl = bm->comm;
    SHMCOMM& comm = gbl->user[bm->connection.shm.slot];
    msg.status = MBM_NORMAL;
    ::memcpy(&comm.request, &msg, sizeof(MBMMessage));
    comm.server_answer  = 0;
    comm.client_request = 1;
    ::sem_post(&gbl->client_data_ready);
    return msg.status;
  }

  /// Read server response
  int _read_response_server(BMID bm, MBMMessage& msg, int* cancelled)  {
    SHMCOMM_gbl* gbl = bm->comm;
    SHMCOMM& comm = gbl->user[bm->connection.shm.slot];
    const struct timespec poll {0, 2000000};
    int count = 2;
    while (1)  {
      int ret = (bm->communication.type == BM_COM_SHM1 || --count >= 0)
	? ::sem_timedwait(&comm.server_answer_ready, &poll)
	: ::sem_wait(&comm.server_answer_ready);
      if ( cancelled && *cancelled )
	break;
      else if ( ret == 0 )
	break;
    }
    comm.server_answer = 0;
    ::memcpy(&msg, &comm.answer, sizeof(MBMMessage));
    if ( cancelled && *cancelled )   {
      return msg.status=MBM_REQ_CANCEL;
    }
    // Communication now OK. Let's check if the actual request failed or not:
    if ( msg.status != MBM_NORMAL && msg.status != MBM_NO_EVENT && msg.status != MBM_NO_FREE_US) {
      ::lib_rtl_signal_message(LIB_RTL_OS,"Failed to execute request '%d' in server. Status=%d \n",
			       msg.type,msg.status);
      return msg.status;
    }
    return msg.status;
  }

  int _mbm_connections_init(MBMClientCommunication& com, int typ)   {
    /** Communication type:                  */
    com.type                   = typ;
    /** MBM Client communication functions   */
    com.m_open_server          = _open_server;
    com.m_close_server         = _close_server;
    com.m_move_server          = 0;
    com.m_communicate_server   = _communicate_server;
    com.m_interrupt_server     = _interrupt_server;
    com.m_clear_server         = 0;
    com.m_send_request_server  = _send_request_server;
    com.m_read_response_server = _read_response_server;
    return MBM_NORMAL;
  }

  int _mbm_connections_init(MBMServerCommunication& com, int typ)   {
    /** Communication type:                  */
    com.type            = typ;
    /** MBM Server communication functions   */
    com.m_accept        = 0;
    com.m_bind          = 0;
    com.m_open          = 0;
    com.m_close         = _close;
    com.m_send_response = _send_response;
    com.m_dispatch      = _dispatch;
    com.m_poll          = _poll;
    com.m_poll_del      = 0;
    com.m_poll_add      = 0;
    com.m_poll_create   = 0;
    com.m_stop          = _stop;
    return MBM_NORMAL;
  }
}

int _mbm_connections_use_shm1(MBMServerCommunication& com)   {
  return _mbm_connections_init(com, BM_COM_SHM1);
}

int _mbm_connections_use_shm2(MBMServerCommunication& com)   {
  return _mbm_connections_init(com, BM_COM_SHM2);
}

int _mbm_connections_use_shm1(MBMClientCommunication& com)   {
  return _mbm_connections_init(com, BM_COM_SHM1);
}

int _mbm_connections_use_shm2(MBMClientCommunication& com)   {
  return _mbm_connections_init(com, BM_COM_SHM2);
}
