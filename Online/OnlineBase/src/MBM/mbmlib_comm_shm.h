//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
/// Framework include files
#include "mbmlib_message.h"


struct SHMCOMM_gbl;
struct SHMCOMM;

struct SHMCOMM  {
  USER*      user;                    // Server cookie
  sem_t      server_answer_ready;     // Client's semaphore to wait for reply
  MBMMessage request;                 // Request data buffer
  MBMMessage answer;                  // Reply data buffer
  char       name[BM_USER_NAME_LEN];  // Client's user name
  int        pid;                     // Client pid
  int        server_answer;           // Flag that server data are ready
  int        client_request;          // Flag that client data are ready
};

struct SHMCOMM_gbl   {
  sem_t table_lock;                // Semaphor to lock client table
  sem_t client_data_ready;         // Flag to wake-up server upon work
  char  name[BM_BUFF_NAME_LEN];
  long  num_user;
  SHMCOMM user[1];
};
