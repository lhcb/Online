//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================

// Framework include files
#include <MBM/bmstruct.h>
#include <MBM/bmserver.h>

#include "mbmlib_client.h"
#include "mbmlib_server.h"
#include "mbmlib_message.h"

// C/C++ include files
#include <cerrno>
#include <poll.h>
#include <fcntl.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/epoll.h>

namespace {

  const char* _fifo_name(char* buff, size_t len, const char* bm, const char* proc)    {
    static const char* dir_name = ::getenv("MBM_FIFO_DIR");
    const char* dir = dir_name ? dir_name : "/tmp";
    size_t prt_len = ::snprintf(buff, len, "%s/bm_%s_%s", dir, bm, proc);
    buff[len-1] = 0;
    if ( prt_len == len )    {
      throw std::runtime_error("Supplied MBM buffer name too long!");
    }
    return buff;
  }

  int _close(MBMConnection& connection)  {
    // First close the server connection
    if ( connection.client.request > 0 )  {
      ::close(connection.client.request);
    }
    // Now close the answer connection
    if ( connection.client.response > 0 ) {
      ::close(connection.client.response);
    }
    if ( 0 != connection.name[0] ) {
      ::unlink(connection.name);
    }
    connection.init();
    return MBM_NORMAL;
  }

  int _open(MBMConnection& connection, const char* bm_name, const char* name)  {
    char text[256];
    const char* fifo = _fifo_name(text, sizeof(text), bm_name, "server_0");
    if( (connection.client.request=::open(fifo, O_WRONLY|O_NONBLOCK)) < 0 ) {
      ::lib_rtl_signal_message(LIB_RTL_OS,"MBM: %s: Cannot open connection '%s' for MBM buffer %s.",
			       name,fifo,bm_name);
      _close(connection);
      return MBM_ERROR;
    }
    _fifo_name(connection.name, sizeof(connection.name), bm_name, name);
    struct stat buff;
    if ( 0 == ::stat(connection.name,&buff) )   {
      ::unlink(connection.name);
    }
    if ( ::mkfifo(connection.name,0666)  ) {
      if ( errno != EEXIST ) { // It is not an error if the file already exists....
	::lib_rtl_signal_message(LIB_RTL_OS,"MBM: %s: Cannot create MBM connection '%s'.",
				 name,connection.name);
	return MBM_ERROR;
      }
    }
    ::chmod(connection.name,0666);
    if( -1 == (connection.client.response = ::open(connection.name, O_NONBLOCK | O_RDWR ))) {
      ::lib_rtl_signal_message(LIB_RTL_OS,
			       "MBM: [%s] Unable to open the answer connection %s.",
			       name,connection.name);
      return MBM_ERROR;
    }
    return MBM_NORMAL;
  }

  int _read(MBMMessage& msg, int fd, int* cancel_flag) {
    int tmp = 0;
    int len = sizeof(MBMMessage);
    char* p = (char*)&msg;
    ::memset(p,0,len);
    while ( tmp < len )  {
      errno = 0;
      int sc = ::read(fd,p+tmp,len-tmp);
      if ( sc >  0 )
	tmp += sc;
      else if ( sc <= 0 && errno == EINTR  )
	continue;
      else if ( sc <= 0 && errno == EAGAIN && cancel_flag && *cancel_flag )
	return msg.status=MBM_ERROR;
      else if ( sc == 0 && errno == EAGAIN )
	continue;
      else if ( sc  < 0 && errno == EAGAIN ) 
	return msg.status=MBM_ERROR;
      else {
	::lib_rtl_output(LIB_RTL_ALWAYS,"MBM: Recv MBM message FAIL");
	return msg.status=MBM_ERROR;
      }
    }
#ifdef _DEBUG_MBM_MSG
    ::lib_rtl_output(LIB_RTL_ALWAYS,"MBM: Recv MBM message %-16s --> %-16s status=%d\n",
		     msg.c_user(),msg.c_type(),msg.status);
#endif
    if ( msg.magic != MBMMessage::MAGIC ) {
      ::lib_rtl_output(LIB_RTL_ERROR,"MBM: Wrong magic word in MBM message:%X instead of %X.\n",
		       msg.magic,MBMMessage::MAGIC);
    }
    return MBM_NORMAL;
  }


  int _write(MBMMessage& msg, int fd) {
    const char* p = (const char*)&msg;
    int tmp = sizeof(MBMMessage), len = sizeof(MBMMessage);
#ifdef _DEBUG_MBM_MSG
    ::lib_rtl_output(LIB_RTL_ALWAYS,"MBM: Send MBM message %-16s --> %-16s status=%d\n",
		     msg.c_user(),msg.c_type(),msg.status);
#endif
    while ( tmp>0 )  {
      int sc = ::write(fd,p+len-tmp,tmp);
      if ( sc > 0 ) tmp -= sc;
      else if ( sc == 0 && errno == EINTR ) continue;
      else return msg.status=MBM_ERROR;
    }
    return msg.status=MBM_NORMAL;
  }

  int _send_response_client(ServerBMID, MBMConnection& connection, const MBMMessage& msg)   {
    const char* p = (const char*)&msg;
    int tmp = sizeof(msg);
    int fd = connection.client.response;
    while ( tmp>0 )  {
      int sc = ::write(fd,p+sizeof(msg)-tmp,tmp);
      if ( sc > 0 ) tmp -= sc;
      else if ( sc == 0 && errno == EINTR ) continue;
      else return MBM_ERROR;
    }
    return MBM_NORMAL;
  }

  int _close_client(ServerBMID, MBMConnection& connection)  {
    return _close(connection);
  }

  int _accept_client(ServerBMID, MBMConnection& connection, const char* bm_name, const char* name)   {
    connection.init();
    _fifo_name(connection.name, sizeof(connection.name), bm_name, name);
    if( -1 == (connection.client.response = ::open(connection.name, O_NONBLOCK | O_RDWR ))) {
      ::lib_rtl_signal_message(LIB_RTL_OS,
			       "MBM: [%s] Unable to open the answer connection %s.",
			       name,connection.name);
      return MBM_ERROR;
    }
    return MBM_NORMAL;
  }

  int _bind_client(ServerBMID, MBMConnection& connection, const char* bm_name, int id)   {
    int fd;
    char text[32];
    ::snprintf(text, sizeof(text), "server_%d", id);
    text[sizeof(text)-1] = 0;
    _fifo_name(connection.name, sizeof(connection.name), bm_name, text);
    connection.name[sizeof(connection.name)-1] = 0;
    if( ::mkfifo(connection.name,0666)  ) {
      if ( errno != EEXIST ) { // It is not an error if the file already exists....
	::lib_rtl_output(LIB_RTL_OS,
			 "MBM: Unable to create the connection: %s.\n",
			 connection.name);
	return MBM_ERROR;
      }
    }
    ::chmod(connection.name,0666);
    if( -1 == (fd=::open(connection.name,O_RDWR|O_NONBLOCK)) ) {
      ::lib_rtl_output(LIB_RTL_OS,
		       "MBM: Unable to open the connection: %s\n",
		       connection.name);
      return MBM_ERROR;
    }
    connection.master.request = fd;
    if ( -1 == (::fcntl(fd,F_SETFL,::fcntl(fd,F_GETFL)|O_NONBLOCK)) ) {
      ::lib_rtl_output(LIB_RTL_OS,
		       "MBM: Unable to set connection: %s non-blocking\n",
		       connection.name);
    }
    connection.master.poll = ::epoll_create(1);
    if ( connection.master.poll < 0 ) {
      ::lib_rtl_output(LIB_RTL_OS,
		       "MBM: Failed to create pollset for server connection %s.\n",
		       connection.name);
      return MBM_ERROR;
    }
    struct epoll_event epoll;
    epoll.events = EPOLLIN | EPOLLPRI | EPOLLERR | EPOLLHUP;
    epoll.data.fd = fd;
    if ( 0 > ::epoll_ctl(connection.master.poll,EPOLL_CTL_ADD,fd,&epoll) ) {
      ::lib_rtl_output(LIB_RTL_OS,
		       "MBM: Failed to add server connection %s to epoll descriptor.\n",
		       connection.name);
      return MBM_ERROR;
    }
    return MBM_NORMAL;
  }

  /// Create new poll pool
  int _poll_create(ServerBMID, MBMConnection& connection, int max_count)   {
    connection.master.poll = ::epoll_create(max_count);
    if ( connection.master.poll < 0 ) {
      ::lib_rtl_output(LIB_RTL_OS,
		       "MBM: Failed to create client epollset: %s.\n",
		       connection.name);
      return MBM_ERROR;
    }
    return MBM_NORMAL;
  }

  /// Add connection to poll pool
  int _poll_add(ServerBMID, MBMConnection& connection, MBMConnection& client)   {
    struct epoll_event epoll;
    epoll.events  = EPOLLERR | EPOLLHUP;
    epoll.data.fd = client.client.response;
    if ( 0 > ::epoll_ctl(connection.master.poll, EPOLL_CTL_ADD, client.client.response, &epoll) ) {
      ::lib_rtl_output(LIB_RTL_OS,
		       "MBM: Failed to client connection %s to epoll descriptor.\n",
		       client.name);
      return MBM_ERROR;
    }
    return MBM_NORMAL;
  }

  /// Remove connection from poll pool
  int _poll_del(ServerBMID, MBMConnection& connection, MBMConnection& client)   {
    struct epoll_event epoll;
    int fd = client.client.response;
    if ( fd >= 0 )  {
      epoll.data.fd = fd;
      if ( 0 > ::epoll_ctl(connection.master.poll, EPOLL_CTL_DEL, fd, &epoll) ) {
	::lib_rtl_output(LIB_RTL_OS,"MBM: Failed to remove client connection %d.\n",fd);
      }
      return MBM_NORMAL;
    }
    return MBM_ERROR;
  }

  /// Poll on connections delivering data
  int _poll(ServerBMID, MBMConnection& connection, int& events, int tmo)  {
    struct epoll_event epoll;
    int nclients = ::epoll_wait(connection.master.poll, &epoll, 1, tmo);
    events = epoll.events;
    if ( nclients > 0 )
      return epoll.data.fd;
    return nclients;
  }

  int _open_client(ServerBMID, MBMConnection& connection, const char* bm_name, const char* name)  {
    return _open(connection, bm_name, name);
  }

#ifdef MBM_CLIENT_STANDALONE
  int _dispatch_client(ServerBMID, int)  {
    ::lib_rtl_output(LIB_RTL_FATAL,"MBM: _dispatch_client: Invalid request in standalone client!");
    return MBM_ERROR;
  }
#else
  int _read_request(MBMConnection& connection, void* buffer, size_t length)   {
    int tmp = 0;
    int len = length;
    char* p = (char*)buffer;
    ::memset(p,0,len);
    while ( tmp < len )  {
      errno = 0;
      int sc = ::read(connection.client.request,p+tmp,len-tmp);
      if ( sc >  0 )
	tmp += sc;
      else if ( sc <= 0 && errno == EINTR  )
	continue;
      else if ( sc == 0 && errno == EAGAIN )
	continue;
      else if ( sc  < 0 && errno == EAGAIN ) 
	return MBM_ERROR;
      else {
	::lib_rtl_output(LIB_RTL_ALWAYS,"MBM: Recv MBM message FAIL");
	return MBM_ERROR;
      }
    }
    return MBM_NORMAL;
  }

  int _dispatch_client(ServerBMID bm, int which)  {
    ServerBMID_t::Server& s = bm->server[which];
    size_t loop_count = 0, check_count = 0, pending_count = 0;
    MBMConnection connection;
    MBMDispatchTimer timer;
    MBMMessage msg(0);

    bm->server[which].running = 1;
    ::lib_rtl_output(LIB_RTL_DEBUG,"++bm_server++ Thread %3d Dispatching Fifo connections....\n",which);

    while ( !bm->stop )   {
      int events = 0;
      ++loop_count;
      if ( bm->stop )  {
	break;
      }
      connection.client.request = _poll(bm, s.connection, events, 200);
      if ( bm->stop )   {
	break;
      }
      if ( !bm->client_thread )   {
	if ( 0 == (loop_count%10) && connection.client.request == 0)
	  timer.update();
	if ( events&(EPOLLERR|EPOLLHUP) || timer.difference > 2500000L )  {
	  int ret = mbmsrv_client_watch_cycle(bm);
	  if ( (ret&1) ) ++check_count;
	  if ( (ret&2) ) ++pending_count;
	  timer.reset();
	}
      }
      if ( connection.client.request < 0 && errno == EINTR )   {
	continue;
      }
      else if ( connection.client.request <= 0 )   {
	mbmsrv_client_watch_cycle(bm);
	continue;
      }
      else if ( events&EPOLLIN ) {
	if ( MBM_NORMAL != _read_request(connection,&msg,sizeof(msg)) ) {
	  ::lib_rtl_output(LIB_RTL_ERROR,"++bm_server++ Error while reading : message is lost [%d: %s]",
			   ::lib_rtl_get_error(), RTL::errorString().c_str());
	  continue;
	}
	else if ( msg.magic != MBMMessage::MAGIC ) {
	  ::lib_rtl_output(LIB_RTL_ERROR,"++bm_server++ Wrong magic word in MBM message:%X instead of %X.",
			   msg.magic,MBMMessage::MAGIC);
	  continue;
	}
	mbmsrv_handle_request(bm,0,msg);
	if ( msg.status != MBM_NO_REPLY ) {
	  // Do not send a message if the user is already free'ed
	  if ( msg.user && msg.user->uid != -1 )  {
	    msg.status = _send_response_client(bm, msg.user->connection,msg);
	  }
	  continue;
	}
	//if ( nreq == 200 ) ProfilerStart("mbm_server.prof");
	//if ( nreq == 5000000 ) break;
      }
    }
    if ( check_count || pending_count )   {
    }
    //ProfilerStop();
    bm->server[which].running = 0;
    return 1;
  }
#endif
  int _open_server(BMID bm, const char* bm_name, const char* name)  {
    return _open(bm->connection, bm_name, name);
  }
  int _close_server(BMID bm)  {
    return _close(bm->connection);
  }

  int _move_server(BMID bm, const char* bm_name, USER*, int serverid)   {
    char text[256];
    ::snprintf(text,sizeof(text),"server_%d", serverid);
    _fifo_name(bm->connection.name, sizeof(bm->connection.name), bm_name, text);
    ::close(bm->connection.client.request);
    if( (bm->connection.client.request=::open(text,O_WRONLY|O_NONBLOCK)) < 0 ) {
      ::lib_rtl_signal_message(LIB_RTL_OS,
			       "MBM: Failed to open server connection '%s' for MBM buffer %s.",
			       text,bm_name);
      return MBM_ERROR;
    }
    return MBM_NORMAL;
  }

  /// Exchange MBM message with server
  int _communicate_server(BMID bm, MBMMessage& msg, int* cancelled)   {
    int fdout = bm->connection.server.request;
    int fdin  = bm->connection.server.response;
    int typ   = msg.type;
    if ( MBM_NORMAL != (msg.status=_write(msg, fdout)) ) {
      ::lib_rtl_signal_message(LIB_RTL_OS,"MBM: Failed to send request '%d' to server.\n",typ);
      return MBM_ERROR;
    }
#ifdef _DEBUG_MBM_MSG
    ::lib_rtl_output(LIB_RTL_ALWAYS,"MBM: Poll MBM message %-16s --> %-16s status=%d\n",
		     msg.c_user(),msg.c_type(),status);
#endif
    struct pollfd fds[2];
    while (1)  {
      int err = 0;
      fds[0].events  = POLLIN;
      fds[0].revents = POLLIN|POLLERR|POLLHUP;
      fds[0].fd      = fdin;
      fds[1].events  = POLLIN;
      fds[1].revents = POLLIN|POLLERR|POLLHUP;
      fds[1].fd      = fdout;

      ::poll(fds,2,100);
      if ( fds[0].revents&POLLIN )   {
	break;
      }
      else if ( cancelled && *cancelled )   {
#ifdef _DEBUG_MBM_MSG
	::lib_rtl_output(LIB_RTL_ALWAYS,"MBM: Recv CANCEL %-16s --> %-16s status=%d\n",
			 msg.c_user(),msg.c_type(),msg.status);
#endif
	return msg.status=MBM_REQ_CANCEL;
      }
      else if ( fds[0].revents&POLLHUP )    {
	::lib_rtl_output(LIB_RTL_ALWAYS,"MBM: Recv POLLHUP(IN) %-16s --> %-16s status=%d\n",
			 msg.c_user(),msg.c_type(),msg.status);
	err = 1;
      }
      else if ( fds[0].revents&POLLERR )    {
	::lib_rtl_output(LIB_RTL_ALWAYS,"MBM: Recv POLLERR(IN) %-16s --> %-16s status=%d\n",
			 msg.c_user(),msg.c_type(),msg.status);
	err = 2;
      }
      else if ( fds[1].revents&POLLHUP )    {
	::lib_rtl_output(LIB_RTL_ALWAYS,"MBM: Recv POLLHUP(OUT) %-16s --> %-16s status=%d\n",
			 msg.c_user(),msg.c_type(),msg.status);
	err = 4;
      }
      else if ( fds[1].revents&POLLERR )    {
	::lib_rtl_output(LIB_RTL_ALWAYS,"MBM: Recv POLLERR(OUT) %-16s --> %-16s status=%d\n",
			 msg.c_user(),msg.c_type(),msg.status);
	err = 8;
      }
      // Known problem with desynchronized excludes on shutdown
      if ( err != 0 && msg.type == MBMMessage::EXCLUDE )   {
	return msg.status = MBM_ERROR;
      }
    }
    if ( MBM_NORMAL != _read(msg,fdin,cancelled) ) {
      ::lib_rtl_output(LIB_RTL_OS,
		       "MBM: Failed to receive answer '%d' from server.\n",typ);
      return MBM_ERROR;
    }
    // Communication now OK. Let's check if the actual request failed or not:
    if ( msg.status != MBM_NORMAL     && msg.status != MBM_NO_EVENT && 
	 msg.status != MBM_NO_FREE_US && msg.status != MBM_NO_ROOM ) {
      ::lib_rtl_output(LIB_RTL_OS,
		       "MBM: Failed to execute request '%d' in server. Status=%d \n",
		       typ,msg.status);
      return msg.status;
    }
    return msg.status;
  }

  /// Clear possibly pending data from communication channel
  int _clear_server(BMID bm)   {
    char buff[sizeof(MBMMessage)];
    while (1)  {
      // Connection is non-blocking, so just read some junks
      int sc = ::read(bm->connection.server.response,buff,sizeof(MBMMessage));
      if ( sc > 0 ) {
	continue;
      }
      else if ( sc == -1 && errno == EINTR ) {
	continue;
      }
      break;
    }
    return MBM_NORMAL;
  }

  /// Send request to server
  int _send_request_server(BMID bm, MBMMessage& msg, bool clear_before)   {
    if ( clear_before )  {
      _clear_server(bm);
    }
    return msg.status=_write(msg,bm->connection.server.request);
  }

  /// Read server response
  int _read_response_server(BMID bm, MBMMessage& msg, int* cancelled)  {
    int fd = bm->connection.server.response;
    struct pollfd fds;
    while (1)  {
      fds.events  = POLLIN;
      fds.revents = POLLIN|POLLERR;
      fds.fd      = fd;
      ::poll(&fds,1,100);
      if ( cancelled && *cancelled )  {
#ifdef _DEBUG_MBM_MSG
	::lib_rtl_output(LIB_RTL_ALWAYS,"MBM CANCEL %-16s --> %-16s status=%d\n",
			 msg.c_user(),msg.c_type(),msg.status);
#endif
	return msg.status=MBM_REQ_CANCEL;
      }
      else if ( fds.revents&POLLIN )
	break;
    }
    if ( MBM_NORMAL != _read(msg, fd, cancelled) ) {
      if ( cancelled && *cancelled )   {
	::lib_rtl_output(LIB_RTL_OS,"Failed to receive answer '%d' [Request cancelled].\n",msg.type);
	return MBM_ERROR;
      }
      ::lib_rtl_output(LIB_RTL_OS,"MBM: Failed to receive answer '%d' from server.\n",msg.type);
      return msg.status=MBM_ERROR;
    }
    // Communication now OK. Let's check if the actual request failed or not:
    switch(msg.status)  {
    case MBM_NORMAL:
      return msg.status;
    case MBM_NO_EVENT:
    case MBM_NO_FREE_US:
    case MBM_ILL_LEN:
    case MBM_ILL_REQ:
    case MBM_NO_ROOM:
      return msg.status;
    default:
      ::lib_rtl_signal_message(LIB_RTL_OS,"Failed to execute request '%d' in server. Status=%d \n",
		       msg.type,msg.status);
    }
    return msg.status;
  }
}

int _mbm_connections_use_fifos(MBMServerCommunication& com)   {
  com.type          = BM_COM_FIFO;
  /** MBM Server communication functions   */
  com.m_accept        = _accept_client;
  com.m_bind          = _bind_client;
  com.m_open          = _open_client;
  com.m_close         = _close_client;
  com.m_send_response = _send_response_client;
  com.m_dispatch      = _dispatch_client;
  com.m_poll          = _poll;
  com.m_poll_del      = _poll_del;
  com.m_poll_add      = _poll_add;
  com.m_poll_create   = _poll_create;
  com.m_stop          = 0;
  return MBM_NORMAL;
}

int _mbm_connections_use_fifos(MBMClientCommunication& com)   {
  com.type                 = BM_COM_FIFO;
  /** MBM Client communication functions   */
  com.m_open_server          = _open_server;
  com.m_close_server         = _close_server;
  com.m_move_server          = _move_server;
  com.m_communicate_server   = _communicate_server;
  com.m_clear_server         = _clear_server;
  com.m_send_request_server  = _send_request_server;
  com.m_read_response_server = _read_response_server;
  return MBM_NORMAL;
}
