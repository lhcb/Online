//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================

// Framework include files
#include <MBM/bmstruct.h>
#include <MBM/bmserver.h>

#include "mbmlib_server.h"

/// MAP buffer manager section
int _mbmsrv_map_buff_section(BufferMemory* bm);
/// MAP buffer manager sections
int _mbmsrv_map_ctrl_section(BufferMemory* bm, bool quiet);
/// Unmap server memory sections
int _mbmsrv_unmap_sections(BufferMemory* bm, bool delete_sections);

/// MAP buffer manager section
int _mbmsrv_map_buff_section(BufferMemory* bm)  {
  char text[128];
  const char* bm_name = bm->bm_name;
  ::snprintf(text,sizeof(text),"bm_buff_%s",bm_name);
  int status = ::lib_rtl_map_section(text,0,&bm->buff_add);
  if ( !lib_rtl_is_success(status) )    {
    ::lib_rtl_output(LIB_RTL_OS,"++bm_serevr++ Error mapping buffer section for MBM buffer %s", bm_name);
    return MBM_ERROR;
  }
  bm->buffer_add  = (char*)bm->buff_add->address;
  return MBM_NORMAL;
}

/// MAP buffer manager sections
int _mbmsrv_map_ctrl_section(BufferMemory* bm, bool quiet)  {
  char text[128];
  size_t ctrl_len, evnt_len, user_len;
  const char* bm_name = bm->bm_name;
  ::snprintf(text,sizeof(text),"bm_ctrl_%s",bm_name);
  int status = ::lib_rtl_map_section(text,0,&bm->gbl_add);
  if ( !lib_rtl_is_success(status) )    {
    if ( !quiet )   {
      ::lib_rtl_output(LIB_RTL_OS,"++bm_server++ Error mapping control section for MBM buffer %s", bm_name);
      return MBM_ERROR;
    }
    return MBM_ERROR;
  }
  bm->gbl         = (char*)bm->gbl_add->address;
  bm->ctrl        = (CONTROL*)bm->gbl;
  ctrl_len        = mbm_section_length(sizeof(CONTROL));
  evnt_len        = mbm_section_length(sizeof(EVENTDesc)+(bm->ctrl->p_emax-1)*sizeof(EVENT));
  user_len        = mbm_section_length(sizeof(USERDesc)+(bm->ctrl->p_umax-1)*sizeof(USER));
  bm->usDesc      = (USERDesc*)(bm->gbl+ctrl_len);
  bm->user        = &bm->usDesc->users[0];
  bm->evDesc      = (EVENTDesc*)(bm->gbl+ctrl_len+user_len);
  bm->event       = &bm->evDesc->events[0];
  bm->bitmap      = bm->gbl+ctrl_len+evnt_len+user_len+evnt_len;
  bm->bitmap_size = bm->ctrl->bm_size;
  bm->buffer_size = bm->ctrl->buff_size;
  return MBM_NORMAL;
}

/// Unmap server memory sections
int _mbmsrv_unmap_sections(BufferMemory* bm, bool delete_sections)  {
  if ( delete_sections )    {
    lib_rtl_gbl_t bm_all;
    if ( bm->gbl        ) ::lib_rtl_delete_section(bm->gbl_add);
    if ( bm->buffer_add ) ::lib_rtl_delete_section(bm->buff_add);
    int status = ::mbmsrv_map_global_buffer_info(&bm_all, true);
    if( ::lib_rtl_is_success(status) )   {
      BUFFERS* buffs = (BUFFERS*)bm_all->address;
      for(int i=0; i < buffs->p_bmax; ++i)  {
	if ( ::strcmp(buffs->buffers[i].name, bm->bm_name)==0 )  {
	  buffs->buffers[i].used = 0;
	  buffs->nbuffer = (buffs->nbuffer>0) ? buffs->nbuffer - 1 : 0;
	  ::memset(buffs->buffers[i].name, 0, sizeof(buffs->buffers[i].name));
	  break;
	}
      }
      ::mbmsrv_unmap_global_buffer_info(bm_all);
    }
  }
  else   {
    if ( bm->gbl        ) ::lib_rtl_unmap_section(bm->gbl_add);
    if ( bm->buffer_add ) ::lib_rtl_unmap_section(bm->buff_add);
  }
  bm->gbl        = 0;
  bm->buff_add   = 0;
  bm->buffer_add = 0;
  bm->bitmap     = 0;
  bm->user       = 0;
  bm->event      = 0;
  bm->ctrl       = 0;
  bm->user       = 0;
  bm->usDesc     = 0;
  bm->event      = 0;
  bm->evDesc     = 0;
  bm->bitmap     = 0;
  return MBM_NORMAL;
}

/// Map global buffer information on this machine
int mbmsrv_map_global_buffer_info(lib_rtl_gbl_t* handle, bool create)  {
  lib_rtl_gbl_t h;
  size_t len = sizeof(BUFFERS)+(MBM_MAX_BUFF-1)*sizeof(BUFFERS::BUFF);
  int status = ::lib_rtl_map_section("bm_buffers", len, &h);
  if( !lib_rtl_is_success(status) && create )    {
    status = ::lib_rtl_create_section("bm_buffers", len, &h,
				      LIB_RTL_GBL_SHARED|LIB_RTL_GBL_RDWR|LIB_RTL_GBL_KEEP);
    if(!lib_rtl_is_success(status))   {   
      return MBM_ERROR;
    }
    BUFFERS* buffs = (BUFFERS*)h->address;
    ::memset(buffs,0,len);
    buffs->nbuffer = 0;
  }
  else if (!lib_rtl_is_success(status)) {
    *handle = 0;
    return MBM_ERROR;
  }
  BUFFERS* b = (BUFFERS*)h->address;
  b->p_bmax = MBM_MAX_BUFF;
  // if GBL was present, but had a smaller size:
  if ( h->size < len )  {
    b->p_bmax = (h->size - sizeof(BUFFERS))/sizeof(BUFFERS::BUFF);
  }
  *handle = h;
  return MBM_NORMAL;
}

/// Unmap global buffer information on this machine
int mbmsrv_unmap_global_buffer_info(lib_rtl_gbl_t handle, bool remove)  {
  if ( handle )  {
    BUFFERS* buffs = (BUFFERS*)handle->address;
    int status = (buffs->nbuffer == 0 && remove) 
      ? ::lib_rtl_delete_section(handle)
      : ::lib_rtl_unmap_section(handle);
    return lib_rtl_is_success(status) ? MBM_NORMAL : MBM_ERROR;
  }
  return MBM_ERROR;
}

/// Interface routine: unlink memory sections
extern "C" int mbmsrv_unlink_memory(ServerBMID bmid) {
  return _mbmsrv_unmap_sections(bmid, true);
}

/// Interface routine: unmap memory
extern "C" int mbmsrv_unmap_memory(BufferMemory* bmid) {
  return _mbmsrv_unmap_sections(bmid, false);
}

/// Interface routine: map memory necessary for monitoring
extern "C" int mbmsrv_map_mon_memory(const char* bm_name, BufferMemory* bm)  {
  if ( bm && bm->magic == MBM_USER_MAGIC ) {
    ::strncpy(bm->bm_name,bm_name,sizeof(bm->bm_name));
    bm->bm_name[sizeof(bm->bm_name)-1] = 0;
    int sc = _mbmsrv_map_ctrl_section(bm, false);
    if ( sc == MBM_NORMAL )  {
      return MBM_NORMAL;
    }
  }
  return MBM_ERROR;
}

/// Interface routine: map memory necessary for monitoring
extern "C" int mbmsrv_map_mon_memory_quiet(const char* bm_name, BufferMemory* bm)  {
  if ( bm && bm->magic == MBM_USER_MAGIC ) {
    ::strncpy(bm->bm_name,bm_name,sizeof(bm->bm_name));
    bm->bm_name[sizeof(bm->bm_name)-1] = 0;
    int sc = _mbmsrv_map_ctrl_section(bm, true);
    if ( sc == MBM_NORMAL )  {
      return MBM_NORMAL;
    }
  }
  return MBM_ERROR;
}

/// Interface routine: map memory necessary for monitoring
extern "C" int mbmsrv_remap_mon_memory(BufferMemory* bm) {
  return _mbmsrv_map_ctrl_section(bm, false);
}

/// Interface routine: Map all memory sections of the server process
extern "C" int mbmsrv_map_memory(const char* bm_name, BufferMemory* bm)  {
  if ( bm && bm->magic == MBM_USER_MAGIC ) {
    ::strncpy(bm->bm_name,bm_name,sizeof(bm->bm_name));
    bm->bm_name[sizeof(bm->bm_name)-1] = 0;
    int sc = _mbmsrv_map_ctrl_section(bm, false);
    if ( sc == MBM_NORMAL )  {
      sc = _mbmsrv_map_buff_section(bm);
      if ( sc == MBM_NORMAL )  {
        return MBM_NORMAL;
      }
    }
  }
  return MBM_ERROR;
}

#include <sys/time.h>

MBMDispatchTimer::MBMDispatchTimer()   {
  ::gettimeofday(&first,0);
  last = now = first;
  last.tv_sec -= first.tv_sec;
  now.tv_sec  -= first.tv_sec;
  difference = 0;
}
void MBMDispatchTimer::update()  {
  ::gettimeofday(&now,0);
  now.tv_sec -= first.tv_sec;
  difference = long(now.tv_sec)*1000000L + long(now.tv_usec)
    - long(last.tv_sec)*1000000L - long(last.tv_usec);
}
