//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
#ifndef ONLINEBASE_MBM_BMMESSAGE_H
#define ONLINEBASE_MBM_BMMESSAGE_H

// Framework include files
#include <MBM/bmdef.h>

// Forward declarations


/** @class MBMMessage bmmessage.h MBM/bmmessage.h
 *
 *  Definition of the exchange structure between the MBM clients and the server.
 *
 *  @author  M.Frank
 *  @version 1.0
 */
struct MBMMessage {
  enum msg_magic { 
    MAGIC=0xFEEDBABE 
  };
  enum msg_type {
    MSG_NONE       = 0,
    INCLUDE        = 1,
    EXCLUDE        = 2,
    RECONNECT      = 3,
    FORCE_SHUTDOWN = 4,
    // Consummer commands
    ADD_REQUEST    = 103,
    DEL_REQUEST    = 104,
    GET_EVENT      = 105,
    WAIT_EVENT     = 106,
    FREE_EVENT     = 107,
    STOP_CONSUMER  = 108,
    PAUSE          = 109,
    GET_EVENT_TRY  = 110,
    // Producer commands
    GET_SPACE_TRY  = 201,
    GET_SPACE      = 202,
    WAIT_SPACE_A   = 204,
    FREE_SPACE     = 205,
    SEND_SPACE     = 206,
    SEND_EVENT     = 207,
    DECLARE_EVENT  = 208,
    CANCEL_REQUEST = 209,
    STOP_PRODUCER  = 210,
    GRANT_UPDATE   = 211,
    // Server commands
    REQUIRE_CONS   = 401,
    UNREQUIRE_CONS = 402,
    //
    //statistics
    PROCESS_EXISTS       = 301,
    STAT_EVENTS_ACTUAL   = 302,
    STAT_EVENTS_SEEN     = 303,
    STAT_EVENTS_PRODUCED = 304,
    STAT_RESET           = 305,
    STAT_MIN_ALLOC       = 306,
    STAT_BUFFER_SIZE,
    STAT_EVENTS_IN_BUFFER,
    STAT_SPACE_IN_BUFFER
  };
  USER*        user;
  int          type;
  int          status;

  struct include_t { // size: 76
    char name[64];
    int  serverid;
    int  pid;
    int  partid;
  };
  typedef include_t exclude_t;
  typedef include_t process_exists_t;

  struct requirement_t  {  // size: 16+16+20=52
    unsigned int mask[BM_MASK_SIZE];
    unsigned int veto[BM_MASK_SIZE];
    int          masktype;
    int          usertype;
    int          freqmode;
    int          evtype;
    float        frequency;
  };

  struct cons_requirement_t  {  // size: 64+16+8 = 88
    char          name[64];
    unsigned int  mask[BM_MASK_SIZE];
    int           evtype;
    int           partid;
  };

  struct get_event_t { // size: 36
    unsigned int  trmask[BM_MASK_SIZE];
    long          offset;
    long          size;
    int           type;
  };

  struct get_space_t { // size: 16
    long          size;
    long          offset;
  }; 

  struct declare_event_t : public get_event_t { // size: 36+80
    int           wait;
    long          freeAddr;
    long          freeSize;
    char          dest[64];
  };

  struct send_event_t : public get_event_t { // size:36+64
    char  dest[64];
  };
  struct space_in_buffer_t { // size: 16
    long total;
    long large;
  };

  union  msg_structs  {
    include_t          include;
    exclude_t          exclude;
    requirement_t      requirement;
    cons_requirement_t cons_requirement;
    get_space_t        get_space;
    declare_event_t    declare_event;
    send_event_t       send_event;
    get_event_t        get_event;
    space_in_buffer_t  space_in_buffer;
    process_exists_t   process_exists;
    long               statistics;
  } data;
  unsigned int magic;

  /// Default constructor
  explicit MBMMessage();
  /// Initializing constructor
  explicit MBMMessage(int typ, USER* u=0, int stat=MBM_ERROR);
  /// Move constructor
  MBMMessage(MBMMessage&& copy) = delete;
  /// Copy constructor
  MBMMessage(const MBMMessage& copy) = delete;
  /// Move assignment
  MBMMessage& operator=(MBMMessage&& copy) = delete;
  /// Copy assignment
  MBMMessage& operator=(const MBMMessage& copy) = delete;
  /// Clean possibly pending messages from the receive fifo (e.g. after a cancel)
  static const char* typeStr(int typ);
  /// Helper to print message type
  const char* c_type() const;
  /// Helper to print message client (only valid in server context!)
  const char* c_user() const;
};

#ifdef _DEBUG
#include <cstring>
#endif

/// Default constructor
inline MBMMessage::MBMMessage()
		  : user(nullptr), type(MSG_NONE), status(MBM_ERROR), magic(MAGIC)
{
#ifdef _DEBUG
  ::memset(&data,0,sizeof(msg_structs));
#endif
}

/// Initializing constructor
inline MBMMessage::MBMMessage(int typ, USER* u, int stat) 
		  : user(u), type(typ), status(stat), magic(MAGIC)
{
#ifdef _DEBUG
  ::memset(&data,0,sizeof(msg_structs));
#endif
}
#endif // ONLINEBASE_MBM_BMMESSAGE_H
