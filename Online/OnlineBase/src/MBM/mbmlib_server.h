//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
#ifndef _MBM_MBMLIB_SERVER_H
#define _MBM_MBMLIB_SERVER_H

#include <RTL/rtl.h>
#include <MBM/bmstruct.h>

#if __cplusplus >= 201103L

// Compile switch if the server should use STL mutexes
#define MBM_HAVE_STD_MUTEX 1
// Compile switch if the server should use STL threads
#define MBM_HAVE_STD_THREAD 1

#include <memory>
#if defined(MBM_HAVE_STD_MUTEX) && MBM_HAVE_STD_MUTEX>0
#include <mutex>
#endif
#if defined(MBM_HAVE_STD_THREAD) && MBM_HAVE_STD_THREAD>0
#include <thread>
#endif

#endif   // __cplusplus >= 201103L

struct  MBMDispatchTimer;
struct  MBMServerCommunication;
struct  ServerBMID_t;
typedef ServerBMID_t* ServerBMID;

#define MBM_USER_MAGIC  0xFEEDBABE
#define MBM_MAX_BUFF    192

#include <RTL/Pack.h>

struct MBMDispatchTimer  {
  struct timeval first, last, now;
  long   difference;
  MBMDispatchTimer();
  void update();
  void reset()   { last = now; }
};

struct MBMServerCommunication  {
  int        type { BM_COM_NONE };
  ServerBMID bm   { nullptr };

  // MBM Server communication functions
  int (*m_accept)(ServerBMID bm, MBMConnection& connection, const char* bm_name, const char* name) = 0;
  int (*m_close)(ServerBMID bm, MBMConnection& connection) = 0;
  int (*m_bind)(ServerBMID bm, MBMConnection& connection, const char* bm_name, int id) = 0;
  int (*m_open)(ServerBMID bm, MBMConnection& connection, const char* bm_name, const char* name) = 0;
  int (*m_send_response)(ServerBMID bm, MBMConnection& connection, const MBMMessage& msg) = 0;

  /// Create new poll pool
  int (*m_poll_create)(ServerBMID bm, MBMConnection& connection, int max_count) = 0;
  /// Add connection to poll pool
  int (*m_poll_add)(ServerBMID bm, MBMConnection& connection, MBMConnection& client) = 0;
  /// Remove connection from poll pool
  int (*m_poll_del)(ServerBMID bm, MBMConnection& connection, MBMConnection& client) = 0;
  /// Poll on connections delivering data
  int (*m_poll)(ServerBMID bm, MBMConnection& connection, int& events, int tmo) = 0;
  /// Server dispatching routine
  int (*m_dispatch)(ServerBMID bm, int which) = 0;
  /// Server dispatching routine
  int (*m_stop)(ServerBMID bm) = 0;

  // MBM Server communication functions
  int accept(MBMConnection& connection, const char* bm_name, const char* name);
  int close(MBMConnection& connection);
  int bind(MBMConnection& connection, const char* bm_name, int id);
  int open(MBMConnection& connection, const char* bm_name, const char* name);
  int send_response(MBMConnection& connection, const MBMMessage& msg);

  /// Create new poll pool
  int poll_create(MBMConnection& connection, int max_count);
  /// Add connection to poll pool
  int poll_add(MBMConnection& connection, MBMConnection& client);
  /// Remove connection from poll pool
  int poll_del(MBMConnection& connection, MBMConnection& client);
  /// Poll on connections delivering data
  int poll(MBMConnection& connection, int& events, int tmo);
  /// Server dispatching routine
  int dispatch(int which);
  /// Stop server thread
  int stop();

  MBMServerCommunication() = default;
  MBMServerCommunication(MBMCommunication&& copy) = delete;
  MBMServerCommunication(const MBMCommunication& copy) = delete;
};

struct ServerBMID_t : public BufferMemory {
  int              _spare;
  int              num_threads           { 0 };
  int              threaded_cleanup      { 0 };
  int              stop                  { 0 };
  int              allow_declare         { 1 };
  int              save_events           { 0 };
  RTL_ast_t        free_event            { nullptr };
  void*            free_event_param      { nullptr };
  RTL_ast_t        alloc_event           { nullptr };
  void*            alloc_event_param     { nullptr };
#ifndef MBM_HAVE_STD_THREAD
  lib_rtl_thread_t client_thread         { nullptr };
#else
  std::unique_ptr<std::thread> client_thread;
#endif
#ifndef MBM_HAVE_STD_MUTEX
  lib_rtl_lock_t   lockid                { nullptr };
  typedef  RTL::Lock  LOCK;
#else
  std::mutex       lockid  { };
  typedef  std::lock_guard<std::mutex> LOCK;
#endif
  MBMServerCommunication communication { };
  MBMConnection          clients  { };
  lib_rtl_gbl_t          comm_add { nullptr };
  SHMCOMM_gbl*           comm     { nullptr };

  /// Callback setup for accident events (client crashes etc.)
  typedef void (*accident_callback_t)(void* param, ServerBMID bmid, long offset, size_t len, int typ, const unsigned int mask[], const char* user);
  accident_callback_t    accident_call  { nullptr };
  void*                  accident_param { nullptr };

  struct Server {
    MBMConnection    connection;
#ifndef MBM_HAVE_STD_THREAD
    lib_rtl_thread_t dispatcher;
#else
    std::unique_ptr<std::thread> dispatcher;
#endif
    int running {0};
    int stop    {0};
  } server[BM_MAX_THREAD];

  struct ConsumerREQ {
    REQ      requirement;
    char     name[BM_USER_NAME_LEN];     // user name match
    int      count;
  } cons_req[BM_MAX_REQS];

  ServerBMID_t();
  ~ServerBMID_t() = default;
};

#include <RTL/Unpack.h>

int _mbm_connections_use_fifos (MBMServerCommunication& com);
int _mbm_connections_use_asio  (MBMServerCommunication& com);
int _mbm_connections_use_unix  (MBMServerCommunication& com);
int _mbm_connections_use_shm1  (MBMServerCommunication& com);
int _mbm_connections_use_shm2  (MBMServerCommunication& com);

#define _CHECK( x )  { int sc = x ; if ( !(sc&1) ) { ::lib_rtl_printf ( "Error in:%s, status=%d\n", #x , sc ); return sc; } }
static inline size_t mbm_section_length(size_t bytes) {
  static const int SECTION_ALIGNMENT=256;
  return size_t((bytes+SECTION_ALIGNMENT-1)/SECTION_ALIGNMENT)*SECTION_ALIGNMENT;
}

#endif // _MBM_MBMLIB_SERVER_H
