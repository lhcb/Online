//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
//
//                  ASCII BUFFER MANAGER DISPLAY
//
//==========================================================================

/// Framework include files
#include <RTL/rtl.h>
#include <MBM/Monitor.h>
#include <CPP/AsciiDisplay.h>
#include <CPP/TimeSensor.h>

/// C/C++ include files
#include <cstdlib>
#include <cstring>

using namespace std;
using namespace SCR;

namespace  {
  struct Par {
    MBM::Monitor* mon   = 0;
    AsciiDisplay* ascii = 0;
    Display* help = 0;
    Display* disp = 0;
    char text[132] = {0};
    Par() = default;
  };
  int handle_text(SCR::Pasteboard* pb, void* par, int k)   {
    Par* p = (Par*)par;
    if ( p->disp )  {
      int len = ::strlen(p->text);
      ::scrc_begin_pasteboard_update (pb);
      if      ( len > 0 && 
		(k == 305 /* KPD_DEL */ ||
		 k == 260 /* CTRL_D  */ ||
		 k == 283 /* BACK_SPACE */) )  {
	p->text[--len] = 0;
      }
      else if ( k == 266 /* ENTER */ || k == 316 /* KPD_ENTER */ ) {
	string res = string("p=")+(len > 0 ? p->text : "*");
	p->ascii->remove_keyboard_handler(p,handle_text);
	::scrc_delete_display(p->disp);
	::scrc_end_pasteboard_update(pb);
	::scrc_fflush(pb);
	p->text[0] = 0;
	p->disp = 0;
	p->mon->optparse(res.c_str());
	return  true;
      } // CR
      else if ( k>=32 && k<126 )   {
	p->text[len] = char(k);
	p->text[++len] = 0;
      }
      ::scrc_put_chars (p->disp, p->text, BLUE|INVERSE, 1, 1, 1);
      ::scrc_end_pasteboard_update (pb);
      ::scrc_fflush (pb);
      ::scrc_set_cursor(p->disp, 1, len);
      return  true;
    }
    return false;
  }
  int handle_ctrl_p(SCR::Pasteboard* pb, void* par, int)   {
    Par* p = (Par*)par;
    ::scrc_begin_pasteboard_update (pb);
    if ( p->disp == 0 )  {
      ::scrc_create_display (&p->disp, 1,  50, BOLD|BLUE|INVERSE, ON, "Enter Partition name ('*' for all)");
      p->text[0] = 0;
      p->ascii->install_keyboard_handler(par,handle_text);
    }
    else   {
      Display* d = p->disp;
      p->disp = 0;
      p->ascii->remove_keyboard_handler(p,handle_text);
      ::scrc_delete_display(d);
    }
    if ( p->disp )  {
      ::scrc_put_chars (p->disp, p->text, BLUE|INVERSE, 1, 1, 1);
      ::scrc_paste_display (p->disp, pb, 10, 20);
      ::scrc_end_pasteboard_update (pb);
      ::scrc_fflush (pb);
      ::scrc_set_cursor (p->disp, 1, 1);
    }
    else  {
      ::scrc_end_pasteboard_update (pb);
      ::scrc_fflush (pb);
    }
    return true;
  }
  int handle_ctrl_h(SCR::Pasteboard* pb, void* par, int)   {
    Par* p = (Par*)par;
    ::scrc_begin_pasteboard_update (pb);
    if ( p->help )  {
      ::scrc_delete_display(p->help);
      p->help = 0;
    }
    else  {
      ::scrc_create_display (&p->help, 15,  50, RED|NORMAL, ON, "Help window");
      ::scrc_put_chars (p->help,"Help Menu                           ", NORMAL, 1, 1, 1);
      ::scrc_put_chars (p->help,"                                    ", NORMAL, 2, 1, 1);
      ::scrc_put_chars (p->help,"CTRL-H    Show/Hide help            ", NORMAL, 3, 1, 1);
      ::scrc_put_chars (p->help,"CTRL-W    Repaint screen            ", NORMAL, 4, 1, 1);
      ::scrc_put_chars (p->help,"CTRL-E    Exit monitor              ", NORMAL, 5, 1, 1);
      ::scrc_put_chars (p->help,"CTRL-P    Select partition name     ", NORMAL, 6, 1, 1);
      ::scrc_put_chars (p->help,"                                    ", NORMAL, 7, 1, 1);
      ::scrc_put_chars (p->help,"        Complaints and suggestions: ", NORMAL, 8, 1, 1);
      ::scrc_put_chars (p->help,"                  M. Frank CERN/LHCb", NORMAL, 9, 1, 1);
      ::scrc_paste_display (p->help, pb, 10, 20);
      ::scrc_set_cursor (p->help, 1, 1);
    }
    ::scrc_end_pasteboard_update (pb);
    ::scrc_fflush (pb);
    return true;
  }
}

/// MBM monitor with keyboard handler CTRL-E for exit, CTRL-W for repaint
extern "C" int mbm_mon_scr(int argc , char** argv) {
  AsciiDisplay* display = new AsciiDisplay("");
  MBM::Monitor mon(argc, argv, display);
  int status = mon.initMonitor();
  if (!lib_rtl_is_success(status))  {
    ::exit(status);
  }
  display->setup_window();
  display->install_keyboard_handler();

  /// Handler to change partition
  Par param;
  param.mon     = &mon;
  param.ascii   = display;
  param.disp    = 0;
  param.text[0] = 0;
  display->install_keyboard_handler(CTRL_P,&param,handle_ctrl_p);
  /// Handler to op-up help menu
  Par help;
  help.mon     = &mon;
  help.ascii   = display;
  help.disp    = 0;
  help.text[0] = 0;
  display->install_keyboard_handler(CTRL_H,&help,handle_ctrl_h);

  ::lib_rtl_install_printer(MBM::Monitor::print, &mon);
  mon.updateDisplay();
  TimeSensor::instance().add(&mon, 1);
  TimeSensor::instance().run();
  return 1;
}
