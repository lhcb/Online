//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  RawFile.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

/// Framework include files
#include <RTL/Datafile.h>
#include <RTL/posix.h>
#include <RTL/rtl.h>

/// C/C++ include files
#include <system_error>
#include <filesystem>
#include <sys/mman.h>
#include <sys/stat.h>
#include <iostream>
#include <iomanip>
#include <cstdint>
#include <cstring>
#include <climits>
#include <fcntl.h>
#include <cerrno>
#ifdef _WIN32
#include <io.h>
#else
#include <unistd.h>
#define O_BINARY 0
#endif

#define HEAP_MEMORY_MAPPED (-100)

namespace fs = std::filesystem;

/// Standard constructor
Online::Datafile::Datafile() : m_descriptor(native_posix_descriptor()) {
}

/// Standard constructor
Online::Datafile::Datafile(const std::string& fname)
  : path(fname), m_descriptor(native_posix_descriptor())  {
}

/// Standard constructor
Online::Datafile::Datafile(const std::string& fname, const posix_t* posix_descriptor)
  : path(fname), m_descriptor(posix_descriptor)  {
}

/// Initializing constructor
Online::Datafile::Datafile(const std::string& nam, int descriptor, const posix_t* posix_descriptor)
  : fd(descriptor), path(nam), m_descriptor(posix_descriptor)
{
}

/// Standard constructor
Online::Datafile::Datafile(int desc)  {
  char path_name[128], file_path[PATH_MAX];
  m_descriptor = native_posix_descriptor();
  ::snprintf(path_name, sizeof(path_name), "/proc/self/fd/%d", desc);
  if ( ::readlink(path_name, file_path, sizeof(file_path)) == -1 )  {
    return;
  }
  this->path = file_path;
  this->fd = desc;
}

/// Standard constructor
Online::Datafile::Datafile(int desc, const posix_t* posix)
  : fd(desc), path("Unknown"), m_descriptor(posix)  {
}

/// Standard constructor
Online::Datafile::Datafile(int desc, bool)
  : fd(desc), path(), m_descriptor(native_posix_descriptor())  {
}

/// Access file path as filesystem entity
std::filesystem::path Online::Datafile::fspath() const   {
  return this->path;
}

/// Access filename extension part of file path
std::filesystem::path Online::Datafile::extension() const   {
  return std::filesystem::path(this->path).extension();
}

/// Access filename part of file path
std::filesystem::path Online::Datafile::filename() const   {
  return std::filesystem::path(this->path).filename();
}

/// Access directory part of file path
std::filesystem::path Online::Datafile::directory() const   {
  return std::filesystem::path(this->path).parent_path();
}

/// Last modification/write time
std::filesystem::file_time_type Online::Datafile::last_write_time()  const   {
  return std::filesystem::last_write_time(this->fspath());
}

/// File system entry type (regular file, symlink, etc)
std::filesystem::file_status Online::Datafile::status() const  {
  return std::filesystem::status(this->fspath());
}

/// File system entry type (regular file, symlink, etc)
std::filesystem::file_type Online::Datafile::type() const  {
  std::error_code ec;
  auto status = std::filesystem::status(this->fspath(), ec);
  if ( !ec ) return status.type();
  return std::filesystem::file_type::not_found;
}

/// Check if file exists
bool Online::Datafile::exists()  const  {
  std::error_code ec;
  return std::filesystem::exists(this->fspath(), ec);
}

/// Check if file is empty
bool Online::Datafile::is_empty()  const  {
  std::error_code ec;
  bool ret = std::filesystem::is_empty(this->fspath(), ec);
  if ( !ec ) return ret;
  return true;
}

/// Set option to specific implemetation of the descriptor
int Online::Datafile::set_option(const std::string& what, const std::string& value)  {
  if ( isOpen() && !isMapped() )
    return m_descriptor->set_option(this->fd, what.c_str(), value.c_str());
  return -1;
}

/// Return error message in case of failure
std::string Online::Datafile::error_message(int error_code)  {
  return std::make_error_code(std::errc(error_code)).message();
}

/// Return error message in case of failure
std::string Online::Datafile::error_message() const  {
  return this->error_message(errno);
}

/// Return file position
long Online::Datafile::seek(long offset, int whence)  {
  if ( isOpen() && !isMapped() )  {
    off64_t file_position = m_descriptor->lseek64(this->fd, offset, whence);
    return file_position;
  }
  else if ( this->m_mem_map.begin )  {
    uint8_t* ptr = this->m_mem_map.ptr;
    switch(whence)  {
    case SEEK_SET:
      ptr = this->m_mem_map.begin;
      break;
    case SEEK_END:
      ptr = this->m_mem_map.end;
      break;
    case SEEK_CUR:
      break;
    default:
      errno = EINVAL;
      return -1;
    }
    ptr += offset;
    if ( ptr < this->m_mem_map.begin || ptr > this->m_mem_map.end )  {
      errno = EINVAL;
      return -1;
    }
    m_mem_map.ptr = ptr;
    off64_t file_position = m_mem_map.ptr - this->m_mem_map.begin;
    return file_position;
  }
  return -1;
}

/// Return file size: either real file size of mapped size
std::size_t Online::Datafile::data_size()   const  {
  if ( isOpen() && !isMapped() )  {
    struct stat sb;
    m_descriptor->fstat(this->fd, &sb); /* To obtain file size */
    return sb.st_size;
  }
  else if ( this->m_mem_map.begin )  {
    return this->m_mem_map.end - this->m_mem_map.begin;
  }
  return 0;
}

/// Return file position
long Online::Datafile::position()  const  {
  if ( isOpen() && !isMapped() )  {
    off64_t file_position = m_descriptor->lseek(this->fd, 0, SEEK_CUR);
    return file_position;
  }
  else if ( this->m_mem_map.begin )  {
    off64_t file_position = this->m_mem_map.ptr - this->m_mem_map.begin;
    return file_position;
  }
  return -1;
}

/// Return file position
long Online::Datafile::position(long offset, bool absolute)  {
  if ( isOpen() && !isMapped() )  {
    return this->seek(offset, absolute ? SEEK_SET : SEEK_CUR);
  }
  else if ( this->m_mem_map.begin && offset > 0 )  {
    if ( absolute && offset >= 0 )  {
      this->m_mem_map.ptr = this->m_mem_map.begin + offset;
    }
    else if ( absolute )  {
      errno = EINVAL;
      return -1;
    }
    else  {
      this->m_mem_map.ptr += offset;
      if ( this->m_mem_map.ptr > this->m_mem_map.end ) this->m_mem_map.ptr = this->m_mem_map.end;
    }
    return this->m_mem_map.ptr - this->m_mem_map.begin;
  }
  errno = EBADF;
  return -1;
}

// Close file descriptor if open
int Online::Datafile::close()  {
  if ( reuse )  {
    this->seek(0, SEEK_SET);    
    this->m_mem_map.ptr = this->m_mem_map.begin;
    return 0;
  }
  else if ( this->fd == HEAP_MEMORY_MAPPED )  {
    this->m_mem_map.begin = this->m_mem_map.end = this->m_mem_map.ptr = 0;
  }
  else if ( this->m_mem_map.begin )  {
    ::munmap(m_mem_map.begin, this->m_mem_map.end-m_mem_map.begin);
    this->m_mem_map.begin = this->m_mem_map.end = this->m_mem_map.ptr = 0;
  }
  else if ( this->fd > 0 )  {
    m_descriptor->close(this->fd);
  }
  this->mode = NONE;
  this->flags = 0;
  this->fd = -1;
  return 0;
}

/// Reset information (close and reset name)
int Online::Datafile::reset(bool close_file)  {
  if ( close_file )  {
    this->path = "";
    return this->close();
  }
  else if ( this->m_mem_map.begin )  {
    this->m_mem_map.ptr = this->m_mem_map.begin;
  }
  else if ( isOpen() )  {
    this->seek(0, SEEK_SET);
  }
  return 0;
}

/// Open file in read or write mode
int Online::Datafile::open(bool silent, const char* file_path, int open_flags, int open_mode)  {
  if ( isOpen() ) this->close();
  this->path = file_path;
  return (open_mode > 0)
    ? this->openWrite(silent, open_flags, open_mode)
    : this->open(silent, open_flags);
}

/// Open file in read mode
int Online::Datafile::open(bool silent, int open_flags)  {
  int flg = O_RDONLY | O_BINARY | O_LARGEFILE | open_flags;
  int dsc = m_descriptor->open(cname(), flg, S_IREAD);
  if ( -1 == dsc )  {
    std::string err = std::make_error_code(std::errc(errno)).message();
    ::lib_rtl_output(LIB_RTL_ERROR,"Datafile: CANNOT OPEN %s: [%s]", cname(), err.c_str());
    return this->fd = dsc;
  }
  ::lib_rtl_output(silent ? LIB_RTL_DEBUG : LIB_RTL_INFO,"Datafile: Opened %s for processing.",cname());
  this->mode |= READING;
  this->flags = flg;
  return this->fd = dsc;
}

/// Open file in write mode
int Online::Datafile::openWrite(bool silent, int open_flags, int open_mode)  {
  fs::path parent = fs::path(this->name()).parent_path();
  if ( !parent.string().empty() )  {
    std::error_code ec;
    auto ret = Online::Datafile::mkdir(parent.string(), S_IRWXU|S_IRWXG|S_IROTH|S_IXOTH, silent);
    if ( !fs::exists(parent.string(), ec) && ret )  {
      ::lib_rtl_output(LIB_RTL_ERROR,"Datafile: CANNOT CREATE DIRECTORY for %s: (writing) [%s]",
		       cname(), ec.message().c_str());
      return this->fd = -1;
    }
  }
  int flg = O_CREAT|O_WRONLY|O_BINARY|O_LARGEFILE|open_flags;
  int mod = S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|open_mode;
  int dsc = m_descriptor->open(cname(), flg, mod);
  if ( -1 == dsc )  {
    ::lib_rtl_output(LIB_RTL_ERROR,"Datafile: CANNOT OPEN %s: for writing [%s]", cname(), RTL::errorString(errno).c_str());
    return this->fd = dsc;
  }
  ::lib_rtl_output(silent ? LIB_RTL_DEBUG : LIB_RTL_INFO,"Datafile: Opened %s for writing.",cname());
  this->mode |= WRITING;
  this->flags = flg;
  return this->fd = dsc;
}

/// Open file in read mode with mapping
int Online::Datafile::openMapped(bool silent, int open_flags)  {
  int dsc = this->open(silent, open_flags);
  if ( -1 != dsc )  {
    struct stat sb;
    ::fstat(dsc, &sb); /* To obtain file size */
    void* ptr = ::mmap(NULL, sb.st_size, PROT_READ, MAP_PRIVATE, dsc, 0);
    if ( ptr == MAP_FAILED )  {
      ::lib_rtl_output(LIB_RTL_ERROR,"Datafile: CANNOT MMAP %s: [%s]", cname(), RTL::errorString(errno).c_str());
      close();
      return -1;
    }
    this->m_mem_map.ptr = this->m_mem_map.begin = (uint8_t*)ptr;
    this->m_mem_map.end = this->m_mem_map.begin + sb.st_size;
    this->flags = O_RDONLY | O_BINARY | O_LARGEFILE;
    this->mode |= MAPPED;
  }
  return dsc;
}

/// Map memory of the file
int Online::Datafile::map_memory(const void* ptr, std::size_t len)  {
  this->m_mem_map.begin = this->m_mem_map.ptr = this->m_mem_map.end = nullptr;
  this->close();
  this->m_mem_map.ptr = this->m_mem_map.begin = (uint8_t*)ptr;
  this->m_mem_map.end = this->m_mem_map.begin + len;
  this->fd   = HEAP_MEMORY_MAPPED;
  this->flags |= MAPPED;
  return 1;
}

/// Map memory of the file
int Online::Datafile::unmap_memory()  {
  this->m_mem_map.begin = this->m_mem_map.ptr = this->m_mem_map.end = nullptr;
  this->flags |= MAPPED;
  this->close();
  this->fd = -1;
  return 1;
}

/// Check file access
int Online::Datafile::access(int file_mode)  {
  return m_descriptor->access(this->path.c_str(), file_mode);
}

/// Write chunk of data
long Online::Datafile::write(const void* data, int len)  {
  if ( isOpen() )  {
    const uint8_t* p = (const uint8_t*) data;
    if ( len < 0 )  {
      errno = EINVAL;
      return -1;
    }
    int64_t tmp = 0;
    while (tmp < len)  {
      int64_t sc = m_descriptor->write(this->fd, p + tmp, len - tmp);
      if (sc > 0)
	tmp += sc;
      else if ( sc == 0 && errno == EINTR )
	continue;
      else if ( sc == 0 )
	break;
      else
	break;//return 0;
    }
    return tmp;
  }
  errno = EBADF;
  return -1;
}

/// Write chunk of data
long Online::Datafile::write(const void* data, long len)  {
  if ( isOpen() )  {
    const uint8_t* p = (const uint8_t*) data;
    if ( len < 0 )  {
      errno = EINVAL;
      return -1;
    }
    int64_t tmp = 0;
    while (tmp < len)  {
      int64_t sc = m_descriptor->write(this->fd, p + tmp, len - tmp);
      if (sc > 0)
	tmp += sc;
      else if ( sc == 0 && errno == EINTR )
	continue;
      else if ( sc == 0 )
	break;
      else
	break;//return 0;
    }
    return tmp;
  }
  errno = EBADF;
  return -1;
}

/// Write chunk of data
long Online::Datafile::write(const void* data, std::size_t len)  {
  if ( isOpen() )  {
    const uint8_t* p = (const uint8_t*) data;
    std::size_t tmp = 0;
    while (tmp < len)  {
      int sc = m_descriptor->write(this->fd, p + tmp, len - tmp);
      if (sc > 0)
	tmp += sc;
      else if ( sc == 0 && errno == EINTR )
	continue;
      else if ( sc == 0 )
	break;
      else
	break;//return 0;
    }
    return tmp;
  }
  errno = EBADF;
  return -1;
}

/// Read chunk of data
long Online::Datafile::read(void* pointer, long len) {
  uint8_t* p = (uint8_t*)pointer;
  if ( len < 0 )  {
    errno = EINVAL;
    return -1;
  }
  if ( isMapped() )  {
    if ( this->m_mem_map.ptr + len <= this->m_mem_map.end )  {
      ::memcpy(p, this->m_mem_map.ptr, len);
      this->m_mem_map.ptr += len;
      return len;
    }
    else if ( this->m_mem_map.ptr < this->m_mem_map.end )  {
      len = this->m_mem_map.end - this->m_mem_map.ptr;
      ::memcpy(p, this->m_mem_map.ptr, len);
      this->m_mem_map.ptr = this->m_mem_map.end;
      return len;
    }
    return -1;
  }
  if ( isOpen() )  {
    int64_t tmp = 0;
    while ( tmp < len )  {
      int64_t sc = m_descriptor->read(this->fd, p + tmp, len - tmp);
      if (sc > 0)
	tmp += sc;
      else if (sc == 0 && errno == EINTR)
	continue;
      else if (sc == 0)
	break;
      else  {
	auto err = RTL::errorString();
	::lib_rtl_output(LIB_RTL_DEBUG,"Datafile: OS READ error file: %s: [%s] position: %ld",
			 cname(), !err.empty() ? err.c_str() : "????????", this->position());	
	return tmp > 0 ? tmp : -1;
      }
    }
    return tmp;
  }
  errno = EBADF;
  return -1;
}

/// Unlink file
int Online::Datafile::unlink()  {
  int sc = m_descriptor->unlink(cname());
  if (sc != 0)     {
    int err_num = errno;
    auto err = RTL::errorString();
    ::lib_rtl_output(LIB_RTL_ERROR,"Datafile: CANNOT UNLINK file: %s: [%s]",
		     cname(), !err.empty() ? err.c_str() : "????????");
    switch(err_num)  {
    case ENOENT:
    case ENOTDIR:
    case EPERM:
    case EACCES:
    case EIO:
    case EROFS:
      return err_num;  // Bad disk ?
    case EFAULT:
    case EBUSY:
    case EISDIR:
    case ELOOP:
    case ENAMETOOLONG:
    case ENOMEM:
    default:
      // Standard stuff: for one or another reason, this file cannot be used.
      // Close it, skip and continue.
      ::lib_rtl_output(LIB_RTL_INFO,"Datafile: File:%s SKIPPED for processing.", cname());
      m_descriptor->close(this->fd);
      this->fd = -1;
      return 1;
    }
  }
  return sc;
}

int Online::Datafile::mkdir(const char* dir_name, int protection, bool silent)  {
  if ( dir_name && dir_name[0] )  { // Empty pathes always exist!
    std::error_code ec, ecexist;
    fs::path p = fs::path(dir_name);
    fs::create_directories(p, ec);
    if ( fs::exists(p, ecexist) )  {
      fs::perms prot = fs::perms::none;
      if ( protection&S_IRUSR ) prot |= fs::perms::owner_read;
      if ( protection&S_IWUSR ) prot |= fs::perms::owner_write;
      if ( protection&S_IXUSR ) prot |= fs::perms::owner_exec;
      if ( protection&S_IRGRP ) prot |= fs::perms::group_read;
      if ( protection&S_IWGRP ) prot |= fs::perms::group_write;
      if ( protection&S_IXGRP ) prot |= fs::perms::group_exec;
      if ( protection&S_IROTH ) prot |= fs::perms::others_read;
      if ( protection&S_IWOTH ) prot |= fs::perms::others_write;
      if ( protection&S_IXOTH ) prot |= fs::perms::others_exec;
      fs::permissions(p, prot, fs::perm_options::replace, ec);
      // Ignore if setting the permissions fail. We will anyhow notice later!
      return 0;
    }
    if ( !silent )  {
      ::lib_rtl_output(LIB_RTL_ERROR,"Datafile: CANNOT create directory %s: %d [%s]",
		       dir_name, ec.value(), ec.message().c_str());
    }
    return ec.value();
  }
  return 0;
}

int Online::Datafile::mkdir(const std::string& dir_name, int protection, bool silent)  {
  return Online::Datafile::mkdir(dir_name.c_str(), protection, silent);
}

int Online::Datafile::rmdir(const char* dir_name, bool recursive, bool silent)  {
  std::error_code ec;
  fs::path p = fs::path(dir_name);

  if ( !fs::exists(p, ec) )  {
    ec = std::make_error_code(std::errc(ENOENT));
    if ( !silent )  {
      ::lib_rtl_output(LIB_RTL_ERROR,"Datafile: CANNOT remove directory %s: %d [%s]",
		       dir_name, ec.value(), ec.message().c_str());
    }
    return ec.value();
  }
  else if ( !fs::is_directory(p,ec) )  {
    if ( !silent )  {
      ::lib_rtl_output(LIB_RTL_ERROR,"Datafile: Entry %s is no directory: %d [%s]",
		       dir_name, ec.value(), ec.message().c_str());
    }
    return ec.value();
  }
  else if ( recursive )  {
    fs::remove_all(p, ec);
    if ( ec )  {
      if ( !silent )  {
	::lib_rtl_output(LIB_RTL_ERROR,"Datafile: CANNOT remove directory %s: %d [%s]",
			 dir_name, ec.value(), ec.message().c_str());
      }
    }
    else if ( !silent )  {
      ::lib_rtl_output(LIB_RTL_ALWAYS,"Datafile: Remove directory %s", dir_name);
    }
    return ec.value();
  }
  auto ret = fs::remove(p, ec);
  if ( !ret || ec )  {
    if ( !silent )  {
      ::lib_rtl_output(LIB_RTL_ERROR,"Datafile: CANNOT remove directory %s: %d [%s]",
		       dir_name, ec.value(), ec.message().c_str());
    }
  }
  else if ( !silent )  {
    ::lib_rtl_output(LIB_RTL_ALWAYS,"Datafile: Remove directory %s", dir_name);
  }
  return ec.value();
}

int Online::Datafile::rmdir(const std::string& dir_name, bool recursive, bool silent)  {
  return Online::Datafile::rmdir(dir_name.c_str(), recursive, silent);
}
