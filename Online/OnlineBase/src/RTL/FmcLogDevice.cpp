//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================

/// Framework include files
#include <RTL/FmcLogDevice.h>

/// C/C++ include files
#include <cerrno>
#include <climits>
#include <cstring>
#include <stdexcept>
#include <system_error>

#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#define BUF_SZ 4096
#define NO_FIFO (-1)

using namespace std;
using namespace RTL;

namespace {
  /// Translate errno into error string
  inline const string cerror(int err)   {
    return make_error_code(errc(err)).message();
  }
}

/// Default constructor
FmcLogDevice::FmcLogDevice() : Logger::LogDevice()   {
  fifoFD         = NO_FIFO;
  allowDrop      = false;
  numRetry       = 2;
  reuse_handles  = true;
  outputLevel    = LIB_RTL_INFO;
  fifoFD  = STDOUT_FILENO;
  int flags = fcntl(fifoFD, F_GETFL, 0);
  ::fcntl(fifoFD, F_SETFL, flags | O_NONBLOCK);
  bytesDropped = 0;
  compileFormat("%TIME%LEVEL%-8NODE: %-32PROCESS %-20SOURCE");
}

/// Default constructor
FmcLogDevice::FmcLogDevice(const std::string& path) : Logger::LogDevice()   {
  const char* fifo = path.empty() ? ::lib_rtl_getenv("LOGFIFO") : path.c_str();
  fifoFD         = NO_FIFO;
  allowDrop      = false;
  numRetry       = 2;
  reuse_handles  = false;
  outputLevel    = LIB_RTL_INFO;
  // Alias to LogPath property of the OutputLogger
  logDevice   = fifo ? fifo : "/run/fmc/logSrv.fifo";
  fifoFD  = this->open(fifo);
  bytesDropped = 0;
  compileFormat("%TIME%LEVEL%-8NODE: %-32PROCESS %-20SOURCE");
}

/// Default destructor
FmcLogDevice::~FmcLogDevice()    {
  close();
}

/// Compile the format string
void FmcLogDevice::compileFormat(const std::string& fmt)   {
  this->Logger::LogDevice::compileFormat(fmt);
}      

/// Close fifo device
void FmcLogDevice::close()  const  {
  int fd = fifoFD;
  if ( fd && fd != NO_FIFO ) ::close(fd);
  fd = NO_FIFO;
}

/// Open fifo device. Returns file descriptor
int FmcLogDevice::open(const char* path)  const {
  if ( !path )  {
    this->except("FmcLogDevice",
		 "Fifo path cannot be dereferenced. Fatal error, check environment.");
    return NO_FIFO;
  }
  else if ( path && fifoFD != NO_FIFO && logDevice == path )  {
    return fifoFD;
  }
  else  {
    int fd=0;
    struct stat statBuf;
    enum outType{ L_DIM=0x1,L_STD=0x2,L_SYS=0x4};
    string fifo_path = path;
    const char* fifo = path;

    if ( fifo_path.length()>0 && fifo_path[0]=='$' )  {
      string fn = fifo_path.substr(1);
      fifo = ::lib_rtl_getenv(fn.c_str());
    }
    // check if fifo is writable 
    if( ::access(fifo,W_OK) != -1) {       /* write access to fifo OK */
      // get fifo information
      if( ::stat(fifo,&statBuf) != -1) {   /* fifo information got    */
	// check if fifo is a FIFO
	if( S_ISFIFO(statBuf.st_mode)) { /* fifo is a FIFO          */
	  // open fifo
	  fd = ::open(fifo,O_RDWR|O_NONBLOCK|O_APPEND);
	  if( fd != NO_FIFO )   {        /* fifo open() failed      */
	    ::close(fd);
	  }
	}
      }
    }
    /// check if fifo is writable
    if( ::access(fifo,W_OK) == -1 )    {              /* access denied */
      if( errno == ENOENT )    {
	this->except("FmcLogDevice", "Fifo path %s not writable [%s].",
		     fifo, cerror(errno).c_str());
      }
      this->except("FmcLogDevice", "Fifo path %s not writable. Check permissions.",
		   fifo, cerror(errno).c_str());
      return NO_FIFO;
    }
    if ( ::stat(fifo,&statBuf) == -1 )  {             /* get fifo info */
      this->except("FmcLogDevice", "Cannot stat fifo path %s. [%s]",
		   fifo, cerror(errno).c_str());
    }
    if( !S_ISFIFO(statBuf.st_mode) )  {     /* check if fifo is a FIFO */
      this->except("FmcLogDevice", "Fifo path %s is not a FIFO device. [%s]",
		   fifo, cerror(errno).c_str());
    }
    /* open error log */
    fd = ::open(fifo,(allowDrop ? O_WRONLY : O_RDWR)|O_NONBLOCK|O_APPEND);
    if ( fd == NO_FIFO )  {
      if ( errno == ENXIO )
	this->except("FmcLogDevice", "Fifo path %s not owned by any process. [%s]",
		     fifo, cerror(errno).c_str());
      this->except("FmcLogDevice", "Failed to open fifo:%s [%s]",
		   fifo, cerror(errno).c_str());
    }
    /*-------------------------------------------------------------------------*/
    if ( allowDrop )  {
      /* Now we are sure that another process has the FIFO open for reading.   */
      /* We unset now the O_NONBLOCK bit to have blocking write (no-drop       */
      /* behaviour).                                                           */
      int status = ::fcntl(fd, F_GETFL);
      if(status<0)    {
	::close(fd);
	this->except("FmcLogDevice", "Cannot fcntl fifo path %s. [%s]",
		     fifo, cerror(errno).c_str());
      }
      status &= ~O_NONBLOCK;                         /* unset O_NONBLOCK bit */
      if ( ::fcntl(fd, F_SETFL,status) == -1 )    {
	::close(fd);
	this->except("FmcLogDevice", "Cannot set O_NONBLOCK bit of fifo %s. [%s]",
		     fifo, cerror(errno).c_str());
      }
    }
    return fd;
  }
}

/// Calls the display action with a given severity level
size_t FmcLogDevice::printmsg(int severity, const char* source, const char* msg)  const  {
  return print_record(false, severity, source, msg);
}

/// Error handling. Throws exception
void FmcLogDevice::exceptmsg(const char* source, const char* msg) const    {
  print_record(true, LIB_RTL_ERROR, source, msg);
}

/// Write output .....
size_t FmcLogDevice::print_record(bool exc, int severity, const char* source, const char* text)  const {
  char header[BUF_SZ];
  if ( severity > LIB_RTL_ALWAYS   ) severity = LIB_RTL_ALWAYS;
  if ( severity < LIB_RTL_VERBOSE  ) severity = LIB_RTL_VERBOSE;
  if ( severity < outputLevel )  {
    return 0;
  }
  size_t hdr_len = formatHeader(header, BUF_SZ/2, severity, source);
  size_t bufAvailLen = BUF_SZ-2-hdr_len;                         /* -2: \n\0 */
  /*-------------------------------------------------------------------------*/
  int fd = fifoFD;
  if ( !fifoFD || fifoFD == NO_FIFO )  {
    fd = ::fileno(stderr);
  }
  string msg, stext(text);
  for(size_t i=0; i*bufAvailLen < stext.length(); i++)  {
    struct timespec delay = {0,1000000};                        /* 0.001 s */
    msg = header + stext.substr(i*bufAvailLen,bufAvailLen)+"\n";
    int tryC = 0;
    for ( tryC=0; tryC < numRetry; tryC++ )    {
      int written = write(fd,msg.c_str(), msg.length());
      if ( written != -1 || errno != EAGAIN ) break;
      nanosleep(&delay,NULL);
      delay.tv_sec *= 2;
      delay.tv_nsec *= 2;
      if(delay.tv_nsec>999999999){
	delay.tv_sec+=1;
	delay.tv_nsec-=1000000000;
      }
    }
    if ( tryC == numRetry )    {
      bytesDropped++;
    }
  }
  //write(fd,"\n",1);
  if ( exc )   {
    throw runtime_error(string(header)+" "+msg);
  }
  return hdr_len+stext.length();
}

