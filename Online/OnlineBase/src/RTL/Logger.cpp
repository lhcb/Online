//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================

/// Framework include files
#include "RTL/Logger.h"
#include "RTL/strdef.h"

/// C/C++ include files
#include <stdexcept>
#include <ctime>
#include <climits>
#include <cstring>
#include <unistd.h>

using namespace std;
using namespace RTL;

#define FMC_TIME_FORMAT "%b%d-%H%M%S"

namespace {

  shared_ptr<Logger::LogDevice> s__dev;

  /// Helper function to steer printing
  bool may_print(const Logger* c, int level)  {
    return level >= c->outputLevel;
  }
  size_t rtl_print(void*, int lvl, const char* fmt, va_list& args) {
    return s__dev->printout(lvl, "RTL", fmt, args);
  }
}

/// Default constructor
Logger::LogDevice::LogDevice() : device_lock(new mutex())  {
  char pathName[PATH_MAX] = "";
  int len = ::readlink("/proc/self/exe",pathName,sizeof(pathName)-1);
  if ( len == -1 )  {
    string err = make_error_code(errc(errno)).message();
    this->except("LogDevice", "Output: readlink(): %s.",err.c_str());
  }
  pathName[len]='\0';
  executable = ::basename(pathName);
  size_t idx = executable.find(".exe");
  if ( idx != string::npos ) executable[idx] = 0;
  compileFormat(format);
  set_io_buffering(Logger::LINE_BUFFERING);
}

/// Default destructor
Logger::LogDevice::~LogDevice()   {
  delete device_lock;
  device_lock = nullptr;
}

/// Set global output device
void Logger::LogDevice::setGlobalDevice(shared_ptr<LogDevice> dev, int level)    {
  s__dev = dev;
  ::lib_rtl_install_printer(rtl_print,s__dev.get());
  ::lib_rtl_set_log_level(level);
}

/// Set global output device
void Logger::LogDevice::setGlobalDevice(shared_ptr<LogDevice> dev, const std::string& printing)    {
  int print_level = LIB_RTL_INFO;
  switch(::toupper(printing[0]))  {
  case '7':
  case 'A':  print_level=LIB_RTL_ALWAYS;   break;
  case '6':
  case 'F':  print_level=LIB_RTL_FATAL;    break;
  case '5':
  case 'E':  print_level=LIB_RTL_ERROR;    break;
  case '4':
  case 'W':  print_level=LIB_RTL_WARNING;  break;
  case '3':
  case 'I':  print_level=LIB_RTL_INFO;     break;
  case '2':
  case 'D':  print_level=LIB_RTL_DEBUG;    break;
  case '1':
  case 'V':  print_level=LIB_RTL_VERBOSE;  break;
  default:
    throw std::runtime_error("LogDevice::setGlobalDevice: Unknown printout level "+printing);
    break;
  }
  setGlobalDevice(dev, print_level);
}

/// Set IO buffering mode. See "man setvbuf" for details
void Logger::LogDevice::set_io_buffering(size_t mode)    {
  size_t buff_size = 1024;
  int buff_mode = _IOLBF;
  switch(mode)    {
  case NO_BUFFERING:
    buff_mode = _IONBF;
    buff_size = 0;
    break;
  case FULL_BUFFERING:
    buff_mode = _IOFBF;
    buff_size = 8192;
    break;
  case LINE_BUFFERING:
    buff_mode = _IOLBF;
    buff_size = 1024;
    break;
  default:
    buff_mode = _IOFBF;
    buff_size = mode;
    break;
  }
  /// Set stdout to line - buffered mode
  if ( 0 != ::setvbuf(stdout, nullptr, buff_mode, buff_size) )    {
    string err = make_error_code(errc(errno)).message();
    this->except("LogDevice",
		 "Output: Failed to set stdout to line buffered mode: %s.",
		 err.c_str());
  }
  /// Set stderr to line - buffered mode
  if ( 0 != ::setvbuf(stderr, nullptr, buff_mode, buff_size) )    {
    string err = make_error_code(errc(errno)).message();
    this->except("LogDevice",
		 "Output: Failed to set stderr to line buffered mode: %s.",
		 err.c_str());
  }
}

/// Access global output device
shared_ptr<Logger::LogDevice> Logger::LogDevice::getGlobalDevice()    {
  if ( !s__dev.get() )  {
    s__dev = make_shared<LogDevice>();
  }
  return s__dev;
}

/// Check if a global output device exists
bool Logger::LogDevice::existsGlobalDevice()    {
  return s__dev.get() != 0;
}

/// Install as global logger for RTL
void Logger::LogDevice::install_rtl_printer(int level)    {
  ::lib_rtl_install_printer(rtl_print,getGlobalDevice().get());
  ::lib_rtl_set_log_level(level);
}

/// Extract format item during compilation
pair<size_t,string> Logger::LogDevice::extract_format(const string& tag)    {
  size_t idx, start, end;
  if ( (idx=format.find(tag)) != string::npos )  {
    format = RTL::str_replace(format, tag, "s");
    start = format.rfind('%',idx);
    for(end = idx + 1; format[end] && format[end]!='%';) ++end;
    return make_pair(idx,format.substr(start,end-start));
  }
  return make_pair(string::npos,format);
}

/// Compile the format string
void Logger::LogDevice::compileFormat(const string& fmt)  {
  string tmp;
  pair<size_t,string> item;
  if ( !fmt.empty() ) format = fmt;
  formatItems.clear();

  // First order format statements according to their occurrence
  tmp = format;
  while( (item=extract_format("TIME")).first != string::npos )
    formatItems[item.first] = make_pair(TIME_FORMAT, item.second);
  format = tmp;
  while( (item=extract_format("NODE")).first != string::npos )
    formatItems[item.first] = make_pair(NODE_FORMAT, item.second);
  format = tmp;
  while( (item=extract_format("LEVEL")).first != string::npos )
    formatItems[item.first] = make_pair(LEVEL_FORMAT, item.second);
  format = tmp;
  while( (item=extract_format("PROCESS")).first != string::npos )
    formatItems[item.first] = make_pair(PROCESS_FORMAT, item.second);
  format = tmp;
  while( (item=extract_format("UTGID")).first != string::npos )
    formatItems[item.first] = make_pair(PROCESS_FORMAT, item.second);
  format = tmp;
  while( (item=extract_format("SOURCE")).first != string::npos )
    formatItems[item.first] = make_pair(SOURCE_FORMAT, item.second);
  format = tmp;
  while( (item=extract_format("EXEC")).first != string::npos )
    formatItems[item.first] = make_pair(EXEC_FORMAT, item.second);

  // Now reformat the formatting string
  format = tmp;
  while( (item=extract_format("TIME")).first != string::npos );
  while( (item=extract_format("NODE")).first != string::npos );
  while( (item=extract_format("LEVEL")).first != string::npos );
  while( (item=extract_format("PROCESS")).first != string::npos );
  while( (item=extract_format("UTGID")).first != string::npos );
  while( (item=extract_format("SOURCE")).first != string::npos );
  while( (item=extract_format("EXEC")).first != string::npos );
}

/// Format header string according to precompiled format
size_t Logger::LogDevice::formatHeader(char* buffer, size_t buff_len, int severity, const char* source)   const   {
  char temp[32];
  int lvl = severity;
  const char* p_lvl = "?????";
  buffer[0] = 0;
  if ( lvl> LIB_RTL_ALWAYS  ) lvl = LIB_RTL_ALWAYS;
  if ( lvl< LIB_RTL_VERBOSE ) lvl = LIB_RTL_VERBOSE;
  switch(lvl)   {
  case LIB_RTL_VERBOSE:   p_lvl = "VERB  "; break;
  case LIB_RTL_DEBUG:     p_lvl = "DEBUG "; break;
  case LIB_RTL_INFO:      p_lvl = "INFO  "; break;
  case LIB_RTL_WARNING:   p_lvl = "WARN  "; break;
  case LIB_RTL_ERROR:     p_lvl = "ERROR "; break;
  case LIB_RTL_FATAL:     p_lvl = "FATAL "; break;
  case LIB_RTL_ALWAYS:    p_lvl = "INFO  "; break;
  default:                p_lvl = "INFO  "; break;
  }
  time_t now;
  int hdr_len = 0;
  for(const auto& item : formatItems)   {
    switch(item.second.first)  {
    case TIME_FORMAT:
      now = ::time(0);
      ::lib_rtl_timestr_r(temp,sizeof(temp),FMC_TIME_FORMAT,&now);
      hdr_len += ::snprintf(buffer+hdr_len,buff_len-2-hdr_len, item.second.second.c_str(), temp);
      break;
    case NODE_FORMAT:
      hdr_len += ::snprintf(buffer+hdr_len,buff_len-2-hdr_len, item.second.second.c_str(), RTL::nodeNameShort().c_str());
      break;
    case LEVEL_FORMAT:
      hdr_len += ::snprintf(buffer+hdr_len,buff_len-2-hdr_len, item.second.second.c_str(), p_lvl);
      break;
    case PROCESS_FORMAT:
      hdr_len += ::snprintf(buffer+hdr_len,buff_len-2-hdr_len, item.second.second.c_str(), RTL::processName().c_str());
      break;
    case SOURCE_FORMAT:
      hdr_len += ::snprintf(buffer+hdr_len,buff_len-2-hdr_len, item.second.second.c_str(), source);
      break;
    case EXEC_FORMAT:
      hdr_len += ::snprintf(buffer+hdr_len,buff_len-2-hdr_len, item.second.second.c_str(), executable.c_str());
      break;
    default:
      break;
    }
  }
  buffer[hdr_len] = ' ';
  buffer[hdr_len+1] = 0;
  hdr_len += 1;
  return hdr_len;
}

/// Calls the display action with a given severity level
size_t Logger::LogDevice::printmsg(int severity, const char* source, const char* msg)  const  {
  if ( severity >= ::lib_rtl_log_level() )   {
    char text[4096], header[2048];
    size_t len1 = formatHeader(header, sizeof(header), severity&0xFF, source);
    size_t len2 = ::strlen(msg);
    if ( len1+len2+3 < sizeof(text) )  {
      ::memcpy(text, header, len1);
      text[len1] = ' ';
      ::memcpy(text+len1+1, msg, len2);
      size_t len = len1+len2+1;
      if ( msg[len2-1] != '\n' && msg[len2-2] != '\n' )  {
	text[len] = '\n';
	++len;
      }
      {
	lock_guard<mutex> lock(*device_lock);
	return ::write(STDOUT_FILENO, text, len);
      }
    }
  }
  return 0; // '1' from fputc
}

/// Calls the display action with a given severity level
size_t Logger::LogDevice::printout(int severity, const char* source, const char* fmt, va_list& args)  const  {
  if ( severity >= ::lib_rtl_log_level() )   {
    char text[4096];
    ::vsnprintf(text, sizeof(text), fmt, args);
    text[sizeof(text)-1] = 0;
    va_end(args);
    return printmsg(severity, source, text);
  }
  va_end(args);
  return 0;
}

/// Calls the display action with a given severity level
size_t Logger::LogDevice::printout(int severity, const char* source, const char* fmt, ...)  const  {
  if ( severity >= ::lib_rtl_log_level() )   {
    va_list args;
    va_start(args, fmt);
    return printout(severity, source, fmt, args);
  }
  return 0;
}

/// Throw exception with message
void Logger::LogDevice::exceptmsg(const char* source, const char* msg)  const  {
  char header[2048];
  formatHeader(header, sizeof(header), LIB_RTL_ERROR, source);   {
    lock_guard<mutex> lock(*device_lock);
    ::fprintf(stdout, "%s %s\n", header, msg);
    ::fflush(stdout);
  }
  throw runtime_error(header+string(" ")+msg);
}

/// Throw exception with message
void Logger::LogDevice::except(const char* source, const char* fmt, va_list& args)  const  {
  char text[4096];
  ::snprintf(text, sizeof(text), fmt, args);
  text[sizeof(text)-1] = 0;
  va_end(args);
  exceptmsg(source, text);
}

/// Error handling. Throws exception
void Logger::LogDevice::except(const char* source, const char* fmt, ...) const   {
  va_list args;
  va_start(args, fmt);
  except(source, fmt, args);
}

/// Default constructor
Logger::Logger(shared_ptr<LogDevice>& dev, const string& src, int lvl)
  : device(dev), source(src), outputLevel(lvl)
{
}

/// Default constructor
Logger::Logger(shared_ptr<LogDevice>&& dev, const string& src, int lvl)
  : device(dev), source(src), outputLevel(lvl)
{
}

/// Set IO buffering mode for stdout, stderr. See LogDevice for details
void Logger::set_io_buffering(size_t mode)    {
  getGlobalDevice()->set_io_buffering(mode);
}

/// Set global output device
void Logger::setGlobalDevice(shared_ptr<LogDevice> dev, int level)    {
  LogDevice::setGlobalDevice(dev, level);
}

/// Access global output device
shared_ptr<Logger::LogDevice> Logger::getGlobalDevice()    {
  return LogDevice::getGlobalDevice();
}

/// Install as global logger for RTL
void Logger::install_rtl_printer(int level)    {
  LogDevice::install_rtl_printer(level);
}
#if 0
/// All in one initialization if fifo string is not empty
void Logger::install_fifolog(const char*)    {
  Logger::install_log(log_args(LIB_RTL_WARNING));
}

/// All in one initialization if fifo string is not empty
void Logger::install_fifolog(const char*, const log_args& args)    {
  Logger::install_log(args);
}
#endif
/// All in one initialization if fifo string is not empty
void Logger::install_log(const log_args& args)    {
  if ( args.debug )
    install_rtl_printer(LIB_RTL_DEBUG);
  else if ( args.level > 0 )
    install_rtl_printer(args.level);
  else
    install_rtl_printer(LIB_RTL_INFO);
  getGlobalDevice()->compileFormat(args.fmt.c_str());
  getGlobalDevice()->set_io_buffering(LINE_BUFFERING);
  getGlobalDevice()->printout(LIB_RTL_DEBUG,"RTL","Initialized logger");
}

/// Print startup message
void Logger::print_startup(const char* fmt, ...)  {
  Logger log(LogDevice::getGlobalDevice(), RTL::processName(), LIB_RTL_VERBOSE);
  va_list args;
  va_start(args, fmt);
  string msg = log.format(fmt, args);
  log.always("%s started on host:%s", msg.c_str(), RTL::nodeNameShort().c_str());
}

/// Helper function to steer printing. Returns true if logging is enabled for level
bool Logger::do_print(int level)  const  {
  return level >= this->outputLevel;
}

/// Clone the logger
unique_ptr<Logger> Logger::clone(const string& src, int lvl)  {
  shared_ptr<LogDevice> dev = device;
  return make_unique<Logger>(dev, src, lvl);
}

/// Format output string
string Logger::format(const char* fmt, ...)  {
  va_list args;
  va_start(args, fmt);
  return format(fmt, args);
}

/// Format output string
string Logger::format(const char* fmt, va_list& args)  {
  char text[4096];
  ::vsnprintf(text, sizeof(text), fmt, args);
  text[sizeof(text)-1] = 0;
  va_end(args);
  return text;
}

/// Calls the display action with a given severity level
size_t Logger::printmsg(int severity, const char* src, const char* msg)  const   {
  return device->printmsg(severity, src, msg);
}

/// Calls the display action with a given severity level
size_t Logger::printout(int severity, const char* fmt, va_list& args)  const   {
  return device->printout(severity, source.c_str(), fmt, args);
}

/// Exception throwing. Technically returns error code. Throws internally exception
void Logger::except(const char* fmt, ...) const    {
  va_list args;
  va_start(args, fmt);
  return except(fmt, args);
}

/// Calls the display action with ERROR and throws an runtime_error exception
void Logger::except(const char* fmt, va_list& args)  const   {
  device->except(source.c_str(), fmt, args);
}

/// Always printout handling
void Logger::always(const char* fmt,...) const   {
  va_list args;
  va_start(args, fmt);
  printout(LIB_RTL_ALWAYS, fmt, args);
}

/// Always printout handling
void Logger::always(const char* fmt, va_list& args) const   {
  printout(LIB_RTL_ALWAYS, fmt, args);
}

/// Debug printout handling
void Logger::debug(const char* fmt,...) const   {
  if ( may_print(this,LIB_RTL_DEBUG) )  {
    va_list args;
    va_start(args, fmt);
    printout(LIB_RTL_DEBUG, fmt, args);
  }
}

/// Debug printout handling
void Logger::debug(const char* fmt, va_list& args) const   {
  if ( may_print(this,LIB_RTL_DEBUG) )  {
    printout(LIB_RTL_DEBUG, fmt, args);
  }
}

/// Info printout handling
void Logger::info(const char* fmt, ...) const   {
  if ( may_print(this,LIB_RTL_INFO) )  {
    va_list args;
    va_start(args, fmt);
    printout(LIB_RTL_INFO, fmt, args);
  }
}

/// Info printout handling
void Logger::info(const char* fmt, va_list& args) const  {
  if ( may_print(this,LIB_RTL_INFO) )  {
    printout(LIB_RTL_INFO, fmt, args);
  }
}

/// Warning printout handling
void Logger::warning(const char* fmt, ...) const   {
  if ( may_print(this,LIB_RTL_WARNING) )  {
    va_list args;
    va_start(args, fmt);
    printout(LIB_RTL_WARNING, fmt, args);
  }
}

/// Warning printout handling
void Logger::warning(const char* fmt, va_list& args) const   {
  if ( may_print(this,LIB_RTL_WARNING) )  {
    printout(LIB_RTL_WARNING, fmt, args);
  }
}

/// Error handling. Returns error code
int Logger::error(const char* fmt, ...) const   {
  if ( may_print(this,LIB_RTL_ERROR) )  {
    va_list args;
    va_start(args, fmt);
    printout(LIB_RTL_ERROR, fmt, args);
  }
  return LIB_RTL_ERROR;
}

/// Error handling. Technically returns error code. Throws internally exception
int Logger::error(const char* fmt, va_list& args) const   {
  if ( may_print(this,LIB_RTL_ERROR) )  {
    printout(LIB_RTL_ERROR, fmt, args);
  }
  return LIB_RTL_ERROR;
}

/// Error handling: print exception. Technically returns error code.
int Logger::error(const exception& e, const char* msg, ...) const   {
  if ( may_print(this,LIB_RTL_ERROR) )  {
    string fmt = "Exception: ";
    fmt += msg;
    fmt += " [";
    fmt += e.what();
    fmt += " ]";
    va_list args;
    va_start(args, msg);
    printout(LIB_RTL_ERROR, fmt.c_str(), args);
  }
  return LIB_RTL_ERROR;
}

/// Fatal handling. Technically returns error code. Throws internally exception
int Logger::fatal(const char* msg, va_list& args) const   {
  if ( may_print(this,LIB_RTL_FATAL) )  {
    printout(LIB_RTL_FATAL, msg, args);
  }
  return LIB_RTL_ERROR;
}

/// Fatal handling. Returns error code
int Logger::fatal(const char* msg, ...) const   {
  if ( may_print(this,LIB_RTL_FATAL) )  {
    va_list args;
    va_start(args, msg);
    printout(LIB_RTL_FATAL, msg, args);
  }
  return LIB_RTL_ERROR;
}

/// Error handling. Technically returns error code. Throws internally exception
int Logger::throwError(const string& msg) const   {
  error(msg.c_str());
  throw runtime_error(msg);
  return LIB_RTL_ERROR;
}

/// Error handling. Technically returns error code. Throws internally exception
int Logger::throwError(const char* msg,...) const   {
  va_list args;
  va_start(args, msg);
  except(msg, args);
  return LIB_RTL_ERROR;
}

/// Error handling. Technically returns error code. Throws internally exception
int Logger::throwError(const char* msg, va_list& args) const   {
  except(msg, args);
  return LIB_RTL_ERROR;
}

/// Generic output logging
void Logger::output(int level, const char* msg,...)   const   {
  if ( may_print(this,level) )  {
    va_list args;
    va_start(args, msg);
    printout(level, msg, args);
  }
}

