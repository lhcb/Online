//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : OnlineKernel
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include "RTL/Process.h"
#include "RTL/Logger.h"
#include <RTL/strdef.h>
#include <RTL/rtl.h>

// C/C++ include files
#include <cstdio>
#include <csignal>
#include <iostream>
#include <sstream>
#include <climits>
#include <cstring>
#include <cerrno>
#include <filesystem>

#ifdef _WIN32
#define SIGKILL SIGTERM
#else
#include <sys/wait.h>
#endif
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/prctl.h>

namespace {
  bool s_debug = false;
}

/// Standard constructor
RTL::Process::Process() : m_pid(-1), m_status(0), m_state(INVALID)  {
}

/// Standard constructor
RTL::Process::Process(int id) : m_pid(id), m_status(0), m_state(id<=0 ? INVALID : RUNNING)  {
}

/// Constructor to start new process
RTL::Process::Process(const std::string& name, const std::string& exe, const char* a[])
  : m_pid(-1), m_status(0), m_state(INVALID), m_name(name), m_exec(exe)
{
  for(size_t i=0; a[i] != 0; ++i)
    m_args.push_back(a[i]);
}

/// Constructor to start new process
RTL::Process::Process(const std::string& name, const std::string& exe, const char* a[], const std::string& o)
  : m_pid(-1), m_status(0), m_state(INVALID), m_name(name), m_exec(exe), m_output(o)
{
  for(size_t i=0; a[i] != 0; ++i)
    m_args.push_back(a[i]);
}

/// Constructor to start new process
RTL::Process::Process(const std::string& name, const std::string& exe, const char* a[], const std::string& o, const std::string& in)
  : m_pid(-1), m_status(0), m_state(INVALID), m_name(name), m_exec(exe), m_input(in), m_output(o)
{
  for(size_t i=0; a[i] != 0; ++i)
    m_args.push_back(a[i]);
}

/// Constructor to start new process
RTL::Process::Process(const std::string& name, const std::string& exe, const Arguments& a) 
  : m_pid(-1), m_status(0), m_state(INVALID), m_name(name), m_exec(exe), m_args(a)
{
}

/// Constructor to start new process
RTL::Process::Process(const std::string& name, const std::string& exe, const Arguments& a, const std::string& o) 
  : m_pid(-1), m_status(0), m_state(INVALID), m_name(name), m_exec(exe), m_output(o), m_args(a)
{
}

/// Constructor to start new process
RTL::Process::Process(const std::string& name, const std::string& exe, const Arguments& a, const std::string& o, const std::string& in) 
  : m_pid(-1), m_status(0), m_state(INVALID), m_name(name), m_exec(exe), m_input(in), m_output(o), m_args(a)
{
}

/// Standard destructor
RTL::Process::~Process()    {
  if ( m_pid > 0 )    {
    if ( !this->is_running() )   {
      this->wait_for(100);
    }
  }
}

/// Update environment variables
void RTL::Process::updateEnvironment(const std::string& key, const std::string& value)   {
  // std::cout << "RTL::Process::updateEnvironment: " << key << " = " << value << std::endl;
  m_environ[key] = value;
}

/// Update environment variables
void RTL::Process::updateEnvironment(const Environment& env)   {
  for(const auto& e : env)     {
    updateEnvironment(e.first, e.second);
  }
}

/// String representation of process
std::string  RTL::Process::to_string()  const   {
  std::stringstream str;
  str << m_name << "[" << m_exec << ", pid:" << m_pid << "]" << "(";
  for(std::size_t i=0, n=m_args.size(); i<n; ++i)  {
    str << m_args[i];
    if (i+1 < n) str << ", ";
  }
  str << ")  logs:" << m_output;
  return str.str();
}

/// String representation of process
std::string RTL::Process::bash_command()  const   {
  std::stringstream str;
  if ( !m_name.empty() )
    str << "UTGID=" << m_name << " exec -a " << m_name << " ";
  str << m_exec << " ";
  for(std::size_t i=0, n=m_args.size(); i<n; ++i)  {
    str << m_args[i];
    if (i+1 < n) str << " ";
  }
  return str.str();
}

/// Debugging flag
void RTL::Process::setDebug(bool val) {
  s_debug = val;
}

/// Access to debugging flag
bool RTL::Process::debug() {
  return s_debug;
}

/// Check if process is running
int RTL::Process::is_running() const  {
#ifdef __linux
  if ( m_pid > 0 ) {
    int ret = ::kill(m_pid, 0);
    if ( ret == 0 ) {
      return 1;
    }
  }
  return 0;
#else
  return 0;
#endif
}

/// Check if the process is running. If not restart 'restarts' times.
int RTL::Process::ensure(int restarts, int millis_between)    {
  for( int i=0; i < restarts; ++i )   {
    if ( this->is_running() ) return 1;
    if ( this->m_pid > 0 ) this->wait(WAIT_NOBLOCK);
    this->m_state = INVALID;
    this->m_pid = -1;
    ::lib_rtl_output(LIB_RTL_ALWAYS,
		     "Process::ensure [%s]: Restart Number: %d",
		     m_name.c_str(), i);
    this->start(false);
    ::lib_rtl_sleep(millis_between);
    if ( this->is_running() ) return 1;
  }
  return this->is_running() ? 1 : 0;
}

/// Send signal to process specified by m_pid
int RTL::Process::sendSignal(int sig) {
#ifdef __linux
  if ( m_pid >= 0 ) {
    int ret = ::kill(m_pid, sig);
    if ( ret == 0 ) {
      return 1;
    }
  }
  return INVALID;
#else
  return 0;
#endif
}

/// Send signal to process specified by m_pid
int RTL::Process::sendSignalAll(int sig) {
#ifdef __linux
  if ( m_pid >= 0 ) {
    ::lib_rtl_output(LIB_RTL_DEBUG,"Send signal %d to %d\n",sig,int(-m_pid));
    int ret = ::kill(-m_pid, sig);
    if ( ret == 0 ) {
      return 1;
    }
  }
  return INVALID;
#else
  return 0;
#endif
}

/// Start process. Will NOT add new processes to new process group.
int RTL::Process::start()    {
  return start(false);
}

/// Attach input device to process
void RTL::Process::attach_input(const std::string& input)  {
  if ( !this->is_limbo() )   {
    throw std::runtime_error("+++ RTL::Process::attachInput("+m_name+
			     "): modifying the input device is only possible "
			     "if the process was not yet started!");
  }
  if ( !input.empty() ) {
    char pathn[PATH_MAX];
    std::string in = RTL::str_expand_env(input);
    if ( 0 != ::realpath(in.c_str(), pathn) )  {
      ::strncpy(pathn, in.c_str(), sizeof(pathn)-1);
      pathn[sizeof(pathn)-1] = 0;
    }
    int fd = ::open(pathn, O_RDONLY|O_LARGEFILE);
    ::lib_rtl_sleep(10);
    if ( fd == -1 )  {
      ::lib_rtl_output(LIB_RTL_ERROR, "Child: %s FAILED to open input file: %s [%s]",
		       m_name.c_str(), pathn, ::strerror(errno));
    }
    else  {
      ::lib_rtl_output(LIB_RTL_DEBUG, "Child: %s input redirected from: %s",
		       m_name.c_str(), pathn);
      ::close(STDIN_FILENO);
      ::dup2(fd, STDIN_FILENO);
    }      
  }
}

/// Attach output device to process
void RTL::Process::attach_output(const std::string& output)  {
  bool have_output = false;
  if ( !this->is_limbo() )   {
    throw std::runtime_error("+++ RTL::Process::attachOutput("+m_name+
			     "): modifying the output device is only possible "
			     "if the process was not yet started!");
  }
  if ( output == "stdout" )   {
    ::lib_rtl_output(LIB_RTL_DEBUG, "Child: %s output file: %s",
		     m_name.c_str(), output.c_str());
    ::close(STDERR_FILENO);
    ::dup2(STDOUT_FILENO, STDERR_FILENO);
    have_output = true;
  }
  else if ( output == "stderr" )   {
    ::lib_rtl_output(LIB_RTL_DEBUG, "Child: %s output file: %s",
		     m_name.c_str(), output.c_str());
    ::close(STDOUT_FILENO);
    ::dup2(STDERR_FILENO, STDOUT_FILENO);
    have_output = true;
  }
  else if ( !output.empty() ) {
    char pathn[PATH_MAX];
    struct stat stat_buff;
    std::string out = RTL::str_expand_env(output);
    if ( 0 != ::realpath(out.c_str(), pathn) )  {
      ::strncpy(pathn, output.c_str(), sizeof(pathn)-1);
      pathn[sizeof(pathn)-1] = 0;
    }
    if ( 0 == ::stat(pathn, &stat_buff) )  {
      ::unlink(pathn);
    }
    int fd = ::open(pathn, O_WRONLY|O_CREAT|O_LARGEFILE|O_TRUNC, S_IRWXU|S_IRWXG);
    ::lib_rtl_sleep(10);
    if ( fd == -1 )  {
      ::lib_rtl_output(LIB_RTL_ERROR, "Child: %s FAILED to open output file: %s [%s]",
		       m_name.c_str(), pathn, ::strerror(errno));
    }
    else   {
      ::lib_rtl_output(LIB_RTL_DEBUG, "Child: %s output file: %s",
		       m_name.c_str(), pathn);
      ::close(STDERR_FILENO);
      ::dup2(fd, STDERR_FILENO);
      ::close(STDOUT_FILENO);
      ::dup2(fd, STDOUT_FILENO);
    }
    have_output = true;
  }
  if ( have_output )   {
    RTL::Logger::set_io_buffering(RTL::Logger::LINE_BUFFERING);
  }
}

/// Set working directory
void RTL::Process::set_working_directory(const std::string& workdir)  {
  std::error_code ec;
  if ( !workdir.empty() )   {
    auto wd = std::filesystem::path(RTL::str_expand_env(workdir));
    if ( !std::filesystem::exists(wd, ec) )   {
      ::lib_rtl_output(LIB_RTL_INFO,
		       "+++ Desired working directory does not exist: %s [%d: %s]",
		       wd.c_str(), ec.value(), ec.message().c_str());
    }
    std::filesystem::current_path(wd, ec);
    if ( ec )  {
      ::lib_rtl_output(LIB_RTL_ERROR,
		       "+++ FAILED to change to desired working directory: %s [%d: %s]",
		       wd.c_str(), ec.value(), ec.message().c_str());
    }
    ::lib_rtl_output(LIB_RTL_DEBUG,"+++ Working directory: %s", wd.c_str());
    ::fflush(stdout);
  }
}

/// Setup the processes arguments ready for calling execve
char** RTL::Process::setup_arguments()   {
  char **arg = new char*[m_args.size()+2];
  arg[0] = ::strdup(m_name.c_str());
  for(size_t i=0; i<m_args.size(); ++i)
    arg[i+1] = ::strdup(m_args[i].c_str());
  arg[m_args.size()+1] = 0;
  return arg;
}

/// Setup the processes environment ready for calling execve
char** RTL::Process::setup_environment()   {
  int cnt = 0;
  char **e, **env;

  m_environ["UTGID"] = m_name;
  m_environ["RTL_OUTPUT_BUFFERING"] = "RTL_LINE_BUFFERING";
  for(cnt=0, e=environ; *e; ++e) ++cnt;
  env = new char*[cnt+m_environ.size()+2];

  cnt = 0;
  for(const auto& ev : m_environ)   {
    std::string v = ev.first + "=" + ev.second;
    env[cnt++] = ::strdup(v.c_str());
  }
  for(e=environ; *e; ++e)  {
    bool fnd = false;
    for(const auto& ev : m_environ)   {
      std::string n = ev.first + "=";
      if ( *e == ::strstr(*e, n.c_str()) )  {
	fnd = true;
	break;
      }
    }
    if ( !fnd )  {
      env[cnt++] = ::strdup(*e);
    }
  }
  env[cnt++] = 0;
#if 0
  ::lib_rtl_output(LIB_RTL_INFO,"+++ Process environment:");
  for(e=env; *e; ++e)  {
    if ( strncmp(*e,"DIM",3) == 0)
      ::lib_rtl_output(LIB_RTL_INFO,"+++ %s", *e);
  }
#endif
  return env;
}

/// Start process
int RTL::Process::start(bool new_process_group)    {
  if ( m_pid >= 0 || m_state != INVALID ) {
    return 0;
  }
#ifdef __linux
  auto ppid = ::getpid();
  m_pid = ::fork();
  if (m_pid == 0)  {   // child
    std::string utgid;
    struct stat stat_buff;
    //
    if ( m_parent_death_sig != 0 )   {
      if ( ::getppid() != ppid )   {
	::lib_rtl_output(LIB_RTL_ERROR,
			 "RTL::Process::start: Invalid parent pid: %d <> %d",
			 ::getppid(), ppid);
      }
      if ( -1 == ::prctl(PR_SET_PDEATHSIG, m_parent_death_sig) )   {
	::lib_rtl_output(LIB_RTL_ERROR,
			 "++ Failed to inherit parent death signal! [%s]",
			 RTL::errorString(errno).c_str());
      }
    }
    ::lib_rtl_install_printer(0,0);
    if ( Process::debug() ) {
      std::stringstream str;
      ::lib_rtl_sleep(10);
      str.str("");
      str << "Child: Starting process:" << m_name;
      if ( !m_output.empty() ) str << " output redirected to:" << m_output;
      if ( !m_input.empty()  ) str << " input redirected from:" << m_input;
      str << std::endl;
      ::lib_rtl_output(LIB_RTL_INFO, str.str().c_str());
    }
    attach_output(m_output);
    attach_input(m_input);
    set_working_directory(this->working_dir);
    std::filesystem::path exe = RTL::str_expand_env(m_exec);
    ::lib_rtl_output(LIB_RTL_DEBUG,"+++ Execute run-time image: %s", exe.c_str());
    ::fflush(stdout);
    //
    // Set new process group if signalled:
    if ( new_process_group )   {
      ::setpgrp();
    }
    char** arg = setup_arguments();
    char** env = setup_environment();
    int status = 0;
    // Start new image. retry if EAGAIN. Otherwise break and return error!
    while ( true )   {
      if ( 0 == ::stat(exe.c_str(), &stat_buff) )
	status = ::execve(exe.c_str(), arg, env);
      else
	status = ::execvpe(exe.c_str(), arg, env);
      /// If execve returned, something went awfully wrong. We have to signal the failure
      if ( status < 0 )  {
	std::error_code ec = RTL::get_error(errno);
	::lib_rtl_output(LIB_RTL_DEBUG,"+++ Process: execve FAILED. [%s]", ec.message().c_str());
      }
      if ( errno != EAGAIN )  {
	status = errno;
	break;
      }
    }
    delete [] arg;
    delete [] env;
    return status;
  }
  else if (m_pid > 0)   {  // parent process
    if ( Process::debug() ) {
      std::stringstream str;
      str.str("");
      str << "Parent: Started process:" << m_name;
      if ( !m_output.empty() ) str << " output redirected to:" << m_output;
      str << std::endl;
      ::lib_rtl_output(LIB_RTL_INFO,str.str().c_str());
    }
    m_state = RUNNING;
    m_status = 0;
    return 1;
  }
  std::cerr << "Failed to fork" << std::endl;
#endif
  return 0;
}

/// Send a signal to the process
int RTL::Process::signal(int signum)    {
  return sendSignal(signum);
}

/// Send a signal to the process and all its children (SIGTERM)
int RTL::Process::signalall(int signum)    {
  return sendSignalAll(signum);
}

 /// Terminate the process (SIGTERM)
int RTL::Process::stop()    {
  return sendSignal(SIGTERM);
}

/// Terminate the process and all its children (SIGTERM)
int RTL::Process::stopall()    {
  return sendSignalAll(SIGTERM);
}

/// Kill the process (SIGKILL)
int RTL::Process::kill()    {
  return sendSignal(SIGKILL);
}

/// Kill the process and all its children (SIGKILL)
int RTL::Process::killall()    {
  return sendSignalAll(SIGKILL);
}

/// Interrupt the process (SIGINT)
int RTL::Process::interrupt()    {
  return sendSignal(SIGINT);
}

/// Wait for process to terminate
int RTL::Process::wait(int flag)    {
#ifdef __linux
  int ret = -1;
  int opt = WNOHANG;
  switch(flag) {
  case WAIT_BLOCK:
    opt = WUNTRACED|WCONTINUED;
  case WAIT_NOBLOCK:
  default:
    break;
  }
  if ( m_state == ENDED )  {
    return ENDED;
  }
  else if ( m_pid <= 0 )   {
    return m_state = ENDED;
  }
  else  {
    do {
      ret = ::waitpid(m_pid, &m_status, opt);
    } while (ret == -1 && errno == EINTR);
  }
  
  std::stringstream str;
  if ( ret == -1 && errno == ESRCH ) {        // No such process
    str << "waitpid() '" << m_name << "' exited: Status=" << WEXITSTATUS(m_status) << std::endl;
    m_pid = -1;
    return m_state = ENDED;
  }
  else if ( ret == -1 && errno == ECHILD ) {  // No child process
    str << "waitpid() '" << m_name << "' exited: Status=" << WEXITSTATUS(m_status) << std::endl;
    m_pid = -1;
    return m_state = ENDED;
  }
  else if ( ret == -1 ) {                     // General error condition
    return INVALID;
  }
  else if ( ret == 0 && (opt&WNOHANG) ) {     // No child has ended yet
    return INVALID;
  }
  else if( WIFEXITED(m_status) )      {
    str << "waitpid() '" << m_name << "' exited: Status=" << WEXITSTATUS(m_status) << std::endl;
    m_pid = -1;
    return m_state = ENDED;
  }
  else if( WIFSTOPPED(m_status) )      {
    return m_state = STOPPED;
  }
  else if( WIFCONTINUED(m_status) )      {
    return m_state = RUNNING;
  }
  else if( WIFSIGNALED(m_status) )      {
    str << "waitpid() '" << m_name << "' exited due to a signal: " << WTERMSIG(m_status) << std::endl;
    return m_state = ENDED;
  }
  if ( str.str().length() > 0 )  {
    ::lib_rtl_output(LIB_RTL_INFO,str.str().c_str());
  }
  return RUNNING;
#else
  return 0;
#endif
}

/// Wait BLOCKED secs seconds for process to terminate
int RTL::Process::wait_for(unsigned long millisecs, int flag)    {
  struct timeval start, now;
  ::gettimeofday(&start, nullptr);
  long expires = start.tv_sec*1000000 + start.tv_usec + 1000*millisecs;
  long last    = expires;
  int  status  = INVALID;

  do  {
    status = this->wait(flag|WAIT_NOBLOCK);
    if ( status == STOPPED || status == ENDED ) break;
    ::lib_rtl_usleep(expires-last > 100 ? 100 : 10);
    ::gettimeofday(&now, nullptr);
    last = now.tv_sec*1000000 + now.tv_usec;
  } while ( last < expires );
  return status;
}

/// Wait BLOCKED secs seconds for process to terminate
int RTL::Process::wait_for(unsigned long millisecs)    {
  return this->wait_for(millisecs, WAIT_NOBLOCK);
}
