//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : OnlineKernel
//
//  Author     : Markus Frank
//==========================================================================
// Include files
#include "RTL/ProcessGroup.h"

#include <cmath>
#include <cstdio>
#include <signal.h>
#include "RTL/rtl.h"
#ifndef _WIN32
#include <sys/wait.h>
#endif
#include <sys/time.h>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <cerrno>

#include "RTL/DllAccess.h"
using OnlineBase::currentCommand;

/// Standard constructor
RTL::ProcessGroup::ProcessGroup() : Process(0)   {
}

/// Standard destructor
RTL::ProcessGroup::~ProcessGroup() {
  m_procs.clear();
  m_pid = -1;
}

/// Get a process by PID
RTL::Process* RTL::ProcessGroup::get(int pid) const {
  for( auto* p : m_procs )  {
    if ( pid == p->pid() ) return p;
  }
  return 0;
}

/// String representation of process
std::string RTL::ProcessGroup::to_string()  const   {
  std::stringstream str;
  str << "Processgroup [" << m_procs.size() << " entries]" << m_name << std::endl;
  for( std::size_t i=0, n=m_procs.size(); i < n; ++i )  {
    str << "[ id:" << std::setw(3) << i << " " << m_procs[i]->to_string() << "]" << std::endl;
  }
  return str.str();
}

/// Check if the process is running. If not restart 'restarts' times.
int RTL::ProcessGroup::ensure(int /* restarts */, int /* millis_between */)  {
  throw std::runtime_error("Call "+std::string(__func__)+" not implemented for ProcessGroup");
}

/// Start process group
int RTL::ProcessGroup::start()    {
  int status = 1, iret;
  for( auto* p : m_procs )  {
    if ( !lib_rtl_is_success(iret = p->start()) )
      status = iret;
  }
  return status;
}

/// Start process group
int RTL::ProcessGroup::start(bool new_process_group)    {
  int status = 1, iret;
  for( auto* p : m_procs )  {
    if ( !lib_rtl_is_success(iret = p->start(new_process_group)) )
      status = iret;
  }
  return status;
}

/// Stop process group
int RTL::ProcessGroup::stop()    {
  for(std::size_t i=m_procs.size(); i>0; --i)   {
    if ( i < m_procs.size() )  {
      if ( m_procs[i] ) m_procs[i]->stop();
    }
  }
  return 1;
}

/// Stop process group
int RTL::ProcessGroup::stop(std::size_t first, std::size_t last)   {
  if ( first > last )   {
    return this->stop(std::max(0UL,last), first);
  }
  for(std::size_t i=last; i >= first; --i)   {
    if ( i < m_procs.size() )  {
      if ( m_procs[i] ) m_procs[i]->stop();
    }
  }
  return 1;
}

/// Wait for process group.end
int RTL::ProcessGroup::wait(int flag)    {
#ifdef __linux
  int status = 1;
  int cnt = m_procs.size();
  int opt = WNOHANG;
  switch(flag) {
  case WAIT_BLOCK:
    opt = WUNTRACED|WCONTINUED;
  case WAIT_NOBLOCK:
  default:
    break;
  }
  while(cnt>=0) {
    int sc = 0;
    pid_t pid = -1;
    do {
      pid = waitpid(0, &sc, opt);
    } while (pid == -1 && errno == EINTR);
    if ( pid != -1 ) {
      RTL::Process* p = get(pid);
      if ( p && RTL::Process::debug() ) {
	std::stringstream str;
	str << "Process " << p->name() << " ended. Status=" << sc;
	if ( !p->output().empty() ) str << " output redirected to:" << p->output();
	str << std::endl;
	::lib_rtl_output(LIB_RTL_INFO,str.str().c_str());
      }
    }
    else if ( pid == -1 ) {
      return -1;
    }
  }
  return status;
#else
  return 0;
#endif
}

/// Wait for process group.end
int RTL::ProcessGroup::wait(const std::vector<RTL::Process*>& procs, int flag)   {
  int status = 0;
  time_t start = ::time(0);
  for( Process* p : procs )  {
    if ( p )  {
      long diff = this->m_max_wait - long(::time(0) - start);
      // Wait full allowed time for first process. Then just scan
      int sc = (diff > 0) ? p->wait_for(diff, flag) : p->wait(flag|WAIT_NOBLOCK);
      if ( !(sc == STOPPED || sc == ENDED) ) ++status;
    }
  }
  return status;
}

/// Wait BLOCKED secs seconds for processes to terminate
int RTL::ProcessGroup::wait_for(unsigned long millisecs)    {
  return this->wait_for(m_procs, millisecs, WAIT_NOBLOCK);
}

/// Disabled: Throws exception
int RTL::ProcessGroup::wait_for(unsigned long millisecs, int flag)   {
  return this->wait_for(m_procs, millisecs, flag);
}

/// Wait BLOCKED secs seconds for processes to terminate
int RTL::ProcessGroup::wait_for(const std::vector<RTL::Process*>& procs, unsigned long millisecs)   {
  return this->wait_for(procs, millisecs, WAIT_NOBLOCK);
}

/// Wait BLOCKED secs seconds for processes to terminate
int RTL::ProcessGroup::wait_for(const std::vector<RTL::Process*>& procs, unsigned long millisecs, int flag)   {
  struct timeval start, now;
  ::gettimeofday(&start, nullptr);
  long expires = start.tv_sec*1000000 + start.tv_usec + 1000*millisecs;
  long last    = expires;
  int  status  = INVALID;
  int  count   = 0;

  do  {
    count = 0;
    status = procs.empty() ? ENDED : INVALID;
    for( Process* p : procs ) {
      if ( p )  {
	status = p->wait(flag);
	if ( status == STOPPED ) status = ENDED;
	if ( status != ENDED ) break;
	++count;
      }
    }
    if ( status == ENDED ) break;
    ::lib_rtl_usleep(expires-last > 100 ? 100 : 10);
    ::gettimeofday(&now, nullptr);
    last = now.tv_sec*1000000 + now.tv_usec;
  } while ( last < expires );
  return count;
}

/// Add a new process
RTL::Process* RTL::ProcessGroup::add(RTL::Process* p) {
  if ( p ) m_procs.emplace_back(p);
  return p;
}

/// Add multiple processes
RTL::ProcessGroup& RTL::ProcessGroup::add(std::vector<RTL::Process*>&& procs) {
  for ( auto* p : procs )
    if ( p ) this->add(p);
  procs.clear();
  return *this;
}

/// Merge two process groups
RTL::ProcessGroup& RTL::ProcessGroup::merge(RTL::ProcessGroup&& pg) {
  return this->add( std::move(pg.m_procs) );
}

/// Remove a process from the list
int RTL::ProcessGroup::remove(const std::string& name, RTL::Process** proc) {
  if ( proc ) *proc = 0;
  for(Processes::iterator i=m_procs.begin(); i!=m_procs.end();++i) {
    if ( name == (*i)->name() ) {
      if ( proc ) *proc = (*i);
      else        delete (*i);
      m_procs.erase(i);
      return 1;
    }
  }
  return 0;
}

/// Remove a process from the list
int RTL::ProcessGroup::remove(RTL::Process* proc) {
  for(Processes::iterator i=m_procs.begin(); i!=m_procs.end();++i) {
    if ( (*i) == proc )  {
      delete (*i);
      m_procs.erase(i);
      return 1;
    }
  }
  return 0;
}

/// Kill the processes (SIGKILL)
int RTL::ProcessGroup::kill()   {
  int status = 1, iret;
  for( auto* p : m_procs )  {
    if ( p && !lib_rtl_is_success(iret = p->kill()) )
      status = iret;
  }
  return status;
}

/// Kill the processes and their children (SIGKILL)
int RTL::ProcessGroup::killall()   {
  int status = 1, iret;
  for( auto* p : m_procs )  {
    if ( p && !lib_rtl_is_success(iret = p->killall()) )
      status = iret;
  }
  return status;
}

/// Remove all processes
int RTL::ProcessGroup::removeAll() {
  for( auto* p : m_procs )
    delete p;
  m_procs.clear();
  return 1;
}

/// Check if process is running
int RTL::ProcessGroup::is_running() const  {
  if ( !m_procs.empty() )  {
    for( auto* p : m_procs )
      if ( !(p && p->is_running()) )
	return 0;
    return 1;
  }
  return 0;
}

