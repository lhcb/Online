//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
/// Framework include files
#include <RTL/rtl.h>
#include <RTL/bits.h>

/// C/C++ include files
#include <cstdio>
#include <cstdlib>
#include <cstring>

#define Max(a,b) (a>b)?a:b;
#define Min(a,b) (a>b)?b:a;
#define BITS_PER_SHORT 16

namespace {
  const uint8_t bit_mask[]={
    uint8_t(0x01), uint8_t(0x02), uint8_t(0x04), uint8_t(0x08), 
    uint8_t(0x10), uint8_t(0x20), uint8_t(0x40), uint8_t(0x80)
  };
  const uint8_t inv_mask[]={
    uint8_t(~0x01), uint8_t(~0x02), uint8_t(~0x04), uint8_t(~0x08), 
    uint8_t(~0x10), uint8_t(~0x20), uint8_t(~0x40), uint8_t(~0x80)
  };
}

int32_t mask_and2 (const uint32_t* mask1,const uint32_t* mask2,uint32_t* mask3,int mask_size)   {
  int32_t result=0;
  for (int32_t i=0; i<mask_size; ++i)  {
    mask3[i] = mask1[i] & mask2[i];
    result  |= mask3[i];
  }
  return result;
}

int32_t mask_and3 (const uint32_t* mask1,const uint32_t* mask2,const uint32_t* mask3,uint32_t* mask4,int32_t mask_size)   {
  int32_t result=0;
  for (int32_t i=0; i<mask_size; ++i)  {
    mask4[i] = ((mask1[i] & mask2[i]) & mask3[i]);
    result  |= mask4[i];
  }
  return result;
}

int32_t mask_and4(const uint32_t* mask1,const uint32_t* mask2,const uint32_t* mask3,const uint32_t* mask4,uint32_t* mask5,int32_t mask_size)  {
  int32_t result=0;
  for (int32_t i=0; i<mask_size; ++i)  {
    mask5[i] = (((mask1[i] & mask2[i]) & mask3[i]) & mask4[i]);
    result  |= mask5[i];
  }
  return result;
}

int32_t mask_or2 (const uint32_t* mask1, const uint32_t* mask2,uint32_t* mask3,const int32_t mask_size)   {
  int32_t result = 0;
  for (int32_t i=0;i<mask_size;++i)  {
    mask3[i] = mask1[i] | mask2[i];
    result  |= mask3[i];
  }
  return result;
}

int32_t mask_or3 (const uint32_t* mask1, const uint32_t* mask2,const uint32_t* mask3,uint32_t* mask4,const int32_t mask_size)   {
  int32_t result = 0;
  for (int32_t i=0;i<mask_size;++i)  {
    mask4[i] = mask1[i] | mask2[i] | mask3[i];
    result  |= mask3[i];
  }
  return result;
}

int32_t mask_or4   (const uint32_t* mask1,const uint32_t* mask2,const uint32_t* mask3,const uint32_t* mask4,uint32_t* mask5,int32_t mask_size)  {
  int32_t result=0;
  for (int32_t i=0; i<mask_size; ++i)  {
    mask5[i] = (((mask1[i] | mask2[i]) | mask3[i]) | mask4[i]);
    result  |= mask5[i];
  }
  return result;
}

int32_t mask_and_ro2 (const uint32_t* mask1,const uint32_t* mask2,const int32_t mask_size)   {
  int32_t result=0;
  for (int32_t i=0;i<mask_size;++i)  {
    result |= (mask1[i] & mask2[i]);
  }
  return result;
}

int32_t mask_and_ro3 (const uint32_t* mask1,const uint32_t* mask2, const uint32_t* mask3,const int32_t mask_size)   {
  int32_t result=0;
  for (int32_t i=0;i<mask_size;++i)  {
    result |= ((mask1[i] & mask2[i]) & mask3[i]);
  }
  return result;
}

int32_t mask_or_ro2 (const uint32_t* mask1,const uint32_t* mask2, const int32_t mask_size) {
  int32_t result = 0;
  for (int32_t i=0;!result && i<mask_size;++i)  {
    result |= (mask1[i] | mask2[i]);
  }
  return result;
}

int32_t mask_or_ro3 (const uint32_t* mask1,const uint32_t* mask2, const uint32_t* mask3, const int32_t mask_size) {
  int32_t result = 0;
  for (int32_t i=0; !result && i<mask_size; ++i)  {
    result |= (mask1[i] | mask2[i] | mask3[i]);
  }
  return result;
}

int32_t mask_or_ro4 (const uint32_t* mask1,const uint32_t* mask2, const uint32_t* mask3, const uint32_t* mask4, const int32_t mask_size) {
  int32_t result = 0;
  for (int32_t i=0;!result && i<mask_size;++i)  {
    result |= (mask1[i] | mask2[i] | mask3[i] | mask4[i]);
  }
  return result;
}

int32_t mask_summ(const uint32_t* mask, int32_t mask_size)   {
  int32_t result = 0;
  for (int32_t i=0;i<mask_size;++i)  {
    result |= mask[i];
  }
  return result;
}

int32_t BF_alloc(void* bitfield, int32_t bf_size, int32_t size_wanted, int32_t* pos_found)   {
  bool set          = false;
  char* base        = (char*)bitfield;
  int32_t nbytes    = bf_size/Bits::BITS_PER_BYTE;
  int32_t fld_size  = bf_size%Bits::BITS_PER_BYTE;
  for(int32_t i=0, j=0, pos=0, len=0; i < nbytes; ++i, j=0)  {
    const int32_t c = base[i];
    if ( !set && 0xFF == c )  {
      continue;
    }
    else if ( set && 0 == c )  {
      len += Bits::BITS_PER_BYTE;
      if ( len < size_wanted )  {
        continue;
      }
      *pos_found = pos;
      return BF_set(base, pos, size_wanted);
    }
    else {
      const uint8_t* msk = bit_mask+j;
      for(int32_t nbit=(i<nbytes) ? Bits::BITS_PER_BYTE : fld_size; j< nbit; ++j, ++msk )  {
        if ( 0 == (c & *msk) )  {
          ++len;
          if ( set && len >= size_wanted )  {
            *pos_found = pos;
            return BF_set(base, pos, size_wanted);
          }
          else if ( !set )  {
            len = 1;
            set = true;
            pos = (i*Bits::BITS_PER_BYTE) + j;
          }
          continue;
        }
        set = false;
      }
    }
  }
  *pos_found = bf_size;
  return 0;
}

int32_t BF_count(const void* bitfield, int32_t bf_size, int32_t* pos, int32_t* size) {
  bool set = false;  
  const uint8_t* msk = bit_mask;
  const uint8_t* base = (const uint8_t*)bitfield;
  int32_t max_pos = bf_size, max_len = 0, len = 0, start_pos = 0;
  for(int32_t i=0, j=0, c=*base; i < bf_size/Bits::BITS_PER_BYTE; ++i, j=0)  {
    c = *(base+i);
    if ( !set && ((c == 0xFF)) )  {// || (c != 0 && max_len > Bits::BITS_PER_BYTE)) ) {
      continue;
    }
    else if ( c == 0 )   {
      if ( !set )  {
	start_pos = i*Bits::BITS_PER_BYTE;
	set = true;
	len = 0;
      }
      len += Bits::BITS_PER_BYTE;
      continue;
    }
    else  {
      for( j=0, msk=bit_mask+j; j<Bits::BITS_PER_BYTE; ++j, ++msk)  {
	if ( 0 == (c & *msk) )  {
	  if ( !set )  {
	    start_pos = (i*Bits::BITS_PER_BYTE) + j;
	    set = true;
	    len = 0;
	  }
	  ++len;
	  continue;
	}
	if ( len > max_len )  {
	  max_pos = start_pos;
	  max_len = len;
	}
	len = 0;
	set = false;
	//start_pos = (i*Bits::BITS_PER_BYTE) + j;
      }
    }
    //start_pos = Bits::BITS_PER_BYTE*i + j;
    //len = Bits::BITS_PER_BYTE;
    //set = true;
    if ( len > max_len )  {
      max_pos = start_pos;
      max_len = len;
    }
  }
  if ( len > max_len )  {
    max_pos = start_pos;
    max_len = len;
  }
  *pos    = max_pos;
  *size   = max_len;
  return 1;
}

int32_t BF_set(void* bitfield, int32_t pos, int32_t len)   {
  const uint8_t* msk;
  char* base = (char*)bitfield;
  int32_t j, k, bit = pos%Bits::BITS_PER_BYTE;
  base += pos/Bits::BITS_PER_BYTE;
  if ( bit > 0 )  {
    msk = bit_mask+bit;
    j=(len>=Bits::BITS_PER_BYTE-bit) ? Bits::BITS_PER_BYTE : len+bit;
    for ( k=bit; k<j; ++k, ++msk)
      *base = char(*base|*msk);
    len -= j-bit;
    ++base;
  }
  if ( len/Bits::BITS_PER_BYTE )  {
    j = len/Bits::BITS_PER_BYTE;
    len   -= j*Bits::BITS_PER_BYTE;
    ::memset(base,0xFF,j);
    base += j;
  }
  for ( k=0, msk=bit_mask; k<len; ++k,++msk )
    *base = char(*base|*msk);
  return 1;
}

int32_t BF_free(void* bitfield, int32_t pos, int32_t len) {
  const uint8_t* msk;
  char* base = (char*)bitfield;
  int32_t j, k, bit = pos%Bits::BITS_PER_BYTE;
  base += pos/Bits::BITS_PER_BYTE;
  if ( bit > 0 )  {
    msk = inv_mask+bit;
    j=(len>=Bits::BITS_PER_BYTE-bit) ? Bits::BITS_PER_BYTE : len+bit;
    for(k=bit; k<j; ++k, ++msk)  {
      *base = char(*base&*msk);
    }
    len -= j-bit;
    ++base;
  }
  if ( len/Bits::BITS_PER_BYTE )  {
    j = len/Bits::BITS_PER_BYTE;
    len -=  j*Bits::BITS_PER_BYTE;
    ::memset(base,0x0,j);
    base += j;
  }
  for(k=0, msk = inv_mask; k<len; ++k,++msk)  {
    *base = char(*base&*msk);
  }
  return 1;
}

void BF_print(const void* field, int32_t len, size_t ncols, bool prt_hdr)  {
  size_t i, j, k, n;
  FILE* output = stdout;
  if ( prt_hdr )  {
    printf("\n");
    for(j=0, n=0; j < ncols; ++j )  {
      for(k=0; k<32; ++k, ++n)  {
        char c = (char)((n+1))%10 == 0 ? (n+1)/10+'0' : ' ';
        ::printf("%c",c);
      }
      if ( j < (ncols-1) ) ::fputc(' ',output);
    }
    printf("\n");
    for(j=0, n=0; j < 4; ++j )  {
      for(k=0; k<32; ++k, ++n)  {
        ::printf("%d",int32_t((n+1)%10));
      }
      if ( j < (ncols-1) ) ::fputc(' ',output);
    }
    ::fputs("\n\n",output);
  }
  std::vector<std::string> words;
  Bits::dumpWords(field, len, words);
  for(i = 0, k = 0; i < len/sizeof(int32_t)/ncols; ++i )  {
    for(j = 0; j < ncols; ++j )  {
      ::fputs(words[i*ncols+j].c_str(),output);
      if ( j < (ncols-1) ) printf (" ");
      ++k;
    }
    ::fputs("\n",output);
  }
  while(k<sizeof(words))  {
    ::fputs(words[k++].c_str(),stdout);
  }
  ::fputs("\n",output);
}

void Bits::dumpWords(const void* field, int32_t len, std::vector<std::string>& words)  {
  uint8_t* txt = (uint8_t*)field;
  char word[33];
  words.clear();
  ::memset(word,'0',sizeof(word));
  word[32] = 0;
  len += len%sizeof(int32_t) ? 1 : 0;
  for(int32_t i = 0; i < len; i += sizeof(int32_t) )  {
    for (int32_t k = 0; k<Bits::BITS_PER_BYTE; ++k)  {
      word[k]    = (txt[i]&(1<<k))   ? '1' : '0';
      word[k+Bits::BITS_PER_BYTE]   = char(i+1<len ? (txt[i+1]&(1<<k)) ? '1' : '0' : 0);
      word[k+2*Bits::BITS_PER_BYTE] = char(i+2<len ? (txt[i+2]&(1<<k)) ? '1' : '0' : 0);
      word[k+3*Bits::BITS_PER_BYTE] = char(i+3<len ? (txt[i+3]&(1<<k)) ? '1' : '0' : 0);
    }
    words.push_back(word);
  }
}

#ifdef _WIN32
static inline int32_t generic_ffs(int32_t x)  {
  int32_t r = 1;
  if (!x)
    return 0;
  if (!(x & 0xffff)) {
    x >>= 16;
    r += 16;
  }
  if (!(x & 0xff)) {
    x >>= 8;
    r += 8;
  }
  if (!(x & 0xf)) {
    x >>= 4;
    r += 4;
  }
  if (!(x & 3)) {
    x >>= 2;
    r += 2;
  }
  if (!(x & 1)) {
    x >>= 1;
    r += 1;
  }
  return r;
}

static inline int32_t generic_ffc(int32_t x)  {
  int32_t r = 1;
  if (!x)
    return 0;
  if (!(x & 0xffff)) {
    x >>= 16;
    r += 16;
  }
  if (!(x & 0xff)) {
    x >>= 8;
    r += 8;
  }
  if (!(x & 0xf)) {
    x >>= 4;
    r += 4;
  }
  if (!(x & 3)) {
    x >>= 2;
    r += 2;
  }
  if (!(x & 1)) {
    x >>= 1;
    r += 1;
  }
  return r;
}

#define ffs generic_ffs
#define ffc generic_ffc
#endif
static uint32_t mask[] = {
  0x00000001, 0x00000002, 0x00000004, 0x00000008,
  0x00000010, 0x00000020, 0x00000040, 0x00000080,
  0x00000100, 0x00000200, 0x00000400, 0x00000800, 
  0x00001000, 0x00002000, 0x00004000, 0x00008000, 
  0x00010000, 0x00020000, 0x00040000, 0x00080000, 
  0x00100000, 0x00200000, 0x00400000, 0x00800000, 
  0x01000000, 0x02000000, 0x04000000, 0x08000000, 
  0x10000000, 0x20000000, 0x40000000, 0x80000000
};

int32_t lib_rtl_ffc_unaligned (int32_t* start, int32_t* len, const void* base, int32_t* position)  {
  uint32_t i, j, v;
  char* b = ((char*)base) + ((*start)/Bits::BITS_PER_BYTE);
  for (i=*start, j=i+*len, v=*(uint32_t*)b; i<j; ++i)  {
    if ( (v & mask[i%32]) == 0 )  {
      *position = i;
      return 1;
    }
  }
  *position = i;
  return i>31 ? 0 : 1;
}

int32_t lib_rtl_ffc (int32_t* start, int32_t* len, const void* base, int32_t* position)  {
  uint32_t i, j;
  uint8_t* b = ((uint8_t*)base) + ((*start)/Bits::BITS_PER_BYTE);
  for (i = *start, j = i+*len; i<j; )  {
    if ( (b[i/8] & mask[i%8]) == 0 )  {
      *position = i;
      return 1;
    }
    ++i;
  }
  *position = i;
  return (i>31) ? 0 : 1;
}

int32_t lib_rtl_ffs (int32_t* pos, int32_t* size, const void* base, int32_t* ret)  {
  int32_t retval;
  int32_t val= *(int32_t*)base;
  int32_t p = *pos;
  if (p<=32) {
    if (p)
      val&=~(1<<(p-1));
    retval=ffs(val);
    if (retval==0) {
      *ret=*size;
      return 0;
    }
    retval--;
  } else {
    printf("ffs retval for > 32 bit size not yet implemented");
    return 0;
  }
  if (retval>=(*size)) {
    *ret=*size;
    return 0;
  }
  *ret=retval;
  return 1;
#if 0
  char* b = ((char*)base) + ((*start)/Bits::BITS_PER_BYTE);
  for (uint32_t i=*start, j=i+*len, v=*(uint32_t*)b; i<j; ++i)  {
    if ( (v & mask[i%32]) != 0 )  {
      *position = i;
      return 1;
    }
  }
  *position = i;
  return i>31 ? 0 : 1;
#endif
}
