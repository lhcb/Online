//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
/// Framework include files
#include <RTL/rtl.h>
#include <RTL/bits.h>

/// C/C++ include files
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <cstring>
#include <strings.h>

int32_t BF_count2(const void* base,int32_t bf_size,int32_t* pos,int32_t* size);
int32_t BF_set2(void* base, int32_t pos, int32_t len);
int32_t BF_free2(void* base,int32_t pos, int32_t len);


namespace {
  const uint8_t bit_mask[]={
    uint8_t(0x01), uint8_t(0x02), uint8_t(0x04), uint8_t(0x08), 
    uint8_t(0x10), uint8_t(0x20), uint8_t(0x40), uint8_t(0x80)
  };
  const uint8_t inv_mask[]={
    uint8_t(~0x01), uint8_t(~0x02), uint8_t(~0x04), uint8_t(~0x08), 
    uint8_t(~0x10), uint8_t(~0x20), uint8_t(~0x40), uint8_t(~0x80)
  };
  const uint64_t bit_mask2[]={
    0x1ULL,
    0x2ULL,
    0x4ULL,
    0x8ULL,    // 4
    0x1FULL,
    0x2FULL, 
    0x4FULL,
    0x8FULL,   // 8
    0x1FFULL,
    0x2FFULL, 
    0x4FFULL,
    0x8FFULL,  // 12
    0x1FFFULL,
    0x2FFFULL, 
    0x4FFFULL,
    0x8FFFULL,  // 16
    0x1FFFFULL,
    0x2FFFFULL, 
    0x4FFFFULL,
    0x8FFFFULL,  // 20
    0x1FFFFFULL,
    0x2FFFFFULL, 
    0x4FFFFFULL,
    0x8FFFFFULL,  // 24
    0x1FFFFFFULL,
    0x2FFFFFFULL, 
    0x4FFFFFFULL,
    0x8FFFFFFULL,  // 28
    0x1FFFFFFFULL,
    0x2FFFFFFFULL, 
    0x4FFFFFFFULL,
    0x8FFFFFFFULL,  // 32
    0x1FFFFFFFFULL,
    0x2FFFFFFFFULL, 
    0x4FFFFFFFFULL,
    0x8FFFFFFFFULL,  // 36
    0x1FFFFFFFFFULL, 
    0x2FFFFFFFFFULL, 
    0x4FFFFFFFFFULL,
    0x8FFFFFFFFFULL,  // 40
    0x1FFFFFFFFFFULL,
    0x2FFFFFFFFFFULL, 
    0x4FFFFFFFFFFULL,
    0x8FFFFFFFFFFULL,  // 44
    0x1FFFFFFFFFFFULL,
    0x2FFFFFFFFFFFULL, 
    0x4FFFFFFFFFFFULL,
    0x8FFFFFFFFFFFULL,  // 48
    0x1FFFFFFFFFFFFULL,
    0x2FFFFFFFFFFFFULL, 
    0x4FFFFFFFFFFFFULL,
    0x8FFFFFFFFFFFFULL,  // 52
    0x1FFFFFFFFFFFFFULL,
    0x2FFFFFFFFFFFFFULL, 
    0x4FFFFFFFFFFFFFULL,
    0x8FFFFFFFFFFFFFULL,  // 56
    0x1FFFFFFFFFFFFFFULL,
    0x2FFFFFFFFFFFFFFULL, 
    0x4FFFFFFFFFFFFFFULL,
    0x8FFFFFFFFFFFFFFULL,  // 60
    0x1FFFFFFFFFFFFFFFULL,
    0x2FFFFFFFFFFFFFFFULL, 
    0x4FFFFFFFFFFFFFFFULL,
    0x8FFFFFFFFFFFFFFFULL  // 64
  };
}

#define BITS_PER_LONGLONG 64
#define Max(a,b) (a>b)?a:b;
#define Min(a,b) (a>b)?b:a;

int BF_alloc2(void* bitfield, int32_t bf_size, int32_t size_wanted, int32_t* pos_found)   {
  uint8_t* ptr = (uint8_t*)bitfield;
  int32_t set = 0, idx, idq, nvals = bf_size/BITS_PER_LONGLONG;
  uint64_t* base = (uint64_t*)ptr;
  uint64_t c, cc;
  for(int32_t i=0, pos=0, len=0; i <= nvals; ++i, ++base)  {
    c = *base;
    cc = ~c;
    if ( !cc )  {
      set = 0;
      continue;  // All bits set
    }
    if ( !set )  {
      pos  = i*BITS_PER_LONGLONG;
      len  = 0;
      set  = 1;
    }
    if ( !c )   {
      len += BITS_PER_LONGLONG;
      if ( len < size_wanted ) continue;  // Interval insufficient. continue.
      if ( pos + len > bf_size ) len = bf_size-pos; // Check interval edge
      if ( len < size_wanted ) continue;  // Interval insufficient. continue.
      *pos_found = pos;
      return BF_set2(ptr, pos, size_wanted);
    }
    idx = ::ffsll(c);
    len += (idx-1);
    if ( len >= size_wanted  )  {    // Interval found. return it.
      if ( pos + len > bf_size ) len = bf_size-pos; // Check interval edge
      if ( len >= size_wanted  )  {  // Interval found. return it.
	*pos_found = pos;
	return BF_set2(ptr, pos, size_wanted);
      }
    }
    pos = i*BITS_PER_LONGLONG + idx; // Note: pos is ZERO based. ffs not!
    c &= ~(bit_mask2[idx-1]);
    if ( !c )   {
      len =  BITS_PER_LONGLONG - idx - 1;
      if ( len >= size_wanted )   {
	*pos_found = pos;
	return BF_set2(ptr, pos, size_wanted);
      }
    }
    else {
      do {
	idq = ::ffsll(c);
	idq = idq ? idq-1 : BITS_PER_LONGLONG-1;
	len = idq - idx;
	if ( len >= size_wanted )   {
	  *pos_found = pos;
	  return BF_set2(ptr, pos, size_wanted);
	}
	idx = idq + 1;
	c &= ~(bit_mask2[idq]);
	pos = i*BITS_PER_LONGLONG + idx; // Note: pos is ZERO based. ffs not!
      } while(c);
      if ( pos < (i+1)*BITS_PER_LONGLONG )  {
	set = 1;
	len = BITS_PER_LONGLONG - idx;
      }
      //::printf("Out of loop: %d set:%d\n", pos, set);
    }
  }
  *pos_found = bf_size;
  return 0;
}

int32_t BF_count2(const void* bitfield, int32_t bf_size, int32_t* pos, int32_t* size) {
  bool set = false;  
  const uint8_t* msk = bit_mask;
  const uint8_t* base = (const uint8_t*)bitfield;
  int32_t max_pos = bf_size, max_len = 0, len = 0, start_pos = 0;
  for(int32_t i=0, j=0, c=*base; i < bf_size/Bits::BITS_PER_BYTE; ++i, j=0)  {
    c = *(base+i);
    if ( !set && ((c == 0xFF)) )  {// || (c != 0 && max_len > Bits::BITS_PER_BYTE)) ) {
      continue;
    }
    else if ( c == 0 )   {
      if ( !set )  {
	start_pos = i*Bits::BITS_PER_BYTE;
	set = true;
	len = 0;
      }
      len += Bits::BITS_PER_BYTE;
      continue;
    }
    else  {
      for( j=0, msk=bit_mask+j; j<Bits::BITS_PER_BYTE; ++j, ++msk)  {
	if ( 0 == (c & *msk) )  {
	  if ( !set )  {
	    start_pos = (i*Bits::BITS_PER_BYTE) + j;
	    set = true;
	    len = 0;
	  }
	  ++len;
	  continue;
	}
	if ( len > max_len )  {
	  max_pos = start_pos;
	  max_len = len;
	}
	len = 0;
	set = false;
	//start_pos = (i*Bits::BITS_PER_BYTE) + j;
      }
    }
    //start_pos = Bits::BITS_PER_BYTE*i + j;
    //len = Bits::BITS_PER_BYTE;
    //set = true;
    if ( len > max_len )  {
      max_pos = start_pos;
      max_len = len;
    }
  }
  if ( len > max_len )  {
    max_pos = start_pos;
    max_len = len;
  }
  *pos    = max_pos;
  *size   = max_len;
  return 1;
}

int32_t BF_set2(void* bitfield, int32_t pos, int32_t len)   {
  uint8_t* base = (uint8_t*)bitfield;
  const uint8_t* msk;
  int32_t j, k, bit = pos%Bits::BITS_PER_BYTE;
  base += pos/Bits::BITS_PER_BYTE;
  if ( bit > 0 )  {
    msk = bit_mask+bit;
    j=(len>=Bits::BITS_PER_BYTE-bit) ? Bits::BITS_PER_BYTE : len+bit;
    for ( k=bit; k<j; ++k, ++msk)
      *base = uint8_t(*base|*msk);
    len -= j-bit;
    ++base;
  }
  if ( len/Bits::BITS_PER_BYTE )  {
    j = len/Bits::BITS_PER_BYTE;
    len   -= j*Bits::BITS_PER_BYTE;
    ::memset(base,0xFF,j);
    base += j;
  }
  for ( k=0, msk=bit_mask; k<len; ++k,++msk )
    *base = uint8_t(*base|*msk);
  return 1;
}

int32_t BF_free2(void* bitfield, int32_t pos, int32_t len) {
  uint8_t* base = (uint8_t*)bitfield;
  const uint8_t* msk;
  int32_t j, k, bit = pos%Bits::BITS_PER_BYTE;
  base += pos/Bits::BITS_PER_BYTE;
  if ( bit > 0 )  {
    msk = inv_mask+bit;
    j=(len>=Bits::BITS_PER_BYTE-bit) ? Bits::BITS_PER_BYTE : len+bit;
    for(k=bit; k<j; ++k, ++msk)  {
      *base = uint8_t(*base&*msk);
    }
    len -= j-bit;
    ++base;
  }
  if ( len/Bits::BITS_PER_BYTE )  {
    j = len/Bits::BITS_PER_BYTE;
    len -=  j*Bits::BITS_PER_BYTE;
    ::memset(base,0x0,j);
    base += j;
  }
  for(k=0, msk = inv_mask; k<len; ++k,++msk)  {
    *base = uint8_t(*base&*msk);
  }
  return 1;
}
