//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  RawFile.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================

/// Framework include files
#include <RTL/posix.h>
#include <RTL/rtl.h>

/// C/C++ include files
#include <sys/stat.h>
#include <fcntl.h>
#include <climits>
#include <cerrno>
#include <cctype>
#include <mutex>
#ifdef _WIN32
#include <io.h>
#else
#include <unistd.h>
#define O_BINARY 0
#endif

/// Descriptor for the POSIX file system
const Online::posix_t* native_posix_descriptor()   {
  static Online::posix_t p;
  if ( !p.open ) {
    p.set_option = [] (int, const char*, const char* ) { return -1; };
    p.unbuffered = Online::posix_t::COMPLETE;
    p.open       = ::open;
    p.close      = ::close;
    p.read       = ::read;
    p.write      = ::write;
    p.lseek      = ::lseek;
    p.lseek64    = ::lseek64;
    p.access     = ::access;
    p.unlink     = ::unlink;
    p.stat       = ::stat;
    p.stat64     = ::stat64;
    p.fstat      = ::fstat;
    p.fstat64    = ::fstat64;

    p.buffered  = Online::posix_t::COMPLETE;
    p.fopen     = ::fopen;
    p.fclose    = ::fclose;
    p.fwrite    = ::fwrite;
    p.fread     = ::fread;
    p.fseek     = ::fseek;
    p.ftell     = ::ftell;
    p.fileno    = ::fileno;

    p.directory = Online::posix_t::COMPLETE;
    p.rmdir     = ::rmdir;
    p.mkdir     = ::mkdir;
    p.opendir   = ::opendir;
    p.readdir   = ::readdir;
    p.closedir  = ::closedir;
  }
  return &p;
}

/// ------------------------------------------------------------------------------
std::size_t Online::posix_print(int debug, const char* format, ...)  {
  if ( debug )   {
    va_list args;
    va_start( args, format );
    std::size_t len = ::lib_rtl_log(debug > 1 ? debug : LIB_RTL_ALWAYS, format, args);
    va_end(args);
    return len;
  }
  return 0;
}

/// ------------------------------------------------------------------------------
Online::posix_t::posix_t()  {
}
/// ------------------------------------------------------------------------------
int  Online::posix_t::new_fd      ()   {
  static std::mutex m;
  static int fd_count = 0xABAB;
  std::lock_guard<std::mutex> guard(m);
  ++fd_count;
  return fd_count;
}
/// ------------------------------------------------------------------------------
int Online::posix_t::fileno_specific(const FILE* file)   {
  int fd = (int)(long(file)&0xFFFF);
  return fd;
}
/// ------------------------------------------------------------------------------
int  Online::posix_t::truncate_disk ( int fd, std::size_t len )  {
  return ::ftruncate(fd, len);
}
/// ------------------------------------------------------------------------------
int  Online::posix_t::access_disk ( const char* nam, int mode )  {
  return ::access(nam, mode);
}
/// ------------------------------------------------------------------------------
int  Online::posix_t::unlink_disk ( const char* nam )  {
  return ::unlink(nam);
}
/// ------------------------------------------------------------------------------
int  Online::posix_t::lseek_disk  ( int fd, long offset, int how )  {
  return ::lseek(fd, offset, how);
}
/// ------------------------------------------------------------------------------
int  Online::posix_t::lseek64_disk( int fd, int64_t offset, int how )  {
  return ::lseek64(fd, offset, how);
}
/// ------------------------------------------------------------------------------
int Online::posix_t::fstat_disk( int fd, struct stat* statbuf ) {
  return ::fstat(fd, statbuf);
}
/// ------------------------------------------------------------------------------
int Online::posix_t::fstat64_disk( int fd, struct stat64* statbuf ) {
  return ::fstat64(fd, statbuf);
}
/// ------------------------------------------------------------------------------
int Online::posix_t::stat_disk   ( const char* path, struct stat* statbuf )  {
  return ::stat(path, statbuf);
}
/// ------------------------------------------------------------------------------
int Online::posix_t::stat64_disk ( const char* path, struct stat64* statbuf )  {
  return ::stat64(path, statbuf);
}
/// ------------------------------------------------------------------------------
std::pair<int,int> Online::posix_t::fopen_disk(const char* path, const char* mode)  {
  FILE* file = ::fopen(path, mode);
  if ( file )    {
    int fd = ::dup(::fileno(file));
    ::fclose(file);
    char m = ::tolower(mode[0]);
    int flags = 0;
    if      ( m == 'w' ) flags = O_WRONLY;
    else if ( m == 'r' ) flags = O_RDONLY;
    else if ( m == 'a' ) flags = O_APPEND;
    return { fd, flags };
  }
  return { -1, 0 };
}
/// ------------------------------------------------------------------------------
int Online::posix_t::open_disk(const char* path, int flags, va_list& args)  {
  int fd = -1;
  int mode = 0;
  if (flags & O_CREAT)    {
    mode = va_arg(args, int);
    va_end(args);
    fd = ::open(path, flags, mode);
  }
  else   {
    fd = ::open(path, flags);
  }
  return fd;
}
/// ------------------------------------------------------------------------------
long Online::posix_t::read_disk(int fd, void* ptr, std::size_t len)   {
  std::size_t total = 0;
  uint8_t* pointer = (uint8_t*)ptr;
  for(; total < len; )   {
    ssize_t sc = ::read(fd, pointer + total, len - total);
    if ( sc == 0 && errno == EINTR )
      continue;
    else if ( sc == 0 )
      break;
    else if ( sc > 0  )
      total += sc;
    else if ( sc < 0  ) {
      return -1;
    }
  }
  return total;
}
/// ------------------------------------------------------------------------------
long Online::posix_t::write_disk(int fd, const void* ptr, std::size_t len)   {
  long total=0, count=len;
  const uint8_t* pointer = (const uint8_t*)ptr;
  for(; total < count; )   {
    ssize_t sc = ::write(fd, pointer+total, len-total);
    int err = errno;
    if ( sc == 0 && err == EINTR )
      continue;
    else if ( sc == 0 )
      break;
    else if ( sc > 0  )
      total += sc;
    else if ( sc < 0  )
      return -1;
  }
  return total;
}
