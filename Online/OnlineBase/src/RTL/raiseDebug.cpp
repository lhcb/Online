//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : OnlineBase
//
//  Author     : Beat Jost
//==========================================================================
#include <RTL/raiseDebug.h>
#include <csignal>
#include <sys/types.h>
#include <unistd.h>

void RTL::raiseDebug()
{
  ::kill(::getpid(),SIGSTOP);
}



