//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================

/// Framework include files
#include "rtl_internal.h"
#include <RTL/Logger.h>

/// C/C++ include files
#include <vector>
#include <ctime>
#include <cerrno>
#include <memory>
#include <thread>
#include <cstring>
#include <climits>
#include <iostream>
#include <fcntl.h>

using namespace std;

#if defined(_WIN32)
typedef __int64       int64_t;
namespace { 
  inline FILE* popen(__CXX_CONST char* cmd, __CXX_CONST char* typ)  { return ::_popen(cmd, typ); }
  inline int pclose(FILE* file)  {  return ::_plcose(file);  }
}
#endif   /* _WIN32      */

#ifdef _WIN32
#define vsnprintf _vsnprintf
#include <winsock.h>

#include <stdexcept>

namespace {
  struct __init__ {
    __init__()  {
      static WSADATA g_WSAData;
      memset(&g_WSAData, 0, sizeof(WSADATA));
      if (WSAStartup ( MAKEWORD(1,1), &g_WSAData) != 0)    {
        throw runtime_error("RTL: WSAStartup failed!");
      }
    }
  };
  static __init__ g_init;
}

#else
#include <cstdlib>
#include <unistd.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/statfs.h>
#include <sys/statvfs.h>
#include <linux/magic.h>

#endif

#include "RTL/strdef.h"

namespace RTL  {
  static int exit_status;
  static bool s_RTL_exit_handler_print = true;
  static bool s_exitHandlerRunning = false;

  struct EXHDEF   {
    int   flink;
    int  (*exit_handler)(void*);
    void *exit_param;
    int  *exit_status;
  };

  struct ExitHandler : public vector<EXHDEF>  {
    ExitHandler();
    ~ExitHandler();
    static void execute();
    static vector<EXHDEF>& exitHandlers();
    static bool isActive() {  return s_exitHandlerRunning; }
  };
}

#ifdef USE_PTHREADS
#include <unistd.h>
#include <signal.h>
#include <mutex>
#define ERROR_SUCCESS 0
std::string RTL::errorString(int status)  {
  if ( status != 0 )   {
#if 0
    static std::mutex s_err_lock;
    std::lock_guard<std::mutex> lock(s_err_lock);
    return string(::strerror(status));
#endif
    return std::make_error_code(std::errc(status)).message();
  }
  return "success";
}

namespace RTL {
  namespace {
    static char rtl_processName[256] = "";
    static char rtl_dataInterface[64] = "";
    static char rtl_nodeName[64] = "";
    static bool rtl_exit_handler_active = false;
    static bool rtl_sleep_on_fatal      = false;
    static bool rtl_gdb_on_fatal        = false;
    static bool rtl_have_backtrace      = true;
  }
  /**@class ExitSignalHandler
   *
   * Small class to manipulate default signal handling
   *
   * @author M.Frank
   */
  class ExitSignalHandler {
  protected:
    struct sig_entry_t  {
      void* user_context  { nullptr };
      lib_rtl_sig_handler_t user_handler { nullptr };
    };
    struct sig_handler_t  {
      string name { };
      struct sigaction old_action { };
      struct sigaction handler_action { };
      vector<sig_entry_t> user_handlers  { };
    };
    typedef map<int, sig_handler_t> SigMap;
    char    m_gdb_command[256];

    SigMap  m_map;
    /// Default constructor
    ExitSignalHandler();
    /// Default destructor
    ~ExitSignalHandler();

  public:
    static ExitSignalHandler& instance();
    /// Initialize the exit handler. Subscribe to default signals
    void init();
    /// Install handler for a single signal
    void install(int num, const string& name, struct sigaction& action);
    /// Invoke gdb backtrace
    void gdb_backtrace();
    /// Subscribe to a given signal with a user context and a user handler. The context MUST be unique!
    int subscribe(int signum, void* user_context, lib_rtl_sig_handler_t handler);
    /// Unsubscribe from a given signal with a user context identifier
    int unsubscribe(int signum, void* user_context);
    /// (Re-apply signal handlers in case they got overwritten
    int apply_handlers();

    /// Static handler callback for system signal handler
    static void handler(int signum, siginfo_t *info,void * );
    /// Print backtrace
    static void back_trace(int signum);
  };
  static ExitSignalHandler& s_RTL_ExitSignalHandler = ExitSignalHandler::instance();
}
extern "C" void* lib_rtl_exithandler_instance() {
  return &RTL::s_RTL_ExitSignalHandler;
}

/// Default constructor
RTL::ExitSignalHandler::ExitSignalHandler()  {
  rtl_sleep_on_fatal = ::lib_rtl_getenv("RTL_SLEEP_ON_FATAL") != 0;
  rtl_gdb_on_fatal   = ::lib_rtl_getenv("RTL_GDB_ON_FATAL")   != 0;
  rtl_have_backtrace = ::lib_rtl_getenv("RTL_NO_BACKTRACE")   == 0;
  ::snprintf(m_gdb_command, sizeof(m_gdb_command),
	     "gdb --pid %d --ex \"set pagination off\" "
	     "--ex \"thread apply all bt\" --ex quit -batch",
	     ::lib_rtl_pid());
  if ( RTL::processName().empty() )  {  }
  init();
  const char* feature = ::lib_rtl_getenv("RTL_OUTPUT_BUFFERING");
  if ( feature )  {
    if ( 0 == ::strcasecmp(feature, "RTL_LINE_BUFFERING") )  {
      RTL::Logger::set_io_buffering(RTL::Logger::LINE_BUFFERING);
    }
    else if ( 0 == ::strcasecmp(feature, "RTL_FULL_BUFFERING") )  {
      RTL::Logger::set_io_buffering(RTL::Logger::FULL_BUFFERING);
    }
    else if ( 0 == ::strcasecmp(feature, "RTL_NO_BUFFERING") )  {
      RTL::Logger::set_io_buffering(RTL::Logger::NO_BUFFERING);
    }
  }
}

/// Default destructor
RTL::ExitSignalHandler::~ExitSignalHandler() {
}

RTL::ExitSignalHandler& RTL::ExitSignalHandler::instance() {
  static ExitSignalHandler inst;
  return inst;
}

/// Initialize the exit handler. Subscribe to default signals
void RTL::ExitSignalHandler::init()  {
  struct sigaction new_action;
  sigemptyset(&new_action.sa_mask);
  new_action.sa_handler   = 0;
  new_action.sa_sigaction = handler;
  new_action.sa_flags     = SA_SIGINFO;
  install(SIGILL,  "SIGILL",  new_action);
  //INSTALL_SIGNAL(SIGINT,   new_action);
  install(SIGTERM, "SIGTERM",  new_action);
  install(SIGHUP,  "SIGHUP",   new_action);

  // INSTALL_SIGNAL(SIGKILL,  new_action);
  install(SIGQUIT, "SIGQUIT",  new_action);
  install(SIGBUS,  "SIGBUS",   new_action);
  install(SIGXCPU, "SIGXCPU",  new_action);
  sigaddset(&new_action.sa_mask,SIGSEGV);
  sigaddset(&new_action.sa_mask,SIGABRT);
  sigaddset(&new_action.sa_mask,SIGFPE);
  install(SIGABRT, "SIGABRT",  new_action);
  install(SIGFPE,  "SIGFPE",   new_action);
  install(SIGSEGV, "SIGSEGV",  new_action);
#if 0
  new_action.sa_sigaction = 0;
  new_action.sa_flags     = 0;
  new_action.sa_handler   = SIG_IGN;
  install(SIGPIPE, "SIGPIPE",  new_action);
#endif
}

/// Subscribe to a given signal with a user context and a user handler. The context MUST be unique!
int RTL::ExitSignalHandler::subscribe(int signum, void* user_context, lib_rtl_sig_handler_t user_handler)   {
  //::lib_rtl_output(LIB_RTL_ALWAYS,"Subscribing to signal %d %d %d", signum,
  //	  	     !rtl_exit_handler_active ? 1 : 0, !ExitHandler::isActive() ? 1 : 0);
  if ( !rtl_exit_handler_active && !ExitHandler::isActive() )    {
    auto ihandler = m_map.find(signum);
    if ( ihandler == m_map.end() )   {
      char text[32];
      struct sigaction new_action;
      sigemptyset(&new_action.sa_mask);
      new_action.sa_handler   = 0;
      new_action.sa_sigaction = RTL::ExitSignalHandler::handler;
      new_action.sa_flags     = SA_SIGINFO;
      ::snprintf(text, sizeof(text),"%08X",signum);
      install(signum, text, new_action);
      ihandler = m_map.find(signum);
    }
    if ( ihandler != m_map.end() )   { // Should always be true
      sig_entry_t entry {user_context, user_handler};
      ihandler->second.user_handlers.emplace_back(entry);
      // ::lib_rtl_output(LIB_RTL_ALWAYS,"Subscribed handler %p to signal %d",
      //                  (void*)user_handler, signum);
      return 1;
    }
  }
  return 0;
}

/// Unsubscribe from a given signal with a user context identifier
int RTL::ExitSignalHandler::unsubscribe(int signum, void* user_context)   {
  if( !rtl_exit_handler_active && !ExitHandler::isActive() )    {
    auto ihandler = m_map.find(signum);
    if( ihandler != m_map.end() )   {
      auto & handlers = ihandler->second.user_handlers;
      for( auto it = handlers.begin(); it != handlers.end(); ++it )   {
	if( it->user_context == user_context )   {
	  handlers.erase(it);
	  return 1;
	}
      }
    }
  }
  return 0;
}

/// (Re-apply signal handlers in case they got overwritten
int RTL::ExitSignalHandler::apply_handlers()   {
  auto& imp = RTL::ExitSignalHandler::instance();
  struct sigaction old { };
  // ::lib_rtl_output(LIB_RTL_INFO, "Re-apply signal handlers");
  for( const auto& e : imp.m_map )  {
    ::sigaction (e.first, &e.second.handler_action, &old);
    // ::lib_rtl_output(LIB_RTL_DEBUG,
    //	     "Re-apply signal handler for %-10s [%3ld entries]",
    //	     e.second.name.c_str(), e.second.user_handlers.size());
  }
  return 1;
}

/// Install handler for a single signal
void RTL::ExitSignalHandler::install(int num, const string& name, struct sigaction& action) {
  auto& action_entry = m_map[num];
  int res = ::sigaction (num, &action, &action_entry.old_action);
  if( res != 0 ) {
    cout << "Failed to install exit handler for " << name << endl;
    return;
  }
  //cout << "Successfully installed handler for " << name << endl;
  action_entry.handler_action = action;
  action_entry.name = name;
}

/// Invoke gdb backtrace
void RTL::ExitSignalHandler::gdb_backtrace()  {
  ::system(m_gdb_command);
}

#ifdef _WIN32
/// Create simple backtrace
void RTL::ExitSignalHandler::back_trace(int ) {
}
#else
#include <execinfo.h>
/// Create simple backtrace
void RTL::ExitSignalHandler::back_trace(int /* signum */) {
  if ( rtl_gdb_on_fatal )   {
    instance().gdb_backtrace();
  }
  if ( rtl_have_backtrace )   {
    void *bt[256];
    char text[512];
    int bt_size = ::backtrace(bt, sizeof(bt) / sizeof(void *));
    size_t len = ::snprintf(text,sizeof(text),"\n[INFO] Process: '%s' (ExitSignalHandler) %s\n",
			    rtl_processName,"---------------------- Backtrace ----------------------\n");
    text[sizeof(text)-2] = '\n';
    text[sizeof(text)-1] = 0;
    ::write(STDERR_FILENO,text,len);
    len = ::snprintf(text,sizeof(text),"[INFO] Process: '%s' Number of elements in backtrace: %d\n", rtl_processName, bt_size);
    text[sizeof(text)-2] = '\n';
    text[sizeof(text)-1] = 0;
    ::write(STDERR_FILENO,text,len);
    ::backtrace_symbols_fd(bt, bt_size, STDERR_FILENO);
    for (int i = 0; i < bt_size; i++) {
      len = ::snprintf(text,sizeof(text),"[INFO] Process: '%s' (SignalHandler) %02d --> %p\n", rtl_processName, i, bt[i]);
      text[sizeof(text)-2] = '\n';
      text[sizeof(text)-1] = 0;
      ::write(STDERR_FILENO,text,len);
    }
  }
}
#endif

/// Static handler callback for system signal handler
void RTL::ExitSignalHandler::handler(int signum, siginfo_t *info, void *ptr) {
  SigMap& m = instance().m_map;
  SigMap::iterator iter_handler = m.find(signum);

  if( iter_handler != m.end() ) {
    auto hdlr = iter_handler->second.handler_action.sa_handler;
    func_cast<void (*)(int)> dsc0(hdlr);
    func_cast<void (*)(int,siginfo_t*, void*)> dsc(dsc0.ptr);

    /// Avoid recursive calls
    if ( dsc.fun == RTL::ExitSignalHandler::handler )  {
      dsc.fun = nullptr;
    }
    if ( rtl_exit_handler_active )  {
      return;
    }

    rtl_exit_handler_active = true;
    if( s_RTL_exit_handler_print ) {{
	char text[512];
	size_t len = ::snprintf(text,sizeof(text),
				"[FATAL] Process: '%s' (SignalHandler) RTL:Handled signal: %d [%s] Old action:%p Mem:%p Code:%08X\n",
				rtl_processName,signum,iter_handler->second.name.c_str(),dsc.ptr,info->si_addr,info->si_code);
	text[sizeof(text)-2] = '\n';
	text[sizeof(text)-1] = 0;
	::write(STDERR_FILENO,text,len);
	// Debugging hack, if enabled (default: NO)
	if( rtl_sleep_on_fatal )  {
	  bool _s_sleep = true;
	  len = ::snprintf(text,sizeof(text),
			   "[FATAL] Process: '%s' (SignalHandler) Sleeping for debugging....%s\n",
			   rtl_processName, _s_sleep ? "YES" : "NO");
	  text[sizeof(text)-2] = '\n';
	  text[sizeof(text)-1] = 0;
	  ::write(STDERR_FILENO,text,len);
	  while( _s_sleep ) lib_rtl_sleep(100000);
	}
      }
      if( !iter_handler->second.user_handlers.empty() )    {
	auto& handlers = iter_handler->second.user_handlers;
	for( auto ih = handlers.rbegin(); ih != handlers.rend(); ++ih )   {
	  if( ih->user_handler )  {
	    int ret = (*(ih->user_handler))(ih->user_context, signum, info, ptr);
	    if( ret == LIB_RTL_SIGNAL_HANDLED )   {
	      rtl_exit_handler_active = false;
	      return;
	    }
	  }
	}
      }
      if( signum == SIGSEGV || signum == SIGBUS || signum == SIGILL || signum == SIGABRT )  {
        back_trace(signum);
      }
      else if( info->si_signo == SIGSEGV || info->si_signo == SIGBUS || info->si_signo == SIGILL || info->si_signo == SIGABRT )  {
        back_trace(info->si_signo);
      }
    }
    if( signum == SIGINT || signum == SIGHUP || signum == SIGFPE || signum == SIGPIPE || signum == SIGTERM || signum == SIGQUIT ) {
      if( dsc.fun && (dsc0.fun != SIG_IGN) )
        dsc.fun(signum, info, ptr);
      else if( signum == SIGHUP )
	::_exit(signum);
      if( signum == SIGTERM || signum == SIGQUIT )  {
	RTL::ExitHandler::execute();
	::_exit(signum);
      }
    }
    else if( signum == SIGSEGV && hdlr && hdlr != SIG_IGN && hdlr != SIG_DFL ) {
      // Need to ignore prior crash handlers: the ROOT exit handler fails!
      RTL::ExitHandler::execute();
      ::_exit(0);
    }
    else if( hdlr && hdlr != SIG_IGN && dsc.fun )  {
      RTL::ExitHandler::execute();
      dsc.fun(signum, info, ptr);
    }
    else if( hdlr == SIG_DFL && !rtl_exit_handler_active ) {
      RTL::ExitHandler::execute();
      ::_exit(0);
    }
    rtl_exit_handler_active = false;
    return;
  }
  RTL::ExitHandler::execute();
  rtl_exit_handler_active = false;
}

#elif _WIN32

#include <windows.h>
#include <process.h>

std::string RTL::errorString(int status)  {
  static char s[1024] = "No error reporting implemented";
  static int len = sizeof(s)-1;
  void* lpMessageBuffer;
  ::FormatMessage( 
                  FORMAT_MESSAGE_ALLOCATE_BUFFER |  FORMAT_MESSAGE_FROM_SYSTEM,
                  0,
                  status,
                  MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), //The user default language
                  (LPTSTR) &lpMessageBuffer,
                  0,
                  0 );
  if ( lpMessageBuffer )  {
    strncpy(s, (const char*)lpMessageBuffer, len);
    s[len] = 0;
    size_t l = strlen(s);
    if ( l > 1 )  {
      s[l-1] = 0;
      if ( !::isalnum(s[l-2]) ) s[l-2]=0;
    }
    ::LocalFree( lpMessageBuffer ); 
  }
  else {
    ::snprintf(s,sizeof(s),"RTL Error: Unknown error code: %08X",status);
  }
  return s;
}
#endif


/// Subscribe to a given signal with a user context and a user handler. The context MUST be unique!
extern "C" int lib_rtl_subscribe_signal(int signum, void* user_context, lib_rtl_sig_handler_t handler)   {
  return RTL::ExitSignalHandler::instance().subscribe(signum, user_context, handler);
}

/// Unsubscribe from a given signal with a user context identifier
extern "C" int lib_rtl_unsubscribe_signal(int signum, void* user_context)   {
  return RTL::ExitSignalHandler::instance().unsubscribe(signum, user_context);
}

/// (Re-apply signal handlers in case they got overwritten
extern "C" int lib_rtl_apply_signal_handlers()   {
  return RTL::ExitSignalHandler::instance().apply_handlers();
}

/// Set signal handling output level
extern "C" void lib_rtl_signal_log(int value) {
  RTL::s_RTL_exit_handler_print = (value!=0);
}

std::string RTL::errorString()  {
  return errorString(errno);
}

RTL::ExitHandler::ExitHandler() {
}

RTL::ExitHandler::~ExitHandler() {
  execute();
}

extern "C" int lib_rtl_event_exithandler();
extern "C" int lib_rtl_lock_exithandler();
extern "C" int lib_rtl_gbl_exithandler();
void RTL::ExitHandler::execute()  {
  if ( !s_exitHandlerRunning )  {
    s_exitHandlerRunning = true;
    lib_rtl_install_printer(nullptr, nullptr);
    const vector<EXHDEF>& v = exitHandlers();
    for (vector<EXHDEF>::const_reverse_iterator i=v.rbegin(); i != v.rend(); ++i)  {
      const EXHDEF& hdlr = *i;
      if ( hdlr.exit_handler )  {
        (*hdlr.exit_handler)(hdlr.exit_param);
      }
    }
    exitHandlers().clear();
    lib_rtl_event_exithandler();
    lib_rtl_lock_exithandler();
    lib_rtl_gbl_exithandler();
  }
}

vector<RTL::EXHDEF>& RTL::ExitHandler::exitHandlers() {
  static ExitHandler s_exitHandlers;
  return s_exitHandlers;
}

/// Access to error code from socket library
extern "C" int lib_rtl_socket_error()  {
#ifdef _WIN32
  return ::WSAGetLastError();
#else
  return errno;
#endif
}

extern "C" int lib_rtl_remove_rundown(lib_rtl_rundown_handler_t,void*)    {
  return 1;
}

extern "C" int lib_rtl_declare_rundown(lib_rtl_rundown_handler_t,void*)   {
  return 1;
}

extern "C" int lib_rtl_declare_exit(int (*hdlr)(void*), void* param)  {
#if defined(_WIN32) || defined(__linux)
  static bool first = true;
  if ( first )  {
    first = false;
    ::atexit(RTL::ExitHandler::execute);
  }
  RTL::EXHDEF h;
  h.exit_handler = hdlr;
  h.exit_param   = param;
  h.exit_status  = &RTL::exit_status;
  RTL::ExitHandler::exitHandlers().push_back(h);
  return 1;
#endif
}

extern "C" int lib_rtl_remove_exit(int (*hdlr)(void*), void* param) {
#if defined(_WIN32) || defined(__linux)
  RTL::ExitHandler::iterator i=RTL::ExitHandler::exitHandlers().begin();
  RTL::ExitHandler::iterator j=RTL::ExitHandler::exitHandlers().end();
  for(; i!=j; ++i)  {
    if ( (*i).exit_handler == hdlr && (*i).exit_param == param )  {
      RTL::ExitHandler::exitHandlers().erase(i);
      return 1;
    }
  }
  return 0;
#else
  return 1;
#endif
}

extern "C" void* lib_rtl_alloc_int_pointer_map()   {
  return new map<int,void*>;
}

/// Invoke debugger in given process to show backtrace of all threads (default: self)
extern "C" int lib_rtl_backtrace(int pid)    {
  char gdb_command[256];
  char line[132];
  ::snprintf(line, sizeof(line), "+---------------------------------------+\n");
  ::snprintf(gdb_command, sizeof(gdb_command),
	     "gdb --pid %d --ex \"set pagination off\" "
	     "--ex \"thread apply all bt\" --ex quit -batch", pid);
  ::write(STDOUT_FILENO, line, strlen(line));
  ::write(STDOUT_FILENO, gdb_command, strlen(gdb_command));
  ::write(STDOUT_FILENO, "\n", 2);
  ::write(STDOUT_FILENO, line, strlen(line));
  ::system(gdb_command);
  ::write(STDOUT_FILENO, line, strlen(line));
  return 1;
}

extern "C" int lib_rtl_start_debugger()    {
  RTL::ExitSignalHandler::instance().gdb_backtrace();
  return 1;
}

extern "C" int lib_rtl_wait_debugger(bool print_gdb_command)    {
  struct _trick_optimizer  {
    static int verify_debug(int *wait_for_debugger)   {
      if ( wait_for_debugger )  {
	int& value = *wait_for_debugger;
	if ( value == 0 )  {
	  ::printf("Exit debugger!\n");
	}
	return value;
      }
      return 1;
    }
  };
  if ( print_gdb_command )   {
    char text[1024], line[132];
    ::snprintf(line, sizeof(line), "+---------------------------------------+\n");
    ::snprintf(text, sizeof(text), "| gdb --pid %d\n", ::lib_rtl_pid());
    ::write(STDOUT_FILENO, line, strlen(line));
    ::write(STDOUT_FILENO, text, strlen(text));
    ::write(STDOUT_FILENO, line, strlen(line));
  }
  auto debug_thread = std::make_unique<std::thread>([]{
      int wait_for_debugger = 1;
      while( wait_for_debugger )  {
	::lib_rtl_sleep(100);
	if ( 0 == _trick_optimizer::verify_debug(&wait_for_debugger) )
	  break;
      }
    });
  debug_thread->join();
  return 1;
}

extern "C" int lib_rtl_default_return()  {
#if defined(_VMS) || defined(_WIN32)
  return 1;
#elif defined(__linux) || defined(_OSK)
  return 0;
#else
#endif
}

/// Disable intercepts
extern "C" int lib_rtl_disable_intercept() {
  return lib_rtl_default_return();
}

/// Enable intercpets
extern "C" int lib_rtl_enable_intercept()    {
  return lib_rtl_default_return();
}

extern "C" int lib_rtl_get_process_name(char* process, size_t len)  {
  const char *tmp;
  char buff[32], buff2[64];
  size_t resultant_length = sizeof(buff2);
  tmp = ::lib_rtl_getenv("UTGID");
  if ( !tmp ) tmp = ::lib_rtl_getenv("PROCESSNAME");
  if ( !tmp ) tmp = ::lib_rtl_getenv("PROCESS");
  if ( !tmp ) { ::snprintf(buff,sizeof(buff),"P%06d",lib_rtl_pid()); tmp=buff;}
  ::str_trim(tmp, buff2, &resultant_length);
  ::strncpy(process, buff2, len);
  return tmp ? strlen(tmp)+1>len ? 0 : 1 : 0;
}

extern "C" int lib_rtl_get_node_name(char* node, size_t len)  {
  char n[64];
  const char *tmp = ::lib_rtl_getenv("NODE");
#if defined(_WIN32)
  if ( !tmp ) tmp = ::lib_rtl_getenv("COMPUTERNAME");
  if ( !tmp && 0 == ::gethostname (n,sizeof(n)) ) tmp = n;
#elif defined(_OSK)
#else
  const char* dim_host = ::lib_rtl_getenv("DIM_HOST_NODE");
  if ( dim_host ) tmp = dim_host;
  else if ( 0 == ::gethostname (n,sizeof(n)) ) tmp = n;
#endif
  ::strncpy(node,tmp != 0 ? tmp : "UNKNOWN", len);
  return 1;
}

extern "C" int lib_rtl_get_datainterface_name(char* node, size_t len)  {
  const char *tmp = ::lib_rtl_getenv("DATAINTERFACE");
  if ( !tmp )  {
    char n[64], nn[70];
    if ( 0 == ::gethostname (n,sizeof(n)) )  {
      if ( strchr(n,'.') != 0 ) *strchr(n,'.') = 0;
      ::strncpy(nn,n,sizeof(nn));
      nn[sizeof(nn)-1] = 0;
      ::strncat(nn,"-d",3);
      hostent* h = ::gethostbyname(nn);
      if ( h ) {
        tmp = inet_ntoa(*(in_addr*)h->h_addr);
      }
      else {
        ::strncat(nn,"1",3);
        if ( (h=::gethostbyname(nn)) ) {
          tmp = inet_ntoa(*(in_addr*)h->h_addr);
        }
        else if ( (h=::gethostbyname(n)) ) {
          tmp = inet_ntoa(*(in_addr*)h->h_addr);
        }
      }
    }
  }
  ::strncpy(node,tmp != 0 ? tmp : "UNKNOWN", len);
  node[len-1] = 0;
  return 1;
}

/// Creates a pipe and executes a command.
extern "C" FILE* lib_rtl_pipe_open(const char* command, const char* mode) {
  return ::popen(command, mode);
}

/// Waits for new command processor and closes stream on associated pipe.
extern "C" int lib_rtl_pipe_close(FILE* stream) {
  return ::pclose(stream);
}

/// Safe wrapper around getenv
const char* lib_rtl_getenv(const char* value) {
  return value ? std::getenv(value) : 0;
}

/// Access total/free disk space on file system (linux:statvfs call)
extern "C" int lib_rtl_diskspace(const char* name, 
                                 unsigned long long int* blk_size,
                                 unsigned long long int* total_blk,
                                 unsigned long long int* availible_blk)
{
#ifdef _WIN32
  DWORD SectorsPerCluster,BytesPerSector,NumberOfFreeClusters,TotalNumberOfClusters;
  if ( ::GetDiskFreeSpace(name,&SectorsPerCluster,&BytesPerSector,
                          &NumberOfFreeClusters,&TotalNumberOfClusters) ) {
    *blk_size = BytesPerSector;
    *total_blk = SectorsPerCluster*TotalNumberOfClusters;
    *availible_blk = SectorsPerCluster*NumberOfFreeClusters;
    return 1;
  }
#else
  static unsigned long s_root_file_system_id = ~0x0L;
  struct statfs stat_slash;
  struct statvfs disk;
  if ( s_root_file_system_id == (unsigned long)~0x0L )   {
    if ( 0 == ::statvfs("/",&disk) ) {
      ::statfs("/",&stat_slash);
      s_root_file_system_id = (unsigned long)disk.f_fsid;
    }
  }
  if ( 0 == ::statvfs(name,&disk) ) {
    // Check file system id. Only accept root fs if name="/"
    if ( disk.f_fsid != s_root_file_system_id || name[1] == 0 )  {
      *blk_size = disk.f_bsize;
      *total_blk = long(float(disk.f_blocks)*float(disk.f_frsize)/float(disk.f_bsize));
      *availible_blk = disk.f_bfree;
      return 1;
    }
    struct statfs stat_disk;
    if ( 0 == ::statfs(name,&stat_disk) )  {
      if ( stat_disk.f_type == NFS_SUPER_MAGIC )  {
	if ( stat_disk.f_bfree != stat_slash.f_bfree || 
	     stat_disk.f_bavail != stat_slash.f_bavail )  {
	  *blk_size = stat_disk.f_bsize;
	  *total_blk = long(float(stat_disk.f_blocks)*float(stat_disk.f_frsize)/float(stat_disk.f_bsize));
	  *availible_blk = stat_disk.f_bfree;
	  return 1;
	}
      }
    }
    errno = ENOENT; // Set errno to non-exiting directory!
  }
  *blk_size = -1;
  *total_blk = 0;
  *availible_blk = 0;
#endif
  return 0;
}

/// Access total/free disk space on file system (linux:statvfs call)
int lib_rtl_diskspace_access(const char* name, unsigned long long int* blk_size,
			     unsigned long long int* total_blk,
			     unsigned long long int* availible_blk,
			     int mode)   {
  int sc = lib_rtl_diskspace(name, blk_size, total_blk, availible_blk);
  if ( lib_rtl_is_success(sc) )  {
    return lib_rtl_access(name,mode);
  }
  return sc;
}

/// access checks it is ok to read, write, execute the file *name
/// mode is a bit-mask (1 read, 2 write, 4 execute, 8 file exists)
extern "C" int lib_rtl_access(const char *name, int mode)
{
#ifdef _WIN32
  // under windows AFAICS this requires using the security API so we just assume it's fine
  return 1;
#else
  int amode = 0;

  if (mode & 0x1) amode |= R_OK;
  if (mode & 0x2) amode |= W_OK;
  if (mode & 0x4) amode |= X_OK;
  if (mode & 0x8) amode |= F_OK; 	
  if (::access(name, amode)) return 0;
  return 1;
#endif
}

/// POSIX/ISO compiant wrapper around unlink
extern "C" int lib_rtl_unlink(const char* fname) {
#ifdef _WIN32
  return ::_unlink(fname);
#else
  return ::unlink(fname);
#endif
}

/// Read path associated to file name
extern "C" int lib_rtl_readlink(const char* fname, char* path, size_t path_len)   {
  int len = ::readlink(fname, path, path_len);
  if ( len != -1 )  {
    path[size_t(len) < path_len ? len : path_len-1] = 0;
    return 1;
  }
  return 0;
}

/// Read path according from file descriptor
extern "C" int lib_rtl_file_name(int fd, char* path, size_t path_len)   {
  char buf[128];
  ::snprintf(buf, sizeof(buf), "/proc/self/fd/%d", fd);
  return lib_rtl_readlink(buf, path, path_len);
}

extern "C" int rtl_test_main(int /* argc */, char** /* argv */)  {
  cout << "Executing empty test action ..... finished ......" << endl;
  return 1;
}

extern "C" const char* lib_rtl_timestr(const char* fmt, const time_t* tp)  {
  static char timestr[256];
  return ::lib_rtl_timestr_r(timestr,sizeof(timestr),fmt,tp);
}

extern "C" const char* lib_rtl_timestr_r(char* timestr, size_t timestrLen, const char* fmt, const time_t* tp)  {
  struct tm ti;
  time_t t;
  if ( 0 == tp )  {
    tp = &t;
    ::time(&t);
  }
  struct tm *now = ::localtime_r(tp,&ti);
  ::strftime(timestr,timestrLen,fmt,now);
  timestr[timestrLen-1] = 0;
  return timestr;
}

extern "C" const char* lib_rtl_gmtimestr(const char* fmt, const time_t* tp)  {
  static char timestr[256];
  return ::lib_rtl_gmtimestr_r(timestr,sizeof(timestr),fmt,tp);
}

extern "C" const char* lib_rtl_gmtimestr_r(char* timestr, size_t timestrLen, const char* fmt, const time_t* tp)  {
  struct tm ti;
  time_t t;
  if ( 0 == tp )  {
    tp = &t;
    ::time(&t);
  }
  struct tm *now = ::gmtime_r(tp,&ti);
  ::strftime(timestr,timestrLen,fmt,now);
  timestr[timestrLen-1] = 0;
  return timestr;
}

/// Enable or disable some debugging features
extern "C" int lib_rtl_enable(const char* feature, int on_off)  {
  if ( feature )   {
    if ( 0 == ::strcasecmp(feature, "RTL_GDB_ON_FATAL") )  {
      RTL::rtl_gdb_on_fatal = (on_off != 0);
      on_off ? ::setenv(feature, "ON", 1) : ::unsetenv(feature);
    }
    else if ( 0 == ::strcasecmp(feature, "RTL_BACKTRACE") )  {
      RTL::rtl_have_backtrace = (on_off != 0);
      on_off ? ::setenv(feature, "ON", 1) : ::unsetenv(feature);
    }
    else if ( 0 == ::strcasecmp(feature, "RTL_NO_BACKTRACE") )  {
      RTL::rtl_have_backtrace = (on_off == 0);
      on_off == 0 ? ::setenv(feature, "ON", 1) : ::unsetenv(feature);
    }
    else if ( 0 == ::strcasecmp(feature, "RTL_SLEEP_ON_FATAL") )  {
      RTL::rtl_sleep_on_fatal = (on_off != 0);
      on_off ? ::setenv(feature, "ON", 1) : ::unsetenv(feature);
    }
    else if ( 0 == ::strcasecmp(feature, "RTL_LINE_BUFFERING") )  {
      ::setenv("RTL_OUTPUT_BUFFERING","RTL_LINE_BUFFERING",1);
      RTL::Logger::set_io_buffering(RTL::Logger::LINE_BUFFERING);
    }
    else if ( 0 == ::strcasecmp(feature, "RTL_FULL_BUFFERING") )  {
      ::setenv("RTL_OUTPUT_BUFFERING","RTL_FULL_BUFFERING",1);
      RTL::Logger::set_io_buffering(RTL::Logger::FULL_BUFFERING);
    }
    else if ( 0 == ::strcasecmp(feature, "RTL_NO_BUFFERING") )  {
      ::setenv("RTL_OUTPUT_BUFFERING","RTL_NO_BUFFERING",1);
      RTL::Logger::set_io_buffering(RTL::Logger::NO_BUFFERING);
    }
    else if ( 0 == ::strcasecmp(feature, "RESET") )  {
      RTL::RTL_reset();
    }
    else if ( 0 == ::strcasecmp(feature, "LIST") )  {
      char text[] =
	" RTL_LINE_BUFFERING \n"
	" RTL_FULL_BUFFERING \n"
	" RTL_NO_BUFFERING   \n"
	" RTL_SLEEP_ON_FATAL \n"
	" RTL_GDB_ON_FATAL   \n"
	" RTL_BACKTRACE      \n"
	" RTL_NO_BACKTRACE   \n\n";
	::write(STDOUT_FILENO, text, sizeof(text));
    }
    else  {
      throw std::runtime_error("Invalid RTL feature setting requested: "+std::string(feature));
    }
    return 1;
  }
  throw std::runtime_error("Invalid RTL feature setting requested (null) ");
}

extern "C" int lib_rtl_enable_backtrace()  {
  ::lib_rtl_enable("RTL_GDB_ON_FATAL",1);
  ::lib_rtl_enable("RTL_LINE_BUFFERING",1);
  return 1;
}

extern "C" int lib_rtl_reset()   {
  RTL::RTL_reset();
  return 1;
}

namespace RTL {
  static string s_processName, s_dataInterfaceName, s_nodeName, s_nodeNameShort;

  void RTL_reset() {
    s_processName = s_dataInterfaceName = s_nodeName = s_nodeNameShort = "";
    rtl_processName[0] = 0;
  }
  void RTL_init_sigs() {
#ifndef _WIN32
    RTL::ExitSignalHandler::instance().init();
#endif
  }
  const string& processName()  {
    if ( s_processName.empty() )  {
      ::lib_rtl_get_process_name(rtl_processName, sizeof(rtl_processName));
      s_processName = rtl_processName;
    }
    return s_processName;
  }
  const string& dataInterfaceName()  {
    if ( s_dataInterfaceName.empty() )  {
      ::lib_rtl_get_datainterface_name(rtl_dataInterface, sizeof(rtl_dataInterface));
      s_dataInterfaceName = rtl_dataInterface;
    }
    return s_dataInterfaceName;
  }
  const string& nodeName()  {
    if ( s_nodeName.empty() )  {
      ::lib_rtl_get_node_name(rtl_nodeName,sizeof(rtl_nodeName));
      s_nodeName = rtl_nodeName;
    }
    return s_nodeName;
  }
  const string& nodeNameShort()  {
    if ( s_nodeNameShort.empty() )  {
      s_nodeNameShort = nodeName();
      if ( s_nodeNameShort.find(".") != string::npos )  {
        s_nodeNameShort = s_nodeNameShort.substr(0,s_nodeNameShort.find("."));
      }
    }
    return s_nodeNameShort;
  }

  /// Convert errno to proper STL error code using the system category
  std::error_code get_error(int error_value)    {
    return std::make_error_code(std::errc(error_value));
  }

  /// Convert errno to proper STL error code using the system category
  std::error_code get_error()    {
    return std::make_error_code(std::errc(errno));
  }

  /// Access current local time string
  string timestr(time_t tp, const char* fmt)    {
    struct tm ti;
    char str[256];
    if ( 0 == tp ) tp = ::time(0);
    struct tm *now = ::localtime_r(&tp, &ti);
    ::strftime(str, sizeof(str), fmt, now);
    str[sizeof(str)-1] = 0;
    return str;
  }

  /// Access current GMT time string
  string gmtimestr(time_t tp, const char* fmt)    {
    struct tm ti;
    char str[256];
    if ( 0 == tp ) tp = ::time(nullptr);
    struct tm *now = ::gmtime_r(&tp, &ti);
    ::strftime(str,sizeof(str),fmt,now);
    str[sizeof(str)-1] = 0;
    return str;
  }

  std::string fileFromLink(const std::string& link)   {
    char filePath[PATH_MAX];
    int res = lib_rtl_readlink(link.c_str(),filePath,sizeof(PATH_MAX));
    return lib_rtl_is_success(res) ? filePath : "";
  }

  std::string fileFromDescriptor(int fd)   {
    char filePath[PATH_MAX];
    int res = lib_rtl_file_name(fd,filePath,sizeof(filePath));
    return lib_rtl_is_success(res) ? filePath : "";
  }

  std::string signalName(int signum)    {
    switch(signum)   {
#define SIG_NAME(x)  case x: return #x ;
      SIG_NAME(SIGHUP);
      SIG_NAME(SIGINT);
      SIG_NAME(SIGQUIT);
      SIG_NAME(SIGILL);
      SIG_NAME(SIGTRAP);
      SIG_NAME(SIGABRT);
      SIG_NAME(SIGBUS);
      SIG_NAME(SIGFPE);
      SIG_NAME(SIGKILL);
      SIG_NAME(SIGUSR1);
      SIG_NAME(SIGSEGV);
      SIG_NAME(SIGUSR2);
      SIG_NAME(SIGPIPE);
      SIG_NAME(SIGALRM);
      SIG_NAME(SIGTERM);
      SIG_NAME(SIGSTKFLT);
      SIG_NAME(SIGCHLD);
      SIG_NAME(SIGCONT);
      SIG_NAME(SIGSTOP);
      SIG_NAME(SIGTSTP);
      SIG_NAME(SIGTTIN);
      SIG_NAME(SIGTTOU);
      SIG_NAME(SIGURG);
      SIG_NAME(SIGXCPU);
      SIG_NAME(SIGXFSZ);
      SIG_NAME(SIGVTALRM);
      SIG_NAME(SIGPROF);
      SIG_NAME(SIGWINCH);
      SIG_NAME(SIGPOLL);
      SIG_NAME(SIGPWR);
      SIG_NAME(SIGSYS);
    default:
      break;
    }
    if ( signum == SIGRTMIN )
      return "SIGRTMIN";
    else if ( signum == SIGRTMAX )
      return "SIGRTMAX";
    else if ( signum > SIGRTMIN && signum < SIGRTMAX )   {
      return "SIGRT_" + std::to_string(signum-SIGRTMIN);
    }
    return "SIGINVALID" + std::to_string(signum);;
  }
}
