//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
#include "RTL/rtl.h"
#include "RTL/strdef.h"
#include <sstream>
#include <cstdarg>
#include <cstring>
#include <cstdio>
#include <string>
#include <vector>
#include <list>


namespace   {

  int string_assign(const char* p, const char* fmt, ...)  {
    va_list marker;
    va_start( marker, fmt );     /* Initialize variable arguments. */
    std::string *s = va_arg(marker, std::string*);
    va_end(marker);
    if ( s ) {
      *s = p; 
      return 1;
    }
    return 0;
  }

  int rtl_getitem(const char* strval, void* value, const char* fmt, int (*deformat)(const char*, const char*,...))  {
    if ( value && fmt )  {
      (*deformat)(strval,fmt,value);
      return 1;
    }
    return 0;
  }

  int rtl_getopt(RTL::CLI& cli, const char* name, const char* strval, int nchars, void* value, const char* fmt, int (*deformat)(const char*, const char*,...))  {
    const char* loc = cli.getopt(name, nchars);
    if ( loc )  {
      cli.values[name] = loc;
      rtl_getitem(loc, value, fmt, deformat);
      return 1;
    }
    cli.values[name] = strval;
    return 0;
  }

  std::vector<std::pair<int,const char*> > rtl_getvector(RTL::CLI& cli, int first_arg, const char* name, int nchars)   {
    std::vector<std::pair<int, const char*> > ret;
    if ( name )  {
      for (int i=first_arg; i<cli.numberOfArguments(); ++i)  {
	const char* cptr = cli.argument(i);
	if ( *cptr == '-' || *cptr == '/' ) {
	  if ( cptr[1] == '?' ||
	       (cptr[1] == 'h' && cptr[2] == 'e') || 
	       (cptr[1] == 'H' && cptr[2] == 'E')) {
	    cli.call_help();
	  }
	  else if ( cptr[1] == name[0] && ::strncmp(name,cptr+1,nchars)==0 )  {
	    const char* loc = ::strchr_safe(cptr+1,'=');
	    if ( loc )  {	    
	      ret.push_back(std::make_pair(i, loc+1));
	    }
	    continue;
	  }
	}
      }
    }
    return ret;
  }

  template <typename T> 
  int rtl_get_pushable(RTL::CLI& cli, const char* name, int nchars, 
		       T& value, 
		       const char* fmt, 
		       int (*deformat)(const char*, const char*,...))
  {
    std::vector<std::pair<int,const char*> > ret = rtl_getvector(cli, 1, name, nchars);
    if ( ret.empty() )
      return (int)value.size();
    value.clear();
    for( const auto& i : ret )   {
      typename T::value_type val;
      rtl_getitem(i.second, &val, fmt, deformat);    
      value.push_back(val);
    }
    return (int)value.size();
  }
}

void RTL::CLI::print_args()   const  {
  ::fprintf(stderr, "\n    Args given: ");
  for(std::size_t i=0; i < m_argc; ++i)
    ::fprintf(stderr, "%s ", m_argv[i]);
  ::fprintf(stderr, "\n");	
}

void RTL::CLI::print_args(int argc, char** argv)  {
  ::fprintf(stderr, "\n    Args given: ");
  for(int i=0; i < argc; ++i)
    ::fprintf(stderr, "%s ", argv[i]);
  ::fprintf(stderr, "\n");	
}

void RTL::CLI::call_help()  const  {
  if ( m_help2 ) {
    (*m_help2)(m_argc, m_argv);
    print_args();
  }
  else if ( m_help ) {
    (*m_help)();
    print_args();
  }
  ::exit(EINVAL);
}

std::pair<int,const char*> 
RTL::CLI::getopt(int first_arg, const char* name, int nchars)   {
  if ( name )  {
    for (int i=first_arg; i<numberOfArguments(); ++i)  {
      const char* cptr = argument(i);
      if ( *cptr == '-' || *cptr == '/' ) {
	if ( cptr[1] == '?' ||
	     (cptr[1] == 'h' && cptr[2] == 'e') || 
	     (cptr[1] == 'H' && cptr[2] == 'E')) {
	  call_help();
	}
        else if ( cptr[1] == name[0] && ::strncmp(name,cptr+1,nchars)==0 )  {
          const char* loc = ::strchr_safe(cptr+1,'=');
          if ( loc )  {
            return std::make_pair(i, loc+1);
          }
          return std::make_pair(i,cptr + strlen(cptr));
        }
      }
    }
    auto i = values.find(name);
    if ( i != values.end() ) 
      return std::make_pair(m_argc, (*i).second.c_str());
    return std::make_pair(m_argc,(const char*)0);
  }
  call_help();
  return std::make_pair(m_argc,(const char*)0);
}

const char* RTL::CLI::getopt(const char* name, int nchars)  {
  auto ret = getopt(1, name, nchars);
  return ret.second;
}

template <> int RTL::CLI::getopt(const char* name, int nchars, char& value)   {
  std::stringstream str;
  str << value;
  return rtl_getopt(*this, name, str.str().c_str(), nchars, &value, "%c", ::sscanf);
}

template <> int RTL::CLI::getopt(const char* name, int nchars, unsigned char& value)   {
  std::stringstream str;
  str << value;
  return rtl_getopt(*this, name, str.str().c_str(), nchars, &value, "%c", ::sscanf);
}

template <> int RTL::CLI::getopt(const char* name, int nchars, short& value)   {
  std::stringstream str;
  str << value;
  return rtl_getopt(*this, name, str.str().c_str(), nchars, &value, "%d", ::sscanf);
}

template <> int RTL::CLI::getopt(const char* name, int nchars, unsigned short& value)    {
  std::stringstream str;
  str << value;
  return rtl_getopt(*this, name, str.str().c_str(), nchars, &value, "%d", ::sscanf);
}

template <> int RTL::CLI::getopt(const char* name, int nchars, int& value)   {
  std::stringstream str;
  str << value;
  return rtl_getopt(*this, name, str.str().c_str(), nchars, &value, "%d", ::sscanf);
}

template <> int RTL::CLI::getopt(const char* name, int nchars, unsigned int& value)    {
  std::stringstream str;
  str << value;
  return rtl_getopt(*this, name, str.str().c_str(), nchars, &value, "%d", ::sscanf);
}

template <> int RTL::CLI::getopt(const char* name, int nchars, long& value)      {
  std::stringstream str;
  str << value;
  return rtl_getopt(*this, name, str.str().c_str(), nchars, &value, "%ld", ::sscanf);
}

template <> int RTL::CLI::getopt(const char* name, int nchars, unsigned long& value)      {
  std::stringstream str;
  str << value;
  return rtl_getopt(*this, name, str.str().c_str(), nchars, &value, "%ld", ::sscanf);
}

template <> int RTL::CLI::getopt(const char* name, int nchars, float& value)       {
  std::stringstream str;
  str << value;
  return rtl_getopt(*this, name, str.str().c_str(), nchars, &value, "%f", ::sscanf);
}

template <> int RTL::CLI::getopt(const char* name, int nchars, double& value)      {
  std::stringstream str;
  str << value;
  return rtl_getopt(*this, name, str.str().c_str(), nchars, &value, "%f", ::sscanf);
}

template <> int RTL::CLI::getopt(const char* name, int nchars, std::string& value)       {
  std::stringstream str;
  str << value;
  return rtl_getopt(*this, name, str.str().c_str(), nchars, &value, "%s", ::string_assign);
}

template <> int RTL::CLI::getopt(const char* name, int nchars, std::vector<std::string>& value)       {
  return rtl_get_pushable(*this, name, nchars, value, "%s", ::string_assign);
}

template <> int RTL::CLI::getopt(const char* name, int nchars, std::vector<char>& value)       {
  return rtl_get_pushable(*this, name, nchars, value, "%d", ::sscanf);
}

template <> int RTL::CLI::getopt(const char* name, int nchars, std::vector<unsigned char>& value)       {
  return rtl_get_pushable(*this, name, nchars, value, "%d", ::sscanf);
}

template <> int RTL::CLI::getopt(const char* name, int nchars, std::vector<short>& value)       {
  return rtl_get_pushable(*this, name, nchars, value, "%d", ::sscanf);
}

template <> int RTL::CLI::getopt(const char* name, int nchars, std::vector<unsigned short>& value)       {
  return rtl_get_pushable(*this, name, nchars, value, "%d", ::sscanf);
}

template <> int RTL::CLI::getopt(const char* name, int nchars, std::vector<int>& value)       {
  return rtl_get_pushable(*this, name, nchars, value, "%d", ::sscanf);
}

template <> int RTL::CLI::getopt(const char* name, int nchars, std::vector<unsigned int>& value)       {
  return rtl_get_pushable(*this, name, nchars, value, "%d", ::sscanf);
}

template <> int RTL::CLI::getopt(const char* name, int nchars, std::vector<long>& value)       {
  return rtl_get_pushable(*this, name, nchars, value, "%ld", ::sscanf);
}

template <> int RTL::CLI::getopt(const char* name, int nchars, std::vector<unsigned long>& value)       {
  return rtl_get_pushable(*this, name, nchars, value, "%ld", ::sscanf);
}

template <> int RTL::CLI::getopt(const char* name, int nchars, std::vector<float>& value)       {
  return rtl_get_pushable(*this, name, nchars, value, "%f", ::sscanf);
}

template <> int RTL::CLI::getopt(const char* name, int nchars, std::vector<double>& value)       {
  return rtl_get_pushable(*this, name, nchars, value, "%f", ::sscanf);
}

template <> int RTL::CLI::getopt(const char* name, int nchars, std::list<std::string>& value)       {
  return rtl_get_pushable(*this, name, nchars, value, "%s", ::string_assign);
}

template <> int RTL::CLI::getopt(const char* name, int nchars, std::list<char>& value)       {
  return rtl_get_pushable(*this, name, nchars, value, "%d", ::sscanf);
}

template <> int RTL::CLI::getopt(const char* name, int nchars, std::list<unsigned char>& value)       {
  return rtl_get_pushable(*this, name, nchars, value, "%d", ::sscanf);
}

template <> int RTL::CLI::getopt(const char* name, int nchars, std::list<short>& value)       {
  return rtl_get_pushable(*this, name, nchars, value, "%d", ::sscanf);
}

template <> int RTL::CLI::getopt(const char* name, int nchars, std::list<unsigned short>& value)       {
  return rtl_get_pushable(*this, name, nchars, value, "%d", ::sscanf);
}

template <> int RTL::CLI::getopt(const char* name, int nchars, std::list<int>& value)       {
  return rtl_get_pushable(*this, name, nchars, value, "%d", ::sscanf);
}

template <> int RTL::CLI::getopt(const char* name, int nchars, std::list<unsigned int>& value)       {
  return rtl_get_pushable(*this, name, nchars, value, "%d", ::sscanf);
}

template <> int RTL::CLI::getopt(const char* name, int nchars, std::list<long>& value)       {
  return rtl_get_pushable(*this, name, nchars, value, "%ld", ::sscanf);
}

template <> int RTL::CLI::getopt(const char* name, int nchars, std::list<unsigned long>& value)       {
  return rtl_get_pushable(*this, name, nchars, value, "%ld", ::sscanf);
}

template <> int RTL::CLI::getopt(const char* name, int nchars, std::list<float>& value)       {
  return rtl_get_pushable(*this, name, nchars, value, "%f", ::sscanf);
}

template <> int RTL::CLI::getopt(const char* name, int nchars, std::list<double>& value)       {
  return rtl_get_pushable(*this, name, nchars, value, "%f", ::sscanf);
}
