//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
#define RTL_IMPLEMENTATION
#include <map>
#include <memory>
#include <cerrno>
#include <cstdio>
#include <cstdlib>
#include <string>
#include "rtl_internal.h"
#include "fcntl.h"
#include <system_error>

#if defined(__linux)
#include "unistd.h"
#include <sys/mman.h>
#elif defined(_WIN32)
#include <windows.h>
#endif

using namespace RTL;
static auto s_map = std::make_unique<lib_rtl_gbl_map_t>();
lib_rtl_gbl_map_t& RTL::allSections() {
  return *s_map.get();
}

extern "C" int lib_rtl_gbl_exithandler() {
  if ( s_map.get() )  {
    lib_rtl_gbl_map_t m = allSections();
    lib_rtl_gbl_map_t::iterator i = m.begin();
    for( ; i != m.end(); ++i ) {
      lib_rtl_delete_section((*i).second);
    }
    delete s_map.release();
  }
  return 1;
}

/// Create named global section
int lib_rtl_create_section(const char* sec_name, std::size_t size, lib_rtl_gbl_t* address, int flags) {
  auto h = std::make_unique<lib_rtl_gbl_desc>();
  ::snprintf(h->name,sizeof(h->name),"/%s",sec_name);
  h->addaux  = h.get();
  h->flags   = flags;
  *address   = 0;
  //::lib_rtl_output(LIB_RTL_DEBUG,"Create global section %s of size:%d\n",h->name, h->size);
#if defined(__linux)
  long pgSize  = ::sysconf(_SC_PAGESIZE);
  long siz     = (size/pgSize)*pgSize + (((size%pgSize)==0) ? 0 : pgSize);  //  multiple of page size
  bool keep    = (flags&LIB_RTL_GBL_KEEP) != 0;
  int sysprot  = 0;
  int sysflags = 0;
  if ( flags&LIB_RTL_GBL_READ    ) sysprot  |= PROT_READ;
  if ( flags&LIB_RTL_GBL_WRITE   ) sysprot  |= PROT_WRITE;
  if ( flags&LIB_RTL_GBL_EXEC    ) sysprot  |= PROT_EXEC;
  if ( flags&LIB_RTL_GBL_FIXED   ) sysflags |= MAP_FIXED;
  if ( flags&LIB_RTL_GBL_LOCKED  ) sysflags |= MAP_LOCKED;
  if ( flags&LIB_RTL_GBL_SHARED  ) sysflags |= MAP_SHARED;
  if ( flags&LIB_RTL_GBL_PRIVATE ) sysflags |= MAP_PRIVATE;
  if ( flags&LIB_RTL_GBL_ANON    ) sysflags |= MAP_ANONYMOUS;

  h->size   = siz;
  h->fd = ::shm_open(h->name,O_RDWR|O_CREAT|O_EXCL,0666);
  if ( h->fd != -1 ) {
    void* mapping = 0;
    char path[1024];
    ::snprintf(path,sizeof(path),"/dev/shm%s",h->name);
    ::chmod(path,0666);
    ::ftruncate(h->fd, h->size);

    if ( flags&LIB_RTL_NUMA0 )
      mapping = ::lib_rtl_allocate(siz, LIB_RTL_NUMA0);
    else if ( flags&LIB_RTL_NUMA1 )
      mapping = ::lib_rtl_allocate(siz, LIB_RTL_NUMA1);
    else if ( flags&LIB_RTL_NUMA2 )
      mapping = ::lib_rtl_allocate(siz, LIB_RTL_NUMA2);
    else if ( flags&LIB_RTL_NUMA3 )
      mapping = ::lib_rtl_allocate(siz, LIB_RTL_NUMA3);

    h->address = ::mmap (mapping, h->size, sysprot, sysflags, h->fd, 0);
    if ( h->address != MAP_FAILED && h->address != 0 )  {
      // Ensure that all this memory is placed at the proper
      if ( flags&LIB_RTL_NUMA0 )
	::lib_rtl_memmove(h->address, h->size, LIB_RTL_NUMA0);
      else if ( flags&LIB_RTL_NUMA1 )
	::lib_rtl_memmove(h->address, h->size, LIB_RTL_NUMA1);
      else if ( flags&LIB_RTL_NUMA2 )
	::lib_rtl_memmove(h->address, h->size, LIB_RTL_NUMA2);
      else if ( flags&LIB_RTL_NUMA3 )
	::lib_rtl_memmove(h->address, h->size, LIB_RTL_NUMA3);

      if ( !keep ) {
        allSections().insert(std::make_pair(h->name,h.get()));
      }
      ::close(h->fd);
      h->fd = -1;
      *address = h.release();
      return 1;
    }
    ::close(h->fd);
    h->fd = 0;
  }
  // ::shm_unlink(h->name);
#elif defined(_WIN32)
  long siz  = (size/4096)*4096 + (((size%4096)==0) ? 0 : 4096);  //  multiple of page size
  // Setup inherited security attributes (FIXME: merge somewhere else)
  SECURITY_ATTRIBUTES   sa = {sizeof(SECURITY_ATTRIBUTES), 0, true};
  h->size   = siz;
  h->addaux = ::CreateFileMapping(INVALID_HANDLE_VALUE,&sa,PAGE_READWRITE,0,siz,h->name);
  if (h->addaux != 0 && ::GetLastError() == ERROR_ALREADY_EXISTS)   { 
    ::CloseHandle(h->addaux); 
    h->addaux = ::OpenFileMapping(FILE_MAP_ALL_ACCESS,FALSE,h->name);
  }
  if ( h->addaux )  {
    h->address = ::MapViewOfFile(h->addaux,FILE_MAP_ALL_ACCESS,0,0,siz);
    if ( h->address != 0 )  {
      h->fd = 0;
      *address = h.release();
      return 1;
    }
  }
#endif
  int err = lib_rtl_get_error();
  ::lib_rtl_output(LIB_RTL_ERROR,"Error creating section [%s]. Status %d [%s]\n",h->name,err,std::make_error_code(std::errc(err)).message().c_str());
  return 0;
}

/// Delete named global section
int lib_rtl_delete_section(lib_rtl_gbl_t h)    {
  std::unique_ptr<lib_rtl_gbl_desc> dsc{(lib_rtl_gbl_desc*)h};
  int sc = 0;
  if ( dsc.get() )  {
#ifdef __linux
    char path[1024];
    if ( dsc->fd >= 0 ) ::close(dsc->fd);
    if ( h->address )   {
      ::munmap(h->address,h->size);
      dsc->fd = -1;
      if ( dsc->flags&LIB_RTL_NUMA0 )
	::lib_rtl_free(dsc->address, h->size, LIB_RTL_NUMA0);
      else if ( dsc->flags&LIB_RTL_NUMA1 )
	::lib_rtl_free(dsc->address, h->size, LIB_RTL_NUMA1);
      else if ( dsc->flags&LIB_RTL_NUMA2 )
	::lib_rtl_free(dsc->address, h->size, LIB_RTL_NUMA2);
      else if ( dsc->flags&LIB_RTL_NUMA3 )
	::lib_rtl_free(dsc->address, h->size, LIB_RTL_NUMA3);
    }
    ::shm_unlink(dsc->name);
    lib_rtl_gbl_map_t& m = allSections();
    lib_rtl_gbl_map_t::iterator i=m.find(dsc->name);
    if ( i != m.end() ) {
      m.erase(i);
    }
    sc = 1;
    ::snprintf(path,sizeof(path),"/dev/shm%s",dsc->name);
    ::unlink(path);
#else
    sc = ::lib_rtl_unmap_section(h);
#endif
  }
  return sc;
}

/// Delete named global section
int lib_rtl_delete_named_section(const char* name)    {
  if ( name )  {
    lib_rtl_gbl_map_t& m = allSections();
    lib_rtl_gbl_map_t::iterator i=m.find(name);
    if ( i != m.end() ) {
      return ::lib_rtl_delete_section(i->second);
    }
    /// Global section is not in the repository.
    /// The only thing we can do is to delete the file it maps.
    int sc = ::shm_unlink(name)==0 ? 1 : 0;
    if ( sc == 0 ) return 1;
  }
  errno = EINVAL;
  return 0;
}

/// Map global section a a specific address
int lib_rtl_map_section(const char* sec_name, std::size_t size, lib_rtl_gbl_t* address, int flags)   {
  auto h = std::make_unique<lib_rtl_gbl_desc>();
  ::snprintf(h->name,sizeof(h->name),"/%s",sec_name);
  h->addaux = h.get();
  *address = 0;
  //::lib_rtl_output(LIB_RTL_DEBUG,"Map global section %s of size:%d\n",h->name, h->size);
#if defined(__linux)
  std::size_t pgSize   = ::sysconf(_SC_PAGESIZE);
  int sysprot  = 0;
  int sysflags = 0;
  if ( flags&LIB_RTL_GBL_READ    ) sysprot  |= PROT_READ;
  if ( flags&LIB_RTL_GBL_WRITE   ) sysprot  |= PROT_WRITE;
  if ( flags&LIB_RTL_GBL_EXEC    ) sysprot  |= PROT_EXEC;
  if ( flags&LIB_RTL_GBL_FIXED   ) sysflags |= MAP_FIXED;
  if ( flags&LIB_RTL_GBL_LOCKED  ) sysflags |= MAP_LOCKED;
  if ( flags&LIB_RTL_GBL_SHARED  ) sysflags |= MAP_SHARED;
  if ( flags&LIB_RTL_GBL_PRIVATE ) sysflags |= MAP_PRIVATE;
  if ( flags&LIB_RTL_GBL_ANON    ) sysflags |= MAP_ANONYMOUS;
  std::size_t required_size =
    std::size_t((size+pgSize-1)/pgSize)*pgSize;  //  multiple of page size
  h->fd = ::shm_open(h->name, O_RDWR,0666);
  h->size = required_size;
  if ( -1 == h->fd )  {
    ::shm_unlink(h->name);
    return 0;
  }
  struct stat st_buf;
  ::fstat(h->fd,&st_buf); // Must succeed, since file could be opened
  std::size_t actual_size = std::size_t(st_buf.st_size);
  if ( 0 == size || h->size != actual_size )  {
    h->size = actual_size;
    if ( actual_size < required_size )  {
      if ( 0 == ::ftruncate(h->fd, required_size) )   {
	h->size = required_size;
      }
    }
  }
  // Map whole section
  h->address = ::mmap (0, h->size, sysprot, sysflags, h->fd, 0);
  if ( h->address != MAP_FAILED && h->address != 0 )  {
    ::close(h->fd);
    h->fd = -1;
    *address = h.release();
    return 1;
  }
  // File was already successfully opened
  ::close(h->fd);
  return 0;
#elif defined(_WIN32)
  h->size   = (int((size+4095)/4096))*4096;
  h->addaux = ::OpenFileMapping(FILE_MAP_ALL_ACCESS,FALSE,h->name);
  if ( h->addaux )  {
    h->address = ::MapViewOfFile(h->addaux,FILE_MAP_ALL_ACCESS,0,0,size==0 ? 0 : h->size);
    if ( h->address != 0 )  {
      h->fd = 0;
      *address = h.release();
      return 1;
    }
  }
  int err = lib_rtl_get_error();
  ::lib_rtl_output(LIB_RTL_ERROR,"Error mapping section [%s]. Status %d [%s]\n",h->name,err,std::make_error_code(std::errc(err)).message().c_str());
  return 0;
#endif
}

/// Unmap global section: address is quadword: void*[2]
int lib_rtl_unmap_section(lib_rtl_gbl_t handle)   {
  if ( handle )  {
    std::unique_ptr<lib_rtl_gbl_desc> h{(lib_rtl_gbl_desc*)handle};
    //lib_rtl_gbl_desc* h = (lib_rtl_gbl_desc*)handle;
#if defined(__linux)
    int sc = ::munmap(h->address,h->size)==0 ? 1 : 0;
    if ( h->fd >= 0 ) {
      //static int cnt = 0;
      /* int ret = */ ::close(h->fd);
      /* ::lib_rtl_output(LIB_RTL_DEBUG,"%d Close FD:%d err=%d\n",++cnt,h->fd,ret); */
    }
    h->fd = -1;
    if ( h->address )   {
      if ( h->flags&LIB_RTL_NUMA0 )
	::lib_rtl_free(h->address, h->size, LIB_RTL_NUMA0);
      else if ( h->flags&LIB_RTL_NUMA1 )
	::lib_rtl_free(h->address, h->size, LIB_RTL_NUMA1);
      else if ( h->flags&LIB_RTL_NUMA2 )
	::lib_rtl_free(h->address, h->size, LIB_RTL_NUMA2);
      else if ( h->flags&LIB_RTL_NUMA3 )
	::lib_rtl_free(h->address, h->size, LIB_RTL_NUMA3);
    }
#elif defined(_WIN32)
    int sc = (::UnmapViewOfFile(h->address) == 0) ? 0 : 1;
    if ( 0 != sc ) ::CloseHandle(h->addaux);
#endif
    return sc;
  }
  return 0;
}

/// Flush global section to disk file
int lib_rtl_flush_section(lib_rtl_gbl_t handle)   {
  if ( handle )  {
    lib_rtl_gbl_desc* h = (lib_rtl_gbl_desc*)handle;
#if defined(_WIN32)
    DWORD sc = ::FlushViewOfFile(h->addaux,h->size);
    if ( sc == 0 )  {
      return 0;
    }
#elif defined(__linux)
    ::msync(h->address, h->size, MS_INVALIDATE|MS_SYNC);
#endif
  }
  return 1;
}
