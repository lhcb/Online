//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================

/// Framework include files
#include <RTL/hash.h>
#include <netinet/in.h>

/// C/C++ include files
#include <string>
#include <cstdint>


#define QUOTIENT  0x04c11db7
namespace  {
  class CRC32Table  {
  public:
    uint32_t m_data[256];
    CRC32Table()  {
      uint32_t crc;
      for (int i = 0; i < 256; i++)    {
	crc = i << 24;
	for (int j = 0; j < 8; j++)   {
	  if (crc & 0x80000000)
	    crc = (crc << 1) ^ QUOTIENT;
	  else
	    crc = crc << 1;
	}
	m_data[i] = htonl(crc);
      }
    }
    const uint32_t* data() const { return m_data; }
  };
}

namespace RTL   {

  /// We need it so often: one-at-time 32 bit hash function
  uint32_t hash32(const char* key) {
    uint32_t hash = 0;
    const int8_t* k = (const int8_t*)key;
    for (; *k; k++) {
      hash += *k;
      hash += (hash << 10);
      hash ^= (hash >> 6);
    }
    hash += (hash << 3);
    hash ^= (hash >> 11); hash += (hash << 15);
    return hash;
  }

  /// Compute HASH32 checksum from string
  uint32_t hash32(const std::string& key) {
    return hash32(key.c_str());
  }

  /// Compute HASH32 checksum from buffer
  uint32_t hash32(const void* ptr, std::size_t len)  {
    uint32_t hash = 0;
    const int8_t* k = (const int8_t*)ptr;
    for (std::size_t i=0; i<len; ++i, ++k) {
      hash += *k;
      hash += (hash << 10);
      hash ^= (hash >> 6);
    }
    hash += (hash << 3);
    hash ^= (hash >> 11);
    hash += (hash << 15);
    return hash;
  }

  /// Compute 16 bit fletcher checksum from buffer
  uint16_t fletcher16(const void* buffer, std::size_t len) {
    uint32_t c0, c1;
    const uint8_t *data = (const uint8_t *)buffer;
    /*  Found by solving for c1 overflow: */
    /* n > 0 and n * (n+1) / 2 * (2^8-1) < (2^32-1). */
    for (c0 = c1 = 0; len > 0; ) {
      size_t blocklen = len;
      if (blocklen > 5802) {
	blocklen = 5802;
      }
      len -= blocklen;
      do {
	c0 = c0 + *data++;
	c1 = c1 + c0;
      } while (--blocklen);
      c0 = c0 % 255;
      c1 = c1 % 255;
    }
    return (c1 << 8 | c0);
  }

  /// Compute 32 bit fletcher checksum from buffer
  uint32_t fletcher32(const void* buffer, std::size_t len) {
    uint32_t c0, c1;
    const uint16_t *data = (const uint16_t *)buffer;
    len = (len + 1) & ~1;      /* Round up len to words */

    /* We similarly solve for n > 0 and n * (n+1) / 2 * (2^16-1) < (2^32-1) here. */
    /* On modern computers, using a 64-bit c0/c1 could allow a group size of 23726746. */
    for (c0 = c1 = 0; len > 0; ) {
      size_t blocklen = len;
      if (blocklen > 360*2) {
	blocklen = 360*2;
      }
      len -= blocklen;
      do {
	c0 = c0 + *data++;
	c1 = c1 + c0;
      } while ((blocklen -= 2));
      c0 = c0 % 65535;
      c1 = c1 % 65535;
    }
    return (c1 << 16 | c0);
  }

  inline uint32_t murmur_32_scramble(uint32_t k) {
    k *= 0xcc9e2d51;
    k = (k << 15) | (k >> 17);
    k *= 0x1b873593;
    return k;
  }
  /// Compute 32 bit Murmur3 hash from checksum
  uint32_t murmur3_32(uint32_t seed, const void* data, size_t len)  {
    const uint8_t* key = (const uint8_t*)data;
    uint32_t h = seed;
    uint32_t k;
    /* Read in groups of 4. */
    for (size_t i = len >> 2; i; i--) {
      // Here is a source of differing results across endiannesses.
      // A swap here has no effects on hash properties though.
      k = *(uint32_t*)key;
      key += sizeof(uint32_t);
      h ^= murmur_32_scramble(k);
      h = (h << 13) | (h >> 19);
      h = h * 5 + 0xe6546b64;
    }
    /* Read the rest. */
    k = 0;
    for (size_t i = len & 3; i; i--) {
      k <<= 8;
      k |= key[i - 1];
    }
    // A swap is *not* necessary here because the preceding loop already
    // places the low bytes in the low places according to whatever endianness
    // we use. Swaps only apply when the memory is copied in a chunk.
    h ^= murmur_32_scramble(k);
    /* Finalize. */
    h ^= len;
    h ^= h >> 16;
    h *= 0x85ebca6b;
    h ^= h >> 13;
    h *= 0xc2b2ae35;
    h ^= h >> 16;
    return h;
  }
  /// Compute 32 bit Murmur3 hash from checksum
  uint32_t murmur3_32(const void* data, size_t len)  {
    return murmur3_32(0, data, len);
  }
  
  /// Compute ADLER32 checksum from buffer
  uint32_t adler32(uint32_t adler, const void* ptr, std::size_t len)   {
    const char* buf = (const char*)ptr;
#define DO1(buf,i)  {s1 +=(uint8_t)buf[i]; s2 += s1;}
#define DO2(buf,i)  DO1(buf,i); DO1(buf,i+1);
#define DO4(buf,i)  DO2(buf,i); DO2(buf,i+2);
#define DO8(buf,i)  DO4(buf,i); DO4(buf,i+4);
#define DO16(buf)   DO8(buf,0); DO8(buf,8);

    static constexpr const uint32_t BASE = 65521;    /* largest prime smaller than 65536 */
    /* NMAX is the largest n such that 255n(n+1)/2 + (n+1)(BASE-1) <= 2^32-1 */
    static constexpr const uint32_t NMAX = 5550;
    uint32_t s1 = adler & 0xffff;
    uint32_t s2 = (adler >> 16) & 0xffff;
    int k;

    if (buf == NULL) return 1;

    while (len > 0) {
      k = len < NMAX ? (int)len : NMAX;
      len -= k;
      while (k >= 16) {
	DO16(buf);
	buf += 16;
	k -= 16;
      }
      if (k != 0) do {
	  s1 += (uint8_t)*buf++;
	  s2 += s1;
	} while (--k);
      s1 %= BASE;
      s2 %= BASE;
    }
    uint32_t result = (s2 << 16) | s1;
    return result;
  }

  /// Compute ADLER32 checksum from buffer with update possibility
  uint32_t adler32(const void* ptr, std::size_t len)   {
    return adler32(0, ptr, len);
  }
  
  // Only works for word aligned data and assumes that the data is an exact number of words
  // Copyright  1993 Richard Black. All rights are reserved.
  uint32_t crc32(const void *ptr, std::size_t len)    {
    static CRC32Table table;
    const int8_t   *data = (const int8_t*)ptr;
    const uint32_t *crctab = table.data();
    const uint32_t *p = (const uint32_t *)data;
    const uint32_t *e = (const uint32_t *)(data + len);
    if ( len < 4 || (uint64_t(data)%sizeof(uint32_t)) != 0 ) return ~0x0;
    uint32_t result = ~*p++;
    while( p < e )  {
#if defined(LITTLE_ENDIAN)
      result = crctab[result & 0xff] ^ result >> 8;
      result = crctab[result & 0xff] ^ result >> 8;
      result = crctab[result & 0xff] ^ result >> 8;
      result = crctab[result & 0xff] ^ result >> 8;
      result ^= *p++;
#else
      result = crctab[result >> 24] ^ result << 8;
      result = crctab[result >> 24] ^ result << 8;
      result = crctab[result >> 24] ^ result << 8;
      result = crctab[result >> 24] ^ result << 8;
      result ^= *p++;
#endif
    }
    return ~result;
  }

  /// Compute CRC16 CCITT checksum from buffer
  uint16_t crc16_unused (const void* ptr, std::size_t len) {
    //
    // TODO: CRC16 is unstable across platforms (x86 vs. arm).
    const uint8_t* data = (const uint8_t*)ptr;
    uint8_t  x;
    uint16_t crc = 0xFFFF;
    while ( len-- )  {
      x = crc >> 8 ^ *data++;
      x ^= (x >> 4);
      crc = (crc << 8) ^ ((uint16_t)(x << 12)) ^ ((uint16_t)(x <<5)) ^ ((uint16_t)x);
    }
    return crc;
  }
  
  uint16_t crc16_byte(uint16_t crc, const uint8_t data)  {
    /** CRC table for the CRC-16. The poly is 0x8005 (x^16 + x^15 + x^2 + 1) */
    static constexpr uint16_t const crc16_table[256] = {
      0x0000, 0xC0C1, 0xC181, 0x0140, 0xC301, 0x03C0, 0x0280, 0xC241,
      0xC601, 0x06C0, 0x0780, 0xC741, 0x0500, 0xC5C1, 0xC481, 0x0440,
      0xCC01, 0x0CC0, 0x0D80, 0xCD41, 0x0F00, 0xCFC1, 0xCE81, 0x0E40,
      0x0A00, 0xCAC1, 0xCB81, 0x0B40, 0xC901, 0x09C0, 0x0880, 0xC841,
      0xD801, 0x18C0, 0x1980, 0xD941, 0x1B00, 0xDBC1, 0xDA81, 0x1A40,
      0x1E00, 0xDEC1, 0xDF81, 0x1F40, 0xDD01, 0x1DC0, 0x1C80, 0xDC41,
      0x1400, 0xD4C1, 0xD581, 0x1540, 0xD701, 0x17C0, 0x1680, 0xD641,
      0xD201, 0x12C0, 0x1380, 0xD341, 0x1100, 0xD1C1, 0xD081, 0x1040,
      0xF001, 0x30C0, 0x3180, 0xF141, 0x3300, 0xF3C1, 0xF281, 0x3240,
      0x3600, 0xF6C1, 0xF781, 0x3740, 0xF501, 0x35C0, 0x3480, 0xF441,
      0x3C00, 0xFCC1, 0xFD81, 0x3D40, 0xFF01, 0x3FC0, 0x3E80, 0xFE41,
      0xFA01, 0x3AC0, 0x3B80, 0xFB41, 0x3900, 0xF9C1, 0xF881, 0x3840,
      0x2800, 0xE8C1, 0xE981, 0x2940, 0xEB01, 0x2BC0, 0x2A80, 0xEA41,
      0xEE01, 0x2EC0, 0x2F80, 0xEF41, 0x2D00, 0xEDC1, 0xEC81, 0x2C40,
      0xE401, 0x24C0, 0x2580, 0xE541, 0x2700, 0xE7C1, 0xE681, 0x2640,
      0x2200, 0xE2C1, 0xE381, 0x2340, 0xE101, 0x21C0, 0x2080, 0xE041,
      0xA001, 0x60C0, 0x6180, 0xA141, 0x6300, 0xA3C1, 0xA281, 0x6240,
      0x6600, 0xA6C1, 0xA781, 0x6740, 0xA501, 0x65C0, 0x6480, 0xA441,
      0x6C00, 0xACC1, 0xAD81, 0x6D40, 0xAF01, 0x6FC0, 0x6E80, 0xAE41,
      0xAA01, 0x6AC0, 0x6B80, 0xAB41, 0x6900, 0xA9C1, 0xA881, 0x6840,
      0x7800, 0xB8C1, 0xB981, 0x7940, 0xBB01, 0x7BC0, 0x7A80, 0xBA41,
      0xBE01, 0x7EC0, 0x7F80, 0xBF41, 0x7D00, 0xBDC1, 0xBC81, 0x7C40,
      0xB401, 0x74C0, 0x7580, 0xB541, 0x7700, 0xB7C1, 0xB681, 0x7640,
      0x7200, 0xB2C1, 0xB381, 0x7340, 0xB101, 0x71C0, 0x7080, 0xB041,
      0x5000, 0x90C1, 0x9181, 0x5140, 0x9301, 0x53C0, 0x5280, 0x9241,
      0x9601, 0x56C0, 0x5780, 0x9741, 0x5500, 0x95C1, 0x9481, 0x5440,
      0x9C01, 0x5CC0, 0x5D80, 0x9D41, 0x5F00, 0x9FC1, 0x9E81, 0x5E40,
      0x5A00, 0x9AC1, 0x9B81, 0x5B40, 0x9901, 0x59C0, 0x5880, 0x9841,
      0x8801, 0x48C0, 0x4980, 0x8941, 0x4B00, 0x8BC1, 0x8A81, 0x4A40,
      0x4E00, 0x8EC1, 0x8F81, 0x4F40, 0x8D01, 0x4DC0, 0x4C80, 0x8C41,
      0x4400, 0x84C1, 0x8581, 0x4540, 0x8701, 0x47C0, 0x4680, 0x8641,
      0x8201, 0x42C0, 0x4380, 0x8341, 0x4100, 0x81C1, 0x8081, 0x4040
    };
    return (crc >> 8) ^ crc16_table[(crc ^ data) & 0xff];
  }

  /// Compute CRC16 checksum from buffer with update possibility
  uint16_t crc16(uint16_t crc, const void* data, size_t len)    {
    const uint8_t *buffer = (const uint8_t*)data;
    while (len--)
      crc = crc16_byte(crc, *buffer++);
    return crc;
  }

  /// Compute CRC16 checksum from buffer
  uint16_t crc16(const void* data, size_t len)    {
    return crc16(0xFFFF, data, len);
  }

  /// Compute CRC8 checksum from buffer with update possibility
  uint8_t crc8(uint8_t c, const void* ptr, std::size_t len) {
    static constexpr const uint8_t crc8_table[] =
      { 0, 94,188,226, 97, 63,221,131,194,156,126, 32,163,253, 31, 65,
	157,195, 33,127,252,162, 64, 30, 95,  1,227,189, 62, 96,130,220,
	35,125,159,193, 66, 28,254,160,225,191, 93,  3,128,222, 60, 98,
	190,224,  2, 92,223,129, 99, 61,124, 34,192,158, 29, 67,161,255,
	70, 24,250,164, 39,121,155,197,132,218, 56,102,229,187, 89,  7,
	219,133,103, 57,186,228,  6, 88, 25, 71,165,251,120, 38,196,154,
	101, 59,217,135,  4, 90,184,230,167,249, 27, 69,198,152,122, 36,
	248,166, 68, 26,153,199, 37,123, 58,100,134,216, 91,  5,231,185,
	140,210, 48,110,237,179, 81, 15, 78, 16,242,172, 47,113,147,205,
	17, 79,173,243,112, 46,204,146,211,141,111, 49,178,236, 14, 80,
	175,241, 19, 77,206,144,114, 44,109, 51,209,143, 12, 82,176,238,
	50,108,142,208, 83, 13,239,177,240,174, 76, 18,145,207, 45,115,
	202,148,118, 40,171,245, 23, 73,  8, 86,180,234,105, 55,213,139,
	87,  9,235,181, 54,104,138,212,149,203, 41,119,244,170, 72, 22,
	233,183, 85, 11,136,214, 52,106, 43,117,151,201, 74, 20,246,168,
	116, 42,200,150, 21, 75,169,247,182,232, 10, 84,215,137,107, 53
      };
    const uint8_t *s = (const uint8_t*)ptr;
    while (len--) c = crc8_table[c ^ *s++];
    return c;
  }

  /// Compute CRC16 checksum from buffer
  uint8_t crc8(const void* data, size_t len)    {
    return crc8(0, data, len);
  }


  /// Compute 16 bit BSD checksum from buffer with update possibility
  uint16_t bsd16(uint16_t chksum, const void* ptr, std::size_t len) {
    const int8_t* data = (const int8_t*)ptr;
    int32_t checksum = chksum;  // The checksum mod 2^16.
    for(std::size_t i=0; i<len; ++i)   {
      int32_t ch = data[i];
      checksum = (checksum >> 1) + ((checksum & 1) << 15);
      checksum += ch;
      checksum &= 0xffff;       // Keep it within bounds.
    }
    return (0xFFFF&checksum);
  }
  /// Compute 16 bit BSD checksum from buffer
  uint16_t bsd16(const void* ptr, std::size_t len) {
    return bsd16(0, ptr, len);
  }
  
}
