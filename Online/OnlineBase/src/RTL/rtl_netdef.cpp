//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================

/// C/C++ include files
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <cerrno>
#include <cstring>
#include <unistd.h>

/// Framework include files
#include <RTL/netdef.h>

/// Bind the socket address to the first free port availible
extern "C" int net_bind_free_server_port(struct sockaddr_in* addr)   {
  struct sockaddr* add = (struct sockaddr*)addr;
  int sock = ::socket(AF_INET, SOCK_STREAM, 0);
  if ( sock > 0 )  {
    /// Set port number, using htons function to use proper byte order
    addr->sin_family = AF_INET;
    addr->sin_port = 0;
    // Set all bits of the padding field to 0
    ::memset(addr->sin_zero, 0, sizeof(addr->sin_zero));
    if ( 0 == ::bind(sock, add, sizeof(struct sockaddr)) )   {
      unsigned int addrlen = sizeof(struct sockaddr);
      if ( 0 == ::getsockname(sock, add, &addrlen) )  {
	if ( addr->sin_port < 8080 )  {
	  ::close(sock);
	  addr->sin_port = 0;
	  return net_bind_free_server_port(addr);
	}
	return sock;
      }
    }
    ::close(sock);
    return -1;
  }
  return sock;
}
  

/// Run-time-library namespace declaration
namespace RTL  {

  /// Get first free port number in the system
  std::pair<int, unsigned short> get_free_server_port()    {

    unsigned short port = -1;
    /// Try to bind port 0 to get the next free server port number:
    int sock = ::socket(AF_INET, SOCK_STREAM, 0);
    int err = 0;

    if ( sock > 0 )   {
      struct sockaddr_in addr;
      addr.sin_family = AF_INET;
      /// Set port number, using htons function to use proper byte order
      addr.sin_port = 0;
      // Set the IP address to desired host to connect to
      addr.sin_addr.s_addr = INADDR_ANY;
      // Set all bits of the padding field to 0
      ::memset(addr.sin_zero, 0, sizeof(addr.sin_zero));

      if ( 0 == ::bind(sock, (struct sockaddr *)&addr, sizeof(addr)) )   {
	unsigned int addrlen = sizeof(addr);
	if ( 0 == ::getsockname(sock, (struct sockaddr *)&addr, &addrlen) )  {
	  port = addr.sin_port;
	  if ( port < 8080 )  {
	    port = get_free_server_port().second;
	  }
	}
	else   {
	  err = errno;
	}
      }
      else if ( 0 != errno )   {
	err = errno;
      }
      else  {
	err = ENOTSOCK;
      }
      ::shutdown(sock, 2);
      ::close(sock);
      return { err, port };
    }
    return { 0 == errno ? ENFILE : errno, -1};
  }

  /// Get first free port numbers in the system
  std::pair<int, std::vector<unsigned short> > get_free_server_ports(std::size_t count)    {
    std::pair<int, std::vector<unsigned short> > result(0, {});
    std::vector<int> sockets;
    int attempts = 2*count;
    while( result.second.size() < count && --attempts >= 0 )   {
      /// Try to bind port 0 to get the next free server port number:
      int sock = ::socket(AF_INET, SOCK_STREAM, 0);

      if ( sock > 0 )   {
	struct sockaddr_in addr;
	addr.sin_family = AF_INET;
	/// Set port number, using htons function to use proper byte order
	addr.sin_port = 0;
	// Set the IP address to desired host to connect to
	addr.sin_addr.s_addr = INADDR_ANY;
	// Set all bits of the padding field to 0
	::memset(addr.sin_zero, 0, sizeof(addr.sin_zero));

	if ( 0 == ::bind(sock, (struct sockaddr *)&addr, sizeof(addr)) )   {
	  unsigned int addrlen = sizeof(addr);
	  if ( 0 == ::getsockname(sock, (struct sockaddr *)&addr, &addrlen) )  {
	    if ( addr.sin_port < 8080 )  {
	      addr.sin_port = get_free_server_port().second;
	    }
	    result.second.emplace_back(addr.sin_port);
	    sockets.emplace_back(sock);
	    continue;
	  }
	}
	::shutdown(sock, 2);
	::close(sock);
      }
    }
    if ( result.second.size() < count )
      result.first = EINVAL;
    for( auto sock : sockets )  {
      ::shutdown(sock, 2);
      ::close(sock);
    }
    return result;
  }
}
