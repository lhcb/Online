//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
#include <RTL/strdef.h>
#include <RTL/rtl.h>
#include <regex>
#include <cctype>
#include <cstring>
#include <cstdlib>
#include <stdexcept>

enum { STR_NOMATCH, STR_MATCH };

/// ________________________________________________________________________
int str_upcase(const char* src, char* dst, size_t dest_len)  {
  if ( dst )  {
    char* s1=dst, *p=dst+dest_len;
    while(*src && s1<p) {
      *s1++ = char(::toupper(*src++));
    }
    *s1 = 0;
    return 1;
  }
  return 0;
}

/// ________________________________________________________________________
int str_lowcase(const char* src, char* dst, size_t dest_len)  {
  if ( dst )  {
    char* s1=dst, *p=dst+dest_len;
    while(*src && s1<p) {
      *s1++ = char(::tolower(*src++));
    }
    *s1 = 0;
    return 1;
  }
  return 0;
}

/// ________________________________________________________________________
int str_trim(const char* src, char* dst, size_t* resultant_length)    {
  if ( dst )  {
    if ( src )  {
      size_t s2_length = strlen(src);		// Length of string
      // Determine the end of the string
      while ((s2_length > 0) && ((src[s2_length-1] == ' ') || (src[s2_length-1] == '\t')))	{
        s2_length--;
      }
      *resultant_length = *resultant_length < s2_length ? *resultant_length - 1 : s2_length;
      dst[*resultant_length] = 0;
      // Now, copy that much to the destination
      memcpy(dst, src, *resultant_length);
      return *resultant_length;
    }
    memset(dst,0,*resultant_length);
  }
  return 0;
}

/// ________________________________________________________________________
static bool match1(const char* pat, const char* str, bool case_sensitive) {
  //
  // Credits: Code from Alessandro Felice Cantatore.
  // 
  static char mapCaseTable[256];
  static char mapNoCaseTable[256];
  static bool first = true;

  const char *s, *p, *table;
  bool star = false;
  if ( first ) {
    for (int i = 0; i < 256; ++i) {
      mapCaseTable[i] = char(i);
      mapNoCaseTable[i] = char(( i >= 'a' && i <='z' ) ? i+'A'-'a' : i);
    }
    first = false;
  }
  table = case_sensitive ? mapCaseTable : mapNoCaseTable;
  
 loopStart:
  for (s = str, p = pat; *s; ++s, ++p) {
    switch (*p) {
    case '?':
      if (*s == '.') goto starCheck;
      break;
    case '*':
      star = true;
      str = s, pat = p;
      do { ++pat; } while (*pat == '*');
      if (!*pat) return true;
      goto loopStart;
    default:
      if ( *(table+*s) != *(table+*p) )
        goto starCheck;
      break;
    } /* endswitch */
  } /* endfor */
  while (*p == '*') ++p;
  return (!*p);
  
 starCheck:
  if (!star) return false;
  str++;
  goto loopStart;
}

#if 0
/// ________________________________________________________________________
static bool match0(const char *pattern, const char *candidate)  {
  switch (*pattern)   {
  case '\0':
    return !*candidate;
  case '*':
    return match0(pattern+1, candidate) || (*candidate && match0(pattern, candidate+1));
  case '?':
    return *candidate && match0(pattern+1, candidate+1);
  default:
    return (*pattern == *candidate) && match0(pattern+1, candidate+1);   
  }
}
#endif

/// ________________________________________________________________________
int str_match_wild (const char *candidate_string, const char *pattern_string)   {
  return match1(pattern_string,candidate_string,true) ? STR_MATCH : STR_NOMATCH;
}

/// ________________________________________________________________________
int strcase_match_wild (const char *candidate_string, const char *pattern_string)   {
  return match1(pattern_string,candidate_string,false) ? STR_MATCH : STR_NOMATCH;
}

/// ________________________________________________________________________
/// Escape JSON special characters if necessary
std::size_t str_escape_json(char* output, std::size_t max_out, const char* input, std::size_t in_len)   {
  char* o = output;
  const char* p = input;
  const char* stop = output + max_out;
  for( const char* e = input + in_len; p < e; ++p)  {
    switch (*p) {
      /// Escape Quote and slash
    case '"':    o[0] = '\\'; o[1] = '"';                                                 o += 2; break;
    case '\\':   o[0] = '\\'; o[1] = '\\';                                                o += 2; break;
      /// Escape x00 through x1f
    case '\x00': o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '0'; o[5] = '0'; o += 6; break;
    case '\x01': o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '0'; o[5] = '1'; o += 6; break;
    case '\x02': o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '0'; o[5] = '2'; o += 6; break;
    case '\x03': o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '0'; o[5] = '3'; o += 6; break;
    case '\x04': o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '0'; o[5] = '4'; o += 6; break;
    case '\x05': o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '0'; o[5] = '5'; o += 6; break;
    case '\x06': o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '0'; o[5] = '6'; o += 6; break;
    case '\x07': o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '0'; o[5] = '7'; o += 6; break;
    case '\b':   o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '0'; o[5] = '8'; o += 6; break;
    case '\t':   o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '0'; o[5] = '9'; o += 6; break;
    case '\n':   o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '0'; o[5] = 'a'; o += 6; break;
    case '\x0b': o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '0'; o[5] = 'b'; o += 6; break;
    case '\f':   o[0] = '\\'; o[1] = 'f';                                                 o += 2; break;
    case '\r':   o[0] = '\\'; o[1] = 'r';                                                 o += 2; break;
    case '\x0e': o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '0'; o[5] = 'e'; o += 6; break;
    case '\x0f': o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '0'; o[5] = 'f'; o += 6; break;

    case '\x10': o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '1'; o[5] = '0'; o += 6; break;
    case '\x11': o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '1'; o[5] = '1'; o += 6; break;
    case '\x12': o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '1'; o[5] = '2'; o += 6; break;
    case '\x13': o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '1'; o[5] = '3'; o += 6; break;
    case '\x14': o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '1'; o[5] = '4'; o += 6; break;
    case '\x15': o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '1'; o[5] = '5'; o += 6; break;
    case '\x16': o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '1'; o[5] = '6'; o += 6; break;
    case '\x17': o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '1'; o[5] = '7'; o += 6; break;
    case '\x18': o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '1'; o[5] = '8'; o += 6; break;
    case '\x19': o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '1'; o[5] = '9'; o += 6; break;
    case '\x1a': o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '1'; o[5] = 'a'; o += 6; break;
    case '\x1b': o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '1'; o[5] = 'b'; o += 6; break;
    case '\x1c': o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '1'; o[5] = 'c'; o += 6; break;
    case '\x1d': o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '1'; o[5] = 'd'; o += 6; break;
    case '\x1e': o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '1'; o[5] = 'e'; o += 6; break;
    case '\x1f': o[0] = '\\'; o[1] = 'u'; o[2] = '0'; o[3] = '0'; o[4] = '1'; o[5] = 'f'; o += 6; break;
      /// Rest stays as is
    default:
      *o = *p; ++o;
    }
    if ( o+6 > stop )   {
      break;
    }
  }
  /// This is the number of characters used in the output buffer
  return o-output;
}

/// ________________________________________________________________________
/// Expand environment variables in argitrary string
std::string RTL::str_expand_env(const std::string& path)   {
  std::regex env( "\\$\\{([^}]+)\\}" );
  std::string text = path;
  std::smatch match;
  while ( std::regex_search( text, match, env ) ) {
    const char *s = ::lib_rtl_getenv( match[1].str().c_str() );
    const std::string var( s == NULL ? "" : s );
    text.replace( match[0].first, match[0].second, var );
  }
  return text;
}

/// ________________________________________________________________________
/// C++ version to convert a string to lower case
std::string RTL::str_lower(const std::string& str) {
  std::string res = str.c_str();
  for(char* p=(char*)res.c_str(); *p; ++p) *p = ::tolower(*p);
  return res;
}

/// ________________________________________________________________________
/// C++ version to convert a string to upper case
std::string RTL::str_upper(const std::string& str) {
  std::string res = str.c_str();
  for(char* p=(char*)res.c_str(); *p; ++p) *p = ::toupper(*p);
  return res;
}

/// ________________________________________________________________________
/// C++ version: replace all occurrences of a string
std::string RTL::str_replace(const std::string& str, const std::string& pattern, const std::string& replacement) {
  std::string res = str;
  for(size_t id=res.find(pattern); id != std::string::npos; id = res.find(pattern) )
    res.replace(id,pattern.length(),replacement);
  return res;
}

/// ________________________________________________________________________
/// C++ version: replace all occurrences of a string
std::string RTL::str_replace(const std::string& str, const std::string& pattern, char replacement) {
  std::string rep { "  " };
  rep[0] = replacement;
  rep[1] = '\0';
  return str_replace(str, pattern, rep);
}

/// ________________________________________________________________________
/// C++: split string according to non-empty delimiter character
std::vector<std::string> RTL::str_split(const std::string& source, char delimiter)  {
  std::string tmp;
  std::vector<std::string> result;
  size_t start = 0, idx = source.find(delimiter, start);
  result.reserve(10);
  tmp = source.substr(start, idx);
  if ( !tmp.empty() ) result.emplace_back(tmp);
  while( idx != std::string::npos )  {
    start = idx+1;
    idx = source.find(delimiter, start);
    tmp = source.substr(start, idx-start);
    if ( !tmp.empty() ) result.emplace_back(tmp);
  }
  return result;
}

/// ________________________________________________________________________
/// C++: split string according to non-empty delimiter string
std::vector<std::string> RTL::str_split(const std::string& source, const char* delimiter)  {
  if ( delimiter )   {
    return str_split(source, std::string(delimiter));
  }
  throw std::runtime_error("RTL: Cannot split string with NULL delimiter");
}

/// ________________________________________________________________________
/// C++: split string according to non-empty delimiter string
std::vector<std::string> RTL::str_split(const std::string& source, const std::string& delimiter)  {
  if ( !delimiter.empty() )   {
    std::string tmp;
    std::vector<std::string> result;
    size_t start = 0, idx = source.find(delimiter, start);
    result.reserve(10);
    tmp = source.substr(start, idx);
    if ( !tmp.empty() ) result.emplace_back(tmp);
    while( idx != std::string::npos )  {
      start = idx+1;
      idx = source.find(delimiter, start);
      tmp = source.substr(start, idx-start);
      if ( !tmp.empty() ) result.emplace_back(tmp);
    }
    return result;
  }
  throw std::runtime_error("RTL: Cannot split string with empty delimiter");
}

#if 0
/// ________________________________________________________________________
int str_match_wild (const char *candidate_string, const char *pattern_string)   {
  int result = STR_NOMATCH;
  if ( candidate_string && pattern_string )   {
    size_t s1_len = strlen(candidate_string);
    size_t s2_len = strlen(pattern_string);	
    size_t s1_pos = 0;
    size_t s2_pos = 0;
    
    result = STR_MATCH;
    for ( s1_pos = 0; s1_pos < s1_len; s1_pos++  )	  {
      if ( candidate_string[s1_pos] == pattern_string[s2_pos] )  {
        result = STR_MATCH;
        s2_pos++;
      }
      else if ( pattern_string[s2_pos] == '%' )	 {
        result = STR_MATCH;
        s2_pos++;
      }
      else if ( pattern_string[s2_pos] == '*' )   {
        if (s2_pos+1 >= s2_len )	{ // wild card last char 
          s1_pos = s1_len;
          s2_pos++;
          result = STR_MATCH;
        }
        else	 { // wild card not last character
          if ( candidate_string[s1_pos] == pattern_string[s2_pos+1] )  {
            s2_pos += 2;
          } 
        }
      }
      else  {
        result = STR_NOMATCH;
        // s1_pos = s1_len;
        s2_pos = 0;
      }
    } // end for
    
    
    // Special case last char is * which can represent nothing
    if (( pattern_string[s2_pos] == '*' ) && ( s2_pos+1 == s2_len ))
      s2_pos++;
    
    // we left uncheck characters in the candidate string
    if ( s2_pos != s2_len ) 	{
      result = STR_NOMATCH;
    }
  }
  return result;
}
#endif
