//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
#include "rtl_internal.h"
#include <mutex>
#include <cerrno>
#include <fcntl.h>
#include <unistd.h>
#include <system_error>
#define ERROR_SUCCESS 0

using namespace std;
namespace RTL  {
  static void*    s_rtl_printer_arg = nullptr;
  static int      s_rtl_print_level = LIB_RTL_WARNING;
  static size_t (*s_rtl_printer)(void*, int, const char*, va_list& args) = 0;
}

namespace {
  std::mutex s_rtl_output_lock {};
}

extern "C" int lib_rtl_get_error()   {
#ifdef USE_PTHREADS
  return errno;
#elif _WIN32
  return ::GetLastError();
#endif
}

extern "C" int lib_rtl_run_ast (RTL_ast_t astadd, void* param, int)    {
#if defined(_WIN32) || defined(__linux)
  if ( astadd )  {
    return (*astadd)(param);
  }
#endif
  return 1;
}

extern "C" int lib_rtl_pid()  {
  return ::getpid();
}

int lib_rtl_signal_message(int action, const char* fmt, ...)  {
  int status = 1;
  if ( fmt )  {
    int err;
    va_list args;
    char buff[2048];

    va_start( args, fmt );
    ::vsnprintf(buff, sizeof(buff), fmt, args);
    buff[sizeof(buff)-1] = 0;
    va_end(args);

    switch(action) {
    case LIB_RTL_ERRNO:
      err = errno;
      if ( err != 0 )  {
	string errstr = std::make_error_code(std::errc(err)).message();
        ::lib_rtl_output(LIB_RTL_ERROR, "RTL:%4d : %s", err, errstr.c_str());
        ::lib_rtl_output(LIB_RTL_ERROR, "            %s", buff);
        status = 0;
      }
      break;
    case LIB_RTL_DEFAULT:
      ::lib_rtl_output(LIB_RTL_ERROR, "RTL: %s", buff);
      status = 0;
      break;
    case LIB_RTL_OS:
    default:
      err = ::lib_rtl_get_error();
      if ( err != ERROR_SUCCESS )   {
	string errstr = std::make_error_code(std::errc(err)).message();
        ::lib_rtl_output(LIB_RTL_ERROR, "RTL:%4d : %s", err, errstr.c_str());
        ::lib_rtl_output(LIB_RTL_ERROR, "            %s",buff);
        status = 0;
      }
      break;
    }
  }
  return status;
}

/// Set minimum printout level (default: LIB_RTL_WARNING)
extern "C" void lib_rtl_set_log_level(int level)    {
  RTL::s_rtl_print_level = level;
  if ( level < LIB_RTL_VERBOSE ) RTL::s_rtl_print_level = LIB_RTL_VERBOSE;
  if ( level > LIB_RTL_ALWAYS  ) RTL::s_rtl_print_level = LIB_RTL_ALWAYS;
}

/// Get minimum print level
extern "C" int lib_rtl_log_level()   {
  return RTL::s_rtl_print_level;
}

/// Printout redirection
extern "C" size_t lib_rtl_output(int level, const char* format, ...)   {
  if ( level >= RTL::s_rtl_print_level )  {
    va_list args;
    va_start( args, format );
    size_t result = ::lib_rtl_log(level, format, args);
    va_end(args);
    return result;
  }
  return 0;
}

/// Printout redirection
extern "C" size_t lib_rtl_log(int level, const char* format, va_list& args)   {
  size_t result = 0;
  if ( level >= RTL::s_rtl_print_level )  {
    switch(level) {
    case LIB_RTL_ERRNO:
    case LIB_RTL_OS:
      if ( errno != 0 )  {
	int status = errno;
	string err = std::make_error_code(std::errc(status)).message();
	::lib_rtl_output(LIB_RTL_ERROR, "RTL:%4d [%s]", status, err.c_str());
	errno = 0;
      }
      level = LIB_RTL_ERROR;
      break;
    case LIB_RTL_DEFAULT:
      level = LIB_RTL_ERROR;
      break;
    default:
      break;
    }

    if ( RTL::s_rtl_printer != 0 )  {
      std::lock_guard<std::mutex> lock(s_rtl_output_lock);
      result = (*RTL::s_rtl_printer)(RTL::s_rtl_printer_arg, level, format, args);
    }
    else  {
      std::lock_guard<std::mutex> lock(s_rtl_output_lock);
      result = ::vfprintf(stdout, format, args);
      ::fputc('\n', stdout);
      ::fflush(stdout);
    }
  }
  return result;
}

/// Printout redirection
extern "C" size_t lib_rtl_printf(const char* format, ...)   {
  size_t result;
  va_list args;
  va_start( args, format );
  if ( RTL::s_rtl_printer != 0 )  {
    result = (*RTL::s_rtl_printer)(RTL::s_rtl_printer_arg, LIB_RTL_ALWAYS, format, args);
  }
  else  {
    result = ::vfprintf(stdout, format, args);
    ::fflush(stdout);
  }
  va_end(args);
  return result;
}

/// Install RTL printer 
extern "C" void lib_rtl_install_printer(size_t (*func)(void*, int, const char*, va_list& args), void* param)  {
  RTL::s_rtl_printer = func;
  RTL::s_rtl_printer_arg = param;
}
