//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================

/// C/C++ include files
#include <ctime>
#include <thread>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <system_error>

/// Framework include files
#include <NET/UdpConnection.h>
#include <NET/TcpConnection.h>
#include <CPP/EventHandler.h>
#include <CPP/SmartPointer.h>
#include <RTL/rtl.h>
#include <RTL/Logger.h>
#include <TAN/TanDB.h>
#include <TAN/TanInterface.h>
#include <WT/wtdef.h>

#define EINVOPER       0xDEAD

inline const char *timestr (void)  {
  time_t curr;
  time(&curr);
  char *tim = ctime(&curr);
  tim[24] = 0;
  return tim;
}

class Receivehandler : public CPP::EventHandler  {
protected:
  TcpNetworkChannel*    _pNetwork;
  TanDataBase::Entry*   _pEntry;
public:
  explicit Receivehandler ( EventReactor* reactor )
    : EventHandler(reactor), _pNetwork(0), _pEntry(0)  {
  }
  Receivehandler ( EventReactor* reactor, TcpNetworkChannel* chan, TanDataBase::Entry* entry )
    : EventHandler(reactor), _pNetwork(chan), _pEntry(entry)
  {
  }
  virtual ~Receivehandler() {
  }
  TcpNetworkChannel* channel()     {
    return _pNetwork;
  }
  TanDataBase::Entry* _Entry()     {
    return _pEntry;
  }
  void _Set( TcpNetworkChannel* chan, TanDataBase::Entry* entry )  {
    _pNetwork = chan;
    _pEntry   = entry;
  }
  void _Delete()  {
    TcpNetworkChannel* chan = channel();
    chan->cancel();
    delete chan;
    delete this;
  }
};


#define NAME_SERVER_SUCCESS   1
#define NAME_SERVER_ERROR     0

/** @class Nameservice NameServer.cpp TAN/NameServer.cpp
 *
 * Interpretation of nameserver requests
 * - Interprete the received message and build the appropriate
 *   reply.
 *
 *  @author  M.Frank
 *  @version 1.0
 */
class NameService : public CPP::EventReactor  {
protected:
  //@Man: Protected member variables
  /// Reference to tan database object
  TanDataBase&         m_tandb;
  /// Smart pointer to hold the connection object
  NetworkConnection*   m_connection;
  /// Service port
  NetworkChannel::Port m_port;
  /// Shutdown flag
  bool                 m_shutdown;

public:
  //@Man Public member functions
  /// Standard constructor
  explicit NameService(TanDataBase& db, NetworkConnection* ptr = 0, bool verbose = false);
  /// Standard destructor
  virtual ~NameService();
  /// handle Tan request
  virtual void handleMessage(TanDataBase::Entry*& ent, TanMessage& rec_msg, TanMessage& snd_msg );
  /// Access database handle
  TanDataBase& tandb()  {
    return m_tandb;
  }
  /// Set shutdown flag
  virtual void enable_shutdown()   {
    m_shutdown = true;
  }
  /// Reactor's Event dispatching overlayed method
  int handle ( EventHandler* /* handler */ )  override {
    return NAME_SERVER_SUCCESS;
  }
  /// Abstract member function: Act on Nameservice requests
  virtual void handle() {
  }
  /// Abstract: suspend the service
  virtual int suspend() {
    return NAME_SERVER_SUCCESS;
  }
  /// Abstract: resume the service
  virtual int resume()   {
    return NAME_SERVER_SUCCESS;
  }
  /// run the service
  virtual void run();
  /// Nameserver termination handler
  static int termination_handler(void*         user_context,
				 int           sig_number,
				 siginfo_t* /* info */,
				 void*      /* ptr */);
};

/** @class UdpNameService NameServer.cpp TAN/NameServer.cpp
 *
 *  handle TAN messages in UDP mode
 *  A network connection is forseen to reply the
 *  requested information.
 *
 *  @author  M.Frank
 *  @version 1.0
 */
class UdpNameService : public NameService {
public:
  //@Man Public member functions
  /// Standard constructor
  explicit UdpNameService(TanDataBase& db, bool verbose = false);
  /// Standard destructor
  virtual ~UdpNameService()   {
  }
  /// Reactor's Event dispatching overlayed method
  int handle ( CPP::EventHandler* /* handler */ )  override {
    return NAME_SERVER_SUCCESS;
  }
  /// handle Single request
  void handle() override;
  /// suspend the service
  int suspend()       override {
    return true;
  }
  /// resume the service
  int resume()       override {
    return true;
  }
};


/** @class TcpNameService NameServer.cpp TAN/NameServer.cpp
 *
 *  handle TAN messages in TCP/IP mode
 *  A network connection is forseen to reply the
 *  requested information.
 *
 *  @author  M.Frank
 *  @version 1.0
 */
class TcpNameService : public NameService {
protected:
  //@Man: Protected member variables
  /// Pointer to Accept Event handler
  EventHandler*      m_pAccepthandler;
  /// Pointer to TCP networkconnection
  TcpConnection*     m_tcp;
  /// Pointer to Network Channel
  TcpNetworkChannel* m_pNetwork;
public:
  //@Man Public member functions
  /// Standard constructor with initialization
  TcpNameService(TanDataBase& db, int port, bool verbose = false);
  /// Standard constructor
  explicit TcpNameService(TanDataBase& db, bool verbose = false);
  /// Standard destructor
  virtual ~TcpNameService();
  /// Overloaded abstract member function: Act on Nameservice requests
  void handle() override;
  /// Overloaded abstract member function: suspend the service
  int suspend() override;
  /// Overloaded abstract member function: resume the service
  int resume() override;
  /// Reactor's Event dispatching overlayed method
  int handle ( CPP::EventHandler* handler ) override;
  /// handle Accept request
  int handleAcceptRequest ( CPP::EventHandler* handler );
  /// handle receive request
  int handleReceiveRequest ( CPP::EventHandler* handler );
};

// ----------------------------------------------------------------------------
//                                      M.Frank
// ----------------------------------------------------------------------------
NameService::NameService(TanDataBase& db, NetworkConnection* ptr, bool verbose)
  : m_tandb(db), m_connection(ptr), m_port(::tan_nameserver_port()), m_shutdown(false)
{
  if ( verbose )  {
    ::lib_rtl_output(LIB_RTL_INFO,"%s","+======================================================================+\n");
    ::lib_rtl_output(LIB_RTL_INFO,"%s","|         N A M E S E R V E R      S T A R T I N G                     |\n");
    ::lib_rtl_output(LIB_RTL_INFO,"%s","|         %32s                             |\n",timestr());
    ::lib_rtl_output(LIB_RTL_INFO,"%s","+======================================================================+\n");
    ::fflush(stdout);
  }
  if ( m_connection )  {
    if ( NetworkConnection::NETCONNECTION_SUCCESS != m_connection->Status() )  {
      ::lib_rtl_output(LIB_RTL_ERROR,"NameService> Error initializing the network connection. [port:%d]\n", m_port);
      ::exit(ptr->Status());
    }
  }
  ::wtc_init();
}

// ----------------------------------------------------------------------------
//                                      M.Frank
// ----------------------------------------------------------------------------
NameService::~NameService()  {
  if ( m_connection ) delete m_connection;
}
// ----------------------------------------------------------------------------
/// Nameserver termination handler
//                                      M.Frank
// ----------------------------------------------------------------------------
int NameService::termination_handler(void*         user_context,
				     int           sig_number,
				     siginfo_t* /* info */,
				     void*      /* ptr */)
{
  if ( sig_number == SIGQUIT || sig_number == SIGTERM )    {
    NameService* ns = (NameService*)user_context;
    ns->enable_shutdown();
    std::cout << "Nameserver exit!" << std::endl << std::flush;
    ::_exit(0);
  }
  return LIB_RTL_SIGNAL_HANDLED;
}
// ----------------------------------------------------------------------------
//                                      M.Frank
// ----------------------------------------------------------------------------
void NameService::run()  {
  while ( !m_shutdown ) {
    handle();
  }
  ::lib_rtl_output(LIB_RTL_INFO,"%s> Nameserver on %s exiting.\n",RTL::nodeName().c_str());
}
// ----------------------------------------------------------------------------
static void _printEntry(const char* msg, TanDataBase::Entry* ent) {
  if ( msg ) {
    if ( ent ) {
      /*
        ::lib_rtl_output(LIB_RTL_DEBUG,"%s> handle message: %s Name:%s Port=%d [%d] %p\n",
        RTL::nodeName().c_str(),msg,ent->_Name(),ent->port(),
        ntohs(ent->port()),(void*)ent);
        }
        else {
        ::lib_rtl_output(LIB_RTL_DEBUG,"%s> handle message: %s [No entry]\n",RTL::nodeName().c_str(),msg);
      */
    }
  }
}
// ----------------------------------------------------------------------------
//                                      M.Frank
// ----------------------------------------------------------------------------
void NameService::handleMessage( TanDataBase::Entry*& ent, TanMessage& rec_msg, TanMessage& snd_msg )    {
  try  {
    TanMessage::Type func = TanMessage::Type(rec_msg.function());
    NetworkChannel::Port port = 0;

    if ( &snd_msg != &rec_msg ) snd_msg = rec_msg;
    snd_msg.m_error  = TAN_SS_SUCCESS;
    snd_msg.m_length = sizeof(snd_msg);

    switch ( func )  {
    case TanMessage::ALLOCATE:
      if ( (port = htons(m_tandb.allocatePort(ent))) == 0 )
	snd_msg.m_error = m_tandb.Error();
      _printEntry("ALLOCATE", ent);
      break;

    case TanMessage::DEALLOCATE:
      _printEntry("DEALLOCATE", ent);
      if ( TAN_SS_SUCCESS != m_tandb.freePort(ent) )
	snd_msg.m_error = m_tandb.Error();
      break;

    case TanMessage::DECLARE_PORT:
      if ( TAN_SS_SUCCESS != m_tandb.declarePort(ent, rec_msg.m_sin.sin_port) )
	snd_msg.m_error = m_tandb.Error();
      _printEntry("DECLARE", ent);
      break;

    case TanMessage::ALIAS:
      _printEntry("ALIAS",ent);
      if ( TAN_SS_SUCCESS != m_tandb.insertAlias(ent) )
	snd_msg.m_error = m_tandb.Error();
      break;

    case TanMessage::DEALIAS:
      _printEntry("DEALIAS", ent);
      if ( TAN_SS_SUCCESS != m_tandb.removeAlias(ent) )
	snd_msg.m_error = m_tandb.Error();
      break;

    case TanMessage::DUMP:
      _printEntry("DUMP", ent);
      m_tandb.Dump(std::cout);
      break;

    case TanMessage::SHUTDOWN:
      _printEntry("SHUTDOWN", ent);
      ::lib_rtl_output(LIB_RTL_INFO,"%s","+======================================================================+\n");
      ::lib_rtl_output(LIB_RTL_INFO,"%s","|                  TAN NAMESERVER SHUTDOWN requested.                  |\n");
      ::lib_rtl_output(LIB_RTL_INFO,"%s","+======================================================================+\n");
      m_shutdown = true;
      break;

    case TanMessage::INQUIRE:                                 // Inquire service...
      _printEntry("INQUIRE", ent);
      if ( (port=htons(m_tandb.findPort(rec_msg))) == 0 )  {
	snd_msg.m_error = m_tandb.Error();
      }
      break;

    default:
      _printEntry("TAN_SS_UNKNOWNMODE",0);
      snd_msg.m_error = TAN_SS_UNKNOWNMODE;
      break;
    }
    if ( port != 0 )  {
      snd_msg.m_sin.sin_family  = rec_msg.m_sin.sin_family;
      snd_msg.m_sin.sin_port    = port;
    }
    else if ( snd_msg.m_error != TAN_SS_SUCCESS )  {
      snd_msg.m_sin.sin_family      = 0;
      snd_msg.m_sin.sin_port        = 0;
      snd_msg.m_sin.sin_addr.s_addr = 0;
    }
    return;
  }
  catch( const std::exception& e )   {
    ::lib_rtl_output(LIB_RTL_ALWAYS,"Nameserver: Exception %s", e.what());
  }
  catch( ... )   {
    ::lib_rtl_output(LIB_RTL_ALWAYS,"Nameserver: UNKNOWN exception");
  }
}

// ----------------------------------------------------------------------------
//                                      M.Frank
// ----------------------------------------------------------------------------
UdpNameService::UdpNameService(TanDataBase& db, bool verbose)
  : NameService(db, nullptr, verbose)
{
#ifdef SERVICE
  m_port = UdpConnection::servicePort(NAME_SERVICE_NAME);
#endif
  if ( verbose )  {
    ::lib_rtl_output(LIB_RTL_INFO,"|         U D P         N A M E    S E R V I C E                       |%c",'\n');
    ::lib_rtl_output(LIB_RTL_INFO,"|         Port(local): %6d %04X Network:%6d %04X                 |\n",
                     m_port, m_port, htons(m_port), htons(m_port));
    ::lib_rtl_output(LIB_RTL_INFO,"+======================================================================+%c",'\n');
    ::fflush(stdout);
  }
}

void UdpNameService::handle ()   {
  // ----------------------------------------------------------------------------
  //                                      M.Frank
  // ----------------------------------------------------------------------------
  static UdpConnection     conn(m_port);
  TanDataBase::Entry      *ent = nullptr;
  TanMessage               req, rep;
  NetworkChannel::Address  addr;
  UdpNetworkChannel        snd;

  addr.sin_port = conn.port();
  addr.sin_family = conn.family();
  addr.sin_addr.s_addr = INADDR_ANY;
  ::memset(addr.sin_zero,0,sizeof(addr.sin_zero));

  int status = conn.recvChannel().recv(&req, sizeof(req), 0, 0, &addr);
  if ( status != sizeof(req) )  {
    lib_rtl_output(LIB_RTL_ERROR,"%s","NameService::handle> Error receiving message\n");
  }
  else  {      // handle the request....
    ent = 0;
    addr.sin_port = req.m_sin.sin_port;
    handleMessage( ent, req, rep );
#ifndef SERVICE
    // Swap port to reply connection
    addr.sin_port = htons(m_port+1);
#endif
    //lib_rtl_output(LIB_RTL_DEBUG,"send to port:%04X\n",addr.sin_port);
    status = snd.send(&rep, sizeof(rep), 0, 0, &addr);
    if ( status != sizeof(rep) )  {
      ::lib_rtl_output(LIB_RTL_ERROR,"NameService::handle> Error sending message to [%s] on port 0x%X\n",
                       inet_ntoa(rep.address()), rep.port());
    }
  }
  if ( m_shutdown ) ::exit(0);
}

// ----------------------------------------------------------------------------
//                                      M.Frank
// ----------------------------------------------------------------------------
TcpNameService::TcpNameService(TanDataBase& db, bool verbose)
  : NameService(db, nullptr, verbose)
{
#ifdef SERVICE
  m_tcp = new TcpConnection(NAME_SERVICE_NAME);
#else
  m_tcp = new TcpConnection(::tan_nameserver_port());
#endif
  if ( !m_tcp->Ok() )    {
    std::string err = m_tcp->recvChannel().errMsg();
    throw std::runtime_error("Failed to instantiate TAN name service ["+err+"]");
  }
  m_port = m_tcp->port();
  m_connection = m_tcp;
  if ( verbose )  {
    ::lib_rtl_output(LIB_RTL_INFO,"%s","|         T C P / I P   N A M E    S E R V I C E                       |\n");
    ::lib_rtl_output(LIB_RTL_INFO,"|         Port(local): %6d %04X Network:%6d %04X                 |\n",
                     m_port, m_port, htons(m_port), htons(m_port));
    ::lib_rtl_output(LIB_RTL_INFO,"%s","+======================================================================+\n");
  }
  m_pAccepthandler = new EventHandler(this);
  m_pNetwork = &((TcpNetworkChannel&)m_tcp->recvChannel());
  m_pNetwork->queueAccept ( m_port, m_pAccepthandler );              // Rearm
}

// ----------------------------------------------------------------------------
//                                      M.Frank
// ----------------------------------------------------------------------------
TcpNameService::TcpNameService(TanDataBase& db, int port, bool verbose)
  : NameService(db, nullptr, verbose)
{
  m_tcp = new TcpConnection(port);
  if ( !m_tcp->Ok() )    {
    std::string err = m_tcp->recvChannel().errMsg();
    throw std::runtime_error("Failed to instantiate TAN name service ["+err+"]");
  }
  m_port = m_tcp->port();
  m_connection = m_tcp;
  if ( verbose )  {
    ::lib_rtl_output(LIB_RTL_INFO,"|         T C P / I P   N A M E    S E R V I C E                       |\n");
    ::lib_rtl_output(LIB_RTL_INFO,"%s","|         Port(local): %6d %04X Network:%6d %04X                 |\n",
                     m_port, m_port, htons(m_port), htons(m_port));
    ::lib_rtl_output(LIB_RTL_INFO,"%s","+======================================================================+\n");
  }
  m_pAccepthandler = new EventHandler(this);
  m_pNetwork = &((TcpNetworkChannel&)m_tcp->recvChannel());
  m_pNetwork->reuseAddress();
  m_pNetwork->queueAccept ( m_port, m_pAccepthandler );              // Rearm
}

TcpNameService::~TcpNameService()   {
  // ----------------------------------------------------------------------------
  //                                      M.Frank
  // ----------------------------------------------------------------------------
  if ( m_pNetwork )   {
    m_pNetwork->_unqueueIO( m_tcp->port() );
    m_pNetwork->cancel();
  }
  if ( m_pAccepthandler )  {
    delete m_pAccepthandler;
  }
}

// ----------------------------------------------------------------------------
//                                      M.Frank
// ----------------------------------------------------------------------------
void TcpNameService::handle()   {
  void* par;
  unsigned int fac;
  int sub_status, status;
  status = ::wtc_wait( &fac, &par, &sub_status );
  ::lib_rtl_output(LIB_RTL_INFO,"Wait (%d,%d) -> %s\n", 
		   status, sub_status, RTL::errorString(sub_status).c_str());
}

// ----------------------------------------------------------------------------
//                                      M.Frank
// ----------------------------------------------------------------------------
int TcpNameService::handle ( EventHandler* handler )  {
  int status = NAME_SERVER_SUCCESS;
  try {
    if ( handler == m_pAccepthandler )
      status=handleAcceptRequest ( handler );
    else if ( handler != 0 )
      status=handleReceiveRequest ( handler );
    if ( m_shutdown ) ::exit(0);
  }
  catch(const std::exception& e)  {
    ::lib_rtl_output(LIB_RTL_ERROR,"TcpNameService::handle> Exception: %s",e.what());
  }
  catch(...)  {
    ::lib_rtl_output(LIB_RTL_ERROR,"TcpNameService::handle> %s","UNKNOWN Exception");
  }
  return status;
}

// ----------------------------------------------------------------------------
//                                      M.Frank
// ----------------------------------------------------------------------------
int TcpNameService::handleAcceptRequest ( EventHandler* handler )  {
  int retry = 1;                                                   //
  NetworkChannel::Address address;                                 //
  NetworkChannel::Channel channel = m_pNetwork->accept(address);   // Accept
  //int accept_error = m_pNetwork->error();                          //
  int status = m_pNetwork->queueAccept(m_port,handler);            // Rearm ACCEPT
  if ( !lib_rtl_is_success(status) )  {
    lib_rtl_output(LIB_RTL_ERROR,"handleAcceptRequest> Accept Rearm FAILED %d RetryCount:%d %s",
                   m_pNetwork->error(),retry,m_pNetwork->errMsg().c_str());             //
  }                                                                //
  if ( channel <= 0 )   {                                          // Error!
    return NAME_SERVER_SUCCESS;                                    // Return status code
  }                                                                //
  else  {                                                          // Event handling:
    TanDataBase::Entry* entry = m_tandb.AllocateEntry(channel);    // Allocate database entry
    if ( entry != 0 )  {                                           // SUCCESS:
      Receivehandler*    hand = new Receivehandler(this);          // Add handler
      TcpNetworkChannel* chan = new TcpNetworkChannel(channel);    // Create new connection
      chan->reuseAddress();
      hand->_Set ( chan, entry );                                  //   Update handler's parameters
      return handleReceiveRequest( hand );                         //
    }                                                              // and return
    return TAN_SS_NOMEM;                                           // Failed to allocate TAN slot
  }                                                                //
}                                                                  //

// ----------------------------------------------------------------------------
//                                      M.Frank
// ----------------------------------------------------------------------------
int TcpNameService::handleReceiveRequest ( EventHandler* handler )  {
  Receivehandler*     hand = (Receivehandler*)handler;
  TanDataBase::Entry*  ent = hand->_Entry();
  TcpNetworkChannel*  chan = hand->channel();
  int status = NAME_SERVER_SUCCESS, num_byte;

  try  {
    num_byte = chan->recv( &ent->_Message(), sizeof(ent->_Message()));
    if ( num_byte <= 1 )  {          // Socket closed by Client
      m_tandb.Close ( ent );         // No need to return error
      //status = chan->error();      // condition in this case!
      chan->_unqueueIO (m_port);
    }
    else {
      TanMessage reply;
      handleMessage( ent, ent->_Message(), reply);
      num_byte = chan->send(&reply, sizeof(reply));
      if ( num_byte <= 0 ) {
	m_tandb.Close ( ent );
	status = chan->error();
	chan->_unqueueIO (m_port);
      }
      else  {
	int func = reply.function();
	switch( func ) {
	  //case TanMessage::DUMP:
	  //case TanMessage::INQUIRE:       // Inquire service...
	case TanMessage::DEALLOCATE:        // Deallocate port
	  // Here the channel must be closed.
	  // No task dead message! chan->recv(&reply, 1);
	  chan->_unqueueIO (m_port);
	  break;
	default:
	  if ( reply.error() == TAN_SS_SUCCESS )  {         // Only way to exit
	    status = chan->queueReceive (m_port, hand);     // with success!
	    if ( !lib_rtl_is_success(status) ) {
	      ::lib_rtl_output(LIB_RTL_ERROR,"Error rearming receive: %s",chan->errMsg().c_str());
	    }
	    if ( func != TanMessage::INQUIRE )
	      return status;
	  }
	  m_tandb.Close ( ent );
	  chan->_unqueueIO (m_port);
	  break;
	}
      }
    }
    hand->_Delete();
  }
  catch( const std::exception& e )   {
    ::lib_rtl_output(LIB_RTL_ALWAYS,"Nameserver: Exception %s", e.what());
  }
  catch( ... )   {
    ::lib_rtl_output(LIB_RTL_ALWAYS,"Nameserver: UNKNOWN exception");
  }
  return status;
}

// ----------------------------------------------------------------------------
//                                      M.Frank
// ----------------------------------------------------------------------------
int TcpNameService::suspend  ()   {
  delete m_connection;
  m_connection = 0;
  m_tcp = 0;
  return NAME_SERVER_SUCCESS;
}

// ----------------------------------------------------------------------------
//                                      M.Frank
// ----------------------------------------------------------------------------
int TcpNameService::resume ()   {
  int retry = 0;
 New_allocation:
  if ( m_connection != 0 ) suspend();
  m_connection = m_tcp = new TcpConnection(m_port);
  m_pNetwork = &((TcpNetworkChannel&)m_tcp->recvChannel());
  if ( m_pNetwork->error() != 0 && ++retry < 5 )  {
    lib_rtl_output(LIB_RTL_INFO,"resume-Retry# %d> %s\n", retry, m_pNetwork->errMsg().c_str());
    goto New_allocation;
  }
  return m_pNetwork->queueAccept ( m_tcp->port(), m_pAccepthandler );
}

#include <sys/prctl.h>

extern "C" int  tanmon( int argc, char* argv[]);
extern "C" int  tan_nameserver (int argc, char* argv[]) {
  // ----------------------------------------------------------------------------
  //                                      M.Frank
  // ----------------------------------------------------------------------------
  char *c;
  std::string pa = NAMESERVICE_PUBAREA_NAME;
  const char* port = nullptr;
  NameService *srv = 0;
  int port_offset  = 0;
  bool inquirer = false, allocator = false, tcp = false, udp = false, nowait = false;
  bool delgbl = false, verbose = false, monitor = false, propagate_exit=false;

  RTL::Logger::install_log(RTL::Logger::log_args(LIB_RTL_WARNING));
  while( --argc > 0 )      {
    if ( *(c = *++argv) == '-' )   {
      switch( *++c | 0x20 )  {
      case 'i':  inquirer  = true;             break;
      case 'a':  allocator = true;             break;
      case 't':  tcp       = true;             break;
      case 'u':  udp       = true;             break;
      case 'n':  nowait    = true;             break;
      case 'v':  verbose   = true;             break;
      case 'd':  delgbl    = true;             break;
      case 'm':  monitor   = true;             break;
      case 'p':  pa        = *++argv;  --argc; break;
      case 'q':  port      = *++argv;  --argc; break;
      case 'o':  port_offset = ::strtol(*++argv, &c, 10);  --argc; break;
      case 'x':  propagate_exit = true;        break;
      default:
      Options:
        ::lib_rtl_output(LIB_RTL_ALWAYS,"NameServer    %s", " -<opt> [-opt]");
        ::lib_rtl_output(LIB_RTL_ALWAYS,"  -a(llocator)      %s", "listen and serve (DE)ALLOCATION requests");
        ::lib_rtl_output(LIB_RTL_ALWAYS,"  -i(nqquirer)      %s", "listen and serve INQUIRE        requests");
        ::lib_rtl_output(LIB_RTL_ALWAYS,"  -tcp              %s", "run service in tcp/ip mode (default:udp/INQUIRE tcp/ALLOCATE)");
        ::lib_rtl_output(LIB_RTL_ALWAYS,"  -udp              %s", "run service in udp mode    (default:udp/INQUIRE tcp/ALLOCATE)");
        ::lib_rtl_output(LIB_RTL_ALWAYS,"  -m(onitor)        %s", "run tan monitor in server process.      ");
        ::lib_rtl_output(LIB_RTL_ALWAYS,"  -n(owait)         %s", "Continue execution after routine call. Requires wtc_wait later!");
        ::lib_rtl_output(LIB_RTL_ALWAYS,"  -p(ubarea) <name> Name of the publishing area. Default: %s", pa.c_str());
        ::lib_rtl_output(LIB_RTL_ALWAYS,"  -q    <number>    Port number Default: %d.", ::tan_nameserver_port());
        ::lib_rtl_output(LIB_RTL_ALWAYS,"  -o    <number>    Port number offset Default: %d.", port_offset);
        ::lib_rtl_output(LIB_RTL_ALWAYS,"  -v(erbose)        %s", "Print header at startup.                ");
        return 0x1;
      }
    }
  }
  if ( verbose )  {
    RTL::Logger::print_startup("Target Name Server starting up");
  }
  if ( port )   {
    char* end = 0;
    ::tan_set_nameserver_port(::strtol(port, &end, 10));
  }

  /// Initialize the TAN database with the correct name the first time
  if ( (inquirer && allocator) || (!inquirer && !allocator) ) {
    goto Options;
  }
  try  {
    if ( inquirer )   {
      auto& db = TanDataBase::Instance(pa.c_str(), false);
      if ( tcp ) srv = new TcpNameService(db, ::tan_nameserver_port()+1, verbose);
      else       srv = new UdpNameService(db, verbose);
    }
    else if ( allocator )   {
      if ( delgbl ) {
#ifdef __linux
	::unlink( "/dev/shm/sem.TANDB" );
	::unlink(("/dev/shm/sem."+pa+"_lock").c_str());
	::unlink(("/dev/shm/"+pa).c_str());
#endif
      }
      auto& db = TanDataBase::Instance(pa.c_str(), true);
      if ( udp ) srv = new UdpNameService(db, verbose);
      else       srv = new TcpNameService(db, verbose);
    }
    if ( propagate_exit )   {
      ::prctl(PR_SET_PDEATHSIG, SIGTERM);
    }
    ::lib_rtl_signal_log(0);
    ::lib_rtl_subscribe_signal(SIGQUIT, srv, NameService::termination_handler);
    ::lib_rtl_subscribe_signal(SIGTERM, srv, NameService::termination_handler);
    if ( port_offset > 0 )  {
      srv->tandb().setPortOffset(port_offset);
    }
    if ( !srv )   {
      goto Options;
    }
    if ( monitor )   {
      new std::thread([]() { const char* a[] = {"tanmon", "-c", nullptr}; tanmon(2, (char**)a); });
    }
    if ( !nowait )  {
      srv->run();
      delete srv;
    }
    return 0x0;
  }
  catch (const std::exception& e)   {
    std::string err = e.what();
    ::lib_rtl_output(LIB_RTL_ALWAYS,"+++ FAILED to start name service: %s", err.c_str());
    if ( err.find("Address already in use") != std::string::npos )
      return EADDRINUSE;
    return EINVAL;
  }
}
