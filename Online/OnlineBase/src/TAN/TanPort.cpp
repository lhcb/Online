//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cctype>
#include <RTL/rtl.h>

#define NAME_SERVICE_PORT    6789

namespace   {
  int tan_port_num = 0;
}

// ----------------------------------------------------------------------------
// C Interface: Set nameserver port number
// ----------------------------------------------------------------------------
extern "C" void online_tan_set_nameserver_port(int num)  {
  tan_port_num = num;
}

// ----------------------------------------------------------------------------
// C Interface: Get nameserver port number
// ----------------------------------------------------------------------------
extern "C" int online_tan_nameserver_port()  {
  if ( 0 == tan_port_num )    {
    char* end;
    const char* tan_port_env = ::lib_rtl_getenv("TAN_PORT");
    tan_port_num = NAME_SERVICE_PORT;
    if ( tan_port_env )   {
      for( const char* p = tan_port_env; *p; ++p )   {
	if ( !::isdigit(*p) )
	  return tan_port_num;
      }
      if ( 0 == ::strlen(tan_port_env) )
	return tan_port_num;
      tan_port_num = ::strtol(tan_port_env, &end, 10);
    }
  }
  return tan_port_num;
}
