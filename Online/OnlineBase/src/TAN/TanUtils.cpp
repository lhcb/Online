//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
/// Framework include files
#include <RTL/rtl.h>
#include <TAN/TanInterface.h>

/// C/C++ include files
#include <iostream>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cctype>
#include <cerrno>

namespace {
  enum {
    TAN_SHUTDOWN = 1<<0,
    TAN_DB_DUMP  = 1<<1,
    TAN_TEST_ALLOCATE = 1<<2,
    TAN_TEST_ALIAS = 1<<3,
  };
  struct tan_util   {
    bool wait = true;
    bool quiet = false;
    int _dump_db(const std::string& node_name, int flags)   {
      if ( !quiet )  {
	if ( flags & TAN_DB_DUMP )   {
	  if ( TAN_SS_SUCCESS != ::tan_dump_dbase( node_name.c_str() ) )   {
	    ::lib_rtl_output(LIB_RTL_ERROR,"tan_nameserver_db_dump: "
			     "Failed to send DBDUMP TAN message to %s",
			     node_name.c_str());
	    return EINVAL;
	  }
	  ::lib_rtl_output(LIB_RTL_INFO,"tan_nameserver_db_dump: "
			   "Successfully sent DBDUMP TAN message to %s",
			   node_name.c_str());
	}
      }
      return 0;
    }
    int _shutdown(const std::string& node_name, int flags)   {
      if ( flags & TAN_SHUTDOWN )   {
	if ( TAN_SS_SUCCESS != ::tan_shutdown( node_name.c_str() ) )   {
	  ::lib_rtl_output(LIB_RTL_ERROR,"tan_nameserver_shutdown: "
			   "Failed to send TAN shutdown message to %s",
			   node_name.c_str());
	  return EINVAL;
	}
	::lib_rtl_output(LIB_RTL_INFO,"tan_nameserver_shutdown: "
			 "Successfully sent TAN shutdown message to %s",
			 node_name.c_str());
      }
      return 0;
    }
    int _test_alloc(const std::string& node_name,
		    const std::string& process,
		    const std::string& alias,
		    int flags)   {
      if ( flags & TAN_TEST_ALLOCATE )   {
	char c;
	NetworkChannel::Port port = 0;
	std::string proc = process.empty() ? RTL::processName() : process;
	_dump_db(node_name, TAN_DB_DUMP);
	if ( TAN_SS_SUCCESS != ::tan_allocate_port_number( proc.c_str(), &port) )   {
	  ::lib_rtl_output(LIB_RTL_ERROR,"tan_nameserver_allocate: "
			   "Failed to send ALLOCATE TAN message to %s",
			   node_name.c_str());
	  return EINVAL;
	}
	::lib_rtl_output(LIB_RTL_INFO,"tan_nameserver_allocate: "
			 "Successfully sent ALLOCATE TAN message to %s",
			 node_name.c_str());
	_dump_db(node_name, TAN_DB_DUMP);
	if ( wait )   {
	  ::printf("Return to continue.");
	  std::cin >> c;
	}
	if ( flags & TAN_TEST_ALIAS )   {
	  if ( TAN_SS_SUCCESS != ::tan_declare_alias( alias.c_str()) )   {
	    ::lib_rtl_output(LIB_RTL_ERROR,"tan_nameserver_declare_alias: "
			     "Failed to send DECLARE_ALIAS TAN message to %s",
			     alias.c_str());
	    return EINVAL;
	  }
	  ::lib_rtl_output(LIB_RTL_INFO,"tan_nameserver_allocate: "
			   "Successfully sent ALLOCATE TAN message to %s",
			   alias.c_str());
	  _dump_db(node_name, TAN_DB_DUMP);
	  if ( wait )  {
	    ::printf("Return to continue.");
	    std::cin >> c;
	  }
	  ::tan_remove_alias(alias.c_str());
	}
	::tan_deallocate_port_number(proc.c_str());
	_dump_db(node_name, TAN_DB_DUMP);
      }
      return 0;
    }
  };
}

// ----------------------------------------------------------------------------
// C Interface: Request TAN nameserver shutdown
// ----------------------------------------------------------------------------
extern "C" int tan_nameserver_utility(int argc, char** argv, int flags)  {
  std::string node_name = RTL::nodeName(), alias, process = RTL::processName();
  const char* tan_host = ::lib_rtl_getenv("TAN_NODE");
  int port_number = -1;
  bool wait = true, quiet = false;
  int status;
  RTL::CLI cli(argc, argv, [](int , char** ) {
    ::printf("$> tan_nameserver_util -opt [-opt]                 \n"
	     "   -node=<node-name>     Send shutdown to tan listening on node <node-name> \n"
	     "                            default: %s\n"
	     "   -port=<port-number>   Send shutdown to tan listening to <port-number>    \n"
	     "                            default: %d\n\n"
	     "   -allocate=<proc-name> Test port allocation \n"
	     "   -alias=<alias-name>   Test port alias declaration \n"
	     , RTL::nodeName().c_str(), ::tan_nameserver_port());
      ::exit(EINVAL);
  });
  cli.getopt("node", 4, node_name);
  cli.getopt("port", 4, port_number);
  cli.getopt("alias", 3, alias);
  cli.getopt("allocate", 3, process);
  wait = cli.getopt("nowait", 4) == 0;
  quiet = cli.getopt("quiet", 3);
  if ( cli.getopt("dump",     1) ) flags |= TAN_DB_DUMP;
  if ( cli.getopt("shutdown", 1) ) flags |= TAN_SHUTDOWN;
  if ( !process.empty() ) flags |= TAN_TEST_ALLOCATE|TAN_DB_DUMP;
  if ( !alias.empty()   ) flags |= TAN_TEST_ALLOCATE|TAN_TEST_ALIAS|TAN_DB_DUMP;

  if ( nullptr == tan_host && node_name.empty() )   {
    ::lib_rtl_output(LIB_RTL_ERROR,"tan_nameserver_util: "
		     "No node given: TAN_NODE is empty and "
		     "no command line arg -node=<name>");
    return EINVAL;
  }
  if ( node_name.empty() )  {
    std::string env = "TAN_NODE="+node_name;
    ::putenv((char*)env.c_str());
  }
  if ( port_number > 0 )   {
    tan_set_nameserver_port( port_number );
  }
  tan_util util { wait, quiet };
  status = util._test_alloc(node_name, process, alias, flags);
  if ( status ) return status;
  status = util._dump_db(node_name, flags);
  if ( status ) return status;
  status = util._shutdown(node_name, flags);
  if ( status ) return status;
  return 0;
}
// ----------------------------------------------------------------------------
// C Interface: Request TAN nameserver shutdown
// ----------------------------------------------------------------------------
extern "C" int tan_nameserver_shutdown(int argc, char** argv)  {
  return tan_nameserver_utility(argc, argv, TAN_SHUTDOWN);
}
// ----------------------------------------------------------------------------
// C Interface: Request TAN nameserver database dump
// ----------------------------------------------------------------------------
extern "C" int tan_nameserver_db_dump(int argc, char** argv)  {
  return tan_nameserver_utility(argc, argv, TAN_DB_DUMP);
}
// ----------------------------------------------------------------------------
// C Interface: Request TAN nameserver database dump
// ----------------------------------------------------------------------------
extern "C" int tan_nameserver_test_allocate(int argc, char** argv)  {
  return tan_nameserver_utility(argc, argv, TAN_TEST_ALLOCATE);
}
