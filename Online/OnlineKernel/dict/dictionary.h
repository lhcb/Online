//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
#include <cstdarg>
#include <iostream>
#include <ctime>
#include <map>
#include <vector>

#if defined(__CINT__) || defined(__MAKECINT__) || defined(__CLING__) || defined(__ROOTCLING__)
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#endif

#include <RTL/rtl.h>
#include <RTL/que.h>
#include <RTL/GlobalSection.h>
#if defined(__CINT__) || defined(__MAKECINT__) || defined(__CLING__) || defined(__ROOTCLING__)
#pragma link C++ namespace RTL;
#pragma link C++ class qentry_t;
#pragma link C++ class RTL::GlobalSection;
#endif

#include <RTL/bits.h>
#if defined(__CINT__) || defined(__MAKECINT__) || defined(__CLING__) || defined(__ROOTCLING__)
#pragma link C++ namespace Bits;
#pragma link C++ class Bits::BitMask<1>;
#pragma link C++ class Bits::BitMask<2>;
#pragma link C++ class Bits::BitMask<3>;
#pragma link C++ class Bits::BitMask<4>;
#endif

/// MBM
#include <MBM/bmstruct.h>
#include <MBM/bmdef.h>

#if defined(__CINT__) || defined(__MAKECINT__) || defined(__CLING__) || defined(__ROOTCLING__)
#pragma link C++ namespace MBM;
#pragma link C++ class REQ;
#pragma link C++ class USER;
#pragma link C++ class EVENT;
#pragma link C++ class CONTROL;
#pragma link C++ class BUFFERS;
#pragma link C++ class BufferMemory;

#pragma link C++ enum MBM::BufferStates;
#pragma link C++ enum MBM::EVTIDs;
#pragma link C++ enum MBM::BIDs;
#pragma link C++ enum MBM_EvTypes;
#pragma link C++ enum MBM_Communications;
#pragma link C++ enum MBM_Dimensions;
#pragma link C++ enum MBM_Internals;
#pragma link C++ enum MBM_MaskTypes;
#pragma link C++ enum MBM_RequestTypes;
#pragma link C++ enum MBM_FrequencyTypes;
#pragma link C++ enum MBM_IncludeModes;
#pragma link C++ enum MBM_Status;
#pragma link C++ typedef TriggerMask;
#pragma link C++ typedef UserMask;
#pragma link C++ typedef VetoMask;
#endif

#include <AMS/amsdef.h>
#include <WT/wtdef.h>

#include <SCR/scr.h>
#include <SCR/MouseSensor.h>
#if defined(__CINT__) || defined(__MAKECINT__) || defined(__CLING__) || defined(__ROOTCLING__)
#pragma link C++ namespace SCR;
#pragma link C++ class SCR::MouseEvent;
#pragma link C++ class SCR::MouseSensor;
#pragma link C++ enum  WT_Constants;
#endif

#include <CPP/FSM.h>
#include <CPP/IocSensor.h>
#include <CPP/AmsSensor.h>
#include <CPP/TimeSensor.h>
#include <CPP/Interactor.h>
#include <CPP/Event.h>
#include <CPP/BasicRequest.h>
#include <CPP/BasicQueue.h>
#include <CPP/PubArea.h>
#include <CPP/Table.h>

#if defined(__CINT__) || defined(__MAKECINT__) || defined(__CLING__) || defined(__ROOTCLING__)
#pragma link C++ namespace CPP;
#pragma link C++ class CPP::Event;
#pragma link C++ class CPP::Message;
#pragma link C++ class CPP::Table;
#pragma link C++ class CPP::Sensor;
#pragma link C++ class CPP::IocSensor;
#pragma link C++ class CPP::AmsSensor;
#pragma link C++ class CPP::TimeSensor;
#pragma link C++ class CPP::PyInteractor;
#pragma link C++ class FSM;
#pragma link C++ enum  FSM::ErrCond;
#pragma link C++ class _PubArea;
#pragma link C++ class BasicRequest;
#pragma link C++ class BasicQueue<BasicRequest>;
#endif

#include <NET/DataTransfer.h>
#include <NET/IOPortManager.h>
#include <NET/NetworkChannel.h>
#include <NET/NetworkConnection.h>
#include <NET/TcpNetworkChannel.h>
#include <NET/TcpConnection.h>
#include <NET/UdpNetworkChannel.h>
#include <NET/UdpConnection.h>
#include <TAN/TanInterface.h>
#include <TAN/TanMessage.h>
#if defined(__CINT__) || defined(__MAKECINT__) || defined(__CLING__) || defined(__ROOTCLING__)
#pragma link C++ namespace DataTransfer;
#pragma link C++ class NetworkAddress;
#pragma link C++ class NetworkChannel;
#pragma link C++ class NetworkConnection;
#pragma link C++ class SmartObject<NetworkConnection>;
#pragma link C++ class TcpNetworkChannel;
#pragma link C++ class TcpNetworkAddress;
#pragma link C++ class TcpConnection;
#pragma link C++ class UdpNetworkAddress;
#pragma link C++ class UdpNetworkChannel;
#pragma link C++ class UdpConnection;
#pragma link C++ class IOPortManager;
//#pragma link C++ class ;

#pragma link C++ class hostent;
#pragma link C++ class sockaddr_in;
#pragma link C++ class SmartObject<TanInterface>;
#pragma link C++ class TanInterface;
#pragma link C++ class TanMessage;
#endif
