from __future__ import print_function
#==========================================================================
#  LHCb Online software suite
#--------------------------------------------------------------------------
# Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
# All rights reserved.
#
# For the licensing terms see OnlineSys/LICENSE.
#
# Author     : M.Frank
#
#==========================================================================
from builtins import str
TimeEvent = 1
NetEvent  = 2
UpiEvent  = 3
IocEvent  = 4
QioEvent  = 5
TkEvent   = 6
PVSSEvent = 7

CPP = None
gbl = None
std = None

Interactor  = None
Event       = None
BaseSensor  = None
IocSensor   = None
TimeSensor  = None
Sensor      = None
UpiSensor   = None


def load_onlinekernel():
  import os
  import sys
  import platform
  #
  # Add ROOT to the python path in case it is not yet there....
  sys.path.append(os.environ['ROOTSYS'] + os.sep + 'lib')
  from ROOT import gSystem
  #
  print('INFO:   Loading library: libOnlineKernelDict')
  result = gSystem.Load("libOnlineKernelDict")
  if result < 0:
    raise Exception('onlinekernel: Failed to load the dataflow library libOnlineKernelDict: ' + gSystem.GetErrorStr())
  print('INFO:   Import Online namespace from ROOT')
  from ROOT import Online as module
  return module

# We are nearly there ....
name_space = __import__(__name__)
def import_namespace_item(ns, nam):
  scope = getattr(name_space, ns)
  attr  = getattr(scope, nam)
  setattr(name_space, nam, attr)
  return attr

# ---------------------------------------------------------------------------
#
try:
  import ROOT
  Online = load_onlinekernel()
  from ROOT import CPP as CPP
except Exception as X:
  import sys
  print('ERROR:  +--%-100s--+'%(100 * '-'), )
  print('ERROR:  |  %-100s  |'%('Failed to load onlinekernel framework library:'), )
  print('ERROR:  |  %-100s  |'%(str(X)), )
  print('ERROR:  +--%-100s--+'%(100 * '-'), )
  sys.exit(1)

#Interactor           = CPP.PyInteractor
Event                = CPP.Event
Sensor               = CPP.Sensor
AmsSensor            = CPP.AmsSensor
IocSensor            = CPP.IocSensor
TimeSensor           = CPP.TimeSensor
#from ROOT import UpiSensor as UpiSensor


# ---------------------------------------------------------------------------
class c_interface:
  def get_attr(self, slot, name):
    namespace = ROOT
    idx = name.find('::')
    if idx < 0:
      pass
    elif idx == 0:
      name = name[2:]
    elif idx > 0:
      namespace = getattr(ROOT, name[:idx])
      name = name[idx+2:]
      slot = name
    setattr(self, slot, getattr(namespace, name))
    if not getattr(self, slot):
      print('ERROR: Failed to load slot '+slot+': '+name)
      
  def __init__(self, name, start, functions, structures=None, enums=None, typedefs=None):
    self.functions = functions
    self.structures = structures
    self.enums = enums
    self.typedefs = typedefs
    self.name = name
    if self.functions:
      for s in self.functions:
        self.get_attr(s[start:], s)
    if self.structures:
      for s in self.structures:
        self.get_attr(s, s)
    if self.enums:
      for s in self.enums:
        self.get_attr(s, s)
    if self.typedefs:
      for s in self.typedefs:
        self.get_attr(s, s)

# ---------------------------------------------------------------------------
tan = c_interface(name='TAN', start=4,
                  functions=['tan_set_nameserver_port',
                             'tan_nameserver_port',
                             'tan_is_connected',
                             'tan_declare_port_number',
                             'tan_allocate_port_number',
                             'tan_allocate_port_number_ex',
                             'tan_allocate_new_port_number',
                             'tan_allocate_new_port_number_ex',
                             'tan_deallocate_port_number',
                             'tan_get_address_by_name',
                             'tan_declare_alias',
                             'tan_remove_alias',
                             'tan_dump_dbase',
                             'tan_shutdown',
                             'tan_host_name'],
                  structures=['TanMessage',
                              'TanInterface',
                              'hostent',
                              'sockaddr_in',
                              'SmartObject<TanInterface>'])

# ---------------------------------------------------------------------------
wtc = c_interface(name='WT', start=3,
                  functions=['wtc_init',
                             'wtc_shutdown',
                             'wtc_subscribe',
                             'wtc_remove',
                             'wtc_insert',
                             'wtc_insert_head',
                             'wtc_test_input',
                             'wtc_wait',
                             'wtc_get_routines',
                             'wtc_flush',
                             'wtc_flushp',
                             'wtc_spy_next_entry',
                             'wtc_error',
                             'wtc_restore_stack',
                             'wtc_add_stack',
                             ],
                  enums=['WT_Constants'])

# ---------------------------------------------------------------------------
mbm = c_interface(name='MBM', start=4,
                  functions=['mbm_qmtest_check_no_active_buffers',
                             'mbm_qmtest_check_start',
                             'mbm_deinstall',
                             'mbm_dump',
                             'mbm_mon',
                             'mbm_include',
                             'mbm_include_read',
                             'mbm_include_write',
                             'mbm_connect',
                             'mbm_exclude',
                             'mbm_buffer_address',
                             'mbm_buffer_size',
                             'mbm_buffer_memory',
                             'mbm_clear',
                             'mbm_buffer_name',
                             'mbm_add_req',
                             'mbm_del_req',
                             'mbm_get_event_a',
                             'mbm_get_event_try',
                             'mbm_get_event',
                             'mbm_free_event',
                             'mbm_pause',
                             'mbm_get_space_a',
                             'mbm_get_space_try',
                             'mbm_declare_event',
                             'mbm_declare_event_try',
                             'mbm_declare_event_and_send',
                             'mbm_free_space',
                             'mbm_send_space',
                             'mbm_wait_space',
                             'mbm_wait_space_a',
                             'mbm_cancel_request',
                             'mbm_set_cancelled',
                             'mbm_is_cancelled',
                             'mbm_stop_consumer',
                             'mbm_stop_producer',
                             'mbm_grant_update',
                             'mbm_events_actual',
                             'mbm_events_produced',
                             'mbm_events_seen',
                             'mbm_reset_statistics',
                             'mbm_min_alloc',
                             'mbm_events_in_buffer',
                             'mbm_space_in_buffer',
                             'mbm_process_exists',
                             'mbm_wait_event',
                             'mbm_wait_event_a',
                             'mbm_map_mon_memory',
                             'mbm_map_mon_memory_quiet',
                             'mbm_unmap_mon_memory',
                             'mbm_get_control_table',
                             'mbm_get_user_table',
                             'mbm_get_event_table',
                             'mbm_map_global_buffer_info',
                             'mbm_unmap_global_buffer_info' ],
                  structures=['REQ','USER','EVENT','CONTROL','BUFFERS','BufferMemory'],
                  enums=['MBM_EvTypes',
                         'MBM_Communications',
                         'MBM_Dimensions',
                         'MBM_Internals',
                         'MBM_MaskTypes',
                         'MBM_RequestTypes',
                         'MBM_FrequencyTypes',
                         'MBM_IncludeModes',
                         'MBM_Status',
                         'MBM::BIDs',
                         'MBM::EVTIDs',
                         'MBM::BufferStates'],
                  typedefs=['UserMask',
                            'VetoMask',
                            'TriggerMask'])

# ---------------------------------------------------------------------------
scr = c_interface(name='scrc', start=5,
                  functions=['scrc_change_rendition',
                             'scrc_insert_line',
                             'scrc_insert_char',
                             'scrc_delete_line',
                             'scrc_delete_char',
                             'scrc_resetANSI',
                             'scrc_setANSI',
                             'scrc_set_configure_handler',
                             'scrc_set_mouse_handler',
                             'scrc_handle_mouse',
                             'scrc_enable_mouse',
                             'scrc_disable_mouse',
                             'scrc_init_windows',
                             'scrc_open_window',
                             'scrc_move_window_to',
                             'scrc_move_window',
                             'scrc_change_window',
                             'scrc_get_window_size',
                             'scrc_get_window_position',
                             'scrc_window_moved',
                             'scrc_put_display_on_window',
                             'scrc_remove_display_from_window',
                             'scrc_delete_window',
                             'scrc_get_window_display',
                             'scrc_show_window',
                             'scrc_next_window',
                             'scrc_prev_window',
                             'scrc_window_has_display',
                             'scrc_hide_screen',
                             'scrc_hide_window',
                             'scrc_moving_window',
                             'scrc_iconify_window',
                             'scrc_send_window_to_icon',
                             'scrc_memory_of_windows',
                             'scrc_memory_of_window',
                             'scrc_memory_of_screen',
                             'scrc_configure_windows',
                             'scrc_change_pbd_characteristics',
                             'scrc_disable_unsolicited_input',
                             'scrc_enable_unsolicited_input',
                             'scrc_get_smgid',
                             'scrc_save_screen_rearm',
                             'scrc_test_input',
                             'scrc_handler_keyboard',
                             'scrc_get_last_key',
                             'scrc_read',
                             'scrc_fputs',
                             'scrc_wait',
                             'scrc_save_screen_rearm',
                             'scrc_rearm_keyboard',
                             'scrc_ast_keyboard',
                             'scrc_uniconify_display',
                             'scrc_iconify_display',
                             'scrc_delete_physical_pasteboard',
                             'scrc_read_keyboard',
                             'scrc_init_screen',
                             'scrc_end_screen',
                             'scrc_create_pasteboard',
                             'scrc_memory_of_pasteboard',
                             'scrc_get_pasteboard_infos',
                             'scrc_change_pasteboard',
                             'scrc_delete_pasteboard',
                             'scrc_delete_display',
                             'scrc_clear_screen',
                             'scrc_save_screen',
                             'scrc_restore_screen',
                             'scrc_repaint_screen',
                             'scrc_create_display',
                             'scrc_memory_of_display',
                             'scrc_enable_scroll',
                             'scrc_enable_resize',
                             'scrc_enable_drag',
                             'scrc_get_display_attr',
                             'scrc_read_from_display',
                             'scrc_set_border',
                             'scrc_paste_display',
                             'scrc_unpaste_display',
                             'scrc_bring_display_to_back',
                             'scrc_move_display',
                             'scrc_change_display',
                             'scrc_get_title',
                             'scrc_draw_box',
                             'scrc_draw_block',
                             'scrc_undraw_block',
                             'scrc_display_at',
                             'scrc_display_occluded',
                             'scrc_occluded',
                             'scrc_put_chars',
                             'scrc_erase_line',
                             'scrc_put_char_all',
                             'scrc_restore_cursor',
                             'scrc_save_cursor',
                             'scrc_fflush',
                             'scrc_action_moving_display',
                             'scrc_moving_display',
                             'scrc_action_resizing_display',
                             'scrc_resizing_display',
                             'scrc_load_font',
                             'scrc_ring_bell',
                             'scrc_check_key_buffer',
                             'scrc_set_cursor',
                             'scrc_set_cursor_abs',
                             'scrc_draw_char_on_pb',
                             'scrc_draw_char',
                             'scrc_get_char',
                             'scrc_put_char',
                             'scrc_putc',
                             'scrc_putes',
                             'scrc_puts',
                             'scrc_puti',
                             'scrc_begin_pasteboard_update',
                             'scrc_end_pasteboard_update',
                             'scrc_count_unmodified',
                             'cursor',
                             'scrc_set_scroll',
                             'scrc_cursor_on',
                             'scrc_cursor_off',
                             'scrc_get_console_dimensions',
                             'scrc_top_left_corner',
                             'scrc_top_right_corner',
                             'scrc_bottom_left_corner',
                             'scrc_bottom_right_corner',
                             'scrc_horizontal_bar',
                             'scrc_vertical_bar'],
                  structures=['SCR::Window',
                              'SCR::Paste_entry',
                              'SCR::Window',
                              'SCR::Screen',
                              'SCR::Pasteboard',
                              'SCR::Display',
                              'SCR::Update'],
                  enums=['SCR::ansi_val',
                         'SCR::scr_constants',
                         'SCR::scr_on_enums'])

# ---------------------------------------------------------------------------
