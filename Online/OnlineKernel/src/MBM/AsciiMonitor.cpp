//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
/*-----------------------------------------------------------------------*/
/*                                                                       */
/*                  ASCII BUFFER MANAGER DISPLAY                         */
/*                                                                       */
/*-----------------------------------------------------------------------*/
#include <cstdlib>
#include "MBM/Monitor.h"
#include "CPP/AsciiDisplay.h"
#include "SCR/ScrDisplay.h"

extern "C" int mbm_scr(int argc , char** argv) {
  MBM::Monitor mon(argc, argv, new ScrDisplay());
  return mon.monitor();
}

