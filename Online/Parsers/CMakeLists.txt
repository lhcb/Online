#===============================================================================
#  LHCb Online software suite
#-------------------------------------------------------------------------------
# Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
# All rights reserved.
#
# For the licensing terms see OnlineSys/LICENSE.
#
# Author     : M.Frank
#
#===============================================================================
#[=======================================================================[.rst:
Online/Parsers
--------------
#]=======================================================================]
#
online_library(Parsers
        src/XML/DocumentHandler.cpp
        src/XML/UriReader.cpp
        src/XML/XMLElements.cpp
        src/XML/XMLParsers.cpp
        src/XML/XMLTags.cpp
        src/Evaluator/Evaluator.cpp
        src/Evaluator/ExpressionEvaluator.cpp
        src/Evaluator/setStdMath.cpp
        src/Evaluator/setSystemOfUnits.cpp
        src/Parsers/Exceptions.cpp
        src/Parsers/Primitives.cpp
        src/Parsers/Printout.cpp)
target_include_directories(Parsers PRIVATE include/parsers_detail)
target_include_directories(Parsers INTERFACE 
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include/parsers_detail>
  $<INSTALL_INTERFACE:include/parsers_detail>
)
target_compile_options(Parsers PRIVATE -Wno-implicit-fallthrough)
target_compile_definitions(Parsers PUBLIC DD4HEP_NONE=1)
target_compile_definitions(Parsers PUBLIC dd4hep=Online)
target_compile_definitions(Parsers PUBLIC DD4HEP_USE_BOOST=1)
target_compile_definitions(Parsers PUBLIC DD4HEP_USE_TINYXML)
target_compile_definitions(Parsers PUBLIC DD4hep_Flavor=Gaudi)
target_compile_definitions(Parsers PUBLIC DD4HEP_PARSERS_NO_ROOT=1)
target_compile_definitions(Parsers PUBLIC DD4HEP_NS_NAME=Online)
target_link_libraries(Parsers PRIVATE Threads::Threads Boost::headers)
#
#===============================================================================
online_library(Options
        src/Options/Analyzer.cpp
        src/Options/Catalog.cpp
        src/Options/IncludedFiles.cpp
        src/Options/Message.cpp
        src/Options/Node.cpp
        src/Options/Parser.cpp
        src/Options/Position.cpp
        src/Options/Property.cpp
        src/Options/PropertyName.cpp
        src/Options/PropertyValue.cpp
        src/Options/Units.cpp
        src/Options/Utils.cpp
        src/Options/sys.cpp)

target_link_libraries(Options PRIVATE Boost::headers Online::Parsers)
#
#===============================================================================
online_library(Properties
        src/DD4hep/BasicGrammar.cpp
        src/DD4hep/BasicGrammarTypes.cpp
        src/DD4hep/ComponentProperties.cpp
        src/DD4hep/ExtraProperties.cpp
        src/DD4hep/PluginManager.cpp
        src/DD4hep/Plugins.cpp
        src/Spirit/Evaluators.cpp
        src/Spirit/ExtraParsers.cpp
        src/Spirit/ParserStandardList_Mapint_bool.cpp
        src/Spirit/ParserStandardList_Mapint_double.cpp
        src/Spirit/ParserStandardList_Mapint_float.cpp
        src/Spirit/ParserStandardList_Mapint_int.cpp
        src/Spirit/ParserStandardList_Mapint_long.cpp
        src/Spirit/ParserStandardList_Mapint_string.cpp
        src/Spirit/ParserStandardList_Mapped_bool.cpp
        src/Spirit/ParserStandardList_Mapped_double.cpp
        src/Spirit/ParserStandardList_Mapped_float.cpp
        src/Spirit/ParserStandardList_Mapped_int.cpp
        src/Spirit/ParserStandardList_Mapped_long.cpp
        src/Spirit/ParserStandardList_Mapped_string.cpp
        src/Spirit/ParserStandardList_Mapstring_bool.cpp
        src/Spirit/ParserStandardList_Mapstring_double.cpp
        src/Spirit/ParserStandardList_Mapstring_float.cpp
        src/Spirit/ParserStandardList_Mapstring_int.cpp
        src/Spirit/ParserStandardList_Mapstring_long.cpp
        src/Spirit/ParserStandardList_Mapstring_string.cpp
        src/Spirit/ParserStandardList_list_bool.cpp
        src/Spirit/ParserStandardList_list_double.cpp
        src/Spirit/ParserStandardList_list_float.cpp
        src/Spirit/ParserStandardList_list_int.cpp
        src/Spirit/ParserStandardList_list_long.cpp
        src/Spirit/ParserStandardList_list_string.cpp
        src/Spirit/ParserStandardList_set_bool.cpp
        src/Spirit/ParserStandardList_set_double.cpp
        src/Spirit/ParserStandardList_set_float.cpp
        src/Spirit/ParserStandardList_set_int.cpp
        src/Spirit/ParserStandardList_set_long.cpp
        src/Spirit/ParserStandardList_set_string.cpp
        src/Spirit/ParserStandardList_vector_bool.cpp
        src/Spirit/ParserStandardList_vector_double.cpp
        src/Spirit/ParserStandardList_vector_float.cpp
        src/Spirit/ParserStandardList_vector_int.cpp
        src/Spirit/ParserStandardList_vector_long.cpp
        src/Spirit/ParserStandardList_vector_string.cpp
        src/Spirit/ParsersObjects_PxPyPzEVector.cpp
        src/Spirit/ParsersObjects_XYZPoint.cpp
        src/Spirit/ParsersObjects_XYZVector.cpp
        src/Spirit/ParsersStandardMisc1.cpp
        src/Spirit/ParsersStandardMisc2.cpp
        src/Spirit/ParsersStandardMisc3.cpp
        src/Spirit/ParsersStandardMisc4.cpp
        src/Spirit/ParsersStandardMisc5.cpp
        src/Spirit/ParsersStandardSingle.cpp
        src/Spirit/ToStream.cpp)
target_link_libraries(Properties
  PUBLIC  Online::Parsers
  PRIVATE Boost::headers Gaudi::GaudiPluginService ROOT::Core ROOT::GenVector ${CMAKE_DL_LIBS})
if ( NOT BUILD_SHARED_LIBS )
  target_compile_definitions(Properties PRIVATE DD4HEP_BUILD_STATIC_LIBS=1)
endif()
#
#===============================================================================
online_install_includes(include)
