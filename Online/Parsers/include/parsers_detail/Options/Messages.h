#ifndef JOBOPTIONSVC_MESSAGES_H_
#define JOBOPTIONSVC_MESSAGES_H_
// ============================================================================
// STD & STL
// ============================================================================
#include <string>
#include <vector>
#include <iostream>
// ============================================================================
#include "Position.h"
// ============================================================================
namespace Gaudi { namespace Parsers {
    class Messages final {
    protected:
      int    m_level;
      size_t (*m_print)(int level, const char* fmt, ...);
      std::string m_currentFilename;
    public:
      Messages(int level, size_t (*print)(int level, const char* fmt, ...));
      void AddDebug(const std::string& info);
      void AddInfo(const std::string& info);
      void AddWarning(const std::string& warning);
      void AddError(const std::string& error);
      void AddFatal(const std::string& fatal);
      void AddInfo(const Position& pos, const std::string& info);
      void AddWarning(const Position& pos, const std::string& warning);
      void AddError(const Position& pos, const std::string& error);
    private:
      void AddMessage(int level, const std::string& message);      
      void AddMessage(int level, const Position& pos, const std::string& message);
    };

    // ============================================================================

    // ============================================================================
  }  /* Gaudi */ }  /* Parsers */
// ============================================================================

#endif  // JOBOPTIONSVC_MESSAGES_H_
