//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  ExtraProperties.cpp
//--------------------------------------------------------------------------
//
//  Package    : Dataflow
//
//  Author     : Markus Frank
//==========================================================================
// Note:
//
// This is an example file, which shows how to instantiate extera parsers,
// which are not part of DD4hep, but may be interesting for a
// client application.
//
//==========================================================================

#include <cstdint>

/// Framework includes
#include "Parsers/spirit/Parsers.h"
PARSERS_DECL_FOR_SINGLE(long long)
PARSERS_DECL_FOR_SINGLE(unsigned long long)

PARSERS_DECL_FOR_SINGLE(uint32_t)
PARSERS_DECL_FOR_SINGLE(uint64_t)
PARSERS_DECL_FOR_LIST(uint32_t)
PARSERS_DECL_FOR_LIST(uint64_t)

//==========================================================================
#include "Parsers/spirit/ParsersFactory.h"
typedef std::map<std::string, std::vector<std::string> >           map_string_vector_string_t;
typedef std::map<std::string, std::vector<int> >                   map_string_vector_int_t;

typedef std::pair<int, int>                                        pair_int_int_t;
typedef std::pair<std::string, std::pair<int, int> >               pair_string_pair_int_int_t;

typedef std::map<std::string,  std::pair<int, int> >               map_string_pair_int_int_t;
typedef std::vector<std::pair<std::string, std::pair<int, int> > > vector_pair_string_pair_int_int_t;

typedef std::map<std::string,  std::map<std::string,std::string> > map_string_map_string_string_t;

PARSERS_DEF_FOR_SINGLE(map_string_pair_int_int_t)
PARSERS_DEF_FOR_SINGLE(pair_string_pair_int_int_t)
PARSERS_DEF_FOR_SINGLE(vector_pair_string_pair_int_int_t)
PARSERS_DEF_FOR_SINGLE(map_string_map_string_string_t)

#include "Parsers/spirit/ParsersStandardListCommon.h"
PARSERS_DEF_FOR_LIST(uint32_t)
PARSERS_DEF_FOR_LIST(uint64_t)

#include "DD4hep/detail/BasicGrammar_inl.h"
#include "DD4hep/detail/ComponentProperties_inl.h"

DD4HEP_DEFINE_PARSER_GRAMMAR_CONT(uint32_t,eval_item)
DD4HEP_DEFINE_PARSER_GRAMMAR_CONT(uint64_t,eval_item)

#if !defined(DD4HEP_HAVE_ALL_PARSERS)
DD4HEP_DEFINE_PROPERTY_CONT(uint32_t)
DD4HEP_DEFINE_PROPERTY_CONT(uint64_t)
#endif

typedef std::map<int, uint32_t> map_int_uint_t;
DD4HEP_DEFINE_PROPERTY_TYPE(map_int_uint_t)
typedef std::map<std::string, uint32_t> map_str_uint_t;
DD4HEP_DEFINE_PROPERTY_TYPE(map_str_uint_t)

typedef std::map<int, uint64_t> map_int_ulong_t;
DD4HEP_DEFINE_PROPERTY_TYPE(map_int_ulong_t)
typedef std::map<std::string, uint64_t> map_str_ulong_t;
DD4HEP_DEFINE_PROPERTY_TYPE(map_str_ulong_t)

DD4HEP_DEFINE_PARSER_GRAMMAR(long long,eval_item)
DD4HEP_DEFINE_PARSER_GRAMMAR(unsigned long long,eval_item)

DD4HEP_DEFINE_PROPERTY_TYPE(long long)
DD4HEP_DEFINE_PROPERTY_TYPE(unsigned long long)

DD4HEP_DEFINE_PARSER_GRAMMAR(map_string_vector_string_t,eval_obj)
DD4HEP_DEFINE_PROPERTY_TYPE(map_string_vector_string_t)

DD4HEP_DEFINE_PARSER_GRAMMAR(map_string_map_string_string_t,eval_obj)
DD4HEP_DEFINE_PROPERTY_TYPE(map_string_map_string_string_t)

DD4HEP_DEFINE_PARSER_GRAMMAR(map_string_vector_int_t,eval_obj)
DD4HEP_DEFINE_PROPERTY_TYPE(map_string_vector_int_t)

DD4HEP_DEFINE_PROPERTY_TYPE(pair_int_int_t)
DD4HEP_DEFINE_PARSER_GRAMMAR(pair_string_pair_int_int_t,eval_pair)

DD4HEP_DEFINE_PARSER_GRAMMAR(map_string_pair_int_int_t,eval_container)
DD4HEP_DEFINE_PROPERTY_TYPE(map_string_pair_int_int_t)

DD4HEP_DEFINE_PARSER_GRAMMAR(vector_pair_string_pair_int_int_t,eval_container)
DD4HEP_DEFINE_PROPERTY_TYPE(vector_pair_string_pair_int_int_t)

