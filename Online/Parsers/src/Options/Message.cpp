// ============================================================================
// INCLUDE
// ============================================================================
#include <sstream>
#include <stdexcept>
#include "Options/Messages.h"
#include "Options/Position.h"
// ============================================================================
// BOOST:
// ============================================================================
#include <boost/format.hpp>
// ============================================================================
// Namespaces:
// ============================================================================
namespace gp = Gaudi::Parsers;


gp::Messages::Messages(int level, size_t (*print)(int lvl, const char* fmt, ...))
  : m_level(level), m_print(print)
{
}

void gp::Messages::AddMessage(int level, const std::string& m)   {
  if ( m_level <= level )  {
    if ( m_print )  {
      (*m_print)(level, "%s", m.c_str());
      return;
    }
    switch(level)  {
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
      std::cout << m << std::endl;
      break;
    case 6:
    default:
      std::cout << m << std::endl;
      throw std::runtime_error(m);
      break;
    }
  }
}

void gp::Messages::AddMessage(int level, const Position& pos, const std::string& m)   {
  if (pos.filename() != m_currentFilename) {
    AddMessage(level,"# =======> "+pos.filename());
    m_currentFilename = pos.filename();
  }
  std::stringstream stream;
  stream << "# " << boost::format("(%1%,%2%): %3%") % pos.line() % pos.column() % m;
  AddMessage(level,stream.str());
}

void gp::Messages::AddDebug(const std::string& m)    {  AddMessage(2,m); }
void gp::Messages::AddInfo(const std::string& m)     {  AddMessage(3,m); }
void gp::Messages::AddWarning(const std::string& m)  {  AddMessage(4,m); }
void gp::Messages::AddError(const std::string& m)    {  AddMessage(5,m); }
void gp::Messages::AddFatal(const std::string& m)    {  AddMessage(6,m); }
void gp::Messages::AddInfo(const Position& pos, const std::string& m)     {  AddMessage(3,pos,m); }
void gp::Messages::AddWarning(const Position& pos, const std::string& m)  {  AddMessage(4,pos,m); }
void gp::Messages::AddError(const Position& pos, const std::string& m)    {  AddMessage(5,pos,m); }
