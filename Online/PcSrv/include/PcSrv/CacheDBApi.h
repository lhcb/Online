//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================
#ifndef RPC_CACHEDBAPI_H
#define RPC_CACHEDBAPI_H

// Framework include files
#include "PcSrv/Task.h"

// C/C++ include files
#include <vector>
#include <memory>
#include <mutex>
#include <chrono>
#include <regex>

/// Namespace for the dimrpc based implementation
namespace taskdb {

  ///  Structure to represent the CacheDB API
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.05.2017
   */
  class CacheDBApi  {
  public:
    /// Definition of the task collection type
    typedef std::vector<Task>          tasks_t;
    typedef std::vector<TaskSet>       sets_t;
    typedef std::vector<NodeClass>     classes_t;
    typedef std::vector<NodeType>      nodes_t;
    typedef std::vector<TasksInSet>    tasks_in_set_t;
    typedef std::vector<SetsInClass>   sets_in_class_t;
    typedef std::vector<ClassesInNode> classes_in_node_t;
    typedef std::chrono::system_clock::time_point  time_point_t;
    typedef std::map<std::string, std::unique_ptr<std::regex> > regex_cache_t;
    std::string               m_blob;
    mutable std::mutex        m_lock;
    std::unique_ptr<Snapshot> m_snapshot;
    time_point_t              m_connectTime;
    mutable regex_cache_t     m_regexCache;
    mutable size_t            m_num_calls = 0;
    mutable size_t            m_num_errs = 0;


    void check() const;

  public:
    /// Default constructor
    CacheDBApi();
    /// Move constructor
    CacheDBApi(CacheDBApi&& copy) = delete;
    /// Copy constructor
    CacheDBApi(const CacheDBApi& copy) = delete;
    /// Default destructor
    virtual ~CacheDBApi();

    size_t numberOfCalls()    const   {  return m_num_calls;   }
    size_t numberOfErrors()   const   {  return m_num_errs;    }

    /// Access CacheDB to check if a database update is required
    bool       needDbUpdate(const std::string& host, int port, long stamp) const;

    /// Access CacheDB to retrieve the tasks for this node
    tasks_t    getTasksByNode(const std::string& match)   const;

    /// Attach snapshot object
    void setSnapShot(std::string&& data);

    /// Attach snapshot object
    void setSnapShotUnzipped(std::vector<unsigned char>&& data);

    /// Access the decompressed snapshot
    const Snapshot& snapshot()   const;

    /// Retrieve the database cache (compressed and base64 encoded)
    std::string getCache(long stamp)  const;

    /// Access CacheDB to add a new task
    /** Add a Tasks instance to the Tasks table
     *
     * \param  task               [REQUIRED]  Name of the task to be added
     * \param  utgid              [OPTIONAL]  Task unique identifier
     * \param  command            [OPTIONAL]  Executable or script to be executed
     * \param  task_parameters    [OPTIONAL]  String with parameters required to submit the desired command
     * \param  command_parameters [OPTIONAL]  String with parameters required to execute the command
     * \param  restart_parameters [OPTIONAL]  String with parameters steering subsequent launch
     * \param  description        [OPTIONAl]  String containing explicatory text describing the command
     * \return                                Statuscode indicating success or failure / Exception
     */
    int addTask(const std::string& name,
		const std::string& utgid="",
		const std::string& command="",
		const std::string& task_parameters="",
		const std::string& command_parameters="",
		const std::string& restart_parameters="",
		const std::string& description="")   const;
    /// Access CacheDB to retrieve matching tasks
    tasks_t    getTask(const std::string& match)   const;
    /// Access CacheDB to delete matching tasks
    int        deleteTask(const std::string& match)  const;

    /// Add new task set to the database
    /** Add new task set to the database
     *
     * \param  name               [REQUIRED]  Name of the task set to be added
     * \param  description        [OPTIONAl]  String containing explicatory text
     * \return                                Statuscode indicating success or failure / Exception
     */
    int addSet(const std::string& name,
	       const std::string& description="")   const;
    /// Delete task set from the database
    /** Delete task set from the database
     *
     * \param  name               [REQUIRED]  Name of the task set to be deleted
     * \return                                Statuscode indicating success or failure / Exception
     */
    int deleteSet(const std::string& name)   const;
    /// Access CacheDB to retrieve matching task sets
    sets_t     getSet(const std::string& match)   const;
    /// Access CacheDB to retrieve matching task in a given task set
    tasks_in_set_t tasksInSet(const std::string& match)   const;

    /// Add new node class to the database
    /** Add new node class to the database
     *
     * \param  name               [REQUIRED]  Name of the node class to be added
     * \param  description        [OPTIONAl]  String containing explicatory text
     * \return                                Statuscode indicating success or failure / Exception
     */
    int addClass(const std::string& name,
		 const std::string& description="")   const;
    /// Delete node class from the database
    /** Delete node class from the database
     *
     * \param  name               [REQUIRED]  Name of the node class to be deleted
     * \return                                Statuscode indicating success or failure / Exception
     */
    int deleteClass(const std::string& name)   const;
    /// Access CacheDB to retrieve matching node classes
    classes_t  getClass(const std::string& match)   const;
    /// Access CacheDB to retrieve matching task sets in a given node class
    sets_in_class_t taskSetsInClass(const std::string& match)   const;

    /// Add new node type to the database
    /** Add new node type to the database
     *
     * \param  name               [REQUIRED]  Name of the node type to be added
     * \param  description        [OPTIONAl]  String containing explicatory text
     * \return                                Statuscode indicating success or failure / Exception
     */
    int addNode(const std::string& name,
		int   priority,
		const std::string& description="")   const;
    /// Delete node type from the database
    /** Delete node type from the database
     *
     * \param  name               [REQUIRED]  Name of the node type to be deleted
     * \return                                Statuscode indicating success or failure / Exception
     */
    int deleteNode(const std::string& name)   const;
    /// Access CacheDB to retrieve matching node types
    nodes_t    getNode(const std::string& match)   const;

    /// Access CacheDB to retrieve matching node classes in a given node type
    classes_in_node_t classesInNode(const std::string& match)   const;
  };
}       // End namespace taskdb
#endif  // RPC_CACHEDBAPI_H
