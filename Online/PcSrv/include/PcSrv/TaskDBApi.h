//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================
#ifndef RPC_TASKDBAPI_H
#define RPC_TASKDBAPI_H

// Framework include files
#include "PcSrv/Task.h"
#include "RPC/RpcClientHandle.h"

// C/C++ include files
#include <vector>

/// Namespace for the dimrpc based implementation
namespace taskdb {

  ///  Structure to represent the TaskDB API
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.05.2017
   */
  class TaskDBApi : public rpc::XmlRpcClientH  {
  public:
    /// Definition of the task collection type
    typedef Task                       task_t;
    typedef std::vector<Task>          tasks_t;
    typedef std::vector<TaskSet>       sets_t;
    typedef std::vector<NodeClass>     classes_t;
    typedef std::vector<NodeType>      nodes_t;
    typedef std::vector<TasksInSet>    tasks_in_set_t;
    typedef std::vector<SetsInClass>   sets_in_class_t;
    typedef std::vector<ClassesInNode> classes_in_node_t;

    mutable size_t num_calls = 0;

  public:
    /// Constructors
    using rpc::XmlRpcClientH::XmlRpcClientH;

    /// Access the number of calls performed by this interface
    size_t numberOfCalls()   const   {  return num_calls;  }

    int test(bool with_time=true)  const;

    /// Access TaskDB to check if a database update is required
    bool       needDbUpdate(const std::string& host, int port, long stamp) const;

    /// Access TaskDB to retrieve the tasks for this node
    tasks_t    getTasksByNode(const std::string& match)   const;

    /// Retrieve the database cache (compressed and base64 encoded)
    std::string getCacheRaw(long stamp)  const;

    /// Retrieve the database cache
    std::vector<unsigned char> getCache(long stamp)  const;

    /// Access TaskDB to add a new task
    /** Add a Tasks instance to the Tasks table
     *
     * \param  task               [REQUIRED]  Name of the task to be added
     * \param  utgid              [OPTIONAL]  Task unique identifier
     * \param  command            [OPTIONAL]  Executable or script to be executed
     * \param  task_parameters    [OPTIONAL]  String with parameters required to submit the desired command
     * \param  command_parameters [OPTIONAL]  String with parameters required to execute the command
     * \param  restart_parameters [OPTIONAL]  String with parameters steering subsequent launch
     * \param  description        [OPTIONAl]  String containing explicatory text describing the command
     * \return                                Statuscode indicating success or failure / Exception
     */
    int addTask(const std::string& name,
		const std::string& utgid="",
		const std::string& command="",
		const std::string& task_parameters="",
		const std::string& command_parameters="",
		const std::string& restart_parameters="",
		const std::string& description="")   const;
    /// Access TaskDB to retrieve matching tasks
    tasks_t    getTask(const std::string& match)   const;
    /// Access TaskDB to delete matching tasks
    int        deleteTask(const std::string& match)  const;

    /// Add new task set to the database
    /** Add new task set to the database
     *
     * \param  name               [REQUIRED]  Name of the task set to be added
     * \param  description        [OPTIONAl]  String containing explicatory text
     * \return                                Statuscode indicating success or failure / Exception
     */
    int addSet(const std::string& name,
	       const std::string& description="")   const;
    /// Delete task set from the database
    /** Delete task set from the database
     *
     * \param  name               [REQUIRED]  Name of the task set to be deleted
     * \return                                Statuscode indicating success or failure / Exception
     */
    int deleteSet(const std::string& name)   const;
    /// Access TaskDB to retrieve matching task sets
    sets_t     getSet(const std::string& match)   const;
    /// Access TaskDB to retrieve matching task in a given task set
    tasks_in_set_t tasksInSet(const std::string& match)   const;

    /// Add new node class to the database
    /** Add new node class to the database
     *
     * \param  name               [REQUIRED]  Name of the node class to be added
     * \param  description        [OPTIONAl]  String containing explicatory text
     * \return                                Statuscode indicating success or failure / Exception
     */
    int addClass(const std::string& name,
		 const std::string& description="")   const;
    /// Delete node class from the database
    /** Delete node class from the database
     *
     * \param  name               [REQUIRED]  Name of the node class to be deleted
     * \return                                Statuscode indicating success or failure / Exception
     */
    int deleteClass(const std::string& name)   const;
    /// Access TaskDB to retrieve matching node classes
    classes_t  getClass(const std::string& match)   const;
    /// Access TaskDB to retrieve matching task sets in a given node class
    sets_in_class_t taskSetsInClass(const std::string& match)   const;

    /// Add new node type to the database
    /** Add new node type to the database
     *
     * \param  name               [REQUIRED]  Name of the node type to be added
     * \param  description        [OPTIONAl]  String containing explicatory text
     * \return                                Statuscode indicating success or failure / Exception
     */
    int addNode(const std::string& name,
		int   priority,
		const std::string& description="")   const;
    /// Delete node type from the database
    /** Delete node type from the database
     *
     * \param  name               [REQUIRED]  Name of the node type to be deleted
     * \return                                Statuscode indicating success or failure / Exception
     */
    int deleteNode(const std::string& name)   const;
    /// Access TaskDB to retrieve matching node types
    nodes_t    getNode(const std::string& match)   const;

    /// Access TaskDB to retrieve matching node classes in a given node type
    classes_in_node_t classesInNode(const std::string& match)   const;
  };
}       // End namespace taskdb
#endif  // RPC_TASKDBAPI_H
