
def printObject(type, key, object):
    print ('%-16s:      %s'%(type, object[key],))
    for k in object.keys():
      if k != key:
        print ('%-16s:      %-16s %s'%('',k+':',object[k],))

def printObjects(type, key, objects):
  cnt = 0
  for obj in objects:
    cnt = cnt+1
    pref = '%s.%-3d'%(type, cnt,)
    printObject(pref, key, obj)

def printTasks(objects):      printObjects('Task',      'task',          objects)
def printTaskSets(objects):   printObjects('TaskSet',   'task_set',      objects)
def printClasses(objects):    printObjects('NodeClass', 'node_class',    objects)
def printNodeTypes(objects):  printObjects('NodeType',  'regex',         objects)

replacements = {}
replacements['<TOP_LOGGER>'] = 'ecs03'
replacements['<SCRIPTS>'] = '/group/online/dataflow/scripts'

def printTasksFMC(node, tasks):
  for t in tasks:
    cmd = 'pcAdd -u %s %s %s %s'%(t['utgid'],t['submit_parameters'],t['command'],t['command_parameters'],)
    cmd = cmd.replace('<NODE>',node)
    for k,v in replacements.items():
      cmd = cmd.replace(k,v)
    print (cmd)

