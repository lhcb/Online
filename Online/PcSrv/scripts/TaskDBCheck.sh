#!/bin/bash
export UTGID
#
export HOST=`hostname -s`
if [ -r /etc/sysconfig/dim ]; then
   . /etc/sysconfig/dim
   export DIM_DNS_NODE
fi
#
cd /home/frankm/upgrade_sw/Online;
. setup.x86_64-centos7-gcc8-do0.vars;
exec -a ${UTGID} gentest.exe libPcSrvLib.so run_task_check -server=${DIM_DNS_NODE} -port=3501 -node=${HOST} -dns=${DIM_DNS_NODE} -print=3
