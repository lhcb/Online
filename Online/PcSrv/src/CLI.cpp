//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================

// C/C++ include files
#include <sstream>
#include <fstream>
#include <iostream>
#include <stdexcept>

// Framework include files
#include <PcSrv/Task.h>
#include <PcSrv/TaskDBApi.h>
#include <HTTP/HttpClient.h>
#include <RPC/XMLRPC.h>
#include <RTL/rtl.h>

typedef taskdb::TaskDBApi Api;

namespace {
  std::ostream& logger(const std::string& prefix="")  {
    return std::cout << prefix;
  }

  //------------------------------------------------------------------------
  template <typename F> int exception_wrapper(const std::string& function, F func)   {
    int ret = EINVAL;
    try {
      ret = func();
    }
    catch(const std::exception& e)   {
      logger(function) << "  Exception: " << e.what() << std::endl;
    }
    catch(...)   {
      logger(function) << "  UNKNOWN Exception...." << std::endl;
    }
    return ret;
  }

  //------------------------------------------------------------------------
  std::unique_ptr<rpc::RpcClientBase> get_connection(int argc, char** argv)   {
    std::string port  = "3500";
    std::string mount = "/TDBDATA/XMLRPC";
    std::string host  = "ecs03.lbdaq.cern.ch";
    RTL::CLI cli(argc, argv, [] (int, char**)   {
	std::cout << "    -host=<host-name>   TaskDB server host name   \n"
	     << "    -port=<port-number> TaskDB server port        \n"
	     << "    -mount=<mount-name> Web server directory name \n"
	     << std::endl;
      });
    cli.getopt("port",  3, port);
    cli.getopt("host",  3, host);
    cli.getopt("mount", 3, mount);
    return std::make_unique<rpc::RpcClient<http::HttpClient> >(host, mount, port);
  }

  //------------------------------------------------------------------------
  std::size_t dump_objects(const Api::tasks_t& tasks)   {
    for( const auto& t : tasks )    {
      logger() << "Task: name:        " << t.name << std::endl;
      logger() << "      utgid:       " << t.utgid << std::endl;
      logger() << "      command:     " << t.command << std::endl;
      logger() << "      params:      " << t.command_parameters << std::endl;
      logger() << "      submit:      " << t.submit_parameters << std::endl;
      logger() << "      restart:     " << t.restart_parameters << std::endl;
      logger() << "      description: " << t.description << std::endl;
    }
    logger()   << "Got a total of " << tasks.size() << " tasks." << std::endl;
    return tasks.size();
  }

  //------------------------------------------------------------------------
  std::size_t dump_objects(const std::vector<taskdb::TaskSet>& obj)   {
    for( const auto& o : obj )    {
      logger() << "TaskSet: name:     " << o.name << std::endl;
      logger() << "      description: " << o.description << std::endl;
    }
    logger()   << "Got a total of " << obj.size() << " task sets." << std::endl;
    return obj.size();
  }

  //------------------------------------------------------------------------
  std::size_t dump_objects(const std::vector<taskdb::NodeClass>& obj)   {
    for( const auto& o : obj )    {
      logger() << "Node class: name:  " << o.name << std::endl;
      logger() << "      description: " << o.description << std::endl;
    }
    logger()   << "Got a total of " << obj.size() << " node classes." << std::endl;
    return obj.size();
  }

  //------------------------------------------------------------------------
  std::size_t dump_objects(const std::vector<taskdb::NodeType>& obj)   {
    for( const auto& o : obj )    {
      logger() << "Node type: name:   " << o.name << std::endl;
      logger() << "      description: " << o.description << std::endl;
    }
    logger()   << "Got a total of " << obj.size() << " node types." << std::endl;
    return obj.size();
  }

  //------------------------------------------------------------------------
  template <typename T> std::size_t dump_links(const std::vector<T>& obj)   {
    for( const auto& o : obj )    {
      logger() << "Link name:     " << o.name << std::endl;
    }
    logger()   << "Got a total of " << obj.size() << " links." << std::endl;
    return obj.size();
  }

  //------------------------------------------------------------------------
  int check_result(int res)   {
    logger() << ((res == 1) ? "SUCCESS" : "FAILED") << std::endl;
    return res;
  }

  void _print_header(RTL::CLI& cli, const std::string& msg)  {
    if ( cli.getopt("show_header", 4) != nullptr )  {
      std::string port;
      std::string mount;
      std::string host;
      cli.getopt("port",  3, port);
      cli.getopt("host",  3, host);
      cli.getopt("mount", 3, mount);
      std::cout << "+-------------------------------------------------------------------" << std::endl
		<< "| Executing: " << msg;
      if ( !host.empty()  ) std::cout << " -host="  << host;
      if ( !mount.empty() ) std::cout << " -mount=" << mount;
      if ( !port.empty()  ) std::cout << " -port="  << port;
      std::cout << std::endl
		<< "+-------------------------------------------------------------------" << std::endl;
    }
  }
}

//--------------------------------------------------------------------------
extern "C" int taskdb_test_connection(int argc, char** argv)   {
  return exception_wrapper(__func__, [argc, argv] {
      int loop = 1;
      RTL::CLI cli(argc, argv, [] (int ac, char** av)  {
	  std::cout << "  taskdb_test_connection -opt [-opt] \n";
	  get_connection(ac, av);
	  ::exit(0);
	});
      _print_header(cli, "taskdb_test_connection  -loop="+std::to_string(loop));
      Api api(get_connection(argc, argv));
      cli.getopt("loop", 4, loop);
      int result = 0;
      for(int i=0; i<loop; ++i)   {
	result = api.test(cli.getopt("time",4) != 0);
	std::cout << "  taskdb_test_connection executed. Result: " << result << std::endl;
      }
      return result;
    });
}

//--------------------------------------------------------------------------
extern "C" int taskdb_tasks_by_node(int argc, char** argv)   {
  return exception_wrapper(__func__, [argc, argv] {
      std::string node  = "hlta1120";
      RTL::CLI cli(argc, argv, [] (int ac, char** av)  {
	  std::cout << "  taskdb_tasks_by_node -opt [-opt]                \n";
	  std::cout << "    -node=<node-name>   Node name to query tasks  \n";
	  get_connection(ac, av);
	  ::exit(0);
	});
      cli.getopt("match",  3, node);
      _print_header(cli, "taskdb_tasks_by_node -node="+node);
      Api api(get_connection(argc,argv));
      auto tasks = api.getTasksByNode(node);
      dump_objects(tasks);
      return 1;
    });
}

//--------------------------------------------------------------------------
extern "C" int taskdb_task(int argc, char** argv)   {
  std::string func = __func__;
  return exception_wrapper(func, [func, argc, argv] {
      std::string match  = "*";
      RTL::CLI cli(argc, argv, [] (int ac, char** av)  {
	  std::cout << "  taskdb_task -opt [-opt]                               \n";
	  std::cout << "    -match=<match-name>   Match task name to query tasks \n";
	  get_connection(ac, av);
	  ::exit(0);
	});
      cli.getopt("match",  3, match);
      _print_header(cli, "taskdb_task -match="+match);
      Api api(get_connection(argc,argv));
      auto tasks = api.getTask(match);
      dump_objects(tasks);
      return 1;
    });
}

//--------------------------------------------------------------------------
extern "C" int taskdb_delete_task(int argc, char** argv)   {
  return exception_wrapper(__func__, [argc, argv] {
      std::string match  = "*";
      RTL::CLI cli(argc, argv, [] (int ac, char** av)  {
	  std::cout << "  taskdb_delete_task -opt [-opt]                               \n";
	  std::cout << "    -match=<match-name>   Match task name to query tasks \n";
	  get_connection(ac, av);
	  ::exit(0);
	});
      cli.getopt("match",  3, match);
      _print_header(cli, "taskdb_delete_task -match="+match);
      Api api(get_connection(argc,argv));
      auto result = api.deleteTask(match);
      return check_result(result);
    });
}

//--------------------------------------------------------------------------
extern "C" int taskdb_set(int argc, char** argv)   {
  return exception_wrapper(__func__, [argc, argv] {
      std::string match  = "*";
      RTL::CLI cli(argc, argv, [] (int ac, char** av)  {
	  std::cout << "  taskdb_set -opt [-opt]                               \n";
	  std::cout << "    -match=<match-name>   Match name to query sets \n";
	  get_connection(ac, av);
	  ::exit(0);
	});
      cli.getopt("match", 3, match);
      _print_header(cli, "taskdb_set -match="+match);
      Api api(get_connection(argc,argv));
      auto result = api.getSet(match);
      dump_objects(result);
      return 1;
    });
}

//--------------------------------------------------------------------------
extern "C" int taskdb_tasks_in_set(int argc, char** argv)   {
  return exception_wrapper(__func__, [argc, argv] {
      std::string match  = "*";
      RTL::CLI cli(argc, argv, [] (int ac, char** av)  {
	  std::cout << "  taskdb_tasks_in_set -opt [-opt]                  \n";
	  std::cout << "    -match=<match-name>   Match name to query sets \n";
	  get_connection(ac, av);
	  ::exit(0);
	});
      cli.getopt("match",  3, match);
      _print_header(cli, "taskdb_tasks_in_set -match="+match);
      Api api(get_connection(argc,argv));
      auto result = api.tasksInSet(match);
      dump_links(result);
      return 1;
    });
}

//--------------------------------------------------------------------------
extern "C" int taskdb_class(int argc, char** argv)   {
  return exception_wrapper(__func__, [argc, argv] {
      std::string match  = "*";
      RTL::CLI cli(argc, argv, [] (int ac, char** av)  {
	  std::cout << "  taskdb_class -opt [-opt]                               \n";
	  std::cout << "    -match=<match-name>   Match name to query classes.   \n";
	  get_connection(ac, av);
	  ::exit(0);
	});
      cli.getopt("match",  3, match);
      _print_header(cli, "taskdb_class -match="+match);
      Api api(get_connection(argc,argv));
      auto result = api.getClass(match);
      dump_objects(result);
      return 1;
    });
}

//--------------------------------------------------------------------------
extern "C" int taskdb_tasksets_in_class(int argc, char** argv)   {
  return exception_wrapper(__func__, [argc, argv] {
      std::string match  = "*";
      RTL::CLI cli(argc, argv, [] (int ac, char** av)  {
	  std::cout << "  taskdb_tasksets_in_class -opt [-opt]                      \n";
	  std::cout << "    -match=<match-name>   Match name to query classes   \n";
	  get_connection(ac, av);
	  ::exit(0);
	});
      cli.getopt("match",  3, match);
      _print_header(cli, "taskdb_tasksets_in_class -match="+match);
      Api api(get_connection(argc,argv));
      auto result = api.taskSetsInClass(match);
      dump_links(result);
      return 1;
    });
}

//--------------------------------------------------------------------------
extern "C" int taskdb_tasks_in_class(int argc, char** argv)   {
  return exception_wrapper(__func__, [argc, argv] {
      std::string match  = "*";
      RTL::CLI cli(argc, argv, [] (int ac, char** av)  {
	  std::cout << "  taskdb_tasks_in_class -opt [-opt]                  \n";
	  std::cout << "    -match=<match-name>   Match name to query classes \n";
	  get_connection(ac, av);
	  ::exit(0);
	});
      cli.getopt("match",  3, match);
      _print_header(cli, "taskdb_tasks_in_class -match="+match);
      Api api(get_connection(argc,argv));
      std::map<std::string, Api::task_t> all_tasks;
      Api::sets_in_class_t sets = api.taskSetsInClass(match);
      for( const auto& s : sets )    {
	Api::tasks_in_set_t tasks_in_set = api.tasksInSet(s.name);
	for(const auto& tis : tasks_in_set)  {
	  Api::tasks_t tasks = api.getTask(tis.name);
	  for(const auto& task : tasks)  {
	    all_tasks[task.name] = task;
	  }
	}
      }
      Api::tasks_t matching_tasks;
      for(const auto& t : all_tasks)
	matching_tasks.emplace_back(t.second);
      dump_objects(matching_tasks);
      return 1;
    });
}

//--------------------------------------------------------------------------
extern "C" int taskdb_node(int argc, char** argv)   {
  return exception_wrapper(__func__, [argc, argv] {
      std::string match  = "*";
      RTL::CLI cli(argc, argv, [] (int ac, char** av)  {
	  std::cout << "  taskdb_node -opt [-opt]                               \n";
	  std::cout << "    -match=<match-name>   Match name to query nodes.   \n";
	  get_connection(ac, av);
	  ::exit(0);
	});
      cli.getopt("match",  3, match);
      _print_header(cli, "taskdb_node -match="+match);
      Api api(get_connection(argc,argv));
      auto result = api.getNode(match);
      dump_objects(result);
      return 1;
    });
}

//--------------------------------------------------------------------------
extern "C" int taskdb_tasks_in_node(int argc, char** argv)   {
  return exception_wrapper(__func__, [argc, argv] {
      std::string match  = "*";
      RTL::CLI cli(argc, argv, [] (int ac, char** av)  {
	  std::cout << "  taskdb_tasks_in_class -opt [-opt]                  \n";
	  std::cout << "    -match=<match-name>   Match name to query classes \n";
	  get_connection(ac, av);
	  ::exit(0);
	});
      cli.getopt("match",  3, match);
      _print_header(cli, "taskdb_tasks_in_node -match="+match);
      Api api(get_connection(argc,argv));
      std::map<std::string, Api::task_t> all_tasks;
      Api::classes_in_node_t classes = api.classesInNode(match);
      for(const auto& c : classes)   {
	Api::sets_in_class_t sets = api.taskSetsInClass(c.name);
	for( const auto& s : sets )    {
	  Api::tasks_in_set_t tasks_in_set = api.tasksInSet(s.name);
	  for(const auto& tis : tasks_in_set)  {
	    Api::tasks_t tasks = api.getTask(tis.name);
	    for(const auto& task : tasks)  {
	      all_tasks[task.name] = task;
	    }
	  }
	}
      }
      Api::tasks_t matching_tasks;
      for(const auto& t : all_tasks)
	matching_tasks.emplace_back(t.second);
      dump_objects(matching_tasks);
      return 1;
    });
}

//--------------------------------------------------------------------------
extern "C" int taskdb_classes_in_node(int argc, char** argv)   {
  return exception_wrapper(__func__, [argc, argv] {
      std::string match  = "*";
      RTL::CLI cli(argc, argv, [] (int ac, char** av)  {
	  std::cout << "  taskdb_classes_in_node -opt [-opt]                    \n";
	  std::cout << "    -match=<match-name>   Match name to query classes   \n";
	  get_connection(ac, av);
	  ::exit(0);
	});
      cli.getopt("match",  3, match);
      _print_header(cli, "taskdb_classes_in_node -match="+match);
      Api api(get_connection(argc,argv));
      auto result = api.classesInNode(match);
      dump_links(result);
      return 1;
    });
}

//--------------------------------------------------------------------------
extern "C" int taskdb_need_db_update(int argc, char** argv)   {
  return exception_wrapper(__func__, [argc, argv] {
      std::string match  = "*", node;
      long time_stamp = 1;
      int port = 0;
      RTL::CLI cli(argc, argv, [] (int ac, char** av)  {
	  std::cout << "  taskdb_need_db_update -opt [-opt]                    \n";
	  get_connection(ac, av);
	  ::exit(0);
	});
  
      cli.getopt("port",  3, port);
      cli.getopt("node",  3, node);
      _print_header(cli,"taskdb_need_db_update -node="+node+" -port="+std::to_string(port));
      if ( cli.getopt("now", 3) != 0 ) time_stamp = ::time(0);
      Api api(get_connection(argc,argv));
      auto result = api.needDbUpdate(node, port, time_stamp);
      std::cout << "need_db_update: " << result << std::endl;
      return 1;
    });
}

//--------------------------------------------------------------------------
extern "C" int taskdb_dump_cache(int argc, char** argv)   {
  return exception_wrapper(__func__, [argc, argv] {
      std::string output;
      long time_stamp = 1;
      RTL::CLI cli(argc, argv, [] (int ac, char** av)  {
	  std::cout << " taskdb_dump_cache  -opt [-opt]            \n";
	  std::cout << "   -output=<file-name>   Output file name  \n";
	  get_connection(ac, av);
	  ::exit(0);
	});
      cli.getopt("output", 4, output);
      if ( cli.getopt("now", 3) != 0 ) time_stamp = ::time(0);
      _print_header(cli, "taskdb_dump_cache -output="+output);
      Api api(get_connection(argc,argv));
      auto data = api.getCache(time_stamp);
      std::cout << "taskdb_dump_cache: " << std::endl;
      if ( output.empty() )   {
	data.push_back(0);
	std::cout << (const char*)&data.at(0) << std::endl;
	return 1;
      }
      std::ofstream out(output, std::ios::out|std::ios::binary);
      out.write((const char*)&data.at(0), data.size());
      std::cout << " .... wrote " << data.size()
		<< " bytes to output file: " << output << std::endl;
      return 1;
    });
}
