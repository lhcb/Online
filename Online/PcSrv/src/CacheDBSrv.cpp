//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : PcSrv
//
//  Author     : Markus Frank
//==========================================================================

/// Framework include files
#include <PcSrv/Task.h>
#include <CPP/Interactor.h>
#include <PcSrv/TaskDBApi.h>
#include <PcSrv/CacheDBApi.h>
#include <RPC/HttpXmlRpcHandler.h>
#include <HTTP/HttpClient.h>
#include <RTL/rtl.h>

/// C/C++ include files
#include <iostream>
#include <sstream>
#include <chrono>
#include <ctime>

namespace taskdb  {

  /**@class CacheDBSrv CacheDBSrv.h GaudiOnline/CacheDBSrv.h
   *
   * Readout monitor DIM server for a single node
   *
   * @author M.Frank
   */
  class CacheDBSrv : public CPP::Interactor {
  protected:
    /// Cluster container type definition
    typedef std::unique_ptr<std::thread>             thread_t;
    typedef std::unique_ptr<rpc::HttpServer>         httpsrv_t;
    typedef std::vector<TaskDBApi>                   servers_t;
    typedef std::chrono::duration<int>               duration_t;
    typedef std::chrono::system_clock::time_point    time_point_t;

    /// HTTP server thread
    thread_t                m_thread;
    /// HTTP server object
    httpsrv_t               m_http_server;
    /// Cluster container
    servers_t               m_servers;
    /// Process name
    std::string             m_name;
    std::string             m_node;
    std::string             m_mount  { "/TDBDATA/XMLRPC" };

    CacheDBApi              m_cache;
    time_point_t            m_connectTime;
    duration_t              m_update_interval;

    /// Port number with default value
    int                     m_port   { 3501 };
    /// Printout level
    long                    m_print = LIB_RTL_WARNING;
    int                     m_status_id = 0;
    int                     m_update_id = 0;
    int                     m_dbCheckUpdateTmo = 10;

    std::string  m_current_db_server;
    std::string  m_current_db_status;
    std::string  m_current_db_query;
    long         m_dis_dns_ID     =  0;
    int          m_dim_query_ID   =  0;
    int          m_dim_status_ID  =  0;
    int          m_dim_server_ID  =  0;


    struct {
      long numRequests        = 0;
      long numErrors          = 0;
      long numUpdates         = 0;
      long numChecks          = 0;
      long numThreads         = 0;
      long numServerHandled   = 0;
      long numServerUnhandled = 0;
      long numServerErrors    = 0;
    } m_counters;

    /// HTTP RPC server callback on errors
    void onServerError()            {   ++m_counters.numServerErrors;    }
    /// HTTP RPC server callback on handled requests
    void onServerHandled();
    //void onServerHandled(void* args[]);
    /// HTTP RPC server callback on unhandled requests
    void onServerUnhandled()        {   ++m_counters.numServerUnhandled; }

    /// Check the source server for pending database updates
    void checkUpdates();
    /// Load database from source server
    void loadUpdates();

    /// Test functions
    bool test()  const              {   return true;            }
    int  smallintTest()  const      {   return 123;             }
    int  intTest()  const           {   return 123456;          }
    double floatTest()  const       {   return 654321.0;        }
    std::string stringTest()  const {   return "Hello World!";  }
    struct tm timeTest()  const {
      struct tm tm;
      time_t tim = ::time(0);
      ::localtime_r(&tim, &tm);
      return tm;
    }

  public:
    /// Standard constructor with initialization
    CacheDBSrv(int argc, char** argv);
    /// Default destructor
    virtual ~CacheDBSrv();
    /// Access the name
    const std::string& name() const  {  return m_name; }
    /// Help printout in case of -h /? or wrong arguments
    static void help();
    /// DIM callback to force database reload from server
    static void cachedb_update_call(void* tag, void* buffer, int* size);
    /// Interactor override ....
    virtual void handle(const CPP::Event& ev) override;
    /// Setup RPC callbacks
    virtual void setupServer(rpc::HttpServer* server);
  };
}

/// Framework include files
#include <dim/dis.h>
#include <RTL/strdef.h>
#include <RTL/Logger.h>
#include <CPP/Event.h>
#include <CPP/IocSensor.h>
#include <CPP/TimeSensor.h>
#include <RPC/XMLRPC.h>
#include <HTTP/HttpRequest.h>
#include <Parsers/Primitives.h>

/// C/C++ include files
#include <thread>
#include <fstream>

namespace {
  constexpr static const char* NO_CURRENT_SERVER = "None";
  /// Feed data to DIS when updating data
  static void feed_std_string(void* tag, void** buff, int* size, int* /* first */) {
    static const char* data = "";
    std::string* s = *(std::string**)tag;
    if ( !s->empty() )  {
      *buff = (void*)s->data();
      *size = s->length()+1;
      return;
    }
    *buff = (void*)data;
    *size = 1;
  }
  struct DimLock  {
    DimLock()   { ::dim_lock();    }
    ~DimLock()  { ::dim_unlock();  }
  };
}

using namespace std;
using namespace std::chrono;
using namespace taskdb;

typedef void* pvoid;

#define CMD_CHECK_DB 1
#define CMD_LOAD_DB  2


namespace taskdb {
  size_t print_msg2(void* arg,int lvl,const char* fmt,va_list& args);
  void print_startup(const char* msg);
}

/// Standard constructor
CacheDBSrv::CacheDBSrv(int argc, char** argv)  {
  string addr = "0.0.0.0", dis_server, server;
  int    threads = 0, client_debug = 0;
  RTL::CLI cli(argc, argv, CacheDBSrv::help);
  m_node = RTL::nodeNameShort();
  m_name = RTL::str_upper(m_node)+"/"+RTL::processName();
  cli.getopt("print",  3, m_print);
  cli.getopt("utgid",  3, m_name);
  cli.getopt("node",   3, m_node);
  cli.getopt("address",3, addr);
  cli.getopt("threads",3, threads);
  cli.getopt("server", 3, server);
  cli.getopt("port",   3, m_port);
  cli.getopt("mount",  3, m_mount);
  cli.getopt("rpcdebug", 8, client_debug);

  bool   debug   = cli.getopt("debug",2) != 0;
  string dbg_opt = debug ? "[Debugging ON]" : "[Debugging OFF]";
  stringstream db_servers;

  if ( m_node == "localhost" )   {
    ::setenv("DIM_HOST_NODE", m_node.c_str(), 1);
    RTL::RTL_reset();
  }
  if ( m_name[0] != '/' ) m_name = '/'+m_name;
  dis_server = "/"+RTL::str_upper(m_node)+m_name;
  RTL::Logger::install_log(RTL::Logger::log_args(LIB_RTL_WARNING));
  RTL::Logger::print_startup(("-- HTTP CacheDB RPC service "+dbg_opt).c_str());
  m_counters.numThreads = threads;
  m_status_id  = ::dis_add_service((m_name+"/Counters").c_str(),"L",&m_counters,sizeof(m_counters),0,0);
  m_update_id  = ::dis_add_cmnd((m_name+"/ForceUpdate").c_str(),"I",cachedb_update_call,long(this));

  int  tmo = 10000;
  bool file_server = server.substr(0,7) == "file://";
  if ( file_server )   {
    std::vector<unsigned char> cache_data;
    std::string   fname = server.substr(7);
    std::ifstream in(fname, std::ios::in);
    while( !in.eof() )  {
      unsigned char c = 0;
      in.read((char*)&c, 1);
      if ( in.good() )
	cache_data.push_back(c);
    }
    m_cache.setSnapShotUnzipped(std::move(cache_data));
  }
  else   {
    if ( !server.empty() )   {
      m_servers.emplace_back(TaskDBApi(rpc::client<http::HttpClient>(server, this->m_mount, this->m_port, tmo)));
      m_servers.back()->setDebug(client_debug);
      db_servers << server << ':' << this->m_port << "  ";
    }
    if ( RTL::str_upper(RTL::nodeNameShort()) == "ECS03" )  {
      m_servers.emplace_back(TaskDBApi(rpc::client<http::HttpClient>("ecs03.lbdaq.cern.ch", this->m_mount, "3510", tmo)));
      m_servers.back()->setDebug(client_debug);
      db_servers << "ecs03:3510  ";
    }
    else   {
      m_servers.emplace_back(TaskDBApi(rpc::client<http::HttpClient>("ecs03.lbdaq.cern.ch", this->m_mount, "3501", tmo)));
      m_servers.back()->setDebug(client_debug);
      db_servers << "ecs03:3501  ";

      m_servers.emplace_back(TaskDBApi(rpc::client<http::HttpClient>("ecs03.lbdaq.cern.ch", this->m_mount, "3502", tmo)));
      m_servers.back()->setDebug(client_debug);
      db_servers << "ecs03:3502  ";
    }
    IocSensor::instance().send(this, CMD_LOAD_DB, 0UL);
    TimeSensor::instance().add(this, m_dbCheckUpdateTmo, pvoid(CMD_CHECK_DB));
  }
  {
    auto handler = std::make_unique<rpc::HttpXmlRpcHandler>();
    handler->debug = debug ? 1 : 0;
    try  {
      m_http_server.reset(new rpc::HttpServer(std::move(handler),addr,this->m_port,rpc::HttpServer::SERVER,this->m_mount));
      m_http_server->setDebug(debug);
      m_http_server->onError(Online::Callback(this).make(&CacheDBSrv::onServerError));
      m_http_server->onHandled(Online::Callback(this).make(&CacheDBSrv::onServerHandled));
      m_http_server->onUnhandled(Online::Callback(this).make(&CacheDBSrv::onServerUnhandled));
      setupServer(m_http_server.get());
    }
    catch(const std::exception& e)   {
      std::string err = e.what();
      if ( err.find("Address already in use") != std::string::npos )    {
	::lib_rtl_output(LIB_RTL_INFO,"CacheDBSrv",
			 "+++++ HTTP server startup failed: %s", err.c_str());
	::lib_rtl_output(LIB_RTL_INFO,"CacheDBSrv",
			 " exception type: %s", Online::typeName(typeid(*(&e))).c_str());
	::_exit(EADDRINUSE);
      }
      ::lib_rtl_output(LIB_RTL_INFO,"CacheDBSrv",
		       "+++++ HTTP server startup failed: %s", err.c_str());
      ::lib_rtl_output(LIB_RTL_INFO,"CacheDBSrv",
		       " exception type: %s", Online::typeName(typeid(*(&e))).c_str());
      ::_exit(errno);
    }
    m_thread.reset(new thread([this,threads,&db_servers] {
	  auto dbs = db_servers.str();
	  ::lib_rtl_output(LIB_RTL_ALWAYS,
			   "Running HTTP xmlrpc service on http://%s:%d%s with %d additional threads. %s %s",
			   this->m_node.c_str(), this->m_port, this->m_mount.c_str(), threads, 
			   dbs.empty() ? "" : "DB servers:", dbs.c_str());
	  this->m_http_server->run(threads);
	  ::lib_rtl_output(LIB_RTL_DEBUG,"HTTP xmlrpc service http://%s:%d%s shutdown requested.",
			   this->m_node.c_str(), this->m_port, this->m_mount.c_str());
	  this->m_http_server.reset();
	  IocSensor::instance().exit();
	}));
  }
  
  std::string node = RTL::str_upper(RTL::nodeNameShort());
  m_current_db_server = NO_CURRENT_SERVER;
  m_dis_dns_ID     = ::dis_add_dns(node.c_str(), ::dim_get_dns_port());
  m_dim_server_ID  = ::dis_add_service_dns(m_dis_dns_ID,
					   (dis_server+"/server").c_str(),
					   "C",0,0,feed_std_string,(long)&m_current_db_server);
  m_dim_status_ID  = ::dis_add_service_dns(m_dis_dns_ID,
					   (dis_server+"/status").c_str(),
					   "C",0,0,feed_std_string,(long)&m_current_db_status);
  m_dim_query_ID   = ::dis_add_service_dns(m_dis_dns_ID,
					   (dis_server+"/query").c_str(),
					   "C",0,0,feed_std_string,(long)&m_current_db_query);
  ::dis_start_serving(dis_server.c_str());
}

/// Default destructor
CacheDBSrv::~CacheDBSrv() {
  if ( m_dim_server_ID ) ::dis_remove_service(m_dim_server_ID);
  if ( m_dim_status_ID ) ::dis_remove_service(m_dim_status_ID);
  if ( m_dim_query_ID  ) ::dis_remove_service(m_dim_query_ID);
  this->m_thread->join();
  this->m_thread.reset();
}

/// HTTP RPC server callback on handled requests
void CacheDBSrv::onServerHandled()          {
  ++m_counters.numServerHandled;
}

#if 0
void CacheDBSrv::onServerHandled(void* args[])          {
  if ( args && args[0] )   {
    const http::Request* req = (const http::Request*)args[0];
    try  {
      ::lib_rtl_output(LIB_RTL_INFO,"CacheDBSrv",
		       "+++++ Handled HTTP Request: %s uri:%s from: %s:%d [%ld bytes]",
		       req->method.c_str(), req->uri.c_str(), 
		       req->remote_host_name().c_str(), req->port(), 
		       req->content.size());
    }
    catch(const std::exception& e)  {
      ::lib_rtl_output(LIB_RTL_INFO,"CacheDBSrv",
		       "+++++ Handled HTTP Request: %s uri:%s endpoint: %s [%ld bytes]",
		       req->method.c_str(), req->uri.c_str(), e.what(),
		       req->content.size());
    }
  }
}
#endif

/// DIM callback to force database reload from server
void CacheDBSrv::cachedb_update_call(void* tag, void* buffer, int* size)   {
  if ( tag && buffer && size )   {
    CacheDBSrv* server = *(CacheDBSrv**)tag;
    server->m_connectTime = system_clock::from_time_t(0);
    IocSensor::instance().send(server, CMD_LOAD_DB, 0UL);
  }
}

/// Help printout in case of -h /? or wrong arguments
void CacheDBSrv::help() {
  ::lib_rtl_output(LIB_RTL_ALWAYS,"romon_domainrpc -opt [-opt]                          \n"
                   "     -print=<integer>   Printout value and verbosity.               \n"
		   "     -rpcdbg=<integer>  Set debug level for RPC client (0...3)      \n"
                   "     -utgid=<name>      Server name for DIM publishing/commands.    \n"
		   "                        Default: env(UTGID)                         \n"
		   "     -node=<name>       Node name to register with source server.   \n"
		   "                        Default: host name                          \n"
		   "     -address=<name>    Server listening address.                   \n"
		   "                        Default: 0.0.0.0.                           \n"
		   "     -threads=<number>  Number of client serving threads.           \n"
		   "                        Default: single threaded mode.              \n"
		   "     -server=<name>     Source server: Host name.                   \n"
		   "                        Defaults: ecs03.lbdaq.cern.ch:3501 +        \n"
		   "                        slavetasks.service.consul.lhcb.cern.ch:port \n"
		   "     -port=<number>     Source server: Port.                        \n"
		   "                        Default: 3501                               \n"
		   "     -mount=<name>      Source server: URI mount point.             \n"
		   "                        Default: /TDBDATA/XMLRPC                    \n");
}

/// Setup RPC callbacks
void CacheDBSrv::setupServer(rpc::HttpServer* server)   {
  server->define("test",            xmlrpc::Call(this).make(&CacheDBSrv::test));
  server->define("boolTest",        xmlrpc::Call(this).make(&CacheDBSrv::test));
  server->define("smallintTest",    xmlrpc::Call(this).make(&CacheDBSrv::smallintTest));
  server->define("intTest",         xmlrpc::Call(this).make(&CacheDBSrv::intTest));
  server->define("floatTest",       xmlrpc::Call(this).make(&CacheDBSrv::floatTest));
  server->define("stringTest",      xmlrpc::Call(this).make(&CacheDBSrv::stringTest));
  server->define("timeTest",        xmlrpc::Call(this).make(&CacheDBSrv::timeTest));
  server->define("getCache",        xmlrpc::Call(&m_cache).make(&CacheDBApi::getCache));
  server->define("needDbUpdate",    xmlrpc::Call(&m_cache).make(&CacheDBApi::needDbUpdate));
  //server->define("addTask",         xmlrpc::Call(&m_cache).make(&CacheDBApi::addTask));
  server->define("getTask",         xmlrpc::Call(&m_cache).make(&CacheDBApi::getTask));
  server->define("deleteTask",      xmlrpc::Call(&m_cache).make(&CacheDBApi::deleteTask));
  server->define("addSet",          xmlrpc::Call(&m_cache).make(&CacheDBApi::addSet));
  server->define("deleteSet",       xmlrpc::Call(&m_cache).make(&CacheDBApi::deleteSet));
  server->define("getSet",          xmlrpc::Call(&m_cache).make(&CacheDBApi::getSet));
  server->define("tasksInSet",      xmlrpc::Call(&m_cache).make(&CacheDBApi::tasksInSet));
  server->define("addClass",        xmlrpc::Call(&m_cache).make(&CacheDBApi::addClass));
  server->define("deleteClass",     xmlrpc::Call(&m_cache).make(&CacheDBApi::deleteClass));
  server->define("getClass",        xmlrpc::Call(&m_cache).make(&CacheDBApi::getClass));
  server->define("taskSetsInClass", xmlrpc::Call(&m_cache).make(&CacheDBApi::taskSetsInClass));
  server->define("addNode",         xmlrpc::Call(&m_cache).make(&CacheDBApi::addNode));
  server->define("deleteNode",      xmlrpc::Call(&m_cache).make(&CacheDBApi::deleteNode));
  server->define("getNode",         xmlrpc::Call(&m_cache).make(&CacheDBApi::getNode));
  server->define("classesInNode",   xmlrpc::Call(&m_cache).make(&CacheDBApi::classesInNode));
  server->define("getTasksByNode",  xmlrpc::Call(&m_cache).make(&CacheDBApi::getTasksByNode));
  server->start(true);
}

/// Check the source server for pending database updates
void CacheDBSrv::checkUpdates()    {
  std::string failed;
  time_t stamp  = system_clock::to_time_t(m_connectTime);
  lib_rtl_output(LIB_RTL_DEBUG,"Checking for updates.....");
  m_counters.numRequests = m_cache.numberOfCalls();
  m_counters.numErrors   = m_cache.numberOfErrors();
  ++m_counters.numChecks;
  for( const auto& s : m_servers )    {
    try  {
      auto result   = s.needDbUpdate(m_node, 1234, stamp);
      std::stringstream info;
      {
	DimLock dimLock;
	info << ::lib_rtl_timestr() << ": " << s->name() << ": Checked for updates: "
	     << (result ? "No updates pending" : "Need to update tasks from DB")
	     << " Time stamp: " << long(stamp);
	m_current_db_server = s->name();
	m_current_db_query = info.str();
      }
      ::lib_rtl_output(LIB_RTL_INFO, "+++ %s", info.str().c_str());
      if ( result )   {
	IocSensor::instance().send(this, CMD_LOAD_DB);
	/// Counter update done after loading data
      }
      else  {
	::dis_update_service(m_status_id);
      }
      ::dis_update_service(m_dim_server_ID);
      ::dis_update_service(m_dim_query_ID);
      TimeSensor::instance().add(this, m_dbCheckUpdateTmo, pvoid(CMD_CHECK_DB));
      return;
    }
    catch(const exception& e)    {
      failed += s->name();
      failed += " ";
      failed += e.what();
      failed += " ";
      ::lib_rtl_output(LIB_RTL_DEBUG,
		       "+++ Failed to check database server %s for updates: %s",
		       s->name().c_str(), e.what());
      ::lib_rtl_sleep(1000);
    }
    catch(...)    {
      failed += s->name();
      failed += " ";
      ::lib_rtl_output(LIB_RTL_DEBUG,
		       "+++ Failed to check database server %s for updates.",
		       s->name().c_str());
      ::lib_rtl_sleep(1000);
    }
  }
  TimeSensor::instance().add(this, m_dbCheckUpdateTmo, pvoid(CMD_CHECK_DB));
  ::lib_rtl_output(LIB_RTL_WARNING,
		   "+++ Failed to check database server(s) for update. Will retry later. %s", failed.c_str());  
  {
    DimLock dimLock;
    m_current_db_server = NO_CURRENT_SERVER;
    m_current_db_query = "Update query FAILED -- no server available";
    ::dis_update_service(m_dim_server_ID);
    ::dis_update_service(m_dim_query_ID);
  }
  ::lib_rtl_sleep(5000);
}

/// Load database from source server
void CacheDBSrv::loadUpdates()    {
  std::string failed;
  time_t stamp  = system_clock::to_time_t(m_connectTime);
  for( const auto& s : m_servers )    {
    try  {
      auto data = s.getCacheRaw(stamp);
      std::stringstream info;
      m_connectTime = system_clock::now();
      m_cache.setSnapShot(std::move(data));
      info << ::lib_rtl_timestr() << ": " << s->name()
	   << ": Successfully loaded updated task DB.";
      ++m_counters.numUpdates;
      ::lib_rtl_output(LIB_RTL_DEBUG,"+++ %s",info.str().c_str());
      {
	DimLock dimLock;
	m_current_db_server = s->name();
	m_current_db_status = info.str();
      }
      ::dis_update_service(m_dim_server_ID);
      ::dis_update_service(m_dim_status_ID);
      ::dis_update_service(m_status_id);
      return;
    }
    catch(const exception& e)    {
      failed += s->name();
      failed += " ";
      failed += e.what();
      failed += " ";
      ::lib_rtl_output(LIB_RTL_DEBUG,
		       "+++ Failed to load database server %s updates: %s",
		       s->name().c_str(), e.what());
      ::lib_rtl_sleep(1000);
    }
    catch(...)    {
      failed += s->name();
      failed += " ";
      ::lib_rtl_output(LIB_RTL_DEBUG,
		       "+++ Failed to load database server %s updates.",
		       s->name().c_str());
      ::lib_rtl_sleep(1000);
    }
  }
  std::stringstream info;
  info << ::lib_rtl_timestr() << ": Failed to load database server(s) updates. "
       << "Will retry later. " << failed;

  ::lib_rtl_output(LIB_RTL_WARNING, "+++ %s", info.str().c_str());
  m_current_db_status = info.str();
  m_current_db_server = NO_CURRENT_SERVER;
  ::dis_update_service(m_dim_server_ID);
  ::dis_update_service(m_dim_status_ID);
  ::lib_rtl_sleep(5000);
}

/// Interactor override ....
void CacheDBSrv::handle(const CPP::Event& ev) {
  switch(ev.eventtype) {
  case TimeEvent:
    IocSensor::instance().send(this, int(long(ev.timer_data)&0xFFFFFFFF), 0UL);
    return;
  case IocEvent:
    switch(ev.type) {
    case CMD_CHECK_DB:
      checkUpdates();
      return;
    case CMD_LOAD_DB:
      loadUpdates();
      return;

    default:
      break;
    }
    break;
  default:
    break;
  }
}

/// Main entry point to start the application
extern "C" int taskdb_cache_server(int argc, char** argv) {
  CacheDBSrv src(argc, argv);
  IocSensor::instance().run();
  return 1;
}

/// Main entry point to start the application
extern "C" int run_pcsrv_cachedb(int argc, char** argv) {
  return taskdb_cache_server(argc, argv);
}
