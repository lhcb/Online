//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include "PcSrv/Task.h"
#include "RPC/XMLRPC.h"
#include <iostream>
#include <ctime>

using namespace std;

ostream& operator<< (ostream& os, const taskdb::Task& t)   {
  os << "Task: name:        " << t.name << endl;
  os << "      utgid:       " << t.utgid << endl;
  os << "      command:     " << t.command << endl;
  os << "      params:      " << t.command_parameters << endl;
  os << "      submit:      " << t.submit_parameters << endl;
  os << "      restart:     " << t.restart_parameters << endl;
  os << "      description: " << t.description << endl;
  return os;
}

ostream& operator<< (ostream& os, const taskdb::TaskSet& o)   {
  os << "TaskSet: name:     " << o.name << endl;
  os << "      description: " << o.description << endl;
  return os;
}

ostream& operator<< (ostream& os, const taskdb::NodeClass& o)   {
  os << "Node class: name:  " << o.name << endl;
  os << "      description: " << o.description << endl;
  return os;
}

ostream& operator<< (ostream& os, const taskdb::NodeType& o)   {
  os << "Node type: name:   " << o.name << endl;
  os << "      priority:    " << o.priority << endl;
  os << "      description: " << o.description << endl;
  return os;
}

ostream& operator<< (ostream& os, const vector<taskdb::Task>& tasks)   {
  for( const auto& t : tasks ) os << t;
  os   << "Got a total of " << tasks.size() << " tasks." << endl;
  return os;
}

ostream& operator<< (ostream& os, const vector<taskdb::TaskSet>& obj)   {
  for( const auto& o : obj ) os << o;
  os   << "Got a total of " << obj.size() << " task sets." << endl;
  return os;
}

ostream& operator<< (ostream& os, const vector<taskdb::NodeClass>& obj)   {
  for( const auto& o : obj ) os << o;
  os   << "Got a total of " << obj.size() << " node classes." << endl;
  return os;
}

ostream& operator<< (ostream& os, const vector<taskdb::NodeType>& obj)   {
  for( const auto& o : obj ) os << o;
  os   << "Got a total of " << obj.size() << " node types." << endl;
  return os;
}

template <typename T> static ostream& dump_links(ostream& os, const vector<T>& obj)   {
  for( const auto& o : obj )    {
    os << "Link name:     " << o.name << endl;
  }
  os   << "Got a total of " << obj.size() << " links." << endl;
  return os;
}

ostream& operator<< (ostream& os, const vector<taskdb::TasksInSet>& obj)   {
  return dump_links(os, obj);
}

ostream& operator<< (ostream& os, const vector<taskdb::SetsInClass>& obj)   {
  return dump_links(os, obj);
}

ostream& operator<< (ostream& os, const vector<taskdb::ClassesInNode>& obj)   {
  return dump_links(os, obj);
}

/// operator less to support maps/sets
bool taskdb::TaskSet::operator<(const TaskSet& copy) const  {
  if ( name < copy.name ) return true;
  if ( description < copy.description ) return true;
  return false;
}

// Example how to implement data marshalling for an arbitrary class
namespace xmlrpc {
  template <> taskdb::TaskSet XmlCoder::to() const {
    taskdb::TaskSet val;
    xml_h struc = element.child(_rpcU(struct));
    for(xml_coll_t c(struc,_rpcU(member)); c; ++c)   {
      string n = c.child(_rpcU(name)).text();
      if      ( n == "task_set" )
	val.name    = Arg::get_string(c);
      else if ( n == "description"    )
	val.description = Arg::get_string(c);
    }
    return val;
  }

  template <> const XmlCoder& XmlCoder::from(const taskdb::TaskSet& b) const {
    Structure(element).add("task_set",    b.name);
    Structure(element).add("description", b.description);
    return *this;
  }
}

XMLRPC_IMPLEMENT_OBJECT_MARSCHALLING(struct,taskdb::TaskSet)
XMLRPC_IMPLEMENT_CONTAINER_CONVERSION(vector,taskdb::TaskSet)
XMLRPC_IMPLEMENT_OBJECT_MARSCHALLING(array,vector<taskdb::TaskSet>)
XMLRPC_IMPLEMENT_CONTAINER_CONVERSION(list,taskdb::TaskSet)
XMLRPC_IMPLEMENT_OBJECT_MARSCHALLING(array,list<taskdb::TaskSet>)

/// operator less to support maps/sets
bool taskdb::NodeClass::operator<(const NodeClass& copy) const  {
  if ( name < copy.name ) return true;
  if ( description < copy.description ) return true;
  return false;
}

// Example how to implement data marshalling for an arbitrary class
namespace xmlrpc {
  template <> taskdb::NodeClass XmlCoder::to() const {
    taskdb::NodeClass val;
    xml_h struc = element.child(_rpcU(struct));
    for(xml_coll_t c(struc,_rpcU(member)); c; ++c)   {
      string n = c.child(_rpcU(name)).text();
      if      ( n == "node_class"    )
	val.name    = Arg::get_string(c);
      else if ( n == "description"    )
	val.description = Arg::get_string(c);
    }
    return val;
  }

  template <> const XmlCoder& XmlCoder::from(const taskdb::NodeClass& b) const {
    Structure(element).add("node_class",  b.name);
    Structure(element).add("description", b.description);
    return *this;
  }
}

XMLRPC_IMPLEMENT_OBJECT_MARSCHALLING(struct,taskdb::NodeClass)
XMLRPC_IMPLEMENT_CONTAINER_CONVERSION(vector,taskdb::NodeClass)
XMLRPC_IMPLEMENT_OBJECT_MARSCHALLING(array,vector<taskdb::NodeClass>)
XMLRPC_IMPLEMENT_CONTAINER_CONVERSION(list,taskdb::NodeClass)
XMLRPC_IMPLEMENT_OBJECT_MARSCHALLING(array,list<taskdb::NodeClass>)

/// operator less to support maps/sets
bool taskdb::NodeType::operator<(const NodeType& copy) const  {
  if ( name < copy.name ) return true;
  if ( description < copy.description ) return true;
  return false;
}

// Example how to implement data marshalling for an arbitrary class
namespace xmlrpc {
  template <> taskdb::NodeType XmlCoder::to() const {
    taskdb::NodeType val;
    xml_h struc = element.child(_rpcU(struct));
    for(xml_coll_t c(struc,_rpcU(member)); c; ++c)   {
      string n = c.child(_rpcU(name)).text();
      if      ( n == "regex" )
	val.name    = Arg::get_string(c);
      else if ( n == "priority" )
	val.priority = Arg::get_string(c);
      else if ( n == "description" )
	val.description = Arg::get_string(c);
    }
    return val;
  }

  template <> const XmlCoder& XmlCoder::from(const taskdb::NodeType& b) const {
    Structure(element).add("regex",       b.name);
    Structure(element).add("priority",    b.priority);
    Structure(element).add("description", b.description);
    return *this;
  }
}

XMLRPC_IMPLEMENT_OBJECT_MARSCHALLING(struct,taskdb::NodeType)
XMLRPC_IMPLEMENT_CONTAINER_CONVERSION(vector,taskdb::NodeType)
XMLRPC_IMPLEMENT_OBJECT_MARSCHALLING(array,vector<taskdb::NodeType>)
XMLRPC_IMPLEMENT_CONTAINER_CONVERSION(list,taskdb::NodeType)
XMLRPC_IMPLEMENT_OBJECT_MARSCHALLING(array,list<taskdb::NodeType>)

/// operator less to support maps/sets
bool taskdb::TasksInSet::operator<(const TasksInSet& copy) const  {
  if ( name < copy.name ) return true;
  return false;
}

// Example how to implement data marshalling for an arbitrary class
namespace xmlrpc {
  template <> taskdb::TasksInSet XmlCoder::to() const {
    taskdb::TasksInSet val;
    xml_h struc = element.child(_rpcU(struct));
    for(xml_coll_t c(struc,_rpcU(member)); c; ++c)   {
      string n = c.child(_rpcU(name)).text();
      if      ( n == "task" )
	val.name = Arg::get_string(c);
      else if ( n == "task_set"    )
	val.name = Arg::get_string(c);
      else if ( n == "node_class"    )
	val.name = Arg::get_string(c);
    }
    return val;
  }

  template <> const XmlCoder& XmlCoder::from(const taskdb::TasksInSet& b) const {
    Structure(element).add("task", b.name);
    return *this;
  }
}

XMLRPC_IMPLEMENT_OBJECT_MARSCHALLING(struct,taskdb::TasksInSet)
XMLRPC_IMPLEMENT_CONTAINER_CONVERSION(vector,taskdb::TasksInSet)
XMLRPC_IMPLEMENT_OBJECT_MARSCHALLING(array,vector<taskdb::TasksInSet>)
XMLRPC_IMPLEMENT_CONTAINER_CONVERSION(list,taskdb::TasksInSet)
XMLRPC_IMPLEMENT_OBJECT_MARSCHALLING(array,list<taskdb::TasksInSet>)

/// operator less to support maps/sets
bool taskdb::SetsInClass::operator<(const SetsInClass& copy) const  {
  if ( name < copy.name ) return true;
  return false;
}

// Example how to implement data marshalling for an arbitrary class
namespace xmlrpc {
  template <> taskdb::SetsInClass XmlCoder::to() const {
    taskdb::SetsInClass val;
    xml_h struc = element.child(_rpcU(struct));
    for(xml_coll_t c(struc,_rpcU(member)); c; ++c)   {
      string n = c.child(_rpcU(name)).text();
      if ( n == "task_set" ) val.name = Arg::get_string(c);
    }
    return val;
  }
  template <> const XmlCoder& XmlCoder::from(const taskdb::SetsInClass& b) const {
    Structure(element).add("task_set", b.name);
    return *this;
  }
}

XMLRPC_IMPLEMENT_OBJECT_MARSCHALLING(struct,taskdb::SetsInClass)
XMLRPC_IMPLEMENT_CONTAINER_CONVERSION(vector,taskdb::SetsInClass)
XMLRPC_IMPLEMENT_OBJECT_MARSCHALLING(array,vector<taskdb::SetsInClass>)
XMLRPC_IMPLEMENT_CONTAINER_CONVERSION(list,taskdb::SetsInClass)
XMLRPC_IMPLEMENT_OBJECT_MARSCHALLING(array,list<taskdb::SetsInClass>)

/// operator less to support maps/sets
bool taskdb::ClassesInNode::operator<(const ClassesInNode& copy) const  {
  if ( name < copy.name ) return true;
  return false;
}

// Example how to implement data marshalling for an arbitrary class
namespace xmlrpc {
  template <> taskdb::ClassesInNode XmlCoder::to() const {
    taskdb::ClassesInNode val;
    xml_h struc = element.child(_rpcU(struct));
    for(xml_coll_t c(struc,_rpcU(member)); c; ++c)   {
      string n = c.child(_rpcU(name)).text();
      if ( n == "node_class" ) val.name = Arg::get_string(c);
    }
    return val;
  }
  template <> const XmlCoder& XmlCoder::from(const taskdb::ClassesInNode& b) const {
    Structure(element).add("node_class", b.name);
    return *this;
  }
}

XMLRPC_IMPLEMENT_OBJECT_MARSCHALLING(struct,taskdb::ClassesInNode)
XMLRPC_IMPLEMENT_CONTAINER_CONVERSION(vector,taskdb::ClassesInNode)
XMLRPC_IMPLEMENT_OBJECT_MARSCHALLING(array,vector<taskdb::ClassesInNode>)
XMLRPC_IMPLEMENT_CONTAINER_CONVERSION(list,taskdb::ClassesInNode)
XMLRPC_IMPLEMENT_OBJECT_MARSCHALLING(array,list<taskdb::ClassesInNode>)

/// operator less to support maps/sets
bool taskdb::Task::operator<(const Task& copy) const  {
  if ( name    < copy.name    ) return true;
  if ( utgid   < copy.utgid   ) return true;
  if ( command < copy.command ) return true;
  if ( command_parameters < copy.command_parameters ) return true;
  if ( submit_parameters  < copy.submit_parameters    ) return true;
  if ( restart_parameters < copy.restart_parameters    ) return true;
  if ( description        < copy.description        ) return true;
  return false;
}

namespace xmlrpc {
  template <> taskdb::Task XmlCoder::to() const {
    taskdb::Task val;
    xml_h struc = element.child(_rpcU(struct));
    for(xml_coll_t c(struc,_rpcU(member)); c; ++c)   {
      string n = c.child(_rpcU(name)).text();
      if      ( n == "task"    )
	val.name    = Arg::get_string(c);
      else if ( n == "utgid"   )
	val.utgid   = Arg::get_string(c);
      else if ( n == "command" )
	val.command = Arg::get_string(c);
      else if ( n == "command_parameters" ) 
	val.command_parameters = Arg::get_string(c);
      else if ( n == "submit_parameters"    )
	val.submit_parameters = Arg::get_string(c);
      else if ( n == "restart_parameters"    )
	val.restart_parameters = Arg::get_string(c);
      else if ( n == "description"    )
	val.description = Arg::get_string(c);
    }
    return val;
  }

  template <> const XmlCoder& XmlCoder::from(const taskdb::Task& b) const {
    Structure(element).add("task",        b.name);
    Structure(element).add("utgid",       b.utgid);
    Structure(element).add("command",     b.command);
    Structure(element).add("command_parameters",  b.command_parameters);
    Structure(element).add("submit_parameters",   b.submit_parameters);
    Structure(element).add("restart_parameters",  b.restart_parameters);
    Structure(element).add("description", b.description);
    return *this;
  }
}

XMLRPC_IMPLEMENT_OBJECT_MARSCHALLING(struct,taskdb::Task)
XMLRPC_IMPLEMENT_CONTAINER_CONVERSION(vector,taskdb::Task)
XMLRPC_IMPLEMENT_OBJECT_MARSCHALLING(array,vector<taskdb::Task>)
XMLRPC_IMPLEMENT_CONTAINER_CONVERSION(list,taskdb::Task)
XMLRPC_IMPLEMENT_OBJECT_MARSCHALLING(array,list<taskdb::Task>)

ostream& operator << (ostream& os, const taskdb::Snapshot& shot)   {
  int cnt = 0;
  time_t tm = shot.timeStamp;
  os << "++++ TaskDB Snapshot from: ";
  os << ::ctime(&tm);
  for(const auto& o : shot.tasks)
    os << "Task[" << cnt++ << "] " << o.first << " " << o.second.first.name
	 << " dsc:" << o.second.first.description
	 << endl;
  cnt = 0;
  for(const auto& o : shot.sets)   {
    os << "Set[" << cnt++ << "]: " << o.first << " " << o.second.first.name 
	 << " #Tasks: " << o.second.second.size() << endl;
    for(const auto& t : o.second.second)
      os << "\t --> " << t.name << endl;
  }
  for(const auto& o : shot.classes)   {
    os << "Cls: " << o.first << " " << o.second.first.name 
	 << " #Sets: " << o.second.second.size() << endl;
    for(const auto& s : o.second.second)
      os << "\t --> " << s.name << endl;
  }
  for(const auto& o : shot.types)   {
    os << "Typ: " << o.first << " " << o.second.first.name 
	 << " #Cls: " << o.second.second.size() << endl;
    for(const auto& c : o.second.second)
      os << "\t --> " << c.name << endl;
  }
  return os;
}

// Example how to implement data marshalling for an arbitrary class
namespace xmlrpc {

  template <> taskdb::Snapshot::Tsk XmlCoder::to() const {
    taskdb::Snapshot::Tsk val;
    for(xml_coll_t c(element,_rpcU(member)); c; ++c)   {
      string n = c.child(_rpcU(name)).text();
      if ( n == "task" )
	val.first = XmlCoder(c.child(_rpcU(value))).to<taskdb::Task>();
    }
    return val;
  }
  template <> const XmlCoder& XmlCoder::from(const taskdb::Snapshot::Tsk& b) const {
    Structure s(element), task = Structure();
    s.add("task", task);
    XmlCoder(task.root).from(b.first);
    return *this;
  }

  template <> taskdb::Snapshot::Set XmlCoder::to() const {
    taskdb::Snapshot::Set val;
    for(xml_coll_t c(element, _rpcU(member)); c; ++c)   {
      string n = c.child(_rpcU(name)).text();
      xml_h  h = c.child(_rpcU(value));
      if      ( n == "taskset" ) val.first  = XmlCoder(h).to<taskdb::TaskSet>();
      else if ( n == "tasks"   ) val.second = XmlCoder(h).to<vector<taskdb::TasksInSet> >();
    }
    return val;
  }
  template <> const XmlCoder& XmlCoder::from(const taskdb::Snapshot::Set& b) const {
    Structure s(element), taskset = Structure(), tasks = Structure();
    s.add("taskset",   taskset);
    s.add("tasks",     tasks);
    XmlCoder(taskset.root).from(b.first);
    XmlCoder(tasks.root).from(b.second);
    return *this;
  }

  template <> taskdb::Snapshot::Class XmlCoder::to() const {
    taskdb::Snapshot::Class val;
    for(xml_coll_t c(element, _rpcU(member)); c; ++c)   {
      string n = c.child(_rpcU(name)).text();
      xml_h  h = c.child(_rpcU(value));
      if      ( n == "cls"  ) val.first  = XmlCoder(h).to<taskdb::NodeClass>();
      else if ( n == "sets" ) val.second = XmlCoder(h).to<vector<taskdb::SetsInClass> >();
    }
    return val;
  }
  template <> const XmlCoder& XmlCoder::from(const taskdb::Snapshot::Class& b) const {
    Structure s(element), cls = Structure();
    Array sets = Array();
    s.add("cls",   cls);
    s.add("sets", sets);
    XmlCoder(cls.root).from(b.first);
    XmlCoder(sets.root).from(b.second);
    return *this;
  }
  template <> taskdb::Snapshot::Type XmlCoder::to() const {
    taskdb::Snapshot::Type val;
    for(xml_coll_t c(element,_rpcU(member)); c; ++c)   {
      string n = c.child(_rpcU(name)).text();
      xml_h  h = c.child(_rpcU(value));
      if      ( n == "type"    ) val.first  = XmlCoder(h).to<taskdb::NodeType>();
      else if ( n == "classes" ) val.second = XmlCoder(h).to<vector<taskdb::ClassesInNode> >();
    }
    return val;
  }
  template <> const XmlCoder& XmlCoder::from(const taskdb::Snapshot::Type& b) const {
    Structure s(element), type = Structure();
    Array classes = Array();
    s.add("type", type);
    s.add("classes", classes);
    XmlCoder(type.root).from(b.first);
    XmlCoder(classes.root).from(b.second);
    return *this;
  }
}
XMLRPC_IMPLEMENT_STRINGMAP_CONVERSION(std::map,std::string,taskdb::Snapshot::Tsk)
XMLRPC_IMPLEMENT_STRINGMAP_CONVERSION(std::map,std::string,taskdb::Snapshot::Set)
XMLRPC_IMPLEMENT_STRINGMAP_CONVERSION(std::map,std::string,taskdb::Snapshot::Type)
XMLRPC_IMPLEMENT_STRINGMAP_CONVERSION(std::map,std::string,taskdb::Snapshot::Class)

namespace xmlrpc {
  template <> taskdb::Snapshot XmlCoder::to() const {
    using namespace taskdb;
    Snapshot val;
    xml_h struc = element.child(_rpcU(struct));
    for(xml_coll_t c(struc,_rpcU(member)); c; ++c)   {
      string n = c.child(_rpcU(name)).text();
      //xml_h h = c.child(_rpcU(value));
      if      ( n == "timeStamp" ) val.timeStamp = Arg::get_int(c);
      else if ( n == "tasks"     ) val.tasks     = XmlCoder(c).to<map<string, Snapshot::Tsk> >();
      else if ( n == "sets"      ) val.sets      = XmlCoder(c).to<map<string, Snapshot::Set> >();
      else if ( n == "classes"   ) val.classes   = XmlCoder(c).to<map<string, Snapshot::Class> >();
      else if ( n == "types"     ) val.types     = XmlCoder(c).to<map<string, Snapshot::Type> >();
    }
    return val;
  }

  template <> const XmlCoder& XmlCoder::from(const taskdb::Snapshot& snapshot) const {
    Structure s(element),
      tasks = Structure(), sets = Structure(),
      types = Structure(), classes = Structure();
    s.add("timeStamp", snapshot.timeStamp);
    s.add("tasks",     tasks);
    s.add("sets",      sets);
    s.add("classes",   classes);
    s.add("types",     types);
    XmlCoder(tasks.root)  .from(snapshot.tasks);
    XmlCoder(sets.root)   .from(snapshot.sets);
    XmlCoder(classes.root).from(snapshot.classes);
    XmlCoder(types.root)  .from(snapshot.types);
    return *this;
  }
}

XMLRPC_IMPLEMENT_OBJECT_MARSCHALLING(struct,taskdb::Snapshot)
