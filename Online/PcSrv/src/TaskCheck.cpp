//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : PcSrv
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include <PcSrv/TaskCheck.h>
#include <CPP/TimeSensor.h>
#include <CPP/IocSensor.h>
#include <HTTP/HttpClient.h>
#include <RPC/RpcClientHandle.h>
#include <RPC/XMLRPC.h>
#include <RTL/Logger.h>
#include <RTL/strdef.h>
#include <RTL/rtl.h>
#include <dim/dic.h>
#include <dim/dis.h>

// C/C++ include files
#include <ctime>
#include <cstring>
#include <sstream>
#include <iostream>

using namespace xmlrpc;
using namespace taskdb;
using std::chrono::duration_cast;
using std::chrono::system_clock;


namespace taskdb {

  size_t print_msg2(void* arg,int lvl,const char* fmt,va_list& args);
  void print_startup(const char* msg);
}
namespace {
  constexpr static const char* NO_CURRENT_SERVER = "None";
  /// Feed data to DIS when updating data
  static void feed_std_string(void* tag, void** buff, int* size, int* /* first */) {
    static const char* data = "";
    std::string* s = *(std::string**)tag;
    if ( !s->empty() )  {
      *buff = (void*)s->data();
      *size = s->length()+1;
      return;
    }
    *buff = (void*)data;
    *size = 1;
  }
  struct DimLock  {
    DimLock()   { ::dim_lock();    }
    ~DimLock()  { ::dim_unlock();  }
  };
}

enum Commands {
  TASKCHECK_LOAD_DB = 100,
  TASKCHECK_LOAD_TM = 101,
  TASKCHECK_CHECK_DB = 200,
  TASKCHECK_CHECK_TM = 300,
  LAST_CMD
};
typedef void* pvoid;

/// Default constructor
TaskCheck::TaskCheck(const std::string& dns, const std::string& node) {
  std::string n = RTL::str_upper(RTL::nodeNameShort());
  m_dns = dns;
  m_node = node;
  m_connectTime   = system_clock::from_time_t(1);
  m_current_db_server = NO_CURRENT_SERVER;
  m_dim_server    = '/' + n + '/' + RTL::processName();
  m_dis_dns_ID    = ::dis_add_dns(n.c_str(), ::dim_get_dns_port());
  m_dim_server_ID = ::dis_add_service_dns(m_dis_dns_ID,
					  (m_dim_server+"/server").c_str(),
					  "C",0,0,feed_std_string,(long)&m_current_db_server);
  m_dim_status_ID = ::dis_add_service_dns(m_dis_dns_ID,
					  (m_dim_server+"/status").c_str(),
					  "C",0,0,feed_std_string,(long)&m_current_db_status);
  m_dim_query_ID  = ::dis_add_service_dns(m_dis_dns_ID,
					  (m_dim_server+"/query").c_str(),
					  "C",0,0,feed_std_string,(long)&m_current_db_query);
  ::dis_start_serving_dns(m_dis_dns_ID, m_dim_server.c_str());
}

/// Default destructor
TaskCheck::~TaskCheck()   {
  if ( m_dim_server_ID ) ::dis_remove_service(m_dim_server_ID);
  if ( m_dim_status_ID ) ::dis_remove_service(m_dim_status_ID);
  if ( m_dim_query_ID  ) ::dis_remove_service(m_dim_query_ID);
}

/// Add submit command string replacement 
void TaskCheck::replaceString(const std::string& from, const std::string& to)   {
  m_replacements[from] = to;
}

/// Set strict matching
bool TaskCheck::setStrictMatching(bool new_value)    {
  bool tmp = m_strict_match;
  m_strict_match = new_value;
  return tmp;
}

/// Initalization callback
bool TaskCheck::initialize()   {
  if ( !m_inited )   {
    m_tmName = "/FMC/"+RTL::str_upper(m_node)+"/task_manager/";
    m_tmInfo = ::dic_info_service((m_tmName+"longList").c_str(), MONITORED,
				  0, 0, 0, tmSrvInfoHandler, long(this), 0, 0);
    TimeSensor::instance().add(this, 1, pvoid(TASKCHECK_LOAD_DB));
    TimeSensor::instance().add(this, m_dbCheckUpdateTmo, pvoid(TASKCHECK_CHECK_DB));
    TimeSensor::instance().add(this, 1, pvoid(TASKCHECK_CHECK_TM));
    m_inited = true;
  }
  return true;
}

/// Dim callback to retrieve the task content
void TaskCheck::tmSrvInfoHandler(void* tag, void* address, int* size) {
  if ( tag ) {
    TaskCheck* tc = *(TaskCheck**)tag;
    if ( address && size && *size>0 )   {
      char* ptr = (char*)address;
      std::vector<char>* data = new std::vector<char>();
      data->reserve(*size+1);
      data->insert(data->end(), ptr, ptr + *size);
      IocSensor::instance().send(tc, int(TASKCHECK_LOAD_TM), data);
    }
  }
}

/// Shutdown callback
bool TaskCheck::shutdown()    {
  return true;
}

/// Disentable the task content in the task manager
void TaskCheck::load_tm_info(const std::vector<char>& data)   {
  tm_tasks_t tasks;
  TmSrvTask task;
  size_t count = 0;
  size_t size = data.size();
  const char* start = &data[0];
  for ( const char* p=start; p<start+size; ++count )   {
    size_t rest = (count%6);
    switch(rest)    {
    case 0:
      task.pid     = ::atol(p);
      break;
    case 1:
      task.exe     = p;
      break;
    case 2:
      task.utgid   = p;
      break;
    case 3:
      break;
    case 4:
      task.state   = p;
      break;
    case 5:
      if ( task.state.find("terminated") == std::string::npos)  {
	::lib_rtl_output(LIB_RTL_DEBUG,"Task: %-32s %-32s [%s]",
			 task.exe.c_str(), task.utgid.c_str(), task.state.c_str());
	tasks[task.utgid] = task;
	task = TmSrvTask();
      }
      break;
    default:
      break;
    }
    p += ::strlen(p)+1;
  }
  {
    std::lock_guard<std::mutex> lock(m_lock);
    m_tm_tasks = std::move(tasks);
    m_info_changed = true;
  }
  ::lib_rtl_output(LIB_RTL_DEBUG,"+++ Updated %ld Tasks from the tmSrv information.", m_tm_tasks.size());
}

/// Load all relevant information from the database server
void TaskCheck::load_db_info()    {
  int count = 0;
  std::string bad_servers;
  for( const auto& s : servers )    {
    try  {
      auto vec_tasks = s.getTasksByNode(m_node);
      db_tasks_t tasks;
      for ( auto& t : vec_tasks )
	tasks[t.utgid] = std::move(t);
      {
	std::lock_guard<std::mutex> lock(m_lock);
	count = synchronize_tm_to_db(tasks);
	m_db_tasks = std::move(tasks);
	m_info_changed = true;
      }
      std::stringstream info;
      info << ::lib_rtl_timestr() << ": " << s->name() << ": Updated " << m_db_tasks.size()
	   << " Tasks from the database. Synchronized " << count << ".";
      ::lib_rtl_output(count>0 ? LIB_RTL_INFO : LIB_RTL_DEBUG,"+++ %s",info.str().c_str());
      if ( !m_db_tasks.empty() )   {
	m_connectTime = system_clock::now();
	{
	  DimLock dimLock;
	  m_current_db_server = s->name();
	  m_current_db_status = info.str();
	  ::dis_update_service(m_dim_server_ID);
	  ::dis_update_service(m_dim_status_ID);
	}
      }
      return;
    }
    catch(const std::exception& e)    {
      bad_servers += s->name() + " ";
      ::lib_rtl_output(LIB_RTL_DEBUG,
		       "+++ %s: Failed to connect server: %s", s->name().c_str(), e.what());
    }
    catch(...)    {
      bad_servers += s->name() + " ";
      ::lib_rtl_output(LIB_RTL_DEBUG,
		       "+++ %s: Failed to connect server.", s->name().c_str());
    }
  }
  {
    DimLock dimLock;
    m_current_db_status = "Database update FAILED -- no server available";
    m_current_db_server = NO_CURRENT_SERVER;
    ::dis_update_service(m_dim_server_ID);
    ::dis_update_service(m_dim_status_ID);
  }
  ::lib_rtl_output(LIB_RTL_WARNING,"+++ Failed to connect servers %sfor update."
		   " Will retry later.", bad_servers.c_str());
  TimeSensor::instance().add(this, m_dbReconnectTmo, pvoid(TASKCHECK_LOAD_DB));
}

void TaskCheck::check_db_for_update()    {
  time_t stamp  = system_clock::to_time_t(m_connectTime);
  std::string bad_servers;
  for( const auto& s : servers )    {
    try  {
      auto result = s.needDbUpdate(m_node, 1234, stamp);
      {
	DimLock dimLock;
	std::stringstream info;
	info << ::lib_rtl_timestr() << ": " << s->name() << ": Checked for updates. "
	     << (result ? "No updates known" : "Need to update tasks from DB");
	m_current_db_server = s->name();
	m_current_db_query = info.str();
	::dis_update_service(m_dim_server_ID);
	::dis_update_service(m_dim_query_ID);
      }
      if ( result || m_db_tasks.empty() )   {
	::lib_rtl_output(LIB_RTL_INFO, "+++ Need to update tasks from DB (%s): %ld",
			 s->name().c_str(), long(stamp));
	IocSensor::instance().send(this, TASKCHECK_LOAD_DB);
	TimeSensor::instance().add(this, m_dbCheckUpdateTmo, pvoid(TASKCHECK_CHECK_DB));  
	return;
      }
      ::lib_rtl_output(LIB_RTL_DEBUG, "+++ %s: Checked for updates: None necessary %ld",
		       s->name().c_str(), long(stamp));
      TimeSensor::instance().add(this, m_dbCheckUpdateTmo, pvoid(TASKCHECK_CHECK_DB));  
      return;
    }
    catch(const std::exception& e)    {
      ::lib_rtl_output(LIB_RTL_DEBUG,
		       "+++ Failed to check server %s for updates: %s",
		       s->name().c_str(), e.what());
      bad_servers += s->name() + " ";
    }
    catch(...)    {
      ::lib_rtl_output(LIB_RTL_DEBUG,"+++ Failed to check server %s for updates.",
		       s->name().c_str());
      bad_servers += s->name() + " ";
    }
  }
  TimeSensor::instance().add(this, m_dbCheckUpdateTmo, pvoid(TASKCHECK_CHECK_DB));  
  ::lib_rtl_output(LIB_RTL_WARNING,"+++ Failed to check server(s) %sfor update."
		   " Will retry later.", bad_servers.c_str());
  {
    DimLock dimLock;
    m_current_db_query  = "Update query FAILED -- no server available";
    m_current_db_server = NO_CURRENT_SERVER;
    ::dis_update_service(m_dim_server_ID);
    ::dis_update_service(m_dim_query_ID);
  }
}

/// Kill task instance using the tmSrv
bool TaskCheck::kill_task(const std::string& utgid)   {
  ::lib_rtl_output(LIB_RTL_ALWAYS, "======= KILL  %s.",utgid.c_str());
  return true;
}

/// Start task instance using the tmSrv
bool TaskCheck::start_task(const Task& task)   {
  time_point_t now = system_clock::now();
  StartInfo& si  = m_start_info[task.name];
#if 0
  if ( 0 == si.hash || si.hash != std::hash(task.restart_parameters) )    {
    si.hash = std::hash(task.restart_parameters);
  }
#endif
  long diff = system_clock::to_time_t(now) - system_clock::to_time_t(si.last_submit);
  ::lib_rtl_output(LIB_RTL_DEBUG, "%s: Diff:%ld %ld %ld",task.name.c_str(),
		   diff,
		   duration_cast<std::chrono::seconds>(si.restart_interval).count(),
		   duration_cast<std::chrono::seconds>(si.pause_interval).count());    
  if ( diff < duration_cast<std::chrono::seconds>(si.restart_interval).count() )    {
    if ( si.print == 0 )  {
      si.print = 1;
      ::lib_rtl_output(LIB_RTL_ALWAYS, "Ignore task restart of %s [Restart-interval]. "
		       "Waiting for %ld seconds.",task.name.c_str(), 
		       duration_cast<std::chrono::seconds>(si.restart_interval).count()-diff);    
    }
    return false;
  }
  if ( si.restart_attempts >= si.max_restart_attempts &&
       diff < duration_cast<std::chrono::seconds>(si.pause_interval).count() )    {
    if ( si.print == 1 )  {
      si.print = 2;
      ::lib_rtl_output(LIB_RTL_ALWAYS, "Ignore task %s [Exceeded restart-attempts]. "
		       "Waiting for %ld seconds.",task.name.c_str(), 
		       duration_cast<std::chrono::seconds>(si.pause_interval).count()-diff);
    }
    return false;
  }
  else if ( si.restart_attempts >= si.max_restart_attempts )  {
    si.restart_attempts = 0;
  }
  ++si.restart_attempts;
  si.last_submit = now;
  si.print = 0;

  std::string command, svc = m_tmName + "start";
  std::stringstream cmd;
  cmd << task.submit_parameters << " -u " << task.utgid 
      << " " << task.command << " " << task.command_parameters;
  command = cmd.str();
  for(const auto& r : m_replacements)
    command = RTL::str_replace(command,r.first,r.second);
  int ret = ::dic_cmnd_service(svc.c_str(), (void*)command.c_str(), command.length()+1);
  if ( ret == 1 )   {
    ::lib_rtl_output(LIB_RTL_INFO, "======= Successfully STARTED %s [%s].",task.utgid.c_str(),command.c_str());
  }
  else   {
    ::lib_rtl_output(LIB_RTL_INFO, "======= FAILED to start      %s [%s].",task.utgid.c_str(),command.c_str());
  }
  return true;
}

/// Check which tasks have to be killed to to changes in the database
int TaskCheck::synchronize_tm(const db_tasks_t& processes)  {
  int count = 0;
  for ( const auto& t : processes )   {
    auto itm = m_tm_tasks.find(t.second.utgid);
    if ( itm == m_tm_tasks.end() )   {
      start_task(t.second);
      ++count;
    }
  }
  return count;
}

/// Check which tasks have to be killed to to changes in the database
int TaskCheck::synchronize_tm_to_db(const db_tasks_t& processes)  {
  int count = 0;
  std::vector<std::string> killed;
  ::lib_rtl_output(LIB_RTL_DEBUG, "+++ Matching database [%ld tasks] to tmSrv information [%ld tasks].",
		   processes.size(), m_tm_tasks.size());
  if ( m_strict_match )   {
    for ( auto& t : m_tm_tasks )   {
      const auto& utgid = t.second.utgid;
      auto i = processes.find(utgid);
      if ( i == processes.end() )   {
	killed.push_back(utgid);
	kill_task(utgid);
	++count;
      }
    }
  }
  for ( const auto& utgid : killed )   {
    auto i = m_tm_tasks.find(utgid);
    if ( i != m_tm_tasks.end() )
      m_tm_tasks.erase(i);
  }
  for ( auto& t : processes )   {
    const auto& utgid = t.second.utgid;
    auto i = m_db_tasks.find(utgid);
    if ( i != m_db_tasks.end() )  {
      const auto& task = t.second;
      if ( task.name != i->second.name ||
	   task.command != i->second.command ||
	   task.submit_parameters != i->second.submit_parameters ||
	   task.command_parameters != i->second.command_parameters )  {
	kill_task(task.utgid);
	++count;
      }
    }
  }
  count += synchronize_tm(processes);
  return count;
}

/// Interactor overload: interrupt handler callback
void TaskCheck::handle(const Event& event)   {
  switch(event.eventtype) {
  case NetEvent:
    break;
  case TimeEvent:
    handleTimer(event);
    break;
  case IocEvent:
    handleIoc(event);
    break;
  default:
    break;
  }
}

/// Handle IOV event
void TaskCheck::handleIoc(const Event& event)   {
  switch(event.type)   {
  case TASKCHECK_LOAD_DB:   {
    load_db_info();
    break;
  }
  case TASKCHECK_LOAD_TM:   {
    std::unique_ptr<std::vector<char> > data(event.iocPtr<std::vector<char> >());
    load_tm_info(*data);
    break;
  }
  case TASKCHECK_CHECK_DB:   {
    check_db_for_update();
    break;
  }
  default:
    break;
  }
}

/// Handle Time event
void TaskCheck::handleTimer(const Event& event)    {
  switch(long(event.timer_data))   {
  case TASKCHECK_LOAD_DB:
    IocSensor::instance().send(this, TASKCHECK_LOAD_DB);
    break;
  case TASKCHECK_CHECK_DB:
    IocSensor::instance().send(this, TASKCHECK_CHECK_DB);
    break;
  case TASKCHECK_CHECK_TM:  {
    //::lib_rtl_output(LIB_RTL_ALWAYS, "+++ Regular check..... %ld",::time(0));     
    {
      std::lock_guard<std::mutex> lock(m_lock);
      if ( !m_info_changed )   {
	TimeSensor::instance().add(this, 1, pvoid(TASKCHECK_CHECK_TM));
	return;
      }
      m_info_changed = false;
    }
    int count = synchronize_tm(m_db_tasks);
    ::lib_rtl_output(LIB_RTL_DEBUG, "+++ Matching tmSrv [%ld tasks] to database information [%ld tasks]. Acted on %d tasks",
		     m_tm_tasks.size(), m_db_tasks.size(), count);
    TimeSensor::instance().add(this, 1, pvoid(TASKCHECK_CHECK_TM));
    break;
  }
  default: 
    break;
  }
}

extern "C" int taskdb_check(int argc, char** argv)   {
  std::string node, dns;
  std::string port  = "3501";
  std::string mount = "/TDBDATA/XMLRPC";
  std::string server;
  std::string tm_dns;
  int    print_level = LIB_RTL_INFO;
  RTL::CLI cli(argc, argv, [] (int, char**)  {
      std::cout << " run_task_check  -opt [-opt]                            \n"
		<< "    -dns=<node-name>          DNS node name             \n"
		<< "    -node=<node-name>         Node name to query tasks  \n"
		<< "    -server=<server-name>     TaskDB server host name   \n"
		<< "    -port=<port-number>       TaskDB server port        \n"
		<< "    -tm_dns=<node-name>       Task manager DNS node     \n"
		<< "    -mount=<mount-name>       Web server directory name \n"
		<< std::endl;
      ::exit(0);
    });
  cli.getopt("dns",     3, dns);
  cli.getopt("node",    3, node);
  cli.getopt("port",    3, port);
  cli.getopt("server",  3, server);
  cli.getopt("mount",   3, mount);
  cli.getopt("tm_dns",  3, tm_dns);
  cli.getopt("print",   3, print_level);

  ::dis_set_dns_node(tm_dns.c_str());
  ::dic_set_dns_node(tm_dns.c_str());
  RTL::Logger::install_rtl_printer(print_level);
  RTL::Logger::getGlobalDevice()->compileFormat("%LEVEL %-19SOURCE");

  TaskCheck chk(dns, node);
  lib_rtl_output(LIB_RTL_ALWAYS,"-%s- Domain RPC service of type: TaskCheck started. DNS:%s TM:%s %s%s",
		 node.c_str(), dns.c_str(), tm_dns.c_str(), server.empty() ? "" : "Closest Server:",
		 server.empty() ? "" : server.c_str());
  if ( cli.getopt("debug", 3) != 0 )   {
    int wait = 1;
    while ( wait ) ::lib_rtl_sleep(100);
  }
  if ( !server.empty() )   {
    auto ports = RTL::str_split(port,',');
    for( const auto& p : ports )  {
      lib_rtl_output(LIB_RTL_ALWAYS,"-%s- Use TaskDB cache: %s : %s",
		     node.c_str(), server.c_str(), p.c_str());
      chk.servers.emplace_back(TaskDBApi(rpc::client<http::HttpClient>(server, mount, p)));
    }
  }
  /// These are the event builder nodes. They do not have a cache
  if ( 0 == cli.getopt("nodefaults", 5) )  {
    auto clk = ::clock()%2;
    chk.servers.emplace_back(TaskDBApi(rpc::client<http::HttpClient>("ecs03.lbdaq.cern.ch", mount, clk ? "3501" : "3502")));
    chk.servers.emplace_back(TaskDBApi(rpc::client<http::HttpClient>("ecs03.lbdaq.cern.ch", mount, clk ? "3502" : "3501")));
    // chk.servers.emplace_back(make_unique<http::HttpClient>("slavetasks.service.consul.lhcb.cern.ch", mount, port));
  }
  chk.replaceString("<NODE>",       node);
  chk.replaceString("<DIM_DNS>",    dns);
  chk.replaceString("<TOP_LOGGER>", "ecs03");
  chk.replaceString("<SCRIPTS>",    "/group/online/dataflow/scripts");

  chk.initialize();
  IocSensor::instance().run();
  return 1;
}

/// Run TaskDBCheck process
extern "C" int run_task_check(int argc, char** argv)   {
  return taskdb_check(argc, argv);
}


/// Callback to check if the cache server is up
extern "C" int rtl_check_taskdb_server(int argc, char** argv)   {
  std::string mount = "/TDBDATA/XMLRPC";
  std::string node="localhost", port="3501", server="localhost";
  RTL::CLI cli(argc, argv, [] (int, char**)  {
      std::cout << " run_check_taskdb_server  -opt [-opt]                   \n"
		<< "    -dns=<node-name>          DNS node name             \n"
		<< "    -server=<server-name>     TaskDB server host name   \n"
		<< "    -port=<port-number>       TaskDB server port        \n"
		<< std::endl;
      ::exit(0);
    });
  bool quiet = cli.getopt("quiet", 3);
  cli.getopt("node",    3, node);
  cli.getopt("port",    3, port);
  cli.getopt("mount",   3, mount);
  cli.getopt("server",  3, server);
  try  {
    TaskDBApi api(rpc::client<http::HttpClient>(server, mount, port));
    api.needDbUpdate(node, 0, 0);
  }
  catch(const std::exception& e)   {
    if ( !quiet )   {
      lib_rtl_output(LIB_RTL_ALWAYS,"+++ TaskDB server: %s:%s%s %s",
		     server.c_str(), port.c_str(), mount.c_str(),
		     "NOT REACHABLE.");
      
    }
    return EINVAL;
  }
  lib_rtl_output(LIB_RTL_ALWAYS,"+++ TaskDB server: %s:%s%s %s",
		 server.c_str(), port.c_str(), mount.c_str(),
		 "REACHABLE.");
  return 0;
}

