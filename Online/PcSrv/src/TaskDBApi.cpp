//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : PcSrv
//
//  Author     : Markus Frank
//==========================================================================

/// Framework include files
#include <PcSrv/TaskDBApi.h>
#include <RTL/Compress.h>
#include <RPC/XMLRPC.h>

/// C/C++ include files
#include <iostream>
#include <sstream>

using namespace std;
using namespace xmlrpc;
using namespace taskdb;

namespace  {
  int handle_response_status(const MethodResponse& r)   {
    return r.data<string>() == "Success" ? 1 : 0;
  }
}

/// Access TaskDB to delete matching tasks
int TaskDBApi::test(bool with_time)  const  {
  {
    MethodCall c;
    c.setMethod("boolTest");
    MethodResponse r = this->call(c);
    auto result = r.data<bool>();
    cout << "Result<bool>:             " << result << endl;
    ++num_calls;
  }
  {
    MethodCall c;
    c.setMethod("smallintTest");
    MethodResponse r = this->call(c);
    auto result = r.data<char>();
    cout << "Result<char>:             " << (int)result << endl;
    ++num_calls;
  }
  {
    MethodCall c;
    c.setMethod("smallintTest");
    MethodResponse r = this->call(c);
    auto result = r.data<unsigned char>();
    cout << "Result<unsigned char>:    " << (int)result << endl;
    ++num_calls;
  }
  {
    MethodCall c;
    c.setMethod("smallintTest");
    MethodResponse r = this->call(c);
    auto result = r.data<short>();
    cout << "Result<short>:            " << result << endl;
    ++num_calls;
  }
  {
    MethodCall c;
    c.setMethod("smallintTest");
    MethodResponse r = this->call(c);
    auto result = r.data<unsigned short>();
    cout << "Result<unsigned short>:   " << result << endl;
    ++num_calls;
  }
  {
    MethodCall c;
    c.setMethod("intTest");
    MethodResponse r = this->call(c);
    auto result = r.data<int>();
    cout << "Result<int>:              " << result << endl;
    ++num_calls;
  }
  {
    MethodCall c;
    c.setMethod("intTest");
    MethodResponse r = this->call(c);
    auto result = r.data<unsigned int>();
    cout << "Result<unsigned int>:     " << result << endl;
    ++num_calls;
  }
  {
    MethodCall c;
    c.setMethod("intTest");
    MethodResponse r = this->call(c);
    auto result = r.data<long>();
    cout << "Result<long>:             " << result << endl;
    ++num_calls;
  }
  {
    MethodCall c;
    c.setMethod("intTest");
    MethodResponse r = this->call(c);
    auto result = r.data<unsigned long>();
    cout << "Result<unsigned long>:    " << result << endl;
    ++num_calls;
  }
  {
    MethodCall c;
    c.setMethod("floatTest");
    MethodResponse r = this->call(c);
    auto result = r.data<float>();
    cout << "Result<float>:            " << result << endl;
    ++num_calls;
  }
  {
    MethodCall c;
    c.setMethod("floatTest");
    MethodResponse r = this->call(c);
    auto result = r.data<double>();
    cout << "Result<double>:           " << result << endl;
    ++num_calls;
  }
  {
    MethodCall c;
    c.setMethod("stringTest");
    MethodResponse r = this->call(c);
    string result = r.data<string>();
    cout << "Result<string>:           " << result << endl;
    ++num_calls;
  }
  if ( with_time )  {
    MethodCall c;
    c.setMethod("timeTest");
    MethodResponse r = this->call(c);
    auto result = r.data<tm>();
    cout << "Result<tm>:               " << _toString(result) << endl;
    ++num_calls;
  }
  return 1;
}

/// Retrieve the database cache
string TaskDBApi::getCacheRaw(long stamp)  const    {
  MethodCall c;
  c.setMethod("getCache").addParam(stamp);
  MethodResponse r = this->call(c);
  ++num_calls;
  return r.data<string>();
}

/// Retrieve the database cache in XML form
vector<unsigned char> TaskDBApi::getCache(long stamp)  const    {
  string data = getCacheRaw(stamp);
  return Online::compress::decompress("gzip", Online::compress::base64_decode(data));
}

/// Access TaskDB to retrieve the tasks for this node
TaskDBApi::tasks_t TaskDBApi::getTasksByNode(const string& node)  const  {
  MethodCall c;
  c.setMethod("getTasksByNode").addParam(node);
  MethodResponse r = this->call(c);
  ++num_calls;
  return r.data<tasks_t>();
}

/// Access TaskDB to add a new task
int TaskDBApi::addTask(const string& name,
		       const string& utgid,
		       const string& command,
		       const string& task_parameters,
		       const string& command_parameters,
		       const string& restart_parameters,
		       const string& description)   const   
{
  MethodCall c;
  c.setMethod("addTask")
    .addParam(name)
    .addParam(utgid)
    .addParam(command)
    .addParam(task_parameters)
    .addParam(command_parameters)
    .addParam(restart_parameters)
    .addParam(description);
  ++num_calls;
  return handle_response_status(this->call(c));
}

/// Access TaskDB to retrieve matching tasks
TaskDBApi::tasks_t TaskDBApi::getTask(const string& match)  const  {
  MethodCall c;
  c.setMethod("getTask").addParam(match);
  MethodResponse r = this->call(c);
  ++num_calls;
  return r.data<tasks_t>();
}

/// Access TaskDB to delete matching tasks
int TaskDBApi::deleteTask(const string& match)  const  {
  MethodCall c;
  c.setMethod("deleteTask").addParam(match);
  ++num_calls;
  return handle_response_status(this->call(c));
}

/// Add new task set to the database
int TaskDBApi::addSet(const string& name,
		      const string& description)   const   {
  MethodCall c;
  c.setMethod("addSet")
    .addParam(name)
    .addParam(description);
  ++num_calls;
  return handle_response_status(this->call(c));
}

/// Delete task set from the database
int TaskDBApi::deleteSet(const string& name)   const   {
  MethodCall c;
  c.setMethod("deleteSet").addParam(name);
  ++num_calls;
  return handle_response_status(this->call(c));
}

/// Access TaskDB to retrieve matching task sets
TaskDBApi::sets_t TaskDBApi::getSet(const string& match)   const   {
  MethodCall c;
  c.setMethod("getSet").addParam(match);
  MethodResponse r = this->call(c);
  ++num_calls;
  return r.data<sets_t>();
}

/// Access TaskDB to retrieve matching task in a given task set
TaskDBApi::tasks_in_set_t TaskDBApi::tasksInSet(const string& set_name)   const   {
  MethodCall c;
  c.setMethod("tasksInSet").addParam(set_name);
  MethodResponse r = this->call(c);
  ++num_calls;
  return r.data<tasks_in_set_t>();
}

/// Add new node class to the database
int TaskDBApi::addClass(const string& name,
			const string& description)   const   {
  MethodCall c;
  c.setMethod("addClass")
    .addParam(name)
    .addParam(description);
  ++num_calls;
  return handle_response_status(this->call(c));
}

/// Delete node class from the database
int TaskDBApi::deleteClass(const string& name)   const   {
  MethodCall c;
  c.setMethod("deleteClass").addParam(name);
  ++num_calls;
  return handle_response_status(this->call(c));
}

/// Access TaskDB to retrieve matching node classes
TaskDBApi::classes_t TaskDBApi::getClass(const string& match)   const   {
  MethodCall c;
  c.setMethod("getClass").addParam(match);
  MethodResponse r = this->call(c);
  ++num_calls;
  return r.data<classes_t>();
}

/// Access TaskDB to retrieve matching task in a given task set
TaskDBApi::sets_in_class_t TaskDBApi::taskSetsInClass(const string& match)   const  {
  MethodCall c;
  c.setMethod("taskSetsInClass").addParam(match);
  MethodResponse r = this->call(c);
  ++num_calls;
  return r.data<sets_in_class_t>();
}

/// Add new node type to the database
int TaskDBApi::addNode(const string& name,
		       int priority=0,
		       const string& description)   const   {
  MethodCall c;
  stringstream str;
  if ( priority != 0 ) str << priority;
  c.setMethod("addNode")
    .addParam(name)
    .addParam(str.str())
    .addParam(description);
  ++num_calls;
  return handle_response_status(this->call(c));
}

/// Delete node type from the database
int TaskDBApi::deleteNode(const string& name)   const   {
  MethodCall c;
  c.setMethod("deleteNode").addParam(name);
  ++num_calls;
  return handle_response_status(this->call(c));
}

/// Access TaskDB to retrieve matching node types
TaskDBApi::nodes_t    TaskDBApi::getNode(const string& match)   const   {
  MethodCall c;
  c.setMethod("getNode").addParam(match);
  MethodResponse r = this->call(c);
  ++num_calls;
  return r.data<nodes_t>();
}

/// Access TaskDB to retrieve matching task in a given task set
TaskDBApi::classes_in_node_t TaskDBApi::classesInNode(const string& match)   const  {
  MethodCall c;
  c.setMethod("classesInNode").addParam(match);
  MethodResponse r = this->call(c);
  ++num_calls;
  return r.data<classes_in_node_t>();
}

/// Access TaskDB to check if a database update is required
bool TaskDBApi::needDbUpdate(const string& host, int port, long stamp)   const  {
  MethodCall c;
  c.setMethod("needDbUpdate").addParam(host).addParam(port).addParam(stamp);
  MethodResponse r = this->call(c);
  ++num_calls;
  return r.data<bool>();
}
