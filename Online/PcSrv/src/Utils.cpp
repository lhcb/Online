//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================

// C/C++ include files
#include <sstream>
#include <iostream>

// Framework include files
#include "PcSrv/Task.h"
#include "RPC/XMLRPC.h"
#include "RTL/rtl.h"

using namespace std;

namespace taskdb {
  size_t print_msg(void* arg,int lvl,const char* fmt,va_list& args) {
    if ( lvl >= long(arg) ) {
      size_t result;
      string format = RTL::nodeNameShort() + RTL::timestr() + " ";
      switch(lvl) {
      case LIB_RTL_VERBOSE:
	format += "VERBOSE ";
	break;
      case LIB_RTL_DEBUG:
	format += "DEBUG   ";
	break;
      case LIB_RTL_INFO:
	format += "INFO    ";
	break;
      case LIB_RTL_WARNING:
	format += "WARNING ";
	break;
      case LIB_RTL_ERROR:
	format += "ERROR   ";
	break;
      case LIB_RTL_FATAL:
	format += "FATAL   ";
	break;
      case LIB_RTL_ALWAYS:
	format += "ALWAYS  ";
	break;
      default:
	format += "UNKNWON ";
	break;
      }
      format += fmt;
      format += "\n";
      result = ::vfprintf(stdout, format.c_str(), args);
      ::fflush(stdout);
      return result;
    }
    return 1;
  }
  size_t print_msg2(void* arg,int lvl,const char* fmt,va_list& args) {
    if ( lvl >= long(arg) ) {
      size_t result;
      string format = RTL::timestr(::time(0),"%b%d-%H%M%S");
      static string utgid;
      if ( utgid.empty() )  {
	utgid = ::lib_rtl_getenv("UTGID") ? ::lib_rtl_getenv("UTGID") : "gentest";
	utgid += "(";
	utgid += "main";
	utgid += ") ";
      }
      switch(lvl) {
      case LIB_RTL_VERBOSE:
	format += "[DEBUG]";
	break;
      case LIB_RTL_DEBUG:
	format += "[DEBUG]";
	break;
      case LIB_RTL_INFO:
	format += "[INFO] ";
	break;
      case LIB_RTL_WARNING:
	format += "[WARN] ";
	break;
      case LIB_RTL_ERROR:
	format += "[ERROR]";
	break;
      case LIB_RTL_FATAL:
	format += "[FATAL]";
	break;
      case LIB_RTL_ALWAYS:
	format += "[ALWAY]";
	break;
      default:
	format += "UNKNWON ";
	break;
      }
      format += RTL::nodeNameShort();
      format += ": ";
      format += utgid;
      format += fmt;
      format += "\n";
      result = ::vfprintf(stdout, format.c_str(), args);
      ::fflush(stdout);
      return result;
    }
    return 1;
  }
  void print_startup(const char* msg) {
    ::lib_rtl_output(LIB_RTL_ALWAYS,"%s %s > Readout monitor %s started on %s.",
		     ::lib_rtl_timestr(), RTL::processName().c_str(), msg,
		     RTL::nodeNameShort().c_str());
  }
}
