//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//
//==========================================================================
/** **************************************************************************
 * \brief This file creates Python wrappers for the DimRpc and DimRpcInfo
 * DIM classes.
 *
 * \authors M. Frank, N. Neufeld, R. Stoica
 * \date Nov. 2007 - September 2008
 *
 * *************************************************************************/

#define DIMCPP_MODULE
#include <dim/dis.hxx>
#include <dim/dic.hxx>
#include <cctype>
#include <cstdlib>
#include <cstdio>
#include <mutex>

#define PY_SSIZE_T_CLEAN
extern "C" {
#include "Python.h"
#include "structmember.h"
}
#include "pydim_utils.cpp"


/** @addtogroup dim
 * @{
 */

namespace  {

  std::mutex pydimrpc_call_lock;

#define PYDIMRPC_MAGIC 0xFEEDBABEDEADCAFEUL

  class DimRpc_Object;
  class PyDimRpcWrapper;
  class DimRpcInfo_Object;
  class PyDimRpcInfoWrapper;

  bool DimRpc_verify_self(const DimRpc_Object* self, int check);
  bool DimRpcInfo_verify_self(const DimRpcInfo_Object* self, int check);

  /****************************************************************************
   * DimRpc Object
   * **************************************************************************/
  class DimRpc_Object {
  public:
    PyObject_HEAD
    unsigned long magic          { PYDIMRPC_MAGIC };
    PyDimRpcWrapper *cpp_dimRpc  { nullptr };
    char *format_in              { nullptr };
    char *format_out             { nullptr };
    PyObject *nolink             { nullptr };
  };

  /// DimRpc Wrapper
  /**
   * This class is only a proxy to pass the call from the C++ rpcHandler
   * function to the Python implementation. For this a reference to the
   * real Python object is needed.
   * Need to reimplement the constructor, destructor and rpcHandler
   */
  class PyDimRpcWrapper : public DimRpc   {
  public:
    unsigned long magic   { PYDIMRPC_MAGIC };
  private:
    DimRpc_Object *self { nullptr }; // note that self can be a derived class
    PyObject      *name { nullptr };
  public:
    PyDimRpcWrapper (char *dim_name, char *fmt_in, char *fmt_out, DimRpc_Object *obj)
      : DimRpc(dim_name, fmt_in, fmt_out)
    {
      if (obj) {
	Py_INCREF(obj);
      }
      this->self = obj;
      this->name = PyString_FromString("rpcHandler");
    }

    ~PyDimRpcWrapper()    {
      this->magic = 0UL;
      Py_XDECREF(this->self);
      Py_XDECREF(this->name);
    }

    void rpcHandler() override    {
      /** This function is called by the DIM library and will not have
       * the Python interpretor lock. All Python API calls must be protected
       * using Ensure() and Release().
       */
      if ( !DimRpc_verify_self(this->self, 7) )   {
	return;
      }
      else if ( this != this->self->cpp_dimRpc )   {
	PyErr_SetString(PyExc_AttributeError, "C++ Invalid call to PyDimRpcWrapper::rpcHandler method [internal error]");
	return;
      }
      else {
	std::lock_guard<std::mutex> lock(pydimrpc_call_lock);
	PyGILState_STATE gstate = PyGILState_Ensure();
	PyObject* obj = (PyObject*)this->self;
	PyObject *res = PyObject_CallMethodNoArgs(obj, this->name);
	if ( !res ) {
	  print ("C++ Invalid call to virtual PyDimRpcWrapper::rpcHandler method %p", (void*)res);
	  PyErr_Print();
	}
	Py_XDECREF(res);
	PyGILState_Release(gstate);
      }
    }
  }; //end PyDimRpcWrapper

  bool DimRpc_verify_self(const DimRpc_Object* self, int check)   {
    if ( !self )   {
      PyErr_SetString(PyExc_AttributeError, "C++ No 'self' Python object found. Can't call to python layer");
      return false;
    }
    if ( (check&1) && !self->cpp_dimRpc )   {
      // should never reach this point
      PyErr_SetString(PyExc_AttributeError, "C++ Dim RPC object is NULL");
      return false;
    }
    if ( (check&2) && self->magic != PYDIMRPC_MAGIC )  {
      PyErr_SetString(PyExc_AttributeError, "C++ Dim RPC object is invalid [bad magic code]");
      return false;
    }
    if ( (check&4) && self->cpp_dimRpc->magic != PYDIMRPC_MAGIC )  {
      PyErr_SetString(PyExc_AttributeError, "C++ Dim RPC client object is invalid [bad magic code]");
      return false;
    }
    return true;
  }

  /****************************************************************************
   * DimRpcInfo Object
   * **************************************************************************/
  class DimRpcInfo_Object  {
  public:
    PyObject_HEAD
    unsigned long magic           { PYDIMRPC_MAGIC };
    PyDimRpcInfoWrapper *cpp_dimRpcInfo  { nullptr };
    char *format_in                      { nullptr };
    char *format_out                     { nullptr };
    PyObject *nolink                     { nullptr };
  };

  /****************************************************************************
   * DimRpcInfo Wrapper
   * **************************************************************************/
  class PyDimRpcInfoWrapper: public DimRpcInfo
  /** This class is only a proxy to pass the call from the C++ rpcInfoHandler
   * function to the Python implementation. For this a reference to the
   * real Python object is needed.
   * Need to reimplement the constructor, destructor and rpcInfoHandler
   */
  {
  public:
    unsigned long magic   { PYDIMRPC_MAGIC };
  private:
    DimRpcInfo_Object *self { nullptr }; // note that self can be a derived class
    PyObject          *name { nullptr };
  public:
    /** it is much easier to provide a simple setter for the python object
     * reference rather than reimplement all 12 costructors.
     * This needs to be called imediatelly after the init method of the
     * object.
     */
    PyDimRpcInfoWrapper(const char *info_name, int time, void *nolink, int nolinksize)
      : DimRpcInfo(info_name, time, nolink, nolinksize) {
      this->name = PyString_FromString("rpcInfoHandler");
    }

    PyDimRpcInfoWrapper(const char *info_name, void *nolink, int nolinksize)
      : DimRpcInfo(info_name, nolink, nolinksize) {
      this->name = PyString_FromString("rpcInfoHandler");
    }

    int setPyRef (DimRpcInfo_Object *myself)    {
      if (!myself)
	return 0;
      PyObject* obj = (PyObject*)myself;
      Py_INCREF(obj);
      this->self = myself;
      return 1;
    }

    ~PyDimRpcInfoWrapper()    {
      this->magic = 0UL;
      Py_XDECREF(this->self);
      Py_XDECREF(this->name);
    }

    void rpcInfoHandler() override    {
      if ( !DimRpcInfo_verify_self(this->self, 7) )   {
	return;
      }
      else if ( this != this->self->cpp_dimRpcInfo )   {
	PyErr_SetString(PyExc_AttributeError, "C++ Invalid call to PyDimRpcWrapper::rpcHandler method [internal error]");
	return;
      }
      else   {
	PyObject* obj = (PyObject*)this->self;
	PyGILState_STATE gstate = PyGILState_Ensure();
	PyObject *res = PyObject_CallMethodNoArgs(obj, this->name);
	if (!res) {
	  print ("C++ Invalid call to virtual DimRpcInfo::rpcInfoHandler method %p", (void*)res);
	  PyErr_Print();
	}
	else {
	  Py_XDECREF(res);
	}
	PyGILState_Release(gstate);
      }
    }
  }; //end PyDimRpcInfoWrapper

  bool DimRpcInfo_verify_self(const DimRpcInfo_Object* self, int check)  {
    if ( !self )   {
      PyErr_SetString(PyExc_AttributeError, "C++ No 'self' Python object found. Can't call to python layer");
      return false;
    }
    if ( (check&1) && !self->cpp_dimRpcInfo ) {
      // should never reach this point
      PyErr_SetString(PyExc_AttributeError, "C++ Dim RPC info object is NULL");
      return false;
    }
    if ( (check&2) && self->magic != PYDIMRPC_MAGIC )  {
      PyErr_SetString(PyExc_AttributeError, "C++ Dim RPC info object is invalid [bad magic code]");
      return false;
    }
    if ( (check&4) && self->cpp_dimRpcInfo->magic != PYDIMRPC_MAGIC )  {
      PyErr_SetString(PyExc_AttributeError, "C++ Dim RPC info client object is invalid [bad magic code]");
      return false;
    }
    return true;
  }

  union func_ptr  {
    typedef PyObject* (*DimRpc_func_t) (DimRpc_Object*);
    typedef PyObject* (*DimRpc_arg_func_t) (DimRpc_Object*, PyObject*);
    typedef PyObject* (*DimRpc_info_func_t) (DimRpcInfo_Object*);
    typedef PyObject* (*DimRpc_info_arg_func_t) (DimRpcInfo_Object*, PyObject*);
    PyCFunction   pyc;
    DimRpc_func_t dim;
    DimRpc_arg_func_t dimarg;
    DimRpc_info_func_t diminfo;
    DimRpc_info_arg_func_t diminfoarg;
    func_ptr(DimRpc_func_t h)  {  dim = h;    }
    func_ptr(DimRpc_arg_func_t h)  {  dimarg = h;    }
    func_ptr(DimRpc_info_func_t h)  {  diminfo = h;    }
    func_ptr(DimRpc_info_arg_func_t h)  {  diminfoarg = h;    }
  };
}

static void DimRpc_dealloc(DimRpc_Object *self) {
  /** Dealocates a DimRpc object */
  self->magic = 0UL;
  delete self->cpp_dimRpc;
  PyObj_TYPE(self)->tp_free(self);
}

static int DimRpc_init(DimRpc_Object *self, PyObject *args, PyObject *kwds) {
  /** Allocates a new DimRpc_Object and a PyDimRpcWrapper inside it
   */
  char *name = nullptr, *format_in = nullptr, *format_out = nullptr;
  static const char *kwlist[] = {"name", "format_in", "format_out", NULL};

  if ( !PyArg_ParseTupleAndKeywords(args, kwds, "sss", (char**)kwlist,
                                    &name, &format_in, &format_out)
       ) {
    print("Invalid arguments. Received: %s %s %s", name, format_in, format_out);
    return -1;
  }
  if (!verify_dim_format(format_in) || !verify_dim_format(format_out)) {
    PyErr_SetString(PyExc_AttributeError, "Invalid formats specified");
    return -1;
  }
  self->magic = PYDIMRPC_MAGIC;
  self->cpp_dimRpc = new PyDimRpcWrapper(name, format_in, format_out, self);
  self->format_in  = new char[strlen(format_in)+1];
  self->format_out = new char[strlen(format_out)+1];
  strcpy(self->format_in, format_in);
  strcpy(self->format_out, format_out);
  //print("Created new dimRpc proxy");
  return 0;
}

static PyObject *DimRpc_new(PyTypeObject *type, PyObject* /* args */, PyObject* /* kwds */) {
  /** Allocates a new DimRpc_Object and initialises the cpp_dimRpc to NULL */
  DimRpc_Object *self = (DimRpc_Object*)type->tp_alloc(type, 0);
  if ( self != nullptr )  {
    self->cpp_dimRpc = nullptr;
    self->format_in = nullptr;
    self->format_out = nullptr;
  }
  /* in case of errors self is NULL and the error string is already set */
  return (PyObject*)self;
}

static PyObject *DimRpc_name (DimRpc_Object *self)  {
  if ( !DimRpc_verify_self(self,7) )   {
    return nullptr;
  }
  PyObject *res = PyString_FromString(self->cpp_dimRpc->getName());
  return res; // res is a new reference and will be owned by the caller
}

static PyObject *DimRpc_getInt (DimRpc_Object *self)  {
  if ( !DimRpc_verify_self(self,7) )   {
    return nullptr;
  }
  long  cpp_res = self->cpp_dimRpc->getInt();
  PyObject *res = PyInt_FromLong(cpp_res);
  return res; // res is a new reference and will be owned by the caller
}

static PyObject *DimRpc_getFloat (DimRpc_Object *self)  {
  if ( !DimRpc_verify_self(self,7) )   {
    return nullptr;
  }
  double cpp_res = self->cpp_dimRpc->getFloat();
  PyObject  *res = PyFloat_FromDouble(cpp_res);
  return res; // res is a new reference and will be owned by the caller
}

static PyObject *DimRpc_getDouble (DimRpc_Object *self)  {
  if ( !DimRpc_verify_self(self,7) )   {
    return nullptr;
  }
  double cpp_res = self->cpp_dimRpc->getDouble();
  PyObject *res = PyFloat_FromDouble(cpp_res);
  return res; // res is a new reference and will be owned by the caller
}

static PyObject *DimRpc_getString (DimRpc_Object *self)  {
  if ( !DimRpc_verify_self(self,7) )   {
    return nullptr;
  }
  char *cpp_res = self->cpp_dimRpc->getString();
  PyObject *res = PyString_FromString(cpp_res);
  return res; // res is a new reference and will be owned by the caller
}

static PyObject *DimRpc_getSize (DimRpc_Object *self)  {
  if ( !DimRpc_verify_self(self,7) )   {
    return nullptr;
  }
  int cpp_res = self->cpp_dimRpc->getSize();
  PyObject *res = PyInt_FromLong(cpp_res);
  return res; // res is a new reference and will be owned by the caller
}

static PyObject *DimRpc_getData(DimRpc_Object * self)   {
  if ( !DimRpc_verify_self(self,7) )   {
    return nullptr;
  }
  char* buff = (char*)self->cpp_dimRpc->getData();
  int buff_size = self->cpp_dimRpc->getSize();
  PyObject *res = dim_buf_to_tuple(self->format_in, buff, buff_size);
  if (res && PyTuple_Size(res)==1){
    // in case there is only an object
    PyObject *tmp = PyTuple_GetItem(res, 0);
    Py_INCREF(tmp);
    Py_DECREF(res);
    return tmp;
  }
  return res; // res is a new reference and will be owned by the caller
}

static PyObject *DimRpc_setData (DimRpc_Object* self, PyObject* args)  {
  /** Gets some Python objects and converts them to the appropiate C++ values.
   * The conversion is done based on the arguments supplied when the RPC
   * command was created.
   */
  if ( !DimRpc_verify_self(self,7) )   {
    return nullptr;
  }
  char *buff = nullptr;
  unsigned int buff_size=0;
  if (iterator_to_allocated_buffer(args, self->format_out, &buff, &buff_size) ) {
    self->cpp_dimRpc->setData(buff, buff_size);
    delete [] buff;
  }
  else {
    PyErr_SetString(PyExc_AttributeError,
		    "Could not convert arguments to C buffer");
    return nullptr;
  }
  Py_RETURN_NONE;
}

static PyObject *DimRpc_rpcHandler (DimRpc_Object* /* self */) {
  print("RPC call received in C++\n");
  Py_RETURN_NONE;
}

static PyMethodDef DimRpc_methods[] = {
  {"name"      , func_ptr(DimRpc_name).pyc       , METH_NOARGS,
   "Returns the name of the service."                        },
  {"getData"   , func_ptr(DimRpc_getData).pyc    , METH_NOARGS,
   "Returns received integer data as a Python int"           },
  {"getInt"    , func_ptr(DimRpc_getInt).pyc     , METH_NOARGS,
   "Returns received integer data as a Python int"           },
  {"getDouble" , func_ptr(DimRpc_getDouble).pyc  , METH_NOARGS,
   "Returns received double data as Python float"            },
  {"getFloat"  , func_ptr(DimRpc_getFloat).pyc   , METH_NOARGS,
   "Returns received float data as a Python float"           },
  {"getString" , func_ptr(DimRpc_getString).pyc  , METH_NOARGS,
   "Returns received string data as a Python string"         },
  {"getSize"   , func_ptr(DimRpc_getSize).pyc    , METH_NOARGS,
   "Returns the total received data size as a Python int"    },
  {"setData"   , func_ptr(DimRpc_setData).pyc    , METH_VARARGS,
   "Sets results data according to the initial format string"},
  {"rpcHandler", func_ptr(DimRpc_rpcHandler).pyc , METH_NOARGS,
   "Dummy function for the rpcHandler part"                  },
  {nullptr, nullptr, 0, nullptr}  /* Sentinel */
};

static PyMethodDef dimcpp_methods[] = {
  {nullptr, nullptr, 0, nullptr}  /* Sentinel */
};

// disable some warnings such that we can use the recommended
// to define a new Python type see
// https://docs.python.org/3.8/extending/newtypes_tutorial.html#the-basics
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmissing-field-initializers"
#pragma GCC diagnostic ignored "-Wpedantic"
#if defined(__clang__) && __clang_major__ >= 10
#pragma clang diagnostic ignored "-Wc99-designator"
#endif

static PyTypeObject _make_DimRpc_Type()   {
  PyTypeObject type { PyVarObject_HEAD_INIT(NULL,0) };
  type.tp_name      = "dim.DimRpc";
  type.tp_basicsize = sizeof(DimRpc_Object);
  type.tp_dealloc   = (destructor)DimRpc_dealloc;
  type.tp_flags     = Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE;
  type.tp_doc       = "DimRpc object";
  type.tp_methods   = DimRpc_methods;
  type.tp_init      = (initproc)DimRpc_init;
  type.tp_new       = DimRpc_new;
  type.tp_free      = _PyObject_Del;
  type.tp_del       = (destructor)DimRpc_dealloc;
#if PY_MAJOR_VERSION >= 3
  type.tp_finalize  = (destructor)DimRpc_dealloc;
#endif
  return type;
}

static PyTypeObject DimRpc_Type = _make_DimRpc_Type();
#pragma GCC diagnostic pop

static void DimRpcInfo_dealloc(DimRpcInfo_Object *self) {
  /** Dealocates a DimRpcInfo objet */
  self->magic = 0UL;
  delete self->cpp_dimRpcInfo;
  Py_XDECREF(self->nolink);
  PyObj_TYPE(self)->tp_free(self);
}

static int DimRpcInfo_init(DimRpcInfo_Object* self, PyObject* args, PyObject* kwds)  {
  /** Allocates a new DimRpcInfo_Object and a PyDimRpcInfoWrapper inside it.
   */
  char *name = nullptr, *format_in = nullptr, *format_out = nullptr;
  PyObject *arg1 = nullptr, *arg2 = nullptr;
  static const char *kwlist[] = {"name", "format_in", "format_out", "timeout", "nolink", nullptr};

  if (!PyArg_ParseTupleAndKeywords(args, kwds, "sssO|O", (char**)kwlist,
                                   &name,
                                   &format_in,
                                   &format_out,
				   &arg1, &arg2)
     )
  {
    print("Invalid arguments for RPC Info %s", name);
    return -1;
  }
  if ((arg2 && !PyInt_Check(arg1)) || (!arg1))  {
    /* we have an unknown object as argument arg1 */
    print ("Invalid arguments");
    return -1;
  }
  /* It is useless to perform a back and forth conversion for the default
   * failure arguments
   */
  if (arg2 && PyInt_Check(arg1)) {
    /* this means we have a time argument */
    Py_INCREF(arg2);
    //print("Created new DimRpcInfo proxy %s with timeout:%ld", name, PyInt_AsLong(arg1));
    self->cpp_dimRpcInfo = new PyDimRpcInfoWrapper(name, PyInt_AsLong(arg1), nullptr, 0);
    self->nolink = arg2;
  }
  else {
    /* we don't have a time argument and arg1 is the failure param */
    Py_INCREF(arg1);
    //print("Created new DimRpcInfo proxy %s without timeout\n", name);
    self->cpp_dimRpcInfo = new PyDimRpcInfoWrapper(name, nullptr, 0);
    self->nolink = arg1;
  }
  // Need to pad. See pydim_utils.cpp line 403
  if ( PyUnicode_Check(self->nolink) )  {
    auto* ptr = PyUnicode_FromFormat(" %V ", self->nolink);
    Py_DECREF(self->nolink);
    self->nolink = ptr;
    Py_INCREF(ptr);
  }
  self->cpp_dimRpcInfo->setPyRef(self);
  self->magic = PYDIMRPC_MAGIC;
  if (self->cpp_dimRpcInfo) {
    self->format_in = new char[strlen(format_in)+1];
    self->format_out = new char[strlen(format_out)+1];
    strcpy(self->format_in, format_in);
    strcpy(self->format_out, format_out);
    return 0;
  }
  return -1;
}

static PyObject *DimRpcInfo_new(PyTypeObject* type, PyObject* /* args */, PyObject* /* kwds */) {
  /// Allocates a new DimRpcInfo_Object and initialises the cpp_dimRpcInfo to NULL
  DimRpcInfo_Object *self = (DimRpcInfo_Object*)type->tp_alloc(type, 0);
  if (self != nullptr) {
    self->cpp_dimRpcInfo = nullptr;
    self->format_in = nullptr;
    self->format_out = nullptr;
    self->nolink = nullptr;
  }
  /// in case of errors self is NULL and the error string is already set
  return (PyObject *)self;
}

static PyObject* DimRpcInfo_name (DimRpcInfo_Object * self)  {
  if ( !DimRpcInfo_verify_self(self,7) )   {
    return nullptr;
  }
  // res is a new reference and will be owned by the caller
  return PyString_FromString(self->cpp_dimRpcInfo->getName());
}

static PyObject *DimRpcInfo_getInt (DimRpcInfo_Object * self)  {
  int *cpp_res = nullptr, size = 0;

  if ( !DimRpcInfo_verify_self(self,7) )   {
    return nullptr;
  }
  /* Making sure we don't block the other Python threads
   * All calls to the DIM API are potentialy blocking
   */
  Py_BEGIN_ALLOW_THREADS
  size = self->cpp_dimRpcInfo->getSize();
  Py_END_ALLOW_THREADS
  if (!size) {
      /* this means the RPC failed and we must provide the
       * default failure parameter
       */
      return self->nolink;
  }
  Py_BEGIN_ALLOW_THREADS
  cpp_res = (int*)self->cpp_dimRpcInfo->getData();
  Py_END_ALLOW_THREADS
  PyObject *res = PyInt_FromLong( (long)*cpp_res );
  return res; /* res is a new reference and will be owned by the caller */
}

static PyObject *DimRpcInfo_getFloat (DimRpcInfo_Object * self)  {
  float *cpp_res = nullptr;
  int size = 0;

  if ( !DimRpcInfo_verify_self(self,7) )   {
    return nullptr;
  }
  /* Making sure we don't block the other Python threads
   * All calls to the DIM API are potentialy blocking
   */
  Py_BEGIN_ALLOW_THREADS
  size = self->cpp_dimRpcInfo->getSize();
  Py_END_ALLOW_THREADS
  if (!size) {
    /* this means the RPC failed and we must provide the
     * default failure parameter
     */
    return self->nolink;
  }
  Py_BEGIN_ALLOW_THREADS
  cpp_res = (float*)self->cpp_dimRpcInfo->getData();
  Py_END_ALLOW_THREADS
  PyObject *res = PyFloat_FromDouble( (double)*cpp_res );
  return res; /* res is a new reference and will be owned by the caller */
}

static PyObject *DimRpcInfo_getDouble (DimRpcInfo_Object * self) {
  double *cpp_res = nullptr;
  int size = 0;

  if ( !DimRpcInfo_verify_self(self,7) )   {
    return nullptr;
  }
  /* Making sure we don't block the other Python threads
   * All calls to the DIM API are potentialy blocking
   */
  Py_BEGIN_ALLOW_THREADS
  size = self->cpp_dimRpcInfo->getSize();
  Py_END_ALLOW_THREADS
  if (!size) {
    /* this means the RPC failed and we must provide the
     * default failure parameter
     */
    return self->nolink;
  }
  Py_BEGIN_ALLOW_THREADS
  cpp_res = (double*)self->cpp_dimRpcInfo->getData();
  Py_END_ALLOW_THREADS
  PyObject *res = PyFloat_FromDouble(*cpp_res);
  return res; /* res is a new reference and will be owned by the caller */
}

static PyObject *DimRpcInfo_getString (DimRpcInfo_Object * self) {
  char * cpp_res = nullptr;
  int size = 0;

  if ( !DimRpcInfo_verify_self(self,7) )   {
    return nullptr;
  }
  /* Making sure we don't block the other Python threads.
   * All calls to the DIM API are potentialy blocking!
   */
  Py_BEGIN_ALLOW_THREADS
  size = self->cpp_dimRpcInfo->getSize();
  Py_END_ALLOW_THREADS
  if (!size) {
    /* this means the RPC failed and we must provide the
     * default failure parameter
     */
    return  self->nolink;
  }
  Py_BEGIN_ALLOW_THREADS
  cpp_res = self->cpp_dimRpcInfo->getString();
  Py_END_ALLOW_THREADS
  PyObject *res = PyString_FromString(cpp_res);
  return res; // res is a new reference and will be owned by the caller
}

static PyObject *DimRpcInfo_getSize (DimRpcInfo_Object * self)  {
  /** From the Python point of view, calling this procedure is useless.
   * Proving it for the sake of completion
   */
  int cpp_res = 0;
  if ( !DimRpcInfo_verify_self(self,7) )   {
    return nullptr;
  }
  /* Making sure we don't block the other Python threads */
  Py_BEGIN_ALLOW_THREADS
  cpp_res = self->cpp_dimRpcInfo->getSize();
  Py_END_ALLOW_THREADS
  PyObject *res = PyInt_FromLong(cpp_res);
  return res; // res is a new reference and will be owned by the caller
}

static PyObject *DimRpcInfo_getData(DimRpcInfo_Object * self)   {
  char *buff = nullptr;
  int buff_size=0;

  if ( !DimRpcInfo_verify_self(self,7) )   {
    return nullptr;
  }
  /* Making sure we don't block the other Python threads */
  Py_BEGIN_ALLOW_THREADS
  buff = (char*)self->cpp_dimRpcInfo->getData();
  buff_size = self->cpp_dimRpcInfo->getSize();
  Py_END_ALLOW_THREADS
  if (!buff || !buff_size)   {
    /* The callback has failed and we provide the error object */
    Py_INCREF(self->nolink);  // Is this necessary ?
    return self->nolink;
  }
  PyObject *res = dim_buf_to_tuple(self->format_out, buff, buff_size);
  /* In case of a single object tuple, return the object not the tuple */
  if (res && PyTuple_Size(res)==1) {
    // in case there is only an object
    PyObject *tmp = PyTuple_GetItem(res, 0);
    Py_INCREF(tmp);
    Py_DECREF(res);
    return tmp;
  }
  return res; // res is a new reference and will be owned by the caller
}

static PyObject* DimRpcInfo_setData (DimRpcInfo_Object* self, PyObject* args)  {
  /** Gets some Python objects and converts them to the appropiate C++ values.
   * The conversion is done based on the arguments supplied when the RPC
   * command was created.
   */
  char *buff = nullptr;
  unsigned int buff_size=0;

  if ( !DimRpcInfo_verify_self(self,7) )   {
    return nullptr;
  }
  //printPyObject(args);
  if ( iterator_to_allocated_buffer(args, self->format_in,
				    (char**)&buff, &buff_size) ) {
    /* The setData() method of DimInfo is blocking. This creates a
     * deadlock between the calling thread that holds the python
     * global interpretor lock and the DIM global lock
     */
    Py_BEGIN_ALLOW_THREADS
    self->cpp_dimRpcInfo->setData(buff, buff_size);
    Py_END_ALLOW_THREADS
      delete [] buff;
  } else {
    PyErr_SetString(PyExc_AttributeError,
		    "Could not convert arguments to C buffer");
    return nullptr;
  }
  Py_RETURN_NONE;
}


static PyObject *
DimRpcInfo_rpcInfoHandler (DimRpcInfo_Object* /* self */) {
  /** Dummy method for the python DimRpcInfo class.
   * Not really needed, provided just to make all the DimRpcInfo class
   * methods visible in Python
   */
  //print("RPC call received in C++\n");
  Py_RETURN_NONE;
}

static PyMethodDef DimRpcInfo_methods[] = {
  {"name"      , func_ptr(DimRpcInfo_name).pyc       , METH_NOARGS,
   "Returns the name of the service."                        },
  {"getData"   , func_ptr(DimRpcInfo_getData).pyc    , METH_NOARGS,
   "Returns the received C complex data as a Python objects"           },
  {"getInt"    , func_ptr(DimRpcInfo_getInt).pyc     , METH_NOARGS,
   "Returns the received C integer data as a Python int"           },
  {"getDouble" , func_ptr(DimRpcInfo_getDouble).pyc  , METH_NOARGS,
   "Returns the received C double data as a Python float"            },
  {"getFloat"  , func_ptr(DimRpcInfo_getFloat).pyc   , METH_NOARGS,
   "Returns the received C float data as a Python float"           },
  {"getString" , func_ptr(DimRpcInfo_getString).pyc  , METH_NOARGS,
   "Returns the received C string data as a Python string"         },
  {"getSize"   , func_ptr(DimRpcInfo_getSize).pyc    , METH_NOARGS,
   "Returns the total received C data size as a Python int"    },
  {"setData"   , func_ptr(DimRpcInfo_setData).pyc    , METH_VARARGS,
   "Sets results data according to the initial format string"},
  {"rpcInfoHandler", func_ptr(DimRpcInfo_rpcInfoHandler).pyc , METH_NOARGS,
   "Dummy function for the rpcHandler part"                  },
  {nullptr, nullptr, 0, nullptr}  /* Sentinel */
};

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmissing-field-initializers"
#pragma GCC diagnostic ignored "-Wpedantic"
#if defined(__clang__) && __clang_major__ >= 10
#pragma clang diagnostic ignored "-Wc99-designator"
#endif

static PyTypeObject _make_DimRpcInfo_Type()   {
  PyTypeObject type { PyVarObject_HEAD_INIT(nullptr,0) };
  type.tp_name      = "dim.DimRpcInfo";
  type.tp_basicsize = sizeof( DimRpcInfo_Object );
  type.tp_dealloc   = (destructor)DimRpcInfo_dealloc;
  type.tp_flags     = Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE;
  type.tp_doc       = "DimRpcInfo object";
  type.tp_methods   = DimRpcInfo_methods;
  type.tp_init      = (initproc)DimRpcInfo_init;
  type.tp_new       = DimRpcInfo_new;
  type.tp_free      = _PyObject_Del;
  type.tp_del       = (destructor)DimRpcInfo_dealloc;
#if PY_MAJOR_VERSION >= 3
  type.tp_finalize = (destructor)DimRpc_dealloc; /* tp_finalize */
#endif
  return type;
}

static PyTypeObject DimRpcInfo_Type = _make_DimRpcInfo_Type();
#pragma GCC diagnostic pop

#if PY_MAJOR_VERSION >= 3
static struct PyModuleDef dimcpp_module = {
    PyModuleDef_HEAD_INIT,
    "dimcpp",                 /* name of module */
    "DIM C++ methods",        /* module documentation, may be NULL */
    -1,                       /* size of per-interpreter state of the module,
                                 or -1 if the module keeps state in global variables. */
    dimcpp_methods,
    nullptr,                  /* m_reload */
    nullptr,                  /* m_traverse */
    nullptr,                  /* m_clear */
    nullptr                   /* m_free */
};
#define PyMODINIT_RETURN(x)   return x
#else
#define PyMODINIT_RETURN(x)   return
#endif

#ifndef PyMODINIT_FUNC  /* declarations for DLL import/export */
#if PY_MAJOR_VERSION >= 3
#define PyMODINIT_FUNC PyObject*
#else
#define PyMODINIT_FUNC void
#endif
#endif

PyMODINIT_FUNC
#if PY_MAJOR_VERSION >= 3
PyInit_dimcpp()
#else
initdimcpp()
#endif
{
  /* This is needed if the program is threaded.
   * Creates the global interpretor lock
   */
#if PY_MAJOR_VERSION <=2 || (PY_MAJOR_VERSION == 3 && PY_MINOR_VERSION < 7)
  PyEval_InitThreads();
#endif
  /// Initialize python module
  PyObject *module = nullptr;
  /* Initializing Python classes.
   * Basically this functions will make the necessary initializations
   * to the _Type objects.
   */
  if (PyType_Ready(&DimRpc_Type) < 0) {
    ::print("Could not initialize type DimRpc\n");
    PyMODINIT_RETURN(module);
  }
  if (PyType_Ready(&DimRpcInfo_Type) < 0) {
    ::printf("Could not initialize type DimRpcInfo\n");
    PyMODINIT_RETURN(module);
  }

  /* Initializing Python module */
#if PY_MAJOR_VERSION >= 3
  module = ::PyModule_Create(&dimcpp_module);
#else
  module = Py_InitModule3("dimcpp", dimcpp_methods, "DIM C++ methods");
#endif
  if (module == nullptr) {
    ::printf("Could not initialise dimcpp module\n");
    PyMODINIT_RETURN(module);
  }
  /* Making sure that the new class objects will not by cleaned out by the
   * garbage collector
   */
  PyObject *rpc_type = (PyObject *)&DimRpc_Type;
  Py_INCREF(rpc_type);
  PyObject *info_type = (PyObject *)&DimRpcInfo_Type;
  Py_INCREF(info_type);
  /* Adding the objects to the created module */
  PyModule_AddObject(module, "DimRpc", rpc_type);
  PyModule_AddObject(module, "DimRpcInfo", info_type);
  /* Magic call to DIM API. Makes sure all the data type sizes are respected.*/
  dic_disable_padding();

  PyMODINIT_RETURN(module);
}
