#!/bin/env python

"""
An example for showing how a client can run the commands on a DIM server. 

"""
from __future__ import print_function

from builtins import input
from builtins import range
import sys
import math
import time
import argparse

# Import the pydim module
import pydim

global num_call
num_call = 0

def command1(args):
    global num_call
    num_call = num_call + 1
    f = [math.pi, math.e, 42][num_call%3]
    #The argument must be a tuple
    call_args = (f,)
    print("Calling command 1. Arguments: %s" % call_args)
    res = pydim.dic_cmnd_service(args.name+"/example-command-1", call_args, "F")

def command2(args):
    global num_call
    num_call = num_call + 1
    text = ["hola", "hi", "bonjour"][num_call%3]
    call_args = (num_call, text)
    print("Calling command 2. Arguments: %s, %s" % call_args)
    res = pydim.dic_cmnd_service(args.name+"/example-command-2", call_args, "I:1;C")

def command_exit(server):
    print("Calling command "+server+"/EXIT.")
    res = pydim.dic_cmnd_service(server+"/EXIT", (0,), "I")

def wait_answer(curr_count):
    global num_call
    start = time.time()
    while curr_count+1 < num_call:
      time.sleep(0.1)
      diff = time.time() - start
      if diff > 20.0:
        raise TimeoutError(f'Answer from dim server missing after {diff} seconds.')

def help():
    print("""This is a DIM client for the commands example server.

The following options are available:
    
    1     Run command 1
    2     Run command 2
    H     Display this help text
    E     Tell server to exit event loop
    Q     Exit this program

    """)

def main():
    global num_call

    parser = argparse.ArgumentParser(allow_abbrev=False)
    parser.add_argument(
        '--name',
        type=str,
        default='Server',
        help='Server name',
    )
    parser.add_argument(
        "--wait",
        type=int,
        default=-1,
        help='Wait before starting loop',
    )
    args = parser.parse_args()

    if args.wait > 0:
      print('Wait for %d seconds before starting loop'%(args.wait,))
      time.sleep(args.wait)

    # check if a Dim DNS node has been configured.
    if not pydim.dis_get_dns_node():
        print("No Dim DNS node found. Please set the environment variable DIM_DNS_NODE")

    help()

    exit = False
    while not exit:
        
        action = input("Action (Press 'Q' to exit): ")

        curr_call_count = num_call
        if action == "1":
            command1(args)
        elif action == "2":
            command2(args)
        elif action == "E":
            command_exit(args.name)
        elif action == "D":
            command_exit('DIM_DNS')
        elif action == "Q":
            exit = True
            print("Bye!")
        elif action == "H":
            help()

        wait_answer(curr_call_count)

if __name__ == "__main__":
    main()
