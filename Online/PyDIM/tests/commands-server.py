#!/bin/env python

"""
An example of a DIM server with commands.

"""
from __future__ import print_function

import sys
import time
import argparse

# Import the pydim module
import pydim

def command_callback1(value, tag):
    """
    value: A tuple containing a float
    tag: A context argument (possibly empty)
    
    """
    print("command_callback1 called. Argument: %s %d" % (value[0], tag))
    sys.stdout.flush()
    
def command_callback2(cmd, tag):
    """
    
    cmd: A tuple containing an integer and a string of variable length
    tag: A context argument (possibly empty) 
    """
    print("command_callback2 called. Arguments: %s %s and tag%s" % (cmd[0], cmd[1][:-1], tag))
    sys.stdout.flush()

def main():
    parser = argparse.ArgumentParser(allow_abbrev=False)
    parser.add_argument(
        '--wait',
        type=int,
        default=-1,
        help='Wait before starting loop',
    )
    parser.add_argument(
        '--name',
        type=str,
        default='Server',
        help='Server name',
    )

    args = parser.parse_args()
    if args.wait > 0:
      print('Wait for %d seconds before starting loop'%(args.wait,))
      time.sleep(args.wait)

    cmd1 = pydim.dis_add_cmnd(args.name+'/example-command-1', 'F', command_callback1, 2)
    cmd2 = pydim.dis_add_cmnd(args.name+'/example-command-2', 'I:1;C', command_callback2, 3) 

    if not cmd1 or not cmd2:
      print("An error occurred while registering the commands")
      sys.exit(1)

    pydim.dis_start_serving(args.name)
    print("Starting the server")
    sys.stdout.flush()
    while True:
        time.sleep(1)

if __name__ == "__main__":
    main()
