#!/usr/bin/env python

"""
  An example for showing clients for DIM servers.

  The client will query the values from the two services created in
  `service-server.py`, thus it should be run along with that script.

"""
from __future__ import print_function

import sys
import time
import argparse

# Import the pydim module
import pydim

global stop_execution
stop_execution = False

def client_callback1(tag, *vals):
    """
    Callback function for the service 1.
    
    Callback functions receive as many arguments as values are returned by the
    service. For example, as the service 1 returns only one string this callback
    function has only one argument. 
    
    """
    global stop_execution
    data = ''
    for v in vals:
        data = data + str(v)[:-1] + ' [' + str(type(v)) + ']  '
    sys.stderr.write("Client callback function for service 1: %d arguments, tag: %X, data: %s \n"%(len(vals), tag, data, ))
    if type(vals[0]) == type(str()) and vals[0][:-1] == 'stop':
        sys.stderr.write('Stop triggered!\n')
        stop_execution = True

def client_callback2(tag, *vals):
    """
    Callback function for service 2.
    
    As the service 2 returned two arguments
    """
    data = ''
    for v in vals:
        data = data + str(v) + ' [' + str(type(v)) + ']  '
    sys.stderr.write("Client callback function for service 2. %d arguments, tag: %X, data: %s \n"%(len(vals), tag, data, ))

def main():
    """
    A client for subscribing to two DIM services
    """
    global stop_execution
    
    parser = argparse.ArgumentParser(allow_abbrev=False)
    parser.add_argument(
        "--wait",
        type=int,
        default=-1,
        help='Wait before starting loop',
    )
    parser.add_argument(
        "--name",
        type=str,
        default="Server",
        help='Server name',
    )

    args = parser.parse_args()
    if args.wait > 0:
      print('Wait for %d seconds before starting loop'%(args.wait,))
      time.sleep(args.wait)

    # Again, check if a Dim DNS node has been configured.
    # Normally this is done by setting an environment variable named DIM_DNS_NODE
    # with the host name, e.g. 'localhost'.
    #
    if not pydim.dim_get_dns_node():
        print("No Dim DNS node found. Please set the environment variable DIM_DNS_NODE")
        sys.exit(1)

    print('DIM_DNS  host:%s'%(pydim.dim_get_dns_node(),))
    print('DIM_DNS  port:%d'%(pydim.dim_get_dns_port(),))
    
    # The function `dic_info_service` allows to subscribe to a service.
    # The arguments are the following:
    # 1. The name of the service.
    # 2. Service description string
    # 3. Callback function that will be called when the service is 
    #    updated.
    res1 = pydim.dic_info_service(args.name+"/example-service-1", "C", client_callback1, pydim.MONITORED, 0, 0xfeed, '???')
    res2 = pydim.dic_info_service(args.name+"/example-service-2", "F:1;I:1;", client_callback2, pydim.MONITORED, 0, 0xcafe, None)
    
    if not res1 or not res2:
        print("There was an error registering the clients")
        sys.exit(1)
    
    # Wait for updates
    while not stop_execution:
        time.sleep(0.5)
        sys.stdout.flush()

if __name__=="__main__":
    main()
