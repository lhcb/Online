#!/usr/bin/python
#############################################################################
# Python script to test the PyDimRpc and PyDimRpc classes
#############################################################################

import time, threading
from pydim import DimRpc, DimRpcInfo, dis_start_serving
from pydim.debug import *

e = threading.Event()
## 
# @addtogroup examples
# @{
# 
#############################################################################
# Implement some test classes functionality
#############################################################################
class PyRpc (DimRpc):
    def __init__(self):
         DimRpc.__init__(self, 'testRpc', 'I', 'I')
         self.counter = 0

    def rpcHandler(self):
      try:
         SAY('Server side funciton called. Getting parameters...')
         i = self.getInt()
         SAY("getInt - received: ", i)
         i = self.getData()
         SAY("getData - received: ", i)
         self.counter += i
         SAY('Setting response data ', self.counter+1)
         self.setData(self.counter+1) 
      except Exception as X:
        import traceback
        formatted_lines = traceback.format_exc().splitlines()
        for l in formatted_lines:
           SAY('++ %s'%(l, ))


class PyRpcInfo(DimRpcInfo):
    def __init__(self, count):
        DimRpcInfo.__init__(self, 'testRpc', 'I', 'I', 30, None)
        self.count = count
        self.value = 1

    def rpcHandler(self):
      try:
        SAY("PyRpcInfo: Non blocking RPC call executed")
        i = self.getInt()
        SAY("PyRpcInfo: Received value %d", i)
      except Exception as X:
        import traceback
        formatted_lines = traceback.format_exc().splitlines()
        for l in formatted_lines:
           SAY('++ %s'%(l, ))
       
        
    def run(self):
        DEBUG("Calling thread started")
        cnt = self.count
        if cnt < 0:
            cnt = 10000000
        while not e.is_set() and cnt>0:
            cnt = cnt - 1
            SAY('Calling server function') 
            self.setData(self.value)
            if self.value % 2:
                # blocking   
                SAY("Getting return value as int: ", self.getInt())
            else:
                # non blocking
                pass
            time.sleep(1)
##
# @}
#

#############################################################################
# Execute tests
#############################################################################

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(allow_abbrev=False)
    parser.add_argument(
      '--counter',
      type=int,
      default=-1,
      help='Loop counter for test',
    )
    args = parser.parse_args()
    DEBUG("Creating PyDimRpc ...")
    myrpc = PyRpc()
    dis_start_serving()
    time.sleep(2)

    DEBUG("Creating PyDimRpcInfo ...")
    myrpcCaller = PyRpcInfo(args.counter)

    DEBUG("Starting DIM ...")
    time.sleep(3)
    t = threading.Thread(target = myrpcCaller.run)
    t.start()
    t.join()
    time.sleep(1)
