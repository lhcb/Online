#!/usr/bin/python
#############################################################################
# Python script to test the PyDimRpc and PyDimRpc classes
#############################################################################

from __future__ import print_function
from builtins import str
from builtins import range
import time, unittest, random
from threading import *
from pydim import *
from pydim.debug import *

RPC_TIMEOUT=30

##
# @addtogroup examples
# @{
#
class PyRpcInt (DimRpc):
    def __init__(self):
         DimRpc.__init__(self, 'testRpc1', 'I', 'I')
         self.counter = 0

    def rpcHandler(self):
         SAY('Getting value')
         i = self.getInt()
         SAY("getInt - received: ", i)
         i = self.getData()
         SAY("getData - received: ", i)
         self.counter += i
         SAY('Setting response data %08X'%(self.counter+1,))
         self.setData(self.counter+1) 


class PyRpcIntInfo(DimRpcInfo):
    def __init__(self):
        DimRpcInfo.__init__(self, 'testRpc1', 'I', 'I', timeout=RPC_TIMEOUT, nolink=-1)
        self.value = -1
        self.count = 10

    def rpcInfoHandler(self):
        SAY("Getting value")
        i = self.getInt()
        SAY("Received value ", i)
        self.value = i
        
# ---------------------------------------------------------------------------
class PyRpcString (DimRpc):
    def __init__(self, name='testRpc2'):
         DimRpc.__init__(self, name, 'C', 'C')
         self.counter = 0

    def rpcHandler(self):
         s = self.getData()[:-1]
         SAY("getData - received: ", s)
         l = [x for x in s]
         l.sort()
         s = ""
         for x in l: s += str(x)
         SAY('Setting response data ', s)
         self.setData(s)

class PyRpcStringInfo(DimRpcInfo):
    def __init__(self, name='testRpc2'):
        DimRpcInfo.__init__(self, name, 'C', 'C', timeout=RPC_TIMEOUT, nolink='Error')
        self.value = 'Error'

    def rpcInfoHandler(self):
        s = self.getData()[:-1]
        SAY("PyRpcStringInfo: Received value '"+s+"'")
        self.value = s

# ---------------------------------------------------------------------------
class PyRpcComposite (DimRpc):
    def __init__(self):
        self.format_in = 'I:1;F:2;D:2;X:1;C'
        self.format_out = 'C:10;I'
        DimRpc.__init__(self, 'testRpc3', self.format_in, self.format_out)
        self.counter = 0

    def rpcHandler(self):
         s = self.getData()
         SAY("getData - received: ", str(s))         
         tup = ("It's OK123", 1, 2, 3, 4, 5, 6, 7, 8, 9)
         SAY('Setting response data: ', tup)
         self.setData("It's OK123", 1, 2, 3, 4, 5, 6, 7, 8, 9)
         #self.setData(tup)

class PyRpcCompositeInfo(DimRpcInfo):
    def __init__(self):
        self.format_in = 'I:1;F:2;D:2;X:1;C'
        self.format_out = 'C:10;I'
        DimRpcInfo.__init__(self, 'testRpc3', self.format_in, self.format_out, timeout=RPC_TIMEOUT, nolink=None)
        self.value = None 

    def rpcInfoHandler(self):
        v = self.getData()
        SAY("Received value ", str(v))
        self.value = v

#############################################################################
# Implements test classes functionality
#############################################################################
myRpcInt = PyRpcInt()
myRpcIntCaller = PyRpcIntInfo()
myRpcString = PyRpcString()
myRpcStringCaller = PyRpcStringInfo()
myRpcComposite = PyRpcComposite()
myRpcCompositeCaller = PyRpcCompositeInfo()
#myRpcString = PyRpcString()
myRpcFailCaller = PyRpcStringInfo('testRcp4')
SAY("Starting DIM")
dic_disable_padding()
dis_start_serving()
dic_disable_padding()
time.sleep(1)

def header(msg):
    print('\n')
    print('+' + 80*'-')
    print('|  ' + msg)
    print('+' + 80*'-')

class TestDimRpc(unittest.TestCase):

    def test_RpcInteger(self):
        header('testRpcString: Running RPC int test')
        time.sleep(1)
        count = 1
        res = 0
        for i in range(1,10):
            count += i
            myRpcIntCaller.setData(i)
            time.sleep(0.2)
            res = myRpcIntCaller.getData()
            self.assertEqual(count, res) 

    def test_RpcString(self):
        header('testRpcString: Running RPC string test')
        time.sleep(1) # dim threads are not initialized imediately unfortunately 
        r = list(range(1,10))
        s1 = ""
        s2 = ""
        for x in r: s1 += str(x)
        for x in r: s2 = str(x) + s2
        myRpcStringCaller.setData(s2)
        time.sleep(2)
        res1 = myRpcStringCaller.getData()[:-1]
        res2 = myRpcStringCaller.value  
        SAY(f's1:"{s1}"  res1:"{res1}"')
        SAY(f's2:"{s2}"  res2:"{res2}"')
        self.assertEqual(s1, res1)  # See pydim_utils.cpp line 404 and 411
        self.assertEqual(s1, res2)  # to correct for trailing \0

    def test_RpcComposite(self):
        header('testRpcComposite: Running RPC test using composite parameters')
        tup = (1, 2.1, 2.2, 3.1, 3.2, 1234567890123, "Test string")
        SAY("Setting data", tup)
        myRpcCompositeCaller.setData(1, 2.1, 2.2, 3.1, 3.2, 1234567890123, "Test string")
        time.sleep(2)
        res2 = myRpcCompositeCaller.value
        self.assertEqual(tup, res2)
        res1 = myRpcCompositeCaller.getData() 
        tup=("It's OK123", 1, 2, 3, 4, 5, 6, 7, 8, 9)
        self.assertEqual(tup, res1)

    def test_RpcFailure(self):
        header('testRpcFailure: Running RPC test using strings')
        myRpcFailCaller.setData('ALA BALA PORTOCALA')
        time.sleep(2)
        res1 = myRpcFailCaller.value
        SAY('value: %s'%(str(myRpcFailCaller.value),))
        SAY('res1:  **%s**'%(str(res1),))
        self.assertEqual('Error', res1)
        #res2 = myRpcFailCaller.getData()
        #SAY('value: %s'%(str(myRpcFailCaller.getData()),))
        #SAY('res2:  **%s**'%(str(res2),))
        #self.assertEqual(res1, res2[1:-1])

#############################################################################
# Execute tests
#############################################################################
if __name__ == '__main__':
    unittest.main()
