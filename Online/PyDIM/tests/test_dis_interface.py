from __future__ import print_function
from builtins import object
import sys
import pydim
import pydim.debug as debug
from   pydim.debug import SAY, ERROR, DEBUG
from   time import sleep

debug._DEBUG = 1


global counter
counter = 1

def dummy(*args):
    print('I am an unbound dummy func', args)

def dis_service1 (tag):
    print('function service1 called')
    global counter
    return (counter, counter+1, 10.0, 10.0 , 'CUCU', 'A')

class Struct(object):
    def dummy(self, *args):
        print('function servce2 called')
        print("I am dummy func bound to Struct. Received", args)

    def service2(self, tag):
        global counter
        counter += 1
        return (float(counter),)

def test_dis_interface():
    s = Struct()
    print(80*'-')
    x = pydim.dis_get_dns_node()
    pydim.dis_set_dns_node('tralala')
    if not pydim.dis_get_dns_node() == 'tralala':
        ERORR('get/set dns failed')
        sys.exit(1)
    pydim.dis_set_dns_node(x)
    print('dis_get/dns_node functions tested')

    print(80*'-')
    x = pydim.dim_get_dns_port()
    pydim.dim_set_dns_port(12345)
    if not pydim.dim_get_dns_port() == 12345:
        ERROR('get/set dns port failed. Received ports',x,pydim.dim_get_dns_port() )
    pydim.dim_set_dns_port(x)
    print('dis_get/dns_port functions tested')
    
    print('pydim: calling dis_add_exit_handler(func)', pydim.dis_add_exit_handler(dummy))
    print('pydim: calling dis_add_exit_handler(callable)', pydim.dis_add_exit_handler(s.dummy))

    print('pydim: calling dis_add_client_exit_handler(func)', pydim.dis_add_client_exit_handler(dummy))
    print('pydim: calling dis_add_client_exit_handler(callable)', pydim.dis_add_client_exit_handler(s.dummy))

    print('pydim: calling dis_start_serving()', pydim.dis_start_serving())
    #print('pydim: calling dis_stop_serving()', pydim.dis_stop_serving())
    print('pydim: calling dis_start_serving()', pydim.dis_start_serving())

    id1 = pydim.dis_add_service('blabla', 'I:2;D:2;C:4;C', dis_service1, 1)
    print("pydim: pydim.dis_add_service('blabla', 'I:2;D:2;C:4;C', dis_service1, 1)", id1)
    
    print('dis_update_service', pydim.dis_update_service(id1))
    print('dis_selective_update_service: ', pydim.dis_selective_update_service(id1,(1,2,3)))
    print('dis_selective_update_service: ', pydim.dis_selective_update_service(id1,[1,2,3]))

    print('dis_set_quality:              ', pydim.dis_set_quality(1, 10))
    print('dis_set_timestamp:            ', pydim.dis_set_timestamp(1,1,1))
    print('dis_remove_service:           ',pydim.dis_remove_service(1))
    #
    # print('dis_get_next_cmnd:          ',pydim.dis_get_next_cmnd(10))
    print('dis_get_client:               ', pydim.dis_get_client('fdas'))
    print('dis_get_conn_id:              ', pydim.dis_get_conn_id())
    print('dis_get_timeout:              ', pydim.dis_get_timeout(1, 1))
    print('dis_get_client_services:      ', pydim.dis_get_client_services(10))
    print('dis_set_client_exit_handler:  ', pydim.dis_set_client_exit_handler(1,1))
    print('dis_get_error_services:       ', pydim.dis_get_error_services())

    i=1
    sleep(1)
    print("Calling pydim.dis_add_cmnd('test1', 'C:20', dummy, 1)")
    pydim.dis_add_cmnd('test1', 'C:20', dummy, 1)
    print("Calling pydim.dis_add_cmnd('test2', 'F:2;D:2;I:3;S:1;C:5;C:1', Struct().dummy, 2)")
    pydim.dis_add_cmnd('test2', 'F:2;D:2;I:3;S:1;C:5;C:1', Struct().dummy, 2)

    print("Calling pydim.dis_add_service('Test Service Nr.1', 'F:1;I:1;D:2;C:4;C', dis_service1, 1)")
    svc1 = pydim.dis_add_service('Test Service Nr.1', 'F:1;I:1;D:2;C:4;C', dis_service1, 1)
    print("Calling pydim.dis_add_service('Test Service Nr.2', 'D:1', Struct().service2, 2)")
    svc2 = pydim.dis_add_service('Test Service Nr.2', 'F:1', Struct().service2, 2)

    print(("Starting serving services 1 and 2. Their ids are", svc1, svc2))
    pydim.dis_start_serving()
    for i in range(5):
         global counter
         if counter%2:
             counter += 1
             print("Updating service nr. 1")
             print("pydim.dis_update_service( svc1, (counter, counter+1, 999.0, 999.0 , 'BAUD', 'B') )",
                   pydim.dis_update_service( svc1, (counter, counter+1, 999.0, 999.0 , 'BAUD', 'B') ))
             print("Updating service nr. 2")
             print("pydim.dis_update_service(svc2, (float(100),))", pydim.dis_update_service(svc2, (float(100),)))
         else:
             print("Updating service nr. 1", pydim.dis_update_service( svc1 ))
             print("Updating service nr. 2", pydim.dis_update_service( svc2 ))
         sleep(1)

global __dic_service_not_refreshed
__dic_service_refreshed = 0

def dic_command_callback(*args):
    print('dic callback called. Args are', args)
    global __dic_service_refreshed
    __dic_service_refreshed = 1

def test_dic_interface():    
    pydim.dic_info_service('SERVICE_NO_FUNC', 'C:1;F:1;C:20', dic_command_callback)
    while not __dic_service_refreshed:
       sleep(1)

if __name__=='__main__':
    print('++++ pydim module content')
    print(dir(pydim))
    print('++++ Testing DIS interface ')
    test_dis_interface()
    print('++++ Testing DIC interface ')
    test_dic_interface()
