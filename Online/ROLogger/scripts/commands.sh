#!/bin/bash
#
# -- Run DIM message producer from stdout
#  test_output_server -dns1 pluscc05 -dns2 pluscc04 -utgid BLABLA_1
#
#
#
#
#
# -- Run node logger picking up messages from local tasks on one node and publish the messages to kafka
#  gentest libROLogSrv.so run_kafka_node_logger -b pluscc05 -m (.*)_LOGS -t partition_logs -B 10.128.96.12:30400
#
#
# -- Run terminal logger picking up messages from publication point on ecs03 matchinf source nodes MON* and PLUS*
#  gentest libROLogSrv.so run_output_logger -b KAFKALOG -d ecs03 -m "MON(.*)" -m "PLUS(.*)"
#
#
# -- Run terminal logger picking up messages from publication point on each node: DNS: pluscc05 source: pluscc04
#  gentest libROLogSrv.so run_output_logger -N -b pluscc05 -S "(.*)_LOGS" -m pluscc04
#
#
# -- Run kafka logger publishing to DIM services on ecs03
#  gentest libROLogSrv.so run_kafka_logserver -t partition_logs -b 10.128.96.12:30400 -i 2000 
#
#
# -- Run terminal logger picking up messages from the kafka instance and dump them to terminal
#  gentest libROLogSrv.so run_output_logger -K -b 10.128.96.12:30400 -t partition_logs -i 2000 -S pluscc05
#  gentest libROLogSrv.so run_output_logger -K -b 10.128.96.12:30400 -t partition_logs -m "HLT2270(.*)"
#
#
# ===============================================================================================================
# -- Setup for a single node:
#
# -- Node collector and local publisher
#  gentest libROLogSrv.so run_publish_logger -N -b pluscc05 -S (.*)_LOGS -m pluscc05 -D pluscc05 -n /pluscc05 -Q LOGS
#
# -- Node publisher using Dim to ECS03 (Dim top level logger)
#  gentest libROLogSrv.so run_publish_logger -N -b pluscc05 -S /PLUSCC05/LOGS -m pluscc05 -D ecs03 -n /pluscc05 -Q LOGS
#
# -- Node publisher using kafka to elastic search
#  gentest libROLogSrv.so run_kafka_logger -N -b pluscc05 -S /PLUSCC05/LOGS -t partition_logs -B 10.128.96.12:30400 -P 2











