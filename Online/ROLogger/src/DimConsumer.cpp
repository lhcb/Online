//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  ROLogger
//--------------------------------------------------------------------------
//
//  Package    : ROLogger
//
//  Description: Readout message logging in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================
#ifndef ROLOGGER_DIMCONSUMER_H
#define ROLOGGER_DIMCONSUMER_H

/// Framework include files
#include <RTL/strdef.h>
#include "LogServer.h"

/// C/C++ include files
#include <string>
#include <vector>
#include <regex>


/// rologger namespace declaration
namespace rologger  {

  namespace  {

    class ConsumerImp : public Consumer::Implementation  {
    public:
      typedef std::vector<std::regex> Matches;
      typedef std::map<std::string, unsigned int> ServiceList;
      ServiceList    service_list;
      Matches        node_match;
      std::string    dns;
      std::string    broker;
      long           dns_ID = 0;
      unsigned int   brokerServices = 0;

      /// Signal handler to stop
      static void sig_stop (int sig);
      /// DimInfo overload to process messages
      static void analyze_services(void* tag, void* address, int* size);
      /// Analyze the DIM service list of the broker
      void analyze_services(char* services, size_t len);
      /// DimInfo overload to process messages
      static void process_messages(void* tag, void* address, int* size);
      /// Analyze a single payload messages
      void process_messages(char* payload, size_t len);

      /// Remove set of host services
      void _remove_hosts(char* services);
      /// Add a set of host services
      void _add_hosts(char* services);

    public:
      /// Default constructor
      ConsumerImp() = default;
      /// Default destructor
      virtual ~ConsumerImp() = default;
      /// Base consumer overload: Add new listening topic
      virtual void add_topic(const std::string& name)  override;
      /// Base consumer overload: Start listening to rologger topics
      virtual int  listen(const std::string& brokers)  override;
      /// Base consumer overload: Shutdown processing
      virtual void shut_down()  override;
    };
  }
}
#endif // ROLOGGER_DIMCONSUMER_H

/// Framework include files
#include "DimConsumer.h"
#include <WT/wtdef.h>
#include <RTL/rtl.h>
#include <dim/dic.hxx>

/// C/C++ include files
#include <csignal>

using namespace rologger;
static int no_link = 0;

/// Signal handler to stop
void ConsumerImp::sig_stop (int /* sig */) {
  ::wtc_insert(WT_FACILITY_EXIT,nullptr);
}

/// DimInfo overload to process messages
void ConsumerImp::process_messages(void* tag, void* address, int* size)   {
  if ( address && tag && size && *size>0 )   {
    ConsumerImp* it = *(ConsumerImp**)tag;
    it->process_messages((char*)address, *size);
  }
}

/// Analyze a single payload messages
void ConsumerImp::process_messages(char* payload, size_t len)   {
  /// Start measuring from first message received
  if ( !monitor.t_start )   {
    monitor.t_start = monitor.t_last = rd_clock();
  }
  monitor.messages++;
  monitor.bytes += len;
  for( auto l : listeners )
    l->handle_payload("???", payload, len, 0, 0);
  auto now = rd_clock();
  if ( now - monitor.t_last > monitor_interval )   {
    for( auto l : listeners )
      l->handle_monitoring(monitor);
    monitor.t_last        = now;
    monitor.messages_last = monitor.messages;
    monitor.bytes_last    = monitor.bytes;
  }
}

/// DimInfo overload to process messages
void ConsumerImp::analyze_services(void* tag, void* address, int* size)   {
  if ( address && tag && size && *size>0 )   {
    ConsumerImp* it = *(ConsumerImp**)tag;
    it->analyze_services((char*)address, *size);
  }
}

/// Remove set of host services
void ConsumerImp::_remove_hosts(char* services)   {
  for( char *e, *l, *host=services; host; )  {
    l = e = host + 1;
    if ( (host[0] == '/') && (e=::strchr(e,'|')) && (l=::strstr(l,"/log")) && (l < e) )  {
      *e = 0;
      ServiceList::iterator i=service_list.find(host);
      if ( i != service_list.end() )   {
	::dic_release_service((*i).second);
	service_list.erase(i);
	::lib_rtl_output(LIB_RTL_VERBOSE,"+++ Remove service: %s",host);
      }
      *e = '|';
    }
    host = ::strstr(e,"|\n");
    if ( host ) host += 2;
  }
}

/// Add a set of host services
void ConsumerImp::_add_hosts(char* services)   {
  for( char *e, *l, *host=services; host; )  {
    l = e = host + 1;
    if ( (host[0] == '/') && (e=::strchr(e,'|')) && (l=::strstr(l,"/log")) && (l < e) )  {
      *e = 0;
      *l = 0;
      bool match = node_match.empty();
      if ( !match )   {
	for ( const auto& m : node_match )  {
	  if ( std::regex_match(host, m) )  {
	    match = true;
	    break;
	  }
	}
      }
      *l = '/';
      if ( match && service_list.find(host) == service_list.end() )   {
	unsigned int id = ::dic_info_service_dns(this->dns_ID, host, MONITORED,
						 0, 0, 0, process_messages, 
						 (long)this, &no_link, sizeof(no_link));
	service_list.insert(std::make_pair(host,id));
	::lib_rtl_output(LIB_RTL_VERBOSE,"+++ Adding service: %s",host);
      }
      *e = '|';
    }
    host = ::strstr(e,"|\n");
    if ( host ) host += 2;
  }
}

/// Analyze the DIM service list of the broker
void ConsumerImp::analyze_services(char* services, size_t /* len */)   {
  if ( services )   {
    if ( *services == '+' )   {
      _add_hosts(services + 1);
    }
    else if ( *services == '-' )   {
      _remove_hosts(services + 1);
    }
    else  {
      _add_hosts(services);
    }
  }
}

/// Base consumer overload: Add new listening topic
void ConsumerImp::add_topic(const std::string& /* name */)   {
}

/// Base consumer overload: Start listening to rologger topics
int ConsumerImp::listen(const std::string& broker_name)   {
  broker = broker_name;//RTL::str_upper(broker_name);
  if ( broker.empty() )    {
    return EINVAL;
  }
  if ( dns.empty() )   {
    char text[132];
    ::dic_get_dns_node(text);
    dns = text;
  }
  int port = ::dic_get_dns_port();
  this->dns_ID = ::dic_add_dns(dns.c_str(), port);
  std::string svc = broker + "/SERVICE_LIST";
  brokerServices = ::dic_info_service_dns(this->dns_ID, svc.c_str(), MONITORED,
					  0, 0, 0, analyze_services, 
					  (long)this, &no_link, sizeof(no_link));
  ::signal(SIGIO, ConsumerImp::sig_stop);
  return 1;
}

/// Base consumer overload: Shutdown processing
void ConsumerImp::shut_down()   {
}

/// rologger namespace declaration
namespace rologger  {

  template <> std::unique_ptr<Consumer::Implementation>
  Consumer::create<DimLogConsumer>(int argc, char** argv)  {
    auto consumer = std::make_unique<ConsumerImp>();
    bool print_help = false;
    std::string value;

    for( int i=0; i<argc; ++i )   {
      char c = argv[i][0];
      if ( c == '-' )   {
	c = argv[i][1];
	switch(c)   {
	case 'd':
	  consumer->dns = RTL::str_upper(argv[++i]);
	  ::lib_rtl_output(LIB_RTL_VERBOSE,"+++ Set dic_set_dns_node to %s",consumer->dns.c_str());
	  break;
	case 'h':
	  print_help = true;
	  break;
	case 'i':
	  consumer->monitor_interval = 1000 * ::atoi(argv[++i]);
	  break;
	case 'p':
	  ::dim_set_dns_port(::atoi(argv[++i]));
	  break;
	case 'S':
	  value = "/ROLOG/" + RTL::str_upper(argv[++i]) + "/LOGS";
	  ::lib_rtl_output(LIB_RTL_VERBOSE,"+++ Adding node match: %s",value.c_str());
	  consumer->node_match.emplace_back(std::regex(value));
	  break;
	default:
	  break;
	}
	continue;
      }
    }
    if ( print_help )   {
      ::printf("     -S <reg-ex>    Select messages from nodes matching the   \n"
	       "                    specified regular expression.             \n"
	       "     -i <msecs>     Monitor time interval in milli seconds.   \n"
	       "     -d <node>      Set DIM_DNS_NODE of the message server.   \n"
	       "     -p <port>      Set DIM_DNS_PORT of the message server.   \n"
	       );
      ::exit(0);
    }
    return consumer;
  }
}
