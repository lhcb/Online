//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  ROLogger
//--------------------------------------------------------------------------
//
//  Package    : ROLogger
//
//  Description: Readout message logging in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================

/// Framework include files
#include "DimWriter.h"
#include <RTL/strdef.h>
#include <RTL/rtl.h>
#include <dim/dis.h>

/// C/C++ include files
#include <vector>

/// rologger namespace declaration
namespace rologger  {

  /// Single node publisher
  /**
   *
   *   \author  M.Frank
   *   \version 1.0
   */
  class DimPublish  {
  public:
    std::string  name;
    unsigned int serviceID      = -1;
    unsigned int countID        = -1;
    unsigned int typeID         = -1;
    std::size_t  payload_count  = 0;
    std::size_t  payload_length = 0;
    std::size_t  payload_max    = 0;
    char*        payload        = nullptr;
    long         dns_ID         = 0;
    bool         echo           { false };
    /// DIM callback to supply the payload
    static void feed_message(void* tag, void** buf, int* size, int* first);
    /// DIM callback to supply the payload counter
    static void feed_count(void* tag, void** buff, int* size, int* first);
    /// DIM callback to supply the type
    static void feed_type(void* tag, void** buff, int* size, int* first);
    
  public:
    /// Default constructor
    DimPublish(long dns_id, const std::string& host, const std::string& tag, bool echo);
    /// Move constructor
    DimPublish(DimPublish&& copy) = default;
    /// Copy constructor
    DimPublish(const DimPublish& copy) = default;
    /// Move assignment
    DimPublish& operator=(DimPublish&& copy) = default;
    /// Copy assignment
    DimPublish& operator=(const DimPublish& copy) = default;
    /// Default destructor
    virtual ~DimPublish();
    /// Set message properties
    void set(const void* pay, std::size_t len);
  };
}

/// C/C++ include files
#include <cstring>
#include <iostream>
#include <unistd.h>

using namespace rologger;

/// DIM callback to supply the payload
void DimPublish::feed_message(void* tag, void** buff, int* size, int* /* first */)   {
  static const char* data = "";
  DimPublish* pub = *(DimPublish**)tag;
  if ( pub && pub->payload_length )   {
    *buff = pub->payload;
    *size = pub->payload_length;
    ++pub->payload_count;
    return;
  }
  *buff = (void*)data;
  *size = 0;
}
/// DIM callback to supply the payload counter
void DimPublish::feed_count(void* tag, void** buff, int* size, int* /* first */)   {
  static std::size_t data = -1;
  DimPublish* pub = *(DimPublish**)tag;
  *buff = pub ? &pub->payload_count : &data;
  *size = sizeof(pub->payload_count);
}
/// DIM callback to supply the type
void DimPublish::feed_type(void* /* tag */, void** buff, int* size, int* /* first */)   {
  static char data[5] = "DIM\0";
  *buff = data;
  *size = 4;
}

/// Release dim service
inline void release_dim_service(unsigned int& id)  {
  if ( id != 0 )   {
    ::dis_remove_service(id);
    id = 0;
  }
}
  
/// Default constructor
DimPublish::DimPublish(long dns_id, const std::string& n, const std::string& tag, bool e)
  : name(RTL::str_upper(n)), echo(e)
{
  if ( !name.empty() )   {
    std::string svc = name + ((tag.empty()) ? std::string("/log") : ("/" + tag + "/log"));
    serviceID = ::dis_add_service_dns(dns_id, svc.c_str(), "C", 0, 0, feed_message, (long)this);
    svc = name + ((tag.empty()) ? std::string("/count") : ("/" + tag + "/count"));
    countID   = ::dis_add_service_dns(dns_id, svc.c_str(), "L", 0, 0, feed_count, (long)this);
    svc = name + ((tag.empty()) ? std::string("/type")  : ("/" + tag + "/type"));
    typeID    = ::dis_add_service_dns(dns_id, svc.c_str(), "C", 0, 0, feed_type, (long)this);
  }
  payload = (char*)::malloc(payload_max=1024);
  payload_length = 0;
}

/// Default destructor
DimPublish::~DimPublish()   {
  release_dim_service(serviceID);
  release_dim_service(countID);
  release_dim_service(typeID);
}

void DimPublish::set(const void* pay, std::size_t len)   {
  if ( len < 10*1024 )   {
    if ( payload_max <= len+1 )  {
      payload = (char*)::realloc(payload, payload_max=len+256);
    }
    ::memcpy(payload, pay, len);
    payload[len]   = 0;
    if      ( len >= 1 && payload[len-1] == '\n' ) payload[len-1] = 0, --len;
    else if ( len >= 2 && payload[len-2] == '\n' ) payload[len-2] = 0, len -= 2;
    else if ( len >= 3 && payload[len-3] == '\n' ) payload[len-3] = 0, len -= 3;
    payload_length = len+1;
    if ( payload_length > 1 )   {
      ::dis_update_service(serviceID);
      if ( this->echo )   {
	//std::cout << payload << std::endl << std::flush;
      }
    }
  }
}

/// Default constructor
DimWriter::DimWriter(const std::string& dns, const std::string& n, const std::string& t, bool e)
  : dns_name(RTL::str_upper(dns)), name(RTL::str_upper(n)), tag(RTL::str_upper(t)), echo(e)
{
  //  publish_monitor(dns_name, name, tag);
  if ( this->dns_name.empty() )   {
    char text[132];
    ::dis_get_dns_node(text);
    this->dns_name = text;
  }
  if ( 0 == this->dns_ID )   {
    int port        = ::dis_get_dns_port();
    this->dns_ID    = ::dis_add_dns(this->dns_name.c_str(), port);
  }
  char host[128], pid_str[32];
  ::snprintf(pid_str, sizeof(pid_str), "%d", ::lib_rtl_pid());
  ::gethostname(host,sizeof(host));
  const char* dim_host  = ::getenv("DIM_HOST_NODE");
  dim_host = dim_host ? dim_host : host;
  this->output_trailer += "\",\"physical_host\":\"";
  this->output_trailer += RTL::str_lower(host);
  this->output_trailer += "\",\"host\":\"";
  this->output_trailer += RTL::str_lower(dim_host);
  this->output_trailer += "\",\"pid\":\"";
  this->output_trailer += pid_str;
  this->output_trailer += "\",\"utgid\":\""+RTL::processName();
  this->output_trailer += "\",\"systag\":\"SYSTEM\"}\n";

  service = std::make_unique<DimPublish>(this->dns_ID, name, tag, this->echo);
  ::dis_start_serving_dns(this->dns_ID, (name + '/' + tag).c_str());
  ::lib_rtl_output(LIB_RTL_INFO,
		   "+++ DimWriter setup: DNS:%s  [%ld]  port:%d  server:%s/%s  echo:%s",
		   dns.c_str(), this->dns_ID, ::dim_get_dns_port(), name.c_str(), tag.c_str(),
		   this->echo ? "enabled" : "disabled");
}

/// Default destructor
DimWriter::~DimWriter()   {
}

/// Handler for all messages
void DimWriter::handle_payload(const char* /* topic */,
			       char* payload,   std::size_t plen,
			       char* /* key */, std::size_t /* klen */)
{
  static constexpr long PAYLOAD_MATCH = 0x73656D697440227BL;  // *(long*)"{\"@timest";
  int match = *(long*)payload == PAYLOAD_MATCH;
  if ( match )   {
    service->set(payload, plen);
    if ( this->echo )   {
      ::lib_rtl_output(LIB_RTL_ALWAYS, payload);
    }
  }
  else   {
    if ( payload[0] && payload[0] == ' ' ) {
      ++payload, --plen;
    }
    if ( ::strlen(payload) > 0 )   {
      int null = 0;
      struct tm tim;
      std::time_t now = ::time(0);
      std::size_t out_len = 128+2*plen+this->output_trailer.length();
      ::localtime_r(&now, &tim);
      this->buffer.set_cursor(0);
      this->buffer.allocate(out_len);
      std::size_t tim_len = ::strftime((char*)this->buffer.ptr(),128,"{\"@timestamp\":\"%Y-%m-%dT%H:%M:%S.000%z\",\"message\":\"",&tim);
      std::size_t data_len = str_escape_json((char*)this->buffer.ptr()+tim_len, out_len - tim_len, payload, plen);
      std::strncpy((char*)this->buffer.begin()+tim_len+data_len, this->output_trailer.c_str(), this->output_trailer.length());
      ::memcpy((char*)this->buffer.begin()+tim_len+data_len+this->output_trailer.length(), &null, sizeof(null));
      service->set((char*)this->buffer.begin(), tim_len+data_len+this->output_trailer.length());
      if ( this->echo )   {
	std::cout << (char*)this->buffer.begin() << std::endl;
      }
    }
  }
}
