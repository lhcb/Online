//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  ROLogger
//--------------------------------------------------------------------------
//
//  Package    : ROLogger
//
//  Description: Readout message logging in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================
#ifndef ROLOGGER_DIMOUTPUTWRITER_H
#define ROLOGGER_DIMOUTPUTWRITER_H

/// Framework include files
#include "LogServer.h"
#include <CPP/mem_buff.h>

/// C/C++ include files
#include <map>
#include <memory>

/// rologger namespace declaration
namespace rologger  {

  class DimPublish;

  /// Rologger listener to publish node payload
  /**
   *
   *   \author  M.Frank
   *   \version 1.0
   */
  class DimWriter : public Writer  {
  public:
    std::unique_ptr<DimPublish> service { };
    std::string dns_name        { };
    std::string name            { };
    std::string tag             { };
    std::string real_host       { };
    std::string physical_host   { };
    std::string output_trailer  { };
    Online::mem_buff buffer     { };
    long        dns_ID          { 0 };
    bool        echo            { false };

  public:
    /// Default constructor
    DimWriter(const std::string& dns, const std::string& name, const std::string& tag, bool echo);
    /// Default destructor
    virtual ~DimWriter();
    /// Handler for all messages
    virtual void handle_payload(const char* topic,
				char* payload, size_t plen,
				char* key,     size_t klen)  override;  
  };
}       // End namespace rologger
#endif  // ROLOGGER_DIMOUTPUTWRITER_H
