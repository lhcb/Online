//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  ROLogger
//--------------------------------------------------------------------------
//
//  Package    : ROLogger
//
//  Description: Readout message logging in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================

/// Framework include files
#include "FifoWriter.h"

/// C/C++ include files
#include <vector>
#include <cstring>
#include <stdexcept>
#include <system_error>

#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

using namespace rologger;

namespace  {
  /// Translate errno into error string
  inline const std::string cerror(int err)   {
    return std::make_error_code(std::errc(err)).message();
  }
}

/// Second level object initialization
int FifoWriter::initialize()     {
  std::string& fifo = fifo_name;
  struct stat statBuf;
  char text[1042];

  if ( fifo_fd != -1 )   {
    ::close(fifo_fd);
  }
  if ( fifo.empty() )   {
    throw std::runtime_error("Fifo name is empty. How could I possibly log stuff ?");
  }
  // If not present, create the fifo:
  if( ::access(fifo.c_str(),W_OK) == -1) {
    ::mkfifo(fifo.c_str(), S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH);
  }
  // check if fifo is writable 
  if( ::access(fifo.c_str(),W_OK) == -1) {       // write access to fifo OK
    ::snprintf(text,sizeof(text),"+++ Failed to access output: %s [%s]",
	       fifo.c_str(), cerror(errno).c_str());
    throw std::runtime_error(text);
  }
  // get fifo information
  if( ::stat(fifo.c_str(),&statBuf) == -1) {     // fifo information got
    ::snprintf(text,sizeof(text),"+++ Output: Canno stat device %s! [%s]",
	       fifo.c_str(), cerror(errno).c_str());
    throw std::runtime_error(text);
  }
  // check if fifo is a FIFO
  if( !S_ISFIFO(statBuf.st_mode)) {            // fifo is a FIFO
    ::snprintf(text,sizeof(text),"+++ Output: %s is no fifo!", fifo.c_str());
    throw std::runtime_error(text);
  }
  // open fifo
  fifo_fd = ::open(fifo.c_str(), O_RDWR|O_NONBLOCK|O_APPEND);
  if( fifo_fd == -1 )   {        /* fifo open() failed      */
    ::snprintf(text,sizeof(text),"+++ Failed to open output: %s! [%s]",
	       fifo.c_str(), cerror(errno).c_str());
    throw std::runtime_error(text);
  }
  // Now we are sure that another process has the FIFO open for reading.
  // We unset now the O_NONBLOCK bit to have blocking write (no-drop behaviour).
  int status = ::fcntl(fifo_fd, F_GETFL);
  if ( status < 0 )    {
    ::close(fifo_fd);
    ::snprintf(text,sizeof(text),"+++ Cannot fcntl fifo path %s. [%s]",
	       fifo.c_str(), cerror(errno).c_str());
    throw std::runtime_error(text);
  }
  status &= ~O_NONBLOCK;                         /* unset O_NONBLOCK bit */
  if ( ::fcntl(fifo_fd, F_SETFL,status) == -1 )    {
    ::close(fifo_fd);
    ::snprintf(text,sizeof(text),"+++ Cannot set O_NONBLOCK bit of fifo %s. [%s]",
	       fifo.c_str(), cerror(errno).c_str());
    throw std::runtime_error(text);
  }
  return this->Writer::initialize();
}

/// Handler for all messages
void FifoWriter::handle_payload(const char* /* topic */,
				char* buf, std::size_t len,
				char* /* key */, std::size_t /* klen */)
{
  struct timespec delay = { 0, 1000000 };  /* 0.001 s */
  int tryC = 0, numRetry = 100;
  std::size_t done = 0;

  for ( tryC=0; tryC < numRetry; tryC++ )    {
    int written = ::write(fifo_fd, buf+done, len-done);
    if ( written > 0 )
      done += written;
    else if ( written == -1 )
      break;
    else if ( errno != EAGAIN )
      break;

    if ( done == len ) break;
    nanosleep(&delay,NULL);
    delay.tv_sec *= 2;
    delay.tv_nsec *= 2;
    if(delay.tv_nsec>999999999){
      delay.tv_sec+=1;
      delay.tv_nsec-=1000000000;
    }
  }
  ::write(fifo_fd, "\n", 1);
}
