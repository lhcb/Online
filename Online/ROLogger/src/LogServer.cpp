//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  ROLogger
//--------------------------------------------------------------------------
//
//  Package    : ROLogger
//
//  Description: Readout message logging in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================

/// Framework include files
#include "LogServer.h"
#include <WT/wtdef.h>
#include <RTL/rtl.h>
#include <dim/dis.h>

/// C/C++ include files
#include <cstdio>
#include <cstring>

using namespace std;
using namespace rologger;

rd_ts_t rologger::rd_clock (void) {
  struct timespec ts;
  clock_gettime(CLOCK_MONOTONIC, &ts);
  return ((rd_ts_t)ts.tv_sec * 1000000LLU) + ((rd_ts_t)ts.tv_nsec / 1000LLU);
}

/// Translate errno into error string
string rologger::error(int err)   {
  char   text[1024];
  string e = ::strerror_r(err, text, sizeof(text));
  return e;
}

/// Default destructor
Writer::~Writer()    {
#if 0
  if ( this->monitorID != 0 )   {
    ::dis_remove_service(this->monitorID);
    this->monitorID = 0;
  }
#endif
}

/// Second level object initialization
int Writer::initialize()    {
  return 1;
}

/// Handler for all messages
void Writer::handle_raw(    const char* topic,
			      char*       payload,
			      size_t      plen,
			      char*       key,
			      size_t      klen)
{
  this->handle_payload(topic, payload, plen, key, klen);
}

/// Handler for all messages
void Writer::handle_payload(const char* /* topic   */,
			      char*       /* payload */,
			      size_t      /* plen    */,
			      char*       /* key     */,
			      size_t      /* klen    */)   {
}

/// Handle monitoring 
void Writer::handle_monitoring(const Monitor& mon_info)   {
  if ( print_stats )   {
    print_statistics(mon_info);
  }
}

/// Print raw message to output
void Writer::print_raw_message(const char* payload, std::size_t /* len */)   const    {
  if ( print_raw )    {
    ::lib_rtl_output(LIB_RTL_ALWAYS,"%s", payload);
  }
}

/// Print monitoring information in table form
void Writer::print_statistics(const Monitor& monitor)   const   {
  rd_ts_t now = rd_clock();
  rd_ts_t t_total;
  static int rows_written = 0;
  int    print_header;
  char   extra[512];

  *extra = '\0';
  print_header = !rows_written || !(rows_written % 20);
  if (monitor.t_end_send)
    t_total = monitor.t_end_send - monitor.t_start;
  else if (monitor.t_end)
    t_total = monitor.t_end - monitor.t_start;
  else if (monitor.t_start)
    t_total = now - monitor.t_start;
  else
    t_total = 1;

#define ROW_START()        do {} while (0)
#define COL_HDR(NAME)      printf("| %10.10s ", (NAME))
#define COL_PR64(NAME,VAL) printf("| %10ld ", (VAL))
#define COL_PRF(NAME,VAL)  printf("| %10.2f ", (VAL))
#define ROW_END()          do {                 \
    printf("\n");				\
    rows_written++;				\
  } while (0)

  if (print_header) {
    /* First time, print header */
    ROW_START();
    COL_HDR("elapsed");
    COL_HDR("msgs");
    COL_HDR("bytes");
    COL_HDR("msgs/s");
    COL_HDR("MB/s");
    COL_HDR("rx_err");
    COL_HDR("offset");
    ROW_END();
  }

  ROW_START();
  COL_PR64("elapsed",  t_total / 1000);
  COL_PR64("msgs",     monitor.messages);
  COL_PR64("bytes",    monitor.bytes);
  COL_PR64("m/s",      ((monitor.messages * 1000000) / t_total));
  COL_PRF("MB/s",      (float)((monitor.bytes) / (float)t_total));
  COL_PR64("rx_err",   monitor.messages_dr_err);
  COL_PR64("offset",   monitor.offset);
  ROW_END();
}

/// Print monitoring information in table form
void Writer::print_summary(const Monitor& monitor)   const   {
  rd_ts_t now = rd_clock();
  rd_ts_t t_total = 1;

  if (monitor.t_end_send)
    t_total = monitor.t_end_send - monitor.t_start;
  else if (monitor.t_end)
    t_total = monitor.t_end - monitor.t_start;
  else if (monitor.t_start)
    t_total = now - monitor.t_start;

  ::printf("%% %ld messages (%ld bytes) consumed in %ldms: %ld msgs/s (%.02f MB/s)\n",
	   monitor.messages, monitor.bytes, t_total / 1000, 
	   (monitor.messages * 1000000) / t_total,
	   (float)((monitor.bytes) / (float)t_total));
}

/// Default constructor
Consumer::Consumer(unique_ptr<Implementation>&& impl)
  : consumer(impl.release())
{
}

/// Default destructor
Consumer::~Consumer()   {
  consumer->shut_down();
  consumer.reset();
}

/// Add a writer
void Consumer::add_writer(Writer* writer)   {
  consumer->add_writer(writer);
}

/// Remove a writer
void Consumer::remove_writer(Writer* writer)   {
  consumer->remove_writer(writer);
}

/// Start listening to rologger topics
bool Consumer::listen(const string& brokers)   {
  return consumer->listen(brokers);
}

/// Run the instance
void Consumer::run()   {
  consumer->run();
}

/// Start publishing monitoring information using DIM
void Consumer::publish_monitor(const string& dns_name,
			       const string& name,
			       const string& tag)
{
  consumer->publish_monitor(dns_name, name, tag);
}

/// Start publishing monitoring information using DIM
void Consumer::publish_monitor(long          dns_id,
			       const string& name,
			       const string& tag)
{
  consumer->publish_monitor(dns_id, name, tag);
}

/// Consumer creator
unique_ptr<Consumer::Implementation>
Consumer::create(int consumer_type, vector<char*>& args)
{
  int argc = args.size();
  char** argv = &args[0];
  if ( consumer_type == DIMNODE_CONSUMER )   {
    ::lib_rtl_output(LIB_RTL_ALWAYS,"+++ Starting up instance of DimNodeConsumer");
    return Consumer::create<DimNodeConsumer>(argc, argv);
  }
  else if ( consumer_type == DIMLOG_CONSUMER )  {
    ::lib_rtl_output(LIB_RTL_ALWAYS,"+++ Starting up instance of DimLogConsumer");
    return Consumer::create<DimLogConsumer>(argc, argv);
  }
  else if ( consumer_type == STDIN_CONSUMER )   {
    ::lib_rtl_output(LIB_RTL_ALWAYS,"+++ Starting up instance of StdinConsumer");
    return Consumer::create<StdinConsumer>(argc, argv);
  }
  ::lib_rtl_output(LIB_RTL_ALWAYS,"+++ Unknown consumer type to be created: %d",consumer_type);
  return unique_ptr<Implementation>();
}

/// Add a writer
void Consumer::Implementation::add_writer(Writer* writer)   {
  listeners.push_back(writer);
}

/// Remove a writer
void Consumer::Implementation::remove_writer(Writer* writer)   {
  for(Writers::iterator i=listeners.begin(); i != listeners.end(); ++i)   {
    if ( (*i) == writer )  {
      listeners.erase(i);
      return;
    }
  }
}

static int term_handler(void*      /* user_context */,
			int           sig_number,
			siginfo_t* /* info */,
			void*      /* ptr */)
{
  ::lib_rtl_output(LIB_RTL_ALWAYS,"+++ Handling termination signal %d", sig_number);
  ::lib_rtl_signal_log(0);
  ::wtc_insert(WT_FACILITY_EXIT);
  return LIB_RTL_SIGNAL_HANDLED;
}

/// Run the Rologger instance forever
void Consumer::Implementation::run()   {
  unsigned int facility;
  int substatus;
  void* param;
  ::wtc_init();
  ::lib_rtl_subscribe_signal(SIGQUIT, this, term_handler);
  ::lib_rtl_subscribe_signal(SIGTERM, this, term_handler);
  while (1)  {
    ::wtc_wait( &facility, &param, &substatus );  
    if ( facility == WT_FACILITY_EXIT )   {
      break;
    }
  }
  ::wtc_shutdown();
  ::lib_rtl_unsubscribe_signal(SIGQUIT, this);
  ::lib_rtl_unsubscribe_signal(SIGTERM, this);
  ::lib_rtl_output(LIB_RTL_INFO,"Stop application signal received...");
}

/// Start publishing monitoring information using DIM
void Consumer::Implementation::publish_monitor(const string& dns_name,
					       const string& name,
					       const string& tag)
{
  string dns = dns_name;
  string svc = name+"/"+tag+"/monitor";
  if ( dns.empty() )   {
    char text[132];
    ::dis_get_dns_node(text);
    dns = text;
  }
  if ( 0 == this->monitorID )   {
    int port         = ::dis_get_dns_port();
    this->out_dns_ID = ::dis_add_dns(dns.c_str(), port);
    this->monitorID  = ::dis_add_service_dns(this->out_dns_ID,svc.c_str(),"C",0,0,feed_monitor,(long)this);
    ::dis_start_serving_dns(this->out_dns_ID, (name+"/"+tag).c_str());
    ::lib_rtl_output(LIB_RTL_ALWAYS,"+++ DNS: '%s' port:%d [%ld]   Server: '%s'",
		     dns.c_str(), ::dim_get_dns_port(), this->out_dns_ID, (name+"/"+tag).c_str());
  }
}

/// Start publishing monitoring information using DIM
void Consumer::Implementation::publish_monitor(long dns_id,
					       const string& name,
					       const string& tag)
{
  if ( 0 == this->monitorID )   {
    string svc       = name+"/"+tag+"/input/monitor";
    this->out_dns_ID = dns_id;
    this->monitorID  = ::dis_add_service_dns(this->out_dns_ID,svc.c_str(),"C",0,0,feed_monitor,(long)this);
    ::dis_start_serving_dns(this->out_dns_ID, (name+"/"+tag).c_str());
    ::lib_rtl_output(LIB_RTL_ALWAYS,"+++ DNS id: %ld port:%d  Server: '%s'",
		     this->out_dns_ID, ::dim_get_dns_port(), (name+"/"+tag).c_str());
  }
}

/// DIM callback to supply the monitoring information
void Consumer::Implementation::feed_monitor(void* tag, void** buff, int* size, int* /* first */)   {
  static const char* data = "";
  Consumer::Implementation*   pub  = *(Consumer::Implementation**)tag;
  if ( pub )   {
    *size = sizeof(pub->monitor);
    *buff = &pub->monitor;
    return;
  }
  *buff = (void*)data;
  *size = 0;
}

