//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  ROLogger
//--------------------------------------------------------------------------
//
//  Package    : ROLogger
//
//  Description: Readout message logging in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================

/// Framework include files
#include "DimWriter.h"
#include <RTL/Logger.h>
#include <RTL/rtl.h>

/// C/C++ include files
#include <vector>

namespace  {

  int help(const char* exe)   {
    ::printf("%s -opt [-opt]                                                  \n"
	     "   Run terminal client to display messages from rologger.       \n"
	     "                                                                \n"
	     "     -h             Print this help.                            \n"
	     "     -e             Echo messages to stdout.                    \n"
	     "     -I             Connect to stdin as message source and      \n"
	     "                    forward the message to terminal.            \n"
	     "     -L             Connect to all services on the main pub.    \n"
	     "                    instance on ecs03 (DEFAULT).                \n"
	     "     -N             Run node logger (pick up messages from one  \n"
	     "                    node directly. Do not connect to main       \n"
	     "                    publishing instance.                        \n"
	     "     -b <broker>    Set DIM message broker.                     \n"
	     "     -O <level>     Set RTL output level (1...4).               \n"
	     "     -P             Print statistics                            \n"
	     "     -Q             Additional tag to create DIM service names. \n"
	     "     -m <reg-ex>    Add node matching regular expression.       \n"
	     "     -n <name>      Publishing instance name.                   \n"
	     "     -V <reg-ex>    Veto messages matching regular expression.  \n",
	     exe);
    return 0;
  }
}

extern "C" int run_publish_logger (int argc, char **argv) {
  using namespace rologger;
  bool print_echo    = false;
  bool print_help    = false;
  bool print_stats   = false;
  bool publish_stats = false;
  int  output_level  = LIB_RTL_INFO;
  int  consumer_type = DIMLOG_CONSUMER;
  std::string brokers, value;
  std::vector<char*>   args;
  std::string dns, name, tag;

  if ( ::getenv("UTGID") )  {
    name = ::getenv("UTGID");
  }
  for( int i=0; i<argc; ++i )   {
    char c = argv[i][0];
    if ( c == '-' )   {
      c = argv[i][1];
      switch(c)   {
      case 'b':
	if ( !brokers.empty() ) brokers += ",";
	brokers += argv[++i];
	break;
      case 'h':
	print_help = true;
	args.push_back(argv[i]);
	break;
      case 'e':
	print_echo = true;
	break;
      case 'n':
	name = argv[++i];
	break;
      case 'D':
	dns = argv[++i];
	break;
      case 'L':
	consumer_type = DIMLOG_CONSUMER;
	break;
      case 'N':
	consumer_type = DIMNODE_CONSUMER;
	break;
      case 'I':
	consumer_type = STDIN_CONSUMER;
	break;
      case 'O':
	output_level = ::atol(argv[++i]);
	break;
      case 'p':
	publish_stats = true;
	break;
      case 'P':
	print_stats = true;
	break;
      case 'Q':
	tag = argv[++i];
	break;
      default:
	args.push_back(argv[i]);
	break;
      }
      continue;
    }
    args.push_back(argv[i]);
  }

  RTL::Logger::install_rtl_printer(output_level);
  auto imp = Consumer::create(consumer_type, args);
  if ( !imp.get() || print_help )  {
    help("run_publish_logger");
    return EINVAL;
  }
  Consumer  consumer(std::move(imp));
  DimWriter writer(dns, name, tag, print_echo);
  writer.print_stats = print_stats;
  if ( !consumer.listen(brokers.c_str()) )    {
    exit(EINVAL);
  }
  consumer.add_writer(&writer);
  if ( publish_stats )   {
    consumer.publish_monitor(dns, name, tag);
  }
  consumer.run();
  return 0;
}
