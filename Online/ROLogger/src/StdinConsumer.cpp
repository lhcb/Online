//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  ROLogger
//--------------------------------------------------------------------------
//
//  Package    : ROLogger
//
//  Description: Readout message logging in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================
#ifndef ROLOGGER_STDINCONSUMER_H
#define ROLOGGER_STDINCONSUMER_H

/// Framework include files
#include "LogServer.h"
#include <RTL/strdef.h>

/// C/C++ include files
#include <string>
#include <vector>
#include <mutex>
#include <regex>
#include <csignal>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/epoll.h>

/// rologger namespace declaration
namespace rologger  {

  namespace  {

    class ConsumerImp : public Consumer::Implementation  {
    public:
      static const size_t OUTPUT_MAX_LEN = 8192;
      typedef std::vector<std::regex> Matches;
      bool print_stats  = false;
      bool create_fifo  = false;
      int  poll_timeout = 400;
      int  epollfd      =  -1;
      std::mutex m_file_mutex;

      /// We need here arrays of structs to get the poll descriptors ass-to-ass
      std::vector<std::string>        m_file_name;
      std::vector<std::string>        m_file_buff;
      std::vector<int>                m_file_desc;
      std::vector<struct epoll_event> m_file_poll;
      typedef std::map<int, int>      Files;
      Files                           m_files;
      size_t m_num_files = 0;

      /// Signal handler to stop
      static int sig_stop (void*         user_context,
			   int           sig_number,
			   siginfo_t*    info,
			   void*         ptr);

      /// Analyze a single payload messages
      void process_message(char* payload, size_t len);

      /// Listen to a regular file or a fifo
      int  listen_file(const std::string& file_name);

      /// Listen to a fifo
      int  listen_fifo(const std::string& file_name);

      /// Add new file to the list of supervised files
      int  add_file(const std::string& file_name, int file_desc);

      /// Empty file buffer and issue processing on end-of-line
      int  read_file(int fd, size_t idx, int num_bytes);

    public:

      /// Default constructor
      ConsumerImp();

      /// Default destructor
      virtual ~ConsumerImp();

      /// Base consumer overload: Add new listening topic
      virtual void add_topic(const std::string& name)  override;

      /// Base consumer overload: Start listening to rologger topics
      virtual int  listen(const std::string& brokers)  override;

      /// Base consumer overload: Shutdown processing
      virtual void shut_down()  override;

      /// Run the Rologger instance forever
      virtual void run()  override;
    };
  }
}
#endif // ROLOGGER_STDINCONSUMER_H

/// Framework include files
//#include "StdinConsumer.h"
#include <RTL/rtl.h>

/// C/C++ include files
#include <csignal>
#include <csignal>
#include <cstring>
#include <ctime>
#include <fcntl.h>
#include <dirent.h>
#include <sys/ioctl.h>
using namespace rologger;


/// Default constructor
ConsumerImp::ConsumerImp()    {
  this->epollfd = ::epoll_create(10);
}

/// Default destructor
ConsumerImp::~ConsumerImp()   {
  if ( this->epollfd )  {
    ::close(this->epollfd);
    this->epollfd = 0;
  }
}

/// Signal handler to stop
int ConsumerImp::sig_stop (void*      /* user_context */,
			   int        /* sig_number   */,
			   siginfo_t* /* info         */,
			   void*      /* ptr          */) {
  return 1;
}

/// Analyze a single payload messages
void ConsumerImp::process_message(char* payload, size_t len)   {
  /// Start measuring from first message received
  if ( !monitor.t_start )   {
    monitor.t_start = monitor.t_last = rd_clock();
  }
  monitor.messages++;
  monitor.bytes += len;
  for( auto l : listeners )
    l->handle_payload("???", payload, len, 0, 0);
  auto now = rd_clock();
  if ( now - monitor.t_last > monitor_interval )   {
    for( auto l : listeners )
      l->handle_monitoring(monitor);
    monitor.t_last        = now;
    monitor.messages_last = monitor.messages;
    monitor.bytes_last    = monitor.bytes;
  }
}

/// Base consumer overload: Add new listening topic
void ConsumerImp::add_topic(const std::string& /* name */)   {
}

/// Add new file to the list of supervised files
int ConsumerImp::add_file(const std::string& file_name, int file_desc)   {
  struct epoll_event ev;
  std::lock_guard<std::mutex> lock(m_file_mutex);
  if ( m_num_files >= m_file_name.size() )   {
    for(size_t i=0; i<128; ++i)   {
      m_file_name.push_back("");
      m_file_buff.push_back("");
      m_file_desc.push_back(-1);
      m_file_poll.push_back(ev);
    }
  }
  ev.data.fd = file_desc;
  ev.events  = EPOLLIN | EPOLLPRI | EPOLLERR | EPOLLHUP | EPOLLRDHUP | EPOLLET;
  if (::epoll_ctl(epollfd, EPOLL_CTL_ADD, file_desc, &ev) == -1) {
    ::lib_rtl_output(LIB_RTL_ERROR,"Failed extend epoll for %s [%s].",
		     file_name.c_str(), error(errno).c_str());
    ::exit(EXIT_FAILURE);
    return 0;
  }
  m_file_buff[m_num_files] = "";
  m_file_name[m_num_files] = file_name;
  m_file_desc[m_num_files] = file_desc;
  m_file_poll[m_num_files].data.fd = file_desc;
  m_file_poll[m_num_files].events  = EPOLLIN | EPOLLPRI | EPOLLERR | EPOLLHUP | EPOLLRDHUP | EPOLLET;
  m_files[file_desc] = m_num_files;
  ++m_num_files;
  return 1;
}

/// Listen to a fifo
int  ConsumerImp::listen_fifo(const std::string& file_name)   {
  struct stat statBuf;
  // check if fifo is writable 
  if( ::access(file_name.c_str(),W_OK) == -1 ) {       // write access to fifo OK
    ::lib_rtl_output(LIB_RTL_ERROR,"+++ Failed to access fifo: %s [%s]",
		     file_name.c_str(), error(errno).c_str());
    return 0;
  }
  // get file_name information
  if( ::stat(file_name.c_str(),&statBuf) == -1) {     // fifo information got
    ::lib_rtl_output(LIB_RTL_ERROR,"+++ Cannot stat device %s [%s]",
		     file_name.c_str(), error(errno).c_str());
    return 0;
  }
  // check if fifo is a FIFO
  if( !S_ISFIFO(statBuf.st_mode)) {            // fifo is a FIFO
    ::lib_rtl_output(LIB_RTL_ERROR,"+++ Device %s is no fifo!",
		     file_name.c_str());
    return 0;
  }
  int fd = ::open(file_name.c_str(), O_RDONLY|O_NONBLOCK);
  if ( fd != -1 )   { // Unset O_NONBLOCK
    int flags = ::fcntl(fd, F_GETFL, 0);
    ::fcntl(fd, F_SETFL, flags & ~O_NONBLOCK);
    ::lib_rtl_output(LIB_RTL_ERROR,"+++ Listening to fifo device %s",
		     file_name.c_str());
    return add_file(file_name, fd);
  }
  return 0;
}

/// Listen to a regular file
int  ConsumerImp::listen_file(const std::string& file_name)   {
  int fd = ::open(file_name.c_str(), O_RDONLY);
  if ( fd != -1 )   {
    ::lseek(fd, 0, SEEK_END);
    /// Not sure this has any effect on files:
    int flags = ::fcntl(fd, F_GETFL, 0);
    ::fcntl(fd, F_SETFL, flags | O_NONBLOCK);
    return add_file(file_name, fd);
  }
  return 0;
}

/// Base consumer overload: Start listening to rologger topics
int ConsumerImp::listen(const std::string& file_or_dir_name)   {
  struct stat sb;
  if ( 0 == ::stat(file_or_dir_name.c_str(), &sb) )    {
    if ( (sb.st_mode&S_IFMT) == S_IFDIR )   {
      DIR* dir = ::opendir(file_or_dir_name.c_str());
      int sc = 1;
      for(struct dirent* ent=::readdir(dir); ent; ent=readdir(dir))  {
	int ret = listen(ent->d_name);
	if ( !ret ) sc = ret;
      }
      ::closedir(dir);
      return sc;
    }
    else if ( (sb.st_mode&S_IFMT) == S_IFLNK )   {
      char path[PATH_MAX];
      int  nbytes = ::readlink(file_or_dir_name.c_str(), path, sizeof(path));
      if ( nbytes == -1 )   {
	::lib_rtl_output(LIB_RTL_OS,"Failed to read symlink %s [%s].",
			 file_or_dir_name.c_str(), error(errno).c_str());
	return 0;
      }
      /// Resolve possibly sub-sequent symlinks
      return listen(path);
    }
    else if ( (sb.st_mode&S_IFMT) == S_IFIFO )   {
      return listen_fifo(file_or_dir_name);
    }
    else if ( (sb.st_mode&S_IFMT) == S_IFREG )   {
      return listen_file(file_or_dir_name);
    }
    return 0;
  }
  else if ( this->create_fifo )    {
    const auto& fname = file_or_dir_name;
    int ret = ::mkfifo(fname.c_str(), 0777);
    if ( ret == -1 )   {
      ::lib_rtl_output(LIB_RTL_ERROR,"+++ Failed to create fifo: %s [%s]",
		       fname.c_str(), error(errno).c_str());
      return 0;
    }
    ::chmod(fname.c_str(), 0777);
    return listen_fifo(file_or_dir_name);
  }
  return ENOENT;
}

/// Base consumer overload: Shutdown processing
void ConsumerImp::shut_down()   {
  std::lock_guard<std::mutex> lock(m_file_mutex);
  m_file_name.clear();
  m_file_desc.clear();
  for(size_t i=0; i<m_file_poll.size(); ++i)   {
    int fd = m_file_poll[i].data.fd;
    ::epoll_ctl(epollfd, EPOLL_CTL_DEL, fd, &m_file_poll[i]);
    ::close(fd);
  }
  m_file_poll.clear();
}

/// Empty file buffer and issue processing on end-of-line
int ConsumerImp::read_file(int fd, size_t idx, int num_bytes)    {
  int sc;
  char text[518];
  size_t len;
  std::string& buff = m_file_buff[idx];
  if ( num_bytes > (int)buff.capacity() )    {
    buff.reserve(num_bytes+1024);
  }
  for( int i=0; i < num_bytes; ++i )    {
    sc = ::read(fd, text, sizeof(text)-6);
    if ( sc >  0 )   {
      text[sc] = 0;
      for(char* p=text; *p; ++p)   {
	buff += *p;
	if ( *p == '\n' )   {
	  len = buff.length();
	  if ( len > 2 && buff[0] != '{' && buff[1] != '"' )   {
	    // Not a json message: Need to escape
	    char output_buffer[2*OUTPUT_MAX_LEN];
	    std::size_t out_len = 0;
	    out_len = ::str_escape_json(output_buffer,
					sizeof(output_buffer),
					buff.c_str(), len-1);
	    output_buffer[out_len] = 0;
	    this->process_message(output_buffer, out_len);
	  }
	  else   {
	    this->process_message((char*)buff.c_str(), len-1);
	  }
	  buff = "";
	}
      }
      i += sc;
    }
    else if ( sc <= 0 && errno == EINTR  )
      continue;
    else if ( sc == 0 && errno == EAGAIN )
      continue;
    else {
      return -1;
    }
  }
  return 1;
}

static int term_handler(void*         user_context,
			int           sig_number,
			siginfo_t* /* info */,
			void*      /* ptr */)
{
  ::lib_rtl_output(LIB_RTL_ALWAYS,"+++ Handling termination signal %d", sig_number);
  long* run = (long*)user_context;
  *run = 0;
  ::lib_rtl_sleep(800);
  ::_exit(0);
  return LIB_RTL_SIGNAL_HANDLED;
}

#if 0
static void sigio_handler(int        /* sig_number */,
			  siginfo_t* /* info */,
			  void*      /* ptr */)
{
}
#endif

/// Run the Rologger instance forever
void ConsumerImp::run()   {
  long             run = 1;
 
  if ( !::lib_rtl_subscribe_signal(SIGQUIT, &run, term_handler) )   {
    ::lib_rtl_output(LIB_RTL_OS,"Failed to prepare the signal action for SIGTERM.");
    return;
  }
  if ( !::lib_rtl_subscribe_signal(SIGTERM, &run, term_handler) )   {
    ::lib_rtl_output(LIB_RTL_OS,"Failed to prepare the blocking signal process mask.");
    return;
  }
  if ( !::lib_rtl_subscribe_signal(SIGIO, nullptr, ConsumerImp::sig_stop) )   {
    ::lib_rtl_output(LIB_RTL_OS,"Failed to prepare the blocking signal process mask.");
    return;
  }
#if 0
  sigset_t         sig_mask;
  sigset_t         orig_sig_mask;
  struct sigaction sig_action;
  ::memset (&sig_action, 0, sizeof(sig_action));
  sig_action.sa_sigaction = sigio_handler;
  ::sigemptyset(&sig_mask);
  if ( ::sigprocmask(SIG_BLOCK, &sig_mask, &orig_sig_mask) < 0 ) {
  }
#endif
  int tmo = 200;
  while ( run )    {
    struct epoll_event epoll;
    int nclients = ::epoll_wait(epollfd, &epoll, 1, tmo);
    if ( nclients > 0 )    {
      int fd = epoll.data.fd;
      int bytes_queued = 0;
      if( ::ioctl(fd, FIONREAD, &bytes_queued) < 0)   {
	///
      }
      if ( bytes_queued > 0 )   {
	Files::iterator i = m_files.find(fd);
	if ( i != m_files.end() )   {
	  this->read_file(fd, (*i).second, bytes_queued);
	}
      }
    }
  }
}

/// rologger namespace declaration
namespace rologger  {

  template <> std::unique_ptr<Consumer::Implementation>
  Consumer::create<StdinConsumer>(int argc, char** argv)  {
    auto consumer = std::make_unique<ConsumerImp>();
    bool print_help = false;

    for( int i=0; i<argc; ++i )   {
      char c = argv[i][0];
      if ( c == '-' )   {
	c = argv[i][1];
	switch(c)   {
	case 'h':
	  print_help = true;
	  break;
	case 'c':
	  consumer->create_fifo = true;
	  break;
	case 'f':
	  consumer->listen(argv[++i]);
	  break;
	case 't':
	  consumer->poll_timeout = ::atoi(argv[++i]);
	  break;
	case 'i':
	  consumer->monitor_interval = 1000 * ::atoi(argv[++i]);
	  break;
	default:
	  break;
	}
	continue;
      }
    }
    if ( print_help )   {
      ::printf("     -t <msecs>     Poll timeout in milli-seconds.            \n"
	       "     -i <msecs>     Monitor time interval in milli seconds.   \n");
      ::exit(0);
    }
    return consumer;
  }
}
