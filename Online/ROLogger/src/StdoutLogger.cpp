//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  ROLogger
//--------------------------------------------------------------------------
//
//  Package    : ROLogger
//
//  Description: Readout message logging in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================

/// Framework include files
#include "StdoutWriter.h"
#include <RTL/Logger.h>
#include <RTL/graphics.h>
#include <RTL/strdef.h>
#include <RTL/rtl.h>

/// C/C++ include files
#include <vector>
#include <cerrno>
#include <cstring>

#include <NET/IOPortManager.h>
#include <SCR/scrdef.h>

extern "C" int  scrc_check_key_buffer (char *buffer);
extern "C" void scrc_resetANSI();
extern "C" void scrc_setANSI();

namespace  {
  /// ______________________________________________________________________
  int help()   {
    ::printf("run_output_logger -opt [-opt]                                    \n"
	     "   Run terminal client to display messages from rologger         \n"
	     "                                                                 \n"
	     "     -h             Print this help.                             \n"
	     "     -I             Connect to stdin as message source and       \n"
	     "                    forward the message to terminal.             \n"
	     "     -L             Connect to all services on the main pub.     \n"
	     "                    instance on ecs03 (DEFAULT).                 \n"
	     "     -N             Run node logger (pick up messages from one   \n"
	     "                    node directly. Do not connect to main        \n"
	     "                    publishing instance.                         \n"
	     "     -C             Use colors for output printing.              \n"
	     "     -b <broker>    Set DIM message broker.                      \n"
	     "     -O <level>     Set RTL output level (1...4).                \n"
	     "     -P             Print statistics                             \n"
	     "     -R             Print RAW messages                           \n"
	     "     -m <reg-ex>    Add node matching regular expression.        \n"
	     "     -u <reg-ex>    Add UTGID matching regular expression.       \n"
	     "     -M <reg-ex>    Require messages matching regular expression.\n"
	     "     -V <reg-ex>    Veto messages matching regular expression.   \n"
	     "     DIM consumer options:                                       \n"
	     "     -D <node-name> Consumer option: Set DNS node (if applicable)\n"
	     "     -p <dns-port>  Consumer option: Set DNS port (if applicable)\n"
	     "     -S <match>     DIM service node match /ROLOG/<match>/LOGS   \n"
	     "     -i <number>    DIM Monitor interval [seconds]               \n"
	     );
    return 0;
  }
  /// ______________________________________________________________________
#define KEY_BUF_SIZE 80
  static char Key_buffer[KEY_BUF_SIZE+2] = "";
  static int  Key_ptr   = 0;
  /// ______________________________________________________________________
  int ast_keyboard (void* /* par */)   {
    int fd = ::fileno(stdin);
    int status = IOPortManager::getAvailBytes(fd);
    if( status > 0 )  {
      do  {
	char Last_char = 0;
	IOPortManager::getChar(fd, &Last_char);
	if( Key_ptr >= KEY_BUF_SIZE )  {
	  status = 0;
	}
	else if( Last_char )  {
	  Key_buffer[Key_ptr] = Last_char;
	  Key_ptr++;
	  Key_buffer[Key_ptr] = 0;
	  if( int Last_key_stroke = ::scrc_check_key_buffer (Key_buffer) > 0 )  {
	    Key_ptr = 0;
	    Key_buffer[Key_ptr] = 0;
	    if( Last_char == 5  || Last_key_stroke == SCR::CTRL_E )  {
	      // CTRL-E
	      ::exit(0);
	    }
	    else if( Last_char == SCR::LINEFEED || Last_char == 77 )  {
	      // ENTER || KPD ENTER
	      ::write(fileno(stdout), "\n", 1);
	    }
	    else if( Last_char == 18 )  {
	      // CTRL-R
	      ::write(fileno(stdout), "\033[2J", ::strlen("\033[2J"));
	      graphics::clear_screen();
	    }
	    else if( Last_char == '=' || Last_char == '-' || Last_char == ' ' || Last_char == '_' )  {
	      size_t rows = 0, cols = 0;
	      int fdesc = fileno(stdout);
	      ::fflush(stdout);
	      graphics::consolesize(&rows, &cols);
	      graphics::bold();
	      graphics::white();
	      graphics::nounderline();
	      if ( Last_char == ' ' ) graphics::inverse();
	      ::fflush(stdout);
	      for(size_t i=0; i<cols; ++i)
		::write(fdesc, &Last_char, 1);
	      graphics::plain();
	      graphics::white();
	      graphics::bg_black();
	      ::fflush(stdout);
	      ::write(fdesc, "\n", 1);
	      ::fflush(stdout);
	    }
	  }
	}
      } while( status > 0);
    }
    return 1;
  }
}

/// ________________________________________________________________________
extern "C" int run_output_logger (int argc, char **argv) {
  using namespace rologger;
  std::string             broker, value;
  std::vector<char*>      args;
  std::vector<std::regex> node_matches;
  std::vector<std::regex> utgid_matches;
  std::vector<std::regex> message_veto;
  std::vector<std::regex> message_matches;
  std::string             tag_match;
  int  output_level = LIB_RTL_INFO;
  int  consumer_type =    DIMLOG_CONSUMER;
  bool print_help    =    false;
  bool print_stats   =    false;
  bool print_counter =    false;
  bool print_raw     =    false;
  bool simple_msg    =    false;
  bool have_colors   =    false;
  size_t rows = 0, cols = 0;

  for( int i=0; i<argc; ++i )   {
    char c = argv[i][0];
    if ( c == '-' )   {
      c = argv[i][1];
      switch(c)   {
      case 'b':
	broker = argv[++i];
	break;
      case 'c':
	print_counter = true;
	break;
      case 'C':
	graphics::consolesize(&rows,&cols);
	graphics::bg_black();
	graphics::white();
	for(size_t j=0;j<rows;++j) ::fprintf(stdout,"\n");
	have_colors = true;
	break;
      case 'g':  // Rologger group-id
	args.push_back(argv[i]);
	break;
      case 'h':
	help();
	print_help = true;
	args.push_back(argv[i]);
	break;
      case 's':
	simple_msg = true;
	break;
      case 'O':
	output_level = ::atol(argv[++i]);
	break;
      case 'R':
	::lib_rtl_output(LIB_RTL_ALWAYS,"+++ Print RAW messages.");
	print_raw = true;
	break;
      case 'V':
	value = argv[++i];
	::lib_rtl_output(LIB_RTL_ALWAYS,"+++ Adding message veto:  '%s'",value.c_str());
	message_veto.emplace_back(std::regex(value));
	break;
      case 'm':
        value = RTL::str_lower(argv[++i]);
       	::lib_rtl_output(LIB_RTL_ALWAYS,"+++ Adding node match:    '%s'",value.c_str());
	node_matches.emplace_back(std::regex(value));
	break;
      case 'M':
        value = RTL::str_lower(argv[++i]);
       	::lib_rtl_output(LIB_RTL_ALWAYS,"+++ Adding message match: '%s'",value.c_str());
	message_matches.emplace_back(std::regex(value));
	break;
      case 'u':
        value = argv[++i];
       	::lib_rtl_output(LIB_RTL_ALWAYS,"+++ Adding utgid match:   '%s'",value.c_str());
	utgid_matches.emplace_back(std::regex(value));
	break;
      case 'S':
	args.push_back(argv[i]);
	break;
      case 'L':
	::lib_rtl_output(LIB_RTL_ALWAYS,"+++ Consumer type:        'DIMLOG_CONSUMER'");
	consumer_type = DIMLOG_CONSUMER;
	break;
      case 'N':
	::lib_rtl_output(LIB_RTL_ALWAYS,"+++ Consumer type:        'DIMNODE_CONSUMER'");
	consumer_type = DIMNODE_CONSUMER;
	break;
      case 'I':
	consumer_type = STDIN_CONSUMER;
	break;
      case 'T':
        value = argv[++i];
	tag_match = value;
       	::lib_rtl_output(LIB_RTL_ALWAYS,"+++ Selecting exact tags: '%s'",value.c_str());
	break;
      case 'i':
	[[fallthrough]];
      case 'P':
	print_stats = true;
	[[fallthrough]];
      default:
	args.push_back(argv[i]);
	break;
      }
      continue;
    }
    args.push_back(argv[i]);
  }

  RTL::Logger::install_rtl_printer(output_level);
  /// Attach exit handler
  if ( have_colors )  {
    IOPortManager(0).addEx(0, ::fileno(stdin), ast_keyboard, nullptr);
    ::scrc_setANSI();
    graphics::plain();
    graphics::white();
    graphics::bg_black();
    ::write(fileno(stdout), "\033[2J", ::strlen("\033[2J"));
    ::printf(" CTRL-E                 to exit  \n");
    ::printf(" CTRL-R                 to clear screen \n");
    ::printf(" SPACE, '-', '_' or '=' to write solid line \n");
    ::printf(" ENTER, KPD-ENTER       for line-feed       \n\n");
    ::fflush(stdout);
  }
  StdoutWriter writer;
  writer.message_veto  = std::move(message_veto);
  writer.node_match    = std::move(node_matches);
  writer.utgid_match   = std::move(utgid_matches);
  writer.message_match = std::move(message_matches);
  writer.print_raw     = print_raw;
  writer.print_stats   = print_stats;
  writer.print_counter = print_counter;
  writer.have_colors   = have_colors;
  writer.simple_msg    = simple_msg;
  writer.select_tag    = tag_match;
  writer.initialize();

  auto imp = Consumer::create(consumer_type, args);
  if ( !imp.get() )  {
    return EINVAL;
  }
  Consumer     consumer(std::move(imp));
  if ( print_help )   {
    return 0;
  }
  consumer.add_writer(&writer);
  if ( !consumer.listen(broker) )    {
    return EINVAL;
  }
  ///
  consumer.run();
  return 0;
}
