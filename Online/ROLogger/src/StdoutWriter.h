//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  ROLogger
//--------------------------------------------------------------------------
//
//  Package    : ROLogger
//
//  Description: Readout message logging in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================
#ifndef ROLOGGER_STDOUTWRITER_H
#define ROLOGGER_STDOUTWRITER_H

/// Framework include files
#include "LogServer.h"

/// C/C++ include files
#include <vector>
#include <regex>

/// rologger namespace declaration
namespace rologger  {

  /// Terminal logger class
  /**
   *
   *   \author  M.Frank
   *   \version 1.0
   */
  class StdoutWriter : public Writer  {
  public:
    typedef std::vector<std::regex> Matches;
    Matches node_match;
    Matches utgid_match;
    Matches message_veto;
    Matches message_match;
    std::string select_tag;
    bool    print_counter = false;
    bool    simple_msg    = false;
    bool    have_colors   = false;

    void print_msg(const char* time_stamp,
		   const char* message,
		   const char* systag,
		   const char* utgid,
		   const char* host,
		   const char* physical,
		   const char* counter);
  public:
    /// Default constructor
    StdoutWriter() = default;
    /// Default destructor
    virtual ~StdoutWriter() = default;
    /// Second level object initialization
    virtual int initialize()  override;
    /// Handler for all messages
    virtual void handle_payload(const char* topic,
				char* payload, std::size_t plen,
				char* key, std::size_t klen)  override;
    /// Print raw message
    virtual void print_raw_message(const char*, std::size_t) const override {}
  };
}
#endif // ROLOGGER_STDOUTWRITER_H
