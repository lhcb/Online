//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  ROMon
//--------------------------------------------------------------------------
//
//  Package    : ROMon
//
//  Description: Readout monitoring in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================
#ifndef ROMON_COLLECTIONS_H
#define ROMON_COLLECTIONS_H 1

#include "CPP/Collections.h"

/*
 *   ROMon namespace declaration
 */
namespace ROMon {

  using Online::FixItems;
  using Online::VarItems;
  using Online::TimeStamp;
  using Online::_firstUpdate;
  using Online::_lastUpdate;
}

#endif /* ROMON_COLLECTIONS_H */

