//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  ROMon
//--------------------------------------------------------------------------
//
//  Package    : ROMon
//
//  Description: Readout monitoring in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================
#ifndef ROMON_FMCMONLISTENER_H
#define ROMON_FMCMONLISTENER_H 1

/// Framework includes
#include <ROMon/RODimListener.h>

/// C/C++ include files
#include <regex>

/*
 *   ROMon namespace declaration
 */
namespace ROMon {

  /**@class FMCMonListener FMCMonListener.h ROMon/FMCMonListener.h
   *
   *   DIM FSM status listener to collect the full information from nodes
   *
   *   @author M.Frank
   */
  class FMCMonListener : public RODimListener {
  public:
    struct Descriptor {
      int    len;
      int    actual;
      time_t time;
      int    millitm;
      char*  data;
      Descriptor() : len(0), actual(0), time(0), millitm(0), data(0) {}
      void release() { if ( data ) ::free(data); data = 0; }
      void copy(const void* data, size_t len);
    };
  protected:
    /// String matching of services
    std::regex  m_regex;
    std::string m_match;
    std::string m_regex_match;
    std::string m_item;
    int         m_infoTMO;

  public:
    /// Standard constructor
    FMCMonListener(bool verbose=false);
    /// Standard destructor
    virtual ~FMCMonListener();
    /// Change service name matching string
    void setMatch(const std::string& val) {  m_match = val;    }
    /// Access service name match
    const std::string& match() const      {  return m_match;   }
    /// Change service name matching string
    void setRegex(const std::string& val);
    /// Access service name match
    const std::string& regex_match() const{  return m_regex_match;   }
    /// Change service name item string
    void setItem(const std::string& val)  {  m_item = val;     }
    /// Access service name item
    const std::string& item() const       {  return m_item;    }
    /// Add handler for a given message source
    void addHandler(const std::string& node,const std::string& svc) override;
    /// Remove handler for a given message source
    void removeHandler(const std::string& node, const std::string& svc) override;
    /// DimInfoHandler overload
    static void infoHandler(void* tag, void* address, int* size);
  };
}      // End namespace ROMon
#endif /* ROMON_FMCMONLISTENER_H */

