//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  ROMon
//--------------------------------------------------------------------------
//
//  Package    : ROMon
//
//  Description: Readout monitoring in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================
#ifndef ROMON_FARMLINEDISPLAY_H
#define ROMON_FARMLINEDISPLAY_H 1

// Framework includes
#include "ROMon/ClusterLine.h"
#include "ROMon/FarmDisplayBase.h"
/*
 *   ROMon namespace declaration
 */
namespace ROMon {

  /**@class FarmLineDisplay ROMon.h GaudiOnline/FarmLineDisplay.h
   *
   *   Monitoring display for the LHCb storage system.
   *
   *   @author M.Frank
   */
  class FarmLineDisplay : public FarmDisplayBase  {
  protected:
    typedef PartitionListener        Listener;
    typedef std::vector<Listener*>   Listeners;
    typedef std::map<std::string,ClusterLine*> SubDisplays;

    SubDisplays                      m_lines;
    Listeners                        m_listeners;
    std::unique_ptr<InternalDisplay>   m_summaryDisplay;
    std::string                      m_match;

    int                              m_height;
    int                              m_width;
    int                              m_dense;

    ClusterLine*                     m_currentLine;

    /// Keyboard rearm action
    static int key_rearm (unsigned int fac, void* param);
    /// Keyboard action
    static int key_action(unsigned int fac, void* param);

  public:
    /// Standard constructor
    FarmLineDisplay(int argc, char** argv);

    /// Standard destructor
    virtual ~FarmLineDisplay();

    /// Get the name of the currently selected cluster
    std::string selectedCluster() const override;

    /// Get the name of the currently selected cluster and node
    std::pair<std::string,std::string> selectedNode() const override;

    /// Number of sub-nodes in a cluster
    size_t selectedClusterSize() const;

    /// Handle keyboard interrupts
    int handleKeyboard(int key) override;

    /// Get farm <partition>/<display name> from cursor position
    std::string currentCluster()  const override;

    /// Get farm display from cursor position
    ClusterLine* currentDisplay()  const;

    /// Get farm display name from cursor position
    std::string currentDisplayName()  const override;

    /// Accessor to current subfarm display
    ClusterDisplay* subfarmDisplay() const {  return m_subfarmDisplay; }

    /// Set cursor to position
    void set_cursor() override;

    /// Set cursor to position
    void set_cursor(InternalDisplay* d) override
    { this->InternalDisplay::set_cursor(d); }

    /// Connect to data sources
    void connect(const std::string& section, const std::vector<std::string>& farms);

    /// Interactor overload: Display callback handler
    void handle(const CPP::Event& ev) override;
    /// Connect to data resources
    void connect()  override {  InternalDisplay::connect(); }
    /// DIM command service callback
    void update(const void* /* data */)        override { }
    /// Update display content
    void update(const void* data, size_t len)  override { this->InternalDisplay::update(data,len); }
  };
}      // End namespace ROMon
#endif /* ROMON_FARMLINEDISPLAY_H */
