//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  ROMon
//--------------------------------------------------------------------------
//
//  Package    : ROMon
//
//  Description: Readout monitoring in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================
#ifndef ONLINE_ROMON_HELPDISPLAY_H
#define ONLINE_ROMON_HELPDISPLAY_H 1

// Framework includes
#include "ROMon/InternalDisplay.h"
#include "CPP/Interactor.h"

/*
 *   ROMon namespace declaration
 */
namespace ROMon {

  /**@class HelpDisplay ROMon.h GaudiOnline/FarmDisplay.h
   *
   *   Help display. Dislay shows up on CTRL-h
   *   Help content is in $ROMONROOT/doc/farmMon.hlp
   *
   *   @author M.Frank
   */
  class HelpDisplay : public InternalDisplay {
  private:
    /// Private copy constructor
    HelpDisplay(const HelpDisplay&) : InternalDisplay() {}
    /// Private assignment operator
    HelpDisplay& operator=(const HelpDisplay&) { return *this; }
  public:
    /// Initializing constructor with default file
    HelpDisplay(InternalDisplay* parent, const std::string& title, const std::string& tag);
    /// Initializing constructor with file name
    HelpDisplay(InternalDisplay* parent, const std::string& title, const std::string& tag, const std::string& fname);
    /// Standard destructor
    virtual ~HelpDisplay() {}
    /// Initialize help display with data content
    void _init(const std::string& tag, const std::string& fname);
    /// Update display content
    void update(const void*) override {}
    /// Update display content
    void update(const void* data, size_t len)  override { this->InternalDisplay::update(data,len); }
  };

}      // End namespace ROMon
#endif /* ONLINE_ROMON_HELPDISPLAY_H */
