//============================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//============================================================================
// Name        : DiracMon.cpp
// Author      :
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <sys/socket.h>
#include <linux/netlink.h>
#include <linux/connector.h>
#include <linux/cn_proc.h>
#include <csignal>
#include <cerrno>
#include <stdbool.h>
#include <ctime>
#include <cstring>
#include <cstdlib>
#include <cstdio>

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <list>
#include <map>
#include <set>

#include <dirent.h>
#include <unistd.h>
#include <sys/stat.h>

#include <sys/types.h>
#include <dim/dis.hxx>
#include <RTL/rtl.h>
#include <RTL/strdef.h>

using namespace std;

DimService *StatusService = nullptr;
DimServer *StatusServer = nullptr;
time_t BootTime;
int AccUID;
string account = "lhcbprod";

class Task {
  public:
  int pid { 0 };
  int ppid { 0 };
  string cmdline;
  Task *child { nullptr };
};
class TNode;
typedef map<int, TNode*> NodeMap;
NodeMap Taskmap;
NodeMap OrphMap;
class TNode
{
  public:
    TNode *Parent;
    int Pid;
    int PPid;
    int type;
    time_t starttime;
    list<int> siblings;
    map<int, TNode*> SibMap;
    string cmdline;
    NodeMap *ContMap;
    int NumChilds();
    void AddChild(TNode &n);
    void removeChild(TNode &n, bool del = true);
    TNode();
    TNode(int pid);
    ~TNode();
    TNode(int pid, int Ppid);
    TNode(int pid, int Ppid, NodeMap &map);
    void AddThreads();
    void setCmdline();
    void setCmdline(string c);
    void Print(string &blanks, bool cmd = true);
    int Dump(char* buff, bool nothread = true, bool top = false, int level = -1,
        char del = char(1), int max_level = 25);
    TNode *find(int pid);
    void fillMap(NodeMap &m);
};

string leads;
int getPPID(int, time_t*);
void get_cmd_line(int ipid, string &cmdline);
void ReadDir(string dir, vector<string> &procs);
int getUID(int pid, int &Pid, int &PPid)
{
  std::string filn = "/proc/";
  filn += std::to_string(pid);
  filn += "/status";
  FILE *f = fopen(filn.c_str(), "r");
  char lines[32 * 1024];
  int uid = -1;
  if (f != 0)  {
    fread(lines, 1, sizeof(lines), f);
    fclose(f);
    auto s = RTL::str_split(lines, "\n");
    for (size_t i = 0; i < s.size(); i++)    {
      auto ss = RTL::str_split(s.at(i).c_str(), "\t");
      if (ss.at(0) == "Uid:")      {
        uid = stoi(ss.at(1));
        continue;
      }
      else if (ss.at(0) == "Pid:")     {
        Pid = stoi(ss.at(1));
        continue;
      }
      else if (ss.at(0) == "PPid:")
      {
        PPid = stoi(ss.at(1));
        continue;
      }
    }
  }
  return uid;
}
void findOrphans(int uidpat, std::vector<int> &orphs)
{
  std::string filn = "/proc";
  vector<string> procs;
  procs.clear();
  ReadDir(filn, procs);
  int pid, Pid, PPid;
  if (OrphMap.find(1) == OrphMap.end())
  {
    OrphMap[1] = new TNode(1,0,OrphMap);
  }
  for (size_t i = 0; i < procs.size(); i++)
  {
    pid = stoi(procs[i]);
    int uid = getUID(pid, Pid, PPid);
    if (uid == uidpat)
    {
      if (PPid == 1)
      {
        TNode *n = new TNode(Pid, PPid, OrphMap);
        n->setCmdline();
        orphs.push_back(n->Pid);
      }
    }
  }
  return;
}
void ReadDir(string dir, vector<string> &procs)
{
//  ifstream fin;
  string filepath;
  DIR *dp;
  struct dirent *dirp;
  struct stat filestat;

  dp = opendir(dir.c_str());
  if (dp == NULL)
  {
    return;
  }
  while ((dirp = readdir(dp)))
  {
    filepath = dir + "/" + dirp->d_name;
    string dirn(dirp->d_name);
    // If the file is a directory (or is in some way invalid) we'll skip it
    if (stat(filepath.c_str(), &filestat))
      continue;
    if (dirn.find_first_not_of("0123456789") != string::npos)
      continue;
    if (S_ISDIR(filestat.st_mode))
    {
      procs.push_back(dirn);
    }
    // Endeavor to read a single number from the file and display it
  }
  closedir(dp);
}
enum
{
  TNODE_PROCESS = 1, TNODE_THREAD
};
int TNode::NumChilds()
{
  return siblings.size();
}
void TNode::AddChild(TNode &n)
{
//    printf("Adding PID %d to Parent %d\n", n.Pid,this->Pid);
  siblings.push_back(n.Pid);
  SibMap[n.Pid] = &n;
}
void TNode::removeChild(TNode &n, bool del)
{
//    printf("Removing PID %d from Parent %d\n", n.Pid,Pid);
  ContMap->erase(n.Pid);
  siblings.remove(n.Pid);
  SibMap.erase(n.Pid);

  if (del)
    delete &n;
}
TNode::TNode() :
    Parent(0), Pid(0), PPid(0), starttime(0), ContMap(0)
{
  type = TNODE_PROCESS;
}
TNode::TNode(int pid) :
    Parent(0), Pid(pid), PPid(0), starttime(0), ContMap(0)
{
  type = TNODE_PROCESS;
}
TNode::~TNode()
{
  if (NumChilds() != 0)
  {
//    printf(
//        "FUNNY>>>> deleting TNode (pid %d, ppid %d) while number of siblings is no Zero. Removing siblings...\n",
//        Pid, PPid);
    for (NodeMap::iterator it = SibMap.begin(); it != SibMap.end();
        it = SibMap.begin())
    {
      removeChild(*(*it).second);
    }
  }
  Parent = 0;
}
TNode::TNode(int pid, int Ppid) :
    TNode(pid, Ppid, Taskmap)
{
}
TNode::TNode(int pid, int Ppid, NodeMap &map)
{
  ContMap = &map;
  Pid = pid;
  Parent = 0;
  type = TNODE_PROCESS;
//    printf("New Tree Node with PID %d, Parent PID %d\n",Pid,Ppid);
  map[Pid] = this;
  PPid = Ppid;
  if (PPid == -1)
    return;
  NodeMap::iterator it = map.find(PPid);
  if (it != map.end())
  {
    time_t stt;
    getPPID(pid, &stt);
    starttime = BootTime + stt/100;
    Parent = (*it).second;
    Parent->AddChild(*this);
  }
  else
  {
//      printf("Parent Node (%d) of PID %d not yet in Map. Adding it...\n",PPid, Pid);
    time_t stt;
    int pPPid = getPPID(PPid, &stt);
    if (pPPid == -1)
    {

    }
    Parent = new TNode(PPid, pPPid);
    Parent->starttime = BootTime + stt/100;
    getPPID(pid, &stt);
    starttime = BootTime + stt/100;
    Parent->AddChild(*this);
  }
//    AddThreads()
}
void TNode::AddThreads()
{
  char cpid[128] {""};
//    sprintf(cpid,"%d",Pid);
  string dir = "/proc/";
  dir += cpid;
  dir += "/task";
  vector<string> threads;
  ReadDir(dir, threads);
//    printf ("PID %d Tasks ",Pid);
//    for (size_t i=0;i<threads.size();i++) printf("%s ",threads[i].c_str());
//    printf("\n");
  for (size_t i = 0; i < threads.size(); i++)
  {
    int tid;
    sscanf(threads[i].c_str(), "%d", &tid);
    if ((tid == Pid) || (tid == PPid))
      continue;
    NodeMap::iterator it = ContMap->find(tid);
    if (it == ContMap->end())
    {
      TNode *nod = new TNode(tid, Pid);
      nod->type = TNODE_THREAD;
    }
    else
    {
      AddChild(*(*it).second);
      (*it).second->type = TNODE_THREAD;
    }
  }
}
void TNode::setCmdline()
{
  get_cmd_line(Pid, cmdline);
}
void TNode::setCmdline(string c)
{
  cmdline = c;
}
void TNode::Print(string &blanks, bool cmd )
{
  string l;
  if (cmd)
  {
    printf("%s[%d] %s\n", blanks.c_str(), Pid, cmdline.c_str());
  }
  else
  {
    printf("%s[%d]\n", blanks.c_str(), Pid);
  }
  l = blanks;
  l.append("   ");
  for (list<int>::iterator i = siblings.begin(); i != siblings.end(); i++)
  {
    SibMap[*i]->Print(l, cmd);
  }
}
int TNode::Dump(char* buff, bool nothread, bool top, int level, char del , int max_level)
{
  int ii;
  int s = 0;
  if (level > max_level)
    return s;
  if (nothread && (type == TNODE_THREAD))
    return s;
  if (top)
  {
    s += sprintf(buff + s, "[%d,%ld,%s]", Pid, starttime, cmdline.c_str());
    s += sprintf(buff + s, "%c", del);
  }
  ii = 0;
  for (list<int>::iterator i = siblings.begin(); i != siblings.end(); i++)
  {
    if (nothread && (SibMap[*i]->type == TNODE_THREAD))
      continue;
    if (ii > 0)
    {
//        s+=sprintf(buff+s,"%c",del);
    }
    s += SibMap[*i]->Dump(buff + s, nothread, true, level + 1, del + 1,
        max_level);
    ii++;
//      list<int>::iterator j =i;
//      j++;
//      if (j != siblings.end()) s+=sprintf(buff+s,"%c",del);
  }
//    if (!top) s += sprintf(buff+s,"%c",del);
//    else      s += sprintf(buff+s,"%c",del);
  return s;
}
TNode *TNode::find(int pid)
{
  TNode *ret = 0;
  if (pid == Pid)
  {
    ret = this;
  }
  else
  {

    map<int, TNode*>::iterator i = SibMap.find(pid);
    if (i != SibMap.end())
    {
      ret = (*i).second;
    }
    else
    {
      for (list<int>::iterator j = siblings.begin(); j != siblings.end(); j++)
      {
        ret = SibMap[*j]->find(pid);
        if (ret != 0)
          break;
      }
    }
  }
  return ret;
}
void TNode::fillMap(NodeMap &m)
{
  m[Pid] = this;
  for (list<int>::iterator i = siblings.begin(); i != siblings.end(); i++)
  {
    SibMap[*i]->fillMap(m);
  }
}

//map<int,Task*> Tasks;
//map<int,Task*> All_Tasks;
//set<int> Agent_Tree;
//set<int> AllChilds;

int tmSrv_PID;
static volatile bool need_exit = false;
pthread_mutex_t mtx = PTHREAD_MUTEX_INITIALIZER;

time_t get_Boot_Time()
{
  char line[256 * 4096];
  string sline;
  sline.erase();
  string filen;
  filen.erase();
  filen.append("/proc/stat");
  FILE *f = fopen(filen.c_str(), "r");
  if (f == 0)
    return 0;
//      char *stat = fgets(line,sizeof(line),f);
  size_t cnt = fread(line, 1, sizeof(line), f);
  fclose(f);
  line[cnt] = 0;
  sline.assign(line);
  size_t start;
  size_t end;
  start = sline.find("btime");
  end = sline.find('\n', start);
  char dummy[128];
  time_t btime;
  sscanf(sline.substr(start, end - start + 1).c_str(), "%s %ld", dummy, &btime);
  return btime;
}

void get_cmd_line(int ipid, string &cmdline)
{
  char line[4096];
  char statl[512];
  char pid[128];
  cmdline.erase();
  sprintf(pid, "%d", ipid);
  string filen;
  filen.append("/proc/");
  filen.append(pid);
  filen.append("/cmdline");
  FILE *f = fopen(filen.c_str(), "r");
  if (f == 0)
    return;
//      char *stat = fgets(line,sizeof(line),f);
  size_t cnt = fread(line, 1, sizeof(line), f);
  fclose(f);
  if (cnt == 0)
  {
    string filen;
    filen.append("/proc/");
    filen.append(pid);
    filen.append("/stat");
    FILE *f = fopen(filen.c_str(), "r");
    if (f == 0)
      return;
    //      char *stat = fgets(line,sizeof(line),f);
    cnt = fread(statl, 1, sizeof(statl), f);
    fclose(f);
    char *blk = strchr(statl, ' ');
    blk++;
    char *blk1 = strchr(blk, ' ');
    cnt = blk1 - blk;
    strncpy(line, blk, cnt);
  }
  cmdline.assign(line, cnt);
  for (unsigned int j = 0; j < cmdline.size(); j++)
  {
    if (cmdline[j] == 0)
      cmdline[j] = ' ';
  }
}

int find_tmsrv(char *nam)
{
  vector<string> procs;
  procs.clear();
  ReadDir("/proc", procs);
//  printf("Process ID present\n");
  char line[4096];
  string cmdline;
  for (unsigned int i = 0; i < procs.size(); i++)
  {
    string filen = "/proc/" + procs[i] + "/cmdline";
    FILE *f = fopen(filen.c_str(), "r");
    if (f == 0)
      continue;
    size_t cnt = fread(line, 1, sizeof(line), f);
    fclose(f);
    if (cnt == 0)
      continue;
    cmdline.assign(line, cnt);
    for (unsigned int j = 0; j < cmdline.size(); j++)
    {
      if (cmdline[j] == 0)
        cmdline[j] = ' ';
    }
    if (cmdline.find("tmSrv") == string::npos)
      continue;
    if (cmdline.find(nam) == string::npos)
      continue;
    printf("tmSrv for %s found at PID %s\n", nam, procs[i].c_str());
    int tmSrvPid;
    sscanf(procs[i].c_str(), "%d", &tmSrvPid);
    return tmSrvPid;
  }
//  printf("tmSrv for %s not found\n", nam);
  return -1;
}

/*
 * connect to netlink
 * returns netlink socket, or -1 on error
 */
static int nl_connect()
{
  int rc;
  int nl_sock;
  struct sockaddr_nl sa_nl;

  nl_sock = socket(PF_NETLINK, SOCK_DGRAM, NETLINK_CONNECTOR);
  if (nl_sock == -1)
  {
    perror("socket");
    return -1;
  }

  sa_nl.nl_family = AF_NETLINK;
  sa_nl.nl_groups = CN_IDX_PROC;
  sa_nl.nl_pid = getpid();

  rc = bind(nl_sock, (struct sockaddr *) &sa_nl, sizeof(sa_nl));
  if (rc == -1)
  {
    perror("bind");
    close(nl_sock);
    return -1;
  }
  return nl_sock;
}

namespace
{
  struct __attribute__ ((aligned(NLMSG_ALIGNTO))) NLCN_MSG_MCAST
  {
      struct nlmsghdr nl_hdr;
      struct __attribute__ ((__packed__)) payload
      {
          struct cn_msg cn_msg;
          enum proc_cn_mcast_op cn_mcast;
      } data;
  };
  struct __attribute__ ((aligned(NLMSG_ALIGNTO))) NLCN_MSG_PROCEV
  {
      struct nlmsghdr nl_hdr;
      struct __attribute__ ((__packed__)) payload
      {
          struct cn_msg cn_msg;
          struct proc_event proc_ev;
      } data;
  };
}

/*
 * subscribe on proc events (process notifications)
 */
static int set_proc_ev_listen(int nl_sock, bool enable)
{
  int rc;
  NLCN_MSG_MCAST nlcn_msg;
  memset(&nlcn_msg, 0, sizeof(nlcn_msg));
  nlcn_msg.nl_hdr.nlmsg_len = sizeof(nlcn_msg);
  nlcn_msg.nl_hdr.nlmsg_pid = getpid();
  nlcn_msg.nl_hdr.nlmsg_type = NLMSG_DONE;

  nlcn_msg.data.cn_msg.id.idx = CN_IDX_PROC;
  nlcn_msg.data.cn_msg.id.val = CN_VAL_PROC;
  nlcn_msg.data.cn_msg.len = sizeof(enum proc_cn_mcast_op);

  nlcn_msg.data.cn_mcast = enable ? PROC_CN_MCAST_LISTEN : PROC_CN_MCAST_IGNORE;

  rc = send(nl_sock, &nlcn_msg, sizeof(nlcn_msg), 0);
  if (rc == -1)
  {
    perror("netlink send");
    return -1;
  }

  return 0;
}
static void on_sigint(int)
{
  need_exit = true;
}

static int handle_interrupt(int sig, int flag) {
     int ret;
     struct sigaction act;

     ::sigaction(sig, NULL, &act);
     if (flag)
          act.sa_flags &=  SA_RESTART;
     else
          act.sa_flags |= SA_RESTART;
     ret = sigaction(sig, &act, NULL);
     return ret;
}
/*
 * handle a single process event
 */
void *handle_proc_ev(void *arg)
{
  using proc_enum = decltype(std::declval<proc_event>().what);
  int nl_sock = *(int*) arg;
  int rc;
  NLCN_MSG_PROCEV nlcn_msg;
  signal(SIGINT, &on_sigint);
  handle_interrupt(SIGINT, true);
  pthread_mutex_lock(&mtx);
  pthread_mutex_unlock(&mtx);
  while (!need_exit)
  {
    if (tmSrv_PID == -1)
    {
      tmSrv_PID = find_tmsrv((char*) account.c_str());
      if (tmSrv_PID != -1)
      {
        TNode *tm = new TNode(tmSrv_PID);
        tm->ContMap = &Taskmap;
        Taskmap[tmSrv_PID] = tm;
      }
    }
    rc = recv(nl_sock, &nlcn_msg, sizeof(nlcn_msg), 0);
    pthread_mutex_lock(&mtx);
    if (rc == 0)
    {
      /* shutdown? */
      pthread_mutex_unlock(&mtx);
      return 0;
    }
    else if (rc == -1)
    {
      if (errno == EINTR)
        continue;
      perror("netlink recv");
      pthread_mutex_unlock(&mtx);
      return (void*) -1;
    }
    switch (nlcn_msg.data.proc_ev.what)
    {
      case proc_enum::PROC_EVENT_NONE:
      {
        printf("set mcast listen ok\n");
        break;
      }
      case proc_enum::PROC_EVENT_FORK:
      {
        int ppid = nlcn_msg.data.proc_ev.event_data.fork.parent_tgid;
        int pid = nlcn_msg.data.proc_ev.event_data.fork.child_tgid;
        int tid = nlcn_msg.data.proc_ev.event_data.fork.child_pid;
        int typ = TNODE_PROCESS;
        if (tid != pid)
        {
          ppid = pid;
          pid = tid;
          typ = TNODE_THREAD;
        }
//              printf("fork: parent tid=%d pid=%d (%d) -> child tid=%d pid=%d\n",
//                      nlcn_msg.data.proc_ev.event_data.fork.parent_pid,
//                      nlcn_msg.data.proc_ev.event_data.fork.parent_tgid,tmSrv_PID,
//                      nlcn_msg.data.proc_ev.event_data.fork.child_pid,
//                      nlcn_msg.data.proc_ev.event_data.fork.child_tgid);
        if (Taskmap.find(ppid) == Taskmap.end())
          break;
//          printf("Fork++++++++: TID %d PID %d (PPID %d)\n",tid,pid,ppid);
        TNode *n = new TNode(pid, ppid);
        n->type = typ;
//          printf("Fork-------: TID %d PID %d (PPID %d)\n",tid, pid,ppid);
//          TNode *tms = Taskmap[tmSrv_PID];
//          char b[4096];
//          tms->Dump(b);
//          printf("%s\n",b);
        break;
      }
      case proc_enum::PROC_EVENT_EXEC:
      {
        int pid = nlcn_msg.data.proc_ev.event_data.exec.process_pid;
        NodeMap::iterator it = Taskmap.find(pid);
        if (it == Taskmap.end())
          break;
//          printf("Exec+++++++++: PID %d \n",pid);
        TNode *t = (*it).second;
        t->setCmdline();
//          printf("Exec--------: PID %d (cmdline %s)\n",t->Pid,t->cmdline.c_str());
//          TNode *tms = Taskmap[tmSrv_PID];
//          char b[8*8192];
//          int siz=tms->Dump(b,true,false,0,char(2));
//          b[siz]=0;siz++;
//          StatusService->updateService(b,siz);
//          printf("%s\n",b);
        break;
      }
      case proc_enum::PROC_EVENT_UID:
      {
//                printf("uid change: tid=%d pid=%d from %d to %d\n",
//                        nlcn_msg.data.proc_ev.event_data.id.process_pid,
//                        nlcn_msg.data.proc_ev.event_data.id.process_tgid,
//                        nlcn_msg.data.proc_ev.event_data.id.r.ruid,
//                        nlcn_msg.data.proc_ev.event_data.id.e.euid);
        break;
      }
      case proc_enum::PROC_EVENT_GID:
      {
//                printf("gid change: tid=%d pid=%d from %d to %d\n",
//                        nlcn_msg.data.proc_ev.event_data.id.process_pid,
//                        nlcn_msg.data.proc_ev.event_data.id.process_tgid,
//                        nlcn_msg.data.proc_ev.event_data.id.r.rgid,
//                        nlcn_msg.data.proc_ev.event_data.id.e.egid);
        break;
      }
      case proc_enum::PROC_EVENT_EXIT:
      {
        int pid = nlcn_msg.data.proc_ev.event_data.exit.process_tgid;
        int tid = nlcn_msg.data.proc_ev.event_data.exit.process_pid;
        if (pid == tmSrv_PID)
        {
          vector<int> orphs;
          findOrphans(AccUID, orphs);
          Taskmap.clear();
          tmSrv_PID = -1;
//          break;
        }
        NodeMap::iterator it = Taskmap.find(tid);
        if (it == Taskmap.end())
        {
          it = OrphMap.find(tid);
          if (it == OrphMap.end())
          {
            break;
          }
        }
        TNode *t = (*it).second;
//          printf("---------------EXIT: TID %d, pid=%d exit code: %d cmdline: %s\n",tid,pid,nlcn_msg.data.proc_ev.event_data.exit.exit_code,t->cmdline.c_str());
        TNode *p = t->Parent;
        if (p == 0)
        {
          printf("LOGIC ERROR***** Parent pointer 0\n");
          it = Taskmap.find(t->PPid);
          if (it == Taskmap.end())
          {
            printf("Parent TNode for Pid %d does not exist...\n", t->PPid);
            break;
          }
          else
          {
            p = (*it).second;
          }
        }
        p->removeChild(*t);
//          TNode *tms = Taskmap[tmSrv_PID];
//          char b[8*8192];
//          *b=0;
//          int siz=tms->Dump(b,true,false,0,char(2));
//          b[siz]=0;siz++;
//          StatusService->updateService(b,siz);
//          printf("%s\n",b);
        break;
      }
      default:
      {
        //printf("unhandled proc event\n");
        break;
      }
    }
    pthread_mutex_unlock(&mtx);
  }
  pthread_mutex_unlock(&mtx);
  return 0;
}

int getPPID(int pid, time_t *st)   {
  char fnam[128];
  char stat[4096];
  sprintf(fnam, "/proc/%d/stat", pid);
  FILE *f = fopen(fnam, "r");
  if (f != 0)  {
    void * ret=fgets(stat, sizeof(stat), f);
    fclose(f);
    if (ret == 0)    {
      return -1;
    }
    int ppid;
    auto dstatl = RTL::str_split(stat, " ");
    sscanf(dstatl[3].c_str(), "%d", &ppid);
    sscanf(dstatl[21].c_str(), "%ld", st);
    return ppid;
  }
  else
  {
    return -1;
  }
}

#include "pthread.h"
set<int> AllProcs;
set<int> AllParents;
vector<int> iprocs;
vector<int> ppids;

#include "pwd.h"
int main(int argc, char**argv)   {
  char Empty[1];
  *Empty = 0;
  int nl_sock;
  pthread_t thID;
  vector<string> procs;
  BootTime = get_Boot_Time();
  string ServiceName = "DiracStatus";
  int curarg = 1;
  while (curarg < argc)
  {
    if (strcmp(argv[curarg], "-p") == 0)
    {
      sscanf(argv[curarg + 1], "%d", &tmSrv_PID);
      curarg += 2;
      continue;
    }
    else if (strcmp(argv[curarg], "-n") == 0)
    {
      account = argv[curarg + 1];
      curarg += 2;
      continue;
    }
    else if (strcmp(argv[curarg], "-s") == 0)
    {
      ServiceName = argv[curarg + 1];
      curarg += 2;
      continue;
    }
//    ServiceName = argv[3];
  }
//  curr_agents_pids.insert(tmSrv_PID);
  tmSrv_PID = find_tmsrv((char*) account.c_str());
  struct passwd *pwd = getpwnam(account.c_str());
  AccUID = pwd->pw_uid;
  vector<int> orphs;
  findOrphans(AccUID, orphs);
  int rc = EXIT_SUCCESS;

  nl_sock = nl_connect();
  if (nl_sock == -1)
    exit(EXIT_FAILURE);

  rc = set_proc_ev_listen(nl_sock, true);
  if (rc == -1)
  {
    rc = EXIT_FAILURE;
    close(nl_sock);
    exit(rc);
//      goto out;
  }
//  int status;
  pthread_mutex_lock(&mtx);
  pthread_create(&thID, 0, &handle_proc_ev, (void*) &nl_sock);
  procs.clear();
  ReadDir("/proc", procs);
//  Agent_Tree.insert(tmSrv_PID);
  for (size_t i = 0; i < procs.size(); i++)
  {
    int ip;
    sscanf(procs[i].c_str(), "%d", &ip);
    iprocs.push_back(ip);
    AllProcs.insert(ip);
    time_t starttime;
    int Ppid = getPPID(ip, &starttime);
    if (Ppid == -1)
    {
      continue;
    }
    TNode *nod;
    if (Taskmap.find(ip) == Taskmap.end())
    {
      nod = new TNode(ip, Ppid);
      nod->starttime = BootTime + starttime/100;
    }
    else
    {
      nod = Taskmap[ip];
      nod->starttime = BootTime + starttime/100;
    }
    nod->AddThreads();
    nod->setCmdline();
    if (Ppid == -1)
      continue;
    AllParents.insert(Ppid);
    ppids.push_back(Ppid);
  }
  char b[8 * 8192];
  char b1[8 * 8192];
  TNode *tm;
  int s = 0;
  if (tmSrv_PID != -1)
  {
    tm = Taskmap[tmSrv_PID];
    if (tm == 0)
    {
      tm = new TNode(tmSrv_PID) ;
      Taskmap[tmSrv_PID] = tm;
    }
    tm->ContMap = &Taskmap;
  //  TNode *root=Taskmap[0];
    printf("%d\n", (int) Taskmap.size());
    Taskmap.clear();
    tm->fillMap(Taskmap);
    tm->Print(leads, true);
    *b = 0;
    tm->Dump(b, true, false, 0, char(1), 1);
  }
  b[s] = 0;
  s++;
  string NodeName = RTL::nodeNameShort();
  for (unsigned int i = 0; i < NodeName.size(); i++)
  {
    NodeName[i] = toupper(NodeName[i]);
  }
  string serverName = NodeName + "_" + ServiceName;
  StatusServer = new DimServer();
  StatusServer->autoStartOn();
  StatusServer->start(serverName.c_str());
  StatusService = new DimService(serverName.c_str(), "C", Empty, sizeof(Empty));
  StatusService->updateService(b, s);
//  printf("%s\n",b);
  pthread_mutex_unlock(&mtx);
  while (!need_exit)
  {
    TNode *tms;
    usleep(100000);
    if (tmSrv_PID == -1)
    {
      tmSrv_PID = find_tmsrv((char*) account.c_str());
    }
    if (tmSrv_PID != -1)
    {
      tms = Taskmap[tmSrv_PID];
    }
    else
    {
      tms = 0;
    }
    TNode *Init = OrphMap[1];
    pthread_mutex_lock(&mtx);
    int siz=0;
    if (tms !=0)
    {
      siz = tms->Dump(b1, true, false, 0, char(1), 1);
    }
    siz+= Init->Dump(b1+siz, true, false, 0, char(1), 1);
    pthread_mutex_unlock(&mtx);
    b1[siz] = 0;
    siz++;
    if (strcmp(b, b1) != 0)
    {
      StatusService->updateService(b1, siz);
      memcpy(b, b1, siz);
    }
    int tstat = pthread_tryjoin_np(thID, 0);
    if (tstat != EBUSY)
    {
      exit(0);
    }
  }
  pthread_join(thID, 0);
  set_proc_ev_listen(nl_sock, false);
  return 0;
}
