//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  ROMon
//--------------------------------------------------------------------------
#include "FarmStatus.h"
#include <RTL/rtl.h>
#include <RTL/strdef.h>

#include <algorithm>
#include <iostream>
#include <cstdlib>
#include <cmath>
#include <ctime>

static FILE *outf;
static bool inhibit_act;

FarmStatus::FarmStatus()   {
  static int NoLink { -1  };
  DimInfo *sfinfo  { nullptr };
  string srvcstring;
  char sf[128];

  m_InfoHandler = new StatusInfoHandler(this);
  m_TSInfoHandler = new TaskSummaryInfoHandler(this);
  m_enabledFarm.clear();
  m_TaskDict = new Dictionary("FarmStatus/TaskDictionary");
  m_ConnDict = new Dictionary("FarmStatus/ConnectionDictionary");
  m_ProjDict = new Dictionary("FarmStatus/ProjectDictionary");
  for ( char row='A'; row <= 'E'; row++ )  {
    for ( int rack=1;rack<=10;rack++ )    {
      sprintf(sf,"HLT%c%02d",row,rack);
      srvcstring = "/RO/" + string(sf) + "/TaskSupervisor/Status";
      sfinfo = new DimInfo(srvcstring.c_str(),(void*)(&NoLink),sizeof(int),m_InfoHandler);
      m_infoMap.insert(make_pair(srvcstring,sfinfo));
      srvcstring = "/RO/" + string(sf) + "/TaskSupervisor/Status";
      sfinfo = new DimInfo(srvcstring.c_str(),(void*)(&NoLink),sizeof(int),m_TSInfoHandler);
      m_TaskMap.insert(make_pair(srvcstring,sfinfo));
      m_DimMap.insert( make_pair(sfinfo    ,string(sf)));
      fprintf(outf,"%s\n",sf);
      m_AllpFarms.insert(string(sf));
    }
  }
  char row = 'F';
  for (int rack=1;rack<=12;rack++)  {
    sprintf(sf,"HLT%c%02d",row,rack);
    srvcstring = "/RO/" + string(sf) + "/TaskSupervisor/Status";
    sfinfo = new DimInfo(srvcstring.c_str(),(void*)(&NoLink),sizeof(int),m_InfoHandler);
    m_infoMap.insert(make_pair(srvcstring,sfinfo));
    srvcstring = "/RO/" + string(sf) + "/TaskSupervisor/Status";
    sfinfo = new DimInfo(srvcstring.c_str(),(void*)(&NoLink),sizeof(int),m_TSInfoHandler);
    m_TaskMap.insert(make_pair(srvcstring,sfinfo));
    m_DimMap.insert( make_pair(sfinfo    ,string(sf)));
    fprintf(outf,"%s\n",sf);
    m_AllpFarms.insert(string(sf));
  }
  row = 'M';
  for (int rack=1;rack<=2;rack++)
  {
    sprintf(sf,"HLT%c%02d",row,rack);
    srvcstring = "/RO/" + string(sf) + "/TaskSupervisor/Summary";
    sfinfo = new DimInfo(srvcstring.c_str(),(void*)(&NoLink),sizeof(int),m_InfoHandler);
    m_infoMap.insert(make_pair(srvcstring,sfinfo));
    srvcstring = "/RO/" + string(sf) + "/TaskSupervisor/Status";
    sfinfo = new DimInfo(srvcstring.c_str(),(void*)(&NoLink),sizeof(int),m_TSInfoHandler);
    m_TaskMap.insert(make_pair(srvcstring,sfinfo));
    m_DimMap.insert( make_pair(sfinfo    ,string(sf)));
    fprintf(outf,"%s\n",sf);
    m_AllpFarms.insert(string(sf));
  }
  row = '2';
  for (int rack=1;rack<=48;rack++)  {
    sprintf(sf,"HLT%c%02d",row,rack);
    srvcstring = "/RO/" + string(sf) + "/TaskSupervisor/Summary";
    sfinfo = new DimInfo(srvcstring.c_str(),(void*)(&NoLink),sizeof(int),m_InfoHandler);
    m_infoMap.insert(make_pair(srvcstring,sfinfo));
    srvcstring = "/RO/" + string(sf) + "/TaskSupervisor/Status";
    sfinfo = new DimInfo(srvcstring.c_str(),(void*)(&NoLink),sizeof(int),m_TSInfoHandler);
    m_TaskMap.insert(make_pair(srvcstring,sfinfo));
    m_DimMap.insert( make_pair(sfinfo    ,string(sf)));
    fprintf(outf,"%s\n",sf);
    m_AllpFarms.insert(string(sf));
  }
  vector<string> otherNodes;
  otherNodes.push_back("ECS03");
  otherNodes.push_back("ECS04");
  otherNodes.push_back("MONA08");
  otherNodes.push_back("MONA09");
  otherNodes.push_back("MONA10");
  otherNodes.push_back("STORECTL01");
  otherNodes.push_back("CALD07");

  for (size_t N=0;N<otherNodes.size();N++)    {
    srvcstring = "/RO/";
    srvcstring += otherNodes[N]+"/TaskSupervisor/Summary";
    sfinfo = new DimInfo(srvcstring.c_str(),(void*)(&NoLink),sizeof(int),m_InfoHandler);
    m_infoMap.insert(make_pair(srvcstring,sfinfo));
    srvcstring = "/RO/";
    srvcstring += otherNodes[N]+"/TaskSupervisor/Status";
    sfinfo = new DimInfo(srvcstring.c_str(),(void*)(&NoLink),sizeof(int),m_TSInfoHandler);
    m_TaskMap.insert(make_pair(srvcstring,sfinfo));
    m_DimMap.insert( make_pair(sfinfo    ,string(sf)));
    fprintf(outf,"%s\n",otherNodes[N].c_str());
    m_AllpFarms.insert(otherNodes[N]);
  }
}

void FarmStatus::Analyze()
{
  myActionMap Actions;
  dim_lock();
  osStatus.str("");
  osDetails.str("");
  osStorage.str("");
  m_ConnDict->Update();
  m_TaskDict->Update();
  m_ProjDict->Update();
  for (NodeSet::iterator nit=m_everrecvd.begin();nit != m_everrecvd.end();nit++)
  {
    char Line[1024];
    MyNode *nod;
    string nname= *nit;
    nod = (*m_AllNodes.find(nname)).second;
    vector<int>  badIndices[3];
    badIndices[0].clear();
    badIndices[1].clear();
    badIndices[2].clear();
    int badies = 0;
    if ( m_recvNodes.find(nname) != m_recvNodes.end() )   {
      sprintf(Line,"%s %d/%d/%d|",nname.c_str(),nod->m_state,nod->m_badtasks,nod->m_badconns);
      osStatus  << nname << " "<< nod->m_state << "/" << nod->m_badtasks << "/" << nod->m_badconns << "|";
      osStorage << nname << " "<< nod->m_totDiskSpace << "/" << nod->m_freeDiskSpace
		<< "/" << nod->m_numDisks << "/" << nod->m_numgoodDisks << "|";
      Cluster *clu = this->m_TSInfoHandler->m_cluMap[nod->m_name];
      if (clu == 0) continue;
      Cluster::Node &n = clu->nodes[nod->m_name];
      if ( nod->m_badtasks+nod->m_badconns+n.projects.size() > 0 )     {
        if (nod->m_badconns != 0)   {
          for (size_t k=0;k<n.conns.size();k++)    {
            if (!n.conns[k].second)    {
              badIndices[1].push_back(m_ConnDict->Find(n.conns[k].first));
              badies++;
            }
          }
        }
        if (nod->m_badtasks != 0)     {
          for (size_t k=0;k<n.tasks.size();k++)    {
            if (!n.tasks[k].second)     {
              badIndices[0].push_back(m_TaskDict->Find(n.tasks[k].first));
              badies++;
            }
          }
        }
        for (size_t k=0;k<n.projects.size();k++)    {
          if (!n.projects[k].dataMgr)     {
            badIndices[2].push_back(m_ProjDict->Find(n.projects[k].name));
            badies++;
          }
        }
        if (badies>0)    {
          if ( osDetails.str().size() > 0 )
            osDetails << "|"<<n.name<<" ";
          else
            osDetails << n.name << " ";

          char nums[32];
          for (size_t k = 0;k<3;k++)     {
            if (badIndices[k].size() > 0)   {
              for (size_t l=0;l<badIndices[k].size();l++)    {
                if (l == 0)    {
                  osDetails <<badIndices[k][l];
                  sprintf(nums,"%d",badIndices[k][l]);
                }
                else    {
                  osDetails <<"," << badIndices[k][l];
                  sprintf(nums,",%d",badIndices[k][l]);
                }
              }
            }
            if (k!=2)     {
              osDetails <<"/";
            }
          }
        }
      }
    }
    else  {
      osStatus << nname << " -1/"<< nod->m_badtasks << "/" << nod->m_badconns << "|";
    }
  }
  osStatus  << '\0';
  osDetails << '\0';
  osStorage << '\0';
  m_statusService->setData((void*)osStatus.str().c_str(),osStatus.str().size());
  m_statusService->updateService();
  m_recvNodes.clear();
  m_Nodes.clear();
  m_DetailService->setData((void*)osDetails.str().c_str(),osDetails.str().size());
  m_DetailService->updateService();
  m_StorageService->setData((void*)osStorage.str().c_str(),osStorage.str().size());
  m_StorageService->updateService();
  dim_unlock();
}

StatusInfoHandler::StatusInfoHandler(FarmStatus *e) : m_subfarm(0),m_sfstatus(0)   {
  m_equalizer = e;
}

void StatusInfoHandler::infoHandler()   {
  if ( this->itsService->getSize() > int(sizeof(int)) )   {
    m_sfstatus = (_SFSumm*)this->itsService->getData();
    const _SFSumm* stats = m_sfstatus;
    const _SFSumm::Nodes& nodes = stats->nodes;
    for (_SFSumm::Nodes::const_iterator i = nodes.begin();i!= nodes.end();i=nodes.next(i))  {
      string nname = RTL::str_lower(i->name);
      m_equalizer->m_recvNodes.insert(nname);
      m_equalizer->m_everrecvd.insert(nname);
      myNodeMap::iterator anit = m_equalizer->m_AllNodes.find(nname);
      if (anit == m_equalizer->m_AllNodes.end())   {
	anit = m_equalizer->m_AllNodes.insert(make_pair(nname,new MyNode(nname))).first;
      }
      auto* nod            = anit->second;
      nod->m_state         = i->state;
      nod->m_badconns      = i->numBadConnections;
      nod->m_badtasks      = i->numBadTasks;
      nod->m_subfarm       = stats->name;
      nod->m_numDisks      = i->numDisks;
      nod->m_numgoodDisks  = i->goodDisks;
      nod->m_totDiskSpace  = i->diskSize;
      nod->m_freeDiskSpace = i->diskAvailible;
      NodeSet::iterator en = m_equalizer->m_exclNodes.find(nod->m_name);
      nod->m_excl = (en != m_equalizer->m_exclNodes.end());
    }
  }
}

TaskSummaryInfoHandler::TaskSummaryInfoHandler(FarmStatus *e)   {
  this->m_equalizer = e;
}

void TaskSummaryInfoHandler::infoHandler()   {
  size_t siz = this->itsService->getSize();
  if ( siz > sizeof(int) )    {
    Cluster *clu = nullptr;
    void    *ptr = this->itsService->getData();
    DimInfo *svc = this->getInfo();
    XML::TaskSupervisorParser parser;
    map<DimInfo*,string>::iterator i =m_equalizer->m_DimMap.find(svc);
    if (i != m_equalizer->m_DimMap.end())  {
      map<DimInfo*,Cluster*>::iterator ii = this->m_clusterMap.find(svc);
      if (ii == m_clusterMap.end())    {
	ii = m_clusterMap.insert(make_pair(svc,new Cluster())).first;
      }
      clu = (*ii).second;
      try {
	parser.parseBuffer(i->second,ptr,siz);
	parser.getClusterNodes(*clu);
      } catch(...)  {
	fprintf(outf,"XML Parser Exception\n Data:\n,%s\n",(char*)ptr);
	return;
      }
    }
    for (auto& ni : clu->nodes )  {
      Cluster::Node& n = ni.second;
      m_cluMap[n.name] = clu;
      for (size_t j=0;j<n.connCount;j++)
	m_equalizer->m_ConnDict->Add(n.conns[j].first);
	
      for (size_t j=0;j<n.taskCount;j++)
	m_equalizer->m_TaskDict->Add(n.tasks[j].first);
	
      for (size_t j=0;j<n.projects.size();j++)
	m_equalizer->m_ProjDict->Add(n.projects[j].name);
    }
  }
}

int main(int /* argc   */, char** /* argv */)   {
  const char  *dns = ::lib_rtl_getenv("HLTEQ_DNSNODE");
  const char  *ofile = ::lib_rtl_getenv("HLTEQ_LOGF");
  std::time_t rawtime = ::time(nullptr);

  outf = fopen(ofile ? ofile : "/group/online/FarmStatus.log","a+");
  fprintf(outf,"HLTFileEqualizer starting at... %s",::asctime(::localtime(&rawtime)));

  DimClient::setDnsNode("ecs03");
  inhibit_act = (::lib_rtl_getenv("HLTEQ_INHIBIT_ACT") != 0);
  dns == nullptr ? DimServer::setDnsNode("ecs03") : DimServer::setDnsNode(dns);
  DimServer::start("FarmStatus");
  DimServer::autoStartOn();

  FarmStatus elz;
  elz.m_statusService  = new DimService("FarmStatus/FarmStatus", "C",(void*)"\0",1);
  elz.m_DetailService  = new DimService("FarmStatus/FarmStatusDetails", "C",(void*)"\0",1);
  elz.m_StorageService = new DimService("FarmStatus/StorageStatus", "C",(void*)"\0",1);
  fflush(outf);
  while (1)    {
    sleep (30);
    elz.Analyze();
  }
  return 0;
}


