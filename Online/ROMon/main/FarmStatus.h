//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
/*
 * HLTFileEqualizer.h
 *
 *  Created on: Jun 8, 2012
 *      Author: Beat Jost
 */

#ifndef FARMSTATUS_H
#define FARMSTATUS_H

#include <map>
#include <string>
#include <list>
#include <set>
#include <dim/dic.hxx>
#include <dim/dis.hxx>
#define MBM_IMPLEMENTATION
#include <ROMon/ROMon.h>
#include <ROMon/CPUMon.h>
#include <cstdio>
#include <ctime>
#include <ROMon/TaskSupervisorParser.h>
#include <sstream>

using namespace ROMon;
using namespace std;

class MyNode    {
  public:
    string m_name;
    string m_subfarm;
    int m_state;
    int m_badtasks;
    int m_badconns;
    int m_numDisks;
    int m_numgoodDisks;
    float m_totDiskSpace;
    float m_freeDiskSpace;
    bool m_excl;
    MyNode(const string& n)    {
      m_name = n;
      m_state = 0;
      m_badtasks = 0;
      m_badconns = 0;
      m_numDisks = 0;
      m_numgoodDisks = 0;
      m_totDiskSpace = 0.0;
      m_freeDiskSpace = 0.0;
      m_excl = false;
    };
};
class SFarm
{
  public:
    string m_svcnam;

};

typedef ROMon::SubfarmSummary _SFSumm;
class StatusInfoHandler;
class MBMInfoHandler;
class TaskSummaryInfoHandler;
typedef map<string,MyNode*> myNodeMap;
typedef map<string,list<pair<string,int> > > myActionMap; //list of nodes per subfarm to execute an action on.
typedef set<string> NodeSet;
typedef set<string> StrSet;

class Dictionary   {
  public:
    vector<string> WordList;
    map<string,int> WordMap;
    size_t NumEls;
    DimService *DictService;
    string SvcCont;
    string OCont;
    Dictionary(const string &svc)
    {
      NumEls = 0;
      SvcCont.clear();
      OCont.clear();
      DictService = new DimService(svc.c_str(),"C",(void*)"\0",1);
      WordMap.clear();
      WordList.clear();
    }
    void Add(string &word)
    {
      map<string,int>::iterator i;
      i = WordMap.find(word);
      if (i != WordMap.end())
      {
        return;
      }
      else
      {
        WordList.push_back(word);
        WordMap.insert(make_pair(word,NumEls));
        if (NumEls >0)
        {
          SvcCont += "|"+word+"\0";
        }
        else
        {
          SvcCont += word+"\0";
        }
        NumEls++;
      }
      return;
    }

    void Add(vector<string> &svec)
    {
      size_t i;
      for (i=0;i<svec.size();i++)
      {
        Add(svec[i]);
      }
      return;
    }

    int Find(string &word)
    {
      map<string,int>::iterator i;
      i = WordMap.find(word);
      if (i != WordMap.end())
      {
        return (*i).second;
      }
      return -1;
    }
    void Update()
    {
      if (OCont != SvcCont)
      {
        SvcCont += '\0';
        DictService->setData((void*)SvcCont.c_str(),SvcCont.size());
        DictService->updateService();
        OCont = SvcCont;
      }
    }
};
typedef map<string,float> NodePerfMap;
class FarmStatus    {
  public:
    ostringstream osStatus;
    ostringstream osDetails;
    ostringstream osStorage;
    map<string,SFarm *> m_Farms;
    myNodeMap m_Nodes;
    myNodeMap m_AllNodes;
    map<string,DimInfo*> m_infoMap;
    map<string,DimInfo*> m_TaskMap;
    map<DimInfo*,string> m_DimMap;
    StatusInfoHandler *m_InfoHandler=0;
    TaskSummaryInfoHandler *m_TSInfoHandler;
    DimInfo    *m_DefStateInfo=0;
    DimService *m_statusService=0;
    DimService *m_DetailService=0;
    DimService *m_StorageService=0;
    NodeSet     m_enabledFarm;
    NodeSet     m_recvNodes;
    NodeSet m_everrecvd;
    NodeSet m_BufferrecvNodes;
    NodeSet m_exclNodes;
    set<string> m_AllpFarms;
    NodePerfMap m_nodePerf;
    Dictionary *m_TaskDict=0;
    Dictionary *m_ConnDict=0;
    Dictionary *m_ProjDict=0;
    FarmStatus();
    void Analyze();
    void Dump();
    void BufferDump();
};
typedef ROMon::Nodeset _MBMSF;

class StatusInfoHandler : public DimInfoHandler   {
  public:
    SFarm *m_subfarm;
    _SFSumm *m_sfstatus;
    int m_bufsiz;
    FarmStatus *m_equalizer;
    StatusInfoHandler(FarmStatus *e);
    void infoHandler() override;
};

class TaskSummaryInfoHandler : public DimInfoHandler    {
  public:
    FarmStatus             *m_equalizer;
    map<DimInfo*,Cluster*>  m_clusterMap;
    map<string,Cluster*>    m_cluMap;
    TaskSummaryInfoHandler(FarmStatus *e);
    void infoHandler() override;
};
#endif /* FARMSTATUS_H */
