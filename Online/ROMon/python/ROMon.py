#===============================================================================
#  LHCb Online software suite
#-------------------------------------------------------------------------------
# Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
# All rights reserved.
#
# For the licensing terms see OnlineSys/LICENSE.
#
# Author     : M.Frank
#
#===============================================================================
import sys, PyCintex as Dict
ROMon      = Dict.makeNamespace('ROMon')
gbl        = Dict.makeNamespace('')

