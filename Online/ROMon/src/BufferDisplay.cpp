//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  ROMon
//--------------------------------------------------------------------------
//
//  Package    : ROMon
//
//  Description: Readout monitoring in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================

// Framework include files
#include <ROMon/FarmDisplay.h>
#include <ROMon/CPUMon.h>
#include "ROMonDefs.h"
#include <RTL/strdef.h>
#include <CPP/Event.h>
#include <SCR/scr.h>
#include "sstat.h"

// C++ include files
#include <cstdlib>
#include <sys/time.h>

typedef ROMon::Nodeset::Nodes               Nodes;
typedef ROMon::Node::Buffers                Buffers;
typedef ROMon::MBMBuffer::Clients           Clients;
typedef ROMon::Node::Tasks                  Tasks;
typedef std::vector<std::string>            StringV;

#define DISPLAY_HEIGHT 70
#define DISPLAY_WIDTH  180

ROMon::BufferDisplay::BufferDisplay(InternalDisplay* parent, const std::string& title, const std::string& part) 
  : InternalDisplay(parent, title+" Partition:"+part), m_node(0), m_partition(part)
{
  ::scrc_create_display(&m_display, DISPLAY_HEIGHT, DISPLAY_WIDTH,
			SCR::MAGENTA, SCR::ON, "MBM Monitor display for node:");
  ::gettimeofday(&m_lastStamp.first,0);
  m_lastStamp.second = m_lastStamp.first;
}

void ROMon::BufferDisplay::update(const void* data) {
  using namespace SCR;
  const Nodeset* ns = (const Nodeset*)data;
  char txt[1024], name[128];
  int line = 0, line2 = 0, node = 0;
  if ( nullptr != ns && ns->type == Nodeset::TYPE )  {
    std::string key, nam, bnam_str;
    StringV lines;
    Nodes::const_iterator n;
    std::map<std::string,std::string> entries;
    struct timeval stamp;
    
    bool partitioned = !(m_partition.empty() || m_partition=="*" || RTL::str_upper(m_partition)=="ALL");
    ::gettimeofday(&stamp,0);
    long long int tv_diff  = (stamp.tv_sec-m_lastStamp.first.tv_sec)*1000000 + 
      stamp.tv_usec-m_lastStamp.first.tv_usec;
    long long int cnt_diff = (m_lastStamp.first.tv_sec-m_lastStamp.second.tv_sec)*1000000 + 
      m_lastStamp.first.tv_usec-m_lastStamp.second.tv_usec;
    for (n=ns->nodes.begin(), line=1; n!=ns->nodes.end(); n=ns->nodes.next(n), ++node)  {
      if ( node == m_node ) {
        time_t tim = (*n).time;
        const Buffers& buffs = *(*n).buffers();
        //bool hlt = ::strncmp((*n).name,"hlt",3)==0;
        ::strftime(name,sizeof(name),"%H:%M:%S",::localtime(&tim));
        ::snprintf(txt,sizeof(txt),"MBM Monitor display for node:%s  [%s]  %s %s",
                   (*n).name,name,partitioned ? "Partition:""" : "",
                   partitioned ? m_partition.c_str() : "");
        ::scrc_set_border(m_display, txt, RED|INVERSE);
        for(Buffers::const_iterator ib=buffs.begin(); ib!=buffs.end(); ib=buffs.next(ib))  {
          const Buffers::value_type::Control& c = (*ib).ctrl;
          const char* bnam = (char*)(*ib).name;
          if ( !(!partitioned || ro_match_end(m_partition,bnam)) ) continue;
	  auto iter = m_lastCount.find(bnam);
	  if ( iter == m_lastCount.end() ) 
	    iter = m_lastCount.insert(std::make_pair(bnam,std::make_pair(c.tot_produced,c.tot_produced))).first;
	  long long int bm_count = (*iter).second.first - (*iter).second.second;
	  double tv_freq = double(bm_count)/double(cnt_diff+1)*1e6;
          ::snprintf(name,sizeof(name)," Buffer \"%s\"",bnam);
          ::snprintf(txt,sizeof(txt), "%-26s  Events: Produced:%lld Actual:%lld Seen:%lld Pending:%ld Max:%d",
                     name, c.tot_produced, c.tot_actual, c.tot_seen, c.i_events, c.p_emax);
          ::scrc_put_chars(m_display, txt, NORMAL, ++line,1,1);
	  ::snprintf(txt,sizeof(txt), "%-26s  Space(kB):[Tot:%ld Free:%ld] Users:[Tot:%ld Max:%d]  Freq:%8.3f Hz",
		     "",long((c.bm_size*c.bytes_p_Bit)/1024), long((c.i_space*c.bytes_p_Bit)/1024), 
		     c.i_users, c.p_umax, tv_freq);
          ::scrc_put_chars(m_display, txt, NORMAL, ++line,1,1);
          ::scrc_put_chars(m_display, "  Occupancy [Events]:",NORMAL,++line,1,1);
          draw_bar(29,line, float(c.i_events)/float(c.p_emax),125);
          ::scrc_put_chars(m_display, "            [Space]: ",NORMAL,++line,1,1);
          draw_bar(29,line, float(c.bm_size-c.i_space)/float(c.bm_size),125);
          ::scrc_put_chars(m_display,"",NORMAL,++line,1,1);
	  if ( tv_diff > 3500000 )  {
	    (*iter).second.second = (*iter).second.first;
	    (*iter).second.first  = c.tot_produced;
	  }
        }
	if ( tv_diff > 3500000 )  {
	  m_lastStamp.second = m_lastStamp.first;
	  m_lastStamp.first  = stamp;
	}
        break;
      }
    }
    line2 = line;
    if ( line > 1 ) {
      ::snprintf(txt,sizeof(txt),"%-36s%-5s%7s %4s%12s %-4s %-16s","   Name","Part","PID","State","Seen/Prod","REQ","Buffer");
      ::scrc_put_chars(m_display,txt,INVERSE,++line,1,0);
      ::scrc_put_chars(m_display,txt,INVERSE,line,3+m_display->cols/2,1);
    }
    for (n=ns->nodes.begin(), node=0; n!=ns->nodes.end(); n=ns->nodes.next(n), ++node)  {
      if ( node == m_node ) {
        const Buffers& buffs = *(*n).buffers();
        for(Buffers::const_iterator ib=buffs.begin(); ib!=buffs.end(); ib=buffs.next(ib))  {
          const Clients& clients = (*ib).clients;
          const char* bnam = (const char*)(*ib).name;
          if ( !(!partitioned || ro_match_end(m_partition,bnam)) ) 
            continue;
          for (Clients::const_iterator ic=clients.begin(); ic!=clients.end(); ic=clients.next(ic))  {
            Clients::const_reference c = (*ic);
            const char* cnam = c.name; //(partitioned) ? _procNam(m_partition,c.name) : c.name;
            if ( cnam ) {
              if ( c.type == 'C' )
                ::snprintf(txt,sizeof(txt),"%-36s%-5X%7d C%4s%12ld %c%c%c%c %s",cnam,c.partitionID,c.processID,
                           sstat[(size_t)c.state],c.events,c.reqs[0],c.reqs[1],c.reqs[2],c.reqs[3],bnam);
              else if ( c.type == 'P' )
                ::snprintf(txt,sizeof(txt),"%-36s%-5X%7d P%4s%12ld %4s %s",cnam,c.partitionID,c.processID,
                           sstat[(size_t)c.state],c.events,"",bnam);
              else
                ::snprintf(txt,sizeof(txt),"%-36s%-5X%7d ?%4s%12s %4s %s",cnam,c.partitionID,c.processID,"","","",bnam);
              key = bnam;
              key += c.name;
              entries[key] = txt;
            }
          }
        }
        break;
      }
    }
    lines.clear();
    for(std::map<std::string,std::string>::const_iterator m=entries.begin();m!=entries.end();++m) {
      lines.push_back((*m).second);
    }
    
    for(size_t i=0,len=lines.size(),cnt=len/2+(len%2),j=cnt; i<cnt; ++i, ++j)  {
      if ( j<len ) {
        ::snprintf(name,sizeof(name),"%%-%ds  %%-%ds",m_display->cols/2,m_display->cols/2);
        ::snprintf(txt,sizeof(txt),name,lines[i].c_str(),lines[j].c_str());
      }
      else {
        ::snprintf(name,sizeof(name),"%%-%ds",m_display->cols);
        ::snprintf(txt,sizeof(txt),name,lines[i].c_str());
      }
      ::scrc_put_chars(m_display,txt,NORMAL,++line,1,1);
    }
    if ( line2 <= 1 ) {
      std::time_t t = ::time(0);
      ::scrc_put_chars(m_display,"   Unknown Node. No buffers found.",INVERSE|BOLD,++line,1,1);
      ::strftime(txt,sizeof(txt),"           %H:%M:%S",::localtime(&t));
      ::scrc_put_chars(m_display,txt,INVERSE|BOLD,++line,1,1);
      ::scrc_set_border(m_display,"Unknown Node. No buffers found.",INVERSE|RED|BOLD);
    }
    ::scrc_put_chars(m_display,"",NORMAL,++line,1,1);
    ::scrc_put_chars(m_display,"      << Mouse-Left-Double-Click down here to close the window >>",NORMAL,++line,1,1);
    ::memset(txt,' ',m_display->cols);
    txt[m_display->cols-1]=0;
    while(line<m_display->rows)
      ::scrc_put_chars(m_display,txt,NORMAL,++line,1,1);
  }
}
