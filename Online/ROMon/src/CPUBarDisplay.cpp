//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//=========================================================================
//  ROMon
//-------------------------------------------------------------------------
//
//  Package    : ROMon
//
//  Description: Readout monitoring in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//=========================================================================

// Framework include files
#include "ROMon/FarmDisplay.h"
#include "ROMon/CPUMon.h"
#include "SCR/scr.h"

// C++ include files
#include <cstdlib>

/// Update display content
void ROMon::CPUBarDisplay::updateContent(const CPUfarm& f) {
  using namespace SCR;
  typedef CPUset::Cores _C;
  typedef CPUfarm::Nodes _N;
  char txt[255], text[64];
  int cnt = 0;
  int line = 1;
  time_t t1 = f.time;
  bool show_cores = m_showCores && m_node != "*";
  ::strftime(text,sizeof(text),"%H:%M:%S",::localtime(&t1));
  ::snprintf(txt,sizeof(txt),"      CPU farm:%s %s  [%d nodes]",f.name,text,f.nodes.size());
  ::scrc_set_border(m_display,m_title.c_str(),INVERSE|RED);
  ::scrc_put_chars(m_display,txt,BOLD,++line,3,1);
  ::scrc_put_chars(m_display,"      Mtot:Total memory in MB  Mfree:Free memory in MB"
                   "  Ctxt:Context switch rate in Hz",NORMAL,++line,3,1);
  ::scrc_put_chars(m_display,"      Type 'C' or 'D' to close this window",NORMAL,++line,3,1);
  ::scrc_put_chars(m_display, "", NORMAL, ++line, 1, 1);
  ::snprintf(txt,sizeof(txt)," %-10s %-58s   %-60s","Node",
	     "Average USER CPU usage [%]","Average SYSTEM CPU usage [%]");
  ::scrc_put_chars(m_display,txt,FG_YELLOW|BG_RED|BOLD,++line,1,1);
  ::snprintf(txt,sizeof(txt)," %-15s 0 %%%40s  100 %% %10s 0 %%%40s  100 %%","","","","");
  ::scrc_put_chars(m_display, txt, BOLD, ++line, 1, 1);
  for(_N::const_iterator i=f.nodes.begin(); i!=f.nodes.end(); i=f.nodes.next(i)) {
    const CPUset& cs = (*i);
    if ( m_node == "*" || ::strcasecmp(m_node.c_str(),cs.name) == 0 )   {
      ::snprintf(txt,sizeof(txt),"%9s %6.2f", cs.name, cs.averages.user);
      ::scrc_put_chars(m_display, txt, NORMAL, ++line, 1, 1);
      draw_bar(18, line, cs.averages.user/100e0,   50);
      ::snprintf(txt,sizeof(txt),"%6.2f", cs.averages.system);
      ::scrc_put_chars(m_display, txt, NORMAL, line, 72, 1);
      draw_bar(80, line, cs.averages.system/100e0, 50);
      if ( show_cores )    {
	::scrc_put_chars(m_display, "", NORMAL, ++line, 1, 1);
	::snprintf(txt,sizeof(txt)," %-10s %-58s   %-60s","Core",
		   "User CPU usage [%]","System CPU usage [%]");
	::scrc_put_chars(m_display, txt, FG_YELLOW|BG_RED|BOLD, ++line, 1, 1);
	const _C& cores = cs.cores;
	for(_C::const_iterator ic=cores.begin(); ic!=cores.end(); ic=cores.next(ic)) {
	  const CPU& c = *ic;
	  ::snprintf(txt,sizeof(txt),"Core %3d %6.2f", ++cnt, c.stats.user);
	  ::scrc_put_chars(m_display,txt,NORMAL, ++line, 2, 1);
	  draw_bar(18, line, c.stats.user/100e0,   50);
	  ::snprintf(txt,sizeof(txt),"%6.2f", c.stats.system);
	  ::scrc_put_chars(m_display, txt, NORMAL, line, 72, 1);
	  draw_bar(80, line, c.stats.system/100e0, 50);
	}
	::snprintf(txt,sizeof(txt)," %-15s 0 %%%40s  100 %% %10s 0 %%%40s  100 %%","","","","");
	::scrc_put_chars(m_display, txt, BOLD, ++line, 1, 1);
      }
    }
  }
  if ( 0 == f.nodes.size() ) {
    t1 = ::time(0);
    ::scrc_put_chars(m_display,"",NORMAL,++line,1,1);
    ::strftime(txt,sizeof(txt),"   No CPU information found.         %H:%M:%S",::localtime(&t1));
    ::scrc_put_chars(m_display,txt,INVERSE|BOLD,++line,5,1);
    ::scrc_put_chars(m_display,"",NORMAL,++line,1,1);
    ::scrc_set_border(m_display,m_title.c_str(),INVERSE|RED|BOLD);
  }
}
