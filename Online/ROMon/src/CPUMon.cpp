//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : ROMon
//
//  Author     : Markus Frank
//==========================================================================

/// C/C++ include files
#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <iomanip>

/// Framework include files
#include <ROMon/CPUMon.h>

#include "ROMonDefs.h"

/// Output operator for Memory summary objects
std::ostream& ROMon::operator<<(std::ostream& os, const ROMon::Memory& m) {
  char text[132];
  ::snprintf(text,sizeof(text),"Memory:%d Free:%d Cache:%5d Active:%d Inactive:%d",
             m.memTotal,m.memFree,m.cached,m.active,m.inactive);
  return os << text;
}

/// Output operator for CPU statistics objects
std::ostream& ROMon::operator<<(std::ostream& os, const ROMon::CPU::Stat& s) {
  char text[132];
  ::snprintf(text,sizeof(text),"User:%4.1f%% Sys:%4.1f%% IO:%4.1f%% irq:%4.1f%% sirq:%4.1f%%",
             s.user,s.system,s.iowait,s.IRQ,s.softIRQ);
  return os << text;
}

/// Output operator for CPU objects
std::ostream& ROMon::operator<<(std::ostream& os, const ROMon::CPU& c) {
  char text[132];
  ::snprintf(text,sizeof(text),"    Clock:%5.0f Cache:%5d kB Bogo:%4.2f ",c.clock,c.cache,c.bogomips);
  return os << text << c.stats;
}

/// Output operator for CPUset objects
std::ostream& ROMon::operator<<(std::ostream& os, const ROMon::CPUset& s) {
  int cnt=0;
  os << "  Node:"  << s.name << " Family:" << s.family << " ContextRate:" << s.ctxtRate 
     << " Last update:" << s.time << "." << s.millitm << std::endl
     << "          Memory:" << s.memory << " kB Free:" << s.memfree << " kB Usage:" << s.averages << std::endl;
  for(CPUset::Cores::const_iterator c=s.cores.begin(); c!=s.cores.end(); c=s.cores.next(c))
    os << "   ->     Core:" << std::setw(2) << ++cnt << ": " << *c << std::endl;
  return os;
}

/// Output operator for CPUfarm objects
std::ostream& ROMon::operator<<(std::ostream& os, const ROMon::CPUfarm& f) {
  os << "Farm:"  << f.name << " " << f.nodes.size() << " nodes. Last update:" << f.time << std::endl;
  for(CPUfarm::Nodes::const_iterator n=f.nodes.begin(); n!=f.nodes.end(); n=f.nodes.next(n))
    os << "Node:" << *n << std::endl;
  return os;
}

/// Output operator for Process objects
std::ostream& ROMon::operator<<(std::ostream& os, const ROMon::Process& p) {
  char text[256];
  bool noutgid = ::strncmp(p.utgid,"N/A",3)==0;
  const char *opt = noutgid ? p.cmd : "", *utgid = noutgid ? "N/A: " : p.utgid;
  if ( p.cpu > 99.9 )
    ::snprintf(text,sizeof(text),"    %5d %5d %4.0f %4.1f %6.0f %6.0f %4d %-16s %s%s",
               p.pid,p.ppid,p.cpu,p.mem,p.vsize,p.rss,p.threads,p.owner,utgid,opt);
  else
    ::snprintf(text,sizeof(text),"    %5d %5d %4.1f %4.1f %6.0f %6.0f %4d %-16s %s%s",
               p.pid,p.ppid,p.cpu,p.mem,p.vsize,p.rss,p.threads,p.owner,utgid,opt);
  text[sizeof(text)-1] = 0;
  return os << text;
}

/// Output operator for Procset objects
std::ostream& ROMon::operator<<(std::ostream& os, const ROMon::Procset& s) {
  char text[256];
  ::snprintf(text,sizeof(text),"          %5s %5s %4s %4s %6s %6s %-4s %-16s %s","PID","PPID","%CPU","%MEM","VSize","RSS","#Thr","Owner","UTGID");
  text[sizeof(text)-1] = 0;
  os << "  Node:"  << s.name << " " << s.processes.size() 
     << " Last update:" << s.time << "." << s.millitm << std::endl
     << text << std::endl;
  for(Procset::Processes::const_iterator c=s.processes.begin(); c!=s.processes.end(); c=s.processes.next(c))
    os << "   -> " << *c << std::endl;
  return os;
}

/// Output operator for ProcFarm objects
std::ostream& ROMon::operator<<(std::ostream& os, const ROMon::ProcFarm& f) {
  int cnt = 0;
  os << "Farm:"  << f.name << " " << f.nodes.size() << " nodes. Last update:" << f.time << std::endl;
  for(ProcFarm::Nodes::const_iterator n=f.nodes.begin(); n!=f.nodes.end(); n=f.nodes.next(n))
    os << "   -> Node:" << std::setw(2) << ++cnt << " " << *n << std::endl;
  return os;
}

/// Output operator for Node statistics objects
std::ostream& ROMon::operator<<(std::ostream& os, const ROMon::NodeStats& n) {
  os << *n.cpu() << std::endl << "  " << n.memory << std::endl << *n.procs();
  return os;
}

/// Output operator for disk space statistics objects
std::ostream& ROMon::operator<<(std::ostream& os, const ROMon::Diskspace& n) {
  os << "Local disk:" << n.blockSize << " " << n.numBlocks << "/" << n.freeBlocks << std::endl;
  return os;
}

/// Output operator for HLT statistics objects
std::ostream& ROMon::operator<<(std::ostream& os, const ROMon::DeferredHLTStats& n) {
  os << "   -> Node:" << n.name << std::endl << "  " << n.localdisk << std::endl;
  return os;
}

/// Output operator for HLT subfarm statistics objects
std::ostream& ROMon::operator<<(std::ostream& os, const ROMon::DeferredHLTSubfarmStats& n) {
  os << "Subfarm:" << n.name << std::endl << "  " << std::endl;
  return os;
}

/// Empty constructor
ROMon::Diskspace::Diskspace() {
  reset();
}

/// Reset data content
ROMon::Diskspace* ROMon::Diskspace::reset() {
  ::memset(this,0,sizeof(Diskspace));
  return this;
}

/// Empty constructor
ROMon::CPU::CPU() {
  reset();
}

/// Reset data content
ROMon::CPU* ROMon::CPU::reset() {
  ::memset(this,0,sizeof(CPU));
  return this;
}

/// Default constructor
ROMon::CPUset::CPUset() {
  reset();
}

/// Reset object structure
ROMon::CPUset* ROMon::CPUset::reset() {
  ::memset(this,0,sizeof(CPUset));
  return this;
}

/// Reset object structure
ROMon::CPUfarm* ROMon::CPUfarm::reset() {
  ::memset(this,0,sizeof(CPUfarm));  
  type = TYPE;
  return this;
}

/// Fix-up object sizes
void ROMon::CPUfarm::fixup() {
  type = TYPE;
  totalSize = length();
}

/// Retrieve timestamp of earliest updated node
ROMon::TimeStamp ROMon::CPUfarm::firstUpdate() const {
  return _firstUpdate(nodes);
}

/// Retrieve timestamp of most recent updated node
ROMon::TimeStamp ROMon::CPUfarm::lastUpdate() const {
  return _lastUpdate(nodes);
}

/// Empty constructor
ROMon::Process::Process() {
  reset();
}

/// Reset data content
ROMon::Process* ROMon::Process::reset() {
  ::memset(this,0,sizeof(Process));
  return this;
}

/// Default constructor
ROMon::Procset::Procset() {
  reset();
}

/// Reset object structure
ROMon::Procset* ROMon::Procset::reset() {
  ::memset(this,0,sizeof(Procset));
  return this;
}

/// Reset object structure
ROMon::ProcFarm* ROMon::ProcFarm::reset() {
  ::memset(this,0,sizeof(ProcFarm));  
  type = TYPE;
  return this;
}

/// Fix-up object sizes
void ROMon::ProcFarm::fixup() {
  type = TYPE;
  totalSize = length();
}

/// Retrieve timestamp of earliest updated node
ROMon::TimeStamp ROMon::ProcFarm::firstUpdate() const {
  return _firstUpdate(nodes);
}

/// Retrieve timestamp of most recent updated node
ROMon::TimeStamp ROMon::ProcFarm::lastUpdate() const {
  return _lastUpdate(nodes);
}

/// Reset node structure to allow re-filling
ROMon::NodeStats* ROMon::NodeStats::reset() {
  ::memset(this,0,sizeof(NodeStats)+sizeof(CPUset)+sizeof(Procset));
  type = TYPE;
  return this;
}

/// Access to the CPU part of the node structure
ROMon::CPUset* ROMon::NodeStats::cpu()  const {
  return (CPUset*)(name + sizeof(name));
}

/// Access to the process part of the node structure
ROMon::Procset* ROMon::NodeStats::procs()  const {
  CPUset* c = cpu();
  return (Procset*)(((char*)c) + c->length());
}

/// Fix the lengths before sending. This is the last statement after filling
void ROMon::NodeStats::fixup() {
  type = TYPE;
  cpuSize = cpu()->length();
  procSize = procs()->length();
  totalSize = cpuSize + procSize + sizeof(NodeStats);
}

/// Standard constructor
ROMon::Connection::Connection() {
  reset();
}

/// Reset data structure content
ROMon::Connection* ROMon::Connection::reset() {
  ::memset(node,0,sizeof(node));
  last = 0;
  status = -1;
  return this;
}

/// Standard constructor
ROMon::Connectionset::Connectionset() {
  reset();
}

/// Reset object structure
ROMon::Connectionset* ROMon::Connectionset::reset() {
  ::memset(this,0,sizeof(Connectionset));
  type = TYPE;
  return this;
}

/// Standard constructor
ROMon::NodeSummary::NodeSummary(const std::string& n) {
  reset();
  ::strncpy(name,n.c_str(),sizeof(name));
  name[sizeof(name)-1] = 0;
}

/// Reset data structure content
ROMon::NodeSummary* ROMon::NodeSummary::reset() {
  ::memset(this,0,sizeof(Procset));
  state = DEAD;
  status = BAD;
  return this;
}

/// Default constructor
ROMon::SubfarmSummary::SubfarmSummary(const std::string& n) {
  reset();
  ::strncpy(name,n.c_str(),sizeof(name));
  name[sizeof(name)-1] = 0;
}

/// Reset object structure
ROMon::SubfarmSummary* ROMon::SubfarmSummary::reset() {
  ::memset(this,0,sizeof(SubfarmSummary));
  type = TYPE;
  return this;
}

/// Default constructor
ROMon::DeferredHLTStats::DeferredHLTStats(const std::string& n) {
  reset();
  ::strncpy(name,n.c_str(),sizeof(name));
  name[sizeof(name)-1] = 0;
}

/// Reset object structure
ROMon::DeferredHLTStats* ROMon::DeferredHLTStats::reset() {
  ::memset(this,0,sizeof(DeferredHLTStats));
  type = TYPE;
  return this;
}

/// Access to the buffer part of the node structure
ROMon::DeferredHLTSubfarmStats::Nodes* ROMon::DeferredHLTSubfarmStats::nodes()  const {
  return (Nodes*)(((char*)&runs) + runs.length());
}

/// Fix the lengths before sending. This is the last statement after filling
void ROMon::DeferredHLTSubfarmStats::fixup() {
  type = TYPE;
  runsSize  = runs.length();
  totalSize = runs.data_length() + nodes()->length() + sizeof(DeferredHLTSubfarmStats);
}

/// Reset node structure to allow re-filling
ROMon::DeferredHLTSubfarmStats* ROMon::DeferredHLTSubfarmStats::reset() {
  ::memset(this,0,sizeof(DeferredHLTSubfarmStats)+sizeof(Runs)+sizeof(Nodes));
  type = TYPE;
  return this;
}

/// Retrieve timestamp of earliest updated node
ROMon::TimeStamp ROMon::DeferredHLTSubfarmStats::firstUpdate() const {
  return _firstUpdate(*nodes());
}

/// Retrieve timestamp of most recent updated node
ROMon::TimeStamp ROMon::DeferredHLTSubfarmStats::lastUpdate() const {
  return _lastUpdate(*nodes());
}

