//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  ROMon
//--------------------------------------------------------------------------
//
//  Package    : ROMon
//
//  Description: Readout monitoring in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================

// Framework include files
#include <RTL/strdef.h>
#include <ROMon/CtrlSubfarmDisplay.h>
#include <ROMon/TaskSupervisorParser.h>
#include <dim/dic.hxx>

// C++ include files
#include <cstdlib>
#include <iostream>

#include "ROMonDefs.h"

static void help() {
  std::cout <<"  romon_storage -option [-option]" << std::endl
	    <<"       -h[eaderheight]=<number>     Height of the header        display.                      " << std::endl
	    <<"       -d[elay]=<number>            Time delay in millisecond between 2 updates.              " << std::endl
	    <<"       -s[ervicename]=<name>        Name of the DIM service  providing monitoring information." << std::endl
	    << std::endl;
}


/// Static abstract object creator.
ROMon::ClusterDisplay* ROMon::createCtrlSubfarmDisplay(int width, int height, int posx, int posy, int argc, char** argv) {
  return new CtrlSubfarmDisplay(width,height,posx,posy,argc,argv);
}

/// Standard constructor
ROMon::CtrlSubfarmDisplay::CtrlSubfarmDisplay(int width, int height, int posx, int posy, int argc, char** argv)
  : ClusterDisplay(width,height)
{
  m_position = Position(posx,posy);
  init(argc, argv);
}

/// Standard constructor
ROMon::CtrlSubfarmDisplay::CtrlSubfarmDisplay(int argc, char** argv)   {
  init(argc, argv);
}

void ROMon::CtrlSubfarmDisplay::init(int argc, char** argv)   {
  RTL::CLI cli(argc,argv,help);
  std::string node;
  int hdr_height;
  cli.getopt("headerheight",  1, hdr_height    =   5);
  cli.getopt("delay",         1, m_delay       = 1000);
  cli.getopt("servicename",   1, m_svcName     = "/HLTA01/TaskSupervisor/Status");
  cli.getopt("node",          1, node          = "HLTA01");
  m_readAlways = true;

  if ( !node.empty() ) {
    std::string svc = "HLT/ExcludedNodes/"+RTL::str_upper(node);  
    m_svcID2   = ::dic_info_service((char*)svc.c_str(),MONITORED,0,0,0,excludedHandler,(long)this,0,0);
  }
  setup_window();
  int width    = m_area.width;
  int height   = m_area.height;
  int posx     = m_position.x-2;
  int posy     = m_position.y-2;
  m_nodes      = createSubDisplay(Position(posx,posy+hdr_height),Area(width,height-hdr_height),"Processing Cluster Information");
  m_clusterData.pointer = (char*)&m_cluster;
  showHeader();
  end_update();
}

/// Standard destructor
ROMon::CtrlSubfarmDisplay::~CtrlSubfarmDisplay()  {
  m_clusterData.pointer = 0;
  begin_update();
  delete m_nodes;
  end_update();
}

/// Display the node information
void ROMon::CtrlSubfarmDisplay::showNodes()  {
  char text[1024];
  float GB = 1.0/float(1024*1024*1024);
  Cluster& c = m_cluster;
  MonitorDisplay* disp = m_nodes;
  double gb_total  = 0, gb_avail      = 0, gb_temp = 0;
  std::size_t taskCount = 0, missTaskCount = 0, connCount = 0, missConnCount = 0;
  std::size_t rssCount  = 0, stackCount    = 0, vsizeCount = 0, dev_num = 0, dev_good = 0;
  const char* fmt = " %-12s %3s %8s %5zd/%-4zd %5zd/%-5zd %4ld %5ld %5ld %3.0f %3.0f %-19s %s %s";

  //disp->draw_line_reverse(" ----------------------------------   Cluster information   ----------------------------------");
  disp->draw_line(header_flags,
		  " %-12s %3s %8s    Tasks   Connections %4s %5s %5s %3s %3s %-19s %13s %17s",
		  "","","","RSS","Stack","VSize","CPU","MEM","",""," -------LocalDisks--------");
  disp->draw_line(header_flags,
		  " %-12s %3s %8s found/miss found/miss  %4s %5s %5s %3s %3s %-19s %13s %17s",
		  "Node","","Status","[MB]","[MB]","[MB]","[%]","[%]","Boot time","Last Reading"," GByte    GByte    GByte   #Dev");
  for(Cluster::Nodes::const_iterator i=c.nodes.begin(); i!=c.nodes.end();++i)    {
    const Cluster::Node& n = (*i).second;
    bool ctrl_pc = (0 == ::strcasecmp(n.name.c_str(),c.name.c_str()));
    if ( n.status == "DEAD" ) {
      disp->draw_line_normal(" %-12s %3s %8s %76s",n.name.c_str(),"",n.status.c_str(),n.time.c_str());
    }
    else {
      const char* inc_exc = m_excluded.find(n.name)==m_excluded.end() ? "INC" : "EXC";
      taskCount     += n.taskCount;
      missTaskCount += n.missTaskCount;
      connCount     += n.connCount;
      missConnCount += n.missConnCount;
      rssCount      += int(n.rss/1024);
      stackCount    += int(n.stack/1024);
      vsizeCount    += int(n.vsize/1024);

      if ( !ctrl_pc )  {
	float blk_size = float(n.blk_size != 0 ? n.blk_size : 4096)*GB;
	::snprintf(text,sizeof(text),"%8.0f %8.0f %8.0f %1d/%1d %s",
		   float(n.blk_total)*blk_size, 
		   float(n.blk_temp)*blk_size,
		   float(n.blk_availible)*blk_size,
		   n.dev_num, n.dev_good, (n.dev_num > n.dev_good) ? "Bad Disk(s)" : "");
	dev_num  += n.dev_num;
	dev_good += n.dev_good;
	gb_total += double(n.blk_total)*blk_size;
	gb_avail += double(n.blk_availible)*blk_size;
	gb_temp  += double(n.blk_temp)*blk_size;
      }
      else   {
	::snprintf(text,sizeof(text)," Total     Temp     Free");
      }
      disp->draw_line_normal(fmt,n.name.c_str(),inc_exc,n.status.c_str(),
                             n.taskCount,n.missTaskCount,n.connCount,n.missConnCount,
                             long(n.rss/1024),long(n.stack/1024),long(n.vsize/1024),
                             n.perc_cpu, n.perc_mem, n.boot.c_str(),
                             n.time.length()>10 ? n.time.c_str()+10 : n.time.c_str(),text);
    }
  }
  disp->draw_line_normal("");
  disp->draw_line(summary_flags,
		  " %-12s %3s %8s %5zd/%-4zd %5zd/%-5zd %4d %5d %5d %3s %3s %-19s %13s %8.0f %8.0f %8.0f %1d/%1d",
		  "Total:", "", c.status.c_str(), taskCount, missTaskCount, 
		  connCount, missConnCount, rssCount, stackCount, vsizeCount, "", "", "", "Disk Status:", 
		  gb_total, gb_temp, gb_avail, dev_num, dev_good);

  disp->draw_line_normal("");
  disp->draw_line_normal("");
  for(Cluster::Nodes::const_iterator i=c.nodes.begin(); i!=c.nodes.end();++i) {
    const Cluster::Node& n = (*i).second;
    if ( n.projects.size() > 0 ) {
      disp->draw_line(header_flags,
		      " %-24s %-16s %-14s %-14s %-14s %-14s %-14s",
		      "PVSS Summary/Node:", "Project name","Event Mgr",
		      "Data Mgr","Dist Mgr","FSM Server","<Project State>");
      for(Cluster::Projects::const_iterator q=n.projects.begin(); q != n.projects.end(); ++q)  {
        const Cluster::PVSSProject& p = *q;
        ::snprintf(text,sizeof(text)," %-24s %-16s %-14s %-14s %-14s %-14s %-14s",
                  n.name.c_str(), p.name.c_str(), p.state(p.eventMgr), p.state(p.dataMgr), 
                  p.state(p.distMgr), p.state(p.fsmSrv), p.ok() ? "RUNNING" : "==NOT RUNNING==");
        p.ok() ? disp->draw_line_normal(text) : disp->draw_line_bold(text);
      }
    }
  }
  disp->draw_line_normal("");
  disp->draw_line_normal("");
  disp->draw_line_normal("");
  disp->draw_line_normal("  c or C    ->  Show the CPU information monitor");
  disp->draw_line_normal("  d or D    ->  Show the CPU monitor (bar-format)");
  disp->draw_line_normal("  l or L    ->  Show System info of the subfarm");
  disp->draw_line_normal("  m or M    ->  Show System info of selected node");
  disp->draw_line_normal("  r or R    ->  Show Readout window");
  disp->draw_line_normal("  s or S    ->  Show CPU Usage summary for the whole subfarm");
  disp->draw_line_normal("  p or P    ->  Show tasks with a UTGID");
  disp->draw_line_normal("  w or W    ->  Show WINCC tasks (Controls PCs only) ");
  disp->draw_line_normal("  v or V    ->  Show SYSTEM processes ");
  disp->draw_line_normal("  a or A    ->  Show NON system, NON WINCC, NON utgid processes (The rest)");
  disp->draw_line_normal("  x or X    ->  Show driver processes.");

  disp->draw_line_normal("  CTRL-W    ->  Redraw window");
  disp->draw_line_normal("  CTRL-e    ->  Exit");
  disp->draw_line_normal("  h/H       ->  Help");
}

/// Update header information
void ROMon::CtrlSubfarmDisplay::showHeader()   {
  char text[256];
  text[0] = 0;
  if ( !m_excluded.empty() ) ::snprintf(text,sizeof(text),"%d node(s) excluded", int(m_excluded.size()));    
  //set_header(SCR::YELLOW,"Cluster Information %s", m_svcName.c_str());
  draw_line_normal ("");
  draw_line_reverse("         Task control monitoring on %s   [%s]  %s", 
                    m_cluster.name.c_str(), ::lib_rtl_timestr(), text);
  draw_line_reverse("         Information service:%s data size:%zd", m_svcName.c_str(), m_data.actual);
  draw_line_normal ("");
}

/// DIM command service callback
void ROMon::CtrlSubfarmDisplay::excludedHandler(void* tag, void* address, int* size) {
  if ( address && tag && *size > 0 ) {
    CtrlSubfarmDisplay* disp = *(CtrlSubfarmDisplay**)tag;
    char *p = (char*)address, *end = p+*size;
    std::set<std::string> nodes;
    while(p<end) {
      nodes.insert(RTL::str_upper(p));
      p += (::strlen(p)+1);
    }
    if ( nodes.size() > 0 )  {
      std::lock_guard<std::mutex> lock(disp->m_lock);
      disp->m_excluded = nodes;
    }
  }
}

/// Update all displays
void ROMon::CtrlSubfarmDisplay::update()   {
  try {
    int result = 0;
    XML::TaskSupervisorParser ts;
    {  DimLock lock;
      if ( m_data.actual>0 ) {
        const char* ptr = m_data.data<const char>();
        result = ts.parseBuffer(m_svcName, ptr,::strlen(ptr)+1) ? 1 : 2;
      }
    }
    std::lock_guard<std::mutex> lock(m_lock);
    begin_update();
    m_nodes->begin_update();
    showHeader();
    switch(result) {
    case 0:
      m_nodes->draw_line_normal ("");
      m_nodes->draw_line_bold("   ..... No XML information present .....");
      m_nodes->draw_line_normal ("");
      break;
    case 1:
      m_cluster.nodes.clear();
      ts.getClusterNodes(m_cluster);
      showNodes();
      break;
    case 2:
      m_nodes->draw_line_normal ("");
      m_nodes->draw_line_bold("   ..... Failed to parse XML information .....");
      m_nodes->draw_line_normal ("");
      break;
    default:
      break;
    }
  }
  catch(const std::exception& e) {
    m_nodes->draw_line_normal ("");
    m_nodes->draw_line_bold("   ..... Exception during data parsing: %s",e.what());
    m_nodes->draw_line_normal ("");
  }
  catch(...) {
    m_nodes->draw_line_normal ("");
    m_nodes->draw_line_bold("   ..... Exception during data parsing .....");
    m_nodes->draw_line_normal ("");
  }
  m_nodes->end_update();
  end_update();
}

/// Retrieve cluster name from cluster display
std::string ROMon::CtrlSubfarmDisplay::clusterName() const {
  return m_cluster.name;
}

/// Retrieve node name from cluster display by offset
std::string ROMon::CtrlSubfarmDisplay::nodeName(std::size_t offset) {
  std::size_t cnt = 0;
  const Cluster::Nodes& nodes = m_cluster.nodes;
  for (Cluster::Nodes::const_iterator n=nodes.begin(); n!=nodes.end(); ++n, ++cnt)  {
    if ( cnt == offset ) return std::string((*n).first);
  }
  return "";
}

extern "C" int romon_ctrlsubfarm(int argc,char** argv) {
  ROMon::CtrlSubfarmDisplay disp(argc,argv);
  disp.initialize();
  disp.run();
  return 1;
}
