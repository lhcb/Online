//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//=========================================================================
//  ROMon
//-------------------------------------------------------------------------
//
//  Package    : ROMon
//
//  Description: Readout monitoring in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//=========================================================================

// C++ include files
#include <cstdlib>
#include <sys/timeb.h>

// Framework includes
#include <dim/dic.hxx>
#include <RTL/rtl.h>
#include <RTL/strdef.h>
#include <ROMon/ROMonInfo.h>
#include <ROMon/FMCMonListener.h>
#include "ROMonDefs.h"

/// Copy data to descriptor
void ROMon::FMCMonListener::Descriptor::copy(const void* address, size_t siz) {
  if ( std::size_t(len) < siz ) {
    len = siz+256;
    ::free(data);
    data = (char*)::malloc(len+256);
  }
  actual = siz;
  auto tm = ro_gettime();
  time = tm.first;
  millitm = tm.second;
  ::memcpy(data,address,actual);
}

/// Standard constructor
ROMon::FMCMonListener::FMCMonListener(bool verbose) 
  : RODimListener(verbose), m_infoTMO(10)
{
}

/// Standard destructor
ROMon::FMCMonListener::~FMCMonListener() {
  delete m_dns;
  m_dns = 0;
  DimLock lock;
  for( auto& c : this->m_clients )   {
    Item* it = c.second;
    ::dic_release_service(it->id);
    it->data<Descriptor>()->release();
    it->release();
  }
  this->m_clients.clear();
}

/// Change service name matching string
void ROMon::FMCMonListener::setRegex(const std::string& val)    {
  this->m_regex = std::regex(val);
  this->m_regex_match = val;
}

/// Add handler for a given message source
void ROMon::FMCMonListener::addHandler(const std::string& node,const std::string& svc) {
  DimLock lock;
  Clients::iterator i=m_clients.find(node);
  if ( i == m_clients.end() )  {
    if ( !this->m_regex_match.empty() )   {
      std::smatch sm;
      bool stat = std::regex_match(svc, sm, this->m_regex);
      if ( stat )    {
	Item* itm = Item::create<Descriptor>(this);
	std::string nam = svc;
	if ( !m_item.empty() ) nam += "/" + m_item;
	m_clients[node] = itm;
	itm->id = ::dic_info_service(nam.c_str(),MONITORED,m_infoTMO,0,0,infoHandler,(long)itm,0,0);
	::lib_rtl_output(LIB_RTL_VERBOSE,"[FMCMonListener] %s Create DimInfo: %s@%s id=%d",
			 m_regex_match.c_str(), nam.c_str(), node.c_str(), itm->id);
      }
    }
    else if ( ::str_match_wild(svc.c_str(),m_match.c_str()) )  {
      Item* itm = Item::create<Descriptor>(this);
      std::string nam = svc;
      if ( !m_item.empty() ) nam += "/" + m_item;
      m_clients[node] = itm;
      itm->id = ::dic_info_service(nam.c_str(),MONITORED,m_infoTMO,0,0,infoHandler,(long)itm,0,0);
      ::lib_rtl_output(LIB_RTL_VERBOSE,"[FMCMonListener] %s Create DimInfo: %s@%s id=%d",
		       m_match.c_str(), nam.c_str(), node.c_str(), itm->id);
    }
    ::lib_rtl_output(LIB_RTL_VERBOSE,"[FMCMonListener] %s IGNORE DimInfo: %s@%s",
		     m_match.c_str(), svc.c_str(), node.c_str());
  }
}

/// Remove handler for a given message source
void ROMon::FMCMonListener::removeHandler(const std::string& node, const std::string& svc)   {
  DimLock lock;
  Clients::iterator i=m_clients.find(node);
  if ( i != m_clients.end() ) {
    if ( !this->m_regex_match.empty() )   {
      std::smatch sm;
      bool stat = std::regex_match(svc, sm, this->m_regex);
      if ( stat )    {
	Item* it = (*i).second;
	it->data<Descriptor>()->release();
	::dic_release_service(it->id);
	::lib_rtl_output(LIB_RTL_VERBOSE,"[FMCMonListener] %s Delete DimInfo: %s@%s id=%d",
			 m_match.c_str(), svc.c_str(), node.c_str(), it->id);
	it->release();
	m_clients.erase(i);
      }
    }
    else if ( ::str_match_wild(svc.c_str(),m_match.c_str()) )  {
      Item* it = (*i).second;
      it->data<Descriptor>()->release();
      ::dic_release_service(it->id);
      ::lib_rtl_output(LIB_RTL_VERBOSE,"[FMCMonListener] %s Delete DimInfo: %s@%s id=%d",
		       m_match.c_str(), svc.c_str(), node.c_str(), it->id);
      it->release();
      m_clients.erase(i);
    }
  }
}

/// DimInfo overload to process messages
void ROMon::FMCMonListener::infoHandler(void* tag, void* address, int* size) {
  if ( address && tag && size && *size>0 ) {
    Item* it = *(Item**)tag;
    Descriptor* d = it->data<Descriptor>();
    RODimListener* l = (RODimListener*)it->object;
    d->copy(address,*size);
    if ( l && l->updateHandler() ) l->updateHandler()->update(it);
  }
}
