//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  ROMon
//--------------------------------------------------------------------------
//
//  Package    : ROMon
//
//  Description: Readout monitoring in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================

// Framework include files
#include <ROMon/ClusterDisplay.h>
#include <ROMon/FarmDisplayBase.h>
#include <ROMon/CPUMon.h>
#include <SCR/MouseSensor.h>
#include <CPP/TimeSensor.h>
#include <CPP/IocSensor.h>
#include <RTL/strdef.h>
#include <SCR/scr.h>
#include <WT/wtdef.h>
#include <dim/dic.h>

#include "ROMonDefs.h"

typedef ROMon::Nodeset::Nodes        Nodes;
typedef ROMon::Node::Buffers         Buffers;
typedef ROMon::MBMBuffer::Clients    Clients;
typedef ROMon::Node::Tasks           Tasks;
typedef std::vector<std::string>     StringV;

namespace ROMon {
  InternalDisplay* createBenchmarkDisplay(InternalDisplay* parent,int mode, const std::string& title);
  InternalDisplay* createFarmStatsDisplay(InternalDisplay* parent, const std::string& title);

  ClusterDisplay*  createSubfarmDisplay(int width, int height, int posx, int posy, int argc, char** argv);
  ClusterDisplay*  createHltSubfarmDisplay(int width, int height, int posx, int posy, int argc, char** argv);
  ClusterDisplay*  createCtrlSubfarmDisplay(int width, int height, int posx, int posy, int argc, char** argv);
  ClusterDisplay*  createMonitoringDisplay(int width, int height, int posx, int posy, int argc, char** argv);
}

namespace {
  ScrDisplay* swapMouseSelector(CPP::Interactor* ia, ROMon::ClusterDisplay* from, ROMon::ClusterDisplay* to) {
    if ( from ) {
      SCR::MouseSensor::instance().remove(from->display());
      ScrDisplay* d = dynamic_cast<ScrDisplay*>(from->nodeDisplay());
      if ( d ) SCR::MouseSensor::instance().remove(d->display());
    }
    if ( to ) {
      ScrDisplay* d = dynamic_cast<ScrDisplay*>(to->nodeDisplay());
      SCR::MouseSensor::instance().add(ia,to->display());
      if ( d )  SCR::MouseSensor::instance().add(ia,d->display());
      return d;
    }
    return 0;
  }

  struct DisplayUpdate {
    SCR::Pasteboard* m_pb;
    bool m_flush;
    DisplayUpdate(ROMon::InternalDisplay* d, bool flush=false) : m_pb(d->pasteboard()), m_flush(flush) {
      ::scrc_begin_pasteboard_update(m_pb);
    }
    ~DisplayUpdate() {
      ::scrc_end_pasteboard_update (m_pb);
      if ( m_flush ) ::scrc_fflush(m_pb);
    }
  };
}

ROMon::FarmDisplayBase::FarmDisplayBase()
  : InternalDisplay(0,""), m_subfarmDisplay(0), m_nodeSelector(0),
    m_subDisplayHeight(3), m_anchorX(10), m_anchorY(5), m_mode(HLT_MODE), 
    m_posCursor(0), m_subPosCursor(0), m_reverse(false)
{
  ::dim_init();
}

ROMon::FarmDisplayBase::~FarmDisplayBase()
{
}

/// Show context dependent help window
int ROMon::FarmDisplayBase::showHelpWindow() {
  DisplayUpdate update(this,true);
  std::string input = ::getenv("ROMONDOC") != 0 ? ::getenv("ROMONDOC") : "../doc";
  if ( m_helpDisplay.get() ) {
    SCR::MouseSensor::instance().remove(this,m_helpDisplay->display());
    m_helpDisplay = std::unique_ptr<HelpDisplay>{};
  }
  else if ( m_statsDisplay.get() ) {
    std::string fin = input+"/farmStats.hlp";
    m_helpDisplay = std::make_unique<HelpDisplay>(this,"Help window","stats-subfarm",fin);
  }
  else if ( m_benchDisplay.get() && !(m_mode == CTRL_MODE) ) {
    std::string fin = input+"/benchmark.hlp";
    const char* tag = "benchmark-farm";
    if ( m_mbmDisplay.get() ) tag = "benchmark-node";
    else if ( m_subfarmDisplay ) tag = "benchmark-subfarm";
    m_helpDisplay = std::make_unique<HelpDisplay>(this,"Help window",tag,fin);
  }
  else if ( m_sysDisplay.get() ) 
    m_helpDisplay = std::make_unique<HelpDisplay>(this,"Help window","subfarm_ctrl");
  else if ( m_roDisplay.get() ) 
    m_helpDisplay = std::make_unique<HelpDisplay>(this,"Help window","subfarm");
  else if ( m_mbmDisplay.get() ) 
    m_helpDisplay = std::make_unique<HelpDisplay>(this,"Help window","mbm");
  else if ( m_ctrlDisplay.get() ) 
    m_helpDisplay = std::make_unique<HelpDisplay>(this,"Help window","ctrl");
  else if ( m_procDisplay.get() ) 
    m_helpDisplay = std::make_unique<HelpDisplay>(this,"Help window","procs");
  else if ( m_cpuDisplay.get() ) 
    m_helpDisplay = std::make_unique<HelpDisplay>(this,"Help window","cpu");
  else if ( m_subfarmDisplay && m_mode == CTRL_MODE )
    m_helpDisplay = std::make_unique<HelpDisplay>(this,"Help window","subfarm_ctrl");
  else if ( m_subfarmDisplay )
    m_helpDisplay = std::make_unique<HelpDisplay>(this,"Help window","subfarm");
  else
    m_helpDisplay = std::make_unique<HelpDisplay>(this,"Help window","farm");
  if ( m_helpDisplay.get() ) {
    m_helpDisplay->show(m_anchorY,m_anchorX);
    SCR::MouseSensor::instance().add(this,m_helpDisplay->display());
  }
  return WT_SUCCESS;
}

/// Show subfarm display
int ROMon::FarmDisplayBase::showSubfarm()    {
  std::string dnam = currentDisplayName();
  std::string part_name = currentCluster();

  part_name = part_name.empty() ? m_name : part_name.substr(0,part_name.find("/"));
  if ( m_subfarmDisplay ) {
    DisplayUpdate update(this,true);
    m_nodeSelector = swapMouseSelector(this,m_subfarmDisplay,0);
    m_nodeSelector = swapMouseSelector(this,m_sysDisplay.get(),0);
    m_nodeSelector = swapMouseSelector(this,m_roDisplay.get(),0);
    m_subfarmDisplay->finalize();
    delete m_subfarmDisplay;
    m_subfarmDisplay     = 0;
    m_roDisplay          = std::unique_ptr<ClusterDisplay>{};
    m_sysDisplay         = std::unique_ptr<ClusterDisplay>{};
    m_cpuDisplay         = std::unique_ptr<CPUDisplay>{};
    m_mbmDisplay         = std::unique_ptr<BufferDisplay>{};
    m_procDisplay        = std::unique_ptr<ProcessDisplay>{};
    m_statsDisplay       = std::unique_ptr<InternalDisplay>{};
    m_ctrlDisplay        = std::unique_ptr<CtrlNodeDisplay>{};
    m_benchDisplay       = std::unique_ptr<InternalDisplay>{};
    m_subPosCursor       = 0;
  }
  else if ( !dnam.empty() ) {
    std::string svc = "-servicename="+svcPrefix()+dnam+"/ROpublish";
    std::string part= "-partition="+part_name;
    if ( m_mode == CTRL_MODE ) {
      std::string node = "-node="+RTL::str_upper(dnam);
      svc = "-servicename="+svcPrefix()+RTL::str_upper(dnam)+"/TaskSupervisor/Status";
      const char* argv[] = {"",svc.c_str(), node.c_str(), "-delay=300"};
      m_subfarmDisplay = createCtrlSubfarmDisplay(SUBFARM_WIDTH,SUBFARM_HEIGHT,m_anchorX,m_anchorY,4,(char**)argv);
    }
    else if ( strncasecmp(dnam.c_str(),"mon01",5)==0 && part_name != "ALL" ) {
      std::string relay = "-namerelay="+dnam+"01";
      const char* argv[] = {"", svc.c_str(), part.c_str(), "-delay=300", "-relayheight=12", "-nodeheight=12", relay.c_str()};
      m_subfarmDisplay = createMonitoringDisplay(SUBFARM_WIDTH,SUBFARM_HEIGHT,m_anchorX,m_anchorY,7,(char**)argv);
    }
    else if ( m_mode == HLT_MODE ) {
      const char* argv[] = {"", svc.c_str(), "-delay=300", "-mooresheight=-1", "-nodesheight=35", part.c_str()};
      m_subfarmDisplay = createSubfarmDisplay(SUBFARM_WIDTH,SUBFARM_HEIGHT,m_anchorX,m_anchorY,6,(char**)argv);
    }
    else {
      m_nodeSelector = 0;
      m_subfarmDisplay = 0;
      return WT_SUCCESS;
    }
    m_subfarmDisplay->initialize();
    ::lib_rtl_sleep(200);
    m_nodeSelector = swapMouseSelector(this,m_sysDisplay.get(),m_subfarmDisplay);
    IocSensor::instance().send(this,CMD_UPDATE,m_subfarmDisplay);
    m_subPosCursor = 0;
  }
  return WT_SUCCESS;
}

/// Show window with buffer information of a given node
int ROMon::FarmDisplayBase::showCtrlWindow() {
  DisplayUpdate update(this,true);
  if ( m_ctrlDisplay.get() ) {
    if ( m_helpDisplay.get() ) showHelpWindow();
    SCR::MouseSensor::instance().remove(this,m_ctrlDisplay->display());
    m_ctrlDisplay = std::unique_ptr<CtrlNodeDisplay>{};
  }
  else if ( m_subfarmDisplay || m_sysDisplay ) {
    auto* data = (m_sysDisplay ? m_sysDisplay.get() : m_subfarmDisplay)->data().pointer;
    m_ctrlDisplay = std::make_unique<CtrlNodeDisplay>(this,"CTRL Monitor display");
    m_ctrlDisplay->setNode(m_subPosCursor);
    m_ctrlDisplay->update(data);
    m_ctrlDisplay->show(m_anchorY+5,m_anchorX+12);
    SCR::MouseSensor::instance().add(this,m_ctrlDisplay->display());
  }
  return WT_SUCCESS;
}

/// Show window with buffer information of a given node
int ROMon::FarmDisplayBase::showMbmWindow() {
  DisplayUpdate update(this,true);
  if ( m_mbmDisplay.get() ) {
    if ( m_helpDisplay.get() ) showHelpWindow();
    SCR::MouseSensor::instance().remove(this,m_mbmDisplay->display());
    m_mbmDisplay = std::unique_ptr<BufferDisplay>{};
  }
  else if ( m_roDisplay || m_subfarmDisplay ) {
    auto* data = (m_roDisplay ? m_roDisplay.get() : m_subfarmDisplay)->data().pointer;
    m_mbmDisplay = std::make_unique<BufferDisplay>(this,"MBM Monitor display",m_partition);
    m_mbmDisplay->setNode(m_subPosCursor);
    m_mbmDisplay->update(data);
    m_mbmDisplay->show(m_anchorY+5,m_anchorX+12);
    SCR::MouseSensor::instance().add(this,m_mbmDisplay->display());
  }
  return WT_SUCCESS;
}

/// Show window with processes on a given node
int ROMon::FarmDisplayBase::showProcessWindow(int flag) {
  if ( m_procDisplay.get() ) {
    if ( m_helpDisplay.get() ) showHelpWindow();
    SCR::MouseSensor::instance().remove(this,m_procDisplay->display());
    m_procDisplay = std::unique_ptr<ProcessDisplay>{};
    return WT_SUCCESS;
  }
  std::pair<std::string,std::string> node = selectedNode();
  if ( !node.second.empty() ) {
    m_procDisplay = std::make_unique<ProcessDisplay>(this,node.second,node.first,flag);
    m_procDisplay->show(m_anchorY+2,m_anchorX+12);
    SCR::MouseSensor::instance().add(this,m_procDisplay->display());
  }
  return WT_SUCCESS;
}

/// Show window with CPU information of a given subfarm
int ROMon::FarmDisplayBase::showCpuWindow() {
  DisplayUpdate update(this,true);
  if ( m_cpuDisplay.get() ) {
    if ( m_helpDisplay.get() ) showHelpWindow();
    SCR::MouseSensor::instance().remove(this,m_cpuDisplay->display());
    m_cpuDisplay = std::unique_ptr<CPUDisplay>{};
    return WT_SUCCESS;
  }
  std::pair<std::string,std::string> node = selectedNode();
  if ( node.second.empty() )   {
    node.first = selectedCluster();
    node.second = "*";
  }
  m_cpuDisplay = std::make_unique<CPUDisplay>(this,node.first,node.second);
  m_cpuDisplay->show(m_anchorY,m_anchorX+8);
  SCR::MouseSensor::instance().add(this,m_cpuDisplay->display());
  return WT_SUCCESS;
}

/// Show window with CPU information of a given subfarm
int ROMon::FarmDisplayBase::showCpuBarWindow() {
  DisplayUpdate update(this,true);
  if ( m_cpuDisplay.get() ) {
    if ( m_helpDisplay.get() ) showHelpWindow();
    SCR::MouseSensor::instance().remove(this,m_cpuDisplay->display());
    m_cpuDisplay = std::unique_ptr<CPUBarDisplay>{};
    return WT_SUCCESS;
  }
  std::pair<std::string,std::string> node = selectedNode();
  if ( node.second.empty() )   {
    node.first = selectedCluster();
    node.second = "*";
  }
  m_cpuDisplay = std::make_unique<CPUBarDisplay>(this,node.first,node.second);
  m_cpuDisplay->show(m_anchorY,m_anchorX+8);
  SCR::MouseSensor::instance().add(this,m_cpuDisplay->display());
  return WT_SUCCESS;
}

/// Show window with CPU information of a given subfarm
int ROMon::FarmDisplayBase::showStatsWindow() {
  DisplayUpdate update(this,true);
  std::string cluster_name = selectedCluster();
  if ( m_statsDisplay.get() ) {
    if ( m_helpDisplay.get() ) showHelpWindow();
    SCR::MouseSensor::instance().remove(this,m_statsDisplay->display());
    m_statsDisplay = std::unique_ptr<InternalDisplay>{};
    return WT_SUCCESS;
  }
  if ( !cluster_name.empty() ) {
    m_statsDisplay = std::unique_ptr<InternalDisplay>{createFarmStatsDisplay(this,cluster_name)};
    m_statsDisplay->show(m_anchorY,m_anchorX);
    m_statsDisplay->connect();
    SCR::MouseSensor::instance().add(this,m_statsDisplay->display());
  }
  return WT_SUCCESS;
}

/// Show window with SYSTEM information of a given subfarm
int ROMon::FarmDisplayBase::showSysWindow() {
  DisplayUpdate update(this,true);
  if ( m_sysDisplay.get() ) {
    if ( m_helpDisplay.get() ) showHelpWindow();
    SCR::MouseSensor::instance().remove(this,m_sysDisplay->display());
    m_sysDisplay->finalize();
    m_nodeSelector = swapMouseSelector(this,m_sysDisplay.get(),m_subfarmDisplay);
    m_sysDisplay = std::unique_ptr<ClusterDisplay>{};
    return WT_SUCCESS;
  }
  std::string dnam = RTL::str_upper(selectedCluster());
  if ( !dnam.empty() ) {
    std::string node = "-node="+dnam;
    std::string svc  = "-servicename="+svcPrefix()+dnam+"/TaskSupervisor/Status";
    const char* argv[] = {"", svc.c_str(), node.c_str(), "-delay=300"};
    ClusterDisplay* disp = createCtrlSubfarmDisplay(SUBFARM_WIDTH,SUBFARM_HEIGHT,m_anchorX+3,m_anchorY,3,(char**)argv);
    m_sysDisplay = std::unique_ptr<ClusterDisplay>{disp};
    m_sysDisplay->initialize();
    m_nodeSelector = swapMouseSelector(this,m_subfarmDisplay,m_sysDisplay.get());
    IocSensor::instance().send(this,CMD_UPDATE,m_sysDisplay.get());
  }
  return WT_SUCCESS;
}

/// Show window with SYSTEM information of a given subfarm
int ROMon::FarmDisplayBase::showReadoutWindow() {
  DisplayUpdate update(this,true);
  if ( m_roDisplay.get() ) {
    if ( m_helpDisplay.get() ) showHelpWindow();
    SCR::MouseSensor::instance().remove(this,m_roDisplay->display());
    m_roDisplay->finalize();
    m_nodeSelector = swapMouseSelector(this,m_roDisplay.get(),m_subfarmDisplay);
    m_roDisplay = std::unique_ptr<ClusterDisplay>{};
    return WT_SUCCESS;
  }
  std::string dnam = selectedCluster();
  if ( !dnam.empty() ) {
    std::string svc = "-servicename="+svcPrefix()+RTL::str_upper(dnam)+"/ROpublish";
    std::string part = "-partition="+m_partition;
    const char* argv[] = {"", svc.c_str(), part.c_str(), "-delay=300"};
    ClusterDisplay* disp = createSubfarmDisplay(SUBFARM_WIDTH,SUBFARM_HEIGHT,m_anchorX,m_anchorY,4,(char**)argv);
    m_roDisplay = std::unique_ptr<ClusterDisplay>{disp};
    m_roDisplay->initialize();
    m_nodeSelector = swapMouseSelector(this,m_subfarmDisplay,m_roDisplay.get());
    IocSensor::instance().send(this,CMD_UPDATE,m_roDisplay.get());
  }
  return WT_SUCCESS;
}

/// Show window to run Moore bench marks
int ROMon::FarmDisplayBase::showBenchmarkWindow() {
  DisplayUpdate update(this,true);
  if ( m_benchDisplay.get() ) {
    SCR::MouseSensor::instance().remove(this,m_benchDisplay->display());
    m_benchDisplay = std::unique_ptr<InternalDisplay>{};
  }
  else if ( m_sysDisplay.get() || m_mbmDisplay.get() || m_ctrlDisplay.get() || m_procDisplay.get() ) {
    std::pair<std::string,std::string> node = selectedNode();
    if ( !node.second.empty() ) {
      m_benchDisplay = std::unique_ptr<InternalDisplay>{createBenchmarkDisplay(this,1,node.second)};
      m_benchDisplay->show(m_anchorY+5,m_anchorX+12);
      m_benchDisplay->connect();
      SCR::MouseSensor::instance().add(this,m_benchDisplay->display());
    }
  }
  else if ( m_subfarmDisplay ) {
    std::string cluster_name = selectedCluster();
    if ( !cluster_name.empty() ) {
      m_benchDisplay = std::unique_ptr<InternalDisplay>{createBenchmarkDisplay(this,2,cluster_name)};
      m_benchDisplay->show(m_anchorY+5,m_anchorX+12);
      m_benchDisplay->connect();
      SCR::MouseSensor::instance().add(this,m_benchDisplay->display());
    }
  }
  else {
    m_benchDisplay = std::unique_ptr<InternalDisplay>{createBenchmarkDisplay(this,3,name())};
    m_benchDisplay->show(m_anchorY+5,m_anchorX+12);
    m_benchDisplay->connect();
    SCR::MouseSensor::instance().add(this,m_benchDisplay->display());
  }
  return WT_SUCCESS;
}

/// Handle Mouse interrupt
bool ROMon::FarmDisplayBase::handleMouseEvent(const SCR::MouseEvent* m) {
  if ( m->button == 2 ) {
    IocSensor::instance().send(this,CMD_SHOWHELP,this);
    return true;
  }
  else if ( m->msec != (unsigned int)-1 ) {
    if ( m_helpDisplay.get() )
      IocSensor::instance().send(this,CMD_SHOWHELP,this);
    else if ( m_ctrlDisplay.get() )
      IocSensor::instance().send(this,CMD_SHOWCTRL,this);
    else if ( m_mbmDisplay.get() )
      IocSensor::instance().send(this,CMD_SHOWMBM,this);
    else if ( m_procDisplay.get() )
      IocSensor::instance().send(this,CMD_SHOWPROCS,this);
    else if ( m_cpuDisplay.get() )
      IocSensor::instance().send(this,CMD_SHOWCPU,this);
    else if ( m_statsDisplay.get() )
      IocSensor::instance().send(this,CMD_SHOWSTATS,this);
    else if ( m_sysDisplay.get() )
      IocSensor::instance().send(this,CMD_SHOWSYS,this);
    else
      IocSensor::instance().send(this,CMD_SHOWSUBFARM,this);
    return true;
  }
  return false;
}

/// Handle IOC interrupt
bool ROMon::FarmDisplayBase::handleIocEvent(const CPP::Event& ev) {
  switch(ev.type) {
  case CMD_SHOWSUBFARM:
    showSubfarm();
    return true;
  case CMD_SHOWSYS:
    showSysWindow();
    return true;
  case CMD_SHOWREADOUT:
    showReadoutWindow();
    return true;
  case CMD_SHOWBENCHMARK:
    showBenchmarkWindow();
    return true;
  case CMD_SHOWSTATS:
    showStatsWindow();
    return true;
  case CMD_SHOWCPU:
    showCpuWindow();
    return true;
  case CMD_SHOWCPUBAR:
    showCpuBarWindow();
    return true;
  case CMD_SHOWCTRL:
    showCtrlWindow();
    return true;
  case CMD_SHOWMBM:
    if (m_mode == CTRL_MODE) showCtrlWindow();
    else showMbmWindow();
    return true;
  case CMD_SHOWMBMEX:
    showMbmWindow();
    return true;
  case CMD_SHOWPROCS:
    showProcessWindow(0);
    return true;
  case CMD_SHOWHELP:
    showHelpWindow();
    return true;
  case CMD_SETCURSOR:
    set_cursor();
    return true;
  case CMD_HANDLE_KEY:
    handleKeyboard(int((long)ev.data));
    return true;
  case CMD_NOTIFY: {
    unsigned char* ptr = (unsigned char*)ev.data;
    if ( ptr ) {
      if ( m_benchDisplay.get() ) m_benchDisplay->update(ptr + sizeof(int), *(int*)ptr);
      delete [] ptr;
    }
    return true;
  }
  case CMD_DELETE:
    delete this;
    ::lib_rtl_sleep(200);
    ::exit(0);
    return true;

  default:
    return false;
  }
}

/// Handle keyboard interrupts
int ROMon::FarmDisplayBase::handleKeyboard(int key)    {
  using namespace SCR;
  try {
    switch (key)    {
    case CTRL_W:
      ::scrc_repaint_screen (m_pasteboard);
      break;
    case 'E':
    case CTRL_E:
      delete this;
      ::exit(0);
      break;
    case 'h':
    case 'H':
    case CTRL_H:
      IocSensor::instance().send(this,CMD_SHOWHELP,this);
      break;
    case 'b':
    case 'B':
      IocSensor::instance().send(this,m_roDisplay ? CMD_SHOWMBMEX : CMD_SHOWREADOUT,this);
      break;
    case 'c':
    case 'C':
      IocSensor::instance().send(this,CMD_SHOWCPU,this);
      break;
    case 'd':
    case 'D':
      IocSensor::instance().send(this,CMD_SHOWCPUBAR,this);
      break;
    case 'k':
    case 'K':
      IocSensor::instance().send(this,CMD_SHOWCTRL,this);
      break;
    case 'l':
    case 'L':
      IocSensor::instance().send(this,CMD_SHOWSYS,this);
      break;
    case 'm':
    case 'M':
      IocSensor::instance().send(this,m_sysDisplay ? CMD_SHOWCTRL : CMD_SHOWMBM,this);
      break;
    case '.':
    case KPD_PERIOD:
      IocSensor::instance().send(this,CMD_SHOWMBMEX,this);
      break;
    case 'n':
    case 'N':
      IocSensor::instance().send(this,m_sysDisplay ? CMD_SHOWCTRL : CMD_SHOWSYS,this);
      break;
    case CTRL_L:
      IocSensor::instance().send(this,CMD_SHOWBENCHMARK,this);
      break;
    case 'p':
    case 'P':
      IocSensor::instance().send(this,CMD_SHOWPROCS,this);
      break;
    case 'a':
    case 'A':
      return showProcessWindow(1);
    case 'v':
    case 'V':
      return showProcessWindow(2);
    case 'w':
    case 'W':
      return showProcessWindow(3);
    case 'x':
    case 'X':
      return showProcessWindow(4);
    case CTRL_M:
      break;      
    case 'r':
    case 'R':
      IocSensor::instance().send(this,CMD_SHOWREADOUT,this);
      break;
    case 's':
    case 'S':
      IocSensor::instance().send(this,CMD_SHOWSTATS,this);
      break;
    case RETURN_KEY:
    case ENTER:
      IocSensor::instance().send(this,CMD_SHOWSUBFARM,this);
      break;
    default:
      return WT_ERROR;
    }
  }
  catch(...) {
  }
  set_cursor();
  return WT_SUCCESS;
}
