//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//=========================================================================
//  ROMon
//-------------------------------------------------------------------------
//
//  Package    : ROMon
//
//  Description: Readout monitoring in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//=========================================================================

#define MBM_IMPLEMENTATION
#include <ROMon/ROMon.h>
#include <ROMon/CPUMon.h>
#include <ROMon/Constants.h>
#include <ROMon/FarmStatSrv.h>
#include <CPP/IocSensor.h>
#include <CPP/TimeSensor.h>
#include <CPP/Event.h>
#include <RTL/rtl.h>
#include <RTL/Logger.h>
#include <RTL/strdef.h>
#include <dim/dic.h>
#include <dim/dis.h>

// C++ include files
#include <cstdio>
#include <cstdlib>
#include <cstdarg>
#include <iostream>
#include <algorithm>

#include "ROMonDefs.h"

// Max. 15 seconds without update allowed
static int UPDATE_TIME_MAX = 15;

typedef std::vector<std::string> StringV;

static std::string s_svcPrefix = "/";

static void help() {
  std::cout << "  romon_farmSummary -option [-option]" << std::endl
	    << "       -all                         Show all subfarms." << std::endl
	    << "       -p[artition]=<name>          Partition name providing monitoring information." << std::endl
	    << std::endl;
}

ROMon::SubfarmStatCollector::SubfarmStatCollector(FarmStatSrv* parent, const std::string& title) 
  : m_parent(parent), m_name(title)
{
  m_mbm = m_cpu = 0;
  m_mbmSvc = m_cpuSvc = 0;
  m_lastUpdate  = time(0);
  m_cpuChanged  = false;
  m_mbmChanged  = false;
  m_mbmData = m_name+":()";
  m_cpuData = m_name+":()";
}

ROMon::SubfarmStatCollector::~SubfarmStatCollector() {
  disconnect();
}

/// Connect to publishing dim service
void ROMon::SubfarmStatCollector::connect() {
  std::string svc, svc0 = s_svcPrefix+RTL::str_upper(m_name);
  if ( m_parent->haveMBM() ) {
    svc = svc0 + "/ROpublish";
    m_mbm = ::dic_info_service(svc.c_str(),MONITORED,0,0,0,mbmHandler,(long)this,0,0);
    svc = m_parent->name() + "/" + RTL::str_upper(m_name) + "/MBM";
    m_mbmSvc = ::dis_add_service(svc.c_str(),(char*)"C",0,0,feedString,(long)&m_mbmData);
  }
  if ( m_parent->haveCPU() ) {
    svc = svc0 + "/ROpublish/CPU";
    m_cpu = ::dic_info_service(svc.c_str(),MONITORED,0,0,0,cpuHandler,(long)this,0,0);
    svc = m_parent->name() + "/" +RTL::str_upper(m_name) + "/CPU";
    m_cpuSvc = ::dis_add_service(svc.c_str(),(char*)"C",0,0,feedString,(long)&m_cpuData);
  }
  ::lib_rtl_output(LIB_RTL_DEBUG,"FarmStatSrv> Connected to subfarm: %s",name().c_str());
}

void ROMon::SubfarmStatCollector::disconnect() {
  if ( m_mbm != 0 ) {
    ::dic_release_service(m_mbm);
    m_mbm = 0;
  }
  if ( m_mbmSvc != 0 ) {
    ::dis_remove_service(m_mbmSvc);
    m_mbmSvc = 0;
  }
  if ( m_cpu != 0 ) {
    ::dic_release_service(m_cpu);
    m_cpu = 0;
  }
  if ( m_cpuSvc != 0 ) {
    ::dis_remove_service(m_cpuSvc);
    m_cpuSvc = 0;
  }
}

/// Check monitor for errors
void ROMon::SubfarmStatCollector::check(time_t now) {
  if ( now - lastUpdate() > 5*UPDATE_TIME_MAX ) {
    m_mbmData = m_name+":Timeout";
    m_cpuData = m_name+":Timeout";
  }
}

/// Update monitoring content
void ROMon::SubfarmStatCollector::updateCPU(const CPUfarm& f) {
  int cnt;
  char text[1024];
  CPUset::Cores::const_iterator ic;
  std::string result = m_name+":(";

  for(CPUfarm::Nodes::const_iterator i=f.nodes.begin(); i!=f.nodes.end(); i=f.nodes.next(i)) {
    const CPUset& cs = (*i);
    const CPU::Stat& avg = cs.averages;
    ::snprintf(text,sizeof(text),"[%s:{Avg:%.2f#%.2f#%.2f#%.2f#%.2f}",
	       cs.name,avg.user,avg.system,avg.nice,avg.idle,avg.iowait);
    result += text;
    for(ic=cs.cores.begin(),cnt=0; ic!=cs.cores.end(); ic=cs.cores.next(ic),++cnt) {
      const CPU& c = *ic;
      ::snprintf(text,sizeof(text),"{Core%02d:%.2f#%.2f#%.2f#%.2f#%.2f}",
		 cnt,c.stats.user,c.stats.system,c.stats.nice,c.stats.idle,c.stats.iowait);
      result += text;
    }
    result += "}]";
  }
  result += ")";
  m_lastUpdate = time(0);
  m_cpuChanged = result != m_cpuData;
  if ( m_cpuChanged ) m_cpuData = result;
  ::lib_rtl_output(LIB_RTL_DEBUG,"FarmStatSrv> Updated CPU information of subfarm:%s",name().c_str());
  IocSensor::instance().send(m_parent,CMD_CHECK,this);
}

/// Update monitoring content
void ROMon::SubfarmStatCollector::updateMBM(const Nodeset& ns) {
  char text[1024];
  std::string result = m_name+":(";
  for(Nodeset::Nodes::const_iterator n=ns.nodes.begin(); n!=ns.nodes.end(); n=ns.nodes.next(n))  {
    const Node::Buffers& buffs = *(*n).buffers();
    result += "[";
    result += (*n).name;
    result += ":";
    for(Node::Buffers::const_iterator ib=buffs.begin(); ib!=buffs.end(); ib=buffs.next(ib))  {
      const MBMBuffer::Control& ctrl = (*ib).ctrl;
      ::snprintf(text,sizeof(text),"{%s:%d#%d#%ld#%ld#%ld#%ld}",(*ib).name,
                int((ctrl.bm_size*ctrl.bytes_p_Bit)/1024),int((ctrl.i_space*ctrl.bytes_p_Bit)/1024),
                long(ctrl.p_emax),long(ctrl.i_events),
                long(ctrl.p_umax),long(ctrl.i_users));
      result += text;
    }
    result += "]";
  }
  result += ")";
  m_lastUpdate = time(0);
  m_mbmChanged = result != m_mbmData;
  if ( m_mbmChanged ) m_mbmData = result;
  ::lib_rtl_output(LIB_RTL_DEBUG,"FarmStatSrv> Updated MBM information of subfarm:%s",name().c_str());
  IocSensor::instance().send(m_parent,CMD_CHECK,this);
}

/// DIM command service callback
void ROMon::SubfarmStatCollector::mbmHandler(void* tag, void* address, int* size) {
  if ( address && tag && *size > 0 ) {
    SubfarmStatCollector* disp = *(SubfarmStatCollector**)tag;
    const Nodeset* ns = (const Nodeset*)address;
    if ( ns->type == Nodeset::TYPE ) disp->updateMBM(*ns);
  }
}

/// DIM command service callback
void ROMon::SubfarmStatCollector::cpuHandler(void* tag, void* address, int* size) {
  if ( address && tag && *size > 0 ) {
    SubfarmStatCollector* disp = *(SubfarmStatCollector**)tag;
    const CPUfarm* cs = (const CPUfarm*)address;
    if ( cs->type == CPUfarm::TYPE ) disp->updateCPU(*cs);
  }
}

/// DIM callback on dis_update_service
void ROMon::SubfarmStatCollector::feedString(void* tag, void** buf, int* size, int* ) {
  std::string* s = *(std::string**)tag;
  *size = s->length()+1;
  *buf = (void*)s->c_str();
}

/// Publish summary information
void ROMon::SubfarmStatCollector::publish() {
  ::lib_rtl_output(LIB_RTL_DEBUG,"FarmStatSrv> Publishing data to DIM. MBM:%s CPU:%s",
                   m_mbmChanged ? "YES" : "NO",m_cpuChanged ? "YES" : "NO");
  if ( m_mbmChanged ) ::dis_update_service(m_mbmSvc);
  if ( m_cpuChanged ) ::dis_update_service(m_cpuSvc);
}

/// Interactor overload: Monitor callback handler
void ROMon::SubfarmStatCollector::handle(const CPP::Event& ev)    {
  switch(ev.eventtype) {
  case IocEvent: {
    switch(ev.type) {
    case CMD_UPDATE:
      publish();
      break;
    default:
      break;
    }
    break;
  }
  default:
    break;
  }
}

/// Standard constructor
ROMon::FarmStatSrv::FarmStatSrv(int argc, char** argv) : m_dns(0), m_name("/FarmStats")
{
  std::string service, in_node="ecs03", out_node="ecs03", fifo;
  RTL::CLI cli(argc,argv,help);
  cli.getopt("source",    2, in_node);
  cli.getopt("target",    2, out_node);
  cli.getopt("match",     2, m_match = "*");
  cli.getopt("service",   2, m_name);
  cli.getopt("prefix",    2, s_svcPrefix);
  m_haveMBM = cli.getopt("mbm",2) != 0;
  m_haveCPU = cli.getopt("cpu",2) != 0;

  if ( m_name[0] != '/' ) m_name = '/'+m_name;
  RTL::Logger::install_log(RTL::Logger::log_args(LIB_RTL_WARNING));
  ::dic_set_dns_node((char*)in_node.c_str());
  ::dis_set_dns_node((char*)out_node.c_str());
  RTL::Logger::print_startup("FarmStatSrv");
  if ( m_match == "*" )
    ::lib_rtl_output(LIB_RTL_INFO,"MBM/CPU monitoring enabled on all known subfarms ");
  else
    ::lib_rtl_output(LIB_RTL_INFO,"MBM/CPU monitoring enabled on all known subfarms matching '%s'",m_match.c_str());
  m_dns = ::dic_info_service("DIS_DNS/SERVER_LIST",MONITORED,0,0,0,dnsDataHandler,(long)this,0,0);
  service = "/"+RTL::str_upper(RTL::nodeNameShort())+m_name;
  ::dis_start_serving(service.c_str());
  ::lib_rtl_sleep(1000);
  TimeSensor::instance().add(this,10,(void*)CMD_SHOW);
}

/// Standard destructor
ROMon::FarmStatSrv::~FarmStatSrv()  {  
  if ( 0 != m_dns ) ::dic_release_service(m_dns);
  m_dns = 0;
  for( auto& mon : m_farmMonitors )
    delete mon.second;
  m_farmMonitors.clear();
  ::lib_rtl_output(LIB_RTL_INFO,"Farm stat collectors deleted and resources freed...");
}

/// DIM command service callback
void ROMon::FarmStatSrv::dnsDataHandler(void* tag, void* address, int* size) {
  if ( address && tag && *size > 0 ) {
    FarmStatSrv* disp = *(FarmStatSrv**)tag;
    disp->updateDns(address);
  }
}

/// DIM command service callback
void ROMon::FarmStatSrv::updateDns(const void* address) {
  char *msg = (char*)address;
  std::string svc, node;
  size_t idx, idq;
  switch(msg[0]) {
  case '+':
    getServiceNode(++msg,svc,node);
    idx = svc.find("/ROpublish");
    idq = svc.find("/HLT");
    if ( idx != std::string::npos && idq == 0 ) {
      std::string f = svc.substr(1,idx-1);
      if ( ::strcase_match_wild(f.c_str(),m_match.c_str()) ) {
        IocSensor::instance().send(this,CMD_ADD,new std::string(f));
      }
    }
    break;
  case '-':
    break;
  case '!':  // Service in error
    break;
  default:
    if ( *(int*)msg != *(int*)"DEAD" )  {
      char *at, *p = msg, *last = msg;
      auto farms = std::make_unique<Farms>();
      farms->push_back("");
      while ( last != 0 && (at=strchr(p,'@')) != 0 )  {
        last = strchr(at,'|');
        if ( last ) *last = 0;
        getServiceNode(p,svc,node);
        idx = svc.find("/ROpublish");
        idq = svc.find("/HLT");
        if ( idx != std::string::npos && idq == 0 ) {
          std::string f = svc.substr(1,idx-1);
          if ( ::strcase_match_wild(f.c_str(),m_match.c_str()) ) {
            farms->push_back(f);
          }
        }
        p = last+1;
      }
      if ( !farms->empty() ) {
        IocSensor::instance().send(this,CMD_CONNECT,farms.release());
      }
    }
    break;
  }
}

/// Interactor overload: Monitor callback handler
void ROMon::FarmStatSrv::handle(const CPP::Event& ev) {
  std::lock_guard<std::mutex> lock(m_lock);
  switch(ev.eventtype) {
  case TimeEvent:
    if(ev.timer_data == (void*)CMD_SHOW)IocSensor::instance().send(this,CMD_SHOW,(void*)0);
    return;
  case IocEvent:
    switch(ev.type) {
    case CMD_SHOW: {
      for(SubfarmMonitors::iterator k=m_farmMonitors.begin();k!=m_farmMonitors.end();++k)
        (*k).second->publish();
      TimeSensor::instance().add(this,10,(void*)CMD_SHOW);
      return;
    }
    case CMD_ADD: {
      std::unique_ptr<std::string> val{ev.iocPtr<std::string>()};
      if ( find(m_farms.begin(),m_farms.end(),*val.get()) == m_farms.end() ) {
        m_farms.push_back(*ev.iocPtr<std::string>());
        connect(m_farms);
      }
      return;
    }
    case CMD_CONNECT: {
      m_farms.clear();
      m_farms.assign(ev.iocPtr<StringV>()->begin()+1,ev.iocPtr<StringV>()->end());
      //m_farms = *ev.iocPtr<std::vector<std::string> >();
      connect(m_farms);
      delete ev.iocPtr<std::vector<std::string> >();
      return;
    }
    case CMD_CHECK: {
      std::time_t now = ::time(0);
      for( auto& k : m_farmMonitors )
        k.second->check(now);
      return;
    }
    case CMD_DELETE:
      delete this;
      ::lib_rtl_sleep(200);
      ::exit(0);
      return;
    default:
      break;
    }
    break;
  default:
    break;
  }
}

void ROMon::FarmStatSrv::connect(const std::vector<std::string>& farms) {
  SubfarmMonitors copy;
  ::lib_rtl_output(LIB_RTL_INFO,"FarmStatSrv> Total number of subfarms:%d",int(farms.size()));
  for( const auto& farm : farms )   {
    auto k = m_farmMonitors.find(farm);
    if ( k == m_farmMonitors.end() ) {
      auto* s = new SubfarmStatCollector(this,farm);
      copy.emplace(farm, s);
      s->connect();
    }
    else {
      copy.insert(*k);
      m_farmMonitors.erase(k);
    }
  }
  for( auto& k : m_farmMonitors )
    delete k.second;
  m_farmMonitors = copy;
  ::dis_start_serving((char*)name().c_str());
}

extern "C" int romon_farmstatsrv(int argc,char** argv) {
  int level = LIB_RTL_INFO;
  RTL::CLI cli(argc,argv,help);
  cli.getopt("printlevel", 2, level);
  ROMon::FarmStatSrv* disp = new ROMon::FarmStatSrv(argc,argv);
  IocSensor::instance().run();
  delete disp;
  return 1;
}
