//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  ROMon
//--------------------------------------------------------------------------
//
//  Package    : ROMon
//
//  Description: Readout monitoring in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================

// Framework include files
#include <RTL/rtl.h>
#include <SCR/scr.h>
#include <ROMon/HelpDisplay.h>
#include "ROMonDefs.h"

// C++ include files
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <iostream>

// Initializing constructor with default file
ROMon::HelpDisplay::HelpDisplay(InternalDisplay* parent, const std::string& title, const std::string& tag) 
  : InternalDisplay(parent,title)
{
  std::string input = ::lib_rtl_getenv("ROMONDOC") != 0 ? ::lib_rtl_getenv("ROMONDOC") : "../doc";
  std::string fin = input+"/farmMon.hlp";
  _init(tag,fin);
}

// Initializing constructor with file name
ROMon::HelpDisplay::HelpDisplay(InternalDisplay* parent, const std::string& title, const std::string& tag, const std::string& fin) 
  : InternalDisplay(parent,title)
{
  _init(tag,fin);
}

// Initialize help display with data content
void ROMon::HelpDisplay::_init(const std::string& tag, const std::string& fin)
{
  bool use = false, isHeader=false;
  std::string s, start="<"+tag+">", stop = "</"+tag+">";
  std::string head = m_title + ": " + fin;
  std::ifstream in(fin.c_str());

  ::scrc_create_display(&m_display,60,132,SCR::NORMAL,SCR::ON,head.c_str());
  ::scrc_put_chars(m_display,"Hit CTRL-H to hide the display",SCR::BOLD,2,2,1);
  for(int line=3; in.good(); ) {
    getline(in,s);
    if ( !use && (s.find(start) != std::string::npos || s.find("<common>") != std::string::npos) ) {
      isHeader = true;
      use = true;
      continue;
    }
    if ( use && (s.find(stop) != std::string::npos || s.find("</common>") != std::string::npos) ) {
      use = false;
    }
    if ( use ) {
      ::scrc_put_chars(m_display,s.c_str(),isHeader ? SCR::BOLD: SCR::NORMAL,++line,2,1);
      isHeader = false;
    }
  }
  ::scrc_set_border(m_display,head.c_str(),SCR::INVERSE|SCR::BLUE);
}
