//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  ROMon
//--------------------------------------------------------------------------
//
//  Package    : ROMon
//
//  Description: Readout monitoring in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================

#include <ROMon/InternalDisplay.h>
#include <ROMon/Constants.h>
#include <CPP/IocSensor.h>
#include <CPP/Event.h>
#include <RTL/strdef.h>
#include <SCR/scr.h>
#include <dim/dic.h>
#include "ROMonDefs.h"

// C++ include files
#include <set>
#include <cstring>

using ROMon::InternalDisplay;

static int         s_createFlags = SCR::NORMAL;
static std::string s_prefix = "/";
static std::string s_postfix = "";

/// Global screen lock shared by all displays
std::mutex& InternalDisplay::screenLock() {
  static std::mutex s_lock;
  return s_lock;
}

/// Set create flags
void InternalDisplay::setCreateFlags(int flags) {
  s_createFlags = flags;
}

/// Set create flags
int InternalDisplay::createFlags() {
  return s_createFlags;
}

InternalDisplay::InternalDisplay(InternalDisplay* parent, const std::string& title) 
  : m_pasteboard(0), m_display(0), m_parent(parent), m_name(title), m_title(title), m_svc(0), m_svc2(0)
{
  m_pasteboard = m_parent ? m_parent->pasteboard() : 0;
  m_lastUpdate = time(0);
  screenLock();
}

InternalDisplay::~InternalDisplay() {
  disconnect();
  close();
}

void InternalDisplay::close() {
  if ( m_display ) {
    ::scrc_unpaste_display(m_display,m_pasteboard);
    ::scrc_delete_display(m_display);
    m_display = 0;
  }
}

/// Return service prefix for usage of the bridge
const std::string& InternalDisplay::svcPrefix() {
  return s_prefix;
}

/// Set service prefix for usage of the bridge
void InternalDisplay::setSvcPrefix(const std::string& pref) {
  s_prefix = pref;
}

/// Return service postfix for usage of the bridge
const std::string& InternalDisplay::svcPostfix() {
  return s_postfix;
}

/// Set service postfix for usage of the bridge
void InternalDisplay::setSvcPostfix(const std::string& postf) {
  s_postfix = postf;
}

/// Disconnect from DIM services
void InternalDisplay::disconnect() {
  disconnectService(m_svc2);
  disconnectService(m_svc);
}

/// Disconnect from DIM service
void InternalDisplay::disconnectService(int& svc) const {
  if ( svc != 0 ) {
    ::dic_release_service(svc);
  }
  svc = 0;
}

void InternalDisplay::show(int row, int col) {
  ::scrc_paste_display (m_display, m_pasteboard, row, col);
}

void InternalDisplay::hide() {
  ::scrc_unpaste_display(m_display,m_pasteboard);
}

/// Draw bar to show occupancies
size_t InternalDisplay::draw_bar(int x, int y, float f1, int scale)  {
  size_t len = size_t(float(scale)*f1);
  char txt[1024];
  len = std::min(len,sizeof(txt)-1);
  ::memset(txt,' ',len);
  txt[len] = 0;
  ::scrc_put_chars(m_display,txt,SCR::INVERSE,y,x,0);
  size_t len2 = std::min(scale-len,sizeof(txt)-1);
  ::memset(txt,'.',len2);
  txt[len2] = 0;
  ::scrc_put_chars(m_display,txt,SCR::NORMAL,y,len+x,len2);
  return 1;
}

/// Update display content
void InternalDisplay::update(const void* data, size_t /* len */) {
  return update(data);
}

/// DIM command service callback
void InternalDisplay::dataHandler(void* tag, void* address, int* size) {
  if ( address && tag && *size > 0 ) {
    InternalDisplay* disp = *(InternalDisplay**)tag;
    unsigned char* ptr = new unsigned char[*size+sizeof(int)];
    *(int*)ptr = *size;
    ::memcpy(ptr+sizeof(int),address,*size);
    IocSensor::instance().send(disp,CMD_UPDATE,ptr);
  }
}

/// DIM command service callback
void InternalDisplay::excludedHandler(void* tag, void* address, int* size) {
  if ( address && tag ) {
    InternalDisplay* disp = *(InternalDisplay**)tag;
    char *p = (char*)address, *end = p+*size;
    std::set<std::string> nodes;
    while(p<end) {
      nodes.insert(RTL::str_upper(p));
      p += (::strlen(p)+1);
    }
    IocSensor::instance().send(disp,CMD_EXCLUDE,new std::set<std::string>(nodes));
  }
}

/// Interactor overload: Display callback handler
void InternalDisplay::handle(const CPP::Event& ev)    {
  switch(ev.eventtype) {
  case IocEvent: {
    switch(ev.type) {
    case CMD_NOTIFY: {
      unsigned char* ptr = (unsigned char*)ev.data;
      delete [] ptr;
      break;
    }
    case CMD_UPDATE: {
      DisplayLock lock(screenLock());
      SCR::Pasteboard* pb = pasteboard();
      unsigned char* ptr = (unsigned char*)ev.data;
      if ( pb ) {
        ::scrc_cursor_off(pb);
        ::scrc_begin_pasteboard_update (pb);
      }
      update(ptr + sizeof(int), *(int*)ptr);
      if ( pb ) ::scrc_end_pasteboard_update(pb);
      if ( parent() ) parent()->set_cursor(this);
      if ( pb ) ::scrc_cursor_on(pb);

      if ( parent() )   {
        IocSensor::instance().send(parent(),CMD_NOTIFY,ptr);
      }
      else {
        delete [] ptr;
      }
      break;
    }
    case CMD_EXCLUDE: {
      if ( ev.data ) delete ((std::set<std::string>*)ev.data);
      break;
    }
    default:
      break;
    }
    break;
  }
  default:
    break;
  }
}
