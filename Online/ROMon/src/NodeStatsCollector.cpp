//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  ROMon
//--------------------------------------------------------------------------
//
//  Package    : ROMon
//
//  Description: Readout monitoring in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================
#define MBM_IMPLEMENTATION
#include <ROMon/NodeStatsCollector.h>
#include <ROMon/CPUMonOstream.h>
#include <ROMon/ROMonOstream.h>
#include <ROMon/SysInfo.h>
#include <RTL/Logger.h>
#include <RTL/strdef.h>
#include <RTL/readdir.h>
#include <dim/dis.hxx>
#include "ROMonDefs.h"

/// C/C++ include files
#include <stdexcept>
#include <cmath>
#include <map>
#include <fcntl.h>
#include <sys/stat.h>

using namespace std;
using namespace ROMon;

static void check_dbg(RTL::CLI& cli) {
  volatile bool dbg = cli.getopt("debug",3) != 0;
  while(dbg) {
    ::lib_rtl_sleep(100);
    if ( !dbg ) break;
  }
}

/// Standard constructor
NodeStatsCollector::NodeStatsCollector(int argc, char** argv)
  : m_sys(0), m_print(0), m_verbose(false),
    m_mbmDelay(500), m_mbmSvc(0), m_mbmSize(500), m_mbmBuffer(0),
    m_statDelay(4000), m_statSvc(0), m_statSize(2000), m_statBuffer(0),
    m_hltSize(100), m_hlt_1_Svc(0), m_hlt_2_Svc(0), 
    m_hlt_1_Buffer(0), m_hlt_2_Buffer(0),
    m_mbm(0), m_fsm()
{
  RTL::CLI cli(argc, argv, NodeStatsCollector::help);
  std::string svc, nam, outdns;
  int log_level = LIB_RTL_INFO;
  check_dbg(cli);
  cli.getopt("outdns",    2, outdns);
  cli.getopt("publish",   2, svc);
  cli.getopt("statSize",  5, m_statSize);
  cli.getopt("mbmSize",   4, m_mbmSize);
  cli.getopt("filesSize", 4, m_hltSize);
  cli.getopt("statDelay", 5, m_statDelay);
  cli.getopt("mbmDelay",  4, m_mbmDelay);
  cli.getopt("hlt1Disks", 6, m_hlt1Disks);
  cli.getopt("hlt2Disks", 6, m_hlt2Disks);
  cli.getopt("print",     4, log_level);
  m_print            = cli.getopt("logdata",2) != 0;
  m_verbose          = cli.getopt("verbose",1) != 0;
  m_ignoreDisks      = cli.getopt("ignoredisks",3) != 0;
  m_statSize        *= 1024;
  m_statBuffer       = new char[m_statSize];
  m_mbmSize         *= 1024;
  m_mbmBuffer        = new char[m_mbmSize];
  m_hltSize         *= 1024;
  m_hlt_1_Buffer     = new char[m_hltSize];
  m_hlt_2_Buffer     = new char[m_hltSize];

  RTL::Logger::print_startup("System information COLLECTOR");
  ::lib_rtl_set_log_level(log_level);
  if ( !outdns.empty() )   {
    ::dis_set_dns_node(outdns.c_str());
  }
  CPUMonData cpu(m_statBuffer);
  cpu.node->reset();
  ROMonData mbm(m_mbmBuffer);
  mbm.node->reset();
  new(m_hlt_1_Buffer)     DeferredHLTStats(RTL::nodeNameShort());
  new(m_hlt_2_Buffer)     DeferredHLTStats(RTL::nodeNameShort());

  nam = svc;
  if ( !svc.empty() )  {
    // We also want to see the rpocesses on the subfarm controls PCs
    bool has_mbm = true;
    nam = svc + "/Statistics";
    m_statSvc = ::dis_add_service(nam.c_str(),"C",0,0,feedStats,(long)this);

    nam = svc + "/Readout";
    if ( has_mbm ) m_mbmSvc = ::dis_add_service(nam.c_str(),"C",0,0,feedMBM,(long)this);

    m_hlt_1_Svc = 0;
    nam = svc + "/Hlt1";
    if ( has_mbm ) m_hlt_1_Svc = ::dis_add_service(nam.c_str(),"C",0,0,feedHLT_1,(long)this);

    m_hlt_2_Svc = 0;
    nam = svc + "/Hlt2";
    if ( has_mbm ) m_hlt_2_Svc = ::dis_add_service(nam.c_str(),"C",0,0,feedHLT_2,(long)this);
  }
  else  {
    ::lib_rtl_output(LIB_RTL_ERROR,"%s","Unknown data type -- cannot be published.");
    throw std::runtime_error("Unknown data type and unknwon service name -- cannot be published.");
  }
  m_fsm.setVerbose(m_verbose);
  m_fsm.start();
  ::dis_start_serving(svc.c_str());
}

/// Default destructor
NodeStatsCollector::~NodeStatsCollector() {
  ::dis_remove_service(m_statSvc);
  if ( 0 != m_mbmSvc        ) ::dis_remove_service(m_mbmSvc);
  if ( 0 != m_hlt_1_Svc     ) ::dis_remove_service(m_hlt_1_Svc);
  if ( 0 != m_hlt_2_Svc     ) ::dis_remove_service(m_hlt_2_Svc);
}

/// Help printout in case of -h /? or wrong arguments
void NodeStatsCollector::help() {
  ::lib_rtl_output(LIB_RTL_ALWAYS,"romon_syscollect -opt [-opt]\n");
  ::lib_rtl_output(LIB_RTL_ALWAYS,"    -s[ize]=<number>  Size of the global section in kBytes\n");
  ::lib_rtl_output(LIB_RTL_ALWAYS,"    -d[elay]=<number> Delay in milliseconds between two updates of the information\n");
  ::lib_rtl_output(LIB_RTL_ALWAYS,"    -p[rint]=<number> Make printout to stdout\n");  
  ::lib_rtl_output(LIB_RTL_ALWAYS,"    -v[erbose]        Make printouts to stdout.\n");  
  ::lib_rtl_output(LIB_RTL_ALWAYS,"    -p[ublish]        DIM Servive name.\n");  
}

/// Feed data to DIS when updating data
void NodeStatsCollector::feedStats(void* tag, void** buff, int* size, int* ) {
  static const char* empty = "";
  NodeStatsCollector* h = *(NodeStatsCollector**)tag;
  if ( h ) {
    if ( h->m_sys ) {
      CPUMonData cpu(h->m_statBuffer);
      if ( h->m_statBuffer && cpu.node ) {
        *buff = h->m_statBuffer;
        *size = cpu.node->length();
        if ( h->m_verbose ) {
	  ::lib_rtl_output(LIB_RTL_ALWAYS,
			   "[NodeStatsCollector] Published %d Bytes of Stat data @ %p",
			   *size, *buff);
        }
        return;
      }
    }
  }
  *size = 0;
  *buff = (void*)empty;
}

/// Feed data to DIS when updating data
void NodeStatsCollector::feedMBM(void* tag, void** buff, int* size, int* ) {
  static const char* empty = "";
  NodeStatsCollector* h = *(NodeStatsCollector**)tag;
  if ( h && h->m_mbmBuffer ) {
    ROMonData ro(h->m_mbmBuffer);
    if ( ro.node ) {
      *buff = h->m_mbmBuffer;
      *size  = ro.node->length();
      if ( h->m_verbose ) {
	::lib_rtl_output(LIB_RTL_ALWAYS,
			 "[NodeStatsCollector] Published %d Bytes of MBM data @ %p",
			 *size, *buff);
      }
      return;
    }
  }
  *size = 0;
  *buff = (void*)empty;
}

/// Feed data to DIS when updating data
void NodeStatsCollector::feedHLT_1(void* tag, void** buf, int* size, int* first) {
  NodeStatsCollector* h = *(NodeStatsCollector**)tag;
  h->feedHLT(h->m_hlt_1_Buffer,buf,size,first);
}

/// Feed data to DIS when updating data
void NodeStatsCollector::feedHLT_2(void* tag, void** buf, int* size, int* first) {
  NodeStatsCollector* h = *(NodeStatsCollector**)tag;
  h->feedHLT(h->m_hlt_2_Buffer,buf,size,first);
}

/// Feed data to DIS when updating data
void NodeStatsCollector::feedHLT(char* data_buff, void** buff, int* size, int* ) {
  static const char* empty = "";
  if ( data_buff ) {
    CPUMonData hlt(data_buff);
    if ( hlt.hlt ) {
      *buff = data_buff;
      *size = hlt.hlt->length();
      if ( m_verbose ) {
	::lib_rtl_output(LIB_RTL_ALWAYS,
			 "[NodeStatsCollector] Published %d Bytes of HLTMon data @ %p",
			 *size, *buff);
      }
      return;
    }
  }
  *size = 0;
  *buff = (void*)empty;
}

/// Monitor Node statistics information
int NodeStatsCollector::monitorStats(const std::vector<std::string>& dir_names) {
  CPUMonData buf(m_statBuffer);
  buf.node->type = NodeStats::TYPE;
  if ( 0 == m_sys->update(dir_names) )    {
    ::lib_rtl_sleep(3000);
  }
  if ( m_print ) {
    cout << "CPUMonData: ======================== " << ::lib_rtl_timestr() << "========================" << endl;
    cout << *buf.node << endl;
    cout << "------ Data size: " << m_sys->statistics()->length() << endl;
  }
  return 1;
}

/// Monitor Deferred HLT statistics information
int NodeStatsCollector::monitorHLT(char* buffer, const std::vector<std::string>& dir_names) {
  map<int,int> files;
  unsigned long long blk_size=0,total_blk=0,availible_blk=0;
  DeferredHLTStats* h = new(buffer) DeferredHLTStats(RTL::nodeNameShort());

  /// Now load data into object
  auto time = ro_gettime();
  h->time = time.first;
  h->millitm = time.second;
  //
  // Note: These are the numbers used by the run-control
  // ===================================================
  h->localdisk.blockSize   = 4096;
  h->localdisk.freeBlocks  = 0;
  h->localdisk.numBlocks   = 0;
  h->localdisk.goodDevices = 0;
  h->localdisk.tempBlocks  = 0;
  h->localdisk.numDevices  = (short)dir_names.size();
  for( const auto& dir_name : dir_names )  {
    DIR* dir = ::opendir(dir_name.c_str());
    if ( dir ) {
      struct dirent *entry;
      int ret = ::lib_rtl_diskspace_access(dir_name.c_str(),&blk_size,&total_blk,&availible_blk,W_OK);
      if ( lib_rtl_is_success(ret) )  {
	h->localdisk.blockSize   = blk_size;
	h->localdisk.numBlocks  += total_blk;
	h->localdisk.freeBlocks += availible_blk;
	++h->localdisk.goodDevices;
	/// Remove the blocks allocated in the temporary directory used for auxiliary stuff
	string dnam = dir_name+"/../.tmp", fnam;
	DIR* dir_tmp = ::opendir(dnam.c_str());
	if ( dir_tmp ) {
	  while ( (entry=::readdir(dir_tmp)) != 0 ) {
	    struct stat sb;
	    fnam = dnam + "/" + entry->d_name;
	    if ( 0 == ::stat(fnam.c_str(),&sb) )  {
	      size_t nblk = std::ceil(double(sb.st_size)/double(h->localdisk.blockSize));
	      h->localdisk.freeBlocks += nblk;
	      h->localdisk.tempBlocks += nblk;
	    }
	  }
	  ::closedir(dir_tmp);
	}
      }
      while ( (entry=::readdir(dir)) != 0 ) {
	int run=0, date;
	int status = ::sscanf(entry->d_name,"Run_%010d_%8d-",&run,&date);
	if ( status == 2 ) {
	  map<int,int>::iterator i=files.find(run);
	  if ( i==files.end() ) files[run]=1;
	  else ++((*i).second);
	}
	else if( !(0==::strcmp(entry->d_name,".") || 0==::strcmp(entry->d_name,"..")) ) {
	  // log() << "Strange file name for HLT deferred processing:" << entry->d_name << endl;
	}
      }
      ::closedir(dir);
    }
  }
  DeferredHLTStats::Runs::iterator ir = h->runs.reset();
  map<int,int>::iterator           i  = files.begin();
  for(; i!=files.end(); ++i) {
    *ir = *i;
    ir = h->runs.add(ir);
  }
  if ( m_print ) {
    ::lib_rtl_output(LIB_RTL_ALWAYS,"HLTStats: ======================== %s %s",
		     ::lib_rtl_timestr(),"========================");
    cout << *h << endl;
  }
  return 1;
}

/// Monitor task and MBM information
int NodeStatsCollector::monitorTasks() {
  ROMonData task_data(m_mbmBuffer);
  typedef RODimFSMListener::Clients Clients;
  m_mbm.handle(task_data.node, m_mbmSize);
  Node::Tasks* tasks = task_data.node->tasks();
  Node::Tasks::iterator it = tasks->reset();
  const Clients& cl = m_fsm.clients();
  task_data.node->type = Node::TYPE;
  try  {
    for(Clients::const_iterator ic = cl.begin(); ic != cl.end(); ++ic) {
      const FSMTask* t = (*ic).second->data<FSMTask>();
      if ( ((char*)it) > task_data.str+m_mbmSize ) {
        ::lib_rtl_output(LIB_RTL_ERROR,"%s","Task Buffer section memory too small.....exiting");
        return 0;
      }
      if ( t->processID != -1 ) {
        ::memcpy(it,t,sizeof(FSMTask));
        it = tasks->add(it);
      }
    }
    task_data.node->fixup();
  }
  catch(const exception& e)  {
    ::lib_rtl_output(LIB_RTL_ERROR,"Exception collecting task data: %s", e.what());
    tasks->reset();
  }
  catch (...)  {
    tasks->reset();
  }
  

  if ( m_print ) {
    cout << "ROMonData: ======================== " << ::lib_rtl_timestr() << "========================" << endl;
    cout << *task_data.node << endl;
  }
  return 1;
}

/// Start monitoring activity
int NodeStatsCollector::monitor() {
  std::string node = RTL::nodeNameShort();
  CPUMonData buf(m_statBuffer);
  bool exec = true;
  ::dim_lock();
  SysInfo sys(buf.node,m_statSize,2);
  sys.init();
  m_sys = &sys;
  ::dim_unlock();
  int nclients, stat_delay = m_statDelay;
  if ( m_ignoreDisks ) {
    m_hlt1Disks.clear();
    m_hlt2Disks.clear();
  }
  while(exec)    {
    stat_delay -= m_mbmDelay;
    ::dim_lock();
    try {
      if ( 0 != m_mbmSvc )  {
        monitorTasks();
      }
      if ( stat_delay<=0 ) {
        monitorStats(m_hlt1Disks);
        monitorHLT(m_hlt_1_Buffer,m_hlt1Disks);
        monitorHLT(m_hlt_2_Buffer,m_hlt2Disks);
      }
    }
    catch(const std::exception& e) {
      ::lib_rtl_output(LIB_RTL_ERROR,"Exception collecting task information: %s", e.what());
    }
    catch(...) {
      ::lib_rtl_output(LIB_RTL_ERROR,"%s","Exception collecting task information.");
    }
    if ( 0 != m_mbmSvc ) {
      if ( (nclients=::dis_update_service(m_mbmSvc)) == 0 ) {
        if ( m_print )   {
          //log() << "No client was listening to my MBM information......" << std::endl;
        }
      }
    }
    if ( stat_delay<=0 ) {
      if ( (nclients=::dis_update_service(m_statSvc)) == 0 ) {
        if ( m_print )   {
	  ::lib_rtl_output(LIB_RTL_INFO,"%s",
			   "No client was listening to my node statistics information.");
        }
      }
      if ( 0 != m_hlt_1_Svc ) {
        if ( (nclients=::dis_update_service(m_hlt_1_Svc)) == 0 ) {
          if ( m_print )  {
	    ::lib_rtl_output(LIB_RTL_INFO,"%s",
			     "No client was listening to the deferred HLT information.");
          }
        }
      }
      if ( 0 != m_hlt_2_Svc ) {
        if ( (nclients=::dis_update_service(m_hlt_2_Svc)) == 0 ) {
          if ( m_print )  {
	    ::lib_rtl_output(LIB_RTL_INFO,"%s",
			     "No client was listening to the deferred HLT information.");
          }
        }
      }
      stat_delay = m_statDelay;
    }
    ::dim_unlock();
    ::lib_rtl_usleep(1000*m_mbmDelay);
  }
  return 1;
}

namespace {

  void  error_handler(int severity, int error_code, char* message)    {
    int level = LIB_RTL_INFO;
    switch(severity)   {
    case 3:
      level = LIB_RTL_FATAL;
      break;
    case 2:
      level = LIB_RTL_ERROR;
      break;
    case 1:
      level = LIB_RTL_WARNING;
      break;
    case 0: 
    default:
      level = LIB_RTL_INFO;
      break;
    }
    switch(error_code)   {
    case DIMTCPRDERR:
    case DIMTCPWRRTY:
    case DIMTCPWRTMO:
    case DIMDNSREFUS:
      level = LIB_RTL_INFO;
      break;
    default:
      break;
    }
    ::lib_rtl_output(level,"DIM:  %d [%s]", error_code, message ? message : "Unknown message");
  }
}


extern "C" int romon_syscollect(int argc, char** argv) {
  NodeStatsCollector romon(argc,argv);
  ::dic_add_error_handler(error_handler);
  return romon.monitor();
}
