//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//===========================================================================
//  ROMon
//---------------------------------------------------------------------------
//
//  Package    : ROMon
//
//  Description: Readout monitoring in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//===========================================================================

// Framework includes
#include "dim/dic.hxx"
#include "RTL/rtl.h"
#include "RTL/strdef.h"
#define MBM_IMPLEMENTATION
#include "ROMonDefs.h"
#include "ROMon/ROMon.h"
#include "ROMon/ROMonInfo.h"
#include "ROMon/RODimFSMListener.h"

namespace {
  /// Handler count (for debugging)
  std::size_t s_info_count = 0;

  struct FSMMonitoring {
    unsigned long lastCmd, doneCmd;
    int pid;
    char targetState, state, metaState, pad;
    int partitionID;
  };
}
using namespace ROMon;
using namespace std;


/// Standard destructor
RODimFSMListener::~RODimFSMListener() {
  DimLock lock;
  delete m_dns;
  m_dns = 0;
  for (Clients::iterator i=m_clients.begin(); i != m_clients.end(); ++i) {
    ::dic_release_service((*i).second->id);
    (*i).second->release();
  }
}

/// Add handler for a given message source
void RODimFSMListener::addHandler(const string& node,const string& svc)    {
  static string myNode = RTL::nodeNameShort();
  static string myUtgid = RTL::processName();
  if ( ::str_ncasecmp(node.c_str(),myNode.c_str(),myNode.length()) == 0 ) {
    if ( ::str_ncasecmp(svc.c_str(),myUtgid.c_str(),myUtgid.length()) != 0 ) {
      if ( ::str_ncasecmp(svc.c_str(),"DIS_DNS",7) != 0 ) {
        DimLock lock;
        string nam = svc+"/fsm_status";
        Clients::iterator i=m_clients.find(nam);
        if ( i == m_clients.end() )  {
          Item*    it = Item::create<FSMTask>(this);
          FSMTask* t   = it->data<FSMTask>();
          ::strncpy(t->name,svc.substr(0,svc.find("/")).c_str(),sizeof(t->name)-1);
          t->name[sizeof(t->name)-1] = 0;
          t->processID   = -1;
          t->partitionID = -1;
          m_clients[nam] = it;
          it->id = ::dic_info_service(nam.c_str(),MONITORED,0,0,0,infoHandler,(long)it,0,0);
	  ::lib_rtl_output(LIB_RTL_VERBOSE,"Create DimInfo: %s id:%d", nam.c_str(), it->id);
        }
      }
    }
  }
}

/// Remove handler for a given message source
void RODimFSMListener::removeHandler(const string& /* node */, const string& svc)   {
  DimLock lock;
  string nam = svc+"/fsm_status";
  Clients::iterator i=m_clients.find(nam);
  if ( i != m_clients.end() ) {
    Item* it = (Item*)(*i).second;
    ::dic_release_service(it->id);
    ::lib_rtl_output(LIB_RTL_VERBOSE,"Delete DimInfo: %s id:%d", nam.c_str(), it->id);
    it->release();
    m_clients.erase(i);
  }
}

/// DimInfo overload to process messages
void RODimFSMListener::infoHandler(void* tag, void* address, int* size) {
  if ( address && tag && size && *size>0 ) {
    FSMMonitoring* mon = (FSMMonitoring*)address;
    if ( mon && mon->pid != -1 )   {
      Item* it       = *(Item**)tag;
      FSMTask* t     = it->data<FSMTask>();
      t->processID   = mon->pid;
      t->partitionID = mon->partitionID;
      t->state       = mon->state;
      t->targetState = mon->targetState;
      t->metaState   = mon->metaState;
      t->lastCmd     = mon->lastCmd;
      t->doneCmd     = mon->lastCmd;
      ++s_info_count;
    }
  }
}

extern "C" int romon_test_fsm_listener(int, char**) {
  bool do_run = true;
  RODimFSMListener listener;
  ::lib_rtl_output(LIB_RTL_INFO,"Going asleep");
  while(do_run) {
    ::lib_rtl_sleep(1000);
  }
  return 1;
}

