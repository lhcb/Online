//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  ROMon
//--------------------------------------------------------------------------
//
//  Package    : ROMon
//
//  Description: Readout monitoring in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================
#ifndef ROMON_ROKAFKABRIDGE_H
#define ROMON_ROKAFKABRIDGE_H 1

// Framework include files
#include <CPP/Interactor.h>
#include <dim/dis.h>
#include <dim/dic.h>

// C++ include files
#include <regex>
#include <list>


/// ROMon namespace declaration
namespace ROMon {

  namespace   {

  /// Readout monitor DIM listener for a single node
  /** Readout monitor DIM listener for a single node
   *
   * \version 1.0
   * \author M.Frank
   */
    class Listener  {
    protected:
      std::string m_message;
      std::string m_source;
      Listener*   m_subfarm  { nullptr };
      std::size_t m_message_count {  0 };
      int         m_client        { -1 };
      int         m_service       { -1 };
    public:
      Listener() = default;
      Listener(const Listener& copy) = default;
      Listener& operator=(const Listener& copy) = default;
      /// Constructor with initialization
      Listener(const std::string& name, const std::string& prefix);
      /// Constructor with initialization
      //Listener(const std::string& name, Listener* subfarm);
      /// Default destructor
      virtual ~Listener();
      int service()  const               {  return m_service;         }
      const char* source()  const        {  return m_source.c_str();  }
      void setMessage(const char* msg)   {  m_message = msg;          }
      /// Update handler
      void update(const char* param, size_t len);
      /// Feed data to DIS when updating data
      static void feed(void* tag, void** buf, int* size, int* first);
      /// DimInfo overload to process messages
      static void load(void* tag, void* address, int* size);
    };
  }

  /// Readout monitor DIM server for a single node
  /** Readout monitor DIM server for a single node
   *
   * \version 1.0
   * \author M.Frank
   */
  class ROKafkaBridge : public CPP::Interactor {
  protected:
    /// Cluster container type definition
    typedef std::map<std::string,Listener*> Servers;
    typedef std::vector<std::regex> Matches;
    /// Cluster container
    Servers         m_servers;
    Matches         m_node_match;
    Matches         m_subfarm_match;
    /// Process name
    std::string     m_name;
    /// Prefix for resulting service names
    std::string     m_prefix;
    int             m_dns;

    struct NodeAdder {
      ROKafkaBridge* bridge;
      NodeAdder(ROKafkaBridge* b) : bridge(b) {}
      void operator()(const std::string& n) const { bridge->addNode(n); }
    };
    bool check_match(const std::string& node)  const;
  public:
    /// Standard constructor with initialization
    ROKafkaBridge(int argc, char** argv);
    /// Default destructor
    virtual ~ROKafkaBridge();
    /// Add cluster data points to bridge
    Listener* addNode(const std::string& name);
    /// DIM command service callback
    static void dnsHandler(void* tag, void* address, int* size);
    /// Help printout in case of -h /? or wrong arguments
    static void help();
    /// Interactor override ....
    void handle(const CPP::Event& ev)  override;
  };
}      // End namespace ROMon
#endif /* ROMON_ROKAFKABRIDGE_H */

//====================================================================
//  ROMon
//--------------------------------------------------------------------
//
//  Package    : ROMon
//
//  Description: Readout monitoring in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//====================================================================

// C++ include files
#include <iostream>
#include <stdexcept>

// Framework includes
#include <dim/dic.h>
#include <dim/dis.h>
#include <RTL/rtl.h>
#include <RTL/strdef.h>
#include <RTL/Logger.h>
#include <CPP/Event.h>
#include <CPP/IocSensor.h>
#include <ROMon/Constants.h>
#include "ROMonDefs.h"

#include <algorithm>
#include <memory>

using namespace ROMon;
using namespace std;

/// DimInfo overload to process messages
void Listener::load(void* tag, void* address, int* size) {
  if ( address && tag && size && *size>0 ) {
    char* buff = (char*)address;
    size_t len = *size;
    Listener* l = *(Listener**)tag;
    /// Fix record here if it was not done before
    if      ( buff[len-1] == '\n' ) buff[len-1] = 0, --len;
    else if ( buff[len-2] == '\n' ) buff[len-2] = 0, len -= 2;
    else if ( buff[len-3] == '\n' ) buff[len-3] = 0, len -= 3;
    l->update(buff, len);
  }
}

/// Feed data to DIS when updating data
void Listener::feed(void* tag, void** buff, int* size, int* ) {
  static const char* data = "";
  Listener* l = *(Listener**)tag;
  if ( l ) {
    *buff = (void*)l->m_message.c_str();
    *size = l->m_message.length()+1;
    return;
  }
  *buff = (void*)data;
  *size = 0;
}

/// Constructor with initialization
Listener::Listener(const string& source, const string& prefix)
{
  m_subfarm = this;
  m_source  = source+"/log";
  m_client  = ::dic_info_service(m_source.c_str(),MONITORED,0,0,0,load,(long)this,0,0);
  m_service = ::dis_add_service((prefix+m_source).c_str(),"C",0,0,feed,(long)this);
  ::lib_rtl_output(LIB_RTL_ALWAYS,"Added service: %s",m_source.c_str());
}
#if 0
/// Constructor with initialization
Listener::Listener(const string& source, Listener* subfarm)
{
  m_source  = source+"/log";
  m_subfarm = subfarm;
  m_client  = ::dic_info_service(m_source.c_str(),MONITORED,0,0,0,load,(long)this,0,0);
}
#endif
/// Default destructor
Listener::~Listener()  {
  if ( m_subfarm == this && m_service > 0 ) ::dis_remove_service(m_service);
  m_service = -1;
}

/// Update handler
void Listener::update(const char* msg, size_t /* len */) {
  int svc = m_subfarm->service();
  m_subfarm->setMessage(msg);
  if ( m_subfarm == this )    {
    ::lib_rtl_output(LIB_RTL_VERBOSE,"Update Service: %4d [%s] %s", svc, m_subfarm->source(), msg);
    ::dis_update_service(svc);
    if ( m_source == "/HLT02/LOGS/log" )   { // This is for debugging only.....
      ++m_message_count;
    }
    else  {
      ++m_message_count;
    }
  }
}

/// Standard constructor
ROKafkaBridge::ROKafkaBridge(int argc, char** argv)  {
  string PUBLISHING_NODE = "ECS03", from=PUBLISHING_NODE, to=PUBLISHING_NODE;
  vector<string> nodes(1,string("(.*)")), subfarms;//(1,string("(.*)"));
  RTL::CLI cli(argc, argv, ROKafkaBridge::help);
  int print = LIB_RTL_WARNING, sleep=0;
  m_name = "/"+RTL::processName();

  cli.getopt("nodes", 2, nodes);
  cli.getopt("subfarms", 2, subfarms);
  cli.getopt("publish",2, m_prefix="/RO");
  cli.getopt("print",2, print);
  cli.getopt("from",2, from);
  cli.getopt("to",2, to);
  cli.getopt("sleep",2, sleep);
  if ( sleep > 0 )   {
    do {
      ::lib_rtl_sleep(1000);
    } while ( --sleep > 0 );
  }
  for ( const auto& m : nodes )
    m_node_match.emplace_back(std::regex(RTL::str_upper(m)));
  for ( const auto& m : subfarms )
    m_subfarm_match.emplace_back(std::regex(RTL::str_upper(m)));
  RTL::Logger::install_log(RTL::Logger::log_args(print));
  ::dic_set_dns_node(from.c_str());
  ::dis_set_dns_node(to.c_str());
  m_dns = ::dic_info_service("DIS_DNS/SERVER_LIST",MONITORED,0,0,0,dnsHandler,(long)this,0,0);
  ::dis_start_serving(m_name.c_str());
}

/// Default destructor
ROKafkaBridge::~ROKafkaBridge()    {
}

/// DIM command service callback
void ROKafkaBridge::dnsHandler(void* tag, void* address, int* size) {
  if ( address && tag && *size > 0 ) {
    ROKafkaBridge* listener = *(ROKafkaBridge**)tag;
    char *msg = (char*)address;
    string svc, node;
    size_t idx;
    switch(msg[0]) {
    case '+':
      getServiceNode(++msg,svc,node);
      idx = svc.find("/LOGS");
      if ( idx != string::npos ) {
	if ( listener->check_match(node) )   {
	  IocSensor::instance().send(listener,CMD_ADD,new string(svc));
	}
      }
      break;
    case '-':
      break;
    case '!':
      break;
    default:
      if ( *(int*)msg != *(int*)"DEAD" )  {
        char *at, *p = msg, *last = msg;
        auto farms = std::make_unique<vector<string>>();
        while ( last != 0 && (at=::strchr(p,'@')) != 0 )  {
          last = strchr(at,'|');
          if ( last ) *last = 0;
          getServiceNode(p,svc,node);
          idx = svc.find("/LOGS");
          if ( idx != string::npos ) {
	    if ( listener->check_match(node) )   {
              farms->push_back(svc);
	    }
          }
          p = last+1;
        }
        if ( !farms->empty() )
          IocSensor::instance().send(listener,CMD_CONNECT,farms.release());
      }
      break;
    }
  }
}

bool ROKafkaBridge::check_match(const string& node)   const {
  string n = RTL::str_upper(node);
  for ( const auto& m : m_subfarm_match )   {
    if ( std::regex_match(n, m) )
      return true;
  }
  for ( const auto& m : m_node_match )   {
    if ( std::regex_match(n, m) )
      return true;
  }
  return false;
}

/// Add cluster data points to bridge
Listener* ROKafkaBridge::addNode(const string& node)   {
  string tag = node.substr(1,node.find("/",1)-1);
  auto i = m_servers.find(tag);
  if ( i == m_servers.end() )   {
#if 0
    for ( const auto& m : m_subfarm_match )   {
      if ( std::regex_match(tag, m) )  {
	Listener* listener = new Listener(node, m_prefix);
	m_servers[tag] = listener;
	::lib_rtl_output(LIB_RTL_ALWAYS,"Added subfarm node:%s", node.c_str());
	return listener;
      }
    }
#endif

    for ( const auto& m : m_node_match )   {
      if ( std::regex_match(tag, m) )  {
#if 0
	string subfarm = tag.substr(0, tag.length()-2);
	string subfarm_node = node;
	subfarm_node.replace(1, tag.length(), subfarm);
	Listener* subfarm_listener = addNode(subfarm_node);
	if ( !subfarm_listener )   {
	  ::lib_rtl_output(LIB_RTL_ALWAYS,"FAILED adding worker node:%s [No subfarm]", node.c_str());
	  return nullptr;
	}
	Listener* listener = new Listener(node, subfarm_listener);
#endif
	Listener* listener = new Listener(node, m_prefix);
	m_servers[tag] = listener;
	::lib_rtl_output(LIB_RTL_ALWAYS,"Added worker  node:%s", node.c_str());
	return listener;
      }
    }
    return nullptr;
  }
  return (*i).second;
}

/// Interactor override ....
void ROKafkaBridge::handle(const CPP::Event& ev) {
  typedef vector<string> StringV;
  switch(ev.eventtype) {
  case IocEvent:
    switch(ev.type) {
    case CMD_ADD:  {
      std::unique_ptr<string> server{(string*)ev.data};
      NodeAdder(this)(*server);
      ::dis_start_serving(m_name.c_str());
      return;
    }
    case CMD_CONNECT: {
      std::unique_ptr<StringV> servers{(StringV*)ev.data};
      for_each(servers->begin()+1,servers->end(),NodeAdder(this));
      ::dis_start_serving(m_name.c_str());
      return;
    }
    default:
      break;
    }
    break;
  case TimeEvent:
    return;
  case NetEvent:
    return;
  default:
    break;
  }
}

/// Help printout in case of -h /? or wrong arguments
void ROKafkaBridge::help() {
  ::lib_rtl_output(LIB_RTL_ALWAYS,"romon_kafkabridge -opt [-opt]                                       \n"
                   "     -node=<string>     Regular expression with node match (multiple possible).    \n"
                   "     -subfarm=<string>  Regular expression with subfarm match (multiple possible). \n"
                   "     -from=<string>     DNS Node name from which the datapoints should be consumed.\n"
                   "     -to=<string>       DNS Node to which these data points should be published.   \n"
                   "     -print=<integer>   Printout value and verbosity.                              \n"
                   "     -publish=<string>  Prefix for published services.                           \n\n");
}

/// Main entry point to start the application
extern "C" int romon_kafkabridge(int argc, char** argv) {
  ROKafkaBridge mon(argc,argv);
  RTL::Logger::print_startup("Kafka message bridge");
  IocSensor::instance().run();
  return 1;
}
