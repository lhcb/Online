//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  ROMon
//--------------------------------------------------------------------------
//
//  Package    : ROMon
//
//  Description: Readout monitoring in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================

// C++ include files
#include <cstdio>
#include <cstdlib>

#define MBM_IMPLEMENTATION
#include "ROMonDefs.h"
#include <ROMon/ROMon.h>
#include <MBM/bmstruct.h>
#include <RTL/time.h>
#include <RTL/rtl.h>
#include <dim/dic.h>

#ifdef _WIN32
#include <winsock2.h>
#endif

using namespace ROMon;
using namespace std;

void ROMon::ro_move_string(char* target, const char* source, size_t buff_len)    {
  ::strncpy(target, source, buff_len-1);
  target[buff_len-1]=0;
}

void ROMon::ro_get_node_name(char* name, size_t len) {
  ::strncpy(name,RTL::nodeNameShort().c_str(),len);
  name[len-1] = 0;
}

std::pair<int, unsigned int> ROMon::ro_gettime() {
  timeval tv = {0,0};
  ::gettimeofday(&tv,0);
  return {tv.tv_sec,tv.tv_usec/1000};
}

/// Extract node/service name from DNS info
void ROMon::getServiceNode(char* s, string& svc, string& node) {
  char* at = strchr(s,'@');
  *at = 0;
  svc = s;
  node = at+1;
}

bool ROMon::ro_match_end(const string& pattern, const char* data)  {
  if ( !data ) return false;
  if ( pattern.empty() ) return true;
  size_t len = ::strlen(data);
  if ( len+1 < pattern.length() ) return false;
  const char* ptr = data + len - pattern.length() - 1;
  if ( *ptr != '_' ) return false;
  return ::strcmp(ptr+1,pattern.c_str()) == 0;
}

bool ROMon::ro_match_start(const string& pattern, const char* data)  {
  if ( !data ) return false;
  if ( pattern.empty() ) return true;
  size_t len = ::strlen(data);
  if ( len+1 < pattern.length() ) return false;
  const char* ptr = data + pattern.length() + 1;
  if ( *ptr != '_' ) return false;
  return ::strncmp(data,pattern.c_str(),pattern.length()) == 0;
}

MBMBuffer* MBMBuffer::reset() {
  ::memset(this,0,sizeof(MBMBuffer));  
  return this;
}

MBMClient* MBMClient::reset() {
  ::memset(this,0,sizeof(MBMClient));  
  return this;
}

FSMTask* FSMTask::reset() {
  ::memset(this,0,sizeof(FSMTask));  
  return this;
}

/// Reset node structure to allow re-filling
Node* Node::reset() {
  ::memset(this,0,sizeof(Node)+sizeof(Node::Buffers)+sizeof(Node::Tasks));
  type = TYPE;
  return this;
}

/// Access to the buffer part of the node structure
Node::Buffers* Node::buffers()  const {
  return (Buffers*)(name + sizeof(name));
}

/// Access to the tasks part of the node structure
Node::Tasks* Node::tasks()  const {
  Buffers* b = buffers();
  return (Tasks*)(((char*)b) + b->length());
}

/// Fix the lengths before sending. This is the last statement after filling
void Node::fixup() {
  type = TYPE;
  bmSize = buffers()->length();
  taskSize = tasks()->length();
  totalSize = bmSize + taskSize + sizeof(Node);
}

Nodeset* Nodeset::reset() {
  ::memset(this,0,sizeof(Nodeset));
  type = TYPE;
  return this;
}

/// Retrieve timestamp of earliest updated node
TimeStamp Nodeset::firstUpdate() const {
  return _firstUpdate(nodes);
}

/// Retrieve timestamp of most recent updated node
TimeStamp Nodeset::lastUpdate() const {
  return _lastUpdate(nodes);
}

/// Default constructor
FSMTask::FSMTask() : processID(-1) {
  name[0] = 0;
}

/// Copt constructor
FSMTask::FSMTask(const FSMTask& copy) {
  *this = copy;
}

FSMTask& FSMTask::operator=(const FSMTask& copy) {
  if ( this != &copy )  {
    ::strncpy(name,copy.name,sizeof(name));
    name[sizeof(name)-1]=0;
    processID   = copy.processID;
    state       = copy.state;
    targetState = copy.targetState;
    metaState   = copy.metaState;
    lastCmd     = copy.lastCmd;
    doneCmd     = copy.doneCmd;
  }
  return *this;
}

//==============================================================================

DimLock::DimLock() {
  dim_lock();
}

DimLock::~DimLock() {
  dim_unlock();
}

DimReverseLock::DimReverseLock() {
  dim_unlock();
}

DimReverseLock::~DimReverseLock() {
  dim_lock();
}

