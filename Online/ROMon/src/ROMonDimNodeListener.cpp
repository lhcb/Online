//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  ROMon
//--------------------------------------------------------------------------
//
//  Package    : ROMon
//
//  Description: Readout monitoring in the LHCb experiment
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================

// C++ include files
#include <cstdlib>

// Framework includes
#include "dim/dic.hxx"
#include "RTL/rtl.h"
#include "RTL/strdef.h"
#define MBM_IMPLEMENTATION
#include "ROMonDefs.h"
#include "ROMon/ROMon.h"
#include "ROMon/ROMonInfo.h"
#include "ROMon/RODimNodeListener.h"

using namespace ROMon;

/// Standard destructor
RODimNodeListener::~RODimNodeListener() {
  delete m_dns;
  m_dns = 0;
  DimLock lock;
  for(Clients::iterator i=m_clients.begin(); i != m_clients.end(); ++i)  {
    Item* it = (*i).second;
    ::dic_release_service(it->id);
    it->data<Descriptor>()->release();
    it->release();
  }
}

/// Add handler for a given message source
void RODimNodeListener::addHandler(const std::string& node,const std::string& svc) {
  DimLock lock;
  Clients::iterator i=m_clients.find(svc);
  if ( i == m_clients.end() )  {
    if ( ::str_match_wild(svc.c_str(), m_match.c_str()) )  {
      Item* itm = Item::create<Descriptor>(this);
      m_clients[svc] = itm;
      itm->id = ::dic_info_service(svc.c_str(),MONITORED,0,0,0,infoHandler,(long)itm,0,0);
      ::lib_rtl_output(LIB_RTL_VERBOSE,"[RODimNodeListener] Create DimInfo: %s@%s id=%d",
		       svc.c_str(), node.c_str(), itm->id);
    }
  }
}

/// Remove handler for a given message source
void RODimNodeListener::removeHandler(const std::string& node, const std::string& svc)   {
  DimLock lock;
  Clients::iterator i=m_clients.find(svc);
  if ( i != m_clients.end() ) {
    Item* it = (*i).second;
    it->data<Descriptor>()->release();
    ::dic_release_service(it->id);
    ::lib_rtl_output(LIB_RTL_VERBOSE,"[RODimNodeListener] Delete DimInfo: %s@%s id=%d",
		     svc.c_str(), node.c_str(), it->id);
    it->release();
    m_clients.erase(i);
  }
}

/// DimInfo overload to process messages
void RODimNodeListener::infoHandler(void* tag, void* address, int* size) {
  if ( address && tag && size && *size>0 ) {
    int len = *size;
    Item* it = *(Item**)tag;
    Descriptor* d = it->data<Descriptor>();
    if ( d->len < len ) {
      d->len = len;
      if ( d->data ) ::free(d->data);
      d->data = (char*)::malloc(d->len);
    }
    d->actual = len;
    ::memcpy(d->data,address,d->actual);
  }
}
