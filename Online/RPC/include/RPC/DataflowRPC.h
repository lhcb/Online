//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================
#ifndef RPC_DATAFLOWRPC_H
#define RPC_DATAFLOWRPC_H

// Framework include files
#include <CPP/ObjectProperty.h>
#include <RPC/RpcClientHandle.h>

// C/C++ include files
#include <memory>
#include <vector>
#include <set>

/// Namespace for the xmlrpc based implementation
namespace xmlrpc  {

  /// Forward declarations

  /// Namespace for the dataflow specific implementation
  namespace dataflow   {

    ///  XMLRPC Client to query DNS information
    /**
     *
     *  \author  M.Frank
     *  \version 1.0
     *  \date    02.05.2017
     */
    class DomainInfo : public rpc::XmlRpcClientH  {
    public:
      typedef std::set<std::string> Result;
    public:
      /// Constructors
      using rpc::XmlRpcClientH::XmlRpcClientH;

      /// Access all known DNS domains
      Result domains() const;
      /// Access all nodes known to a DNS domain
      Result nodes(const std::string& dns) const;
      /// Access all nodes known to a DNS domain matching the regular expression
      Result nodesByRegex(const std::string& dns, const std::string& regex) const;
      /// Access all tasks known to a DNS domain matching the regular expression
      Result tasks(const std::string& dns, const std::string& node) const;
      /// Access all tasks known to a DNS domain matching the correct node
      Result tasksByRegex(const std::string& dns, const std::string& regex) const;
    };

    ///  XMLRPC Client to query properties from individual tasks
    /**
     *
     *  \author  M.Frank
     *  \version 1.0
     *  \date    02.05.2017
     */
    class PropertyContainer : public rpc::XmlRpcClientH  {
    public:
      /// Definition of the clients collection type
      typedef std::vector<std::string>             Clients;
      /// Definition of the property collection type
      typedef std::vector<rpc::ObjectProperty>  Properties;

    public:
      /// Constructors
      using rpc::XmlRpcClientH::XmlRpcClientH;

      /// Access the hosted client services
      Clients    clients()  const;
      /// Access all properties of an object
      Properties allProperties()  const;
      /// Access all properties with the same name from all clients
      Properties namedProperties(const std::string& name)  const;
      /// Access all properties of one remote client (service, ect.)
      Properties clientProperties(const std::string& client)  const;
      /// Access a single property of an object
      rpc::ObjectProperty property(const std::string& client,
				   const std::string& name)  const;
      /// Modify a property
      void setProperty(const std::string& client,
		       const std::string& name,
		       const std::string& value)  const;
      /// Modify a property
      void setPropertyObject(const rpc::ObjectProperty& property) const;
      /// Modify a whole bunch of properties. Call returns the number of changes
      int  setProperties(const Properties& properties)  const;
    };

    ///  XMLRPC Client to query properties from individual tasks
    /**
     *
     *  \author  M.Frank
     *  \version 1.0
     *  \date    02.05.2017
     */
    class CommandInterpreter : public rpc::XmlRpcClientH  {
    public:
      /// Constructors
      using rpc::XmlRpcClientH::XmlRpcClientH;

      /// Execute an interpreter request to the ROOT istance (if present)
      int interpreteCommand(const std::string& command)  const;
      /// Execute an interpreter request to the ROOT istance (if present)
      int interpreteCommands(const std::vector<std::string>& commands)  const;
    };
  }     // End namespace xmlrpc
}       // End namespace dataflow
#endif  /* RPC_DATAFLOWRPC_H       */
