//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_RPC_HTTPRPCHANDLER_H
#define ONLINE_RPC_HTTPRPCHANDLER_H

// Framework include files
#include <HTTP/HttpServer.h>

/// Namespace for the http server and client apps
namespace rpc  {

  /// Generic Http request handler interface
  class HttpRpcHandler  {
  public:
    using continue_action = http::HttpRequestHandler::continue_action;
    /// Property: dim server uri
    std::string  uri;

  public:
    /// Standard constructor
    HttpRpcHandler() = default;
    /// Standard destructor
    virtual ~HttpRpcHandler() = default;
    /// Enable debugging
    virtual void setDebug(int val) = 0;
    /// Execute RPC call
    virtual continue_action handle_request(const http::Request& req, http::Reply& rep) = 0;
  };

  /// Generic Http request handler interface
  template <typename IMP> class RpcHandlerImp : public HttpRpcHandler {
  public:
    using continue_action = http::HttpRequestHandler::continue_action;
    IMP imp;
  public:
    /// Standard constructor
    template <class... Args>
      RpcHandlerImp(Args&&... args) : imp(std::forward<Args>(args)...)  {
    }
    /// Standard destructor
    virtual ~RpcHandlerImp() = default;
    /// Enable debugging
    virtual void setDebug(int /* val */)  override  {
    }
    IMP* get()                     {   return &imp;    }
    const IMP* get()  const        {   return &imp;    }
    IMP* operator->()              {   return &imp;    }
    const IMP* operator->()  const {   return &imp;    }
    IMP& operator*()               {   return imp;     }
    const IMP& operator*()  const  {   return imp;     }
    /// Execute RPC call
    virtual continue_action handle_request(const http::Request& req, http::Reply& rep)  override  {
      return imp.handle_request(req, rep);
    }
  };
}      // End namespace http
#endif // ONLINE_RPC_HTTPRPCHANDLER_H
