//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================
#ifndef RPC_HTTP_SERVER_H
#define RPC_HTTP_SERVER_H

// Framework include files
#include <CPP/Callback.h>
#include <HTTP/HttpServer.h>

// C/C++ include files
#include <map>
#include <memory>
#include <stdexcept>

/// Namespace for the http based rpc implementation
namespace rpc  {

  class HttpRpcHandler;

  ///  RPC Server class
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.05.2017
   */
  class HttpServer {
  public:
    /// Handler class specification
    class Handler;
    /// Definition of the Callback signature
    typedef void (*HttpCall)(const http::Request* request);
    /// Callback type declarations
    typedef dd4hep::Callback   Callback;

    /// Reference to the implementation interface
    std::unique_ptr<Handler> implementation;
    /// Server bind address
    std::string     host;
    /// Server bind port
    std::string     port;

    /// Operation mode
    enum Mode { SERVER, BRIDGE };
  public:
    /// Inhibit default construction
    HttpServer() = delete;
    /// Inhibit move construction
    HttpServer(HttpServer&& copy) = delete;
    /// Inhibit copy construction
    HttpServer(const HttpServer& copy) = delete;
    /// Inhibit move assignment
    HttpServer& operator=(HttpServer&& copy) = delete;
    /// Inhibit copy assignment
    HttpServer& operator=(const HttpServer& copy) = delete;

    /// Initializing constructor
    HttpServer(std::unique_ptr<Handler>&& impl,
	       const std::string& address,
	       int   port, 
	       Mode  mode, 
	       const std::string& uri = "/RPC2");
    /// Initializing constructor
    HttpServer(std::unique_ptr<Handler>&& impl,
	       const std::string& address,
	       const std::string& port,
	       const std::string& mode,
	       const std::string& uri = "/RPC2");
    /// Default destructor
    virtual ~HttpServer();
    /// Modify debug flag
    int setDebug(int value);
    /// Modify checked server URI (empty -> No check)
    std::string setUri(const std::string& uri);

    /// Start the rpc service
    void start(bool detached = false, int num_additional_threads = 0);
    /// Run the rpc service (only use in conjunction with start(false); ) 
    /** Use only to execute the callback loop in a seperate thread.
     *  Run the server's io_service loop. If specified additional workers may be used
     */
    void run(int num_additional_threads = 0);

    /// Stop the rpc service
    void stop();

    /// Bind a callback to a function
    template <typename CALL>
    void define(const std::string& name, const CALL& call);

    /// Remove a callback from the rpc interface
    void remove(const std::string& name);

    /// Remove all callbacks from the xmlrpc interface
    void remove_all();

    /// Bind default callbacks to all successfully handled function requests.
    void onHandled(const Callback& call);

    /// Bind default callbacks to all unhandled functions. Call signature must be HttpCall.
    void onUnhandled(const Callback& call);

    /// Bind default callbacks to processing errors. Call signature must be HttpCall.
    void onError(const Callback& call);
  };

  class HttpServer::Handler : public http::HttpRequestHandler  {
    struct ClientBridge   {
      /// Known servers in the DNS domain
      std::map<std::string, std::shared_ptr<HttpRpcHandler> > handlers;
      /// Default constructor
      ClientBridge() = default;
      /// Default destructor
      virtual ~ClientBridge() = default;
    };
  public:
    /// Standard constructor
    explicit Handler();
    /// Default destruction
    virtual ~Handler();

    /// Stop the rpc service
    virtual void stop();
    
    /// Remove a callback from the rpc interface
    virtual bool remove(const std::string& name);

    /// Remove all callbacks from the xmlrpc interface
    virtual void remove_all();

    /// Bind a callback to a function
    template <typename CALL> bool define(const std::string& name, const CALL& call);

    /// Check if the request is serverd by a registered mount point handler
    virtual continue_action handle_mount_request(const http::Request& req, http::Reply& rep);

    /// Handle a request and produce a reply.
    virtual continue_action handle_request(const http::Request& req, http::Reply& rep) override;

  public:
    typedef std::vector<std::shared_ptr<std::thread> > Threads;

    /// Bind default callback to all successfully handled functions.
    std::vector<Callback>             onHandled;
    /// Bind default callback to all unhandled functions.
    std::vector<Callback>             onUnhandled;
    /// Bind default callback to processing errors.
    std::vector<Callback>             onError;

    /// Operations mode
    HttpServer::Mode                  mode;
    /// Handler for mount point dispatching to different clients
    ClientBridge                      mountPoints;
    /// Handler for bridge requests using http
    ClientBridge                      httpBridge;
    /// Handler for bridge requests using dim
    ClientBridge                      dimBridge;
    /// Default server URI
    std::string                       server_uri;
  };

  /// Bind a callback to a function
  template <typename CALL>
    inline void rpc::HttpServer::define(const std::string& name, const CALL& call)   {
    std::lock_guard<std::mutex> lock(implementation->lock);
    if ( !implementation->define(name, call) )   {
      throw std::runtime_error("The RPC call "+name+" is already registered to the RPC interface.");
    }
  }
}       /* End  namespace rpc      */
#endif  /* RPC_HTTP_SERVER_H       */
