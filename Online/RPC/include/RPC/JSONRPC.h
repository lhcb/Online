//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_RPC_JSONRPC_H
#define ONLINE_RPC_JSONRPC_H

// Framework include files
#include <nlohmann/json.hpp>

// C/C++ include files
#include <algorithm>
#include <typeinfo>
#include <ctime>

#define JSONRPC_NS_BEGIN namespace jsonrpc {

#define JSONRPC_NS_END   }


/// Namespace for the JSON RPC implementation
JSONRPC_NS_BEGIN

/// Forward declarations
using   json_h = nlohmann::basic_json<>;
typedef std::string    Unicode;

/// Class representing an JSON-RPC array structure
/** 
 *
 *  \author  M.Frank
 *  \version 1.0
 *  \date    02.05.2017
 */
class Array   {
public:
  /// Reference to owner root element <array/>
  json_h root;

public:
  /// Initializing constructor
  Array(const json_h& root);
  /// Initializing constructor
  Array(json_h&& root);
  /// Default constructor. You have to know what you do here!
  Array() = default;
  /// Copy constructor
  Array(const Array& copy) = delete;
  /// Move constructor
  Array(Array&& copy) = default;
  /// Default destructor
  ~Array();
  /// Assignment operator
  Array& operator=(const Array& c) = delete;
  /// Retrieve string representation of the object
  std::string str() const;
  /// Access to the array's root element
  const json_h& array()  const;
  /// Add a new value to the array
  template <typename T> const json_h& add(const T& val);
};

/// Class representing an JSON-RPC <struct/> structure
/** 
 *
 *  \author  M.Frank
 *  \version 1.0
 *  \date    02.05.2017
 */
class Structure   {
public:
  /// Reference to owner root element <struct/>
  json_h root;

public:
  /// Initializing constructor
  Structure(const json_h& root);
  /// Initializing constructor
  Structure(json_h&& root);
  /// Default constructor. You have to know what you do here!
  Structure() = default;
  /// Copy constructor
  Structure(const Structure& copy) = delete;
  /// Move constructor
  Structure(Structure&& copy) = default;
  /// Default destructor
  ~Structure();
  /// Assignment operator
  Structure& operator=(const Structure& c) = delete;
  /// Retrieve string representation of the object
  std::string str() const;
  /// Access to the array's root element
  const json_h& structure()  const;
  /// Add a new value to the array
  template <typename T> const json_h& add(const std::string& nam, const T& val);
};


/// Class representing an JSON-RPC method call to a remote object
/** 
 *
 *  \author  M.Frank
 *  \version 1.0
 *  \date    02.05.2017
 */
class MethodCall  {
public:
  /// Parameter tag
  json_h      root;

private:
  /// Server interface: Check validity of passed document
  void check();

public:
  /// Client interface to create MethodCalls: Default constructor
  MethodCall();
  /// Initializing constructor
  MethodCall(json_h&& root);
  /// Initializing constructor parsing initial text object
  MethodCall(const char* json_text);
  /// Initializing constructor parsing initial text object
  MethodCall(const std::string& json_text);
  /// Copy constructor
  MethodCall(const MethodCall& copy) = delete;
  /// Move constructor
  MethodCall(MethodCall&& copy);
  /// Default destructor
  ~MethodCall();
  /// Assignment operator
  MethodCall& operator= (MethodCall&& copy);
  /// Assignment operator
  MethodCall& operator= (const MethodCall& copy) = delete;
  /// Retrieve string representation of the object
  std::string str() const;
  /// Access the RPC call sequence id
  int id()  const;
  /// Access the <methodName> field of the call
  std::string method() const;
  /// Number of arguments supplied
  size_t numArgs() const;
  /// Access parameter data element by true type
  template <typename T> T arg(size_t handle)   const;
  /// Access call paramaters array
  json_h& params()  const;
  /// Access call paramaters array
  //const json_h& params()   const;
  /** Client interface to create proper MethodCalls from scratch
   */
  /// Add <methodName> tag to the call stricture
  const MethodCall& setMethod(const std::string& name);
  /// Specific add argument entry <param> to the call stricture
  template <typename T> const MethodCall& addParam(const T& value)  const;
};

///  Class representing the response of a call to a JSON-RPC server method 
/**
 *
 *  \author  M.Frank
 *  \version 1.0
 *  \date    02.05.2017
 */
class MethodResponse  {
public:
  json_h root;

  /// Check validity of passed document
  void check();

public:
  /// Default constructor
  MethodResponse();
  /// Default constructor
  MethodResponse(int id);
  /// Initializing constructor with existing json object
  MethodResponse(json_h&& object);
  /// Initializing constructor from existing JSON document
  MethodResponse(const std::string& json);
  /// Initializing constructor from existing JSON document
  MethodResponse(const void* json, size_t num_bytes);
  /// Initializing constructor from existing JSON document
  MethodResponse(const std::vector<char>& json_text);
  /// Copy constructor
  MethodResponse(const MethodResponse& copy) = delete;
  /// Move constructor
  MethodResponse(MethodResponse&& copy) = default;
  /// Default destructor
  ~MethodResponse() = default;
  /// Assignment operator
  MethodResponse& operator=(const MethodResponse& c) = delete;
  /// Move assignment operation
  MethodResponse& operator=(MethodResponse&& copy) = default;
  /// Set proper answer ID for the client response
  MethodResponse& setId(int id);
  /// Create a response with no return value
  static MethodResponse make();
  /// Add fully qualified json result to the response
  static MethodResponse make(json_h&& result);
  /// Add fully qualified json result to the response
  static MethodResponse make(MethodResponse&& result);
  /// Create a response with a parameter value
  template <typename T> static MethodResponse make(const T& param);
  /// Retrieve string representation of the object
  std::string str() const;
  /// Access the data content of the JSONRPC response structure
  json_h& value();
  /// Check if the response structure contains fault information
  bool isFault()  const;
  /// Check if the response structure contains <params>
  bool isOK()  const;
  /// Access the faults structure from the response
  json_h& fault();
  /// Access the fault code (requires a fault element in the response)
  int faultCode()  const;
  /// Access the fault message string (requires a fault element in the response)
  std::string faultString()  const;
  template <typename T> static MethodResponse encode(const T& p);
  /// Decode the response of a server
  static MethodResponse decode(const std::string& json);
  /// Decode the response of a server
  static MethodResponse decode(const void* ptr, size_t num_bytes);
  /// Decode the response of a server
  static MethodResponse decode(const std::vector<unsigned char>& json);
  /// Set fault information in case of failure. Message is strerror(code).
  static MethodResponse makeFault(int id, int code);
  /// Set fault information in case of failure
  static MethodResponse makeFault(int id, int code, const std::string& reason);
  /// Set fault information in case of failure
  static MethodResponse makeFault(int code, const std::string& reason);
  /// Access return value data element by true type
  template <typename T> T data()   const;
};

/// Definition of the generic callback structure for member functions
/**
 *  The callback structure allows to wrap any member function with up to
 *  3 arguments into an abstract objects, which could later be called
 *  using the argument list. The pointer to the hosting objects is stored
 *  in the callback object. The callback objects in this sense behaves
 *  very similar to a static function.
 *
 *  \author  M.Frank
 *  \date    01/03/2013
 *  \version 1.0
 *  \ingroup DD4HEP
 */
class Call {
public:
  typedef MethodResponse (*func_t)(void* obj, const void* fun, const MethodCall& c);
  /// Structure definition to store callback related data
  typedef struct {
    void *first, *second;
  } mfunc_t;

  void* par;
  func_t call;
  mfunc_t func;

  /// Default constructor
  Call()
    : par(0), call(0) {
    func.first = func.second = 0;
  }
  /// Constructor with object initialization
  Call(void* p)
    : par(p), call(0) {
    func.first = func.second = 0;
  }
  /// Initializing constructor
  Call(void* p, void* mf, func_t c)
    : par(p), call(c) {
    func = *(mfunc_t*) mf;
  }
  /// Check validity of the callback object
  operator bool() const {
    return (call && par && func.first);
  }
  /// Execute the callback with the required number of user parameters
  MethodResponse execute(const MethodCall& c) const {
    return (*this) ? call(par, &func, c) : MethodResponse::makeFault(c.id(),EINVAL,"Unknown method");
  }
  /// Template cast function used internally by the wrapper for type conversion to the object's type
  template <typename T> static T* cast(void* p) {
    return (T*) p;
  }
  /// Template const cast function used internally by the wrapper for type conversion to the object's type
  template <typename T> static const T* c_cast(const void* p) {
    return (const T*) p;
  }

  /// Wrapper around a C++ member function pointer
  /**
   *  Generic wrapper around a object member function.
   *
   *  \author  M.Frank
   *  \date    01/03/2013
   *  \version 1.0
   *  \ingroup DD4HEP
   */
  template <typename T> class Wrapper {
  public:
    typedef T pmf_t;
    /// Union to store pointers to member functions in callback objects
    union Functor {
      mfunc_t ptr;
      pmf_t pmf;
      Functor(const void* f) {
        ptr = *(mfunc_t*) f;
      }
      Functor(pmf_t f) {
        pmf = f;
      }
    };
    static mfunc_t pmf(pmf_t f) {
      const Functor func(f);
      return func.ptr;
    }
  };

  template <typename T> struct ArgType {
    typedef typename std::remove_const<typename std::remove_reference<T>::type>::type type;
  };

  /** Callback setup with no arguments  */
  /// Callback setup function for Callbacks with member functions taking no arguments
  template <typename T> const Call& _make(MethodResponse (*fptr)(void* o, const void* f, const MethodCall& c), T pmf) {
    typename Wrapper<T>::Functor f(pmf);
    func = f.ptr;
    call = fptr;
    return *this;
  }
  /// Callback setup function for Callbacks with member functions with explicit return type taking no arguments
  template <typename R, typename T> const Call& make(R (T::*pmf)()) {
    typedef R (T::*pfunc_t)();
    struct _Wrapper : public Wrapper<pfunc_t> {
      static MethodResponse call(void* o, const void* f, const MethodCall&) {
        return MethodResponse::make((cast<T>(o)->*(typename Wrapper<pfunc_t>::Functor(f).pmf))());
      }
    };
    return _make(_Wrapper::call, pmf);
  }
  /// Callback setup function for Callbacks with const member functions with explicit return type taking no arguments
  template <typename R, typename T> const Call& make(R (T::*pmf)() const) {
    typedef R (T::*pfunc_t)() const;
    struct _Wrapper : public Wrapper<pfunc_t> {
      static MethodResponse call(void* o, const void* f, const MethodCall&) {
        return MethodResponse::make((cast<T>(o)->*(typename Wrapper<pfunc_t>::Functor(f).pmf))());
      }
    };
    return _make(_Wrapper::call, pmf);
  }
  /// Callback setup function for Callbacks with void member functions taking no arguments
  template <typename T> const Call& make(void (T::*pmf)()) {
    typedef void (T::*pfunc_t)() const;
    struct _Wrapper : public Wrapper<pfunc_t> {
      static MethodResponse call(void* o, const void* f, const MethodCall&) {
        (cast<T>(o)->*(typename Wrapper<pfunc_t>::Functor(f).pmf))();
        return MethodResponse::make();
      }
    };
    return _make(_Wrapper::call, pmf);
  }
  /// Callback setup function for Callbacks with const void member functions taking no arguments
  template <typename T> const Call& make(void (T::*pmf)() const) {
    typedef void (T::*pfunc_t)() const;
    struct _Wrapper : public Wrapper<pfunc_t> {
      static MethodResponse call(void* o, const void* f, const MethodCall&) {
        (cast<T>(o)->*(typename Wrapper<pfunc_t>::Functor(f).pmf))();
        return MethodResponse::make();
      }
    };
    return _make(_Wrapper::call, pmf);
  }

  /** Callbacks with 1 argument  */
  /// Callback setup function for Callbacks with member functions with explicit return type taking 1 argument
  template <typename R, typename T, typename A0> const Call& make(R (T::*pmf)(A0)) {
    typedef R (T::*pfunc_t)(A0);
    struct _Wrapper : public Wrapper<pfunc_t> {
      static MethodResponse call(void* o, const void* f, const MethodCall& c) {
        typename ArgType<A0>::type a0 = c.arg<typename ArgType<A0>::type>(0);
        return MethodResponse::make((cast<T>(o)->*(typename Wrapper<pfunc_t>::Functor(f).pmf))(a0));
      }
    };
    return _make(_Wrapper::call, pmf);
  }
  /// Callback setup function for Callbacks with const member functions with explicit return type taking 1 argument
  template <typename R, typename T, typename A0> const Call& make(R (T::*pmf)(A0) const) {
    typedef R (T::*pfunc_t)(A0) const;
    struct _Wrapper : public Wrapper<pfunc_t> {
      static MethodResponse call(void* o, const void* f, const MethodCall& c) {
        typename ArgType<A0>::type a0 = c.arg<typename ArgType<A0>::type>(0);
        return MethodResponse::make((cast<T>(o)->*(typename Wrapper<pfunc_t>::Functor(f).pmf))(a0));
      }
    };
    return _make(_Wrapper::call, pmf);
  }
  /// Callback setup function for Callbacks with void member functions taking 1 argument
  template <typename T, typename A0> const Call& make(void (T::*pmf)(A0)) {
    typedef void (T::*pfunc_t)(const A0);
    struct _Wrapper : public Wrapper<pfunc_t> {
      static MethodResponse call(void* o, const void* f, const MethodCall& c) {
        typename ArgType<A0>::type a0 = c.arg<typename ArgType<A0>::type>(0);
        (cast<T>(o)->*(typename Wrapper<pfunc_t>::Functor(f).pmf))(a0);
        return MethodResponse::make();
      }
    };
    return _make(_Wrapper::call, pmf);
  }
  /// Callback setup function for Callbacks with const void member functions taking 1 argument
  template <typename T, typename A0> const Call& make(void (T::*pmf)(A0) const) {
    typedef void (T::*pfunc_t)(const A0) const;
    struct _Wrapper : public Wrapper<pfunc_t> {
      static MethodResponse call(void* o, const void* f, const MethodCall& c) {
        typename ArgType<A0>::type a0 = c.arg<typename ArgType<A0>::type>(0);
        (cast<T>(o)->*(typename Wrapper<pfunc_t>::Functor(f).pmf))(a0);
        return MethodResponse::make();
      }
    };
    return _make(_Wrapper::call, pmf);
  }

  /** Callback with 2 arguments     */

  /// Callback setup function for Callbacks with member functions with explicit return type taking 2 arguments
  template <typename R, typename T, typename A0, typename A1> const Call& make(R (T::*pmf)(A0, A1)) {
    typedef R (T::*pfunc_t)(A0, A1);
    typedef Wrapper<pfunc_t> _W;
    struct _Wrapper : public _W {
      static MethodResponse call(void* o, const void* f, const MethodCall& c) {
        typename ArgType<A0>::type a0 = c.arg<typename ArgType<A0>::type>(0);
        typename ArgType<A1>::type a1 = c.arg<typename ArgType<A1>::type>(1);
        return MethodResponse::make((cast<T>(o)->*(typename _W::Functor(f).pmf))(a0, a1));
      }
    };
    return _make(_Wrapper::call, pmf);
  }
  /// Callback setup function for Callbacks with const member functions with explicit return type taking 2 arguments
  template <typename R, typename T, typename A0, typename A1> const Call& make(R (T::*pmf)(A0, A1) const) {
    typedef R (T::*pfunc_t)(A0, A1);
    typedef Wrapper<pfunc_t> _W;
    struct _Wrapper : public _W {
      static MethodResponse call(void* o, const void* f, const MethodCall& c) {
        typename ArgType<A0>::type a0 = c.arg<typename ArgType<A0>::type>(0);
        typename ArgType<A1>::type a1 = c.arg<typename ArgType<A1>::type>(1);
        return MethodResponse::make((cast<T>(o)->*(typename _W::Functor(f).pmf))(a0, a1));
      }
    };
    return _make(_Wrapper::call, pmf);
  }
  /// Callback setup function for Callbacks with const void member functions taking 2 arguments
  template <typename T, typename A0, typename A1> const Call& make(void (T::*pmf)(A0, A1)) {
    typedef void (T::*pfunc_t)(A0, A1);
    typedef Wrapper<pfunc_t> _W;
    struct _Wrapper : public _W {
      static MethodResponse call(void* o, const void* f, const MethodCall& c) {
        typename ArgType<A0>::type a0 = c.arg<typename ArgType<A0>::type>(0);
        typename ArgType<A1>::type a1 = c.arg<typename ArgType<A1>::type>(1);
        (cast<T>(o)->*(typename _W::Functor(f).pmf))(a0, a1);
        return MethodResponse::make();
      }
    };
    return _make(_Wrapper::call, pmf);
  }
  /// Callback setup function for Callbacks with const void member functions taking 2 arguments
  template <typename T, typename A0, typename A1> const Call& make(void (T::*pmf)(A0, A1) const) {
    typedef void (T::*pfunc_t)(A0, A1);
    typedef Wrapper<pfunc_t> _W;
    struct _Wrapper : public _W {
      static MethodResponse call(void* o, const void* f, const MethodCall& c) {
        typename ArgType<A0>::type a0 = c.arg<typename ArgType<A0>::type>(0);
        typename ArgType<A1>::type a1 = c.arg<typename ArgType<A1>::type>(1);
        (cast<T>(o)->*(typename _W::Functor(f).pmf))(a0, a1);
        return MethodResponse::make();
      }
    };
    return _make(_Wrapper::call, pmf);
  }

  /** Callback setup for callbacks with 3 arguments  */

  /// Callback setup function for Callbacks with member functions with explicit return type taking 3 arguments
  template <typename R, typename T, typename A0, typename A1, typename A2> const Call& make(R (T::*pmf)(A0, A1, A2)) {
    typedef R (T::*pfunc_t)(A0, A1, A2);
    typedef Wrapper<pfunc_t> _W;
    struct _Wrapper : public _W {
      static MethodResponse call(void* o, const void* f, const MethodCall& c) {
        typename ArgType<A0>::type a0 = c.arg<typename ArgType<A0>::type>(0);
        typename ArgType<A1>::type a1 = c.arg<typename ArgType<A1>::type>(1);
        typename ArgType<A2>::type a2 = c.arg<typename ArgType<A2>::type>(2);
        return MethodResponse::make((cast<T>(o)->*(typename _W::Functor(f).pmf))(a0, a1, a2));
      }
    };
    return _make(_Wrapper::call, pmf);
  }
  /// Callback setup function for Callbacks with const member functions with explicit return type taking 3 arguments
  template <typename R, typename T, typename A0, typename A1, typename A2> const Call& make(R (T::*pmf)(A0, A1, A2) const) {
    typedef R (T::*pfunc_t)(A0, A1, A2);
    typedef Wrapper<pfunc_t> _W;
    struct _Wrapper : public _W {
      static MethodResponse call(void* o, const void* f, const MethodCall& c) {
        typename ArgType<A0>::type a0 = c.arg<typename ArgType<A0>::type>(0);
        typename ArgType<A1>::type a1 = c.arg<typename ArgType<A1>::type>(1);
        typename ArgType<A2>::type a2 = c.arg<typename ArgType<A2>::type>(2);
        return MethodResponse::make((cast<T>(o)->*(typename _W::Functor(f).pmf))(a0, a1, a2));
      }
    };
    return _make(_Wrapper::call, pmf);
  }
  /// Callback setup function for Callbacks with const void member functions taking 3 arguments
  template <typename T, typename A0, typename A1, typename A2> const Call& make(void (T::*pmf)(A0, A1, A2)) {
    typedef void (T::*pfunc_t)(A0, A1, A2);
    typedef Wrapper<pfunc_t> _W;
    struct _Wrapper : public _W {
      static MethodResponse call(void* o, const void* f, const MethodCall& c) {
        typename ArgType<A0>::type a0 = c.arg<typename ArgType<A0>::type>(0);
        typename ArgType<A1>::type a1 = c.arg<typename ArgType<A1>::type>(1);
        typename ArgType<A2>::type a2 = c.arg<typename ArgType<A2>::type>(2);
        (cast<T>(o)->*(typename _W::Functor(f).pmf))(a0, a1, a2);
        return MethodResponse::make();
      }
    };
    return _make(_Wrapper::call, pmf);
  }
  /// Callback setup function for Callbacks with const void member functions taking 3 arguments
  template <typename T, typename A0, typename A1, typename A2> const Call& make(void (T::*pmf)(A0, A1, A2) const) {
    typedef void (T::*pfunc_t)(A0, A1, A2);
    typedef Wrapper<pfunc_t> _W;
    struct _Wrapper : public _W {
      static MethodResponse call(void* o, const void* f, const MethodCall& c) {
        typename ArgType<A0>::type a0 = c.arg<typename ArgType<A0>::type>(0);
        typename ArgType<A1>::type a1 = c.arg<typename ArgType<A1>::type>(1);
        typename ArgType<A2>::type a2 = c.arg<typename ArgType<A2>::type>(2);
        (cast<T>(o)->*(typename _W::Functor(f).pmf))(a0, a1, a2);
        return MethodResponse::make();
      }
    };
    return _make(_Wrapper::call, pmf);
  }

  template <typename T> static Call make(void* p, T pmf) {
    return Call(p).make(pmf);
  }

  template <typename P, typename R, typename T> static T* dyn_cast(P* p, R (T::*)()) {
    return dynamic_cast<T*>(p);
  }
  template <typename P, typename R, typename T> static const T* dyn_cast(const P* p, R (T::*)() const) {
    return dynamic_cast<const T*>(p);
  }

  template <typename P, typename R, typename T, typename A> static T* dyn_cast(P* p, R (T::*)(A)) {
    return dynamic_cast<T*>(p);
  }
  template <typename P, typename R, typename T, typename A> static const T* dyn_cast(const P* p, R (T::*)(A) const) {
    return dynamic_cast<const T*>(p);
  }
};


/// Definition of an actor on sequences of callbacks
/**
 *  A callback sequence is a set of callbacks with identical signature, so that
 *  the can be called consecutively using the same arguments.
 *
 *  \author  M.Frank
 *  \date    01/03/2013
 *  \version 0.1
 *  \ingroup DD4HEP
 */
struct CallSequence {
  typedef std::vector<Call> Calls;
  enum Location { FRONT, END };
  Calls callbacks;
  /// Default constructor
  CallSequence() {
  }
  /// Copy constructor
  CallSequence(const CallSequence& c);

  /// Assignment operator
  CallSequence& operator=(const CallSequence& c)  {
    if ( this != & c ) callbacks = c.callbacks;
    return *this;
  }

  //template <typename TYPE, typename R, typename OBJECT>
  //  CallSequence(const std::vector<TYPE*>& objects, R (TYPE::value_type::*pmf)())  {
  //}
  bool empty() const {
    return callbacks.empty();
  }
  /// Clear the sequence and remove all callbacks
  void clear() {
    callbacks.clear();
  }
  /// Generically Add a new callback to the sequence depending on the location arguments
  void add(const Call& cb,Location where) {
    if ( where == CallSequence::FRONT )
      callbacks.insert(callbacks.begin(),cb);
    else
      callbacks.insert(callbacks.end(),cb);
  }
  /// Execution overload for callbacks with no arguments
  void operator()(const MethodCall& c) const;
  /// Check the compatibility of two typed objects. The test is the result of a dynamic_cast
  static void checkTypes(const std::type_info& typ1, const std::type_info& typ2, void* test);

  /** Callback setup for callbacks with no arguments  */
  /// Add a new callback to a member function with explicit return type and no arguments
  template <typename TYPE, typename R, typename OBJECT>
  void add(TYPE* pointer, R (OBJECT::*pmf)(),Location where=CallSequence::END) {
    checkTypes(typeid(TYPE), typeid(OBJECT), dynamic_cast<OBJECT*>(pointer));
    add(Call(pointer).make(pmf),where);
  }
  /// Add a new callback to a const member function with explicit return type and no arguments
  template <typename TYPE, typename R, typename OBJECT>
  void add(TYPE* pointer, R (OBJECT::*pmf)() const,Location where=CallSequence::END) {
    checkTypes(typeid(TYPE), typeid(OBJECT), dynamic_cast<OBJECT*>(pointer));
    add(Call(pointer).make(pmf),where);
  }
  /// Add a new callback to a void member function with no arguments
  template <typename TYPE, typename OBJECT>
  void add(TYPE* pointer, void (OBJECT::*pmf)(),Location where=CallSequence::END) {
    checkTypes(typeid(TYPE), typeid(OBJECT), dynamic_cast<OBJECT*>(pointer));
    add(Call(pointer).make(pmf),where);
  }
  /// Add a new callback to a const void member function and no arguments
  template <typename TYPE, typename OBJECT>
  void add(TYPE* pointer, void (OBJECT::*pmf)() const,Location where=CallSequence::END) {
    checkTypes(typeid(TYPE), typeid(OBJECT), dynamic_cast<OBJECT*>(pointer));
    add(Call(pointer).make(pmf),where);
  }

  /** Callback setup for callbacks with 1 argument  */
  /// Add a new callback to a member function with explicit return type and 1 argument
  template <typename TYPE, typename R, typename OBJECT, typename A>
  void add(TYPE* pointer, R (OBJECT::*pmf)(A),Location where=CallSequence::END) {
    checkTypes(typeid(TYPE), typeid(OBJECT), dynamic_cast<OBJECT*>(pointer));
    add(Call(pointer).make(pmf),where);
  }
  /// Add a new callback to a void member function and 1 argument
  template <typename TYPE, typename OBJECT, typename A>
  void add(TYPE* pointer, void (OBJECT::*pmf)(A),Location where=CallSequence::END) {
    checkTypes(typeid(TYPE), typeid(OBJECT), dynamic_cast<OBJECT*>(pointer));
    add(Call(pointer).make(pmf),where);
  }
  /// Add a new callback to a const member function with explicit return type and 1 argument
  template <typename TYPE, typename R, typename OBJECT, typename A>
  void add(TYPE* pointer, R (OBJECT::*pmf)(A) const,Location where=CallSequence::END) {
    checkTypes(typeid(TYPE), typeid(OBJECT), dynamic_cast<OBJECT*>(pointer));
    add(Call(pointer).make(pmf),where);
  }
  /// Add a new callback to a const void member function and 1 argument
  template <typename TYPE, typename OBJECT, typename A>
  void add(TYPE* pointer, void (OBJECT::*pmf)(A) const,Location where=CallSequence::END) {
    checkTypes(typeid(TYPE), typeid(OBJECT), dynamic_cast<OBJECT*>(pointer));
    add(Call(pointer).make(pmf),where);
  }


  /** Callback setup for callbacks with 2 arguments  */
  /// Add a new callback to a member function with explicit return type and 2 arguments
  template <typename TYPE, typename R, typename OBJECT, typename A1, typename A2>
  void add(TYPE* pointer, R (OBJECT::*pmf)(A1, A2),Location where=CallSequence::END) {
    checkTypes(typeid(TYPE), typeid(OBJECT), dynamic_cast<OBJECT*>(pointer));
    add(Call(pointer).make(pmf),where);
  }
  /// Add a new callback to a const member function with explicit return type and 2 arguments
  template <typename TYPE, typename R, typename OBJECT, typename A1, typename A2>
  void add(TYPE* pointer, R (OBJECT::*pmf)(A1, A2) const,Location where=CallSequence::END) {
    checkTypes(typeid(TYPE), typeid(OBJECT), dynamic_cast<OBJECT*>(pointer));
    add(Call(pointer).make(pmf),where);
  }
  /// Add a new callback to a void member function with 2 arguments
  template <typename TYPE, typename OBJECT, typename A1, typename A2>
  void add(TYPE* pointer, void (OBJECT::*pmf)(A1, A2),Location where=CallSequence::END) {
    checkTypes(typeid(TYPE), typeid(OBJECT), dynamic_cast<OBJECT*>(pointer));
    add(Call(pointer).make(pmf),where);
  }
  /// Add a new callback to a const void member function with 2 arguments
  template <typename TYPE, typename OBJECT, typename A1, typename A2>
  void add(TYPE* pointer, void (OBJECT::*pmf)(A1, A2) const,Location where=CallSequence::END) {
    checkTypes(typeid(TYPE), typeid(OBJECT), dynamic_cast<OBJECT*>(pointer));
    add(Call(pointer).make(pmf),where);
  }
};

template <typename T> inline MethodResponse MethodResponse::encode(const T& p)   {
  MethodResponse r(0UL);
  json_h res = p;
  r.root["result"] = res;
  return r;
}
template <> inline MethodResponse MethodResponse::encode(const json_h& p)   {
  MethodResponse r(0UL);
  r.root["result"] = p;
  return r;
}

JSONRPC_NS_END

#include <vector>
#include <list>
#include <set>

JSONRPC_NS_BEGIN

template <typename T> void insert_container(std::vector<T>& c, T&& v) { c.emplace_back(v); }
template <typename T> void insert_container(std::list<T>& c, T&& v)   { c.emplace_back(v); }
template <typename T> void insert_container(std::set<T>& c, T&& v)    { c.emplace(v);      }

template <typename T, typename V> void container_to_json(json_h& j, const T& cont)   {
  j = json_h::array();
  for(const auto& o : cont)
    j.push_back(o);
}

template <typename T, typename V> void container_from_json(const json_h& j, T& c) {  
  c.clear();
  if ( j.is_array() )    {
    json_h::array_t arr = j;
    for(auto it = arr.begin(); it != arr.end(); ++it)  {
      V o = *it;
      insert_container(c, std::move(o));
    }
    return;
  }
  throw std::runtime_error("Cannot convert non-array json to STL containers");
}

template <typename T> MethodResponse _make_result(const T& b)   {
  MethodResponse r(0UL); json_h res;
  to_json(res, b);
  r.root["result"] = res;
  return r;
}

JSONRPC_NS_END

#define JSONRPC_IMPLEMENT_OBJECT_MARSCHALLING(typ,clazz)		\
  JSONRPC_NS_BEGIN							\
  /* Add a new parameter to the response		*/		\
  template <>								\
  MethodResponse MethodResponse::make<clazz>(const clazz& b)   {	\
    json_h res;								\
    to_json(res, b);							\
    return MethodResponse::encode(res);					\
  }									\
									\
  /* Add a new parameter to the response		*/		\
  template <> clazz MethodResponse::data<clazz>()  const  {		\
    clazz obj;								\
    from_json(this->root, obj);						\
    return obj;								\
  }									\
									\
  /* Access method call argument by index */				\
  template <> clazz MethodCall::arg<clazz>(size_t which)  const  {	\
    clazz obj;								\
    const json_h& jh = this->params();					\
    const json_h& ah = jh[which];					\
    from_json(ah, obj);							\
    return obj;								\
  }									\
									\
  /* Add a new parameter to the method call		*/		\
  template <> const MethodCall&						\
  MethodCall::addParam<clazz>(const clazz& b)  const  {			\
    Structure str;							\
    to_json(str.root, b);						\
    return *this;							\
  }									\
  JSONRPC_NS_END

#define JSONRPC_IMPLEMENT_CONTAINER_MARSCHALLING(typ,X)			\
  namespace std {							\
    void to_json(jsonrpc::json_h& j, const X& c) {		\
      jsonrpc::container_to_json<X,X::value_type>(j,c);	           	\
    }									\
    void from_json(const jsonrpc::json_h& j, X& c) {		\
      jsonrpc::container_from_json<X,X::value_type>(j,c);	        \
    }                                                                   \
  }			                                                \
  JSONRPC_IMPLEMENT_OBJECT_MARSCHALLING(typ,X)

#define JSONRPC_IMPLEMENT_OBJECT_CONTAINER(typ,X)		        \
  JSONRPC_IMPLEMENT_OBJECT_MARSCHALLING(typ,X)			        \
  JSONRPC_IMPLEMENT_CONTAINER_MARSCHALLING(class,std::vector<X>)	\
  JSONRPC_IMPLEMENT_CONTAINER_MARSCHALLING(class,std::list<X>)	        \
  JSONRPC_IMPLEMENT_CONTAINER_MARSCHALLING(class,std::set<X>)

#endif /* ONLINE_RPC_JSONRPC_H  */
