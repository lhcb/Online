//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_XML_XMLRPC_H
#define ONLINE_XML_XMLRPC_H

// Framework include files
#include <XML/XML.h>
#include <RPC/XMLRPCTags.h>

// C/C++ include files
#include <algorithm>
#include <typeinfo>
#include <ctime>
#include <list>
#include <set>
#include <map>

bool operator<(const tm&, const tm&);

#define XMLRPC_NS_BEGIN namespace xmlrpc {
#define XMLRPC_NS_END   }


/// Namespace for the XML RPC implementation
XMLRPC_NS_BEGIN

std::string    _toString(const struct tm& time);
struct tm      _toTime(const std::string& s);

std::string    _toString(const struct timeval& time);
struct timeval _toTimeVal(const std::string& s);

/// Definition of a single field inside a row
/** Useful to interprete database queries with returned row-sets.
 *
 *  \author  M.Frank
 *  \version 1.0
 */
class Field : public xml_elt_t  {
public:
  /// Initializing constructor
  Field(xml_h elt);
  /// Copy constructor
  Field(const Field& copy) = default;
  /// Move constructor
  Field(Field&& copy) = default;
  /// Default destructor
  virtual ~Field() = default;
  /// Copy assignment
  Field& operator= (const Field& copy) = default;
  /// Move assignment
  Field& operator= (Field&& copy) = default;      
  /// Access xml data type as closest C++ data type
  const std::type_info& type_info()  const;
  /// Access xml data type in string form
  std::string type() const;
  /// Retrieve string representation of the object
  std::string str() const;
  /// Access interpreted field data 
  template <typename T> T get()  const;
  /// Access interpreted field data 
  template <typename T> static std::string type_name();
};


/// Class representing an XML-RPC <struct/> structure
/** 
 *
 *  \author  M.Frank
 *  \version 1.0
 *  \date    02.05.2017
 */
class Structure   {
 public:
  /// Field definition
  typedef xmlrpc::Field Field;
  
  /// Reference to owner root element <struct/>
  xml_h root;

 public:
  /// Initializing constructor
  Structure(xml_h root);
  /// Default constructor. You have to know what you do here!
  Structure() = default;
  /// Copy constructor
  Structure(const Structure& copy) = delete;
  /// Move constructor
  Structure(Structure&& copy) = default;
  /// Default destructor
  ~Structure();
  /// Assignment operator
  Structure& operator=(const Structure& c) = delete;
  /// Retrieve string representation of the object
  std::string str() const;
  /// Access to the array's root element
  xml_h structure()  const;
  /// Add a new value to the array
  xml_h addMember(const std::string& nam, const Unicode& typ, const std::string& val);
  /// Add a new value to the array
  template <typename T> xml_h add(const std::string& nam, const T& val);
};

/// Class representing an XML-RPC array structure
/** 
 *
 *  \author  M.Frank
 *  \version 1.0
 *  \date    02.05.2017
 */
class Array   {
 public:
  /// Field definition
  typedef xmlrpc::Field Entry;
  /// Reference to owner root element <array/>
  xml_h root;

 public:
  /// Initializing constructor: Create new object in document or use existing object
  Array(xml_h root);
  /// Default constructor. You have to know what you do here!
  Array() = default;
  /// Copy constructor
  Array(const Array& copy) = delete;
  /// Move constructor
  Array(Array&& copy) = default;
  /// Default destructor
  ~Array();
  /// Move assignment operator
  Array& operator=(Array&& c) = default;
  /// Assignment operator
  Array& operator=(const Array& c) = delete;
  /// Retrieve string representation of the object
  std::string str() const;
  /// Access to the array's root element
  xml_h array()  const;
  /// Add a new value to the array
  xml_h addValue(const Unicode& typ, const std::string& val);
  /// Collect elements
  std::vector<xml_h> elements()  const;
  /// Add a new value to the array
  template <typename T> xml_h add(const T& val);
};

/// Helper class to interprete array set as xmlrpc returns
/** Useful to interprete database queries with multiple returned row-sets.
 *
 *  Automatic conversion from row-sets to vectors of Tuples is present.
 *  The tuple can hold data sets with different types as they typically 
 *  result from database queries. Data retrieval is costly, because it
 *  requires the interpretation of the xmlrpc text fields.
 *
 *  \author  M.Frank
 *  \version 1.0
 */
class Tuple : public xml_doc_holder_t  {
public:
  /// Field definition
  typedef xmlrpc::Field Field;

  /// ROOT xml element of this tuple
  xml_h root  { nullptr };
  /// Contained field set
  std::vector<Field> fields;

public:
  /// Default constructor
  Tuple();
  /// Initializing constructor
  Tuple(xml_h handle);
  /// Copy constructor
  Tuple(const Tuple& tuple) = delete;
  /// Move constructor
  Tuple(Tuple&& copy);
  /// Move assignment operator
  Tuple& operator=(Tuple&& c);
  /// Assignment operator
  Tuple& operator=(const Tuple& c) = delete;
  /// Operator less for map/set insertion
  bool operator<(const Tuple& tuple)  const   { return root < tuple.root; }
  /// Collect elements
  std::vector<xml_h> elements()  const;
  /// Field access operator
  Field operator[] (std::size_t which)  const { return fields[which];     }
  /// Access number of fields
  std::size_t size()  const                   { return fields.size();     }
  /// Retrieve string representation of the object
  std::string str() const;
  /// Bind Tuple to string map for names item access
  void bind(std::map<std::string, xml_h>& binding)  const;
  /// Move document
  void set_document(xml_doc_holder_t& doc_holder);
  /// Check if the tuple is an array
  template <typename T> bool is()  const;
  /// Check if the tuple is an array
  template <typename T> T get()  const;  
};

/// Initializing constructor
inline Tuple::Field::Field(xml_h elt) : xml_elt_t(elt)
{
}

/// Class representing an XML-RPC method call to a remote object
/** 
 *
 *  \author  M.Frank
 *  \version 1.0
 *  \date    02.05.2017
 */
class MethodCall : public xml_doc_t  {
 public:
  /// Name of the method call
  std::string name;
  /// Parameter tag
  xml_h       params{0};

 private:
  /// Convert anything integer like
  long intData(xml_h handle)  const;

  /// Server interface: Check validity of passed document
  void check();

 public:
  /// Client interface to create MethodCalls: Default constructor
  MethodCall();
  /// Copy constructor
  MethodCall(const MethodCall& copy) = delete;
  /// Move constructor
  MethodCall(MethodCall&& copy);
  /// Server interface: Initializing constructor
  MethodCall(const char* xml);
  /// Server interface: Initializing constructor
  MethodCall(const std::string& xml);
  /// Server interface: Initializing constructor
  MethodCall(const void* xml, size_t num_bytes);
  /// Default destructor
  ~MethodCall();
  /// Assignment operator
  MethodCall& operator= (MethodCall&& copy);
  /// Assignment operator
  MethodCall& operator= (const MethodCall& copy) = delete;
  /// Retrieve string representation of the object
  std::string str() const;

  /** Server interface to interprete MethodCalls which were 
   *  initialized using a valid xml string.
   *  Note: 
   *  Since the argument list is not cached, direct access is not supported!
   */
  /// Access the <methodName> field of the call
  std::string method() const;
  /// Number of arguments supplied
  std::vector<xml_h> arguments() const;
  /// Access parameter data element by true type
  template <typename T> T data(xml_h handle)   const;
  /// Access parameter data element as a string
  std::string text(xml_h handle)   const;

  /** Client interface to create proper MethodCalls from scratch
   */
  /// Add <methodName> tag to the call stricture
  const MethodCall& setMethod(const std::string& name)  const;
  /// Generic add a new value and a data tag to the request structure.
  xml_elt_t         createParam(const Unicode& tag)  const;
  /// Add a NIL value to the request structure
  const MethodCall& addNil()  const;
  /// Synonym: Add a NIL value to the request structure
  const MethodCall& param()  const;
  /// Generic add a new value to the request structure
  const MethodCall& addParam(const Unicode& tag, const std::string& val)  const;
  /// Specific add argument entry <param> to the call structure
  template <typename T> const MethodCall& addParam(const T& value)  const;
  /// Synonym: Generic add a new value to the request structure
  template <typename T> const MethodCall& param(const T& value)  const;
};

/// Synonym: Add a NIL value to the request structure
inline const MethodCall& MethodCall::param()  const   {
  return this->addNil();
}

/// Synonym: Generic add a new value to the request structure
template <typename T> inline const MethodCall& MethodCall::param(const T& value)  const   {
  return this->addParam(value);
}

///  Class representing the response of a call to a XML-RPC server method 
/**
 *
 *  \author  M.Frank
 *  \version 1.0
 *  \date    02.05.2017
 */
class MethodResponse : public xml_doc_holder_t  {
 public:
  /// Reference to the value element of the response to fill arrays/structures etc.
  xml_h  value{0};

 private:
  /// Convert anything integer like
  long intData()  const;
  /// Safe access: Check response value before using it.
  xml_elt_t checked_value() const;
  /// Safe access: Check response handle before using it.
  xml_elt_t checked_handle() const;

  /// Check validity of passed document
  void check();

 public:
  /// Default constructor
  MethodResponse();
  /// Initializing constructor from existing XML document
  MethodResponse(const std::string& xml);
  /// Initializing constructor from existing XML document
  MethodResponse(const void* xml, size_t num_bytes);
  /// Copy constructor
  MethodResponse(const MethodResponse& copy) = delete;
  /// Move constructor
  MethodResponse(MethodResponse&& copy);
  /// Default destructor
  ~MethodResponse();
  /// Assignment operator
  MethodResponse& operator=(const MethodResponse& c) = delete;
  /// Move assignment operation
  MethodResponse& operator=(MethodResponse&& copy);
  /// Create an empty response structure (void return values)
  static MethodResponse make();
  /// Create a response with a parameter value
  template <typename T> static MethodResponse make(const T& param);
  /// Retrieve string representation of the object
  std::string str() const;
  /// Check if the response structure contains fault information
  bool isFault()  const;
  /// Check if the response structure contains <params>
  bool isOK()  const;
  /// Access the faults structure from the response
  Structure fault()  const;
  /// Access the fault code (requires a fault element in the response)
  int faultCode()  const;
  /// Access the fault message string (requires a fault element in the response)
  std::string faultString()  const;
  /// Decode the response of a server
  static MethodResponse decode(const std::string& xml);
  /// Decode the response of a server
  static MethodResponse decode(const void* ptr, size_t num_bytes);
  /// Decode the response of a server
  static MethodResponse decode(const std::vector<unsigned char>& xml);
  /// Add a parameter value to the response structure
  static MethodResponse makeValue(const Unicode& tag);
  /// Add a parameter value to the response structure
  static MethodResponse makeValue(const Unicode& tag, const std::string& val);
  /// Set fault information in case of failure. Message is strerror(code).
  static MethodResponse makeFault(int code);
  /// Set fault information in case of failure
  static MethodResponse makeFault(int code, const std::string& reason);
  /// Access return value data element as string and type
  std::pair<std::string,xml_h> return_value()   const;
  /// Access return value data element by true type
  template <typename T> T data()   const;
};

struct XmlCoder  {
  /// Reference to the xml element
  xml_h element{0};
  /// Default constructor
  XmlCoder() = default;
  /// Copy constructor
  XmlCoder(const XmlCoder& copy) = default;
  /// Initializing constructor
  XmlCoder(xml_h element);
  /// Conversion routine from xml element to the object
  template <typename T> T to()   const;
  /// Conversion routine from the object to the xml element
  template <typename T> const XmlCoder& from(const T& obj)   const;
};

/// Defintion of the helper object to extract arguments from a RPC MethodCall object
/**
 *  \author  M.Frank
 *  \date    01/03/2013
 *  \version 1.0
 *  \ingroup DD4HEP
 */
struct Arg : public XmlCoder {
#ifdef G__ROOT
  /// Default constructor
  Arg() = default;
#else
  /// Default constructor
  Arg() = delete;
#endif
  /// Copy constructor
  Arg(const Arg& copy) = default;
  /// Initializing constructor
  Arg(xml_h argument);
  /// Helper: Get integer value from XML handle. Extracts <value><boolean>...</boolean></value>
  static bool get_bool(xml_h argument_handle);
  /// Helper: Get integer value from XML handle. Extracts <value><int>...</int></value>
  static long get_int(xml_h argument_handle);
  /// Helper: Get double value from XML handle. Extracts <value><double>...</double></value>
  static double get_double(xml_h argument_handle);
  /// Helper: Get double value from XML handle. Extracts <value><string>...</string></value>
  static std::string get_string(xml_h argument_handle);
  /// Helper: Get time value from XML handle. Extracts <value><dateTime.iso....>...</dateTime.iso...></value>
  static struct tm get_time(xml_h argument_handle);
  /// Helper: Get time value from XML handle. Extracts <value><dateTime.iso....>...</dateTime.iso...></value>
  static struct timeval get_timeval(xml_h argument_handle);
};

/// Definition of the generic callback structure for member functions
/**
 *  The callback structure allows to wrap any member function with up to
 *  3 arguments into an abstract objects, which could later be called
 *  using the argument list. The pointer to the hosting objects is stored
 *  in the callback object. The callback objects in this sense behaves
 *  very similar to a static function.
 *
 *  \author  M.Frank
 *  \date    01/03/2013
 *  \version 1.0
 *  \ingroup DD4HEP
 */
class Call {
 public:
  typedef MethodResponse (*func_t)(void* obj, const void* fun, const MethodCall& c);
  /// Structure definition to store callback related data
  typedef struct {
    void *first, *second;
  } mfunc_t;

  void* par;
  func_t call;
  mfunc_t func;

  /// Default constructor
 Call()
   : par(0), call(0) {
    func.first = func.second = 0;
  }
  /// Constructor with object initialization
 Call(void* p)
   : par(p), call(0) {
    func.first = func.second = 0;
  }
  /// Initializing constructor
 Call(void* p, void* mf, func_t c)
   : par(p), call(c) {
    func = *(mfunc_t*) mf;
  }
  /// Check validity of the callback object
  operator bool() const {
    return (call && par && func.first);
  }
  /// Execute the callback with the required number of user parameters
  MethodResponse execute(const MethodCall& c) const {
    return (*this) ? call(par, &func, c) : MethodResponse::makeFault(EINVAL,"Unknown method");
  }
  /// Template cast function used internally by the wrapper for type conversion to the object's type
  template <typename T> static T* cast(void* p) {
    return (T*) p;
  }
  /// Template const cast function used internally by the wrapper for type conversion to the object's type
  template <typename T> static const T* c_cast(const void* p) {
    return (const T*) p;
  }

  /// Wrapper around a C++ member function pointer
  /**
   *  Generic wrapper around a object member function.
   *
   *  \author  M.Frank
   *  \date    01/03/2013
   *  \version 1.0
   *  \ingroup DD4HEP
   */
  template <typename T> class Wrapper {
  public:
    typedef T pmf_t;
    /// Union to store pointers to member functions in callback objects
    union Functor {
      mfunc_t ptr;
      pmf_t pmf;
      Functor(const void* f) {
	ptr = *(mfunc_t*) f;
      }
      Functor(pmf_t f) {
	pmf = f;
      }
    };
    static mfunc_t pmf(pmf_t f) {
      const Functor func(f);
      return func.ptr;
    }
  };

  template <typename T> struct ArgType {
    typedef typename std::remove_const<typename std::remove_reference<T>::type>::type type;
  };

  /** Callback setup with no arguments  */
  /// Callback setup function for Callbacks with member functions taking no arguments
  template <typename T> const Call& _make(MethodResponse (*fptr)(void* o, const void* f, const MethodCall& c), T pmf) {
    typename Wrapper<T>::Functor f(pmf);
    func = f.ptr;
    call = fptr;
    return *this;
  }
  /// Callback setup function for Callbacks with member functions with explicit return type taking no arguments
  template <typename R, typename T> const Call& make(R (T::*pmf)()) {
    typedef R (T::*pfunc_t)();
    struct _Wrapper : public Wrapper<pfunc_t> {
      static MethodResponse call(void* o, const void* f, const MethodCall&) {
	return MethodResponse::make((cast<T>(o)->*(typename Wrapper<pfunc_t>::Functor(f).pmf))());
      }
    };
    return _make(_Wrapper::call, pmf);
  }
  /// Callback setup function for Callbacks with const member functions with explicit return type taking no arguments
  template <typename R, typename T> const Call& make(R (T::*pmf)() const) {
    typedef R (T::*pfunc_t)() const;
    struct _Wrapper : public Wrapper<pfunc_t> {
      static MethodResponse call(void* o, const void* f, const MethodCall&) {
	return MethodResponse::make((cast<T>(o)->*(typename Wrapper<pfunc_t>::Functor(f).pmf))());
      }
    };
    return _make(_Wrapper::call, pmf);
  }
  /// Callback setup function for Callbacks with void member functions taking no arguments
  template <typename T> const Call& make(void (T::*pmf)()) {
    typedef void (T::*pfunc_t)() const;
    struct _Wrapper : public Wrapper<pfunc_t> {
      static MethodResponse call(void* o, const void* f, const MethodCall&) {
	(cast<T>(o)->*(typename Wrapper<pfunc_t>::Functor(f).pmf))();
	return MethodResponse::make();
      }
    };
    return _make(_Wrapper::call, pmf);
  }
  /// Callback setup function for Callbacks with const void member functions taking no arguments
  template <typename T> const Call& make(void (T::*pmf)() const) {
    typedef void (T::*pfunc_t)() const;
    struct _Wrapper : public Wrapper<pfunc_t> {
      static MethodResponse call(void* o, const void* f, const MethodCall&) {
	(cast<T>(o)->*(typename Wrapper<pfunc_t>::Functor(f).pmf))();
	return MethodResponse::make();
      }
    };
    return _make(_Wrapper::call, pmf);
  }

  /** Callbacks with 1 argument  */
  /// Callback setup function for Callbacks with member functions with explicit return type taking 1 argument
  template <typename R, typename T, typename A0> const Call& make(R (T::*pmf)(A0)) {
    typedef R (T::*pfunc_t)(A0);
    struct _Wrapper : public Wrapper<pfunc_t> {
      static MethodResponse call(void* o, const void* f, const MethodCall& c) {
	std::vector<xml_h> args = c.arguments();
	typename ArgType<A0>::type a0 = Arg(args[0]).to<typename ArgType<A0>::type>();
	return MethodResponse::make((cast<T>(o)->*(typename Wrapper<pfunc_t>::Functor(f).pmf))(a0));
      }
    };
    return _make(_Wrapper::call, pmf);
  }
  /// Callback setup function for Callbacks with const member functions with explicit return type taking 1 argument
  template <typename R, typename T, typename A0> const Call& make(R (T::*pmf)(A0) const) {
    typedef R (T::*pfunc_t)(A0) const;
    struct _Wrapper : public Wrapper<pfunc_t> {
      static MethodResponse call(void* o, const void* f, const MethodCall& c) {
	std::vector<xml_h> args = c.arguments();
	typename ArgType<A0>::type a0 = Arg(args[0]).to<typename ArgType<A0>::type>();
	return MethodResponse::make((cast<T>(o)->*(typename Wrapper<pfunc_t>::Functor(f).pmf))(a0));
      }
    };
    return _make(_Wrapper::call, pmf);
  }
  /// Callback setup function for Callbacks with void member functions taking 1 argument
  template <typename T, typename A0> const Call& make(void (T::*pmf)(A0)) {
    typedef void (T::*pfunc_t)(const A0);
    struct _Wrapper : public Wrapper<pfunc_t> {
      static MethodResponse call(void* o, const void* f, const MethodCall& c) {
	std::vector<xml_h> args = c.arguments();
	typename ArgType<A0>::type a0 = Arg(args[0]).to<typename ArgType<A0>::type>();
	(cast<T>(o)->*(typename Wrapper<pfunc_t>::Functor(f).pmf))(a0);
	return MethodResponse::make();
      }
    };
    return _make(_Wrapper::call, pmf);
  }
  /// Callback setup function for Callbacks with const void member functions taking 1 argument
  template <typename T, typename A0> const Call& make(void (T::*pmf)(A0) const) {
    typedef void (T::*pfunc_t)(const A0) const;
    struct _Wrapper : public Wrapper<pfunc_t> {
      static MethodResponse call(void* o, const void* f, const MethodCall& c) {
	std::vector<xml_h> args = c.arguments();
	typename ArgType<A0>::type a0 = Arg(args[0]).to<typename ArgType<A0>::type>();
	(cast<T>(o)->*(typename Wrapper<pfunc_t>::Functor(f).pmf))(a0);
	return MethodResponse::make();
      }
    };
    return _make(_Wrapper::call, pmf);
  }

  /** Callback with 2 arguments     */

  /// Callback setup function for Callbacks with member functions with explicit return type taking 2 arguments
  template <typename R, typename T, typename A0, typename A1> const Call& make(R (T::*pmf)(A0, A1)) {
    typedef R (T::*pfunc_t)(A0, A1);
    typedef Wrapper<pfunc_t> _W;
    struct _Wrapper : public _W {
      static MethodResponse call(void* o, const void* f, const MethodCall& c) {
	std::vector<xml_h> args = c.arguments();
	typename ArgType<A0>::type a0 = Arg(args[0]).to<typename ArgType<A0>::type>();
	typename ArgType<A1>::type a1 = Arg(args[1]).to<typename ArgType<A1>::type>();
	return MethodResponse::make((cast<T>(o)->*(typename _W::Functor(f).pmf))(a0, a1));
      }
    };
    return _make(_Wrapper::call, pmf);
  }
  /// Callback setup function for Callbacks with const member functions with explicit return type taking 2 arguments
  template <typename R, typename T, typename A0, typename A1> const Call& make(R (T::*pmf)(A0, A1) const) {
    typedef R (T::*pfunc_t)(A0, A1);
    typedef Wrapper<pfunc_t> _W;
    struct _Wrapper : public _W {
      static MethodResponse call(void* o, const void* f, const MethodCall& c) {
	std::vector<xml_h> args = c.arguments();
	typename ArgType<A0>::type a0 = Arg(args[0]).to<typename ArgType<A0>::type>();
	typename ArgType<A1>::type a1 = Arg(args[1]).to<typename ArgType<A1>::type>();
	return MethodResponse::make((cast<T>(o)->*(typename _W::Functor(f).pmf))(a0, a1));
      }
    };
    return _make(_Wrapper::call, pmf);
  }
  /// Callback setup function for Callbacks with const void member functions taking 2 arguments
  template <typename T, typename A0, typename A1> const Call& make(void (T::*pmf)(A0, A1)) {
    typedef void (T::*pfunc_t)(A0, A1);
    typedef Wrapper<pfunc_t> _W;
    struct _Wrapper : public _W {
      static MethodResponse call(void* o, const void* f, const MethodCall& c) {
	std::vector<xml_h> args = c.arguments();
	typename ArgType<A0>::type a0 = Arg(args[0]).to<typename ArgType<A0>::type>();
	typename ArgType<A1>::type a1 = Arg(args[1]).to<typename ArgType<A1>::type>();
	(cast<T>(o)->*(typename _W::Functor(f).pmf))(a0, a1);
	return MethodResponse::make();
      }
    };
    return _make(_Wrapper::call, pmf);
  }
  /// Callback setup function for Callbacks with const void member functions taking 2 arguments
  template <typename T, typename A0, typename A1> const Call& make(void (T::*pmf)(A0, A1) const) {
    typedef void (T::*pfunc_t)(A0, A1);
    typedef Wrapper<pfunc_t> _W;
    struct _Wrapper : public _W {
      static MethodResponse call(void* o, const void* f, const MethodCall& c) {
	std::vector<xml_h> args = c.arguments();
	typename ArgType<A0>::type a0 = Arg(args[0]).to<typename ArgType<A0>::type>();
	typename ArgType<A1>::type a1 = Arg(args[1]).to<typename ArgType<A1>::type>();
	(cast<T>(o)->*(typename _W::Functor(f).pmf))(a0, a1);
	return MethodResponse::make();
      }
    };
    return _make(_Wrapper::call, pmf);
  }

  /** Callback setup for callbacks with 3 arguments  */

  /// Callback setup function for Callbacks with member functions with explicit return type taking 3 arguments
  template <typename R, typename T, typename A0, typename A1, typename A2> const Call& make(R (T::*pmf)(A0, A1, A2)) {
    typedef R (T::*pfunc_t)(A0, A1, A2);
    typedef Wrapper<pfunc_t> _W;
    struct _Wrapper : public _W {
      static MethodResponse call(void* o, const void* f, const MethodCall& c) {
	std::vector<xml_h> args = c.arguments();
	typename ArgType<A0>::type a0 = Arg(args[0]).to<typename ArgType<A0>::type>();
	typename ArgType<A1>::type a1 = Arg(args[1]).to<typename ArgType<A1>::type>();
	typename ArgType<A2>::type a2 = Arg(args[2]).to<typename ArgType<A2>::type>();
	return MethodResponse::make((cast<T>(o)->*(typename _W::Functor(f).pmf))(a0, a1, a2));
      }
    };
    return _make(_Wrapper::call, pmf);
  }
  /// Callback setup function for Callbacks with const member functions with explicit return type taking 3 arguments
  template <typename R, typename T, typename A0, typename A1, typename A2> const Call& make(R (T::*pmf)(A0, A1, A2) const) {
    typedef R (T::*pfunc_t)(A0, A1, A2);
    typedef Wrapper<pfunc_t> _W;
    struct _Wrapper : public _W {
      static MethodResponse call(void* o, const void* f, const MethodCall& c) {
	std::vector<xml_h> args = c.arguments();
	typename ArgType<A0>::type a0 = Arg(args[0]).to<typename ArgType<A0>::type>();
	typename ArgType<A1>::type a1 = Arg(args[1]).to<typename ArgType<A1>::type>();
	typename ArgType<A2>::type a2 = Arg(args[2]).to<typename ArgType<A2>::type>();
	return MethodResponse::make((cast<T>(o)->*(typename _W::Functor(f).pmf))(a0, a1, a2));
      }
    };
    return _make(_Wrapper::call, pmf);
  }
  /// Callback setup function for Callbacks with const void member functions taking 3 arguments
  template <typename T, typename A0, typename A1, typename A2> const Call& make(void (T::*pmf)(A0, A1, A2)) {
    typedef void (T::*pfunc_t)(A0, A1, A2);
    typedef Wrapper<pfunc_t> _W;
    struct _Wrapper : public _W {
      static MethodResponse call(void* o, const void* f, const MethodCall& c) {
	std::vector<xml_h> args = c.arguments();
	typename ArgType<A0>::type a0 = Arg(args[0]).to<typename ArgType<A0>::type>();
	typename ArgType<A1>::type a1 = Arg(args[1]).to<typename ArgType<A1>::type>();
	typename ArgType<A2>::type a2 = Arg(args[2]).to<typename ArgType<A2>::type>();
	(cast<T>(o)->*(typename _W::Functor(f).pmf))(a0, a1, a2);
	return MethodResponse::make();
      }
    };
    return _make(_Wrapper::call, pmf);
  }
  /// Callback setup function for Callbacks with const void member functions taking 3 arguments
  template <typename T, typename A0, typename A1, typename A2> const Call& make(void (T::*pmf)(A0, A1, A2) const) {
    typedef void (T::*pfunc_t)(A0, A1, A2);
    typedef Wrapper<pfunc_t> _W;
    struct _Wrapper : public _W {
      static MethodResponse call(void* o, const void* f, const MethodCall& c) {
	std::vector<xml_h> args = c.arguments();
	typename ArgType<A0>::type a0 = Arg(args[0]).to<typename ArgType<A0>::type>();
	typename ArgType<A1>::type a1 = Arg(args[1]).to<typename ArgType<A1>::type>();
	typename ArgType<A2>::type a2 = Arg(args[2]).to<typename ArgType<A2>::type>();
	(cast<T>(o)->*(typename _W::Functor(f).pmf))(a0, a1, a2);
	return MethodResponse::make();
      }
    };
    return _make(_Wrapper::call, pmf);
  }

  template <typename T> static Call make(void* p, T pmf) {
    return Call(p).make(pmf);
  }

  template <typename P, typename R, typename T> static T* dyn_cast(P* p, R (T::*)()) {
    return dynamic_cast<T*>(p);
  }
  template <typename P, typename R, typename T> static const T* dyn_cast(const P* p, R (T::*)() const) {
    return dynamic_cast<const T*>(p);
  }

  template <typename P, typename R, typename T, typename A> static T* dyn_cast(P* p, R (T::*)(A)) {
    return dynamic_cast<T*>(p);
  }
  template <typename P, typename R, typename T, typename A> static const T* dyn_cast(const P* p, R (T::*)(A) const) {
    return dynamic_cast<const T*>(p);
  }
};


/// Definition of an actor on sequences of callbacks
/**
 *  A callback sequence is a set of callbacks with identical signature, so that
 *  the can be called consecutively using the same arguments.
 *
 *  \author  M.Frank
 *  \date    01/03/2013
 *  \version 0.1
 *  \ingroup DD4HEP
 */
struct CallSequence {
  typedef std::vector<Call> Calls;
  enum Location { FRONT, END };
  Calls callbacks;
  /// Default constructor
  CallSequence() {
  }
  /// Copy constructor
  CallSequence(const CallSequence& c);

  /// Assignment operator
  CallSequence& operator=(const CallSequence& c)  {
    if ( this != & c ) callbacks = c.callbacks;
    return *this;
  }

  //template <typename TYPE, typename R, typename OBJECT>
  //  CallSequence(const std::vector<TYPE*>& objects, R (TYPE::value_type::*pmf)())  {
  //}
    bool empty() const {
      return callbacks.empty();
    }
  /// Clear the sequence and remove all callbacks
  void clear() {
    callbacks.clear();
  }
  /// Generically Add a new callback to the sequence depending on the location arguments
  void add(const Call& cb,Location where) {
    if ( where == CallSequence::FRONT )
      callbacks.insert(callbacks.begin(),cb);
    else
      callbacks.insert(callbacks.end(),cb);
  }
  /// Execution overload for callbacks with no arguments
  void operator()(const MethodCall& c) const;
  /// Check the compatibility of two typed objects. The test is the result of a dynamic_cast
  static void checkTypes(const std::type_info& typ1, const std::type_info& typ2, void* test);

  /** Callback setup for callbacks with no arguments  */
  /// Add a new callback to a member function with explicit return type and no arguments
  template <typename TYPE, typename R, typename OBJECT>
    void add(TYPE* pointer, R (OBJECT::*pmf)(),Location where=CallSequence::END) {
    checkTypes(typeid(TYPE), typeid(OBJECT), dynamic_cast<OBJECT*>(pointer));
    add(Call(pointer).make(pmf),where);
  }
  /// Add a new callback to a const member function with explicit return type and no arguments
  template <typename TYPE, typename R, typename OBJECT>
    void add(TYPE* pointer, R (OBJECT::*pmf)() const,Location where=CallSequence::END) {
    checkTypes(typeid(TYPE), typeid(OBJECT), dynamic_cast<OBJECT*>(pointer));
    add(Call(pointer).make(pmf),where);
  }
  /// Add a new callback to a void member function with no arguments
  template <typename TYPE, typename OBJECT>
    void add(TYPE* pointer, void (OBJECT::*pmf)(),Location where=CallSequence::END) {
    checkTypes(typeid(TYPE), typeid(OBJECT), dynamic_cast<OBJECT*>(pointer));
    add(Call(pointer).make(pmf),where);
  }
  /// Add a new callback to a const void member function and no arguments
  template <typename TYPE, typename OBJECT>
    void add(TYPE* pointer, void (OBJECT::*pmf)() const,Location where=CallSequence::END) {
    checkTypes(typeid(TYPE), typeid(OBJECT), dynamic_cast<OBJECT*>(pointer));
    add(Call(pointer).make(pmf),where);
  }

  /** Callback setup for callbacks with 1 argument  */
  /// Add a new callback to a member function with explicit return type and 1 argument
  template <typename TYPE, typename R, typename OBJECT, typename A>
    void add(TYPE* pointer, R (OBJECT::*pmf)(A),Location where=CallSequence::END) {
    checkTypes(typeid(TYPE), typeid(OBJECT), dynamic_cast<OBJECT*>(pointer));
    add(Call(pointer).make(pmf),where);
  }
  /// Add a new callback to a void member function and 1 argument
  template <typename TYPE, typename OBJECT, typename A>
    void add(TYPE* pointer, void (OBJECT::*pmf)(A),Location where=CallSequence::END) {
    checkTypes(typeid(TYPE), typeid(OBJECT), dynamic_cast<OBJECT*>(pointer));
    add(Call(pointer).make(pmf),where);
  }
  /// Add a new callback to a const member function with explicit return type and 1 argument
  template <typename TYPE, typename R, typename OBJECT, typename A>
    void add(TYPE* pointer, R (OBJECT::*pmf)(A) const,Location where=CallSequence::END) {
    checkTypes(typeid(TYPE), typeid(OBJECT), dynamic_cast<OBJECT*>(pointer));
    add(Call(pointer).make(pmf),where);
  }
  /// Add a new callback to a const void member function and 1 argument
  template <typename TYPE, typename OBJECT, typename A>
    void add(TYPE* pointer, void (OBJECT::*pmf)(A) const,Location where=CallSequence::END) {
    checkTypes(typeid(TYPE), typeid(OBJECT), dynamic_cast<OBJECT*>(pointer));
    add(Call(pointer).make(pmf),where);
  }


  /** Callback setup for callbacks with 2 arguments  */
  /// Add a new callback to a member function with explicit return type and 2 arguments
  template <typename TYPE, typename R, typename OBJECT, typename A1, typename A2>
    void add(TYPE* pointer, R (OBJECT::*pmf)(A1, A2),Location where=CallSequence::END) {
    checkTypes(typeid(TYPE), typeid(OBJECT), dynamic_cast<OBJECT*>(pointer));
    add(Call(pointer).make(pmf),where);
  }
  /// Add a new callback to a const member function with explicit return type and 2 arguments
  template <typename TYPE, typename R, typename OBJECT, typename A1, typename A2>
    void add(TYPE* pointer, R (OBJECT::*pmf)(A1, A2) const,Location where=CallSequence::END) {
    checkTypes(typeid(TYPE), typeid(OBJECT), dynamic_cast<OBJECT*>(pointer));
    add(Call(pointer).make(pmf),where);
  }
  /// Add a new callback to a void member function with 2 arguments
  template <typename TYPE, typename OBJECT, typename A1, typename A2>
    void add(TYPE* pointer, void (OBJECT::*pmf)(A1, A2),Location where=CallSequence::END) {
    checkTypes(typeid(TYPE), typeid(OBJECT), dynamic_cast<OBJECT*>(pointer));
    add(Call(pointer).make(pmf),where);
  }
  /// Add a new callback to a const void member function with 2 arguments
  template <typename TYPE, typename OBJECT, typename A1, typename A2>
    void add(TYPE* pointer, void (OBJECT::*pmf)(A1, A2) const,Location where=CallSequence::END) {
    checkTypes(typeid(TYPE), typeid(OBJECT), dynamic_cast<OBJECT*>(pointer));
    add(Call(pointer).make(pmf),where);
  }
};

using ::dd4hep::xml::dump_tree;
using ::dd4hep::xml::_toString;

template <typename T> inline void container_insert(std::vector<T>& c, T& e) {  c.emplace_back(std::move(e)); }
template <typename T> inline void container_insert(std::list<T>& c, T& e)   {  c.emplace_back(std::move(e)); }
template <typename T> inline void container_insert(std::set<T>& c, T& e)    {  c.emplace(std::move(e));    }

template <typename T> inline T container_from_xmlrpc(xml_h element)  {
  T val;
  xml_h arr = element.child(_rpcU(array)).child(_rpcU(data));
  for(xml_coll_t c(arr,_rpcU(value)); c; ++c)   {
    typename T::value_type o = XmlCoder(c).to<typename T::value_type>();
    container_insert(val, o);
  }
  return val;
}
template <typename T> inline void container_to_xmlrpc(xml_h element, const T& elts)  {
  Array a(element);
  for(const auto& e : elts )  {
    xml_h x = a.add(Structure());
    XmlCoder(x).from(e);
  }
}
template <typename K, typename T> inline 
void map_insert(std::map<K,T>& c, K& k, T& e) { c.emplace(std::move(k),std::move(e)); }

template <typename T> inline T string_map_from_xmlrpc(xml_h element)  {
  T val;
  xml_h arr = element.child(_rpcU(value)).child(_rpcU(struct));
  for(xml_coll_t c(arr,_rpcU(member)); c; ++c)   {
    xml_h nam_h = c.child(_rpcU(name));
    xml_h val_h = c.child(_rpcU(value)).child(_rpcU(struct));
    typename T::key_type    k = nam_h.text();
    typename T::mapped_type o = XmlCoder(val_h).to<typename T::mapped_type>();
    map_insert(val, k, o);
  }
  return val;
}
template <typename T> inline xml_h string_map_to_xmlrpc(xml_h element, const T& elts)  {
  Structure s(element);
  for(const auto& e : elts )  {
    s.add("name", e.first);
    xml_h tsk = s.add("task", Structure());
    XmlCoder(tsk).from(e.second);
  }
  return s.root;
}
XMLRPC_NS_END

#define XMLRPC_IMPLEMENT_OBJECT_MARSCHALLING(typ,clazz)			\
  XMLRPC_NS_BEGIN							\
  /* Add a new parameter to the response		*/		\
  template <>								\
  MethodResponse MethodResponse::make<clazz>(const clazz& b)   {	\
    MethodResponse r = makeValue(_rpcU(typ));				\
    XmlCoder(r.value).from(b);						\
    return r;								\
  }									\
									\
  /* Add a new parameter to the response		*/		\
  template <> clazz MethodResponse::data<clazz>()  const  {		\
    return XmlCoder(value.parent()).to<clazz>();			\
  }									\
									\
  /* Add a new parameter to the method call		*/		\
  template <> const MethodCall&						\
  MethodCall::addParam<clazz>(const clazz& b) const  {			\
    xml_elt_t struc = createParam(_rpcU(typ));				\
    XmlCoder(struc).from(b);						\
    return *this;							\
  }									\
  XMLRPC_NS_END

#define XMLRPC_IMPLEMENT_CONTAINER_CONVERSION(container,type)    XMLRPC_NS_BEGIN \
  template <> const XmlCoder& XmlCoder::from(const container<type>& elts) const { container_to_xmlrpc(element,elts); return *this; } \
  template <> container<type> XmlCoder::to() const { return container_from_xmlrpc<container<type> >(element); } \
  XMLRPC_NS_END

#define XMLRPC_IMPLEMENT_STRINGMAP_CONVERSION(container,key,type)    XMLRPC_NS_BEGIN \
  template <> const XmlCoder& XmlCoder::from(const container<key,type>& elts) const { string_map_to_xmlrpc(element,elts); return *this; } \
  template <> container<key,type> XmlCoder::to() const { return string_map_from_xmlrpc<container<key,type> >(element); } \
  XMLRPC_NS_END

#endif /* ONLINE_DDCORE_XML_XMLRPC_H  */
