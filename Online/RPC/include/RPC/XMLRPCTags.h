//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_XML_XMLRPCTAGS_H
#define ONLINE_XML_XMLRPCTAGS_H

// Framework include files
#include <XML/XMLElements.h>

#ifndef UNICODE 
#define UNICODE(x)  extern const ::dd4hep::xml::Tag_t Unicode_##x 
#endif

/// Namespace for the AIDA detector description toolkit
namespace dd4hep {

  /// Namespace for the AIDA detector description toolkit supporting XML utilities 
  namespace xml {
    
    /// Namespace of conditions unicode tags
    namespace cxxrpc  {

      UNICODE(methodCall);
      UNICODE(methodName);
      UNICODE(methodResponse);
      UNICODE(param);
      UNICODE(params);
      UNICODE(value);
      UNICODE(fault);
      UNICODE(name);
      UNICODE(data);

      UNICODE(i4);
      UNICODE(nil);
      UNICODE(int);
      UNICODE(boolean);
      UNICODE(string);
      UNICODE(double);
      UNICODE(base64);
      UNICODE(array);
      UNICODE(struct);
      UNICODE(member);

      UNICODE(redirection);
      UNICODE(server);
      UNICODE(port);
      UNICODE(url);
      UNICODE(to_url);
      UNICODE(to);

      extern const Tag_t Unicode_dateTime;
    }
  }
}
#undef UNICODE // Do not miss this one!

#define _rpcU(x) ::dd4hep::xml::cxxrpc::Unicode_##x

#endif /* ONLINE_XML_XMLRPCTAGS_H  */
