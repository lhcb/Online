//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include "RPC/RpcClientHandle.h"
#include "RPC/ComponentClient.h"
#include "RPC/XMLRPC.h"

using namespace std;
using namespace rpc;

/// Access the hosted client services
ComponentClient::Clients ComponentClient::clients()  const   {
  xmlrpc::traits::Call c;
  c.setMethod("clients");
  auto r = this->call(c);
  Clients ret = r.data<Clients>();
  return ret;
}

/// Access all properties of an object
ComponentClient::Properties ComponentClient::allProperties()  const   {
  xmlrpc::traits::Call c;
  c.setMethod("allProperties");
  auto r = this->call(c);
  Properties ret = r.data<Properties>();
  return ret;
}

/// Access all properties with the same name from all clients
ComponentClient::Properties ComponentClient::namedProperties(const std::string& name)  const   {
  xmlrpc::traits::Call c;
  c.setMethod("namedProperties").addParam(name);
  auto r = this->call(c);
  Properties ret = r.data<Properties>();
  return ret;
}

/// Access all properties of one remote client (service, ect.)
ComponentClient::Properties ComponentClient::clientProperties(const std::string& client)  const   {
  xmlrpc::traits::Call c;
  c.setMethod("clientProperties").addParam(client);
  auto r = this->call(c);
  Properties ret = r.data<Properties>();
  return ret;
}

/// Access a single property of an object
ObjectProperty ComponentClient::property(const std::string& client,
					 const std::string& name)  const   {
  xmlrpc::traits::Call c;
  c.setMethod("property").addParam(client).addParam(name);
  auto r = this->call(c);
  ObjectProperty ret = r.data<ObjectProperty>();
  return ret;
}

/// Modify a property
void ComponentClient::setProperty(const std::string& client,
				  const std::string& name,
				  const std::string& value)  const   {
  xmlrpc::traits::Call c;
  c.setMethod("setProperty").addParam(client).addParam(name).addParam(value);
  auto r = this->call(c);
}

/// Modify a property
void ComponentClient::setPropertyObject(const ObjectProperty& property) const   {
  xmlrpc::traits::Call c;
  c.setMethod("setPropertyObject").addParam(property);
  auto r = this->call(c);
}

/// Modify a whole bunch of properties. Call returns the number of changes
int  ComponentClient::setProperties(const Properties& props)  const   {
  xmlrpc::traits::Call c;
  c.setMethod("setProperties").addParam(props);
  auto r = this->call(c);
  return r.data<int>();
}

/// Execute an interpreter request to the ROOT istance (if present)
int ComponentClient::interpreteCommand(const std::string& command)  const   {
  xmlrpc::traits::Call c;
  c.setMethod("interprete").addParam(command);
  auto r = this->call(c);
  int ret = r.data<int>();
  return ret;
}

/// Execute an interpreter request to the ROOT istance (if present)
int ComponentClient::interpreteCommands(const std::vector<std::string>& commands)  const   {
  xmlrpc::traits::Call c;
  c.setMethod("interprete").addParam(commands);
  auto r = this->call(c);
  int ret = r.data<int>();
  return ret;
}

/// Exit the remote client
int ComponentClient::forceExit()  const   {
  xmlrpc::traits::Call c;
  c.setMethod("forceExit");
  auto r = this->call(c);
  int ret = r.data<int>();
  return ret;
}
