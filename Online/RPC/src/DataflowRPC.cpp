//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include <RPC/XMLRPC.h>
#include "RPC/DataflowRPC.h"
#include "XML/Printout.h"

using namespace std;
using namespace xmlrpc;
using namespace xmlrpc::dataflow;

#define __START_CALL__ ::printf("Calling %s\n",__PRETTY_FUNCTION__)
#define __END_CALL__ ::printf(  "Called  %s\n",__PRETTY_FUNCTION__)

/// Access all known DNS domains
set<string> DomainInfo::domains() const   {
  __START_CALL__;
  MethodResponse r = this->call(MethodCall().setMethod("domains"));
  set<string>  res = r.data<set<string> >();
  __END_CALL__;
  return res;
}

/// Access all nodes known to a DNS domain
set<string> DomainInfo::nodes(const string& dns) const   {
  __START_CALL__;
  MethodResponse r = this->call(MethodCall().setMethod("nodes").addParam(dns));
  set<string>  res = r.data<set<string> >();
  __END_CALL__;
  return res;
}

/// Access all nodes known to a DNS domain matching the regular expression
set<string> DomainInfo::nodesByRegex(const string& dns, const string& regex) const   {
  __START_CALL__;
  MethodResponse r = this->call(MethodCall().setMethod("nodesByRegex").addParam(dns).addParam(regex));
  set<string>  res = r.data<set<string> >();
  __END_CALL__;
  return res;
}

/// Access all tasks known to a DNS domain matching the regular expression
set<string> DomainInfo::tasksByRegex(const string& dns, const string& regex) const   {
  __START_CALL__;
  MethodResponse r = this->call(MethodCall().setMethod("tasksByRegex").addParam(dns).addParam(regex));
  set<string>  res = r.data<set<string> >();
  __END_CALL__;
  return res;
}

/// Access all tasks known to a DNS domain matching the correct node
set<string> DomainInfo::tasks(const string& dns, const string& node) const   {
  __START_CALL__;
  MethodResponse r = this->call(MethodCall().setMethod("tasks").addParam(dns).addParam(node));
  set<string>  res = r.data<set<string> >();
  __END_CALL__;
  return res;
}


/// Access the hosted client services
PropertyContainer::Clients PropertyContainer::clients()  const   {
  __START_CALL__;
  MethodResponse r = this->call(MethodCall().setMethod("clients"));
  Clients      res = r.data<Clients>();
  __END_CALL__;
  return res;
}

/// Access all properties of an object
PropertyContainer::Properties PropertyContainer::allProperties()  const   {
  __START_CALL__;
  MethodResponse r = this->call(MethodCall().setMethod("allProperties"));
  Properties   res = r.data<Properties>();
  __END_CALL__;
  return res;
}

/// Access all properties with the same name from all clients
PropertyContainer::Properties PropertyContainer::namedProperties(const string& name)  const   {
  __START_CALL__;
  auto r = this->call(MethodCall().setMethod("namedProperties").addParam(name));
  auto res = r.data<Properties>();
  __END_CALL__;
  return res;
}

/// Access all properties of one remote client (service, ect.)
PropertyContainer::Properties PropertyContainer::clientProperties(const string& client)  const   {
  __START_CALL__;
  auto r = this->call(MethodCall().setMethod("clientProperties").addParam(client));
  auto res = r.data<Properties>();
  __END_CALL__;
  return res;
}

/// Access a single property of an object
rpc::ObjectProperty PropertyContainer::property(const string& client,
						const string& name)  const   {
  __START_CALL__;
  auto r   = this->call(MethodCall().setMethod("property").addParam(client).addParam(name));
  auto res = r.data<rpc::ObjectProperty>();
  __END_CALL__;
  return res;
}

/// Modify a property
void PropertyContainer::setProperty(const string& client,
				    const string& name,
				    const string& value)  const   {
  __START_CALL__;
  MethodCall c;
  c.setMethod("setProperty").addParam(client).addParam(name).addParam(value);
  /* MethodResponse r   = */ this->call(c);
  __END_CALL__;
}

/// Modify a property
void PropertyContainer::setPropertyObject(const rpc::ObjectProperty& property) const  {
  setProperty(property.client,property.name,property.value);
}

/// Modify a whole bunch of properties. Call returns the number of changes
int  PropertyContainer::setProperties(const Properties& properties)  const   {
  __START_CALL__;
  MethodCall c;
  c.setMethod("setProperties").addParam(properties);
  MethodResponse r = this->call(c);
  int res = r.data<int>();
  __END_CALL__;
  return res;
}

/// Execute an interpreter request to the ROOT istance (if present)
int CommandInterpreter::interpreteCommand(const std::string& command)  const   {
  __START_CALL__;
  MethodCall c;
  c.setMethod("interpreteCommand").addParam(command);
  MethodResponse r   = this->call(c);
  int res = r.data<int>();
  __END_CALL__;
  return res;
}

/// Execute an interpreter request to the ROOT istance (if present)
int CommandInterpreter::interpreteCommands(const std::vector<std::string>& commands)  const   {
  __START_CALL__;
  MethodCall c;
  c.setMethod("interpreteCommands").addParam(commands);
  MethodResponse r   = this->call(c);
  int res = r.data<int>();
  __END_CALL__;
  return res;
}

