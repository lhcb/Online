//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================
#define RPC_HAVE_COMPRESSION

// Framework inclde files
#include "HTTP/Asio.h"
#include "HTTP/HttpServer.h"
#include "RPC/HttpRpcBridge.h"
#include "RTL/Compress.h"

// C/C++ include files
#include <map>

/// Namespace for the HTTP implementation(s)
namespace rpc  {

  /// Forward declarations
  class HttpRpcHandler;

  class HttpRpcBridge::Handler : public http::HttpRequestHandler  {
  public:
    /// Standard constructor
    using http::HttpRequestHandler::HttpRequestHandler;

    /// Handle a request and produce a reply.
    virtual continue_action handle_request(const http::Request& req, http::Reply& rep) override;
    /// Handle a request and produce a reply.
    virtual void handle_bridge_request(const http::Request& req, http::Reply& rep);

  public:
    /// Bind default callback to all successfully handled functions.
    std::vector<Callback>             onHandled;
    /// Bind default callback to all unhandled functions.
    std::vector<Callback>             onUnhandled;
    /// Bind default callback to processing errors.
    std::vector<Callback>             onError;

    std::string                         mount;
    /// Handler for bridge requests
    std::map<std::string, HttpRpcHandler*> httpHandlers;
    /// Redirection map for certain server requests
    using ServerRedirection = std::pair<std::string, std::string>;
    using ServerRedirections = std::map<std::string, ServerRedirection>;
    ServerRedirections server_redirection;

    struct UrlRedirection  {
      std::string server, port, url;
      ~UrlRedirection() = default;
      UrlRedirection(const std::string& s, const std::string& p, const std::string& u)
	: server(s), port(p), url(u) {}
      UrlRedirection() = default;
      UrlRedirection(UrlRedirection&&) = default;
      UrlRedirection(const UrlRedirection&) = delete;
      UrlRedirection& operator=(UrlRedirection&&) = default;
      UrlRedirection& operator=(const UrlRedirection&) = delete;
    };
    using UrlRedirections = std::map<std::string, UrlRedirection>;
    UrlRedirections url_redirection;
    /// Enable debugging if necessary
    int                               debug = 0;
  };
}  /* End of namespace xmlrpc       */

#include "XML/XML.h"
#include "XML/Printout.h"
#include "RPC/HttpRpcHandler.h"
using namespace dd4hep;

// C/C++ include files
#include <stdexcept>

using namespace std;
using namespace http;
using namespace boost;
typedef std::lock_guard<std::mutex>  Lock;

/// Handle a request and produce a reply.
http::HttpRequestHandler::continue_action
rpc::HttpRpcBridge::Handler::handle_request(const http::Request& req, http::Reply& rep)   {
  bool ok  = false;
  int  dbg = debug;
  const void* args[] = { &req, 0 };
  try  {
    if ( req.content_length < req.content.size() )  {
      return read;
    }
    rep = http::Reply::stock_reply(http::Reply::ok);
    handle_bridge_request(req, rep);
    ok = true;
  }
  catch(const std::exception& e)    {
    rep = http::Reply::stock_reply(http::Reply::bad_request);
    rep.content = to_vector("Processing error:"+string(e.what()));
    // ERROR: call user callbacks
    for (const auto& c : onError )
      c.execute(args);
    dbg = 1;
    cout << "HttpRpcBridge: Processing error: " << e.what() << endl;
  }
  catch(...)    {
    rep = http::Reply::stock_reply(http::Reply::bad_request);
    rep.content = to_vector("Unknown exception during server processing.");
    // ERROR: call user callbacks
    for (const auto& c : onError )
      c.execute(args);
    dbg = 1;
    cout << "HttpRpcBridge: UNKNOWN Processing error. " << endl;
  }
  if ( !ok )   {
    time_t now = ::time(0);
    char   nowstr[64];
    string tim = ::ctime_r(&now,nowstr);
    tim = tim.substr(0,tim.find('\n'));
    ///
    ///  Build the headers, which must be of the form:
    ///    HTTP/1.1 200 OK
    ///    Connection: close
    ///    Content-Length: 426
    ///    Content-Type: text/xml
    ///    Date: Fri, 17 Jul 1998 19:55:02 GMT
    ///    Server: UserLand Frontier/5.1.2-WinNT
    rep.headers.resize(5);
    rep.headers[0].name  = "Content-Type";
    rep.headers[0].value = "text/html";
    rep.headers[1].name  = "Content-Length";
    rep.headers[1].value = dd4hep::xml::_toString(rep.content.size());
    rep.headers[2].name  = "Connection";
    rep.headers[2].value = "close";
    rep.headers[3].name  = "Date";
    rep.headers[3].value = tim;
    rep.headers[4].name  = "Server";
    rep.headers[4].value = "LHCbDataflow Http";
  }
  
  if ( dbg > 0 || debug > 0 )   {
    cout << "+++++ HTTP Request:" << req.method
         << " vsn:" << req.version_major << "." << req.version_minor
	 << " uri:" << req.uri << endl;
    if ( debug > 1 )   {
      cout << "     Headers:" << endl;
      for(const auto& h : req.headers )  
	cout << h.name << " : " << h.value << endl;
    }
    if ( debug > 2 )   {
      cout << "        Data:" << (char*)&req.content[0] << endl;
    }
    if ( debug > 1 )   {
      cout << "*****" << endl;
    }
    cout << "+++++ HTTP Reply:  len:" << rep.content.size() << endl;
    if ( debug > 1 )   {
      for(const auto& h : rep.headers )  
	cout << h.name << " : " << h.value << endl;
    }
    if ( debug > 2 ) cout << "        Data:" << (char*)&rep.content[0] << "|data-end|" << endl;
  }
  return write;
}

/// Initializing constructor
rpc::HttpRpcBridge::HttpRpcBridge(const string& h, int p)
  : implementation(new Handler()), host(h)
{
  stringstream str;
  str << p;
  port = str.str();
}

/// Default destructor
rpc::HttpRpcBridge::~HttpRpcBridge() {
  delete implementation;
}

/// Define the mount point
void rpc::HttpRpcBridge::setMount(const string& mnt)  {
  implementation->mount = mnt;
}

/// Modify debug flag
int rpc::HttpRpcBridge::setDebug(int value)  {
  int tmp = implementation->debug;
  implementation->debug = value;
  return tmp;
}

/// Add server redirection request to other node
void rpc::HttpRpcBridge::addServerRedirection(const string& req, const string& server_node, const string& srv_port)  {
  implementation->server_redirection[req] = make_pair(server_node, srv_port);
}

/// Add URL redirection request to other node and new URL
void rpc::HttpRpcBridge::addUrlRedirection(const std::string& url, 
					   const std::string& server,
					   const std::string& srv_port,
					   const std::string& new_url)
{
  implementation->url_redirection[url] = Handler::UrlRedirection(server, srv_port, new_url);
}

/// Start the xmlrpc service
void rpc::HttpRpcBridge::start(bool detached, int num_additional_threads)   {
  implementation->open(host,port);
  if ( detached ) {
    return;
  }
  implementation->run(num_additional_threads);
}

/// Run the server's io_context loop. If specified additional workers may be used
void rpc::HttpRpcBridge::run(int num_additional_threads)   {
  implementation->run(num_additional_threads);
}

/// Stop the xmlrpc service
void rpc::HttpRpcBridge::stop()   {
}

/// Bind default callback to all unhandled functions.
void rpc::HttpRpcBridge::onUnhandled(const Callback& call)   {
  Lock lock(implementation->lock);
  implementation->onUnhandled.push_back(call);
}


/// Bind default callback to all successfully handled function requests.
void rpc::HttpRpcBridge::onHandled(const Callback& call)   {
  Lock lock(implementation->lock);
  implementation->onHandled.push_back(call);
}

/// Bind default callback to processing errors.
void rpc::HttpRpcBridge::onError(const Callback& call)   {
  Lock lock(implementation->lock);
  implementation->onError.push_back(call);
}

#include "HTTP/HttpClient.h"
namespace  {

  struct BridgeHandler : public rpc::HttpRpcHandler   {
  public:
    string host, port;
    int    debug = 0;
    /// Standard constructor
    BridgeHandler(const std::string& srv, 
		  const std::string& port,
		  int dbg = 0);
    /// Standard destructor
    virtual ~BridgeHandler() = default;
    /// Enable debugging
    virtual void setDebug(int val)   override  { this->debug = val; }
    /// Execute RPC call
    virtual continue_action handle_request(const http::Request& req, http::Reply& rep)  override;
  };

  /// Standard constructor
  BridgeHandler::BridgeHandler(const std::string& h, 
			       const std::string& p,
			       int dbg)
    : host(h), port(p), debug(dbg)
  {
    //asio::ip::tcp::resolver::query query(host,port);
    //asio::ip::tcp::resolver::results_type = resolver.resolve(query);
  }

  /// Execute RPC call
  BridgeHandler::continue_action 
  BridgeHandler::handle_request(const http::Request& req, http::Reply& rep)   {
    using namespace chrono;
    stringstream str;
    try  {
      using namespace asio::ip;
      if ( debug>0 )  {
	printout(INFO,"HttpRpcBridge","BridgeHandler: Handling request(%s:%s)",
		 host.c_str(), port.c_str());
      }
      using namespace chrono;
      size_t                         req_len;
      asio::io_context               io;
      system::error_code             ec;
      tcp::socket                    socket(io);
      tcp::resolver                  resolver(io);
      stringstream                   req_str;
      string                         hdr, mount;
      string                         contentType = "text/html";
      string                         acceptedEncoding;

      req_str << "GET " << req.uri  << " HTTP/1.0\r\n"
	  << "User-Agent: "     << "HttpRpcBridge\r\n"
	  << "Host: "           << "HttpRpcBridge-Host\r\n";

      for(const auto& h : req.headers)  {
	if ( h.name == "Content-Type" )
	  contentType = h.value;
	else if ( h.name == "Host" )
	  req_str << h.name << ": " << h.value << "\r\n";
	else if ( h.name == "Accept" )
	  req_str << h.name << ": " << h.value << "\r\n";
	else if ( h.name == "Accept-Language" )
	  req_str << h.name << ": " << h.value << "\r\n";
	else if ( h.name == "Accept-Encoding" )
	  acceptedEncoding = h.value;
	else if ( h.name == "Connection" )
	  req_str << h.name << ": " << h.value << "\r\n";
	else if ( h.name == "Content-Length" )
	  req_str << h.name << ": " << h.value << "\r\n";
	else if ( h.name == "User-Agent" )
	  req_str << h.name << ": " << h.value << "\r\n";
	else if ( h.name == "Content-Encoding" )
	  req_str << h.name << ": " << h.value << "\r\n";
      }
      req_str << "Content-Type: "   << contentType << "\r\n";
      if ( !acceptedEncoding.empty() )   {
	req_str << "Accepted-Encoding: "   << acceptedEncoding << "\r\n";
      }
      else {
#if defined(RPC_HAVE_COMPRESSION)
	//req_str << "Accept-Encoding: " << "gzip" << "\r\n";
#endif
      }
      req_str << "\r\n";
      hdr = req_str.str();
      vector<asio::const_buffer> request = {asio::const_buffer(hdr.c_str(), hdr.length()),
					    asio::const_buffer(req.content.data(), req.content.size())  };
      int contentLength = 0;
      string contentEncoding = "";
      req_len = asio::detail::buffer_size(request[0]) + asio::detail::buffer_size(request[1]);
#if BOOST_ASIO_VERSION < 103400 
      asio::connect(socket, resolver.resolve({host, port}));
#else
      asio::connect(socket, resolver.resolve(tcp::v4(), host, port));
#endif
      socket.set_option(asio::socket_base::reuse_address(true));
      socket.set_option(asio::socket_base::linger(true,0));
      size_t len = asio::write(socket, request, ec);
      if ( len != req_len )  {
	system::error_code errcode(errno,system::system_category());
	socket.shutdown(tcp::socket::shutdown_both);
	socket.close();
	errno = ENOTCONN;
	str << "XMLRPC [ENOTCONN Failed to write to RPC server] " << errcode.message();
	throw runtime_error(str.str());
      }
      asio::streambuf response;
      asio::read_until(socket, response, "\r\n");

      // Check that response is OK.
      istream response_stream(&response);
      string http_version;
      response_stream >> http_version;
      unsigned int status_code;
      response_stream >> status_code;
      string status_message;
      getline(response_stream, status_message);
      if (!response_stream || http_version.substr(0, 5) != "HTTP/")    {
	system::error_code errcode(errno=EINVAL,system::system_category());
	str << "XMLRPC [EINVAL Invalid XML-RPC response] " << errcode.message();
	errno = errcode.value();
	throw runtime_error(str.str());
      }
      if (status_code != 200)    {
	system::error_code errcode(errno=EINVAL,system::system_category());
	str << "XMLRPC [EINVAL Invalid XML-RPC bad HTTP status code] " << errcode.message();
	errno = errcode.value();
	throw runtime_error(str.str());
      }
      // Read the response headers, which are terminated by a blank line.
      asio::read_until(socket, response, "\r\n\r\n");
      // Process the response headers.
      size_t idx;
      rep.headers.clear();
      HttpHeader contentLengthHdr, contentEncodingHdr;
      while (getline(response_stream, hdr) && hdr != "\r")   {
	HttpHeader header;
	if ( (idx=hdr.find(':'))  != string::npos )
	  header.name = hdr.substr(0,idx);
	if ( (idx=hdr.find(' ',idx)) != string::npos )  {
	  header.value = hdr.substr(idx+1,hdr.find('\r')-idx-1);
	}
	if ( debug>0 )  {
	  printout(INFO,"RPCClient","HTTP Header: %s",hdr.c_str());
	}

	if ( header.name == "Content-Length" )   {
	  contentLength    = ::atol(header.value.c_str());
	  contentLengthHdr = header;
	  continue;
	}
	else if ( header.name == "Content-Encoding" )   {
	  contentEncoding    = header.value;
	  contentEncodingHdr = header;
	  continue;
	}
	rep.headers.push_back(header);
      }
      int resp_len = 0, got_bytes = 0;
      vector<unsigned char>& resp = rep.content;
      if ( contentLength > 0 )   {
	resp.reserve(contentLength);
      }
      do {
	char c = (char)response_stream.get();
	if ( !response_stream.good() ) break;
	++got_bytes;
	resp.push_back(c);
      }  while ( true );
      if ( contentLength > 0 )   {
	unsigned char chunk[0x4000];
	int num_bytes = 0;
	do {
	  num_bytes = asio::read(socket,asio::buffer(chunk, contentLength-got_bytes), ec);
	  if ( ec ) break;
	  if ( num_bytes < 0 ) break;
	  copy(chunk,chunk+num_bytes,back_inserter(resp));
	  got_bytes += num_bytes;
	} while ( !ec && got_bytes < contentLength );
      }
      else   {
	resp_len = asio::read(socket, response, asio::transfer_all(), ec);
	do {
	  char c = (char)response_stream.get();
	  if ( !response_stream.good() ) break;
	  ++got_bytes;
	  resp.push_back(c);
	}  while ( true );
      }
      if ( resp_len != contentLength && got_bytes != contentLength )   {
	printout(ERROR,"RPCClient","Handling response[%ld bytes]: got %ld bytes. [error:%d %s]",
		 contentLength, resp_len, ec.value(), ec.message().c_str());
      }
      else  {
	resp_len = got_bytes;
      }
      socket.shutdown(asio::ip::tcp::socket::shutdown_both,ec);
      socket.close(ec);
      if ( contentEncoding.empty() && !acceptedEncoding.empty() && rep.content.size() > 500 )  {
	// May compress here....
	string used;
	char txt[32];
	size_t rep_len = rep.content.size();
	rep.content = Online::compress::compress(acceptedEncoding,rep.content, used);
	::snprintf(txt,sizeof(txt),"%ld",rep.content.size());
	if ( debug > 0 )  {
	  cout << "HttpRpcBridge: Compression [" << used << "] " << req.uri 
	       << " from " << rep_len << " to " << rep.content.size() << endl;
	}
	contentEncodingHdr.name  = "Content-Encoding";
	contentEncodingHdr.value = used;
	contentLengthHdr.value = txt;
      }
      rep.headers.push_back(contentLengthHdr);
      if ( !contentEncodingHdr.value.empty() ) 
	rep.headers.push_back(contentEncodingHdr);
      return continue_action::write;
    }
    catch(const std::exception& e)   {
      str << e.what();
    }
    catch( ... )   {
      boost::system::error_code errcode(errno,boost::system::system_category());
      str << "XMLRPC fault [" << errcode.value() << "] (UNKOWN Exception): " << errcode.message();
    }
    throw runtime_error(str.str());
  }
}

/// Handle a request and produce a reply.
void rpc::HttpRpcBridge::Handler::handle_bridge_request(const http::Request& req,
							http::Reply& rep)
{
  /// Handle http-http request. Locked access due to multithreading.
  size_t idx, idq;
  HttpRpcHandler* hdlr = 0;
  string uri = req.uri;
  string port = "2500", server, server_reg;
  string refer;
  for(const auto& h : req.headers )  {
    if ( h.name == "Referer" )   {
      string tmp = h.value.substr(h.value.find("//"));
      refer = tmp.substr(tmp.find('/',3));
      // cout << h.name << ": " << h.value << endl;
      break;
    }
  }
  string mnt = uri.substr(0,idx=uri.find('/',1)), new_uri;
  //  debug = 1;
  if ( mnt != mount )   {
    throw std::runtime_error("The request to access the document: "+uri+" cannot be served. [Invalid mount:"+mnt+"]");
  }
  idq = uri.find('/',idx+1);
  server = uri.substr(idx+1,idq==string::npos ? string::npos : idq-idx-1);
  if ( server.empty() )   {
    throw std::runtime_error("The request to access the document: "+uri+" cannot be served. [Invalid server:"+server+"]");
  }

  // Don't know how these special cases develop. This is somehow an interplay 
  // between ExtJs and relative pathes.
  /// Specially treatet nodes:
  UrlRedirections::const_iterator ui = this->url_redirection.find(uri);
  if ( ui != this->url_redirection.end() )   {
    server  = (*ui).second.server;
    port    = (*ui).second.port;
    new_uri = (*ui).second.url;
  }
  else  {
    new_uri = idq==string::npos ? string("/") : uri.substr(idq);
  }
  server_reg = server;
  /// Specially treatet nodes:
  ServerRedirections::const_iterator ri = this->server_redirection.find(server);
  if ( ri != this->server_redirection.end() )   {
    server = (*ri).second.first;
    port   = (*ri).second.second;
  }
  {
    Lock lck(lock);
    auto i = httpHandlers.find(server_reg);
    if ( i == httpHandlers.end() )  {
      hdlr = new BridgeHandler(server, port, debug);
      httpHandlers.insert(make_pair(server_reg,hdlr));
    }
    else  {
      hdlr = (*i).second;
    }
  }
  printout(debug ? ALWAYS : DEBUG,"BridgeHandler",
	   "Forwarding request: %s **to** %s [%s]",
	   uri.c_str(), server.c_str(), refer.c_str());
  const_cast<http::Request*>(&req)->uri = new_uri;
  hdlr->setDebug(debug);
  hdlr->handle_request(req, rep);
}
