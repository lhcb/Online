//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================
#define RPC_HAVE_COMPRESSION

// Framework inclde files
#include <RPC/HttpRpcServer.h>
#include <RPC/HttpRpcHandler.h>
#include <HTTP/HttpServer.h>

// C/C++ include files
#include <iostream>
#include <stdexcept>

using namespace std;
using namespace boost;

/// Standard constructor
rpc::HttpServer::Handler::Handler()    {
}

/// Default destruction
rpc::HttpServer::Handler::~Handler()    {
}

/// Initializing constructor
rpc::HttpServer::HttpServer(std::unique_ptr<Handler>&& imp, 
			    const std::string& h,
			    int p,
			    Mode m,
			    const std::string& uri) 
  : implementation(std::move(imp)), host(h)
{
  port = std::to_string(p);
  implementation->mode = m;
  implementation->server_uri = uri;
}

/// Initializing constructor
rpc::HttpServer::HttpServer(std::unique_ptr<Handler>&& imp,
			    const std::string& h,
			    const std::string& p,
			    const std::string& m,
			    const std::string& uri)
  : implementation(std::move(imp)), host(h), port(p)
{
  implementation->mode = (m=="SERVER") ? SERVER : BRIDGE;
  implementation->server_uri = uri;
}

/// Default destructor
rpc::HttpServer::~HttpServer()  {
  implementation.reset();
}

/// Modify debug flag
int rpc::HttpServer::setDebug(int value)  {
  int tmp = implementation->debug;
  implementation->debug = value;
  return tmp;
}

/// Modify checked server URI (empty -> No check)
string rpc::HttpServer::setUri(const std::string& value)  {
  string tmp = implementation->server_uri;
  implementation->server_uri = value;
  return tmp;
}

/// Start the rpc service
void rpc::HttpServer::start(bool detached, int num_additional_threads)   {
  implementation->open(host,port);
  if ( detached ) {
    return;
  }
  implementation->run(num_additional_threads);
}

/// Stop the rpc service
void rpc::HttpServer::stop()   {
  std::lock_guard<std::mutex> lock(implementation->lock);
  implementation->stop();
}

/// Run the server's io_context loop. If specified additional workers may be used
void rpc::HttpServer::run(int num_additional_threads)   {
  implementation->run(num_additional_threads);
}

/// Remove a callback from the rpc interface
void rpc::HttpServer::remove(const string& name)   {
  std::lock_guard<std::mutex> lock(implementation->lock);
  if ( !implementation->remove(name) )   {
    throw runtime_error("The RPC call "+name+" is not registered to the RPC interface.");
  }
}

/// Remove all callbacks from the xmlrpc interface
void rpc::HttpServer::remove_all()   {
  std::lock_guard<std::mutex> lock(implementation->lock);
  implementation->remove_all();
}

/// Bind default callback to all unhandled functions.
void rpc::HttpServer::onUnhandled(const Callback& call)   {
  std::lock_guard<std::mutex> lock(implementation->lock);
  implementation->onUnhandled.push_back(call);
}

/// Bind default callback to all successfully handled function requests.
void rpc::HttpServer::onHandled(const Callback& call)   {
  std::lock_guard<std::mutex> lock(implementation->lock);
  implementation->onHandled.push_back(call);
}

/// Bind default callback to processing errors.
void rpc::HttpServer::onError(const Callback& call)   {
  std::lock_guard<std::mutex> lock(implementation->lock);
  implementation->onError.push_back(call);
}

/// Stop the rpc service
void rpc::HttpServer::Handler::stop()   {
}

/// Remove a callback from the rpc interface
bool rpc::HttpServer::Handler::remove(const std::string& /* name */)   {
  return false;
}

/// Remove all callbacks from the xmlrpc interface
void rpc::HttpServer::Handler::remove_all()   {
}

/// Check if the request is serverd by a registered mount point handler
rpc::HttpServer::Handler::continue_action 
rpc::HttpServer::Handler::handle_mount_request(const http::Request& req, http::Reply& rep)    {
  size_t      idx = req.uri.rfind("/");
  std::string uri = req.uri.substr(0, idx==0 ? string::npos : idx);
  while ( uri.length() > 0 )   {
    auto handler = this->mountPoints.handlers.find(uri);
    if ( handler != this->mountPoints.handlers.end() )   {
      return handler->second->handle_request(req, rep);
    }
    idx = uri.rfind("/");
    uri = uri.substr(0, idx);
  }
  return Handler::invalid;
}

/// Handle a request and produce a reply.
rpc::HttpServer::Handler::continue_action 
rpc::HttpServer::Handler::handle_request(const http::Request& req, http::Reply& rep)    {
  auto answer = handle_mount_request(req, rep);
  if ( answer != Handler::invalid )   {
    return answer;
  }
  std::cout << "Bad request:" << req.uri << std::endl;
  rep = http::Reply::stock_reply(http::Reply::bad_request);
  rep.headers.clear();
  rep.headers.reserve(5);
  rep.headers.emplace_back(http::HttpHeader("Content-Type",   "text/html"));
  rep.headers.emplace_back(http::HttpHeader("Content-Length", std::to_string(rep.content.size())));
  rep.headers.emplace_back(http::HttpHeader("Connection",     "close"));
  rep.headers.emplace_back(http::HttpHeader("Date",           get_date_string()));
  rep.headers.emplace_back(http::HttpHeader("Server",         "LHCbDataflow Generic RPC"));
  return write;
}
