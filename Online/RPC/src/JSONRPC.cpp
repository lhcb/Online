//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include <RPC/JSONRPC.h>

// C/C++ include files
#include <sstream>
#include <cstring>
#include <cerrno>
#include <stdexcept>
#include <typeinfo>

using namespace std;
using namespace jsonrpc;
typedef char* p_char;
#define _rpcU(x)  std::string(#x)

JSONRPC_NS_BEGIN
const char DATETIME_FORMAT[] = "%Y-%m-%dT%H:%M:%SZ";

long new_id()   {
  static long last_id = 0;
  return ++last_id;
}

struct tm _toTime(const string& s)   {
  struct tm tm;
  ::strptime(s.c_str(), DATETIME_FORMAT, &tm);
  return tm;
}

struct timeval _toTimeVal(const string& s)   {
  struct tm tm = _toTime(s);
  return { ::mktime(&tm), 0 };
}

/// Do-nothing version. Present for completeness and argument interchangeability  \ingroup DD4HEP_JSON
std::string _toString(const struct tm& p) {
  char text[128];
  ::strftime(text,sizeof(text), DATETIME_FORMAT, &p);
  return text;
}

string _toString(const struct timeval& tv)   {
  struct tm p;
  ::localtime_r(&tv.tv_sec, &p);
  return _toString(p);
}

string string_encode(const string& s)   {
  return s;
}
string string_decode(const string& s)   {
  return s;
}

//---------------------------------------------------------------------------------
/// Add a new value to the array
template <typename T> inline const json_h& Array::add(const T& /* val */)   {
  throw runtime_error("Array: Invalid JSON-RPC datatype:"+string(typeid(T).name()));
}

/// Add a new value to the structure object
template <typename T> inline const json_h& Structure::add(const string& /* nam */, const T& /* val */)   {
  throw runtime_error("Structure: Invalid JSON-RPC datatype:"+string(typeid(T).name()));
}

template <typename T> inline T MethodCall::arg(size_t /* handle */)   const   {
  throw runtime_error("MethodCall: Invalid JSON-RPC datatype:"+string(typeid(T).name()));
}

/// Add argument entry <param> to the call stricture
template <typename T> inline MethodCall& addParam(const string& name, const T& /* value */)  {
  throw runtime_error("MethodCall: Invalid JSON-RPC parameter:"+name+": "+string(typeid(T).name()));
}

/// Add a new parameter to the response
template <typename T> inline MethodResponse MethodResponse::make(const T& /* param */)  {
  throw runtime_error("MethodResponse: Invalid JSON-RPC response datatype:"+string(typeid(T).name()));
}

/// Access return value data element by true type
template <typename T> inline T MethodResponse::data()   const   {
  throw runtime_error("MethodResponse: Invalid JSON-RPC response datatype:"+string(typeid(T).name()));
}

//---------------------------------------------------------------------------------
/// Add a new parameter to the response
template <> const json_h& Array::add<bool>(const bool& p)
{  this->root.emplace_back(p); return this->root.back();   }
/// Add a new parameter to the response
template <> const json_h& Array::add<char>(const char& p)
{  this->root.emplace_back(p); return this->root.back();   }
/// Add a new parameter to the response
template <> const json_h& Array::add<signed char>(const signed char& p)
{  this->root.emplace_back(p); return this->root.back();   }
/// Add a new parameter to the response
template <> const json_h& Array::add<unsigned char>(const unsigned char& p)
{  this->root.emplace_back(p); return this->root.back();   }
/// Add a new parameter to the response
template <> const json_h& Array::add<short>(const short& p)
{  this->root.emplace_back(p); return this->root.back();   }
/// Add a new parameter to the response
template <> const json_h& Array::add<unsigned short>(const unsigned short& p)
{  this->root.emplace_back(p); return this->root.back();   }
/// Add a new parameter to the response
template <> const json_h& Array::add<int>(const int& p)
{  this->root.emplace_back(p); return this->root.back();   }
/// Add a new parameter to the response
template <> const json_h& Array::add<unsigned int>(const unsigned int& p)
{  this->root.emplace_back(p); return this->root.back();   }
/// Add a new parameter to the response
template <> const json_h& Array::add<long>(const long& p)
{  this->root.emplace_back(p); return this->root.back();   }
/// Add a new parameter to the response
template <> const json_h& Array::add<unsigned long>(const unsigned long& p)
{  this->root.emplace_back(p); return this->root.back();   }
/// Add a new parameter to the response
template <> const json_h& Array::add<double>(const double& p)
{  this->root.emplace_back(p); return this->root.back();   }
/// Add a new parameter to the response
template <> const json_h& Array::add<float>(const float& p)
{  this->root.emplace_back(p); return this->root.back();   }
/// Add a new parameter to the response
template <> const json_h& Array::add<string>(const string& p)
{  this->root.emplace_back(p); return this->root.back();   }
/// Add a new parameter to the response
template <> const json_h& Array::add<p_char>(const p_char& p)
{  this->root.emplace_back(p); return this->root.back();   }
/// Add a new parameter to the response
template <> const json_h& Array::add<struct tm>(const struct tm& p)
{  this->root.emplace_back(_toString(p)); return this->root.back();   }
/// Add a new parameter to the response
template <> const json_h& Array::add<struct timeval>(const struct timeval& p)
{  this->root.emplace_back(_toString(p)); return this->root.back();   }
/// Add a new parameter to the response
template <> const json_h& Array::add<Array>(const Array& p)
{  this->root.push_back(p.root); return this->root.back();   }
/// Add a new parameter to the response
template <> const json_h& Array::add<Structure>(const Structure& p)
{  this->root.push_back(p.root); return this->root.back();   }
///
/// ---------------------------------------------------------------------------------
/// Add a new parameter to the response
template <> const json_h& Structure::add<bool>(const string& nam, const bool& p)
{  this->root.emplace(nam, p);   return this->root.at(nam);                                 }
/// Add a new parameter to the response
template <> const json_h& Structure::add<char>(const string& nam, const char& p)
{  this->root.emplace(nam, p);   return this->root.at(nam);                                 }
/// Add a new parameter to the response
template <> const json_h& Structure::add<signed char>(const string& nam, const signed char& p)
{  this->root.emplace(nam, p);   return this->root.at(nam);                                 }
/// Add a new parameter to the response
template <> const json_h& Structure::add<unsigned char>(const string& nam, const unsigned char& p)
{  this->root.emplace(nam, p);   return this->root.at(nam);                                 }
/// Add a new parameter to the response
template <> const json_h& Structure::add<short>(const string& nam, const short& p)
{  this->root.emplace(nam, p);   return this->root.at(nam);                                 }
/// Add a new parameter to the response
template <> const json_h& Structure::add<unsigned short>(const string& nam, const unsigned short& p)
{  this->root.emplace(nam, p);   return this->root.at(nam);                                 }
/// Add a new parameter to the response
template <> const json_h& Structure::add<int>(const string& nam, const int& p)
{   this->root.emplace(nam, p);   return this->root.at(nam);                                }
/// Add a new parameter to the response
template <> const json_h& Structure::add<unsigned int>(const string& nam, const unsigned int& p)
{   this->root.emplace(nam, p);   return this->root.at(nam);                                }
/// Add a new parameter to the response
template <> const json_h& Structure::add<long>(const string& nam, const long& p)
{   this->root.emplace(nam, p);   return this->root.at(nam);                                }
/// Add a new parameter to the response
template <> const json_h& Structure::add<unsigned long>(const string& nam, const unsigned long& p)
{   this->root.emplace(nam, p);   return this->root.at(nam);                                }
/// Add a new parameter to the response
template <> const json_h& Structure::add<double>(const string& nam, const double& p)
{   this->root.emplace(nam, p);   return this->root.at(nam);                                }
/// Add a new parameter to the response
template <> const json_h& Structure::add<float>(const string& nam, const float& p)
{   this->root.emplace(nam, p);   return this->root.at(nam);                                }
/// Add a new parameter to the response
template <> const json_h& Structure::add<p_char>(const string& nam, const p_char& p)
{   this->root.emplace(nam, p ? string_encode(p) : string());   return this->root.at(nam);  }
/// Add a new parameter to the response
template <> const json_h& Structure::add<string>(const string& nam, const string& p)
{   this->root.emplace(nam, p.empty() ? p : string_encode(p));   return this->root.at(nam); }
/// Add a new parameter to the response
template <> const json_h& Structure::add<struct tm>(const string& nam, const struct tm& p)
{  this->root.emplace(nam, _toString(p));   return this->root.at(nam);                      }
/// Add a new parameter to the response
template <> const json_h& Structure::add<struct timeval>(const string& nam, const struct timeval& p)
{  this->root.emplace(nam, _toString(p));   return this->root.at(nam);                      }
/// Add a new parameter to the response
template <> const json_h& Structure::add<Array>(const string& nam, const Array& arr)
{   this->root.emplace(nam, arr.root);   return this->root.at(nam);                         }
/// Add a new parameter to the response
template <> const json_h& Structure::add<Structure>(const string& nam, const Structure& str)
{   this->root.emplace(nam, str.root);   return this->root.at(nam);                         }

/// ---------------------------------------------------------------------------------
/// Access data element by true type
template <> string MethodCall::arg<string>(size_t handle)   const
{   return string_decode(this->params()[handle].get<string>());  }
/// Access data element by true type
template <> bool MethodCall::arg<bool>(size_t handle)   const
{   return this->params()[handle].get<bool>();                   }
/// Access data element by true type
template <> char MethodCall::arg<char>(size_t handle)   const
{   return this->params()[handle].get<char>();                   }
/// Access data element by true type
template <> signed char MethodCall::arg<signed char>(size_t handle)   const
{   return this->params()[handle].get<signed char>();            }
/// Access data element by true type
template <> unsigned char MethodCall::arg<unsigned char>(size_t handle)   const
{   return this->params()[handle].get<unsigned char>();          }
/// Access data element by true type
template <> short MethodCall::arg<short>(size_t handle)   const
{   return this->params()[handle].get<short>();                  }
/// Access data element by true type
template <> unsigned short MethodCall::arg<unsigned short>(size_t handle)   const
{   return this->params()[handle].get<unsigned short>();         }
/// Access data element by true type
template <> int MethodCall::arg<int>(size_t handle)   const
{   return this->params()[handle].get<int>();                    }
/// Access data element by true type
template <> unsigned int MethodCall::arg<unsigned int>(size_t handle)   const
{   return this->params()[handle].get<unsigned int>();           }
/// Access data element by true type
template <> long MethodCall::arg<long>(size_t handle)   const
{   return this->params()[handle].get<long>();                   }
/// Access data element by true type
template <> unsigned long MethodCall::arg<unsigned long>(size_t handle)   const
{   return this->params()[handle].get<unsigned long>();          }
/// Access data element by true type
template <> double MethodCall::arg<double>(size_t handle)   const
{   return this->params()[handle].get<double>();                 }
/// Access data element by true type
template <> float MethodCall::arg<float>(size_t handle)   const
{   return this->params()[handle].get<float>();                  }
/// Access data element by true type
template <> struct tm MethodCall::arg<struct tm>(size_t handle)   const
{   return _toTime(this->params()[handle].get<string>());        }
/// Access data element by true type
template <> struct timeval MethodCall::arg<struct timeval>(size_t handle)   const
{   return _toTimeVal(this->params()[handle].get<string>());     }

/// Add a new parameter to the rpc method call
template <> const MethodCall& MethodCall::addParam<bool>(const bool& p)  const
{    this->params().push_back(p);   return *this;                 }
/// Add a new parameter to the rpc method call
template <> const MethodCall& MethodCall::addParam<char>(const char& p)  const
{    this->params().push_back(p);   return *this;                 }
/// Add a new parameter to the rpc method call
template <> const MethodCall& MethodCall::addParam<signed char>(const signed char& p)  const
{    this->params().push_back(p);   return *this;                 }
/// Add a new parameter to the rpc method call
template <> const MethodCall& MethodCall::addParam<unsigned char>(const unsigned char& p)  const
{    this->params().push_back(p);   return *this;                 }
/// Add a new parameter to the rpc method call
template <> const MethodCall& MethodCall::addParam<short>(const short& p)  const
{    this->params().push_back(p);   return *this;                 }
/// Add a new parameter to the rpc method call
template <> const MethodCall& MethodCall::addParam<unsigned short>(const unsigned short& p)  const
{    this->params().push_back(p);   return *this;                 }
/// Add a new parameter to the rpc method call
template <> const MethodCall& MethodCall::addParam<int>(const int& p)  const
{    this->params().push_back(p);   return *this;                 }
/// Add a new parameter to the rpc method call
template <> const MethodCall& MethodCall::addParam<unsigned int>(const unsigned int& p)  const
{    this->params().push_back(p);   return *this;                 }
/// Add a new parameter to the rpc method call
template <> const MethodCall& MethodCall::addParam<long>(const long& p)  const
{    this->params().push_back(p);   return *this;                 }
/// Add a new parameter to the rpc method call
template <> const MethodCall& MethodCall::addParam<unsigned long>(const unsigned long& p)  const
{    this->params().push_back(p);   return *this;                 }
/// Add a new parameter to the rpc method call
template <> const MethodCall& MethodCall::addParam<double>(const double& p)  const
{    this->params().push_back(p);   return *this;                 }
/// Add a new parameter to the rpc method call
template <> const MethodCall& MethodCall::addParam<float>(const float& p)  const
{    this->params().push_back(p);  return *this;                  }
/// Add a new parameter to the rpc method call
template <> const MethodCall& MethodCall::addParam<struct tm>(const struct tm& p)  const
{    this->params().push_back(_toString(p));  return *this;       }
/// Add a new parameter to the rpc method call
template <> const MethodCall& MethodCall::addParam<struct timeval>(const struct timeval& p)  const
{    this->params().push_back(_toString(p));  return *this;       }
/// Add a new parameter to the rpc method call
template <> const MethodCall& MethodCall::addParam<string>(const string& p)  const
{    this->params().push_back(string_encode(p));  return *this;   }
/// Add a new parameter to the rpc method call
template <> const MethodCall& MethodCall::addParam<p_char>(const p_char& p)  const
{    this->params().push_back(p ? string_encode(p) : string(""));  return *this; }
/// Add a new parameter to the rpc method call
template <> const MethodCall& MethodCall::addParam<Structure>(const Structure& str)  const
{    this->params().push_back(str.root);  return *this;           }
/// Add a new parameter to the rpc method call
template <> const MethodCall& MethodCall::addParam<Array>(const Array& arr)  const
{    this->params().push_back(arr.root);  return *this;           }

//---------------------------------------------------------------------------------
/// Add a new parameter to the response
template <> MethodResponse MethodResponse::make<bool>(const bool& p)
{    return encode(p);                                   }
/// Add a new parameter to the response
template <> MethodResponse MethodResponse::make<char>(const char& p)
{    return encode(p);                                   }
/// Add a new parameter to the response
template <> MethodResponse MethodResponse::make<signed char>(const signed char& p)
{    return encode(p);                                   }
/// Add a new parameter to the response
template <> MethodResponse MethodResponse::make<unsigned char>(const unsigned char& p)
{    return encode(p);                                   }
/// Add a new parameter to the response
template <> MethodResponse MethodResponse::make<short>(const short& p)
{    return encode(p);                                   }
/// Add a new parameter to the response
template <> MethodResponse MethodResponse::make<unsigned short>(const unsigned short& p)
{    return encode(p);                                   }
/// Add a new parameter to the response
template <> MethodResponse MethodResponse::make<int>(const int& p)
{    return encode(p);                                   }
/// Add a new parameter to the response
template <> MethodResponse MethodResponse::make<unsigned int>(const unsigned int& p)
{    return encode(p);                                   }
/// Add a new parameter to the response
template <> MethodResponse MethodResponse::make<long>(const long& p)
{    return encode(p);                                   }
/// Add a new parameter to the response
template <> MethodResponse MethodResponse::make<unsigned long>(const unsigned long& p)
{    return encode(p);                                   }
/// Add a new parameter to the response
template <> MethodResponse MethodResponse::make<double>(const double& p)
{    return encode(p);                                   }
/// Add a new parameter to the response
template <> MethodResponse MethodResponse::make<float>(const float& p)
{    return encode(p);                                   }
/// Add a new parameter to the response
template <> MethodResponse MethodResponse::make<string>(const string& p)
{    return encode(string_encode(p));                    }
/// Add a new parameter to the response
template <> MethodResponse MethodResponse::make<struct tm>(const struct tm& p)
{    return encode(_toString(p));                         }
/// Add a new parameter to the response
template <> MethodResponse MethodResponse::make<struct timeval>(const struct timeval& p)
{    return encode(_toString(p));                         }
/// Add a new parameter to the response
template <> MethodResponse MethodResponse::make<p_char>(const p_char& p)
{    return encode(p ? string_encode(p) : string());     }
/// Add a new parameter to the response
template <> MethodResponse MethodResponse::make<Structure>(const Structure& s)
{    return encode(s.root);                              }
/// Add a new parameter to the response
template <> MethodResponse MethodResponse::make<Array>(const Array& a)
{    return encode(a.root);                              }

/// Access data element by true type
template <> string MethodResponse::data<string>()   const
{   return string_decode(this->root.at(_rpcU(result)));             }
/// Access data element by true type
template <> bool MethodResponse::data<bool>()   const
{   return this->root.at(_rpcU(result));                            }
/// Access data element by true type
template <> signed char MethodResponse::data<signed char>()   const
{   return this->root.at(_rpcU(result));                            }
/// Access data element by true type
template <> unsigned char MethodResponse::data<unsigned char>()   const
{   return this->root.at(_rpcU(result));                            }
/// Access data element by true type
template <> short MethodResponse::data<short>()   const
{   return this->root.at(_rpcU(result));                            }
/// Access data element by true type
template <> unsigned short MethodResponse::data<unsigned short>()   const
{   return this->root.at(_rpcU(result));                            }
/// Access data element by true type
template <> int MethodResponse::data<int>()   const
{   return this->root.at(_rpcU(result));                            }
/// Access data element by true type
template <> unsigned int MethodResponse::data<unsigned int>()   const
{   return this->root.at(_rpcU(result));                            }
/// Access data element by true type
template <> long MethodResponse::data<long>()   const
{   return this->root.at(_rpcU(result));                            }
/// Access data element by true type
template <> unsigned long MethodResponse::data<unsigned long>()   const
{   return this->root.at(_rpcU(result));                            }
/// Access data element by true type
template <> double MethodResponse::data<double>()   const
{   return this->root.at(_rpcU(result));                            }
/// Access data element by true type
template <> float MethodResponse::data<float>()   const
{   return this->root.at(_rpcU(result));                            }
/// Access data element by true type
template <> struct tm MethodResponse::data<struct tm>()   const
{   return _toTime(this->root.at(_rpcU(result)));                   }
/// Access data element by true type
template <> struct timeval MethodResponse::data<struct timeval>() const
{   return _toTimeVal(this->root.at(_rpcU(result)));                }

JSONRPC_NS_END

/// Initializing constructor
Array::Array(const json_h& r) : root(r)  {
}

/// Initializing constructor
Array::Array(json_h&& r) : root(r)  {
}

/// Default destructor
Array::~Array()  {
}

/// Retrieve string representation of the object
string Array::str() const   {
  stringstream out;
  out << root.dump();
  return out.str();
}

/// Access to the array's root element
const json_h& Array::array()  const  {
  return this->root;
}

/// Initializing constructor
Structure::Structure(const json_h& r) : root(r)  {
}

/// Initializing constructor
Structure::Structure(json_h&& r) : root(r)  {
}

/// Default destructor
Structure::~Structure()  {
}

/// Retrieve string representation of the object
string Structure::str() const   {
  stringstream out;
  out << root.dump();
  return out.str();
}

/// Access to the array's root element
const json_h& Structure::structure()   const  {
  return this->root;
}

/// Initializing constructor
MethodCall::MethodCall()
{
  this->root = json_h::object({{"jsonrpc", "2.0"},{"params",json_h::array({})},{"id",new_id()}});
}

/// Initializing constructor
MethodCall::MethodCall(json_h&& handle)
  : root(handle)
{
}

/// Initializing constructor parsing initial text object
MethodCall::MethodCall(const char* json_text)
{
  this->root = json_h::parse(json_text);
}

/// Initializing constructor parsing initial text object
MethodCall::MethodCall(const string& json_text)
{
  this->root = json_h::parse(json_text);
}

/// Move constructor
MethodCall::MethodCall(MethodCall&& copy)
  : root(std::move(copy.root))
{
}

/// Default destructor
MethodCall::~MethodCall()   {
}

/// Move constructor
MethodCall& MethodCall::operator = (MethodCall&& copy)  {
  this->root = std::move(copy.root);
  return *this;
}

/// Access the RPC call sequence id
int MethodCall::id()  const    {
  return this->root.at("id");
}

/// Access call paramaters array
json_h& MethodCall::params()  const  {
  auto* r = const_cast<json_h*>(&this->root);
  return r->at("params");
}
#if 0
/// Access call paramaters array
const json_h& MethodCall::params()   const  {
  return this->root.at("params");
}
#endif
/// Check validity of passed document
void MethodCall::check()      {
  string m = this->root.at("method");
  if ( !m.empty() )  {
    return;
  }
  throw runtime_error("Invalid JSON-RPC methodCall object!");
}

/// Retrieve string representation of the object
string MethodCall::str() const   {
  stringstream out;
  out << root.dump();
  return out.str();
}

/// Access the <methodName> field of the call
string MethodCall::method()  const  {
  return this->root["method"];
}

/// Add <methodName> tag to the call stricture
const MethodCall& MethodCall::setMethod(const string& nam) {
  this->root["method"] = nam;
  return *this;
}

/// Number of arguments supplied
size_t MethodCall::numArgs() const   {
  return this->root.at("params").size();
}

/// Initializing constructor
MethodResponse::MethodResponse()
{
  this->root = json_h::object({{"jsonrpc", "2.0"},{"id",0}});
}

/// Initializing constructor
MethodResponse::MethodResponse(int id)
{
  this->root = json_h::object({{"jsonrpc", "2.0"},{"id",id}});
}

/// Add fully qualified json result to the response
MethodResponse::MethodResponse(json_h&& object)   {
  this->root = json_h::object({{"jsonrpc", "2.0"},{"id",0},{"result",std::move(object)}});
}

/// Initializing constructor from existing JSON document
MethodResponse::MethodResponse(const std::string& json_text)
{
  this->root = json_h::parse(json_text);
}

/// Initializing constructor from existing JSON document
MethodResponse::MethodResponse(const void* json, size_t num_bytes)
{
  const char* p = (const char*)json;
  string s(p, p+num_bytes);
  this->root = json_h::parse(s);
}

/// Initializing constructor from existing JSON document
MethodResponse::MethodResponse(const std::vector<char>& json_text)
{
  string s(json_text.begin(), json_text.end());
  this->root = json_h::parse(s);
}

/// Check validity of passed document
void MethodResponse::check()  {
  if ( this->root.find(_rpcU(result)) != this->root.end() )   {
    return;
  }
  else if ( this->root.find(_rpcU(error)) != this->root.end() )  {
    json_h fault = this->root.at(_rpcU(error));
    if ( fault.find(_rpcU(code)) != fault.end() )
      return;
  }
  throw runtime_error("Invalid JSON-RPC document [No proper ROOT handle]");
}

/// Access the data content of the JSONRPC response structure
json_h& MethodResponse::value()   {
  if ( this->root.find(_rpcU(result)) != this->root.end() )
    return this->root.at(_rpcU(result));
  return this->root.at(_rpcU(error));
}

/// Check if the response structure contains fault information
bool MethodResponse::isFault()  const  {
  return this->root.find(_rpcU(error)) != this->root.end();
}

/// Access the faults strcuture from the response
json_h& MethodResponse::fault()  {
  if ( isFault() )  
    return root.at(_rpcU(error));
  throw runtime_error("Invalid JSON-RPC response [no <faults> element found]");
}

/// Access the fault code (requires a fault element in the response)
int MethodResponse::faultCode()  const   {
  if ( this->root.find(_rpcU(error)) != this->root.end() )  {
    json_h fault = this->root.at(_rpcU(error));
    if ( fault.find(_rpcU(code)) != fault.end() )
      return fault.at(_rpcU(code));
  }
  return 0; // Cannot really throw exception here if analysing already an error
}

/// Access the fault message string (requires a fault element in the response)
string MethodResponse::faultString()  const   {
  if ( this->root.find(_rpcU(error)) != this->root.end() )  {
    json_h fault = this->root.at(_rpcU(error));
    if ( fault.find(_rpcU(message)) != fault.end() )
      return fault.at(_rpcU(message));
  }
  return "Invalid root"; // Cannot really throw exception here if analysing already an error
}

/// Check if the response structure contains <params>
bool MethodResponse::isOK()  const  {
  return this->root.find(_rpcU(result)) != this->root.end();
}

/// Set proper answer ID for the client response
MethodResponse& MethodResponse::setId(int id)   {
  this->root[_rpcU(id)] = id;
  return *this;
}

/// Retrieve string representation of the object
string MethodResponse::str() const   {
  stringstream out;
  out << this->root.dump();
  return out.str();
  //return to_string(this->root);
}

/// Create a response with no return value
MethodResponse MethodResponse::make()   {
  return MethodResponse(0);
}

/// Add fully qualified json result to the response
MethodResponse MethodResponse::make(json_h&& result)   {
  MethodResponse r(0);
  r.root[_rpcU(result)] = std::move(result);
  return r;
}

/// Add fully qualified json result to the response
MethodResponse MethodResponse::make(MethodResponse&& result)   {
  MethodResponse r(0);
  r.root = std::move(result.root);
  return r;
}

/// Set fault information in case of failure. Message is strerror(code).
MethodResponse MethodResponse::makeFault(int id, int code)   {
  return makeFault(id, code,
		   std::error_condition(code,std::system_category()).message());
}

/// Set fault information in case of failure
MethodResponse MethodResponse::makeFault(int id, int code, const string& reason)   {
  MethodResponse resp(id);
  resp.root["error"] = json_h::object({{"code",code},{"message", reason}});
  return resp;
}

/// Set fault information in case of failure
MethodResponse MethodResponse::makeFault(int code, const string& reason)   {
  MethodResponse resp(0);
  resp.root["error"] = json_h::object({{"code",code},{"message", reason}});
  return resp;
}

/// Decode the response of a server
MethodResponse MethodResponse::decode(const string& json_text)   {
  return MethodResponse(json_text);
}

/// Decode the response of a server
MethodResponse MethodResponse::decode(const std::vector<unsigned char>& json_text)   {
  return MethodResponse(&json_text[0], json_text.size());
}

/// Decode the response of a server
MethodResponse MethodResponse::decode(const void* ptr, size_t num_bytes)   {
  return MethodResponse(ptr, num_bytes);
}

/// Copy constructor
CallSequence::CallSequence(const CallSequence& c) : callbacks(c.callbacks) {
}

/// Execution overload for callbacks with no arguments
void CallSequence::operator()(const MethodCall& c) const {
  if (!callbacks.empty()) {
    for (Calls::const_iterator i = callbacks.begin(); i != callbacks.end(); ++i)
      (*i).execute(c);
  }
}

/// Check the compatibility of two typed objects. The test is the result of a dynamic_cast
void CallSequence::checkTypes(const type_info& typ1, const type_info& typ2, void* test) {
  if (!test) {
    string msg = "The types ";
    msg += typ1.name();
    msg += " and ";
    msg += typ2.name();
    msg += " are unrelated. Cannot install a callback for these 2 types.";
    throw runtime_error(msg);
  }
}

#define JSONRPC_IMPLEMENT_PRIMITIVE_MARSCHALLING(X)    JSONRPC_NS_BEGIN \
  template <> MethodResponse MethodResponse::make< X >(const X& b)   {	\
    return MethodResponse::encode(b);					\
  }									\
  template <> X MethodCall::arg< X >(size_t handle)  const	 {	\
    return this->params()[handle].get< X >();				\
  }									\
  JSONRPC_NS_END
#define JSONRPC_IMPLEMENT_PRIMITIVE_CONTAINER(typ,X)	\
  JSONRPC_IMPLEMENT_PRIMITIVE_MARSCHALLING(std::vector<X>)	\
  JSONRPC_IMPLEMENT_PRIMITIVE_MARSCHALLING(std::list<X>)	\
  JSONRPC_IMPLEMENT_PRIMITIVE_MARSCHALLING(std::set<X>)

//JSONRPC_IMPLEMENT_PRIMITIVE_CONTAINER(0,bool)
JSONRPC_IMPLEMENT_PRIMITIVE_CONTAINER(0,char)
JSONRPC_IMPLEMENT_PRIMITIVE_CONTAINER(0,unsigned char)
JSONRPC_IMPLEMENT_PRIMITIVE_CONTAINER(0,short)
JSONRPC_IMPLEMENT_PRIMITIVE_CONTAINER(0,unsigned short)
JSONRPC_IMPLEMENT_PRIMITIVE_CONTAINER(0,int)
JSONRPC_IMPLEMENT_PRIMITIVE_CONTAINER(0,unsigned int)
JSONRPC_IMPLEMENT_PRIMITIVE_CONTAINER(0,long)
JSONRPC_IMPLEMENT_PRIMITIVE_CONTAINER(0,unsigned long)
JSONRPC_IMPLEMENT_PRIMITIVE_CONTAINER(0,float)
JSONRPC_IMPLEMENT_PRIMITIVE_CONTAINER(0,double)
JSONRPC_IMPLEMENT_PRIMITIVE_CONTAINER(0,string)
//JSONRPC_IMPLEMENT_PRIMITIVE_CONTAINER(0,struct timeval)
//JSONRPC_IMPLEMENT_PRIMITIVE_CONTAINER(0,struct tm)
