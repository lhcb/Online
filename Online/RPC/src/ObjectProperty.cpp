//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include <CPP/ObjectProperty.h>
#include <RPC/XMLRPC.h>

// Example how to implement data marshalling for an arbitrary class
namespace xmlrpc {
  template <> rpc::ObjectProperty XmlCoder::to() const {
    rpc::ObjectProperty val;
    xml_h struc = element.child(_rpcU(struct));
    for(xml_coll_t c(struc,_rpcU(member)); c; ++c)   {
      std::string n = c.child(_rpcU(name)).text();
      if      ( n == "client" ) val.client = Arg::get_string(c);
      else if ( n == "name"   ) val.name   = Arg::get_string(c);
      else if ( n == "value"  ) val.value  = Arg::get_string(c);
    }
    return val;
  }

  template <> const XmlCoder& XmlCoder::from(const rpc::ObjectProperty& b) const {
    Structure(element).add("client", b.client);
    Structure(element).add("name",   b.name);
    Structure(element).add("value",  b.value);
    return *this;
  }
}

XMLRPC_IMPLEMENT_OBJECT_MARSCHALLING(struct,rpc::ObjectProperty)

XMLRPC_IMPLEMENT_CONTAINER_CONVERSION(std::vector,rpc::ObjectProperty)
XMLRPC_IMPLEMENT_OBJECT_MARSCHALLING(array,std::vector<rpc::ObjectProperty>)

XMLRPC_IMPLEMENT_CONTAINER_CONVERSION(std::list,rpc::ObjectProperty)
XMLRPC_IMPLEMENT_OBJECT_MARSCHALLING(array,std::list<rpc::ObjectProperty>)

#include <RPC/JSONRPC.h>
using json = nlohmann::json;

namespace rpc {
  void to_json(json& j, const ObjectProperty& p) {
    j = json{{"client", p.client}, {"name", p.name}, {"value", p.value}};
  }
  void from_json(const json& j, ObjectProperty& p) {
    j.at("client") .get_to(p.client);
    j.at("name")   .get_to(p.name);
    j.at("value")  .get_to(p.value);
  }
}

//JSONRPC_IMPLEMENT_OBJECT_MARSCHALLING(typ,X)
JSONRPC_IMPLEMENT_OBJECT_CONTAINER(class,rpc::ObjectProperty)
