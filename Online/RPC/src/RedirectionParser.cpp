//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include <RPC/RedirectionParser.h>
#include <RPC/XMLRPCTags.h>
#include <XML/DocumentHandler.h>
#include <XML/XML.h>

rpc::RedirectionParser::Redirections rpc::RedirectionParser::parse(const std::string& uri)    {
  Redirections redirs;
  xml_doc_holder_t doc(xml_handler_t().load(uri));
  for(xml_coll_t c(doc.root(), _rpcU(redirection)); c; ++c)   {
    xml_elt_t e = c;
    std::map<std::string, std::string> re;
    if ( e.hasAttr(_rpcU(server)) )   {
      re["server"] = e.attr<std::string>(_rpcU(server));
      re["to"]     = e.attr<std::string>(_rpcU(to));
      re["port"]   = e.attr<std::string>(_rpcU(port));
      redirs.push_back(re);
    }
    else if ( e.hasAttr(_rpcU(url)) )    {
      re["url"]    = e.attr<std::string>(_rpcU(url));
      re["to"]     = e.attr<std::string>(_rpcU(to));
      re["port"]   = e.attr<std::string>(_rpcU(port));
      re["to_url"] = e.attr<std::string>(e.hasAttr(_rpcU(to_url)) ? _rpcU(to_url) : _rpcU(url));
      redirs.push_back(re);
    }
  }
  return redirs;
}
