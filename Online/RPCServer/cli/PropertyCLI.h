//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================
#ifndef RPC_PROPERTYCLI_H
#define RPC_PROPERTYCLI_H

/// Framework include files
#include <HTTP/HttpClient.h>
#include <RPC/DataflowRPC.h>

// C/C++ include files
#include <memory>

/// Namespace for the xmlrpc implementation
namespace xmlrpc {

  /// Cli information access class
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   */
  class PropertyCLI {
  protected:
    dataflow::DomainInfo        m_domainClient;
    dataflow::PropertyContainer m_taskClient;
  public:
    /// Flag for printing
    int print = 2;

  public:
    /// Default constructor
    PropertyCLI();
    /// No copy constructor
    PropertyCLI(const PropertyCLI& copy) = delete;
    /// Move constructor
    PropertyCLI(PropertyCLI&& copy) = default;
    /// Default destructor
    virtual ~PropertyCLI() = default;
    /// No assignment operator
    PropertyCLI& operator=(const PropertyCLI& copy) = delete;
    /// Move assignment operator
    PropertyCLI& operator=(PropertyCLI&& copy) = default;
    /// Access the domain client
    dataflow::DomainInfo& domainClient();
    /// Access the domain client
    dataflow::PropertyContainer& taskClient(const std::string& dns, const std::string& task);
    /// Printout of a single client option
    virtual void printOption(const std::string& client,
			     const std::string& name,
			     const std::string& value)  const;

    /// Show the known DNS domains
    int showDomains();
    /// Show all nodes in a DNS domain
    int showNodes(const std::string& dns);
    /// Listthe tasksof a given node
    int showTasks(const std::string& dns,const std::string& node);
    /// List the tasksof a given node according toa selection
    int showTasksByRegex(const std::string& dns,const std::string& match);
    /// Show all the options of a particular task
    int showOpts(const std::string& dns, const std::string& task);
    /// Set a single option to an existing process
    int setOption(const std::string& dns, const std::string& task, const std::string& opt);
    /// Download options from file to an existing process
    int downloadOptions(const std::string& dns, const std::string& task, const std::string& file);
  };
}       /// End xmlrpc namespace */
#endif  /*  RPC_PROPERTYCLI_H    */
