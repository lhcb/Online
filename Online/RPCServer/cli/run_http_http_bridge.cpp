//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  XMLRPC_tests.cpp
//--------------------------------------------------------------------------
//
//  Package    : XML
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include <RPC/RedirectionParser.h>
#include <RPC/HttpRpcBridge.h>
#include <XML/Printout.h>
#include <LOG/FifoLog.h>
#include <RTL/Logger.h>
#include <dim/dis.h>

// C/C++ include files
#include <iostream>
#include <cstring>

namespace  {
  void help_server()  {
    ::printf
      ("run_http_http_bridge -opt [-opt]\n"
       "   -host    <host-name>    Server host name. Default: 0.0.0.0          \n"
       "   -port    <port-number>  Server port       Default: 8000             \n"
       "   -utgid   <process-name> Dim server name   Default: HttpDimRpcBridge \n"
       "   -threads <number>       Number of additional helper threads         \n"
       "   -options <file>         Supply redirection information.             \n"
       "   -debug                  Set debug flag ON                           \n");
    ::exit(EINVAL);
  }
}

extern "C" int run_http_http_bridge(int argc, char** argv)   {
  using namespace dd4hep;
  int debug   = 0;
  int threads = 0;
  int port    = 8000;
  std::string host = "0.0.0.0", utgid=RTL::processName(), mount, fifo, options;
  for(int i = 0; i < argc && argv[i]; ++i)  {
    if ( 0 == ::strncmp("-port",argv[i],4) )
      port = ::atol(argv[++i]);
    else if ( 0 == ::strncmp("-host",argv[i],4) )
      host = argv[++i];
    else if ( 0 == ::strncmp("-utgid",argv[i],4) )
      utgid = argv[++i];
    else if ( 0 == ::strncmp("-mount",argv[i],4) )
      mount = argv[++i];
    else if ( 0 == ::strncmp("-threads",argv[i],4) )
      threads = ::atol(argv[++i]);
    else if ( 0 == ::strncmp("-debug",argv[i],4) )
      debug = ((i+1) == argc || argv[i+1][0] == '-') ? 1 : ::atol(argv[++i]);
    else if ( 0 == ::strncmp("-options",argv[i],4) )
      options = argv[++i];
    else if ( 0 == ::strncmp("-help",argv[i],2) )
      help_server();
  }
  RTL::Logger::install_log(RTL::Logger::log_args(LIB_RTL_WARNING));
  rpc::HttpRpcBridge server(host,port);
  server.setDebug(debug);
  server.setMount(mount);
  if ( !options.empty() )    {
    rpc::RedirectionParser parser;
    auto result = parser.parse(options);
    for(auto& m : result)   {
      if ( m.find("server") != m.end() )
	server.addServerRedirection(m["server"], m["to"], m["port"]);
      if ( m.find("url") != m.end() )
	server.addUrlRedirection(m["url"], m["to"], m["port"], m["to_url"]);
    }
  }
  printout(INFO,"HTTPSERVER",
	   "TEST> Starting test XMLRPC bridge with URI:%s:%d [%d threads]\n",
	   host.c_str(), port, threads);
  ::dis_start_serving(utgid.c_str());
  server.start(false, threads);
  return 0;
}
