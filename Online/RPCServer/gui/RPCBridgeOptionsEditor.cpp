//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================

/// Framework include files
#include "RPC/GUI/RPCBridgeOptionsEditor.h"
#include "HTTP/HttpClient.h"
#include "CPP/IocSensor.h"

/// ROOT include files
#include "TGComboBox.h"
#include "TGTextEntry.h"

ClassImp(xmlrpc::RPCBridgeOptionsEditor)

using namespace std;
using namespace xmlrpc;

/// Standard initializing constructor
RPCBridgeOptionsEditor::RPCBridgeOptionsEditor(TGWindow* p, CPP::Interactor* g)
  : RPCOptionsEditor(p,g)
{
}

/// GUI handler: Default destructor
RPCBridgeOptionsEditor::~RPCBridgeOptionsEditor()   {
}

/// Create communication client
void RPCBridgeOptionsEditor::createClient(GuiCommand* c)  {
  this->OptionsEditor::createClient(c);
  auto cl = rpc::client<http::HttpClient>("ecs03", 2600);
  (*cl)->userRequestHeaders.emplace_back("Dim-DNS",dns);
  (*cl)->userRequestHeaders.emplace_back("Dim-Server",process);
  client.handler = std::move(cl);
  CPP::IocSensor::instance().send(this,EDITOR_LOAD_OPTIONS);
}
