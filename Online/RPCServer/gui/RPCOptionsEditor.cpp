//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================

/// Framework include files
#include "RPC/GUI/RPCOptionsEditor.h"
#include "RPC/GUI/GuiException.h"
#include "RPC/GUI/GuiMsg.h"
#include "RPC/DataflowRPC.h"
#include "HTTP/HttpClient.h"

/// ROOT include files
#include "TGTextEntry.h"
#include "TGComboBox.h"

ClassImp(xmlrpc::RPCOptionsEditor)

using namespace std;
using namespace xmlrpc;

/// Standard initializing constructor
RPCOptionsEditor::RPCOptionsEditor(TGWindow* p, CPP::Interactor* g)
  : OptionsEditor(p,g)
{
}

/// GUI handler: Default destructor
RPCOptionsEditor::~RPCOptionsEditor()   {
}

/// Create communication client
void RPCOptionsEditor::createClient(GuiCommand* c)  {
  this->OptionsEditor::createClient(c);

  auto cl = rpc::client<http::HttpClient>(dns, 2600);
  (*cl)->userRequestHeaders.emplace_back("Dim-Server",process);
  //(*cl)->setDebug(3);
  client.handler = std::move(cl);
  CPP::IocSensor::instance().send(this,EDITOR_LOAD_OPTIONS);
}

/// Load options of a single client
void RPCOptionsEditor::loadOptions()   {
  if ( client )   {
    GuiMsg("RPC: Load Options: %s %s %s %s",client->name().c_str(), dns.c_str(), node.c_str(), process.c_str()).send(gui);
    properties = client.allProperties();
    GuiMsg("RPC: Load Options: Got %ld client properties",properties.size()).send(gui);
    for( size_t i=0; i<properties.size(); ++i )
      properties[i].flag = OPTION_ENABLED;
    if ( client->debug() > 2 )   {
      vector<string> clients  = client.clients();
      GuiMsg("RPC: Got %ld Clients",clients.size()).send(gui);
      if ( client->debug() > 3 )   {
	for( size_t i=0; i<clients.size(); ++i )
	  GuiMsg("RPC: %s..%s: Client %ld : %s",dns.c_str(),process.c_str(),i,clients[i].c_str()).send(gui);
	if ( client->debug() > 4 )   {
	  for( size_t i=0; i<properties.size(); ++i )
	    GuiMsg("RPC: %s..%s: Property %ld : %s.%s = %s",
		   dns.c_str(),process.c_str(),i,
		   properties[i].client.c_str(),
		   properties[i].name.c_str(),
		   properties[i].value.c_str()).send(gui);
	}
      }
    }
    optsBatch = -1;
    show_next_table_batch();
    return;
  }
  GuiException(kMBOk,kMBIconStop,"RPC: Load Options",
	       "No valid RPC client present [Internal Error].\nCannot load options.").send(gui);
}

/// Send single option to the target process
void RPCOptionsEditor::sendOption(const Row& row)    {
  string nam = row.label->GetText();
  for(auto& p : properties)  {
    string pnam = p.client+"."+p.name;
    if ( pnam == nam )   {
      GuiMsg("RPC: Sending option %s::%s::%s  %s.%s = %s [row:%ld]",
	     dns.c_str(), node.c_str(), process.c_str(),
	     p.client.c_str(), p.name.c_str(), p.value.c_str(), row.id)
	.send(gui);
      if ( client )  {
	try  {
	  client.setProperty(p.client,p.name,p.value);
	  p.flag |=  OPTION_SENT;
	  p.flag &= ~OPTION_CHANGED;
	  show_options_table_row(row, p);
	  GuiMsg("RPC: Successfully updated %s::%s::%s  %s.%s = %s [row:%ld]",
		 dns.c_str(), node.c_str(), process.c_str(),
		 p.client.c_str(), p.name.c_str(), p.value.c_str(), row.id)
	    .send(gui);
	  return;
	}
	catch(const std::exception& e)   {
	  GuiException(kMBOk,kMBIconStop,"RPC Exception",e.what()).send(gui);
	}
      }
      GuiException(kMBOk,kMBIconStop,"RPC: Send Option",
		   "No valid RPC client present for %s. How can this happen?",
		   nam.c_str()).send(gui);
      return;
    }
  }
  GuiException(kMBOk,kMBIconStop,"RPC: Send Option",
	       "Cannot locate property: %s = %s",
	       nam.c_str(), row.option->GetText()).send(gui);
}

/// Send all changed options to the client process
void RPCOptionsEditor::sendChanges()    {
  Properties updates;
  for(const auto& p : properties)  {
    if ( (p.flag&OPTION_CHANGED) == OPTION_CHANGED )
      updates.push_back(p);
  }
  if ( updates.size() > 0 )   {
    if ( client )  {
      if ( updates.size() == size_t(client.setProperties(updates)) )
	update_sent_options();
      return;
    }
    GuiException(kMBOk,kMBIconStop,"RPC: Send Changes",
		 "No valid RPC client present [Internal Error].\n"
		 "Cannot send changes.").send(gui);
    return;
  }
  GuiException(kMBOk,kMBIconAsterisk,"RPC: Send Changes",
	       "No updates pending. Nothing to be sent.").send(gui);
}
