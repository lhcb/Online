//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================

/// Framework include files
#include "RPC/GUI/XMLRPCGUI.h"
#include "CPP/IocSensor.h"
#include "RTL/rtl.h"
#include "RPC/GUI/GuiMsg.h"

#include "TApplication.h"
#include "TGFrame.h"
#include "TGLayout.h"
#include "TRint.h"
#include "TGMenu.h"

// C/C++ include files
#include <iostream>
#include <memory>
#include <thread>

extern "C" void   XInitThreads();

using namespace xmlrpc;
using namespace std;

TGMainFrame* global_main_window = 0;

namespace {
  size_t gui_print(void* arg,int lvl,const char* fmt,va_list& args) {
    if ( lvl >= long(arg) ) {
      size_t result;
      string format;
      switch(lvl) {
      case LIB_RTL_VERBOSE:
	format = "VERBOSE ";
	break;
      case LIB_RTL_DEBUG:
	format = "DEBUG   ";
	break;
      case LIB_RTL_INFO:
	format = "INFO    ";
	break;
      case LIB_RTL_WARNING:
	format = "WARNING ";
	break;
      case LIB_RTL_ERROR:
	format = "ERROR   ";
	break;
      case LIB_RTL_FATAL:
	format = "FATAL   ";
	break;
      case LIB_RTL_ALWAYS:
	format = "ALWAYS  ";
	break;
      default:
	break;
      }
      format += fmt;
      format += "\n";
      char buffer[1024];
      result = ::vsnprintf(buffer,sizeof(buffer), format.c_str(), args);
      buffer[sizeof(buffer)-1] = 0;
      GuiMsg("RTL: %s",buffer).send((CPP::Interactor*)arg);
      return result;
    }
    return 1;
  }
}

extern "C" void rpcproperties_gui()   {
  std::pair<int, char**> a(0,{0});
  cout << "Calling XInitThreads to support concurrency...." << endl;
  XInitThreads();
  gApplication = new TRint("PropertyRint", &a.first, a.second);
  //                                                       geometry: x * y
  TGMainFrame *fMain = new TGMainFrame(gClient->GetRoot(), 1000, 800);
  XMLRPCGUI* gui = new XMLRPCGUI(fMain);
  gui->createMenus(gui->menuBar());
  fMain->AddFrame(gui, new TGLayoutHints(kLHintsCenterX|kLHintsCenterY|kLHintsExpandX|kLHintsExpandY,1,1,1,1));
  global_main_window = fMain;

  ::lib_rtl_install_printer(gui_print,gui);

  fMain->MapSubwindows();
  fMain->Layout();
  //                        geometry: x * y
  fMain->Resize(fMain->GetDefaultWidth(),800);
  fMain->MapWindow();
  fMain->Connect("CloseWindow()", "TApplication", gApplication, "Terminate()");
  fMain->SetWindowName("XMLRPC Job Options Editor");
  fMain->SetIconName("JobOptions");
  fMain->SetIconPixmap("tmacro_t.xpm");
  new thread([] { CPP::IocSensor::instance().run(); });
  gApplication->Run();
}
