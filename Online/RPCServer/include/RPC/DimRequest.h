//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================
#ifndef RPC_DIMREQUEST_H
#define RPC_DIMREQUEST_H

/// Namespace for the dimrpc based implementation
namespace rpc {

  ///  XMLRPC request structure for DIM servers and clients
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.05.2017
   */
  class DimRequest  {
  public:
    /// Definition of the magic word
    enum { MAGIC = 0xFEEDBABE };
    enum { COMPRESSION_NONE = 0, COMPRESSION_DEFLATE, COMPRESSION_GZIP };
    ///Magic word
    unsigned int magic = MAGIC;
    /// Cookie: Process id
    unsigned int pid  = 0;
    /// Cookie: Host identifier (IP number) 
    unsigned int host = 0;
    /// Cookie: Local client message identifier
    int mid  = 0;
    /// Message size
    unsigned int size = 0;
    /// Size of encoding string
    unsigned int encoding;
    /// Start of payload
    char data[1];

  public:
    /// Thread-safe ID creation
    int newID();
  };
}       // End namespace xmlrpc
#endif  // RPC_DIMREQUEST_H
