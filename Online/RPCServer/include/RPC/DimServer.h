//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================
#ifndef RPC_DIM_SERVER_H
#define RPC_DIM_SERVER_H

// Framework include files
#include <CPP/Callback.h>

// C/C++ include files
#include <string>
#include <memory>
#include <mutex>
#include <map>

namespace xmlrpc  {  class Call;  }
namespace jsonrpc {  class Call;  }

/// Namespace for the dim based xmlrpc implementation
namespace rpc  {

  /// Forward declarations
  class DimRequest;
  
  ///  XMLRPC Server class
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.05.2017
   */
  class DimServer {
  public:
    /// Handler class specification
    class Handler;
    /// Definition of the Callback signature
    typedef void (*DimCall)(const DimRequest* request);
    /// Callback type declarations
    typedef dd4hep::Callback   Callback;

    /// Reference to the implementation interface
    std::unique_ptr<Handler> implementation;
    /// Server bind address
    std::string     server;
    /// Server bind port
    std::string     dns;

  public:
    /// Initializing constructor with user defined handler
    DimServer(std::unique_ptr<Handler>&& handler, const std::string& server, const std::string& dns_name="");
    /// Initializing constructor
    DimServer(const std::string& server, const std::string& mount, const std::string& dns="");
    /// Default destructor
    virtual ~DimServer();
    /// Modify debug flag
    int setDebug(int value);

    /// Start the xmlrpc service
    void start(bool detached = false);
    /// Stop the xmlrpc service
    void stop();

    /// Bind a callback to a function
    void define(const std::string& name, const xmlrpc::Call& call);
    /// Bind a callback to a function
    void define(const std::string& name, const jsonrpc::Call& call);
    /// Remove a callback from the xmlrpc interface
    void remove(const std::string& name);
    /// Remove all callbacks from the xmlrpc interface
    void remove_all();

    /// Bind default callbacks to all successfully handled function requests.
    void onHandled(const Callback& call);
    /// Bind default callbacks to all unhandled functions. Call signature must be DimCall.
    void onUnhandled(const Callback& call);
    /// Bind default callbacks to processing errors. Call signature must be DimCall.
    void onError(const Callback& call);
  };

  ///  XMLRPC Client class based on DIM
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.05.2017
   */
  class DimServer::Handler   {
  public:
    /// Standard constructor
    explicit Handler(const std::string& mount);
    /// Default destructor
    virtual ~Handler();
    /// Run the server's io_context loop.
    void run()  {}
    /// Open the server connection and enable client interactions
    void open(const std::string& server, const std::string& dns);
    /// Handle client request
    void handle(const DimRequest* request);
    /// Update client about result
    int update_client(const DimRequest* req, std::string&& rep);

  public:
    /// Definition of the XMLRPC callback map
    typedef std::map<std::string,xmlrpc::Call>  XmlCalls;
    typedef std::map<std::string,jsonrpc::Call> JsonCalls;

    /// Server mount point
    std::string                       mount;
    /// Object Lock to ensure we have no race conditions when editing the call-map
    std::mutex                        lock;
    /// The map of registered XML-RPC calls
    XmlCalls                          xml_calls;
    /// The map of registered JSON-RPC calls
    JsonCalls                         json_calls;
    /// Bind default callback to all successfully handled functions.
    std::vector<Callback>             onHandled;
    /// Bind default callback to all unhandled functions.
    std::vector<Callback>             onUnhandled;
    /// Bind default callback to processing errors.
    std::vector<Callback>             onError;
    /// DIM reqest identifier
    int                               request_id = -1;
    /// DIM response identifier
    int                               response_id = -1;
    /// DIM response buffer           
    std::unique_ptr<DimRequest>       response;
    /// DIM response buffer length
    size_t                            response_len = 0;
    /// Enable debugging if necessary
    int                               debug = 0;
  };
}       /* End  namespace xmlrpc      */
#endif  /* RPC_DIM_SERVER_H          */
