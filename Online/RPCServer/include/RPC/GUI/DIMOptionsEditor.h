//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================

#ifndef RPC_DIMOPTIONSEDITOR_H
#define RPC_DIMOPTIONSEDITOR_H

/// Framework include files
#include "RPC/GUI/OptionsEditor.h"

/// Namespace for the dimrpc based implementation
namespace xmlrpc  {

  /// Job options editor panel. 
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.05.2017
   */
  class DIMOptionsEditor : public OptionsEditor  {
  public:
    /// DIM response service identifier
    int         response_id = 0;
    static void dim_process_properties(void* tag, void* address, int* size);
    void        dim_load_properties(char* prop, int len);

  public:

    /// Standard Constructor
    DIMOptionsEditor(TGWindow* p, CPP::Interactor* gui);
    /// Default destructor
    virtual ~DIMOptionsEditor();

    /** RPC interaction routines using DIM commands/services  */
    /// Create/Reset communication client
    virtual void createClient(GuiCommand* c)  override;
    /// Load options of a single client
    virtual void loadOptions()  override;
    /// Send single option to the target process
    virtual void sendOption(const Row& row)  override;
    /// Send all changed options to the client process
    virtual void sendChanges()  override;

    /// ROOT class definition
    ClassDefOverride(DIMOptionsEditor,0);
  };
}       // End namespace xmlrpc
#endif  /* RPC_DIMOPTIONSEDITOR_H  */
