//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================
#ifndef RPC_GUIMSG_H
#define RPC_GUIMSG_H

/// Framework include files
#include "CPP/IocSensor.h"

/// C++ include files
#include <string>
#include <memory>


/// Namespace for the dimrpc based implementation
namespace xmlrpc  {

  /// The main instance of the XML-RPC Gui
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.05.2017
   */
  class GuiMsg : private std::unique_ptr<std::string> {
  public:
    /// Initializing constructor
    GuiMsg(const char* fmt, ...);
    
    /// Default constructor inhibited
    GuiMsg() = delete;
    /// Copy constructor inhibited
    GuiMsg(const GuiMsg& gui) = delete;
    /// Move constructor inhibited
    GuiMsg(GuiMsg&& gui) = delete;
    /// Default destructor
    ~GuiMsg() = default;
    
    /// Assignment operator inhibited
    GuiMsg& operator=(const GuiMsg& gui) = delete;
    /// Move assignment inhibited
    GuiMsg& operator=(GuiMsg&& gui) = default;
    /// Access the string behind
    std::string* release();
    /// Access the string behind
    std::string* get();
    /// Set the string to a new value
    GuiMsg& str(const std::string& value);
    /// Set the string to a new value
    GuiMsg& str(const char* fmt, ...);
    /// Access the string behind
    std::string& str();
    /// Access the string behind
    const std::string& str()  const;
    /// Send message to display target
    void send(CPP::Interactor* target)  const;
  };
}       // End namespace xmlrpc
#endif  /* RPC_GUIMSG_H         */
