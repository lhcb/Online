//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================
#ifndef RPC_MESSAGEBOX_H
#define RPC_MESSAGEBOX_H

/// Framework include files
#include "TGMsgBox.h"
#include "TGPicture.h"

/// C++ include files

/// Namespace for the dimrpc based implementation
namespace xmlrpc  {

  /// Message Box class. TMsgBox does not work for me....
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.05.2017
   */
  class MessageBox {
  public:
    MessageBox(const TGWindow *p = 0, const TGWindow *main = 0,
		  const char *title = 0, const char *msg = 0, const TGPicture *icon = 0,
		  Int_t buttons = kMBDismiss, Int_t *ret_code = 0,
		  UInt_t options = kVerticalFrame,
		  Int_t text_align = kTextCenterX | kTextCenterY);
    MessageBox(const TGWindow *p, const TGWindow *main,
		  const char *title, const char *msg, EMsgBoxIcon icon,
		  Int_t buttons = kMBDismiss, Int_t *ret_code = 0,
		  UInt_t options = kVerticalFrame,
		  Int_t text_align = kTextCenterX | kTextCenterY);
    ~MessageBox();
  };
}       // End namespace xmlrpc
#endif  /* RPC_MESSAGEBOX_H         */
