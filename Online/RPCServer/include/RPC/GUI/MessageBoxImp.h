//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================

/*************************************************************************
 * Copyright (C) 1995-2000, Rene Brun and Fons Rademakers.               *
 * All rights reserved.                                                  *
 *                                                                       *
 * For the licensing terms see $ROOTSYS/LICENSE.                         *
 * For the list of contributors see $ROOTSYS/README/CREDITS.             *
 *************************************************************************/

#ifndef ROOT_MessageBoxImp
#define ROOT_MessageBoxImp

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TMsgBox                                                              //
//                                                                      //
// A message dialog box.                                                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#include "TGMsgBox.h"

/// Namespace for the dimrpc based implementation
namespace xmlrpc  {

  class MessageBoxImp : public TGTransientFrame {

  protected:
    TGButton            *fYes, *fNo, *fOK, *fApply;   // buttons in dialog box
    TGButton            *fRetry, *fIgnore, *fCancel;  // buttons in dialog box
    TGButton            *fClose, *fYesAll, *fNoAll;   // buttons in dialog box
    TGButton            *fNewer, *fAppend, *fDismiss; // buttons in dialog box
    TGIcon              *fIcon;                       // icon
    TGHorizontalFrame   *fButtonFrame;                // frame containing buttons
    TGHorizontalFrame   *fIconFrame;                  // frame containing icon and text
    TGVerticalFrame     *fLabelFrame;                 // frame containing text
    TGLayoutHints       *fL1, *fL2, *fL3, *fL4, *fL5; // layout hints
    TList               *fMsgList;                    // text (list of TGLabels)
    Int_t               *fRetCode;                    // address to store return code
    bool                 fLock;

    void PMsgBox(const char *title, const char *msg, const TGPicture *icon,
		 Int_t buttons, Int_t *ret_code, Int_t text_align);

  private:
    MessageBoxImp(const MessageBoxImp&);              // not implemented
    MessageBoxImp& operator=(const MessageBoxImp&);   // not implemented

  public:
    MessageBoxImp(const TGWindow *p = 0, const TGWindow *main = 0,
		  const char *title = 0, const char *msg = 0, const TGPicture *icon = 0,
		  Int_t buttons = kMBDismiss, Int_t *ret_code = 0,
		  UInt_t options = kVerticalFrame,
		  Int_t text_align = kTextCenterX | kTextCenterY);
    MessageBoxImp(const TGWindow *p, const TGWindow *main,
		  const char *title, const char *msg, EMsgBoxIcon icon,
		  Int_t buttons = kMBDismiss, Int_t *ret_code = 0,
		  UInt_t options = kVerticalFrame,
		  Int_t text_align = kTextCenterX | kTextCenterY);
    virtual ~MessageBoxImp();

    virtual void Wait();
    virtual void DeleteWindow()  override;
    virtual void CloseWindow()  override;
    virtual Bool_t ProcessMessage(Long_t msg, Long_t parm1, Long_t parm2)  override;
    virtual Bool_t HandleKey(Event_t* event)  override;
    ClassDefOverride(MessageBoxImp,0)  // A message dialog box
      };
}
#endif
