//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================
#ifndef RPC_RPCOPTIONSEDITOR_H
#define RPC_RPCOPTIONSEDITOR_H

/// Framework include files
#include "RPC/GUI/OptionsEditor.h"

/// ROOT include files

// C++ ioncludes

/// Forward declarations

/// Namespace for the dimrpc based implementation
namespace xmlrpc  {

  /// Job options editor panel
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.05.2017
   */
  class RPCOptionsEditor : public OptionsEditor  {

  public:
    /// Standard Constructor
    RPCOptionsEditor(TGWindow* p, CPP::Interactor* gui);
    /// Default destructor
    virtual ~RPCOptionsEditor();

    /** RPC interaction routines  */
    /// Create communication client
    virtual void createClient(GuiCommand* c)  override;
    /// Load options of a single client
    virtual void loadOptions()  override;
    /// Send single option to the target process
    virtual void sendOption(const Row& row)  override;
    /// Send all changed options to the client process
    virtual void sendChanges()  override;

    /// ROOT class definition
    ClassDefOverride(RPCOptionsEditor,0);
  };
}       // End namespace xmlrpc
#endif  /* RPC_RPCOPTIONSEDITOR_H  */
