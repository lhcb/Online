//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================
#ifndef RPC_VICTIMNODEEDITOR_H
#define RPC_VICTIMNODEEDITOR_H

/// Framework include files
#include "RPC/GUI/OptionsEditor.h"

/// ROOT include files

// C/C++ include files

class TGComboBox;

/// Namespace for the dimrpc based implementation
namespace xmlrpc  {

  /// Editor of the readout clients at a given DNS node
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.05.2017
   */
  class VictimNodeEditor : public OptionsEditor  {
  protected:
    TGTextEntry*     optsFile = 0;
    TGTextButton*    addOpt = 0;
    TGPictureButton* addOptP = 0;
    Pixel_t          color_inhibit = 0;
  public:
    /// Standard initializing constructor
    VictimNodeEditor(TGWindow* parent, CPP::Interactor* gui);
    /// Default destructor
    virtual ~VictimNodeEditor();

    /// Initialize the display, setup the widgets
    virtual void init(int num_rows)  override;
    /// Initialize single row of the options table
    virtual Row init_options_table_row(TGGroupFrame* grp, int i)  override;
    /// Show single row in the options table
    virtual void show_options_table_row(const Row& row, const ObjectProperty& p)  override;
    /// Reset the content of the options editor
    virtual void reset_options_editor(const std::string& client, const std::string& opt_val)  override;

    /** Interactor interface      */
    /// Interactor interrupt handler callback
    virtual void handle(const CPP::Event& ev)  override;

    /// Create/Reset communication client
    virtual void createClient(GuiCommand* c)  override;
    /// Load options of a single client
    virtual void loadOptions()  override;
    /// Send single option to the target process
    virtual void sendOption(const Row& row)  override;
    /// Send options to victim processes
    virtual void sendChanges()  override;

    /// Save the not-disabled options to a file
    virtual void saveOptionsFile()  override;
    /// GUI handler: Handle option enable/disable
    virtual void click_handleOption();

    /// ROOT class definition
    ClassDefOverride(VictimNodeEditor,0);
  };
}       // End namespace xmlrpc
#endif  /* RPC_VICTIMNODEEDITOR_H  */
