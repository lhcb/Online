//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================
#ifndef RPC_VICTIMNODEEXPLORER_H
#define RPC_VICTIMNODEEXPLORER_H

/// Framework include files
#include "RPC/DataflowRPC.h"
#include "CPP/Interactor.h"

/// ROOT include files
#include "TGFrame.h"

// C/C++ include files
#include <memory>

class TGGroupFrame;
class TGTextButton;
class TGComboBox;
class TGLabel;

/// Namespace for the dimrpc based implementation
namespace xmlrpc  {

  /// Explorer of the readout clients at a given DNS node
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.05.2017
   */
  class VictimNodeExplorer : public TGCompositeFrame, public CPP::Interactor  {
  public:
    struct LineEntry  {
      bool              isValid = false;
      TGLabel*          label   = 0;
      TGComboBox*       input   = 0;
      TGTextButton*     reload  = 0;
      TGTextButton*     load    = 0;
      TGLabel*          fill    = 0;
    };
    TGCompositeFrame*   group   = 0;
    /// Line entry to set dim-dns node
    LineEntry dns;
    /// Line entry to select single client
    LineEntry node;

    Pixel_t   disabled, enabled;
    xmlrpc::dataflow::DomainInfo domain_client;
    CPP::Interactor*    gui = 0;

  public:
    enum IDS {
      DNS_ID_OFFSET        = 100,
      DNS_LABEL            = DNS_ID_OFFSET+1,
      DNS_INPUT            = DNS_ID_OFFSET+2,
      DNS_LOAD             = DNS_ID_OFFSET+3,
      DNS_RELOAD           = DNS_ID_OFFSET+4,
      DNS_FILL             = DNS_ID_OFFSET+5,
      NODE_ID_OFFSET       = 200,
      NODE_LABEL           = NODE_ID_OFFSET+1,
      NODE_INPUT           = NODE_ID_OFFSET+2,
      NODE_LOAD            = NODE_ID_OFFSET+3,
      NODE_RELOAD          = NODE_ID_OFFSET+4,
      NODE_FILL            = NODE_ID_OFFSET+5,
      LAST
    };
    /// Standard initializing constructor
    VictimNodeExplorer(TGWindow* parent, CPP::Interactor* gui);
    /// Default destructor
    virtual ~VictimNodeExplorer();

    /// Interactor interrupt handler callback
    virtual void handle(const CPP::Event& ev)  override;
    /// Deferred execution using wtc
    void exec_call(Int_t cmd);

    /// Load the DNS domains into the widget
    void loadDns();
    /// Callback to clear the node text field
    void clearNodeField();
    /// Load all DNS nodes (callback of "Load")
    void loadNodes();
    /// Load all DNS clients of a DNS (callback of "Load")
    void setVictimNode();
    /// Callback when the DNS name gets edited
    void dnsChanged(Int_t selected_id);
    /// Callback when the node name gets edited
    void nodeChanged(Int_t selected_id);
    /// ROOT class definition
    ClassDefOverride(VictimNodeExplorer,0);
  };
}       // End namespace xmlrpc
#endif  /* RPC_VICTIMNODEEXPLORER_H  */
