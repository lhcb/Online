//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================
#ifndef RPC_XMLRPCGUI_H
#define RPC_XMLRPCGUI_H

/// Framework include files
#include "CPP/Interactor.h"

/// ROOT include files
#include "TGFrame.h"
#include <string>

/// Forward declarations
class TGTab;
class TGMenuBar;
class TGPopupMenu;

/// Namespace for the dimrpc based implementation
namespace xmlrpc  {

  /// Forward declarations
  class OutputWindow;
  class OptionsEditor;
  class OptionsExplorer;
  class VictimNodeEditor;

  /// The main instance of the XML-RPC Gui
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.05.2017
   */
  class XMLRPCGUI : public TGCompositeFrame, public CPP::Interactor  {
  public:
    /// Reference to the menu bar
    TGMenuBar*    guiMenuBar = 0;
    TGPopupMenu*  menuFile   = 0;
    TGPopupMenu*  menuHelp   = 0;
    
    /// Reference to the tab pane
    TGTab*        tabs = 0;
    ///
    OptionsExplorer*  explorer = 0;
    ///
    OptionsEditor*    editor = 0;
    OptionsEditor*    dim_editor = 0;
    OptionsEditor*    bridge_editor = 0;
    /// Reference to the victim node editor
    VictimNodeEditor* victim_editor = 0;
    /// Reference to the output logger window.
    OutputWindow*     output = 0;

    enum ETestCommandIdentifiers {
      M_FILE_OPEN,
      M_FILE_SAVE,
      M_FILE_SAVEAS,
      M_FILE_CLOSE,
      M_FILE_PRINT,
      M_FILE_PRINTSETUP,
      M_FILE_EXIT,

      M_HELP_CONTENTS,
      M_HELP_SEARCH,
      M_HELP_ABOUT,
    };

  public:
    /// Default constructor inhibited
#ifdef G__ROOT
    XMLRPCGUI() = default;
#else
    XMLRPCGUI() = delete;
#endif
    /// Copy constructor inhibited
    XMLRPCGUI(const XMLRPCGUI& gui) = delete;
    /// Move constructor inhibited
    XMLRPCGUI(XMLRPCGUI&& gui) = delete;    
    /// Standard Initializing constructor
    XMLRPCGUI(TGFrame* parent);
    /// Default destructor
    virtual ~XMLRPCGUI();

    /// Assignment operator inhibited
    XMLRPCGUI& operator=(const XMLRPCGUI& gui) = delete;
    /// Move assignment inhibited
    XMLRPCGUI& operator=(XMLRPCGUI&& gui) = delete;

    /// Interactor interrupt handler callback
    virtual void handle(const CPP::Event& ev)  override;
    /// Create a menu bar and add it to the parent
    TGMenuBar* menuBar();
    /// Create the menues of the application
    void createMenus(TGMenuBar* bar);
    /// Menu callback handler
    void handleMenu(Int_t id);

    /// Show the about box of this application
    void aboutBox();
    /// Show a message box indicating this command is not implemented
    void notImplemented(const std::string& msg);
    /// The user try to close the main window, while a message dialog box is still open.
    void tryToClose();
    /// Terminate the App no need to use SendCloseMessage()
    void terminate();
    /// Close the window
    void CloseWindow();

    /// ROOT class definition
    ClassDefOverride(XMLRPCGUI,0);
  };
}       // End namespace xmlrpc
#endif  /* RPC_XMLRPCGUI_H  */
