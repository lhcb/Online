//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_RPC_HTTPXMLRPCHANDLER_H
#define ONLINE_RPC_HTTPXMLRPCHANDLER_H

// Framework include files
#include <HTTP/HttpCacheHandler.h>
#include <RPC/HttpRpcServer.h>
#include <RPC/XMLRPC.h>

/// Namespace for the http based xmlrpc implementation
namespace rpc  {

  class HttpXmlRpcHandler : public HttpServer::Handler, public http::HttpCacheHandler  {
    
  public:
    /// Definition of the RPC callback map
    typedef std::map<std::string, xmlrpc::Call> Calls;
    /// The map of registered XML-RPC calls
    Calls  calls;
    /// Compression parameters
    compression_params_t compression;

  public:
    /// Standard constructor
    using HttpServer::Handler::Handler;

    /// Stop the rpc service
    virtual void stop()  override;

    /// Remove a callback from the rpc interface
    virtual bool remove(const std::string& name)  override;

    /// Remove all callbacks from the xmlrpc interface
    virtual void remove_all()  override;

    /// Handle a request and produce a reply.
    virtual continue_action handle_request(const http::Request& req, http::Reply& rep) override;

    /// Handle a request and produce a reply.
    virtual void handle_server_request(const http::Request& req, http::Reply& rep);

    /// Handle a request and produce a reply.
    virtual void handle_bridge_request(const std::string& dns, 
				       const std::string& server,
				       const http::Request& req, http::Reply& rep);
  };
}       /* End  namespace xmlrpc         */
#endif  /* ONLINE_RPC_HTTPXMLRPCSERVER_H */
