//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include <RPC/DimServer.h>
#include <RTL/Compress.h>

#include <RPC/XMLRPC.h>
#include <RPC/JSONRPC.h>
#include <RPC/DimRequest.h>
#include <XML/Printout.h>
#include <unistd.h>
#ifdef HAVE_DIM
#include "dim/dis.h"
#else
#define DIM_SERVER
#include "fake_dim.h"
#endif

// C/C++ include files
#include <chrono>
#include <cstring>
#include <sstream>
#include <iostream>
#include <stdexcept>

using namespace rpc;
using namespace std;
typedef std::lock_guard<std::mutex>  Lock;

// Framework include files
#include "RPC/DimRequest.h"


namespace {
  void handle_request(void* tag, void* buffer, int* size)   {
    if ( tag && buffer && size )   {
      rpc::DimServer::Handler* server = *(rpc::DimServer::Handler**)tag;
      server->handle((rpc::DimRequest*)buffer);
    }
    return;
  }
  void feed_result(void* tag, void** buffer, int* size, int* first)   {
    rpc::DimServer::Handler* server = *(rpc::DimServer::Handler**)tag;
    rpc::DimRequest* msg = server->response.get();
    if ( (first && *first) || !msg )  {
      static int empty = 0;
      *buffer = &empty;
      *size = 0;
      return;
    }
    msg = server->response.get();
    *buffer = msg;
    *size =   msg->size+sizeof(rpc::DimRequest);
  }
}

DimServer::Handler::Handler(const string& m) : mount(m) {
}

DimServer::Handler::~Handler()  {
  if ( request_id  != -1 ) ::dis_remove_service(request_id);
  if ( response_id != -1 ) ::dis_remove_service(response_id);
}

void DimServer::Handler::open(const std::string& server,const std::string& dns)  {
  if ( request_id  != -1 ) ::dis_remove_service(request_id);
  if ( response_id != -1 ) ::dis_remove_service(response_id);
  if ( !dns.empty()      ) ::dis_set_dns_node(dns.c_str());

  string name = server + mount;
  request_id = ::dis_add_cmnd(name.c_str(),"C",handle_request,(long)this);
  if ( request_id <= 0 )   {
    throw runtime_error("Failed dis_add_cmnd("+name+")");
  }
  name = server + mount + "/Reply";
  response_id = ::dis_add_service(name.c_str(),"C",0,0,feed_result,(long)this);
  if ( response_id <= 0 )   {
    throw runtime_error("Failed dis_add_service("+name+")");
  }
}

namespace  {
  template <typename RESPONSE, typename CALL, typename CALLS>
  std::string handle_rpc(DimServer::Handler* srv, CALLS& calls, const char* data)    {
    using namespace xmlrpc;
    const void* args[] = { data, 0 };
    RESPONSE mr;
    try  {
      CALL   call(data);
      string method = call.method();
      const auto& callback = calls.find(method);
      if ( callback == calls.end() )   {
	mr = RESPONSE::makeFault(EOPNOTSUPP,"RPC call not supported.");
	// Unhandled call: call user callbacks
	for (const auto& cb : srv->onUnhandled )
	  cb.execute(args);
      }
      else  {
	const auto& c = (*callback).second;
	mr = c.execute(call);
	// Successfully handled call: call user callbacks
	for (const auto& cb : srv->onHandled )
	  cb.execute(args);
      }
    }
    catch(const std::exception& e)    {
      mr = RESPONSE::makeFault(EINVAL, e.what());
      // ERROR: call user callbacks
      for (const auto& cb : srv->onError )
	cb.execute(args);
    }
    catch(...)    {
      mr = RESPONSE::makeFault(EINVAL, "DIM-RPC: Unknown exception during RPC execution");
      // ERROR: call user callbacks
      for (const auto& c : srv->onError )
	c.execute(args);
    }
    return mr.str();
  }
}

/// Handle client request
void DimServer::Handler::handle(const DimRequest* req)    {
  string the_response;
  const char* data = req->data;
  if ( req->magic == DimRequest::MAGIC && req->size <= 0 )  {
    const void* args[] = { req, 0 };
    for (const auto& c : this->onError )
      c.execute(args);
    the_response = "Invalid DIM-RPC: Bad command length.";
  }
#if 0 // This should not be necessary!
  const DimRequest* rq = req;
  if ( req->magic == *(unsigned int*)"<method" )  {
    rq = 0;
    data = (const char*)req;
  }
#endif
  else if ( *(int*)data == *(int*)"<?xml version" )
    the_response = handle_rpc<xmlrpc::MethodResponse,xmlrpc::MethodCall>(this, xml_calls, data);
  else if ( data[0] == '{' && data[1] == '"' )
    the_response = handle_rpc<jsonrpc::MethodResponse,jsonrpc::MethodCall>(this, json_calls, data);
  this->update_client(req, std::move(the_response));
}

int DimServer::Handler::update_client(const DimRequest* req, string&& answer)    {
  char client_name[256];
  int num_client = 0;
  int    pid[] = { ::dis_get_client(client_name), 0, 0};
  string used;
  size_t len = answer.length()+sizeof(DimRequest);
  Lock   lck(lock);  {
    if ( response_len < len )  {
      response.reset((DimRequest*)::operator new(response_len=len));
    }
    response->pid      = req->pid;
    response->host     = req->host;
    response->mid      = req->mid;
    switch(req->encoding)   {
    case DimRequest::COMPRESSION_GZIP:
    case DimRequest::COMPRESSION_DEFLATE:  {
      vector<unsigned char> buff(Online::compress::compress("gzip deflate",
							    Online::compress::to_vector(answer),
							    used));
      if ( buff.size() < len && ::tolower(used[0]) == 'g' )   {
	response->encoding = DimRequest::COMPRESSION_GZIP;
	response->size     = buff.size();
	::memcpy(response->data, buff.data(), buff.size());
      }
      else if ( buff.size() < len && ::tolower(used[0]) == 'd' )   {
	response->encoding = DimRequest::COMPRESSION_DEFLATE;
	response->size     = buff.size();
	::memcpy(response->data, buff.data(), buff.size());
      }
      else  {
	response->size     = answer.length();
	response->encoding = DimRequest::COMPRESSION_NONE;
	::memcpy(response->data, answer.c_str(), answer.length());
	break;
      }
      break;
    }
    case DimRequest::COMPRESSION_NONE:
    default:
      response->size     = answer.length();
      response->encoding = DimRequest::COMPRESSION_NONE;
      ::memcpy(response->data, answer.c_str(),answer.length());
      break;
    }
    num_client = ::dis_selective_update_service(response_id,pid);
  }
  if ( debug )   {
    cout << "+++++ HTTP Request   pid:" << std::hex << req->pid
         << " host: " << req->host
         << " id: "   << req->mid << std::dec
         << " Size:"  << int(req->size) << endl;
    if ( debug > 1 ) cout << " Data:" << req->data << endl;
    cout << "*****" << endl;
    cout << "+++++ HTTP Response No.Clients: " << num_client
	 << " (" << client_name << ") "
	 << " Size:" << int(response->size) << endl;
    if ( debug > 1 ) cout << " Data:" << response->data << "|data-end|" << endl;
  }
  return num_client;
}

/// Initializing constructor
DimServer::DimServer(const string& srv_name, const string& mount, const string& dns_name)
  : implementation(new Handler(mount)), server(srv_name), dns(dns_name)
{
}

/// Initializing constructor with user defined handler
DimServer::DimServer(unique_ptr<Handler>&& handler, const string& srv_name, const string& dns_name)
  : implementation(std::move(handler)), server(srv_name), dns(dns_name)
{
}

/// Default destructor
DimServer::~DimServer() {
  this->implementation.reset();
}

/// Modify debug flag
int DimServer::setDebug(int value)  {
  int tmp = this->implementation->debug;
  this->implementation->debug = value;
  return tmp;
}

/// Start the xmlrpc service
void DimServer::start(bool detached)   {
  this->implementation->open(server, dns);
  if ( !detached )  {
    bool run = true;
    ::dis_start_serving(server.c_str());
    while(run) 
      ::usleep(100000);
  }
}

/// Stop the xmlrpc service
void DimServer::stop()   {
  Lock lock(this->implementation->lock);
  this->implementation->json_calls.clear();
  this->implementation->xml_calls.clear();
}

/// Bind a callback to a function
void DimServer::define(const string& name, const xmlrpc::Call& call)   {
  Lock lock(this->implementation->lock);
  auto i = this->implementation->xml_calls.find(name);
  if ( i == this->implementation->xml_calls.end() )  {
    this->implementation->xml_calls.insert(make_pair(name,call));
    return;
  }
  throw runtime_error("The RPC call "+name+" is already registered to the RPC interface.");
}

/// Bind a callback to a function
void DimServer::define(const string& name, const jsonrpc::Call& call)   {
  Lock lock(this->implementation->lock);
  auto i = this->implementation->json_calls.find(name);
  if ( i == this->implementation->json_calls.end() )  {
    this->implementation->json_calls.insert(make_pair(name,call));
    return;
  }
  throw runtime_error("The RPC call "+name+" is already registered to the RPC interface.");
}

/// Remove a callback from the xmlrpc interface
void DimServer::remove(const string& name)   {
  Lock lock(this->implementation->lock);
  bool found = false;
  auto i = this->implementation->xml_calls.find(name);
  if ( i == this->implementation->xml_calls.end() )  {
    this->implementation->xml_calls.erase(i);
    found = true;
  }
  auto j = this->implementation->json_calls.find(name);
  if ( j == this->implementation->json_calls.end() )  {
    this->implementation->json_calls.erase(j);
    found = true;
  }
  if ( found )   {
    return;
  }
  throw runtime_error("The RPC call "+name+" is not registered to the RPC interface.");
}

/// Remove all callbacks from the xmlrpc interface
void DimServer::remove_all()   {
  Lock lock(this->implementation->lock);
  this->implementation->xml_calls.clear();
  this->implementation->json_calls.clear();
}

/// Bind default callback to all unhandled functions.
void DimServer::onUnhandled(const Callback& call)   {
  Lock lock(this->implementation->lock);
  this->implementation->onUnhandled.push_back(call);
}


/// Bind default callback to all successfully handled function requests.
void DimServer::onHandled(const Callback& call)   {
  Lock lock(this->implementation->lock);
  this->implementation->onHandled.push_back(call);
}

/// Bind default callback to processing errors.
void DimServer::onError(const Callback& call)   {
  Lock lock(this->implementation->lock);
  this->implementation->onError.push_back(call);
}

