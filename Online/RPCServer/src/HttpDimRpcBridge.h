//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================
#include <RTL/Compress.h>
#include <RPC/DimRequest.h>
#include <RPC/HttpRpcHandler.h>
#include <dim/dic.h>
#include <mutex>
#include <condition_variable>

namespace rpc {

  class HttpDimBridgeHandler : public rpc::HttpRpcHandler  {
  public:
    /// Lock for the conditions variable
    std::mutex                 lock;
    /// Conditions variable to oprerly sequence the threads
    std::condition_variable    condition;
    /// The method response of the server
    std::vector<unsigned char> response;
    /// Encoding method, if a compressed blob is sent.
    unsigned int               encoding;
    /// Flag: set when data are ready
    int                        ready       = 0;
    /// Current message identifier to properly select the correct answer
    int                        message_id  = 0;
    /// DIM response service identifier
    int                        response_id = 0;
    /// Timeout waiting for messages
    int                        timeout = 5000;
    /// Debug flag
    int                        debug = 0;
    /// Property: dim server name
    std::string                server;

    /// Standard constructor
    HttpDimBridgeHandler(const std::string& srv, const std::string& uri, int dbg = false);
    /// Standard destructor
    virtual ~HttpDimBridgeHandler();
    /// Enable debugging
    virtual void setDebug(int val)  override  { this->debug = val; }
    /// Execute RPC call
    virtual continue_action handle_request(const http::Request& req, http::Reply& rep)  override;
  };
}

namespace   {
  static rpc::DimRequest proc;

  void rpc_callback(void* tag, int* code)  {
    if ( tag && code )  {}
  }

  void feed_result(void* tag, void* buffer, int* size)   {
    if ( tag && buffer && size && *size )   {
      rpc::HttpDimBridgeHandler* client = *(rpc::HttpDimBridgeHandler**)tag;
      rpc::DimRequest* msg = (rpc::DimRequest*)buffer;
      if ( msg->pid == proc.pid )  {
        if ( msg->host == proc.host )  {
          if ( msg->mid == client->message_id )  {
            unique_lock<mutex> lk(client->lock);
            if ( !client->ready )  {
	      client->encoding = rpc::DimRequest::COMPRESSION_NONE;
              try  {
		client->response.clear();
		client->encoding = msg->encoding;
		client->response.resize(msg->size);
		::memcpy(client->response.data(), msg->data, msg->size);
              }
              catch(const std::exception& e)   {
                boost::system::error_code errcode(errno,boost::system::system_category());
                client->response = http::to_vector(MethodResponse::makeFault(errcode.value(),e.what()).str());
              }
              catch( ... )   {
                boost::system::error_code errcode(errno,boost::system::system_category());
                client->response = http::to_vector(MethodResponse::makeFault(errcode.value(),errcode.message()).str());
              }
              client->ready      = 1;
              client->message_id = 0;
              client->condition.notify_one();
            }
          }
        }
      }
    }
  }
}

/// Standard constructor
inline rpc::HttpDimBridgeHandler::HttpDimBridgeHandler(const std::string& server_name,const std::string& uri_val, int dbg)
: debug(dbg)
{
  /// Initialize structure
  if ( !proc.pid )  {
    char host[128];
    ::gethostname(host, sizeof(host));
    struct hostent *he = ::gethostbyname(host);
    if ( he )  {
      proc.pid  = ::getpid();
      proc.host = *(int*)he->h_addr;
      ++proc.mid;
    }
  }
  this->server = server_name;
  this->uri = uri_val;
  std::string reply_name = server + this->uri + "/Reply";
  if ( response_id != -1 ) dic_release_service(response_id);
  this->response_id = ::dic_info_service(reply_name.c_str(),MONITORED,timeout,0,0,feed_result,(long)this,0,0);
  if ( this->response_id <= 0 )   {
    throw std::runtime_error("Failed dic_info_service("+reply_name+")");
  }
  printout(debug>2 ? ALWAYS : DEBUG,"DimClient","Subscribed to response service: %s",reply_name.c_str());
}

/// Standard destructor
inline rpc::HttpDimBridgeHandler::~HttpDimBridgeHandler()  {
  if ( this->response_id != -1 ) ::dic_release_service(this->response_id);
  this->response_id = -1;
}

/// Execute RPC call
inline rpc::HttpDimBridgeHandler::continue_action
rpc::HttpDimBridgeHandler::handle_request(const http::Request& req, http::Reply& rep)   {
  using namespace std::chrono;
  std::stringstream str;
  try  {
    std::size_t len = req.content.size()+sizeof(DimRequest)+1;
    std::unique_ptr<DimRequest> m((DimRequest*)::operator new(len));
    m->magic = DimRequest::MAGIC;
    m->pid   = proc.pid;
    m->host  = proc.host;
    m->mid   = proc.newID();
    m->encoding = DimRequest::COMPRESSION_NONE;
    m->size  = req.content.size();
    ::memcpy(m->data, &req.content[0], req.content.size());
    m->data[m->size] = 0;
    std::string accepted_encoding;
    for(const auto& h : req.headers)  {
      if ( h.name == "Accept-Encoding" )   {
	accepted_encoding = h.value;
	if ( accepted_encoding.find("gzip") != string::npos )
	  m->encoding = DimRequest::COMPRESSION_GZIP;
	else if ( accepted_encoding.find("deflate") != string::npos )
	  m->encoding = DimRequest::COMPRESSION_DEFLATE;
	break;
      }
    }
    {
      if ( debug>3 )  {
        printout(INFO,"RPCClient","Sending request: %s",m->data);
      }
      else if ( debug>1 )  {
	printout(INFO,"RPCClient","Handling response [Length:%ld bytes]",::strlen(m->data));
      }  {
	std::string req_name = this->server + this->uri;
        std::unique_lock<mutex> lck(lock);	
        ready = 0;
        message_id = m->mid;
        int sc = ::dic_cmnd_callback(req_name.c_str(),m.get(),len,rpc_callback,(long)this);
        if ( sc != 1 )   {
          throw std::runtime_error("Failed dic_cmnd_callback("+req_name+")");
        }
        if ( condition.wait_for(lck,chrono::milliseconds(timeout)) == cv_status::timeout ) {
          if ( !ready )  {
            message_id = 0;
            throw std::runtime_error("RPC: Command timeout "+req_name);
          }
        }
      }
      std::vector<unsigned char> dcom_buffer;
      message_id = 0;
      switch(encoding)   {
      case DimRequest::COMPRESSION_DEFLATE:
	rep.content = std::move(response);
	rep.userHeaders.emplace_back(http::HttpHeader("Content-Encoding","deflate"));
	dcom_buffer = Online::compress::decompress("deflate", rep.content);
	break;
      case DimRequest::COMPRESSION_GZIP:
	rep.content = std::move(response);
	rep.userHeaders.emplace_back(http::HttpHeader("Content-Encoding","gzip"));
	dcom_buffer = Online::compress::decompress("gzip", rep.content);
	break;
      case DimRequest::COMPRESSION_NONE:
      default:
	if ( !accepted_encoding.empty() )   {
	  rep.content = std::move(response);
	}
	else  {
	  std::string used;
	  rep.content = Online::compress::compress(accepted_encoding, response, used);
	  if ( !used.empty() )  {
	    rep.userHeaders.emplace_back(http::HttpHeader("Content-Encoding",used));
	  }
	}
	break;
      }
      if ( !dcom_buffer.empty() )    {
	std::cout << "RPCClient: response[]." << std::endl;
      }

      response.clear();
      if ( debug > 3 )  {
        printout(INFO,"RPCClient","Handling response: %s", &rep.content[0]);
      }
      else if ( debug>1 )  {
	printout(INFO,"RPCClient","Handling response [Length:%ld bytes]", rep.content.size());
      }
    }
    return continue_action::write;
  }
  catch(const std::exception& e)   {
    str << e.what();
  }
  catch( ... )   {
    std::error_code errcode(errno, std::system_category());
    str << "RPC fault [" << errcode.value() << "] (UNKOWN Exception): " << errcode.message();
  }
  throw std::runtime_error(str.str());
}
