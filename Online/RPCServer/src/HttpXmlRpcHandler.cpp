//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================

// Framework inclde files
#include <RPC/HttpXmlRpcHandler.h>
#include <RPC/HttpRpcHandler.h>
#include <RPC/XMLRPC.h>
#include <XML/Printout.h>

// C/C++ include files
#include <mutex>
#include <memory>
#include <cstring>
#include <iostream>
#include <stdexcept>

using namespace dd4hep;
using namespace std;
using namespace boost;
typedef std::lock_guard<std::mutex>  Lock;

/// Bind a callback to a function
template <>
bool rpc::HttpServer::Handler::define(const std::string& name, const xmlrpc::Call& call)  {
  auto* ptr = (rpc::HttpXmlRpcHandler*)this;
  auto i = ptr->calls.find(name);
  if ( i == ptr->calls.end() )  {
    ptr->calls.insert(make_pair(name,call));
  }
  return (i == ptr->calls.end());
}

/// Remove a callback from the rpc interface
bool rpc::HttpXmlRpcHandler::remove(const string& name)   {
  auto i = this->calls.find(name);
  if ( i != this->calls.end() )  {
    this->calls.erase(i);
  }
  return (i != this->calls.end());
}

/// Remove a callback from the rpc interface
void rpc::HttpXmlRpcHandler::remove_all()   {
  this->calls.clear();
}

/// Stop the rpc service
void rpc::HttpXmlRpcHandler::stop()   {
  this->calls.clear();
}

/// Handle a request and produce a reply.
rpc::HttpServer::Handler::continue_action
rpc::HttpXmlRpcHandler::handle_request(const http::Request& req, http::Reply& rep)   {
  if ( req.content_length > req.content.size() )  {
    return read;
  }
  else if ( rep.bytes_total > 0 && rep.bytes_total == rep.bytes_sent )   {
    return close_read;
  }
  const void* args[] = { &req, 0 };
  try  {
    if ( !(req.method == "POST" && (server_uri.empty() || req.uri == server_uri)) )  {
      rep = http::Reply::stock_reply(http::Reply::bad_request);
      // ERROR: call user callbacks
      for (const auto& c : onError )
        c.execute(args);
      goto Done;
    }
    string server, dns;
    for(const auto& h : req.headers)  {
      if ( h.name == "Dim-Server" )  {
	server = h.value;
	break;
      }
    }
    for(const auto& h : req.headers)  {
      if ( h.name == "Dim-DNS" )  {
	dns = h.value;
	break;
      }
    }
    rep = http::Reply::stock_reply(http::Reply::ok);
    switch(mode)  {
    case rpc::HttpServer::SERVER:
      handle_server_request(req, rep);
      break;
    case rpc::HttpServer::BRIDGE:
      handle_bridge_request(dns, server, req, rep);
      break;
    default:
      server.empty() ? 
	handle_server_request(req, rep) :
	handle_bridge_request(dns, server, req, rep);
      break;
    }
  }
  catch(const std::exception& e)    {
    xmlrpc::MethodResponse response = xmlrpc::MethodResponse::makeFault(EINVAL,e.what());
    rep.content = http::to_vector(response.str());
    // ERROR: call user callbacks
    for (const auto& c : onError )
      c.execute(args);
  }
  catch(...)    {
    xmlrpc::MethodResponse response = xmlrpc::MethodResponse::makeFault(EINVAL,"Unknown exception during RPC execution");
    rep.content = http::to_vector(response.str());
    // ERROR: call user callbacks
    for (const auto& c : onError )
      c.execute(args);
  }
 Done:
  ///
  ///  Build the headers, which must be of the form:
  ///    HTTP/1.1 200 OK
  ///    Connection: close
  ///    Content-Length: 426
  ///    Content-Type: text/xml
  ///    Date: Fri, 17 Jul 1998 19:55:02 GMT
  ///    Server: UserLand Frontier/5.1.2-WinNT
  rep.headers.clear();
  rep.headers.reserve(5);
  rep.headers.emplace_back(http::HttpHeader("Content-Type",   "text/xml"));
  rep.headers.emplace_back(http::HttpHeader("Content-Length", std::to_string(rep.content.size())));
  rep.headers.emplace_back(http::HttpHeader("Connection",     "close"));
  rep.headers.emplace_back(http::HttpHeader("Date",           get_date_string()));
  rep.headers.emplace_back(http::HttpHeader("Server",         "LHCbDataflow XMLRPC"));
  if ( debug > 0 )  {
    cout << "+++++ Handled HTTP Request:" << req.method
	 << " uri:"  << req.uri
	 << " from:" << req.remote_host_name() << ":" << req.port()
	 << " ["     << req.content.size()     << " bytes]" << endl;
  }
  if ( debug > 2 )   {
    cout << "+++++ HTTP Request:" << req.method << endl
         << "         uri:" << req.uri << endl
         << "         vsn:" << req.version_major << "." << req.version_minor << endl
         << "     Headers:" << endl;
    for(const auto& h : req.headers )  
      cout << h.name << " : " << h.value << endl;
    if ( debug > 3 ) cout << "        Data:" << (char*)&req.content[0] << endl;
    cout << "*****" << endl;
    cout << "+++++ HTTP Reply:  len:" << rep.content.size() << endl;
    for(const auto& h : rep.headers )  
      cout << h.name << " : " << h.value << endl;
    if ( debug > 3 ) cout << "        Data:" << (char*)&rep.content[0] << "|data-end|" << endl;
  }
  return write;
}

/// Handle a request and produce a reply.
void rpc::HttpXmlRpcHandler::handle_server_request(const http::Request& req, http::Reply& rep)   {
  const void* args[] = { &req, 0 };
  xmlrpc::MethodCall call(req.content.data(), req.content.size());
  std::string call_key = call.str();

  /// First check if the requested entry is already in the cache:
  if ( this->use_cache(this->lock) )    {
    auto [use,reply] = this->check_cache(call_key, req);
    if ( use )   {
      rep = std::move(reply);
      return;
    }
  }

  std::string method = call.method();
  const auto& callback = calls.find(method);
  rep = http::Reply::stock_reply(http::Reply::ok);
  if ( callback == calls.end() )   {
    xmlrpc::MethodResponse response = xmlrpc::MethodResponse::makeFault(EOPNOTSUPP);
    rep.content = http::to_vector(response.str());
    // Unhandled call: call user callbacks
    for (const auto& c : onUnhandled )
      c.execute(args);
    return;
  }
  string encoding;
  const xmlrpc::Call& c = (*callback).second;
  xmlrpc::MethodResponse response = c.execute(call);
  rep.content = http::to_vector(response.str());
  
  if ( this->compression.enable )   {
    this->compress_reply(req, rep, encoding);
  }
  /// Add entry to cache if enabled
  if ( this->reply_cache && !call_key.empty() && rep.status == http::HttpReply::ok )    {
    rep = this->add_cache(call_key, encoding, std::move(rep));
  }
  // Successfully handled call: call user callbacks
  for (const auto& cb : onHandled )
    cb.execute(args);
}

using xmlrpc::MethodResponse;
#define HANDLER HttpXmlRpcHandler

/// Handle the bridge requests:
#include "HttpBridgeRequest.h"
