//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include <RPC/RpcPropertyUI.h>
#include <stdexcept>
#include <RPC/XMLRPC.h>
#include <RPC/JSONRPC.h>
#include <RPC/DimServer.h>
#include <RPC/HttpRpcServer.h>
#include <RPC/HttpXmlRpcHandler.h>
#include <RPC/HttpJsonRpcHandler.h>

using namespace std;
using args_t = const vector<string>;

namespace rpc   {

  unique_ptr<DimServer> create_srv(unique_ptr<DimServer::Handler>&& handler, args_t& args)   {
    return make_unique<DimServer>(std::move(handler), args[0], args[1]);
  }

  unique_ptr<HttpServer> create_srv(unique_ptr<HttpServer::Handler>&& handler, args_t& args)   {
    return ( args.size() > 3 )
      ? make_unique<HttpServer>(std::move(handler), args[0], args[1], args[2], args[3])
      : make_unique<HttpServer>(std::move(handler), args[0], args[1], args[2]);
  }

  template <typename SERVER, typename CALL, typename HANDLER> inline
  unique_ptr<RpcPropertyUI<SERVER,CALL> > create_rpc_property_ui(UI* ui, unique_ptr<HANDLER>&& handler, args_t& args)    {
    auto rpc_ui    = make_unique<RpcPropertyUI<SERVER,CALL> >();
    rpc_ui->call   = make_unique<CALL>(ui);
    rpc_ui->server = create_srv(std::move(handler), args);
    rpc_ui->start_server();
    return rpc_ui;
  }
}

extern "C" rpc::RpcUI* create_rpc_ui(const string& type, rpc::UI* ui, args_t& args)    {
  using namespace rpc;
  if ( ::strcasecmp(type.c_str(),"dimxmlrpc") == 0 )  {
    auto handler = make_unique<DimServer::Handler>(args.size() > 2 ? args[2] : "/RPC2");
    return create_rpc_property_ui<DimServer,xmlrpc::Call>(ui, std::move(handler), args).release();
  }
  if ( ::strcasecmp(type.c_str(),"dimjsonrpc") == 0 )   {
    auto handler = make_unique<DimServer::Handler>(args.size() > 2 ? args[2] : "/JSONRPC");
    return create_rpc_property_ui<DimServer,jsonrpc::Call>(ui, std::move(handler), args).release();
  }
  if ( ::strcasecmp(type.c_str(),"httpxmlrpc") == 0 )   {
    auto handler = make_unique<HttpXmlRpcHandler>();
    return create_rpc_property_ui<HttpServer,xmlrpc::Call>(ui, std::move(handler), args).release();
  }
  if ( ::strcasecmp(type.c_str(),"httpjsonrpc") == 0 )   {
    auto handler = make_unique<HttpJsonRpcHandler>();
    return create_rpc_property_ui<HttpServer,jsonrpc::Call>(ui, std::move(handler), args).release();
  }
  return nullptr;
}
