//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : XML
//
//  Author     : Markus Frank
//==========================================================================

#include "Objects.h"
#include <RPC/JSONRPC.h>

namespace test_rpc   {

  void to_json(jsonrpc::json_h& j, const Object& o) {
    j = jsonrpc::json_h{{"a", o.a}, {"b", o.b}};
  }

  void from_json(const jsonrpc::json_h& j, Object& o) {
    j.at("a").get_to(o.a);
    j.at("b").get_to(o.b);
  }
}

//JSONRPC_IMPLEMENT_OBJECT_MARSCHALLING(struct,test_rpc::Object)
JSONRPC_IMPLEMENT_OBJECT_CONTAINER(struct,test_rpc::Object)
