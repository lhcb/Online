//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : XML
//
//  Author     : Markus Frank
//==========================================================================

#include "Objects.h"
#include "Score.h"
#include <stdexcept>
#include <unistd.h>

using namespace test_rpc;
using namespace std;

void Callable::call_unregistered_method()   {
  logger("Calling") << "[" << s_test_count << "] " << " \"void call_unregistered_method()\" ... SHOULD NEVER BE CALLED!" << endl;
}

void Callable::call_exception_method()   {
  logger("Calling") << "[" << s_test_count << "] " << " \"void call_exception_method()\" ... testing exception mechanism" << endl;
  throw runtime_error("This is some strange exception, which happened during processing....");
}

void Callable::call_void()  {
  logger("Calling") << "[" << s_test_count << "] " << " \"void call_void()\" ... " << endl;
  ++s_test_count;
}

void Callable::call_void_const()  const {
  logger("Calling") << "[" << s_test_count << "] " << " \"void call_void_const()  const\" ... " << endl;
  ++s_test_count;
}

void Callable::call_void_int(int a0)   {
  logger("Calling") << "[" << s_test_count << "] " << " \"void call_void_int(int:" << a0 << ")\" ... " << endl;
  ++s_test_count;
}

/// Try to run with overload
void Callable::call_void_int(int a0, int a1)   {
  logger("Calling") << "[" << s_test_count << "] " << " \"void call_void_int(int:" << a0 << ", " << a1 << ")\" ... " << endl;
  ++s_test_count;
}

void Callable::call_void_string(const std::string& a0)   {
  logger("Calling") << "[" << s_test_count << "] " << " \"void call_void_string(string:" << a0 << ")\" ... " << endl;
  ++s_test_count;
}

int Callable::call_int_int_const(int a0)  const {
  logger("Calling") << "[" << s_test_count << "] " << " \"int call_int_int_const(int:" << a0 << ")  const\" = " << a0 << endl;
  ++s_test_count;
  return a0;
}

int Callable::call_int_int_double_string(int a0, double a1, const string& a2)  {
  logger("Calling") << "[" << s_test_count << "] " << " \"int call_int_int_double_string(int:" << a0 
       << ", double:" << a1 
       << ", string:" << a2 << ")  const\" ... " << endl;
  ++s_test_count;
  return 87654321;
}

Object Callable::call_Object_Object(const Object& b)  {
  logger("Calling") << "[" << s_test_count << "] " << " \"Object call_Object_Object(int: arg.a=" << b.a 
       << ", int: arg.b=" << b.b << ")  const\" ... " << endl;
  ++s_test_count;
  return {b.a*100,b.b*100};
}

Object Callable::call_Object_vector(const vector<Object>& vb)    {
  logger("Calling") << "[" << s_test_count << "] " << " \"Object call_Object_vector(size:" << vb.size()
		    << ")  const\" ... " << endl;
  ++s_test_count;
  return {1,2};
}

Object Callable::call_Object_list(const list<Object>& vb)    {
  logger("Calling") << "[" << s_test_count << "] " << " \"Object call_Object_list(size:" << vb.size()
		    << ")  const\" ... " << endl;
  ++s_test_count;
  return {3,4};
}

Object Callable::call_Object_set(const set<Object>& vb)    {
  logger("Calling") << "[" << s_test_count << "] " << " \"Object call_Object_set(size:" << vb.size()
		    << ")  const\" ... " << endl;
  ++s_test_count;
  return {5,6};
}

/// Exit process
int Callable::call_exit()   {
  logger("Calling") << "[" << s_test_count << "] " << " \"Object call_exit() ..." << endl;
  ++s_test_count;
  ::exit(0);
  return 0;
}

/// Default destructor
PropertyContainer::~PropertyContainer()   {
  for(auto p : prop) delete p;
  prop.clear();
}

/// Access the hosted client services
PropertyContainer::Clients PropertyContainer::clients()  const   {
  set<string> cl;
  for(auto p : prop) cl.insert(p->client);
  Clients ret(cl.size());
  copy(cl.begin(), cl.end(), ret.begin());
  return ret;
}

/// Access all properties of an object
PropertyContainer::Properties PropertyContainer::allProperties()  const   {
  Properties res;
  for(auto p : prop) res.push_back(*p);
  return res;
}

/// Access all properties with the same name from all clients
PropertyContainer::Properties PropertyContainer::namedProperties(const string& name)  const   {
  Properties res;
  for(auto p : prop) { if ( p->name == name ) res.push_back(*p); }
  return res;
}

/// Access all properties of one remote client (service, ect.)
PropertyContainer::Properties PropertyContainer::clientProperties(const string& client)  const   {
  Properties res;
  for(auto p : prop) { if ( p->client == client ) res.push_back(*p); }
  return res;
}

/// Access a single property of an object
rpc::ObjectProperty PropertyContainer::property(const string& client,
						   const string& name)  const   {
  for(auto p : prop) {
    if ( p->client == client && p->name == name )
      return *p;
  }
  throw runtime_error("property: Unknown property: "+client+"."+name);
}

/// Modify a property
void PropertyContainer::setProperty(const string& client,
				    const string& name,
				    const string& value)  const   {
  for(auto p : prop) {
    if ( p->client == client && p->name == name )  {
      p->value = value;
      return;
    }
  }
  throw runtime_error("setProperty: Unknown property: "+client+"."+name);
}

/// Modify a property
void PropertyContainer::setPropertyObject(const rpc::ObjectProperty& property) const  {
  setProperty(property.client,property.name,property.value);
}

/// Exit process
int PropertyContainer::call_exit()   {
  logger("Calling") << "[" << s_test_count << "] " << " \"Object call_exit() ..." << endl;
  ++s_test_count;
  logger("TEST>  Counts: ") << "Total score: " << Online::rpc::test::_TestCount::instance().counter << std::endl;
  ::exit(0);
  return 0;
}
