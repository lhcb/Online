//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : XML
//
//  Author     : Markus Frank
//==========================================================================
#ifndef RPC_TEST_OBJECTS_H
#define RPC_TEST_OBJECTS_H

// Framework include files
#include <CPP/ObjectProperty.h>
#include <XML/Printout.h>

// C/C++ include files
#include <iostream>
#include <vector>
#include <string>
#include <list>
#include <set>

namespace test_rpc {


  class Object  {
  public:
    int a = 1;
    int b = 2;
    Object() = default;
    Object(Object&&) = default;
    Object(const Object&) = default;
    Object& operator=(const Object&) = default;
    Object(int aa, int bb);
    bool operator<(const Object& c) const {
      if ( a < c.a ) return true;
      if ( b < c.b ) return true;
      return false;
    }
  };
  inline Object::Object(int aa, int bb) : a(aa), b (bb) {
  }

  class Callable  {
#ifdef CallableHandler
  public:
    CallableHandler handler;
#endif
  public:
    void   call_unregistered_method();
    void   call_exception_method();
    void   call_void();
    void   call_void_const()  const;
    void   call_void_int(int a0);
    void   call_void_int(int a0, int a1);
    int    call_int_int_const(int a0)  const;
    void   call_void_string(const std::string& a0);
    int    call_int_int_double_string(int a0, double a1, const std::string& a2);
    Object call_Object_Object(const Object& b);
    Object call_Object_set(const std::set<Object>& vb);
    Object call_Object_list(const std::list<Object>& vb);
    Object call_Object_vector(const std::vector<Object>& vb);
    /// Exit process
    int    call_exit();
  };

  class PropertyContainer  {
  public:
    /// Definition of the clients collection type
    typedef std::vector<std::string>    Clients;
    /// Definition of the property collection type
    typedef std::vector<rpc::ObjectProperty>  Properties;
    /// Definition of the internally used property collection type
    typedef std::vector<rpc::ObjectProperty*> PropertyRefs;
    PropertyRefs prop;

  public:
    /// Default constructor
    PropertyContainer() = default;
    /// Default destructor
    ~PropertyContainer();
    /// Access the hosted client services
    Clients    clients()  const;
    /// Access all properties of an object
    Properties allProperties()  const;
    /// Access all properties with the same name from all clients
    Properties namedProperties(const std::string& name)  const;
    /// Access all properties of one remote client (service, ect.)
    Properties clientProperties(const std::string& client)  const;
    /// Access a single property of an object
    rpc::ObjectProperty property(const std::string& client,
				 const std::string& name)  const;
    /// Modify a property
    void setProperty(const std::string& client,
		     const std::string& name,
		     const std::string& value)  const;
    /// Modify a property
    void setPropertyObject(const rpc::ObjectProperty& property) const;
    /// Exit process
    int call_exit();
  };
}
#endif  // RPC_TEST_OBJECTS_H
