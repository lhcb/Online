//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  XMLRPC_tests.cpp
//--------------------------------------------------------------------------
//
//  Package    : XML
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include "Score.h"

Online::rpc::test::_TestCount::_TestCount(int) {
}

Online::rpc::test::_TestCount::~_TestCount() {
  logger("TEST>  Counts: ") << "Total score: " << counter << std::endl;
}

Online::rpc::test::_TestCount& Online::rpc::test::_TestCount::instance() {
  static _TestCount inst(0);
  return inst; 
}

namespace {
  bool s_logger_have_time = true;
}

std::ostream& Online::rpc::test::logger(const std::string& prefix)  {
  if ( s_logger_have_time )   {
    time_t t = ::time(0);
    struct tm tm;
    ::localtime_r(&t, &tm);
    char buff[124];
    ::strftime(buff,sizeof(buff),"%H:%M:%S %d-%m-%Y ",&tm);
    return std::cout << buff << prefix;
  }
  return std::cout << prefix;
}

void Online::rpc::test::logger_have_time(bool value)   {
  s_logger_have_time = value;
}

