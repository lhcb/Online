//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  XMLRPC_tests.cpp
//--------------------------------------------------------------------------
//
//  Package    : XML
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#define RPC_NS           xmlrpc
#define Callable         DimCallable
#define CallableHandler  rpc::DimClient

#include <RPC/DimClient.h>
using namespace xmlrpc;
#include "Objects.h"
#include "test_rpc_client.h"

namespace  {
  void help_client()  {
    ::printf
      ("xmlrpc_client -opt [-opt]\n"
       "   -host=<host-name>     Client host name. Default: localhost\n"
       "   -port=<port-number>   Client port       Default: 8000   \n");
    ::exit(EINVAL);
  }
}

extern "C" int test_dimxmlrpc_client(int argc, char** argv)   {
  int loop  = 1, debug = 0;
  std::string server = "Test", dns = "";
  for(int i = 0; i < argc && argv[i]; ++i)  {
    if ( 0 == ::strncmp("-dns",argv[i],4) )
      dns = argv[++i];
    else if ( 0 == ::strncmp("-server",argv[i],4) )
      server = argv[++i];
    else if ( 0 == ::strncmp("-debug",argv[i],4) )
      debug = 1;
    else if ( 0 == ::strncmp("-loop",argv[i],4) )
      loop  = ::atol(argv[++i]);
    else if ( 0 == ::strncmp("-help",argv[i],2) )
      help_client();
  }

  Callable client;
  client.handler.setDebug(debug != 0);
  client.handler.open(server,dns);
  logger("DIM-SERVER") << "TEST> Starting test DIM-XMLRPC client: " << dns << ":" << server << std::endl;
  return call_client(argc, argv, client, loop);
}
