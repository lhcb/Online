//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  XMLRPC_tests.cpp
//--------------------------------------------------------------------------
//
//  Package    : XML
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include <RPC/DimServer.h>
#include <XML/Printout.h>
#include "test_rpc_server.h"

namespace  {
  void help_server()  {
    ::printf
      ("xmlrpc_server -opt [-opt]\n"
       "   -server=<server-name>     Server name. Default: \"<proc-name>/Test\"\n"
       "   -dns=<dns-name>           DNS host name.\n");
    ::exit(EINVAL);
  }
}

extern "C" int test_dimxmlrpc_server(int argc, char** argv)   {
  using namespace dd4hep;
  int debug = 0;
  std::string name = "Test", dns = "";
  for(int i = 0; i < argc && argv[i]; ++i)  {
    if ( 0 == ::strncmp("-dns",argv[i],4) )
      dns = argv[++i];
    else if ( 0 == ::strncmp("-server",argv[i],4) )
      name = argv[++i];
    else if ( 0 == ::strncmp("-debug",argv[i],4) )
      debug = 1;
    else if ( 0 == ::strncmp("-help",argv[i],2) )
      help_server();
  }

  rpc::DimServer server(name,dns);
  server.setDebug(debug != 0);
  printout(INFO,"DIM-SERVE","TEST> Starting test DIM-XMLRPC server: %s:%s\n",dns.c_str(),name.c_str());
  return call_server(argc, argv, xmlrpc::Call(), server);
}
