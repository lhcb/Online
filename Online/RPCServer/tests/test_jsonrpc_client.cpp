//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  XMLRPC_tests.cpp
//--------------------------------------------------------------------------
//
//  Package    : XML
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include <RPC/JSONRPC.h>
#include <RPC/RpcClientHandle.h>
#include <HTTP/HttpClient.h>

using namespace          jsonrpc;
#define RPC_NS           jsonrpc
#define CallableHandler  rpc::JsonRpcClientH
#define Callable         HttpJsonRpcCallable

#include "Objects.h"
#include "test_rpc_client.h"

extern "C" int test_jsonrpc_client(int argc, char** argv)   {
  RPCCli cli(argc, argv);
  Callable client { rpc::client<http::HttpClient>(cli.host, cli.port) };
  client.handler.setDebug(cli.debug != 0);
  logger("TESTClient") << "Starting test JSON RPC client with URI:" << cli.host << ":" << cli.port << endl;
  return call_client(argc, argv, client, cli.loop, cli.call_exit, false);
}
