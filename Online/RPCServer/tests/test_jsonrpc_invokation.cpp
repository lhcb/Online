//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  JSONRPC_tests.cpp
//--------------------------------------------------------------------------
//
//  Package    : JSON
//
//  Author     : Markus Frank
//==========================================================================

// C/C++ include files
#include <iostream>
#include <sstream>

/// Framework includes
#include "Objects.h"
#include "Score.h"
#include <RPC/JSONRPC.h>

using namespace std;
using namespace jsonrpc;
using namespace test_rpc;

namespace {
  void c1()  {
    Callable a;
    MethodCall call(R"({"jsonrpc": "2.0", "method": "test", "id": 1})"_json);
    Call callback = Call(&a).make(&Callable::call_void);
    MethodResponse response = callback.execute(call);
    logger("MethodResponse: ") << response.str() << endl;
  }
  void c2()  {
    Callable a;
    MethodCall call(R"({"jsonrpc": "2.0", "method": "test", "id": 1})"_json);
    Call callback = Call(&a).make(&Callable::call_void_const);
    MethodResponse response = callback.execute(call);
    logger("MethodResponse: ") << response.str() << endl;
  }
  /// c3   Test mehod overloading. Need to ensure the proper function pointer is extracted
  /// c31  Polymorph function with one argument
  void c31()  {
    Callable a;
    MethodCall call(R"({"jsonrpc": "2.0", "method": "test", "params": [12345], "id": 1})"_json);
    void (test_rpc::Callable::*c0)(int) = &test_rpc::Callable::call_void_int;
    Call callback = Call(&a).make(c0);
    MethodResponse response = callback.execute(call);
    logger("MethodResponse: ") << response.str() << endl;
  }
  /// c32  Polymorph function with two arguments
  void c32()  {
    Callable a;
    MethodCall call(R"({"jsonrpc": "2.0", "method": "test", "id": 1, "params": [12345,67890]})"_json);
    void (test_rpc::Callable::*c0)(int,int) = &test_rpc::Callable::call_void_int;
    Call callback = Call(&a).make(c0);
    MethodResponse response = callback.execute(call);
    logger("MethodResponse: ") << response.str() << endl;
  }
  void c4()  {
    Callable a;
    MethodCall call(R"({"jsonrpc": "2.0", "method": "test", "params": [12345], "id": 1})"_json);
    Call callback = Call(&a).make(&Callable::call_int_int_const);
    MethodResponse response = callback.execute(call);
    int result = response.data<int>();
    logger("MethodResponse: ") << " int: " << result << " from (" << response.str() << ")" << endl;
  }
  void c5()  {
    Callable a;
    MethodCall call(R"({"jsonrpc": "2.0", "method": "test", "params": [12345,6789.12,"Hello World"], "id": 1})"_json);
    Call callback = Call(&a).make(&Callable::call_int_int_double_string);
    MethodResponse response = callback.execute(call);
    int result = response.data<int>();
    logger("MethodResponse: ") << " int: " << result << " from (" << response.str() << ")" << endl;
  }
  void c6()  {
    Callable a;
    MethodCall call(R"({"jsonrpc": "2.0", "method": "test", "id": 1, "params": [{"a": 12345, "b": 67890}]})"_json);
    Call callback = Call(&a).make(&Callable::call_Object_Object);
    MethodResponse response = callback.execute(call);
    Object result = response.data<Object>();
    logger("MethodResponse: ") << " Object(" << result.a << "," << result.b << ") from (" << response.str() << ")" << endl;
  }
}

extern "C" int test_jsonrpc_invokation(int , char** )   {
  c1();
  c2();
  c31();
  c32();
  c4();
  c5();
  // c6();
  return 1;
}
extern "C" int test_jsonrpc_object_invokation(int , char** )   {
  c6();
  return 1;
}
