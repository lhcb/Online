//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  XMLRPC_tests.cpp
//--------------------------------------------------------------------------
//
//  Package    : XML
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include <RPC/HttpJsonRpcHandler.h>
#include "test_rpc_server.h"
#include "test_rpc_cli.h"

extern "C" int test_jsonrpc_server(int argc, char** argv)   {
  using namespace dd4hep;
  test_rpc::RPCCli cli(argc, argv);
  rpc::HttpServer server(std::make_unique<rpc::HttpJsonRpcHandler>(),
			 cli.host, cli.port,rpc::HttpServer::SERVER);
  server.setDebug(cli.debug != 0);
  printout(INFO,"HTTPSERVER","TEST> Starting test JSON RPC server with URI:%s:%d\n", cli.host.c_str(), cli.port);
  //return call_example_server(argc, argv, jsonrpc::Call(), server);
  return call_server<jsonrpc::Call>(argc, argv, jsonrpc::Call(), server);
}
