//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  XMLRPC_tests.cpp
//--------------------------------------------------------------------------
//
//  Package    : XML
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include <RTL/rtl.h>

namespace test_rpc {

  /// Cli to start clients
  class RPCCli : public RTL::CLI   {
  public:
    int loop  = 1, port  = 8000, debug = 0, call_exit = 0;
    std::string host = "localhost";
    std::string type;
  public:
    RPCCli(int argc, char** argv);
  };
}

inline test_rpc::RPCCli::RPCCli(int argc, char** argv)
 : RTL::CLI(argc, argv, [] {
     ::printf
       ("xmlrpc_<program> -opt [-opt]\n"
	"   -host=<host-name>     Client host name. Default: localhost\n"
	"   -port=<port-number>   Client port       Default: 8000     \n"
	"   -type=<test-type>     Test type (jsonrpc, xmlrpc)         \n"
	"   -debug                Enable debugging to stdout          \n"
	"   -help                 Show this help                      \n");
     ::exit(EINVAL);
   })
{
  this->getopt("host", 4, this->host);
  this->getopt("port", 4, this->port);
  this->getopt("loop", 4, this->loop);
  this->getopt("type", 4, this->type);
  this->debug = this->getopt("debug", 3) != 0;
  this->call_exit = this->getopt("exit", 3) != 0;
}
