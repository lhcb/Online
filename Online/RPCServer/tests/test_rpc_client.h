//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  XMLRPC_tests.cpp
//--------------------------------------------------------------------------
//
//  Package    : XML
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include "Score.h"
#include "Objects.h"
#include "test_rpc_cli.h"
#include <RTL/rtl.h>

// C/C++ include files
#include <iostream>
#include <cstring>

using namespace std;
using namespace test_rpc;

int Callable::call_exit()   {
  try  {
    logger("Calling") << "[" << s_test_count << "] " << " \"int call_exit()\" ... " << endl;
    handler.call(RPC_NS::MethodCall().setMethod("call_exit"));
  }
  catch(...)   {
  }
  logger("Calling") << "[" << s_test_count << "] " << "Server exit initiated. All done." << endl;
  return 0;
}

void Callable::call_exception_method()   {
  logger("Calling") << "[" << s_test_count << "] " << " \"void call_exception_method()\" ... " << endl;
  handler.call(RPC_NS::MethodCall().setMethod("call_exception_method"));
}

void Callable::call_unregistered_method()   {
  logger("Calling") << "[" << s_test_count << "] " << " \"void unregistered_method()\" ... Should give an exception." << endl;
  handler.call(RPC_NS::MethodCall().setMethod("call_unregistered_method"));
}

void Callable::call_void()  {
  logger("Calling") << "[" << s_test_count << "] " << " \"void call_void()\" ... " << endl;
  handler.call(RPC_NS::MethodCall().setMethod("call_void"));
  ++s_test_count;
}

void Callable::call_void_const()  const {
  logger("Calling") << "[" << s_test_count << "] " << " \"void call_void_const()  const\" ... " << endl;
  handler.call(RPC_NS::MethodCall().setMethod("call_void_const"));
  ++s_test_count;
}

void Callable::call_void_int(int a0)   {
  RPC_NS::MethodCall c;
  c.setMethod("call_void_int").addParam(a0);
  logger("Calling") << "[" << s_test_count << "] " << " \"void call_void_int(int:" << a0 << ")\" ... " << endl;
  handler.call(c);
  ++s_test_count;
}

void Callable::call_void_int(int a0, int a1)   {
  RPC_NS::MethodCall c;
  c.setMethod("call_void_int2").addParam(a0).addParam(a1);
  logger("Calling") << "[" << s_test_count << "] " << " \"void call_void_int(2)(int:" << a0 << ", " << a1 << ")\" ... " << endl;
  handler.call(c);
  ++s_test_count;
}

void Callable::call_void_string(const std::string& a0)   {
  RPC_NS::MethodCall c;
  c.setMethod("call_void_string").addParam(a0);
  logger("Calling") << "[" << s_test_count << "] " << " \"void call_void_string(string:" << a0 << ")\" ... " << endl;
  handler.call(c);
  ++s_test_count;
}

int Callable::call_int_int_const(int a0)  const {
  RPC_NS::MethodCall c;
  c.setMethod("call_int_int_const").addParam(a0);
  logger("Calling") << "[" << s_test_count << "] " << " \"int call_int_int_const(int:" << a0 << ")  const\" ... " << endl;
  RPC_NS::MethodResponse r = handler.call(c);
  ++s_test_count;
  int ret = r.data<int>();
  ++s_test_count;
  logger("Calling") << "[" << s_test_count << "] " << " \"call_int_int_const\":  Result=" << ret << endl;
  return ret;
}

int Callable::call_int_int_double_string(int a0, double a1, const string& a2)  {
  RPC_NS::MethodCall c;
  c.setMethod("call_int_int_double_string").addParam(a0).addParam(a1).addParam(a2);
  logger("Calling") << "[" << s_test_count << "] " << " \"int call_int_int_double_string(int:" << a0 
		    << ", double:" << a1 
		    << ", string:" << a2 << ")  const\" ... " << endl;
  RPC_NS::MethodResponse r = handler.call(c);
  ++s_test_count;
  int ret = r.data<int>();
  ++s_test_count;
  logger("Calling") << "[" << s_test_count << "] " << " \"call_int_int_double_string\":  Result=" << ret << endl;
  return ret;
}

Object Callable::call_Object_Object(const Object& b)  {
  RPC_NS::MethodCall c;
  c.setMethod("call_Object_Object").addParam(b);
  logger("Calling") << "[" << s_test_count << "] " << " \"Object call_Object_Object(int: .a=" << b.a 
		    << ", int: .b=" << b.b << ")  const\" ... " << endl;
  ++s_test_count;
  RPC_NS::MethodResponse r = handler.call(c);
  Object ret = r.data<Object>();
  logger("Calling") << "[" << s_test_count << "] " << " \"call_Object_Object\":  Result= (" << ret.a << " , " << ret.b << ")" << endl;
  ++s_test_count;
  return ret;
}

Object Callable::call_Object_vector(const vector<Object>& b)  {
  RPC_NS::MethodCall c;
  c.setMethod("call_Object_vector").addParam(b);
  logger("Calling") << "[" << s_test_count << "] " << " \"Object call_Object_vector(size:" << b.size()
		    << ")  const\" ... " << endl;
  ++s_test_count;
  RPC_NS::MethodResponse r = handler.call(c);
  Object ret = r.data<Object>();
  logger("Calling") << "[" << s_test_count << "] " << " \"call_Object_vector\":  Result= (" << ret.a << " , " << ret.b << ")" << endl;
  ++s_test_count;
  return ret;
}

Object Callable::call_Object_list(const list<Object>& b)  {
  RPC_NS::MethodCall c;
  c.setMethod("call_Object_list").addParam(b);
  logger("Calling") << "[" << s_test_count << "] " << " \"Object call_Object_list(size:" << b.size()
		    << ")  const\" ... " << endl;
  ++s_test_count;
  RPC_NS::MethodResponse r = handler.call(c);
  Object ret = r.data<Object>();
  logger("Calling") << "[" << s_test_count << "] " << " \"call_Object_list\":  Result= (" << ret.a << " , " << ret.b << ")" << endl;
  ++s_test_count;
  return ret;
}

Object Callable::call_Object_set(const set<Object>& b)  {
  RPC_NS::MethodCall c;
  c.setMethod("call_Object_set").addParam(b);
  logger("Calling") << "[" << s_test_count << "] " << " \"Object call_Object_set(size:" << b.size()
		    << ")  const\" ... " << endl;
  ++s_test_count;
  RPC_NS::MethodResponse r = handler.call(c);
  Object ret = r.data<Object>();
  logger("Calling") << "[" << s_test_count << "] " << " \"call_Object_set\":  Result= (" << ret.a << " , " << ret.b << ")" << endl;
  ++s_test_count;
  return ret;
}

namespace  {

  /// Main client entry point
  template <typename T> int call_client(int, char**, T& client, int loop, int call_exit=false, int call_objects=true)   {
    logger("call_client") << " loop:" << loop << " call objects: " << call_objects << " call exit: " << call_exit << endl;
    vector<Object> vector_objects;
    vector_objects.push_back(Object(1,10));
    vector_objects.push_back(Object(2,20));
    vector_objects.push_back(Object(3,30));
    vector_objects.push_back(Object(4,40));
    list<Object> list_objects;
    list_objects.push_back(Object(1,10));
    list_objects.push_back(Object(2,20));
    list_objects.push_back(Object(3,30));
    list_objects.push_back(Object(4,40));
    set<Object> set_objects;
    set_objects.insert(Object(1,10));
    set_objects.insert(Object(2,20));
    set_objects.insert(Object(3,30));
    set_objects.insert(Object(4,40));
    do   {
      try   {
	client.call_void();
	client.call_void_int(10);
	client.call_void_int(10,20);
	client.call_int_int_const(loop);
	client.call_int_int_double_string(10,99.6,"Hello World");
	if ( call_objects )   {
	  Object obj{33,66};
	  Object ret = client.call_Object_Object(obj);
	  //logger("TEST") << " \"call_Object_Object\":  Result= (" << ret.a << " , " << ret.b << ")" << endl;
	  ret = client.call_Object_set(set_objects);
	  //logger("TEST") << " \"call_Object_set\":     Result= (" << ret.a << " , " << ret.b << ")" << endl;
	  ret = client.call_Object_list(list_objects);
	  //logger("TEST") << " \"call_Object_list\":    Result= (" << ret.a << " , " << ret.b << ")" << endl;
	  ret = client.call_Object_vector(vector_objects);
	  //logger("TEST") << " \"call_Object_vector\":  Result= (" << ret.a << " , " << ret.b << ")" << endl;
	}
	try  {
	  client.call_unregistered_method();
	}
	catch(const std::exception& e)   {
	  logger("EXCEPT ") << "Calling client.call_unregistered_method(): " << e.what() << endl;
	  ++s_test_count;
	}

	try  {
	  client.call_exception_method();
	}
	catch(const std::exception& e)   {
	  logger("EXCEPT ") << "Calling client.call_exception_method(): " << e.what() << endl;
	  ++s_test_count;
	}
      }
      catch(const std::exception& e)   {
	logger("EXCEPT ") << "UNEXPECTED LOOP exception: " << e.what() << endl;
	++s_test_count;
      }
    }  while ( --loop > 0 );
    if ( call_exit )   {
      client.call_exit();
    }
    return 0;
  }
}
