//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  XMLRPC_tests.cpp
//--------------------------------------------------------------------------
//
//  Package    : XML
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include <RPC/XMLRPC.h>
#include <RPC/RpcClientHandle.h>
#include <HTTP/HttpClient.h>

using namespace          xmlrpc;
#define RPC_NS           xmlrpc
#define CallableHandler  rpc::XmlRpcClientH
#define Callable         HttpXmlRpcCallable

#include "test_rpc_client.h"

extern "C" int test_xmlrpc_client(int argc, char** argv)   {
  RPCCli cli(argc, argv);
  Callable client { rpc::client<http::HttpClient>(cli.host, cli.port) };
  client.handler.setDebug(cli.debug != 0);
  logger("TESTClient") << "Starting test XML RPC client with URI:" << cli.host << ":" << cli.port << endl;
  return call_client(argc, argv, client, cli.loop, cli.call_exit);
}
