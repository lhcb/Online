//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  XMLRPC_tests.cpp
//--------------------------------------------------------------------------
//
//  Package    : XML
//
//  Author     : Markus Frank
//==========================================================================

// C/C++ include files
#include <iostream>
#include <sstream>

/// Framework includes
#include "Objects.h"
#include "Score.h"
#include "RPC/XMLRPC.h"

using namespace std;
using namespace xmlrpc;
using namespace test_rpc;

namespace {
  void c1()  {
    Callable a;
    MethodCall call("<?xml version=\"1.0\" encoding=\"UTF-8\">\n"
		    "<methodCall>"
		    "  <methodName>test</methodName>\n"
		    "  <params>\n"
		    "  </params>\n"
		    "</methodCall>");
    Call callback = Call(&a).make(&Callable::call_void);
    MethodResponse response = callback.execute(call);
    logger("MethodResponse: ") << response.str() << endl;
  }
  void c2()  {
    Callable a;
    MethodCall call("<?xml version=\"1.0\" encoding=\"UTF-8\">\n"
		    "<methodCall>"
		    "  <methodName>test</methodName>\n"
		    "  <params>\n"
		    "  </params>\n"
		    "</methodCall>");
    Call callback = Call(&a).make(&Callable::call_void_const);
    MethodResponse response = callback.execute(call);
    logger("MethodResponse: ") << response.str() << endl;
  }
  /// c3   Test mehod overloading. Need to ensure the proper function pointer is extracted
  /// c31  Polymorph function with one argument
  void c31()  {
    Callable a;
    MethodCall call("<?xml version=\"1.0\" encoding=\"UTF-8\">\n"
		    "<methodCall>\n"
		    "  <methodName>test</methodName>\n"
		    "  <params>\n"
		    "    <param><value><int>12345</int></value></param>\n"
		    "  </params>\n"
		    "</methodCall>");
    void (test_rpc::Callable::*c0)(int) = &test_rpc::Callable::call_void_int;
    Call callback = Call(&a).make(c0);
    MethodResponse response = callback.execute(call);
    logger("MethodResponse: ") << response.str() << endl;
  }
  /// c32  Polymorph function with two arguments
  void c32()  {
    Callable a;
    MethodCall call("<?xml version=\"1.0\" encoding=\"UTF-8\">\n"
		    "<methodCall>\n"
		    "  <methodName>test</methodName>\n"
		    "  <params>\n"
		    "    <param><value><int>12345</int></value></param>\n"
		    "    <param><value><int>67890</int></value></param>\n"
		    "  </params>\n"
		    "</methodCall>");
    void (test_rpc::Callable::*c0)(int,int) = &test_rpc::Callable::call_void_int;
    Call callback = Call(&a).make(c0);
    MethodResponse response = callback.execute(call);
    logger("MethodResponse: ") << response.str() << endl;
  }
  void c4()  {
    Callable a;
    MethodCall call("<?xml version=\"1.0\" encoding=\"UTF-8\">\n"
		    "<methodCall>\n"
		    "  <methodName>test</methodName>\n"
		    "  <params>\n"
		    "    <param><value><int>12345</int></value></param>\n"
		    "  </params>\n"
		    "</methodCall>");
    Call callback = Call(&a).make(&Callable::call_int_int_const);
    MethodResponse response = callback.execute(call);
    logger("MethodResponse: ") << response.str() << endl;
  }
  void c5()  {
    Callable a;
    MethodCall call("<?xml version=\"1.0\" encoding=\"UTF-8\">\n"
		    "<methodCall>\n"
		    "  <methodName>test</methodName>\n"
		    "  <params>\n"
		    "    <param><value><int>12345</int></value></param>\n"
		    "    <param><value><double>6789.12</double></value></param>\n"
		    "    <param><value><string>Hello World</string></value></param>\n"
		    "  </params>\n"
		    "</methodCall>");
    Call callback = Call(&a).make(&Callable::call_int_int_double_string);
    MethodResponse response = callback.execute(call);
    logger("MethodResponse: ") << response.str() << endl;
  }
  void c6()  {
    Callable a;
    MethodCall call("<?xml version=\"1.0\" encoding=\"UTF-8\">\n"
		    "<methodCall>\n"
		    "  <methodName>test</methodName>\n"
		    "  <params>\n"
		    "    <param><value><struct>\n"
		    "      <member><name>a</name><value><i4>111</i4></value></member>\n"
		    "      <member><name>b</name><value><i4>222</i4></value></member>\n"
		    "    </struct></value></param>\n"
		    "    <param><value><double>6789.12</double></value></param>\n"
		    "    <param><value><string>Hello World</string></value></param>\n"
		    "  </params>\n"
		    "</methodCall>");
    Call callback = Call(&a).make(&Callable::call_Object_Object);
    MethodResponse response = callback.execute(call);
    logger("MethodResponse: ") << response.str() << endl;
  }
}

extern "C" int test_xmlrpc_invokation(int argc, char** /* argv */)   {
  Online::rpc::test::logger_have_time(argc <= 1);
  c1();
  c2();
  c31();
  c32();
  c4();
  c5();
  c6();
  return 0;
}
