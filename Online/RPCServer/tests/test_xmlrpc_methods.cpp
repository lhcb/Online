//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  XMLRPC_tests.cpp
//--------------------------------------------------------------------------
//
//  Package    : XML
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include "RPC/XMLRPC.h"
#include "Score.h"

using namespace std;
using namespace xmlrpc;

namespace {
  void fill_structure(Structure& a, int level=1);
  void fill_array(Array& a, int level=1);
  void decodeArray(const std::string& prefix, xml_elt_t e);
  void decodeStruct(const std::string& prefix, xml_elt_t e);

  template <typename T> void test_encode_response(const string& tag, const T& val)  {
    MethodResponse doc = MethodResponse::make(val);
    logger("+++ MethodResponse<"+tag+">: ") << endl
					    << doc.str()
					    << endl;
    ++s_test_count;
  }

  /// Decode xmlrpc-data iteratively to primitives
  void decodeItem(const std::string& prefix, xml_elt_t elt)  {
    string tag = elt.tag();
    if ( tag == "int" || tag == "i4" )  {
      logger(prefix) << "(int)    " << elt.text() << endl;
      ++s_test_count;
    }
    else if ( tag == "double" )   {
      logger(prefix) << "(double) " << elt.text() << endl;
      ++s_test_count;
    }
    else if ( tag == "string" )   {
      logger(prefix) << "(string) " << elt.text() << endl;
      ++s_test_count;
    }
    else if ( tag == "boolean" )   {
      logger(prefix) << "(bool)   " << elt.text() << endl;
      ++s_test_count;
    }
    else if ( tag == "struct" )   {
      decodeStruct(prefix+"(struct)",elt);
      ++s_test_count;
    }
    else if ( tag == "array" )   {
      decodeArray(prefix+"(array)",elt);
      ++s_test_count;
    }
    else   {
      logger(prefix) << "UNKNOWN data type: " << tag << endl;
    }
  }

  /// Decode xmlrpc-array to values
  void decodeArray(const std::string& prefix, xml_elt_t e)  {
    for(xml_coll_t data(e,_rpcU(data)); data; ++data)   {
      int count = 0;
      stringstream str;
      for(xml_coll_t value(data,_rpcU(value)); value; ++value)   {
	for(xml_coll_t type(value,_U(star)); type; ++type)   {
	  str.str("");
	  str << prefix << "[" << count << "]";
	  decodeItem(str.str(),type);
	  ++count;
	}
      }
      return; // According to the standard exactly 1 data element is allowed!
    }
  }

  /// Decode xmlrpc-structure to values
  void decodeStruct(const std::string& prefix, xml_elt_t structure)  {
    for(xml_coll_t m(structure,_rpcU(member)); m; ++m)   {
      xml_elt_t n = xml_elt_t(m).child(_U(name));
      xml_elt_t v = xml_elt_t(m).child(_U(value));
      // According to the standard exactly 1 data element is allowed!
      decodeItem(prefix+"."+n.text(),xml_coll_t(v,_U(star)));
    }
  }

  /// Fill xmlrpc-array with default values
  void fill_array(Array& a, int level)   {
    a.add(true);
    ++s_test_count;
    a.add('0');
    ++s_test_count;
    a.add((signed char)-1);
    ++s_test_count;
    a.add((unsigned char)1);
    ++s_test_count;
    a.add((short)-2);
    ++s_test_count;
    a.add((unsigned short)2);
    ++s_test_count;
    a.add((int)-3);
    ++s_test_count;
    a.add((unsigned int)3);
    ++s_test_count;
    a.add((long)-4);
    ++s_test_count;
    a.add((unsigned long)4);
    ++s_test_count;
    a.add((float)5);
    ++s_test_count;
    a.add((double)6);
    ++s_test_count;
    a.add(_toString(7));
    ++s_test_count;
    if ( level >= 0 )   {
      xml_elt_t h = a.add(Structure());
      Structure s(h);
      fill_structure(s, level-1);
    }
  }

  /// Fill xmlrpc-structure with default values
  void fill_structure(Structure& a, int level)   {
    a.add("member_bool",true);
    ++s_test_count;
    a.add("member_char",'0');
    ++s_test_count;
    a.add("member_signed_char",(signed char)-1);
    ++s_test_count;
    a.add("member_unsigned_char",(unsigned char)1);
    ++s_test_count;
    a.add("member_short",(short)-2);
    ++s_test_count;
    a.add("member_unsigned_short",(unsigned short)2);
    ++s_test_count;
    a.add("member_int",(int)-3);
    ++s_test_count;
    a.add("member_unsigned_int",(unsigned int)3);
    ++s_test_count;
    a.add("member_long",(long)-4);
    ++s_test_count;
    a.add("member_unsigned_long",(unsigned long)4);
    ++s_test_count;
    a.add("member_float",(float)5);
    ++s_test_count;
    a.add("member_double",(double)6);
    ++s_test_count;
    a.add("member_string",_toString(7));
    ++s_test_count;
    if ( level >= 0 )   {
      Array arr(a.add("member_array",Array()));
      fill_array(arr, level-1);
    }
  }

  MethodResponse create_nested_array(bool prt=true)  {
    MethodResponse doc = MethodResponse::make(Array());
    Array array(doc.value);
    Array sub_array_1(array.add(Array()));
    Array sub_array_2(array.add(Array()));
    Array sub_array_3(array.add(Array()));
    Array ss_array_1(sub_array_1.add(Array()));

    fill_array(sub_array_1,3);
    fill_array(sub_array_2,3);
    fill_array(sub_array_3,3);
    fill_array(ss_array_1,3);
    if ( prt )  {
      logger("MethodResponse<create_nested_array>: ") << doc.str() << endl;
    }
    return doc;
  }

  MethodResponse create_nested_struct(bool prt=true)  {
    MethodResponse doc = MethodResponse::make(Structure());
    Structure structure(doc.value);
    Structure sub_structure_1(structure.add("sub_structure_1",Structure()));
    Structure sub_structure_2(structure.add("sub_structure_2",Structure()));
    Structure sub_structure_3(structure.add("sub_structure_3",Structure()));
    Structure ss_structure_1(sub_structure_1.add("subsub_structure_1",Structure()));

    fill_structure(sub_structure_1,3);
    fill_structure(sub_structure_2,3);
    fill_structure(sub_structure_3,3);
    fill_structure(ss_structure_1,3);
    if ( prt )  {
      logger("MethodResponse<create_nested_struct>: ") << endl << doc.str() << endl;
    }
    return doc;
  }

  template <typename T> void test_decode_response(const string& tag, const T& val)  {
    MethodResponse doc = MethodResponse::make(val);
    string dsc = "MethodResponse<"+tag+"> ("+doc.value.tag()+"): ";
    logger(dsc) << endl << doc.str() << endl;
    decodeItem("response",doc.value);
  }
}

extern "C" int test_xmlrpc_encode_response(int argc, char** /* argv */)   {
  Online::rpc::test::logger_have_time(argc <= 1);
  test_encode_response("bool",       true);
  test_encode_response("int",        int(123456));
  test_encode_response("long",       long(123456));
  test_encode_response("double",     double(123456.789));
  test_encode_response("string",     string("Hello world!"));
  test_encode_response("Array",      Array());
  test_encode_response("Structure",  Structure());
  return 0;
}

extern "C" int test_xmlrpc_encode_array(int argc, char** /* argv */)   {
  Online::rpc::test::logger_have_time(argc <= 1);
  create_nested_array();
  return 0;
}

extern "C" int test_xmlrpc_encode_struct(int argc, char** /* argv */)   {
  Online::rpc::test::logger_have_time(argc <= 1);
  create_nested_struct();
  return 0;
}

extern "C" int test_xmlrpc_decode_response(int argc, char** /* argv */)   {
  Online::rpc::test::logger_have_time(argc <= 1);
  test_decode_response("bool",       true);
  test_decode_response("int",        int(123456));
  test_decode_response("long",       long(123456));
  test_decode_response("double",     double(123456.789));
  test_decode_response("string",     string("Hello world!"));
  test_decode_response("Array",      Array());
  test_decode_response("Structure",  Structure());
  return 0;
}

extern "C" int test_xmlrpc_decode_struct(int argc, char** /* argv */)   {
  Online::rpc::test::logger_have_time(argc <= 1);
  MethodResponse doc = create_nested_struct(false);
  decodeItem("value",doc.value);
  return 0;
}

extern "C" int test_xmlrpc_decode_array(int argc, char** /* argv */)   {
  Online::rpc::test::logger_have_time(argc <= 1);
  MethodResponse doc = create_nested_array(false);
  decodeItem("value",doc.value);
  return 0;
}
