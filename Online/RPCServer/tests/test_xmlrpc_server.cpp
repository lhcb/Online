//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//  XMLRPC_tests.cpp
//--------------------------------------------------------------------------
//
//  Package    : XML
//
//  Author     : Markus Frank
//==========================================================================

/// Framework includes
#include <RPC/HttpXmlRpcHandler.h>
#include "test_rpc_server.h"
#include "test_rpc_cli.h"

extern "C" int test_xmlrpc_server(int argc, char** argv)   {
  using namespace dd4hep;
  test_rpc::RPCCli cli(argc, argv);
  rpc::HttpServer server(std::make_unique<rpc::HttpXmlRpcHandler>(),
			 cli.host, cli.port,rpc::HttpServer::SERVER);
  server.setDebug(cli.debug != 0);
  printout(INFO,"HTTPSERVER","TEST> Starting test XML RPC server with URI:%s:%d\n", cli.host.c_str(), cli.port);
  return call_server<xmlrpc::Call>(argc, argv, xmlrpc::Call(), server);
}
