#ifdef RAWSIZE_BANKDESCR_H
#else
#define RAWSIZE_BANKDESCR_H


namespace Online   {

#define HISTPERHIST 3
  std::string titqual[HISTPERHIST] = {" (all Triggers)", " (HLTAccept)", " (noBias)"};
  std::string namqual[HISTPERHIST] = {"(all)", "(HLTAccept)", "(HLT-Pass)"};
  // titqual[0] = " (all Triggers)";
  // titqual[1] = " (nonLumi)";
  // titqual[2] = " (Lumi)";
  // namqual[0] = "(all)";
  // namqual[1] = "(nonLumi)";
  // namqual[2] = "(Lumi)";

  class BankDescr
  {
  public:
    bool isError;
    std::string name;
    int rootbankidx;
    double sum;
    double siz;
    double xmin,xmax,binw;
    int nbin;
    int nentries;
    AIDA::IHistogram1D *h[HISTPERHIST];
    std::string h_name[HISTPERHIST];
    AIDA::IProfile1D *p[HISTPERHIST];
    std::string p_name[HISTPERHIST];
    bool noprofile;
    bool nohist;

    BankDescr()
      {
	nentries = 0;
	sum=0;
	isError = false;
      }
    void init(const std::string& nam, bool noprof)
    {
      int i;
      name = nam;
      noprofile = noprof;
      for (i=0;i<HISTPERHIST;i++)
	{
	  h_name[i] = "h"+name+namqual[i];
	  p_name[i] = "p"+name+namqual[i];
	}

    };
    void init(int bn, bool noprof)
    {
      noprofile = noprof;
      switch (bn)
	{
	case bank_types_t::ITError:
	  {
	    isError = true;
	    rootbankidx=bank_types_t::IT;
	    break;
	  }
	case bank_types_t::TTError:
	  {
	    isError = true;
	    rootbankidx=bank_types_t::TT;
	    break;
	  }
	case bank_types_t::VeloError:
	  {
	    isError = true;
	    rootbankidx=bank_types_t::Velo;
	    break;
	  }
	case bank_types_t::OTError:
	  {
	    isError = true;
	    rootbankidx=bank_types_t::OT;
	    break;
	  }
	case bank_types_t::EcalPackedError:
	  {
	    isError = true;
	    rootbankidx=bank_types_t::EcalPacked;
	    break;
	  }
	case bank_types_t::HcalPackedError:
	  {
	    isError = true;
	    rootbankidx=bank_types_t::HcalPacked;
	    break;
	  }
	case bank_types_t::PrsPackedError:
	  {
	    isError = true;
	    rootbankidx=bank_types_t::PrsPacked;
	    break;
	  }
	case bank_types_t::L0CaloError:
	  {
	    isError = true;
	    rootbankidx=bank_types_t::L0Calo;
	    break;
	  }
	case bank_types_t::L0MuonError:
	  {
	    isError = true;
	    rootbankidx=bank_types_t::L0Muon;
	    break;
	  }
	case bank_types_t::MuonError:
	  {
	    isError = true;
	    rootbankidx=bank_types_t::Muon;
	    break;
	  }
	case bank_types_t::L0DUError:
	  {
	    isError = true;
	    rootbankidx=bank_types_t::L0DU;
	    break;
	  }
	case bank_types_t::L0PUError:
	  {
	    isError = true;
	    rootbankidx=bank_types_t::L0PU;
	    break;
	  }
	default:
	  {
	    isError=false;
	    rootbankidx = -1;
	    break;
	  }
	}
      name = event_print::bankType(bn);
      int i;
      for (i=0;i<HISTPERHIST;i++)
	{
	  h_name[i] = "h"+name+namqual[i];
	  p_name[i] = "p"+name+namqual[i];
	}
    }
  };
}
#endif
