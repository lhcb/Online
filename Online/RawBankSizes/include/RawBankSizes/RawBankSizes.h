//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : B.Jost
//
//==========================================================================

#ifndef COMPONENT_RawBankSizes_H
#define COMPONENT_RawBankSizes_H 1

// Include files from system
#include <math.h>
#include <map>

#include <EventData/bank_header_t.h>
#include <EventData/bank_types_t.h>
#include <AIDA/IProfile1D.h>
#include <AIDA/IHistogram1D.h>
#include "Structure.h"
#include "BankDescr.h"

// from Gaudi
#include <Gaudi/Algorithm.h>
#include <GaudiKernel/IHistogramSvc.h>
#include <GaudiKernel/DataObjectHandle.h>

/// Forward declarations
namespace LHCb { class RawBank; }

///  Online namespace declaration
namespace Online   {

  class RawBankSizes : public Gaudi::Algorithm  {
    typedef std::map<std::string,BankDescr*>          detbmap;
    typedef detbmap::iterator                         detbmiter;
    typedef std::pair<detbmiter, bool>                detbinsrtp;
    typedef std::map<std::string,HParam::HistParams*> dethmap;
    typedef dethmap::iterator                         dethmiter;
    typedef std::pair<dethmiter, bool>                dethinsrtp;

    typedef std::vector<std::pair<const bank_header_t*, const void*> > evt_data_t;
    typedef std::vector<std::pair<const LHCb::RawBank*, const void*> > lb_evt_data_t;

  protected:
    StatusCode fill_histos(const evt_data_t& data, int runno);

  public:
    /// Standard constructor
    RawBankSizes(const std::string& name, ISvcLocator* pSvcLocator);

    virtual ~RawBankSizes();

    /// Algorithm initialization
    StatusCode initialize() override;
    /// Algorithm execution
    StatusCode execute   (const EventContext& context) const override  final;
    /// Algorithm finalization
    StatusCode finalize  () override;

  protected:

    DataObjectReadHandle<lb_evt_data_t> m_rawData{this,"RawData","Banks/RawData"};
    std::mutex                          m_mutex;

    std::vector<std::string>            m_detectorNames;
    std::vector<std::string>            m_bankNames;
    detbmap                             m_detectors;
    dethmap                             m_dethparams;

    std::vector<int>                    m_bankTypes;
    std::map<std::string,IHistogram1D*> m_dethist;
    std::map<int,double >               m_detsums;
    std::map<std::string,int>           m_max;
    int                                 m_def;
    int                                 m_bin;
    bool                                m_prof;
    bool                                m_dumpOptions;
    BankDescr                           m_banks[bank_types_t::LastType];
    BankDescr                           m_totsize;
    HParam::HistParams                  m_hparams[bank_types_t::LastType];
    HParam::HistParams                  m_totevpar;
    HParam::HistParams                  m_HLTratiopar;

    IHistogram1D                       *h_totev[HISTPERHIST];
    IProfile1D                         *p_banks[HISTPERHIST];
    IHistogram1D                       *h_HLTratio[HISTPERHIST];
  };
}
#endif // COMPONENT_RawBankSizes_H
