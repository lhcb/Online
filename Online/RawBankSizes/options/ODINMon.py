"""
     Online Raw bank size monitoring application configuration

     @author M.Frank
"""
__version__ = "1.0"
__author__  = "Markus Frank <Markus.Frank@cern.ch>"

import os
import sys
import Configurables
import Gaudi.Configuration as Gaudi
import OnlineEnvBase as OnlineEnv

import GaudiOnline
from Configurables import Online__FlowManager as FlowManager


class ODINMon(GaudiOnline.Application):
  def __init__(self, outputLevel, partitionName, partitionID, classType):
    GaudiOnline.Application.__init__(self,
                                     outputLevel=outputLevel,
                                     partitionName=partitionName, 
                                     partitionID=partitionID,
                                     classType=classType)

  def setup_algorithms(self):
    import Gaudi.Configuration as Gaudi
    import Configurables
    input              = self.setup_event_input()
    input.MakeRawEvent = 1
    monitor            = Configurables.Online__ODINMonitor('ODINMon')
    monitor.RawData    = '/Event/Banks/RawData'
    sequence           = GaudiOnline.Sequencer('Processor')
    sequence.Members   = [input, self.updateAndReset, monitor]
    self.app.TopAlg    = [sequence]
    self.broker.DataProducers = self.app.TopAlg
    return self

  def configure(self):
    self.setup_fifolog()
    self.setup_monitoring('ODINMon')
    self.setup_mbm_access('Events', True)
    self.setup_hive(FlowManager("EventLoop"), 40)
    self.updateAndReset.saveHistograms = 1
    if self.partitionName == 'FEST':
      self.updateAndReset.saverCycle     = 600
    else:
      self.updateAndReset.saverCycle     = 600
    self.updateAndReset.saveSetDir     = "/hist/Savesets"

    self.setup_algorithms()
    self.config.expandTAE           = False
    self.config.burstPrintCount     = 30000
    self.config.MBM_numConnections  = 1
    self.config.MBM_numEventThreads = 1
    # Mode slection::  synch: 0 async_queued: 1 sync_queued: 2
    self.config.execMode            = 0
    self.config.numEventThreads     = 1
    #
    self.config.MBM_requests = [
      'EvType=2;TriggerMask=0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF;VetoMask=0,0,0,0;MaskType=ANY;UserType=USER;Frequency=PERC;Perc=100.0'
    ]
    self.enableUI()
    print('Setup complete....')



ODINMon(outputLevel=OnlineEnv.OutputLevel,
             partitionName=OnlineEnv.PartitionName,
             partitionID=OnlineEnv.PartitionID,
             classType=GaudiOnline.Class1).configure()
