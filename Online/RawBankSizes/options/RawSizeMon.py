"""
     Online Raw bank size monitoring application configuration

     @author M.Frank
"""
__version__ = "1.0"
__author__  = "Markus Frank <Markus.Frank@cern.ch>"

import os
import sys
import Configurables
import Gaudi.Configuration as Gaudi
import OnlineEnvBase as OnlineEnv

import GaudiOnline
from Configurables import Online__FlowManager as FlowManager


class RawSizeMon(GaudiOnline.Application):
  def __init__(self, outputLevel, partitionName, partitionID, classType):
    GaudiOnline.Application.__init__(self,
                         outputLevel=outputLevel,
                         partitionName=partitionName, 
                         partitionID=partitionID,
                         classType=classType)

  def setup_algorithms(self):
    import Gaudi.Configuration as Gaudi
    import Configurables
    input = self.setup_event_input()
    self.input                  = input
    self.config.debug           = False
    self.config.dump_options    = False
    monitor                     = Configurables.Online__RawBankSizes('RawBankSizes')
    monitor.RawData             = '/Event/Banks/RawData'
    monitor.Bins                = 100
    monitor.DumpOptions         = False
    monitor.D_L0                =  (  -1,    0.0,   4000.0, 0,   -1, "L0")
    monitor.D_PRS               =  (  -1,    0.0,   6000.0, 0,   -1, "PRS")
    monitor.D_ECal              =  ( 100, 4000.0,  11000.0, 0,   -1, "ECal")
    monitor.D_HCal              =  ( 100,    0.0,   3000.0, 0,   -1, "HCal")
    monitor.D_Velo              =  ( 100,    0.0,  22000.0, 0,   -1, "Velo")
    monitor.D_RICH              =  ( 100,    0.0,  70000.0, 0,   -1, "RICH")
    monitor.D_IT                =  (  -1,    0.0,  16000.0, 0,   -1, "IT")
    monitor.D_TT                =  (  -1,    0.0,  16000.0, 0,   -1, "TT")
    monitor.D_OT                =  (  -1,    0.0,  24000.0, 0,   -1, "OT")
    monitor.D_Plume             =  ( 100,    0.0,  24000.0, 0,   -1, "Plume")
    monitor.D_Muon              =  ( 100,    0.0,   5000.0, 0,   -1, "Muon")
    monitor.D_HLT               =  (1000,    0.0, 100000.0, 0,   -1, "HLT")
    monitor.D_Online            =  ( 100,    0.0,    400.0, 0,   -1, "Online")
    monitor.D_LHCb              =  ( 100,    0.0,  20000.0, 0,   -1, "LHCb")
    monitor.D_Unknown           =  ( 100,    0.0,  20000.0, 0,   -1, "Unknown")
    monitor.D_TDET              =  ( 100,    0.0,  20000.0, 0,   -1, "TDET")

    monitor.D_UT                =  ( 100,    0.0,  60000.0, 0,   -1, "UT")
    monitor.D_FT                =  ( 100,    0.0,  40000.0, 0,   -1, "FT")
    monitor.D_VP                =  ( 100,    0.0,  60000.0, 0,   -1, "VP")
    monitor.D_Calo              =  ( 100,    0.0,  20000.0, 0,   -1, "Calo")

    # -----------------------------------------------------------------------
    #                               nbin     min       max smin smax detector
    monitor.DAQ                 =  (   5,    0.0,    100.0, 0,   -1, "Online" )
    monitor.DstAddress          =  (  -1,    0.0,  10000.0, 0,   -1, "LHCb" )
    monitor.DstBank             =  (  -1,    0.0,  10000.0, 0,   -1, "LHCb" )
    monitor.DstData             =  (  -1,    0.0,  10000.0, 0,   -1, "LHCb" )
    monitor.EcalE               =  ( 100,    0.0,  10000.0, 0,  128, "ECal" )
    monitor.EcalPacked          =  ( 100, 3000.0,  11000.0, 0,   26, "ECal" )
    monitor.EcalPackedError     =  ( 100,    0.0,  10000.0, 0,  128, "ECal" )
    monitor.EcalTrig            =  ( 100,    0.0,  10000.0, 0,  128, "ECal" )

    monitor.FTCluster           =  ( 100,    0.0,  35000.0, 0, 1024, "FT" )
    monitor.FTGeneric           =  ( 100,    0.0,  35000.0, 0, 1024, "FT" )
    monitor.FTSpecial           =  ( 100,    0.0,  35000.0, 0, 1024, "FT" )
    monitor.FTNZS               =  ( 100,    0.0,  50000.0, 0, 1024, "FT" )
    monitor.FTCalibration       =  ( 100,    0.0,  50000.0, 0, 1024, "FT" )
    monitor.FTError             =  ( 100,    0.0,  20000.0, 0, 1024, "FT" )

    monitor.FileID              =  (  -1,    0.0,  10000.0, 0,   -1, "LHCb" )
    monitor.GaudiHeader         =  (  -1,    0.0,  10000.0, 0,   -1, "LHCb" )
    monitor.GaudiSerialize      =  (  -1,    0.0,  10000.0, 0,   -1, "LHCb" )
    monitor.HC                  =  ( 100,    0.0,  10000.0, 0, 1024, "Unknown" )
    monitor.HCError             =  ( 100,    0.0,  10000.0, 0, 1024, "Unknown" )
    monitor.HLTRatio            =  ( 100,    0.0,      1.0, 0,   10, "LHCb" )
    monitor.HcalE               =  ( 100,    0.0,  10000.0, 0,  128, "HCal" )
    monitor.HcalPacked          =  ( 100, 1000.0,   3000.0, 0,   10, "HCal" )
    monitor.HcalPackedError     =  ( 100,    0.0,  10000.0, 0,  128, "HCal" )
    monitor.HcalTrig            =  ( 100,    0.0,  10000.0, 0,  128, "HCal" )

    monitor.HltDecReports       =  ( 100,    0.0,   1000.0, 0,   -1, "HLT" )
    monitor.HltLumiSummary      =  (   5,    0.0,    100.0, 0,   -1, "HLT" )
    monitor.HltRoutingBits      =  (  10,    0.0,    100.0, 0,   -1, "HLT" )
    monitor.HltSelReports       =  ( 500,    0.0, 100000.0, 0,   -1, "HLT" )
    monitor.HltTrackReports     =  ( 100,    0.0,  10000.0, 0,   -1, "HLT" )
    monitor.HltVertexReports    =  (  50,    0.0,    200.0, 0,   -1, "HLT" )

    monitor.IT                  =  (  -1,    0.0,  12000.0, 0,   80, "IT" )
    monitor.ITError             =  (  -1,    0.0,    600.0, 0,  128, "IT" )
    monitor.ITFull              =  (  -1,    0.0,  10000.0, 0,  128, "IT" )
    monitor.ITPedestal          =  (  -1,    0.0,  10000.0, 0,  128, "IT" )
    monitor.ITProcFull          =  (  -1,    0.0,  10000.0, 0,  128, "IT" )

    monitor.L0Calo              =  (  -1,    0.0,   1000.0, 0,    5, "L0" )
    monitor.L0CaloError         =  (  -1,    0.0,  10000.0, 0,  128, "L0" )
    monitor.L0CaloFull          =  (  -1,    0.0,  10000.0, 0,  128, "L0" )
    monitor.L0DU                =  (  -1,  100.0,    200.0, 0,    1, "L0" )
    monitor.L0DUError           =  (  -1,    0.0,  10000.0, 0,    1, "L0" )
    monitor.L0Muon              =  (  -1,    0.0,    100.0, 0,   10, "L0" )
    monitor.L0MuonCtrlAll       =  (  -1,    0.0,  10000.0, 0,  128, "L0" )
    monitor.L0MuonError         =  (  -1,    0.0,  10000.0, 0,  128, "L0" )
    monitor.L0MuonProcCand      =  (  -1,    0.0,   1000.0, 0,   10, "L0" )
    monitor.L0MuonProcData      =  (  -1,    0.0,   1400.0, 0,  128, "L0" )
    monitor.L0MuonRaw           =  (  -1,    0.0,  10000.0, 0,  128, "L0" )
    monitor.L0PU                =  (  -1,  200.0,    400.0, 0,    1, "L0" )
    monitor.L0PUError           =  (  -1,    0.0,  10000.0, 0,    1, "L0" )
    monitor.L0PUFull            =  (  -1, 3000.0,   4000.0, 0,    1, "L0" )

    monitor.Muon                =  ( 100,    0.0,   5000.0, 0,   20, "Muon" )
    monitor.MuonError           =  ( 100,    0.0,  10000.0, 0,  128, "Muon" )
    monitor.MuonFull            =  ( 100,    0.0,  10000.0, 0,  128, "Muon" )
    monitor.MuonSpecial         =  ( 100,    0.0,  10000.0, 0,  128, "Muon" )
    monitor.ODIN                =  (   5,    0.0,    100.0, 0,   -1, "Online" )

    monitor.OT                  =  (  -1,    0.0,  36000.0, 200,900, "OT" )
    monitor.OTError             =  (  -1,    0.0,  10000.0, 0,  128, "OT" )
    monitor.OTRaw               =  (  -1,    0.0,  10000.0, 200,900, "OT" )
    monitor.PrsE                =  (  -1,    0.0,  10000.0, 0,  128, "PRS" )
    monitor.PrsPacked           =  (  -1,    0.0,   6000.0, 0,   10, "PRS" )
    monitor.PrsPackedError      =  (  -1,    0.0,  10000.0, 0,  128, "PRS" )
    monitor.PrsTrig             =  (  -1,    0.0,  10000.0, 0,  128, "PRS" )

    monitor.Rich                =  ( 150,    0.0,  90000.0, 0,   32, "RICH" )
    monitor.RichError           =  ( 150,    0.0,  90000.0, 0,   32, "RICH" )
    monitor.RichCommissioning   =  ( 150,    0.0,  90000.0, 0,   32, "RICH" )
    monitor.TAEHeader           =  ( 100,    0.0,  10000.0, 0,  128, "Online" )

    monitor.TT                  =  (  -1,    0.0,  16000.0, 0,  128, "TT" )
    monitor.TTError             =  (  -1,    0.0,  10000.0, 0,  128, "TT" )
    monitor.TTFull              =  (  -1, 3000.0,   4000.0, 0,  128, "TT" )
    monitor.TTPedestal          =  (  -1,    0.0,  10000.0, 0,  128, "TT" )
    monitor.TTProcFull          =  (  -1,    0.0,  10000.0, 0,  128, "TT" )

    monitor.TestDet             =  ( 100,    0.0,  10000.0, 0,  128, "TDET" )
    monitor.TotEv               =  ( 500,    0.0, 500000.0, 0,   -1, "LHCb" )

    monitor.UT                  =  ( 100,    0.0,  30000.0, 0, 1024, "UT" )
    monitor.UTFull              =  ( 100,    0.0,  30000.0, 0, 1024, "UT" )
    monitor.UTError             =  ( 100,    0.0,  30000.0, 0, 1024, "UT" )
    monitor.UTPedestal          =  ( 100,    0.0,  50000.0, 0, 1024, "UT" )

    monitor.VL                  =  ( 100,    0.0,  10000.0, 0, 1024, "VP" )
    monitor.VP                  =  ( 100,    0.0,  50000.0, 0, 1024, "VP" )
    monitor.VPRetinaCluster     =  ( 100,    0.0,  50000.0, 0, 1024, "VP" )
    monitor.Velo                =  ( 200,    0.0,  35000.0, 0,  128, "Velo" )
    monitor.VeloError           =  (  50,    0.0,    500.0, 0,  128, "Velo" )
    monitor.VeloFull            =  ( 500,    0.0, 500000.0, 0,  128, "Velo" )
    monitor.VeloPedestal        =  ( 100,    0.0,  30000.0, 0,  128, "Velo" )
    monitor.VeloProcFull        =  ( 100,    0.0,  30000.0, 0,  128, "Velo" )

    monitor.Calo                =  ( 100,    0.0,  10000.0, 0, 1024, "Calo" )
    monitor.CaloError           =  ( 100,    0.0,  10000.0, 0, 1024, "Calo" )
    monitor.CaloSpecial         =  ( 100,    0.0,  10000.0, 0, 1024, "Calo" )
    monitor.Plume               =  ( 100,    0.0,   5000.0, 0,   -1, "Plume" )
    monitor.PlumeSpecial        =  ( 100,    0.0,   5000.0, 0,   -1, "Plume" )
    monitor.PlumeError          =  ( 100,    0.0,   5000.0, 0,   -1, "Plume" )

    self.enableUI()

    input            = self.setup_event_input()
    sequence         = GaudiOnline.Sequencer('Processor')
    sequence.Members = [input, self.updateAndReset, monitor]
    self.app.TopAlg  = [sequence]
    self.broker.DataProducers = self.app.TopAlg
    return self

application = RawSizeMon(outputLevel=OnlineEnv.OutputLevel,
                         partitionName=OnlineEnv.PartitionName,
                         partitionID=OnlineEnv.PartitionID,
                         classType=GaudiOnline.Class1)
#
application.setup_fifolog()
application.setup_monitoring('RawBankSizes')
application.setup_mbm_access('Events', True)
application.setup_hive(FlowManager("EventLoop"), 40)
application.config.expandTAE    = False
application.updateAndReset.saveHistograms = 1
application.updateAndReset.saverCycle     = 600
application.updateAndReset.saveSetDir     = "/hist/Savesets"
#
application.setup_algorithms()
#
# Mode slection::  synch: 0 async_queued: 1 sync_queued: 2
application.config.execMode            = 0
application.config.numEventThreads     = 1
#
application.config.MBM_requests = [
  'EvType=2;TriggerMask=0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF;VetoMask=0,0,0,0;MaskType=ANY;UserType=USER;Frequency=PERC;Perc=100.0'
]
print('Setup complete....')

