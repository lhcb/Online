#!/bin/bash
#
. /group/online/dataflow/scripts/preamble.sh;
. ${FARMCONFIGROOT}/job/createEnvironment.sh  $*;
#
setup_options_path MONITORING;
#
#export MBM_EVENT_EXPLORER=YES;
#
exec -a ${UTGID} genPython.exe `which gaudirun.py` ${RAWBANKSIZESROOT}/options/EventSizeMon.py --application=OnlineEvents;
