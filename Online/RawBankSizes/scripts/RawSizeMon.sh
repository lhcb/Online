#!/bin/bash
#
export CMTCONFIG=x86_64_v2-el9-gcc13-opt;
export CMTCONFIG=x86_64_v2-el9-gcc13-do0;
#
cd /group/online/dataflow/cmtuser/OnlineRelease;
. setup.${CMTCONFIG}.vars;
. ${FARMCONFIGROOT}/job/createEnvironment.sh  $*;
#
setup_options_path MONITORING;
#
exec -a ${UTGID} genPython.exe `which gaudirun.py` ${RAWBANKSIZESROOT}/options/RawSizeMon.py --application=OnlineEvents;
