//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : GaudiOnline
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include <Gaudi/Algorithm.h>
#include <GaudiKernel/IHistogramSvc.h>
#include <GaudiKernel/DataObjectHandle.h>
#include <EventData/bank_header_t.h>
#include <AIDA/IHistogram1D.h>

/// Forward declarations
namespace LHCb { class RawBank; }

///  Online namespace declaration
namespace Online {

  /// Small example algorithm histogramming the event size
  /**@class EventSize EventSize.cpp
   *
   * Accumulate the total event size and fill a histogram
   *
   * @author:  M.Frank
   * @version: 1.0
   */
  class GAUDI_API EventSize: public Gaudi::Algorithm   {

  private:
    typedef std::vector<std::pair<const LHCb::RawBank*, const void*> > lb_evt_data_t;

  private:
    DataObjectReadHandle<lb_evt_data_t> m_rawData {this,"RawData","Banks/RawData"};
    Gaudi::Property<std::string>        m_histPath{this,"HistogramPath", "EventSize/TotalSize","Place of histogram"};
    Gaudi::Property<int>                m_bins    {this,"Bins",     100, "Number of histogram bins"};
    Gaudi::Property<double>             m_low     {this,"Low",      0.0, "Low edge"};
    Gaudi::Property<double>             m_high    {this,"High",   200e3, "High edge"};
    AIDA::IHistogram1D* m_eveLen = 0;

  public:
    /// Standard Algorithm Constructor(s)
    using Algorithm::Algorithm;

    /// Algorithm overload: Initialize the algorithm
    StatusCode initialize() override {
      if ( !histoSvc()->findObject(m_histPath, m_eveLen).isSuccess() )  {
	m_eveLen = histoSvc()->book(m_histPath, "Total raw event size", m_bins, m_low, m_high);
	declareInfo("EventSize/TotalSize", m_eveLen, m_eveLen->title());
      }
      return StatusCode::SUCCESS;
    }

    /// Algorithm overload: Start the algorithm
    StatusCode start() override {
      return StatusCode::SUCCESS;
    }

    /// Algorithm overload: Finalize the algorithm
    StatusCode finalize() override {
      m_eveLen = 0;
      return StatusCode::SUCCESS;
    }

    /// Algorithm overload: Event execution routine
    StatusCode execute(const EventContext& /* context */ ) const override final {
      typedef std::vector<std::pair<const bank_header_t*, const void*> > evt_data_t;
      if ( !m_eveLen )   {
	error() << "Where the hack did my histogram go?" << endmsg;
	return StatusCode::FAILURE;
      }
      const evt_data_t* event = reinterpret_cast<evt_data_t*>(m_rawData.get());
      if ( event )   {
	std::size_t len = 0;
	std::for_each(std::begin(*event), std::end(*event),
		      [&len](const auto& b) { len += b.first->totalSize(); });
	m_eveLen->fill(double(len), 1.0);
	return StatusCode::SUCCESS;
      }
      error() << "No raw data object found at " << m_rawData << endmsg;
      return StatusCode::SUCCESS; // We want to continue!
    }
  };
}

DECLARE_COMPONENT( Online::EventSize )
