//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : GaudiOnline
//
//  Author     : Markus Frank
//==========================================================================

// C/C++ Include files
#include <utility>

// from Gaudi / Online
#include <EventData/odin_t.h>
#include <RawBankSizes/RawBankSizes.h>

// local
//-----------------------------------------------------------------------------
// Implementation file for class : RawBankSizes
//
// 2008-03-27 : Olivier Deschamps
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( Online::RawBankSizes )

using namespace Online;

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
RawBankSizes::RawBankSizes( const std::string& name,
			    ISvcLocator* pSvcLocator)
  : Algorithm( name , pSvcLocator )
{
  m_detectorNames.push_back("L0");
  m_detectorNames.push_back("PRS");
  m_detectorNames.push_back("ECal");
  m_detectorNames.push_back("HCal");
  m_detectorNames.push_back("Calo");
  m_detectorNames.push_back("Velo");
  m_detectorNames.push_back("RICH");
  m_detectorNames.push_back("VP");
  m_detectorNames.push_back("UT");
  m_detectorNames.push_back("FT");
  m_detectorNames.push_back("IT");
  m_detectorNames.push_back("TT");
  m_detectorNames.push_back("OT");
  m_detectorNames.push_back("Plume");
  m_detectorNames.push_back("Muon");
  m_detectorNames.push_back("HLT");
  m_detectorNames.push_back("Online");
  m_detectorNames.push_back("LHCb");
  m_detectorNames.push_back("TDET");
  m_detectorNames.push_back("Unknown");

  declareProperty( "bankNames"   , m_bankNames);
  declareProperty( "DumpOptions" , m_dumpOptions = false);
  declareProperty( "MaxSizeMap"  , m_max);
  declareProperty( "MaxSizeDef"  , m_def  = 500);
  declareProperty( "Bins"        , m_bin  = 100);
  declareProperty( "Profile"     , m_prof = true);

  // default bank types list == all banks !
  for(int i = 0 ; i != (int) bank_types_t::LastType; i++)   {
    std::string bname = event_print::bankType(i);
    if ( bname != "UNKNOWN" )   {
      m_bankNames.push_back( bname );
      declareProperty(bname,m_hparams[i]/*,"(0,0.0,0.0,0,-2)"*/);
      continue;
    }
    warning() << "RawBankSizes: Unknown bank type: " << i << endmsg;
  }
  declareProperty("TotEv",m_totevpar/*,"(0,0.0,0.0,0,-2)"*/);
  declareProperty("HLTRatio",m_HLTratiopar/*,"(0,0.0,0.0,0,-2)"*/);
  for (const auto& d : m_detectorNames )   {
    BankDescr *b = new BankDescr();
    b->init(d,true);
    HParam::HistParams *p = new HParam::HistParams();
    declareProperty("D_"+d,*p);
    m_detectors.insert(std::pair<std::string,BankDescr*>(d,b));
    m_dethparams.insert(std::pair<std::string,HParam::HistParams*>(d,p));
  }
  m_totsize.init("Total",false);
}
//=============================================================================
// Destructor
//=============================================================================
RawBankSizes::~RawBankSizes()
{
  for( auto& i : m_detectors )
    delete i.second;
  for( auto& i : m_dethparams )
    delete i.second;
  m_detectors.clear();
  m_dethparams.clear();
}

//=============================================================================
// Initialization
//=============================================================================
StatusCode RawBankSizes::initialize()  {
  auto& opt_svc = serviceLocator()->getOptsSvc();
  std::string prefix = name()+".";
  std::string title;

  if ( m_dumpOptions )   {
    const auto& props = getProperties();
    for(const auto* p : props)    {
      std::string val = p->toString();
      always() << prefix << p->name() << "  =  " << val 
	       << " [" << System::typeinfoName(*p->type_info()) << "]  "
	       << opt_svc.get(prefix+p->name())
	       << endmsg;
    }
  }
  // convert bankNames to bankTypes
  for( const auto& bn : m_bankNames )  {
    bool found = false;
    for( int i = 0 ; i != (int)bank_types_t::LastType; i++ )    {
      std::string bname = event_print::bankType(i);
      if( bname == bn)  {
        m_bankTypes.push_back(i);
        found = true;
	break;
      }
    }
    if( !found ) warning() << "Requested bank '" << bn << "' is not a valid name" << endmsg;
  }
  for( int i = 0; i != bank_types_t::LastType; i++ )   {
    m_banks[i].init(i, (m_hparams[i].s_max < 0));
    if ( m_hparams[i].n_bin == -2 )   {
      std::string bt = event_print::bankType(i);
      warning() << "No Histogram Options for Bank " << bt << endmsg;
    }
    m_banks[i].nohist    = (m_hparams[i].n_bin <= 0);
    m_banks[i].noprofile = m_banks[i].nohist || (m_hparams[i].s_max <= 0);
  }
  for( int i = 0; i != (int)bank_types_t::LastType; i++ )   {
    auto& dsc = m_banks[i];
    auto& par = m_hparams[i];

    if ( !dsc.nohist )      {
      double binw = (par.xmax - par.xmin) / par.n_bin;
      dsc.xmin = par.xmin;
      dsc.xmax = par.xmax;
      dsc.binw = binw;
      if ( ((int)binw % 4) != 0 )  {
	info() << "Binwidth for " << dsc.name << " not a multiple of 4. " << binw << endmsg;
      }
      dsc.nbin = par.n_bin;
      for (int hindx = 0; hindx < HISTPERHIST; hindx++)  {
	title = dsc.name + " Size in Bytes" + titqual[hindx];
	//error() << "Booking: " << title << endmsg;
	dsc.h[hindx]  = histoSvc()->book( dsc.h_name[hindx], title, par.n_bin, par.xmin, par.xmax);
	declareInfo(dsc.h_name[hindx], dsc.h[hindx],title);
      }
    }
    else   {
      info() << "No Histogram booked for Bank " << dsc.name << endmsg;
    }
    if ( !dsc.noprofile )	{
      int idx = (dsc.isError) ? dsc.rootbankidx : i;
      auto& p = m_hparams[idx];
      for (int hindx=0; hindx < HISTPERHIST; hindx++ )    {
	title = dsc.name + " Size vs. SourceID " + titqual[hindx];
	dsc.p[hindx]  = histoSvc()->bookProf(dsc.p_name[hindx], title, p.s_max-p.s_min+1, (float)p.s_min, (float)p.s_max);
	declareInfo(dsc.p_name[hindx], dsc.p[hindx], title);
      }
    }
    else   {
      info() << "No Profile booked for Bank " << dsc.name << endmsg;
    }
  }

  m_totsize.xmin = m_totevpar.xmin;
  m_totsize.xmax = m_totevpar.xmax;
  m_totsize.nbin = m_totevpar.n_bin;
  m_totsize.binw = (m_totsize.xmax-m_totsize.xmin)/m_totsize.nbin;

  if (((int)m_totsize.binw % 4) != 0)   {
    // Beat: This is wrong and was m_banks[i], which clearly accesses bad memory!
    printf("Binwidth for %s not a multiple of 4. %f\n",m_banks[0].name.c_str(),m_totsize.binw);
  }
  for(int hindx = 0; hindx<HISTPERHIST; hindx++ )   {
    title = "Total Event Size in Bytes"+titqual[hindx];
    h_totev[hindx] = histoSvc()->book("TotSize"+namqual[hindx], title,
				      m_totevpar.n_bin, m_totevpar.xmin,m_totevpar.xmax);
    declareInfo("TotSize"+namqual[hindx], h_totev[hindx], title);
    title = "Ratio HLT Total Size"+titqual[hindx];
    h_HLTratio[hindx] = histoSvc()->book( "HLTRatio"+namqual[hindx],title,m_HLTratiopar.n_bin,
					  m_HLTratiopar.xmin,m_HLTratiopar.xmax);
    declareInfo("HLTRatio"+namqual[hindx], h_HLTratio[hindx],title);
    title = "Bank Size vs. Bank Number"+titqual[hindx];
    p_banks[hindx] = histoSvc()->bookProf("Banksp"+namqual[hindx], title,
					  bank_types_t::LastType+1, -1.0, (float)bank_types_t::LastType);
    declareInfo("Banksp"+namqual[hindx],p_banks[hindx], title);
  }
  for (const auto& j : m_dethparams )   {
    detbmiter bd = m_detectors.find(j.first);
    if ( j.second->n_bin > 0 )   {
      bd->second->xmin = j.second->xmin;
      bd->second->xmax = j.second->xmax;
      bd->second->nbin = j.second->n_bin;
      bd->second->binw = (j.second->xmax-j.second->xmin)/j.second->n_bin;
      if (((int)bd->second->binw % 4) != 0)    {
	// Beat: This is wrong and was m_banks[i], which clearly accesses bad memory!
	info() << "Binwidth for " << m_banks[0].name << " not a multiple of 4. " << bd->second->binw << endmsg;
      }
      for (int hindx=0; hindx<HISTPERHIST; hindx++)    {
	title = "Total Size for "+j.first+" in Bytes"+titqual[hindx];
	bd->second->h[hindx] = histoSvc()->book("Total_"+j.first+namqual[hindx], title,
						j.second->n_bin, j.second->xmin,j.second->xmax);
	declareInfo("Total_"+j.first+namqual[hindx], bd->second->h[hindx], title);
      }
    }
  }
  return StatusCode::SUCCESS;
}

//=============================================================================
// Finalization
//=============================================================================
StatusCode RawBankSizes::finalize()  {
  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode RawBankSizes::execute(const EventContext& /* context */)  const {
  const evt_data_t* event = reinterpret_cast<evt_data_t*>(m_rawData.get());
  int runno = 100;
  if ( !event )   {
    warning() << "Raw data not found at location '" << m_rawData << endmsg;
    return StatusCode::SUCCESS;
  }
  for( const auto& b : *event )   {
    const auto* hdr = b.first;
    if ( hdr->type() == bank_types_t::ODIN )   {
      auto* This = const_cast<RawBankSizes*>(this);
      if ( hdr->version() < 7 )    {
	const auto* odin = (const run2_odin_t*)b.second;
	runno = odin->run_number();
      }
      else   {
	const auto* odin = (const run3_odin_t*)b.second;
	runno = odin->run_number();
      }
      return This->fill_histos(*event, runno);
    }
  }
  /// No ODIN Bank. Contrinue execution
  return StatusCode::SUCCESS;
}

StatusCode RawBankSizes::fill_histos(const evt_data_t& data, int /* runno */)   {
  size_t tot_sum = 0;
  bool   nolumi = true;
  std::lock_guard<std::mutex> lock(m_mutex);

  for( const auto& j : m_detectors )
    j.second->sum = 0.0;

  for( const auto& type : m_bankTypes )
    m_banks[type].siz = 0.0;

  for( const auto& dsc : data )  {
    const auto* b = dsc.first;
    if ( b->type() == bank_types_t::HltRoutingBits )   {
      const auto* trMask = (const unsigned int*)dsc.second;
      if ( (trMask[1] & 0x10000) != 0 )   {
        nolumi = false;
      }
      break;
    }
  }
  for( const auto& dsc : data )  {
    const bank_header_t* b = dsc.first;
    int    id  = b->type();
    double siz = b->size();
    double src = b->sourceID();
    tot_sum += siz;
    m_banks[id].siz += siz;
    if ( !m_banks[id].noprofile )   {
      m_banks[id].p[0             ]->fill(src, siz);
      m_banks[id].p[nolumi ? 1 : 2]->fill(src, siz);
    }
  }

  for( auto typ : m_bankTypes )  {
    auto& bnk = m_banks[typ];
    if ( bnk.siz > 0.0 )   {
      double siz = bnk.siz;
      if ( !bnk.nohist )   {
        bnk.h[0]->fill(siz);
	bnk.h[nolumi ? 1 : 2]->fill(siz);
      }
      p_banks[0]->fill(typ, bnk.siz);
      p_banks[nolumi ? 1 : 2]->fill(typ, bnk.siz);
    }
    const std::string& d = m_hparams[typ].det;
    detbmiter j = m_detectors.find(d);
    j->second->sum += m_banks[typ].siz;
  }

  for (detbmiter j=m_detectors.begin(); j != m_detectors.end(); j++)  {
    double x = j->second->sum;
    if ( x > 0 )   {
      j->second->h[0             ]->fill(x);
      j->second->h[nolumi ? 1 : 2]->fill(x);
    }
  }

  double total = tot_sum;
  if ( total > 0 )  {
    detbmiter j   = m_detectors.find("HLT");
    double    rat = j->second->sum/total;
    h_totev[0]->fill(total);
    h_HLTratio[0]->fill(rat);
    h_totev[nolumi ? 1 : 2]->fill(total);
    h_HLTratio[nolumi ? 1 : 2]->fill(rat);
  }
  return StatusCode::SUCCESS;
}
