from __future__ import print_function
from builtins import range
import os
from cppyy import gbl

gbl.gInterpreter.Declare("#include <Event/RawBank.h>")
RawBank = gbl.LHCb.RawBank
banks = []
for t in dir(RawBank):
  a = getattr(RawBank, t)
  if type(a) == int and a != RawBank.MagicPattern:
    banks.append((a, t))
banks = [e[1] for e in sorted(banks, key = lambda e: e[0])[:-1]]

aaaa = os.getenv("RAWBANKSIZESROOT")
#print aaaa
g=open(aaaa+"/options/RawSize_HistParams.opts","r+")
banks.append("TotEv")
banks.append("HLTRatio");
opts = g.readlines()
opts.append("shit")
newbanks = []
outopts = []
#print banks
#print opts
print("========= Running WriteOptions.py....")
#print (len(banks), len(opts))
for i in range(len(opts)):
 opts[i] = opts[i].replace("//","")
for i, bank in enumerate(banks):
  found = False
  for j in range(len(opts)):
    if opts[j].find("//")<0:
      #print opts[j], '=====', opts[j].split("=")[0]
      items = opts[j].split("=")[0].split(".")
      if len(items)<2: continue
      optbank = items[1]
      ii=opts[j].find(bank)
      if bank==optbank:
        #print ("options for bank "+banks +" "+optbank+" found. Adding to outopts")
        outopts.append(opts[j])
        opts.remove(opts[j])
        found = True
        break
  if found:
    continue
  else:
    print(("No options found for bank ",bank," ",optbank," adding default options"))
    outopts.append("RawBankSizes."+banks[i]+"=(100,0.0,10000.0,0,1024,\"Unknown\");"+"\r\n")
opts.remove("shit") #len(opts)-1)
for i in range(len(opts)):
 opts[i] = "//"+opts[i]
#print newbanks

outopts.sort()
#print(opts)
g.seek(0)
g.truncate()
g.writelines(outopts)
g.writelines(opts)
g.close()
