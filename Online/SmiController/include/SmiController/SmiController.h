//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_CONTROLLER_SMI_CONTROLLER_H
#define ONLINE_CONTROLLER_SMI_CONTROLLER_H

//// Framework include files
#include <SmiController/config.h>
#include <CPP/Interactor.h>
#include <RTL/Logger.h>

/// C/C++ include files
#include <thread>
#include <memory>
#include <vector>
#include <map>

///  FiniteStateMachine namespace declaration
namespace FiniteStateMachine   {

  /// Forward declarations
  class SmiTask;

  /**@class SMiController  SmiController.h Controller/SmiController.h
   *
   * \author  M.Frank
   * \date    01/03/2013
   * \version 0.1
   */
  class SmiController : public CPP::Interactor  {
  public:
    enum ControllerCommands  {
      CONTROLLER_CONFIG = 1,
      CONTROLLER_INITIALIZE = 1,
      CONTROLLER_LOAD_TASKS,
      CONTROLLER_EXIT,
      CONTROLLER_PUBLISH_TASKS,
      CONTROLLER_TASKS_DEAD,
      CONTROLLER_SETSTATE,
      CONTROLLER_SETNODESTATE,
      CONTROLLER_INVOKE_TRANSITION,
      LAST
    };
    enum SimConfig   {
      SIM_SMI_CTRL = 1<<0,
      SIM_SMI_TASKS = 1<<1,
      LAST_RESPONSE
    };

    /// Helper class to handle DIM errors
    /**
     * \author  M.Frank
     * \date    01/03/2013
     * \version 0.1
     */
    class CtrlDimErrors;

    /// Helper class to represent the SMI proxy object of the controller
    /**
     * \author  M.Frank
     * \date    01/03/2013
     * \version 0.1
     */
    class CtrlProxy;

    /// Helper class to represent SMI proxy objects of physical dependent tasks
    /**
     * \author  M.Frank
     * \date    01/03/2013
     * \version 0.1
     */
    class TaskProxy;

    /// Helper class to represent the SMI object set handler
    /**
     * \author  M.Frank
     * \date    01/03/2013
     * \version 0.1
     */
    class TaskSet;
    class NodeProxy;

    /** Helper class to pass the controller configuration at startup
     *
     * \author  M.Frank
     * \date    01/03/2013
     * \version 0.1
     */
    struct config_t   {
      /// Controller's utgid
      std::string name;
      /// DNS domain ( defaults to ENV(DIM_DNS_NODE) )
      std::string dns;
      /// DIM task manager domain name (tmsSrv DNS)
      std::string tms_dns;
      /// Partition name
      std::string partition;
      /// Path to the architecture file
      std::string architecture;
      /// Path to the runinfo file
      std::string runinfo;
      /// Service name to retrieve number of instances
      std::string num_thread_svc;
      /// SMI Domain name of the controller
      std::string smi_domain;
      /// SMI file describing the FSM
      std::string smi_file;
      /// SMI DNS node
      std::string smi_dns;
      /// SMI debug level if smiSM is in-process or forked.
      std::string smi_debug  {"1"};
      /// UTGID of SMI process
      std::string smi_utgid;
      /// SMI script path to run SMI as a shell (standalone=3)
      std::string smi_script;
      /// SMI log file name (if requested)
      std::string smi_log_file;
      /// stdin and stderr output files
      std::string stdout_file, stderr_file;
      /// Replacement items
      std::map<std::string, std::string> replacements;
      /// Number of active worker processes on the node
      int         num_workers  = -1;
      /// Flag to steer CPU binding
      int         bind_cpus    = 0;
      /// Flag to steer standalone execution
      int         standalone   = 0;
      /// Output level for debug and error messages
      int         output_level = LIB_RTL_WARNING;
      /// PID of smiSM process if forked
      int         pid_smiSM = 0;
    public:
      /// Default constructor
      config_t() = default;
      /// Default move constructor
      config_t(config_t&& copy) = default;
      /// Default copy constructor
      config_t(const config_t& copy) = default;
      /// Default copy assignment
      config_t& operator=(config_t&& copy) = default;
      /// Default move assignment
      config_t& operator=(const config_t& copy) = default;
      /// Fully qualified domain string (config domain + "_" + host name)
      std::string smiDomain()  const;
    }  config;

    struct monitor_t  {
      unsigned long lastCmd, doneCmd;
      int   pid;
      char  targetState, state, metaState, pad;
      int   partitionID;
    } m_monitor;

    typedef std::map<std::string, std::unique_ptr<TaskProxy> > DependentTasks;

    /// Name of the current state
    std::string                     m_state;
    /// Target state name
    std::string                     m_target_state;
    /// Name of the processing node state
    std::string                     m_node_state;

    /// Reference to the output logger
    std::unique_ptr<RTL::Logger>    m_log;
    /// SMI controller thread (in process or out of process)
    std::unique_ptr<std::thread>    m_runner;
    /// Reference to SMI object manipulating the taskset
    std::unique_ptr<TaskSet>        m_taskset;
    /// Reference to SMI object manipulating a single node
    std::unique_ptr<TaskSet>        m_nodeFSM;
    /// Reference to SMI object manipulating the controller itself
    std::unique_ptr<CtrlProxy>      m_self;
    /// Reference to SMI object representing the node's state
    std::unique_ptr<NodeProxy>      m_nodeProxy;
    /// Reference to DIM error handler
    std::unique_ptr<CtrlDimErrors>  m_dimErrors;

    class DfTaskProxy;
    std::unique_ptr<DfTaskProxy>    m_defTask;
    std::unique_ptr<DfTaskProxy>    m_vipTask;

    /// Reference to SMI proxy representing the controlled tasks
    DependentTasks                  m_tasks;

    /// Variable to publish the task information
    std::string                     m_task_info;

    /// Variable to publish the instance information
    long                            m_instance_info      = 0;

    long                            m_tms_dic_dns_ID     = 0;
    long                            m_smi_dic_dns_ID     = 0;
    long                            m_smi_dis_dns_ID     = 0;
    long                            m_local_dic_dns_ID   = 0;
    long                            m_local_dis_dns_ID   = 0;

    /// Pointer to the dim service receiving commands
    int                             m_command_ID         = 0;
    /// Pointer to the secondary dim service receiving commands
    int                             m_userCmd_ID         = 0;
    /// Pointer to the dim service serving the FSM state
    int                             m_status_ID          = 0;
    /// Pointer to the dim service serving the FSM state
    int                             m_state_ID           = 0;
    /// Pointer to the dim service serving the FSM sub status information
    int                             m_sub_status_ID      = 0;
    /// Pointer to the dim service publishing the instance information
    int                             m_fsm_instance_ID    = 0;
    /// Pointer to DIM command to force publishing new instance information
    int                             m_fsm_instance_CMDID = 0;
    /// Pointer to the dim service publishing the task information
    int                             m_fsm_tasks_ID       = 0;
    /// Pointer to dim client with maximum instance tag
    int                             m_num_worker_ID      = 0;
    /// Pointer to dim client with node state
    int                             m_node_state_ID      = 0;
    /// Maximal number of active threads on the node
    int                             m_num_worker_threads = 1;

    /// Variable to check initialization
    bool                            m_inited             = false;
    bool                            m_smi_started        = false;

    /// Basic controller configuration: chains initialize() and start_smi()
    void configure();
    /// Controller configuration
    void initialize();
    /// SMI startup
    void start_smi();

    /// Transition action: callback on "load"
    void load_tasks();
    /// Transition action: callback on "kill"
    void kill_tasks();
    /// Transition action: callback on "destroy". Ensures all dependent tasks are killed.
    void force_kill_tasks();

    /// Declare process state to DIM service
    void declare_state(const std::string& new_state, const std::string& opt="");
    /// Declare FSM sub-state
    void declare_sub_state(int new_state);

    /// Handler to fake dependent task response(s) in simulation mode
    void simulate(const std::string& cmd)   const;

    /// Check if all dependent tasks have answered the request and - if yes - complete the transition
    void check_for_completion();

    std::vector<const char*> setup_smi_child_env();

  public:
    /// Default constructor
    SmiController(const config_t& config);

    /// Default destructor
    virtual ~SmiController();

    /// Reference to the output logger
    RTL::Logger& logger()  {  return *m_log;    }

    /// Interactor Interrupt handling routine
    void handle(const CPP::Event& ev) override;

    /// Configure and initialize the controller
    bool attach_tasks(std::map<std::string, SmiTask*>&& tasks);

    /// Invoke single transition request on SMI machinery
    SmiErrCond invoke_transition(const std::string& cmd);

    /// Access the currently declared state
    const std::string& state() const {  return m_state; }

    /// Access the target state during a transition
    const std::string& target_state() const {  return m_target_state; }

    /// Set the target state
    void set_target_state(const std::string& new_state);

    /// Publish state information when transition is completed
    void publish_info();

    /// Adjust the number of dependent tasks to be controlled (ignore overcounted dependent tasks)
    void publish_instances(int max_tag);

    /// Publish state information of the dependent tasks
    void publish_tasks();
    
    /// Run the process and start the SMI engine
    void run();
  };
}      //  End namespace
#endif //  ONLINE_FINITESTATEMACHINE_SMICONTROLLER_H
