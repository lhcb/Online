//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Author     : Markus Frank
//==========================================================================
//
//      Finite state machine implementation to control
//      and manipulate process groups
//
//==========================================================================
#ifndef ONLINE_FINITESTATEMACHINE_TASK_H
#define ONLINE_FINITESTATEMACHINE_TASK_H

// Framework include files
#include <SmiController/config.h>

// C/C++ include files
#include <vector>
#include <string>
#include <map>

/*
 *  FiniteStateMachine namespace declaration
 */
namespace FiniteStateMachine  {

  /**@class SmiTask  SmiTask.h Ctrl/SmiTask.h
   *
   * @author  M.Frank
   * @date    01/03/2013
   * @version 0.1
   */
  class SmiTask  {
  public:

    /// Definition of the timeout table
    typedef std::map<std::string,int> TimeoutTable;

    /// Task's nick name
    std::string               nick;
    /// Task's UTGID
    std::string               utgid;
    /// Timeout table for various transitions
    TimeoutTable              timeouts;
    /// String with start command
    std::string               command;
    /// Pointer array with process arguments (to be set by the sub-class)
    std::vector<std::string>  argv;
    /// Pointer array with environment variables (to be set by the sub-class)
    std::vector<std::string>  envp;
    /// Pointer array with process arguments (to be set by the sub-class)
    std::vector<std::string>  fmcArgs;
    /// Flag if the task should not be started, only controlled
    bool                      doStart {true};
    /// Flag to indicate this is a VIP task
    bool                      isVIP   {false};

  public:
    /// Class Constructor
    SmiTask(const std::string& nick_name, const std::string& utgid);
    /// Standatrd destructor
    virtual ~SmiTask() = default;
    /** Task manager handling  */
    /// Clone the environment of the existing process
    void clone_env();
    /// Set the process arguments from single string
    void set_args(const std::string& args);
    /// Add entries to the process arguments from single string
    void add_args(const std::string& args);
    /// Set the FMC arguments from single string
    void set_fmc_args(const std::string& args);
    /// Add the FMC arguments from single string
    void add_fmc_args(const std::string& args);

    /** Timer and timeout handling   */
    /// Add entry in transition timeout table (timeout in seconds)
    void add_timeout(const std::string& transition, int tmp);
    /// Remove entry in transition timeout table
    void remove_timeout(const std::string& transition);
  };   //  End class State
}      //  End namespace
#endif //  ONLINE_FINITESTATEMACHINE_TASK
