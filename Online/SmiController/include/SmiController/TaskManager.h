//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Author     : Markus Frank
//==========================================================================
//
//      Finite state machine implementation to control
//      and manipulate process groups
//
//==========================================================================
#ifndef ONLINE_FINITESTATEMACHINE_TASKMANAGER_H
#define ONLINE_FINITESTATEMACHINE_TASKMANAGER_H

// Framework include files

// C/C++ include files
#include <string>

/// FiniteStateMachine namespace declaration
namespace FiniteStateMachine {

  /**@class TaskManager  TaskManager.h Ctrl/TaskManager.h
   *
   * @author  M.Frank
   * @date    01/03/2013
   * @version 0.1
   */
  class TaskManager   {
  protected:
    /// Copy constructor
    TaskManager(const TaskManager& c) = default;
    /// Assignment operator
    TaskManager& operator=(const TaskManager& c) = default;

  public:
    /// Standard constructor
    TaskManager() = default;
    /// Standard destructor
    virtual ~TaskManager() = default;

    /// Instance accessor
    static TaskManager& instance(const std::string& node, long domain=0);
    /// Set task manager type
    static void set_type_default(const std::string& type);


    /// Start a process
    virtual int start(const std::string& utgid, const std::string& fmc_args, const std::string& cmd, const std::string& args) = 0;
    /// Kill a process
    virtual int stop(const std::string& utgid, int sig_num, int wait_before_kill) = 0;
    /// Kill a process
    virtual int kill(const std::string& utgid, int sig_num) = 0;
  };   //  End class State
}      //  End namespace 
#endif //  ONLINE_FINITESTATEMACHINE_TASKMANAGER
