//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Author     : Markus Frank
//==========================================================================
//
//      Finite state machine implementation to control
//      and manipulate process groups
//
//==========================================================================
#ifndef ONLINE_FINITESTATEMACHINE_TASKMANAGERFMC_H
#define ONLINE_FINITESTATEMACHINE_TASKMANAGERFMC_H

// Framework include files
#include <SmiController/TaskManager.h>

// C/C++ include files
#include <string>

/// FiniteStateMachine namespace declaration
namespace FiniteStateMachine {

  /**@class TaskManagerFMC  TaskManagerFMC.h Ctrl/TaskManagerFMC.h
   *
   * @author  M.Frank
   * @date    01/03/2013
   * @version 0.1
   */
  class TaskManagerFMC : public TaskManager  {
  protected:
    /// Node name
    std::string m_node;
    /// Name of the DIM command to start a process
    std::string m_start;
    /// Name of the DIM command to stop a process
    std::string m_stop;
    /// Name of the DIM command to kill a process
    std::string m_kill;
    /// DIM domain ID
    long        m_domain = -1;
    
    /// Copy constructor
    TaskManagerFMC(const TaskManagerFMC& c) = default;
    /// Assignment operator
    TaskManagerFMC& operator=(const TaskManagerFMC& c) = default;
  public:
    /// Standard constructor
    TaskManagerFMC(const std::string& node, long domain);
    /// Standard destructor
    virtual ~TaskManagerFMC() = default;

    /// Start a process
    virtual int start(const std::string& utgid, const std::string& fmc_args, const std::string& cmd, const std::string& args) override;
    /// Kill a process
    virtual int stop(const std::string& utgid, int sig_num, int wait_before_kill)  override;
    /// Kill a process
    virtual int kill(const std::string& utgid, int sig_num) override;
  };   //  End class State
}      //  End namespace 
#endif //  ONLINE_FINITESTATEMACHINE_TASKMANAGERFMC
