//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Author     : Markus Frank
//==========================================================================
//
//      Finite state machine implementation to control
//      and manipulate process groups
//
//==========================================================================
#ifndef ONLINE_FINITESTATEMACHINE_TASKMANAGERFORK_H
#define ONLINE_FINITESTATEMACHINE_TASKMANAGERFORK_H

// Framework include files
#include <SmiController/TaskManager.h>
#include <RTL/Process.h>

// C/C++ include files
#include <map>
#include <string>
#include <memory>

/// FiniteStateMachine namespace declaration
namespace FiniteStateMachine {

  /**@class TaskManagerFork  TaskManagerFork.h Ctrl/TaskManagerFork.h
   *
   * @author  M.Frank
   * @date    01/03/2013
   * @version 0.1
   */
  class TaskManagerFork : public TaskManager  {
  protected:

    typedef std::map<std::string, std::unique_ptr<RTL::Process> > Processes;
    Processes   m_processes;
    std::string m_node;

    /// Copy constructor
    TaskManagerFork(const TaskManagerFork& c) = default;
    /// Assignment operator
    TaskManagerFork& operator=(const TaskManagerFork& c) = default;

  public:
    /// Standard constructor
    TaskManagerFork(const std::string& node);
    /// Standard destructor
    virtual ~TaskManagerFork() = default;

    /// Start a process
    virtual int start(const std::string& utgid, const std::string& fmc_args, const std::string& cmd, const std::string& args) override;
    /// Kill a process
    virtual int stop(const std::string& utgid, int sig_num, int wait_before_kill)  override;
    /// Kill a process
    virtual int kill(const std::string& utgid, int sig_num) override;
  };   //  End class State
}      //  End namespace 
#endif //  ONLINE_FINITESTATEMACHINE_TASKMANAGERFORK
