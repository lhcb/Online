! ----------------------------------------------------------------------
!   Manager class definition
!
!   M.Frank
! ----------------------------------------------------------------------
class: Manager_CLASS
!panel: 
    state: RUNNING	!color: FwStateOKPhysics
        action: AddTask(string UTGID = "")	!visible: 1
            create_object &VAL_OF_UTGID of_class Task_CLASS
            insert &VAL_OF_UTGID in DEF_SETSTATES
            insert &VAL_OF_UTGID in DEF_SETACTIONS

        action: AddVIP(string UTGID = "")	!visible: 1
            create_object &VAL_OF_UTGID of_class Task_CLASS
            insert &VAL_OF_UTGID in VIP_SETSTATES
            insert &VAL_OF_UTGID in VIP_SETACTIONS

        action: RemoveTask(string UTGID = "")	!visible: 1
            remove &VAL_OF_UTGID from DEF_SETSTATES
            remove &VAL_OF_UTGID from DEF_SETACTIONS
            destroy_object &VAL_OF_UTGID of_class Task_CLASS

        action: RemoveVIP(string UTGID = "")	!visible: 1
            remove &VAL_OF_UTGID from VIP_SETSTATES
            remove &VAL_OF_UTGID from VIP_SETACTIONS
            destroy_object &VAL_OF_UTGID of_class Task_CLASS

        action: RemoveController(string UTGID = "")	!visible: 1
            remove &VAL_OF_UTGID from CONTROLLER_SETSTATES
            remove &VAL_OF_UTGID from CONTROLLER_SETACTIONS
            destroy_object &VAL_OF_UTGID of_class Controller_CLASS

! ----------------------------------------------------------------------
!   Controller class definition
!
!   M.Frank
! ----------------------------------------------------------------------
class: Controller_CLASS/associated
!panel: Controller.pnl
    state: UNKNOWN	    !color: FwStateAttention2
        action: Create	    !visible: 1
        action: Destroy	    !visible: 1
        action: Kill	    !visible: 1
        action: Reset	    !visible: 1
        action: Reconnect   !visible: 1
    state: OFFLINE	    !color: FwStateAttention2
        action: Load	    !visible: 1
        action: Kill	    !visible: 1
        action: Destroy	    !visible: 1
        action: Reconnect   !visible: 1

    state: NOT_READY	    !color: FwStateAttention1
        action: Configure   !visible: 1
        action: Load	    !visible: 1
        action: Reset	    !visible: 1
        action: Kill	    !visible: 1
        action: Unload	    !visible: 1
        action: Destroy	    !visible: 1

    state: READY	    !color: FwStateOKNotPhysics
        action: Start	    !visible: 1
        action: Reset	    !visible: 1
        action: Kill	    !visible: 1
        action: Load	    !visible: 1
        action: Destroy	    !visible: 1

    state: PAUSED	    !color: FwStateOKNotPhysics
        action: Stop	    !visible: 1
        action: Continue    !visible: 1
        action: Reset	    !visible: 1
        action: Kill	    !visible: 1
        action: Destroy	    !visible: 1

    state: RUNNING	    !color: FwStateOKPhysics
        action: Stop	    !visible: 1
        action: Pause	    !visible: 1
        action: Reset	    !visible: 1
        action: Kill	    !visible: 1
        action: Load	    !visible: 1
        action: Destroy	    !visible: 1

    state: ERROR	    !color: FwStateAttention3
        action: Recover	    !visible: 1
        action: Kill	    !visible: 1
        action: Destroy	    !visible: 1

! ----------------------------------------------------------------------
!   Generic task class definition
!
!   M.Frank
! ----------------------------------------------------------------------
class: Task_CLASS/associated
!panel: Task.pnl
    state: UNKNOWN	    !color: FwStateAttention2
        action: Create	    !visible: 1
        action: Reset	    !visible: 1
        action: Unload	    !visible: 1
        action: Kill	    !visible: 1
        action: Check	    !visible: 1

    state: OFFLINE	    !color: FwStateAttention2
        action: Create	    !visible: 1
        action: Reset	    !visible: 1
        action: Unload	    !visible: 1
        action: Kill	    !visible: 1
        action: Check	    !visible: 1

    state: NOT_READY	    !color: FwStateAttention1
        action: Configure   !visible: 1
        action: Unload	    !visible: 1
        action: Kill	    !visible: 1
        action: Reset	    !visible: 1
        action: Check	    !visible: 1

    state: READY	    !color: FwStateOKNotPhysics
        action: Start	    !visible: 1
        action: Reset	    !visible: 1
        action: Kill	    !visible: 1
        action: Unload	    !visible: 1

    state: PAUSED	    !color: FwStateOKNotPhysics
        action: Stop	    !visible: 1
        action: Continue    !visible: 1
        action: Reset	    !visible: 1
        action: Kill	    !visible: 1

    state: RUNNING	    !color: FwStateOKPhysics
        action: Stop	    !visible: 1
        action: Pause	    !visible: 1
        action: Reset	    !visible: 1
        action: Kill	    !visible: 1

    state: ERROR	    !color: FwStateAttention3
        action: Recover	    !visible: 1
        action: Reset	    !visible: 1
        action: Stop	    !visible: 1
        action: Kill	    !visible: 1
        action: Unload	    !visible: 1

! ----------------------------------------------------------------------
!   Top node class definition
!
!   M.Frank
! ----------------------------------------------------------------------
class: TOP_Node_CLASS
!panel: StreamControl/UpgradeMonArch.pnl

     function: d0_create
            if ( any_in CONTROLLER_SETSTATES in_state  {OFFLINE,NOT_READY,READY,RUNNING} ) then
	      do Load all_in CONTROLLER_SETACTIONS
            endif
            if ( (all_in CONTROLLER_SETSTATES in_state {OFFLINE,NOT_READY,READY,RUNNING}) and 
	    	 (any_in VIP_SETSTATES in_state        {OFFLINE}) ) then
	      do Create all_in VIP_SETACTIONS
	    endif
            if ( (all_in CONTROLLER_SETSTATES in_state {OFFLINE,NOT_READY,READY,RUNNING}) and 
	    	 (all_in VIP_SETSTATES in_state        {OFFLINE,NOT_READY,READY,RUNNING}) and
	    	 (any_in DEF_SETSTATES in_state        {OFFLINE}) ) then
	      do Create all_in DEF_SETACTIONS
	    endif

	    if ( all_in ALLCHILDREN_SETSTATES in_state {NOT_READY,READY,RUNNING} ) then
              move_to NOT_READY
	    endif
  
    function: d0_configure
            do Configure all_in ALLCHILDREN_SETACTIONS
            if ( any_in ALLCHILDREN_SETSTATES in_state {UNKNOWN,OFFLINE,NOT_READY,ERROR} )  then
              move_to ERROR
            endif
            if ( all_in ALLCHILDREN_SETSTATES in_state {READY,RUNNING} ) then
              move_to READY
            endif
  
    function: d0_continue
            if ( all_in ALLCHILDREN_SETSTATES in_state READY )  then
              do Start all_in ALLCHILDREN_SETACTIONS
            endif
            if ( any_in ALLCHILDREN_SETSTATES in_state PAUSED ) then
              do Continue all_in ALLCHILDREN_SETACTIONS
            endif
            if ( all_in ALLCHILDREN_SETSTATES in_state RRUNNING )  then
               move_to RUNNING
            endif
  
    function: d0_stop
            if ( any_in ALLCHILDREN_SETSTATES in_state {PAUSED,RUNNING} ) then
              do Stop all_in ALLCHILDREN_SETACTIONS
            endif
            if ( any_in ALLCHILDREN_SETSTATES in_state ERROR ) then
              do Recover all_in ALLCHILDREN_SETACTIONS
	    endif
	      
    function: d0_pause
            if ( any_in ALLCHILDREN_SETSTATES in_state      RUNNING ) then
              do Pause all_in ALLCHILDREN_SETACTIONS
            endif
	    !!!!!!!   do Pause all_in ALLCHILDREN_SETACTIONS
            if ( all_in ALLCHILDREN_SETSTATES in_state     {READY,PAUSED} )  then
               move_to PAUSED
	    endif

    function: d0_kill
            do Kill all_in ALLCHILDREN_SETACTIONS
            if ( all_in ALLCHILDREN_SETSTATES in_state {UNKNOWN,OFFLINE} )  then
              do Destroy all_in CONTROLLER_SETACTIONS
            endif
            if ( all_in CONTROLLER_SETSTATES in_state {UNKNOWN,OFFLINE} )  then
              move_to OFFLINE
	    endif

    function: d0_reset
            if ( any_in ALLCHILDREN_SETSTATES in_state {PAUSED,RUNNING} )  then
              do Stop all_in ALLCHILDREN_SETACTIONS 
            endif
            if ( any_in ALLCHILDREN_SETSTATES not_in_state NOT_READY ) then
              do Reset all_in ALLCHILDREN_SETACTIONS
            endif
            if ( any_in ALLCHILDREN_SETSTATES not_in_state {UNKNOWN,OFFLINE} )  then
              do Unload all_in ALLCHILDREN_SETACTIONS
            endif
            do Kill all_in ALLCHILDREN_SETACTIONS
            if ( all_in ALLCHILDREN_SETSTATES in_state {UNKNOWN,OFFLINE} )  then
              do Destroy all_in CONTROLLER_SETACTIONS
            endif
            if ( all_in CONTROLLER_SETSTATES in_state {UNKNOWN,OFFLINE} )  then
              move_to OFFLINE
	    endif

    function: d0_destroy
            if ( any_in ALLCHILDREN_SETSTATES in_state {RUNNING,PAUSED} )  then
              do Stop all_in ALLCHILDREN_SETACTIONS 
            endif
            if ( any_in ALLCHILDREN_SETSTATES not_in_state {UNKNOWN,OFFLINE,NOT_READY} ) then
              do Reset all_in ALLCHILDREN_SETACTIONS
            endif
            if ( any_in ALLCHILDREN_SETSTATES not_in_state {UNKNOWN,OFFLINE} )  then
              do Unload all_in ALLCHILDREN_SETACTIONS
            endif
            do Kill all_in ALLCHILDREN_SETACTIONS
            if ( all_in ALLCHILDREN_SETSTATES in_state {UNKNOWN,OFFLINE} )  then
              do Destroy all_in CONTROLLER_SETACTIONS
            endif
            if ( all_in CONTROLLER_SETSTATES in_state {UNKNOWN,OFFLINE} )  then
              move_to OFFLINE
            endif

	    
    state: OFFLINE	!color: FwStateAttention1
        when ( any_in ALLCHILDREN_SETSTATES in_state ERROR                     ) move_to ERROR
        when ( any_in CONTROLLER_SETSTATES  in_state ERROR                     ) move_to ERROR
        when ( all_in ALLCHILDREN_SETSTATES in_state {RUNNING}                 )  move_to RUNNING
        when ( all_in ALLCHILDREN_SETSTATES in_state {READY,RUNNING}           )  move_to READY
        when ( all_in ALLCHILDREN_SETSTATES in_state {NOT_READY,READY,RUNNING} ) move_to NOT_READY

        action: Create	!visible: 1
	    call d0_create

	action: Stop	!visible: 1
	    call d0_destroy

	action: Reset	!visible: 1
	    call d0_reset

        action: Kill	!visible: 1
	    call d0_kill

	action: Destroy	!visible: 1
	    call d0_destroy


    state: NOT_READY	!color: FwStateAttention1
        when ( any_in ALLCHILDREN_SETSTATES in_state ERROR             ) move_to ERROR
        when ( any_in CONTROLLER_SETSTATES  in_state ERROR             ) move_to ERROR
        when ( all_in ALLCHILDREN_SETSTATES in_state {RUNNING}         )  move_to RUNNING
        when ( all_in ALLCHILDREN_SETSTATES in_state {READY,RUNNING}   )  move_to READY
        when ( any_in VIP_SETSTATES in_state {UNKNOWN,OFFLINE,ERROR}   ) move_to ERROR
        when ( any_in ALLCHILDREN_SETSTATES in_state {UNKNOWN,OFFLINE} ) move_to OFFLINE

        action: Configure	!visible: 1
	    call d0_configure

	action: Reset	!visible: 1
	    call d0_reset

	 action: Destroy	!visible: 1
	    call d0_destroy
	      
	 action: Kill	!visible: 1
	    call d0_kill

	    
    state: READY	!color: FwStateOKNotPhysics
        when ( any_in ALLCHILDREN_SETSTATES  in_state ERROR                       ) move_to ERROR
        when ( any_in CONTROLLER_SETSTATES   in_state ERROR                       ) move_to ERROR
        when ( all_in ALLCHILDREN_SETSTATES  in_state RUNNING                     ) move_to RUNNING
        when ( any_in VIP_SETSTATES          in_state {UNKNOWN,OFFLINE,ERROR}     ) move_to ERROR
        when ( (any_in ALLCHILDREN_SETSTATES in_state {UNKNOWN,OFFLINE,NOT_READY} ) and
	       (all_in ALLCHILDREN_SETSTATES in_state {UNKNOWN,OFFLINE,NOT_READY,READY,RUNNING}) ) move_to NOT_READY
        when ( any_in ALLCHILDREN_SETSTATES  in_state {NOT_READY}                 )  move_to NOT_READY

        action: Start	!visible: 1
            do Start all_in ALLCHILDREN_SETACTIONS
            if ( all_in ALLCHILDREN_SETSTATES in_state RUNNING )  then
              move_to RUNNING
            endif

        action: Reset	!visible: 1
	    call d0_reset

        action: Destroy	!visible: 1
	    call d0_destroy

        action: Kill	!visible: 1
	    call d0_kill
	    

    state: PAUSED	!color: FwStateOKNotPhysics
        when ( any_in ALLCHILDREN_SETSTATES in_state ERROR                     ) move_to ERROR
        when ( any_in CONTROLLER_SETSTATES  in_state ERROR                     ) move_to ERROR
        when ( any_in VIP_SETSTATES         in_state {UNKNOWN,OFFLINE,ERROR}   ) move_to ERROR
        when ( all_in ALLCHILDREN_SETSTATES in_state RUNNING                   ) move_to RUNNING
        when ( all_in ALLCHILDREN_SETSTATES in_state READY                     ) move_to READY
        when ( all_in ALLCHILDREN_SETSTATES in_state {RUNNING,READY}           ) move_to READY
        when ( all_in ALLCHILDREN_SETSTATES in_state {NOT_READY,READY,RUNNING} ) move_to NOT_READY
        when ( all_in ALLCHILDREN_SETSTATES in_state {UNKNOWN,OFFLINE,NOT_READY,READY} ) move_to OFFLINE

        action: Stop	!visible: 1
	    call d0_stop

        action: Pause	!visible: 1
	    call d0_pause

        action: Continue  !visible: 1
	    call d0_continue

        action: Reset	!visible: 1
	    call d0_reset

        action: Destroy	!visible: 1
	    call d0_destroy

        action: Kill	!visible: 1
	    call d0_kill
	    

    state: RUNNING	!color: FwStateOKPhysics
        when ( any_in ALLCHILDREN_SETSTATES in_state {ERROR,UNKNOWN,OFFLINE,NOT_READY,CREATED,READY} ) move_to ERROR
        when ( any_in CONTROLLER_SETSTATES  in_state ERROR  ) move_to ERROR
        when ( any_in ALLCHILDREN_SETSTATES in_state PAUSED ) move_to PAUSED
        when ( all_in ALLCHILDREN_SETSTATES in_state READY  ) move_to READY

        action: Stop	!visible: 1
	    call d0_stop

        action: Pause	!visible: 1
	    call d0_pause

        action: Reset	!visible: 1
	    call d0_reset

        action: Destroy	!visible: 1
	    call d0_destroy

        action: Kill	!visible: 1
	    call d0_kill

	    
    state: ERROR	!color: FwStateAttention3
        when ( any_in ALLCHILDREN_SETSTATES in_state ERROR                    ) stay_in_state
        when ( any_in CONTROLLER_SETSTATES  in_state ERROR                    ) stay_in_state
        when ( any_in DEF_SETSTATES         in_state {OFFLINE,UNKNOWN}        )  move_to NOT_READY
        when ( any_in CONTROLLER_SETSTATES  in_state {OFFLINE,UNKNOWN}        )  move_to NOT_READY
        when ( all_in ALLCHILDREN_SETSTATES in_state READY                    )  move_to READY
        when ( all_in ALLCHILDREN_SETSTATES in_state RUNNING                  )  move_to RUNNING
        when ( all_in ALLCHILDREN_SETSTATES in_state {RUNNING, READY}         )  move_to READY
        when ( all_in ALLCHILDREN_SETSTATES in_state {RUNNING, PAUSED}        )  move_to PAUSED
        when ( all_in ALLCHILDREN_SETSTATES in_state {RUNNING, READY, PAUSED} )  move_to PAUSED
        when ( all_in ALLCHILDREN_SETSTATES in_state {OFFLINE,UNKNOWN}        )  move_to NOT_READY

        action: Recover	!visible: 1
            if ( any_in VIP_SETSTATES in_state         {UNKNOWN,OFFLINE,ERROR} )  then
              do Destroy all_in CONTROLLER_SETSTATES
            endif
	    if (all_in ALLCHILDREN_SETACTIONS in_state {OFFLINE,UNKNOWN})  then
	      move_to OFFLINE
            endif
            do Recover all_in ALLCHILDREN_SETACTIONS
	    if (all_in ALLCHILDREN_SETACTIONS not_in_state ERROR)  then
	      move_to OFFLINE
            endif
 
        action: Reset	!visible: 1
	    call d0_reset

        action: Stop	!visible: 1
	    call d0_stop

        action: Kill	!visible: 1
	    call d0_kill


object:    ProcessingNode is_of_class TOP_Node_CLASS
object:    Manager is_of_class Manager_CLASS

object:    Controller is_of_class Controller_CLASS
objectset: CONTROLLER_SETSTATES  is_of_class VOID {Controller}
objectset: CONTROLLER_SETACTIONS is_of_class VOID {Controller}

object:    DefTask is_of_class Task_CLASS
objectset: DEF_SETSTATES   is_of_class VOID {DefTask}
objectset: DEF_SETACTIONS  is_of_class VOID {DefTask}

object:    VipTask is_of_class Task_CLASS
objectset: VIP_SETSTATES   is_of_class VOID {VipTask}
objectset: VIP_SETACTIONS  is_of_class VOID {VipTask}

objectset: ALLCHILDREN_SETACTIONS union {VIP_SETSTATES,DEF_SETACTIONS} is_of_class VOID
objectset: ALLCHILDREN_SETSTATES  union {VIP_SETSTATES,DEF_SETSTATES}  is_of_class VOID

