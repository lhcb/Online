#!/bin/bash
# =========================================================================
#
#  Generic farm task startup script
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    20/05/2013
#
# =========================================================================
#
export MBM_OUTPUT_BUFFER=EventOutput;
exec -a ${UTGID} genPython.exe `which gaudirun.py` ${FARMCONFIGROOT}/job/Passthrough.py --application=OnlineEvents;
