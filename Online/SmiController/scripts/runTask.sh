#!/bin/bash
# =========================================================================
#
#  Generic farm task startup script
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    20/05/2020
#
# =========================================================================
#
export UTGID=${UTGID}
if test -n "${LOGFIFO}"; then
    export LOGFIFO=${LOGFIFO};
fi;
if test -z "${LOGFIFO}" -a -e /dev/shm/logs.dev; then
    export LOGFIFO=/dev/shm/logs.dev;
fi;
if [ -r /etc/sysconfig/dim ]; then
   . /etc/sysconfig/dim;
   if test "`echo $DIM_DNS_NODE | cut -b 1-5`" = "fmc01"; then
     DIM_DNS_NODE=`hostname -s`;
   fi;
   export DIM_DNS_NODE;
fi;
#
export CMTCONFIG=x86_64_v2-el9-gcc13-opt;
export CMTCONFIG=x86_64_v2-el9-gcc13-do0;
#
. /group/online/dataflow/cmtuser/OnlineRelease/setup.${CMTCONFIG}.vars;
. ${FARMCONFIGROOT}/job/createEnvironment.sh  $*;
#
export GEN_OPTIONS=/group/online/dataflow/options/${PARTITION}/MONITORING;
#
export ONLINETASKS=/group/online/dataflow/templates;
export INFO_OPTIONS=/group/online/dataflow/options/${PARTITION}/MONITORING/OnlineEnv.opts;
export PREAMBLE_OPTS=${FARMCONFIGROOT}/options/Empty.opts;
export PYTHONPATH=${GEN_OPTIONS}:${PYTHONPATH}
#
monitoring_options()
{
    echo "/group/online/dataflow/options/${PARTITION_NAME}/MONITORING/${UTGID}.opts";
}
dataflow_default_options()
{
    echo "-opts=`monitoring_options`";
}
#
dataflow_task()
{    echo  "exec -a ${UTGID} genRunner.exe libDataflow.so dataflow_run_task -mon=Dataflow_DIMMonitoring -class=$1";  }
#
export LOGFIFO;
## echo "ERROR: INFO_OPTIONS ${INFO_OPTIONS}";
if test "${TASK_TYPE}" = "Controller"; then
    . ${FARMCONFIGROOT}/job/Controller.sh;
elif test -f ${SMICONTROLLERROOT}/scripts/${TASK_TYPE}.sh; then
    cd ${SMICONTROLLERROOT}/scripts;
    . ./${TASK_TYPE}.sh $*;
elif test "${TASK_TYPE}" = "TestExpandTAE"; then
    export EXPAND_TAE=1;
    export MBM_EVENT_EXPLORER=0.0001;
    exec -a ${UTGID} genPython.exe `which gaudirun.py` ${FARMCONFIGROOT}/job/Passthrough.py --application=OnlineEvents;
elif test "${TASK_TYPE}" = "TestSweepTAE"; then
    export ODIN_DUMP=1;
    export SWEEP_TAE=1;
    #export MBM_EVENT_EXPLORER=0.0001;
    exec -a ${UTGID} genPython.exe `which gaudirun.py` ${FARMCONFIGROOT}/job/Passthrough.py --application=OnlineEvents;
elif test "${TASK_TYPE}" = "TestSweepTAErndm"; then
    export ODIN_DUMP=1;
    export SWEEP_TAE=2;
    #export MBM_EVENT_EXPLORER=0.0001;
    exec -a ${UTGID} genPython.exe `which gaudirun.py` ${FARMCONFIGROOT}/job/Passthrough.py --application=OnlineEvents;
elif test -n "`echo ${TASK_TYPE} | grep TestEventMulti`"; then
    export COUNTER_TEST=1;
    export HISTOGRAM_TEST=1;
    #export JOBOPTSDUMPFILE=/tmp/${UTGID}.job.options;
    exec -a ${UTGID} genPython.exe `which gaudirun.py` ${FARMCONFIGROOT}/job/Passthrough.py --application=OnlineEvents;
elif test -n "`echo ${TASK_TYPE} | grep TestEventMon`"; then
    export COUNTER_TEST=1;
    export HISTOGRAM_TEST=1;
    exec -a ${UTGID} genPython.exe `which gaudirun.py` ${FARMCONFIGROOT}/job/Passthrough.py --application=OnlineEvents;
elif test -n "`echo ${UTGID} | grep TestEvent`"; then
    export MBM_OUTPUT_BUFFER=EventOutput;
    exec -a ${UTGID} genPython.exe `which gaudirun.py` ${FARMCONFIGROOT}/job/Passthrough.py --application=OnlineEvents;
    # exec -a ${UTGID} genPython.exe `which gaudirun.py` ${SMICONTROLLERROOT}/scripts/EventProcessor.py --application=OnlineEvents;
else
    #echo "LOGFIFO: $LOGFIFO OUTPUT_LEVEL: ${OUTPUT_LEVEL}";
    exec -a ${UTGID} genPython.exe ${DATAFLOWROOT}/python/DimTask.py -utgid ${UTGID} -partition 103 -print ${OUTPUT_LEVEL};
fi;
