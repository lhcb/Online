//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include <SmiController/SmiTaskConfiguration.h>
#include <SmiController/SmiController.h>
#include <SmiController/SmiTask.h>
#include <SmiController/TaskManager.h>
#include <smixx/smiuirtl.hxx>
#include <smixx/smirtl.hxx>
#include <CPP/IocSensor.h>
#include <CPP/Event.h>
#include <RTL/DllAccess.h>
#include <RTL/strdef.h>
#include <RTL/rtl.h>

// C/C++ include files
#include <cstdio>
#include <cerrno>
#include <csignal>
#include <cstdarg>
#include <mutex>
#include <sstream>
#include <sys/stat.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

extern "C" void smi_set_print_off();

using namespace std;
using namespace FiniteStateMachine;

#define ERROR_CONTROL (-9999)

namespace  {
  struct SubState  {
    static constexpr char SUCCESS_ACTION = 'S';
    static constexpr char EXEC_ACTION    = 'E';
    static constexpr char FAILED_ACTION  = 'F';
    static constexpr char UNKNOWN_ACTION = 'U';
  };
  class ctrl_dim_lock_t   {
  public:
    ctrl_dim_lock_t()   {  ::dim_lock();   }
    ~ctrl_dim_lock_t()  {  ::dim_unlock(); }
  };
}

/// Helper class to handle DIM errors
/**
 * \author  M.Frank
 * \date    01/03/2013
 * \version 0.1
 */
class SmiController::CtrlDimErrors : public DimErrorHandler   {
public:
  bool m_flag;
  SmiController& m_controller;
  void errorHandler(int severity, int code, char *msg) override  {
    if (m_flag)  {
      switch(code)   {
      case DIMTCPCNERR:
	this->m_controller.logger().info("++ DIM: Code %d.%x %s", severity, code, msg);
        return;
      case DIMTCPRDERR:
      case DIMTCPWRRTY:
      case DIMTCPWRTMO:
      case DIMDNSREFUS:
	this->m_controller.logger().info("++ DIM: Code %d.%x %s", severity, code, msg);
        return;
      default:
	break;
      }
      switch(severity)   {
      case 3:
	this->m_controller.logger().error("++ DIM: Code %d.%x %s", severity, code, msg);
	return;
      case 2:
	this->m_controller.logger().error("++ DIM: Code %d.%x %s", severity, code, msg);
	return;
      case 1:
	this->m_controller.logger().warning("++ DIM: Code %d.%x %s", severity, code, msg);
	return;
      case 0: 
	this->m_controller.logger().info("++ DIM: Code %d.%x %s", severity, code, msg);
	return;
      default:
	this->m_controller.logger().info("++ DIM: Code %d.%x %s", severity, code, msg);
	return;
      }
    }
  }
  CtrlDimErrors (SmiController& c) : DimErrorHandler(), m_controller(c)  {
    this->m_flag = true;
  }
  void start()  {
    this->m_flag = true;
  }
  void stop()  {
    this->m_flag = false;
  }
};

/// Helper base class to represent a SMI object
/**
 * \author  M.Frank
 * \date    01/03/2020
 * \version 0.1
 */
class SmiControllerObject : public SmiObject  {
public:
  /// Reference to the controller object
  SmiController* m_controller = 0;
public:
  /// Initializing constructor
  SmiControllerObject(SmiController* ctrl, const std::string& dns, const std::string& nam)
    : SmiObject(dns.c_str(), ::dim_get_dns_port(), ctrl->config.smiDomain().c_str(), nam.c_str()),
      m_controller(ctrl)
  {  }
  /// Default destructor
  virtual ~SmiControllerObject() = default;
  /// Callback to handle state changes
  virtual void smiStateChangeHandler()   override final  {}
  /// Callback to handle state changes
  virtual void smiExecutingHandler()   override final  {}
};

/// Helper base class to represent a SMI proxy
/**
 *
 * \author  M.Frank
 * \date    01/03/2020
 * \version 0.1
 */
class SmiControllerProxy : public SmiProxy  {
protected:
  /// Reference to the managing machine
  SmiController* m_controller {0};
public:
  /// Initializing constructor
  SmiControllerProxy(SmiController* ctrl, const std::string& dns, const std::string& nam);
  /// Default destructor
  virtual ~SmiControllerProxy() = default;
  /// SMI domain exit overload
  virtual void smiDomainExitHandler()   override final   {}
};

/// Initializing constructor
SmiControllerProxy::SmiControllerProxy(SmiController* ctrl, const std::string& dns, const std::string& nam)
  : SmiProxy(dns.c_str(), ::dim_get_dns_port(), nam.c_str()), m_controller(ctrl)
{
  this->setVolatile();
  this->attach(ctrl->config.smiDomain().c_str());
}

/// Helper class to represent the SMI object set handler
/**
 *
 * \author  M.Frank
 * \date    01/03/2013
 * \version 0.1
 */
class SmiController::TaskSet : public SmiControllerObject  {
public:
  using SmiControllerObject::SmiControllerObject;
};

/// Helper class to represent SMI processing node object
/**
 *
 * \author  M.Frank
 * \date    01/03/2020
 * \version 0.1
 */
class SmiController::NodeProxy : public SmiControllerObject  {
public:
  using SmiControllerObject::SmiControllerObject;
  void invoke_transition(const std::string& cmd);
};

void SmiController::NodeProxy::invoke_transition(const std::string& cmd)   {
  if ( cmd == Transition::CONFIGURE )
    this->sendCommand(RTL::str_upper(cmd).c_str());
  else if ( cmd == Transition::START  )    
    this->sendCommand(RTL::str_upper(cmd).c_str());
  else if ( cmd == Transition::STOP   )    
    this->sendCommand(RTL::str_upper(cmd).c_str());
  else if ( cmd == Transition::CONTINUE )
    this->sendCommand(RTL::str_upper(cmd).c_str());
  else if ( cmd == Transition::RESET  )
    this->sendCommand(RTL::str_upper(cmd).c_str());
  else if ( cmd == Transition::UNLOAD )    
    this->sendCommand(RTL::str_upper(Transition::DESTROY).c_str());
  else if ( cmd == Transition::PAUSE  )    
    this->sendCommand(RTL::str_upper(cmd).c_str());
  else if ( cmd == Transition::KILL )      
    this->sendCommand(RTL::str_upper(cmd).c_str());
  else if ( cmd == Transition::DESTROY )
    this->sendCommand(RTL::str_upper(cmd).c_str());
  else if ( cmd == Transition::RECOVER )
    this->sendCommand(RTL::str_upper(cmd).c_str());
  else if ( cmd == Transition::CREATE )    
    this->sendCommand(RTL::str_upper(cmd).c_str());
  else if ( cmd == Transition::LOAD )
    this->sendCommand(RTL::str_upper(Transition::CREATE).c_str());
  else if ( cmd == "!state" )  {
    string st = this->m_controller->m_state;
    this->m_controller->m_state = "";
    this->m_controller->declare_state(st);
  }
  else if ( cmd == "error" )  {
    auto& log = this->m_controller->logger();
    this->m_controller->set_target_state(State::ERROR);
    this->m_controller->declare_state(this->m_controller->target_state());
    log.warning("++ User requested transition to ERROR: %s [%s]",
		cmd.c_str(), this->m_controller->state().c_str());
  }
  else  {
    auto& log = this->m_controller->logger();
    log.error("++ NodeProxy: Unknown transition request: %s", cmd.c_str());
  }
}

/// Helper class to represent SMI controller proxy
/**
 * \author  M.Frank
 * \date    01/03/2020
 * \version 0.1
 */
class SmiController::CtrlProxy : public  SmiControllerProxy {
public:
  using SmiControllerProxy::SmiControllerProxy;
  /// SMI command handler overload
  virtual void smiCommandHandler()  override final;
};

/// SMI command handler overload
void SmiController::CtrlProxy::smiCommandHandler()   {
  string cmd = RTL::str_lower(this->getAction());
  auto& log  = this->m_controller->logger();
  log.info("++ Request to execute transition %s", cmd.c_str());
  IocSensor::instance().send(this->m_controller, CONTROLLER_INVOKE_TRANSITION, new std::string(cmd));
}


/// Helper class to represent dummy SMI task proxy
/**
 * \author  M.Frank
 * \date    01/03/2020
 * \version 0.1
 */
class SmiController::DfTaskProxy : public SmiControllerProxy  {
public:
  using SmiControllerProxy::SmiControllerProxy;
  /// SMI command handler overload
  virtual void smiCommandHandler()  override final;
};

/// SMI command handler overload
void SmiController::DfTaskProxy::smiCommandHandler()   {
  string cmd = RTL::str_lower(this->getAction());
  if ( cmd == Transition::CONFIGURE )
    this->setState(State::READY);
  else if ( cmd == Transition::INITIALIZE )
    this->setState(State::READY);
  else if ( cmd == Transition::START  )
    this->setState(State::RUNNING);
  else if ( cmd == Transition::CONTINUE )
    this->setState(State::RUNNING);
  else if ( cmd == Transition::STOP   )
    this->setState(State::READY);
  else if ( cmd == Transition::RESET  )
    this->setState(State::OFFLINE);
  else if ( cmd == Transition::UNLOAD )
    this->setState(State::UNKNOWN);
  else if ( cmd == Transition::PAUSE  )
    this->setState(State::PAUSED);
  else if ( cmd == Transition::CREATE )
    this->setState(State::OFFLINE);
  else if ( cmd == Transition::LOAD )
    this->setState(State::OFFLINE);
  else if ( cmd == Transition::KILL )
    this->setState(State::UNKNOWN);
  else if ( cmd == "check"  )   {
    std::string st = this->getState();
    this->setState(st.c_str());
  }
  else  {
    this->setState(State::ERROR);
  }
}

/// Helper class to represent SMI task proxy objects
/**
 * \author  M.Frank
 * \date    01/03/2020
 * \version 0.1
 */
class SmiController::TaskProxy : public SmiProxy, public CPP::Interactor  {
public:
  enum TaskState  {
    TASK_NONE               = 0x0FFFFFFF,
    TASK_LIMBO              = 1<<20,
    TASK_STARTING           = 1<<21,
    TASK_ALIVE              = 1<<22,
    TASK_FAILED             = 1<<24,
    TASK_TRANSITION         = 1<<27,
    TASK_IDLE               = 1<<28,
    // Timeout values
    TASK_TIMEOUT            = 1<<29,
    TASK_UNLOAD_TIMEOUT,
    TASK_TERMINATE_TIMEOUT,
    TASK_KILL_TIMEOUT,
    TASK_START_TIMEOUT,
    // Auxiliare commands
    TASK_PUBLISH
  };

public:
  struct FSMMonitoring  {
    unsigned long lastCmd, doneCmd;
    int pid;
    char targetState, state, metaState, pad;
    int partitionID;
  } m_monitor;
public:
  /// TimerID definition
  typedef std::pair<void*,unsigned long> TimerID;

  /// Image of the taskinfo data
  std::unique_ptr<SmiTask>  m_task;
  /// Data logging interface
  RTL::Logger               m_log;
  /// Dependent Task's execution node
  std::string               m_node;
  /// Dependent Task's name
  std::string               m_name;
  /// Meta state: dead/alive handling
  TaskState                 m_meta = TASK_LIMBO;
  /// Data lock
  std::mutex                m_lock;

  /// ID of the DIM command to send commands to the dependent task
  std::pair<int,int>        m_dim_state {0,0};
  /// Service IDs for auxiliary and debug services
  std::pair<int,int>        m_dbg_state {0,0};
  /// ID of timeout timer
  TimerID                   m_timer_id  {0,0};
  /// DIC domain id for the task manager
  long                      m_tms_dic_dns_ID      {0};
  /// DIC domain id for the local communication
  long                      m_local_dic_dns_ID    {0};
  /// DIS domain id for the local communication
  long                      m_local_dis_dns_ID    {0};
  /// Debug data buffers
  std::string               m_dbg_data1, m_dbg_data2;

  /// Reference to the managing machine
  CPP::Interactor*          m_controller          {0};

  /// Flag to store if dependent task is alive
  bool                      m_alive               = false;
  /// Flag to see if the dependent task has answered to a request
  bool                      m_answered            = false;

  bool                      m_dim_active          = false;
  bool                      m_dim_dead            = true;

public:
  /// Initializing constructor
  TaskProxy(SmiController* ctrl, const std::string& nam, const std::string& nod);
  /// Default destructor
  virtual ~TaskProxy();
  /// SMI command handler overload
  virtual void smiCommandHandler()  override final;
  virtual void smiDomainExitHandler()   override final;
  /// DTQ overload to process timeout(s)
  static void tmo_handler(void* tag);
  /// DimInfo overload to process messages
  static void info_handler(void* tag, void* address, int* size);
  /// DimInfo overload to process messages
  static void state_handler(void* tag, void* address, int* size);
  /// DimInfo overload to process auxiliary debug publishings
  static void dbg_handler_1(void* tag, void** buff, int* size, int* first);
  /// DimInfo overload to process auxiliary debug publishings
  static void dbg_handler_2(void* tag, void** buff, int* size, int* first);

  /// Interactor Interrupt handling routine
  void handle(const CPP::Event& ev) override;
  const char* proxy_state()   const    {
    return this->getState()  ? this->getState() : State::OFFLINE;
  }
  /// Check if the task is alive
  bool is_alive()  const
  {  return m_alive;                  }
  /// Check if the dependent task answered to the current request
  bool answered()  const
  {  return m_answered;               }
  /// Check if the dependent task did not answer within timeout
  bool timed_out()  const   
  {  return m_meta >= TASK_TIMEOUT;  }
  /// Check if the dependent task is limbo
  bool is_limbo()  const
  {  return !is_alive() || (m_meta&TASK_LIMBO) || (m_meta&TASK_STARTING);    }
  /// Access meta state as string
  const char* metaStateName() const;

  /// Handle state changes of dependent task
  virtual void handle_state(const std::string& new_state);
  /// Handle timeout according to timer ID
  virtual void handle_timeout();
  /// Handle timeout on unload transition according to timer ID
  void handle_unload_timeout();
  /// Start dependent task process
  void start();
  /// Kill dependent task process
  void kill();
  /// Forced kill of task process.
  void force_kill();
  /// Start the dependent task's transition timeout
  void start_timer(int reason, const std::string& transition="any");
  /// Stop the dependent task's transition timeout
  void stop_timer();

  /// Publish instance information
  void publish_instances(int count);

  /// Publish state and debug information
  void publish_info();
  void notify_controller();

};

/// Access meta state as string
static const char* _metaStateName(int meta)   {
  switch(meta)  {
#define MakeName(x) case SmiController::TaskProxy::TASK_##x : return #x
    MakeName(NONE);
    MakeName(IDLE);
    MakeName(LIMBO);
    MakeName(STARTING);
    MakeName(ALIVE);
    MakeName(FAILED);
    MakeName(TRANSITION);
    // Timeout handling
    MakeName(TIMEOUT);
    MakeName(UNLOAD_TIMEOUT);
    MakeName(TERMINATE_TIMEOUT);
    MakeName(KILL_TIMEOUT);
    MakeName(START_TIMEOUT);
#undef  MakeName
  }
  return "UNKNOWN";
}

SmiController::TaskProxy::TaskProxy(SmiController* ctrl, const std::string& nam, const std::string& nod)
  : SmiProxy(ctrl->config.smi_dns.c_str(), ::dim_get_dns_port(), nam.c_str()), 
    m_log(RTL::Logger::LogDevice::getGlobalDevice(), nam, ctrl->config.output_level),
    m_node(nod), m_name(nam), m_controller(ctrl)
{
  string proc = RTL::processName();
  const SmiController::config_t& config = ctrl->config;
  auto items = RTL::str_split(nam, "_");
  if ( items.size() > 2 ) this->m_log.source = items[2];
#if 1
  this->m_tms_dic_dns_ID   = ctrl->m_tms_dic_dns_ID;
  this->m_local_dic_dns_ID = ctrl->m_local_dic_dns_ID;
  this->m_local_dis_dns_ID = ctrl->m_local_dis_dns_ID;
#else
  this->m_tms_dic_dns_ID   = ::dic_add_dns(config.tms_dns.c_str(),   ::dim_get_dns_port());
  this->m_local_dic_dns_ID = ::dic_add_dns(config.dns.c_str(),       ::dim_get_dns_port());
#endif
  this->m_dim_state.first  = ::dic_info_service_dns(this->m_local_dic_dns_ID,
						    (nam+"/status").c_str(),
						    MONITORED, 0, 0, 0, state_handler, (long)this, 0, 0);
  this->m_dim_state.second = ::dic_info_service_dns(this->m_local_dic_dns_ID,
						    (nam+"/fsm_status").c_str(),
						    MONITORED, 0, 0, 0, info_handler, (long)this, 0, 0);
  this->m_local_dis_dns_ID = 0;//::dis_add_dns(config.dns.c_str(), ::dim_get_dns_port());
  this->m_dbg_state.first  = ::dis_add_service_dns( this->m_local_dis_dns_ID,
						    (proc+"/Task/"+nam+"/CtrlState").c_str(),
						    "C", 0, 0, dbg_handler_1, (long)this);
  this->m_dbg_state.second = 0;

  this->setVolatile();
  this->attach(config.smiDomain().c_str());
  this->setState(State::OFFLINE);
}

SmiController::TaskProxy::~TaskProxy()   {
  if ( this->m_dbg_state.first  ) ::dis_remove_service(this->m_dbg_state.first);
  if ( this->m_dbg_state.second ) ::dis_remove_service(this->m_dbg_state.second);
  if ( this->m_dim_state.first  ) ::dic_release_service(this->m_dim_state.first);
  if ( this->m_dim_state.second ) ::dic_release_service(this->m_dim_state.second);
  this->m_dbg_state = make_pair(0,0);
  this->m_dim_state = make_pair(0,0);
}

void SmiController::TaskProxy::smiCommandHandler()   {
  string cmd = RTL::str_lower(getAction());
  this->m_answered = false;
  IocSensor::instance().send(this, TASK_TRANSITION, new string(cmd));
}

void SmiController::TaskProxy::smiDomainExitHandler()   {
}

/// DTQ overload to process timeout(s)
void SmiController::TaskProxy::tmo_handler(void* tag)  {
  if ( tag ) {
    TaskProxy* proxy = (TaskProxy*)tag;
    proxy->handle_timeout();
  }
}

/// DimInfo overload to process messages
void SmiController::TaskProxy::info_handler(void* tag, void* address, int* size) {
  if ( tag && size ) {
    TaskProxy* proxy = *(TaskProxy**)tag;
    int len = *size;
    if ( address && len > 0 )   {
      ::memcpy(&proxy->m_monitor,address,sizeof(FSMMonitoring));
      IocSensor::instance().send(proxy, TASK_PUBLISH, proxy);
    }
    else   {
      time_t now = ::time(0);
      proxy->m_monitor.metaState   = MonMetaState::ACTION_UNKNOWN;
      proxy->m_monitor.targetState = MonState::UNKNOWN;
      proxy->m_monitor.state       = MonState::UNKNOWN;
      proxy->m_monitor.partitionID = 0;
      proxy->m_monitor.lastCmd     = now;
      proxy->m_monitor.doneCmd     = now;
      proxy->m_monitor.pid         = -1;
      proxy->m_monitor.pad         = 0;
    }
  }
}

/// DimInfo overload to process messages
void SmiController::TaskProxy::state_handler(void* tag, void* address, int* size) {
  if ( tag ) {
    int         len  = size ? *size : 0;
    TaskProxy* proxy = *(TaskProxy**)tag;
    const char* msg  = (const char*)address;
    if ( len > 0 )  {
      if ( !proxy->m_dim_active )    {
	IocSensor::instance().send(proxy, TASK_TRANSITION, new string("!state"));
      }
      proxy->m_alive      = true;
      proxy->m_dim_active = true;
      proxy->m_dim_dead   = false;
    }
    else   {
      proxy->m_dim_dead   = true;
      proxy->m_alive      = false;
    }
    proxy->handle_state(len > 0 ? msg : State::OFFLINE);
  }
}

/// Feed data to DIS when updating state
void SmiController::TaskProxy::dbg_handler_1(void* tag, void** buff, int* size, int* /* first */)   {
  static const char* defaults = "Type:NONE|State:DEAD|Status:LIMBO|Meta:LIMBO|Alive:NO_|Answered:NO_";
  TaskProxy* proxy = *(TaskProxy**)tag;
  if ( proxy )  {
    stringstream str;
    str << "Type:"      << "PHYSICAL"
	<< "|State:"     << proxy->proxy_state()
	<< "|Meta:"      << proxy->metaStateName()
	<< "|Alive:"     << (proxy->is_alive()   ? "YES" : "NO_")
	<< "|Answered:"  << (proxy->answered()   ? "YES" : "NO_")
	<< "|DimActive:" << (proxy->m_dim_active ? "YES" : "NO_")
	<< "|DimDead:"   << (proxy->m_dim_dead   ? "YES" : "NO_");
    {
      lock_guard<mutex> lock(proxy->m_lock);
      proxy->m_dbg_data1 = str.str();
      *buff = (void*)proxy->m_dbg_data1.c_str();
      *size = proxy->m_dbg_data1.length()+1;
    }
    return;
  }
  *buff = (void*)defaults;
  *size = ::strlen(defaults)+1;
}

/// Feed data to DIS when updating state
void SmiController::TaskProxy::dbg_handler_2(void* tag, void** buff, int* size, int* /* first */)   {
  static const char* defaults = "Type:NONE|State:DEAD|Status:LIMBO|Meta:LIMBO|Alive:NO_|Answered:NO_";
  TaskProxy* s = *(TaskProxy**)tag;
  if ( s )  {
  }
  *buff = (void*)defaults;
  *size = ::strlen(defaults)+1;
}

/// Interactor Interrupt handling routine
void SmiController::TaskProxy::handle(const CPP::Event& event)    {
  switch(event.type)  {
  case TASK_PUBLISH:
    publish_info();
    return;

  case TASK_TRANSITION:  {
    unique_ptr<string> c(event.iocPtr<string>());
    const string& transition = *c;
    this->m_log.info("++ smiCommandHandler: command:%s  action:%s\n", getCommand(), transition.c_str());
    this->m_answered = false;
    if ( transition == Transition::CREATE )   {
      if ( this->is_limbo() ) this->start();
    }
    else if ( (transition == Transition::UNLOAD) &&
	      (this->m_meta == TASK_LIMBO || this->m_meta == TASK_STARTING) )  {
      this->force_kill();
    }
    else if ( (transition == Transition::KILL) &&
	      (this->m_meta == TASK_LIMBO || this->m_meta == TASK_STARTING) )  {
      this->force_kill();
    }
    else if ( transition == Transition::KILL )   {
      this->kill();
    }
    else if ( transition == Transition::RECOVER )  {
      this->kill();
    }
    else if ( transition == "check"  )   {
      std::string st = this->proxy_state();
      this->setState(st.c_str());
    }
    else if ( transition == "!state" || transition == "get_state" )   {
      ::dic_cmnd_service_dns(this->m_local_dic_dns_ID, this->m_task->utgid.c_str(),
			     (void*)transition.c_str(), transition.length()+1);
    }
    else   {
      this->m_meta = TASK_TRANSITION;
      ::dic_cmnd_service_dns(this->m_local_dic_dns_ID, this->m_task->utgid.c_str(), 
			     (void*)transition.c_str(),transition.length()+1);
      this->m_log.debug("++ Send request \"%s\" [state now:%s]", transition.c_str(), this->proxy_state());
      start_timer(TASK_TRANSITION, transition);
    }
    return;
  }
  default:
    return;
  }
}

//==============================================================================
/// Start the dependent task's transition timeout
void SmiController::TaskProxy::start_timer(int reason, const std::string& transition)   {
  auto i = this->m_task->timeouts.find(RTL::str_lower(transition));
  if ( i == this->m_task->timeouts.end() ) i = this->m_task->timeouts.find("any");
  int tmo = i==this->m_task->timeouts.end() ? 5 : (*i).second;
  if ( this->m_timer_id.first )  {
    ::dtq_stop_timer(this->m_timer_id.first);
  }
  this->m_timer_id = TimerID(this,reason);
  if ( transition.empty() )
    this->m_log.debug("++ START dependent task timer with %d seconds id:%X", tmo, this->m_timer_id.first);
  else
    this->m_log.debug("++ START dependent task timer with %d seconds on %s id:%X",
		      tmo, transition.c_str(), this->m_timer_id.first);
  ::dtq_start_timer(tmo, tmo_handler, this->m_timer_id.first);
}

//==============================================================================
/// Stop the dependent task's transition timeout
void SmiController::TaskProxy::stop_timer()  {
  this->m_log.debug("++ KILL dependent task timer id:%X", this->m_timer_id.first);
  if ( this->m_timer_id.first )  {
    ctrl_dim_lock_t lock;
    ::dtq_stop_timer(this->m_timer_id.first);
  }
  this->m_timer_id = TimerID(0,0);
}

//==============================================================================
/// Handle timeout according to timer ID
void SmiController::TaskProxy::handle_timeout()  {
  this->m_log.info("++ TaskProxy TIMEOUT. State:%08X [%s] value:%08X [%s]",
		   this->m_meta, this->metaStateName(), this->m_timer_id.second,
		   _metaStateName(this->m_timer_id.second));
  if ( this->m_timer_id.second == TASK_UNLOAD_TIMEOUT ) {
    this->handle_unload_timeout();
  }
  else if ( this->m_timer_id.second == TASK_TERMINATE_TIMEOUT ) {
    this->handle_unload_timeout();
  }
  else if ( is_limbo() )  {
    this->m_meta = TASK_LIMBO;
    this->setState(State::OFFLINE);
    this->notify_controller();
  }
  else if ( m_timer_id.second == TASK_TRANSITION )  {
    std::string st = this->proxy_state();
    this->m_meta = TASK_TIMEOUT;
    this->setState(st.c_str());
    this->notify_controller();
  }
  this->publish_info();
}

//==============================================================================
/// Handle timeout on unload transition according to timer ID
void SmiController::TaskProxy::handle_unload_timeout()  {
  this->force_kill();
}

//==============================================================================
/// Start dependent task process
void SmiController::TaskProxy::start()  {
  const char* utgid = this->m_task->utgid.c_str();
  m_answered = false;
  if ( this->m_task->doStart )    {
    string fmc_args, cmd_args;
    const char* cmd = this->m_task->command.c_str();

    for(size_t i=0; i < this->m_task->fmcArgs.size();++i)
      fmc_args += this->m_task->fmcArgs[i] + " ";
    for(size_t i=0; i < this->m_task->argv.size();++i)
      cmd_args += this->m_task->argv[i] + " ";

    // Start the process using the FMC task manager
    m_log.info("++ %s: starting task: %s %s %s", utgid, fmc_args.c_str(), cmd, cmd_args.c_str());
    start_timer(TASK_START_TIMEOUT, Transition::LOAD);
    m_meta = TASK_STARTING;
    TaskManager::instance(m_node,m_tms_dic_dns_ID).start(this->m_task->utgid,fmc_args,this->m_task->command,cmd_args);
  }
  else   {
    m_log.info("++ %s: NOT starting task: (Controlled ONLY)", utgid);
    start_timer(TASK_START_TIMEOUT, Transition::LOAD);
    m_meta = TASK_STARTING;
  }
  publish_info();
}

//==============================================================================
/// Start dependent task process
void SmiController::TaskProxy::kill()  {
  string cmd = Transition::UNLOAD;
  stop_timer();
  m_answered = false;
  start_timer(TASK_UNLOAD_TIMEOUT,Transition::UNLOAD);
  int ret = ::dic_cmnd_service_dns(m_local_dic_dns_ID,
				   this->m_task->utgid.c_str(),
				   (void*)cmd.c_str(),
				   cmd.length()+1);
  m_log.info("++ %s \"%s\" command to dependent task [now:%s]",
	     ret != 1 ? "FAILED to send" : "Sent", cmd.c_str(), metaStateName());
  publish_info();
}

//==============================================================================
/// Kill dependent task process. Base class implementation will throw an exception
void SmiController::TaskProxy::force_kill()   {
  this->m_answered = false;
  TaskManager::instance(this->m_node, this->m_tms_dic_dns_ID).kill(this->m_task->utgid,SIGKILL);
  this->m_alive    = false;
  this->m_meta     = TASK_LIMBO;
  this->setState(State::OFFLINE);
  this->publish_info();
  this->notify_controller();
}

//==============================================================================
/// Access meta state as string
const char* SmiController::TaskProxy::metaStateName() const  {
  return _metaStateName(m_meta);
}

//==============================================================================
/// Handle state changes of dependent task
void SmiController::TaskProxy::handle_state(const std::string& new_state)   {
  this->stop_timer();
  this->m_meta     = TASK_IDLE;
  this->m_answered = true;
  this->setState(new_state.c_str());
  this->m_log.info("++ TaskProxy ANSWER. Set state:%s  Meta: %s [%08X]",
		   new_state.c_str(), this->metaStateName(), this->m_meta);
  this->publish_info();
  this->notify_controller();
}

//==============================================================================
/// Publish instance information
void SmiController::TaskProxy::publish_instances(int count)    {
  string cmd_instances = m_name +"/NumInstances";
  this->m_log.info("++ TaskProxy: Propagate instance info [%d] to %s", count, cmd_instances.c_str());
  ::dic_cmnd_service_dns(this->m_local_dic_dns_ID, cmd_instances.c_str(), (void*)&count, sizeof(int));
}

//==============================================================================
/// Publish state and debug information
void SmiController::TaskProxy::publish_info()   {
  ::dis_update_service(m_dbg_state.first);
}

//==============================================================================
/// Publish state and debug information
void SmiController::TaskProxy::notify_controller()   {
  if ( this->m_meta == TASK_IDLE )
    IocSensor::instance().send(this->m_controller, TASK_IDLE, this);
  else if ( this->m_meta == TASK_FAILED )
    IocSensor::instance().send(this->m_controller, TASK_FAILED, this);
  else if ( timed_out() )
    IocSensor::instance().send(this->m_controller, TASK_TIMEOUT, this);
  else
    IocSensor::instance().send(this->m_controller, TASK_TRANSITION, this);
}

//==============================================================================
/// Fully qualified domain string (config domain + "_" + host name)
std::string SmiController::config_t::smiDomain()  const   {
  return this->smi_domain;
}

//==============================================================================
/// Default constructor
SmiController::SmiController(const config_t& cfg)
  : config(cfg), m_num_worker_threads(cfg.num_workers)
{
  this->m_log.reset(new RTL::Logger(RTL::Logger::LogDevice::getGlobalDevice(), "Controller", cfg.output_level));
  this->m_num_worker_threads = config.num_workers;
  this->m_node_state = State::OFFLINE;

  this->m_local_dic_dns_ID    = ::dic_add_dns(config.dns.c_str(),     ::dim_get_dns_port());
  this->m_tms_dic_dns_ID      = ::dic_add_dns(config.tms_dns.c_str(), ::dim_get_dns_port());
  this->m_smi_dic_dns_ID      = ::dic_add_dns(config.smi_dns.c_str(), ::dim_get_dns_port());
  this->m_local_dis_dns_ID    = 0;//::dis_add_dns(config.dns.c_str(),     ::dim_get_dns_port());
  this->m_smi_dis_dns_ID      = ::dis_add_dns(config.smi_dns.c_str(), ::dim_get_dns_port());

  //::dis_start_serving_dns(m_local_dis_dns_ID, config.name.c_str());

  /// Switch off printing if level > INFO (ie. WARNING, ERROR, FATAL)
  if ( cfg.output_level > LIB_RTL_INFO )   {
    ::smi_set_print_off();
  }
  this->m_dimErrors = make_unique<CtrlDimErrors>(*this);
  DimClient::addErrorHandler(this->m_dimErrors.get());
}

/// Default destructor
//==============================================================================
SmiController::~SmiController()   {
  this->m_dimErrors->stop();
  this->m_vipTask.reset();
  this->m_defTask.reset();
  this->m_nodeProxy.reset();
  this->m_self.reset();
  this->m_taskset.reset();
  if ( this->m_command_ID      != 0 ) ::dis_remove_service(this->m_command_ID);
  if ( this->m_status_ID       != 0 ) ::dis_remove_service(this->m_status_ID);
  if ( this->m_state_ID        != 0 ) ::dis_remove_service(this->m_status_ID);
  if ( this->m_sub_status_ID   != 0 ) ::dis_remove_service(this->m_sub_status_ID);
  if ( this->m_fsm_tasks_ID    != 0 ) ::dis_remove_service(this->m_fsm_tasks_ID);
  if ( this->m_fsm_instance_ID != 0 ) ::dis_remove_service(this->m_fsm_instance_ID);
  if ( this->m_num_worker_ID   != 0 ) ::dic_release_service(this->m_num_worker_ID);
  if ( this->m_node_state_ID   != 0 ) ::dic_release_service(this->m_node_state_ID);
  DimClient::addErrorHandler(nullptr);
  this->m_dimErrors.reset();
}

//==============================================================================
void SmiController::configure()   {
  IocSensor::instance().send(this, CONTROLLER_INITIALIZE, nullptr);
}

//==============================================================================
void SmiController::initialize()   {
  class dim_handlers {
  public:
    /// Feed data to DIS when updating data
    static void feed_std_string(void* tag, void** buff, int* size, int* /* first */) {
      static const char* data = "";
      string* s = *(string**)tag;
      if ( !s->empty() )  {
	*buff = (void*)s->data();
	*size = s->length()+1;
	return;
      }
      *buff = (void*)data;
      *size = 1;
    }
    /// Feed data to DIS when updating data
    static void feed_int(void* tag, void** buff, int* size, int* /* first */) {
      static const int data = -1;
      int* s = *(int**)tag;
      if ( s )  {
	*buff = s;
	*size = sizeof(int);
	return;
      }
      *buff = (void*)&data;
      *size = sizeof(int);
    }
    /// DIM Service update handler to publish the FSM state 
    static void handle_node_state(void* tag, void* address, int* size)   {
      if ( tag && address && size && *size > 0 )   {
	SmiController* ctrl = *(SmiController**)tag;
	IocSensor::instance().send(ctrl, SmiController::CONTROLLER_SETNODESTATE, new string((char*)address));
      }
    }
    /// DIM Service update handler to change the required number of threads in dependent processes
    static void handle_num_threads(void* tag, void* address, int* size) {
      if ( tag && address ) {
	int len = size ? *size : 0;
	if ( len > 0 )  {
	  SmiController* c = *(SmiController**)tag;
	  int            m = *(int*)address;
	  if ( len == sizeof(int) && m > 0 ) {
	    c->publish_instances(m);
	  }
	}
      }
    }
    /// DIM client update handler to communicate state changes to the controller object
    static void change_transition(void* tag, void* address, int* size)   {
      if ( tag && address && size && *size > 0 )   {
	SmiController* controller = *(SmiController**)tag;
	char   cmd[512];
	size_t len = std::min((size_t)*size, sizeof(cmd)-1);
	::strncpy(cmd, (const char*)address, len);
	cmd[len] = 0;
	if ( controller->config.standalone )
	  controller->m_nodeProxy->invoke_transition(cmd);
	else
	  controller->invoke_transition(cmd);
      }
    }

  };
  const auto& nam       = config.name;
  this->m_monitor.targetState = this->m_monitor.state = MonState::UNKNOWN;
  this->m_monitor.lastCmd     = this->m_monitor.doneCmd = (int)::time(0);
  this->m_monitor.metaState   = SubState::SUCCESS_ACTION;
  this->m_monitor.pid         = ::lib_rtl_pid();
  this->m_monitor.partitionID = -1;
  this->m_monitor.pad         = 0;

  this->m_dimErrors->start();
  this->m_command_ID    = ::dis_add_cmnd_dns(   this->m_local_dis_dns_ID,
						config.name.c_str(),
					        "C", dim_handlers::change_transition,(long)this);
  this->m_userCmd_ID    = ::dis_add_cmnd_dns(   this->m_local_dis_dns_ID,
						(config.name+"/command").c_str(),
					        "C", dim_handlers::change_transition,(long)this);
  this->m_status_ID     = ::dis_add_service_dns(this->m_local_dis_dns_ID,
						(nam+"/status").c_str(),
						"C",0,0,dim_handlers::feed_std_string,(long)&this->m_node_state);
  //						"C",0,0,dim_handlers::feed_std_string,(long)&this->m_state);
  this->m_state_ID      = ::dis_add_service_dns(this->m_local_dis_dns_ID,
						(nam+"/state").c_str(),
						"C",0,0,dim_handlers::feed_std_string,(long)&this->m_node_state);
  this->m_sub_status_ID = ::dis_add_service_dns(this->m_local_dis_dns_ID,
						(nam+"/fsm_status").c_str(),
						"L:2;I:1;C",&this->m_monitor,sizeof(this->m_monitor),0,0);
  this->m_fsm_tasks_ID  = ::dis_add_service_dns(this->m_local_dis_dns_ID,
						(nam+"/tasks").c_str(),
						"C",0,0,dim_handlers::feed_std_string,(long)&this->m_task_info);
  this->m_fsm_instance_ID     = ::dis_add_service_dns(this->m_local_dis_dns_ID,
						      (nam+"/instances").c_str(),
						      "I",0,0,dim_handlers::feed_int,(long)&this->m_instance_info);
  this->m_fsm_instance_CMDID  = ::dis_add_cmnd_dns(   this->m_local_dis_dns_ID,
						      (config.name+"/set_instances").c_str(),
						      "I", dim_handlers::handle_num_threads,(long)this);
  this->m_log->debug("++ DNS port: %d dic id: %lx TMS dic id: %lx SMI dic ID: %lx SMI dis id: %lx",
		     ::dim_get_dns_port(),
		     this->m_local_dic_dns_ID, this->m_tms_dic_dns_ID,
		     this->m_smi_dic_dns_ID, this->m_smi_dis_dns_ID);
  if ( config.num_workers < 1 )  {
    this->m_log->info("++ Invalid initial instance count:%d -> forced to 1", config.num_workers);
    config.num_workers = 1;
  }
  if ( !config.num_thread_svc.empty() )  {
    this->m_log->info("++ Using dim service %s to adjust number of instances",config.num_thread_svc.c_str());
    this->m_num_worker_ID = ::dic_info_service_dns(this->m_local_dic_dns_ID,
						   config.num_thread_svc.c_str(),
						   MONITORED,0,0,0,dim_handlers::handle_num_threads,
						   (long)this,0,0);
  }
  string slice = config.standalone ? string("Manager") : config.smiDomain() + "_Manager";
  this->m_taskset = make_unique<TaskSet>(this, config.smi_dns, slice);

  if ( config.standalone )   {
    string node_state = RTL::str_upper("SMI/"+config.smi_domain+"/PROCESSINGNODE");
    bool   print      = config.output_level <= LIB_RTL_INFO;
    this->m_self            = make_unique<CtrlProxy>  (this, config.smi_dns, "Controller");
    this->m_nodeProxy       = make_unique<NodeProxy>  (this, config.smi_dns, "ProcessingNode");
    this->m_defTask         = make_unique<DfTaskProxy>(this, config.smi_dns, "DefTask");
    this->m_vipTask         = make_unique<DfTaskProxy>(this, config.smi_dns, "VipTask");
    this->m_node_state_ID   = ::dic_info_service_dns(this->m_local_dic_dns_ID,
						     node_state.c_str(),
						     MONITORED, 0, 0, 0,
						     dim_handlers::handle_node_state,
						     (long)this, 0, 0);
    print ? this->m_self->setPrintOn()     : m_self->setPrintOff();
    print ? this->m_defTask->setPrintOn()  : m_defTask->setPrintOff();
    print ? this->m_vipTask->setPrintOn()  : m_vipTask->setPrintOff();
    this->m_self->setState(State::OFFLINE);
    this->m_defTask->setState(State::OFFLINE);
    this->m_vipTask->setState(State::OFFLINE);
  }
  //config.name = RTL::str_upper(string(m_self->getDomain()) + "::Controller");
  ::dis_start_serving_dns(this->m_local_dis_dns_ID, config.name.c_str());
  this_thread::sleep_for(chrono::milliseconds(200));
  this->m_inited = true;
}

//==============================================================================
/// Set the target state
void SmiController::set_target_state(const std::string& new_state)  {
  this->m_target_state = new_state;
  this->m_monitor.lastCmd = ::time(0);
  this->m_monitor.state = m_state[0];
  this->m_monitor.metaState = MonMetaState::ACTION_EXEC;
  this->m_monitor.targetState = new_state[0];
  if (        this->m_state == State::READY     ) this->m_monitor.state       = MonState::READY;
  else if (   this->m_state == State::RUNNING   ) this->m_monitor.state       = MonState::RUNNING;
  else if (   this->m_state == State::PAUSED    ) this->m_monitor.state       = MonState::PAUSED;
  else if (   this->m_state == State::STOPPED   ) this->m_monitor.state       = MonState::STOPPED;
  else if (   this->m_state == State::OFFLINE   ) this->m_monitor.state       = MonState::OFFLINE;
  else if (   this->m_state == State::UNKNOWN   ) this->m_monitor.state       = MonState::UNKNOWN;
  else if (   this->m_state == State::ERROR     ) this->m_monitor.state       = MonState::ERROR;
  if (        new_state     == State::READY     ) this->m_monitor.targetState = MonState::READY;
  else if (   new_state     == State::RUNNING   ) this->m_monitor.targetState = MonState::RUNNING;
  else if (   new_state     == State::PAUSED    ) this->m_monitor.targetState = MonState::PAUSED;
  else if (   new_state     == State::STOPPED   ) this->m_monitor.targetState = MonState::STOPPED;
  else if (   new_state     == State::OFFLINE   ) this->m_monitor.targetState = MonState::OFFLINE;
  else if (   new_state     == State::UNKNOWN   ) this->m_monitor.targetState = MonState::UNKNOWN;
  else if (   new_state     == State::ERROR     ) this->m_monitor.targetState = MonState::ERROR;
}

//==============================================================================
void SmiController::load_tasks()    {
  SmiTaskConfiguration cfg(config.replacements, config.partition,
			   config.architecture, config.runinfo, 
			   config.stdout_file,  config.stderr_file);
  auto tasks = cfg.taskList(config.bind_cpus, config.output_level);
  this->m_log->info("++ Found %ld tasks in architecture %s %s [0x%X]", 
		    tasks.size(), config.architecture.c_str(),
		    config.standalone ? "Standalone mode ON" : "",
		    config.standalone);
  if ( !attach_tasks(std::move(tasks)) )  {
    this->m_log->error("++ Failed to interprete XML tasklist.");
  }
  this->m_taskset->sendCommand("REMOVETASK/UTGID=DEFTASK");
  this->m_taskset->sendCommand("REMOVEVIP/UTGID=VIPTASK");
  for( const auto& proxy : this->m_tasks )   {
    const string& utgid = proxy.second->m_task->utgid;
    bool  is_vip = proxy.second->m_task->isVIP;
    if ( is_vip )   {
      this->m_log->info("++ %s: [%s] --> ADDVIP/UTGID=%s",
			proxy.second->getDomain(), proxy.second->proxy_state(), utgid.c_str());
      this->m_taskset->sendCommand(("ADDVIP/UTGID="+utgid).c_str());
    }
    else   {
      this->m_log->info("++ %s: [%s] --> ADDTASK/UTGID=%s",
			proxy.second->getDomain(), proxy.second->proxy_state(), utgid.c_str());
      this->m_taskset->sendCommand(("ADDTASK/UTGID="+utgid).c_str());
    }
  }
  this->m_log->debug("++ Sleep 2 seconds after ADDTASKS.");
  this_thread::sleep_for(chrono::milliseconds(2000));
  IocSensor::instance().send(this, CONTROLLER_SETSTATE, nullptr);
}

//==============================================================================
void SmiController::kill_tasks()   {
  this->set_target_state(State::UNKNOWN);
  for( const auto& proxy : this->m_tasks )
    proxy.second->kill();
  this->m_dimErrors->stop();
  IocSensor::instance().send(this, CONTROLLER_TASKS_DEAD, this);
}

//==============================================================================
void SmiController::force_kill_tasks()   {
  //::lib_rtl_sleep(1500);
  for( const auto& proxy : this->m_tasks )
    proxy.second->force_kill();
  IocSensor::instance().send(this, CONTROLLER_TASKS_DEAD, this);
}

/// Declare process state to DIM service
void SmiController::declare_state(const std::string& new_state, const std::string& opt)  {
  string old_state = this->m_state;
  ::dim_lock();
  this->m_state = new_state;
  this->m_monitor.state = m_state[0];
  ::dim_unlock();
  this->m_log->info("++ Declare state:%s  %s",new_state.c_str(),opt.c_str());
  ::dis_update_service(this->m_status_ID);
  if ( new_state == State::ERROR )
    this->declare_sub_state(SubState::FAILED_ACTION);
  else if ( old_state == new_state )
    this->declare_sub_state(SubState::FAILED_ACTION);
  else
    this->declare_sub_state(SubState::SUCCESS_ACTION);
}

//==============================================================================
/// Declare FSM sub-state
void SmiController::declare_sub_state(int new_state)  {
  this->m_monitor.metaState = char(new_state);
  switch(new_state)   {
  case SubState::SUCCESS_ACTION:
    this->m_monitor.doneCmd = (int)::time(0);
    break;
  case SubState::EXEC_ACTION:
    this->m_monitor.lastCmd = (int)::time(0);
    break;
  case SubState::FAILED_ACTION:
    this->m_monitor.doneCmd = (int)::time(0);
    break;
  case SubState::UNKNOWN_ACTION:
  default:
    this->m_monitor.doneCmd = (int)::time(0);
    this->m_monitor.metaState = MonState::UNKNOWN;
    break;
  }
  ::dis_update_service(this->m_sub_status_ID);
}

//==============================================================================
void SmiController::publish_instances(int count)   {
  // If the service has an invalid value, take the initial number of instances
  if ( count > 0 )  {
    this->m_instance_info = count;
    config.num_workers = count;
    this->m_log->always("++ Adjust number of instance threads to %d.",config.num_workers);
    ::dis_update_service(this->m_fsm_instance_ID);
    for( const auto& proxy : this->m_tasks )
      proxy.second->publish_instances(count);

  }
  //controlInstances();
  IocSensor::instance().send(this, CONTROLLER_PUBLISH_TASKS, this);
}

//==============================================================================
/// Publish state information of the dependent tasks
void SmiController::publish_tasks()  {
  string tmp;
  tmp.reserve(2048);
  for( const auto& proxy : m_tasks )  {
    const auto& p = proxy.second;
    tmp += p->m_task->utgid + "/";
    tmp += p->proxy_state();
    tmp += "/";
    tmp += p->metaStateName();
    tmp += '|'; //'\0';
  }
  this->m_log->debug("++ Publishing the dependent task state.....");
  if ( this->m_task_info != tmp )  {
    if ( tmp.length()>1 ) tmp[tmp.length()-1] = 0;
    ::dim_lock();
    this->m_task_info = std::move(tmp);
    ::dim_unlock();
    ::dis_update_service(this->m_fsm_tasks_ID);
  }
}

//==============================================================================
bool SmiController::attach_tasks(std::map<std::string, SmiTask*>&& tasks)    {
  vector<string> names;
  m_log->info("++ Configuring %ld tasks.",tasks.size());
  for( const auto& s : m_tasks )   {
    auto i = tasks.find(s.first);
    if ( i == tasks.end() )   {
      names.push_back(s.first);
    }
  }
  for( const auto& s : names )   {
    auto i = this->m_tasks.find(s);
    if ( i != this->m_tasks.end() )  {
      this->m_log->info("++ Remove dependent task %s from taskset.",s.c_str());
      this->m_taskset->sendCommand(("REMOVETASK/"+i->second->m_task->utgid).c_str());
      this->m_tasks.erase(i);
    }
  }

  this_thread::sleep_for(chrono::milliseconds(200));
  for( const auto& task : tasks)   {
    const auto& nam = task.second->utgid;
    this->m_log->info("++ Adding dependent task: %s", nam.c_str());
    auto i = this->m_tasks.find(task.first);
    if ( i == this->m_tasks.end() )   {
      auto proxy = make_unique<TaskProxy>(this, nam, RTL::nodeNameShort());
      proxy->setState(State::UNKNOWN);
      i = this->m_tasks.insert(make_pair(task.first, std::move(proxy))).first;
    }
    else   {
      IocSensor::instance().send(i->second.get(), TaskProxy::TASK_TRANSITION, new string("!state"));
    }
    auto& proxy = i->second;
    config.output_level < LIB_RTL_INFO ? proxy->setPrintOn() : proxy->setPrintOff();
    proxy->m_task.reset(task.second);
    proxy->m_task->add_fmc_args("-DDIM_DNS_NODE="+RTL::str_upper(config.dns));
    proxy->m_task->add_fmc_args("-DDIM_DNS_PORT="+std::to_string(::dim_get_dns_port()));
    proxy->m_task->add_fmc_args("-DDIM_HOST_NODE="+RTL::str_upper(RTL::nodeNameShort()));
  }
  //::dis_start_serving_dns(m_local_dis_dns_ID, config.name.c_str());
  this_thread::sleep_for(chrono::milliseconds(200));
  return true;
}

//==============================================================================
/// Check if all dependent tasks have answered the request and - if yes - complete the transition
void SmiController::check_for_completion()    {
  size_t answered = 0;
  size_t timeouts = 0;
  for( const auto& proxy : this->m_tasks )  {
    if ( proxy.second->answered()  ) ++answered;
    if ( proxy.second->timed_out() ) ++timeouts;
  }
  this->m_log->info("++ Completion check: total: %ld answered: %ld timeout: %ld state:%s target:%s", 
		    this->m_tasks.size(), answered, timeouts, this->m_state.c_str(), this->m_target_state.c_str());
  if ( timeouts>0 && (timeouts+answered) == this->m_tasks.size() )   {
    this->declare_state(State::ERROR);
  }
}

//==============================================================================
/// Invoke single transition request on SMI machinery
SmiErrCond SmiController::invoke_transition(const string& command)   {
  if ( !command.empty() )    {
    string cmd = RTL::str_lower(command);
    if ( cmd == Transition::LOAD )   {
      this->m_dimErrors->start();
      IocSensor::instance().send(this, CONTROLLER_LOAD_TASKS, this);
      return SMI_CONTROLLER_SUCCESS;
    }
    else if ( cmd == Transition::KILL )   {
      this->kill_tasks();
      return SMI_CONTROLLER_SUCCESS;
    }
    else if ( cmd == Transition::DESTROY )    {
      this->set_target_state(State::UNKNOWN);
      this->force_kill_tasks();
      IocSensor::instance().send(this, CONTROLLER_EXIT, nullptr);
      return SMI_CONTROLLER_SUCCESS;
    }
    else   {
      if ( cmd == Transition::CONFIGURE )   {
	this->m_dimErrors->start();
	this->set_target_state(State::READY);
      }
      else if ( cmd == Transition::INITIALIZE )
	this->set_target_state(State::READY);
      else if ( cmd == Transition::START )
	this->set_target_state(State::RUNNING);
      else if ( cmd == Transition::CONTINUE )
	this->set_target_state(State::RUNNING);
      else if ( cmd == Transition::STOP )
	this->set_target_state(State::READY);
      else if ( cmd == Transition::PAUSE )
	this->set_target_state(State::PAUSED);
      else if ( cmd == Transition::RESET )   {
	this->m_dimErrors->stop();
	this->set_target_state(State::OFFLINE);
      }
      else if ( cmd == Transition::RECOVER )   {
	this->m_dimErrors->stop();
	this->set_target_state(State::UNKNOWN);
      }
      else if ( cmd == Transition::UNLOAD )   {
	this->m_dimErrors->stop();
	this->set_target_state(State::UNKNOWN);
	IocSensor::instance().send(this, TaskProxy::TASK_LIMBO, this);
      }
      else if ( cmd == "error" )    {
        this->set_target_state(State::ERROR);
	this->declare_state(m_target_state);
	this->m_log->warning("++ User requested transition to ERROR: %s [%s]",
			     cmd.c_str(), m_state.c_str());
	this->m_self->setState(this->m_state.c_str());
	return SMI_CONTROLLER_SUCCESS;
      }
      else    {
	this->m_log->warning("++ Unknown transition: '%s'", cmd.c_str());
      }
      this->declare_state(m_target_state);
      IocSensor::instance().send(this, CONTROLLER_SETSTATE, nullptr);
      return SMI_CONTROLLER_SUCCESS;
    }
    this->declare_state(m_state);
    return SMI_CONTROLLER_SUCCESS;
  }
  return SMI_CONTROLLER_ERROR;
}

//==============================================================================
/// Interactor Interrupt handling routine
void SmiController::handle(const CPP::Event& event)    {
  switch(event.type)  {
  case CONTROLLER_INVOKE_TRANSITION:  {
    unique_ptr<string> cmd(event.iocPtr<string>());
    SmiErrCond ret = this->invoke_transition(cmd->c_str());
    if ( ret == SMI_CONTROLLER_SUCCESS )   {
      this->m_log->info("++ Request to execute transition %s SUCCEEDED",cmd->c_str());
    }
    else   {
      this->m_log->error("++ Request to execute transition %s FAILED",cmd->c_str());
    }
    break;
  }
  case CONTROLLER_EXIT:
    this->declare_state(m_target_state);
    if ( !config.smi_utgid.empty() )   {
      TaskManager::instance(RTL::nodeNameShort(), this->m_tms_dic_dns_ID).kill(config.smi_utgid,SIGKILL);
    }
    this_thread::sleep_for(chrono::milliseconds(200));
    if ( 0 == ::kill(config.pid_smiSM, SIGTERM) )   {
      this_thread::sleep_for(chrono::milliseconds(300));
      ::kill(config.pid_smiSM, SIGKILL);
    }
    this_thread::sleep_for(chrono::milliseconds(500));
    _exit(0);
    return;

  case CONTROLLER_INITIALIZE:
    this->initialize();
    return;

  case CONTROLLER_SETNODESTATE:   {
    static bool first_set_state = true;
    string prev = this->m_node_state;
    unique_ptr<string> st(event.iocPtr<string>());
    this->m_node_state = st->c_str();
    if ( first_set_state )   {
      // If this is the first update and SMI is not UNKNOWN or OFFLINE,
      // the controller is restarting after a crash and we need to reconnect the dependent tasks
      first_set_state = false;
      if ( this->m_node_state == State::READY ||
	   this->m_node_state == State::RUNNING ||
	   this->m_node_state == State::PAUSED )  {
	this->m_state = m_node_state;
	this->m_self->setState(this->m_state.c_str());
	this->set_target_state(this->m_state);
	this->load_tasks();
	this->declare_state(m_state);
      }
    }
    this->m_log->info("++ New controller (NODE) state: %s [prev:%s]", this->m_node_state.c_str(), prev.c_str());
    ::dis_update_service(this->m_state_ID);
    return;
  }

  case CONTROLLER_SETSTATE:
    if ( config.standalone )   {
      const char* state = event.iocPtr<const char>();
      if ( !state ) state = this->m_state.c_str();
      this->m_self->setState(state);
    }
    return;

  case CONTROLLER_PUBLISH_TASKS:
    this->publish_tasks();
    return;

  case CONTROLLER_LOAD_TASKS:
    this->set_target_state(State::NOT_READY);
    this->load_tasks();
    this->declare_state(m_target_state);
    return;

  case CONTROLLER_TASKS_DEAD:
    for( const auto& proxy : this->m_tasks )  {
      const string& utgid = proxy.second->m_task->utgid;
      bool  vip = proxy.second->m_task->isVIP;
      if ( vip )   {
	this->m_log->info("++ %s: [%s] --> REMOVEVIP/UTGID=%s",
			  proxy.second->getDomain(), proxy.second->proxy_state(), utgid.c_str());
	this->m_taskset->sendCommand(("REMOVEVIP/UTGID="+utgid).c_str());
      }
      else   {
	this->m_log->info("++ %s: [%s] --> REMOVETASK/UTGID=%s",
			  proxy.second->getDomain(), proxy.second->proxy_state(), utgid.c_str());
	this->m_taskset->sendCommand(("REMOVETASK/UTGID="+utgid).c_str());
      }
    }
    IocSensor::instance().send(this, CONTROLLER_SETSTATE, (void*)State::OFFLINE);
    IocSensor::instance().send(this, CONTROLLER_PUBLISH_TASKS, this);
    return;

  case TaskProxy::TASK_TRANSITION:
    return;

  case TaskProxy::TASK_TIMEOUT:
  case TaskProxy::TASK_FAILED:   {
    TaskProxy* proxy = event.iocPtr<TaskProxy>();
    this->m_log->warning("++ Dependent Task %-7s: %-32s   %s / %s",
			 event.type==TaskProxy::TASK_TIMEOUT ? "TIMEOUT" : "FAILED",
			 proxy->m_task->utgid.c_str(), proxy->metaStateName(), proxy->proxy_state());
    this->declare_sub_state(MonMetaState::ACTION_FAILED);
    this->check_for_completion();
    IocSensor::instance().send(this, CONTROLLER_PUBLISH_TASKS, this);
    return;
  }

  case TaskProxy::TASK_IDLE:   {
    TaskProxy* proxy = event.iocPtr<TaskProxy>();
    this->m_log->info("++ Dependent Task IDLE: %-32s   %s / %s",
		      proxy->m_task->utgid.c_str(), proxy->metaStateName(), proxy->proxy_state());
    this->declare_sub_state(MonMetaState::ACTION_SUCCESS);
    this->check_for_completion();
    IocSensor::instance().send(this, CONTROLLER_PUBLISH_TASKS, this);
    return;
  }

  default:
    break;
  }
}

#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

vector<const char*> SmiController::setup_smi_child_env()    {
  vector<const char*> env;
  bool have_log = !config.smi_log_file.empty();
  for(size_t i=0; environ[i]; ++i)   {
    if ( have_log && ::strncmp(environ[i],"LOGFIFO=",8) == 0 )
      continue;
    if ( have_log && ::strncmp(environ[i],"LD_PRELOAD=",8) == 0 )
      continue;
    if ( ::strncmp(environ[i],"UTGID=",6) != 0 )
      env.emplace_back(environ[i]);
  }
  if ( have_log )    {
    int fdlog = ::open(config.smi_log_file.c_str(), O_APPEND|O_CREAT, S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH);
    if ( fdlog <= 0 )    {
      m_log->error("++ Failed to open SMI log file %s. Ignored: No log files for SMI....",
		   config.smi_log_file.c_str());
      m_log->error("++               SMI log error %s.",
		   std::error_condition(errno,std::system_category()).message().c_str());
    }
    else   {
      int err;
      err = ::dup2(fdlog, ::fileno(stderr));
      // err = ::dup2(fdlog, 2);
      if ( err < 0 )   {
	m_log->error("++               SMI log error (STDERR) %s.", ::strerror(errno));
      }
      err = ::dup2(fdlog, ::fileno(stdout));
      // err = ::dup2(fdlog, 1);
      if ( err < 0 )   {
	m_log->error("++               SMI log error (STDOUT) %s.", ::strerror(errno));
      }
    }
  }
  return env;
}

#include <sys/prctl.h>

//==============================================================================
/// SMI startup
void SmiController::start_smi()    {
  if ( !this->m_smi_started )    {
    if ( config.standalone )   {
      if ( config.smi_file.empty() )   {
	this->m_log->error("++ Invalid argument -smi_file='%s'\n",config.smi_file.c_str());
	::exit(EINVAL);
      }

      struct stat buff;
      string smi_file = config.smi_file + ".sobj";
      if ( -1 == ::stat(smi_file.c_str(), &buff) )   {
	this->m_log->error("++ No valid SMI file specified which describes the FSM! [%s: %s]",
			   config.smi_file.c_str(),
			   std::error_condition(errno,std::system_category()).message().c_str());
	::exit(EINVAL);
      }
      if ( config.standalone == 2 )   {
	this->m_log->info("++ Executing SMI server as in-process thread.");
	void* handle = LOAD_LIB("libsmixx_SM.so");
	if ( handle )  {
	  Function fun(GETPROC(handle, "main"));
	  if ( fun.function )    {
	    thread* thr = new thread([] ()   {
	      IocSensor::instance().run();
	    });
	    if ( thr )   {
	      m_log->info("++ Executing internal SMI server in thread.");
	      const char* args[] = {"smiSM", "-u", "-t", "-d", config.smi_debug.c_str(),
		"-loopMaxChangesFatal", "100",
		"-dns", config.smi_dns.c_str(),
		config.smi_domain.c_str(),
		config.smi_file.c_str(), nullptr};
	      /// Wait with server start until controller is initialized
	      while ( !this->m_inited )
		this_thread::sleep_for(chrono::milliseconds(100));
	      this_thread::sleep_for(chrono::milliseconds(1000));
	      /// function will never return....
	      (*fun.function)(sizeof(args)/sizeof(args[0])-1, (char**)args);
	      thr->join();
	    }
	  }
	}
      }
      else if ( config.standalone == 1 )   {
	this->m_log->info("++ Executing external SMI server in child process.");
	config.smi_utgid = RTL::processName()+"_SMI";
	thread* thr = new thread([this] ()   {
	  int   ppid = ::lib_rtl_pid();
	  pid_t pid = ::fork();
	  if ( pid == 0 )  {  // Child
	    string utgid = "UTGID=" + config.smi_utgid;
	    const char* args[] = {utgid.c_str()+6, "libsmixx_SM.so", "main", 
	      "-u", "-t", "-d", config.smi_debug.c_str(), 
	      "-loopMaxChangesFatal", "100",
	      "-dns", config.smi_dns.c_str(),
	      config.smi_domain.c_str(),
	      config.smi_file.c_str(), nullptr};
	    vector<const char*> env = setup_smi_child_env();
	    env.emplace_back(utgid.c_str());
	    if ( ::getppid() != ppid )   {
	      // Error!
	    }
	    if ( -1 == ::prctl(PR_SET_PDEATHSIG, SIGTERM) )   {
	      this->m_log->error("++ Failed to inherit parent death signal! [%s]",
				 std::error_condition(errno,std::system_category()).message().c_str());
	    }
	    if ( config.smi_log_file.empty() )   {
	      env.emplace_back(nullptr);
	      ::execvpe("genRunner",(char**)args, (char**)&env[0]);
	    }
	    else   {
	      env.emplace_back(("LOGFILE_APPEND="+config.smi_log_file).c_str());
	      env.emplace_back(nullptr);
	      ::execvpe("gentest",(char**)args, (char**)&env[0]);
	    }
	    m_log->error("++ Failed to execvp to smiSM! [%s]",
			 std::error_condition(errno,std::system_category()).message().c_str());
	    ::exit(errno);
	  }
	  else if ( pid > 0 )  {
	    config.pid_smiSM = pid;
	    while(1)   {
	      int status = 0;
	      pid_t dead_id = ::waitpid(pid, &status, 0);
	      if ( (dead_id > 0) && this->m_target_state != State::UNKNOWN )    {
		m_log->error("++ SMI process %d [%d] died.... Status:%d", dead_id, pid, status);
		//::exit(ESRCH);
		break;
	      }
	      else if ( (dead_id == -1) && (-1 == ::kill(pid, 0)) )  {
		break;
	      }
	    }
	    this->set_target_state(State::UNKNOWN);
	    this->force_kill_tasks();
	    IocSensor::instance().send(this, CONTROLLER_EXIT, nullptr);
	  }
	  else if ( pid < 0 )    {        // failed to fork
	    this->m_log->error("++ Failed to fork child process for smiSM! [%s]",
			       std::error_condition(errno,std::system_category()).message().c_str());
	    ::exit(errno);
	  }
	});
	if ( !thr )   {
	}
      }
      else if ( config.standalone == 3 )   {
	this->m_log->info("++ Executing external SMI server in child process.");
	config.smi_utgid = RTL::processName()+"_SMI";
	thread* thr = new thread([this] ()   {
	  pid_t pid = ::fork();
	  if ( pid == 0 )  {  // Child
	    string utgid = "UTGID=" + config.smi_utgid;
	    string command = config.smi_script + " libsmixx_SM.so main -d " + config.smi_debug + 
	      " -u -t -dns " + config.smi_dns + " -loopMaxChangesFatal 100 " + 
	      config.smi_domain + " " + config.smi_file;
	    const char* args[] = {utgid.c_str()+6, "--norc", "-c", command.c_str(), nullptr};
	    vector<const char*> env = setup_smi_child_env();
	    env.emplace_back(utgid.c_str());
	    env.emplace_back(nullptr);
	    ::lib_rtl_sleep(1000);
	    RTL::Logger::set_io_buffering(RTL::Logger::LINE_BUFFERING);
	    ::execvpe("bash",(char**)args, (char**)&env[0]);
	    this->m_log->error("++ Failed to execvp to smiSM! [%s]",
			       std::error_condition(errno,std::system_category()).message().c_str());
	    ::exit(errno);
	  }
	  else if ( pid > 0 )  {
	    config.pid_smiSM = pid;
	    while(1)   {
	      int status = 0;
	      pid_t dead_id = ::waitpid(pid, &status, 0);
	      if ( dead_id == pid && WIFEXITED(status) )    {
		if ( this->m_target_state != State::UNKNOWN )    {
		  this->m_log->error("++ SMI process %d [%d] died.... Status:%d", dead_id, pid, WEXITSTATUS(status));
		  //::exit(ESRCH);
		  break;
		}
	      }
	    }
	  }
	  else if ( pid < 0 )    {        // failed to fork
	    this->m_log->error("++ Failed to fork child process for smiSM! [%s]",
			       std::error_condition(errno,std::system_category()).message().c_str());
	    ::exit(errno);
	  }
	});
	if ( !thr )   {
	}
      }
    }
    //this_thread::sleep_for(chrono::milliseconds(2000));
    ::lib_rtl_sleep(2000);
  }
  this->m_smi_started = true;
}

//==============================================================================
/// Run the process and start the SMI engine
void SmiController::run()   {
  // In standalone mode start the in-process smi state manager
  this->start_smi();
  IocSensor::instance().run();
}
