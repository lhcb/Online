//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Author     : Markus Frank
//==========================================================================
//
//      Finite state machine implementation to control
//      and manipulate process groups
//
//==========================================================================

// Framework include files
#include <SmiController/SmiTask.h>
#include <RTL/rtl.h>

// C/C++ include files
#include <stdexcept>
#include <cstring>

using namespace FiniteStateMachine;
using namespace std;

/// Class Constructor
SmiTask::SmiTask(const std::string& nick_name, const std::string& utg)
  : nick(nick_name), utgid(utg)
{
}

/// Clone the environment of the existing process
void SmiTask::clone_env()  {
  envp.clear();
  for(char** e=environ; *e; ++e) envp.push_back(*e);
}

/// Set the process arguments from single string
void SmiTask::set_args(const string& args)  {
  argv.clear();
  add_args(args);
}

/// Set the process arguments from single string
void SmiTask::add_args(const string& args)  {
  std::string copy = "--";
  copy = args.c_str();
  for( char* p = (char*)copy.c_str(), *savePtr=0;; p=0)  {
    char* token = ::strtok_r(p," ",&savePtr);
    if ( !token ) break;
    argv.push_back(token);
  }
}

/// Set the FMC arguments from single string
void SmiTask::set_fmc_args(const string& args)  {
  fmcArgs.clear();
  add_fmc_args(args);
}

/// Add the FMC arguments from single string
void SmiTask::add_fmc_args(const string& args)  {
  std::string copy = "--";
  copy = args.c_str();
  for( char* p = (char*)copy.c_str(), *savePtr=0;; p=0)  {
    char* token = ::strtok_r(p," ",&savePtr);
    if ( !token ) break;
    fmcArgs.push_back(token);
  }
}

/// Add entry in transition timeout table (timeout in seconds)
void SmiTask::add_timeout(const std::string& tr, int value)  {
  timeouts[tr] = value;
}

/// Remove entry in transition timeout table
void SmiTask::remove_timeout(const std::string& tr)  {
  TimeoutTable::iterator i = timeouts.find(tr);
  if ( i != timeouts.end() ) timeouts.erase(i);
}
