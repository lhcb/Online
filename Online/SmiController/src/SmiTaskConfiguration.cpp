//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Author     : Markus Frank
//==========================================================================
//
//      Finite state machine implementation to control
//      and manipulate process groups
//
//==========================================================================
// Framework include files
#include "SmiController/SmiTaskConfiguration.h"
#include "SmiController/TasklistHandlers.h"
#include "SmiController/SmiTask.h"
#include "XML/XML.h"
#include "RTL/strdef.h"
#include "RTL/Logger.h"
#include "RTL/Sys.h"
#include "RTL/rtl.h"

// C/C++ include files
#include <iostream>
#include <sstream>
#include <climits>
#include <unistd.h>

using namespace std;
using namespace Online;
using namespace FiniteStateMachine;

//==============================================================================
/// Standard constructor
SmiTaskConfiguration::SmiTaskConfiguration(const replacements_t& repl,
					   const string& part, 
					   const string& cfg, 
					   const string& info,
					   const string& out,
					   const string& err)
  : m_replacements(repl), m_partition(part),  m_config(cfg), m_runinfo(info), 
    m_stdout_file(out), m_stderr_file(err)
{
}

//==============================================================================
/// Default destructor
SmiTaskConfiguration::~SmiTaskConfiguration()  {
}

//==============================================================================
/// Analyse the configuration file and attach the corresponding slaves to the FSM machine
std::map<std::string,SmiTask*> SmiTaskConfiguration::taskList(int bind_cpu, int print_level)  {
  Tasklist tasks;
  size_t num_sockets = 1;
  map<int,string> affinity_args;
  TasklistAnalyzer analyzer(tasks);
  char text[32], instances_text[64];
  std::map<std::string,SmiTask*> slave_tasks;
  string node = RTL::str_upper(RTL::nodeNameShort());
  RTL::Logger log(RTL::Logger::getGlobalDevice(), "XmlConfig", print_level);

  affinity_args[0] = "";
  if ( bind_cpu != 0 )  {
    vector<RTL::SystemCPU> cpu_info;
    RTL::read(cpu_info);
    set<int> physical_cpu_slots = RTL::physical_cpu_ids(cpu_info);
    num_sockets = physical_cpu_slots.size();
    for(int i=0, n=int(num_sockets); i<n; ++i)   {
      stringstream str;
      set<int> pslots = RTL::processor_ids(cpu_info,i);
      for(std::set<int>::const_iterator it=pslots.begin(); it!=pslots.end();++it) 
	str << " -a " << (*it);
      affinity_args[i] = str.str();
    }
  }

  log.info("Partition: %s Node: %s sockets: %d",
	   m_partition.c_str(), node.c_str(), num_sockets);

  xml::enableEnvironResolution(false);
  for( const auto& r : m_replacements )   {
    try   {
      xml::_toDictionary(Unicode(r.first),Unicode(r.second));
    }
    catch(...)   {
    }
  }
  xml::DocumentHolder doc(xml::DocumentHandler().load(m_config));
  xml_h inventory = doc.root();
  xml_coll_t(inventory,_Unicode(task)).for_each(analyzer);
  xml_coll_t(inventory,_Unicode(include)).for_each(analyzer);
  log.info("------------------------------------ Task list -------------------------------------");
  for(Tasklist::Tasks::const_iterator i=tasks.begin(); i!=tasks.end(); ++i)  {
    Tasklist::Task* t = *i;
    long   instances = t->instances;
    string arguments = t->arguments(), fmc_start = t->fmcStartParams(), utgid=t->utgid;
    fmc_start = RTL::str_replace(fmc_start, "${NODE}",         node);
    fmc_start = RTL::str_replace(fmc_start, "${PARTITION}",    m_partition);
    fmc_start = RTL::str_replace(fmc_start, "${RUNINFO}",      m_runinfo);
    fmc_start = RTL::str_replace(fmc_start, "${NAME}",         t->name);
    arguments = RTL::str_replace(arguments, "${NODE}",         node);
    arguments = RTL::str_replace(arguments, "${PARTITION}",    m_partition);
    arguments = RTL::str_replace(arguments, "${RUNINFO}",      m_runinfo);
    arguments = RTL::str_replace(arguments, "${NAME}",         t->name);
    arguments = RTL::str_replace(arguments, "${ARCHITECTURE}", m_config);
    utgid     = RTL::str_replace(utgid,     "${NODE}",         node);
    utgid     = RTL::str_replace(utgid,     "${PARTITION}",    m_partition);
    utgid     = RTL::str_replace(utgid,     "${RUNINFO}",      m_runinfo);
    utgid     = RTL::str_replace(utgid,     "${NAME}",         t->name);
    map<int,int> num_instance, init_instance, task_instance;

    // Init counters
    for(size_t j=0; j<=num_sockets; ++j) {
      num_instance[j]  = 0;
      init_instance[j] = 0;
      task_instance[j] = 0;
    }

    // Determine the number of task instances per CPU slot
    for( long j=0; j < instances; ++j )
      ++init_instance[j%num_sockets];
    for( long j=0; j < instances; ++j )
      ++num_instance[j%num_sockets];

    // Create 'real' and 'internal' slaves
    for( long j=0; j < instances; ++j )   {
      // If bind_cpu=1: start with numa node 0, if bind_cpu=2 start with numa node 1 etc.
      int  cpu_slot  = (bind_cpu>0) ? (j+bind_cpu-1)%num_sockets : j%num_sockets;
      int  proc_slot = cpu_slot%num_sockets;
      int  task_id   = task_instance[cpu_slot];
      if ( bind_cpu && instances>0 )
	::snprintf(text,sizeof(text),"%1d%02d", proc_slot, task_id);
      else
	::snprintf(text,sizeof(text),"%d", int(j));
      string instance_utgid = RTL::str_replace(utgid, "${INSTANCE}", text);
      string instance_args  = RTL::str_replace(arguments, "${INSTANCE}", text);
      string instance_fmc   = RTL::str_replace(fmc_start, "${INSTANCE}", text) + " -DUTGID="+instance_utgid;
      string nick_name      = (t->name + "_") + text;
      string cmd            = t->command;

      ++task_instance[cpu_slot];
      ::snprintf(instances_text,sizeof(instances_text),"%d",int(init_instance[proc_slot]-1));

      instance_args += " -instances=";
      instance_args += instances_text;
      log.info("+---- Task:%s [%ld/%ld] UTGID: %s. %s %s [%s slave]",
	       t->name.c_str(), j, instances, instance_utgid.c_str(),
	       "Total number of processes to be forked:",
	       instances_text, "PHYSICAL");
      string affinity = instances > 0 ? affinity_args[cpu_slot] : string("");
      /// In this section we only handle FMC started processes
      instance_fmc += " -O " + m_stdout_file + " -E " + m_stderr_file;
      // Set information about the CPU slot the process should execute on
      if ( bind_cpu && instances>1 )   {
	::snprintf(text,sizeof(text)," -DCPU_SLOT=%d",cpu_slot);
	instance_fmc += text;
	instance_fmc += affinity;
      }
      instance_args += " -logfifo=" + m_stdout_file;
      for( const auto& r : m_replacements )   {
	string pattern = "${"+r.first+"}";
	instance_args = RTL::str_replace(instance_args, pattern, r.second);
	instance_fmc  = RTL::str_replace(instance_fmc, pattern, r.second);
	nick_name     = RTL::str_replace(nick_name, pattern, r.second);
	utgid         = RTL::str_replace(utgid, pattern, r.second);
	cmd           = RTL::str_replace(cmd, pattern, r.second);
      }
      SmiTask* s = new SmiTask(nick_name, instance_utgid);
      slave_tasks[instance_utgid] = s;
      s->set_fmc_args(instance_fmc);
      s->set_args(instance_args);
      s->doStart = t->doStart;
      s->isVIP   = t->isVIP;
      s->command = cmd;
      for(auto it=t->timeouts.begin(); it!=t->timeouts.end(); ++it)
	s->add_timeout(it->action, it->timeout);
      log.info("|     tmStart -m %s %s %s %s",
	       node.c_str(),instance_fmc.c_str(),cmd.c_str(),instance_args.c_str());
    }
  }
  return slave_tasks;
}
