//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Author     : Markus Frank
//==========================================================================
//
//      Finite state machine implementation to control
//      and manipulate process groups
//
//==========================================================================

// Framework include files
#include <SmiController/TaskManager.h>
#include <SmiController/TaskManagerFMC.h>
#include <SmiController/TaskManagerFork.h>
#include <RTL/strdef.h>
#include <RTL/rtl.h>

// C/C++ include files
#include <stdexcept>
#include <string>
#include <map>

namespace {

  enum tm_types  {
    TASKMANAGER_FMC,
    TASKMANAGER_FORK
  } s_tm_type { TASKMANAGER_FMC };

}

/// Set task manager type
void FiniteStateMachine::TaskManager::set_type_default(const std::string& type)  {
  if ( RTL::str_upper(type).find("FMC") == 0 )
    s_tm_type = TASKMANAGER_FMC;
  else if ( RTL::str_upper(type).find("FORK") == 0 )
    s_tm_type = TASKMANAGER_FORK;
  else
    ::lib_rtl_output(LIB_RTL_ERROR,"+++ Unknown task manager type requested: %s", type.c_str());
}

/// Instance accessor
FiniteStateMachine::TaskManager&
FiniteStateMachine::TaskManager::instance(const std::string& node, long domain)  {
  static std::map<std::string,TaskManager*> m;
  std::map<std::string,TaskManager*>::iterator i = m.find(node);
  if ( i == m.end() ) {
    switch ( s_tm_type )   {
    case TASKMANAGER_FMC:
      i = m.insert(std::make_pair(node,new TaskManagerFMC(node, domain))).first;
      break;
    case TASKMANAGER_FORK:
      i = m.insert(std::make_pair(node,new TaskManagerFork(node))).first;
      break;
    default:
      ::lib_rtl_output(LIB_RTL_ERROR,"+++ Unknown task manager type requested: %d", int(s_tm_type));
      throw std::runtime_error("+++ Unknown task manager type requested: " + std::to_string(s_tm_type));
    }
  }
  return *((*i).second);
}
