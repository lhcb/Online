//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Author     : Markus Frank
//==========================================================================
//
//      Finite state machine implementation to control
//      and manipulate process groups
//
//==========================================================================

// Framework include files
#include <SmiController/TaskManagerFork.h>
#include <RTL/strdef.h>
#include <RTL/rtl.h>

// C/C++ include files
#include <cstdarg>
#include <map>

using namespace FiniteStateMachine;

namespace  {
  
  int start_process(RTL::Process* process)   {
    if ( process )   {
      if ( !process->is_running() )   {
	::lib_rtl_output(LIB_RTL_ALWAYS,"+++ Fork process: %s",
			 process->to_string().c_str());
	process->enable_parent_death(SIGTERM);
	return process->start(false);
      }
      return 1;
    }
    return 0;
  }

  int kill_process(RTL::Process* process, int signum, int wait)   {
    if ( process )   {
      if ( !process->is_running() )   {
	::lib_rtl_output(LIB_RTL_ALWAYS,"+++ Process: %s send signal %s",
			 process->name().c_str(), RTL::signalName(signum).c_str());
	process->signalall(signum);
	if ( wait > 0 )  {
	  if ( process->wait_for(wait) != process->ENDED )  {
	    process->signalall(SIGKILL);
	    process->wait();
	  }
	}
	else if ( process->is_running() )   {
	  switch ( signum )  {
	  case SIGKILL:
	  case SIGTERM:
	  case SIGQUIT:
	  case SIGSEGV:
	    process->wait_for(500);
	    break;
	  default:
	    break;
	  }
	}
      }
      return 1;
    }
    return 0;
  }
}

/// Standard constructor
TaskManagerFork::TaskManagerFork(const std::string& node)  {
  std::string n = RTL::str_upper(node);
  std::string c = RTL::str_upper(RTL::nodeName());
  if ( c.find(n) != 0 && n != "LOCALHOST" && n != "127.0.0.1" )   {
    throw std::runtime_error("+++ Forking TaskManager can only work on local node!");
  }
}

/// Start a process
int TaskManagerFork::start(const std::string& utgid, const std::string& fmc_args, const std::string& cmd, const std::string& args) {
  Processes::iterator itr = m_processes.find(utgid);
  std::vector<std::string> fmc_arguments = RTL::str_split(fmc_args, " ");
  std::vector<std::string> arguments;
  std::vector<std::string> env_val;
  std::string str_log = ".log";
  std::string tmp;
  std::size_t idx;
  
  if ( itr == m_processes.end() )   {
    auto p = std::make_unique<RTL::Process>(utgid, cmd, arguments);
    itr = m_processes.emplace(utgid,std::move(p)).first;
  }

  auto& proc = itr->second;
  // Note: if the process is already running, new values are not taken into account!
  arguments = RTL::str_split(RTL::str_replace(args,"&quot;","'"), " ");
  proc->setArgs(std::move(arguments));
  for ( std::size_t i=0; i<fmc_arguments.size(); ++i )   {
    const auto& a = fmc_arguments[i];
    if ( a.length() >= 2 )   {
      if ( a == "-e" ) continue;
      if ( a == "-o" ) continue;
      if ( a == "-d" ) continue;
      if ( a[0] == '-' )    {
	switch(a[1])   {
	case 'u':
	  ++i;
	  break;
	case 'd':
	  ++i;
	  break;
	case 'e':
	case 'o':
	  ++i;        // Default output fifo. Redirect to local per process log file
	case 'E':
	case 'O':
	  idx = utgid.find('_');
	  tmp = (idx == std::string::npos) ? utgid : utgid.substr(idx+1);
	  proc->setOutput(tmp+".log");
	  break;
	case 'a':
	  proc->affinity = fmc_arguments[++i];
	  break;
	case 'r':
	  proc->priority = fmc_arguments[++i];
	  break;
	case 'p':
	  proc->nice = fmc_arguments[++i];
	  break;
	case 'w':
	  proc->working_dir = fmc_arguments[++i];
	  break;
	case 'D':
	  if ( a.length() > 2 )
	    env_val = RTL::str_split(fmc_arguments[i].substr(2), "=");
	  else   {
	    auto n = fmc_arguments[++i];
	    idx = n.find("=");
	    env_val = { n.substr(0, idx), n.substr(idx+1) };
	  }
	  proc->updateEnvironment(env_val[0], RTL::str_replace(env_val[1],"&quot;","'"));
	  break;
	}
      }
    }
  }
  return start_process(proc.get());
}

/// Kill a process
int TaskManagerFork::stop(const std::string& utgid, int sig_num, int wait_before_kill)  {
  Processes::iterator i = m_processes.find(utgid);
  if ( i != m_processes.end() )   {
    return kill_process((*i).second.get(), sig_num, wait_before_kill);
  }
  return 0;
}

/// Kill a process
int TaskManagerFork::kill(const std::string& utgid, int sig_num) {
  Processes::iterator i = m_processes.find(utgid);
  if ( i != m_processes.end() )   {
    return kill_process((*i).second.get(), sig_num, 0);
  }
  return 0;
}
