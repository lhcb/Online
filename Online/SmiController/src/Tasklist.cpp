//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Author     : Markus Frank
//==========================================================================
//
//      Finite state machine implementation to control
//      and manipulate process groups
//
//==========================================================================

// Framework include files
#include "SmiController/Tasklist.h"

using namespace std;
using namespace FiniteStateMachine;

/// String with the FMC parameters propery concatenated
string Tasklist::Task::fmcStartParams() const  {
  string res = "";
  if ( !group.empty() ) res += " -g "+group;
  if ( !user.empty()  ) res += " -n "+user;
  for(auto i=fmcParams.begin(); i!=fmcParams.end(); ++i)  {
    res += " ";
    res += (*i).second;
  }
  return res;
}

/// String with the argument list propery concatenated
string Tasklist::Task::arguments() const  {
  string res;
  for(auto i=args.begin(); i!=args.end(); )  {
    res += (*i).first;
    if ( !(*i).second.empty() ) res += "="+(*i).second;
    ++i;
    if ( i != args.end() ) res += " ";
  }
  return res;
}

/// Constructor
Tasklist::Tasklist() {
}

/// Standard destructor
Tasklist::~Tasklist()   {
  for (Tasklist::Tasks::iterator i=tasks.begin(); i!=tasks.end(); ++i)
    delete (*i);
  tasks.clear();
}
