//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Author     : Markus Frank
//==========================================================================
//
//      Finite state machine implementation to control
//      and manipulate process groups
//
//==========================================================================

// Framework include files
#include <SmiController/TasklistHandlers.h>
#include <XML/XML.h>
#include <RTL/rtl.h>
#include <RTL/strdef.h>

// C/C++ include files
#include <iostream>
#include <unistd.h>

using namespace dd4hep;
using namespace dd4hep::xml;
using namespace FiniteStateMachine;

DECLARE_UNICODE_TAG(tasks_inventory)
DECLARE_UNICODE_TAG(task)
DECLARE_UNICODE_TAG(command)
//DECLARE_UNICODE_TAG(argument)
DECLARE_UNICODE_TAG(fmcparam)
DECLARE_UNICODE_TAG(ioparam)
DECLARE_UNICODE_TAG(action)
DECLARE_UNICODE_TAG(user)
DECLARE_UNICODE_TAG(group)
DECLARE_UNICODE_TAG(instances)
DECLARE_UNICODE_TAG(timeout)
DECLARE_UNICODE_TAG(from)
DECLARE_UNICODE_TAG(to)
//DECLARE_UNICODE_TAG(start)
DECLARE_UNICODE_TAG(vip)

namespace {
  std::string _xml_escape(const std::string& val)   {
#if 0
    std::string v = RTL::str_replace(val, "&quot;","\"");
    v = RTL::str_replace(v, "&apos","'");
    v = RTL::str_replace(v, "&lt","<");
    v = RTL::str_replace(v, "&gt",">");
    if ( val != v )  {
      std::cout << "Replaced: " << val << " " << v << std::endl; 
    }
    return v;
#endif
    return val;
  }
}

namespace FiniteStateMachine {
  struct TaskParams : public xml_h {
    TaskParams(xml_h h) : xml_h(h) {}
    std::string name()  const     { return  _xml_escape(attr<std::string>(_U(name)));   }
    std::string user()  const     { return  hasAttr(_U(user))  ? attr<std::string>(_U(user))  : std::string(); }
    std::string group() const     { return  hasAttr(_U(group)) ? attr<std::string>(_U(group)) : std::string(); }
    std::size_t instances() const { return  hasAttr(_U(instances)) ? attr<int>(_U(instances)) : 1;        }
    std::string command() const   { return  _xml_escape(child(_U(command)).text());     }
    bool hasValue() const         { return  hasAttr(_U(value));            }
    std::string type()  const     { return  _xml_escape(attr<std::string>(_U(type)));   }
    std::string value() const     { return  _xml_escape(attr<std::string>(_U(value)));  }
    std::string action() const    { return  _xml_escape(attr<std::string>(_U(action))); }
    int timeout() const           { return  attr<int>(_U(value)); }
    int is_vip() const            { return  hasAttr(_U(vip)) ? attr<bool>(_U(vip)) : false; }
    int start() const             {
      std::string a = hasAttr(_U(start)) ? attr<std::string>(_U(start)) : "True";
      a[0] = ::toupper(a[0]);
      return  a[0] == 'T' || a[0] == 'Y' || a[0] == '1';
    }
  };
}

/// Action operator when analyzing data
void TasklistPrinter::operator()(const xml_h& h)  {
  std::string pref = prefix.empty() ? prefix : prefix + "  ";
  if ( h.tag() == "task" )   {
    TaskParams t = h;
    std::cout << pref 
	 << "+-- Task:" << t.name() 
	 << " [" << t.user() << "," << t.group() << "] " 
	 << " VIP: " << (const char*)(t.is_vip() ? "true" : "false")
	 << " Instances: " << t.instances()
	 << std::endl;
    if ( t.start() )   {
      std::cout << pref
	   << "|   Command:" << t.command()
	   << std::endl << "|   IO:     ";
      std::cout << pref << "|   I/O parameters: ";
      xml_coll_t(t,_U(ioparam)).for_each(Args());
      std::cout << std::endl << pref << "|   Args:   ";
      xml_coll_t(t,_U(argument)).for_each(Args());
      std::cout << std::endl << pref << "|   TMO:    ";
      xml_coll_t(t,_U(timeout)).for_each(Timeouts());
      std::cout << std::endl << pref << "|   FMC:    ";
      xml_coll_t(t,_U(fmcparam)).for_each(Params());
    }
    else   {
      std::cout << pref << "|   ******** Task not started by controller!";
    }
  }
  else if ( h.tag() == "include" )   {
    const auto* ref = h.attr<const XmlChar*>(_U(ref));
    DocumentHolder doc(xml::DocumentHandler().load(h, ref));
    std::cout << pref << "|   Include: " << h.attr<std::string>(_U(ref)) << std::endl;
    xml_coll_t(doc.root(),_Unicode(task)).for_each(*this);
    xml_coll_t(doc.root(),_Unicode(include)).for_each(*this);
  }
  else if ( h.tag() == "param" )   {
    TaskParams par(h);
    std::cout << pref << "|   Param:  [" << par.type() << "] "
	      << par.name() << " = " << par.value() << std::endl;
  }
  std::cout << std::endl 
       << pref << "+-----------------------------------------------------------------------------------" 
       << std::endl;
}

/// Action operator when analyzing data
void TasklistPrinter::Args::operator()(const xml_h& h)  {
  TaskParams a = h;
  std::cout << a.name() << "=" << a.value() << " ";
}

/// Action operator when analyzing data
void TasklistPrinter::Timeouts::operator()(const xml_h& h)  {
  TaskParams a = h;
  std::cout << std::endl << "   Timeout: " << a.action() << " = " << a.timeout();
}

/// Action operator when analyzing data
void TasklistPrinter::Params::operator()(const xml_h& h)  {
  TaskParams p = h;
  std::string nam = p.name();
  std::string val = p.hasValue() ? p.value() : std::string();
  if      ( nam == "define"                 ) std::cout << " -D " << val;
  else if ( nam == "utgid"                  ) std::cout << " -u " << val;
  else if ( nam == "wd"                     ) std::cout << " -w " << val;
  else if ( nam == "priority"               ) std::cout << " -r " << val;
  else if ( nam == "nice"                   ) std::cout << " -p " << val;
  else if ( nam == "stderr" && val.empty()  ) std::cout << " -e";
  else if ( nam == "stdout" && val.empty()  ) std::cout << " -o";
  else if ( nam == "stderr"                 ) std::cout << " -E " << val;
  else if ( nam == "stdout"                 ) std::cout << " -O " << val;
  else if ( nam == "affinity"               ) std::cout << " -a " << val;
  else if ( nam == "daemon"                 ) std::cout << " -d ";
}

/// Action operator for printing a single task of a task-list
void TasklistPrinter::operator()(const Tasklist::Timeouts& timeouts)   {
  for(const auto& to : timeouts)
    (*this)(to);
}

/// Action operator for printing a single task of a task-list
void TasklistPrinter::operator()(const Tasklist::Timeout& to)   {
  std::cout << to.action << "=" << to.timeout << " ";
}

/// Action operator for printing a single task of a task-list
void TasklistPrinter::operator()(const Tasklist::Params& pars)   {
  for(const auto& p : pars)
    (*this)(p);
}

/// Action operator for printing a single task of a task-list
void TasklistPrinter::operator()(const Tasklist::Param& p)   {
  std::cout << p.first << "=" << p.second << " ";
}

/// Action operator for printing a single task of a task-list
void TasklistPrinter::operator()(const Tasklist& tasklist)   {
  for( const Tasklist::Task* t : tasklist.tasks )
    (*this)(*t);
}

/// Action operator for printing a single task of a task-list
void TasklistPrinter::operator()(const Tasklist::Task& t)   {
  std::string pref = prefix.empty() ? prefix : prefix + "  ";
  std::cout << pref 
       << "+-- Task:" << t.name
       << " [" << t.user << "," << t.group << "] " 
       << " VIP: " << (const char*)(t.isVIP ? "true" : "false")
       << " Instances: " << t.instances
       << std::endl;
  if ( t.doStart )   {
    std::cout << pref << "|   Command:" << t.command;
    std::cout << std::endl << pref << "|   I/O parameters: ";
    (*this)(t.ioParams);
    std::cout << std::endl << pref << "|   Args:   ";
    (*this)(t.args);
    std::cout << std::endl << pref << "|   TMO:    ";
    (*this)(t.timeouts);
    std::cout << std::endl << pref << "|   FMC:    ";
    (*this)(t.fmcParams);
  }
  else   {
    std::cout << pref << "|   ******** Task not started by controller!";
  }
  std::cout << std::endl;
}

/// Action operator when analyzing data
void TasklistAnalyzer::operator()(const xml_h& h)  {
  if ( h.tag() == "task" )   {
    TaskParams t    = h;
    Task* task      = new Task();
    task->name      = t.name();
    task->user      = t.user();
    task->group     = t.group();
    task->command   = t.command();
    task->instances = t.instances();
    task->doStart   = t.start();
    task->isVIP     = t.is_vip();
    tasks.tasks.push_back(task);
    xml_coll_t(t,_U(argument)).for_each(Args(task));
    xml_coll_t(t,_U(fmcparam)).for_each(Params(task));
    xml_coll_t(t,_U(ioparam)).for_each(Params(task));
    xml_coll_t(t,_U(timeout)).for_each(Timeouts(task));
    return;
  }
  else if ( h.tag() == "include" )   {
    const auto* ref = h.attr<const XmlChar*>(_U(ref));
    DocumentHolder doc(xml::DocumentHandler().load(h, ref));
    xml_coll_t(doc.root(),_Unicode(task)).for_each(*this);
    xml_coll_t(doc.root(),_Unicode(include)).for_each(*this);
  }
  else if ( h.tag() == "param" )   {
  }
}

/// Action operator when analyzing data
void TasklistAnalyzer::Args::operator()(const xml_h& h)  {
  TaskParams  par = h;
  std::string val = par.hasValue() ? par.value() : std::string();
  task->args.push_back(std::make_pair(par.name(),val));
}

/// Action operator when analyzing data
void TasklistAnalyzer::Timeouts::operator()(const xml_h& h)  {
  TaskParams a = h;
  task->timeouts.push_back(Tasklist::Timeout(RTL::str_lower(a.action()),a.timeout()));
}

/// Action operator when analyzing data
void TasklistAnalyzer::Params::operator()(const xml_h& h)  {
  TaskParams p = h;
  std::string fmc_par, nam = p.name(), val = p.hasValue() ? p.value() : std::string();
  if      ( nam == "define"                 ) fmc_par = "-D " + val;
  else if ( nam == "wd"                     ) fmc_par = "-w " + val;
  else if ( nam == "priority"               ) fmc_par = "-r " + val;
  else if ( nam == "nice"                   ) fmc_par = "-p " + val;
  else if ( nam == "affinity"               ) fmc_par = "-a " + val;
  else if ( nam == "daemon"                 ) fmc_par = "-d ";
  else if ( nam == "start"                  ) task->doStart = ::toupper(val[0])=='T' || ::toupper(val[0])=='Y';
  else if ( nam == "utgid"                  ) task->utgid   = val;
  else if ( nam == "output"                 ) task->ioParams.push_back(std::make_pair(nam,val.c_str()));
  else if ( nam == "input"                  ) task->ioParams.push_back(std::make_pair(nam,val.c_str()));
  else if ( nam == "stderr" || nam == "stdout" )  {
    val = RTL::fileFromDescriptor(nam=="stdout" ? STDOUT_FILENO : STDERR_FILENO);
    fmc_par = (nam=="stdout") ? "-O " : " -E ";
    fmc_par += val;
    fmc_par += " -D LOGFIFO=";
    fmc_par += val;
  }
#if 0
  else if ( nam == "stderr" && val.empty()  ) fmc_par = "-e";
  else if ( nam == "stdout" && val.empty()  ) fmc_par = "-o";
  else if ( nam == "stderr"                 ) fmc_par = "-E " + val;
  else if ( nam == "stdout"                 ) fmc_par = "-O " + val;
#endif

  if ( !fmc_par.empty() )  {
    task->fmcParams.push_back(std::make_pair(nam,fmc_par.c_str()));
  }
}

