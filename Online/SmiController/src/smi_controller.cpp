//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include <SmiController/SmiController.h>
#include <SmiController/TaskManager.h>
#include <RTL/FmcLogDevice.h>
#include <RTL/Logger.h>
#include <RTL/strdef.h>
#include <RTL/rtl.h>
#include <dim/dis.h>
#include <dim/dic.h>

// C/C++ include files
#include <cstdio>
#include <cerrno>
#include <climits>
#include <unistd.h>
#include <sys/stat.h>

using namespace FiniteStateMachine;

static void help_ctrl() {
  ::fprintf(stdout,
	    " fsm_ctrl -arg=<value> [ -arg=<value> ... ]                                            \n"
	    "          -help                     Print this help message.                           \n"
	    "          -debug                    Sleep until debugger is attached.                  \n"
	    "          -dns=[node-name]          Main DNS node name.                                \n"
	    "          -tms_dns=[node-name]      DNS node name of task manager.                     \n"
	    "          -partition=[string]       Partition name.                                    \n"
	    "          -runinfo=[string]         Path to file with python formatted.                \n"
	    "                                    information from RunInfo datapoint.                \n"
	    "          -taskconfig=[string]      Path to xml file with slave information.           \n"
	    "          -service=[name]           DIM service name to steer number lof threads.       \n"
	    "          -count=[number]           Number of Moore processes to be forked.            \n"
	    "          -replacements=[string]    String with replacement items to interprete        \n"
	    "                                    the architecture. Items are name-value pairs       \n"
	    "                                    separated by a smi-colon:                          \n"
	    "                                    Name1:<value1>,Name2:<value2>,....                 \n"
	    "          -bindcpus=[number]        Flag to bind main processors to CPU slots.         \n"
	    "                                                                                       \n"
	    "          -smidomain=<name>         SMI domain name to join.                           \n"
	    "                                    WINCCOA slice where the manager is located.        \n"
	    "          -smifile=[path]           SMI file to load if (standalone != 1)              \n"
	    "          -smidebug=[number]        SMI debug level default: 1.                        \n"
	    "          -standalone=[number]      Flag to run in standalone mode without WinCC.      \n"
	    "          -smilog=[path]            SMI log file if out of process (standalone != 1)   \n"
	    "          -smiscript=[path]         Run SMI from script (requires -standalone=3)       \n"
	    "                                                                                       \n"
	    "          -print=[number]           Print level (0 for debug printouts).               \n"
	    "          -loggertype=[name]        Output logger type.                                \n"
	    "          -sleep=[number]           Sleep a number of seconds at startup               \n"
	    );
  ::exit(EINVAL);
}

static void invalid_arg(const char* fmt, ...) {
  va_list args;
  va_start(args, fmt);
  ::vfprintf(stderr,fmt,args);
  va_end (args);
  help_ctrl();
}

static int exit_handler(void* param)  {
  if ( param )  {
#if 0
    char text[512];
    ::snprintf(text,sizeof(text),"[INFO] Exithandler called by %s", (char*)(param ? param : ""));
    ::write(STDOUT_FILENO,text,sizeof(text));
#endif
  }
  return 1;
}


/// Test routine

extern "C" int smi_controller(int argc, char** argv)    {
  static      SmiController::config_t config;
  auto        logger  = std::make_shared<RTL::Logger::LogDevice>();
  RTL::CLI    cli(argc, argv, help_ctrl);
  std::string logger_type = "term";
  std::string replacements;
  char        log_path[PATH_MAX];
  int         secs_sleep=0;
  bool        forking = false;
  bool        dbg = false;

  if ( ::lib_rtl_getenv("LOGFIFO") )   {
    struct stat stat;
    std::string path = ::lib_rtl_getenv("LOGFIFO");
    if ( 0 == ::stat(path.c_str(), &stat) )   {
      if ( S_ISFIFO(stat.st_mode) )   {
	config.stderr_file = path;
	config.stdout_file = path;
      }
    }
  }

  if ( config.stderr_file.empty() || config.stdout_file.empty() )   {
    ssize_t  len = ::readlink("/proc/self/fd/1", log_path, sizeof(log_path));
    if ( -1 == len )   {
      ::fprintf(stderr, "Failed to determine stdout file name. [%s]\n",
		std::error_condition(errno,std::system_category()).message().c_str());
      return EINVAL;
    }
    log_path[len] = 0;
    config.stdout_file = log_path;

    len = ::readlink("/proc/self/fd/2", log_path, sizeof(log_path));
    if ( -1 == len )   {
      ::fprintf(stderr, "Failed to determine stderr file name. [%s]\n",
		std::error_condition(errno,std::system_category()).message().c_str());
      return EINVAL;
    }
    log_path[len] = 0;
    config.stderr_file = log_path;
  }

  config.name = RTL::processName();
  config.dns  = ::lib_rtl_getenv("DIM_DNS_NODE") ? ::lib_rtl_getenv("DIM_DNS_NODE") : "";
  forking = cli.getopt("forking", 4) != nullptr;
  dbg     = cli.getopt("debug",   3) != nullptr;
  cli.getopt("partition",    2, config.partition);
  cli.getopt("runinfo",      2, config.runinfo);
  cli.getopt("taskconfig",   2, config.architecture);
  cli.getopt("service",      2, config.num_thread_svc);
  cli.getopt("dns",          2, config.dns);
  cli.getopt("tmsdns",       2, config.tms_dns);

  cli.getopt("smidomain",    5, config.smi_domain);
  cli.getopt("smilog",       5, config.smi_log_file);
  cli.getopt("smifile",      5, config.smi_file);
  cli.getopt("smidns",       5, config.smi_dns);
  cli.getopt("smidebug",     5, config.smi_debug);
  cli.getopt("smiscript",    5, config.smi_script);
  cli.getopt("replacements", 3, replacements);
  cli.getopt("count",        2, config.num_workers);
  cli.getopt("bindcpus",     2, config.bind_cpus);
  cli.getopt("standalone",   2, config.standalone);

  cli.getopt("loggertype",   2, logger_type);
  cli.getopt("print",        2, config.output_level);
  cli.getopt("sleep",        2, secs_sleep);

  if ( secs_sleep > 0 ) {
    for(secs_sleep *= 1000; secs_sleep >= 0; secs_sleep -= 100)
      ::lib_rtl_sleep(100);
  }
  logger_type = RTL::str_lower(logger_type);
  if ( RTL::str_lower(logger_type).substr(0,3) == "dim" )   {
    logger.reset(new RTL::Logger::LogDevice());
    logger->compileFormat("%-8LEVEL %-24SOURCE");
  }
  else if ( RTL::str_lower(logger_type).substr(0,4) == "fifo" )   {
    logger.reset(new RTL::Logger::LogDevice());
    logger->compileFormat("%-8LEVEL %-24SOURCE");
  }
  else if ( RTL::str_lower(logger_type.substr(0,3)) == "fmc" ) {
    /// Need to install a proper printer, which directy writes in the fifo
    logger.reset(new RTL::FmcLogDevice(::lib_rtl_getenv("LOGFIFO")));
    logger->compileFormat("%TIME %LEVEL%-8NODE: %-32PROCESS %-20SOURCE");
  }
  else  {
    logger->compileFormat("%TIME %LEVEL%-8NODE: %-32PROCESS %-20SOURCE");
  }
  RTL::Logger::set_io_buffering(RTL::Logger::LINE_BUFFERING);
  RTL::Logger::LogDevice::setGlobalDevice(logger, config.output_level);
  RTL::Logger log(RTL::Logger::LogDevice::getGlobalDevice(),"Controller", config.output_level);
  if ( dbg )   {
    ::lib_rtl_wait_debugger(true);
  }

  TaskManager::set_type_default(forking ? "FORKING" : "FMC");
  config.dns     = RTL::str_upper(config.dns);
  config.tms_dns = RTL::str_upper(config.tms_dns);
  config.smi_dns = RTL::str_upper(config.smi_dns);
  if ( !replacements.empty() )   {
    auto items = RTL::str_split(replacements,',');
    for( const auto& it : items )   {
      const auto& nv = RTL::str_split(it,':');
      config.replacements.emplace(nv[0], nv[1]);
    }
  }
  /// For backwards compatibility: add the instance argument:
  if ( config.replacements.find("NUMBER_OF_INSTANCES") == config.replacements.end() )  {
    char text[64];
    ::snprintf(text,sizeof(text),"%d",config.num_workers);
    config.replacements.emplace("NUMBER_OF_INSTANCES", text);
  }
  if     ( config.name.substr(0,3)=="P00") invalid_arg("Invalid UTGID environment='%s'\n",    config.name.c_str());
  else if( config.partition.empty()      ) invalid_arg("Invalid argument -partition='%s'\n",  config.partition.c_str());
  else if( config.architecture.empty()   ) invalid_arg("Invalid argument -taskcondig='%s'\n", config.architecture.c_str());
  else if( config.runinfo.empty()        ) invalid_arg("Invalid argument -runinfo='%s'\n",    config.runinfo.c_str());
  else if( config.dns.empty()            ) invalid_arg("Invalid argument -dns='%s'\n",        config.dns.c_str());
  else if( config.tms_dns.empty()        ) invalid_arg("Invalid argument -tms_dns='%s'\n",    config.tms_dns.c_str());
  else if( config.smi_dns.empty()        ) invalid_arg("Invalid argument -smi_dns='%s'\n",    config.smi_dns.c_str());
  else if( config.smi_domain.empty()     ) invalid_arg("Invalid argument -smi_domain='%s'\n", config.smi_domain.c_str());
  else if( config.num_workers < 0        ) invalid_arg("Invalid argument -count='%d'\n",      config.num_workers);
  else if( config.standalone == 3 && config.smi_script.empty() )   {
    invalid_arg("Invalid argument -standalone='3' in conjunction with empty smiscript parameter!\n");
  }
  ::lib_rtl_signal_log(1);
  ::lib_rtl_declare_exit(exit_handler, ::strdup(config.name.c_str()));

  log.info("Task partition:             %s", config.partition.c_str());
  log.info("Task runinfo:               %s", config.runinfo.c_str());
  log.info("Task architecture:          %s", config.architecture.c_str());
  log.info("Task dns:                   %s tms dns: %s smi dns: %s",
	   config.dns.c_str(), config.tms_dns.c_str(), config.smi_dns.c_str());
  log.info("Task started... tag service:%s Standalone mode: %s", 
	   config.num_thread_svc.c_str(), config.standalone ? "ON" : "OFF");
  log.info("SMI: domain                 %s %s %s",
	   config.smi_domain.c_str(), config.smi_file.empty() ? "" : "file:", config.smi_file.c_str());

  ::dis_set_dns_node(config.dns.c_str());
  ::dic_set_dns_node(config.dns.c_str());
  ::dim_init();

  SmiController ctrl(config);
  // ctrl.start_smi();
  ctrl.configure();
  while(1)   {
    try {
      ctrl.run();
    }
    catch(const std::exception& e)   {
      log.warning("Controller EXCEPTION:%s. Blank terminate avoided.",e.what());
    }
    catch(...)   {
      log.warning("Controller UNKNOWN EXCEPTION. Blank terminate avoided.");
    }
  }
  return 1;
}
