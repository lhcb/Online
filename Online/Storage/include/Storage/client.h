//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_STORAGE_CLIENT_H
#define ONLINE_STORAGE_CLIENT_H

/// Framework include files
#include <Storage/communication.h>

/// C/C++ include files
#include <vector>
#include <memory>

/// Online Namespace
namespace Online   {

  /// Namespace for the storage interface
  namespace storage   {

    ///  RPC Client interface class
    /**
     *
     *  \author  M.Frank
     *  \version 1.0
     *  \date    02.05.2017
     */
    class client   {
    public:
      /// Definition of the header collection
      typedef std::vector<header_t> reqheaders_t;
      class sync  {};
      class async {};

      std::string  host;
      std::string  port = "8000";
      reqheaders_t user_headers {};

      int          timeout = 10000;
      int          debug = 0;

    public:
      /// Static constructor for concrete implementations
      template <typename T>
      static std::unique_ptr<client> create(const std::string& host, const std::string& port, int tmo, int dbg);

    public:
      /// Initializing constructor
      client(const std::string& host, const std::string& port, int tmo, int dbg);
      /// Default constructor
      client() = delete;
      /// NO Move constructor
      client(client&& copy) = delete;
      /// NO Copy constructor
      client(const client& copy) = delete;
      /// NO move operator
      client& operator=(client&& copy) = delete;
      /// NO assignment operator
      client& operator=(const client& copy) = delete;
      /// Default destructor
      virtual ~client() = default;

      /// Access name
      virtual std::string name()  const;
      /// Build generic HTTP request
      virtual std::string build_request(const std::string&           func,
					const std::string&           url,
					const std::vector<header_t>& headers) const;

      /// Open connection to server
      virtual std::error_code open() const = 0;
      /// Exec generic statement
      virtual reply_t request(const std::string& cmd, const std::string& url, const reqheaders_t& headers=reqheaders_t()) const = 0;
      /// Connect client to given URI and execute an HTTP GET
      virtual reply_t get(const std::string& url, const reqheaders_t& headers=reqheaders_t()) const = 0;
      /// Connect client to given URI and execute DELETE statement
      virtual reply_t del(const std::string& url, const reqheaders_t& headers=reqheaders_t()) const = 0;
      /// Connect client to given URI and execute RPC call
      virtual reply_t put(const std::string& url, const void* data, size_t len, const reqheaders_t& headers=reqheaders_t()) const = 0;
      /// Connect client to given URI and execute UPDATE statement
      virtual reply_t update(const std::string& url, const void* data, size_t len, const reqheaders_t& headers=reqheaders_t())  const = 0;
    };

    ///  XMLRPC Client base class for dataflow queries
    /**
     *
     *  \author  M.Frank
     *  \version 1.0
     *  \date    02.05.2017
     */
    class client_handle  {
    public:
      typedef client* handler_t;
      handler_t handler;

    public:
      /// NO Default constructor
      client_handle() = default;
      /// Initializing constructor
      client_handle(handler_t cl);
      /// Move constructor
      client_handle(client_handle&& copy) = default;
      /// Copy constructor
      client_handle(const client_handle& copy) = default;
      /// Assignment move operator
      client_handle& operator=(client_handle&& copy) = default;
      /// Assignment copy operator
      client_handle& operator=(const client_handle& copy) = default;
      /// Default destructor
      virtual ~client_handle() = default;
    };

  }     // End namespace storage
}       // End namespace Online
#endif  // RPC_RPC_CLIENT_H

