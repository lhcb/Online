//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : Storage
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_STORAGE_FDB_DBASE_H
#define ONLINE_STORAGE_FDB_DBASE_H

/// C/C++ include files
#include <mutex>
#include <string>
#include <memory>
#include <vector>
#include <system_error>

/// Online Namespace
namespace Online   {

  /// Namespace for the storage interface
  namespace storage   {

    /// Forward declarations
    class client;
    
    /// Wrapper around file database instance
    /**
     *
     *  \author  M.Frank
     *  \version 1.0
     *  \date    02.02.2021
     */
    class fdb_dbase_t   {
    public:
      /// Static constructor for concrete implementations
      template <typename T, typename... Args>
	static std::unique_ptr<fdb_dbase_t> create(Args... args);

    public:

      /// Data file properties
      /**
       *
       *  \author  M.Frank
       *  \version 1.0
       *  \date    02.02.2021
       */
      struct file_t {
	std::string name, /* state, size, */ date, host;
	int64_t state, size;
      };
      
      /// Interface class to database handler
      /**
       *  To implement a new database technology in principle only
       *  this class needs to be re-implemented e.g. using MYSQL.
       *
       *  \author  M.Frank
       *  \version 1.0
       *  \date    02.02.2021
       */
      class handler_t   {
      public:
	typedef fdb_dbase_t::file_t file_t;
	static constexpr int STATE_OPEN    = 0;
	static constexpr int STATE_WRITTEN = 1;
	static constexpr int STATE_READ    = 2;

	std::string       _type { };
	int               _debug { 0 };
	bool              _inited = false;

	/// Check the existence of a given object in the database
	virtual std::error_code
	  query_file(const std::string& object_name, file_t& file, int state) = 0;
	
      public:

	/// Initializing constructor
	handler_t(const std::string& t, int dbg);

	/// Default destructor
	virtual ~handler_t();

	/// Check the existence of a given object in the database
	virtual std::error_code query(std::string&       object,
				      std::string&       host,
				      std::string&       date,
				      std::size_t&       length);
	
	/// Check the existence of a given object in the database
	virtual std::error_code next( std::string&       object,
				      std::string&       host,
				      std::string&       date,
				      std::size_t&       length);
	
	/// Add a new object to the database
	virtual std::error_code add(  const std::string& object,
				      const std::string& date,
				      std::size_t        length,
				      const std::string& host) = 0;
	
	/// Remove an object from the database
	virtual std::error_code del(  const std::string& object) = 0;

	/// lock database record
	virtual std::error_code set(  const std::string& object, int value) = 0;
      };
      /// File database lock
      /**
       *  \author  M.Frank
       *  \version 1.0
       *  \date    02.02.2021
       */
      class lock_t  final  {
	/// Reference to locked object
	fdb_dbase_t* fdb;

      public:
	/// Standard constructor
      lock_t(fdb_dbase_t* db) : fdb(db)  { fdb->lock();    }
	/// Standard destructor
	~lock_t()                          { fdb->unlock();  }
      };

      std::mutex                 _lock;
      std::string                _server;
      std::string                _location_prefix;
      std::unique_ptr<handler_t> _engine { };
      int                        _debug  { 0 };

    public:
      /// Standard constructor
      fdb_dbase_t(const std::string& server, const std::string& location_prefix);

      /// Standard destructor
      virtual ~fdb_dbase_t();

      /// Take global lock of the database obect
      virtual void lock()      {   _lock.lock();     }
      /// Release global lock of the database obect
      virtual void unlock()    {   _lock.unlock();   }

      /// Access the node specification of the next free node
      virtual std::string get_free_host_uri(const std::string& )  const;

      /// Transform the object name to the object key for the database lookup
      virtual std::string object_key(const std::string& obj)  const;

      /// Transform the object key and the host specs to the network name of the file
      virtual std::string network_file(const std::string& host, const std::string& obj)  const;
      
      /// Check the existence of a given object in the database
      virtual std::error_code query_object( const std::string& object_name,
					    std::string&       access_name,
					    std::string&       date,
					    std::size_t&       length,
					    bool               direct);

      /// Get the next object with the name matching the prefix
      virtual std::error_code delete_next(  const std::string& prefix,
					    std::string&       access_name,
					    std::string&       date,
					    std::size_t&       length,
					    bool               direct);

      /// Check the existence of a given object in the database
      virtual std::error_code delete_object(const std::string& object_name,
					    std::string&       access_name,
					    std::string&       date,
					    std::size_t&       length,
					    bool               direct);

      /// Add a new object to the database
      virtual std::error_code add_object(   const std::string& object_name,
					    const std::string& date,
					    std::size_t        length,
					    std::string&       access_name,
					    bool               direct);

      /// Update the object state of an object in the database
      virtual std::error_code update_object_state(const std::string& object_name,
						  const std::string& state);
    };


    /// Monitor object to publish values
    /**
     *
     *  \author  M.Frank
     *  \version 1.0
     *  \date    02.02.2021
     */
    class fdb_dbase_monitor_t   {
    public:
      struct Data  {
	int num_get_success       {0};
	int num_get_errors        {0};
	int num_get_not_found     {0};
	int num_get_unauthorized  {0};
	
	int num_del_success       {0};
	int num_del_errors        {0};
	int num_del_not_found     {0};
	int num_del_unauthorized  {0};

	int num_put_success       {0};
	int num_put_errors        {0};
	int num_put_bad_request   {0};
	int num_put_unauthorized  {0};	

	int num_upda_success      {0};
	int num_upda_errors       {0};
	int num_upda_bad_request  {0};
      } data;

      std::vector<int> services;

      void add(int svc)  {  services.emplace_back(svc); }

    public:
      /// Standard constructor
      fdb_dbase_monitor_t();
      /// Default destructor
      ~fdb_dbase_monitor_t();
    };

  }     // End namespace storage
}       // End namespace Online
#endif  // ONLINE_STORAGE_FDB_DBASE_H
