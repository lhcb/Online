//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : Storage
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_STORAGE_FDB_SERVER_H
#define ONLINE_STORAGE_FDB_SERVER_H

/// Framework include files
#include <RTL/rtl.h>
#include <CPP/Interactor.h>
#include <HTTP/HttpServer.h>
#include <Storage/communication.h>

/// C/C++ include files
#include <string>
#include <filesystem>

namespace http   {
  
  template <typename T> class basic_http_server;

  /// file database interface implementation
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.02.2021
   */
  template <typename T> 
  class basic_http_server<T>::handler_t : public HttpRequestHandler  {

  public:
    typedef typename T::traits         traits;
    typedef typename traits::dbase_t   dbase_t;
    typedef typename traits::monitor_t monitor_t;

  public:
    /// Reference to the database instance
    std::unique_ptr<dbase_t>   dbase     { };
    /// Reference to the monitoring instance
    std::unique_ptr<monitor_t> monitor   { };
    /// Type name to improve printouts
    std::string                type      { "FDB" };
    /// Flag if operation is enabled / disabled
    bool                       enabled   { true };
    
    
  public:

    /// Standard constructor
    explicit handler_t();
    /// Standard destructor
    virtual ~handler_t();

    /// Specific handler for GET requests
    virtual continue_action handle_get(const request_t& req, reply_t& rep) override  {
      return this->HttpRequestHandler::handle_get(req, rep);
    }
    /// Specific handler for PUT requests
    virtual continue_action handle_put(const request_t& req, reply_t& rep) override  {
      return this->HttpRequestHandler::handle_put(req, rep);
    }
    /// Specific handler for POST requests
    virtual continue_action handle_post(const request_t& req, reply_t& rep) override  {
      return this->HttpRequestHandler::handle_post(req, rep);
    }
    /// Specific handler for POST requests
    virtual continue_action handle_update(const request_t& req, reply_t& rep) override  {
      return this->HttpRequestHandler::handle_post(req, rep);
    }
    /// Specific handler for DELETE requests
    virtual continue_action handle_delete(const request_t& req, reply_t& rep) override  {
      return this->HttpRequestHandler::handle_delete(req, rep);
    }
    /// Handle a request and produce a reply.
    virtual continue_action handle_request(const request_t& req, reply_t& rep) override  {
      return this->HttpRequestHandler::handle_request(req, rep);
    }
    /// Callback to eventually handle actions on connection shutdown
    virtual void            handle_shutdown(const HttpRequest& req, const HttpReply& rep)  override  {
      this->HttpRequestHandler::handle_shutdown(req, rep);
    }
    /// Handle possible statistics summaries.
    virtual void handle_stat(const request_t& req, reply_t& rep)  override  {
      this->HttpRequestHandler::handle_stat(req, rep);
    }
    /// Callback on "GET" when handling control sequences like shutdown
    virtual bool handle_control(const request_t& req, reply_t& rep)    {
      auto prefix = req.uri.substr(0, 8);
      if ( prefix == "/control" )   {
	if ( rep.content.empty() )   {
	  rep = reply_t::stock_reply(reply_t::ok);
	  if ( req.uri == "/control=exit" || req.uri == "/control=shutdown" )    {
	    rep.content = http::to_vector("SHUTDOWN request of \""+this->type+"\" handled successfully.\n\n");
	    ::lib_rtl_output(LIB_RTL_ALWAYS,"+++ %s: shutdown.....\n", this->type.c_str());
	  }
	  else if ( req.uri == "/control=enable" )    {
	    rep.content = http::to_vector("ENABLE request of \""+this->type+"\" handled successfully.\n\n");
	    ::lib_rtl_output(LIB_RTL_ALWAYS,"+++ %s enabled.", this->type.c_str());
	    this->enabled = true;
	  }
	  else if ( req.uri == "/control=disable" )    {
	    rep.content = http::to_vector("DISABLE request of \""+this->type+"\" handled successfully.\n\n");
	    ::lib_rtl_output(LIB_RTL_ALWAYS,"+++ %s disabled.", this->type.c_str());
	    this->enabled = false;
	  }
	  ::fflush(stdout);
	  rep.headers.resize(3);
	  rep.headers[0].name  = "Content-Length";
	  rep.headers[0].value = std::to_string(rep.content.size());
	  rep.headers[1].name  = "Content-Type";
	  rep.headers[1].value = "text/plain";
	  rep.headers[2].name  = "Connection";
	  rep.headers[2].value = "close";
	}
	return true;
      }
      return false;
    }
  };
}

/// Framework include files
#include <RTL/rtl.h>
#include <RTL/Logger.h>

/// Online Namespace
namespace Online   {

  /// Namespace for the storage interface
  namespace storage   {

    struct fs  {      class traits;    };
    struct db  {      class traits;    };
    using http::basic_http_server;

    /// Create and configure a server instance
    /**
     *
     *  \author  M.Frank
     *  \version 1.0
     *  \date    02.02.2021
     */
    template <typename T> class SrvRun : public CPP::Interactor {
    public:
      std::unique_ptr<http::basic_http_server<T> > ptr  { };
      std::string local             { };
      std::string server            { };
      std::string file_dir          { };
      std::string dbase             { };
      std::string prefix            { "/objects" };
      size_t      buffer_size       { 16*1024 };
      int         num_threads       { 0 };
      int         application_debug { 0 };
      int         connection_debug  { 0 };
      bool        startup           { false };

      class http_signal_handler  {
      public:
	typename basic_http_server<T>::handler_t* handler  { nullptr };
	bool executed { false };
	void handle(const boost::system::error_code&, // Result of operation.
			int signum)  {                    // Indicates which signal occurred.
	  std::string type = handler ? handler->type : std::string("????");
	  ::lib_rtl_output(LIB_RTL_ALWAYS,
			   "+++ %s: Termination request received. Handling signal %s.",
			   type.c_str(), RTL::signalName(signum).c_str());
	  this->executed = true;
	  ::fflush(stdout);
	  ::lib_rtl_sleep(100);
	}
      };
      http_signal_handler sig_handler { nullptr };
      
    public:
      /// ======================================================================================
      using CPP::Interactor::Interactor;

      /// ======================================================================================
      void create(int argc, char* argv[])   {
	std::string localhost = RTL::nodeName();
	int         prt = LIB_RTL_INFO;

	local  = localhost+":80";

	RTL::CLI cli(argc, argv, [](int, char**) {});
	startup = cli.getopt("start",  5) != 0;
	cli.getopt("connection_debug", 16, connection_debug);
	cli.getopt("application_debug",17, application_debug);
	cli.getopt("database",          4, dbase);
	cli.getopt("server",            4, server);
	cli.getopt("local",             4, local);
	cli.getopt("threads",           7, num_threads);
	cli.getopt("files",             5, file_dir);
	cli.getopt("prefix",            5, prefix);
	cli.getopt("print",             4, prt);
	cli.getopt("buffersize",        6, buffer_size);

	RTL::Logger::install_log(RTL::Logger::log_args(prt));
	if ( !file_dir.empty() )  {
	  std::error_code ec;
	  std::filesystem::path files(file_dir);
	  std::filesystem::path abs_path = std::filesystem::absolute(files, ec);
	  if ( ec )  {
	    ::lib_rtl_output(LIB_RTL_ERROR,
			     "+++ Failed to resolve directory: '%s' [%s]",
			     files.c_str(), ec.message().c_str());
	    ::fflush(stdout);
	    ::exit(EINVAL);
	  }
	  if ( !std::filesystem::exists( abs_path, ec ) )   {
	    std::filesystem::create_directories( abs_path, ec );
	    if ( ec )    {
	      ::lib_rtl_output(LIB_RTL_ERROR,
			       "+++ Failed to create working directory: '%s' [%s]",
			       abs_path.c_str(), ec.message().c_str());
	      ::fflush(stdout);
	      ::exit(EINVAL);
	    }
	  }
	  std::filesystem::current_path( abs_path, ec );
	  if ( ec )    {
	    ::lib_rtl_output(LIB_RTL_ERROR,
			     "+++ Failed to change to working directory: '%s' [%s]",
			     abs_path.c_str(), ec.message().c_str());
	    ::fflush(stdout);
	    ::exit(EINVAL);
	  }
	}

	uri_t uri(local);
	ptr = std::make_unique<http::basic_http_server<T> >(uri.host, uri.port);
	std::stringstream str;
	str << ptr->handler().type << ":";
	if ( !file_dir.empty() ) str << " File repository: "   << file_dir;
	if ( !server.empty()   ) str << " Server URI: http://" << server;
	if ( !local.empty()    ) str << " Local URI: http://"  << local;
	::lib_rtl_output(LIB_RTL_ALWAYS, "+++ %s", str.str().c_str());

	ptr->handler().buffer_size = buffer_size;
	ptr->set_debug(connection_debug);
	::lib_rtl_output(LIB_RTL_ALWAYS, "+++ %s: Starting <%s> on http://%s:%s with %d threads.",
			 ptr->handler().type.c_str(),
			 typeid(T).name(),
			 uri.host.c_str(),
			 uri.port.c_str(),
			 num_threads);
      }
      /// ======================================================================================
      void start()   {
	std::string typ;
	try   {
	  auto& imp = ptr->handler();
	  typ = imp.type;
	  this->sig_handler.handler = &imp;
	  imp.signals.async_wait(std::bind(&http_signal_handler::handle, &this->sig_handler,
					   std::placeholders::_1, std::placeholders::_2));
	  ptr->start(false, num_threads);
	  ::lib_rtl_output(LIB_RTL_ALWAYS,"+++ %s: finished...\n", typ.c_str());
	  while ( !this->sig_handler.executed )  { ::lib_rtl_sleep(1); }
	  ptr.reset();
	  ::fflush(stdout);
	}
	catch(const std::exception& e)   {
	  ::lib_rtl_output(LIB_RTL_ALWAYS, "%s: +++ %s", typ.c_str(), e.what());
	}
	catch(...)   {
	  ::lib_rtl_output(LIB_RTL_ALWAYS, "%s: +++ Unknown exception.", typ.c_str());
	}
	::fflush(stdout);
      }
    };
  }     // End namespace storage
}       // End namespace Online
#endif  // ONLINE_STORAGE_FDB_SERVER_H
