//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================

// Framework inclde files
#include "Setup.h"
#include <RTL/Logger.h>
#include <Storage/communication.h>

// C/C++ include files
#include <fstream>
#include <iostream>
#include <filesystem>
#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>

using Online::fdb_test::Setup;

Setup::Setup(const std::string& n, int argc, char* argv[], void (*help)(int,char**))
  : name(n)
{
  void (*hlp)(int,char**) = [](int, char**) {};
  RTL::CLI cli(argc, argv, help ? help : hlp);

  process = RTL::processName();
  server  = RTL::nodeName()+":80";
  cli.getopt("check",      4, check_file);
  cli.getopt("server",     4, server);
  cli.getopt("url",        3, url);
  cli.getopt("run",        3, run);
  cli.getopt("file",       4, file);
  cli.getopt("print",      4, prt);
  cli.getopt("debug",      4, debug);
  cli.getopt("length",     4, length);
  cli.getopt("count",      4, count);
  cli.getopt("version",    4, version);
  RTL::Logger::install_log(RTL::Logger::log_args(prt));
  ::lib_rtl_output(LIB_RTL_DEBUG,"%s: Test starting....", name.c_str());
}

Setup::~Setup()   {
  ::lib_rtl_output(LIB_RTL_DEBUG,"%s: Test finished!", name.c_str());
}

Online::storage::fdb_client Setup::client()    {
  Online::storage::fdb_client client(this->version);
  Online::storage::uri_t theurl(this->server);
  client.fdbclient = Online::storage::client::create<Online::storage::client::sync>(theurl.host, theurl.port, 10000, debug);
  return client;
}

Setup& Setup::print(reply_t& reply)    {
  reply.print_header(std::cout, this->name+": ");
  if ( reply.content.size() < 1024 )   {
    reply.content.push_back(0);
    reply.print_content(std::cout, this->name+": ");
  }
  return *this;
}

Setup& Setup::check(const reply_t& reply)    {
  if ( !this->check_file.empty() )   {
    check_reply(this->check_file, reply);
  }
  return *this;
}
    
std::vector<unsigned char> Setup::read_file(const std::string& fname)   {
  std::error_code ec;
  if ( !std::filesystem::exists(fname, ec) )  {
    ::lib_rtl_output(LIB_RTL_ERROR, "%s: Cannot access file: %s [%s]",
		     name.c_str(), fname.c_str(), ec.message().c_str());
    ::exit(ec.value());
  }
  std::vector<unsigned char> data;
  std::size_t num_bytes = 0, num_total = std::filesystem::file_size(fname);
  data.resize(num_total);
  std::ifstream in(fname.c_str(), std::ifstream::in);
  while( in.good() && num_bytes < num_total )  {
    int len = in.readsome((char*)&data.at(0)+num_bytes, num_total-num_bytes);
    if ( len > 0 ) num_bytes += len;
  }
  if ( num_bytes != num_total )   {
    ::lib_rtl_output(LIB_RTL_ALWAYS,
		     "%s: Failed to read file: %s Got %ld out of %ld bytes",
		     name.c_str(), fname.c_str(), num_bytes, num_total);
    ::exit(EINVAL);
  }
  return data;
}
  
std::size_t Setup::write_file(const reply_t& reply)    {
  std::ofstream out(file, std::ofstream::out|std::ofstream::trunc|std::ofstream::binary);
  if ( !out.is_open() )   {
    ::lib_rtl_output(LIB_RTL_INFO,
		     "%s: CANNOT open output file %s  [%s]",
		     name.c_str(), file.c_str(), RTL::get_error(errno).message().c_str());
    return -1;
  }
  out.write((const char*)&reply.content[0], long(reply.content.size()));
  if ( !out.good() )   {
    ::lib_rtl_output(LIB_RTL_INFO,
		     "%s: FAILED to download %ld bytes from %s to file:%s",
		     name.c_str(), reply.content.size(), url.c_str(), file.c_str());
    return 0;
  }
  else  {
    ::lib_rtl_output(LIB_RTL_INFO,
		     "%s: Successfully downloaded %ld bytes from %s to file:%s",
		     name.c_str(), reply.content.size(), url.c_str(), file.c_str());
  }
  out.close();
  return reply.content.size();
}

long Setup::check_reply(const std::string& thefile, const reply_t& reply)   {
  long ret = 0;
  if ( !this->check_file.empty() )   {
    std::size_t num_byte = 0;
    const unsigned char* p = &reply.content.at(0), *q = p + reply.content.size();
    std::ifstream in(this->check_file.c_str(), std::ifstream::in);
    while( in.good() && p < q )  {
      int c = in.get();
      if ( c != *p )   {
	++ret;
	if ( ret == 0 )   {
	  ::lib_rtl_output(LIB_RTL_ALWAYS,
			   "%s: Buffer diffs at byte %ld got: %02X expected: %02X",
			   name.c_str(), long(q-p), *p, c);
	}
      }
      ++p;
      ++num_byte;
    }
    ::lib_rtl_output(LIB_RTL_ALWAYS,
		     "%s: Checked %ld / %ld data against file: %s  %ld errors",
		     name.c_str(), num_byte, reply.bytes_total, thefile.c_str(), ret);
  }
  return ret;
}

/// Generate buffer with pre-set data
Setup::buffer_t Setup::generate(std::size_t len, long offset)  const   {
  buffer_t buffer;
  buffer.first = len;
  buffer.second.allocate(len, true);
  for(long i=0, n=len/sizeof(std::size_t); i<n; ++i)  {
    long data = i + offset;
    buffer.second.append(&data, sizeof(data));
  }
  return buffer;
}

