//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================
#ifndef STORAGE_FDB_TEST_SETUP_H
#define STORAGE_FDB_TEST_SETUP_H 1

// Framework inclde files
#include <Storage/fdb_client.h>
#include <CPP/mem_buff.h>
#include <RTL/rtl.h>

namespace Online  {

  namespace fdb_test  {

    class Setup   {
    public:
      typedef storage::reply_t     reply_t;
      typedef storage::fdb_client  fdb_client;
      typedef std::pair<std::size_t, mem_buff> buffer_t;
      
      std::string server, log, url, file, name, check_file, run;
      std::string process;
      std::size_t length  { 0 };
      std::size_t count   { 0 };
      int         prt     { LIB_RTL_INFO };
      int         version { 0 };
      int         debug   { 0 };

    public:
      Setup(const std::string& n, int argc, char* argv[], void (*help)(int,char**)=nullptr);
      ~Setup();
      fdb_client client();
      Setup& print(reply_t& reply);
      Setup& check(const reply_t& reply);
      std::vector<unsigned char> read_file(const std::string& fname);
      std::size_t write_file(const reply_t& reply);
      long check_reply(const std::string& file, const reply_t& reply);
      /// Generate buffer with pre-set data
      buffer_t generate(std::size_t len, long offset)  const;
    };
  }
}
#endif // STORAGE_FDB_TEST_SETUP_H
