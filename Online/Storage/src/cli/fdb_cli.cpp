//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================
/*

gentest libStorageCli.so  fdb_test_client_get  -server=${HOST}:8000 -url=/objects/data/072908_0000000072.raw

gentest libStorageCli.so  fdb_cli_save         -server=${HOST}:8000 -url=/objects/data/072908_0000000072.raw
gentest libStorageCli.so  fdb_cli_put          -server=${HOST}:8000 -url=/objects/data/072908_0000000072.raw

gentest libStorageCli.so  fdb_cli_get          -server=${HOST}:8000 -url=/objects/data/072908_0000000072.raw

gentest libStorageCli.so  fdb_test_delete      -server=${HOST}:8000 -url=/objects/data/072908_0000000072.raw -file=/home/frankm/data/072908_0000000072.raw

gentest libStorageCli.so  fdb_test_put         -server=${HOST}:8000 -url=/objects/data/072908_0099999999.raw -file=/home/frankm/data/072908_0000000072.raw

gentest libStorageCli.so  fdb_test_client_save -server=${HOST}:8000 -url=/objects/data/072908_0099999999.raw -file=/home/frankm/data/072908_0000000072.raw

gentest libStorageCli.so  fdb_test_save        -server=${HOST}:8000 -url=/objects/data/072908_0099999999.raw -file=/home/frankm/data/072908_0000000072.raw

gentest libStorageCli.so  fdb_cli_db_delete     -server=${HOST}:8000 -url=/objects/data/testing/0000000000/2021-03-10__14-34/0000000000_mon0101_14-34-53_21842.raw


*/
// Framework inclde files
#include "Setup.h"

// C/C++ include files
#include <iostream>
#include <filesystem>

using namespace std;
using namespace Online::storage;
using Online::fdb_test::Setup;
//
//  ------------------------ CLI -----------------------
//
//==========================================================================
extern "C" int fdb_cli_get(int argc, char** argv)    {
  Setup  setup("fdb_test_next_get",argc, argv, [](int /* ac */, char** /* av */)  {
    ::fprintf(stderr,
	      "fdb_cli_get -opt [-opt]                                       \n"
	      "     -server=<name:port>    Server access specification.      \n"
	      "     -url=<url-spec>        URL of the file to resolve        \n"
	      "     -file=<file-path>      Override natural name when saving \n"
	      "                            [optional]                        \n"
	      "     -check=<file-path>     Optional check data against <file>\n"
	      "     -print=<level>         Set print level 1=DEBUG, 5=FATAL  \n"
	      "     -help                  Prnt this help.                   \n");
  });
  size_t length = 0;
  time_t date   = 0;
  if ( setup.file.empty() )    {
    setup.file = filesystem::path(setup.url).filename().string();
    ::lib_rtl_output(LIB_RTL_INFO, "%s: Will save data to %s",
		     setup.name.c_str(), setup.file.c_str());
  }
  auto client = setup.client();
  auto reply  = client.get_object(setup.url, date, length);
  if ( reply.status == reply.ok )   {
    setup.print(reply).check(reply);
    setup.write_file(reply);
    cout << "fdb_cli_get: " << setup.url << " finished successfully." << endl;
  }
  else   {
    setup.print(reply);
    cout << "fdb_cli_get: " << setup.url << " FAILED." << endl;
  }
  return 0;
}
//
//==========================================================================
extern "C" int fdb_cli_save(int argc, char** argv)    {
  Setup setup("fdb_cli_save", argc, argv, [](int /* ac */, char** /* av */)  {
    ::fprintf(stderr,
	      "fdb_cli_save -opt [-opt]                                      \n"
	      "     -server=<name:port>    Server access specification.      \n"
	      "     -url=<url-spec>        URL of the file to resolve        \n"
	      "     -print=<level>         Set print level 1=DEBUG, 5=FATAL  \n"
	      "     -help                  Prnt this help.                   \n");
  });
  fdb_client client = setup.client();
  vector<unsigned char> data = setup.read_file(setup.file);
  auto reply = client.save_object(setup.url, std::move(data));
  if ( reply.status == reply.created )    {
    cout << "fdb_cli_save: " << setup.file << " to "
	 << setup.url << " finished successfully." << endl;
  }
  else  {
    setup.print(reply);
    cout << "fdb_cli_save: [ERROR] " << setup.file << " to "
	 << setup.url << " FAILED." << endl;
  }
  return 0;
}
extern "C" int fdb_cli_put(int argc, char** argv)    {
  return fdb_cli_save(argc, argv);
}
//
//==========================================================================
extern "C" int fdb_cli_next_delete(int argc, char** argv)    {
  Setup  setup("fdb_cli_next_delete", argc, argv, [](int /* ac */, char** /* av */)  {
    ::fprintf(stderr,
	      "fdb_cli_next_delete -opt [-opt]                               \n"
	      "     -server=<name:port>    Server access specification.      \n"
	      "     -url=<url-spec>        URL of the file to resolve        \n"
	      "     -file=<file-path>      Override natural name when saving \n"
	      "     -check=<file-path>     Optional check data against <file>\n"
	      "     -print=<level>         Set print level 1=DEBUG, 5=FATAL  \n"
	      "     -help                  Prnt this help.                   \n");
  });
  if ( setup.file.empty() )    {
    ::lib_rtl_output(LIB_RTL_INFO,
		     "%s: CANNOT save data file. No filename given!",
		     setup.name.c_str());
    return EINVAL;
  }
  size_t length = 0;
  time_t date   = 0;  
  auto   client = setup.client();
  auto   reply  = client.next_object_delete(setup.url, date, length);
  if ( reply.status == reply.ok )   {
    setup.print(reply).check(reply);
    setup.write_file(reply);
    cout << "fdb_cli_next_delete: " << setup.url << " finished successfully." << endl;
  }
  else   {
    setup.print(reply);
    cout << "fdb_cli_next_delete: " << setup.url << " FAILED." << endl;
  }
  return 0;
}
//
//==========================================================================
extern "C" int fdb_cli_delete(int argc, char** argv)    {
  Setup  setup("fdb_cli_delete", argc, argv, [](int /* ac */, char** /* av */)  {
    ::fprintf(stderr,
	      "fdb_cli_delete -opt [-opt]                                    \n"
	      "     -server=<name:port>    Server access specification.      \n"
	      "     -url=<url-spec>        URL of the file to resolve        \n"
	      "     -file=<file-path>      Override natural name when saving \n"
	      "                            [optional]                        \n"
	      "     -check=<file-path>     Optional check data against <file>\n"
	      "     -print=<level>         Set print level 1=DEBUG, 5=FATAL  \n"
	      "     -help                  Prnt this help.                   \n");
  });
  auto   client = setup.client();
  size_t length = 0;
  time_t date   = 0;
  if ( setup.file.empty() )    {
    setup.file = filesystem::path(setup.url).filename().string();
    ::lib_rtl_output(LIB_RTL_INFO, "%s: Will save data to %s",
		     setup.name.c_str(), setup.file.c_str());
  }
  auto reply  = client.delete_object(setup.url, date, length);
  if ( reply.status == reply.ok )   {
    setup.print(reply).check(reply);
    setup.write_file(reply);
    cout << "fdb_cli_delete: " << setup.url << " finished successfully." << endl;
  }
  else   {
    setup.print(reply);
    cout << "fdb_cli_delete: " << setup.url << " FAILED." << endl;
  }
  return 0;
}
//
//==========================================================================
extern "C" int fdb_cli_db_delete(int argc, char** argv)    {
  Setup  setup("fdb_cli_db_delete",argc, argv, [](int,char**)  {
    ::fprintf(stderr,
	      "fdb_cli_db_delete -opt [-opt]                                 \n"
	      "   Remove entry from the database only.                     \n\n"
	      "     -server=<name:port>    Server access specification.      \n"
	      "     -url=<url-spec>        URL of the file to resolve        \n"
	      "     -print=<level>         Set print level 1=DEBUG, 5=FATAL  \n"
	      "     -help                      Print this help.              \n");
  });
  time_t date   = 0;
  size_t length = 0;
  auto   client = setup.client();
  auto reply    = client.db_object_delete(setup.url, date, length);
  if ( reply.status == reply_t::ok )   {
    ::lib_rtl_output(LIB_RTL_ALWAYS, "Successfully removed dbase entry: %s",
		     setup.url.c_str());
  }
  else   {
    ::lib_rtl_output(LIB_RTL_ALWAYS, "FAILED to remove dbase entry: %s",
		     setup.url.c_str());
    setup.print(reply);
  }
  return 0;
}
