//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================
/*
gentest libStorageCli.so fdb_test_parse_uri "-url=http://pluscc08.lbdaq.cern.ch:12345/path/to/file/data.xml?query=5&data"

*/
// Framework inclde files
#include "Setup.h"

// C/C++ include files
#include <iostream>
#include <filesystem>
#include <sys/types.h>
#include <sys/stat.h>
#include <chrono>

using namespace std;
using namespace Online::storage;
using namespace Online::fdb_test;
namespace chrono = std::chrono;
//
//==========================================================================
extern "C" int fdb_test_path(int argc, char** argv)    {
  RTL::CLI cli(argc, argv, [](int, char**) {});
  string name;
  cli.getopt("path", 3, name);
  if ( !name.empty() )   {
    string dir;
    vector<string> v;
    filesystem::path p(name);
    p = p.parent_path();
    for( auto i=p.begin(); i != p.end(); ++i )   {
      cout << (*i) << endl;
      v.emplace_back(i->string());
    }
    dir = "";
    struct stat stat;
    for( size_t i=1; i < v.size(); ++i )  {
      dir += "/";
      dir += v[i];
      if ( -1 == ::stat(dir.c_str(), &stat) )   {
	cout << dir << endl;
      }
    }
  }
  return 0;
}
//
//==========================================================================
extern "C" int fdb_test_parse_uri(int argc, char** argv)    {
  RTL::CLI cli(argc, argv, [](int, char**) {});
  string url;

  cli.getopt("url", 3, url);
  if ( !url.empty() )   {
    uri_t u(url);
    cout << "Protocol:'" << u.protocol << "' ";
    cout << "Host:'"     << u.host     << "' ";
    cout << "Port:'"     << u.port     << "' ";
    cout << "Path:'"     << u.path     << "' ";
    cout << "Query:'"    << u.query    << "' " << endl;
  }
  return 0;
}
//
//==========================================================================
extern "C" int fdb_test_db_client_get(int argc, char** argv)    {
  Setup  setup("fdb_test_client_get",argc, argv, [](int, char**)  {
    ::fprintf(stderr,
	      "fdb_test_client_get -opt [-opt]                             \n"
	      "     -server=<name:port>    Server access specification.    \n"
	      "     -url=<url-spec>        URL of the file to receive      \n"
	      "     -print=<level>         Set print level 1=DEBUG, 5=FATAL\n");
  });
  uri_t url(setup.server);
  auto   client = client::create<client::sync>(url.host, url.port, 10000, setup.debug);
  auto   reply  = client->get(setup.url);
  setup.print(reply);
  if ( reply.status == reply.permanent_redirect )   {
    auto* loc = reply.header(http::constants::location);
    if ( loc )   {
      ::lib_rtl_output(LIB_RTL_INFO,"+++ %s: Successfully resolved URI: %s to %s",
		       setup.process.c_str(), setup.url.c_str(), loc->value.c_str());
    }
    return 0;
  }
  ::lib_rtl_output(LIB_RTL_ERROR,"+++ %s: FAILED to resolve URI: %s",
		   setup.process.c_str(), setup.url.c_str());
  return EINVAL;
}
//
//==========================================================================
extern "C" int fdb_test_client_next_delete(int argc, char** argv)    {
  Setup  setup("fdb_test_client_get_next_delete",argc, argv);
  auto   client = setup.client();
  size_t length = 0;
  time_t date = 0;

  auto reply = client.next_object_delete(setup.url, date, length);
  reply.content.push_back(0);
  setup.print(reply).check(reply);
  if ( reply.status == reply.ok )   {
    auto* loc = reply.header(http::constants::location);
    if ( loc )   {
      std::filesystem::path p = std::filesystem::path(loc->value).filename();
      ::lib_rtl_output(LIB_RTL_INFO,"+++ %s: Successfully resolved: %s to %s",
		       setup.process.c_str(), p.c_str(), setup.url.c_str());
      return 0;
    }
    ::lib_rtl_output(LIB_RTL_INFO,"+++ %s: Successfully loaded data: %s",
		       setup.process.c_str(), setup.url.c_str());
    return 0;
  }
  ::lib_rtl_output(LIB_RTL_ERROR,"+++ %s: FAILED to load data for URI: %s",
		   setup.process.c_str(), setup.url.c_str());
  return EINVAL;
}
//
//==========================================================================
extern "C" int fdb_test_client_save(int argc, char** argv)    {
  Setup  setup("fdb_test_client_save", argc, argv);
  struct stat stat;
  int    ret_stat = ::stat(setup.file.c_str(), &stat);
  if ( -1 == ret_stat )   {
    int err = errno;
    ::lib_rtl_output(LIB_RTL_ERROR,
		     "fdb_test_save: Cannot access file: %s [%s]",
		     setup.file.c_str(), make_error_code(errc(err)).message().c_str());
    return err;
  }
  uri_t url(setup.server);
  auto client = client::create<client::sync>(url.host, url.port, 10000, setup.debug);
  client::reqheaders_t hdrs  {
    { http::constants::content_length, stat.st_size           },
    { http::constants::content_type,   "rawdata/lhcb"         },
    { http::constants::date,           header_t::now()        },
    { http::constants::expect,         "100-continue"         }
  };
  auto reply = client->request(http::constants::put, setup.url, hdrs);
  reply.content.push_back(0);
  reply.print(cout,"fdb_test_client_save: ");
  return 0;
}
//
//==========================================================================
extern "C" int fdb_test_client_put(int argc, char** argv)    {
  std::string url;
  Setup  setup("fdb_test_client_put", argc, argv);
  auto client = setup.client();
  auto buffer = setup.generate(setup.length, 0);
  auto reply  = client.save_object_record(setup.file, url, buffer.first);
  if ( reply.status == reply.permanent_redirect || reply.status == reply.continuing )  {
    ::lib_rtl_output(LIB_RTL_ALWAYS,"+++ %s: Successfully registered %ld byte file: %s",
		     setup.process.c_str(), buffer.first, setup.file.c_str());
    ::lib_rtl_output(LIB_RTL_ALWAYS,"+++ %s: URL: %s", setup.process.c_str(), url.c_str());
    reply = client.save_object_data(url, buffer.second.begin(), buffer.first);
    if ( reply.status == reply.created )  {
      ::lib_rtl_output(LIB_RTL_ALWAYS,"+++ %s: Successfully saved %ld bytes as %s",
		       setup.process.c_str(), buffer.first, setup.file.c_str());
      return 0;
    }
    ::lib_rtl_output(LIB_RTL_ERROR,"+++ %s: FS server replied %s", setup.process.c_str(),
		     http::HttpReply::stock_status(reply.status).c_str());
  }
  ::lib_rtl_output(LIB_RTL_ERROR,"+++ %s: Failed to save %ld bytes as %s",
		   setup.process.c_str(), buffer.first, setup.file.c_str());
  ::lib_rtl_output(LIB_RTL_ERROR,"+++ %s: %s", setup.process.c_str(),
		   http::HttpReply::stock_status(reply.status).c_str());
  return reply.status;
}
//
//==========================================================================
extern "C" int fdb_test_db_server_stress(int argc, char** argv)    {
  Setup  setup("fdb_test_client_get_next_delete",argc, argv);
  std::vector<std::string> files;
  std::size_t length = 10*1024;
  std::size_t count = 0;
  std::string url;

  ::lib_rtl_output(LIB_RTL_ALWAYS,"+++ %s: Will perform %ld x %ld turns. Server: %s run: %s",
		   setup.process.c_str(), setup.count, setup.length, 
		   setup.server.c_str(), setup.run.c_str());

  files.reserve(setup.count);
  for(std::size_t i=0; i < setup.count; ++i)
    files.emplace_back("/objects/fdb_test/"+setup.run+"/Run000"+setup.run+"."+std::to_string(i)+".raw");
    
  auto start = chrono::system_clock::now();
  auto now   = start;
  long duration = 0;
  for(std::size_t j=0; j < setup.length; ++j)  {
    for(std::size_t i=0; i < setup.count; ++i)  {
      const auto& loc = files[i];
      auto client = setup.client();
      auto reply  = client.save_object_record(loc, url, i * length);
      ++count;
      if ( reply.status == reply.permanent_redirect )   {
	if ( (count%1000) == 0 )   {
	  now = chrono::system_clock::now();
	  duration = chrono::duration_cast<chrono::milliseconds>(now-start).count();
	  ::lib_rtl_output(LIB_RTL_INFO,"+++ %s: Performed %ld database operations.  %7.3f msec/op",
			   setup.process.c_str(), count, double(duration)/double(count));
	}
	auto cl = setup.client();
	client::reqheaders_t hdrs { { http::constants::state, "WRITTEN" } };
        reply  = cl.fdbclient->update(loc, nullptr, 0, hdrs);
	++count;
	if ( reply.status == reply.ok )  {
	  if ( (count%1000) == 0 )  {
	    now = chrono::system_clock::now();
	    duration = chrono::duration_cast<chrono::milliseconds>(now-start).count();
	    ::lib_rtl_output(LIB_RTL_INFO,"+++ %s: Performed %ld database operations.  %7.3f msec/op",
			     setup.process.c_str(), count, double(duration)/double(count));
	  }
	  continue;
	}
	::lib_rtl_output(LIB_RTL_ERROR,"+++ %s: FAILED to update database record: %s [Operations: %ld]",
			 setup.process.c_str(), loc.c_str(), count);
	return EINVAL;
      }
      ::lib_rtl_output(LIB_RTL_ERROR,"+++ %s: FAILED to manipulate database record: %s [Operations: %ld]",
		       setup.process.c_str(), loc.c_str(), count);
      return EINVAL;
    }

    for(std::size_t i=0; i < setup.count; ++i)  {
      std::size_t len = 0;
      std::time_t tim = 0;
      const auto& loc = files[i];
      auto client = setup.client();
      auto reply  = client.db_object_delete(loc, tim, len);
      ++count;
      if ( reply.status == reply.ok )   {
	const auto* hdr = reply.header(http::constants::data_length);
	if ( hdr ) len = hdr->as<std::size_t>();
	hdr = reply.header(http::constants::date);
	if ( hdr ) tim = hdr->as<std::time_t>();
	/// Check for data inconsistencies
	if ( len != i*length )   {
	  reply.print(cout,"");
	  ::lib_rtl_output(LIB_RTL_ERROR,"+++ %s: %s INCONSISTENT data: len: %ld should be: %ld",
			   setup.process.c_str(), loc.c_str(), len, i*length);
	}
	if ( (count%1000) == 0 )   {
	  now = chrono::system_clock::now();
	  duration = chrono::duration_cast<chrono::milliseconds>(now-start).count();
	  ::lib_rtl_output(LIB_RTL_INFO,"+++ %s: Performed %ld database operations.  %7.3f msec/op",
			   setup.process.c_str(), count, double(duration)/double(count));
	}
	continue;
      }
      ::lib_rtl_output(LIB_RTL_ERROR,"+++ %s: FAILED to manipulate database record: %s [Operations: %ld]",
		       setup.process.c_str(), loc.c_str(), count);
      return EINVAL;
    }
  }
  ++count;
  now = chrono::system_clock::now();
  duration = chrono::duration_cast<chrono::milliseconds>(now-start).count(); 
  ::lib_rtl_output(LIB_RTL_INFO,"+++ %s: All done. Performed %ld database operations.  %7.3f msec/op",
		   setup.process.c_str(), count, double(duration)/double(count));
  return 0;
}
