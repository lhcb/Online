//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC-GUI
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include <Storage/client_sync.h>
#include <HTTP/Asio.h>
#include <RTL/rtl.h>

// C/C++ include files
#include <set>
#include <chrono>
#include <cstring>
#include <iostream>
#include <stdexcept>
#include <unistd.h>

using namespace std;
using namespace boost;
using namespace Online::storage;

///  Helper class responsible for synchronous I/O
/**
 *
 *  \author  M.Frank
 *  \version 1.0
 *  \date    02.05.2017
 */
class Online::storage::client_sync::io_t  {
public:
  client_sync*            client {0};
  asio::io_context        context;
  asio::ip::tcp::socket   socket;
  asio::ip::tcp::resolver resolver;
  io_t(client_sync* cl) : client(cl), context(), socket(context), resolver(context)
  {
  }
  ~io_t()
  {
    if ( socket.is_open() ) close();
  }
  std::error_code open();
  void close();
};

/// Open connection
error_code Online::storage::client_sync::io_t::open()    {
  using namespace asio::ip;
  system::error_code ec(system::errc::success, system::system_category());
  if ( !socket.is_open() )   {
#if BOOST_ASIO_VERSION < 103400    
    auto conns = resolver.resolve({tcp::v4(), client->host, client->port}, ec);
#else    // version >= 1.34.0
    auto conns = resolver.resolve(tcp::v4(), client->host, client->port, ec);
#endif
    if ( error_code_ok(ec) )   {
      asio::connect(socket, conns, ec);
      if ( error_code_ok(ec) )   {
	socket.set_option(asio::socket_base::reuse_address(true));
	socket.set_option(asio::socket_base::linger(true,0));
      }
    }
  }
  return make_error_code(errc(ec.value()));
}

/// Open connection
void Online::storage::client_sync::io_t::close()    {
  if ( socket.is_open() )   {
    system::error_code ec(system::errc::success, system::system_category());
    socket.shutdown(asio::ip::tcp::socket::shutdown_both, ec);
    socket.close();
    //::lib_rtl_output(LIB_RTL_ERROR,"client_sync::io_t::close: Connection closed!\n");
  }
}

template <>
unique_ptr<client> client::create<client::sync>(const string& host, const string& port, int tmo, int dbg)   {
  auto obj = make_unique<client_sync>(host, port, tmo, dbg);
  return obj;
}

/// Initializing constructor
client_sync::client_sync(const string& h, const string& p, int tmo, int dbg)
  : client(h, p, tmo, dbg), io(make_unique<io_t>(this))
{
}

/// Default destructor
client_sync::~client_sync() {
  io.reset();
}

/// Initializing constructor
error_code client_sync::open()  const  {
  if ( !io->socket.is_open() )   {
    return io->open();
  }
  return make_error_code(errc(0));
}

/// Receive data buffer from server peer
reply_t::status_type client_sync::receive(reply_t& reply)    const    {
  const header_t* hdr = reply.header(http::constants::content_length);
  if ( hdr )   {
    reply.bytes_total = hdr->as<long>();
    if ( reply.bytes_total > 0 )   {
      unsigned char* buff;
      system::error_code ec;
      long chunk_size = 1024*128;
      long num_bytes, todo;
      reply.bytes_data = reply.content.size();
      reply.content.resize(reply.bytes_total);
      buff = &reply.content[0];

      do   {
	todo = reply.bytes_data+chunk_size<reply.bytes_total
	  ? chunk_size : reply.bytes_total-reply.bytes_data;
	num_bytes = asio::read(io->socket, asio::buffer(buff+reply.bytes_data, todo), ec);
#if 0
	::lib_rtl_output(LIB_RTL_ERROR,"receive: Got data chunk of %ld bytes [now: %ld] %s",
			 num_bytes, reply.bytes_data, ec.message().c_str());
	unsigned char* p = buff;
	size_t mx = std::min(1024L,num_bytes);
	for( size_t i=0; i<mx; i+=16 )   {
	  ::lib_rtl_output(LIB_RTL_ERROR,"%06d - %02x%02x%02x%02x %02x%02x%02x%02x %02x%02x%02x%02x %02x%02x%02x%02x  ",
			   i, p[0], p[1], p[2], p[3], p[4], p[5], p[6], p[7], p[8], p[9], p[10], p[11], p[12], p[13], p[14], p[15]);
	  p += 16;
	}
#endif
	if ( num_bytes < 0 ) break;
	reply.bytes_data += num_bytes;
	if ( !error_code_ok(ec) ) break;
      } while ( error_code_ok(ec) && reply.bytes_data < reply.bytes_total );
      if ( !error_code_ok(ec) )    {
	::lib_rtl_output(LIB_RTL_ALWAYS,"receive error: %s\n",ec.message().c_str());
      }
      if ( reply.bytes_data < reply.bytes_total )    {
	::lib_rtl_output(LIB_RTL_INFO,
			 "Receive incomplete: Got %ld bytes [expected: %ld]",
			 reply.bytes_data, reply.bytes_total);
      }
      if ( reply.bytes_data >= reply.bytes_total )    {
	::lib_rtl_output(LIB_RTL_INFO,
			 "Receive Successful: Got %ld bytes [expected: %ld]",
			 reply.bytes_data, reply.bytes_total);
      }
      if ( reply.bytes_data == reply.bytes_total )    {
	return reply.status;
      }
      return reply.status;
    }
  }
  return (reply.status=reply_t::no_content);
}

/// Send data buffer
reply_t::status_type client_sync::send(const void* data, size_t len)  const   {
  if ( data && len > 0 )   {
    error_code ec = io->open();
    if ( error_code_ok(ec) )   {
      system::error_code err;
      vector<asio::const_buffer> request = {asio::const_buffer(data, len)};
      size_t req_len = asio::detail::buffer_size(request[0]);
      size_t snd_len = asio::write(io->socket, request, err);
      if ( snd_len != req_len )  {
	io->close();
	errno = ENOTCONN;
	return reply_t::service_unavailable;
      }
      return reply_t::ok;
    }
    return reply_t::service_unavailable;
  }
  return reply_t::no_content;
}

/// Exec generic statement and receive client http reply header
reply_t client_sync::get_reply()  const   {
  string       name, value, header, version, message;
  size_t       idq, idx, len;
  unsigned int code;
  reply_t      reply;
  asio::streambuf response;
  system::error_code ec;

  asio::read_until(io->socket, response, "\r\n\r\n", ec);

  if ( error_code_ok(ec) )   {
    istream response_stream(&response);
      
    // Check that response is OK.
    response_stream >> version;
    response_stream >> code;
    getline(response_stream, message);
    if (!response_stream || version.substr(0, 5) != "HTTP/")    {
      reply = reply_t::stock_reply(reply_t::http_version_not_supported);
      reply.headers.emplace_back(http::constants::error_cause,"Invalid HTTP header");
      io->close();
      return reply;
    }
    reply.status = reply_t::status_type(code);
    // Read the response headers up to the terminator
    while( getline(response_stream, header) && header != "\r" )   {
      idx = header.find(' ');
      idq = header.rfind('\r');
      if ( idq != string::npos && idx != string::npos )   {
	name  = header.substr(0,idx-1);
	value = header.substr(idx+1, idq-idx-1);
	reply.headers.emplace_back(name, value);
      }
    }
    // Assign the rest of the streambuf to the content
    len = response.in_avail();
    if ( len > 0 )   {
      reply.content.resize(len);
      unsigned char* reply_content = &reply.content[0];
      response.sgetn((char*)reply_content, len);
    }
    return reply;
  }
  if ( ec == system::errc::connection_reset )
    reply = reply_t::stock_reply(reply_t::internal_server_error);
  else if ( ec == system::errc::connection_aborted )
    reply = reply_t::stock_reply(reply_t::internal_server_error);
  else if ( ec == system::errc::network_down )
    reply = reply_t::stock_reply(reply_t::bad_gateway);
  else if ( ec == system::errc::network_unreachable )
    reply = reply_t::stock_reply(reply_t::bad_gateway);
  else
    reply = reply_t::stock_reply(reply_t::internal_server_error);
  reply.headers.emplace_back(http::constants::error_cause, "Reply error ["+ec.message()+"]");
  return reply;
}

/// Exec generic statement
reply_t client_sync::request(const string& cmd, const string& url, const reqheaders_t& headers)  const   {
  reply_t reply;
  if ( !cmd.empty() && !url.empty() )    {
    string req  = this->build_request(cmd, url, headers);
    auto status = this->send(req.c_str(), req.length());
    if ( status == reply_t::ok )    {
      reply = get_reply();
    }
    else   {
      reply = reply_t::stock_reply(status);
    }
    return reply;
  }
  reply.status = reply_t::bad_request;
  return reply;
}

/// Connect client to given URI and execute RPC call
reply_t client_sync::get(const string& url, const reqheaders_t& headers)  const   {
  reqheaders_t hdrs(headers);
  hdrs.push_back({http::constants::content_length,"0"});
  reply_t reply = this->request(http::constants::get, url, hdrs);
  ::lib_rtl_output(LIB_RTL_DEBUG,"client_sync::get: request");
  switch(reply.status)   {
  case reply_t::continuing:
  case reply_t::ok:
    ::lib_rtl_output(LIB_RTL_DEBUG,"client_sync::get: receive");
    this->receive(reply);
    ::lib_rtl_output(LIB_RTL_DEBUG,"client_sync::get: receive .... done");
    break;
  case reply_t::temp_redirect:
    break;
  default:
    break;
  }
  return reply;
}

/// Connect client to given URI and execute DELETE statement
reply_t client_sync::del(const string& url, const reqheaders_t& headers)  const   {
  reqheaders_t hdrs(headers);
  hdrs.push_back({http::constants::content_length,"0"});
  reply_t reply = this->request(http::constants::del, url, hdrs);
  ::lib_rtl_output(LIB_RTL_DEBUG,"client_sync::delete: request");
  switch(reply.status)   {
  case reply_t::continuing:
  case reply_t::ok:
    ::lib_rtl_output(LIB_RTL_DEBUG,"client_sync::delete: receive");
    this->receive(reply);
    ::lib_rtl_output(LIB_RTL_DEBUG,"client_sync::delete: receive .... done");
    break;
  case reply_t::temp_redirect:
    break;
  default:
    break;
  }
  return reply;
}

/// Connect client to given URI and execute RPC call
reply_t client_sync::put(const string& url, const void* data, size_t len, const reqheaders_t& headers)  const   {
  reqheaders_t hdrs(headers);
  hdrs.push_back(header_t(http::constants::content_length,len));
  reply_t reply = this->request(http::constants::put, url, hdrs);
  switch(reply.status)   {
  case reply_t::continuing:
  case reply_t::ok:
    reply.status = this->send(data, len);    /// Answer should be 201 HTTP Created
    if ( reply.status == reply_t::ok )  {
      reply = this->get_reply();
    }
    break;
  case reply_t::temp_redirect:
    break;
  default:
    break;
  }
  return reply;
}

/// Connect client to given URI and execute RPC call
reply_t client_sync::update(const string& url, const void* data, size_t len, const reqheaders_t& headers)  const   {
  reqheaders_t hdrs(headers);
  hdrs.push_back(header_t(http::constants::content_length,len));
  reply_t reply = this->request(http::constants::update, url, hdrs);
  switch(reply.status)   {
  case reply_t::continuing:
  case reply_t::ok:
    if ( len > 0 )   {
      reply.status = this->send(data, len);    /// Answer should be 201 HTTP Created
      if ( reply.status == reply_t::ok )  {
	reply = this->get_reply();
      }
    }
    break;
  case reply_t::temp_redirect:
    break;
  default:
    break;
  }
  return reply;
}
