//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : Storage
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include "Storage/fdb_client.h"

// C/C++ include files
#include <ctime>
#include <cstring>
#include <stdexcept>

using namespace std;
using namespace Online::storage;

/// Initializing constructor
fdb_client::fdb_client(int vsn) : version(vsn) {
} 

/// Read object data from the specified location without removing disk object
reply_t fdb_client::get_object(const string& location, time_t& date, size_t& length)   {
  reply_t reply = fdbclient->request(http::constants::get, location);
  length = 0;
  date   = 0;
  if ( reply.status == reply_t::temp_redirect || reply.status == reply_t::permanent_redirect )  {
    reply.status = reply_t::permanent_redirect;
    for ( const auto& h : reply.headers )   {
      if ( ::strcasecmp(h.name.c_str(),http::constants::location) == 0 )   {
	uri_t u(h.value);
	unique_ptr<client> cl(create_client(u.host, u.port, dataTMO, this->debug));
	if ( error_code_ok(cl->open(), this->debug) )   {
	  reply = cl->get(u.path);
	  if ( reply.status == reply_t::ok )  {
	    const header_t* hdr = reply.header(http::constants::date);
	    if ( hdr ) date = hdr->as_time();
	    hdr = reply.header(http::constants::content_length);
	    if ( hdr ) length = hdr->as<size_t>();
	    return reply;
	  }
	  return reply;
	}
	reply.status = reply_t::bad_gateway;
	return reply;
      }
    }
    reply.status = reply_t::not_found;
  }
  return reply;
}

/// Read AND Delete object data from disk at the specified location
reply_t fdb_client::delete_object(const string& location, time_t& date, size_t& length)   {
  reply_t reply = fdbclient->request(http::constants::del, location);
  length = 0;
  date   = 0;
  if ( reply.status == reply_t::temp_redirect || reply.status == reply_t::permanent_redirect )  {
    reply.status = reply_t::not_found;
    for ( const auto& h : reply.headers )   {
      if ( ::strcasecmp(h.name.c_str(),http::constants::location) == 0 )   {
	uri_t u(h.value);
	unique_ptr<client> cl(create_client(u.host, u.port, dataTMO, this->debug));
	if ( error_code_ok(cl->open(), this->debug) )   {
	  reply = cl->del(u.path);
	  if ( reply.status == reply_t::ok )  {
	    const header_t* hdr = reply.header(http::constants::date);
	    if ( hdr ) date = hdr->as_time();
	    hdr = reply.header(http::constants::content_length);
	    if ( hdr ) length = hdr->as<size_t>();
	  }
	  return reply;
	}
	reply.status = reply_t::bad_gateway;
	return reply;
      }
    }
  }
  return reply;
}

/// Read AND Delete object data from disk at the specified location
reply_t fdb_client::next_object_get(string& prefix, time_t& date, size_t& length)   {
  string location;
  if ( version == 0 ) location = "/next?prefix="+prefix;
  else if ( version > 0 ) location = "/dataflow/next/"+prefix;
  reply_t reply = fdbclient->request(http::constants::get, location);

  length = 0;
  date   = 0;
  if ( reply.status == reply_t::gone || reply.status == reply_t::not_found )    {
    return reply;
  }
  if ( reply.status == reply_t::temp_redirect || reply.status == reply_t::permanent_redirect )  {
    reply.status = reply_t::not_found;
    for ( const auto& h : reply.headers )   {
      if ( ::strcasecmp(h.name.c_str(),http::constants::location) == 0 )   {
	uri_t u(h.value);
	unique_ptr<client> cl(create_client(u.host, u.port, dataTMO, this->debug));
	if ( error_code_ok(cl->open(), this->debug) )   {
	  auto hdr_location = h.value;
	  reply = cl->get(u.path);
	  if ( reply.status == reply_t::ok )  {
	    const header_t* hdr = reply.header(http::constants::date);
	    if ( hdr ) date = hdr->as_time();
	    hdr = reply.header(http::constants::content_length);
	    if ( hdr ) length = hdr->as<size_t>();
	    prefix = hdr_location;
	  }
	  return reply;
	}
	reply.status = reply_t::bad_gateway;
	return reply;
      }
    }
  }
  return reply;
}

/// Read AND Delete object data from disk at the specified location
reply_t fdb_client::next_object_delete(string& prefix, time_t& date, size_t& length)   {
  string location;
  if ( version == 0 ) location = "/next?prefix="+prefix;
  else if ( version > 0 ) location = "/dataflow/next/"+prefix;
  reply_t reply = fdbclient->request(version == 0 ? http::constants::del : http::constants::get, location);

  length = 0;
  date   = 0;
  if ( reply.status == reply_t::gone || reply.status == reply_t::not_found )    {
    return reply;
  }
  if ( reply.status == reply_t::temp_redirect || reply.status == reply_t::permanent_redirect )  {
    reply.status = reply_t::not_found;
    for ( const auto& h : reply.headers )   {
      if ( ::strcasecmp(h.name.c_str(),http::constants::location) == 0 )   {
	uri_t u(h.value);
	unique_ptr<client> cl(create_client(u.host, u.port, dataTMO, this->debug));
	if ( error_code_ok(cl->open(), this->debug) )   {
	  auto hdr_location = h.value;
	  reply = version == 0 ? cl->del(u.path) : cl->get(u.path);
	  if ( reply.status == reply_t::ok )  {
	    const header_t* hdr = reply.header(http::constants::date);
	    if ( hdr ) date = hdr->as_time();
	    hdr = reply.header(http::constants::content_length);
	    if ( hdr ) length = hdr->as<size_t>();
	    prefix = hdr_location;
	  }
	  return reply;
	}
	reply.status = reply_t::bad_gateway;
	return reply;
      }
    }
  }
  return reply;
}

/// Helper: Delete the specified object data from the database
reply_t fdb_client::db_object_delete(const string& location, time_t& date, size_t& length)   {
  reply_t reply = fdbclient->request(http::constants::del, location);
  length = 0;
  date   = 0;
  if ( reply.status == reply_t::temp_redirect || reply.status == reply_t::permanent_redirect )  {
    reply.status = reply_t::ok;
  }
  return reply;
}

/// Save object record to database and return the location to store the data
reply_t fdb_client::save_object_record(const string& location, string& url, size_t len, const client::reqheaders_t& opts)   {
  struct tm tim;
  time_t now = ::time(nullptr);
  client::reqheaders_t hdrs  {
    { http::constants::content_length, len                    },
    { http::constants::content_type,   "rawdata/lhcb"         },
    { http::constants::date,           ::gmtime_r(&now, &tim) },
    { http::constants::expect,         "100-continue"         }
  };
  /// Add optional headers if required
  if ( !opts.empty() )   {
    hdrs.insert(hdrs.end(), opts.begin(), opts.end());
  }
  auto reply = fdbclient->request(http::constants::put, location, hdrs);
  if ( reply.status == reply_t::temp_redirect || reply.status == reply_t::permanent_redirect )  {
    reply.status = reply_t::permanent_redirect;
    for ( const auto& h : reply.headers )   {
      if ( ::strcasecmp(h.name.c_str(),http::constants::location) == 0 )   {
	url = h.value;
	return reply;
      }
    }
    return reply_t::stock_reply(reply_t::not_found);
  }
  return reply;
}

/// Save object data to disk at the specified location
reply_t fdb_client::save_object_data(const string& location, const void* data, size_t len, const client::reqheaders_t& opts)   {
  struct tm tim;
  time_t now = ::time(nullptr);
  client::reqheaders_t hdrs  {
    { http::constants::content_type, "rawdata/lhcb"         },
    { http::constants::date,         ::gmtime_r(&now, &tim) },
    { http::constants::expect,       "100-continue"         }
  };
  uri_t u(location);
  unique_ptr<client> cl(create_client(u.host, u.port, dataTMO, this->debug));
  if ( error_code_ok(cl->open(), this->debug) )   {
    /// Add optional headers if required
    if ( !opts.empty() )   {
      hdrs.insert(hdrs.end(), opts.begin(), opts.end());
    }
    auto reply = cl->put(u.path, data, len, hdrs);
    if ( reply.status == reply_t::created )   {
      return reply;
    }
    return reply;
  }
  return reply_t::stock_reply(reply_t::bad_gateway);
}

/// Save object data to disk at the specified location
reply_t fdb_client::save_object(const string& location, const void* data, size_t len)   {
  string url;
  auto reply = save_object_record(location, url, len);
  if ( reply.status == reply_t::temp_redirect || reply.status == reply_t::permanent_redirect )  {
    reply.status = reply_t::permanent_redirect;
    return save_object_data(url, data, len);
  }
  return reply;
}

/// Save object data to disk at the specified location
reply_t fdb_client::save_object(const string& location, vector<unsigned char>&& data)   {
  return save_object(location, &data[0], data.size());
}
