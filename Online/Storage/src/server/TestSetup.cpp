//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================

// Framework inclde files
#include <sqldb/sqlite.h>
#include <HTTP/HttpHeader.h>  
#include <RTL/Logger.h>
#include "TestSetup.h"
#include "fdb_sqldb.h"

// C/C++ include files
#include <sstream>
#include <fstream>
#include <iostream>

using Online::storage::server::TestSetup;

/// Initializing constructor
TestSetup::TestSetup(const std::string& n,
		     int argc, char* argv[],
		     void (*help)(int argc, char** argv))
  : name(n)
{
  void (*hlp)(int,char**) = [](int, char**) {
    std::cout << "storage_sqlite_<test> -opt [-opt] " << std::endl
	      << "  -database=<name>         Path to database file " << std::endl
	      << "  -stream=<name>           Stream name (optional) " << std::endl
	      << "  -count=<number>          Iteration count (optional) " << std::endl
	      << "  -entry=<number>          Entry identifier (optional) " << std::endl
	      << "  -print=<print-level>     Set print level (default: INFO) " << std::endl
	      << "  -match=<expression>      Search expression for runs/files " << std::endl;
    ::exit(0);
  };
  RTL::CLI cli(argc, argv, help ? help : hlp);
  cli.getopt("database", 8, database);
  cli.getopt("stream",   5, stream);
  cli.getopt("count",    5, count);
  cli.getopt("entry",    5, entry);
  cli.getopt("run",      3, run);
  cli.getopt("print",    4, prt);
  cli.getopt("match",    4, match);
  this->have_dump = cli.getopt("dump", 4) != 0;
  this->have_time = cli.getopt("time", 4) != 0;
  this->unlink_db = cli.getopt("unlink", 6) != 0;
  RTL::Logger::install_log(RTL::Logger::log_args(prt));
  ::lib_rtl_output(LIB_RTL_ALWAYS,"%s: Test starting....", name.c_str());
}

/// Default destructor
TestSetup::~TestSetup()   {
  if ( this->status == 1 && this->have_dump )   {
    this->dump_db();
  }
  ::lib_rtl_output(LIB_RTL_ALWAYS,
		   "%s: Test finished %s!",
		   name.c_str(),
		   this->status == 1 ? "successfully" : "NOT successfully");
}

/// Dump database content
int TestSetup::dump_db()   {
  using file_t = Online::storage::fdb_dbase_t::file_t;
  std::string error, sql;
  std::vector<file_t> files;
  sqldb::database fdb_database = this->db();
  if ( !fdb_database.is_valid() )    {
    ::lib_rtl_output(LIB_RTL_ERROR,"%s: Can't open database [%s]",
		     this->database.c_str(), fdb_database.errmsg().c_str());
    this->status = 0;
    return EINVAL;
  }
  sql =
    "SELECT Name, State, Size, Date, Host"
    " FROM Files"
    " WHERE Size>?1";
  if ( !this->match.empty() )
    sql += " AND Name LIKE ?2";

  sqldb::statement query = sqldb::statement::create(fdb_database, sql.c_str());
  ::lib_rtl_output(LIB_RTL_DEBUG, "SQL: %s", sql.c_str());

  if ( query.is_valid() )  {
    sqldb::status_t ret = query.execute();
    int num_records = 0;
    int size = 0;
    file_t f;
    query.bind(0, size);
    if ( !this->match.empty() ) query.bind(1, sql=this->match+'%');
    while ( (ret = query.fetch_one()) == sqldb::OK )   {
      f.name  = query.get<std::string>(0);
      f.state = query.get<int>(1);
      f.size  = query.get<int64_t>(2);
      f.date  = query.get<std::string>(3);
      f.host  = query.get<std::string>(4);
      ::lib_rtl_output(LIB_RTL_INFO, "%4ld File: %s Host: %s %s",
		       num_records, f.name.c_str(), f.host.c_str(),
		       this->have_time ? f.date.c_str() : "");
      ::lib_rtl_output(LIB_RTL_INFO, "           State:%d  %9ld bytes",
		       f.state, f.size);
      ++num_records;
    }
  }
  else  {
    ::lib_rtl_output(LIB_RTL_ERROR, "SQL: Invalid query: %s  [%s]",
		     sql.c_str(), fdb_database.errmsg().c_str());
    this->status = 0;
  }
  fdb_database.close();
  return 0;
}

/// Access SQLDB database object
sqldb::database TestSetup::db()  const    {
  sqldb::database instance;
  instance.open<sqlite>(this->database);
  if ( instance.intern->db == nullptr )    {
    ::lib_rtl_output(LIB_RTL_ERROR,"%s: Can't open database [%s]",
		     name.c_str(), instance.errmsg().c_str());
  }
  return instance;
}
