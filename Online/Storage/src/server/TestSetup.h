//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : Online/Storage
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_STORAGE_TESTSETUP_H
#define ONLINE_STORAGE_TESTSETUP_H

// Framework inclde files
#include <sqldb/sqldb.h>

// C/C++ include files
#include <string>

/// Online Namespace
namespace Online   {

  /// Namespace for the storage interface
  namespace storage   {    
  
    /// Namespace for the server interface
    namespace server   {    

      /// Setup object to run server tests
      /**
       *  \author  M.Frank
       *  \version 1.0
       */
      class TestSetup   {
      public:
	std::string name, database, stream, entry, match;
	std::size_t count = 0;
	long        run = 0;
	int         status = 1;
	int         prt = LIB_RTL_INFO;
	bool        have_time = false;
	bool        have_dump = false;
	bool        unlink_db = false;

      public:
	/// Initializing constructor
	TestSetup(const std::string& n, int argc, char* argv[],
		  void (*help)(int argc, char** argv)=nullptr);
	/// Default destructor
	~TestSetup();
	/// Dump database content
	int dump_db();
	/// Access SQLDB database object
	sqldb::database db()  const;
      };
    }     // End namespace server
  }     // End namespace storage
}       // End namespace Online
#endif  // ONLINE_STORAGE_TESTSETUP_H
