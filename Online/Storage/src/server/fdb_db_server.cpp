//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include <HTTP/HttpReply.h>
#include <Storage/fdb_dbase.h>
#include <Storage/fdb_server.h>
#include <Storage/communication.h>
#include <RTL/strdef.h>
#include <RTL/rtl.h>
#include <dim/dis.h>

// C/C++ include files

class Online::storage::db::traits {
public:
  typedef fdb_dbase_t         dbase_t;
  typedef fdb_dbase_monitor_t monitor_t;
};

using namespace Online::storage;

static constexpr const double kByte = 1024e0;
static constexpr const double MByte = kByte*kByte;

static inline std::string object_name(const std::string& obj, std::string opt="")   {
  static const std::string  PATTERN_NEXT = "/next?prefix=";
  bool get_next = (0 == ::strncasecmp(obj.c_str(),
				      PATTERN_NEXT.c_str(),
				      PATTERN_NEXT.length())); 
  if ( get_next )  {
    return obj.substr(PATTERN_NEXT.length())+opt;
  }
  return obj;
}

/// Standard constructor
fdb_dbase_monitor_t::fdb_dbase_monitor_t()   {
  std::string nam = "/"+RTL::str_upper(RTL::nodeNameShort())+"/"+RTL::processName();
  add( ::dis_add_service((nam+"/DATA").c_str(),"C",&data,sizeof(data),0,0) );
  add( ::dis_add_service((nam+"/GET").c_str(),"L",
			 &data.num_get_success,sizeof(data.num_get_success),0,0) );
  add( ::dis_add_service((nam+"/GET_errors").c_str(),"L",
			 &data.num_get_errors,sizeof(data.num_get_errors),0,0) );
  add( ::dis_add_service((nam+"/GET_not_found").c_str(),"L",
			 &data.num_get_not_found,sizeof(data.num_get_not_found),0,0) );
  add( ::dis_add_service((nam+"/GET_unauthorized").c_str(),"L",
			 &data.num_get_unauthorized,sizeof(data.num_get_unauthorized),0,0) );

  add( ::dis_add_service((nam+"/DEL").c_str(),"L",
			 &data.num_del_success,sizeof(data.num_del_success),0,0) );
  add( ::dis_add_service((nam+"/DEL_errors").c_str(),"L",
			 &data.num_del_errors,sizeof(data.num_del_errors),0,0) );
  add( ::dis_add_service((nam+"/DEL_not_found").c_str(),"L",
			 &data.num_del_not_found,sizeof(data.num_del_not_found),0,0) );
  add( ::dis_add_service((nam+"/DEL_unauthorized").c_str(),"L",
			 &data.num_del_unauthorized,sizeof(data.num_del_unauthorized),0,0) );

  add( ::dis_add_service((nam+"/PUT").c_str(),"L",
			 &data.num_put_success,sizeof(data.num_put_success),0,0) );
  add( ::dis_add_service((nam+"/PUT_errors").c_str(),"L",
			 &data.num_put_errors,sizeof(data.num_put_errors),0,0) );
  add( ::dis_add_service((nam+"/PUT_bad_request").c_str(),"L",
			 &data.num_put_bad_request,sizeof(data.num_put_bad_request),0,0) );
  add( ::dis_add_service((nam+"/PUT_unauthorized").c_str(),"L",
			 &data.num_put_unauthorized,sizeof(data.num_put_unauthorized),0,0) );

  add( ::dis_add_service((nam+"/UPDA").c_str(),"L",
			 &data.num_upda_success,sizeof(data.num_upda_success),0,0) );
  add( ::dis_add_service((nam+"/UPDA_errors").c_str(),"L",
			 &data.num_upda_errors,sizeof(data.num_upda_errors),0,0) );
  add( ::dis_add_service((nam+"/UPDA_bad_request").c_str(),"L",
			 &data.num_upda_bad_request,sizeof(data.num_upda_bad_request),0,0) );
}

/// Default destructor
fdb_dbase_monitor_t::~fdb_dbase_monitor_t()   {
  for( auto svc : services ) ::dis_remove_service(svc);
}

/// Standard constructor
template <> http::basic_http_server<db>::handler_t::handler_t()   {
  this->type = "Storage Database Server";
  ::lib_rtl_output(LIB_RTL_ALWAYS,"+++ %s: starting.....\n", this->type.c_str());
}

/// Standard destructor
template <> http::basic_http_server<db>::handler_t::~handler_t()   {
  ::lib_rtl_output(LIB_RTL_ALWAYS,"+++ %s: stopped.....\n", this->type.c_str());
}

/// Callback to eventually handle actions on connection shutdown
template <> void
http::basic_http_server<db>::handler_t::handle_shutdown(const request_t& req, const reply_t& rep)   {
  if ( req.uri == "/control=exit" && rep.status == reply_t::ok )    {
    this->manager.stop_all();
    ::kill(::lib_rtl_pid(), SIGTERM);
    return;
  }
}


/// Specific handler for DELETE requests
template <> http::HttpRequestHandler::continue_action
http::basic_http_server<db>::handler_t::handle_delete(const request_t& req, reply_t& rep)    {
  std::size_t length = 0;
  std::string date, access_name, obj = req.uri;
  auto* hdr_location = req.header(http::constants::location);

  std::error_code ec;  {
    dbase_t::lock_t lck(dbase.get());
    std::size_t idx = req.uri.find("/next?prefix=");
    if ( idx == 0 )  {
      obj = object_name(req.uri, "%");
    }
    else if ( req.uri.find("/dataflow/next/") == 0 )  {
      obj = object_name("/next?prefix="+req.uri.substr(15), "%");
    }
    ec = dbase->delete_object(obj, access_name, date, length, hdr_location != nullptr);
  }
  if ( ec == std::errc::no_such_file_or_directory )   {
    rep = reply_t::stock_reply(reply_t::not_found);
    ++monitor->data.num_del_not_found;
  }
  else if ( ec == std::errc::permission_denied )   {
    rep = reply_t::stock_reply(reply_t::unauthorized);
    ++monitor->data.num_del_unauthorized;
  }
  else if ( !error_code_ok(ec) )   {
    rep = reply_t::stock_reply(reply_t::not_found);
    ++monitor->data.num_del_not_found;
  }
  if ( !error_code_ok(ec, this->debug) )   {
    header_t h(http::constants::error_cause,"Failed to delete "+req.uri+" ["+ec.message()+"]");
    ::lib_rtl_output(LIB_RTL_ERROR,"+++ %s: %-6s %s %-20s = %s",
		     this->type.c_str(), req.method.c_str(), reply_t::stock_status(rep.status).c_str(),
		     h.name.c_str(), h.value.c_str());
    rep.headers.emplace_back(std::move(h));
    ++monitor->data.num_del_errors;
  }
  else   {
    bool mb = length > 3*MByte;
    rep = reply_t::stock_reply(reply_t::permanent_redirect);
    rep.headers.emplace_back(http::constants::location, access_name);
    rep.headers.emplace_back(http::constants::data_length, length);
    rep.headers.emplace_back(http::constants::date, date);
    ::lib_rtl_output(LIB_RTL_INFO,"+++ %s: %-6s '%s' %.1f %cB [%s]",
		     this->type.c_str(), req.method.c_str(), obj.c_str(),
		     double(length)/(mb ? MByte : kByte), mb ? 'M' : 'k',
		     req.remote_address().to_string().c_str());
    ++monitor->data.num_del_success;
  }
  return write;
}

/// Specific handler for GET requests
template <> http::HttpRequestHandler::continue_action
http::basic_http_server<db>::handler_t::handle_get(const request_t& req, reply_t& rep)    {
  std::error_code ec;
  std::size_t length = 0;
  std::string date, access_name;

  if ( handle_control(req, rep) )    {
    return write;
  }
  else  {
    if ( req.uri.find("/dataflow/next/") == 0 )  {
      return handle_delete(req, rep);
    }
    auto* hdr_location = req.header(http::constants::location);
    std::string obj = req.uri.find("/next?prefix=") == 0 ? object_name(req.uri,"%") : req.uri;
    dbase_t::lock_t lck(dbase.get());
    ec = dbase->query_object(obj, access_name, date, length, hdr_location != nullptr);
  }
  if ( ec == std::errc::no_such_file_or_directory )   {
    rep = reply_t::stock_reply(reply_t::not_found);
    ++monitor->data.num_get_not_found;
  }
  else if ( ec == std::errc::permission_denied )  {
    rep = reply_t::stock_reply(reply_t::unauthorized);
    ++monitor->data.num_get_unauthorized;
  }
  else if ( !error_code_ok(ec, this->debug) )  {
    rep = reply_t::stock_reply(reply_t::not_found);
    ++monitor->data.num_get_not_found;
  }
  if ( !error_code_ok(ec, this->debug) )   {
    header_t h(http::constants::error_cause,"Failed to get "+req.uri+" ["+ec.message()+"]");
    ::lib_rtl_output(LIB_RTL_ERROR,"+++ %s: %-6s %s %-20s = %s",
		     this->type.c_str(), req.method.c_str(), reply_t::stock_status(rep.status).c_str(),
		     h.name.c_str(), h.value.c_str());
    rep.headers.emplace_back(std::move(h));
    ++monitor->data.num_get_errors;
  }
  else   {
    bool mb = length > 3*MByte;
    rep = reply_t::stock_reply(reply_t::permanent_redirect);
    rep.headers.emplace_back(http::constants::location, access_name);
    rep.headers.emplace_back(http::constants::request_uri, req.uri);
    ::lib_rtl_output(LIB_RTL_INFO,"+++ %s: %-6s '%s' %.1f %cB [%s]",
		     this->type.c_str(), req.method.c_str(), access_name.c_str(),
		     double(length)/(mb ? MByte : kByte), mb ? 'M' : 'k',
		     req.remote_address().to_string().c_str());
    ++monitor->data.num_get_success;
  }
  return write;
}

/// Specific handler for PUT requests
template <> http::HttpRequestHandler::continue_action
http::basic_http_server<db>::handler_t::handle_put(const request_t& req, reply_t& rep)    {
  const header_t *hdr_date, *hdr_len, *hdr_location;
  hdr_location = req.header(http::constants::location);
  if ( (hdr_date=req.header(http::constants::date)) == nullptr )   {
    header_t h(http::constants::error_cause,"Missing date header");
    rep = reply_t::stock_reply(reply_t::bad_request);
    ::lib_rtl_output(LIB_RTL_ERROR,"+++ %s: %-6s bad_request %-20s = %s",
		     this->type.c_str(), req.method.c_str(), h.name.c_str(), h.value.c_str());
    ++monitor->data.num_put_bad_request;
    rep.headers.emplace_back(std::move(h));
  }
  else if ( (hdr_len=req.header(http::constants::content_length)) == nullptr )   {
    header_t h(http::constants::error_cause,"Missing content length header");
    rep = reply_t::stock_reply(reply_t::bad_request);
    ::lib_rtl_output(LIB_RTL_ERROR,"+++ %s: %-6s bad_request %-20s = %s",
		     this->type.c_str(), req.method.c_str(), h.name.c_str(), h.value.c_str());
    ++monitor->data.num_put_bad_request;
    rep.headers.emplace_back(std::move(h));
  }
  else   {
    const std::string&   date = hdr_date->value;
    std::size_t          len  = hdr_len->as<size_t>();
    std::string          access_name;
    std::error_code ec;  {
      dbase_t::lock_t lck(dbase.get());
      ec = dbase->add_object(req.uri, date, len, access_name, hdr_location != nullptr);
    }
    if ( ec == std::errc::permission_denied )  {
      header_t h(http::constants::error_cause,"Failed to add "+req.uri+" "+ec.message());
      rep = reply_t::stock_reply(reply_t::unauthorized);
      ::lib_rtl_output(LIB_RTL_ERROR,"+++ %s: %-6s %s  %-20s = %s",
		       this->type.c_str(), req.method.c_str(), reply_t::stock_status(rep.status).c_str(),
		       h.name.c_str(), h.value.c_str());
      ++monitor->data.num_put_unauthorized;
      rep.headers.emplace_back(std::move(h));
    }
    else if ( !error_code_ok(ec, this->debug) )   {
      header_t h(http::constants::error_cause,
		 "Failed to add object "+req.uri+" in dbase: "+ec.message());
      rep = reply_t::stock_reply(reply_t::bad_request);
      ::lib_rtl_output(LIB_RTL_ERROR,"+++ %s: %-6s %s  %-20s = %s",
		       this->type.c_str(), req.method.c_str(), reply_t::stock_status(rep.status).c_str(),
		       h.name.c_str(), h.value.c_str());
      ++monitor->data.num_put_bad_request;
      rep.headers.emplace_back(std::move(h));
    }
    else   {
      bool mb = len > 3*MByte;
      std::string remote = "UNKNOWN-ERROR";
      try  {
	remote = req.remote_address().to_string();
      }
      catch (...) {
      }
      rep = reply_t::stock_reply(reply_t::permanent_redirect);
      rep.content.clear();
      rep.headers.clear();
      rep.headers.emplace_back(http::constants::location, access_name);
      rep.headers.emplace_back(http::constants::data_length, len);
      rep.headers.emplace_back(http::constants::date, date);
      ::lib_rtl_output(LIB_RTL_INFO,"+++ %s: %-6s '%s' %.1f %cB [%s]",
		       this->type.c_str(), req.method.c_str(), req.uri.c_str(),
		       double(len)/(mb ? MByte : kByte), mb ? 'M' : 'k',
		       remote.c_str());
      ++monitor->data.num_put_success;
      return write;
    }
  }
  ++monitor->data.num_put_errors;
  return write;
}

/// Specific handler for POST requests
template <> http::HttpRequestHandler::continue_action
http::basic_http_server<db>::handler_t::handle_update(const request_t& req, reply_t& rep)    {
  const header_t *hdr_state;
  if ( (hdr_state=req.header(http::constants::state)) == nullptr )   {
    header_t h(http::constants::error_cause,"Missing state header");
    rep = reply_t::stock_reply(reply_t::bad_request);
    ::lib_rtl_output(LIB_RTL_ERROR,"+++ %s: %-6s bad_request %-20s = %s",
		     this->type.c_str(), req.method.c_str(), h.name.c_str(), h.value.c_str());
    ++monitor->data.num_upda_bad_request;
    rep.headers.emplace_back(std::move(h));
  }
  else   {
    std::error_code ec;  {
      dbase_t::lock_t lck(dbase.get());
      ec = dbase->update_object_state(req.uri, hdr_state->value);
    }
    if ( !error_code_ok(ec, this->debug) )   {
      header_t h(http::constants::error_cause,
		 "Failed to update object "+req.uri+" in dbase: "+ec.message());
      rep = reply_t::stock_reply(reply_t::bad_request);
      ::lib_rtl_output(LIB_RTL_ERROR,"+++ %s: %-6s %s  %-20s = %s",
		       this->type.c_str(), req.method.c_str(), reply_t::stock_status(rep.status).c_str(),
		       h.name.c_str(), h.value.c_str());
      ++monitor->data.num_upda_errors;
      rep.headers.emplace_back(std::move(h));
    }
    else   {
      ++monitor->data.num_upda_success;
      rep = reply_t::stock_reply(reply_t::ok);
      rep.content.clear();
      rep.headers.clear();
    }
  }
  return write;
}

/// Handle a request and produce a reply.
template <> http::HttpRequestHandler::continue_action
http::basic_http_server<db>::handler_t::handle_request(const request_t& req, reply_t& rep)    {
  if ( 0 == rep.bytes_sent )   {
    auto ret = this->HttpRequestHandler::handle_request(req, rep);
    if ( this->debug > 0 )   {
      std::string status = reply_t::stock_status(rep.status);
      ::lib_rtl_output(LIB_RTL_INFO,"%s::handle: Serving %s %s status: %d [%s]",
		       this->type.c_str(), req.method.c_str(), req.uri.c_str(), rep.status, status.c_str());
      for ( const auto& h : rep.headers )
	::lib_rtl_output(LIB_RTL_INFO,"%s::handle: %-20s = %s",
			 this->type.c_str(), h.name.c_str(), h.value.c_str());
      for ( const auto& h : rep.userHeaders )
	::lib_rtl_output(LIB_RTL_INFO,"%s::handle: %-20s = %s",
			 this->type.c_str(), h.name.c_str(), h.value.c_str());
    }
    return ret;
  }
  return close;
}

/// Namespace for the http server and client apps
namespace http   {
  template class basic_http_server<Online::storage::db>;
}

#include "fdb_sqldb.h"

extern "C" int fdb_sqlite_server(int argc, char** argv)   {
  std::string srv = "/"+RTL::str_upper(RTL::nodeNameShort())+"/"+RTL::processName();
  Online::storage::SrvRun<Online::storage::db> s;
  s.create(argc, argv);
  auto* sql = new Online::storage::fdb_dbase_t(s.server, s.prefix);
  sql->_debug = s.application_debug;
  sql->_engine.reset(new Online::storage::sqldb_handler_t(s.dbase, s.ptr->handler().type, s.application_debug));
  s.ptr->implementation->dbase.reset(sql);
  s.ptr->implementation->monitor.reset(new fdb_dbase_monitor_t());
  ::dis_start_serving(srv.c_str());
  s.start();
  return 0;
}
