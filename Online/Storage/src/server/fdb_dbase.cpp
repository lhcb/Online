//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include <Storage/fdb_dbase.h>
#include <RTL/rtl.h>

// C/C++ files

using namespace Online::storage;

namespace {
  template<typename T> inline int _prt(const T* o)  {
    return o->_debug > 1 ? LIB_RTL_ALWAYS : LIB_RTL_DEBUG;
  }
}

/// Initializing constructor
fdb_dbase_t::handler_t::handler_t(const std::string& t, int dbg) : _type(t), _debug(dbg)  {
}

/// Default destructor
fdb_dbase_t::handler_t::~handler_t()  {
}

/// Check the existence of a given object in the database
std::error_code
fdb_dbase_t::handler_t::query(std::string& object,
			      std::string& host,
			      std::string& date,
			      std::size_t& length)    {
  file_t file;
  std::error_code ec = this->query_file(object, file, handler_t::STATE_WRITTEN);
  if ( ec.value() == 0 )   {
    object = file.name;
    host   = file.host;
    length = file.size;
    date   = file.date;
    ::lib_rtl_output(_prt(this), "+++ %s: Query  '%s'", _type.c_str(), object.c_str());
    return ec;
  }
  ::lib_rtl_output(LIB_RTL_ERROR,"+++ %s: Query: file '%s' NOT FOUND!", _type.c_str(), object.c_str());
  return std::make_error_code(std::errc::no_such_file_or_directory);
}

/// Check the existence of a given object in the database
std::error_code
fdb_dbase_t::handler_t::next( std::string& object,
			      std::string& host,
			      std::string& date,
			      std::size_t& length)     {
  file_t file;
  std::error_code ec = this->query_file(object, file, handler_t::STATE_WRITTEN);
  if ( ec.value() == 0 )   {
    object = file.name;
    host   = file.host;
    length = file.size;
    date   = file.date;
    ec = this->set(object, handler_t::STATE_READ);
    if ( ec.value() == 0 )   {
      ::lib_rtl_output(_prt(this), "+++ %s: Next   '%s'", _type.c_str(), object.c_str());
      return ec;
    }
    return std::make_error_code(std::errc::protocol_error);
  }
  ::lib_rtl_output(LIB_RTL_ERROR,"+++ %s: Query: file '%s' NOT FOUND!", _type.c_str(), object.c_str());
  return std::make_error_code(std::errc::no_such_file_or_directory);
}

/// Standard constructor
fdb_dbase_t::fdb_dbase_t(const std::string& server, const std::string& location_prefix)
  : _server(server), _location_prefix(location_prefix)  {
}

/// Standard destructor
fdb_dbase_t::~fdb_dbase_t()    {
  _engine.reset();
  _lock.unlock();
}

/// Access the node specification of the next free node
std::string fdb_dbase_t::get_free_host_uri(const std::string& )  const {
  return "http://" + _server;
}

/// Transform the object name to the object key for the database lookup
std::string fdb_dbase_t::object_key(const std::string& obj)  const {
  return obj.substr(_location_prefix.length(), std::string::npos);
}

/// Transform the object key and the host specs to the network name of the file
std::string fdb_dbase_t::network_file(const std::string& host, const std::string& obj)  const  {
  return host + (obj[0] == '/' ? obj : '/'+obj);
}

/// Check the existence of a given object in the database
std::error_code fdb_dbase_t::query_object(const std::string& object,
					  std::string&       access,
					  std::string&       date,
					  std::size_t&       length,
					  bool               direct)
{
  std::string host, obj = direct ? object : this->object_key(object);
  auto ec = this->_engine->query(obj, host, date, length);
  if ( ec.value() == 0 )   {
    access  = direct ? obj : this->network_file(host, obj);
    ::lib_rtl_output(_prt(this), "+++ %s: Lookup '%s'", _engine->_type.c_str(), obj.c_str());
    return ec;
  }
  ::lib_rtl_output(LIB_RTL_ERROR,"+++ %s: FAILED lookup: '%s'  [%s]",
		   _engine->_type.c_str(), object.c_str(), ec.message().c_str());
  return ec;
}

/// Check the existence of the next object of a sequence
std::error_code fdb_dbase_t::delete_next( const std::string& object,
					  std::string&       access,
					  std::string&       date,
					  std::size_t&       length,
					  bool               direct)
{
  std::string host, obj = direct ? object : this->object_key(object);
  auto ec = this->_engine->next(obj, host, date, length);
  if ( ec.value() == 0 )   {
    ec     = this->_engine->del(obj);
    access = direct ? obj : this->network_file(host, obj);
    if ( ec.value() == 0 )   {
      ::lib_rtl_output(_prt(this), "+++ %s: Lookup '%s'", _engine->_type.c_str(), obj.c_str());
      return ec;
    }
  }
  ::lib_rtl_output(LIB_RTL_ERROR,"+++ %s: FAILED sequence lookup: '%s' [%s]",
		   _engine->_type.c_str(), object.c_str(), ec.message().c_str());
  return ec;
}

/// Check the existence of a given object in the database
std::error_code fdb_dbase_t::delete_object(const std::string& object,
					   std::string&       access,
					   std::string&       date,
					   std::size_t&       length,
					   bool               direct)
{
  std::string host, obj = direct ? object : this->object_key(object);
  auto ec = this->_engine->query(obj, host, date, length);
  if ( !ec )   {
    ec     = this->_engine->del(obj);
    access = direct ? obj : this->network_file(host, obj);
    if ( !ec )   {
      ::lib_rtl_output(_prt(this), "+++ %s: Remove '%s'", _engine->_type.c_str(), obj.c_str());
      return ec;
    }
  }
  ::lib_rtl_output(_prt(this), "+++ %s: FAILED remove: '%s' [%s]",
		   _engine->_type.c_str(), object.c_str(), ec.message().c_str());
  return ec;
}

/// Add a new object to the database
std::error_code
fdb_dbase_t::add_object(const std::string& object,
			const std::string& date,
			std::size_t        length,
			std::string&       access,
			bool               direct)
{
  std::string obj  = direct ? object        : this->object_key(object);
  std::string host = direct ? std::string() : this->get_free_host_uri(object);
  auto ec = this->_engine->add(obj, date, length, host);
  if ( !ec )   {
    access = direct ? obj : this->network_file(host, obj);
    ::lib_rtl_output(_prt(this), "+++ %s: Add    '%s' %s %s",
		     _engine->_type.c_str(), object.c_str(),
		     this->_debug > 1 ? "access:" : "",
		     this->_debug > 1 ? access.c_str() : "");
    return direct ? this->_engine->set(obj, handler_t::STATE_WRITTEN) : ec;
  }
  ::lib_rtl_output(LIB_RTL_ERROR,"+++ %s: FAILED add: '%s' access: %s [%s]",
		   _engine->_type.c_str(), object.c_str(), access.c_str(), ec.message().c_str());
  return ec;
}

/// Update the object state of an object in the database
std::error_code
fdb_dbase_t::update_object_state(const std::string& object, const std::string& state)  {
  file_t file;
  std::error_code ec;
  std::string obj = this->object_key(object);
  if ( state == "WRITTEN" )   {
    ec = this->_engine->query_file(obj, file, handler_t::STATE_OPEN);
    if ( !ec )   {
      ec = this->_engine->set(obj, handler_t::STATE_WRITTEN);
    }
  }
  else if ( state == "READ" )   {
    ec = this->_engine->query_file(obj, file, handler_t::STATE_WRITTEN);
    if ( !ec )   {
      ec = this->_engine->set(obj, handler_t::STATE_READ);
    }
  }
  if ( !ec )   {
    ::lib_rtl_output(this->_debug > 1 ? LIB_RTL_ALWAYS : LIB_RTL_DEBUG,
		     "+++ %s: Update '%s' state: %s",
		     _engine->_type.c_str(), object.c_str(), state.c_str());
    return ec;
  }
  ::lib_rtl_output(LIB_RTL_ERROR,"+++ %s: FAILED update '%s' state: %s [%s]",
		   _engine->_type.c_str(), object.c_str(), state.c_str(), ec.message().c_str());
  return ec;
}
