//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include <HTTP/HttpReply.h>
#include <Storage/client.h>
#include <Storage/fdb_server.h>
#include <filesystem>
#include <RTL/strdef.h>
#include <dim/dis.h>

// C/C++ include files
#include <cstring>
#include <unistd.h>

// Put private stuff in anonymous namespace
namespace {

  /// Database object to check fs
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.02.2021
   */
  struct fs_db   {
    std::string server;
    std::string file_dir;
    
  public:
    /// Standard constructor
    fs_db(const std::string& srv, const std::string& dir);
    std::string local_file(const std::string& object_name)  const
    { return file_dir + object_name;   }
  };

  /// Monitor object to publish values
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.02.2021
   */
  struct fs_monitor   {
  public:
    typedef http::basic_http_server<Online::storage::fs>::handler_t handler_t;
    struct mon_data   {
      typedef std::pair<long, std::atomic<long>* > counter_t;
      int  numGET             {0};
      int  numPUT             {0};
      int  numDEL             {0};
      int  numGET_not_found   {0};
      int  numDEL_not_found   {0};
      int  numPUT_error       {0};
      long bytesGET           {0};
      long bytesPUT           {0};
      long bytesDEL           {0};
      counter_t num_connections         {0,nullptr};
      counter_t num_connections_opened  {0,nullptr};
      counter_t num_connections_closed  {0,nullptr};
      counter_t num_connections_stopped {0,nullptr};
    } data;
    
    handler_t&        handler;
    std::vector<int>  services;
    
    void add(int svc)  {  services.emplace_back(svc); }
  public:
    /// Standard constructor
    fs_monitor(handler_t& hdlr);
    /// Default destructor
    ~fs_monitor();
  };
  
  /// File I/O object
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.02.2021
   */
  class fs_io : public Online::storage::reply_t::Context  {
  public:
  static  constexpr long data_chunk_size {1024*1024};
    size_t st_data  {0};
    size_t st_size  {0};
    int    fid     {-1};
  public:
    /// 
    fs_io() = default;
    virtual ~fs_io()    {
      if ( fid >= 0 )    {
	::close(fid);
	fid = -1;
      }
    }
    /// Close file
    void close()   {
      if ( fid >= 0 )    {
	::close(fid);
	fid = -1;
      }
    }
    /// 
    std::error_code open_read (const std::string& file_name)   {
      std::error_code ec;
      fid = ::open(file_name.c_str(), O_LARGEFILE | O_RDONLY, S_IREAD);
      if ( -1 == fid )    {
	ec = std::error_code(errno, std::system_category());
      }
      return ec;
    }
    /// 
    std::error_code open_write(const std::string& file_name)   {
      std::error_code ec;
      fid = ::open(file_name.c_str(),
		   O_LARGEFILE | O_WRONLY | O_CREAT | O_EXCL | O_EXCL,
		   S_IRWXG | S_IRWXU);
      if ( -1 == fid )    {
	ec = std::error_code(errno, std::system_category());
      }
      return ec;
    }
    /// 
    long read(long len, unsigned char* buffer)   {
      unsigned char* p = (unsigned char*)buffer;
      long tmp = 0;
      while ( tmp < len )   {
	int sc = ::read(fid, p + tmp, len - tmp);
	if (sc > 0)
	  tmp += sc;
	else if (sc == 0)
	  return 0;
	else return -1;
      }
      return len;
    }
    /// 
    long write(long len, const unsigned char* buffer)   {
      const unsigned char* p = (const unsigned char*)buffer;
      long tmp = len;
      while (tmp > 0)  {
	long sc = ::write(fid, p + len - tmp, tmp);
	if (sc > 0)
	  tmp -= sc;
	else return 0;
      }
      return len;
    }
    ///
    std::error_code unlink(const std::string& name)    {
      if ( 0 == ::unlink(name.c_str()) )
	return std::error_code(0, std::system_category());
      return std::error_code(errno, std::system_category());
    }
  };

  /// Feed data to DIS when updating state
  void counter_handler(void* tag, void** buff, int* size, int* /* first */)   {
    typedef std::pair<long, std::atomic<long>* > counter_t;
    static long defaults = 0;
    counter_t* h = *(counter_t**)tag;
    if ( h )  {
      h->first = h->second->load(std::memory_order_relaxed);
      *buff = &h->first;
      *size = sizeof(h->first);
      return;
    }
    *buff = &defaults;
    *size = sizeof(defaults);
  }
}

class Online::storage::fs::traits {
public:
  typedef fs_db      dbase_t;
  typedef fs_monitor monitor_t;
};

using namespace Online::storage;

/// Standard constructor
fs_db::fs_db(const std::string& srv, const std::string& dir)
  : server(srv), file_dir(dir)
{
  if ( !file_dir.empty() && file_dir[0] != '/' )  {
    std::filesystem::path p = std::filesystem::absolute(std::filesystem::path(dir));
    file_dir = p.string();
  }
}

/// Standard constructor
fs_monitor::fs_monitor(handler_t& hdlr) : handler(hdlr)
{
  std::string nam = "/"+RTL::str_upper(RTL::nodeNameShort())+"/"+RTL::processName();
  data.num_connections.second         = &handler.num_connections;
  data.num_connections_opened.second  = &handler.num_connections_opened;
  data.num_connections_closed.second  = &handler.num_connections_closed;
  data.num_connections_stopped.second = &handler.num_connections_stopped;

  add( ::dis_add_service((nam+"/Connections_active").c_str(), "L", 0, 0,
			 counter_handler, (long)&data.num_connections) );
  add( ::dis_add_service((nam+"/Connections_opened").c_str(), "L", 0, 0,
			 counter_handler, (long)&data.num_connections_opened) );
  add( ::dis_add_service((nam+"/Connections_closed").c_str(), "L", 0, 0,
			 counter_handler, (long)&data.num_connections_closed) );
  add( ::dis_add_service((nam+"/Connections_stopped").c_str(), "L", 0, 0,
			 counter_handler, (long)&data.num_connections_stopped) );
  add( ::dis_add_service((nam+"/DATA").c_str(),"C",&data,sizeof(data),0,0) );
  add( ::dis_add_service((nam+"/GET").c_str(),"L",&data.numGET,sizeof(data.numGET), 0, 0) );
  add( ::dis_add_service((nam+"/PUT").c_str(),"L",&data.numPUT,sizeof(data.numPUT), 0, 0) );
  add( ::dis_add_service((nam+"/DEL").c_str(),"L",&data.numDEL,sizeof(data.numDEL), 0, 0) );
  add( ::dis_add_service((nam+"/GET_bytes").c_str(),"L",&data.bytesGET,sizeof(data.bytesGET),0,0) );
  add( ::dis_add_service((nam+"/PUT_bytes").c_str(),"L",&data.bytesPUT,sizeof(data.bytesPUT),0,0) );
  add( ::dis_add_service((nam+"/DEL_bytes").c_str(),"L",&data.bytesDEL,sizeof(data.bytesDEL),0,0) );
  add( ::dis_add_service((nam+"/PUT_error").c_str(),"L",
			 &data.numPUT_error,sizeof(data.numPUT_error),0,0) );
  add( ::dis_add_service((nam+"/GET_not_found").c_str(),"L",
			 &data.numGET_not_found,sizeof(data.numGET_not_found),0,0) );
  add( ::dis_add_service((nam+"/DEL_not_found").c_str(),"L",
			 &data.numDEL_not_found,sizeof(data.numDEL_not_found),0,0) );
}

/// Default destructor
fs_monitor::~fs_monitor()  {
  for( auto svc : services ) ::dis_remove_service(svc);
}

/// Standard constructor
template <> http::basic_http_server<fs>::handler_t::handler_t()   {
  this->type = "Storage File Server";
  ::lib_rtl_output(LIB_RTL_ALWAYS,"+++ %s: starting.....\n", this->type.c_str());
}

/// Standard destructor
template <> http::basic_http_server<fs>::handler_t::~handler_t()   {
}

/// Callback to eventually handle actions on connection shutdown
template <> void
http::basic_http_server<fs>::handler_t::handle_shutdown(const request_t& req, const reply_t& rep)   {
  if ( req.uri == "/control=exit" && rep.status == reply_t::ok )    {
    this->manager.stop_all();
  }
}

/// Specific handler for GET requests
template <> http::HttpRequestHandler::continue_action
http::basic_http_server<fs>::handler_t::handle_get(const request_t& req, reply_t& rep)   {
  if ( this->handle_control(req, rep) )    {
    if ( req.uri == "/control=exit" || req.uri == "/control=shutdown" )    {
      ::kill(::lib_rtl_pid(), SIGTERM);
    }
    return write;
  }
  auto* ctxt = (fs_io*)rep.context.get();
  if ( 0 == rep.bytes_sent )    {
    struct stat stat;
    const  std::string fname = dbase->local_file(req.uri);

    if ( -1 == ::stat(fname.c_str(), &stat) )   {
      rep = reply_t::stock_reply(reply_t::not_found);
      rep.headers.emplace_back(constants::error_cause,"File '"+fname+"' does not exist");
      ++monitor->data.numGET_not_found;
      return write;
    }
    rep = reply_t::stock_reply(reply_t::ok);
    rep.headers.clear();
    rep.content.clear();
    rep.content.reserve(fs_io::data_chunk_size);
    rep.headers.emplace_back(constants::content_length, stat.st_size);
    rep.headers.emplace_back(constants::content_type,   "rawdata/lhcb");
    rep.headers.emplace_back(constants::date,           header_t::date_time(stat.st_ctime));
    rep.context.reset( ctxt=new fs_io() );
    ctxt->st_size = stat.st_size;
    ctxt->st_data = 0;
    // Now open the file, read a chunk and send it.
    // The rest gets sent by consecutive calls
    std::error_code ec = ctxt->open_read(fname);
    if ( !error_code_ok(ec, this->debug) )   {
      rep = reply_t::stock_reply(reply_t::not_found);
      rep.headers.emplace_back(constants::error_cause,"File '"+fname+"' does not exist");
      ++monitor->data.numGET_not_found;
    }
    return write;
  }
  else if ( rep.status == reply_t::not_found )  {
    // We are here if the open for read failed
    return none;
  }
  else if ( ctxt && ctxt->st_size > ctxt->st_data )   {
    long len = (ctxt->st_size >= fs_io::data_chunk_size+ctxt->st_data)
      ? fs_io::data_chunk_size : ctxt->st_size-ctxt->st_data;
    rep.content.resize(len);
    long rd = ctxt->read(len, &rep.content.at(0));
    if ( rd == len || rd >= 0 )   {
      ctxt->st_data += rd;
    }
    else   {
      std::string err = std::make_error_code(std::errc(errno)).message();
      ::lib_rtl_output(LIB_RTL_ERROR, "+++ %s: %-6s Failed to read '%s' [%s] only got %ld out of %ld bytes",
		       this->type.c_str(), req.method.c_str(), req.uri.c_str(),
		       err.c_str(), rd, len);
    }
    if ( ctxt->st_data == ctxt->st_size )   {
      ::lib_rtl_output(LIB_RTL_INFO, "+++ %s: %-6s Last chunk '%s' %.1f MB",
		       this->type.c_str(), req.method.c_str(), req.uri.c_str(), double(ctxt->st_size)/(1024e0*1024e0));
    }
    return write;
  }
  else if ( ctxt && ctxt->st_size == ctxt->st_data )   {
    std::chrono::time_point<std::chrono::system_clock> null;
    long  net = std::chrono::duration_cast<std::chrono::milliseconds>(req.netio-null).count();
    long  wrt = std::chrono::duration_cast<std::chrono::milliseconds>(req.handling-null).count();
    double mb = double(ctxt->st_size)/(1024e0*1024e0);
    ::lib_rtl_output(LIB_RTL_ALWAYS, "+++ %s: %-6s          %ld bytes netio: %ld ms %.1f MB/s wrt: %ld ms %.1f MB/s",
		     this->type.c_str(), req.method.c_str(), ctxt->st_size,
		     net, mb/std::max(1e-6, double(net)/1e3),
		     wrt, mb/std::max(1e-6, double(wrt)/1e3));
    ++monitor->data.numGET;
    monitor->data.bytesGET += ctxt->st_data;
    return none;
  }
  ::lib_rtl_output(LIB_RTL_ERROR, "+++ %s: %-6s Failed to read '%s' size:%ld / %ld %s [Inconsistemt context]",
		   this->type.c_str(), req.method.c_str(), req.uri.c_str(), rep.bytes_sent, rep.bytes_total,
		   rep.stock_status(rep.status).c_str());
  rep.context.reset();
  return none;
}

/// Specific handler for DELETE requests
template <> http::HttpRequestHandler::continue_action
http::basic_http_server<fs>::handler_t::handle_delete(const request_t& req, reply_t& rep)    {
  auto* ctxt = (fs_io*)rep.context.get();
  std::error_code ec;
  if ( 0 == rep.bytes_sent )    {
    struct stat stat;
    const  std::string fname = dbase->local_file(req.uri);

    if ( -1 == ::stat(fname.c_str(), &stat) )   {
      rep = reply_t::stock_reply(reply_t::not_found);
      rep.headers.emplace_back(constants::error_cause,"File '"+fname+"' does not exist");
      ++monitor->data.numDEL_not_found;
      return write;
    }
    rep = reply_t::stock_reply(reply_t::ok);
    rep.headers.clear();
    rep.content.clear();
    rep.content.reserve(fs_io::data_chunk_size);
    rep.headers.emplace_back(constants::content_length, stat.st_size);
    rep.headers.emplace_back(constants::content_type,   "rawdata/lhcb");
    rep.headers.emplace_back(constants::date,           header_t::date_time(stat.st_ctime));
    rep.headers.emplace_back(constants::location,       req.uri);
    rep.context.reset( ctxt=new fs_io() );
    ctxt->st_size = stat.st_size;
    ctxt->st_data = 0;
    // Now open the file, read a chunk and send it.
    // The rest gets sent by consecutive calls
    
    ec = ctxt->open_read(fname);
    if ( !error_code_ok(ec, this->debug) )   {
      rep = reply_t::stock_reply(reply_t::not_found);
      rep.headers.emplace_back(constants::error_cause,"File '"+fname+"' does not exist");
      ++monitor->data.numDEL_not_found;
    }
    return write;
  }
  else if ( rep.status == reply_t::not_found )  {
    // We are here if the open for read failed
    return none;
  }
  else if ( ctxt && ctxt->st_size > ctxt->st_data )   {
    long len = (ctxt->st_size >= fs_io::data_chunk_size+ctxt->st_data)
      ? fs_io::data_chunk_size : ctxt->st_size-ctxt->st_data;
    rep.content.resize(len);
    long rd = ctxt->read(len, &rep.content.at(0));
    if ( rd == len || rd >= 0 )   {
      ctxt->st_data += rd;
    }
    else   {
      std::string err = std::make_error_code(std::errc(errno)).message();
      ::lib_rtl_output(LIB_RTL_ERROR, "+++ %s: %-6s Failed to read '%s' [%s] only got %ld out of %ld bytes",
		       this->type.c_str(), req.method.c_str(), req.uri.c_str(), err.c_str(), rd, len);
    }
    if ( ctxt->st_data == ctxt->st_size )   {
      const  std::string fname = dbase->local_file(req.uri);
      ctxt->close();
      ec = ctxt->unlink(fname.c_str());
      if ( !error_code_ok(ec, this->debug) )   {
      }
      ::lib_rtl_output(LIB_RTL_INFO, "+++ %s: %-6s Last chunk '%s' %.1f MB",
		       this->type.c_str(), req.method.c_str(), req.uri.c_str(), double(ctxt->st_size)/(1024e0*1024e0));
    }
    return write;
  }
  else if ( ctxt && ctxt->st_size == ctxt->st_data )   {
    std::chrono::time_point<std::chrono::system_clock> null;
    long  net = std::chrono::duration_cast<std::chrono::milliseconds>(req.netio-null).count();
    long  wrt = std::chrono::duration_cast<std::chrono::milliseconds>(req.handling-null).count();
    double mb = double(ctxt->st_size)/(1024e0*1024e0);
    ::lib_rtl_output(LIB_RTL_ALWAYS, "+++ %s: %-6s          %ld bytes netio: %ld ms %.1f MB/s wrt: %ld ms %.1f MB/s",
		     this->type.c_str(), req.method.c_str(), ctxt->st_size,
		     net, mb/std::max(1e-6, double(net)/1e3),
		     wrt, mb/std::max(1e-6, double(wrt)/1e3));
    ++monitor->data.numDEL;
    monitor->data.bytesDEL += ctxt->st_data;
    return close;
  }
  ::lib_rtl_output(LIB_RTL_ERROR, "+++ %s: %-6s Failed to read '%s' size:%ld / %ld %s [Inconsistemt context]",
		   this->type.c_str(), req.method.c_str(), req.uri.c_str(), rep.bytes_sent, rep.bytes_total,
		   rep.stock_status(rep.status).c_str());
  rep.context.reset();
  return close;
}

/// Specific handler for PUT requests
template <> http::HttpRequestHandler::continue_action
http::basic_http_server<fs>::handler_t::handle_put(const request_t& req, reply_t& rep)    {
  auto* ctxt = (fs_io*)rep.context.get();
  if ( nullptr == ctxt )    {
    struct stat stat;
    const header_t *hdr_date, *hdr_len;
    const std::string fname = dbase->local_file(req.uri);

    if ( rep.status == reply_t::created )   {
      return none;
    }
    else if ( 0 == ::stat(fname.c_str(), &stat) )   {
      rep = reply_t::stock_reply(reply_t::conflict);
      rep.headers.emplace_back(constants::error_cause, "Target already exists");
      ++monitor->data.numPUT_error;
      ::lib_rtl_output(LIB_RTL_INFO, "+++ %s: %-6s FAILED [Target already exists] %s",
		       this->type.c_str(), req.method.c_str(), fname.c_str());
      return write;
    }
    else if ( (hdr_date=req.header(constants::date)) == nullptr )   {
      rep = reply_t::stock_reply(reply_t::bad_request);
      rep.headers.emplace_back(constants::error_cause,"Missing date header [Protocol Violation]");
      ++monitor->data.numPUT_error;
      return write;
    }
    else if ( (hdr_len=req.header(constants::content_length)) == nullptr )   {
      rep = reply_t::stock_reply(reply_t::bad_request);
      rep.headers.emplace_back(constants::error_cause,"Missing content length header [Protocol Violation]");
      ++monitor->data.numPUT_error;
      return write;
    }
    std::error_code ec;
    std::filesystem::path p = std::filesystem::path(fname).parent_path();
    p = std::filesystem::absolute(p);
    if ( !std::filesystem::exists(p, ec) )  {
      std::filesystem::create_directories(p, ec);
      if ( ec )   {
	int err = ec.value();
	::lib_rtl_output(LIB_RTL_ERROR, "+++ %s: %-6s Failed to create parent directory %s [%s]",
			 this->type.c_str(), req.method.c_str(), p.c_str(), ec.message().c_str());
	if ( err == EACCES || err == ENOENT || err == EPERM || err == EROFS )
	  rep = reply_t::stock_reply(reply_t::unauthorized);
	else
	  rep = reply_t::stock_reply(reply_t::bad_request);
	rep.headers.emplace_back(constants::error_cause,err);
	++monitor->data.numPUT_error;
	return write;
      }
      ::lib_rtl_output(LIB_RTL_INFO, "+++ %s: %-6s Created parent directory %s",
		       this->type.c_str(), req.method.c_str(), p.c_str());
    }
    
    rep = reply_t::stock_reply(reply_t::continuing);
    rep.context.reset(ctxt=new fs_io());
    ctxt->st_size = hdr_len->as<size_t>();
    ctxt->st_data = 0;
    // Now open the file, read a chunk and send it.
    // The rest gets sent by handle_request_cont_full
    ec = ctxt->open_write(fname);
    if ( !error_code_ok(ec, this->debug) )   {
      rep = reply_t::stock_reply(reply_t::forbidden);
      rep.headers.emplace_back(constants::error_cause, "Failed to open file "+fname+" ["+ec.message()+"]");
      ++monitor->data.numPUT_error;
    }
    else  {
      ::lib_rtl_output(LIB_RTL_DEBUG, "+++ %s: %-6s Opened file: '%s'",
		       this->type.c_str(), req.method.c_str(), fname.c_str());
    }
    if ( req.content.empty() )   {
      return write;
    }
    // This should not happen: The client must first receive the continue!
    ::lib_rtl_output(LIB_RTL_ERROR, "+++ %s: %-6s Received non-empty data on request to write "
		     "'%s' [Protocol Violation]", this->type.c_str(), req.method.c_str(), fname.c_str());
    return write;
  }
  else if ( ctxt && ctxt->st_data < ctxt->st_size && req.content.empty() )   {
    return read;
  }
  else if ( ctxt && ctxt->st_data < ctxt->st_size )   {
    long len = req.content.size();
    if ( len > 0 )   {
      long wrt = ctxt->write(len, &req.content.at(0));
      ctxt->st_data += wrt;
      if ( wrt != len )   {
	std::string err = std::make_error_code(std::errc(errno)).message();
	::lib_rtl_output(LIB_RTL_ERROR, "+++ %s: %-6s Failed to write '%s' [%s] only wrote %ld out of %ld bytes",
			 this->type.c_str(), req.method.c_str(),
			 req.uri.c_str(), err.c_str(), wrt, len);
	++monitor->data.numPUT_error;
      }
    }
    return read;
  }
  else if ( ctxt && ctxt->st_data >= ctxt->st_size && rep.status != reply_t::created )  {
    std::chrono::time_point<std::chrono::system_clock> null;
    long  net = std::chrono::duration_cast<std::chrono::milliseconds>(req.netio-null).count();
    long  wrt = std::chrono::duration_cast<std::chrono::milliseconds>(req.handling-null).count();
    double mb = double(ctxt->st_size)/(1024e0*1024e0);
    ::lib_rtl_output(LIB_RTL_ALWAYS, "+++ %s: %-6s Wrote '%s' %.0f %cB",
		     this->type.c_str(), req.method.c_str(), req.uri.c_str(),
		     mb > 10e0 ? mb : mb*1024e0, mb > 10e0 ? 'M' : 'k');
    ::lib_rtl_output(LIB_RTL_ALWAYS, "+++ %s: %-6s        %ld bytes netio: %ld ms %.1f MB/s wrt: %ld ms %.1f MB/s",
		     this->type.c_str(), req.method.c_str(), ctxt->st_size,
		     net, mb/std::max(1e-6, double(net)/1e3),
		     wrt, mb/std::max(1e-6, double(wrt)/1e3));
    ++monitor->data.numPUT;
    monitor->data.bytesPUT += ctxt->st_size;

    rep.context.reset();
    rep = reply_t::stock_reply(reply_t::created);
    rep.headers.clear();
    rep.content.clear();
    rep.headers.emplace_back(constants::content_length, 0);
    rep.headers.emplace_back(constants::content_location, req.uri);

    uri_t u(dbase->server);
    std::unique_ptr<client> cl(client::create<client::sync>(u.host, u.port, 10000, this->debug));
    if ( error_code_ok(cl->open(), this->debug) )   {
      client::reqheaders_t hdrs  {
	{ http::constants::state, "WRITTEN" }
      };
      auto object = "/objects" + req.uri;
      auto reply = cl->update(object, nullptr, 0, hdrs);
      if ( reply.status == reply_t::ok )   {
	return write;
      }
    }
    ++monitor->data.numPUT_error;
    ::lib_rtl_output(LIB_RTL_ERROR, "+++ %s: %-6s FAILED to update '%s' to state WRITTEN!",
		     this->type.c_str(), req.method.c_str(), req.uri.c_str());
    return write;
  }
  ::lib_rtl_output(LIB_RTL_ERROR, "+++ %s: %-6s Finished request '%s'  %ld bytes",
		   this->type.c_str(), req.method.c_str(), req.uri.c_str(), ctxt ? ctxt->st_size : 0);
  /// The request is handled. Trigger the shutdown of the connection
  return none;
}

/// Handle a request and produce a reply.
template <> http::HttpRequestHandler::continue_action
http::basic_http_server<fs>::handler_t::handle_request(const request_t& req,reply_t& rep)  {
  auto ret = this->HttpRequestHandler::handle_request(req, rep);
  if ( rep.bytes_sent == rep.bytes_total )    {
    if ( this->debug > 0 )   {
      ::lib_rtl_output(LIB_RTL_INFO, "%s::handle: Request %s %s status: %d",
		       this->type.c_str(), req.method.c_str(), req.uri.c_str(), rep.status);
      for ( const auto& h : rep.headers )
	::lib_rtl_output(LIB_RTL_INFO, "%s::handle: %-20s = %s",
			 this->type.c_str(), h.name.c_str(), h.value.c_str());
      for ( const auto& h : rep.userHeaders )
	::lib_rtl_output(LIB_RTL_INFO, "%s::handle: %-20s = %s",
			 this->type.c_str(), h.name.c_str(), h.value.c_str());
    }
  }
  return ret;
}

/// Namespace for the http server and client apps
namespace http   {
  template class basic_http_server<Online::storage::fs>;
}

extern "C" int fdb_fs_file_server(int argc, char** argv)   {
  std::string srv = RTL::processName();
  SrvRun<fs> s;
  s.create(argc, argv);  
  s.ptr->implementation->dbase.reset(new fs_db(s.server, s.file_dir));
  s.ptr->implementation->monitor.reset(new fs_monitor(*s.ptr->implementation));
  ::dis_start_serving(srv.c_str());
  s.start();
  return 0;
}
