//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================

// Framework include files
#include "fdb_sqldb.h"
#include <sqldb/sqlite.h>
#include <RTL/strdef.h>
#include <RTL/rtl.h>

// C/C++ files
#include <system_error>

/// Initializing constructor
Online::storage::sqldb_handler_t::sqldb_handler_t(const std::string& p, const std::string& t, int dbg)
  : fdb_dbase_t::handler_t(t, dbg), path(p)
{
  this->init(path);
}

/// Default destructor
Online::storage::sqldb_handler_t::~sqldb_handler_t()   {
  this->fini();
  if ( this->database.is_valid() )   {
    this->database.commit();
    this->database.close();
  }
}

/// Initialize object
std::error_code
Online::storage::sqldb_handler_t::init(const std::string& db_name)   {
  if ( !this->_inited )   {
    std::string err;
    const char* nam = db_name.c_str();
    this->database  = sqldb::database::open<sqlite>(db_name, err);
    if ( this->database.is_valid() )   {
      const char* sql = "CREATE TABLE Files ("
	"Name   TEXT PRIMARY KEY, "
	"State  INT  NOT NULL, "
	"Size   INT  NOT NULL, "
	"Date   TEXT NOT NULL, "
	"Host   TEXT NOT NULL)";
      ::lib_rtl_output(LIB_RTL_INFO,"+++ %s: sqlite-db: %s", _type.c_str(), nam);
      if ( this->database.execute(err, sql) == sqldb::OK )  {
	::lib_rtl_output(LIB_RTL_INFO,"+++ %s: sqlite-db: Table FILES created successfully", _type.c_str());
      }
      else   {
	const char* sql_table = "CREATE TABLE IF NOT EXISTS Files ("
	  "Name   TEXT PRIMARY KEY, "
	  "State  INT  NOT NULL, "
	  "Size   INT  NOT NULL, "
	  "Date   TEXT NOT NULL, "
	  "Host   TEXT NOT NULL)";
	if ( this->database.execute(err, sql_table) != sqldb::OK )  {
	  ::lib_rtl_output(LIB_RTL_ERROR,"%s: Create table: SQL error: %s", nam, err.c_str());
	  return std::make_error_code(std::errc::permission_denied);
	}
	::lib_rtl_output(LIB_RTL_INFO,"+++ %s: sqlite-db: Successfully linked table FILES", _type.c_str());
      }
      /// Create prepared statement to insert records
      this->insert_record.prepare(database,
				  "INSERT INTO Files (Name, State, Size, Date, Host) "
				  "VALUES (?, ?, ?, ?, ?)");
      this->delete_record.prepare(database,
				  "DELETE FROM Files WHERE Name=?1 ");
      this->query_record.prepare (database,
				  "SELECT Name, State, Size, Date, Host FROM Files "
				  "WHERE Name LIKE ?1 AND State = ?2 ");
      this->lock_record.prepare  (database,
				  "UPDATE Files SET State=?1 WHERE Name=?2 ");
      this->_inited = true;
      return std::make_error_code(std::errc(0));
    }
    ::lib_rtl_output(LIB_RTL_INFO,"+++ %s: %s: FAILED to access db: [%s]",
		     _type.c_str(), nam, err.c_str());
    return std::make_error_code(std::errc::permission_denied);
  }
  return std::make_error_code(std::errc(0));
}

/// Finalize object
void Online::storage::sqldb_handler_t::fini()   {
  if ( this->_inited )   {
    this->insert_record.finalize();
    this->delete_record.finalize();
    this->query_record.finalize();
    this->lock_record.finalize();
    this->database.close();
    this->_inited = false;
  }
}
 
/// Check the existence of a given object in the database
std::error_code
Online::storage::sqldb_handler_t::query_file(const std::string& object_name,
					     file_t&            file,
					     int                state)
{
  this->query_record.reset();
  this->query_record.bind(0, object_name);
  this->query_record.bind(1, state);
  int ret = this->query_record.execute();
  while ( (ret = query_record.fetch_one()) == sqldb::OK )   {
    file.name  = query_record.get<std::string>(0);
    file.state = query_record.get<int>(1);
    file.size  = query_record.get<int64_t>(2);
    file.date  = query_record.get<std::string>(3);
    file.host  = query_record.get<std::string>(4);
    this->query_record.reset();
    return std::make_error_code(std::errc(0));
  }
  return std::make_error_code(std::errc::no_such_file_or_directory);
}

/// Add a new object to the database
std::error_code
Online::storage::sqldb_handler_t::add  (const std::string& object_name,
					const std::string& date,
					std::size_t        length,
					const std::string& host)    {
  this->query_record.reset();
  this->insert_record.bind(0, object_name);
  this->insert_record.bind(1, STATE_OPEN);
  this->insert_record.bind(2, length);
  this->insert_record.bind(3, date);
  this->insert_record.bind(4, host);
  this->database.begin();
  int ret = this->insert_record.execute();
  this->database.commit();
  this->insert_record.reset();
  if ( ret == sqldb::DONE )   {
    return std::make_error_code(std::errc(0));
  }
  else if ( ret != sqldb::OK )    {
    return std::make_error_code(std::errc::file_exists);    
  }
  return std::make_error_code(std::errc(0));
}

/// Remove an object from the database
std::error_code
Online::storage::sqldb_handler_t::del  (const std::string& object_name)    {
  this->delete_record.bind(0, object_name);
  this->database.begin();
  int ret = this->delete_record.execute();
  this->delete_record.reset();
  this->database.commit();
  if ( ret == sqldb::OK )
    return std::make_error_code(std::errc(0));
  else if ( ret == sqldb::DONE )
    return std::make_error_code(std::errc(0));
  return std::make_error_code(std::errc::no_such_file_or_directory);
}

/// lock database record
std::error_code
Online::storage::sqldb_handler_t::set (const std::string& object_name, int value)     {
  this->lock_record.bind(0, value);
  this->lock_record.bind(1, object_name);
  this->database.begin();
  int ret = this->lock_record.execute();
  this->lock_record.reset();
  this->database.commit();
  if ( ret == sqldb::DONE )
    return std::make_error_code(std::errc(0));
  else if ( ret == sqldb::OK )
    return std::make_error_code(std::errc(0));
  return std::make_error_code(std::errc::no_such_file_or_directory);
}
