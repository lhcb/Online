//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================
#ifndef ONLINE_STORAGE_FDB_SQLDB_H
#define ONLINE_STORAGE_FDB_SQLDB_H

// Framework include files
#include <Storage/fdb_dbase.h>
#include <sqldb/sqldb.h>

// C/C++ include files
#include <system_error>

/// Online Namespace
namespace Online   {

  /// Namespace for the storage interface
  namespace storage   {
    
    /// Interface class to database handler
    /**
     *  To implement a new database technology in principle only
     *  this class needs to be re-implemented e.g. using MYSQL.
     *
     *  \author  M.Frank
     *  \version 1.0
     *  \date    02.02.2021
     */
    class sqldb_handler_t : public fdb_dbase_t::handler_t  {
    public:
      std::string      path;
      sqldb::database  database;
      sqldb::statement insert_record;
      sqldb::statement query_record;
      sqldb::statement delete_record;
      sqldb::statement lock_record;

      /// Check the existence of a given object in the database
      virtual std::error_code
	query_file(const std::string& object_name, file_t& file, int state)  override;
	
    public:

      /// Initializing constructor
      sqldb_handler_t(const std::string& p, const std::string& t, int dbg);

      /// Default destructor
      virtual ~sqldb_handler_t();

      /// Initialize object
      std::error_code init(const std::string& db_name);

      /// Finalize object
      void fini();

      /// Add a new object to the database
      std::error_code add(  const std::string& object,
			    const std::string& date,
			    std::size_t        length,
			    const std::string& host)  override;
	
      /// Remove an object from the database
      std::error_code del(  const std::string& object)  override;

      /// lock database record
      std::error_code set(  const std::string& object, int value)  override;
    };
  }     // End namespace storage
}       // End namespace Online
#endif  // ONLINE_STORAGE_FDB_SQLDB_H
