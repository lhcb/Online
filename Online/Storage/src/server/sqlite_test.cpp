//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================
/*
gentest libStorageServer.so storage_sqlite_create_database -database=sqlite_test.dbase

gentest libStorageServer.so storage_sqlite_populate_database_direct -database=sqlite_test.dbase -stream=HLT1 -run=11223344 -count=100
gentest libStorageServer.so storage_sqlite_populate_database_direct -database=sqlite_test.dbase -stream=HLT1 -run=11223345 -count=100
gentest libStorageServer.so storage_sqlite_populate_database_direct -database=sqlite_test.dbase -stream=HLT1 -run=11223346 -count=100

gentest libStorageServer.so storage_sqlite_update_database_direct   -database=sqlite_test.dbase -stream=HLT1 -run=11223346 -count=100

gentest libStorageServer.so fdb_cli_dumpdb -database=/home/frankm/storage_files.dbase
*/

// Framework inclde files
#include <sqldb/sqlite.h>
#include <HTTP/HttpHeader.h>  
#include <RTL/Logger.h>
#include "fdb_sqldb.h"
#include "TestSetup.h"

// C/C++ include files
#include <sstream>
#include <fstream>
#include <iostream>

//  
//
//==========================================================================
extern "C" int storage_sqlite_create_database(int argc, char** argv)    {
  Online::storage::server::TestSetup setup("storage_sqlite_create_database", argc, argv);
  if ( setup.unlink_db )   {
    ::unlink(setup.database.c_str());
  }
  auto db = setup.db();
  std::string error;
  const char* sql = "CREATE TABLE IF NOT EXISTS Files ("
    "Name   TEXT PRIMARY KEY, "
    "State  INT  NOT NULL, "
    "Size   INT  NOT NULL, "
    "Date   TEXT NOT NULL, "
    "Host   TEXT NOT NULL)";
  if ( db.execute(error, sql) != sqldb::OK )   {
    ::lib_rtl_output(LIB_RTL_ERROR,"%s: failed to execute %s :  %s",
		     db.name(), sql, error.c_str());
    setup.status = 0;
  }
  db.commit();
  db.close();
  return 0;
}
//  
//
//==========================================================================
extern "C" int storage_sqlite_unlink_database(int argc, char** argv)    {
  Online::storage::server::TestSetup setup("storage_sqlite_unlink_database", argc, argv);
  int ret = ::unlink(setup.database.c_str());
  if ( 0 != ret )   {
    ::lib_rtl_output(LIB_RTL_ALWAYS,"Failed to unlink database: %s", setup.database.c_str());
    return EINVAL;
  }
  return 0;
}
//  
//
//==========================================================================
extern "C" int storage_sqlite_populate_database(int argc, char** argv)    {
  Online::storage::server::TestSetup setup("storage_sqlite_populate_database", argc, argv);
  auto db = setup.db();
  try {
    sqldb::statement insert;
    insert.prepare(db,
		   "INSERT INTO Files  ('Name', 'State', 'Size', 'Date', 'Host') "
		   "VALUES ( ? , ? , ? , ? , ? )");
    for(size_t i=0; i<setup.count; ++i)    {
      std::string date   = http::HttpHeader::now();
      std::string host   = "http://127.0.0.1:8100";
      size_t      length = 4*1024;
      char        obj[1024];

      ::snprintf(obj, sizeof(obj), "/objects/%08ld/%s/%010ld_%08ld.raw",
		 setup.run, setup.stream.c_str(), setup.run, i);
      insert.reset();
      insert.bind(1, obj);
      insert.bind(2, 0);
      insert.bind(3, length);
      insert.bind(4, date);
      insert.bind(5, host);
      int ret = insert.execute();
      if ( ret != sqldb::OK )    {
	::lib_rtl_output(LIB_RTL_ERROR,"%s: failed INSERT: %s",
			 db.name(), insert.errmsg().c_str());
	setup.status = 0;
	return EINVAL;
      }
      insert.reset();
    }
    insert.finalize();
    db.commit();
    db.close();
  }
  catch(const std::exception& e)   {
    ::lib_rtl_output(LIB_RTL_ERROR,"%s: exception: %s",db.name(),e.what());
  }
  return 0;
}
//  
//
//==========================================================================
extern "C" int storage_sqlite_populate_database_direct(int argc, char** argv)    {
  Online::storage::server::TestSetup setup("storage_sqlite_populate_database", argc, argv);
  auto db = setup.db();
  try {
    for(size_t i=0; i<setup.count; ++i)    {
      std::string error, date = http::HttpHeader::now();
      std::string host = "http://127.0.0.1:8100";
      size_t      length = 4*1024;
      char        obj[1024];

      ::snprintf(obj, sizeof(obj), "/objects/%08ld/%s/%010ld_%08ld.raw",
		 setup.run, setup.stream.c_str(), setup.run, i);
      sqldb::status_t ret =
	db.execute_sql(error,
		       "INSERT INTO Files  ('Name', 'State', 'Size', 'Date', 'Host') "
		       "VALUES ( '%s' , %d , %ld , '%s' , '%s' )",
		       obj, 0, length, date.c_str(), host.c_str());
      if ( ret != sqldb::OK )    {
	::lib_rtl_output(LIB_RTL_ERROR,"%s: failed INSERT: %s",
			 db.name(), db.errmsg().c_str());
	setup.status = 0;
	return EINVAL;
      }
    }
    db.commit();
    db.close();
  }
  catch(const std::exception& e)   {
    ::lib_rtl_output(LIB_RTL_ERROR,"%s: exception: %s", db.name(), e.what());
  }
  return 0;
}
//  
//
//==========================================================================
extern "C" int storage_sqlite_read_database_direct(int argc, char** argv)    {
  typedef Online::storage::fdb_dbase_t::file_t file_t;
  Online::storage::server::TestSetup setup("storage_sqlite_populate_database", argc, argv);
  std::string error;
  auto db = setup.db();
  if ( db.is_valid() )   {
    sqldb::statement query =
      sqldb::statement::create(db, "SELECT Name, State, Size, Date, Host"
			       " FROM Files"
			       " WHERE Size>%d",0);
    if ( query.is_valid() )  {
      sqldb::status_t ret = query.execute();
      std::stringstream s;
      file_t f;
      if ( ret != sqldb::OK )    {
	::lib_rtl_output(LIB_RTL_INFO, "%s: Failed to execute SQL statement: %s",
			 db.name(), db.errmsg().c_str()); 
	setup.status = 0;
	return EINVAL;
      }
      while ( (ret = query.fetch_one()) == sqldb::OK )   {
	f.name  = query.get<std::string>(0);
	f.state = query.get<int>(1);
	f.size  = query.get<int64_t>(2);
	f.date  = query.get<std::string>(3);
	f.host  = query.get<std::string>(4);
	s << f.name << " '" << f.state << "' '" << f.size << "' ";
	if ( setup.have_time ) s << "'" << f.date << "' ";
	s << f.host;
	::lib_rtl_output(LIB_RTL_INFO, "TABLE: %s", s.str().c_str());      
	s.str("");
      }
    }
    db.commit();
    db.close();
  }
  return 0;
}
//  
//
//==========================================================================
extern "C" int storage_sqlite_update_database_direct(int argc, char** argv)    {
  typedef Online::storage::fdb_dbase_t::file_t file_t;
  Online::storage::server::TestSetup setup("storage_sqlite_populate_database", argc, argv);
  auto db = setup.db();
  try {
    std::string error;
    std::stringstream s;
    std::vector<file_t> files;
    sqldb::status_t ret;
    sqldb::statement query =
      sqldb::statement::create(db,
			       "SELECT Name, State, Size, Date, Host"
			       " FROM Files"
			       " WHERE Size>%d",0);
    if ( query.is_valid() )  {
      file_t f;
      ret = query.execute();
      while ( (ret = query.fetch_one()) == sqldb::OK )   {
	f.name  = query.get<std::string>(0);
	f.state = query.get<int>(1);
	f.size  = query.get<int64_t>(2);
	f.date  = query.get<std::string>(3);
	f.host  = query.get<std::string>(4);
	files.push_back(f);
      }
    }
    else  {
      setup.status = 0;
    }
    for(size_t i=0; i<files.size(); ++i)   {
      const file_t& f = files[i];
      ret = sqldb::ERROR;
      if ( f.state == 0 )   {
	ret = db.execute_sql(error,
			     "UPDATE Files"
			     " SET State=1"
			     " WHERE Name='%s'",
			     f.name.c_str());
      }
      else if ( f.state == 1 )   {
	ret = db.execute_sql(error,
			     "UPDATE Files"
			     " SET State=0"
			     " WHERE Name='%s'",
			     f.name.c_str());
      }
      if ( ret != sqldb::OK )  {
	::lib_rtl_output(LIB_RTL_INFO, "SQL Failure: %s", error.c_str());      
	setup.status = 0;
      }
    }
    db.commit();
    db.close();
  }
  catch(const std::exception& e)   {
    ::lib_rtl_output(LIB_RTL_ERROR,"%s: exception: %s", db.name(), e.what());
  }
  return 0;
}
//  
//
//==========================================================================
extern "C" int fdb_cli_dumpdb(int argc, char** argv)    {
  Online::storage::server::TestSetup setup("fdb_cli_dumpdb", argc, argv, [](int,char**)  {
    ::fprintf(stderr,
	      "fdb_cli_dumpdb -opt [-opt]                                       \n"
	      "     -database=<file-path>  Path to the SQLite file to dump.     \n"
	      "     -name=<match>          Print only files matching a pattern. \n"
	      "     -help                  Print this help.                     \n");
  });
  return setup.dump_db();
}
