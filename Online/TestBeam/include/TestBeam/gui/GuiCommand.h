//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : TestBeam-GUI
//
//  Author     : Markus Frank
//==========================================================================
#ifndef TESTBEAM_GUICOMMAND_H
#define TESTBEAM_GUICOMMAND_H

/// C++ include files
#include <string>

/// Namespace for the TestBeam software
namespace testbeam  {

  enum  GuiCommands  {
    GUI_INVOKE_CMD = 1,
    GUI_SHOW_OUTPUTLINE,
    GUI_SHOW_OUTPUTBOTTOM,
    GUI_UPDATE_OUTPUT,
    GUI_EXCEPTION,

    TMSRV_LAUNCH,
    TMSRV_KILL,

    LOGSRV_LAUNCH,
    LOGSRV_KILL,

    LOGVIEWER_LAUNCH,
    LOGVIEWER_KILL,

    CONTROLLER_COMMAND,
    CONTROLLER_KILLALL,

    CONTROLLER_LAUNCH,
    CONTROLLER_KILL,
    CONTROLLER_CREATE,
    CONTROLLER_LOAD,
    CONTROLLER_CONFIG,
    CONTROLLER_INIT,
    CONTROLLER_START,
    CONTROLLER_PAUSE,
    CONTROLLER_CONTINUE,
    CONTROLLER_STOP,
    CONTROLLER_RESET,
    CONTROLLER_UNLOAD,
    CONTROLLER_DESTROY,
    CONTROLLER_RECOVER,


    GEN_START_PROCESS,
    GEN_KILL_PROCESS,

    APPLY_PARAMS,
    GUI_LAST
  };

  /// Helper class to shuffle comamnds
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    18.11.2018
   */
  class GuiCommand  final {
  public:
    std::string dns, node, client, opts;
  public:
  GuiCommand(const std::string& d, const std::string& n, const std::string& c, const std::string& o="") 
    : dns(d), node(n), client(c), opts(o) {}
    /// Default constructor inhibited
    GuiCommand() = default;
    /// Copy constructor inhibited
    GuiCommand(const GuiCommand& gui) = default;
    /// Move constructor inhibited
    GuiCommand(GuiCommand&& gui) = default;    
    /// Default destructor
    ~GuiCommand() = default;

    /// Assignment operator inhibited
    GuiCommand& operator=(const GuiCommand& gui) = default;
    /// Move assignment inhibited
    GuiCommand& operator=(GuiCommand&& gui) = default;
  };
}       // End namespace TestBeam
#endif  /* TESTBEAM_GUICOMMAND_H  */
