//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : TESTBEAM-GUI
//
//  Author     : Markus Frank
//==========================================================================
#ifndef TESTBEAM_INFRASTRUCTUREPANEL_H
#define TESTBEAM_INFRASTRUCTUREPANEL_H

/// Framework include files
#include <TestBeam/gui/TaskManager.h>
#include <TestBeam/gui/TaskHandler.h>
#include <CPP/Interactor.h>
#include <RTL/rtl.h>

/// ROOT include files
#include <TGFrame.h>

// C/C++ include files
#include <memory>
#include <mutex>

class TGGroupFrame;
class TGTextButton;
class TGComboBox;
class TGTextEntry;
class TGLabel;

/// Namespace for the TestBeam software
namespace testbeam  {

  /// Explorer of the readout clients at a given DNS node
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    18.11.2018
   */
  class InfrastructurePanel :
    public TGCompositeFrame,
    public CPP::Interactor  {
  public:
    TGGroupFrame*     group    = nullptr;
    struct LineEntry  {
      TGLabel*          label  = nullptr;
      TGTextEntry*      input  = nullptr;
    };
    struct TaskEntry  {
      TGLabel*          label  = nullptr;
      TGTextEntry*      utgid  = nullptr;
      TGTextEntry*      status = nullptr;
      TGTextButton*     start  = nullptr;
      TGTextButton*     kill   = nullptr;
    };
    CPP::Interactor*    gui = nullptr;
    TaskManager&        taskManager;
    TaskHandler         taskHandler;

    LineEntry           maindns, dns, node, part;
    TaskEntry           tmSrv, dnsSrv, logSrv, tanSrv, logViewer, mbmmon, mbmdmp, storage, did;
    Pixel_t             disabled, enabled, executing, green, red, blue, yellow, white, black;

    void _configTask(TaskEntry& task);

  public:
    enum IDS {
      UPDATE_DEPENDENT_VALUES = 10,
      PART_ID_OFFSET          = 100,
      MAINDNS_LABEL           = PART_ID_OFFSET+1,
      MAINDNS_INPUT           = PART_ID_OFFSET+2,
      DNS_LABEL               = PART_ID_OFFSET+3,
      DNS_INPUT               = PART_ID_OFFSET+4,
      PART_LABEL              = PART_ID_OFFSET+5,
      PART_INPUT              = PART_ID_OFFSET+6,
      NODE_LABEL              = PART_ID_OFFSET+7,
      NODE_INPUT              = PART_ID_OFFSET+8,
      ARCH_LABEL              = PART_ID_OFFSET+11,
      ARCH_INPUT              = PART_ID_OFFSET+12,

      TMSRV_ID_OFFSET         = TaskManager::TMSRV_ID_OFFSET,
      TMSRV_STATUS,
      TMSRV_START,
      TMSRV_KILL,
      TMSRV_LIST,
      TMSRV_LABEL,

      LOGSRV_ID_OFFSET        = TaskManager::LOGSRV_ID_OFFSET,
      LOGSRV_STATUS,
      LOGSRV_START,
      LOGSRV_KILL,
      LOGSRV_LABEL,

      TANSRV_ID_OFFSET        = TaskManager::TANSRV_ID_OFFSET,
      TANSRV_STATUS,
      TANSRV_START,
      TANSRV_KILL,
      TANSRV_LABEL,

      LOGVIEWER_ID_OFFSET     = TaskManager::LOGVIEWER_ID_OFFSET,
      LOGVIEWER_STATUS,
      LOGVIEWER_START,
      LOGVIEWER_KILL,
      LOGVIEWER_LABEL,

      MBMMON_ID_OFFSET        = TaskManager::MBMMON_ID_OFFSET,
      MBMMON_STATUS,
      MBMMON_START,
      MBMMON_KILL,
      MBMMON_LABEL,

      MBMDMP_ID_OFFSET        = TaskManager::MBMDMP_ID_OFFSET,
      MBMDMP_STATUS,
      MBMDMP_START,
      MBMDMP_KILL,
      MBMDMP_LABEL,

      STORAGE_ID_OFFSET       = TaskManager::STORAGE_ID_OFFSET,
      STORAGE_STATUS,
      STORAGE_START,
      STORAGE_KILL,
      STORAGE_LABEL,

      DNSSRV_ID_OFFSET           = TaskManager::DNSSRV_ID_OFFSET,
      DNSSRV_STATUS,
      DNSSRV_START,
      DNSSRV_KILL,
      DNSSRV_LABEL,

      DID_ID_OFFSET           = TaskManager::DID_ID_OFFSET,
      DID_STATUS,
      DID_START,
      DID_KILL,
      DID_LABEL,

      /// END of IOC parameters
      LAST

    };
    /// Standard initializing constructor
    InfrastructurePanel(TGFrame* parent, CPP::Interactor* gui, TaskManager& tm);
    /// Default destructor
    virtual ~InfrastructurePanel();

    void init();
    void updateDependentValues();
    void updateTaskStatus(TaskEntry& entry, const std::string& state);
    /// Interactor interrupt handler callback
    virtual void handle(const CPP::Event& ev)  override;

    /// ROOT class definition
    ClassDefOverride(InfrastructurePanel,0);
  };
}       // End namespace testbeam
#endif  /* TESTBEAM_INFRASTRUCTUREPANEL_H  */
