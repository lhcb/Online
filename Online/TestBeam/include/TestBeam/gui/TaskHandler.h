//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : TESTBEAM-GUI
//
//  Author     : Markus Frank
//==========================================================================
#ifndef TESTBEAM_TASKHANDLER_H
#define TESTBEAM_TASKHANDLER_H

#include <TObject.h>

/// Namespace for the TestBeam software
namespace testbeam  {

  class TaskManager;

  /// Explorer of the readout clients at a given DNS node
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    18.11.2018
   */
  class TaskHandler  {
  public:
    TaskManager& taskManager;
    
  public:
    /// Standard initializing constructor
    TaskHandler(TaskManager& tm);

    /// Default destructor
    virtual ~TaskHandler() = default;

    /// Kill all DAQ processes
    virtual void kill_all_tasks();

    /// Start tmSrv process
    virtual void tmSrv_start();
    /// Kill tmSrv process
    virtual void tmSrv_kill();

    /// Start dns process
    virtual void dnsSrv_start();
    /// Kill dns process
    virtual void dnsSrv_kill();

    /// Start logSrv process
    virtual void logSrv_start();
    /// Kill logSrv process
    virtual void logSrv_kill();

    /// Start controller process
    virtual void controller_start();
    /// Kill controller process
    virtual void controller_kill();

    /// Start logViewer process
    virtual void logViewer_start();
    /// Kill logViewer process
    virtual void logViewer_kill();

    /// Start mbmmon process
    virtual void mbmmon_start();
    /// Kill mbmmon process
    virtual void mbmmon_kill();

    /// Start mbmdmp process
    virtual void mbmdmp_start();
    /// Kill mbmdmp process
    virtual void mbmdmp_kill();

    /// Start storage process
    virtual void storage_start();
    /// Kill storage process
    virtual void storage_kill();

    /// Start tanSrv process
    virtual void tanSrv_start();
    /// Kill tanSrv process
    virtual void tanSrv_kill();

    /// Start local DID process
    virtual void did_start();
    /// Kill did process
    virtual void did_kill();

    /// ROOT class definition
    ClassDef(TaskHandler,0);
  };
}       // End namespace testbeam
#endif  /* TESTBEAM_TASKHANDLER_H  */
