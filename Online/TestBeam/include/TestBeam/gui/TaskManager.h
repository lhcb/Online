//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Author     : Markus Frank
//==========================================================================
//
//      Finite state machine implementation to control
//      and manipulate process groups
//
//==========================================================================
#ifndef ONLINE_TESTBEAM_TASKMANAGER_H
#define ONLINE_TESTBEAM_TASKMANAGER_H

// Framework include files
#include <TestBeam/gui/config.h>
#include <CPP/Interactor.h>
#include <RTL/rtl.h>

// C/C++ include files
#include <vector>
#include <string>
#include <memory>
#include <mutex>

/* 
 *  Testbeam namespace declaration
 */
namespace testbeam {

  /**@class TaskManager  TaskManager.h Ctrl/TaskManager.h
   *
   * @author  M.Frank
   * @date    01/03/2013
   * @version 0.1
   */
  class TaskManager : public CPP::Interactor   {
  public:
    enum IDS {
      ID_DIFF                 = 200,
      TMSRV_ID_OFFSET         = 2 * ID_DIFF,
      TMSRV_STATUS,
      TMSRV_START,
      TMSRV_KILL,
      TMSRV_LIST,
      TMSRV_KILLALL,

      LOGSRV_ID_OFFSET        = TMSRV_ID_OFFSET     + ID_DIFF,
      LOGSRV_STATUS,
      LOGSRV_START,
      LOGSRV_KILL,

      LOGVIEWER_ID_OFFSET     = LOGSRV_ID_OFFSET    + ID_DIFF,
      LOGVIEWER_STATUS,
      LOGVIEWER_START,
      LOGVIEWER_KILL,

      MBMMON_ID_OFFSET        = LOGVIEWER_ID_OFFSET + ID_DIFF,
      MBMMON_STATUS,
      MBMMON_START,
      MBMMON_KILL,

      MBMDMP_ID_OFFSET        = MBMMON_ID_OFFSET    + ID_DIFF, 
      MBMDMP_STATUS,
      MBMDMP_START,
      MBMDMP_KILL,

      TANSRV_ID_OFFSET        = MBMDMP_ID_OFFSET    + ID_DIFF,
      TANSRV_STATUS,
      TANSRV_START,
      TANSRV_KILL,

      STORAGE_ID_OFFSET       = TANSRV_ID_OFFSET    + ID_DIFF,
      STORAGE_STATUS,
      STORAGE_START,
      STORAGE_KILL,

      DID_ID_OFFSET           = STORAGE_ID_OFFSET   + ID_DIFF,
      DID_STATUS,
      DID_START,
      DID_KILL,

      DNSSRV_ID_OFFSET           = DID_ID_OFFSET   + ID_DIFF,
      DNSSRV_STATUS,
      DNSSRV_START,
      DNSSRV_KILL,

      CTRL_ID_OFFSET          = DNSSRV_ID_OFFSET       + ID_DIFF,
      CTRL_STATUS,
      CTRL_START,
      CTRL_KILL,
      CTRL_TASKS,

      CMD_UPDATE_DEPENDENT_VALUES = 10000,
      CMD_HANDLE_SUBSCRIPTION,
    };

    static constexpr const char* TASK_DEAD    = "DEAD";
    static constexpr const char* TASK_RUNNING = "RUNNING";

    class dim_info_t  {
    public:
      int replyID = 0;
      int svcID   = 0;
      Interactor* recipient = nullptr;
      char fmt    = 'I';
      std::string data;
    dim_info_t(int id, Interactor* r, char f='I') : replyID(id), recipient(r), fmt(f) {}
      dim_info_t() = default;
      dim_info_t(dim_info_t&& copy) = default;
      dim_info_t(const dim_info_t& copy) = default;
      dim_info_t& operator = (dim_info_t&& copy) = default;
      dim_info_t& operator = (const dim_info_t& copy) = default;
      ~dim_info_t();

      void dic_release();
      void dic_subscribe(long domain, const std::string& name, void (*cb)(void*, void*, int*));
    };

    class task_state_t   {
    public:
      std::string  name;
      std::string  state;
      std::string  meta;
    };

    class task_t : public task_state_t  {
    public:
      int identifier;
      dim_info_t dim;
    task_t(int id) : identifier(id)  {}
    };

    class internals_t   {
    public:
      bool use_short_list = true;
      long mainDnsDIC = 0;
      long dnsDIC = 0;
      long dnsDIS = 0;
      std::string  old_np;
      std::mutex   dim_protection;
      dim_info_t   tmSrvList;
      dim_info_t   ctrlTasks;
      std::map<std::string, task_t*> tasks;
      std::vector<task_state_t> controller_tasks;
      std::map<long, std::map<CPP::Interactor*, const void*> > subscriptions;
      CPP::Interactor* gui = nullptr;
    };

  protected:
    /// Node name
    std::string m_node;
    /// Name of the DIM command to start a process
    std::string m_start;
    /// Name of the DIM command to stop a process
    std::string m_stop;
    /// Name of the DIM command to kill a process
    std::string m_kill;

    std::string         m_user;
    std::string         m_dnsNode;
    std::string         m_mainDNS;
    std::string         m_runinfo;
    std::string         m_startup;
    std::string         m_ctrlType;
    std::string         m_partition;
    std::string         m_ctrlScript;
    std::string         m_architecture;

    std::string         m_logFifo;
    std::string         m_replacements;

  private:

    std::unique_ptr<task_t>    m_tmSrv;
    std::unique_ptr<task_t>    m_tanSrv;
    std::unique_ptr<task_t>    m_logSrv;
    std::unique_ptr<task_t>    m_logView;
    std::unique_ptr<task_t>    m_mbmmon;
    std::unique_ptr<task_t>    m_mbmdmp;
    std::unique_ptr<task_t>    m_storage;
    std::unique_ptr<task_t>    m_did;
    std::unique_ptr<task_t>    m_dns;
    std::unique_ptr<task_t>    m_ctrl;

    std::unique_ptr<internals_t> internals;

    long                m_numSlaves    = 0;


    /// Copy constructor
    TaskManager(const TaskManager& c) = delete;
    /// Assignment operator
    TaskManager& operator=(const TaskManager& c) = delete;

    void updateTaskStatus(task_t& task);
    void updateSubscriptions(long facility);
    void doUpdateDependentValues();

    template <typename T> T safe_copy(const T& obj)  const;

  public:
    /// Standard constructor
    TaskManager(CPP::Interactor* g);
    /// Standard destructor
    virtual ~TaskManager() = default;

    void init(RTL::CLI& opts);

    /// Interactor interrupt handler callback
    virtual void handle(const CPP::Event& ev)  override;

    /// Start a process
    int fmc_start(const std::string& utgid, const std::string& fmc_args, const std::string& cmd, const std::string& args) const;
    /// Kill a process
    int fmc_stop(const std::string& utgid, int sig_num, int wait_before_kill)  const;
    /// Kill a process
    int fmc_kill(const std::string& utgid, int sig_num) const;

    void subscribe(int facility, CPP::Interactor* target, const void* param = nullptr);

    std::string userName()     const   {   return m_user;         }
    void        setUserName(const std::string& val);

    std::string nodeName()     const   {   return m_node;         }
    void        setNodeName(const std::string& val);

    std::string mainDNS()      const   {   return m_mainDNS;      }
    void        setMainDNS(const std::string& val);

    std::string dnsNode()      const   {   return m_dnsNode;      }
    void        setDnsName(const std::string& val);

    std::string partition()    const   {   return m_partition;    }
    void        setPartition(const std::string& val);

    std::string architecture()  const  {   return m_architecture;  }
    void        setArchitecture(const std::string& val);

    std::string runinfo()       const  {   return m_runinfo;       }
    void        setRuninfo(const std::string& val);

    std::string ctrlScript()    const  {   return m_ctrlScript;    }
    void        setCtrlScript(const std::string& val);

    std::string replacements()  const  {   return m_replacements;  }
    void        setReplacements(const std::string& val);

    long        numSlaves()     const  {   return m_numSlaves;     }
    void        setNumSlaves(long value) { m_numSlaves = value;    }

    std::string logFifo()       const  {   return m_logFifo;       }

    std::string tmSrvName()     const;
    std::string tanSrvName()    const;
    std::string logSrvName()    const;
    std::string logViewName()   const;
    std::string mbmmonName()    const;
    std::string mbmdmpName()    const;
    std::string storageName()   const;
    std::string ctrlName()      const;
    std::string dnsSrvName()    const;
    std::string didName()       const;

    std::string tmSrvState()    const;
    std::string tanSrvState()   const;
    std::string logSrvState()   const;
    std::string logViewState()  const;
    std::string mbmmonState()   const;
    std::string mbmdmpState()   const;
    std::string storageState()  const;
    std::string ctrlState()     const;
    std::string dnsSrvState()   const;
    std::string didState()      const;
    
    std::vector<task_state_t>  ctrlTasks()  const;

    std::mutex& data_protection()  const;

    /// Set the UTGID names according to the parameter settings
    void updateDependentValues();

    std::string startDefaults() const;
    std::string killDefaults() const;

    virtual void startProcess(const std::string& args);
    virtual void killProcess(const std::string& args);

    /// Kill all DAQ processes
    virtual void kill_all_tasks();

    /// Start tmSrv process
    virtual void tmSrv_start();
    /// Kill tmSrv process
    virtual void tmSrv_kill();

    /// Start dns process
    virtual void dnsSrv_start();
    /// Kill dns process
    virtual void dnsSrv_kill();

    /// Start logSrv process
    virtual void logSrv_start();
    /// Kill logSrv process
    virtual void logSrv_kill();

    /// Start controller process
    virtual void controller_start();
    /// Kill controller process
    virtual void controller_kill();

    /// Start logViewer process
    virtual void logViewer_start();
    /// Kill logViewer process
    virtual void logViewer_kill();

    /// Start mbmmon process
    virtual void mbmmon_start();
    /// Kill mbmmon process
    virtual void mbmmon_kill();

    /// Start mbmdmp process
    virtual void mbmdmp_start();
    /// Kill mbmdmp process
    virtual void mbmdmp_kill();

    /// Start storage process
    virtual void storage_start();
    /// Kill storage process
    virtual void storage_kill();

    /// Start tanSrv process
    virtual void tanSrv_start();
    /// Kill tanSrv process
    virtual void tanSrv_kill();

    /// Start local DID process
    virtual void did_start();
    /// Kill did process
    virtual void did_kill();

  };   //  End class State
}      //  End namespace 
#endif //  ONLINE_TESTBEAM_TASKMANAGER
