#ifndef TESTBEAM_CONFIG_H
#define TESTBEAM_CONFIG_H

#define CONTROLLER_TYPE    "Controller"
//#define CONTROLLER_COMMAND_OPT ""
//#define CONTROLLER_STATUS_OPT  "/status"
#define CONTROLLER_COMMAND_OPT "/command"
#define CONTROLLER_STATUS_OPT  "/state"

#endif  // TESTBEAM_CONFIG_H
