#!/bin/bash
# =========================================================================
#
#  Default script to start the passthrough process on a farm node.
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    20/05/2013
#
# =========================================================================
#
eval `python ${FARMCONFIGROOT}/job/ConfigureCheckpoint.py -r ${RUNINFO} -s `;
##
## BAD HACKS!
##
export DIM_DNS_NODE=`hostname -s`;
export PYTHONPATH=`pwd`/SciFiMonitoring/scripts:$PYTHONPATH;
###
GaudiCheckpoint.exe libGaudiOnline.so OnlineTask \
    -msgsvc=LHCb::FmcMessageSvc \
    -tasktype=LHCb::Class1Task \
    -main=/group/online/dataflow/templates/options/Main.opts \
    -opt=command="import SciFiMon as sf; sf.run()"
