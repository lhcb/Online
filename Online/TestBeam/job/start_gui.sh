#!/bin/bash
# =========================================================================
#
#  Start testbeam gui.
#  Requires: project setup ( $> source setup.x86_64-centos7-gcc7-opt.vars )
#
#  Author   M.Frank
#  Version: 1.0
#  Date:    20/05/2013
#
# =========================================================================
#
gentest libTestBeamGui.so testbeam_node_gui \
    -runinfo=${TESTBEAMROOT}/options/OnlineEnvBase.py \
    -instances=32 -maxinst=32 \
    -partition=Upgrade        \
    -dns=${HOST} -node=${HOST} -maindns=${DIM_DNS_NODE} "$*";
