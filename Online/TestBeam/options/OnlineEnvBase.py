# ---------------- General partition parameters:    
PartitionID              = 65535
PartitionIDName          = "FFFF"
PartitionName            = "LHCb"
Activity                 = "Upgrade"
TAE                      = 0
OutputLevel              = 3
AcceptRate               = 1.0
OnlineVersion            = "Online_v6r15"
passThroughDelay         = 0
AcceptRate               = 1.0
MooreStartupMode         = 0
MooreVersion             = "Moore_v28r3"
MooreOnlineVersion       = "MooreOnline_v28r3"
OnlineVersion            = "Online_v6r15"
HltArchitecture          = "../options/lbDataflowArch_Hive"
