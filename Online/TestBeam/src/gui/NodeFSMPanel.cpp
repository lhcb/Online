//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : TestBeam-GUI
//
//  Author     : Markus Frank
//==========================================================================

/// Framework include files
#include <TestBeam/gui/NodeFSMPanel.h>
#include <TestBeam/gui/GuiException.h>
#include <TestBeam/gui/GuiCommand.h>
#include <TestBeam/gui/GuiMsg.h>
#include <CPP/IocSensor.h>
#include <CPP/Event.h>
#include <dim/dic.h>
#include <dim/dis.h>

/// ROOT include files
#include <TGLabel.h>
#include <TGButton.h>
#include <TGTextEntry.h>
#include <TGComboBox.h>
#include <TGTableLayout.h>

// C/C++ include files
#include <sstream>

ClassImp(testbeam::NodeFSMPanel)

namespace {
  std::string guiserv_name()   {
    return "TestBeamGUI_"+RTL::processName();
  }
  std::string _selectedText(TGComboBox* c)  {
    TGLBEntry* selected = c->GetSelectedEntry();
    return selected ? selected->GetTitle() : "";
  }
  /// DIM service update handler
  void run_no_update(void* tag, void** buf, int* size, int* first)  {
    testbeam::NodeFSMPanel* h = *(testbeam::NodeFSMPanel**)tag;
    if ( *first )  {
    }
    if ( h )  {
      *size = sizeof(h->m_currentRun);
      *buf  = &h->m_currentRun;
    }
  }
}

/// Standard initializing constructor
testbeam::NodeFSMPanel::NodeFSMPanel(TGFrame* pParent, CPP::Interactor* g, TaskManager& tm)
  : TGCompositeFrame(pParent, 100, 100, kVerticalFrame), gui(g), taskManager(tm), taskHandler(tm)
{
  gClient->GetColorByName("#c0c0c0", disabled);
  gClient->GetColorByName("white",   enabled);
  gClient->GetColorByName("#30B030", green);
  gClient->GetColorByName("#C04040", red);
  gClient->GetColorByName("#4040F0", blue);
  gClient->GetColorByName("#F0F000", yellow);
  gClient->GetColorByName("#C0C000", executing);
  gClient->GetColorByName("#000000", black);
  white       = enabled;
}

void testbeam::NodeFSMPanel::init()   {
  std::string utgid;
  TGGroupFrame* para_group   = new TGGroupFrame(this, "Processing infrastructure");
  para_group->SetLayoutManager(new TGTableLayout(para_group, 11, 9));
  
  maindns.label  = new TGLabel(    para_group,  "Main DNS:",            MAINDNS_LABEL);
  maindns.input  = new TGTextEntry(para_group,  taskManager.mainDNS().c_str(),      MAINDNS_INPUT);
  maindns.input->SetMaxWidth(100);

  dns.label      = new TGLabel(    para_group,  "Local DNS:",           DNS_LABEL);
  dns.input      = new TGTextEntry(para_group,  taskManager.dnsNode().c_str(),          DNS_INPUT);
  dns.input->SetMaxWidth(100);

  node.label     = new TGLabel(    para_group,  "Node",                 NODE_LABEL);
  node.input     = new TGTextEntry(para_group,  taskManager.nodeName().c_str(),         NODE_INPUT);

  std::stringstream str;
  str << m_numSlaves;
  numslave.label = new TGLabel(    para_group,  "Num.slaves",           NUMSLAVE_LABEL);
  numslave.input = new TGTextEntry(para_group,  str.str().c_str(),      NUMSLAVE_INPUT);

  script.label   = new TGLabel(    para_group,  "Startup script",       NUMSLAVE_LABEL);
  script.input   = new TGTextEntry(para_group,  taskManager.ctrlScript().c_str(),   SCRIPT_INPUT);

  arch.label     = new TGLabel(    para_group,  "Architecture",         ARCH_LABEL);
  arch.input     = new TGTextEntry(para_group,  taskManager.architecture().c_str(), ARCH_INPUT);

  part.label     = new TGLabel(    para_group,  "Partition",            PART_LABEL);
  part.input     = new TGTextEntry(para_group,  taskManager.partition().c_str(),    PART_INPUT);
  
  replace.label  = new TGLabel(    para_group,  "Replacements",         REPLACE_LABEL);
  replace.input  = new TGTextEntry(para_group,  taskManager.replacements().c_str(), REPLACE_INPUT);

  str.str("");
  str << m_currentRun;
  runnumber.label  = new TGLabel(    para_group,  "Run number",         RUNNUMBER_LABEL);
  runnumber.input  = new TGTextEntry(para_group, str.str().c_str(),     RUNNUMBER_INPUT);

  arch.input->Connect(     "TextChanged(const char*)", "testbeam::NodeFSMPanel", this, "architectureChanged(const char*)");
  script.input->Connect(   "TextChanged(const char*)", "testbeam::NodeFSMPanel", this, "scriptChanged(const char*)");
  maindns.input->Connect(  "TextChanged(const char*)", "testbeam::NodeFSMPanel", this, "mainDnsChanged(const char*)");
  dns.input->Connect(      "TextChanged(const char*)", "testbeam::NodeFSMPanel", this, "dnsChanged(const char*)");
  node.input->Connect(     "TextChanged(const char*)", "testbeam::NodeFSMPanel", this, "hostChanged(const char*)");
  numslave.input->Connect( "TextChanged(const char*)", "testbeam::NodeFSMPanel", this, "numslaveChanged(const char*)");
  part.input->Connect(     "TextChanged(const char*)", "testbeam::NodeFSMPanel", this, "partitionChanged(const char*)");
  replace.input->Connect(  "TextChanged(const char*)", "testbeam::NodeFSMPanel", this, "replacementChanged(const char*)");
  runnumber.input->Connect("TextChanged(const char*)", "testbeam::NodeFSMPanel", this, "runnumberChanged(const char*)");

  apply          = new TGTextButton(para_group, "Apply parameters", APPLY_PARAMS);
  apply->Connect("Clicked()", "testbeam::NodeFSMPanel", this, "applyParams()");

  ctrl.label     = new TGLabel(     para_group, "controller",   CTRL_LABEL);
  ctrl.utgid     = new TGTextEntry( para_group, taskManager.ctrlName().c_str());
  ctrl.start     = new TGTextButton(para_group, "Start",        CTRL_START);
  ctrl.status    = new TGTextEntry( para_group, TaskManager::TASK_DEAD);
  ctrl.kill      = new TGTextButton(para_group, "Kill",         CTRL_KILL);
  ctrl.start->Connect("Clicked()", "testbeam::TaskHandler", &taskHandler, "controller_start()");
  ctrl.kill->Connect( "Clicked()", "testbeam::TaskHandler", &taskHandler, "controller_kill()");
  ctrl.start->SetEnabled(kFALSE);
  ctrl.kill->SetEnabled(kFALSE);

  logSrv.label   = new TGLabel(     para_group, "logSrv",       LOGSRV_LABEL);
  logSrv.utgid   = new TGTextEntry( para_group, taskManager.logSrvName().c_str());
  logSrv.start   = new TGTextButton(para_group, "Start",        LOGSRV_START);
  logSrv.status  = new TGTextEntry( para_group, TaskManager::TASK_DEAD);
  logSrv.kill    = new TGTextButton(para_group, "Kill",         LOGSRV_KILL);
  logSrv.start->Connect("Clicked()", "testbeam::TaskHandler", &taskHandler, "logSrv_start()");
  logSrv.kill->Connect( "Clicked()", "testbeam::TaskHandler", &taskHandler, "logSrv_kill()");
  logSrv.start->SetEnabled(kFALSE);
  logSrv.kill->SetEnabled(kFALSE);

  tmSrv.label    = new TGLabel(     para_group, "tmSrv",        TMSRV_LABEL);
  tmSrv.utgid    = new TGTextEntry( para_group, taskManager.tmSrvName().c_str());
  tmSrv.start    = new TGTextButton(para_group, "Start",        TMSRV_START);
  tmSrv.status   = new TGTextEntry( para_group, TaskManager::TASK_DEAD);
  tmSrv.kill     = new TGTextButton(para_group, "Kill",         TMSRV_KILL);
  tmSrv.start->Connect("Clicked()", "testbeam::TaskHandler", &taskHandler, "tmSrv_start()");
  tmSrv.kill->Connect( "Clicked()", "testbeam::TaskHandler", &taskHandler, "tmSrv_kill()");
  tmSrv.start->SetEnabled(kFALSE);
  tmSrv.kill->SetEnabled(kFALSE);

  logViewer.label   = new TGLabel(     para_group, "logViewer", LOGVIEWER_LABEL);
  logViewer.utgid   = new TGTextEntry( para_group, taskManager.logViewName().c_str());
  logViewer.start   = new TGTextButton(para_group, "Start",     LOGVIEWER_START);
  logViewer.status  = new TGTextEntry( para_group,TaskManager::TASK_DEAD);
  logViewer.kill    = new TGTextButton(para_group, "Kill",      LOGVIEWER_KILL);
  logViewer.start->Connect("Clicked()", "testbeam::TaskHandler", &taskHandler, "logViewer_start()");
  logViewer.kill->Connect( "Clicked()", "testbeam::TaskHandler", &taskHandler, "logViewer_kill()");
  logViewer.start->SetEnabled(kFALSE);
  logViewer.kill->SetEnabled(kFALSE);

  mbmmon.label      = new TGLabel(     para_group, "mbmmon",    MBMMON_LABEL);
  mbmmon.utgid      = new TGTextEntry( para_group, taskManager.mbmmonName().c_str());
  mbmmon.start      = new TGTextButton(para_group, "Start",     MBMMON_START);
  mbmmon.status     = new TGTextEntry( para_group,  TaskManager::TASK_DEAD);
  mbmmon.kill       = new TGTextButton(para_group, "Kill",      MBMMON_KILL);
  mbmmon.start->Connect("Clicked()", "testbeam::TaskHandler", &taskHandler, "mbmmon_start()");
  mbmmon.kill->Connect( "Clicked()", "testbeam::TaskHandler", &taskHandler, "mbmmon_kill()");
  mbmmon.start->SetEnabled(kFALSE);
  mbmmon.kill->SetEnabled(kFALSE);

#define _PAD 0
#define _PADL 15
#define _PADY  5
  para_group->AddFrame(maindns.label,    new TGTableLayoutHints(0, 1, 0, 1, kLHintsLeft|kLHintsCenterY,_PAD,_PAD,_PADY,_PAD));
  para_group->AddFrame(maindns.input,    new TGTableLayoutHints(1, 2, 0, 1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PAD,_PAD,_PADY,_PAD));
  para_group->AddFrame(dns.label,        new TGTableLayoutHints(0, 1, 1, 2, kLHintsLeft|kLHintsCenterY,_PAD,_PAD,_PADY,_PAD));
  para_group->AddFrame(dns.input,        new TGTableLayoutHints(1, 2, 1, 2, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PAD,_PAD,_PADY,_PAD));
  para_group->AddFrame(node.label,       new TGTableLayoutHints(0, 1, 2, 3, kLHintsLeft|kLHintsCenterY,_PAD,_PADY,_PADY,_PAD));
  para_group->AddFrame(node.input,       new TGTableLayoutHints(1, 2, 2, 3, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PAD,_PAD,_PADY,_PAD));
  para_group->AddFrame(part.label,       new TGTableLayoutHints(0, 1, 3, 4, kLHintsLeft|kLHintsCenterY,_PAD,_PADY,_PADY,_PAD));
  para_group->AddFrame(part.input,       new TGTableLayoutHints(1, 2, 3, 4, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PAD,_PAD,_PADY,_PAD));
  para_group->AddFrame(numslave.label,   new TGTableLayoutHints(0, 1, 4, 5, kLHintsLeft|kLHintsCenterY,_PAD,_PAD,_PADY,_PAD));
  para_group->AddFrame(numslave.input,   new TGTableLayoutHints(1, 2, 4, 5, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PAD,_PAD,_PADY,_PAD));
  para_group->AddFrame(runnumber.label,  new TGTableLayoutHints(0, 1, 5, 6, kLHintsLeft|kLHintsCenterY,_PAD,_PAD,_PADY,_PAD));
  para_group->AddFrame(runnumber.input,  new TGTableLayoutHints(1, 5, 5, 6, kLHintsLeft|kLHintsCenterY|kLHintsExpandY|kLHintsExpandX, _PAD,_PAD,_PADY,_PAD));
  para_group->AddFrame(replace.label,    new TGTableLayoutHints(0, 1, 6, 7, kLHintsLeft|kLHintsCenterY,_PAD,_PADY,_PADY,_PAD));
  para_group->AddFrame(replace.input,    new TGTableLayoutHints(1, 6, 6, 7, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PAD,_PAD,_PADY,_PAD));

  para_group->AddFrame(script.label,     new TGTableLayoutHints(0, 1, 8, 9, kLHintsLeft|kLHintsCenterY|kLHintsExpandX|kLHintsExpandY,_PAD,_PAD,_PADY,_PAD));
  para_group->AddFrame(script.input,     new TGTableLayoutHints(1, 6, 8, 9, kLHintsLeft|kLHintsCenterY|kLHintsExpandX|kLHintsExpandY,_PAD,_PAD,_PADY,_PAD));
  para_group->AddFrame(arch.label,       new TGTableLayoutHints(0, 1, 9,10, kLHintsLeft|kLHintsCenterY,_PAD,_PAD,_PADY,_PAD));
  para_group->AddFrame(arch.input,       new TGTableLayoutHints(1, 6, 9,10, kLHintsLeft|kLHintsCenterY|kLHintsExpandY|kLHintsExpandX, _PAD,_PAD,_PADY,_PAD));

  int row = 0;
  para_group->AddFrame(logSrv.label,     new TGTableLayoutHints(3, 4, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD));
  para_group->AddFrame(logSrv.utgid,     new TGTableLayoutHints(4, 5, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PAD,_PAD));
  para_group->AddFrame(logSrv.start,     new TGTableLayoutHints(5, 6, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PAD,_PAD));
  para_group->AddFrame(logSrv.kill,      new TGTableLayoutHints(6, 7, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PAD,_PAD)); 
  para_group->AddFrame(logSrv.status,    new TGTableLayoutHints(7, 9, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PAD,_PAD)); 
  ++row;
  para_group->AddFrame(tmSrv.label,      new TGTableLayoutHints(3, 4, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD));
  para_group->AddFrame(tmSrv.utgid,      new TGTableLayoutHints(4, 5, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PAD,_PAD));
  para_group->AddFrame(tmSrv.start,      new TGTableLayoutHints(5, 6, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PAD,_PAD));
  para_group->AddFrame(tmSrv.kill,       new TGTableLayoutHints(6, 7, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PAD,_PAD)); 
  para_group->AddFrame(tmSrv.status,     new TGTableLayoutHints(7, 9, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY|kLHintsExpandX, _PADL,_PAD,_PAD,_PAD)); 
  ++row;
  para_group->AddFrame(logViewer.label,  new TGTableLayoutHints(3, 4, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD));
  para_group->AddFrame(logViewer.utgid,  new TGTableLayoutHints(4, 5, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PAD,_PAD));
  para_group->AddFrame(logViewer.start,  new TGTableLayoutHints(5, 6, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PAD,_PAD));
  para_group->AddFrame(logViewer.kill,   new TGTableLayoutHints(6, 7, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PAD,_PAD)); 
  para_group->AddFrame(logViewer.status, new TGTableLayoutHints(7, 9, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PAD,_PAD)); 
  ++row;
  para_group->AddFrame(mbmmon.label,     new TGTableLayoutHints(3, 4, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD));
  para_group->AddFrame(mbmmon.utgid,     new TGTableLayoutHints(4, 5, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PAD,_PAD));
  para_group->AddFrame(mbmmon.start,     new TGTableLayoutHints(5, 6, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PAD,_PAD));
  para_group->AddFrame(mbmmon.kill,      new TGTableLayoutHints(6, 7, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PAD,_PAD)); 
  para_group->AddFrame(mbmmon.status,    new TGTableLayoutHints(7, 9, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY, _PADL,_PAD,_PAD,_PAD)); 
  ++row;
  para_group->AddFrame(ctrl.label,       new TGTableLayoutHints(3, 4, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PADY,_PAD));
  para_group->AddFrame(ctrl.utgid,       new TGTableLayoutHints(4, 5, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PAD,_PAD));
  para_group->AddFrame(ctrl.start,       new TGTableLayoutHints(5, 6, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PAD,_PAD));
  para_group->AddFrame(ctrl.kill,        new TGTableLayoutHints(6, 7, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PAD,_PAD)); 
  para_group->AddFrame(ctrl.status,      new TGTableLayoutHints(7, 9, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY,_PADL,_PAD,_PAD,_PAD)); 
  ++row;
  para_group->AddFrame(apply,            new TGTableLayoutHints(5, 9, row, row+1, kLHintsExpandX|kLHintsExpandY|kLHintsCenterY, _PADL,_PAD,_PADY,_PAD));

  TGGroupFrame* cmds_group = new TGGroupFrame(this, "Commands");
  cmds_group->SetLayoutManager(new TGTableLayout(cmds_group, 2, 9));
  ctrl_label   = new TGLabel(cmds_group, "Controller");
  ctrl_command = new TGComboBox(cmds_group, CONTROLLER_COMMAND);
  ctrl_command->AddEntry("launch",   CONTROLLER_LAUNCH);
  ctrl_command->AddEntry("load",     CONTROLLER_LOAD);
  ctrl_command->AddEntry("configure",CONTROLLER_CONFIG);
  ctrl_command->AddEntry("reset",    CONTROLLER_RESET);
  ctrl_command->AddEntry("unload",   CONTROLLER_UNLOAD);
  ctrl_command->Select(CONTROLLER_LAUNCH);
  ctrl_command->Connect("Selected(Int_t)",  "testbeam::NodeFSMPanel", this, "commandChanged(Int_t)");
  ctrl_killall   = new TGTextButton(cmds_group, "Kill all",    CONTROLLER_KILLALL);
  ctrl_autostart = new TGTextButton(cmds_group, "AutoStart",    CTRL_AUTOSTART);
  ctrl_autostop  = new TGTextButton(cmds_group, "AutoStop",    CTRL_AUTOSTOP);
  ctrl_killall->Connect(   "Clicked()", "testbeam::TaskHandler", &taskHandler, "kill_all_tasks()");
  ctrl_autostart->Connect( "Clicked()", "testbeam::NodeFSMPanel", this, "autoStart()");
  ctrl_autostop->Connect(  "Clicked()", "testbeam::NodeFSMPanel", this, "autoStop()");

  ctrl_status = new TGTextEntry(cmds_group);
  cmds_group->AddFrame(ctrl_label,    new TGTableLayoutHints(0, 1, 0, 1, kLHintsLeft|kLHintsCenterY|kLHintsExpandX|kLHintsExpandY,_PADL,1,3,1));
  cmds_group->AddFrame(ctrl_command,  new TGTableLayoutHints(1, 2, 0, 1, kLHintsLeft|kLHintsCenterY|kLHintsExpandX|kLHintsExpandY,_PADL,1,3,1));
  cmds_group->AddFrame(ctrl_status,   new TGTableLayoutHints(2, 3, 0, 1, kLHintsLeft|kLHintsCenterY|kLHintsExpandX|kLHintsExpandY,_PADL,1,3,1));
  cmds_group->AddFrame(ctrl_autostart,new TGTableLayoutHints(5, 6, 0, 1, kLHintsLeft|kLHintsCenterY|kLHintsExpandX|kLHintsExpandY,_PADL,1,3,1));
  cmds_group->AddFrame(ctrl_autostop, new TGTableLayoutHints(6, 7, 0, 1, kLHintsLeft|kLHintsCenterY|kLHintsExpandX|kLHintsExpandY,_PADL,1,3,1));
  cmds_group->AddFrame(ctrl_killall,  new TGTableLayoutHints(7, 8, 0, 1, kLHintsLeft|kLHintsCenterY|kLHintsExpandX|kLHintsExpandY,_PADL,1,3,1));
  m_cmds_group = cmds_group;
  TGGroupFrame* child_group = new TGGroupFrame(this, new TGString("Controller's Child States"));
  int num_rows = NUM_ENTRIES_CHILDREN;
  child_group->SetLayoutManager(new TGTableLayout(child_group, (num_rows+2)/2, 6));
  for (int i = 0; i < num_rows; ++i)    {
    Child c;
    int  col = i%2==0 ? 0 : 3;
    row = i/2;
    c.id = i;
    c.name  = new TGTextEntry(child_group, row == 0 ? "Process name" : "");
    c.name->SetToolTipText("Child name");
    c.name->SetBackgroundColor(enabled);
    c.name->SetEnabled(kFALSE);
    c.name->SetForegroundColor(black);
    c.name->SetBackgroundColor(row == 0 ? disabled : white);

    c.state = new TGTextEntry(child_group, row == 0 ? "State" : "");
    c.state->SetToolTipText("Child state");
    c.state->SetBackgroundColor(enabled);
    c.state->SetEnabled(kFALSE);
    c.state->SetForegroundColor(black);
    c.state->SetBackgroundColor(row == 0 ? disabled : white);

    c.meta = new TGTextEntry(child_group, row == 0 ? "Meta state" : "");
    c.meta->SetToolTipText("Child meta state");
    c.meta->SetBackgroundColor(enabled);
    c.meta->SetEnabled(kFALSE);
    c.meta->SetForegroundColor(black);
    c.meta->SetBackgroundColor(row == 0 ? disabled : white);

    child_group->AddFrame(c.name,  new TGTableLayoutHints(col,   col+1, row, row+1, kLHintsLeft|kLHintsCenterY|kLHintsExpandY, 15,1,0,0)); 
    child_group->AddFrame(c.state, new TGTableLayoutHints(col+1, col+2, row, row+1, kLHintsLeft|kLHintsCenterY, 1,5,0,0)); 
    child_group->AddFrame(c.meta,  new TGTableLayoutHints(col+2, col+3, row, row+1, kLHintsLeft|kLHintsCenterY, 1,5,0,0)); 
    children.push_back(c);
  }

  AddFrame(para_group, new TGLayoutHints(kLHintsRight|kLHintsTop|kLHintsExpandX, 1,1,1,1));
  AddFrame(cmds_group, new TGLayoutHints(kLHintsRight|kLHintsTop|kLHintsExpandX, 1,1,1,1));
  AddFrame(child_group, new TGLayoutHints(kLHintsRight|kLHintsTop|kLHintsExpandX|kLHintsExpandY, 1,1,1,1));

  maindns.input->Resize(250,   maindns.input->GetDefaultHeight());
  dns.input->Resize(250,       dns.input->GetDefaultHeight());
  node.input->Resize(250,      node.input->GetDefaultHeight());
  part.input->Resize(250,      part.input->GetDefaultHeight());
  numslave.input->Resize(150,  numslave.input->GetDefaultHeight());
  runnumber.input->Resize(150, replace.input->GetDefaultHeight());
  replace.input->Resize(650,   replace.input->GetDefaultHeight());
  script.input->Resize(650,    script.input->GetDefaultHeight());
  arch.input->Resize(650,      arch.input->GetDefaultHeight());

  _configTask(tmSrv);
  _configTask(logSrv);
  _configTask(logViewer);
  _configTask(mbmmon);
  _configTask(ctrl);

  apply->Resize(400, 200);

  TGFont *font = gClient->GetFont("-*-arial-bold-r-*-*-24-*-*-*-*-*-iso8859-1");
  ctrl_command->Resize(220,20);
  gClient->NeedRedraw(ctrl_command);
  ctrl_status->Resize(300,45);
  ctrl_status->SetFont(font);
  ctrl_status->SetAlignment(kTextCenterX);
  gClient->NeedRedraw(ctrl_status);
  ctrl_killall->Resize(220,20);
  gClient->NeedRedraw(ctrl_killall);
  for (int i = 0; i < num_rows; ++i)    {
    const Child& c = children[i];
    c.name->Resize(240,c.name->GetDefaultHeight()-1);
    c.state->Resize(120,c.state->GetDefaultHeight()-1);
    c.meta->Resize(120,c.state->GetDefaultHeight()-1);
    gClient->NeedRedraw(c.name);
    gClient->NeedRedraw(c.state);
    gClient->NeedRedraw(c.meta);
  }

  taskManager.subscribe(TaskManager::LOGVIEWER_STATUS, this, nullptr);
  taskManager.subscribe(TaskManager::MBMMON_STATUS, this, nullptr);
  taskManager.subscribe(TaskManager::CTRL_STATUS, this, nullptr);
  taskManager.subscribe(TaskManager::CTRL_TASKS, this, nullptr);
  taskManager.subscribe(TaskManager::TMSRV_STATUS, this, nullptr);
  // taskManager.subscribe(TaskManager::TMSRV_LIST, this, nullptr);
  taskManager.subscribe(TaskManager::LOGSRV_STATUS, this, nullptr);
  taskManager.subscribe(TaskManager::CMD_UPDATE_DEPENDENT_VALUES, this, nullptr);

  ::dis_start_serving(guiserv_name().c_str());
}

void testbeam::NodeFSMPanel::_configTask(TaskEntry& entry)   {
  double height = entry.kill->GetDefaultHeight();
  entry.kill  ->Resize(120, height);
  entry.kill->SetForegroundColor(black);
  entry.start ->Resize(220, height);
  entry.start->SetForegroundColor(black);
  entry.utgid ->Resize(270, height);
  entry.utgid->SetForegroundColor(black);
  entry.status->Resize(100, height);
  entry.utgid ->SetEnabled(kFALSE);
  entry.start ->SetEnabled(kTRUE);
  entry.kill  ->SetEnabled(kTRUE);
  entry.status->SetEnabled(kFALSE);
  entry.status->SetBackgroundColor(red);
  entry.status->SetForegroundColor(white);
}

/// Default destructor
testbeam::NodeFSMPanel::~NodeFSMPanel()   {
  if ( 0 != m_runnumber_id      ) ::dis_remove_service(m_runnumber_id);
}

void testbeam::NodeFSMPanel::enableParams()   {
  maindns.input->SetEnabled(kTRUE);
  numslave.input->SetEnabled(kTRUE);
  dns.input->SetEnabled(kTRUE);
  node.input->SetEnabled(kTRUE);
  part.input->SetEnabled(kTRUE);
  replace.input->SetEnabled(kTRUE);
  script.input->SetEnabled(kTRUE);
  arch.input->SetEnabled(kTRUE);

  ctrl.utgid->SetEnabled(kTRUE);
  ctrl.start->SetEnabled(kFALSE);
  ctrl.kill->SetEnabled(kFALSE);

  tmSrv.utgid->SetEnabled(kTRUE);
  tmSrv.start->SetEnabled(kFALSE);
  tmSrv.kill->SetEnabled(kFALSE);

  logSrv.utgid->SetEnabled(kTRUE);
  logSrv.start->SetEnabled(kFALSE);
  logSrv.kill->SetEnabled(kFALSE);

  logViewer.utgid->SetEnabled(kTRUE);
  logViewer.start->SetEnabled(kFALSE);
  logViewer.kill->SetEnabled(kFALSE);

  mbmmon.kill->SetEnabled(kFALSE);
  mbmmon.utgid->SetEnabled(kTRUE);
  mbmmon.start->SetEnabled(kFALSE);

  apply->SetText("Apply parameters");
  if ( 0 != m_runnumber_id     ) ::dis_remove_service(m_runnumber_id);
  m_runnumber_id = 0;
  m_applied = false;
}

void testbeam::NodeFSMPanel::applyParams()  {
  if ( m_applied )   {
    std::string c1 = tmSrv.status->GetText();
    std::string c2 = logSrv.status->GetText();
    std::string c3 = ctrl.status->GetText();
    if ( c1 == TaskManager::TASK_DEAD && c2 == TaskManager::TASK_DEAD && c3 == TaskManager::TASK_DEAD )   {
      enableParams();
      return;
    }
    GuiException(kMBOk,kMBIconAsterisk,"Invalid Request",
		 "You can only re-edit the parameters\n"
		 "Once all the dependent processes are dead:\n"
		 " -- logSrv \n"
		 " -- tmSrv \n"
		 " -- DAQ controller \n"
		 "This request is ignored.").send(gui);
    return;
  }
  m_applied = true;
  apply->SetText("Edit parameters");
  maindns.input->SetEnabled(kFALSE);
  numslave.input->SetEnabled(kFALSE);
  dns.input->SetEnabled(kFALSE);
  node.input->SetEnabled(kFALSE);
  part.input->SetEnabled(kFALSE);
  replace.input->SetEnabled(kFALSE);
  arch.input->SetEnabled(kFALSE);
  script.input->SetEnabled(kFALSE);

  ctrl.utgid->SetEnabled(kFALSE);
  ctrl.start->SetEnabled(kTRUE);
  ctrl.kill->SetEnabled(kTRUE);

  tmSrv.utgid->SetEnabled(kFALSE);
  tmSrv.start->SetEnabled(kTRUE);
  tmSrv.kill->SetEnabled(kTRUE);

  logSrv.utgid->SetEnabled(kFALSE);
  logSrv.start->SetEnabled(kTRUE);
  logSrv.kill->SetEnabled(kTRUE);

  logViewer.utgid->SetEnabled(kFALSE);
  logViewer.start->SetEnabled(kTRUE);
  logViewer.kill->SetEnabled(kTRUE);

  mbmmon.kill->SetEnabled(kTRUE);
  mbmmon.utgid->SetEnabled(kFALSE);
  mbmmon.start->SetEnabled(kTRUE);

  taskManager.updateDependentValues();
  this->updateDependentValues();
  std::string svc = taskManager.ctrlName() + "/tasks";
  if ( 0 != m_runnumber_id     ) ::dis_remove_service(m_runnumber_id);
  m_runnumber_id     = ::dis_add_service((taskManager.partition()+"/RunInfo/RunNumber").c_str(),"I",0,0,run_no_update,(long)this);
  ::dis_update_service(m_runnumber_id);
  ::dis_start_serving(guiserv_name().c_str());
}

void testbeam::NodeFSMPanel::updateDependentValues()  {
  GuiMsg("NodeFSMPanel: Update dependent values.....").send(gui);
  if ( tmSrv.utgid )  {
    tmSrv.utgid->SetText(taskManager.tmSrvName().c_str());
    gClient->NeedRedraw(tmSrv.utgid);
  }
  if ( logSrv.utgid )  {
    logSrv.utgid->SetText(taskManager.logSrvName().c_str());
    gClient->NeedRedraw(logSrv.utgid);
  }
  if ( logViewer.utgid )  {
    logViewer.utgid->SetText(taskManager.logViewName().c_str());
    gClient->NeedRedraw(logViewer.utgid);
  }
  if ( mbmmon.utgid )  {
    mbmmon.utgid->SetText(taskManager.mbmmonName().c_str());
    gClient->NeedRedraw(mbmmon.utgid);
  }
  if ( ctrl.utgid )  {
    ctrl.utgid->SetText(taskManager.ctrlName().c_str());
    gClient->NeedRedraw(ctrl.utgid);
  }
}

void testbeam::NodeFSMPanel::updateChildren()   {
  const auto& tasks = taskManager.ctrlTasks();
  size_t ichld = 2;
  for(const auto& task : tasks)   {
    if ( ichld < NUM_ENTRIES_CHILDREN )  {
      const Child& c = children[ichld];
      c.name->SetText(task.name.c_str());
      c.state->SetText(task.state.c_str());
      c.meta->SetText(task.meta.c_str());
    }
    ++ichld;
  }
  for(size_t i=ichld; i<NUM_ENTRIES_CHILDREN; ++i)  {
    const Child& c = children[i];
    c.name->SetText("");
    c.state->SetText("");
    c.meta->SetText("");
  }
  for( const auto& c : children )   {
    gClient->NeedRedraw(c.name);
    gClient->NeedRedraw(c.state);
    gClient->NeedRedraw(c.meta);
  }
}

void testbeam::NodeFSMPanel::updateCtrlCommand(std::string state)   {
  ctrl_command->RemoveAll();
  ctrl_status->SetText(state.c_str());
  ctrl_command->Disconnect("Selected(Int_t)");
  if ( state == TaskManager::TASK_DEAD )   {
    ctrl_command->AddEntry("launch",   CONTROLLER_LAUNCH);
    ctrl_command->AddEntry("reset",    CONTROLLER_RESET);
    ctrl_command->Select(CONTROLLER_LAUNCH);
    ctrl_status->SetBackgroundColor(blue);
    ctrl_status->SetForegroundColor(white);
    m_autoRun = 0;
  }
  else if ( state == "OFFLINE" )   {
    ctrl_command->AddEntry("load",     CONTROLLER_LOAD);
    ctrl_command->AddEntry("unload",   CONTROLLER_UNLOAD);
    ctrl_command->AddEntry("reset",    CONTROLLER_RESET);
    ctrl_command->AddEntry("destroy",  CONTROLLER_DESTROY);
    ctrl_command->Select(CONTROLLER_LOAD);
    ctrl_status->SetBackgroundColor(blue);
    ctrl_status->SetForegroundColor(white);
  }
  else if ( state == "NOT_READY" )   {
    ctrl_command->AddEntry("configure",CONTROLLER_CONFIG);
    ctrl_command->AddEntry("reset",    CONTROLLER_RESET);
    ctrl_command->AddEntry("unload",   CONTROLLER_UNLOAD);
    ctrl_command->AddEntry("destroy",  CONTROLLER_DESTROY);
    ctrl_command->AddEntry("create",   CONTROLLER_CREATE);
    ctrl_command->Select(CONTROLLER_CONFIG);
    ctrl_status->SetBackgroundColor(yellow);
    ctrl_status->SetForegroundColor(black);
  }
  else if ( state == "CREATED" )   {
    ctrl_command->AddEntry("configure",CONTROLLER_CONFIG);
    ctrl_command->AddEntry("reset",    CONTROLLER_RESET);
    ctrl_command->AddEntry("unload",   CONTROLLER_UNLOAD);
    ctrl_command->AddEntry("destroy",  CONTROLLER_DESTROY);
    ctrl_command->Select(CONTROLLER_CONFIG);
    ctrl_status->SetBackgroundColor(yellow);
    ctrl_status->SetForegroundColor(black);
  }
  else if ( state == "READY" )   {
    ctrl_command->AddEntry("start",    CONTROLLER_START);
    ctrl_command->AddEntry("reset",    CONTROLLER_RESET);
    ctrl_command->AddEntry("unload",   CONTROLLER_UNLOAD);
    ctrl_command->AddEntry("destroy",  CONTROLLER_DESTROY);
    ctrl_command->Select(CONTROLLER_START);
    ctrl_status->SetBackgroundColor(blue);
    ctrl_status->SetForegroundColor(white);
  }
  else if ( state == "RUNNING" )   {
    ctrl_command->AddEntry("stop",     CONTROLLER_STOP);
    ctrl_command->AddEntry("pause",    CONTROLLER_PAUSE);
    ctrl_command->AddEntry("reset",    CONTROLLER_RESET);
    ctrl_command->Select(CONTROLLER_STOP);
    ctrl_status->SetBackgroundColor(green);
    ctrl_status->SetForegroundColor(white);
    m_autoRun = 0;
  }
  else if ( state == "PAUSED" )   {
    ctrl_command->AddEntry("continue", CONTROLLER_CONTINUE);
    ctrl_command->AddEntry("stop",     CONTROLLER_STOP);
    ctrl_command->AddEntry("reset",    CONTROLLER_RESET);
    ctrl_command->AddEntry("pause",    CONTROLLER_PAUSE);
    ctrl_command->Select(CONTROLLER_STOP);
    ctrl_status->SetBackgroundColor(blue);
    ctrl_status->SetForegroundColor(white);
    m_autoRun = 0;
  }
  else if ( state == "ERROR" )   {
    ctrl_command->AddEntry("recover",  CONTROLLER_RECOVER);
    ctrl_command->AddEntry("stop",     CONTROLLER_STOP);
    ctrl_command->AddEntry("reset",    CONTROLLER_RESET);
    ctrl_command->Select(CONTROLLER_RECOVER);
    ctrl_status->SetBackgroundColor(red);
    ctrl_status->SetForegroundColor(white);
    m_autoRun = 0;
  }
  gClient->NeedRedraw(ctrl_command);
  gClient->NeedRedraw(ctrl_status);
  ctrl_command->Connect("Selected(Int_t)",  "testbeam::NodeFSMPanel", this, "commandChanged(Int_t)");
}

void testbeam::NodeFSMPanel::issueCommand(const std::string& cmd)  {
  std::string dp = taskManager.ctrlName() + CONTROLLER_COMMAND_OPT;
  int ret = ::dic_cmnd_service((char*)dp.c_str(),(void*)cmd.c_str(),cmd.length()+1);
  if ( ret == 1 )  {
    ctrl_status->SetText("Executing transition");
    ctrl_status->SetBackgroundColor(executing);
    ctrl_status->SetForegroundColor(black);
    GuiMsg("Successfully sent command: %s->%s",dp.c_str(),cmd.c_str()).send(gui);
    return;
  }
  GuiMsg("FAILED to send command: %s->%s",dp.c_str(),cmd.c_str()).send(gui);
}

/// Update tmSrv status pane
void testbeam::NodeFSMPanel::updateTaskStatus(TaskEntry& entry, const std::string& state)    {
  if ( !state.empty() )   {
    if ( state == TaskManager::TASK_DEAD )   {
      entry.start->SetEnabled(kTRUE);
      entry.kill->SetEnabled(kFALSE);
      entry.status->SetText(TaskManager::TASK_DEAD);
      entry.status->SetBackgroundColor(red);
      IocSensor::instance().send(this, CHECK_ENABLE_PARAM, this);
    }
    else  {
      entry.start->SetEnabled(kFALSE);
      entry.kill->SetEnabled(kTRUE);
      entry.status->SetText(TaskManager::TASK_RUNNING);
      entry.status->SetBackgroundColor(green);
    }
  }
  gClient->NeedRedraw(entry.status);
}

void testbeam::NodeFSMPanel::commandChanged(int value)   {
  std::string sel = _selectedText(ctrl_command);
  GuiMsg("Received command request [%d]: %s", value, sel.c_str()).send(gui);
  if ( sel == "launch" )
    taskHandler.controller_start();
  else
    issueCommand(sel);
}

/// Get the controller running in auto mode
void testbeam::NodeFSMPanel::autoStart()   {
  std::string state = ctrl_status->GetText();
  GuiMsg("Check autostart in state: %s",state.c_str()).send(gui);
  m_autoRun = CTRL_AUTOSTART;
  if ( state == "DEAD" )
    taskHandler.controller_start();
  else if ( state == "OFFLINE" )
    issueCommand("load");
  else if ( state == "NOT_READY" )
    issueCommand("configure");
  else if ( state == "READY" )
    issueCommand("start");
  else if ( state == "RUNNING" )
    m_autoRun = 0;
}

/// Get the controller offline in auto mode
void testbeam::NodeFSMPanel::autoStop()   {
  std::string state = ctrl_status->GetText();
  GuiMsg("Check autostart in state: %s",state.c_str()).send(gui);
  m_autoRun = CTRL_AUTOSTOP;
  if ( state == "DEAD" )
    m_autoRun = 0;
  else if ( state == "OFFLINE" )
    issueCommand("destroy");
  else if ( state == "NOT_READY" )
    issueCommand("unload");
  else if ( state == "READY" )
    issueCommand("reset");
  else if ( state == "RUNNING" )
    issueCommand("stop");
}

void testbeam::NodeFSMPanel::runnumberChanged(const char* value)   {
  std::string runno = value;
  int run = 0;
  if ( 1 == ::sscanf(runno.c_str(),"%d",&run) )  {
    m_currentRun = run;
    ::dis_update_service(m_runnumber_id);
    return;
  }
  GuiException(kMBOk,kMBIconAsterisk,"Invalid Value",
	       "The given run number is invalid.\n"
	       "Current run is: %d\n", m_currentRun).send(gui);
}

void testbeam::NodeFSMPanel::architectureChanged(const char* value)   {
  taskManager.setArchitecture(value);
  if ( arch.input )  {
    arch.input->SetText(taskManager.architecture().c_str());
    gClient->NeedRedraw(arch.input);
  }
  taskManager.updateDependentValues();
}

void testbeam::NodeFSMPanel::scriptChanged(const char* value)   {
  taskManager.setCtrlScript(value);
  if ( script.input )  {
    script.input->SetText(taskManager.ctrlScript().c_str());
    gClient->NeedRedraw(script.input);
  }
  taskManager.updateDependentValues();
}

void testbeam::NodeFSMPanel::partitionChanged(const char* value)   {
  GuiMsg("New partition name: %s",value).send(gui);
  taskManager.setPartition(value);
  if ( part.input )  {
    part.input->SetText(taskManager.partition().c_str());
    gClient->NeedRedraw(part.input);
  }
  taskManager.updateDependentValues();
  if ( 0 != m_runnumber_id ) ::dis_remove_service(m_runnumber_id);
  auto svc = taskManager.partition()+"/RunInfo/RunNumber";
  m_runnumber_id = ::dis_add_service(svc.c_str(),"I",0,0,run_no_update,(long)this);
  ::dis_start_serving(guiserv_name().c_str());
  ::dis_update_service(m_runnumber_id);
}

void testbeam::NodeFSMPanel::replacementChanged(const char* value)   {
  taskManager.setReplacements(value);
  if ( replace.input )  {
    replace.input->SetText(taskManager.replacements().c_str());
    gClient->NeedRedraw(replace.input);
  }
  taskManager.updateDependentValues();
}

void testbeam::NodeFSMPanel::hostChanged(const char* value)   {
  taskManager.setNodeName(value);
  if ( node.input )  {
    node.input->SetText(taskManager.nodeName().c_str());
    gClient->NeedRedraw(node.input);
  }
  taskManager.updateDependentValues();
}

void testbeam::NodeFSMPanel::dnsChanged(const char* value)   {
  taskManager.setDnsName(value);
  if ( dns.input )   {
    dns.input->SetText(taskManager.dnsNode().c_str());
    gClient->NeedRedraw(dns.input);
  }
  taskManager.updateDependentValues();
}

void testbeam::NodeFSMPanel::mainDnsChanged(const char* value)    {
  taskManager.setMainDNS(value);
  if ( maindns.input )   {
    maindns.input->SetText(taskManager.mainDNS().c_str());
    gClient->NeedRedraw(maindns.input);
  }
  taskManager.updateDependentValues();
}

void testbeam::NodeFSMPanel::runInfoChanged(const char* value)   {
  taskManager.setRuninfo(value);
#if 0
  if ( runinfo.input )   {
    runinfo.input->SetText(taskManager.runinfo().c_str());
    gClient->NeedRedraw(runinfo.input);
  }
  taskManager.updateDependentValues();
#endif
}

void testbeam::NodeFSMPanel::setNumberOfSlaves(int value)   {
  m_numSlaves = value;
  if ( numslave.input )  {
    std::stringstream str;
    str << m_numSlaves;
    taskManager.setNumSlaves(m_numSlaves);
    numslave.input->Disconnect("TextChanged(const char*)");
    numslave.input->SetText(str.str().c_str());
    gClient->NeedRedraw(numslave.input);
    numslave.input->Connect("TextChanged(const char*)", "testbeam::NodeFSMPanel", this, "numslaveChanged(const char*)");
    GuiMsg("Number of slaves changed and is now: %d", m_numSlaves).send(gui);
  }
}

void testbeam::NodeFSMPanel::numslaveChanged(const char* value)    {
  ::sscanf(value,"%ld",&m_numSlaves);
  taskManager.setNumSlaves(m_numSlaves);
  GuiMsg("Number of slaves changed and is now: %d", m_numSlaves).send(gui);
}

/// Interactor interrupt handler callback
void testbeam::NodeFSMPanel::handle(const CPP::Event& ev)   {
  switch(ev.eventtype) {
  case IocEvent: {
    switch(ev.type)   {
    case TaskManager::TMSRV_LIST:
      updateTaskStatus(logViewer, taskManager.logViewState());
      updateTaskStatus(mbmmon,    taskManager.mbmmonState());
      break;

    case TaskManager::LOGSRV_STATUS:
      updateTaskStatus(logSrv, taskManager.logSrvState());
      break;

    case TaskManager::TMSRV_STATUS:
      updateTaskStatus(tmSrv, taskManager.tmSrvState());
      break;

    case TaskManager::LOGVIEWER_STATUS:
      updateTaskStatus(logViewer, taskManager.logViewState());
      break;

    case TaskManager::MBMMON_STATUS:
      updateTaskStatus(mbmmon, taskManager.mbmmonState());
      break;

    case TaskManager::CTRL_TASKS:
      updateChildren();
      break;

    case TaskManager::CTRL_STATUS:   {
      GuiMsg("New Controller status: %s",taskManager.ctrlState().c_str()).send(gui);
      updateTaskStatus(ctrl, taskManager.ctrlState());
      if ( m_autoRun && taskManager.ctrlState() != TaskManager::TASK_DEAD )
	IocSensor::instance().send(this, m_autoRun, this);
      updateCtrlCommand(taskManager.ctrlState());
      break;
    }

    case TaskManager::CMD_UPDATE_DEPENDENT_VALUES:
      updateDependentValues();
      break;

    case CTRL_AUTOSTART:
      autoStart();
      break;

    case CTRL_AUTOSTOP:
      autoStop();
      break;

    case CHECK_ENABLE_PARAM:
      break;

    default:
      break;
    }
    break;
  }
  case TimeEvent: {
    long cmd = long(ev.timer_data);
    switch(cmd) {
    default:
      break;
    }
    break;
  }
  default:
    break;
  }
}
