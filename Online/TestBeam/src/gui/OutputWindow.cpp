//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : TestBeam-GUI
//
//  Author     : Markus Frank
//==========================================================================

/// Framework include files
#include <TestBeam/gui/OutputWindow.h>
#include <TestBeam/gui/GuiCommand.h>
#include <TestBeam/gui/GuiMsg.h>
#include <CPP/Event.h>

// ROOT include files
#include <TApplication.h>

// C/C++ include files
#include <memory>

ClassImp(testbeam::OutputWindow)

#define MAX_LINE_COUNT 300

/// Standard initializing constructor
testbeam::OutputWindow::OutputWindow(TGWindow* p)
  : TGDockableFrame(p,-1,kVerticalFrame)
{
  Pixel_t background, foreground;
  TGLayoutHints* hints = new TGLayoutHints(kLHintsCenterX|kLHintsCenterY|kLHintsExpandX|kLHintsExpandY,0,0,0,0);
  gClient->GetColorByName("#c0c0c0", background);
  gClient->GetColorByName("#000000", foreground);
  group = new TGGroupFrame(this, "Output Logger");
  view  = new TGTextView(group, 500, 94, 99, kFixedWidth | kFixedHeight);
  view->SetBackground(background);
  view->SetForegroundColor(foreground);
  group->AddFrame(view, hints);
  AddFrame(group, hints);
  EnableUndock(kTRUE);
  EnableHide(kFALSE);
  Connect("Undocked()", "testbeam::OutputWindow", this, "handleDocking()");
  //exit = new TGTextButton(this, "&Close", -1);
  //exit->Connect("Clicked()", "TApplication", gApplication, "Terminate()");
  //AddFrame(exit, new TGLayoutHints(kLHintsRight,3,3,3,3));
  Resize(GetDefaultSize().fWidth,200);
}


/// Default destructor
testbeam::OutputWindow::~OutputWindow()   {
}

/// Interactor interrupt handler callback
void testbeam::OutputWindow::handle(const CPP::Event& ev)    {
  switch(ev.eventtype) {
  case IocEvent: {
    switch(ev.type) {
    case GUI_SHOW_OUTPUTLINE:  {
      std::unique_ptr<std::string> c(ev.iocPtr<std::string>());
      if ( !c->empty() )   {
	TGText* text = view->GetText();
	view->AddLine(c->c_str());
	std::printf("%ld: %s\n",text->RowCount(),c->c_str());
	if ( text->RowCount()%10 == 0 )  {
	  IocSensor::instance().send(this,GUI_SHOW_OUTPUTBOTTOM);
	}
      }
      return;
    }
    case GUI_SHOW_OUTPUTBOTTOM:
      view->ShowBottom();
      return;
    case GUI_UPDATE_OUTPUT:
      view->Update();
      return;
    default:
      break;
    }
    std::printf("OutputWindow::handle. Unhandled IOC event: %d\n",ev.type);
    return;
  }
  case TimeEvent: {
    long cmd = long(ev.timer_data);
    switch(cmd) {
    default:
      break;
    }
    break;
  }
  default:
    break;
  }
  GuiMsg("OutputWindow::handle. Unhandled event: %d\n",ev.eventtype).send(this);
}

/// Handle docking and other signals
void testbeam::OutputWindow::handleDocking()   {
  this->UndockContainer();
}
