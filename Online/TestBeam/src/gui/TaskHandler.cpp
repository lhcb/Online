//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : TestBeam-GUI
//
//  Author     : Markus Frank
//==========================================================================

/// Framework include files
#include <TestBeam/gui/TaskHandler.h>
#include <TestBeam/gui/TaskManager.h>

#include <CPP/IocSensor.h>
#include <CPP/Event.h>

ClassImp(testbeam::TaskHandler)

/// Standard initializing constructor
testbeam::TaskHandler::TaskHandler(TaskManager& tm) : taskManager(tm)
{
}

/// Kill all DAQ processes
void testbeam::TaskHandler::kill_all_tasks()   {
  IocSensor::instance().send(&taskManager, TaskManager::TMSRV_KILLALL, this);
}

/// Start tmSrv process
void testbeam::TaskHandler::tmSrv_start()   {
  IocSensor::instance().send(&taskManager, TaskManager::TMSRV_START, this);
}

/// Kill tmSrv process
void testbeam::TaskHandler::tmSrv_kill()   {
  IocSensor::instance().send(&taskManager, TaskManager::TMSRV_KILL, this);
}

/// Start local DNSSRV process
void testbeam::TaskHandler::dnsSrv_start()   {
  IocSensor::instance().send(&taskManager, TaskManager::DNSSRV_START, this);
}

/// Kill dnsSrv process
void testbeam::TaskHandler::dnsSrv_kill()   {
  IocSensor::instance().send(&taskManager, TaskManager::DNSSRV_KILL, this);
}

/// Start logSrv process
void testbeam::TaskHandler::logSrv_start()   {
  IocSensor::instance().send(&taskManager, TaskManager::LOGSRV_START, this);
}

/// Kill logSrv process
void testbeam::TaskHandler::logSrv_kill()   {
  IocSensor::instance().send(&taskManager, TaskManager::LOGSRV_KILL, this);
}

/// Start controller process
void testbeam::TaskHandler::controller_start()   {
  IocSensor::instance().send(&taskManager, TaskManager::CTRL_START, this);
}

/// Kill controller process
void testbeam::TaskHandler::controller_kill()   {
  IocSensor::instance().send(&taskManager, TaskManager::CTRL_KILL, this);
}

/// Start logViewer process
void testbeam::TaskHandler::logViewer_start()   {
  IocSensor::instance().send(&taskManager, TaskManager::LOGVIEWER_START, this);
}

/// Kill logViewer process
void testbeam::TaskHandler::logViewer_kill()   {
  IocSensor::instance().send(&taskManager, TaskManager::LOGVIEWER_KILL, this);
}

/// Start mbmmon process
void testbeam::TaskHandler::mbmmon_start()   {
  IocSensor::instance().send(&taskManager, TaskManager::MBMMON_START, this);
}

/// Kill mbmmon process
void testbeam::TaskHandler::mbmmon_kill()   {
  IocSensor::instance().send(&taskManager, TaskManager::MBMMON_KILL, this);
}

/// Start mbmdmp process
void testbeam::TaskHandler::mbmdmp_start()   {
  IocSensor::instance().send(&taskManager, TaskManager::MBMDMP_START, this);
}

/// Kill mbmdmp process
void testbeam::TaskHandler::mbmdmp_kill()   {
  IocSensor::instance().send(&taskManager, TaskManager::MBMDMP_KILL, this);
}

/// Start storage process
void testbeam::TaskHandler::storage_start()   {
  IocSensor::instance().send(&taskManager, TaskManager::STORAGE_START, this);
}

/// Kill storage process
void testbeam::TaskHandler::storage_kill()   {
  IocSensor::instance().send(&taskManager, TaskManager::STORAGE_KILL, this);
}

/// Start tanSrv process
void testbeam::TaskHandler::tanSrv_start()   {
  IocSensor::instance().send(&taskManager, TaskManager::TANSRV_START, this);
}

/// Kill tanSrv process
void testbeam::TaskHandler::tanSrv_kill()   {
  IocSensor::instance().send(&taskManager, TaskManager::TANSRV_KILL, this);
}

/// Start local DID process
void testbeam::TaskHandler::did_start()   {
  IocSensor::instance().send(&taskManager, TaskManager::DID_START, this);
}

/// Kill did process
void testbeam::TaskHandler::did_kill()   {
  IocSensor::instance().send(&taskManager, TaskManager::DID_KILL, this);
}
