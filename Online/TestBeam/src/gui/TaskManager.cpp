//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Author     : Markus Frank
//==========================================================================
//
//      Finite state machine implementation to control
//      and manipulate process groups
//
//==========================================================================

// Framework include files
#include <TestBeam/gui/config.h>
#include <TestBeam/gui/TaskManager.h>
#include <TestBeam/gui/GuiException.h>
#include <TestBeam/gui/GuiCommand.h>
#include <TestBeam/gui/GuiMsg.h>
#include <CPP/Event.h>
#include <RTL/strdef.h>
#include <RTL/rtl.h>
#include <dim/dic.h>
#include <dim/dim.h>

#include <TSystem.h>

// C/C++ include files
#include <map>
#include <sstream>
#include <cstdarg>
#include <sys/stat.h>
#include <unistd.h>

namespace {

  std::string _tmKill()   {
    struct stat stat;
    if ( 0 == ::stat("/opt/FMC/bin/tmKill", &stat) )
      return "LD_LIBRARY_PATH=/opt/FMC/lib /opt/FMC/bin/tmKill";
    if ( 0 == ::stat("/usr/local/bin/tmKill", &stat) )
      return "LD_LIBRARY_PATH=NONE /usr/local/bin/tmKill";
    std::printf("No tmKill command found -- check node setup! Using default: 'tmKill'\n");
    return "tmKill";
  }
  std::string _tmStart()   {
    struct stat stat;
    if ( 0 == ::stat("/opt/FMC/bin/tmStart", &stat) )
      return "LD_LIBRARY_PATH=/opt/FMC/lib   /opt/FMC/bin/tmStart";
    if ( 0 == ::stat("/usr/local/bin/tmStart", &stat) )
      return "LD_LIBRARY_PATH=NONE /usr/local/bin/tmStart";
    std::printf("No tmStart command found -- check node setup! Using default: 'tmStart'\n");
    return "tmStart";
  }
  std::string _tmSrv()   {
    struct stat stat;
    if ( 0 == ::stat("/opt/FMC/sbin/tmSrv", &stat) )
      return "-DLD_LIBRARY_PATH=/opt/FMC/lib /opt/FMC/sbin/tmSrv";
    if ( 0 == ::stat("/usr/local/sbin/tmSrv", &stat) )
      return "/usr/local/sbin/tmSrv";
    std::printf("No tmSrv command found -- check node setup! Using default: 'tmSrv'\n");
    return "tmSrv";
  }
  /// DIM command service callback
  void dim_feed(void* tag, void* address, int* size) {
    testbeam::TaskManager::dim_info_t* h = *(testbeam::TaskManager::dim_info_t**)tag;
    if ( address && *size )    {
      if ( h->fmt == 'A' )
	h->data = (char*)address;
      else
	h->data = "ALIVE";
    }
    else  {
      h->data = testbeam::TaskManager::TASK_DEAD;
    }
    IocSensor::instance().send(h->recipient, h->replyID, h);
  }
  /// DIM command service callback
  void dim_task_list(void* tag, void* address, int* size) {
    testbeam::TaskManager::dim_info_t* h = *(testbeam::TaskManager::dim_info_t**)tag;
    if ( address && *size )    {
      std::vector<char>* data = new std::vector<char>();
      char* ptr = (char*)address;
      data->reserve(*size+1);
      data->insert(data->end(), ptr, ptr + *size);
      IocSensor::instance().send(h->recipient, h->replyID, data);
    }
  }

  bool s_debug = true;
  int _execDim(long domain, const std::string& datapoint, const char* fmt, ...)   {
    va_list args;
    va_start( args, fmt);
    char str[4096];
    std::size_t len = ::vsnprintf(str,sizeof(str),fmt,args);
    va_end (args);
    str[len]=0;
    if ( s_debug )   {
      std::printf("Domain:%ld Service:%s  Cmd:%s\n",domain, datapoint.c_str(),str);
      ::fflush(stdout);
    }
    int ret = ::dic_cmnd_service_dns(domain, (char*)datapoint.c_str(), str, len+1);
    if ( ret == 1 )  {
      return 1;
    }
    return 0;
  }
}

testbeam::TaskManager::dim_info_t::~dim_info_t()   {
}

void testbeam::TaskManager::dim_info_t::dic_release()   {
  if ( svcID != 0 )   {
    ::dic_release_service(svcID);
  }
}

void testbeam::TaskManager::dim_info_t::dic_subscribe(long domain, const std::string& name, void (*cb)(void*, void*, int*))   {
  this->svcID = ::dic_info_service_dns(domain, name.c_str(),MONITORED,1,0,0,cb,long(this),0,0);
}

template <typename T> T testbeam::TaskManager::safe_copy(const T& obj)  const   {
  std::lock_guard<std::mutex> lock(data_protection());
  T copy = obj;
  return copy;
}

/// Standard constructor
testbeam::TaskManager::TaskManager(CPP::Interactor* gui)
{
  internals = std::make_unique<internals_t>();
  internals->gui = gui;
}

void testbeam::TaskManager::init(RTL::CLI& opts)    {
  std::string value;
  m_tmSrv   = std::make_unique<task_t>(TMSRV_STATUS);
  m_tanSrv  = std::make_unique<task_t>(TANSRV_STATUS);
  m_logSrv  = std::make_unique<task_t>(LOGSRV_STATUS);
  m_logView = std::make_unique<task_t>(LOGVIEWER_STATUS);
  m_mbmmon  = std::make_unique<task_t>(MBMMON_STATUS);
  m_mbmdmp  = std::make_unique<task_t>(MBMDMP_STATUS);
  m_storage = std::make_unique<task_t>(STORAGE_STATUS);
  m_did     = std::make_unique<task_t>(DID_STATUS);
  m_dns     = std::make_unique<task_t>(DNSSRV_STATUS);
  m_ctrl    = std::make_unique<task_t>(CTRL_STATUS);

  opts.getopt("dns",         3, value);
  setDnsName(value);
  opts.getopt("maindns",     3, value);
  setMainDNS(value);
  opts.getopt("node",        2, value);
  setNodeName(value);
  opts.getopt("user",        2, value);
  setUserName(value);
  opts.getopt("instances",   2, m_numSlaves);
  opts.getopt("runinfo",     2, value);
  setRuninfo(value);
  opts.getopt("partition",   2, value);
  setPartition(value);
  opts.getopt("architecture",2, value);
  setArchitecture(value);
  opts.getopt("startup",     2, m_startup);
  opts.getopt("controller",  2, m_ctrlType);
  opts.getopt("ctrl_script", 9, m_ctrlScript);
  opts.getopt("replacements",9, m_replacements);
  m_ctrlType = CONTROLLER_TYPE;

  m_tmSrv->state   = TASK_DEAD;
  m_logSrv->state  = TASK_DEAD;
  m_tanSrv->state  = TASK_DEAD;
  m_logView->state = TASK_DEAD;
  m_mbmmon->state  = TASK_DEAD;
  m_mbmdmp->state  = TASK_DEAD;
  m_storage->state = TASK_DEAD;
  m_did->state     = TASK_DEAD;
  m_dns->state     = TASK_DEAD;
  m_ctrl->state    = TASK_DEAD;
  updateDependentValues();

#if 0
  m_start = "/FMC/"+n+"/task_manager/start";
  m_stop  = "/FMC/"+n+"/task_manager/stop";
  m_kill  = "/FMC/"+n+"/task_manager/kill";
#endif
}

std::vector<testbeam::TaskManager::task_state_t> testbeam::TaskManager::ctrlTasks()  const   {
  return safe_copy(internals->controller_tasks);
}

std::string testbeam::TaskManager::tmSrvName()     const  {
  return safe_copy(m_tmSrv->name);    
}

std::string testbeam::TaskManager::tanSrvName()   const  {
  return safe_copy(m_tanSrv->name);   
}

std::string testbeam::TaskManager::logSrvName()    const  {
  return safe_copy(m_logSrv->name);   
}

std::string testbeam::TaskManager::logViewName()   const  {
  return safe_copy(m_logView->name);  
}

std::string testbeam::TaskManager::mbmmonName()    const  {
  return safe_copy(m_mbmmon->name);   
}

std::string testbeam::TaskManager::mbmdmpName()   const  {
  return safe_copy(m_mbmdmp->name);  
}

std::string testbeam::TaskManager::storageName()   const  {
  return safe_copy(m_storage->name);  
}

std::string testbeam::TaskManager::ctrlName()      const  {
  return safe_copy(m_ctrl->name);     
}

std::string testbeam::TaskManager::dnsSrvName()       const  {
  return safe_copy(m_dns->name);      
}

std::string testbeam::TaskManager::didName()       const  {
  return safe_copy(m_did->name);      
}

std::string testbeam::TaskManager::tmSrvState()    const  {
  return safe_copy(m_tmSrv->state);   
}

std::string testbeam::TaskManager::tanSrvState()   const  {
  return safe_copy(m_tanSrv->state);   
}

std::string testbeam::TaskManager::logSrvState()   const  {
  return safe_copy(m_logSrv->state);  
}

std::string testbeam::TaskManager::logViewState()  const  {
  return safe_copy(m_logView->state); 
}

std::string testbeam::TaskManager::mbmmonState()   const  {
  return safe_copy(m_mbmmon->state);  
}

std::string testbeam::TaskManager::mbmdmpState()   const  {
  return safe_copy(m_mbmdmp->state);  
}

std::string testbeam::TaskManager::storageState()  const  {
  return safe_copy(m_storage->state); 
}

std::string testbeam::TaskManager::ctrlState()     const  {
  return safe_copy(m_ctrl->state);    
}

std::string testbeam::TaskManager::didState()      const  {
  return safe_copy(m_did->state);     
}

std::string testbeam::TaskManager::dnsSrvState()      const  {
  return safe_copy(m_dns->state);     
}

void testbeam::TaskManager::setNodeName(const std::string& val)   {
  std::lock_guard<std::mutex> lock(data_protection());
  m_node = RTL::str_upper(val);
}

void testbeam::TaskManager::setUserName(const std::string& val)   {
  std::lock_guard<std::mutex> lock(data_protection());
  m_user = val;
}

void testbeam::TaskManager::setMainDNS(const std::string& val)   {
  std::lock_guard<std::mutex> lock(data_protection());
  m_mainDNS = RTL::str_upper(val);
}

void testbeam::TaskManager::setDnsName(const std::string& val)   {
  std::lock_guard<std::mutex> lock(data_protection());
  m_dnsNode = RTL::str_upper(val);
}

void testbeam::TaskManager::setPartition(const std::string& val)   {
  std::lock_guard<std::mutex> lock(data_protection());
  m_partition = val;
}

void testbeam::TaskManager::setRuninfo(const std::string& val)   {
  std::lock_guard<std::mutex> lock(data_protection());
  m_runinfo = gSystem->ExpandPathName(val.c_str());
}

void testbeam::TaskManager::setCtrlScript(const std::string& val)   {
  std::lock_guard<std::mutex> lock(data_protection());
  m_ctrlScript = val;
}

void testbeam::TaskManager::setReplacements(const std::string& val)   {
  std::lock_guard<std::mutex> lock(data_protection());
  m_replacements = gSystem->ExpandPathName(val.c_str());
}

void testbeam::TaskManager::setArchitecture(const std::string& val)   {
  std::lock_guard<std::mutex> lock(data_protection());
  m_architecture = gSystem->ExpandPathName(val.c_str());
}

void testbeam::TaskManager::subscribe(int facility, CPP::Interactor* target, const void* param)   {
  if ( target )    {
    std::lock_guard<std::mutex> lock(data_protection());
    auto& entry = internals->subscriptions[facility];
    entry[target] = param;
  }
}

std::mutex& testbeam::TaskManager::data_protection()  const   {
  return internals->dim_protection;
}

void testbeam::TaskManager::updateTaskStatus(task_t& task)   {
  std::lock_guard<std::mutex> lock(data_protection());
  task.state = task.dim.data;
  IocSensor::instance().send(this, CMD_HANDLE_SUBSCRIPTION, task.identifier);
}

void testbeam::TaskManager::updateSubscriptions(long facility)   {
  auto iter = internals->subscriptions.find(facility);
  if ( iter != internals->subscriptions.end() )   {
    for(auto& entry : iter->second )   {
      CPP::Interactor* actor = entry.first;
      IocSensor::instance().send(actor, facility, long(entry.second));
    }
  }
}

/// Interactor interrupt handler callback
void testbeam::TaskManager::handle(const CPP::Event& ev)   {
  switch(ev.eventtype) {
  case IocEvent: {
    switch(ev.type) {
    case CMD_UPDATE_DEPENDENT_VALUES:
      doUpdateDependentValues();
      break;

    case CMD_HANDLE_SUBSCRIPTION:  {
      std::lock_guard<std::mutex> lock(data_protection());
      updateSubscriptions((long)ev.data);
      break;
    }

    case TMSRV_LIST:   {
      std::unique_ptr<std::vector<char> > data(ev.iocPtr<std::vector<char> >());
      std::lock_guard<std::mutex> lock(data_protection());
      const char* tasks = &data->at(0);
      std::size_t len = data->size();
      std::map<std::string, std::pair<task_t*, std::string> > status;
      status[m_tanSrv->name]  = std::make_pair(m_tanSrv.get(),  m_tanSrv->state);
      status[m_logView->name] = std::make_pair(m_logView.get(), m_logView->state);
      status[m_mbmmon->name]  = std::make_pair(m_mbmmon.get(),  m_mbmmon->state);
      status[m_mbmdmp->name]  = std::make_pair(m_mbmdmp.get(),  m_mbmdmp->state);
      status[m_did->name]     = std::make_pair(m_did.get(),     m_did->state);
      for( auto& s : status ) s.second.first->state = TASK_DEAD;
      for( const char* t = tasks; t != nullptr && t<tasks+len; )   {
	if ( internals->use_short_list )   {
	  auto i = status.find(t);
	  if ( i != status.end() )
	    i->second.first->state = TASK_RUNNING;
	}
	else  {
	  const char *utgid = 0, *state = 0; // *pid, *command, *date, *flags;
	  //if ( t ) pid     = t;
	  if ( t ) /* command = */ t = ::strchr(t+1, '\0');
	  if ( t ) utgid   = t = ::strchr(t+1, '\0');
	  if ( t ) /* date    = */ t = ::strchr(t+1, '\0');
	  if ( t ) state   = t = ::strchr(t+1, '\0');
	  if ( t ) /* flags   = */ t = ::strchr(t+1, '\0');
	  if ( utgid && state )  {
	    auto i = status.find(utgid);
	    if ( i != status.end() )   {
	      bool running    = ::strstr(state,"running") != nullptr;
	      bool terminated = ::strstr(state,"terminated") != nullptr;
	      if ( running )   {
		i->second.first->state = TASK_RUNNING;
		i->second.first->meta  = "ALIVE";
	      }
	      else if ( terminated && i->second.first->state != TASK_RUNNING )   {
		i->second.first->state = TASK_DEAD;
		i->second.first->meta  = "LIMBO";
	      }
	    }
	  }
	}
	if ( t ) {
	  t = ::strchr(t, '\0');
	  ++t;
	}
      }
      for( auto& s : status )  {
	if ( s.second.second != s.second.first->state )   {
	  IocSensor::instance().send(this, CMD_HANDLE_SUBSCRIPTION, s.second.first->identifier);
	}
      }
      updateSubscriptions(TMSRV_LIST);
      break;
    }

    case TMSRV_STATUS:
      updateTaskStatus(*m_tmSrv);
      break;
    case TMSRV_START:
      tmSrv_start();
      break;
    case TMSRV_KILL:
      tmSrv_kill();
      break;
    case TMSRV_KILLALL:
      kill_all_tasks();
      break;

    case TANSRV_STATUS:
      updateTaskStatus(*m_tanSrv);
      break;
    case TANSRV_START:
      tanSrv_kill();
      break;
    case TANSRV_KILL:
      tanSrv_kill();
      break;

    case LOGSRV_STATUS:
      updateTaskStatus(*m_logSrv);
      break;
    case LOGSRV_START:
      logSrv_start();
      break;
    case LOGSRV_KILL:
      logSrv_kill();
      break;

    case LOGVIEWER_STATUS:
      updateTaskStatus(*m_logView);
      break;
    case LOGVIEWER_START:
      logViewer_start();
      break;
    case LOGVIEWER_KILL:
      logViewer_kill();
      break;

    case MBMMON_STATUS:
      updateTaskStatus(*m_mbmmon);
      break;
    case MBMMON_START:
      mbmmon_start();
      break;
    case MBMMON_KILL:
      mbmmon_kill();
      break;

    case MBMDMP_STATUS:
      updateTaskStatus(*m_mbmdmp);
      break;
    case MBMDMP_START:
      mbmdmp_start();
      break;
    case MBMDMP_KILL:
      mbmdmp_kill();
      break;

    case STORAGE_STATUS:
      updateTaskStatus(*m_storage);
      break;
    case STORAGE_START:
      storage_start();
      break;
    case STORAGE_KILL:
      storage_kill();
      break;

    case DID_STATUS:
      updateTaskStatus(*m_did);
      break;
    case DID_START:
      did_start();
      break;
    case DID_KILL:
      did_kill();
      break;

    case DNSSRV_STATUS:
      updateTaskStatus(*m_dns);
      break;
    case DNSSRV_START:
      dnsSrv_start();
      break;
    case DNSSRV_KILL:
      dnsSrv_kill();
      break;

    case CTRL_STATUS:
      updateTaskStatus(*m_ctrl);
      break;
    case CTRL_START:
      controller_start();
      break;
    case CTRL_KILL:
      controller_kill();
      break;

    case CTRL_TASKS:   {
      task_state_t task;
      std::vector<task_state_t> tasks;
      auto* r = ev.iocPtr<dim_info_t>();
      std::lock_guard<std::mutex> lock(data_protection());
      std::string data = r->data, proc;
      std::size_t idx = 0, idq = data.find('|',0), idd, ide, stop=0;
      if ( !data.empty() && data != TASK_DEAD )  {
	do   {
	  proc = data.substr(idx,idq-idx);
	  idx = idq + 1;
	  idd = proc.find('/');
	  task.name  = proc.substr(0,idd);
	  ide   = proc.find('/',idd+1);
	  task.state = proc.substr(idd+1, ide-idd-1);
	  task.meta = (ide != std::string::npos) ? proc.substr(ide + 1) : std::string();
	  tasks.emplace_back(std::move(task));
	  idq = data.find('|', idx);
	  if ( idq == std::string::npos && stop == 0 ) idq += idx, stop=1;
	}  while( idq != std::string::npos );
      }
      internals->controller_tasks = std::move(tasks);
      IocSensor::instance().send(this, CMD_HANDLE_SUBSCRIPTION, long(CTRL_TASKS));
      break;
    }

    default:
      break;
    }
    break;
  }
  case TimeEvent: {
    long cmd = long(ev.timer_data);
    switch(cmd) {
    default:
      break;
    }
    break;
  }
  default:
    break;
  }
}

void testbeam::TaskManager::updateDependentValues()   {
  IocSensor::instance().send(this, CMD_UPDATE_DEPENDENT_VALUES, this);
  IocSensor::instance().send(this, CMD_HANDLE_SUBSCRIPTION, long(CMD_UPDATE_DEPENDENT_VALUES));
}

void testbeam::TaskManager::doUpdateDependentValues()   {
  std::string n = "/"+nodeName()+"/";
  std::string np = n+partition()+"/";
  if ( np != internals->old_np )   {
    std::lock_guard<std::mutex> lock(data_protection());
    m_logFifo        = "/tmp/log"+m_partition+".fifo";
    m_tmSrv->name    = "/FMC"+n+"task_manager";
    m_logSrv->name   = np + "LOGS";
    m_tanSrv->name   = np + "TanServ";
    m_logView->name  = np + "LogViewer";
    m_mbmmon->name   = np + "MBMMon";
    m_mbmdmp->name   = np + "MBMDump";
    m_storage->name  = np + "Storage";
    m_did->name      = np + "DID";
    m_dns->name      = np + "DNS";
    m_ctrl->name     = m_partition + "_" + nodeName() + "_Controller";

    internals->tasks.clear();
    internals->tasks[m_tmSrv->name]   = m_tmSrv.get();
    internals->tasks[m_tanSrv->name]  = m_tanSrv.get();
    internals->tasks[m_logSrv->name]  = m_logSrv.get();
    internals->tasks[m_logView->name] = m_logView.get();
    internals->tasks[m_mbmmon->name]  = m_mbmmon.get();
    internals->tasks[m_mbmdmp->name]  = m_mbmdmp.get();
    internals->tasks[m_storage->name] = m_storage.get();
    internals->tasks[m_did->name]     = m_did.get();
    internals->tasks[m_dns->name]     = m_dns.get();
    internals->tasks[m_ctrl->name]    = m_ctrl.get();

    internals->mainDnsDIC = ::dic_add_dns(m_mainDNS.c_str(), ::dim_get_dns_port());
    internals->dnsDIC     = ::dic_add_dns(m_dnsNode.c_str(), ::dim_get_dns_port());

    internals->tmSrvList.dic_release();
    internals->tmSrvList = dim_info_t(TMSRV_LIST,   this, 'A');
    if ( internals->use_short_list )
      internals->tmSrvList.dic_subscribe(internals->dnsDIC, m_tmSrv->name+"/list",dim_task_list);
    else
      internals->tmSrvList.dic_subscribe(internals->dnsDIC, m_tmSrv->name+"/longList",dim_task_list);

    internals->ctrlTasks.dic_release();
    internals->ctrlTasks = dim_info_t(CTRL_TASKS,    this, 'A');
    internals->ctrlTasks.dic_subscribe(internals->dnsDIC, m_ctrl->name+"/tasks",dim_feed);

    m_tmSrv->dim.dic_release();
    m_tmSrv->state       = TASK_DEAD;
    m_tmSrv->dim         = dim_info_t(TMSRV_STATUS,  this, 'I');
    m_tmSrv->dim.dic_subscribe(internals->dnsDIC, m_tmSrv->name+"/success",dim_feed);

    m_logSrv->dim.dic_release();
    m_logSrv->state      = TASK_DEAD;
    m_logSrv->dim        = dim_info_t(LOGSRV_STATUS, this, 'I');
    m_logSrv->dim.dic_subscribe(internals->dnsDIC, m_logSrv->name+"/VERSION_NUMBER",dim_feed);

    m_storage->dim.dic_release();
    m_storage->state     = TASK_DEAD;
    m_storage->dim       = dim_info_t(STORAGE_STATUS, this, 'I');
    m_storage->dim.dic_subscribe(internals->dnsDIC, m_storage->name+"/VERSION_NUMBER",dim_feed);

    m_did->dim           = dim_info_t(DID_STATUS, this, 'I');

    m_dns->dim.dic_release();
    m_dns->state       = TASK_DEAD;
    m_dns->dim         = dim_info_t(DNSSRV_STATUS,  this, 'I');
    m_dns->dim.dic_subscribe(internals->dnsDIC,"DIS_DNS/VERSION_NUMBER",dim_feed);

    m_ctrl->dim.dic_release();
    m_ctrl->dim = dim_info_t(CTRL_STATUS,   this, 'A');
    m_ctrl->dim.dic_subscribe(internals->dnsDIC, m_ctrl->name + CONTROLLER_STATUS_OPT, dim_feed);

    internals->old_np = np;
  }
}

/// Start a process
int testbeam::TaskManager::fmc_start(const std::string& utgid, const std::string& fmc_args, const std::string& cmd, const std::string& args)  const {
  return _execDim(internals->dnsDIS,m_start,"-u %s %s %s %s",utgid.c_str(),fmc_args.c_str(),cmd.c_str(),args.c_str());
}
/// Kill a process
int testbeam::TaskManager::fmc_stop(const std::string& utgid, int sig_num, int wait_before_kill)  const {
  return _execDim(internals->dnsDIS,m_stop,"-s %d -d %d %s",sig_num,wait_before_kill,utgid.c_str());
}

/// Kill a process
int testbeam::TaskManager::fmc_kill(const std::string& utgid, int sig_num)  const {
  return _execDim(internals->dnsDIS,m_stop,"-s %d %s",sig_num,utgid.c_str());
}

std::string testbeam::TaskManager::killDefaults()  const   {
  std::string def = "-s 9 ";
  return def;
}

std::string testbeam::TaskManager::startDefaults()  const   {
  std::string def = "-E " + m_logFifo + " -O " + m_logFifo + " -n " + m_user + " ";
  return def;
}

/// Start tmSrv process
void testbeam::TaskManager::tmSrv_start()   {
  if ( m_tmSrv->state == TASK_DEAD )   {
    std::stringstream cmd;
    if ( m_mainDNS.empty() )  {
      cmd << "export DIM_DNS_NODE=" << m_node << ";export UTGID=" << m_tmSrv->name << ";"
	  << _tmSrv() << " -l 2 -N " << m_node << " -p 2 -u online -U root --no-auth --no-ptrace-workaround&";
      system(cmd.str().c_str());
    }
    else   {
      cmd << _tmStart() << " -N " << m_mainDNS << " -m " << m_node << " " << startDefaults()
	  << " -n root -g root -DDIM_DNS_NODE=" << m_node << " -u " << m_tmSrv->name << " "
	  << _tmSrv() << " -l 2 -N " << m_node << " -p 2 -u online -U root --no-auth --no-ptrace-workaround";
      system(cmd.str().c_str());
    }
    return;
  }
  GuiException(kMBOk,kMBIconAsterisk,"Invalid Request",
	       "The task manager is already running.\n"
	       "This request is ignored.").send(internals->gui);
}

/// Kill tmSrv process
void testbeam::TaskManager::tmSrv_kill()   {
  std::string cmd;
  if ( m_mainDNS.empty() )   {
    cmd = "pkill -9 tmSrv";
    system(cmd.c_str());
    return;
  }
  cmd = _tmKill() + " -N "+m_mainDNS+" -m "+m_node+" " + killDefaults() + m_tmSrv->name;
  system(cmd.c_str());
  //FiniteStateMachine::TaskManager(m_node, m_mainDNS_id).kill(m_tmSrv->name, 9);
}

/// Start logSrv process
void testbeam::TaskManager::logSrv_start()   {
  if ( m_logSrv->state == TASK_DEAD )   {
    std::stringstream cmd;
    if ( m_mainDNS.empty() )   {
      cmd << "export DIM_DNS_NODE="  << m_node    << "; "
	  << "export UTGID="         << m_logSrv->name << ";";
      cmd << "export LOGFIFO="       << m_logFifo << "; "
	  << "export DIM_HOST_NODE=" << m_node    << "; ";
      cmd << "/group/online/dataflow/scripts/NodeLogger.sh&";
      system(cmd.str().c_str());
    }
    else   {
      cmd << _tmStart() << " -N " << m_mainDNS << " -m " << m_node 
	<< " -e -o -n online -DDIM_DNS_NODE=" << m_node << " -u " << m_logSrv->name << " "
	<< "-DLOGFIFO="         << m_logFifo   << " "
	<< "-DDIM_HOST_NODE="   << m_node      << " "
	<< "-DPUBLISH_SERVICE=" << m_partition << "/LOGS "
	<< "/group/online/dataflow/scripts/NodeLogger.sh";
      system(cmd.str().c_str());
    }
    return;
  }
  GuiException(kMBOk,kMBIconAsterisk,"Invalid Request",
	       "The log server is already running.\n"
	       "This request is ignored.").send(internals->gui);
}

/// Kill logSrv process
void testbeam::TaskManager::logSrv_kill()   {
  std::string cmd;
  if ( m_mainDNS.empty() )
    cmd = "pkill -9 logSrv";
  else
    cmd = _tmKill() + " -N "+m_mainDNS+" -m "+m_node+" " + killDefaults() + m_logSrv->name;
  system(cmd.c_str());
}

/// Start controller process
void testbeam::TaskManager::controller_start()   {
  std::string state = m_ctrl->state;
  if ( state == TASK_DEAD )   {
    std::stringstream cmd;
    if ( m_startup.empty() )   {
      cmd << startDefaults();
      if ( ::getenv("BINARY_TAG") )   {
	cmd << " -DBINARY_TAG=" << ::getenv("BINARY_TAG");
      }
      cmd << " -u "         << m_ctrl->name
	  << "   "          << m_ctrlScript
	  << " -type="      << m_ctrlType
	  << " -partition=" << m_partition
	  << " -taskinfo="  << m_architecture
	  << " -count="     << m_numSlaves
	  << " -runinfo="   << m_runinfo
	  << " -logfifo="   << m_logFifo
	  << " -smidns="    << m_dnsNode
	  << " -tmsdns="    << m_dnsNode
	  << " -dimdns="    << m_dnsNode
	  << " -replacements=NUMBER_OF_SLAVES:" << m_numSlaves;
      if ( !m_replacements.empty() )   {
	cmd << "," << m_replacements;
      }
    }
    else   {
      cmd << startDefaults() << " -u " << m_ctrl->name << " " << m_startup;
    }
    startProcess(cmd.str());
    return;
  }
  GuiException(kMBOk,kMBIconAsterisk,"Invalid Request",
	       "The controller is already running.\n"
	       "Current state: %s\n"
	       "This request is ignored.", state.c_str()).send(internals->gui);
}

/// Kill controller process
void testbeam::TaskManager::controller_kill()   {
  std::string cmd = killDefaults() + m_ctrl->name;
  killProcess(cmd);
}

/// Start mbmmon process
void testbeam::TaskManager::mbmmon_start()   {
  std::stringstream cmd;
  cmd << "xterm  -ls -132 -geometry 132x65 -title \"" << m_mbmmon->name << "\" -e \""
      << "export UTGID=" << m_mbmmon->name << "; "
      << "exec -a " << m_mbmmon->name << " gentest libOnlineKernel.so mbm_mon_scr -p=" << m_partition
      << "\" &";
  system(cmd.str().c_str());
  std::printf("%s\n",cmd.str().c_str());
}

/// Kill mbmmon process
void testbeam::TaskManager::mbmmon_kill()   {
  std::stringstream cmd;
  cmd << killDefaults() << m_mbmmon->name;
  killProcess(cmd.str());
}

/// Start mbmdmp process
void testbeam::TaskManager::mbmdmp_start()   {
  std::stringstream cmd;
  cmd << ""
      << "xterm  -ls -132 -geometry 132x35 -title \"" << m_mbmdmp->name << "\" -e \""
      << "export UTGID=" << m_mbmdmp->name << "; exec -a mbm_dump mbm_dump"
      << " -name="      << "mbm_dump"
      << " -buffer="    << "Output_" << partition()
      << " -type="      << "MDF"
      << " -partition=" << partition()
      << "\" &";
  system(cmd.str().c_str());
  std::printf("%s\n",cmd.str().c_str());
}

/// Kill mbmdmp process
void testbeam::TaskManager::mbmdmp_kill()   {
  std::stringstream cmd;
  cmd << killDefaults() << m_mbmdmp->name;
  killProcess(cmd.str());
}

/// Start tanSrv process
void testbeam::TaskManager::tanSrv_start()   {
  std::stringstream cmd;
  cmd << "xterm -title \"" << m_tanSrv->name << "\" -geo 132x12 -e \""
      << "export UTGID="   << m_tanSrv->name << "; "
      << "exec -a " << m_tanSrv->name << " gentest libOnlineBase.so tan_nameserver -a -tcp -d -m"
      << "\" &";
  system(cmd.str().c_str());
  std::printf("%s\n",cmd.str().c_str());
}

/// Kill tanSrv process
void testbeam::TaskManager::tanSrv_kill()   {
  std::stringstream cmd;
  cmd << killDefaults() << m_tanSrv->name;
  killProcess(cmd.str());
}

/// Start storage process
void testbeam::TaskManager::storage_start()   {
  std::stringstream cmd;
#if 0
  cmd << "export UTGID=" << m_storage->name << ";"
      << "xterm  -ls -132 -geometry 132x35 -title \"" << m_storage->name << "\" -e \""
      << "export UTGID="   << m_storage->name << "; "
      << "exec -a StorageFS genRunner libStorageServer.so fdb_fs_file_server "
      << "-local=0.0.0.0:8100 "
      << "-server=xxeb09:8000 "
      << "-files=/daqarea1/objects "
      << "-logger=" << m_logFifo << " "
      << "-threads=6 -print=3 "
      << "\"&";
  system(cmd.str().c_str());
#endif
  cmd << startDefaults()
      << " -u "              << m_storage->name
      << " -D LOGFIFO="      << m_logFifo
      << " -D DIM_DNS_NODE=" << m_dnsNode
      << " /group/online/dataflow/scripts/StorageFS.sh";
  startProcess(cmd.str());
  std::printf("%s\n",cmd.str().c_str());
}

/// Kill storage process
void testbeam::TaskManager::storage_kill()   {
  std::stringstream cmd;
  cmd << killDefaults() << m_storage->name;
  killProcess(cmd.str());
}

/// Start dns process
void testbeam::TaskManager::dnsSrv_start()   {
  std::stringstream cmd;
  cmd << "xterm -title \"" << m_dns->name << "\" -geo 132x10 -e \""
      << "export DIM_DNS_NODE="   << m_dnsNode << "; "
      << "export UTGID="          << m_dns->name << "; "
      << "exec -a " << m_dns->name << " dns.exe"
      << "\" &";
  system(cmd.str().c_str());
  std::printf("%s\n",cmd.str().c_str());
}

/// Kill dns process
void testbeam::TaskManager::dnsSrv_kill()   {
  std::stringstream cmd;
  cmd << killDefaults() << m_dns->name;
  killProcess(cmd.str());
}

/// Start local DID process
void testbeam::TaskManager::did_start()   {
  std::stringstream cmd;
  cmd << "export UTGID=" << m_did->name << ";"
      << "exec -a " << m_did->name << " did.exe -dns=" << m_dnsNode << "&";
  system(cmd.str().c_str());
}

/// Kill did process
void testbeam::TaskManager::did_kill()   {
  std::stringstream cmd;
  cmd << killDefaults() << m_did->name;
  killProcess(cmd.str());
}

/// Start logViewer process
void testbeam::TaskManager::logViewer_start()   {
  std::stringstream cmd;
  cmd << "xterm -sl 20000 -ls -132 -geometry 230x50 -title \"" << m_logView->name << "\" -e \"";
  cmd << "export UTGID=" << m_logView->name << ";";
  cmd << "exec -a " << m_logView->name << " gentest libROLogger.so run_output_logger -C -N -O 1"
      << " -b " << m_node << " -S /" << m_dnsNode << "/" << m_partition << "/LOGS";
  cmd << "\" &";
  std::printf("%s\n",cmd.str().c_str());
  system(cmd.str().c_str());
}

/// Kill logViewer process
void testbeam::TaskManager::logViewer_kill()   {
  std::stringstream cmd;
  cmd << killDefaults() << m_logView->name;
  killProcess(cmd.str());
}

void testbeam::TaskManager::startProcess(const std::string& args)   {
  std::string dp = "/FMC/"+RTL::str_upper(m_node)+"/task_manager/start";
  int ret = ::dic_cmnd_service_dns(internals->dnsDIC, dp.c_str(),(char*)args.c_str(),args.length()+1);
  if ( ret == 1 )  {
    GuiMsg("Successfully started process: %s->%s",dp.c_str(),args.c_str()).send(internals->gui);
    return;
  }
  GuiMsg("FAILED to start process: %s->%s",dp.c_str(),args.c_str()).send(internals->gui);
}

void testbeam::TaskManager::killProcess(const std::string& args)   {
  std::string dp = "/FMC/"+RTL::str_upper(m_node)+"/task_manager/kill";
  int ret = ::dic_cmnd_service_dns(internals->dnsDIC, dp.c_str(),(void*)args.c_str(),args.length()+1);
  if ( ret == 1 )  {
    GuiMsg("Successfully killed process: %s->%s",dp.c_str(),args.c_str()).send(internals->gui);
    return;
  }
  GuiMsg("FAILED to kill process: %s->%s",dp.c_str(),args.c_str()).send(internals->gui);
}

void testbeam::TaskManager::kill_all_tasks()   {
  killProcess(killDefaults()+" "+m_partition+"_"+m_node+"_*");
}
