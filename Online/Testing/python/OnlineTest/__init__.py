#==========================================================================
#  LHCb Online software suite
#--------------------------------------------------------------------------
# Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
# All rights reserved.
#
# For the licensing terms see OnlineSys/LICENSE.
#
#--------------------------------------------------------------------------
import itertools
import logging
import os
import re
import shutil
import copy
from   collections import OrderedDict
from   itertools import takewhile
from   pathlib import Path
from   tempfile import NamedTemporaryFile

import GaudiConf.QMTest.QMTTest
from   GaudiConf.QMTest.BaseTest import FilePreprocessor
from   GaudiConf.QMTest.BaseTest import h_count_re as HISTO_SUMMARY_RE
from   GaudiConf.QMTest.BaseTest import (normalizeExamples,
                                     normalizeDate,
                                     normalizeEOL,
                                     maskPointers,
                                     skipEmptyLines,
                                     RegexpReplacer,
                                     LineSkipper,
                                     SortGroupOfLines,
                                     BlockSkipper)

# -------------------------------------------------------------------------------------
class DummyProcessor(FilePreprocessor):
  # -----------------------------------------------------------------------------------
  def __init__(self):
    pass
  # -----------------------------------------------------------------------------------
  def __processLine__(self, line):
    return line

# -------------------------------------------------------------------------------------
class SanSkipper(FilePreprocessor):
  # -----------------------------------------------------------------------------------
  def __init__(self, when):
    self.when = when
    self.regex = re.compile(self.when)
    self._skipping = False
    self.end = 0
  # -----------------------------------------------------------------------------------
  def __processLine__(self, line):
    if self.regex.search(line):
      self._skipping = True
      self.end = 0
      return None
    elif self._skipping and len(line)>0 and line.find('    #') < 0:
      self.end = self.end + 1
      if self.end > 1:
        self._skipping = False
      return line
    elif self._skipping:
      return None
    return line

# -------------------------------------------------------------------------------------
class BlockSkipper(FilePreprocessor):
  """ Skip all lines between `start` and `end`.

  Skips blocks of lines identified by a starting line containing the
  substring `start` and an ending line containing the substring `end`.
  Both the starting and the ending line are excluded from the output.
  If `being` is `None`, lines starting from the beginning are skipped.
  If `end` is `None`, all lines until the end are skipped.
  """
  # -----------------------------------------------------------------------------------
  def __init__(self, start, end=None):
    self.start = start
    self.end = end
  # -----------------------------------------------------------------------------------
  def __processFile__(self, lines):
    skipping = self.start is None
    output_lines = []
    for line in lines:
      if self.start is not None and self.start in line:
        skipping = True
      if not skipping:
        output_lines.append(line)
      if self.end is not None and self.end in line:
        skipping = False
    return output_lines
    
# -------------------------------------------------------------------------------------
mask_uuids = \
  RegexpReplacer("[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}",
                 "########-####-####-####-############")

# -------------------------------------------------------------------------------------
remove_universal_times = RegexpReplacer(
  "[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}.[0-9]{2,6}.[0-9]{4}",
  ""
) + RegexpReplacer(
  "[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2} ",
  " "
) + RegexpReplacer(
  "[a-zA-Z]{3}[0-9]{2}-[0-9]{6} ",
  " "
)
# -------------------------------------------------------------------------------------
mask_universal_times = RegexpReplacer(
  "[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}.[0-9]{2,6}.[0-9]{4}",
  "####-##-UNIVERSAL--TIME-####"
) + RegexpReplacer(
  "[0-9]{4}[0-9]{2}[0-9]{2}.[0-9]{2}[0-9]{2}[0-9]{2}.[0-9]{3}.",
  "####-##-## TIME-###.### "
) + RegexpReplacer(
  "[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2} ",
  "####-##-## TIME-### "
) + RegexpReplacer(
  "[a-zA-Z]{3}[0-9]{2}-[0-9]{6} ",
  "#####-#TIME# "
)
# -------------------------------------------------------------------------------------
mask_uris = RegexpReplacer(  # For HTTP tests with variable port numbesr
  when="http://localhost:",
  orig=r':\d\d\d+',
  repl=r':####',
) + RegexpReplacer(  # For HTTP tests with variable port numbers
  when="http://127.0.0.1:",
  orig=r':\d\d\d+',
  repl=r':####',
) + RegexpReplacer(  # For IP tests with variable port numbers
  when="-(.*)=127.0.0.1:",
  orig=r':\d\d\d+',
  repl=r':####',
  # Hide specific ODIN type
)
# -------------------------------------------------------------------------------------
mask_mbm = RegexpReplacer(orig=r'   SPR  ', repl=r'  #MBM# ' ) \
+ RegexpReplacer(orig=r'    ac  ', repl=r'  #MBM# ' ) \
+ RegexpReplacer(orig=r'   wev  ', repl=r'  #MBM# ' ) \
# -------------------------------------------------------------------------------------
mask_identifiers = RegexpReplacer("ID[0-9a-fA-F]{4,16}", "##ID##")

# -------------------------------------------------------------------------------------
stderr_preprocessor = (
  maskPointers +
  mask_identifiers +
  mask_mbm +
  mask_universal_times +
  mask_uuids +
  mask_uris +
  normalizeDate +
  normalizeEOL +
  skipEmptyLines +
  RegexpReplacer('"host":"[0-9a-zA-Z\.]{4,64}"', '"host":"#HOST#"') +
  RegexpReplacer('"physical_host":"[0-9a-zA-Z\.]{4,64}"', '"physical_host":"#HOST#"') +
  RegexpReplacer('"pid":"[0-9]{1,8}"', '"pid":"#PID#"') +
  # Skip certain sanitizer stuff from WT using position independent queues
  SanSkipper(
    when='(.*)/OnlineBase/src/WT/wtlib.cpp:(.*):(.*): runtime error: pointer index expression with base 0x(.*) overflowed to 0x(.*)'
  ) +
  SanSkipper(
    when='(.*)/rtl.h:(.*):(.*): runtime error: pointer index expression with base 0x(.*) overflowed to 0x(.*)'
  ) +
  SanSkipper(
    when='(.*)/wtlib.cpp:(.*):(.*): runtime error: pointer index expression with base 0x(.*) overflowed to 0x(.*)'
  ) +
  SanSkipper(
    when='(.*) runtime error: member access within misaligned address 0x(.*) for type (.*), which requires 4 byte alignment'
  ) +
  SanSkipper(
    when='(.*) 0x(.*) in lib_rtl_add_ptr (.*)/RTL/rtl.h'
  ) +
  SanSkipper(
    when='(.*) 0x(.*) in _M_invoke<0> (.*)/std_thread.h'
  ) +
  SanSkipper(
    when='(.*) 0x(.*) in operator.. (.*)/std_thread.h'
  ) +
  SanSkipper(
    when='(.*) 0x(.*) in _M_run (.*)/std_thread.h'
  )
)

# -------------------------------------------------------------------------------------
preprocessor = (
  copy.deepcopy(stderr_preprocessor) +
  RegexpReplacer(                  # hide release directories
    when="cern.ch",
    orig=(r'/afs/cern.ch/lhcb/software/(DEV/nightlies|releases)/'
          r'|/cvmfs/lhcb.cern.ch/lib/lhcb'),
    repl=r'',
  ) + RegexpReplacer(
    when="at 0x########L",
    orig=r'0x########L',
    repl=r'0x########',
  ) + RegexpReplacer(
    when="at 0x####L",
    orig=r'0x####L',
    repl=r'0x####',
  ) + RegexpReplacer(
    when="host:",
    orig=r'host:[0-9a-zA-Z]{4,16}',
    repl=r'host:#HOST#',
  ) + RegexpReplacer(
    when="port:",
    orig=r':\d\d\d+',
    repl=r':####',
  ) + RegexpReplacer(
    when="port=",
    orig=r'=\d\d\d+',
    repl=r'=####',
  ) + RegexpReplacer(
    when="pid:",
    orig=r':\d+',
    repl=r':####',
  ) + RegexpReplacer(
    when="pid=",
    orig=r'=\d+',
    repl=r'=####',
  ) + RegexpReplacer(
    when="StoreExplorerAlg",
    orig=r'LHCb::ODINImplementation::v7::OD',
    repl=r'LHCb::ODIN',
  ) + RegexpReplacer(  # normalize any timing prints
    when="INFO",
    orig=r'\d+\.*\d*(\s+(ns|us|ms|seconds|s|minutes|min)(\s+|\.|$))',
    repl=r'n\1',
  ) + RegexpReplacer(
    orig='Conversion service:',
    repl='Conversion service '
  ) + LineSkipper([
    "#properties =",
    "//GP:",
    "running on",
    "Welcome to",
    "Histograms saving not required.",
    "VERBOSE ServiceLocatorHelper::service: found service AlgExecStateSvc",
    "DEBUG Data Deps for ",
    "SUCCESS Number of counters : ",
    "INFO File 'ROOTIO.xml' does not exist. New file created.",
    "INFO Disconnect from dataset",
    "INFO Disconnected data IO",
    "INFO Referring to dataset",
    "INFO Number of counters : ",
    "INFO Time User   : Tot=",
    "+++ Successfully opened file: ",
    "+++ End of file: ",
    "+++ Closing input file: ",
    " --> restarting",
    "HTTP CacheDB RPC service [Debugging OFF] started on host",
    "[INFO] +++ UTGID:   ",
  ], regexps=[
    r"\s*\(ERROR\) Server Connecting to DIM_DNS",
    r"\s*\(INFO\) Server Connection established to DIM_DNS",
    r"\s*\(ERROR\) Client Connecting to DIM_DNS"
    r"\s*\(INFO\) Client Connection established to DIM_DNS",
    r"\s*DNS version 2033 starting up on",
    r"\[INFO\]\s*execute bash command: gentest",
    r"\[INFO\]\s*execute bash command: genRunner",
    r"\[INFO\]\s*execute bash command: python",
    r"\s*\[QMTEST:IGNORE_LINE\]",
    r"\s*Working directory:",
    r"ToolSvc.SequencerTimer...",
    r"JobOptionsSvc\s*INFO Job options successfully read",
    r"JobOptionsSvc\s*INFO Properties are dumped into",
    r"JobOptionsSvc\s*INFO # =======> ",
    r"JobOptionsSvc\s*WARNING # ",
    r" -runinfo=\s*",
    r"SUCCESS \dD histograms in directory",
    r"SUCCESS \dD profile histograms in directory",
    r"IODataManager\s*INFO Disconnect from dataset ROOTIO",
    r"[FATAL] Process:\s*(SignalHandler) RTL:Handled signal: 15 [SIGTERM] Old action",
  ])
)

# Exclude counters with
counter_preprocessor = LineSkipper([
  ' | "Delta Memory/MB" ',
  ' | "Total Memory/MB" ',
])

# -------------------------------------------------------------------------------------
def _get_new_ref_filename(reference_path):
  """Return the next available filename for a new reference."""
  count = 0
  newrefname = reference_path + '.new'
  while os.path.exists(newrefname):
    count += 1
    newrefname = '{}.~{}~.new'.format(reference_path, count)
    return newrefname
  
# size in bytes after which job option dumps are not added in results,
# see https://gitlab.cern.ch/lhcb/LHCb/-/issues/187
MAX_JOBOPTSDUMP_SIZE = 1024**2

# -----------------------------------------------------------------------------------
def sort_lines_ts(data):
  data = remove_universal_times(data)
  lll = data.split('\n')
  lines = [line for line in lll]
  lll = None
  lines.sort(reverse=True)
  ret = ''
  for line in lines:
    ret = ret + line + '\n'
  return ret
# -----------------------------------------------------------------------------------
def sort_lines_def(data):
  lines = data.split('\n')
  lines.sort(reverse=True)
  ret = ''
  for line in lines:
    ret = ret + line + '\n'
  return ret
# -----------------------------------------------------------------------------------
def log_noop(data):
  return data

# -------------------------------------------------------------------------------------
class Test(GaudiConf.QMTest.QMTTest.QMTTest):
  '''Extension of the original QMTTest of Gaudi adding checking of the counters and histograms'''
  
  # -----------------------------------------------------------------------------------
  def __init__(self, *args, **kwargs):
    """
            OnlineTest initialization
    """
    self.stderr_preprocessor = stderr_preprocessor
    self.required_strings = []
    self.emit_runtime   = False
    self.prerequisites  = ""
    self.validate_refs  = {}
    self.RegexpReplacer = RegexpReplacer
    self.LineSkipper    = LineSkipper
    self.SanSkipper     = SanSkipper
    self.resultDict     = {}
    super().__init__(*args, **kwargs)

  # -----------------------------------------------------------------------------------
  def current_working_dir(self):
    return os.getcwd()

  def disable_sanitizer(self):
    if os.environ['BINARY_TAG'].find('lsan')>0:
      self.err = ''
      self.returnedCode = 0
    return self
  # -----------------------------------------------------------------------------------
  def XMLParser(self, path):
    super().XMLParser(path)
    import xml.etree.ElementTree as ET
    tree = ET.parse(path)
    self.ignore_stdout = False
    self.ignore_stderr = False
    for child in tree.getroot():
      if child.attrib["name"] == "prerequisites":
        value = child[0]  # the <set></set>
        self.prerequisites = [
          el.find("text").text for el in value.findall("tuple")
        ]
      elif child.attrib["name"] == "ignore_stdout":
        self.ignore_stdout = True
      elif child.attrib["name"] == "ignore_stderr":
        self.ignore_stderr = True
      elif child.attrib["name"] == "required_string":
        self.required_strings.append(child[0].text)
      elif child.attrib["name"] == "validate_ref":
        self.validate_refs[child.attrib["log"]] = child.attrib["ref"]
      elif child.attrib["name"] == "emit_runtime":
        self.emit_runtime = True

  def collect_logs(self, match):
    import glob
    for fn in glob.glob(match):
      self.result[os.path.basename(fn)] = open(fn).read()
    return self

  # -----------------------------------------------------------------------------------
  def prepare_log(self, data, sorter=None, replace=None, preproc=None):
    if preproc:
      data = preproc(data)
    if replace:
      for k,v in replace.items():
        data = data.replace(k, v)
    if sorter:
      data = sorter(data)
    return data

  # -----------------------------------------------------------------------------------
  def config_sorter(self, sorter, sort_lines):
    #
    # Line sorting for multi-threading apps to avoid races
    if sort_lines:
      if sorter:
        print('validate_log: line sorting hint ignored. Already other line sorting algorithm specified')
      elif isinstance(sort_lines,str):
        sorter = sort_lines
        if sort_lines == 'remove_timestamp':
          sorter = sort_lines_ts
        elif sort_lines == 'sort_lines':
          sorter = sort_lines_def
        elif sort_lines == 'sort':
          sorter = sort_lines_def
      elif isinstance(sort_lines,bool) and sort_lines == True:
        sorter = sort_lines_def
      elif isinstance(sort_lines,int) and sort_lines > 0:
        sorter = sort_lines_def
      else:
        print('validate_log: line sorting hint ignored. Using default sorting')
      # By default alphabetical sorting if nothing else specified
      if not sorter:
        sorter = sort_lines_def
    return sorter

  # -----------------------------------------------------------------------------------
  def config_processor(self,
                       preproc=preprocessor,
                       optproc=None,
                       skip_lines=[],
                       skip_regex=[],
                       replace={}):
    #
    # Skip identified lines
    if skip_lines:
      if isinstance(skip_lines,str):
        skip_lines = [skip_lines]
      elif isinstance(skip_lines,list) or isinstance(skip_lines,tuple):
        skip_lines = [line for line in skip_lines]
    #
    # Skip identified regular expressions
    if skip_regex:
      if isinstance(skip_regex,str):
        skip_regex = [skip_regex]
      elif isinstance(skip_regex,list) or isinstance(skip_regex,tuple):
        skip_regex = [line for line in skip_regex]
    #
    # Build updated data preprocessor
    if skip_lines or skip_regex:
      preproc = preproc + LineSkipper(strings=skip_lines, regexps=skip_regex)
    #
    if optproc:
      if isinstance(optproc,list):
        for o in optproc:
          preproc = preproc + o
      elif isinstance(optproc,tuple):
        for o in optproc:
          preproc = preproc + o
      else:
        preproc = preproc + optproc
    #
    return preproc

  # -----------------------------------------------------------------------------------
  def validate_buffer(self, data, 
                      preproc=preprocessor,
                      optproc=None,
                      sorter=None,
                      skip_lines=[],
                      skip_regex=[],
                      sort_lines=None,
                      replace={}):
    sorter = self.config_sorter(sorter, sort_lines)
    proc   = self.config_processor(preproc=preprocessor,
                                   optproc=optproc,
                                   skip_lines=skip_lines,
                                   skip_regex=skip_regex,
                                   replace=replace)
    return self.validateWithReference(stdout=sorter(data), preproc=proc)

  # -----------------------------------------------------------------------------------
  def validate_log(self, key, 
                   preproc=preprocessor,
                   optproc=None,
                   sorter=None,
                   skip_lines=[],
                   skip_regex=[],
                   sort_lines=None,
                   replace={}):
    import errno
    res    = self.result.annotations
    proc   = self.config_processor(preproc=preprocessor,
                                   optproc=optproc,
                                   skip_lines=skip_lines,
                                   skip_regex=skip_regex,
                                   replace=replace)
    sorter = self.config_sorter(sorter, sort_lines)

    if key and isinstance(key,str) and key == 'self.out':
      data = self.prepare_log(self.out, sorter=sorter, replace=replace, preproc=proc)
      return self.validateWithReference(stdout=data, preproc=proc)
    elif key and isinstance(key,str) and key == 'self.err':
      data = self.prepare_log(self.err, sorter=sorter, replace=replace, preproc=proc)
      return self.validateWithReference(stdout=data, preproc=proc)
    elif key and isinstance(key,str) and key in res:
      data = self.prepare_log(self.result[key], sorter=sorter, replace=replace, preproc=proc)
      return self.validateWithReference(stdout=data, preproc=proc)

    match = re.compile(key)
    for k in res.keys():
      if match.search(k):
        data = self.prepare_log(self.result[k], sorter=sorter, replace=replace, preproc=proc)
        return self.validateWithReference(stdout=data, preproc=proc)
    print(f'Validator: Match {str(key)} cannot be found in the existing result list.')
    print(f'Validator: The test validation failed')
    self.returnedCode = errno.ENOENT
    self.resultDict['Test Exit Code'] = { f'Test exit code: {int(self.returnedCode)}' }

  # -----------------------------------------------------------------------------------
  def search_strings_in_buffer(self, buffer, strings):
    found = set()
    if isinstance(strings,str):
      strings = [strings]
    for s in strings:
      if buffer.find(s) < 0:
        match = re.compile(s)
        if match.search(buffer):
          found.add(s)
        else:
          self.causes.append('missing string: "' + s + '"')
          self.result['OnlineTest.expected_string'] = self.result.Quote(s)
      else:
        found.add(s)
    return found

  # -----------------------------------------------------------------------------------
  def search_strings_in_results(self, key, strings, replace=None, preproc=None):
    import errno
    res = self.result.annotations
    if key and isinstance(key,str) and key == 'self.out':
      data = self.prepare_log(self.out, sorter=None, replace=replace, preproc=preproc)
      return self.search_strings_in_buffer(data, strings)
    elif key and isinstance(key,str) and key == 'self.err':
      data = self.prepare_log(self.err, sorter=None, replace=replace, preproc=preproc)
      return self.search_strings_in_buffer(data, strings)
    elif key and isinstance(key,str) and key in res:
      self.result[key] = self.prepare_log(self.result[key], sorter=None, replace=replace, preproc=preproc)
      return self.search_strings_in_buffer(self.result[key], strings)
    match = re.compile(key)
    for k in res.keys():
      if match.search(k):
        self.result[k] = self.prepare_log(self.result[k], sorter=None, replace=replace, preproc=preproc)
        return self.search_strings_in_buffer(self.result[k], strings)
    print(f'Validator: Match {str(key)} cannot be found in the existing result list.')
    print(f'Validator: The test validation failed')
    self.returnedCode = errno.ENOENT
    self.resultDict['Test Exit Code'] = { f'Test exit code: {int(self.returnedCode)}' }

  # -----------------------------------------------------------------------------------
  def job_opts_dump_fn(self):
    return self.name + '.joboptsdump'

  # -----------------------------------------------------------------------------------
  def run(self):
    if self.environment is None:
      self.environment = {}
    #
    # Run the test in its own directory if "per-test" is used.
    # Before every run the test directory is cleaned to avoid
    # surprising non-reproducible behaviour when testing locally.
    if self._common_tmpdir and self.use_temp_dir == "per-test":
      self._common_tmpdir = os.path.join(self._common_tmpdir, self.name)
      p = Path(self._common_tmpdir)
      try:
        p.mkdir()
      except FileExistsError:
        shutil.rmtree(p)
        p.mkdir()
        del pn_buffer
    #
    # always dump the gaudi job options
    self.environment['JOBOPTSDUMPFILE'] = self.job_opts_dump_fn()
    workdir = self.workdir
    if self.use_temp_dir:
      if self._common_tmpdir:
        workdir = self._common_tmpdir
    try:
      os.remove(os.path.join(workdir, self.job_opts_dump_fn()))
    except OSError:
      pass
    #
    # always print GaudiException stack traces
    self.environment['ENABLE_BACKTRACE'] = "1"
    #
    # propagate test name (e.g. to allow customising output filenames)
    self.environment['QMTTEST_NAME'] = self.name
    #
    # allow accessing output of prerequisites without explicit dirname, eg
    #   os.path.join(os.getenv("PREREQUISITE_0", ""), "output.dst")
    for i, p in enumerate(self.prerequisites):
      self.environment[f"PREREQUISITE_{i}"] = "../" + p
    #
    # run the test and validation logic
    resultDict = super(Test, self).run()
    #
    resultDict['Test Exit Code'] = { f'Test exit code: {int(self.returnedCode)}' }
    #
    if not self.emit_runtime and 'Runtime Environment' in resultDict:
      resultDict['Runtime Environment'] = { 'IGNORE_RUNTIME': 'RunTime environment skipped by default' }
    #
    # overwrite the reference file name field with the expanded platform-specific reference.
    if os.path.isfile(self.reference):
      lreference = self._expandReferenceFileName(self.reference)
      resultDict['Output Reference File'] = os.path.relpath(lreference, self.basedir)
    #
    return resultDict

  # -----------------------------------------------------------------------------------
  def ValidateOutput(self, stdout, stderr, result):
    try:
      dump = self.job_opts_dump_fn()
      size = os.path.getsize(dump)
      if size < MAX_JOBOPTSDUMP_SIZE:
        with open(dump) as f:
          result['job_opts_dump'] = result.Quote(f.read())
      else:
        result['job_opts_dump'] = result.Quote(f"\n{os.path.abspath(dump)}\n\n" +
                                               "content too big to store: " +
                                               f"{size} > {MAX_JOBOPTSDUMP_SIZE}")
    except IOError:
      pass
    try:
      for s in self.required_strings:
        if stdout.find(s) < 0:
          self.causes.append('missing string: "' + s + '"')
          self.result['OnlineTest.expected_string'] = result.Quote(s)

      if self.ignore_stdout:
        stdout = ''
      if self.ignore_stderr:
        stderr = ''
      elif self.stderr_preprocessor:
        stderr = self.stderr_preprocessor(stderr)
      return super(Test, self).ValidateOutput(stdout, stderr, result)

    except:
      import traceback
      self.causes.append("Exception in validator")
      result["validator_exception"] = result.Quote(traceback.format_exc().rstrip())
      return result, self.causes

  # -----------------------------------------------------------------------------------
  def validateWithReference(self,
                            stdout=None,
                            stderr=None,
                            result=None,
                            causes=None,
                            preproc=preprocessor,
                            counter_preproc=counter_preprocessor,
                            sensitivities={},
                            exclude_platforms_re=r'^skylake_avx512.*|^x86_64_v4.*'):
    '''Overwrite of the base class method by adding extra checks for counters.
       sensitivities allows to overwrite the default sensitivity of the counter checking (0.0001).
       It is containing a dictionnary with Algorithm name as key and a dictionnary as value
       having counter name a key and sensitivity for that counter as value
    '''
      
    # Skip the stdout/counter validation for some platforms.
    # Compared to qmt's "unsupported_platforms", this is a "soft" disabling
    # since other parts of the validators are run (e.g. warning checks).
    platform = self.environment.get("BINARY_TAG", "")
    if re.match(exclude_platforms_re, platform):
      return

    if stdout is None:
      stdout = self.out
    if stderr is None:
      stderr = self.err
    if result is None:
      result = self.result
    if causes is None:
      causes = self.causes

    # call upper class method
    if self.ignore_stdout:
      stdout = ''
    if self.ignore_stderr:
      stderr = ''
    elif self.stderr_preprocessor:
      stderr = self.stderr_preprocessor(stderr)

    super(Test, self).validateWithReference(stdout, stderr, result, causes, preproc)
    # get reference
    lreference = self._expandReferenceFileName(self.reference)
    if not lreference:
      # BaseTest.validateWithReference should deal with this case
      return
    if os.path.isfile(lreference):
      with open(lreference) as f:
        ref = f.read()
    else:
      # BaseTest.validateWithReference adds "missing ref" to causes so
      # just generate the new ref here.
      ref = ''
      # construct new reference
    try:
      from StringIO import StringIO  ## for Python 2
    except ImportError:
      from io import StringIO  ## for Python 3
    newref = StringIO()
    # sanitize newlines
    new = stdout.splitlines()
    # write the preprocessed stdout (excludes counter tables)
    if preproc:
      new = preproc(new)
      # check if preprocessor is idempotent
      if preproc(new) != new:
        causes.append('validateWithReference preprocessor is not idempotent')
    for l in new:
      newref.write(l.rstrip() + '\n')

    newref = newref.getvalue()
    if causes:
      try:
        # overwrite new ref generated by BaseTest.validateWithReference
        newrefname = os.path.join(self.basedir, result['New Output Reference File'])
      except KeyError:
        newrefname = _get_new_ref_filename(lreference)

      with open(newrefname, "w") as f:
        f.write(newref)

      result['New Output Reference File'] = os.path.relpath(newrefname, self.basedir)
