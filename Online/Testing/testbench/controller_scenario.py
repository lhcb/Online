###############################################################################
# (c) Copyright 2022-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import logging
import asyncio
from emulator import (
  TaskController,
  tasks_terminate,
  print_exception,
  tanmon,
  mbmmon
)

log = logging.getLogger(__name__)

# -----------------------------------------------------------------------------
async def run(tasks, args):

  wait_between_restarts = 5
  for t in tasks:
    t._command_name = t.utgid + '/command'
    t._status_name  = t.utgid + '/state'

  try:
    ctrl = TaskController(args.handler, tasks)
    for i in range(4):
      try:
        log.info('++ Attempt No. %d to create and load controller task.'%(i,))
        if i > 0:
          await ctrl.kill_tasks()
        await ctrl.load(status=['OFFLINE'], skip=[None], timeout=20)
        await ctrl.invoke_transition('load', ['NOT_READY','OFFLINE',None], timeout=200)
        status = ctrl.get_statuses()
        if all(s == 'None' for s in status.values()):
          await asyncio.sleep(wait_between_restarts)
          continue
        if all(s == 'OFFLINE' for s in status.values()):
          await asyncio.sleep(wait_between_restarts)
          continue
        if all(s == 'NOT_READY' for s in status.values()):
          break;
      except Exception as X:
        print_exception(X)
        try:
          tasks_terminate(tasks)
        except:
          pass

    ret = await ctrl.invoke_transition('configure','READY', timeout=30)
    ret.check(ok=['READY'], bad=['ERROR', 'NOT_READY', 'None'])
    ret = await ctrl.invoke_transition('start',    'RUNNING', timeout=30)
    ret.check(ok=['RUNNING'], bad=['ERROR', 'READY', 'NOT_READY', 'None'])

    if args.producer:
      await ctrl.await_processing(args.producer)
    elif args.measure_throughput > 0:
      await ctrl.measure_throughput(args.measure_throughput)

    mbmmon(match='(.*)_'+args.test_identifier_hex, output='MBMMON.log')
    if args.need_tan:  tanmon(output='TANMON.log')

    ret = await ctrl.invoke_transition('stop',  'READY')
    ret.check(ok=['READY'], bad=['ERROR', 'NOT_READY', 'None'])
    ret = await ctrl.invoke_transition('reset',['NOT_READY',None])

    try:
      await ctrl.invoke_transition('destroy',['OFFLINE',None], timeout=30)
    except:
      pass
    log.info(f'++ Scenario processing finished!')
    return await ctrl.check_tasks()
  except Exception as X:
    print(f'Exception while executing scenario: {str(X)}')
    return 101
