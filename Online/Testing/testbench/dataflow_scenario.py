###############################################################################
# (c) Copyright 2022-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import logging
import asyncio
from emulator import TaskController, StatusHandler, mbmmon, tanmon

log = logging.getLogger(__name__)

# -----------------------------------------------------------------------------
async def run(tasks, args):
  import os, re
  regex = re.compile('(.*)(a|l|ub|alub)san')
  tag = str(os.getenv('BINARY_TAG','bla'))
  is_sanitizer = regex.match(tag)
  try:
    #           0=load, 1=configure, 2=start, 3=pause, 4=sleep, 5=stop, 6=reset, 7=unload
    timeouts = [200,    30,          30,      1000,    0.1,     60,     30,      30]
    if is_sanitizer:
      timeouts = [15*tmo for tmo in timeouts]
    elif args.debug_scenario:
      timeouts = [1000 for i in timeouts]

    ctrl = TaskController(args.handler, tasks)
    ret = await ctrl.load(skip=['OFFLINE'], timeout=timeouts[0])
    ret.check(ok=['OFFLINE'], bad=['None'])

    ret = await ctrl.invoke_transition('configure','READY', timeout=timeouts[1])
    ret.check(ok=['READY'], bad=['ERROR', 'NOT_READY', 'None'])
    ret = await ctrl.invoke_transition('start',    'RUNNING', timeout=timeouts[2])
    ret.check(ok=['RUNNING'], bad=['ERROR', 'READY', 'NOT_READY', 'None'])

    if args.producer:
      await ctrl.await_processing(args.producer,timeout=timeouts[3])
    elif args.measure_throughput > 0:
      await ctrl.measure_throughput(args.measure_throughput)

    if ctrl.has_mbm(): mbmmon(match='(.*)_'+args.test_identifier_hex, output='MBMMON.log')
    if args.need_tan:  tanmon(output='TANMON.log')

    await asyncio.sleep(timeouts[4])

    ret = await ctrl.invoke_transition('stop',  'READY', timeout=timeouts[5])
    ret.check(ok=['READY'], bad=['ERROR', 'NOT_READY', 'None'])
    ret = await ctrl.invoke_transition('reset', ['NOT_READY','None'], timeout=timeouts[6])
    await ctrl.unload(timeout=timeouts[7])
    log.info(f'++ Scenario processing finished!')
    return await ctrl.check_tasks()
  except Exception as X:
    log.error(f'Exception while executing scenario: {str(X)}')
    return 101
