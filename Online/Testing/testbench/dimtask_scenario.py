###############################################################################
# (c) Copyright 2022-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import logging
from emulator import (
    TaskController,
    tasks_wait_for_status,
    tasks_terminate,
    call_handler,
    print_exception,
    tanmon, mbmmon
)
log = logging.getLogger(__name__)

# -----------------------------------------------------------------------------
async def run(tasks, args):
  for t in tasks:
    t._status_format = 'I'
    t._status_name   = t.utgid + '/VERSION_NUMBER'
    t._command_name  = t.utgid + '/EXIT'

  ctrl = TaskController(args.handler, tasks)
  await ctrl.load(status=None)

  prod_tasks = []
  if args.producer:
    prod_tasks = ctrl.select_tasks(args.producer)
    log.info("Wait for producer tasks: "+str(prod_tasks))
    if len(prod_tasks):
      try:
        await tasks_wait_for_status(prod_tasks, 2033)
        await tasks_wait_for_status(prod_tasks, 'None')
      except:
        pass
      for t in prod_tasks:
        log.info( '++ Producer task '+t.utgid+' finished !')

  # mbmmon(match='(.*)_'+args.test_identifier_hex)
  if args.need_tan:  tanmon()

  call_handler(args.handler, 'onPaused')
  # Exit all tasks left:
  remaining = [t for t in tasks if t not in prod_tasks]
  tasks_terminate(remaining)
  call_handler(args.handler, 'onOffline')
  return await ctrl.check_tasks()
