###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
import logging
import pathlib
import platform
import xml.etree.ElementTree as ET
from string import Template

log = logging.getLogger("")

class _Param:
  def __init__(self, typ, name, value):
    self.type = typ
    self.name = name
    self.value = value

def node_name():
    return platform.node().split('.', 1)[0].upper()


def rinterp(obj, mapping):
    """Recursively interpolate object with a dict of values."""
    return _rinterp(obj, mapping)


def _rinterp(obj, mapping):
    try:
        return {k: _rinterp(v, mapping) for k, v in obj.items()}
    except AttributeError:
        pass
    try:
        return Template(obj).safe_substitute(mapping)
    except TypeError:
        pass
    try:
        return [_rinterp(v, mapping) for v in obj]
    except TypeError:
        return obj


def parse_task_tree(task, arch_path):
    """Parse task tree into name, n_instances, subprocess.Popen arguments."""

    name = task.attrib["name"]
    try:
        n_instances = int(task.attrib.get("instances", 1))
    except ValueError:  # instances = "NUMBER_OF_INSTANCES"
        n_instances = 1

    params = {"args": [], "env": {}, "cwd": "/"}
    # The controller passes populates `-instances`` as the instances argument
    # in the architecture minus one. `createEnvironment.sh` then populates
    # the NBOFSLAVES variable directly from `-instances`.
    params["args"].append(f"-instances={n_instances-1}")
    params["args"].append(f"-architecture={arch_path}")
    command = task.find("command").text
    for argument in task.findall("argument"):
        arg = argument.attrib["name"]
        if "value" in argument.attrib:
            arg += "=" + argument.attrib["value"]
        params["args"].append(arg)
    for fmcparam in task.findall("fmcparam"):
        if fmcparam.attrib["name"] == "wd":
            params["cwd"] = fmcparam.attrib["value"]
        elif fmcparam.attrib["name"] == "utgid":
            task_name = fmcparam.attrib["value"]
        elif fmcparam.attrib["name"] == "define":
            k, v = fmcparam.attrib["value"].split("=", 1)
            params["env"][k] = v

    #params["args"].append(f"-architecture={n_instances-1}")
    params["executable"] = os.path.join(params["cwd"], command)
    params["args"].insert(0, task_name)
    return name, n_instances, params

def read_xml(path, arch_path=None):
    """"Parse architecture file into a list of task specs."""
    dir_path = pathlib.Path(path).parent
    tree = ET.parse(path)
    inventory = tree.getroot()
    if arch_path is None:
      arch_path = pathlib.Path(path)
    result = {'tasks': [], 'params': []}
    try:
      for elt in inventory:
        if elt.tag == 'task':
          log.info('Add task: %-32s [%s/%s]'%(elt.attrib['name'], elt.attrib['user'], elt.attrib['group'], ))
          result['tasks'].append(parse_task_tree(elt, arch_path))
        elif elt.tag == 'include':
          log.info('Load XML: %s '%(elt.attrib['ref'], ))
          res = read_xml(path = dir_path / elt.attrib['ref'], arch_path=arch_path)
          for k in res.keys():
            if k in result.keys():
              result[k] = result[k] + res[k]
            else:
              result[k] = res[k]
        elif elt.tag == 'param':
          log.info('Add params: %-32s = %s [%s]'%(elt.attrib['name'], elt.attrib['value'], elt.attrib['type'], ))
          result['params'].append(_Param(elt.attrib['type'], elt.attrib['name'], elt.attrib['value']))
    except Exception as e:
      log.error(f'Exception: read_xml({path}, {arch_path}): {str(e)}')
    return result


def instance_args(tasks, replacements):
    """Return Popen arguments for each task instance."""
    result = []
    for name, n_instances, args in tasks:
        for instance in range(n_instances):
            result.append(
                rinterp(
                    args, replacements | {
                        "NODE": node_name(),
                        "NAME": name,
                        "INSTANCE": instance,
                    }))
    return result
