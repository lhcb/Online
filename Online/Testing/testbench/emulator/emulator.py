###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Utilities to emulate online dataflow infrastructure.

- log server replacement
- start and manage a DIM DNS
- start and manage tasks (like "tmSrv")

"""
import os
import re
import sys
import time
import errno
import signal
import random
import atexit
import asyncio
import logging
import subprocess
import traceback
import pathlib
from   datetime import datetime
from   contextlib import asynccontextmanager, AsyncExitStack

from . import asyncdim
from . import logs
from .logs import setup_logging  # noqa

TAN_PORT_OFFSET = 0x4000
DNS_PORT_OFFSET = 0x5000

log = logging.getLogger("")

# -------------------------------------------------------------------------------------
_all_tasks = []

# -------------------------------------------------------------------------------------
def expand_environment(value):
  svalue = str(value)
  while svalue.find('${') >= 0:
    idx   = svalue.find('${')
    idq   = svalue.find('}')
    pre   = svalue[:idx]
    post  = svalue[idq+1:]
    env   = svalue[idx+2:idq]
    rep   = os.getenv(env,'Unknown-value-environment-'+env)
    svalue = pre + rep + post
  while svalue.find('$env{') >= 0:
    svalue = svalue.replace('$env{', '\${')
  #log.debug('Translate "'+str(value)+'" to "'+svalue+'"')
  return svalue

# -------------------------------------------------------------------------------------
def expand_path(path):
  return pathlib.Path(expand_environment(path)).resolve()

# -------------------------------------------------------------------------------------
def _killpg(process, sig):
  if process.returncode is None:
    signalnum = getattr(signal, sig)
    log.warning(f"Sending {sig} to process group {process.pid}")
    try:
      os.killpg(process.pid, signalnum)
      return 1
    except ProcessLookupError:
      pass
  return 0
# -------------------------------------------------------------------------------------
def show_all_task_status():
  for task in _all_tasks:
    log.info(f'Process: pid:{task.pid} return code: {str(task.returncode)}')
  loop = asyncio.get_event_loop()
  for task in asyncio.all_tasks(loop):
    try:
      result = task.result()
    except asyncio.exceptions.InvalidStateError as X:
      result = '(not available)'
    except Exception as X:
      result = str(X.__class__)
    log.info(f'{task.get_name():20s}  done:{task.done()} cancelled:{task.cancelled()} return code:{result}')

# -------------------------------------------------------------------------------------
def _cleanup_tasks():
  if all(p.returncode is not None for p in _all_tasks):
    return
  log.warning("Shutting down stray processes...")
  for p in _all_tasks:
    if p.returncode is None:
      _killpg(p, "SIGTERM")
  for i in range(20):
    time.sleep(0.2)
    if all(p.returncode is not None for p in _all_tasks):
      log.warning('...done')
      return
  for p in _all_tasks:
    _killpg(p, "SIGKILL")
  log.warning('...done')

atexit.register(_cleanup_tasks)

# -------------------------------------------------------------------------------------
def print_exception(exception, msg='++ Exception in user code: '):
  log.warning( str(exception)+':  '+str(msg))
  log.warning( '++ '+('-'*132))
  formatted_lines = traceback.format_exc().splitlines()
  for l in formatted_lines:
    log.warning('++ %s'%(l, ))
  log.warning( '++ '+('-'*132))

# -------------------------------------------------------------------------------------
def call_handler(handler, name):
  if not handler:
    return
  if hasattr(handler, name):
    getattr(handler, name)()
  elif hasattr(handler, 'onStateChanged'):
    getattr(handler, 'onStateChanged')(name)

# -------------------------------------------------------------------------------------
def dump_lines(dev, lines, eol='\n'):
  for line in lines: dev(line + eol)

# -------------------------------------------------------------------------------------
def write_file(fname, lines, mode=None):
  log.info(f'+++ Write file: {fname} [{len(lines)} lines]')
  with open(fname, 'w') as f:
    dump_lines(f.write, lines)
    f.flush()
    f.close()
  if mode is not None:
    os.chmod(fname, mode)
    
# -------------------------------------------------------------------------------------
def dump_opts(opts, fname, key_prepend="OnlineEnv."):
  try:
    with open(fname, 'w') as file:
      logging.getLogger("PropertyProxy").setLevel(logging.WARNING)
      logging.getLogger("ConfigurableDb").setLevel(logging.WARNING)
      from Gaudi.Main import toOpt  # indirectly imports ConfigurableDb
      lines = [
        '// ' + 100*'=',
        f'//  Auto generated online environment options {fname} {datetime.now():%Y.%m.%d %H:%M:%S.000}',
        '// ' + 100*'=']
      for k, v in opts.items():
        value = str(v).replace("'",'"').replace('[','{').replace(']','}')
        value = f"{key_prepend}{k} = {toOpt(v)};"
        lines.append(value)
      lines.append('// ' + 100*'=')
      write_file(fname, lines)
      dump_lines(log.info, lines, eol='')
  except Exception as X:
    print_exception(X,'FAILED to write options file.')
    pass

# -------------------------------------------------------------------------------------
def mbmmon(match=None, output=None):
  cmd  = 'gentest libOnlineBase.so mbm_summary'
  mtch = ''
  if match is not None:
    mtch = f'[ {str(match)} ]'
    cmd  = cmd + ' "-match='+str(match)+'"'
  bar   = '+' + (100*'-')
  lines = os.popen(cmd).readlines()
  lines = ['|  '+ line[:-1] for line in lines]
  lines.insert(0, bar)
  lines.insert(0, f'|        Buffer Manager summary  {mtch}')
  lines.insert(0, bar)
  lines.append(   bar)
  dump_lines(log.info, lines, eol='')
  if output is not None:
    write_file(output, lines)

# -------------------------------------------------------------------------------------
def tanmon(output=None):
  sport = os.getenv('TAN_PORT', 123456)
  port  = int(sport)&0xFFFF;
  gbl   = 'TAN_PUBAREA_ID%04X'%(port,)
  cmd   = 'gentest libOnlineBase.so tanmon -s -p '+gbl
  bar   = '+' + (100*'-')
  lines = os.popen(cmd).readlines()
  lines = ['|  '+ line[:-1] for line in lines]
  lines.insert(0, bar)
  lines.insert(0, f'|        Tan monitor output: [port:{port} gbl:{gbl}]')
  lines.insert(0, bar)
  lines.append(   bar)
  dump_lines(log.info, lines, eol='')
  if output is not None:
    write_file(output, lines)

s_time_start = time.time()
# -------------------------------------------------------------------------------------
def generate_identifier_32(offset=0):
  import hashlib
  if os.getenv('BUILD_ID',None):
    text = '%s---%d'%(os.getenv('BUILD_ID',None), os.getpid(), )
  else:
    text = '%f---%d'%(s_time_start, os.getpid(), )
  identifier = hashlib.blake2s(bytes(text,'utf-8'),digest_size=4).hexdigest()
  value = int(identifier,16) + int(offset)
  return value

# -------------------------------------------------------------------------------------
def get_free_port():
  import socket
  port = -1
  try:
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(('localhost',0))
    addr, port = s.getsockname()
    s.close()
    if port < 8080:
      return get_free_port()
  except:
    pass
  return port

# -------------------------------------------------------------------------------------
def generate_identifier(offset=0):
  identifier = (generate_identifier_32()&0x7FFF) + int(offset)
  #identifier = (os.getpid()&0x7FFF) + int(offset)
  while identifier < 1024:
    identifier += 0x100
  while identifier > 62000:
    identifier -= 0x1000
  return identifier

# -------------------------------------------------------------------------------------
async def _create_subprocess(*, exit_callback=None, **kwargs):
  args = kwargs.pop("args")
  p = await asyncio.create_subprocess_exec(*args, **kwargs)
  exit_task = None
  if exit_callback is not None:
    async def callback():
      retcode = await p.wait()
      log.debug(f"{p.pid:6d} exited with code {retcode}")
      exit_callback(p)

    exit_task = asyncio.create_task(callback())
  return p, exit_task

# -------------------------------------------------------------------------------------
def cancel_all_tasks():
  loop = asyncio.get_event_loop()
  for task in asyncio.all_tasks(loop):
    if hasattr(task,'utgid'):
      log.info(f"Cancel task {task.utgid:24s} [pid:{task.pid:6d}. Return code: {str(task._exit_code)}")
      task.process.cancel()
    else:
      pass

# -------------------------------------------------------------------------------------
class AddressAlreadyInUse(RuntimeError):
  pass

# -------------------------------------------------------------------------------------
def stop_tan(node=None, port=None):
  if node is None:
    node = os.getenv('TAN_NODE','localhost')
  if port is None:
    port = os.getenv('TAN_PORT', 123456)
  cmd = 'gentest libOnlineBase.so tan_nameserver_shutdown -nowait -quiet -node='+str(node)+' -port='+str(port)
  log.info('Executing command: '+cmd)
  return os.system(cmd)

# -------------------------------------------------------------------------------------
@asynccontextmanager
async def start_tan(*, ports=None, debug=False):
  if isinstance(ports, int):
    ports = [ports]
  elif isinstance(ports, list) and len(ports)==0:
    ports = [get_free_port(),get_free_port(),get_free_port()]
  elif not ports:
    ports = [get_free_port(),get_free_port(),get_free_port()]
  else:
    raise TypeError('TAN: No proper port supplied to start nameserver')

  for i, port in enumerate(ports):
    os.environ['TAN_NODE'] = 'localhost'
    os.environ['TAN_PORT'] = str(port)
    if i == 0:
      log.info( f'TAN: Nameserver starting on port:{port} ...' )
    else:
      log.warning( f'TAN: Retry No.{i} starting on port:{port} ...' )
    try:
      async with _start_tan(port=port, debug=debug):
        status = await asyncio.sleep(3.0)
        yield
        return
    except AddressAlreadyInUse:
      log.info( f'start_tan: nameserver port:{port} AddressAlreadyInUse exception')
      pass
    finally:
      log.info( f'start_tan: nameserver port:{port} Unknown excpetion' )

  msg = f'TAN: Nameserver failed to start on ports:{ports}'
  log.fatal(msg)
  raise RuntimeError(msg)

# -------------------------------------------------------------------------------------
@asynccontextmanager
async def _start_tan(*, port, debug=False):
  sport = str(port)
  gbl   = 'TAN_PUBAREA_ID%04X'%((port&0xFFFF),)
  off   = int(3 * (random.random()*0x2000 + 100))
  args  = ['gentest', 'libOnlineBase.so', 'tan_nameserver', '-p', gbl, '-a', '-tcp', '-q', sport, '-o', str(off) ]
  if debug:
    args.append('-v')

  with logs.LogServerThread('TAN') as log_server:
    tan_log = []
    ready = False
    def exit_callback(p):
      log.info( f'TAN: nameserver [pid:{p.pid:6d}, port:{port}] exited with exit code {p.returncode} ready:{ready}' )
      if ready:
        cancel_all_tasks()
      else:
        for line in log_server.readlines():
          tan_log.append(line)
      for line in tan_log:
        if 'Address already in use' in line:
          raise AddressAlreadyInUse('TAN: nameserver  [pid:{p.pid:6d}] ' +
                                    'is already in use [Address already in use]')

    log.info(log_server.fifo_path())
    p, exit_task = await _create_subprocess(
      exit_callback=exit_callback,
      args=args,
      stdout=log_server.fifo(),
      stderr=log_server.fifo(),
      preexec_fn=os.setpgrp,
      env = { 'TAN_NODE':        'localhost',
              'TAN_PORT':        str(port),
              'PATH':            os.environ['PATH'],
              'LD_LIBRARY_PATH': os.environ['LD_LIBRARY_PATH'] } )
    _all_tasks.append(p)
    os.environ['TAN_NODE'] = 'localhost'
    os.environ['TAN_PORT'] = str(port)
    # Need to wait until TAN really is starting to capture the possible [Address already in use] exception
    try:
      cmd = str(args).replace("'","").replace(","," ")
      log.info(f'TAN: nameserver [pid:{p.pid:6d}, port:{port}] start: {cmd}')
      await asyncio.sleep(5)
      if exit_task.done():
        log.info(f'TAN: nameserver [pid:{p.pid:6d}, port:{port}] exited prematurely with exit code: {p.returncode}')
        if e := exit_task.exception():
          raise e
      # Only AFTER this point we can safely assum TAN is running properly
      ready = True
      log.info(f'TAN: nameserver [pid:{p.pid:6d}, port:{port}] running')
      yield
    except AddressAlreadyInUse as exc:
      log.info(f'TAN: nameserver [pid:{p.pid:6d}, port:{port}] exception: AddressAlreadyInUse')
      raise exc
    finally:
      exit_task.cancel()
      await p.wait()
  log.info(f'TAN[{port}]: nameserver: TAN stopped execution...')

# -------------------------------------------------------------------------------------
@asynccontextmanager
async def start_dim_dns(*, executable='dns.exe', ports=None, debug=False):
  if isinstance(ports, int):
    ports = [ports]
  elif isinstance(ports, list) and len(ports)==0:
    ports = [get_free_port(),get_free_port(),get_free_port()]
  elif not ports:
    ports = [get_free_port(),get_free_port(),get_free_port()]
  else:
    raise TypeError('DIM DNS: No proper port supplied to start server')

  for i, port in enumerate(ports):
    if i == 0:
      log.info(f'DIM DNS: Starting on port:{port} ...')
    else:
      log.warning(f'DIM DNS: Retry starting on port:{port} ...')
    try:
      async with _start_dim_dns(executable=executable, port=port, debug=debug):
        status = await asyncio.sleep(3.0)
        yield
      return
    except AddressAlreadyInUse:
      log.info( f'start_dim_dns: port:{port} AddressAlreadyInUse excpetion')
      pass
    finally:
      log.info( f'start_dim_dns: port:{port} Unknown excpetion' )
      pass
  msg = f'DIM DNS: Failed to start on ports {ports}'
  log.fatal(msg)
  raise RuntimeError(msg)

# -------------------------------------------------------------------------------------
@asynccontextmanager
async def _start_dim_dns(*, executable='dns.exe', port=25050, debug=False):
  args = [executable]
  if debug:
    args.append('-d')

  with logs.LogServerThread('DIM_DNS') as log_server:
    ready = False
    #
    def exit_callback(p):
      log.error(f'DIM DNS [pid:{p.pid:6d}] exited with code {p.returncode}')
      if ready:
        cancel_all_tasks()
      else:
        for line in log_server.readlines():
          if 'Address already in use' in line:
            raise AddressAlreadyInUse(
              'DIM DNS [pid:{p.pid:6d}] port is already in use [Address already in use]')
      return
    #
    log.info(log_server.fifo_path())
    p, exit_task = await _create_subprocess(
      exit_callback=exit_callback,
      args=args,
      stdout=log_server.fifo(),
      stderr=log_server.fifo(),
      preexec_fn=os.setpgrp,
      env={
        'DIM_HOST_NODE':   'localhost',
        'DIM_DNS_NODE':    'localhost',
        'DIM_DNS_PORT':    str(port),
        'PATH':            os.environ['PATH'],
        'LD_LIBRARY_PATH': os.environ['LD_LIBRARY_PATH'],
      })

    _all_tasks.append(p)
    #
    # pydim.dis_set_dns_node('localhost')  => does not work...
    os.environ['DIM_HOST_NODE'] = 'localhost'
    os.environ['DIM_DNS_NODE']  = 'localhost'
    os.environ['DIM_DNS_PORT']  = str(port)
    # Wait for the process to respond and check PID
    # First, wait a bit to avoid a (harmless) connection refused error
    # TODO can we avoid that somehow?
    await asyncio.sleep(1)
    if exit_task.done():
      if e := exit_task.exception():
        raise e
    with asyncdim.DimService('DIS_DNS/SERVER_LIST', 'C') as ds:
      value = (await ds.get())[1][1]
      servers, pids = [x.split('|') for x in value.split('\0')]
      assert len(servers) == len(pids)
      for server, pid in zip(servers, pids):
        if server.split('@', 1)[0] == 'DIS_DNS':
          if int(pid) != p.pid:
            # NOTE this check might pass even if this is not our
            # dns in case we're running in a docker container
            # (with --network host).
            msg = ('PID of dns not consistent: ' +
                 f'expected {p.pid:6d} got {pid:6d}. ' +
                 f'Was DNS running on port:{port} already? ')
            log.error(msg)
            raise AddressAlreadyInUse('DIM DNS port is already in use')

    ready = True
    try:
      yield
    finally:
      pid = p.pid
      log.info('++ Cancel DIM_DNS and exit task')
      exit_task.cancel()
      await asyncdim.send_command('DIS_DNS/EXIT', (0, ), 'L')
      try:
        status = await asyncio.wait_for(p.wait(), timeout=1.0)
      except:
        if p.returncode is None:
          try:
            os.kill(pid, 9)
          except:
            pass
        status = await p.wait()
      log.info(f'++ DIM_DNS exited with status: {status}')

# -------------------------------------------------------------------------------------
async def tasks_load(tasks):
  return await asyncio.gather(*[t.load() for t in tasks])

# -------------------------------------------------------------------------------------
def tasks_get_status(tasks, utgid=None):
  statuses = {}
  for t in tasks:
    if utgid is not None and not re.match(utgid, t.utgid):
      continue
    statuses[t.utgid] = str(t._status_value)
  return statuses

# -------------------------------------------------------------------------------------
async def tasks_wait_for_status(tasks, status, *, skip=[], error=['ERROR'], utgid=None, timeout=1000000):
  start_time = time.time()
  statuses   = {}
  if isinstance(status, list):
    allowed_status = [str(s) for s in status]
  else:
    allowed_status = [str(status)]

  log.info(f'Wait for state {str(allowed_status)} of {len(tasks)} tasks: {str([t.utgid for t in tasks])}')

  for t in tasks:
    # Apply UTGID match if required
    if utgid is not None and not re.match(utgid, t.utgid):
      continue
    # Task already is dead: take saved status and continue
    if (t.process is None) or (t._exit_code is not None):
      statuses[t.utgid] = t._status_value
      continue

    # No wait for the status of existing processes
    time_left = timeout - (time.time()-start_time)
    pid = t.process.pid
    retry = 0
    while True:
      try:
        budget = time_left
        if budget < 0: budget = 0
        log.debug(f'Get task status of {t.utgid} with a time budget of: {budget:.2f} seconds.')
        st = await t.status(time_left)
      except asyncio.exceptions.TimeoutError as X:
        log.error(f'{t.utgid:24s} [pid:{pid:6d}] Timeout waiting for response [{time_left:.2f} seconds].')
        st = t._status_value
        t.stack_trace()
      except Exception as X:
        st = t._status_value

      time_left = timeout - (time.time()-start_time)
      st = str(st)
      if st in skip:
        log.info(f'{t.utgid:24s} [pid:{pid:6d}] SKIP status: '+str(st))
      else:
        ok = False
        for s in allowed_status:
          if s == st or str(s) == str(st):
            ok = True
        if ok:
          log.info(f'{t.utgid:24s} [pid:{pid:6d}] Status: '+str(st)+', state OK.')
          statuses[t.utgid] = st
          break
        elif st in error:
          log.info(f'{t.utgid:24s} [pid:{pid:6d}] Status: '+str(st)+', process in ERROR.')
          statuses[t.utgid] = st
          break
        elif st == 'None':
          log.info(f'{t.utgid:24s} [pid:{pid:6d}] Status: '+str(st)+', process DIED.')
          statuses[t.utgid] = st
          t.stack_trace()
          break
        log.info(f'{t.utgid:24s} [pid:{pid:6d}] Status: '+str(st)+' not OK.')
        if time_left > 0.0:
          log.info(f'{t.utgid:24s} [pid:{pid:6d}] Continue to wait for '+str(allowed_status)+'!')
      # Check if we can retry or not:
      retry = retry + 1
      if time_left < 0 and retry > 1:
        break

  time_left = timeout - (time.time()-start_time)
  if time_left < 0.0:
    message = f'Timeout during transition to state: {str(status)} [timeout: {timeout} seconds]'
    log.error(message)
    #raise RuntimeError(message)
  if not all(str(s) in allowed_status for s in statuses.values()):
    message = f'Unexpected statuses: {statuses}'
    log.error(message)
    raise RuntimeError(message)
  return statuses

# -------------------------------------------------------------------------------------
def tasks_are_alive(tasks):
  for task in tasks:
    if not task.process or task._exit_code:
      return False
  return True

# -------------------------------------------------------------------------------------
def tasks_terminate(tasks):
  for task in tasks:
    try:
      task._exit_task.cancel()
    except:
      pass
  for task in tasks:
    try:
      if task.process:
        _killpg(task.process, 'SIGTERM')
    except:
      pass

# -------------------------------------------------------------------------------------
async def tasks_wait_for_exit(tasks):
  for task in tasks:
    if hasattr(task,'_exit_task'):
      try:
        task._exit_task.cancel()
      except Exception as X:
        log.info(f'{task.utgid:24s} [pid:{task.process.pid:6d} on exit_task.cancel: '+str(X)[8:-2])
    
  alive = [task for task in tasks if task.process is not None]
  await asyncio.gather(*[task.wait() for task in alive])
  return [task._exit_code for task in tasks]

# -------------------------------------------------------------------------------------
async def tasks_send_command(tasks, command, *, timeout=None):
  log.info(f'Send command "{command}" to {len(tasks)} tasks: {[t.utgid for t in tasks]}')
  return await asyncio.wait_for(asyncio.gather(*[t.send_command(command) for t in tasks]), timeout)

# -------------------------------------------------------------------------------------
async def async_input(prompt=None):
  """Async version of input()."""
  reader = asyncio.StreamReader()
  loop = asyncio.get_event_loop()
  transport, protocol = await loop.connect_read_pipe(
    lambda: asyncio.StreamReaderProtocol(reader), sys.stdin)

  if prompt is not None:
    print(prompt, end='', flush=True)
  line = await reader.readline()
  # FIXME we can't really close here as a second call to async_input fails
  # transport.close()
  return line.decode("utf8").rstrip("\n")

# -------------------------------------------------------------------------------------
def check_for_orphans(names):
  # Note: must not have asan in LD_PRELOAD => use clean environment
  output = subprocess.check_output(["ps", "-e", "-o", "pid=", "-o", "args="],
                                   encoding="ascii",
                                   env={})
  matches = []
  for line in output.splitlines():
    pid, arg0, *_ = line.split()
    if arg0 in names:
      matches.append(f"{arg0} ({pid})")
  if matches:
    raise RuntimeError("Found already running processes:\n" + "\n".join(matches))

# -------------------------------------------------------------------------------------
def tasks_check_exit_codes(tasks, exit_codes):
  if set(exit_codes) != {0}:
    for t, ec in zip(tasks, exit_codes):
      if ec != 0:
        log.error(f'{t.utgid:24s} exited with non-zero code {ec}')
      log.error(f'{t.utgid:24s} exited with exit code {ec}  -> return 102')
      return 102
  return 0

# -------------------------------------------------------------------------------------
class Task:
  # -----------------------------------------------------------------------------------
  def __init__(self, args, log_server):
    self.process        = None
    self.utgid          = args["args"][0]
    self.pid            = -1
    self._args          = args
    self._log_server    = log_server
    self._status        = None
    self._status_value  = None
    self._status_lock   = asyncio.Lock()
    self._status_task   = None
    self._command_name  = self.utgid
    self._status_name   = self.utgid + '/status'
    self._status_format = 'C'
    self._exit_code     = None
    self._run_down      = False

  # -----------------------------------------------------------------------------------
  def stack_trace(self, pid=None):
    if pid == None and self.process:
      return self.stack_trace(self.process.pid)
    elif pid:
      cmd = [
        "gdb",
        "--pid",
        str(pid),
        "--batch",
        "--eval-command=thread apply all backtrace",
      ]
      cmd_str = '+ '
      for c in cmd: cmd_str = cmd_str + c + ' '
      bar = '+' + (132*'-')
      log.error(bar)
      log.error(f'+  Calling GDB stackstrace for process {self.utgid}  pid:{pid}')
      log.error(bar)
      log.error(cmd_str)
      log.error(bar)
      trace = []
      try:
        gdb = subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        trace = gdb.communicate()[0]
      except TimeoutExpired:
        gdb.kill()
        trace = gdb.communicate()[0]
      trace = trace.decode("utf-8", errors="backslashreplace").split('\n')
      for line in trace:
        log.error(line)
        ## log.error(f'{self.utgid:24s} [pid:{pid:6d}] {line}')
      log.error(bar)
      return 1

  # -----------------------------------------------------------------------------------
  async def __aenter__(self):
    pass

  # -----------------------------------------------------------------------------------
  async def load(self):
    args = self._args
    log_server = self._log_server
    utgid = self.utgid

    # TODO propagate DIM_DNS_PORT/NODE more explicitly...
    args['env']['DIM_HOST_NODE'] = os.environ['DIM_HOST_NODE']
    args['env']['DIM_DNS_NODE']  = os.environ['DIM_DNS_NODE']
    args['env']['DIM_DNS_PORT']  = os.environ['DIM_DNS_PORT']
    args['env']['LOGFIFO']       = log_server.fifo_path()
    args['env']['UTGID']         = utgid
    # in case we diverted TAN: The client must know the node and the port:
    if os.getenv('TAN_NODE',None):
      args['env']['TAN_NODE']    = os.environ['TAN_NODE']
    if os.getenv('TAN_PORT',None):
      args['env']['TAN_PORT']    = os.environ['TAN_PORT']

    def exit_callback(p):
      self._exit_code = p.returncode
      if self._status_task is not None:
        log.warning(f'{utgid:24s} [pid:{p.pid:6d}] exited with code {p.returncode} while waiting for status')
        self._status_value = str(None)
        self._status_task.cancel()
        return
      if self._run_down:
        log.info(f'{self.utgid:24s} [pid:{p.pid:6d}] exited with code {p.returncode}')
      else:
        log.critical(f'{self.utgid:24s} [pid:{p.pid:6d}] exited unexpectedly with code {p.returncode}')
      self._status_value = str(None)
      self.pid = -self.process.pid
      self.process = None
      if p.returncode != 0 and not self._run_down:
        cancel_all_tasks()
      return

    p, exit_task = await _create_subprocess(
      exit_callback=exit_callback,
      stdout=log_server.fifo(),
      stderr=log_server.fifo(),
      preexec_fn=os.setpgrp,
      **args)
    log.info(f'{utgid:24s} [pid:{p.pid:6d}] Started. Status: {self._status_name} Command: {self._command_name}')
    _all_tasks.append(p)

    self._status = asyncdim.DimService(self._status_name, self._status_format)
    self._exit_task = exit_task
    self.utgid = utgid
    self.process = p
    self.pid = p.pid

  # -----------------------------------------------------------------------------------
  async def __aexit__(self, exc_type, exc_val, exc_tb):
    if self._status:
      self._status.__exit__(exc_type, exc_val, exc_tb)
    self._status = None
    if self._exit_task:
      self._exit_task.cancel()
    if self.process:
      # import pdb
      # pdb.set_trace()
      log.warning(f'{self.utgid:24s} [pid:{self.pid:6d}] Kill process and children...')
      _killpg(self.process, 'SIGTERM')
      time.sleep(1.0)
      if self._exit_code is None:
        _killpg(self.process, 'SIGKILL')
    self.process = None

  # -----------------------------------------------------------------------------------
  async def status(self, timeout=1000000):
    if self.process is None:
      raise RuntimeError(f'{self.utgid:24s} [pid:{self.pid:6d}] Task is not running')
    # No concurrency for status() calls
    async with self._status_lock:
      exception = None
      loop = asyncio.get_event_loop()
      self._status_task = loop.create_task(self._status.get(timeout=timeout))
      try:
        await self._status_task
      except asyncio.exceptions.CancelledError as x:
        log.warning(f'{self.utgid:24s} [pid:{pid:6d}] dim:{self._status.name()} Event loop was cancelled.')
        exception = x
      except asyncio.exceptions.TimeoutError as x:
        log.warning(f'{self.utgid:24s} [pid:{pid:6d}] dim:{self._status.name()} Timeout occurred while waiting for service.')
        exception = x
      (_, old), (ts, value) = self._status_task.result()
      self._status_task = None
      pid = self.process.pid
      msg = ''
      if not self.process:
        msg = f'[{os.strerror(errno.ESRCH)}]'
        pid = -1

      log.debug(f'{self.utgid:24s} [pid:{pid:6d}] status changed {old} to {value} {msg}')
      self._status_value = value
      if exception:
        raise exception
      return value

  # -----------------------------------------------------------------------------------
  async def send_command(self, command):
    log.debug(f'{self.utgid:24s} [pid:{self.pid:6d}] sending {command!r}')
    result = None
    if command == 'unload':
      self._run_down = True
    try:
      result = await asyncdim.send_command(self._command_name, (command, ), 'C')
    except Exception as exc:
      log.debug(f'{self.utgid:24s} [pid:{self.pid:6d}] send_command FAILED: {str(exc)}')
    return result

  # -----------------------------------------------------------------------------------
  async def wait(self):
    self._exit_task.cancel()
    if self.process:
      self._exit_code = await self.process.wait()
      self.process = None
    return self._exit_code

# -------------------------------------------------------------------------------------
class StatusHandler:
  class Result:
    def __init__(self, good=0, errors=1):
      self.good = good
      self.errors = errors

  # -----------------------------------------------------------------------------------
  def __init__(self, status, status_codes=None, tasks=None):
    self.status = status
    self.task_statuses = status_codes
    self.result = StatusHandler.Result()
  # -----------------------------------------------------------------------------------
  def check(self, ok=[], bad=[]):
    good = 0
    errs = 0
    for st in self.task_statuses:
      if st in ok:  good = good + 1
      elif st in bad: errs = errs + 1
    if errs > 0:
      msg = f'+++ Status-check[{self.status}]: {good} tasks in OK state. {errs} in BAD state!'
      log.critical(msg)
      raise RuntimeError(msg)
    self.result = StatusHandler.Result(good, errs)
    return StatusHandler.Result(good, errs)

# -------------------------------------------------------------------------------------
class TaskController:
  # -----------------------------------------------------------------------------------
  def __init__(self, handler, tasks):
    self.handler = handler
    self.tasks = tasks
  # -----------------------------------------------------------------------------------
  def _get_state_name(self, status):
    if isinstance(status,list):
      state = status[0]
    elif isinstance(status,tuple):
      state = status[0]
    else:
      state = status
    st = 'on'+(state[0].upper())+(state[1:].lower())
    return st
  # -----------------------------------------------------------------------------------
  def get_statuses(self, tasks=None, utgid=None):
    if not tasks:
      tasks = self.tasks
    return tasks_get_status(tasks=tasks, utgid=utgid)
  # -----------------------------------------------------------------------------------
  def select_tasks(self, match_name, tasks=None):
    import re
    if not tasks:
      tasks = self.tasks
    match = re.compile(match_name)
    selected = [t for t in tasks if match.search(t.utgid)]
    return selected
  # -----------------------------------------------------------------------------------
  def has_mbm(self):
    selected = self.select_tasks('(.*)MBM(.*)')
    return len(selected) > 0
  # -----------------------------------------------------------------------------------
  async def kill_tasks(self, signal='SIGTERM'):
    if self.tasks:
      for task in self.tasks:
        if task.process and not task.process.returncode:
          _killpg(task.process, signal)
      await asyncio.sleep(1)
      for task in self.tasks:
        if task.process and not task.process.returncode:
          _killpg(task.process, 'SIGKILL')
  # -----------------------------------------------------------------------------------
  async def await_processing(self, producer, error=['ERROR'], timeout=1000000):
    prod_tasks = None
    task_statuses = {}
    if producer:
      prod_tasks = self.select_tasks(producer)
      log.info('Wait for producer tasks: '+str([t.utgid for t in prod_tasks]))
      task_statuses = await tasks_wait_for_status(tasks=prod_tasks, status='PAUSED', error=error, timeout=timeout)
      call_handler(handler=self.handler, name='onPaused')
    return StatusHandler(status='PAUSED', status_codes=task_statuses, tasks=prod_tasks)
  # -----------------------------------------------------------------------------------
  async def measure_throughput(self, throughput):
    task_statuses = {}
    if throughput > 0:
      # wait a bit for things to settle and measure throughput
      await asyncio.sleep(throughput / 8)
      task_statuses = await tasks_measure_throughput(tasks=self.tasks, max_duration=throughput)
      call_handler(handler=self.handler, name='onPaused')
    return StatusHandler(status='PAUSED', status_codes=task_statuses, tasks=self.tasks)
  # -----------------------------------------------------------------------------------
  async def load(self, status='NOT_READY', skip=[], error=['ERROR'], timeout=1000000):
    task_statuses = {}
    await tasks_load(tasks=self.tasks)
    call_handler(handler=self.handler, name='onCreated')
    if status and len(status)>0:
      state = self._get_state_name(status)
      task_statuses = await tasks_wait_for_status(tasks=self.tasks, status=status, skip=skip, timeout=timeout)
      call_handler(handler=self.handler, name=state)
    return StatusHandler(status, task_statuses, tasks=self.tasks)
  # -----------------------------------------------------------------------------------
  async def unload(self, status=[None], skip=['OFFLINE'], timeout=1000000):
    self.handler.ignore_exit_codes = True
    alive_tasks = [t for t in self.tasks if t.process is not None]
    await tasks_send_command(tasks=alive_tasks, command='unload')
    task_statuses = await tasks_wait_for_status(tasks=alive_tasks, status=[None], skip=['OFFLINE'], timeout=timeout)
    call_handler(handler=self.handler, name='onOffline')
    return StatusHandler(status='OFFLINE', status_codes=task_statuses)
  # -----------------------------------------------------------------------------------
  async def check_tasks(self):
    # Wait for the tasks to close
    exit_codes = await tasks_wait_for_exit(tasks=self.tasks)
    ret = tasks_check_exit_codes(self.tasks, exit_codes)
    call_handler(handler=self.handler, name='onDone')
    return ret
  # -----------------------------------------------------------------------------------
  async def invoke_transition(self, command, status, skip=[], error=['ERROR'], timeout=1000000):
    task_statuses = {}
    state = self._get_state_name(status)
    alive_tasks = [t for t in self.tasks if t.process is not None]
    if len(alive_tasks)>0:
      await tasks_send_command(tasks=alive_tasks, command=command)
      task_statuses = await tasks_wait_for_status(tasks=alive_tasks, status=status, skip=skip, error=error, timeout=timeout)
      log.info(f'Done... state is {str(status)} tasks: {str([t.utgid for t in self.tasks])[1:-1]}')
    call_handler(handler=self.handler, name=state)
    return StatusHandler(status=status, status_codes=task_statuses)

# -------------------------------------------------------------------------------------
# Unused stuff from Rosen:
# -------------------------------------------------------------------------------------
async def measure_throughput(utgids, max_duration):
  def throughput(start, end):
    return (end[1] - start[1]) / (end[0] - start[0]).total_seconds()

  ta = None
  async with AsyncExitStack() as stack:
    services = [
      stack.enter_context(asyncdim.DimService(u + '/Events/OUT', 'X'))
      for u in utgids
    ]
    # get the first data point per task
    meas = [[(await s.get())[1]] for s in services]
    log.debug(str(meas))
    start_time = datetime.now()
    while (datetime.now() - start_time).total_seconds() < max_duration:
      for s, m, utgid in zip(services, meas, utgids):
        m.extend(await s.get_all())
        log.debug(str(meas))
        t = throughput(m[-2], m[-1])
        ta = throughput(m[0], m[-1])
        n_evt = m[-1][1] - m[0][1]
        log.info(
          f'{utgid:24s}: {t:.1f} ({ta:.1f}) Events/s  ({n_evt} Events)')
      if len(utgids) >= 2:
        t = sum(throughput(m[-2], m[-1]) for m in meas)
        ta = sum(throughput(m[0], m[-1]) for m in meas)
        n_evt = sum(m[-1][1] - m[0][1] for m in meas)
        log.info(f"{'Total':{len(utgids[0])}}: {t:.1f} ({ta:.1f}) Events/s  ({n_evt} Events)")

  if ta is not None:
    # The LHCbPR handler "ThroughputProfileHandler" expects to find
    # exactly one match of the form "Evts/s = 123.456"
    log.info(f"Average total throughput: Evts/s = {ta:.1f}")

  return ta

# -------------------------------------------------------------------------------------
async def tasks_measure_throughput(tasks,
                   max_duration,
                   type_pattern=r".*(HLT|Mon).*"):
  utgids = [t.utgid for t in tasks]
  utgids = [u for u in utgids if re.match(type_pattern, u.split("_")[2])]
  return await measure_throughput(utgids, max_duration)

