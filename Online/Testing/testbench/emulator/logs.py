###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import collections
import json
import logging
import os
import sys
import time
import zlib
import textwrap
import threading
from pathlib import Path


logging_format='%(asctime)s.%(msecs)03d %(name)-18s %(levelname)-12s %(message)s'
logging_date_format='%Y.%m.%d %H:%M:%S'

class ConsoleFormatter(logging.Formatter):
    """Colourful logging formatter."""

    def __init__(self, source_len=25):
        self._source_len = source_len
        fmt = " ".join([
            "%(asctime)s.%(msecs)03d",
            "\33[%(source_color)sm%(source)-{source_len}s",
            "\33[m{color}\33[1m%(levelname)-8s\33[m",
            "%(message)s",
        ])
        yellow = "\33[33m"
        red = "\33[31m"
        self._default = logging.Formatter(
            fmt.format(color="", source_len=source_len), datefmt='%H:%M:%S')
        self._warning = logging.Formatter(
            fmt.format(color=yellow, source_len=source_len),
            datefmt='%H:%M:%S',
        )
        self._error = logging.Formatter(
            fmt.format(color=red, source_len=source_len),
            datefmt='%H:%M:%S',
        )
        self._header = 9 * " "  # TODO
        if not sys.stdout.isatty():
            fmt = " ".join([
                "%(asctime)s.%(msecs)03d",
                "%(source)-{source_len}s",
                "%(levelname)-8s",
                "%(message)s",
            ])
            self._default = self._warning = self._error = logging.Formatter(
                fmt.format(source_len=source_len),
                datefmt='%H:%M:%S',
            )

    def format(self, record):
        if record.levelno < logging.WARNING:
            formatter = self._default
        elif record.levelno < logging.ERROR:
            formatter = self._warning
        else:
            formatter = self._error
        record.msg = textwrap.indent(record.msg, self._header).lstrip()
        if hasattr(record, "source"):
            c = [32, 34, 35, 36][zlib.crc32(record.source.encode()) % 4]
            record.source_color = c
        else:
            s = f"{record.filename}:{record.lineno}"
            if len(s) > self._source_len:
                s = ".." + s[-(self._source_len - 2):]
            record.source = s
            record.source_color = 30
        return formatter.format(record)


_log = None


def setup_logging(log_filename, console_level=logging.INFO):
    global _log, _log_filename
    global logging_format, logging_date_format
    if _log is not None:
        if log_filename != _log_filename:
            raise ValueError('requested log to {} but already have {}'.format(
                log_filename, _log_filename))
        return _log
    _log_filename = log_filename

    # TODO formatting of the console log differs from the one in the file.

    logging.basicConfig(
        level=logging.DEBUG,
        format=logging_format,
        datefmt=logging_date_format,
        filename=log_filename,
        filemode='w')
    console = logging.StreamHandler()
    console.setLevel(console_level)
    console.setFormatter(ConsoleFormatter())
    logging.getLogger('').addHandler(console)
    _log = logging.getLogger(os.path.basename(__file__))
    return _log


class LogServerThread:
    def __init__(self,
                 name,
                 fifo_path: Path = None,
                 output_path=None,
                 console=True,
                 have_time=False):
        self._name = name
        self._fifo = None
        self._have_time = have_time
        if fifo_path is not None:
            if fifo_path.is_dir():
                self._fifo_path = Path(fifo_path / f"{name}.fifo")
            else:
                self._fifo_path = fifo_path
        else:
            self._fifo_path = Path(f"{name}.fifo")
        self._log = logging.getLogger(name)
        self._log.propagate = console
        if output_path:
            handler = logging.FileHandler(filename=output_path, mode="w")
            handler.setLevel(logging.INFO)
            self._log.addHandler(handler)
        self._buffer = collections.deque(maxlen=100)

    def __enter__(self):
        try:
            os.mkfifo(self._fifo_path)
        except FileExistsError:
            import stat
            if not stat.S_ISFIFO(os.stat(self._fifo_path).st_mode):
                raise FileExistsError("File exists but is not a fifo")
        self._thread = threading.Thread(target=self._run, daemon=True)
        self._thread.start()
        self._fifo = open(self._fifo_path, "w")
        return self

    def process_line(self, line):
        # {"@timestamp":"2022-11-23T23:51:30.016+0100","message":"MEPManager                             INFO      Cancelled pending MBM I/O requests.","physical_host":"n8190402","host":"n8190402","pid":"23434","utgid":"TEST_N8190402_MDFProd_0","systag":"TEST"}
        extra = {"source": self._name}
        msg = None
        if line[0] == "{":
           try:
               record = json.loads(line)
               msg = ''
               if self._have_time:
                   #msg = str(record["message"])
                   msg = str(record["@timestamp"]) + '  ' + str(record["message"])
               else:
                   msg = str(record["message"])
           except:
               pass
        if msg is None:
            if self._have_time:
                msg = time.strftime('%Y-%m-%d %H:%M:%S ',time.localtime()) + line[:-1]
            else:
                msg = line[:-1]

        self._buffer.append(line)
        self._log.info(msg, extra=extra)

    def _run(self):
        # open blocks until a writer opens the fifo
        fifo = open(self._fifo_path)
        while True:
            try:
                line = fifo.readline()
                if not line:
                    break
                if len(line) > 1:
                  self.process_line(line)
            except UnicodeDecodeError as exc:
                self._log.info(f'Fifo logger: unicode read error: {str(exc)}')
                continue
            except Exception as exc:
                self._log.info(f'Fifo logger: read error: {str(exc)}')
                fifo.close()
                raise exc
                
    def fifo(self):
        if self._fifo is None:
            raise RuntimeError("fifo hasn't been opened yet")
        return self._fifo

    def fifo_path(self):
        return str(self._fifo_path.resolve())

    def readline(self):
        return self._buffer.popleft()

    def readlines(self):
        lines = []
        try:
            while True:
                lines.append(self.readline())
        except IndexError:
            pass
        return lines

    def __exit__(self, exc_type, exc_value, traceback):
        self._fifo.close()
        self._thread.join()
        os.remove(self._fifo_path)
