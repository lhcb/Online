###############################################################################
# (c) Copyright 2022-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os, sys
import logging
import pathlib
from   datetime import datetime
from   emulator import emulator, architecture

log = logging.getLogger(__name__)

class setup:
  def __init__(self):
    pass

#----------------------------------------------------------------------------
def execute(args, run=200002):
  work_dir   = f'{args.working_dir}'
  farmconfig = os.getenv('FARMCONFIGROOT','(farmconfig)')
  arch       = architecture.read_xml(args.architecture)
  
  log.info('+++ Architecture:           '+ str(args.architecture))

  architecture_dir = pathlib.Path(os.path.dirname(args.architecture))
  os.system(f'cp {args.architecture} {args.working_dir / "Architecture.xml"}')

  # Write main execution script
  script = args.working_dir / 'runTask.sh' 
  log.info('+++ Write execution script: '+ str(script))
  with open( script, 'w') as f:
    farm_config = '${FARMCONFIGROOT}'
    f.write('#!/bin/bash\n#' + 100*'=' + '\n#\n')
    f.write(f'# File auto generated for partition:{args.partition}  {datetime.now():%Y.%m.%d %H:%M:%S.000}\n')
    f.write('#' + 100*'=' + '\n')
    f.write('export UTGID;\n')
    f.write('print() {\n  severity="[$1]";\n  shift;\n  printf "%12s %-24s %9s %s\\n" "`date +%b%y-%H%M%S`" ${UTGIT} ${severity} "$*";\n}\n#\n')
    f.write(f'cd {args.working_dir};\n')
    f.write(f'. ./setup.vars;\n')
    f.write(f'. {farm_config}/job/createEnvironment.sh $*;\n')
    f.write(f'cd {args.working_dir};\n')
    f.write(f'export WORKING_DIR=`pwd`;\n')
    f.write(f'export DYNAMIC_OPTS=`pwd`;\n')
    f.write('export INFO_OPTIONS=${DYNAMIC_OPTS}/OnlineEnv.opts;\n')
    f.write('export ONLINERELEASE=`realpath ${ONLINEBASEROOT}/../..`;\n')
    f.write('#\n#' + 100*'=' + '\n')
    for p in arch['params']:
      if 'environ' in p.type:
        value = emulator.expand_environment(p.value)
        os.environ[p.name] = str(value)
        f.write('export %s="%s";\n'%(p.name, value, ))
        f.write('print INFO "%s=`echo ${%s}`";\n'%(p.name, p.name, ))
        log.info('Set environment: %-40s -> %s'%(p.name, value, ))
    f.write('print INFO "Working directory: `pwd`";\n')
    f.write('print INFO "Executing command: $0 $*";\n')
    f.write('#\n#' + 100*'=' + '\n')
    f.write('. ${COMMAND};\n')
    os.chmod(f.name, 0o777)
  
  # Symlink MBM setup options
  mbm_setup = os.getenv('MBM_SETUP_OPTIONS',None)
  if mbm_setup:
    try:
      output = work_dir + '/MBM_setup.opts'
      if os.path.exists(output):
        os.unlink(output)
      log.info(f'Symlink {str(mbm_setup)} to')
      log.info(f'\t\t\t {output}')
      os.symlink(mbm_setup, output)
    except:
      pass

  # Symlink output configuration override
  output_opts = os.getenv('OUTPUT_OPTIONS',None)
  if output_opts:
    with open(output_opts, 'w') as f:
      f.write( '// ' + 100*'=' + '\n')
      f.write(f'// File auto generated for partition:{args.partition} {datetime.now():%Y.%m.%d %H:%M:%S.000}\n')
      f.write( '// ' + 100*'=' + '\n')
      if 'EBStorage_NULL_Write' in output_opts:
        f.write('Writer.Server          = "None";\n')
        f.write('Writer.NumBuffers      = 1;\n')
        f.write('Writer.NumThreads      = 1;\n')
        f.write('Writer.BufferSizeMB    = 1000;\n')
        f.write('Writer.Stream          = "Full";\n')
        f.write('Writer.FileName        = "/null";\n')
      elif 'EBStorage_NFS_Write' in output_opts:
        f.write('Writer.IdleTimeout     = 30;\n')
        f.write('Writer.HaveFileDB      = 0;\n')
        f.write('Writer.NumThreads      = 2;\n')
        f.write('Writer.NumBuffers      = 4;\n')
        f.write('Writer.BufferSizeMB    = 64;\n')
        f.write('Writer.OutputType      = "POSIX";\n')
        f.write('Writer.FileName = "${WORKING_DIR}/../data/MDF/${RUN}_${TIME}_${NODE}.dat";\n')
        f.write('Writer.MaxFileSizeMB = 500;\n')
      elif 'HLT2_Write' in output_opts:
        f.write('Writer.IdleTimeout     = 30;\n')
        f.write('Writer.HaveFileDB      = 0;\n')
        f.write('Writer.NumThreads      = 2;\n')
        f.write('Writer.NumBuffers      = 4;\n')
        f.write('Writer.BufferSizeMB    = 64;\n')
        f.write('Writer.OutputType      = "POSIX";\n')
        f.write('Writer.FileName = "${WORKING_DIR}/../data/HLT2/${RUN}_${TIME}_${NODE}.dat";\n')
        f.write('Writer.MaxFileSizeMB = 500;\n')

  input_data = os.getenv('INPUT_OPTIONS','blabla')
  #----------------------------------------------------------------------------
  #
  #----------------------------------------------------------------------------
  if 'INPUT_MDF' in input_data:
    mdf_output = os.path.dirname(work_dir) + '/data/MDF'
    with open(work_dir + '/INPUT_MDF.opts', 'w') as f:
      f.write('// ' + 100*'=' + '\n')
      f.write('//  Auto generated input data options \n')
      f.write('// ' + 100*'=' + '\n')
      f.write('Reader.Directories = { "%s" };\n'%(str(mdf_output),))
      f.write('Reader.FilePrefix  = "%010d";\n'%(run,))
      f.write('Reader.Rescan      = 0;\n')
      f.write('Reader.PauseSleep  = 20;\n')
      f.write('Reader.MaxEvents   = 10000;\n')
      f.write('Reader.Buffer      = "Events";\n')
      f.write('Reader.RunNumberService = "";\n')
      f.write('MEPManager.Buffers = { "Events" };\n')
      log.info(f'Wrote input data options: {work_dir}/INPUT_MEP.opts')

    with open(work_dir + '/RunList.opts', 'w') as f:
      f.write('// ' + 100*'=' + '\n')
      f.write('//  Auto generated input data options \n')
      f.write('// ' + 100*'=' + '\n')
      f.write('OnlineEnv.DeferredRuns  = { "MDF/%010d" };\n'%(run,))
      f.write('OnlineEnv.EnableWriting = 1;\n')
      log.info(f'Wrote input data options: {work_dir}/RunList.opts')

  #----------------------------------------------------------------------------
  #
  #----------------------------------------------------------------------------
  if 'INPUT_MEP' in input_data:
    # Create input specification as option file for the reader
    mep_output = os.path.dirname(work_dir) + '/data/MEP'
    with open(work_dir + '/INPUT_MEP.opts', 'w') as f:
      f.write('// ' + 100*'=' + '\n')
      f.write('//  Auto generated input data options \n')
      f.write('// ' + 100*'=' + '\n')
      f.write('Reader.Directories = { "%s" };\n'%(str(mep_output),))
      f.write('Reader.FilePrefix  = "Run%010d_000";\n'%(run,))
      f.write('Reader.Rescan      = 0;\n')
      f.write('Reader.PauseSleep  = 45;\n')
      f.write('Reader.MaxEvents   = 10000;\n')
      log.info(f'Wrote input data options: {work_dir}/INPUT_MEP.opts')

    # Create data input directory if it does not yet exoist:
    os.makedirs(mep_output, exist_ok=True)

    # Create a bunch of symbolic links to the only MEP file we have
    input = os.path.dirname(work_dir) + '/data/mep_data.raw'
    for i in range(10):
      fname = mep_output + '/Run%010d_%08d.mep'%(run, i,)
      try:
        log.info(f'Symlink data to {fname}')
        os.symlink(input, fname)
      except:
        pass

