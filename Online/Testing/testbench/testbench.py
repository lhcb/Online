#!/usr/bin/env python
###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
import sys
import time
import argparse
import asyncio
import logging
import pathlib
import traceback
from   emulator import architecture, emulator

log = logging.getLogger(__name__)

# -------------------------------------------------------------------------------------
#  Intercept handler for specialized treatment of transitions
#
#  \version 1.0
#  \author  M.Frank
# -------------------------------------------------------------------------------------
class handler:
  def __init__(self):
    self.ignore_exit_codes = False

  def onStateChanged(self, state):
    log.info('scene: onStateChanged: '+state)

  def onPaused(self):
    """ We want to wait here a bit to allow all tasks 
        finishing their job after the readers/generators produced the data
        and declared the PAUSED state
    """
    log.info('scene: onPaused: Waiting a bit for the data consumers')
    time.sleep(5)

  def onDone(self):
    """ Ignore all scenario exit codes from here on.
        All tasks have exited. Normally there is nothing that could go wrong anymore!
    """
    self.ignore_exit_codes = True

# ---------------------------------------------------------------------------------------
#  Setup test application
#
#  \version 1.0
#  \author  M.Frank
# ---------------------------------------------------------------------------------------
class setup:
  def __init__(self):
    pass

  # -------------------------------------------------------------------------------------
  def show_example_statement():
    import sys
    msg = \
      """
      Example command line for the batchbench script:

      $> python $TESTBEAMROOT/python/batchbench.py \\
              --working-dir=df_generate_tae \\
              --partition=TEST \\
              --scenario=$TESTBEAMROOT/python/dataflow_scenario.py \\
              --runinfo=$TESTBEAMROOT/python/online_env.py \\
              --architecture=$GAUDIONLINETESTSROOT/tests/architectures/df_generate_mdf.xml \\
              --producer=MDFGen
      """
    print(msg)
    sys.exit(0)
  
  # -------------------------------------------------------------------------------------
  def import_online_environment(path, replacements):
    import importlib.util
    from string import Template
  
    spec = importlib.util.spec_from_file_location('online_env_object', path)
    online_env_module = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(online_env_module)
    items = {}
    for i in dir(online_env_module):
      if len(str(i))>2 and str(i)[:2] != '__':
        val = eval('online_env_module.'+str(i))
        if isinstance(val,str):
          val = Template(val).safe_substitute(replacements)
        items[i] = val
    return items

  # -------------------------------------------------------------------------------------
  def import_module(name, path):
    import importlib.util
    spec = importlib.util.spec_from_file_location(name, path)
    s = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(s)
    return s

  # -------------------------------------------------------------------------------------
  def generate_identifier():
    return emulator.generate_identifier_32()

  def generate_identifier_string():
    return 'ID%08X'%(setup.generate_identifier(),)

  # -------------------------------------------------------------------------------------
  def generate_environment(arch):
    import subprocess
    from datetime import datetime

    exclusions = ['LS_COLORS','SHELL','BASH_FUNC_which%%','DISPLAY','SSH_CONNECTION']
    os.environ['RTL_OUTPUT_BUFFERING'] = 'RTL_LINE_BUFFERING'
    fpath = str(args.working_dir / 'setup.vars')
    vars = open(fpath, 'w')
    vars.write('#!/bin/bash\n#\n#\n')
    for key in os.environ:
      use = True
      for k in exclusions:
        if str(key).find(k) == 0:
          use = False
      if use:
        vars.write('declare -x '+str(key)+'="'+str(os.environ[key])+'";\n')
    vars.close()
    #
    #count = 0
    #cmd = ['/bin/bash', '-c', 'declare -px | grep -v BASH_FUNC > ' + str(args.working_dir / 'setup.vars')]
    #while count <5:
    #  try:
    #    count = count + 1
    #    subprocess.check_call(cmd)
    #    break
    #  except Exception as x:
    #    if count < 5:
    #      time.sleep(0.1)
    #      continue
    #  subprocess.check_call(cmd)
    #
    if args.output_level:
      online_env['OutputLevel'] = args.output_level
    online_env['HasMonitoring'] = 0

    os.environ['WORKING_DIR'] = str(args.working_dir)
    activity = online_env['Activity']
    
    emulator.dump_opts(online_env, args.working_dir / 'OnlineEnv.opts')
    empty  = '# '
    line   = '# ' + 100*'='
    header = '#  Auto generated OnlineEnv for ' + \
      f'partition:{args.partition} activity:{activity}  ' + \
      f'{datetime.now():%Y.%m.%d %H:%M:%S.000}'
    lines = [line, empty, header, empty, line, 'from OnlineEnvBase import *' ]
    emulator.write_file(args.working_dir / 'OnlineEnv.py', lines)

    lines = [line, empty, header, empty, line]
    for k, v in online_env.items():
      lines.append(f'{k} = {v!r}')
    emulator.write_file(args.working_dir / 'OnlineEnvBase.py', lines)

    lines = ['#!/bin/bash',
             line, header, empty, '#  Default task execution script:', empty, line,
             'cd '+str(args.working_dir)+';',
             '. ./setup.vars;',
             '. ${FARMCONFIGROOT}/job/createEnvironment.sh $*;']
    for p in arch['params']:
      if 'environ' in p.type:
        value = emulator.expand_environment(p.value)
        os.environ[p.name] = str(value)
        lines.append('export %s="%s";'%(p.name, value, ))
    lines.append('export PYTHONPATH='+str(args.working_dir)+':${PYTHONPATH};')
    lines.append('. ${COMMAND};')
    fname = args.working_dir / 'runTask.sh'
    emulator.write_file(fname, lines, mode=0o777)

    if args.runtime:
      args.runtime = emulator.expand_path(args.runtime)
      log.info('+++ Loading runtime specializations: {args.runtime}')
      runtime = setup.import_module('runtime', args.runtime)
      runtime.execute(args)

  # -------------------------------------------------------------------------------------
  def initialize_application(args):
    cr_msg = ''
    if wd := args.working_dir:
      if not wd.exists():
        wd.mkdir()
        cr_msg = f'++ Created working directory: {str(wd)}'

    command_line = 'python'
    for arg in sys.argv:
      command_line = command_line + ' ' + arg;

    if args.log_level == logging.DEBUG:
      logging.basicConfig(level=logging.DEBUG)
      logging.getLogger('asyncio').setLevel(logging.DEBUG)
    emulator.setup_logging( args.working_dir / args.log_file, console_level=args.log_level)

    log.info('+' + (100*'='))
    log.info('|  Test command line: ')
    log.info('|  $> '+command_line)
    log.info('+' + (100*'='))

    if len(cr_msg): log.info('| ' + cr_msg)
    log.info(f'| ++ Current working directory is: {pathlib.Path(args.working_dir).resolve()}')

    if data_dir := args.data_dir:
      try:
        directory = data_dir.resolve()
        if not directory.exists():
          directory.mkdir()
          log.info(f'| ++ Created data directory: {str(dir)}')
      except Exception as x:
        log.info(f'| ++ FAILED to create data directory: {str(dir)} [{str(x)}]')
    log.info('+' + (100*'='))

# -------------------------------------------------------------------------------------
parser = argparse.ArgumentParser(allow_abbrev=False)

parser.add_argument(
  '--architecture',
  type=pathlib.Path,
  dest='architecture',
  default=None,
  help='Path to task architecture XML',
)

parser.add_argument(
  '--controller',
  type=pathlib.Path,
  dest='controller',
  default=None,
  help='Path to task architecture XML',
)

parser.add_argument(
  '--working-dir',
  type=pathlib.Path,
  dest='working_dir',
  default=pathlib.Path.cwd(),
  help='Working directory (created if needed)',
)

parser.add_argument(
  '--data-dir',
  type=pathlib.Path,
  dest='data_dir',
  default=pathlib.Path('../data'),
  help='Working directory (created if needed)',
)

parser.add_argument(
  '--task-script',
  type=pathlib.Path,
  dest='task_script',
  default='Invalid-startup-script',
  help='Path to common task startup script',
)

parser.add_argument(
  '--write-unique-id',
  type=pathlib.Path,
  dest='write_unique_id',
  default=None,
  help='Path to file containing unique test identifier',
)

parser.add_argument(
  '--read-unique-id',
  type=pathlib.Path,
  dest='read_unique_id',
  default=None,
  help='Path to file containing unique test identifier',
)

parser.add_argument(
  "--need-tan",
  action='store_true',
  dest='need_tan',
  default=False,
  help="Start TAN names",
)

parser.add_argument(
  "--dim-dns-port",
  type=int,
  default=0,
  help='DIM DNS port (must be unused)',
)

parser.add_argument(
  "--dim_dns_node",
  type=str,
  dest='dim_dns_node',
  default='localhost',
  help="DIM DNS node (default: localhost)",
)

parser.add_argument(
  '--handler',
  type=str,
  default=None,
  help='Placeholder for handler reference',
)

parser.add_argument(
  '--partition',
  type=str,
  default=setup.generate_identifier_string(),
  help='Partition name to use',
)

parser.add_argument(
  '--scenario',
  type=pathlib.Path,
  default=None,
  help='Scenario path',
)

parser.add_argument(
  '--producer',
  dest='producer',
  default=None,
  help='Producer task to wait for PAUSE',
)

parser.add_argument(
  '--runinfo',
  type=pathlib.Path,
  default=None,
  help='Online environment path (runinfo)',
)

parser.add_argument(
  '--runtime',
  type=pathlib.Path,
  default=None,
  help='Specialized online runtime environment (task script, options etc.',
)

parser.add_argument(
  '--output-level',
  type=int,
  default=None,
  dest='output_level',
  choices=[1, 2, 3, 4, 5],
  help='Gaudi OutputLevel',
)

parser.add_argument(
  '-d',
  '--debug',
  action='store_true',
  dest='debug_scenario',
  default=False,
  help='Enable debugging flags in scenarios',
)

parser.add_argument(
  '-D',
  '--bench-debug',
  action='store_const',
  dest='log_level',
  const=logging.DEBUG,
  default=logging.INFO,
  help='Enable debug messages from the testbench',
)

parser.add_argument(
  '--log-file',
  default='emu.log',
  help='Location of logfile (if relative, with respect to --working-dir).',
)

parser.add_argument(
  '--measure-throughput',
  default=10,
  type=float,
  help='How long to measure throughput for.',
)

parser.add_argument(
  '--show-help',
  dest='show_help',
  action='store_const',
  const=True,
  default=False,
  help='Print help line',
)

args = parser.parse_args()

debug = False
if debug:
  import pdb
  pdb.set_trace()

if args.show_help:
  setup.show_example_statement()

setup.initialize_application(args)
#
# Note: Do NOT expand DATA_DIR, since it appears in the reference logs!
#
args.runinfo      = emulator.expand_path(args.runinfo)
args.scenario     = emulator.expand_path(args.scenario)
args.architecture = emulator.expand_path(args.architecture)
args.working_dir  = emulator.expand_path(args.working_dir.resolve())
args.data_dir     = args.data_dir

log.info('+++ Executing basic application setup ...')
os.putenv('DIM_DNS_NODE', args.dim_dns_node)
os.environ['DIM_DNS_NODE'] = args.dim_dns_node
os.putenv('DIM_HOST_NODE', args.dim_dns_node)
os.environ['DIM_HOST_NODE'] = args.dim_dns_node

# Handle test identifiers
test_identifier     = '%d'%(setup.generate_identifier(),)
test_identifier_hex = setup.generate_identifier_string()
if args.write_unique_id:
  if os.path.exists(args.write_unique_id):
    os.remove(args.write_unique_id)
  open(args.write_unique_id,'w').write(test_identifier)
elif args.read_unique_id:
  idstr = open(args.read_unique_id).read()
  test_identifier_hex = 'ID%08X'%(int(idstr),)
  test_identifier = idstr

args.test_identifier     = test_identifier
args.test_identifier_hex = test_identifier_hex
  
log.info('  ')
log.info('++ Partition:         %s '%(str(args.partition), ))
log.info('++ Architecture:      %s '%(str(args.architecture), ))
log.info('++ Ctrl Architecture: %s '%(str(args.controller), ))
log.info('++ Scenario:          %s '%(str(args.scenario), ))
log.info('++ Run info:          %s '%(str(args.runinfo), ))
log.info('++ Task script:       %s '%(str(args.task_script), ))
log.info('++ Working dir:       %s '%(str(args.working_dir), ))
log.info('++ Need TAN:          %s '%(str(args.need_tan), ))
log.info('++ DIM DNS node:      %s '%(str(args.dim_dns_node), ))
log.info('++ Data Producer:     %s '%(str(args.producer), ))
log.info('++ Data Directory:    %s '%(str(args.data_dir), ))
log.info('++ Test identifier    %s [%s]'%(test_identifier,test_identifier_hex, ))

# Architecture and RunInfo replacement variables
replacements = {
  'PARTITION':    args.partition,
  'RUNINFO':      str(args.working_dir.resolve() / 'OnlineEnvBase.py'),
  'BINARY_TAG':   os.environ['BINARY_TAG'],
  'ARCHITECTURE': str(args.architecture.resolve()),
  'DATA_DIR':     str(args.data_dir),
  'WORKING_DIR':  str(args.working_dir.resolve()),
  'TEST_TAG':     test_identifier,
}
for key in replacements.keys():
  os.environ[key] = f'{str(replacements[key])}'

# Trampolin architectures if execution using controller is desired:
architecture_path = args.architecture
if args.controller:
  print(str(args.controller))
  args.controller = emulator.expand_path(args.controller)
  rep = '-replace=WORKING_DIR:'+str(args.working_dir) +\
    ',DATA_DIR:'+str(args.data_dir) +\
    ',ARCHITECTURE:'+str(args.architecture)
  os.environ['CONTROLLER_REPLACEMENTS'] = rep
  print('CONTROLLER_REPLACEMENTS: '+str(os.environ['CONTROLLER_REPLACEMENTS']))
  architecture_path = args.controller

arch = architecture.read_xml(architecture_path)
scenario = setup.import_module('scenario', args.scenario)
online_env = setup.import_online_environment(args.runinfo, replacements)
task_instance_args = architecture.instance_args(arch['tasks'], replacements)

emulator.check_for_orphans([a['args'][0] for a in task_instance_args])

# Generate all setup files needed to execute the processes
setup.generate_environment(architecture.read_xml(args.architecture))

async def main():
  import ctypes
  import contextlib

  log_have_time = True
  os.chdir(args.working_dir)
  try:
    rtl = ctypes.cdll.LoadLibrary("libback_trace.so")   
    async with contextlib.AsyncExitStack() as stack:
      kwargs = {}
      # Start TAN
      if args.need_tan:
        await stack.enter_async_context( emulator.start_tan(**kwargs) )
      # Start DIM DNS
      if args.dim_dns_port:
        kwargs = { 'ports': [args.dim_dns_port] }
      await stack.enter_async_context( emulator.start_dim_dns(**kwargs) )
      #
      # Construct the task entities from the architecture
      tasks = []
      for task_args in task_instance_args:
        short_utgid = '_'.join(task_args['args'][0].split('_')[1:])
        if not len(short_utgid):
          short_utgid = str(task_args['args'][0])
        short_utgid = short_utgid.replace('/','_')
        while short_utgid[0] == '_':
          short_utgid = short_utgid[1:]
        task_log_server = emulator.logs.LogServerThread(
          name=short_utgid,
          console=False,
          fifo_path=args.working_dir,
          output_path=args.working_dir / f'{short_utgid}.log',
          have_time = log_have_time)
        task_args['cwd'] = str(args.working_dir)
        task_args['executable'] = './runTask.sh'
        stack.enter_context(task_log_server)
        # log.info(str(task_args))
        task = emulator.Task( task_args, task_log_server )
        await stack.enter_async_context( task )
        tasks.append( task )
      #
      # Now run the task ensemble1
      args.handler = handler()
      ret = await scenario.run( tasks, args )
      #
      # Stop tan instance if one was requested:
      if args.need_tan:
        try:
          emulator.stop_tan()
        except Exception as X:
          log.warning('++ Exception while stopping TAN: '+str(X))
      #
      log.info('++ Scenario finished. back to main...')
      emulator.show_all_task_status()
      # emulator._cleanup_tasks()
      # Stop dns if one was requested
      if ret and args.handler.ignore_exit_codes:
        log.warning( '++ Scenario exit code: '+str(ret)+' (Ignored. Scenario already finished)')
      elif ret:
        log.warning( '++ Scenario exit code: '+str(ret))
      else:
        log.info( '++ Normal Scenario execution completed. exit code: '+str(ret))
      return ret

  except asyncio.CancelledError as X:
    log.error( '++ Event loop was cancelled: '+str(X) )
    log.error( '++ '+('-'*132))
    formatted_lines = traceback.format_exc().splitlines()
    for l in formatted_lines:
      log.error('++ %s'%(l, ))
    log.error( '++ '+('-'*132))
    return 101

exit( asyncio.run(main(), debug=True) )
