//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
#ifndef SCREEN
#define SCREEN 1
#endif
#include "UPI/upidef.h"
#include "UPI/upicc.h"
#include "UPI/UpiSensor.h"

#pragma link off all globals;
#pragma link off all classes;
#pragma link C++ all functions;
#pragma link C++ class CPP::UpiSensor;

