//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
/*-----------------------------------------------------------------------*/
/*                                                                       */
/*                  ASCII GRAPHICS DISPLAY IMPLEMEMENTATION              */
/*                                                                       */
/*-----------------------------------------------------------------------*/
#ifndef UPI_UPIDISPLAY_H
#define UPI_UPIDISPLAY_H

/// Framework include files
#include <CPP/Interactor.h>
#include <CPP/MonitorDisplay.h>

/// C/C++ include files
#include <cstdarg>

/*
 * UPI namespace
 */
namespace UPI {

  class ReallyClose : public Interactor {
  protected:
    int m_menuID;
    char m_buff[32];
  public:
    ReallyClose(int parent_id,int cmd_id);
    virtual ~ReallyClose();
    void invoke();
    void handle(const Event& ev) override;
  };

  /**@class UpiDisplay UpiDisplay.h UPI/UpiDisplay.h
   *
   * Simple ASCII display implementation for monitoring applications
   *
   * @author Markus Frank
   */
  class UpiDisplay : public Interactor, public MonitorDisplay   {
  protected:
    int          m_menuID, m_lastCMD;
    char**       m_lines;
    ReallyClose* m_quit;
    Area         m_area;
    const Area& area() const                    { return m_area;                       }
    void print_char(int x, int y, int val);
  public:
    enum { CMD_CLOSE=1 };

  private:
    /// No copy constructor allowed
    UpiDisplay(const UpiDisplay& d);
    /// No assignment allowed
    UpiDisplay& operator=(const UpiDisplay& d);

  public:
    /// Update window title/header
    virtual void set_header(int flags, const char* format,...)  override;
    /// Access to the display width in fixed size characters
    size_t width() const                override { return m_area.width;                 }
    /// Access to the display height in fixed size characters
    size_t height() const               override { return m_area.height;                }
    /// Setup display window
    void setup_window() override;
    /// Reset display window
    void reset_window() override;
    /// Start update cycle
    void begin_update() override;
    /// Finish update cycle
    void end_update() override;
    /// Draw empty line to the display
    size_t draw_line() override;
    /// Draw text line with attributes
    size_t draw_line(int flags, const char* format,...)  override;
    /// Draw formatted line in normal rendering
    size_t draw_line_normal(const char* format,...) override;
    /// Draw formatted line in reverse rendering
    size_t draw_line_reverse(const char* format,...) override;
    /// Draw formatted line in bold rendering
    size_t draw_line_bold(const char* format,...) override;
    /// Draw a line bar
    size_t draw_bar() override;
    /// Draw a progress bar
    size_t draw_bar(int x, int y,float ratio,int full_scale) override;
    /// Handle UPI interrupts from menu
    void handle(const Event& ev) override;

    /// Default Constructor
    UpiDisplay(size_t width, size_t height);
    /// Default destructor
    virtual ~UpiDisplay();
  };
}
#endif // UPI_UPIDISPLAY_H
