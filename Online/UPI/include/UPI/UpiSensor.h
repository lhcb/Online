//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  UpiSensor
// -------------------------------------------------------------------------
//
//	Author    : Markus Frank
//
//==========================================================================
#ifndef CPP_UPISENSOR_H
#define CPP_UPISENSOR_H 1

/// Framework include files
#include <CPP/Sensor.h>

/// C/C++ include files
#include <map>

/// CPP namespace declaration
namespace CPP {

  /// Sensor to receive UPI input type
  /**
   *    The UpiSensor  class is derivated from the Sensor class
   *
   *  \author P. Mato
   *  \author M. Frank
   */
  class UpiSensor : public CPP::Sensor {
    typedef std::map<int,CPP::Interactor*> Table;
    Table    m_table;
    UpiSensor();
    virtual ~UpiSensor();
  public:
    static UpiSensor& instance();
    static int newID();
    void  add(CPP::Interactor* source, void* id) override
    {  Sensor::add(source,id);                             }
    virtual void  add(CPP::Interactor* source, int id);
    virtual void  remove(CPP::Interactor* source, int id);
    void  remove(CPP::Interactor* source, void* id) override
    {  Sensor::remove(source,id);                          }
    void  dispatch(void* param) override;
    void  rearm() override { }
  };
}
using CPP::UpiSensor;

#endif    // CPP_UPISENSOR_H
