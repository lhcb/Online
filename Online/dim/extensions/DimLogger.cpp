//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  DimLogger
//--------------------------------------------------------------------------
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================

/// Framework include files
#include "DimLogger.inl.h"

// Ensure that the initialization functions are called.
#ifdef DIM_PRELOAD
extern "C" void dimlog_initialize_logger() __attribute__((constructor));
extern "C" void dimlog_finalize_logger()   __attribute__((destructor)); 
#endif

/// Iniitalize the logger unit
extern "C" void dimlog_initialize_logger()    {
  logger()->run();
}
/// Shutdown the loggetr unit
extern "C" void dimlog_finalize_logger()    {
  logger()->stop();
  ::fprintf(stderr, "\nFinalizing dim logger....\n");  
}

