//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//==========================================================================
//  DimLogger
//--------------------------------------------------------------------------
//
//  Author     : M.Frank
//  Created    : 29/1/2008
//
//==========================================================================
#ifndef DIM_LOGGER_H
#define DIM_LOGGER_H

/// C/C++ include files
#include <string>
#include <memory>

/// DIM namespace declaration
namespace dim  {

  /// Logger class capturing stdout and stderr.
  /**
   *  \author  M.Frank
   *  \version 1.0
   */
  class Logger   {
  public:
    class Implementation;
    /// Place holder for implementation details
    std::unique_ptr<Implementation> imp;

  public:
    /// Initializing constructor
    Logger(long dns_id, const std::string& utgid);
    /// Default destructor
    ~Logger() = default;
    /// Run the logger in a separate thread
    void run();
    /// Stop processing and shutdown
    void stop();
  };
}
#endif // DIM_LOGGER_H
