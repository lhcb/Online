#include <thread>
#include <memory>
#include <mutex>
#include <atomic>
#include <string>

namespace  {

  /// Logger class capturing stdout and stderr.
  /**
   *  \author  M.Frank
   *  \version 1.0
   */
  class DimLogger   {
  public:
    typedef std::unique_ptr<std::thread> thread_t;
    std::mutex  lock;
    std::string real_host;
    std::string physical_host;
    std::string stdout_buffer;
    std::string stderr_buffer;
    std::string output_buffer;
    std::atomic<unsigned long> msg_count;
    thread_t    runner;
    int         service      = -1;
    int         epoll_fd     = -1;
    int         stdout_fd[2] = {-1, -1};
    int         stderr_fd[2] = {-1, -1};

  public:
    /// Initializing constructor
    DimLogger(long dns_id, const std::string& utgid);
    /// Default destructor
    ~DimLogger() = default;
    /// Poll data from stdout/stderr pipes
    void poll();
    /// Run the logger in a separate thread
    void run();
  };
}

#include <dis.h>
#include <cerrno>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/epoll.h>
#include <fcntl.h>

using namespace std;

namespace   {
  namespace   {

    /// Dim service callback to serve data
    void feed_logger(void* tag, void** buf, int* size, int* /* first */)   {
      static string empty("");
      if ( tag && buf && size )   {
	DimLogger* h = *(DimLogger**)tag;
	*size = h->output_buffer.length()+1;
	*buf  = (void*)h->output_buffer.c_str();
	return;
      }
      *size = 0;
      *buf  = (void*)empty.c_str();
    }

    /// Translate errno into error string
    string error(int err)   {
      char   text[1024];
      string e = ::strerror_r(err, text, sizeof(text));
      return e;
    }

    /// Read single data frame
    void read_data(DimLogger* logger, int fd, string& data)    {
      char    text[512];
      ssize_t nbyt = ::read(fd, text, sizeof(text)-2);
      if ( nbyt <= 0 )    {
	return;
      }
      text[nbyt] = 0;
      for(char* p = text; *p; ++p)   {
	if ( *p == '\n' || *p == '\0' )   {
	  struct tm tim;
	  char time_str[128];
	  time_t now = ::time(0);
	  ::localtime_r(&now, &tim);
	  ::strftime(time_str,sizeof(time_str),"{\"@timestamp\":\"%Y-%m-%dT%H:%M:%S.000000%z\",",&tim);
	  {
	    lock_guard<mutex> guard(logger->lock);
	    logger->output_buffer.reserve(512);
	    logger->output_buffer  = time_str;
	    logger->output_buffer += "\"message\":\"";
	    logger->output_buffer += data;
	    logger->output_buffer += "\",\"physical_host\":\"";
	    logger->output_buffer += logger->physical_host;
	    logger->output_buffer += "\",\"host\":\"";
	    logger->output_buffer += logger->real_host;
	    logger->output_buffer += "\"}";
	    ++logger->msg_count;
	    ::dis_update_service(logger->service);
	  }
	  data = "";
	  data.reserve(512);
	  continue;
	}
	data += *p;
      }
    }
  }

  /// Initializing constructor
  DimLogger::DimLogger(long dns_id, const string& utgid)   {
    int    ret = 0, fd;
    string svc = utgid + "_LOGS/log";
    this->msg_count = 0;
    this->service  = ::dis_add_service_dns(dns_id,svc.c_str(),"C",0,0,feed_logger,(long)this);
    this->epoll_fd = ::epoll_create(10);
    ///
    ret = ::pipe(this->stdout_fd);
    if ( ret != 0 )   {
      ::printf("+++ Failed to create stdout pipe. [%s]\n", error(errno).c_str());
    }
    fd  = ::fileno(stdout);
    ret = ::dup2(this->stdout_fd[1],fd);
    if ( ret == -1 )   {
      ::printf("+++ Failed to dup stdout. [%s]\n", error(errno).c_str());
    }
    ///
    ret = ::pipe(this->stderr_fd);
    if ( ret != 0 )   {
      ::printf("+++ Failed to create stderr pipe. [%s]\n", error(errno).c_str());
    }
    fd  = ::fileno(stderr);
    ret = ::dup2(this->stderr_fd[1],fd);
    if ( ret == -1 )   {
      ::printf("+++ Failed to dup stderr. [%s]\n", error(errno).c_str());
    }
    ///
    struct epoll_event ev;
    ev.events   = EPOLLIN | EPOLLRDHUP | EPOLLERR | EPOLLHUP | EPOLLET;
    ///
    ev.data.fd  = this->stdout_fd[0];
    ::fcntl(this->stdout_fd[0], F_SETFL, O_NONBLOCK);
    ret = ::epoll_ctl(this->epoll_fd, EPOLL_CTL_ADD, this->stdout_fd[0], &ev);
    if ( ret != 0 )    {
      ::printf("+++ Failed to add FD %d to epoll structure. [%s]\n",
	       this->stdout_fd[0], error(errno).c_str());
    }
    ev.data.fd  = this->stderr_fd[0];
    ::fcntl(this->stderr_fd[0], F_SETFL, O_NONBLOCK);
    ret = ::epoll_ctl(this->epoll_fd, EPOLL_CTL_ADD, this->stderr_fd[0], &ev);
    if ( ret != 0 )    {
      ::printf("+++ Failed to add FD %d to epoll structure. [%s]\n",
	       this->stdout_fd[0], error(errno).c_str());
    }
    char host[128];
    ::gethostname(host,sizeof(host));
    this->physical_host = host;
    this->real_host     = host;
    ::setvbuf(stdout, nullptr, _IOLBF, 0);
    ::setvbuf(stderr, nullptr, _IOLBF, 0);
    ::dis_start_serving_dns(dns_id,(utgid+"_LOGS").c_str());
  }

  /// Poll data from stdout/stderr pipes
  void DimLogger::poll()    {
    while ( 1 )    {
      struct epoll_event ep[2];
      int nc = ::epoll_wait(this->epoll_fd, ep, sizeof(ep)/sizeof(ep[0]), 100);
      if ( nc > 0 )    {
	for( int i=0; i < nc; ++i )   {
	  if ( ep[i].events&EPOLLIN )   {
	    if      ( ep[i].data.fd == this->stdout_fd[0] )
	      read_data(this, this->stdout_fd[0], this->stdout_buffer);
	    else if ( ep[i].data.fd == this->stderr_fd[0] )
	      read_data(this, this->stderr_fd[0], this->stderr_buffer);
	  }
	}
      }
    }
  }

  /// Run the logger in a separate thread
  void DimLogger::run()    {
    this->runner = make_unique<thread>([this]()  {  this->poll(); });
  }
}

int main(int argc, char** argv)    {
  string dns1, dns2, utgid;
  int    dns_port = ::dis_get_dns_port();

  for(int i=1; i<argc; ++i)   {
    if (      ::strcmp(argv[i],"-dns1") == 0 )
      dns1 = argv[++i];
    else if ( ::strcmp(argv[i],"-dns2") == 0 )
      dns2 = argv[++i];
    else if ( ::strcmp(argv[i],"-utgid") == 0 )
      utgid = argv[++i];
  }
  long dns_id_1 = ::dis_add_dns(dns1.c_str(), dns_port);
  DimLogger  logger(dns_id_1, utgid);
  logger.run();

  long dns_id_2 = ::dis_add_dns(dns2.c_str(), dns_port);
  ::dis_start_serving_dns(dns_id_2,utgid.c_str());

  int count = 0;
  while(1)   {
    char text[1024];
    int wait = 200000;
    unsigned long log_count = logger.msg_count;
    ::snprintf(text,sizeof(text),"++++    message number  %6d   (%ld sent) from %s",
	       count, log_count, utgid.c_str());
    ::usleep(wait);
    ::printf(         "(printf)         %s\n",text);
    ::usleep(wait);
    ::fprintf(stdout, "(fprintf stdout) %s\n",text);
    ::usleep(wait);
    ::fprintf(stderr, "(fprintf stderr) %s\n",text);
    ::usleep(wait);
    cout <<           "(C++ std::cout)  " << text << endl;
    ::usleep(wait);
    cerr <<           "(C++ std::cerr)  " << text << endl;
    ++count;
  }
  return 0;
}
