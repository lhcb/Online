#ifndef __SMIUIRTLDEFS
#define __SMIUIRTLDEFS

#define MAX_NAME 		132

#ifndef solaris
#ifdef sun
#define solaris
#endif
#endif

#include <dic.h>

#ifdef __cplusplus
#define __SMI_CONST const
extern "C"  {
#else
#define __SMI_CONST
#endif

/*
#ifdef WIN32
#include <windows.h>
#include <process.h>
#include <dim_common.h>
#define pause() Sleep(10000)
#endif
*/

#ifndef _PROTO
#ifndef OSK		/* Temorary hack */
#	if defined(__cplusplus) /* || (__STDC__ == 1) || defined(_ANSI_EXT) || defined(ultrix) */
#		define	_PROTO(func,param)	func param
#	else
#		define _PROTO(func,param)	func ()
#	endif
#else
#	define _PROTO(func,param)	func ()
#endif
#ifdef WIN32
#ifdef SMIUILIB
#	define _PROTOE(func,param) __declspec(dllexport) _PROTO(func,param)
/*#	define DllExp __declspec(dllexport)
*/
#else
#	define _PROTOE(func,param) __declspec(dllimport) _PROTO(func,param)
/*#	define DllExp __declspec(dllimport) 
*/
#endif
#else
#	define _PROTOE(func,param) _PROTO(func,param)
/*#	define DllExp
*/
#endif
#endif

#ifdef __VMS
#define VMS
#endif

#ifdef VMS

/* part for CFORTRAN */

#define smiui_current_state		smiui_current_state_
#define smiui_book_statechange	smiui_book_statechange_
#define smiui_cancel_statechange	smiui_cancel_statechange_
#define smiui_get_name			smiui_get_name_
#define smiui_get_state			smiui_get_state_
#define smiui_get_smi_message		smiui_get_smi_message_
#define smiui_get_user_message	smiui_get_user_message_
#define smiui_connect_domain	smiui_connect_domain_
#define smiui_book_connect_domain	smiui_book_connect_domain_
#define smiui_cancel_connect_domain	smiui_cancel_connect_domain_
#define smiui_get_next_object	smiui_get_next_object_
#define smiui_get_next_attribute	smiui_get_next_attribute_
#define smiui_get_next_action	smiui_get_next_action_
#define smiui_send_command		smiui_send_command_
#define smiui_change_option		smiui_change_option_
#define smiui_get_options		smiui_get_options_
#define smiui_book_smi_message	smiui_book_smi_message_
#define smiui_cancel_smi_message	smiui_cancel_smi_message_
#define smiui_book_user_message	smiui_book_user_message_
#define smiui_cancel_user_message	smiui_cancel_user_message_

#ifndef SMIUILIB
#define SMIUI_CURRENT_STATE		smiui_current_state_
#define SMIUI_BOOK_STATECHANGE	smiui_book_statechange_
#define SMIUI_CANCEL_STATECHANGE	smiui_cancel_statechange_
#define SMIUI_GET_NAME			smiui_get_name_
#define SMIUI_GET_STATE			smiui_get_state_
#define SMIUI_GET_SMI_MESSAGE		smiui_get_smi_message_
#define SMIUI_GET_USER_MESSAGE	smiui_get_user_message_
#define SMIUI_CONNECT_DOMAIN	smiui_connect_domain_
#define SMIUI_BOOK_CONNECT_DOMAIN	smiui_book_connect_domain_
#define SMIUI_CANCEL_CONNECT_DOMAIN	smiui_cancel_connect_domain_
#define SMIUI_GET_NEXT_OBJECT	smiui_get_next_object_
#define SMIUI_GET_NEXT_ATTRIBUTE	smiui_get_next_attribute_
#define SMIUI_GET_NEXT_ACTION	smiui_get_next_action_
#define SMIUI_SEND_COMMAND		smiui_send_command_
#define SMIUI_CHANGE_OPTION		smiui_change_option_
#define SMIUI_GET_OPTIONS		smiui_get_options_
#define SMIUI_BOOK_SMI_MESSAGE	smiui_book_smi_message_
#define SMIUI_CANCEL_SMI_MESSAGE	smiui_cancel_smi_message_
#define SMIUI_BOOK_USER_MESSAGE	smiui_book_user_message_
#define SMIUI_CANCEL_USER_MESSAGE	smiui_cancel_user_message_
#endif

#endif

#ifndef SMI_PAR_TYPES
#ifndef WIN32
typedef enum { STRING, INTEGER, FLOAT } SHORT_PAR_TYPES;
#endif
typedef enum { SMI_STRING, SMI_INTEGER, SMI_FLOAT } PAR_TYPES;
#define SMI_PAR_TYPES
#endif

_PROTOE( int smiui_current_state, 	(__SMI_CONST char *obj, __SMI_CONST char *state, __SMI_CONST char* action) );
_PROTOE( int smiui_current_state_dns, 	(dim_long dnsid, __SMI_CONST char *obj, __SMI_CONST char *state, __SMI_CONST char* action) );
_PROTOE( int smiui_send_command, 	(__SMI_CONST char *obj, __SMI_CONST char *cmnd) );
_PROTOE( int smiui_send_command_dns, 	(dim_long dnsid, __SMI_CONST char *obj, __SMI_CONST char *cmnd) );
_PROTOE( int smiui_ep_send_command, 	(__SMI_CONST char *obj, __SMI_CONST char *cmnd) );
_PROTOE( int smiui_send_command_wait, 	(__SMI_CONST char *obj, __SMI_CONST char *cmnd) );
_PROTOE( int smiui_ep_send_command_wait, 	(__SMI_CONST char *obj, __SMI_CONST char *cmnd) );
_PROTOE( int smiui_change_option, 	(__SMI_CONST char *domain, __SMI_CONST char *option, __SMI_CONST char *value) );
_PROTOE( int smiui_change_option_wait, 	(__SMI_CONST char *domain, __SMI_CONST char *option, __SMI_CONST char *value) );
_PROTOE( int smiui_get_options,		(__SMI_CONST char *domain, __SMI_CONST char *optString) );
_PROTOE( int smiui_get_options_dns,		(dim_long dnsid, __SMI_CONST char *domain, char *optString) );
_PROTOE( int smiui_number_of_objects, 	(__SMI_CONST char *domain) );
_PROTOE( int smiui_number_of_objects_dns, 	(dim_long dnsid, __SMI_CONST char *domain) );
_PROTOE( int smiui_connect_domain, 	(__SMI_CONST char *domain) );
_PROTOE( int smiui_connect_domain_dns, 	(dim_long dnsid, __SMI_CONST char *domain) );
_PROTOE( int smiui_book_connect_domain, 	
	(__SMI_CONST char *domain, void (*comm_handler)(...), dim_long par) );
_PROTOE(int smiui_book_connect_domain_dns,
	(dim_long dnsid, __SMI_CONST char *domain, void(*comm_handler)(...), dim_long par));
_PROTOE( int smiui_cancel_connect_domain,
	(__SMI_CONST char *domain) );
_PROTOE( int smiui_shutdown_domain, 	(__SMI_CONST char *domain) );
_PROTOE( int smiui_check_proxy, 	(__SMI_CONST char *proxy) );
_PROTOE( int smiui_check_proxy_dns, 	(dim_long dnsid, char *proxy) );
_PROTOE( int smiui_kill, 	        (__SMI_CONST char *server) );
_PROTOE( int smiui_get_next_object, 	(__SMI_CONST char *domain, __SMI_CONST char *object) );
_PROTOE( int smiui_get_next_attribute, 	(__SMI_CONST char *object, __SMI_CONST char *attribute) );
_PROTOE( int smiui_book_statechange, 	
	(char *object, void (*comm_handler)(...), dim_long par) );
_PROTOE( int smiui_book_statechange_dns, 	
	(dim_long dnsid, __SMI_CONST char *object, void (*comm_handler)(...), dim_long par) );
_PROTOE( int smiui_cancel_statechange, 	(int id) );
_PROTOE( int smiui_get_name, 	
	(int id, char *name) );
_PROTOE( int smiui_get_state, 	
	(int id, int *busy_flg, char *state, int *n_actions) );
_PROTOE( int smiui_get_smi_message, 	
	(int id, __SMI_CONST char *dom, char *mess) );
_PROTOE( int smiui_get_user_message, 	
	(int id, __SMI_CONST char *dom, char *mess) );	
_PROTOE( int smiui_get_action_in_progress, 	
	(int id, __SMI_CONST char *action) );
_PROTOE( int smiui_get_first_action, 	
	(int id, __SMI_CONST char *action, int *n_pars) );
_PROTOE( int smiui_get_next_action, 	
	(int id, __SMI_CONST char *action, int *n_pars) );
_PROTOE( int smiui_get_next_param, 	
	(int id, __SMI_CONST char *par, int *type, int *size) );
_PROTOE( int smiui_get_param_default_value, 	
	(int id, void *value) );
_PROTOE( int smiui_get_next_obj_param, 	
	(int id, __SMI_CONST char *par, int *type, int *size) );
_PROTOE( int smiui_get_obj_param_value, 	
	(int id, void *value) );
_PROTOE( int smiui_book_alloc_statechange, 	
	(__SMI_CONST char *domain, void (*comm_handler)(...), dim_long par) );
_PROTOE( int smiui_book_alloc_statechange_dns, 	
	(dim_long dnsid, __SMI_CONST char *domain, void (*comm_handler)(...), dim_long par) );
_PROTOE( int smiui_cancel_alloc_statechange, 	(int id) );
_PROTOE( int smiui_get_alloc_state, 	
	(int id, int *busy_flg, char *state) );
_PROTOE( int smiui_allocate, 	(__SMI_CONST char *domain) );
_PROTOE( int smiui_release, 	(__SMI_CONST char *domain) );
_PROTOE( int smiui_allocate_to, 	(__SMI_CONST char *domainA, __SMI_CONST char *domainB) );
_PROTOE( int smiui_release_from, 	(__SMI_CONST char *domainA, __SMI_CONST char *domainB) );

_PROTOE( int smiui_get_next_objectset, 	(__SMI_CONST char *domain, __SMI_CONST char *objectset) );
_PROTOE( int smiui_book_objectsetchange, 	
	(__SMI_CONST char *object, void (*comm_handler)(...), dim_long par) );
_PROTOE( int smiui_book_objectsetchange_dns, 	
	(dim_long dnsid, __SMI_CONST char *object, void (*comm_handler)(...), dim_long par) );
_PROTOE( int smiui_cancel_objectsetchange, 	(int id) );
_PROTOE( int smiui_book_smi_message, 	
	(__SMI_CONST char *dom, void (*comm_handler)(...), dim_long par) );
_PROTOE( int smiui_cancel_smi_message, 	(int id) );
_PROTOE( int smiui_book_user_message, 	
	(__SMI_CONST char *dom, void (*comm_handler)(...), dim_long par) );
_PROTOE( int smiui_cancel_user_message, 	(int id) );	
_PROTOE( int smiui_get_first_object_in_set, 	
	(int id, __SMI_CONST char *obj) );
_PROTOE( int smiui_get_next_object_in_set, 	
	(int id, __SMI_CONST char *obj) );
_PROTOE( int smiui_get_setname, 	
	(int id, __SMI_CONST char *objset) );
_PROTOE( int smiui_number_of_objectsets, 	(__SMI_CONST char *domain) );
_PROTOE( int smiui_number_of_objectsets_dns, 	(dim_long dnsid, __SMI_CONST char *domain) );

_PROTOE( void* smiui_create_command,	(__SMI_CONST char* action) );

_PROTOE( int smiui_add_param_to_command, 
	(void* id, __SMI_CONST char* name, __SMI_CONST void* value, int type) );
_PROTOE( int smiui_delete_command, 	(void* pointer) );

_PROTOE( char* smiui_get_command_string, 	(void* id) );

_PROTOE( int smiui_get_action_in_progress_size, 	
	(int id, int *size) );

_PROTOE(dim_long smiui_set_dns, (char *node, int port));
_PROTOE(int smiui_current_state_dns, (dim_long dnsid, __SMI_CONST char *obj, __SMI_CONST char *state, __SMI_CONST char* action));
_PROTOE(int smiui_send_command_dns, (dim_long dnsid, __SMI_CONST char *obj, __SMI_CONST char *cmnd));
_PROTOE(int smiui_ep_send_command_dns, (dim_long dnsid, __SMI_CONST char *obj, __SMI_CONST char *cmnd));
_PROTOE(int smiui_send_command_wait_dns, (dim_long dnsid, __SMI_CONST char *obj, __SMI_CONST char *cmnd));
_PROTOE(int smiui_ep_send_command_wait_dns, (dim_long dnsid, __SMI_CONST char *obj, __SMI_CONST char *cmnd));


#ifdef __cplusplus
}
#endif
#undef __SMI_CONST

#endif

