
#include "argname.hxx"
#include "utilities.hxx"

//--------------------------------------------------------------------
//                                                        June 2018
//                                                        B. Franek
// Copyright Information:
//      Copyright (C) 1996-2018 CCLRC. All Rights Reserved.
//--------------------------------------------------------------------


ArgName::ArgName( )
: _nameString(""), _argNo(0) 
{
	return;
}
//-----------------  Destructor  BF Mar 2020  -------------------------
ArgName::~ArgName() { return; }

void ArgName::init(const Name namestring)
{
	_nameString = namestring;
	_argNo = argNo(_nameString);
	return;
}
//---------------------------------------------------------------------
void ArgName::out()
{
	cout << _nameString << "  " << _argNo << endl;
	return;
}
//---------------------------------------------------------------------
Name ArgName::name(const NameVector& arglist, ArgNameStatus_t& err)
{
	err = ok;
	
	if (_argNo == 0 ) { return _nameString;}
	
	int argNum = arglist.length();
	if (_argNo > argNum) { err = notenoughtargs; return ""; }
	
	return arglist[_argNo-1];
}
//--------------------------------------------------------------------
int ArgName::arg()
{	return _argNo; }
//---------------------------------------------------------------------
bool ArgName::argument()
{
	if ( _argNo <= 0 ) { return false;}
	return true;	
}
