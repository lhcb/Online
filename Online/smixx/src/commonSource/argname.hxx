//--------------------------  ArgName  class  -----------------------
#ifndef ARGNAME_HH
#define ARGNAME_HH

#include "name.hxx"
#include "namevector.hxx"
/** @class ArgName
  This class is a container for 'names' that can be also picked up from
  'argument list'
  @auther Boda Franek
  @date June 2018
*/
//--------------------------------------------------------------------
//                                                        June 2018
//                                                        B. Franek
// Copyright Information:
//      Copyright (C) 1996-2018 CCLRC. All Rights Reserved.
//--------------------------------------------------------------------

	enum ArgNameStatus_t
	{ok = 0, notenoughtargs = 1} ;
class ArgName
{
public :
	ArgName();
	
	~ArgName();
	
	void init(const Name namestring);
	
	void out();
/**
  Will return either the fixed name or its value picked up from
  the argument list
  @param supplied argument list
*/	
	Name name(const NameVector& arglist, ArgNameStatus_t& err);
/**
 returns the value of the argument
*/	
	int arg();
/**
  returns true if the value is picked up from a argument
*/
	bool argument();
	
private :
/**
   _nameString 
   (fixed name)   could be any string. However if it is of the form '$(argn)',
                  it is considered as something that has to be picked from
		  'argument list' and the 'n' is its position in the argument
		  list.
   _argNo         0 means _nameString is not of the form '$(argn)',
	          otherwise n is picked (1...9) as argument position
		  in the arg list		  
*/
	Name _nameString;
	
	int _argNo;  
};


#endif
