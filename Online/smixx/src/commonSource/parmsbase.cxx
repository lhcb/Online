//----------------------  Class  ParmsBase  -----------------------------------
// This class is going eventually to replace all the other parameter clases
//
//                                                      Author: Boda Franek
//                                                      Date : March 2021
//----------------------------------------------------------------------------
#include <stdlib.h>
#include "smixx_common.hxx"
#include <string.h>
#include <assert.h>
#include "name.hxx"
#include "smlline.hxx"
#include "smllinevector.hxx"
#include "param.hxx"
#include "parmsbase.hxx"
#include "utilities.hxx"
#include "errorwarning.hxx"

//-----------------------   Constructor -------------------------------------
ParmsBase::ParmsBase() 
{}
//------------------------- Copy constructor  --------------------------
ParmsBase::ParmsBase(const ParmsBase& parms)
{
		
	int noPars = parms.numOfEntries();
	if (noPars==0) return;

	 
	for ( int i=0; i<noPars; i++ )
	{
		Param par = parms.getParam(i);
		_parameters.push_back(par);
	}
	return;
}
//-----------------------------------------------------------------------
ParmsBase::~ParmsBase() { return; }
//--------------------------  assignement operator ---------------------------
ParmsBase& ParmsBase::operator=(const ParmsBase& parms)
{
	if (numOfEntries() > 0) clear();
// same code follows as in copy constructor
	
	int noPars = parms.numOfEntries();
	if (noPars==0) return *this;

	 
	for ( int i=0; i<noPars; i++ )
	{
		Param par = parms.getParam(i);
		_parameters.push_back(par);
	}	
	
	return *this;
} 
//------------------------  numOfEntries --------------------------------------
int ParmsBase::numOfEntries() const 
{
	return _parameters.size();
}
//------------------------ add -----------------------------------------
void ParmsBase::addParam(const Param& par)
{
	_parameters.push_back(par);
	return;
}
//------------------------------------------------------------------------
void ParmsBase::remove(const Name& parNameToRemove)
{
	int inxErrase = getParIndex(parNameToRemove);
	if ( inxErrase < 0 ) return;
	
	_parameters.erase(_parameters.begin()+inxErrase);

	return;
}
//-----------------------------------------------------------------------
void ParmsBase::clear()
{
	_parameters.clear();
	return;
}
//--------------------------- Parms -------------------------------------------
void ParmsBase::outParms(const Name offset) const 
  {
     Name type, value, name;
	 char* ptn=offset.getString(); cout << ptn << endl;
	 Param par;
	 
	 int nPar = numOfEntries();
	 
     if ( nPar > 0 ) {
        for ( int ii=0; ii<nPar; ii++) {
			par = _parameters.at(ii);
			cout << ptn << par.outString(3) << endl;
        }
     }
  }
//--------------------------------------
void ParmsBase::outParms(int indent) const
{
	Name temp = nBlanks(indent); 
	outParms(temp);
	return;
}
//--------------------------- outString -----------------------
Name ParmsBase::outParmsString(int format) const
{
	Param par;
	Name temp("");
	
	int nPar = numOfEntries();
	if ( nPar <= 0) return temp;
	
	temp = "( ";
	
	for ( int ii=0; ii<nPar; ii++ )
	{
		par = _parameters.at(ii);
		
		if (ii>0) {temp+= ", ";}
		temp += par.outString(format);
	} 
	temp += " )";
	return temp;
}
//-------------------------------------------------------------------------
int ParmsBase::getParIndex(const Name& parName) const
{	
	for ( int ip=0; ip<numOfEntries(); ip++ )
	{
		Param par = _parameters.at(ip);
		if ( par.paramName() == parName )
		{
			return ip;
		}
	}
	
	return -1;
}
//----------------------------------------------------------------
Param ParmsBase::getParam(const int inx) const
{
	return _parameters.at(inx);
}
//--------------------------------------------------------------------------
Name ParmsBase::getParName(int inx) const
{	
	if ( inx < 0 || inx >= numOfEntries() ) 
	{
		cout << " Illegal index : " << inx << endl;
		cout.flush();
		throw FATAL;
	}

	Param par = _parameters.at(inx);
	return par.paramName();
}
//--------------------------------------------------------------------------
Name ParmsBase::getParIndiValue(int inx) const
{	
	if ( inx < 0 || inx >= numOfEntries() ) 
	{
		cout << " Illegal index : " << inx << endl;
		cout.flush();
		throw FATAL;
	}

	Param par = _parameters.at(inx);
	return par.paramIndiValue();
}
//--------------------------------------------------------------------------
Name ParmsBase::getParDeclType(int inx) const
{	
	if ( inx < 0 || inx >= numOfEntries() ) 
	{
		cout << " Illegal index : " << inx << endl;
		cout.flush();
		throw FATAL;
	}

	Param par = _parameters.at(inx);
	return par.paramDeclType();
}
//-------------------------------------------------------------------------
Name ParmsBase::getParCurrValue(int inx) const
{	
	if ( inx < 0 || inx >= numOfEntries() ) 
	{
		cout << " Illegal index : " << inx << endl;
		cout.flush();
		throw FATAL;
	}
	Param par = _parameters.at(inx);
	return par.paramCurrValue();
}
//--------------------------------------------------------------------------
Name ParmsBase::getParDeclType(const Name& parName) const
{	
	int inx = getParIndex(parName);
	if (inx == -1) {return paramcons::notfound;}

	Param par = _parameters.at(inx);
	return par.paramDeclType();	
}
//-------------------------------------------------------------------------
Name ParmsBase::getParCurrValue(const Name& parName) const
{
	int inx = getParIndex(parName);
	if (inx == -1) {return paramcons::notfound;}
	Param par = _parameters.at(inx);
	return par.paramCurrValue();	
}
//-----------------------------------------------------------------
void ParmsBase::setParam(const int inx, const Param& par)
{
	if ( inx < 0 || inx >= numOfEntries() ) 
	{
		cout << " Illegal index : " << inx << endl;
		cout.flush();
		throw FATAL;
	}
	_parameters.at(inx) = par;
	return;
}
//--------------------------------------------------------------------------
void ParmsBase::setParCurrValue(int inx, const Name& value)
{
	if ( inx < 0 || inx >= numOfEntries() ) 
	{
		cout << " Illegal index : " << inx << endl;
		cout.flush();
		throw FATAL;
	}
	
	Param par = _parameters.at(inx);
	par.setParamCurrValue(value); 
	_parameters.at(inx) = par;
	return;
}
//--------------------------------------------------------------------------
int ParmsBase::setParam(const Name& parName, const Param& par)
{
	int inx = getParIndex(parName);
	
	if (inx < 0) {return 0;}
	setParam(inx,par);
	return 1;
}
//---------------------------------------------------------------
int ParmsBase::setParCurrValue(const Name& parName, const Name& value)
{
	int inx = getParIndex(parName);
	
	if (inx < 0) {return 0;}
	setParCurrValue(inx,value);
	return 1;
}
