//----------------------  Class   ParmsBase  -----------------------------------
//
//
//                                                      Author: Boda Franek
//                                                      Date : March 2021
//----------------------------------------------------------------------------
#ifndef PARMSBASE_HH
#define PARMSBASE_HH
#include "typedefs.hxx"
#include <vector>
#include "name.hxx"
#include "param.hxx"
#include "utilities.hxx"
class SMLlineVector;
//------------------------------------------------------------------
class ParmsBase  
{
	public :

		ParmsBase();
		
		ParmsBase( const ParmsBase& );
		
		~ParmsBase(); 

		ParmsBase& operator=( const ParmsBase& );
		
		int numOfEntries() const;
		
		void addParam( const Param& );
		
		void remove( const Name& );
		
		void clear();
//-----------------------------------------------
		void outParms(const Name indent = "" ) const;
		
		void outParms(int indent) const;
	  
		Name outParmsString(int format = 1) const;  // see param.hxx
//-----------------------------------------------
/**
   returns the index of the parameter 'parName'
   if the parameter does not exists, it returns -1
*/
		int getParIndex(const Name& parName) const; 	
/**----------------------------------------------
  geting par attributes using index. Index must be legal,
   i.e. >=0 and <  _parameters.size()
  if not, the program will exit.
*/	
		Param getParam(const int inx) const;
		
		Name getParName(int inx) const;
	
		Name getParIndiValue(int inx) const;
	
		Name getParDeclType(int inx) const;
		
		Name getParCurrValue(int inx) const;
		
/**---------------------------------------------
    geting par attributes using par name. 
	If not found, it returns paramcons::notfound
*/
		Name getParDeclType(const Name& parName) const;
	
		Name getParCurrValue(const Name& parName) const;
/**----------------------------------------------
  seting par attributes using index. Index must be legal,
  i.e. >=0 and <_parameters.size()
  if not, the program will exit.
*/	
		void setParam(const int inx, const Param& param);
		
		void setParCurrValue (int inx, const Name& value);
/**----------------------------------------------
  seting par attributes parameter name.
*/	
		int setParam(const Name& parName, const Param& param);
		
		int setParCurrValue (const Name& parName, const Name& value);

	protected :

		std::vector<Param> _parameters;
};

#endif
