//--------------------------  VarElementVector  class  -----------------------
#ifndef VARELEMENTVECTOR_HH
#define VARELEMENTVECTOR_HH

/** @class VarElementVector
  @author Boda Franek
  @date July 2021
*/
//--------------------------------------------------------------------
//                                                        July 2021
//                                                        B. Franek
//--------------------------------------------------------------------
#include "name.hxx"
#include "varelement.hxx"
#include <vector>

class Parms;

class VarElementVector
{
public :
	VarElementVector();
	
	VarElementVector(const VarElementVector& vec);
	
	~VarElementVector();
	
	VarElement operator [] (const int elem) const;
	
	VarElementVector& operator += (const VarElement& velem);
	
	int length() const;
	
	bool isPresent(const VarElement& velem) const;
/**
    checks if the actual name is in the vector
*/	
	bool isNamePresent(const Name& name, Parms* pPar) const;
	
	void out(const Name offset="") const;
	
private :	
	
	std::vector<VarElement> _vector;
	
}; 
#endif
