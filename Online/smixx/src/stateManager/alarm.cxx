//----------------------------------------------------------------------
//
//                                              Author :  B. Franek
//                                              Date :  January 2016
// Copyright Information:
//      Copyright (C) 1996-2016 CCLRC. All Rights Reserved.
//----------------------------------------------------------------------

#include "alarm.hxx"
#include <stdlib.h>
#include "name.hxx"

extern Name smiDomain;

#include "commhandler.hxx"

extern CommHandler *pCommHandlerGl;

 //--------------------------------------------------------------------------
void Alarm::initialise()
{

	pCommHandlerGl->declareAlarm();
       
       return; 
}
 //--------------------------------------------------------------------------
void Alarm::message(const char* severity, const Name& object, const char* mess)
{
	Name nmSeverity(severity); Name temp;
	
	Name alarmMessage("SMI");
	alarmMessage += " "; alarmMessage += nmSeverity;
	alarmMessage += " "; alarmMessage += smiDomain;
	temp = object;
	if ( object == "-" ) temp = "";
	if (!(temp == ""))
	{
		alarmMessage += "::";
		alarmMessage += temp;
	}
	alarmMessage += ", ";
	alarmMessage += mess;
	char* pAlmess = alarmMessage.getString();
	
	pCommHandlerGl->publishAlarm(pAlmess);
		
	if ( nmSeverity == "FATAL" )
	{
		sleep(3);
		exit(2);
	}
	
       return; 
}
