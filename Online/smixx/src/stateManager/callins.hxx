//-------------------------  Class   C a l l I n s  -------------------------------
#ifndef CALLINS_HH
#define CALLINS_HH

#include "parameters.hxx"
#include "instruction.hxx"
#include "parms.hxx"

class SMIObject;
class State;
class Action;

//                                                  Date :  September 2020
//                                                  Author : Boda Franek
//---------------------------------------------------------------------------


class CallIns : public Instruction
{
public:
	CallIns
		( char lines[][MAXRECL], int lev,int *no_lines, 
		SMIObject *pobj=0, State *pstat=0, Action* act=0);

	~CallIns();
  
	void whatAreYou();
  
	Name outShort();  // produces a short line describing CALL
  
	int execute( Name& endState );   // 0-normal,  1-terminated action, 2-suspended
	
/** returns function name
*/
	Name functionName() const;

	
private:
/**
 will create a parameter string for dispatching to the function
 by updating the generalized values of the function parameters
 by specific current values of local parameters where necessary. 
 If something goes wrong, it does not return...fatal error
@param   parString
*/
	void createOutgoingParameters(Name& parString);
	
//--------------- D A T A ------------------------------
	int _level;   //for diag. printing
  
	Name _functionNm;

	Action* _pFunction;
	  
	SMIObject* _pParentObject;
  
	State* _pParentState;

	Action* _pParentAction;

	int _numOfPara;
  
	Parms _callInsParameters;
	
	int _suspend_flag;
};

#endif
