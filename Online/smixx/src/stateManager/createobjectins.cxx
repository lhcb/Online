//-----------------  Class   C r e a t e O b j e c t I n s  -------------------
#include "smixx_common.hxx"
#include <stdio.h>
#include <assert.h>

//-------------------- Externals -----------------
#include "objectregistrar.hxx"
   extern ObjectRegistrar allSMIObjects;
   extern  Name smiDomain;
#include "registrar.hxx"
   extern Registrar allSMIClasses;

#include "commhandler.hxx"   
   extern CommHandler *pCommHandlerGl;
   
#include "queue_twonames.hxx"
   extern Queue_TwoNames *pStateQGl;
//-------------------------------------------------
#include "utilities.hxx"
#include "ut_sm.hxx"
#include "createobjectins.hxx"
#include "smiobject.hxx"
#include "smiobjectset.hxx"
#include "smiobjectsetsimple.hxx"
#include "parms.hxx"
#include "param.hxx"
#include "options.hxx"
#include "diag.hxx"
#include "getvarelem.hxx"

typedef char OBJLINE[MAXRECL];
//                                                      Date :   May 2010
//                                                     Author:  Boda Franek
// Copyright Information:
//      Copyright (C) 1996-2010 CCLRC. All Rights Reserved.
//---------------------------- CreateObjectIns -------------------------------------
//

CreateObjectIns::CreateObjectIns
       ( char lines[][MAXRECL], const int lev, int& no_lines,
         SMIObject* pobj, State *pstat, Action* pact){

	if ( pstat == NULL ) { } // this will remove compiler warning
	
	_level = lev;

	_pParentObject = pobj;

	_pParentAction = pact;

	
	_objectId = lines[1];
	
	_className = lines[3];

	no_lines = 4;

	for (int i=0; i<no_lines; i++)
	{
		_sobjCode+= lines[i];
	}
	return;
}
//-----------------  Destructor   BF  Mar 2020  --------------------------
CreateObjectIns::~CreateObjectIns()
{
	return;
}

//---------------------------- whatAreYou ---------------------------------

void CreateObjectIns::whatAreYou(){
	Name temp = nBlanks(_level*4+10);
	char* indent = temp.getString();

	cout << indent 
	<< "create_object " << _objectId.outString() << " of_class " << _className
	<< endl;
	
	return;
}
//------------------------------- outShort -------------------------------
Name CreateObjectIns::outShort() const
{
	Name temp;
	
	temp = "create_object ";
	temp += _objectId.outString();
	temp += " of_class ";
	temp += _className;
	return temp;
}

//----------------------------  execute  ---------------------------------------
int CreateObjectIns::execute( Name& endState ){

        int dbg; Options::iValue("d",dbg);
		Name mainIndent = insDiagPrintOffset(_level, _pParentAction);

	endState = "not changed";
	
//debug beg
if( dbg > 5 )
{
	cout << endl << "start ======================= CreateObjectIns::execute ===============";
}
//debug end


//debug beg
if ( dbg > 3 )
{
	cout << mainIndent;
	cout << "executing: " << outShort() << endl;
}
//debug end

	SMIObject* pSMIObj;
	Name objNm("");
	
	int ret = GetVarElem::actualObjectName(_objectId,
					_pParentAction,
					"create_object",
					objNm);
	if (!ret) return 0;
	
// First check that the object of this name is not already there

	pSMIObj = allSMIObjects.gimePointer(objNm);
	if (pSMIObj)
	{
		cout << "*** Warning :  Executing ''create_object'' instruction" << endl
		     << "               Object " << objNm 
		     << " already exists, the instruction is ignored" << endl;
		return 0;
	}

// ---------- Now create the object ----------------------
// the object code of 'create_object' instruction has the same format as that of
// 'object : <object-name> is_of_class <class-name>' declaration. It means that I
// can use the standard SMIObject constructor. The only thing that has to be done is 
// inserting the actual object name in the line no 1. Replacing the line 0
// is not necessary because the constructor does not check it.

	int no_code_lines = _sobjCode.length();
	char* pCode = new char[no_code_lines*MAXRECL];
	OBJLINE* code_lines = (OBJLINE*) pCode; Name lineNm;
//cout << "  no_code_lines " << no_code_lines << endl;	
	for (int il=0; il<no_code_lines; il++)
	{
		if (il == 1)
		{
			strcpy(code_lines[il],&objNm[0]);
		}
		else
		{
			lineNm = _sobjCode[il];
			strcpy(code_lines[il],&lineNm[0]);
		}
//cout << code_lines[il] << endl;
	}
		
	pSMIObj = new SMIObject(&code_lines[0]);
	assert (pSMIObj != 0);
	delete [] pCode;
//---------------  object created ----------------
//
//------  now update client data bases of its server objects and sets

	pSMIObj->updateReferencedObjectsandSets();
	
//----  and now Communication Handler  ---------------------
		
	pCommHandlerGl->declareObj(pSMIObj);      
	pCommHandlerGl->updateDynObjList();
	
	pSMIObj->startUp(pStateQGl,pCommHandlerGl);
	
//Diag::printClientWhensOfAllObjects();
//Diag::printClientWhensOfAllObjectSets();
//Diag::printActualClientWhensOfAllObjects();

	return 0;         // create_object instruction allways finishes
}


