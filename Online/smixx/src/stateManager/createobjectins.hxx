//-------------------------  Class   CreateObjectIns  -------------------------------
#ifndef CREATEOBJECTINS_HH
#define CREATEOBJECTINS_HH

#include "name.hxx"
#include "namevector.hxx"
#include "parameters.hxx"
#include "instruction.hxx"
#include "varelement.hxx"
class SMIObject;
class State;
class Action;
//                                                  Date :    May 2010
//                                                  Author : Boda Franek
// Copyright Information:
//      Copyright (C) 1996-2010 CCLRC. All Rights Reserved.
//---------------------------------------------------------------------------


class CreateObjectIns : public Instruction{
public:
	CreateObjectIns
           ( char lines[][MAXRECL], const int lev, int& no_lines,
              SMIObject* pobj, State *pstat, Action* pact);

	~CreateObjectIns();
	
	void whatAreYou() ;
	
	Name outShort() const ;

	int execute( Name& endState );   // 0-normal,  1-terminated action, 2-suspended
private:
	int _level;

	VarElement _objectId;
/**
      the class name to which the created object is going to belong	
*/
	Name _className;

	SMIObject *_pParentObject;

	Action *_pParentAction;
	
/**
      holds the sobj code of the instruction:
      
      line 0:create_object
      line 1:&VAL_OF_<param-name>
      line 2:    1    0    0
      line 3:<class-name> 
*/
	NameVector _sobjCode;
};

#endif
