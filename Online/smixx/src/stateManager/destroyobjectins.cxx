//-----------------  Class   D e s t r o y O b j e c t I n s  ----------------
#include "smixx_common.hxx"
#include <stdio.h>
#include <assert.h>

//-------------------- Externals -----------------
#include "objectregistrar.hxx"
   extern ObjectRegistrar allSMIObjects;
   extern  Name smiDomain;
#include "registrar.hxx"
   extern Registrar allSMIClasses;
   extern Registrar allSMIObjectSets;

#include "commhandler.hxx"   
   extern CommHandler *pCommHandlerGl;
   
#include "queue_twonames.hxx"
   extern Queue_TwoNames *pStateQGl;
   
#include "queue_name.hxx"
   extern Queue_Name executableObjectQ;
   
#include "resumehandler.hxx"
   extern ResumeHandler resumeHandler;
//-------------------------------------------------
#include "utilities.hxx"
#include "destroyobjectins.hxx"
#include "smiobject.hxx"
#include "smiobjectset.hxx"
#include "smiobjectsetsimple.hxx"
#include "parms.hxx"
#include "param.hxx"
#include "options.hxx"
#include "diag.hxx"
#include "ut_sm.hxx"
#include "getvarelem.hxx"

typedef char OBJLINE[MAXRECL];
//                                                      Date :   Jan 2020
//                                                     Author:  Boda Franek
// Copyright Information:
//      Copyright (C) 1996-2020 CCLRC. All Rights Reserved.
//---------------------------- DestroyObjectIns ------------------------------
//

DestroyObjectIns::DestroyObjectIns
       ( char lines[][MAXRECL], const int lev, int& no_lines,
         SMIObject* pobj, State *pstat, Action* pact){

	if ( pstat == NULL ) { } // this will remove compiler warning
	
	_level = lev;

	_pParentObject = pobj;

	_pParentAction = pact;

	
	_objectId = lines[1];

	no_lines = 2;

	return;
}
//--------------------  Destructor  BF  Mar 2020  -----------------
DestroyObjectIns::~DestroyObjectIns()
{
	return;
}
//---------------------------- whatAreYou ---------------------------------

void DestroyObjectIns::whatAreYou(){
	Name temp = nBlanks(_level*4+10);
	char* indent = temp.getString();

	cout << indent 
	<< "destroy_object " << _objectId.outString() << endl;
	return;
}
//-------------------------- outShort --------------------------------
Name DestroyObjectIns::outShort() const
{
	Name temp;
	temp = "destroy_object ";
	temp += _objectId.outString();
	return temp;
}

//----------------------------  execute  ---------------------------------------
int DestroyObjectIns::execute( Name& endState ){

        int dbg; Options::iValue("d",dbg);
	Name mainIndent = insDiagPrintOffset(_level, _pParentAction);
	
	endState = "not changed";
	
//debug beg
if( dbg > 5 )
{
	cout << endl << "start ======================= DestroyObjectIns::execute ===============";
}
//debug end


//debug beg
if ( dbg > 3 )
{
	cout << mainIndent;
	cout << "executing: " << outShort() << endl;
}
//debug end

	SMIObject* pSMIObj;
	Name objNm;
	
	int ret = GetVarElem::actualObjectName(_objectId,
					_pParentAction,
					"destroy_object",
					objNm);
	if (!ret) return 0;

// First check that the object of this name exists

	pSMIObj = allSMIObjects.gimePointer(objNm);
	if (!pSMIObj)
	{
		cout << "*** Warning :  Executing ''destroy_object''instruction" 
		<< endl
		     << "               Object " << objNm 
		     << " does not exists, the instruction is ignored" << endl;
		return 0;
	}

// Check whether the object was dynamically created.

// ---------- Now destroy the object ----------------------
//        cout << " Executing ''destroy_object'' instruction" << endl
//	     << "   the actual destruction of object " << objNm  
//	     << " not implemented yet" << endl;
/*
   - remove the object from all Object Sets to which it belongs
   - inform Command handler using the new Clara's code.
   - remove the object from State Q and Executable Object Q
   - remove the objects from Resume Handler Qs.
   - object has to inform all its WHEN servers so they can remove it from
     their database. Code for this does not exists yet.
   
*/
// -----------    Remove the object from all the Sets it belongs to.

	int numOfSets = allSMIObjectSets.length();
	int is;
	SMIObjectSet* pSet;
	Name setNm;

	for (is = 0; is < numOfSets; is++ )
	{
		pSet = (SMIObjectSet*)allSMIObjectSets.gimePointer(is);
		setNm = pSet->name();
		if ( pSMIObj->memberOfSet(setNm) )
		{
//debug beg
if ( dbg > 5 )
{
	cout << " removing " << objNm 
	     << " from " << setNm << endl;
}
//debug end
			pSet->remove(objNm);
//debug beg
if ( dbg > 5 )
{
	cout << endl << " New set" << setNm 
	     << " set : " << endl;
	pSet->out("      "); cout << endl;
}
//debug end
		}
	}
// --------------------

// ---- Additionaly we have to erase the object from all sets ----------

	for (is = 0; is < numOfSets; is++ )
	{
		pSet = (SMIObjectSet*)allSMIObjectSets.gimePointer(is);
		if (!pSet->erase(objNm))
		{
			cout << "***** Procedural error executing "
			<< " DestroyObjectIns::exec() " << endl
			<< " Object " << objNm << " is active in set "
			<< pSet->name() << endl;
			abort();		
		}
	}

// ---------------------------------------------------------------------

 
//  --------------  Now deal with Communication Handler
	pCommHandlerGl->undeclareObj(pSMIObj);
	pCommHandlerGl->updateDynObjList();
//  --------------------------------------------	

// ----- remove object references from Resume Handler queues (lists) ------
	resumeHandler.removeObjectRefs(pSMIObj);
// ------------------------------------------------------------------------

// ---- remove object from State Q ----------------------------------------
	while ( 1==1 )
	{
		if (pStateQGl->removeElementFirstName(objNm)) continue;
		break;
	}
// ------------------------------------------------------------------------

// ---- remove object from Executable objects Q ----------------------
	while ( 1==1 )
	{
		if (executableObjectQ.removeItem(objNm)) continue;
		break;
	}
// -------------------------------------------------------------------

// ---- remove object from its servers client data bases -------------
	pSMIObj->removeYourselfFromYourServerClients();

	allSMIObjects.unregisterObject(pSMIObj);
	delete pSMIObj; // this will require reviewing many class destructors
//---------------  object destroyed ----------------
	if ( dbg > 2 )
	{
		print_obj(objNm);
		cout << " destroyed" << endl;
	}
	
//Diag::printClientWhensOfAllObjects();
//Diag::printClientWhensOfAllObjectSets();
//Diag::printActualClientWhensOfAllObjects();

	return 0;         // create_object instruction allways finishes
}


