//-------------------------  Class   D o I n s  -------------------------------
#ifndef DOINS_HH
#define DOINS_HH

#include "parameters.hxx"
#include "instruction.hxx"
#include "parms.hxx"
#include "whenresponse.hxx"
#include "varelement.hxx"

class SMIObject;
class State;
class Action;

//                                                  Date :  5-May-1996
//                                                  Author : Boda Franek
// Copyright Information:
//      Copyright (C) 1996-2001 CCLRC. All Rights Reserved.
//---------------------------------------------------------------------------


class DoIns : public Instruction, public WhenResponse {
public:
  DoIns
 ( char lines[][MAXRECL], int lev,int *no_lines, 
  SMIObject *pobj=0, State *pstat=0, Action* act=0);

  ~DoIns();
  
  void whatAreYou();
  
  Name outShort();  // produces a short line describing DO
  
  int execute( Name& endState );   // 0-normal,  1-terminated action, 2-suspended

  void executeHp();
	
/** returns action name
*/
	Name actionName() const;

/**  overriding the method in 'WhenResponse'
*/	
	void executeResponse();

	
private:
/**
 will create a parameter string for dispatching by updating the generalized values of the other object action parameters by specific current values of local parameters where necessary. 
If something goes wrong, it does not return...fatal error
@param   parString
*/
	void createOutgoingParameters(Name& parString);
	
//--------------- D A T A ------------------------------
  int _level;   //for diag. printing
  
  Name _actionNm;
  
  VarElement _objectId;
  
  VarElement _setId;

//
  SMIObject *_pParentObject;
  
  State* _pParentState;

  Action* _pParentAction;
//
  int _numOfPara;
  Parms _doInsParameters;
};

#endif
