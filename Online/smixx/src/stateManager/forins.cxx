//-----------------------------  ForIns  Class ------- For instruction -------
#include "smixx_common.hxx"
#include <stdio.h>
#include <assert.h>

#include "parameters.hxx"
#include "ut_sm.hxx"
//---------------------- Externals ----------------------
#include "objectregistrar.hxx"
#include "registrar.hxx"
   extern ObjectRegistrar allSMIObjects;
   extern Registrar allSMIObjectSets;
#include "resumehandler.hxx"
   extern ResumeHandler resumeHandler;
//-------------------------------------------------------
#include "instruction_return_status.hxx"
#include "ptrvector.hxx"
#include "forins.hxx"
#include "smiobject.hxx"
#include "state.hxx"
#include "alarm.hxx"
#include "options.hxx"
//
//                                                        B. Franek
//                                                        April 2018
// Copyright Information:
//      Copyright (C) 1996-2018 CCLRC. All Rights Reserved.
//---------------------------------------------------------------------------
//
//------------------------------ ForIns -------------------------------------

ForIns::ForIns
	( char lines[][MAXRECL], int lev, PtrVector& allBlocks, int& no_lines
	, SMIObject *pobj, State *pstat, Action* pact)
{
//-------------------------------------------------------------------------
// Input :
// lines[0] ....... the first line after the 'for'
// lev    ....... the block level

// Output :
// no_lines ....... number of lines in 'for' (not counting the first 'for')
//----------------------------------------------------------------------------

//cout << endl << " FOR constructor started " << endl;

	_pParentObject = pobj;
   
   	_pParentState = pstat;
	
	_pParentAction = pact;

	_level = lev;

	int lin = 0;
	
	
	_argName = lines[lin]; lin++;
	_setName = lines[lin]; lin++;

	int ref_blockno,ref_level;

	sscanf(lines[lin],"%d %d",&ref_blockno,&ref_level); lin++;
//     cout << "Execute block : " << ref_blockno << 
//             " level : " << ref_level << "\n";

	_pInsBlock = static_cast<InsList*>(allBlocks[ref_blockno]);

	no_lines = 3;  // no of lines in the instruction not incl 'for'

	_suspend_flag = fresh;
		
	return ;
}
//--------------  Destructor  BF Mar 2020  ---------------------
ForIns::~ForIns()
{
	return;
}
//==================================================================
void ForIns::whatAreYou()
{
	Name temp = nBlanks(_level*4+10);
	char* ident = temp.getString();

	cout << ident
	     << "-----------------------------------" << endl;
	cout << ident
	     << "for " << _argName << " in " << _setName << endl;
	
	_pInsBlock->listInstructions();
	
	cout << ident << "endfor" << endl;
	cout << ident
	     << "-----------------------------------" << endl;

	return;
}
//========================================================================
int ForIns::execute(Name& endState)
{
	int dbg; Options::iValue("d",dbg);

	endState = "not changed";
	Name objName = _pParentObject->name();
	Name actName = _pParentAction->actionName();

	InstructionReturnStatus_t blockReturnStat;

	blockReturnStat = normal;

//debug beg
if ( dbg > 5 )
{
indent(_level*3+5);
cout  << "   FOR instruction(" << this << ") of " << actName;
indent(_level*3+5);
if (_suspend_flag == fresh)
	{ cout  << " starting execution " << endl;}
else
	{ cout  << " resuming execution " << endl;}
}
//debug end

	int beg;
	SMIObjectSet* ptnSet; Name objInSet;
	void* ptnv;
	
	

	switch ( _suspend_flag)
	{
//----------------------------------------------------------------------------
	case fresh:
//-------------------------- fresh FOR instruction ----------------------------

				
		//**** will store the list of objects in the SET localy
		
		ptnv =allSMIObjectSets.gimePointer(_setName);
		ptnSet = (SMIObjectSet*) ptnv;
		
		_setContents.clear();
		
		ptnSet->reset();
		while (ptnSet->nextObject(objInSet))
		{
			_setContents.push_back(objInSet);
		}
		//****
		
		beg =0;
		goto executeInstructionBlock;
		

//---------------------------------------------------------------------------
	case suspendedDueToSuspendedBlock:
//------------------------- suspended due to suspended instruction block ----

		beg=_suspendedAtObj;
		goto executeInstructionBlock;
	
//---------------------------------------------------------------------------
	default:
//---------------------------------------------------------------------------
		cout << " FORIns.... suspend flag has illegal value : " 
                     << _suspend_flag << "\n";
		Alarm::message("FATAL", objName,"Executing FOR ... internal error");
	}
	
//----------------------------------------------------------------------------		
		executeInstructionBlock:
//----------------------------------------------------------------------------
		int objNum = _setContents.size();
		
		//*** execute the FOR instruction block for every object in 
		//   _setContents. For each object it sets the argument of
		//    the block to be the name of the current object.
		for (int io=beg; io<objNum; io++ )
		{
			//*** before execution of the block, we have to set its
			//    arguments. In case of FOR Ins Block,
			//    there is only one.
			NameVector tempArgs; tempArgs+= _setContents.at(io);
			_pInsBlock->setCurrentArgs(tempArgs);
			//************************************
				
			blockReturnStat = _pInsBlock->execute(endState);
	
			if (blockReturnStat == instructionSuspended ) { 
				_suspend_flag = suspendedDueToSuspendedBlock;
				_suspendedAtObj = io;

//debug beg
if ( dbg > 5 )
{
indent(_level*3+5);
cout  << "   FOR instruction(" << this << ") of " << actName << " suspended " << endl;
}
//debug end
				return instructionSuspended; 
			}
			else if ( blockReturnStat == normal )
			{			}
			else if ( blockReturnStat ==
			          instructionFinishesTerminatingAction ) { break;}
			else { cout << "ForIns...return from execute is illegal\n";
				Alarm::message("FATAL", objName,"Executing IF ... internal error");
			} 
		}
		//***********************************
		
		_suspend_flag = fresh;
//debug beg
if ( dbg > 5 )
{
indent(_level*3+5);
cout  << "   FOR instruction(" << this << ") of " << actName << " finished " << endl;
}
//debug end

		return blockReturnStat;

}


