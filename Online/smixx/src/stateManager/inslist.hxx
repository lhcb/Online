//------------------------- InsList Class ------------------------------------
#ifndef INSLIST_HH
#define INSLIST_HH

#include "parameters.hxx"
#include "instruction.hxx"
#include "doins.hxx"
#include "termins.hxx"
#include "setins.hxx"
#include "action.hxx"
#include "ptrvector.hxx"
class SMIObject;
class State;
class Action;
#include "instruction_return_status.hxx"
//                                                             B. Franek
//                                                             April 1996
// Copyright Information:
//      Copyright (C) 1996-2001 CCLRC. All Rights Reserved.
//-----------------------------------------------------------------------------
//
class IfIns;

class InsList {
public :
  InsList(int blknum, int lev, SMIObject *pobj, State*, Action*);

  ~InsList();

  void initialise( char lines[][MAXRECL], PtrVector& allBlocks, 
                   int& no_lines);
  void listInstructions();
  InstructionReturnStatus_t execute( Name& endState );
  
  	bool hasArgs();
	
	void setCurrentArgs(const NameVector& currArgs);
	
private :

//--- Data -----------
  int _block_id;
  int _level;

	PtrVector _instructions;

  int _isuspended;    // instruction at which it was suspended

  SMIObject *_pParentObject;

  State* _pParentState;
  Action* _pParentAction;
/**
        Some SMI instruction can now have arguments rather than fixed values
	for some of the elements in an instruction (such as 'object name'
	in DO instruction).
	At the time of the execution of the instruction these arguments are
	replaced by their values which are supplied by the 'instruction's
	parent block(list)'.
	Consequently, we now have the following two new private data items:
*/		
	bool _hasArgs;  // true if any of the block's instruction has arguments
	
	NameVector _currentArgs; // Arguments of the current block and
	                         //  supplied from outside of the block
};

#endif
