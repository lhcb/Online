//----------------------------  class Instruction ----------------------------
//                                                      Date : May-2018
//                                                     Author: Boda Franek
// Copyright Information:
//      Copyright (C) 1996-2018 CCLRC. All Rights Reserved.
//---------------------------------------------------------------------------
//
#include "instruction.hxx"
#include "ut_sm.hxx"
#include "action.hxx"

//-----------------------------------------------------------------------------
Instruction::Instruction()
{
	return;
}
//---------------  Destructor  BF Mar 2020  ---------------------------
Instruction::~Instruction() { return; }
//-----------------------------------------------------------
Name Instruction::insDiagPrintOffset(int level, Action* pParentAction) const
{
	int ident = pParentAction->actionDiagPrintOffset() +
				3*level + 5;
	return nBlanks(ident);
}
