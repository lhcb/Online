#ifndef INSTRUCTION_HH
#define INSTRUCTION_HH

#include "namevector.hxx"
class Action;

class Instruction {
public:
	Instruction();
    virtual ~Instruction();
    virtual void whatAreYou()  = 0;
    virtual int execute(Name& endState)  = 0;   // 0-normal,  
                                                // 1-terminated action,
						// 2-suspended
/**
        Some SMI instruction can now have arguments rather than fixed values
	for some of the elements in an instruction such as 'object name'
	in DO instruction.
	At the time of the execution of the instruction these arguments are
	replaced by their values which are supplied by the 'instruction's
	parent block(list)'.
	Consequently, we now have the following two new public methods:
*/		

	virtual bool hasArgs() { return false; };  
	
	virtual void setCurrentArgs(const NameVector& ) { return; };
	
	virtual Name insDiagPrintOffset(int level, Action* pParentAction) const;
	  
protected:

};  

#endif
