//
//  options.cxx
//  smiSM-Options
//
//  Created by Bohumil Franek on 28/03/2016.
//  Copyright © 2016 Bohumil Franek. All rights reserved.
//

#include "version.hxx"

#include <stdlib.h>
#include <string.h>
#include "options.hxx"

Options* Options::_pSinstance = NULL;

Options::Options() {}

void Options::createInstance()
{
    if (!_pSinstance)
    {
        _pSinstance = new Options();  // only one instance is allowed
        _pSinstance->initialise();
    }
}

Options* Options::pOptions()
{
    createInstance();
    return _pSinstance;
}

void Options::takeOption( const char Id[],
                const  char type[],
                const char defvalue[],
                const char name[],
                const char comment[] )
{
    Option temp(Id,type,defvalue,name,comment);
    
    _options.push_back(temp);
    
}

void Options::initialise()
{
//              Id, type, def-value, name,           comment
    takeOption("d","INT","3","Diagnostic Level","default is level 3");
    takeOption("u","BOOL","0","Unlocked IFs","");
    takeOption("t","BOOL","0","New time format","including miliseconds");
    takeOption("dns","STRING","","Dns node","if not specified, the current node is taken");
    takeOption("loopMaxChanges","INT","25","Max no of state changes per second","default is 25");
    takeOption("loopMaxChangesFatal","INT","5000","Fatal no of state changes per second","default is 5000");
    takeOption("loopMinDuration","FLOAT","-1","Min Loop Duration","default is -1, i.e. No Loop Control");
    
    
    return;
}

void Options::printOptions()
{
    createInstance();

    int noOpts = (int)(_pSinstance->_options.size());

    cout << "Options:" << endl;   
    for (int i=0; i<noOpts; i++) 
    {
        cout << _pSinstance->_options[i].outString() << endl;
    }
    
    return;
}

void Options::printUsage()
{
    cout << "  Usage:" << endl << endl;
    cout << "  smiSM [-v]";
    
    int noOpts = (int)_options.size();
    
    for ( int i=0; i<noOpts; i++)
    {
        if ( i>0 && i%3 == 0)
        {
            cout << endl << "       ";
        }
        printf(" [-%s",(_options[i].gimeId()).getString());
        if ( _options[i].gimeType() == "BOOL" )
        {
            cout << "]";
        } else
        {
            cout << " '...']";
        }
    }
    cout << endl;
    return;
}

int Options::cValue(const char *Id, char*& ptr)
{
    Name tempId = Id;
    
    int found = _pSinstance->findId(tempId);
    
    if ( found < 0)
    {
        ptr = NULL; return 0;
    }
    
    ptr = _pSinstance->_options[found].cValue();
    return 1;
}

int Options::iValue(const char *Id, int &val)
{
    Name tempId = Id;
    
    int found = _pSinstance->findId(tempId);
    
    if ( found < 0)
    {
        val = 0; return 0;
    }
    
    val = _pSinstance->_options[found].iValue();
    return 1;
}

int Options::fValue(const char *Id, float &val)
{
    Name tempId = Id;
    
    int found = _pSinstance->findId(tempId);
    
    if ( found < 0)
    {
        val = 0.0; return 0;
    }
    
    val = _pSinstance->_options[found].fValue();
    return 1;
}

int Options::findId(const Name& Id)
{
    int noOpts = (int)_options.size();
    
    for (int i=0; i<noOpts; i++)
    {
        if ( Id == _options[i].gimeId() )
        {
            return i;
        }
    }
    cout << "error    option " << Id << "  not found" << endl;
    return -1;
}

int Options::newValue(const char Id[], const char value[])
{
    int found = _pSinstance->findId(Id);
    
    if ( found < 0 )
    {
        cout << "error    option " << Id << "  not found" << endl;
        return 0;
    }
    
    if (!_pSinstance->_options[found].newValue(value)) return 0;
    return 1;
}


void* Options::gimeConvValue(const char Id[], int& err)
{
    void* ptr = NULL;
    Name tempId = Id;
    err =0;
    
    int noOpts = (int)_options.size();
    
    for (int i=0; i<noOpts; i++)
    {
        if ( tempId == _options[i].gimeId() )
        {
            ptr = _options[i].gimeConvValue();
            return ptr;
        }
    }
    cout << "error    option " << tempId << "  not found" << endl;
    err = 1;
    return ptr;
}

Name Options::gimeType(const char Id[], int& err)
{
    Name tempId = Id;
    err =0;
    
    int noOpts = (int)_options.size();
    
    for (int i=0; i<noOpts; i++)
    {
        if ( tempId == _options[i].gimeId() )
        {
            return _options[i].gimeType();
        }
    }
    cout << "error    option " << tempId << "  not found" << endl;
    err = 1;

    return "";
}

//--------------------------------------------------------------------------
void Options::processCommandLine( int argc, const char* argv[],
                                 Name& domain, Name& sobjFile) {
    createInstance();
    
    int input_error = 0;
    domain = "\0"; sobjFile = "\0";
    
    for(int i=1; i<argc && !input_error; i++)
    {
        if(!strcmp("-v",argv[i])){
            cout << "   Version  - " << smixxVersion << "-     Date : " << smixxDate << endl;
              exit(0);
        }
        if (argv[i][0] == '-')
        {
            int err;
            const char* ptrId = &argv[i][1];
            
            Name type = _pSinstance->gimeType(ptrId,err);
            if (err != 0)
            {
                input_error = 1;
            }
            else
            {
                if ( type == "BOOL")
                {
                    _pSinstance->newValue(ptrId,"1");
                }
                else
                {
                    i++;
                    if ( !_pSinstance->newValue(ptrId, argv[i]) ) {}
                }
            }
        }
        else {
            if (domain == "\0") {
                domain = argv[i]; domain.upCase();
            }
            else if (sobjFile == "\0") {
                sobjFile = argv[i];
            }
            else {
                input_error = 1;
            }
        }
    }
    
    if (domain == "\0" || sobjFile == "\0") input_error =1;
    
    if(input_error) {
        _pSinstance->printUsage();
        exit(2);
    }
    
    char* pStr = sobjFile.getString();
    char* ptnf;
    
    if ( (ptnf = strstr(pStr,".sobj")) ) {  // contains .sobj 
        if (!strcmp(ptnf,".sobj")) { // it is at the end 
            return;
        }
    }
    sobjFile += ".sobj";
    return;
}


Name Options::gimeExportString()
{

    createInstance();

    int noOpts = (int)(_pSinstance->_options.size());
    
    Name temp = "";
    
    for (int i=0; i<noOpts; i++) 
    {
    	if ( i> 0 ) temp += "|";
        temp += _pSinstance->_options[i].gimeExportString();	
    }
    
    return temp;
}
