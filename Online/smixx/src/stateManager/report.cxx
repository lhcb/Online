//----------------------------------------------------------------------
//
//                                              Author :  B. Franek
//                                              Date :  January 2017
// Copyright Information:
//      Copyright (C) 1996-2017 CCLRC. All Rights Reserved.
//----------------------------------------------------------------------

#include "report.hxx"
#include <stdlib.h>
#include "name.hxx"

extern Name smiDomain;

#include "commhandler.hxx"

extern CommHandler *pCommHandlerGl;

 //--------------------------------------------------------------------------
void Report::initialise()
{

	pCommHandlerGl->declareReport();
       
       return; 
}
 //--------------------------------------------------------------------------
void Report::message(const Name& severity, const Name& object, const Name& mess)
{
	
	Name reportMessage("SMI");
	reportMessage += " "; reportMessage += severity;
	reportMessage += " "; reportMessage += smiDomain;
	if (!(object == ""))
	{
		reportMessage += "::";
		reportMessage += object;
	}
	reportMessage += ", ";
	reportMessage += mess;
	char* pRepmess = reportMessage.getString();

//	cout << "  Report Message : " << endl
//	<< pRepmess << endl;	
	pCommHandlerGl->publishReport(pRepmess);
			
       return; 
}
