//-----------------------------------------------------------------------
//                         SMIObjectSet  Class
//                                                 B. Franek
//                                                  03-November-2009
// Copyright Information:
//      Copyright (C) 1999-2009 CCLRC. All Rights Reserved.
//-----------------------------------------------------------------------
//
#ifndef SMIOBJECTSET_HH
#define SMIOBJECTSET_HH


class SMIObject;

#include "name.hxx"
#include "namelist.hxx"
#include "smisetcontainer.hxx"
#include "clientwhens.hxx"

class  SMIObjectSet {

	public :
	
		SMIObjectSet();
		
		virtual ~SMIObjectSet();

		virtual void out(const char* offset) = 0; 

//-----------------------------------------------------------

		Name name() const;

		int numOfObjects() const;

		void reset();

		int nextObject(Name& name); // return 0 means no more
		
		int nextObject(Name& name, SMIObject*& ptnr) ;


/**
  the function adds new client when to this object set. The when is identified
  by its object name, its state name and its index within that state
*/
		int addClientWhen(const char* whenObjName, const char* whenStateName, int whenInx);
/**
    Will print all the client whens
*/
		int printClientWhens() const;


/** Will inform all the SMI objects it contains about their membership
*/
		int informObjectsAboutMembership();
/** Will return the reference to the set's client whens
*/		
		const ClientWhens& gimeClientWhensRef() const;		
/**
  the method will return in the argument the current object list
*/
		void gimeObjectList(SMISetContainer& objectList) const;
/**
  the method will merge into the atgument the current object list
*/
		void mergeInObjectList(SMISetContainer& objectList) ;
	
/**  will inform the Set that there is a client WAIT_FOR refering to it.
     The Set keeps the list of these objects
*/
	void youHaveClientWF(Name& clientobj);
	
/**  will inform the Set that it's client WAIT_FOR can be unregistered.
*/
	void unregisterClientWF(Name& clientobj);

/**
	will provide the list of current WAIT_FOR clients
*/
	NameList& gimeWFClients();
	
/**
      will check if supplied object is one of the clients
*/
	bool isObjectClient(const Name objName);

	virtual int remove(const Name&) {return 1;};

	virtual int erase(const Name&) {return 1;};
/**
  Will remove given object (objName) from its Clients
*/
	int removeObjectFromYourClients(const Name& objName);
	
	protected :

		Name		_name;

		SMISetContainer	_objectList;
/**
  Keeps information about whens in the entire domain which reference this
  object set. This is build after the initialisation stage
*/
		ClientWhens _clientWhens;
	
/** Keeps the list of objects which are suspended because of WAIT_FOR
   and this Set is their server
*/
	NameList _clientObjectsWF;

};
#endif
