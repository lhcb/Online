
//----------------------------- SMISetContainer  Class ---------------------
#ifndef SMISETCONTAINER_HH
#define SMISETCONTAINER_HH

class SMIObject;
class NmdPtnr;

#include "name.hxx"
#include "smisetmember.hxx"
#include <vector>
//
//                                                               July 2018
//                                                               B. Franek
// Copyright Information:
//      Copyright (C) 1996-2018 CCLRC. All Rights Reserved.
//-----------------------------------------------------------------------------

/** @class SMISetContainer
   This class keeps the information about the composition of a SMI Set.  
   From a user point of view it is just a simple container of names of
   objects (and their pointers) that are members of a given Set.
   The speciality of this container is however that the original order of
   the objects in the Set is preserved. (The original means the order
   in which the objects were first inserted.)
   Internally, this is achieved by keeping the information about the
   objects in a vector of instantiations of class 'SMISetMember'
   plus a couple of pointes to navigate around this vector. Objects are
   'switched on' when inserted, or 'switched off' when removed. Once
   an object is inserted the first time (at the end of the vector), it
   can never leave. It can only be 'switched on/off'.
   Or it can be 'erased', i.e. completely removed from the vector.

   NB: Note, there is a difference between being in the Container and being in
       the Set ! Being in the set also means being switched on.
       
  @author Boda Franek
  @date July 2018
*/


class SMISetContainer
{
public :
	SMISetContainer();
	
	SMISetContainer( const SMISetContainer& );

	~SMISetContainer();	
/**
   returns the number of objects in the Set
   (i.e present in the vector and switched on)
*/
	int numOfEntries() const; 

/**
   adds object to the Set.
   (if not present in the vector it adds it to the end,or just switches it on.) 
   @param item   Object name and its pointer
   @retval <1> object was already in the vector but switched off.
                     In this case it is switched on.
	       this is a new object and is added to the vector as
	        'switched on'
   @retval <0>  object already in the vector and was switched on
*/	
	int add(NmdPtnr& item);
/**
   removes object from the Set.
   @param item   Object name and its pointer. (The pointer is ignored.)
   @retval <1>   Object is in the vector and was switched on.
   @retval <0>  object either not found in the vector or it is there
                but switched off
*/
	int remove(NmdPtnr& item);
/**
   removes all objects from Set.
   (by switching them all off)
*/	
	void removeAll();
/**
   completely erases the object from SMISetContainer.
   @param   objNm  Name of the object to be removed.
   @retval <1>   Object was in the vector, switched off and was erased.
                 Or object was not found.
   @retval <0>  Object found, but it was switched on. Nothing was done.   
*/
	int erase(const Name& objName);
/**
   prints the names of the objects and their pointers in  the Set
*/	
	void out(const char indent[] = " ");
/**
   prints the names of the objects  pointers and on/off flags in the Container
*/	
	void outFull(const char indent[] = " ");
/**
   returns true if object is present in the container
*/
	bool isPresent(const Name& objName);
/**
  guarantees that the object returned in the next 'nextItem' call is the
  first object in the Set.
  (this means the first object in the vector that is switched on.)
*/	
	void reset();
/**  
  returns the next object in the Set 
   @param item   Object name and its pointer. 
   @retval <1> success.
   @retval <0> no more objects in the Set.  
*/	
	int nextItem(NmdPtnr& item);
	
private:
/**
   calculates and sets the pointer pointing to the first object in
   _members vector that is switched on.
*/
	void calcFirstOn();
	
//     DATA 

 	std::vector<SMISetMember> _members;
	
/**
   the pointer pointing to the first object in _members vector that
   is switched on.
*/
	int _inxFirstOn;
/**
   the pointer pointing to the object in _members vector that will be returned 
   with the next call to 'nextItem'
*/	
	int _currentInx;
	
};

#endif
