//------------------------  SmpCondTyp2  Class ------------------------------------
#include "smixx_common.hxx"
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "name.hxx"
#include "namelist.hxx"
#include "smpcondtyp2.hxx"
#include "smiobject.hxx"
#include "smiobjectset.hxx"
#include "ut_sm.hxx"
#include "dic.hxx"
#include "alarm.hxx"
#include "options.hxx"
#include "getvarelem.hxx"
//----------------- Externals --------------------
   extern  Name smiDomain;
#include "registrar.hxx"
   extern Registrar allSMIObjectSets;
//------------------------------------------------
//                                                         B. Franek
//                                                         01-Aug-1996
// Copyright Information:
//      Copyright (C) 1996-2001 CCLRC. All Rights Reserved.
//----------------------------------------------------------------------------
SmpCondTyp2::SmpCondTyp2 ( char lines[][MAXRECL], int& no_lines,
                           Action* pAct)
: SmpCond()
{
//-------------------------------------------------------------------------
// Input :
// lines[0] ....... the first line of the simple condition
// Output :
// no_lines ....... number of lines in the simple condition
//----------------------------------------------------------------------------
	_type = 2;
	_frozen = 0;
	_pParentAction = pAct;
	
	int il;

	il = 0;
	
	il++;
	_objectSetId = lines[il];
		
	il++;
	if ( strcmp(lines[il],"all_in") == 0 ) {
		_all_in = 1;
	}
	else if ( strcmp(lines[il],"any_in") == 0 ) {
		_all_in = 0;
	}
	else {
		cout << "Looking for 'any_in' or 'all_in' but found : "
		     << lines[il] << endl;
		Name temp = "-";
		Alarm::message("FATAL",temp," condition initialisation");
	}
	
	il++;

	if (!strcmp(lines[il],"in_state")) {
      		_inState = 1;
	}
	else if (!strcmp(lines[il],"not_in_state")) {
		_inState = 0;
	}
	else {
		cout << " Error initialising Simple condition \n";
		cout.flush();
		Name temp = "-";
		Alarm::message("FATAL",temp,"condition initialisation");
		}

	il++;

	int noOfStates;
	sscanf(lines[il],"%d",&noOfStates);
	assert ( noOfStates > 0 );
    
	Name tmpName;
   
	for (int ist=0; ist < noOfStates; ist++) {
		il++;
		tmpName = lines[il];
		if ( tmpName == "&SMIDOMAIN" ) {
			tmpName = smiDomain;
		}
		_states += tmpName;
	}

	no_lines = il + 1;
	return ;

}
//------------------  Destructor  Mar 2020  BF  ---------------------------
SmpCondTyp2::~SmpCondTyp2()
{
	return;
}
//--------------------------- whatAreYou --------------------------------------
Name SmpCondTyp2::whatAreYou() const {

  Name tmpString;
  
      tmpString += "( ";
      
      if (_all_in == 1) {
         tmpString += "all_in ";
      } else {
         tmpString += "any_in ";
      }
      
      tmpString += _objectSetId.outString();
      
      if ( _inState == 0) {
         tmpString += " not_in_state ";
      } else {
         tmpString += " in_state ";
      }

      Name stateString;
      makeStateString(_states,stateString);
      tmpString += stateString.getString(); 
      
      tmpString += " )";

      return tmpString;

}
//------------------------------  objectsToLock -------------------------------
void SmpCondTyp2::objectsToLock(NameList& list) const {

	
	SMIObjectSet* ptnSet;

	if (_frozen == 1) {
		ptnSet = (SMIObjectSet *)&_frozenSet;
	}
	else {
		Name setNm = objectSetName();
		void* ptnv = allSMIObjectSets.gimePointer(setNm);
//		cout << " ptnv " << ptnv << endl;
		
		if (ptnv == 0) { cout << " Set " << setNm <<
		         "not declared " << endl;
			 Alarm::message("FATAL",setNm,"SET not declared");
		}
		ptnSet = (SMIObjectSet*) ptnv;
	}

	list.removeAll();

	ptnSet->reset();
	Name objinset;
	while (ptnSet->nextObject(objinset)) {
		list.add(objinset);
	}
	return;
}

//------------------------------ freeze ------------------------------------
void SmpCondTyp2::freeze() {

	int dbg; Options::iValue("d",dbg);

	assert(_frozen==0);
	_frozen = 1; 

	Name setNm = objectSetName();
	void* ptnv = allSMIObjectSets.gimePointer(setNm);
	if (ptnv == 0) {
		cout << " Set " << setNm 
	       	      << " not declared " << endl;
	 	Alarm::message("FATAL",setNm,"SET not declared");
	}
	SMIObjectSet* ptnSet = (SMIObjectSet*) ptnv;
	_frozenSet.copyObjectList(*ptnSet);
//debug beg
	if ( dbg > 5 )
	{
		cout << endl << " ==== SmpCondTyp2::freeze() =======" << endl
		<< "   Objects Set " << setNm << " frozen to :" << endl;
		_frozenSet.out(" "); cout << endl;		
	}
//debug end
	
	return;
}
//------------------------------ unfreeze ---------------------------------
void SmpCondTyp2::unfreeze() {
	assert(_frozen==1);
	_frozen = 0;
	return;
}


//---------------------------- evaluate ----------------------------------
int SmpCondTyp2::evaluate() {

	Name setNm = objectSetName();
	
        int noOfStates = _states.length();
//	cout << " Set name " << setNm << " andflg " << _all_in << endl;
	
	SMIObjectSet* ptnSet;

	if (_frozen == 1) {
		ptnSet = &_frozenSet;
	}
	else {
		void* ptnv = allSMIObjectSets.gimePointer(setNm);
//		cout << " ptnv " << ptnv << endl;
		
		if (ptnv == 0) { cout << " Set " << setNm <<
		         "not declared " << endl;
			 Alarm::message("FATAL",setNm,"SET not declared");
		}
		ptnSet = (SMIObjectSet*) ptnv;
	}

	if (ptnSet->numOfObjects() == 0) {return -2;}

	Name objinset; int result;
	result = _all_in;

	SMIObject* pObj;

//       ptnSet->out(" ");
	dim_lock();
	ptnSet->reset();
//cout << endl << "eval_set started execution" << endl;		

	while (ptnSet->nextObject(objinset,pObj)) {
//		cout << objinset <<  "   " <<   pObj << endl;
		if (pObj == 0) {
			cout << " Object " << objinset
			<< " in set " << setNm 
			<< " does not exists" << endl;
			ptnSet->out(" ");
			Alarm::message("FATAL",objinset,"object is not member of SET");
		}
		Name stateName;
		if (pObj->currentState(stateName) < 0) {
			dim_unlock();
//cout << "SMPCondTyp2::evaluate() interupted. One of the objects is busy"
//     << endl <<endl;
			return -1;
		}

		int inStateList = 0;
		for (int ist = 0; ist<noOfStates; ist++) {
			if ( _states[ist] == stateName ) {
				inStateList = 1;
				break;
			}
		}
		if (inStateList == _inState) {
			if (_all_in == 0) {result = 1;}
		}
		else {
			if (_all_in == 1) {result = 0;}
		}
//		cout << " objinset " << objinset 
//		<< " inStateList " << inStateList << " result "
//		 << result << endl;
	}
	dim_unlock();
	return result;
}
//-----------------------------------  BF Sep 2008  ------------------
void SmpCondTyp2::removeObjectFromFrozenObjectSet( const Name& objName, const Name& setName)
{
	int dbg; Options::iValue("d",dbg);

	assert (_frozen==1);
	if ( objectSetName() == setName )
	{
		_frozenSet.remove(objName);
//debug beg
		if ( dbg > 5 )
		{
			cout << endl << " ==== SmpCondTyp2::removeObjectFromFrozenObjectSet =======" << endl
			<< " New frozen set of " << objectSetName() << " :" << endl;
			_frozenSet.out(" "); cout << endl;		
		}
//debug end

	}
	
	return;
} 
//-------------------------------------------------------------------------
Name SmpCondTyp2::objectSetName() const
{	
	Name setNm = _objectSetId.name();
	if ( !(setNm == "") ) return setNm;
	
	void* ptnvSet;
	int ret = GetVarElem::actualSetName( _objectSetId,
								_pParentAction,
								"typ2smpcond",
								 setNm, ptnvSet);
	if (ret) return setNm;
	
	Name temp = "-";
	Alarm::message("FATAL",temp,"simple condition can not be executed");
	return setNm;
}

