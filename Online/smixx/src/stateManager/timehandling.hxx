//----------------------------------------------------------------------
//
//                                              Author :  B. Franek
//                                              Date :  March 2016
// Copyright Information:
//      Copyright (C) 1996-2016 CCLRC. All Rights Reserved.
//----------------------------------------------------------------------
#ifndef TIMEHANDLING_HH
#define TIMEHANDLING_HH

class TimeHandling {
public:
	static double floatTime();
};

#endif
