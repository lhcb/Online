// wfwcontinue.cxx: implementation of the WFWContinue class.
//
//                                                B. Franek
//                                               October 2015
// Copyright Information:
//      Copyright (C) 1999/2015 CCLRC. All Rights Reserved.
//////////////////////////////////////////////////////////////////////

#include "wfwcontinue.hxx"

WFWContinue::WFWContinue( char lines[][MAXRECL], int &noLines)
{
// do
// &CONTINUE
// 0
// &THIS_OBJECT
	Name temp = lines[0]; // to keep compiler happy
	noLines = 4;
	return;
}
//-----------------  Destructor  BF Mar 2020  ----------------------
WFWContinue::~WFWContinue() { return; }

//----------------------------------------------------------------------
Name WFWContinue::nextMove() { return "&CONTINUE";}

Name WFWContinue::outShort() { return "continue"; }
