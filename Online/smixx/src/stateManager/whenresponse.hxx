// whenresponse.hxx: interface for the WhenResponce class.
//
//                                                  B. Franek
//                                                 October 2015
//
//////////////////////////////////////////////////////////////////////
#ifndef WHENRESPONSE_HH
#define WHENRESPONSE_HH
#include "name.hxx"

class WhenResponse 
{
public:
	WhenResponse();

	virtual ~WhenResponse();

	virtual void executeResponse();
	
	virtual Name nextMove();
	
	virtual bool stay_in_state();
	
	virtual Name outShort();

protected :

};

#endif 
