// action.cxx: implementation of the Action class.
//
//                                                B. Franek
//                                               1 June 1999
// Copyright Information:
//      Copyright (C) 1999-2001 CCLRC. All Rights Reserved.
//////////////////////////////////////////////////////////////////////
#include "smixx_common.hxx"
#include <assert.h>
#include "action.hxx"
#include "param.hxx"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Action::Action(const Name& name)
 : SMLUnit("Action",10,name),
   _pActionHeadBlock(NULL), _pInsList(NULL), _actionBlocks() 
{
//   cout << "    action " << _name << endl;
}


Action::~Action()
{
    delete _pSMLcode;
}
//-------------------------------------------------------------------
void Action::translate() {
//  an action consists of action header block (that is where action parameters 
//  are) followed by block of action instructions

// First the header block

	_pActionHeadBlock = new ActionHeadBlock();
	assert (_pActionHeadBlock != 0);

	SMLUnit* pUnit = _pActionHeadBlock;
	Name unitType = "action head";
	_internalSMLUnits.addRegardless(unitType,pUnit);
	pUnit->setParentUnit(this);

	Name name;
	_pInsList = 0;

	SMLline line; 
	int numOfLines = _pSMLcode->length();

// will collect lines untill the first instruction is found
	int listBeg = numOfLines;  // begining of the instruction block
	for (int i = 0; i<numOfLines; i++)
	{
		line = (*_pSMLcode)[i];
		bool insLine = line.instructionLine(name);
		if ( insLine )  // is it an istruction line?
		{
			listBeg = i; break;		
		}
		else { pUnit->acceptLine(line); }
	}
	
// Now the instructions	
	if ( listBeg < numOfLines ) // instruction block found
	{
		_pInsList = new InsList(this,0);
		pUnit = _pInsList;
		assert (pUnit != 0);
		unitType = "InsList";
		_internalSMLUnits.addRegardless(unitType,pUnit);
		pUnit->setParentUnit(this);
	}
	
	for ( int i=listBeg; i<numOfLines; i++)
	{
		line = (*_pSMLcode)[i];
		pUnit->acceptLine(line);
	}

	 translateUnits();

//cout << endl << " Action " << _name << endl;
//printBlocks();
	
	orderBlockPointers();
	
//cout << endl << endl << "  Ordered Blocks "<< endl;
//printBlocks();
	
	return;
}
//-------------------------------------------------------------------
void Action::orderBlockPointers()
{	
	PtrVector newBlocks;
	int numBlocks =_actionBlocks.length();
	int level = 0;
	for (;;)
	{
		if ( newBlocks.length() == numBlocks ) {break;}
	
		for (int i=0; i<numBlocks; i++ )
		{
			InsList* point = (InsList*)_actionBlocks[i];
			if ( level == point->level() )
			{ 
				newBlocks += point;
			}
		}
		level++;
	}
	_actionBlocks = newBlocks;

	for (int i=0; i<numBlocks; i++ )
	{
		InsList* point = (InsList*)_actionBlocks[i];
		point->setId(i);
	}

	return;
}
//-------------------------------------------------------------------------
void Action::printBlocks()
{
	int numBlocks = _actionBlocks.length();
	for (int i=0; i<numBlocks; i++ )
	{
		InsList* point = (InsList*)_actionBlocks[i];
		cout << endl << " Block " << i 
		<< "  Level " << point->level() << endl;
		point->printCode();
	}
	return;
}	
//-----------------------------------  BF April 2018  ----------------
void Action::outSobj(ofstream& sobj) const
{

	sobj << "*ACTION" << endl;

    sobj << _name.getString() << endl;

	int nattr = 0; 
//	int numpar = _pParameters->numOfEntries();
	int numpar = _pActionHeadBlock->_parameters.numOfEntries();

	sobj << "    " << numpar << "    " << nattr << endl;

	for (int ip=0; ip<numpar; ip++) {
		Name name, indiValue, type;
		Param par = _pActionHeadBlock->_parameters.getParam(ip);
		type =	par.paramDeclType();
		name = par.paramName();
		indiValue = par.paramIndiValue();
		
		sobj << type.getString() << endl;
		sobj << name.getString() << endl;
		sobj << indiValue.getString() << endl;
	}

	
//cout << " action " << _name << endl;

	int numBlocks = _actionBlocks.length();
	
	for ( int i=0; i<numBlocks; i++ )
	{
		InsList* ilist = (InsList*)_actionBlocks[i];
		ilist->outSobj(sobj);
	}


	sobj << "*END_ACTION" << endl;
	return;
}
Name Action::name() const
{
	return _name;
}

Name Action::actionName() const
{
	return _name;
}
//-------------------------------------------------------------------------
int Action::numParameters() const
{
	return ( _pActionHeadBlock->_parameters).numOfEntries();
}
//-------------------------------------------------------------------------
Parms* Action::pActionParameters()
{
	return ( &_pActionHeadBlock->_parameters);
}
//-------------------------------------------------------------------------
void Action::registerBlock( InsList* pBlock)
{
	_actionBlocks += pBlock;
	return;
}
