// callins.cxx: implementation of the CallIns class.
//
//                                                B. Franek
//                                              September 2020
//
//////////////////////////////////////////////////////////////////////
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "smlunit.hxx"
#include "smlline.hxx"
#include "name.hxx"
#include "utilities.hxx"
#include "callins.hxx"
#include "action.hxx"
#include "smiobject.hxx"
#include "smiobjectset.hxx"
#include "ut_tr.hxx"
#include "errorwarning.hxx"
#include "nmdptnr.hxx"

#include "registrar.hxx"

extern Registrar allObjects, allObjectSets, allClasses;

#include "objectregistrar.hxx"
extern ObjectRegistrar allSMIObjects;

#include "reservednames.hxx"
#include "param.hxx"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CallIns::CallIns():
	Instruction(),
	_functionNm(""),
	_parameters()
{
   _name = "call";
//   cout << "Call constructor called" << endl;
   return;
}

CallIns::~CallIns()
{
    delete _pSMLcode; // this should not be necessary, SMLUnit destructor will do it.
}
//-----------------------------------------------------------------------
void CallIns::translate() {

//SMLUnit::out(" "); 

	Name token; int idel,jdel; int inext,jnext;
	
	SMLline lineBeingTranslated;
	
	lineBeingTranslated = (*_pSMLcode)[0];
	int ist,jst;
	
	ist = 0; jst = _firstColOfUnit;
//cout << " Starting translating CALL " << ist << " " << jst << endl;
//char temp[] = " "; _pSMLcode->out(temp);

	char del = getNextToken(_pSMLcode,ist,jst," ",token,idel,jdel,inext,jnext);
	token.upCase(); token.trim();

	if ( token == "CALL"){}
	else {
		ErrorWarning::printHead("ERROR",lineBeingTranslated
		,"expected CALL instruction not found");
		cout << " Found instead " << token << endl;
		throw FATAL;
	}

	ist = inext; jst = jnext;

	if (ist < 0) {
		ErrorWarning::printHead("ERROR",lineBeingTranslated
		,"  Nothing follows the keyword DO");
		throw FATAL;
	}
	
	lineBeingTranslated = (*_pSMLcode)[ist];
	del = getNextToken(_pSMLcode,ist,jst," (",_functionNm,idel,jdel,inext,jnext);
	_functionNm.upCase(); 
	if ( !check_name(_functionNm) ) {
		ErrorWarning::printHead("ERROR",lineBeingTranslated);
		cout << " Function: " << _functionNm << " is not a name" << endl;
		throw FATAL;
	}

//cout << " Function : " << _functionNm << endl;

	if ( del == '(' ) {
		ist = idel, jst = jdel;
		_parameters.initFromSMLcode( 0, _pSMLcode, ist,jst,inext,jnext);
// If something goes wrong, Translator is terminated inside the method
	}

	if (inext<0) {return;}  // we are at the end of code
	
//  The following is necessary because the parameter method does not at the
//  find the next non-blank character and so
// we have to do it on its behalf
	ist = inext; jst = jnext;
	int dum1,dum2,dum3,dum4;
	firstNonBlank(_pSMLcode,ist,jst,dum1,dum2,inext,jnext,dum3,dum4);

	if (inext>=0) {
		ErrorWarning::printHead("ERROR",lineBeingTranslated
		,"Some crap follows DO instruction");
		throw FATAL;
    }

	return;
}
//---------------------------------------------------------------------------------
void CallIns::out(const Name offset = "") const
{
	cout << offset << outString() << endl;
  return;
}
//----------------------------------  BF April 2000  ------------------
void CallIns::outSobj(ofstream& sobj) const
{

	sobj << "call" << endl;

	sobj << _functionNm.getString() << endl;

	int numpar = _parameters.numOfEntries();

	sobj << "    " << numpar << endl;

	for (int ip=0; ip<numpar; ip++) {
		Name name, indiValue;
		name = _parameters.getParName(ip);
		indiValue = _parameters.getParIndiValue(ip);
		sobj << name.getString() << endl;
		sobj << indiValue.getString() << endl;
	}

	return;
}
//---------------------------------------------------------------------------
int CallIns::examine()
{
	int retcode = 0;
	int examineFunctionRetCode = 0; // return code returned from 'examineFunction' 
	
	SMLline firstLine = (*_pSMLcode)[0];
	

//beg debug
/*  cout << endl 
  << " ====================== CallIns::examine() ============= " << endl;
	
  cout << "  Parent : " << _pParentUnit->unitId() 
  << "  " << _pParentUnit->unitName() << endl;
		
  cout << "     Complete Ancestry " << endl;
	
	int num;
	NameVector ids,names;
	
	_pParentUnit->ancestry(ids,names);
	
	num = ids.length();

	for (int i=0; i<num; i++)
	{
		cout << "     " << ids[i] << "  " << names[i] << endl;
	}
	
	cout << "Parent Unit Code " << endl;	

	char temp[] = " "; _pSMLcode->out(temp);	
//end debug
*/
//cout << " examining CALL " << endl << firstLine << endl
// << " Function " << _functionNm << endl;

	SMIObject* pParentObject = (SMIObject*)(_pParentUnit->parentPointer("Object")); 
	State* pParentState = (State*)(_pParentUnit->parentPointer("State")); 
	Action* pParentAction = (Action*)(_pParentUnit->parentPointer("Action")); 
		
	examineFunctionRetCode = examineFunction
		( pParentObject,pParentState,pParentAction);		
	
	int iflg = examineUnits();
	
	return retcode + examineFunctionRetCode + iflg;
}

//-------------------------------------------------------------------------------------------------------
Name CallIns::outString() const
{

	Name temp;
	
	temp = "call ";
	temp += _functionNm;
	temp += " ";
	
	temp += _parameters.outParmsString(1);

	return temp;
}
//-------------------------------------------------------------------------
int CallIns::examineParameters
	   ( SMIObject* pParentObject, State* pParentState, Action* pParentAction)
{

	if (pParentState==NULL) {} // unused argument  pacifying compiler
// examines CALL parameters from several different points of views.
// If everything is OK, it returns zero

	SMLline firstLine=firstLineOfUnit();
	int retCode = 0;

	int noPars = _parameters.numOfEntries();
	if ( noPars == 0 ) return retCode; // no parameters - nothing to do
	 
//	Name temp = " "; _parameters.out(temp);

//-----------------------------------------------------------------------------------------	
// For CALL parameters that take their values from local action or objects
// parameters, check for those parameters existence is made.
// Also the value type of the CALL parameter is taken to be that of
// the local action/object parameter

	Param param;
	
	for ( int ip=0; ip<noPars; ip++ )   // CALL parameters loop
	{
		param = _parameters.getParam(ip);
		Name indiValue = param.paramIndiValue();
		Name value, valueType;	

		if ( !param.paramValueAccessible
		            (allSMIObjects,
                      pParentObject,
                      pParentState,
                      pParentAction,
                      valueType) )
		{
			retCode = 1;
			ErrorWarning::printHead("SEVERE WARNING",firstLine);
			cout << " Parameter " << param.paramName() 
			<< "  is not accessible" << endl;
			cout << endl; _pParentUnit->printParents(); cout << endl;		
		}		
	}
	
	if (retCode != 0) return retCode;
	
// we shall now check the compatibility of the parameters with those of
// the called function

	Action* pFunction = pParentObject->gimePointerToFunction(_functionNm);
	
	retCode = retCode + checkCompatibility(pFunction);
	
	return retCode;
}
//------------------------------------------------------------------------------------
int CallIns::checkCompatibility( Action* pFunction)
{
	int retCode = 0;
	SMLline firstLine = firstLineOfUnit();
		
	Parms* pFunParms = pFunction->pActionParameters();
	
	Name offset(" ");
	
//	cout << " CALL parameters" << endl; _parameters.out(offset);
//	cout << endl << " Function parameters" << endl; pFunParms->out(offset);
	
// will loop through CALL parameters and for every one it will check that
// there exists parameter in the function of the same name and
// of the correct type.

	Name callParName, callParType, callParValue;
	int icallpar, iFunPar;
	Name funParType;

	
	SMIObject* parentObject = (SMIObject*) _pParentUnit->parentPointer("Object");
	State* parentState = (State*) _pParentUnit->parentPointer("State");  
	Action* parentAction = (Action*) _pParentUnit->parentPointer("Action");      

	for ( icallpar = 0; icallpar<_parameters.numOfEntries(); icallpar++ )
	{
		Param callPar = _parameters.getParam(icallpar);
		callParName = callPar.paramName();
		
		if ( !callPar.paramValueAccessible
		           (allSMIObjects,
                      parentObject,
                      parentState,
                      parentAction,
                      callParType) )
		{
		//	retcode = 1; review
			ErrorWarning::printHead("SEVERE WARNING",firstLine);
			cout << " Parameter " << callParName << "  not accessible" 
            << endl;
			cout << endl; _pParentUnit->printParents(); cout << endl;
			continue;		
		}
		
		iFunPar = pFunParms->getParIndex(callParName);
		if ( iFunPar < 0 )
		{
			retCode = 1; 
			ErrorWarning::printHead("ERROR",firstLine);
			cout << " Parameter " << callParName 
			<< "  not found among function parameters" << endl;
			cout << endl; _pParentUnit->printParents(); cout << endl;
			continue;		
		}
		else
		{
			if ( callParType == "" ) {}
			else
			{
				funParType = pFunParms->getParDeclType(iFunPar);
				if ( funParType == callParType ) {}
				else
				{
					retCode = 1; 
					ErrorWarning::printHead("ERROR",firstLine);
					cout << " parameter " << callParName;
					cout << " has incompatible type" << endl;
					cout << endl; _pParentUnit->printParents(); cout << endl;
					continue;		
				}
			}
		}
	}
	
//----------------------------
//  will loop through the parameters of the Function and for those that
// do not have default value will check that CALL supplies it.

	Name funParName, funParVal;
	
	for ( iFunPar = 0; iFunPar<pFunParms->numOfEntries(); iFunPar++ )	
	{
		funParName = pFunParms->getParName(iFunPar);
		funParVal = pFunParms->getParIndiValue(iFunPar);
		
		if ( funParVal == paramcons::noval )
		{
			icallpar = _parameters.getParIndex(funParName);
			if ( icallpar < 0 )
			{
				retCode = 1; 
				ErrorWarning::printHead("ERROR",firstLine);
				cout << " Parameter " << funParName << " missing" << endl;
				cout << endl; _pParentUnit->printParents(); cout << endl;		
			}
			
		}
	} 

	return retCode;
}
//-------------------------------------------------------------------------------------------
int CallIns::examineFunction
        ( SMIObject* pParentObject, State* pParentState, Action* pParentAction)
{
	SMLline firstLine = firstLineOfUnit();
	
	int retCode = 0;
	int examineParametersRetCode = 0;

	if (pParentObject->hasFunction(_functionNm))
	{
		examineParametersRetCode = examineParameters
				(pParentObject,pParentState,pParentAction);
	}
	else
	{
		retCode = 1;
		ErrorWarning::printHead("ERROR",firstLine);
			cout << " Function " << _functionNm <<
				" is not declared in object " << pParentObject->unitName() << endl;
				cout << endl; _pParentUnit->printParents(); cout << endl;
		}

	retCode = retCode + examineParametersRetCode;		
	
	return retCode;

}

