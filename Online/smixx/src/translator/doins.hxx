// doins.hxx: interface for the DoIns class.
//
//                                                  B. Franek
//                                                30 September 1999
//
//////////////////////////////////////////////////////////////////////
#ifndef DOINS_HH
#define DOINS_HH

#include "name.hxx"
#include "parms.hxx"
#include "instruction.hxx"
#include "whenresponse.hxx"
#include "varelement.hxx"

class Action;
class SMIObject;
class State;

class DoIns  : public Instruction, public WhenResponse
{
public:
	DoIns();	
/**
     Constructor when object name is not supplied from SML code
     like for example in WHEN
*/
	DoIns(const Name& objectName);

	virtual ~DoIns();

	virtual void translate() ;

	void out(const Name offset) const;

	virtual void outSobj(ofstream& sobj) const;
		
	int examine();
			
  	Name outString();

protected :

	int examineAction
	   ( SMIObject* pParentObject, State* pParentState, Action* pParentAction,
	    SMIObject* pTargetObject);

	int examineParameters
	   ( SMIObject* pParentObject, State* pParentState, Action* pParentAction,
	    SMIObject* pTargetObject);
	   
	int checkCompatibility
	    (const Name& targetObjectStateName, Action* pTargetAction);

//-------------   Data  ----------	
	Name _actionNm;

	Parms _parameters;

/**
  _objectId  is an object to which the action applied. When "", then the
             action is applied to all objects of set _setNm
			 One of the *Nm has to be ""
*/
	VarElement _objectId;
	
	VarElement _setId;  // normally ""; if not then action is applied to all in _setNm

};

#endif 
