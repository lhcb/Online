// forins.cxx: implementation of the ForIns class.
//
//                                                B. Franek
//                                                 Mar 2018
//////////////////////////////////////////////////////////////////////
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "forins.hxx"
#include "smlunit.hxx"
#include "smlline.hxx"
#include "name.hxx"
#include "utilities.hxx"
#include "registrar.hxx"
/*#include "smiobject.hxx"
#include "state.hxx"
#include "action.hxx"
#include "reportins.hxx"

#include "reservednames.hxx"*/
#include "errorwarning.hxx"

	
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

ForIns::ForIns(Action* pParentAction, InsList* pParentList) :
	Instruction(),
	_argName(""),
	_setName(""),
	_pInsBlock(0)
{
	_name = "forins";
	_pParentAction  = pParentAction;
	_pParentList = pParentList;
	return;
}

ForIns::~ForIns()
{
}

void ForIns::translate() {

//cout << endl << " ForIns::translate() " << endl;
//printCode();

	Name token; int ist,jst,idel,jdel; int inext,jnext; 
	
	SMLline lineBeingTranslated;
	
	lineBeingTranslated = (*_pSMLcode)[0];
	
	char del = getNextToken(_pSMLcode,0,0," ",token,idel,jdel,inext,jnext);
	token.upCase(); token.trim();
	
	if ( token == "FOR" ) {}
	else
	{
		ErrorWarning::printHead("ERROR",lineBeingTranslated);
		cout << " Keyword For not found" << endl;
		throw FATAL;
	}

	ist = inext; jst = jnext;

	if (ist < 0 || del != ' ')
	{
		ErrorWarning::printHead("ERROR",lineBeingTranslated);
		cout << "Nothing sensible follows the keyword For"
		     << endl;
		throw FATAL;
	}
	
//lineBeingTranslated.indicateCharacter(" jst ",jst);

	del = getNextToken(_pSMLcode,ist,jst," ",token,idel,jdel,inext,jnext);
	token.upCase(); token.trim();
	
//	cout << " token:" << token << endl;
	
	lineBeingTranslated = (*_pSMLcode)[ist];
	
	if ( !check_name(token) )
	{
		ErrorWarning::printHead("ERROR",lineBeingTranslated);
		cout << token << "   is not a name "
		     << endl;
		throw FATAL;
	}
	
	_argName = token;
	ist = inext; jst = jnext;

	del = getNextToken(_pSMLcode,ist,jst," ",token,idel,jdel,inext,jnext);
	token.upCase(); token.trim();
	
//	cout << " token:" << token << endl;
	
	lineBeingTranslated = (*_pSMLcode)[ist];

	if ( !(token == "IN") ) 
	{
		ErrorWarning::printHead("ERROR",lineBeingTranslated);
		cout << "   looking for \'IN\'  and found : " << token
		     << endl;
		throw FATAL;
	}
	
	ist = inext; jst = jnext;

	del = getNextToken(_pSMLcode,ist,jst," ",token,idel,jdel,inext,jnext);
	token.upCase(); token.trim();
	
//	cout << " token:" << token << endl;
	
	lineBeingTranslated = (*_pSMLcode)[ist];
	
	if ( !check_name(token) )
	{
		ErrorWarning::printHead("ERROR",lineBeingTranslated);
		cout << token << "   is not a name "
		     << endl;
		throw FATAL;
	}
	
	_setName = token;
		
	if ( inext <= 0 ) 
	{
		ErrorWarning::printHead("ERROR",lineBeingTranslated);
		cout  << "  There is some crap at the end of FOR line "
		     << endl;
		throw FATAL;
	}
	
	int insBlockBeg =inext; int insBlockEnd = inext;
	
	// now find the end of the block. The easiest is to search from the end
	// for 'END_FOR'
	
	int noLines = _pSMLcode->length();
	
	SMLline line;
	Name instrType;
	
	for ( int i=noLines-1; i>=insBlockBeg; i-- )
	{
		line = (*_pSMLcode)[i];
		if (line.instructionLine(instrType))
		{
			if ( instrType == "end_for" )
			{
				insBlockEnd = i-1; break;
			}
			else
			{
				ErrorWarning::printHead("ERROR",lineBeingTranslated);
				cout  << "  There is some crap following FOR instruction "
		        	<< endl;
		       		throw FATAL;
			}

		}
		if (!line.commentLine())
		{
			ErrorWarning::printHead("ERROR",lineBeingTranslated);
			cout  << "  There is some crap following FOR instruction "
	        	<< endl;
	      		throw FATAL;
		}
		
	}
	
	if ( insBlockEnd ==insBlockBeg )
	{
		ErrorWarning::printHead("ERROR",lineBeingTranslated);
		cout  << "  There is some crap following FOR instruction "
	        << endl;
	      	throw FATAL;
	}
		
// Now instantiate InsList and collect the instructions

	SMLUnit* pUnit;
	_pInsBlock = new InsList(_pParentAction, _pParentList);
	pUnit = _pInsBlock;
	Name unitType = "InsList";
	
	_internalSMLUnits.addRegardless(unitType,pUnit);
	pUnit->setParentUnit(this);
	
	for (int i = insBlockBeg; i<=insBlockEnd; i++)
	{
		line = (*_pSMLcode)[i];
		pUnit->acceptLine(line);
	}
	
	_pInsBlock->translate();
	
	NameVector args;
	args+= _argName;
	
	_pInsBlock->replaceArgs(args);
	
//	out(" ");
 	
	return;
}


//--------------------------------------------------------------------------
void ForIns::out(const Name offset) const
{
	cout << offset.getString() << "  FOR " << _argName << "  IN  " << _setName << endl;
	cout << offset.getString() << " Instructions:" << endl;
	_pInsBlock->out(offset);
	return;
}
//------------------------------------------  BF July  2017 -----------
void ForIns::outSobj(ofstream& sobj) const
{
	sobj << "for" << endl;
	sobj << _argName.getString() << endl;
	sobj << _setName.getString() << endl;
	
	int id = _pInsBlock->id();
	int level = _pInsBlock->level();
	char tempch[80];
	sprintf(tempch,"%3d%3d",id,level);
	sobj << tempch << endl; 
	
	return;
}
//-------------------------------------------------------------------------
int ForIns::examine()
{
	int retcode = 0;
	
	SMLline firstLine = (*_pSMLcode)[0];
	
	
//beg debug

/* cout << endl 
  << " ====================== ForIns::examine() ============= " << endl;
	
  cout << "  Parent : " << _pParentUnit->unitId() 
  << "  " << _pParentUnit->unitName() << endl;
		
  cout << "     Complete Ancestry " << endl;
	
	int num;
	NameVector ids,names;
	
	_pParentUnit->ancestry(ids,names);
	
	num = ids.length();

	for (int i=0; i<num; i++)
	{
		cout << "     " << ids[i] << "  " << names[i] << endl;
	}
	
	cout << "Parent Unit Code " << endl;	

	char temp[] = " "; _pSMLcode->out(temp);	
*/
//end debug

  cout <<  " ForIns::examine()     not implemented yet " << endl;

	return retcode;
}
