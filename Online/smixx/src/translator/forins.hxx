// forins.hxx: interface for the ForIns class.
//
//                                                  B. Franek
//                                                   March 2018
//
//////////////////////////////////////////////////////////////////////
#ifndef FORINS_HH
#define FORINS_HH

#include "instruction.hxx"
#include "inslist.hxx"
#include "action.hxx"

class NameVector;

class ForIns  : public Instruction 
{
public:
	ForIns(Action* pParentAct, InsList* pParentList);

	virtual ~ForIns();

	virtual void translate() ;

	void out(const Name offset) const;

	virtual void outSobj(ofstream& sobj) const;
	
	int examine();

protected :
/**
      for _argName  in  _setName
         pointer to the block of instructions   (_pInsBlock)
*/

	Name _argName;     
	
	Name _setName;
	
	InsList* _pInsBlock;

	Action* _pParentAction;
	
	InsList* _pParentList;
};
 
#endif 
