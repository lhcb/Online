// This is now an obsolete class and can be deleted   September 2020
/*
// function.cxx: implementation of the Function class.
//
//                                                B. Franek
//                                                 August 2020
// Copyright Information:
//      Copyright (C) 1999-2020 CCLRC. All Rights Reserved.
//////////////////////////////////////////////////////////////////////
#include "smixx_common.hxx"
#include <assert.h>
#include "function.hxx"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Function::Function(const Name& name)
 : Action(name) 
{
//cout << "   Function constructor " << _name << endl;
}


Function::~Function()
{
}

void Function::outSobj(ofstream& sobj) const
{
	sobj << "*FUNCTION" << endl;
	Action::outSobj(sobj);
	sobj << "*END_FUNCTION" << endl;
	return;
}
*/
