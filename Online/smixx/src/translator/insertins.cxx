// insertins.cxx: implementation of the InsertIns class.
//
//                                                B. Franek
//                                               2 July 2001
//////////////////////////////////////////////////////////////////////
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "smlunit.hxx"
#include "smlline.hxx"
#include "name.hxx"
#include "utilities.hxx"
#include "insertins.hxx"
#include "registrar.hxx"
#include "errorwarning.hxx"
#include "varelement.hxx"
#include "action.hxx"

	extern Registrar allObjects;
	extern Registrar allObjectSets;
	extern Registrar allUnits;
	
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

InsertIns::InsertIns()  
{
   _name = "insert";
   return;
}

InsertIns::~InsertIns()
{
    delete _pSMLcode;
}
//--------------------------------------------------------------------
void InsertIns::translate() {

	Name token; int idel,jdel; int inext,jnext; Name objectSet,objectSet1;
	Name inFrom;
	Name tempObjId, tempSetId;
	
	SMLline lineBeingTranslated;
	
	lineBeingTranslated = (*_pSMLcode)[0];
	
	getNextToken(_pSMLcode,0,0," ",token,idel,jdel,inext,jnext);
	token.upCase(); token.trim();

	if ( token == "INSERT" ) {
		_insertRemove = 1;
		
		lineBeingTranslated = (*_pSMLcode)[inext];
		getNextToken(_pSMLcode,inext,jnext," ",tempObjId,idel,jdel,inext,jnext);
		_objectId= tempObjId;
	}
	else if ( token == "REMOVEALL" || token == "REMOVE_ALL") {
		_insertRemove = 0;
		_objectId = "&ALL";
	}
	else  {
		_insertRemove = 0;
		lineBeingTranslated = (*_pSMLcode)[inext];
		getNextToken(_pSMLcode,inext,jnext," ",tempObjId,idel,jdel,inext,jnext);
		_objectId = tempObjId;
	}

	lineBeingTranslated = (*_pSMLcode)[inext];
	getNextToken(_pSMLcode,inext,jnext," ",inFrom,idel,jdel,inext,jnext);
	inFrom.upCase();
	if (    (token == "INSERT" && inFrom == "IN") 		||
		(token == "REMOVE" && inFrom == "FROM") 	||
		( (token == "REMOVEALL" || token == "REMOVE_ALL") && inFrom == "FROM")
	) {}
	else {
		ErrorWarning::printHead("ERROR",lineBeingTranslated
		,"unrecognised instruction");
		throw FATAL;
	}
	lineBeingTranslated = (*_pSMLcode)[inext];
	getNextToken(_pSMLcode,inext,jnext," ",tempSetId,idel,jdel,inext,jnext);
	_setId = tempSetId;
	
        if (inext>0) {
		ErrorWarning::printHead("ERROR",lineBeingTranslated
		,"some crap follows INSERT/REMOVE instruction");
		throw FATAL;
	}
	return;
	
}

//--------------------------------------------------------------------------
void InsertIns::out(const Name offset) const
{
	SMLUnit::out(offset); 
	char* ptn=offset.getString(); cout << ptn ;
	if (_insertRemove)
	{cout << " insert Object " << _objectId.outString() << " in Set ";}
	else 
	{cout << " remove Object " << _objectId.outString() << " from Set ";}
	cout << _setId.outString() << endl;

	return;
}
//------------------------------------------  BF July  2001  -----------
void InsertIns::outSobj(ofstream& sobj) const
{

	sobj << "insert" << endl;

	sobj << "    " << _insertRemove << endl;

        sobj << _objectId.stringForSobj() << endl;

	sobj << _setId.stringForSobj() << endl;
	
	return;
}
//---------------------------------------------------------------------------
int InsertIns::examine()
{
	int retcode = 0;
	SMLline firstLine = (*_pSMLcode)[0];
/*
  cout << endl 
  << " ====================== TermIns::examine() ============= " << endl;
	
  cout << "  Parent : " << _pParentUnit->unitId() 
  << "  " << _pParentUnit->unitName() << endl;
		
  cout << "     Complete Ancestry " << endl;
	
	int num;
	NameVector ids,names;
	
	_pParentUnit->ancestry(ids,names);
	
	num = ids.length();

	for (int i=0; i<num; i++)
	{
		cout << "     " << ids[i] << "  " << names[i] << endl;
	}
*/
	Action* pParentAction = (Action*)(_pParentUnit->parentPointer("Action"));
	
	Parms* pActPars = pParentAction->pActionParameters();
	
	if (!_objectId.actualNameAccessible(pActPars)) retcode = 1;

	void* pvoid; Name objname("");
	
	objname = _objectId.name();
	
	if (_objectId.parName() == "" && !( objname == "&ALL" ) )
	{
// if it is a simple name, we can check the object's existence
		Name objname = _objectId.name();
		pvoid = allObjects.gimePointer(objname);
		if ( pvoid == 0 )
		{
		// we can not consider this error because the object may be later
		// created and user is anticipating its future existence
			ErrorWarning::printHead("WARNING",firstLine);
			cout << " Object " << objname  <<
				" is not declared " << endl;
			cout << endl; _pParentUnit->printParents(); cout << endl;
		}
	}

// check for the existence of the object set

	if (!_setId.actualNameAccessible(pActPars)) retcode = 1;
	
	Name setname("");
	
	setname = _setId.name();
	
	if (_setId.parName() == "" ) // it simple name so we can do checks
	{		
		pvoid = allObjectSets.gimePointer(setname);
		if ( pvoid == 0 )
		{
			retcode = 1; 
			ErrorWarning::printHead("ERROR",firstLine);
			cout << " Object Set " << setname  <<
				" is not declared " << endl;
			cout << endl; _pParentUnit->printParents(); cout << endl;
		}
		else
		{
			Name type = allUnits.gimeName(pvoid);
//	cout << type << "  " << setname << endl;
			if ( type == "ObjectSetUnion" )
			{
				retcode =1;
				ErrorWarning::printHead("ERROR",firstLine);
				cout << "direct insertion/removal into/from UNION is not allowed" << endl;
			}			
		}		
	}


	return  retcode;
}
