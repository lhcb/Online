// inslist.cxx: implementation of the Action class.
//
//                                                B. Franek
//                                             28 September 1999
//////////////////////////////////////////////////////////////////////
#include <stdlib.h>
#include <assert.h>
#include <stdio.h>
#include "smlline.hxx"
#include "inslist.hxx"
#include "doins.hxx"
#include "callins.hxx"
#include "insertins.hxx"
#include "sleepins.hxx"
#include "termins.hxx"
#include "setins.hxx"
#include "ifins.hxx"
#include "waitins.hxx"
#include "createobjectins.hxx"
#include "destroyobjectins.hxx"
#include "waitforins.hxx"
#include "reportins.hxx"
#include "forins.hxx"
#include "errorwarning.hxx"
#include "nmdptnr.hxx"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

InsList::InsList( Action* pParent, InsList* pParentList) : SMLUnit("InsList",10) 
{
	_pParentAction = pParent;
	_pParentList = pParentList;
	if (pParentList == 0 ) { _level = 0; }
	else { _level = _pParentList->_level + 1; }
	pParent->registerBlock(this);
	return;
}

InsList::~InsList()
{
    delete _pSMLcode;
}
//-----------------------------------------------------------------------------
void InsList::translate()
{

//cout << " InsList:: translate started" << endl;
//	cout << endl << " Block Code " << endl;
//	printCode();

	if ( _pSMLcode->length() == 0 ) return;

	Name instrType;
	SMLUnit *pInstr; 
	
	SMLline line; 
	
//  The first line of the block. This is always an instruction line.(Has to be)
	line = (*_pSMLcode)[0];
	int instr = line.instructionLine(instrType);
	if (!instr)
	{
		ErrorWarning::printHead("ERROR",line);
		cout << 
		" The first line of instruction block has to be an instruction  " 
			 << endl;
		throw FATAL;
	}

	int nextBeg; Name nextType; // line numm and type of the next instruction

	int insBeg = 0;
	
	while ( insBeg >= 0 )
	{   //***** Beg of collection loop
//	cout << " instr " << instr << "  instrType " << instrType << endl;

		if      ( instrType == "do" ) {pInstr = new DoIns();}
		else if ( instrType == "call" ) {pInstr = new CallIns();}
		else if ( instrType == "create_object" ){pInstr = new CreateObjectIns();}
		else if ( instrType == "destroy_object" ){pInstr = new DestroyObjectIns();}

		else if ( instrType == "wait_for" ){pInstr = new WaitForIns();}
		else if ( instrType == "insert" ){pInstr = new InsertIns();}
		else if ( instrType == "remove" ){pInstr = new InsertIns();}
		else if ( instrType == "sleep" ){pInstr = new SleepIns();}
		else if ( instrType == "set" ){pInstr = new SetIns();}
		else if ( instrType == "terminate_action" ){pInstr = new TermIns();}
		else if ( instrType == "if" ){pInstr = new IfIns(_pParentAction, this);}
		else if ( instrType == "wait" ){pInstr = new WaitIns();}
		else if ( instrType == "report" ){pInstr = new ReportIns();}
		else if ( instrType == "for" ){pInstr = new ForIns(_pParentAction, this);}
		else 
		{
			ErrorWarning::printHead("ERROR",line);
			cout << " unknown instruction :  " 
			     << instrType << endl;
			throw FATAL;
		}
		pInstr->setParentUnit(this);
		_internalSMLUnits.addRegardless(instrType,pInstr);
		
		pInstr->acceptLine(line);   // collect the first line of the instruction
		
		// collect the remaining lines of the instruction until the next
		// instruction is found or end of the block reached
		if ( instrType == "if" || instrType == "wait_for" 
		    || instrType == "for" )
		     { collectTermIns(pInstr, instrType, insBeg, nextBeg, nextType); }  
		else { collectIns(pInstr, insBeg, nextBeg, nextType); }
		
		insBeg = nextBeg;
		if ( insBeg >0 )
		{
			instrType = nextType;
			line = (*_pSMLcode)[insBeg];  // the first line of the next 
			                              // instruction
		}
	}
	
	
//cout << endl<< " Instructions code " << endl;
//printCodeAllIntUnits();
		         
	translateUnits();

	removeSterileInstructions();
}
//-----------------------------------------------------------------------------
void InsList::collectIns(SMLUnit* pInstr, int begLine,
			int& nextBeg, Name& nextType)
{
//cout << "  InsList:: collectIns started " << endl;
//  Input:
//       pInstr - pointer to the instraction being collected
//       begLine - line number anywhere within the instruction code
//  Output:
//       nextBeg  - line number of the next instruction
//       nextType - type of the next instruction
	
	SMLline line; 
	int numOfLines = _pSMLcode->length();
//cout << endl << (*_pSMLcode)[0] << endl;
	
// start collecting from the line following 'begLine' until either the next
// instruction line is found or end of the InsList reached.

	for ( int j = begLine+1; j<numOfLines; j++)
	{
		line = (*_pSMLcode)[j];
		int instr = line.instructionLine(nextType);
		if (instr) { nextBeg = j; return;}
		pInstr->acceptLine(line);	
	}
	
	nextBeg = -1;   // reached the end of block code
	return;
}
//-----------------------------------------------------------------------
void InsList::collectTermIns(SMLUnit* pInstr, Name instrType, int begLine,
			int& nextBeg, Name& nextType)
{
//  Input:
//       pInstr - pointer to the instraction being collected
//       begLine - line number of the first line of the instruction code
//       instrType - the instruction type
//  Output:
//       nextBeg  - line number of the next instruction
//       nextType - type of the next instruction

		
	SMLline line = (*_pSMLcode)[begLine]; 
	int numOfLines = _pSMLcode->length();
//cout << endl << (*_pSMLcode)[0] << endl;

	Name terminator;  // terminating line of the instruction
	if      ( instrType == "if" ) { terminator = "endif"; }
	else if ( instrType == "wait_for" ) { terminator = "end_wait_for"; }
	else if ( instrType == "for" ) { terminator = "end_for"; }
	else
	{
		ErrorWarning::printHead("ERROR",line);
			cout << " not terminated instruction :  " 
			     << instrType << endl;
			throw FATAL;
	}

	
// start collecting from the line following 'begLine' until 
// the terminator is found.
// We have to be carefull about other instructions of the same type inside the
// block. This is what the 'level' below is for.

	int level = 0;
	
	for ( int j = begLine +1; j<numOfLines; j++)
	{
		line = (*_pSMLcode)[j];
		int instr = line.instructionLine(nextType);
		pInstr->acceptLine(line);
		if (instr) {
			if (nextType == instrType) {level++;}
			if (nextType == terminator )
			{
				if ( level == 0 )
				{// the terminator reached and collected
				 // now collect the rest of the code
				 	collectIns(pInstr, j, nextBeg, nextType);
					return;
				}
				else { level--; }
			}
		}
			
	}
	
	ErrorWarning::printHead("ERROR",line);
		cout << " instruction terminator not found :  " 
		     << instrType << endl;
		throw FATAL;
	
	return;
}	
//----------------------------------------  BF April 2000  --------------
void InsList::outSobj( ofstream& sobj) const
{
	sobj << "*BLOCK" << endl;

	char tempch[80];

	sprintf(tempch,"%3d%3d",_id,_level);
	sobj << tempch << endl;


	int numIns = _internalSMLUnits.length();

	for (int i=0; i < numIns; i++) {
		void* ptnvoid = _internalSMLUnits.gimePointer(i) ;
		Instruction* pInstruction;
		pInstruction = (Instruction*)ptnvoid;
		Registrar insLists;
		pInstruction->outSobj(sobj);
	}
 
}
//----------------------------------------------------------------------------
int InsList::level() const
{
	return _level;
}
//----------------------------------------------------------------------------
int InsList::id() const
{
	return _id;
}
//----------------------------------------------------------------------------
void InsList::setId(const int id)
{
	_id = id;
	return;
}
//-------------------------------------------------------------------
void InsList::replaceArgs(const NameVector& args)
{
	int numIns = _internalSMLUnits.length();

	for (int i=0; i < numIns; i++) {
		void* ptnvoid = _internalSMLUnits.gimePointer(i) ;
		Instruction* pInstruction;
		pInstruction = (Instruction*)ptnvoid;
		
		pInstruction->replaceArgs(args);
	}

	return;
}
//-----------------------------------------------------------------
int InsList::numOfInstructions() const
{
	return _internalSMLUnits.length();
}
//-----------------------------  BF  November 2019  ----------------------------
void InsList::removeSterileInstructions()
{
	int noIns = _internalSMLUnits.length();

	bool present = false;
	for (int i=0; i < noIns; i++)
	{
		void* ptnvoid = _internalSMLUnits.gimePointer(i) ;
		IfIns* pIfIns = (IfIns*)ptnvoid;
		if ( pIfIns->sterileInstruction() ) { present = true; break; }	
	}

	if ( !present ) { return; }//no sterile instructions found 

// There are units to be removed

	Registrar tmpInternalUnits = _internalSMLUnits;
	_internalSMLUnits.clear();
	
	NmdPtnr temp;

	for ( int i=0; i < noIns; i++) {
		void* ptnvoid = tmpInternalUnits.gimePointer(i) ;
		Instruction* pInstruction = (Instruction*)ptnvoid;
				
		if ( pInstruction->sterileInstruction() )
		{
			SMLline firstInsLine = pInstruction->firstLineOfUnit();
			ErrorWarning::printHead("WARNING",firstInsLine);
			cout << " Removing sterile instruction " << endl;
			cout << endl; 
			_pParentUnit->printParents();
			cout << endl;
		}
		else
		{
			temp = tmpInternalUnits[i];
			_internalSMLUnits.addRegardless(temp);
		}
	}	
	
	return;
}
