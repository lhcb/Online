// reportins.hxx: interface for the ReportIns class.
//
//                                                  B. Franek
//                                                   July 2017
//
//////////////////////////////////////////////////////////////////////
#ifndef REPORTINS_HH
#define REPORTINS_HH

#include "instruction.hxx"
#include "indivaluevector.hxx"

class ReportIns  : public Instruction 
{
public:
	ReportIns();

	virtual ~ReportIns();

	virtual void translate() ;

	void out(const Name offset) const;

	virtual void outSobj(ofstream& sobj) const;
	
	int examine();

private :
	void extractReportElements(SMLlineVector* pSMLcode,
				const int ist, const int jst,  
				int& inext, int& jnext);
				
	void getNextElement(SMLlineVector* pSMLcode,
			const int ist, const int jst,
			int& idel, int& jdel, char& del,
			int& inext, int& jnext);
protected :

	Name _severity;  // either INFO, WARNING, ERROR or FATAL
	
	IndiValueVector _msgElements;
  
};
 
#endif 
