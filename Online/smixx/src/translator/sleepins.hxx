// sleeptins.hxx: interface for the SleepIns class.
//
//                                                  B. Franek
//                                                 June 2011
//
//////////////////////////////////////////////////////////////////////
#ifndef SLEEPINS_HH
#define SLEEPINS_HH

#include "instruction.hxx"
#include "param.hxx"
#include "indivalue.hxx"

class SleepIns  : public Instruction 
{
public:
	SleepIns();

	virtual ~SleepIns();

	virtual void translate() ;

	void out(const Name offset) const;

	virtual void outSobj(ofstream& sobj) const;
	
	int examine();


protected :

	IndiValue _seconds;  //  number of seconds to sleep as indiValue
};

#endif 
