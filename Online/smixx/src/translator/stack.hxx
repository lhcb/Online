// stack.hxx: interface for the Stack class.
//
//                                                  B. Franek
//                                                23 February 2000
//
//////////////////////////////////////////////////////////////////////
#ifndef STACK_HH
#define STACK_HH

class StackItem;
class BoolItem;

class Stack 
{
public:
/**
   class constructor. When member runs out of space to store items,
    its size is increased by 'incr' 
*/
	Stack(int incr = 20);

	virtual ~Stack();

	void push(const char item);

	void push(BoolItem*);

	void push(const StackItem&);

/**
  retrieves 'nth' item from the top of the stack. nth one means the top.
*/
	int getFromStack(const int nth, StackItem&) const;

/**
  deletes the item following the top from the stack
*/
	void deleteItem2FromStack();

/**
   deletes 'num' items from the top of the stack
*/
	void deleteTopStack(const int num);

	void out(const Name offset) const;

protected :

	enum { EMPTY = -1};

	int _size; // available size of the stack
	int _incr; // increment by which _size is increased when needed
	
	StackItem* _s;
	int _top;  // index of the top of the stack (starts at 0).
	           // must be smaller then _size

};

#endif 
