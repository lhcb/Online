// SWMove_To.cxx: implementation of the SWMove_To class.
//
//                                                B. Franek
//                                               July 2015
// Copyright Information:
//      Copyright (C) 1999/2015 CCLRC. All Rights Reserved.
//////////////////////////////////////////////////////////////////////

#include <cstdlib>
#include <cassert>
#include "utilities.hxx"
#include "errorwarning.hxx"
#include "swmove_to.hxx"

SWMove_To::SWMove_To(int ident) :
	SMLUnit("SWMove_To",1),
	WhenResponse(),
	_whenId(ident),
	_stateNm(""),
	_actionNm(""),
	_pEndStateActionCode(NULL) 
{
	return;
}
//----------------------------------------------------------------------
void SWMove_To::translate()
{

	Name token; int idel,jdel; int inext,jnext;
	
	SMLline lineBeingTranslated;
	
	lineBeingTranslated = (*_pSMLcode)[0];
	int ist,jst;
	
	ist = 0; jst = _firstColOfUnit;
//cout << " Starting SWmove_to_translate() " << ist << " " << jst << endl;
//char temp[] = " "; _pSMLcode->out(temp);

	getNextToken(_pSMLcode,ist,jst," ",token,idel,jdel,inext,jnext);
	token.upCase(); token.trim();

	if ( token == "MOVE_TO" || token == "ENDINSTATE" ){}
	else {
		ErrorWarning::printHead("ERROR",lineBeingTranslated
		,"expected MOVE_TO or ENDINSTATE");
		cout << " Found instead " << token << endl;
		throw FATAL;
	}
		
	ist = inext; jst = jnext;
	getNextToken(_pSMLcode,ist,jst," ",_stateNm,idel,jdel,inext,jnext);
	_stateNm.upCase(); _stateNm.trim();
	
	if ( !check_name(_stateNm) ) {
		ErrorWarning::printHead("ERROR",lineBeingTranslated);
		cout << " State: " << _stateNm << " is not a name" << endl;
		throw FATAL;
	}
	
// Manufacture action name and generate SML code for it.
//  This will be later on picked up by State::translate() and
//  incorporated into the relevant state
	Name charIdent;
	charIdent = _whenId;
	_actionNm = "&END_IN_STATE_WHEN"; _actionNm += charIdent;
	_pEndStateActionCode = createTermAction(_actionNm,_stateNm);	

        if (inext>0) {
		ErrorWarning::printHead("ERROR",lineBeingTranslated
		,"Some crap follows MOVE_TO state");
		throw FATAL;
	}	

	return;
}
//---------------------------------------------------------------------
SMLlineVector* SWMove_To::createTermAction(const Name& actionName,const Name& stateName)
{
	SMLlineVector* pCode = new SMLlineVector();
	assert (pCode != 0);
	
	Name line0 = "action : "; line0 += actionName;
	Name line1 = "   terminate_action / state = "; line1 += stateName;
	
	SMLline smlline0(-1,line0), smlline1(-1,line1);
	
	(*pCode) += smlline0; (*pCode) += smlline1; 
	
	return pCode;
} 
//----------------------------------------------------------------------
int SWMove_To::examine()
{
//cout << " There is nothing to check for SWMove_To " << endl;
	return 0;
}
//----------------------------------------------------------------------
void SWMove_To::outSobj(ofstream& sobj) const
{
	sobj << "do" << endl;
	sobj << _actionNm.getString() << endl;

	int numpar = 0;

	sobj << "    " << numpar << endl;

	sobj << "&THIS_OBJECT" << endl;
}
//------------------------------------------------------------------------
void SWMove_To::out(const Name offset) const 
{
	cout << "MOVE_TO " << _stateNm << endl;
	cout << " Generated new action:" << endl;
	_pEndStateActionCode->out(offset.getString()); cout << endl;
	return;
}

