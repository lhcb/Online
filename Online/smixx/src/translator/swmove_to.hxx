// swmove_to.hxx: interface for the SWMove_To class.
//
//                                                  B. Franek
//                                                 July 2015
//
//////////////////////////////////////////////////////////////////////
#ifndef SWMOVE_TO_HH
#define SWMOVE_TO_HH

#include "whenresponse.hxx"
#include "smlunit.hxx"

class SWMove_To : public SMLUnit, public WhenResponse
{
public:
	SWMove_To(int);

	~SWMove_To() { delete _pEndStateActionCode;}

	void translate();
	
	int examine();

	void outSobj(ofstream& sobj) const;

	void out(const Name offset) const; 

	SMLlineVector* getEndStateActionCodePointer() const
	 {return _pEndStateActionCode;}


protected :
	SMLlineVector* createTermAction(const Name& actionNm, const Name& stateNm);
	int _whenId;

	Name _stateNm;
	
	Name _actionNm;
	SMLlineVector* _pEndStateActionCode;

};

#endif 
