// waitforins.hxx: interface for the WaitForIns class.
//
//                                                  B. Franek
//                                                11 December 2014
//
// Copyright Information:
//      Copyright (C) 1999-2014 CCLRC. All Rights Reserved.
//////////////////////////////////////////////////////////////////////
#ifndef WAITFORINS_HH
#define WAITFORINS_HH

#include "namevector.hxx"
#include "instruction.hxx"
#include "registrar.hxx"
//#include "smlunit.hxx"

class WaitForIns  : public Instruction 
{
public:
	WaitForIns();

	virtual ~WaitForIns();

	virtual void translate() ;

	void out(const Name offset) const;

	virtual void outSobj(ofstream& sobj) const;
		
	int examine();
		
	Name outString();

protected :	

	NameVector	_refObjects;
	NameVector 	_refObjectSets;
	
	Registrar _whens;

};

#endif 
