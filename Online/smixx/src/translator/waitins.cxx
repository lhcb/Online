// waitins.cxx: implementation of the WaitIns class.
//
//                                                B. Franek
//                                           16 December 2008
// Copyright Information:
//      Copyright (C) 1999/2008 CCLRC. All Rights Reserved.
//////////////////////////////////////////////////////////////////////
#include <cstdlib>
#include <cstring>
#include "utilities.hxx"
#include "waitins.hxx"
#include "smiobject.hxx"
#include "smiobjectset.hxx"
#include "registrar.hxx"
#include "errorwarning.hxx"

extern Registrar allObjects, allObjectSets;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

WaitIns::WaitIns()  
{
   _name = "wait";
//   cout << "wait constructor called" << endl;
   return;
}

WaitIns::~WaitIns()
{
    delete _pSMLcode;
}
//--------------------------------  translate  -------------------------
void WaitIns::translate()
{
//	cout << "  translating wait " << endl;
//	_pSMLcode->out(" ");

// we shall remove the wait's closing bracket. It interferes with translation
// when names inside are of the form $(...)
	removeTheLastBracket();
		
	SMLline lineBeingTranslated;
	
	Name token; int idel,jdel; int inext,jnext;
	int ist,jst;
	
	lineBeingTranslated = (*_pSMLcode)[0];
	char del = getNextToken(_pSMLcode,0,0," (",token,idel,jdel,inext,jnext);
	token.upCase(); token.trim();

	if ( token == "WAIT"){}
	else
	{
		ErrorWarning::printHead("ERROR",lineBeingTranslated);
		cout << " Keyword WAIT not found" << endl;
		throw FATAL;
	}

	ist = inext; jst = jnext;

	if (ist < 0 || del != '(')
	{
		ErrorWarning::printHead("ERROR",lineBeingTranslated);
		cout << "Nothing sensible follows the keyword WAIT"
		     << endl;
		throw FATAL;
	}
	
	lineBeingTranslated = (*_pSMLcode)[ist];
	del = getNextToken(_pSMLcode,ist,jst,"",token,idel,jdel,ist,jst);

	
//	cout << token << endl;	
	
	int iel = 0;
	Name element, temp;
	int objSet;
	Name name;
	
	while ( token.element(iel,',',element) )
	{
//		cout << element << endl;
		element.upCase(); element.trim(); element.trimFront();

		if ( strncmp("ALL_IN ",element.getString(),7) == 0 )
		{   //  object set name should follow
			objSet = 1;
			name = &element[7]; name.trimFront();
		}
		else
		{  
			objSet = 0;
			name = element;
		}

		VarElement elemId = name;
		
		if ( objSet == 0 )
		{
//			cout << " object " << elemId.outString() << endl;
			if ( _refObjects.isPresent(elemId) )
			{
				ErrorWarning::printHead("ERROR",lineBeingTranslated);
				cout << " the object " << elemId.outString()
				     << " occurs in the list more than once"
		     			<< endl;
				throw FATAL;
			}
			_refObjects += elemId;
		}
		else
		{
//			cout << " objectSet " << elemId.outString() << endl;
			if ( _refObjectSets.isPresent(elemId) )
			{
				ErrorWarning::printHead("ERROR",lineBeingTranslated);
				cout << " the object Set " << elemId.outString()
				     << " occurs in the list more than once"
		     			<< endl;
				throw FATAL;
			}
			_refObjectSets += elemId;
		}
		iel++;
	}
//	_refObjects.out(" ");
//	_refObjectSets.out(" ");	
	return;
}
//-----------------------------------------------------------------------
void WaitIns::out(const Name offset) const
{
	SMLUnit::out(offset);
	char* ptn=offset.getString();
	cout << ptn ; cout << "Objects to wait for:" << endl;
	_refObjects.out(offset);
	
	cout << ptn ; cout << "Objects in object sets to wait for:" << endl;
	_refObjectSets.out(offset);
	
	return;
}

//----------------------------------  BF January 2009  ------------------
void WaitIns::outSobj(ofstream& sobj) const
{
	sobj << "wait" << endl;
	
	int numObjects = _refObjects.length();
	sobj << "    " << numObjects << endl;
		
	for (int io=0; io<numObjects; io++)
	{
		VarElement objname = _refObjects[io];
		sobj << objname.stringForSobj() << endl;
	}
	
	int numObjectSets = _refObjectSets.length();
	sobj << "    " << numObjectSets << endl;
		
	for (int ios=0; ios<numObjectSets; ios++)
	{
		VarElement objsetname = _refObjectSets[ios];
		sobj << objsetname.stringForSobj() << endl;
	}


	return;
}

//---------------------------------------------------------------------------
int WaitIns::examine()
{
	int retcode = 0;
	SMLline firstLine = (*_pSMLcode)[0];
/*
  cout << endl 
  << " ====================== WaitIns::examine() ============= " << endl;
	
  cout << "  Parent : " << _pParentUnit->unitId() 
  << "  " << _pParentUnit->unitName() << endl;
		
  cout << "     Complete Ancestry " << endl;
	
	int num;
	NameVector ids,names;
	
	_pParentUnit->ancestry(ids,names);
	
	num = ids.length();

	for (int i=0; i<num; i++)
	{
		cout << "     " << ids[i] << "  " << names[i] << endl;
	}
*/

//Name offset(" ");
//cout << endl; cout << " Objects: "; _refObjects.out(offset); cout << endl;
//cout << endl; cout << " Object Sets "; _refObjectSets.out(offset); cout << endl<< endl;

	int numObjects = _refObjects.length();
	
	for (int i=0; i<numObjects; i++)
	{
		VarElement objId = _refObjects[i];
		Name objname = objId.name();
		if ( objname == "" ) {}   // need expansion, can check parms
		else
		{
			void* pvoid = allObjects.gimePointer(objname);
			if ( pvoid == 0 )
			{
			//	retcode = 1; review
				ErrorWarning::printHead("SEVERE WARNING",firstLine);
				cout << " Object " << objname  <<
				" is not declared " << endl;
				cout << endl; _pParentUnit->printParents(); cout << endl;
			}
		}
	}
	
	int numObjectSets = _refObjectSets.length();
	
	for (int i=0; i<numObjectSets; i++)
	{
		VarElement objsetId = _refObjectSets[i];
		Name objsetname = objsetId.name();
		if ( objsetname == "" ) {} // *** needs expansion, can check parms
		else
		{
			void* pvoid = allObjectSets.gimePointer(objsetname);
			if ( pvoid == 0 )
			{
			//	retcode = 1; review
				ErrorWarning::printHead("SEVERE WARNING",firstLine);
				cout << " Object Set " << objsetname  <<
				" is not declared " << endl;
				cout << endl; _pParentUnit->printParents(); cout << endl;
			}
		}
	}

	int iflg = examineUnits();
	
	return retcode+iflg;
}

//-------------------------------------------------------------------------------------------------------
Name WaitIns::outString() 
{
	Name temp;
	
	temp = "WAIT ( ";
	
	int first = 0;
	int numObjects = _refObjects.length();
	
	for (int i=0; i<numObjects; i++)
	{
		VarElement objid = _refObjects[i];
		if ( first == 0 )
		{
			first = 1;
			temp += objid.outString();
		}
		else
		{
			temp += ", ";
			temp += objid.outString();
		}
	}
	
	int numObjectSets = _refObjectSets.length();
	
	for (int i=0; i<numObjectSets; i++)
	{
		VarElement objsetid = _refObjectSets[i];
		if ( first == 0 ) 
		{ 
			first = 1;
			temp += " all_in ";
			temp += objsetid.outString();
		}
		else
		{
			temp += ", all_in ";
			temp += objsetid.outString();
		} 
		
	}
	
	temp += " )";
	
	return temp;
}
//------------------------------------------------------------------
void WaitIns::removeTheLastBracket()
{

	int ilast, jlast;
	char lastchar = lastNonBlank(*_pSMLcode,ilast,jlast);

	if ( lastchar != ')' )
	{
		ErrorWarning::printHead("ERROR",(*_pSMLcode)[0]);
		cout << "The WAIT instruction does not terminate with )"
		     << endl;
		throw FATAL;
	}
		
	SMLline newline;
	newline = (*_pSMLcode)[ilast];
	Name empty("");
	newline.replace(jlast,jlast,empty);
	_pSMLcode->replace(ilast,newline);

	return;
}
/*int main()
{
	return 1;
}*/
