// waitins.hxx: interface for the WaitIns class.
//
//                                                  B. Franek
//                                                16 December 2008
//
// Copyright Information:
//      Copyright (C) 1999-2008 CCLRC. All Rights Reserved.
//////////////////////////////////////////////////////////////////////
#ifndef WAITINS_HH
#define WAITINS_HH

#include <stdlib.h>
#include "namevector.hxx"
#include "instruction.hxx"
#include "varelementvector.hxx"

class WaitIns  : public Instruction 
{
public:
	WaitIns();

	virtual ~WaitIns();

	virtual void translate() ;

	void out(const Name offset) const;

	virtual void outSobj(ofstream& sobj) const;
		
	int examine();
		
	Name outString();

private :
/**
 will check that the last character is ')' and will replace it by ' '
*/	
	void removeTheLastBracket();
	
protected :	

//	NameVector	_refObjects;
//	NameVector 	_refObjectSets;
	
	VarElementVector _refObjects;
	VarElementVector _refObjectSets;


};

#endif 
