// WFWMove_To.cxx: implementation of the WFWMove_To class.
//
//                                                B. Franek
//                                               July 2015
// Copyright Information:
//      Copyright (C) 1999/2015 CCLRC. All Rights Reserved.
//////////////////////////////////////////////////////////////////////

#include <cstdlib>
#include "utilities.hxx"
#include "errorwarning.hxx"
#include "smiobject.hxx"
#include "wfwmove_to.hxx"

WFWMove_To::WFWMove_To() :
	SMLUnit("WFWMove_To",1),
 	_stateNm("")
{
	return;
}
//----------------------------------------------------------------------
void WFWMove_To::translate()
{

	Name token; int idel,jdel; int inext,jnext;
	
	SMLline lineBeingTranslated;
	
	lineBeingTranslated = (*_pSMLcode)[0];
	int ist,jst;
	
	ist = 0; jst = _firstColOfUnit;
//cout << " Starting WFWmove_to_translate() " << ist << " " << jst << endl;
//char temp[] = " "; _pSMLcode->out(temp);

	getNextToken(_pSMLcode,ist,jst," ",token,idel,jdel,inext,jnext);
	token.upCase(); token.trim();

	if ( token == "ENDINSTATE" || token == "MOVE_TO" ){}
	else {
		ErrorWarning::printHead("ERROR",lineBeingTranslated
		,"expected STAY_IN_STATE");
		cout << " Found instead " << token << endl;
		throw FATAL;
	}
	
	Name stateNm;
	ist = inext; jst = jnext;
	getNextToken(_pSMLcode,ist,jst," ",stateNm,idel,jdel,inext,jnext);
	stateNm.upCase(); stateNm.trim();
	
	if ( !check_name(stateNm) ) {
		ErrorWarning::printHead("ERROR",lineBeingTranslated);
		cout << " State: " << stateNm << " is not a name" << endl;
		throw FATAL;
	}
	
	
	
//	_actionNm = "&MOVE_TO"; _actionNm += "_"; _actionNm += stateNm;
//	 _objectNm = "&THIS_OBJECT";

	_stateNm = stateNm;

        if (inext>0) {
		ErrorWarning::printHead("ERROR",lineBeingTranslated
		,"Some crap follows MOVE_TO state");
		throw FATAL;
	}
	return;


}
//----------------------------------------------------------------------
int WFWMove_To::examine()
{
	int retcode = 0;
	SMIObject* pObj;
	pObj =
	 (SMIObject*)(_pParentUnit->parentPointer("Object"));
	 
	SMLline firstLine = (*_pSMLcode)[0];
	// have to check the existence of the state _stateNm
		
	int undeclared;
	if ( pObj->hasState(_stateNm,undeclared) ) {}
	else
	{
//		retcode = 1;
		ErrorWarning::printHead("SEVERE WARNING",firstLine);
		cout << " Object " << pObj->unitName() <<
		" does not have state " << _stateNm << " declared" << endl;
		cout << endl; _pParentUnit->printParents(); cout << endl;
	}
				

	return retcode;
}
//----------------------------------------------------------------------
void WFWMove_To::outSobj(ofstream& sobj) const
{
// For reasons of backward compatibility, this response is
// simulated like if it was instruction
//    DO &MOVE_TO_'state-name' &THIS_OBJECT
//  should realy be changed sometime in the future.

	sobj << "do" << endl;
	
	Name action = "&MOVE_TO";
	action += "_";
	action += _stateNm;

	sobj << action.getString() << endl;

	int numpar = 0;

	sobj << "    " << numpar << endl;

	sobj << "&THIS_OBJECT" << endl;
}

