// wfwmove_to.hxx: interface for the WFWMove_To class.
//
//                                                  B. Franek
//                                                 July 2015
//
//////////////////////////////////////////////////////////////////////
#ifndef WFWMOVE_TO_HH
#define WFWMOVE_TO_HH

#include "whenresponse.hxx"
#include "smlunit.hxx"

class WFWMove_To : public SMLUnit, public WhenResponse
{
public:
	WFWMove_To();

	~WFWMove_To() {};

	virtual void translate();
	
	virtual int examine();

	virtual void outSobj(ofstream& sobj) const;

	virtual void out(const Name offset) const 
	             { cout << offset.getString()
		      << "MOVE_TO " << _stateNm << endl; return; };

protected :
	Name _stateNm;

};

#endif 
