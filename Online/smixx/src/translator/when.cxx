// when.cxx: implementation of the When class.
//
//                                                B. Franek
//                                               23 September 1999
// Copyright Information:
//      Copyright (C) 1996-2002 CCLRC. All Rights Reserved.
//////////////////////////////////////////////////////////////////////
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdio.h>
#include "action.hxx"
#include "when.hxx"
#include "smlline.hxx"
#include "utilities.hxx"
#include "smiobject.hxx"
#include "errorwarning.hxx"
#include "doins.hxx"
#include "wfwcontinue.hxx"
#include "swstay_in_state.hxx"
#include "wfwmove_to.hxx"
#include "swmove_to.hxx"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

When::When() :
	SMLUnit("when",3),
	_ident(-100),
	_condition(),
	_pResponse(NULL),
	_belongsToWaitFor(false)
{
	return;
}

//-----------------------------------------------------------------
When::When(int ident, bool belongsToWaitFor):
	SMLUnit("when",3),
	_ident(ident),
	 _condition(),
	 _pResponse(NULL),
	_belongsToWaitFor(belongsToWaitFor)
{
	return;
}

When::~When()
{
    delete _pSMLcode;
}
//----------------------------------------------------------------------
void When::translate() {

//cout << endl << " When::translate() " << endl;
//_pSMLcode->out(); cout << endl;

    Name token; int idel,jdel; int inext,jnext;
	int ist,jst;
	SMLline lineBeingTranslated;
	
	lineBeingTranslated = (*_pSMLcode)[0];
	getNextToken(_pSMLcode,0,0," (",token,idel,jdel,inext,jnext);
	token.upCase(); token.trim();
	
	if ( token == "WHEN"){}
	else {
		ErrorWarning::printHead("ERROR",lineBeingTranslated);
		cout << "Keyword WHEN not found" << endl;
		 throw FATAL;
	}
	
	_condition.setParentUnit(this);
    ist = idel; jst = jdel;

	int ibegcond = ist; int jbegcond = jst;
	
	_condition.initialise(_pSMLcode,ist, jst, inext, jnext);
	// the method will either succeed or terminate the program
	
	int iendcond = inext; int jendcond = jnext;
	
	int rowTerm, colTerm;
	_condition.getTermPosition(rowTerm,colTerm);
	
//cout << " Condition terminator: " << _condition.terminator() << endl;	
//_pSMLcode->indicateCharacter("\n character start of terminator",rowTerm,colTerm);

// The condition terminator is 'DO' or 'ENDINSTATE' or 'MOVE_TO' 
// or 'STAY_IN_STATE' or 'CONTINUE'

	Name condTerm = _condition.terminator();

// ----------------------------------------------------------------

	SMLUnit* pUn;
	
	if (_condition.terminator() == "DO" ) {
		Name objName("&THIS_OBJECT");
		DoIns* pDoIns = new DoIns(objName);
		assert (pDoIns != 0);
		pUn = (SMLUnit*)pDoIns;
		_pResponse = (WhenResponse*)pDoIns;
	}
        else if (_condition.terminator() == "ENDINSTATE" || _condition.terminator() == "MOVE_TO") {
	    if (_belongsToWaitFor == false)
	    {
		SWMove_To* pSWMove_To = new SWMove_To(_ident);
		assert (pSWMove_To != 0);
		pUn = (SMLUnit*)pSWMove_To;
		_pResponse = (WhenResponse*)pSWMove_To;	
	    }
	    else
	    {
		WFWMove_To* pWFWMove_To = new WFWMove_To();
		assert (pWFWMove_To != 0);
		pUn = (SMLUnit*)pWFWMove_To;
		_pResponse = (WhenResponse*)pWFWMove_To;
	    }
	}
        else if (_condition.terminator() == "STAY_IN_STATE")
	{
		SWStay_in_State* pSWStay_in_State = new SWStay_in_State();
		assert (pSWStay_in_State != 0);
		pUn = (SMLUnit*)pSWStay_in_State;
		_pResponse = (WhenResponse*)pSWStay_in_State;
	}
	else if (_condition.terminator() == "CONTINUE") 
	{
		WFWcontinue* pWFWcontinue = new WFWcontinue();
		assert (pWFWcontinue != 0);
		pUn = (SMLUnit*)pWFWcontinue;
		_pResponse = (WhenResponse*)pWFWcontinue;
	}
	else
	{
		ErrorWarning::printHead("ERROR",lineBeingTranslated);
		cout << "Internal error, call expert" << endl;
		cout << endl << " Condition terminator: " << _condition.terminator() << endl;
		throw FATAL;
	}
	

	Name unitType("WhenResponse");
	_internalSMLUnits.addRegardless(unitType,pUn);
	pUn->setParentUnit(this);
	pUn->acceptLines(*_pSMLcode,rowTerm,-1);
	pUn->setFirstCol(colTerm);
	_pResponse->translate();
	
	_condition.getHeader(_pSMLcode,ibegcond,jbegcond,iendcond,jendcond);
	
//	_pResponse->out(" ");			
	return;
} 
//---------------------------------------------------------------------
void When::out(const Name offset) const
{
	SMLUnit::out(offset); 
	char* ptn=offset.getString(); cout << ptn << endl;
	_condition.out(offset);
//	_pResponse->out(offset);
	cout << endl;
}
//------------------------------------------- BF April 2000 -----------
void When::outSobj(ofstream& sobj) const
{
	sobj << "*WHEN" << endl;

	_condition.outSobj(sobj);
	_pResponse->outSobj(sobj);

	return;
}
//---------------------------------------------------------------------------	
SMLlineVector* When::getEndStateActionCodePointer() const
{	
	SMLlineVector* tempPointer;
	tempPointer = _pResponse->getEndStateActionCodePointer();
	
	return tempPointer;
}
//--------------------------------------------------------------------------
int When::examine()
{
	int retcode = 0;
	SMLline firstLine = (*_pSMLcode)[0];

//cout << endl << " When::examine   first line:" << firstLine << endl;
/*	
  cout << endl 
  << " ====================== When::examine() =================" << endl;
	
  cout << "  Parent : " << _pParentUnit->unitId() 
  << "  " << _pParentUnit->unitName() << endl;
		
  cout << "     Complete Ancestry " << endl;
	
	int num;
	NameVector ids,names;
	
	_pParentUnit->ancestry(ids,names);
	
	num = ids.length();

	for (int i=0; i<num; i++)
	{
		cout << "     " << ids[i] << "  " << names[i] << endl;
	}
*/
		
	int iflg = _condition.examine();
	retcode = retcode +iflg;
		
	iflg = _pResponse->examine();
	
	retcode = retcode +iflg;
	
	return retcode;
}

//-------------------------------------------------------------------------------------------------------

Name When::outString() 
{
	Name temp;
	
	temp = "WHEN (...) ";
	
	cout << " When::outString()   needs implementing" << endl;
	
	return temp;
}
