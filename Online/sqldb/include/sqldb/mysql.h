//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================
#ifndef SQLDB_MYSQL_INTERFACE_H
#define SQLDB_MYSQL_INTERFACE_H

/// Framework include files
#include <sqldb/sqldb.h>

///  Technology abstraction layer
/**
 *
 *  \author  M.Frank
 *  \version 1.0
 *  \date    02.05.2017
 */
class mysql : public sqldb::traits  {
 public:
  struct dbase_t;
  struct statement_t;
};

#endif // SQLDB_MYSQL_INTERFACE_H
