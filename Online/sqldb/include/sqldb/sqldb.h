//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================
#ifndef SQLDB_SQLDB_INTERFACE_H
#define SQLDB_SQLDB_INTERFACE_H

// C/C++ files
#include <map>
#include <ctime>
#include <string>
#include <vector>
#include <memory>
#include <cstring>
#include <cstdint>
#include <cstdarg>
#include <stdexcept>

/// SQLDB namespace declarations
namespace sqldb  {

  /// Forward declarations

  
  ///  Technology abstraction layer
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.05.2017
   */
  class traits  {
  public:
  };

  enum sqldb_status  {
    OK             = 0,
    ERROR          = EINVAL,
    NO_PERMISSION  = EPERM,
    DONE           = 10000,
    NO_DB_DATA     = 10002,
    INVALID_HANDLE = 10004
  };
  typedef sqldb_status status_t;
  
  enum field_type   {
    TYPE_NULL     = 14,
    TYPE_TINY     = 15,
    TYPE_SHORT    = 16,
    TYPE_LONG     = 17,
    TYPE_LONGLONG = 18,
    TYPE_FLOAT    = 19,
    TYPE_DOUBLE   = 20,
    TYPE_STRING   = 21,
    TYPE_BLOB     = 22,
    TYPE_TIME     = 23
  };

  typedef std::pair<const uint8_t*, std::size_t> blob_t;
  typedef std::vector<unsigned char>             vblob_t;
  
  class   statement;
  class   database;
  class   time;

  /// Call on failure: Throws exception
  sqldb_status invalid_statement(const char* msg = "[Unknown Error]");
  sqldb_status invalid_statement(const std::string& msg);
  
  ///  Connection argument parser
  /**
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.05.2017
   */
  class connection_args   { 
  public:
    /// Argument type definition
    typedef std::map<std::string, std::string> arguments_t;

  public:
    /// Call to parse the connection string
    arguments_t parse(const std::string& /* connect_string */)  const  {
      arguments_t result;
      return result;
    }
  };

  ///  Pointer cast
  /**
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.05.2017
   */
  union  pointers_t   {
    void*                pointer;
    const void*          pvoid;
    const char*          pstring;
    const std::int8_t*   pint8;
    const std::uint8_t*  puint8;
    const std::int16_t*  pint16;
    const std::uint16_t* puint16;
    const std::int32_t*  pint32;
    const std::uint32_t* puint32;
    const std::int64_t*  pint64;
    const std::uint64_t* puint64;
    const float*         preal32;
    const double*        preal64;
    const class time*    ptime;
    pointers_t(const void* p)  { pvoid = p; }
  };
  
  ///  Safe-access handle
  /**
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.05.2017
   */
  template <typename T> class handle   {
  public:
    T* pointer  { nullptr };

    T* invalid_handle()  const  {
      throw std::runtime_error("Attempt to access an invalid handle [NULL pointer]");
    }
    T* safe_pointer()   {
      if ( pointer ) return pointer;
      return invalid_handle();
    }
    const T* safe_pointer()  const  {
      if ( pointer ) return pointer;
      return invalid_handle();
    }
  public:
    handle(T* p) : pointer(p)  {}
    T* operator-> () const { return  pointer;  }
    T& operator*  () const { return *pointer;  }
    operator T*   () const { return  pointer;  }
  };

  /// Time stamp structure
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.05.2017
   */
  class time   {
  public:
    /// Actual time stamp
    time_t  stamp;

  public:
    /// Default constructor
    time() = default;
    /// Initializing constructor
    time(time_t t) : stamp(t)  {}
    /// Move constructor
    time(time&& copy) = default;
    /// Copy constructor
    time(const time& copy) = default;
    /// Move assignment
    time& operator=(time&& copy) = default;
    /// Copy assigment
    time& operator=(const time& copy) = default;
    /// Equality check
    bool  operator==(const time& copy) const   { return stamp == copy.stamp;  }
    /// Operator to convert to integer time representation
    operator time_t ()  const   {  return this->stamp;  }
    /// Conversion to local time in form of struct tm
    struct tm   local_time()   const;
    /// Conversion to greenwich mean time in form of struct tm
    struct tm   gm_time()   const;
    /// Conversion to string
    std::string to_string()   const;
    /// Conversion to string
    std::string to_string(const std::string& format)   const;

    /// Default time format
    static std::string default_format()        {  return "%Y.%m.%d %H:%M:%S"; }
    /// Current time in seconds
    static time_t now()                        {  return ::time(nullptr);     }
    /// Convert integer time representation to time stamp
    static time make_time(time_t int_time)     {  return time(int_time);      }
    /// Convert ascii string to time stamp
    static time make_time(const std::string& asc_time);
    /// Convert ascii string to time stamp
    static time make_time(const std::string& asc_time, const std::string& format);
    /// Convert struct tm time to time stamp
    static time make_time(const struct tm& tm_time);
  };
  
  ///  Data binder shadow
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.05.2017
   */
  struct column_data   {
    char                       buf[128] {0};
    std::vector<unsigned char> vbuf;
    std::size_t                length  { 0 };
    union _item  {
      std::int8_t    int8;
      std::uint8_t   uint8;
      std::int16_t   int16;
      std::uint16_t  uint16;
      std::int32_t   int32;
      std::uint32_t  uint32;
      std::int64_t   int64;
      std::uint64_t  uint64;
      float          real32;
      double         real64;
      unsigned char* blob;
      char*          string;
      const void*    pointer;
      class time     time;
    } item;
    int              type  { TYPE_NULL };
    char             null  { 0 };
    char             error { 0 };

    /// Constructor
    column_data()  {
      this->length = 0;
      this->item.int64 = 0;
      this->type = TYPE_NULL;
    }
    
    /// Copy Constructor
    column_data(const column_data& copy)  {
      this->type = copy.type;
      this->length = copy.length;
      this->item.int64 = copy.item.int64;
    }
    
    /// Assignment operatpr
    column_data& operator=(const column_data& copy)  {
      if ( this != &copy )   {
	this->type = copy.type;
	this->length = copy.length;
	this->item.int64 = copy.item.int64;
      }
      return *this;
    }
    
    /// Check for null data
    bool is_null()  const  {
      return length == 0 || type == TYPE_NULL;
    }
    /// Clear data structure
    void clear()   {
      this->length = 0;
      this->item.int64 = 0;
      this->type = TYPE_NULL;
      this->vbuf.clear();
      this->buf[0] = 0;
    }
    /// Bind pointers for writing
    void bind_pointer(const void* ptr, std::size_t len);
  };
  
  ///  Database handle class
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.05.2017
   */
  class database  final  {
  public:
    typedef std::vector<column_data> columns_t;
    
  public:

    ///  Database implementation class
    /**
     *
     *  \author  M.Frank
     *  \version 1.0
     *  \date    02.05.2017
     */
    class backend  {
    public:
      backend*                db  {nullptr};
      columns_t               params;
      columns_t               fields;
      std::string             name;

      enum action_type { BEGIN, COMMIT, ROLLBACK, CLOSE, ERRNO };

    public:
      /// Initializing constructor
      backend() = default;
      /// Default destructor
      virtual ~backend() = default;
      /// Access error message on failure
      virtual std::string errmsg() = 0;
      /// Access the number of rows affected by the last statement
      virtual long rows_affected() = 0;
      /// Perform multiple actions
      virtual int action(action_type type) = 0;
      /// Execute prepared statement
      virtual sqldb_status execute(std::string& err, const std::string& sql) = 0;
      /// Prepare a new statement and allocate resources.
      virtual sqldb_status prepare(statement& stmt, const std::string& sql) = 0;
      /// Open the database connection using all information in passed string
      template <typename BACKEND>
      static std::pair<std::shared_ptr<backend>, sqldb_status> open(const std::string& name);
    };
    std::shared_ptr<backend> intern;
    
  public:
    /// Initializing constructor from statement handle
    database(const statement& stmt);
    /// Default constructor
    database() = default;
    /// Move constructor
    database(database&& copy) = default;    
    /// Copy constructor
    database(const database& copy) = default;
    /// Destructor
    ~database() = default;

    /// Move assignment
    database& operator = (database&& copy) = default;
    /// Copy assignment
    database& operator = (const database& copy) = default;

    /// Access the name of a database
    std::string_view name()   const;
    /// Access the name of a database
    static std::string_view name(const database& db);
    /// Access error message on failure
    std::string errmsg()  const;
    /// Access error message on failure
    static std::string errmsg(const database& db);
    /// Check handle validity
    bool is_valid()   const;
    /// Access error number of last failed statement
    int error()   const;

    /// Open the database connection using all information in passed string
    template <typename BACKEND> sqldb_status open(const std::string& con)   {
      auto result = backend::open<BACKEND>(con);
      this->intern = result.first;
      if ( this->intern.get() )   {
	this->intern->db = this->intern.get();
      }
      return result.second;
    }
    /// Open the database connection using all information in passed string
    template <typename BACKEND>
    static database open(const std::string& con, std::string& err)   {
      database db;
      err.clear();
      if ( sqldb::OK != db.open<BACKEND>(con) )   {
	err = db.errmsg();
      }
      return db;
    }
    /// Close the connection to the database
    sqldb_status close();
    /// Start transaction
    sqldb_status begin()    const;
    /// Commit transaction
    sqldb_status commit()   const;
    /// Roll-back transaction
    sqldb_status rollback()   const;
    /// Access the number of rows affected by the last changes
    long rows_affected()  const;
    /// Execute SQL statement
    sqldb_status execute(std::string& error, const char* sql)  const;
    /// Execute SQL statement with variable number of arguments
    sqldb_status execute_sql(std::string& error, const char* fmt, ...)  const;
    /// Prepare a new statement and allocate resources.
    sqldb_status prepare(statement& stmt, const std::string& sql)  const;
  };

  /// Check handle validity
  inline bool database::is_valid()   const       {
    return intern.get() && intern->db != nullptr;
  }

  ///  Record class representing a selected row
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.05.2017
   */
  class record   {
  public:
    /// Reference to statement handle
    handle<const statement> stmt  { nullptr };

  protected:
    /// Default constructor
    record() = default;

  public:
    /// Initializing constructor
    record(const statement* s) : stmt(s)  {}
    /// Move constructor
    record(record&& copy) = delete;
    /// Copy constructor
    record(const record& copy) = default;
    /// Destructor
    ~record() = default;

    /// Move assignment
    record& operator = (record&& copy) = delete;
    /// Copy assignment
    record& operator = (const record& copy) = default;
    
    /// Access the number of result fields of the prepared statement
    std::size_t field_count()  const;
    /// Access raw data from result handle
    column_data& field(std::size_t column)   const;
    /// Check if the field is NULL
    bool is_null(std::size_t column)  const;
    /// Access type specific field data
    template <typename T> T get (std::size_t column)   const;  
  };

  ///  Record set class
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.05.2017
   */
  class record_set : public record    {
  protected:
    /// Default constructor
    record_set() = default;

  public:
    /// Initializing constructor
    record_set(const statement* s) : record(s)  {}
    /// Move constructor
    record_set(record_set&& copy) = delete;
    /// Copy constructor
    record_set(const record_set& copy) = default;
    /// Destructor
    ~record_set() = default;

    /// Move assignment
    record_set& operator = (record_set&& copy) = delete;
    /// Copy assignment
    record_set& operator = (const record_set& copy) = default;

    /// Fetch next result row. Stop if result is <> traits::OK
    sqldb_status fetch_one()  const;
  };

  ///  Record binder class
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.05.2017
   */
  class record_bind : public record   {
  protected:
    /// Default constructor
    record_bind() = default;

  public:
    /// Initializing constructor
    record_bind(const statement* s) : record(s)  {}
    /// Move constructor
    record_bind(record_bind&& copy) = delete;
    /// Copy constructor
    record_bind(const record_bind& copy) = default;
    /// Destructor
    ~record_bind() = default;

    /// Move assignment
    record_bind& operator = (record_bind&& copy) = delete;
    /// Copy assignment
    record_bind& operator = (const record_bind& copy) = default;

    /// Generic bind of a single parameter column
    sqldb_status bind(std::size_t column, field_type typ, const void* data, std::size_t len)  const;
    /// Type specific bind operator for blobs
    sqldb_status bind(std::size_t column, const blob_t& data)  const;
    /// Type specific bind operator for blobs
    sqldb_status bind(std::size_t column, const vblob_t& data)  const;
    /// Type specific bind operator for blobs
    sqldb_status bind(std::size_t column, const uint8_t* data, std::size_t len)  const;
    /// Type specific bind operator for strings
    sqldb_status bind(std::size_t column, char* const data)  const;
    /// Type specific bind operator for strings
    sqldb_status bind(std::size_t column, const char* const data)  const;
    /// Type specific bind operator for strings
    sqldb_status bind(std::size_t column, std::string& data)  const;
    /// Type specific bind operator for strings
    sqldb_status bind(std::size_t column, const std::string& data)  const;
    /// Type specific bind operator for blobs
    template <typename T> sqldb_status bind(std::size_t column, T data)  const;
  };
  
  ///  Statement handle class
  /**
   *
   *  \author  M.Frank
   *  \version 1.0
   *  \date    02.05.2017
   */
  class statement : public record_bind  {
  public:
    friend class record;
    friend class record_bind;
    
    typedef std::vector<column_data> columns_t;
  public:

    ///  Statement handle class
    /**
     *
     *  \author  M.Frank
     *  \version 1.0
     *  \date    02.05.2017
     */
    class backend  {
    public:
      backend*  stmt   { nullptr };
      columns_t params;
      columns_t fields;

      enum action_type { EXEC, FETCH, RESET, FINALIZE, ERRNO };

    public:
      /// Initializing constructor
      backend() = default;
      /// Default destructor
      virtual ~backend() = default;
      /// Access error message on failure
      virtual std::string errmsg() = 0;
      /// Character string of the prepared statement
      virtual const char* sql() = 0;
      /// Access the number of rows affected by the last statement
      virtual long rows_affected() = 0;
      /// Perform multiple backend operations
      virtual int  action(action_type type) = 0;
      /// Bind a single column to the data passed
      virtual sqldb_status bind(std::size_t col, field_type typ, const void* data, std::size_t len) = 0;
    };
    std::shared_ptr<backend> intern;

  protected:
    
    /// Access raw data from result handle
    column_data& _field(std::size_t column) const;
    /// Generic bind of a single parameter column
    sqldb_status          _bind(std::size_t column, field_type typ, const void* data, std::size_t len)  const;

  public:

    /// Default constructor
    statement();
    /// Copy constructor
    statement(const statement& copy);

    /// Move assignment
    statement& operator = (statement&& copy) = delete;
    /// Copy assignment
    statement& operator = (const statement& copy);

    /// Create statement and execute SQL statement with variable number of arguments
    static statement create(const database& db, const char* fmt, ...);

    /// Prepare a new statement and allocate resources.
    sqldb_status prepare(const database& db, const std::string& sql);
    /// Execute SQL statement with variable number of arguments
    sqldb_status prepare(const database& db, const char* fmt, ...);
    
    /// Access the number of result fields of the prepared statement
    std::size_t       param_count()  const;
    
    /// Check handle validity
    bool         is_valid()   const;
    /// Access error number of last failed statement
    int          error()   const;
    /// Access error message on failure
    std::string  errmsg()  const;
    /// Character string of the prepared statement
    const char*  sql()  const;

    /// Access the number of rows affected by the last statement
    long         rows_affected()  const;

    /// Execute prepared statement
    sqldb_status execute()  const;

    /// Fetch next result row. Stop if result is <> traits::OK
    sqldb_status fetch_one()  const;
    
    /// Reset SQL statement: Reset all data to re-execute the statement with new parameters
    sqldb_status reset()  const;
    
    /// Finalize and run-down the statement. Release all resources.
    sqldb_status finalize();

    /// Access to the current selected row
    record       current_row()  const;

    /// Access raw parameter data from statement handle
    column_data& param(std::size_t column) const;
  };

  /// Default constructor
  inline statement::statement() : record_bind(this)
  {
  }

  /// Copy constructor
  inline statement::statement(const statement& copy)
    : record_bind(this), intern(copy.intern)
  {
  }

  /// Access the number of result fields of the prepared statement
  inline std::size_t statement::param_count()  const {
    if ( !this->intern.get() )
      throw std::runtime_error("SQLDB: Invalid statement handle!");
    return intern->params.size();
  }
    
  /// Check handle validity
  inline bool statement::is_valid()   const   {
    return intern.get() != nullptr && intern->stmt != nullptr;
  }
  
  /// Access error message on failure
  inline std::string statement::errmsg()  const      {
    if ( !this->intern.get() )
      throw std::runtime_error("SQLDB: Invalid statement handle!");
    return intern->errmsg();
  }

  /// Character string of the prepared statement
  inline const char* statement::sql()  const         {
    if ( !this->intern.get() )
      throw std::runtime_error("SQLDB: Invalid statement handle!");
    return intern->sql();
  }

  /// Access to the current selected row
  inline record statement::current_row()  const   {
    return record(this);
  }

  /// Access raw parameter data from statement handle
  inline column_data& statement::param(std::size_t column) const   {
    if ( !this->intern.get() )
      throw std::runtime_error("SQLDB: Invalid statement handle!");
    if ( column >= intern->params.size() )
      invalid_statement("Invalid column identifier");
    return intern->params.at(column);
  }

  /// Access raw data from result handle
  inline column_data& statement::_field(std::size_t column) const   {
    if ( !this->intern.get() )
      throw std::runtime_error("SQLDB: Invalid statement handle!");
    if ( column >= intern->fields.size() )
      invalid_statement("Invalid column identifier");
    return intern->fields.at(column);
  }

  /// Access the number of rows affected by the last statement
  inline long statement::rows_affected()  const {
    if ( this->intern.get() )  return this->intern->rows_affected();
    throw std::runtime_error("SQLDB: Invalid statement handle!");
  }

  /// Access the number of result fields of the prepared statement
  inline std::size_t record::field_count()  const   {
    if ( this->stmt && this->stmt->intern.get() ) return this->stmt->intern->fields.size();
    throw std::runtime_error("SQLDB: Invalid statement handle!");
  }

  /// Access raw data from result handle
  inline column_data& record::field(std::size_t column)   const   {
    if ( this->stmt ) return this->stmt->_field(column);
    throw std::runtime_error("SQLDB: Invalid statement handle!");
  }
  
  inline bool record::is_null(std::size_t col)  const  {
    if ( this->stmt ) return this->stmt->_field(col).null;
    throw std::runtime_error("SQLDB: Invalid statement handle!");
  }
}       // End namespace sqldb

/// The default is an inline implementation
/// To use a library compile with -DSQLDB_INLINE
///
#ifndef SQLDB_INLINE
#define SQLDB_INLINE inline

#include <sqldb/sqldb-imp.h>

#undef  SQLDB_INLINE
#endif

#endif  // SQLDB_SQLDB_INTERFACE_H
