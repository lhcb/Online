//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================

/// Framework include files
#include <sqldb/mysql.h>

/// MySQL header
#include <mysql.h>

struct mysql::dbase_t     : public MYSQL       {  };
struct mysql::statement_t : public MYSQL_STMT  {  };

/// SQLDB namespace declaration
namespace sqldb  {

  namespace mysql_imp  {
    ///
    inline bool mysql_ok(int status)    {
      return status == 0;
    }
    ///
    inline field_type gen_field_type(enum_field_types typ)   {
      switch(typ)   {
      case MYSQL_TYPE_TINY:      return TYPE_TINY;
      case MYSQL_TYPE_SHORT:     return TYPE_SHORT;
      case MYSQL_TYPE_LONG:      return TYPE_LONG;
      case MYSQL_TYPE_LONGLONG:  return TYPE_LONGLONG;
      case MYSQL_TYPE_FLOAT:     return TYPE_FLOAT;
      case MYSQL_TYPE_DOUBLE:    return TYPE_DOUBLE;
      case MYSQL_TYPE_STRING:    return TYPE_STRING;
      case MYSQL_TYPE_BLOB:      return TYPE_BLOB;
      case MYSQL_TYPE_TIME:      return TYPE_TIME;
      case MYSQL_TYPE_DATE:      return TYPE_TIME;
      case MYSQL_TYPE_DATETIME:  return TYPE_TIME;
      default:
	break;
      }
      throw std::runtime_error("Invalid data type encountered!");
    }
    ///
    inline enum_field_types mysql_field_type(field_type typ)   {
      switch(typ)   {
      case TYPE_TINY:      return MYSQL_TYPE_TINY;
      case TYPE_SHORT:     return MYSQL_TYPE_SHORT;
      case TYPE_LONG:      return MYSQL_TYPE_LONG;
      case TYPE_LONGLONG:  return MYSQL_TYPE_LONGLONG;
      case TYPE_FLOAT:     return MYSQL_TYPE_FLOAT;
      case TYPE_DOUBLE:    return MYSQL_TYPE_DOUBLE;
      case TYPE_STRING:    return MYSQL_TYPE_STRING;
      case TYPE_BLOB:      return MYSQL_TYPE_BLOB;
      case TYPE_TIME:      return MYSQL_TYPE_DATETIME;
      default:
	break;
      }
      throw std::runtime_error("Invalid data type encountered!");
    }

    ///  Technology abstraction layer
    /**
     *  \author  M.Frank
     *  \version 1.0
     *  \date    02.05.2017
     */
    struct column_bind : public MYSQL_BIND   {
    public:
      /// Bind parameter to null value
      sqldb_status bind_store_null(column_data* d)   {
	d->null       = 1;
	this->is_null = &d->null;
	this->buffer  = nullptr;
	this->length  = nullptr;
	this->error   = nullptr;
	return sqldb::OK;
      }
      /// Generic parameter binding
      sqldb_status bind_store(column_data* d, const unsigned char* ptr, std::size_t len)   {
	if ( ptr )  {
	  d->bind_pointer(ptr, len);
	  this->buffer  = d->item.string;
	  this->length  = &d->length;
	  this->is_null = nullptr;
	  this->error   = nullptr;
	  return sqldb::OK;
	}
	return this->bind_store_null(d);
      }
      /// Type specific parameter bind
      template <typename T> sqldb_status bind_store(column_data* d, T& to, const T* from)   {
	if ( from )   {
	  to            = *from;
	  d->length     = sizeof(T);
	  this->buffer  = &to;
	  this->length  = nullptr;
	  this->is_null = nullptr;
	  this->error   = nullptr;
	  return sqldb::OK;
	}
	return this->bind_store_null(d);
      }
      /// Bind output field
      sqldb_status bind_fetch(column_data* d, void* to, std::size_t len)    {
	d->null             = false;
	d->error            = false;
	d->length           = 0;
	this->buffer        = to;
	this->length        = &d->length;
	this->is_null       = &d->null;
	this->error         = &d->error;
	this->buffer_length = len;
	return sqldb::OK;
      }
    };

    ///  Technology abstraction layer for a MySQL prepared statement
    /**
     *  \author  M.Frank
     *  \version 1.0
     *  \date    02.05.2017
     */
    struct statement_backend : public statement::backend  {
    public:
      typedef std::tuple<column_data*, column_bind*, MYSQL_FIELD*> field_t;
      std::vector<column_bind>      _pbind, _fbind;
      std::string                   _sql_string;
      MYSQL_STMT*                   _imp  { nullptr };

      /// Access to the internal statement handle. Throws exception if the handle is invalid
      MYSQL_STMT* handle()   const   {
	if ( this->_imp ) return this->_imp;
	throw std::runtime_error("Failed to access statement handle. [Invalid handle]");
      }
      std::pair<column_data*, column_bind*> param(int col)   {
	return std::make_pair(&params[col], &_pbind[col]);
      }
      field_t field(int col)   {
	return field_t(&this->fields[col], &_fbind[col], &_imp->fields[col]);
      }
      /// Access error message on failure
      virtual std::string errmsg()  override  {
	return ::mysql_stmt_error(this->handle());
      }
      /// Character string of the prepared statement
      virtual const char* sql()  override  {
	return this->_sql_string.c_str();
      }
      /// Access the number of rows affected by the last statement
      virtual long  rows_affected()  override  {
	return ::mysql_stmt_num_rows(this->handle());
      }

      /// Prepare a new statement and allocate resources.
      sqldb_status prepare(mysql::dbase_t* db, const std::string& sql_str)    {
	MYSQL_STMT* thestmt = nullptr;
	if ( _imp ) ::mysql_stmt_reset(_imp);
	if ( _imp ) ::mysql_stmt_close(_imp);
	thestmt = ::mysql_stmt_init(db);
	if ( thestmt )   {
	  int ret = ::mysql_stmt_prepare(thestmt, sql_str.c_str(), sql_str.length());
	  if ( ret == sqldb::OK )    {
#if 0
	    unsigned long type = CURSOR_TYPE_READ_ONLY;
	    int rc = ::mysql_stmt_attr_set(thestmt, STMT_ATTR_CURSOR_TYPE, &type);
	    if ( rc )   {
	      return this->error();
	    }
#endif
	    int num_param = ::mysql_stmt_param_count(thestmt);
	    int num_field = ::mysql_stmt_field_count(thestmt);
	    this->_pbind.resize(num_param, column_bind());
	    this->params.resize(num_param, column_data());
	    this->_fbind.resize(num_field, column_bind());
	    this->fields.resize(num_field, column_data());
	    this->_sql_string = sql_str;
	    this->_imp = thestmt;
	    this->stmt = this;
	    return sqldb::OK;
	  }
	  return sqldb::ERROR;
	}
	return invalid_statement();
      }

      /// Execute prepared statement
      virtual sqldb_status execute()   {
	if( auto* h = this->_imp )  {
	  if( this->_pbind.size() )   {
	    if( ::mysql_stmt_bind_param(h, &this->_pbind.front()) )  {
	      return invalid_statement(::mysql_stmt_error(h));
	    }
	  }
	  if( sqldb::OK == ::mysql_stmt_execute(h) )    {
	    if( sqldb::OK != ::mysql_stmt_store_result(h) )   {
	      if( auto* r = ::mysql_stmt_result_metadata(h) )   {
		::mysql_free_result(r);
		return invalid_statement(::mysql_stmt_error(h));
	      }
	    }
	    return sqldb::OK;
	  }
	  return invalid_statement(::mysql_stmt_error(h));
	}
	return invalid_statement();
      }

      /// Fetch next result row. Stop if result is NON-zero
      virtual sqldb_status fetch_one()  {
	if ( this->_imp )  {
	  std::size_t nfld = this->_fbind.size();
	  if ( nfld > 0 )   {
	    ::memset(&this->_fbind.front(), 0, nfld*sizeof(column_bind));
	    for( std::size_t i=0; i < nfld; i++ )  {
	      this->bind_fetch(i);
	      if ( !mysql_ok(::mysql_stmt_bind_result(this->_imp, &this->_fbind.front())) )
		return invalid_statement(::mysql_stmt_error(this->_imp));
	    }
	  }
	  int ret = ::mysql_stmt_fetch(this->_imp);
	  if( ret == MYSQL_NO_DATA )  { 
	    return sqldb::NO_DB_DATA;
	  }
	  else if( ret == MYSQL_DATA_TRUNCATED )  {
	    for( std::size_t i=0; i < nfld; i++ )  {
	      auto& d = this->fields[i];
	      if( d.error && !d.null && d.length >= sizeof(d.buf) ) {
		auto& b = this->_fbind[i];
		d.vbuf.resize(d.length);
		d.item.blob     = &d.vbuf.front();
		b.buffer        = d.item.blob;
		b.buffer_length = d.length;
		if( !mysql_ok(::mysql_stmt_fetch_column(this->_imp,&b,i,0)) ) {
		  return invalid_statement(::mysql_stmt_error(this->_imp));
		}
	      }
	    }
	  }
	  return sqldb::OK;
	}
	return invalid_statement();
      }

      /// Bind output field identified by column number
      sqldb_status bind_fetch(std::size_t col)  {
	auto* p        = this;
	auto [d, b, f] = p->field(col);

	::memset(b, 0, sizeof(column_bind));
	b->buffer_type = f->type;
	d->type        = gen_field_type(f->type);
	switch(d->type)   {
	case TYPE_TINY:     return b->bind_fetch(d, &d->item.int8,   sizeof(d->item.int8));
	case TYPE_SHORT:    return b->bind_fetch(d, &d->item.int16,  sizeof(d->item.int16));
	case TYPE_LONG:     return b->bind_fetch(d, &d->item.int32,  sizeof(d->item.int32));
	case TYPE_LONGLONG: return b->bind_fetch(d, &d->item.int64,  sizeof(d->item.int64));
	case TYPE_FLOAT:    return b->bind_fetch(d, &d->item.real32, sizeof(d->item.real32));
	case TYPE_DOUBLE:   return b->bind_fetch(d, &d->item.real64, sizeof(d->item.real64));
	case TYPE_STRING:   return b->bind_fetch(d, d->buf,          sizeof(d->buf));
	case TYPE_BLOB:     return b->bind_fetch(d, d->buf,          sizeof(d->buf));
	case TYPE_TIME:     return b->bind_fetch(d, &d->item.time.stamp, sizeof(d->item.int64));
	default:            return invalid_statement("Invalid MySQL Field data type");
	}
	return sqldb::OK;
      }

      /// Bind a single column to the data passed
      virtual sqldb_status bind(std::size_t col, field_type typ, const void* data, std::size_t len)  override {
	pointers_t ptr(data);
	auto [d, b]    = this->param(col);
	::memset(b, 0, sizeof(column_bind));
	d->type        = typ;
	b->buffer_type = mysql_field_type(typ);
	switch(typ)   {
	case TYPE_NULL:     return b->bind_store_null(d);
	case TYPE_TINY:	    return b->bind_store(d, d->item.uint8,  ptr.puint8);
	case TYPE_SHORT:    return b->bind_store(d, d->item.uint16, ptr.puint16);
	case TYPE_LONG:	    return b->bind_store(d, d->item.uint32, ptr.puint32);
	case TYPE_LONGLONG: return b->bind_store(d, d->item.uint64, ptr.puint64);
	case TYPE_FLOAT:    return b->bind_store(d, d->item.real32, ptr.preal32);
	case TYPE_DOUBLE:   return b->bind_store(d, d->item.real64, ptr.preal64);
	case TYPE_STRING:   return b->bind_store(d, ptr.puint8,     len);
	case TYPE_BLOB:	    return b->bind_store(d, ptr.puint8,     len);
	  /// For now store time stamps as long integers (time_t)
	case TYPE_TIME:     return b->bind_store(d, d->item.time.stamp, ptr.pint64);
	default:            return invalid_statement("Invalid MySQL parameter data type");
	}
	return sqldb::OK;
      }

      /// Action routine for various functions
      int action(action_type type)   override   {
	switch(type)   {
	case EXEC:         /// Execute prepared statement
	  return this->execute();
	case FETCH:        /// Fetch next result row. Stop if result is NON-zero
	  return this->fetch_one();
	case RESET:        /// Reset all data to re-execute the statement with new parameters
	  if( auto* h = this->handle() )  {
	    if( !mysql_ok(::mysql_stmt_reset(h)) )
	      return sqldb::ERROR;
	  }
	  return sqldb::OK;
	case FINALIZE:     /// Finalize and run-down the statement. Release all resources.
	  if ( this->_imp )   {
	    ::mysql_stmt_reset(this->_imp);
	    ::mysql_stmt_close(this->_imp);
	    this->_imp = nullptr;
	  }
	  return sqldb::OK;
	case ERRNO:        /// Access error number of last failed statement
	  return ::mysql_stmt_errno(this->handle());
	default:
	  throw std::runtime_error("Invalid action request received!");
	}
      }
    };

    ///  Technology abstraction layer for a MySQL database
    /**
     *  \author  M.Frank
     *  \version 1.0
     *  \date    02.05.2017
     */
    struct dbase_backend : public database::backend  {
      mysql::dbase_t  _imp;
      mysql::dbase_t* _handle {nullptr};

      /// Access to the internal database handle. Throws exception if the handle is invalid
      mysql::dbase_t* handle()   const   {
	if ( _handle ) return _handle;
	throw std::runtime_error("Invalid MySQL database handle");
      }
      /// Access error message on failure
      virtual std::string errmsg()  override  {
	return ::mysql_error(this->handle());
      }
      /// Access the number of rows affected by the last statement
      virtual long rows_affected()  override  {
	return ::mysql_affected_rows(this->handle());
      }
      /// Perform multiple actions
      virtual int action(action_type type)  override  {
	switch(type)    {
	case CLOSE:
	  if ( this->_handle )  {
	    ::mysql_close(this->_handle);
	    ::memset(&_imp, 0, sizeof(_imp));
	    this->_handle = nullptr;
	  }
	  return sqldb::OK;
	case BEGIN:       /// Start transaction
#define _START "START TRANSACTION;"
	  return ::mysql_real_query(this->_handle,_START,sizeof(_START)) ? sqldb::ERROR : sqldb::OK;
	case COMMIT:      /// Commit transaction
	  return ::mysql_commit(this->handle());
	case ROLLBACK:    /// Rollback transaction
	  return ::mysql_rollback(this->handle());
	case ERRNO:       /// Access error number of last failed statement
	  return ::mysql_errno(this->handle());
	default:
	  throw std::runtime_error("Invalid action request received!");
	}
      }
      /// Execute prepared statement
      virtual sqldb_status execute(std::string& err, const std::string& sql)  override  {
	if( this->_handle )  {
	  if( ::mysql_real_query(this->_handle, sql.c_str(), sql.length()) )   {
	    err  = ::mysql_error(this->_handle);
	    return sqldb::ERROR;
	  }
	  return sqldb::OK;
	}
	return sqldb::INVALID_HANDLE;
      }
      /// Prepare a new statement and allocate resources.
      sqldb_status prepare(statement& bs, const std::string& sql_str)   override  {
	auto* stm_imp = new statement_backend();
	bs.intern.reset(stm_imp);
	return stm_imp->prepare(this->handle(), sql_str);
      }
      /// Open database
      sqldb_status open(const std::string& connect_string)   {
	auto args = connection_args().parse(connect_string);
	this->_handle = nullptr;
	if ( nullptr == ::mysql_real_connect(&_imp,
					     args["host"].c_str(),
					     args["user"].c_str(),
					     args["password"].c_str(),
					     args["database"].c_str(),
					     0, nullptr, 0) )   {
	  return sqldb::ERROR;
	}
	this->_handle = &_imp;
	this->name    = args["database"];
	return sqldb::OK;
      }
      
      /// Initializing constructor
      dbase_backend() = default;
    };
  }
}

/// SQLDB namespace declaration
namespace sqldb  {
  
  /// Open the database connection using all information in passed string
  template <>
  std::pair<std::shared_ptr<database::backend>, sqldb_status>
  database::backend::open<mysql>(const std::string& connect_string)   {
    auto db = std::make_shared<mysql_imp::dbase_backend>();
    sqldb_status ret = db->open(connect_string);
    if ( ret == sqldb::OK )
      return make_pair(db, sqldb::OK);
    return make_pair(std::shared_ptr<database::backend>(), ret);
  }
}       // End namespace sqldb
