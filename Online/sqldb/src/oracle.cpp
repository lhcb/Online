//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================

/// Framework include files
#include <sqldb/oracle.h>

/// SQLite3 header
#include "sql.h"
#include "oci.h"
#include "ocidfn.h"

/// C/C++ headers
#include <stdexcept>

#define SQLDB_SQLITE_INLINE

/// SQLDB namespace declaration
namespace sqldb  {
 
  namespace oracle_imp  {

    typedef unsigned short        result_t;
    typedef struct OCIEnv*        EnvH;
    typedef struct OCISvcCtx*     DbcH;
    typedef struct OCIStmt*       StmH;
    typedef struct OCIError*      ErrorH;
    typedef struct OCIBind*       BindH;
    typedef struct OCIParam*      ParamH;
    typedef struct OCIDefine*     DefineH;
    typedef struct OCILobLocator* LobLocatorH;

    class column_bind;
    class statement_backend;

    ///
    inline bool oracle_ok(int status)    {
      return ((status)&(~1)) == 0;
    }
    ///
    inline field_type gen_field_type(int typ)   {
      switch(typ)   {
      case SQLT_UIN:
      case SQLT_NUM:
      case SQLT_INT:              return TYPE_LONGLONG;
      case SQLT_BFLOAT:
      case SQLT_BDOUBLE:
      case SQLT_FLT:              return TYPE_DOUBLE;
      case SQLT_AFC:
      case SQLT_AVC:
      case SQLT_STR:
      case SQLT_VCS:
      case SQLT_LVC:             return TYPE_STRING;
      case SQLT_VBI:
      case SQLT_BIN:
      case SQLT_LBI:
      case SQLT_LVB:             return TYPE_BLOB;
      default:
	break;
      }
      throw std::runtime_error("Invalid data type encountered!");
    }
    ///
    inline int oracle_field_type(field_type typ)   {
      switch(typ)   {
      case TYPE_TINY:
      case TYPE_SHORT:
      case TYPE_LONG:
      case TYPE_LONGLONG:  return SQLT_INT;
      case TYPE_FLOAT:     
      case TYPE_DOUBLE:    return SQLT_FLT;
      case TYPE_STRING:    return SQLT_VCS;
      case TYPE_BLOB:      return SQLT_LBI;
      default:
	break;
      }
      throw std::runtime_error("Invalid data type encountered!");
    }
    
    std::string error_message(int code, ErrorH err)   {
      text b[512];
      int  c;
      std::string msg = "";
      switch (code)   {
      case OCI_SUCCESS:
	msg = "SUCCESS";
	break;
      case OCI_SUCCESS_WITH_INFO:
	msg = "Error - OCI_SUCCESS_WITH_INFO";
	break;
      case OCI_NEED_DATA:
	msg = "Error - OCI_NEED_DATA";
	break;
      case OCI_NO_DATA:
	msg = "Error - OCI_NODATA - ";
	::OCIErrorGet(err,1,0,&c,b,sizeof(b),OCI_HTYPE_ERROR);
	msg += (char*)b;
	break;
      case OCI_ERROR:
	msg = "Error - ";
	::OCIErrorGet(err,1,0,&c,b,sizeof(b),OCI_HTYPE_ERROR);
	msg += (char*)b;
	break;
      case OCI_INVALID_HANDLE:
	msg = "Error - OCI_INVALID_HANDLE - ";
	::OCIErrorGet(err,1,0,&c,b,sizeof(b),OCI_HTYPE_ERROR);
	msg += (char*)b;
	break;
      case OCI_STILL_EXECUTING:
	msg = "Error - OCI_STILL_EXECUTING";
	break;
      default:
	msg = "";
	break;
      }
      return msg;
    }
    
    ///  Technology abstraction layer
    /**
     *
     *  \author  M.Frank
     *  \version 1.0
     *  \date    02.05.2017
     */
    class column_bind  {
    public:
      std::size_t        _column   { 0 };
      statement_backend* _stm      { nullptr };
      BindH              _bind     { nullptr };
      DefineH            _define   { nullptr };
      LobLocatorH        _lob      { nullptr };
      ub2                _sql_type;
      
    public:
      /// Default constructor
      column_bind() = default;
      /// Move constructor
      column_bind(column_bind&& copy) = delete;
      /// Copy constructor
      column_bind(const column_bind& copy) = delete;
      /// Default destructor
      ~column_bind();

      /// Move assignment
      column_bind& operator = (column_bind&& copy) = delete;
      /// Copy assignment
      column_bind& operator = (const column_bind& copy) = delete;

      /// Generic parameter binding
      sqldb_status bind_str(column_data& d, const char* ptr, std::size_t len);
      /// Generic parameter binding
      sqldb_status bind_blob(column_data& d, const unsigned char* ptr, std::size_t len);
      /// Type specific parameter bind
      sqldb_status bind_int(column_data& d, int32_t from);
      /// Type specific parameter bind
      sqldb_status bind_int64(column_data& d, int64_t from);
      /// Type specific parameter bind
      sqldb_status bind_flt(column_data& d, double from);
      /// Read lob data
      sqldb_status fetch_data(column_data& d);
      /// Bind output field
      sqldb_status bind_fetch(column_data& d, void* ptr, std::size_t len);
    };
  
    ///  Technology abstraction layer
    /**
     *
     *  \author  M.Frank
     *  \version 1.0
     *  \date    02.05.2017
     */
    class statement_backend : public statement::backend {
    public:
      typedef std::tuple<column_data*, column_bind*, ub2> field_t;
      typedef std::vector<column_bind> Binders;

      std::vector<ub2> _types;
      Binders     _pbind, _fbind;
      EnvH        _env   { nullptr };
      DbcH        _dbc   { nullptr };
      StmH        _stmt  { nullptr };
      ErrorH      _error { nullptr };
      std::string _sql_str     {   };
      int         _error_code  { 0 };

    public:
      /// Initializing constructor
      statement_backend(EnvH env, DbcH dbc, ErrorH err) : _env(env), _dbc(dbc), _error(err)  {
      }
      /// Default constructor
      statement_backend() = delete;
      /// Move constructor
      statement_backend(statement_backend&& copy) = delete;
      /// Copy constructor
      statement_backend(const statement_backend& copy) = delete;
      /// Default destructor
      virtual ~statement_backend()   {
	if ( _stmt )  {
	  ::OCIHandleFree(_stmt, OCI_HTYPE_STMT);
	  _stmt = nullptr;
	}
	if ( _error )  {
	  ::OCIHandleFree(_error, OCI_HTYPE_ERROR);
	  _error = nullptr;
	}
      }
      /// Move assignment
      statement_backend& operator = (statement_backend&& copy) = delete;
      /// Copy assignment
      statement_backend& operator = (const statement_backend& copy) = delete;

      /// Access to the internal statement handle. Throws exception if the handle is invalid
      StmH handle()   const   {
	if ( this->_stmt ) return this->_stmt;
	throw std::runtime_error("Failed to access statement handle. [Invalid handle]");
      }
      std::pair<column_data*, column_bind*> param(int col)   {
	return std::make_pair(&params[col], &_pbind[col]);
      }
      field_t field(int col)   {
	return field_t(&this->fields[col], &_fbind[col], _types[col]);
      }
      /// Access error message on failure
      std::string errmsg()  override  {
        return error_message(_error_code, _error);
      }
      /// Character string of the prepared statement
      const char* sql()  override  {
	return _sql_str.c_str();
      }
      /// Access the number of rows affected by the last statement
      long rows_affected()  override;
      /// Execute prepared statement
      sqldb_status execute();
      /// Fetch next result row. Stop if result is NON-zero
      sqldb_status fetch_one();
      /// Bind output field identified by column number
      sqldb_status bind_fetch(std::size_t col);
      /// Prepare a new statement and allocate resources.
      sqldb_status prepare(const std::string& sql_str);
      /// Bind a single column to the data passed
      virtual sqldb_status bind(std::size_t col, field_type typ, const void* data, std::size_t len)  override;
      ///
      virtual int action(action_type type)   override;
    };

    ///  Technology abstraction layer for the database
    /**
     *
     *  \author  M.Frank
     *  \version 1.0
     *  \date    02.05.2017
     */
    struct dbase_backend : public database::backend  {
      DbcH        _dbc;
      EnvH        _env;
      ErrorH      _error;
      int         _error_code;
      
      /// Default constructor
      dbase_backend();
      /// Move constructor
      dbase_backend(dbase_backend&& copy) = delete;
      /// Copy constructor
      dbase_backend(const dbase_backend& copy) = delete;
      /// Default destructor
      virtual ~dbase_backend();
      /// Move assignment
      dbase_backend& operator = (dbase_backend&& copy) = delete;
      /// Copy assignment
      dbase_backend& operator = (const dbase_backend& copy) = delete;

      /// Access to the internal database handle. Throws exception if the handle is invalid
      DbcH handle()   const   {
	if ( _dbc ) return _dbc;
	throw std::runtime_error("Invalid MySQL database handle");
      }
      /// Prepare a new statement and allocate resources.
      sqldb_status prepare(statement& bs, const std::string& sql_str)  override;
      /// Open database
      sqldb_status open(const std::string& connect_string);
      /// Access error message on failure
      std::string errmsg()  override  {
	return error_message(_error_code, _error);
      }
      /// Access the number of rows affected by the last statement
      virtual long rows_affected()  override  {
	return -1;
      }
      /// Execute prepared statement
      virtual sqldb_status execute(std::string& error, const std::string& sql_str)  override;
      /// Perform multiple actions
      virtual int action(action_type type)  override;
    };

    /// Default destructor
    column_bind::~column_bind()   {
      if ( _lob )   {
	::OCIDescriptorFree(_lob, OCI_DTYPE_LOB);
	_lob = nullptr;
      }
    }

    /// Generic parameter binding
    sqldb_status column_bind::bind_str(column_data& d, const char* ptr, std::size_t len)   {
      pointers_t p(ptr);
      d.bind_pointer(p.puint8, len);
      auto ret = ::OCIBindByPos(_stm->_stmt, &_bind, _stm->_error,
				_column, p.pointer, len, SQLT_VCS,
				0, 0, 0, 0, 0, OCI_DEFAULT);
      return oracle_ok(ret) ? sqldb::OK : sqldb::ERROR;
    }

    /// Generic parameter binding
    sqldb_status column_bind::bind_blob(column_data& d, const unsigned char* ptr, std::size_t len)   {
      pointers_t p(ptr);
      d.bind_pointer(ptr, len);
      auto ret = ::OCIBindByPos(_stm->_stmt, &_bind, _stm->_error,
				_column, p.pointer, len, SQLT_LVB,
				0, 0, 0, 0, 0, OCI_DEFAULT);
      return oracle_ok(ret) ? sqldb::OK : sqldb::ERROR;
    }

    /// Type specific parameter bind
    sqldb_status column_bind::bind_int(column_data& d, int32_t from)   {
      d.item.int32  = from;
      d.length = sizeof(int);
      result_t ret = ::OCIBindByPos(_stm->_stmt, &_bind, _stm->_error,
				    _column, &d.item.int32, sizeof(d.item.int32), SQLT_INT,
				    0, 0, 0, 0, 0, OCI_DEFAULT);
      return oracle_ok(ret) ? sqldb::OK : sqldb::ERROR;
    }

    /// Type specific parameter bind
    sqldb_status column_bind::bind_int64(column_data& d, int64_t from)   {
      d.item.int64  = from;
      d.length = sizeof(int64_t);
      result_t ret = ::OCIBindByPos(_stm->_stmt, &_bind, _stm->_error,
				    _column, &d.item.int64, sizeof(d.item.int64), SQLT_INT,
				    0, 0, 0, 0, 0, OCI_DEFAULT);
      return oracle_ok(ret) ? sqldb::OK : sqldb::ERROR;
    }

    /// Type specific parameter bind
    sqldb_status column_bind::bind_flt(column_data& d, double from)   {
      d.item.real64 = from;
      d.length = sizeof(double);
      result_t ret = ::OCIBindByPos(_stm->_stmt, &_bind, _stm->_error,
				    _column, &d.item.real64, sizeof(d.item.real64), SQLT_FLT,
				    0, 0, 0, 0, 0, OCI_DEFAULT);
      return oracle_ok(ret) ? sqldb::OK : sqldb::ERROR;
    }

    /// Read lob data
    sqldb_status column_bind::fetch_data(column_data& d)   {
      result_t rc  = OCI_SUCCESS;
      DbcH     db  = _stm->_dbc;
      ErrorH   err = _stm->_error;
      if ( d.type == TYPE_BLOB && nullptr != _lob )   {
	rc = ::OCILobOpen(db, err, _lob, OCI_LOB_READONLY);
	if ( oracle_ok(rc) )    {
	  unsigned int len = 0, rd_len = 0;
	  rc = ::OCILobGetLength(db, err, _lob, &len);
	  if ( oracle_ok(rc) )    {
	    d.length = len;
	    d.vbuf.resize(len);
	    d.item.pointer = &d.vbuf[0];
	    rc = ::OCILobRead(db,err,_lob,&rd_len,1,d.item.blob,len,0,0,0,SQLCS_IMPLICIT);
	  }
	}
	::OCILobClose(db, err, _lob);
      }
      return oracle_ok(rc) ? sqldb::OK : sqldb::ERROR;
    }

    /// Bind output field
    sqldb_status column_bind::bind_fetch(column_data& d, void* ptr, std::size_t len)    {
      result_t rc = OCI_SUCCESS;
      d.null     = false;
      d.error    = false;
      d.length   = 0;
      if ( d.type == TYPE_BLOB )   {
	if ( _lob )   {
	  ::OCIDescriptorFree(_lob, OCI_DTYPE_LOB);
	}
	rc = ::OCIDescriptorAlloc(_stm->_env, (void**)&_lob, OCI_DTYPE_LOB, 0, 0);
	if ( oracle_ok(rc) )    {
	  len = -1;
	  ptr  = &_lob;
	}
      }
      if ( oracle_ok(rc) )    {
	rc = ::OCIDefineByPos(_stm->_stmt, &_define, _stm->_error,
			      _column, ptr, len, _sql_type,
			      0, 0, 0, OCI_DEFAULT);
      }
      return oracle_ok(rc) ? sqldb::OK : sqldb::ERROR;
    }

    /// Access the number of rows affected by the last statement
    long statement_backend::rows_affected()   {
      ub4 row_count = 0;
      result_t ret = ::OCIAttrGet(_stmt, OCI_HTYPE_STMT, &row_count, 0, OCI_ATTR_ROW_COUNT, _error);
      if ( oracle_ok(ret) )
	return row_count;
      _error_code = ret;
      return -1;
    }

    /// Fetch next result row. Stop if result is NON-zero
    sqldb_status statement_backend::fetch_one()  {
      if ( _stmt )  {
	std::size_t nfld = _fbind.size();
	if ( nfld > 0 )   {
	  for( std::size_t i=0; i < nfld; i++ )  {
	    if ( bind_fetch(i) != sqldb::OK )   {
	      return invalid_statement(errmsg());
	    }
	  }
	}
	result_t ret = ::OCIStmtFetch(_stmt, _error, 1, OCI_FETCH_NEXT, OCI_DEFAULT);
	if( ret == OCI_SUCCESS )  { 
	  for( std::size_t i=0; i < nfld; i++ )  {
	    auto& d = fields[i];
	    if ( d.type == TYPE_BLOB )   {
	      if ( sqldb::OK != _fbind[i].fetch_data(d) )   {
		return invalid_statement(errmsg());
	      }
	    }
	  }
	}
	return sqldb::OK;
      }
      return invalid_statement();
    }

    /// Bind output field identified by column number
    sqldb_status statement_backend::bind_fetch(std::size_t col)  {
      auto* p        = this;
      auto [d, b, type] = p->field(col);
      b->_column   = col;
      b->_stm      = this;
      b->_bind     = nullptr;
      b->_define   = nullptr;
      b->_lob      = nullptr;
      b->_sql_type = type;
      d->type      = gen_field_type(type);
      switch(type)   {
      case TYPE_TINY:     return b->bind_fetch(*d, &d->item.int8,   sizeof(d->item.int8));
      case TYPE_SHORT:    return b->bind_fetch(*d, &d->item.int16,  sizeof(d->item.int16));
      case TYPE_LONG:     return b->bind_fetch(*d, &d->item.int32,  sizeof(d->item.int32));
      case TYPE_LONGLONG: return b->bind_fetch(*d, &d->item.int64,  sizeof(d->item.int64));
      case TYPE_FLOAT:    return b->bind_fetch(*d, &d->item.real32, sizeof(d->item.real32));
      case TYPE_DOUBLE:   return b->bind_fetch(*d, &d->item.real64, sizeof(d->item.real64));
      case TYPE_STRING:   return b->bind_fetch(*d, nullptr,         sizeof(d->buf));
      case TYPE_BLOB:     return b->bind_fetch(*d, nullptr,         sizeof(d->buf));
      default:            return invalid_statement("Invalid MySQL Field data type");
      }
      return sqldb::OK;
    }

    /// Bind a single column to the data passed
    sqldb_status statement_backend::bind(std::size_t col, field_type typ, const void* data, std::size_t len)  {
      pointers_t ptr(data);
      auto [d, b]  = param(col);
      d->type      = typ;
      b->_column   = col;
      b->_stm      = this;
      b->_bind     = nullptr;
      b->_define   = nullptr;
      b->_lob      = nullptr;
      b->_sql_type = oracle_field_type(typ);
      switch(typ)   {
      case TYPE_TINY:	  return b->bind_int  (*d, *ptr.puint8);
      case TYPE_SHORT:    return b->bind_int  (*d, *ptr.puint16);
      case TYPE_LONG:	  return b->bind_int  (*d, *ptr.puint32);
      case TYPE_LONGLONG: return b->bind_int64(*d, *ptr.puint64);
      case TYPE_FLOAT:    return b->bind_flt  (*d, *ptr.preal32);
      case TYPE_DOUBLE:   return b->bind_flt  (*d, *ptr.preal64);
      case TYPE_STRING:   return b->bind_str  (*d, ptr.pstring, len);
      case TYPE_BLOB:	  return b->bind_blob (*d, ptr.puint8, len);
      default:            return invalid_statement("Invalid MySQL parameter data type");
      }
      return sqldb::OK;
    }

    /// Prepare a new statement and allocate resources.
    sqldb_status statement_backend::prepare(const std::string& sql_str)    {
      StmH stm;
      ub4 count;
      if ( _stmt )   {
	::OCIHandleFree(_stmt, OCI_HTYPE_STMT);
	_stmt = nullptr;
      }
      result_t ret = ::OCIHandleAlloc(_env, (void**)&stm, OCI_HTYPE_STMT , 0, 0);
      if ( !oracle_ok(ret) )  {
	_error_code = ret;
	return sqldb::ERROR;
      }
#define CHECK(ret) if ( !oracle_ok(ret) )  {			\
	::OCIHandleFree(stm, OCI_HTYPE_STMT);			\
	_types.clear();						\
	_error_code = ret;					\
	return sqldb::ERROR;				}
	  
      ret = ::OCIStmtPrepare(stm, _error,
			     (OraText*)sql_str.c_str(), sql_str.length(),
			     OCI_NTV_SYNTAX, OCI_DEFAULT);
      CHECK(ret);
      ret = ::OCIAttrGet(stm, OCI_HTYPE_STMT, &count, nullptr,
			 OCI_ATTR_BIND_COUNT, _error);
      CHECK(ret);
      params.resize(count, column_data());
      ret = ::OCIAttrGet(stm, OCI_HTYPE_STMT, &count, nullptr,
			 OCI_ATTR_PARAM_COUNT, _error);
      CHECK(ret);
      _types.clear();
      for( std::size_t i=0; i<count; ++i )    {
	ub2 type;
	ParamH colhd { nullptr };
	ret = ::OCIParamGet(stm, OCI_HTYPE_STMT, _error, (void**)&colhd, i);
	CHECK(ret);
	ret = ::OCIAttrGet(colhd, OCI_DTYPE_PARAM, &type,
			   nullptr, OCI_ATTR_DATA_TYPE, _error);
	CHECK(ret);
	_types.push_back(type);
      }
      fields.resize(count, column_data());
      _sql_str = sql_str;
      _stmt = stm;
      stmt = this;
      return sqldb::OK;
    }

    ///
    int statement_backend::action(action_type type)   {
      switch(type)   {
      case RESET:        /// Reset all data to re-execute the statement with new parameters
	return action(EXEC);
      case FINALIZE:     /// Finalize and run-down the statement. Release all resources.
	if( _stmt )   {
	  ::OCIHandleFree(_stmt, OCI_HTYPE_STMT);
	  _stmt = nullptr;
	}
	return sqldb::OK;
      case EXEC:         /// Execute prepared statement
	if( _stmt )   {
	  long iters = 1;
	  _error_code = ::OCIStmtExecute(_dbc,_stmt,_error,iters,0,0,0,OCI_DEFAULT);
	  switch( _error_code )   {
	  case 995:  return sqldb::NO_DB_DATA;  // ORA-00955: name is already used by an existing object
	  case 1405: return sqldb::NO_DB_DATA;  // ORA-01405: fetched column value is NULL
	  default:   return oracle_ok(_error_code) ? sqldb::OK : sqldb::ERROR;
	  }
	}
	return sqldb::ERROR;
      case FETCH:        /// Fetch next result row. Stop if result is NON-zero
	return fetch_one();
      case ERRNO:        /// Access error number of last failed statement
	return _error_code;
      default:
	throw std::runtime_error("Invalid action request received!");
      }
    }

    /// Default constructor
    dbase_backend::dbase_backend()   {
      result_t rc;
      rc = OCIEnvCreate(&_env, OCI_OBJECT, 0, 0, 0, 0, 0, 0);
      if ( oracle_ok(rc) )   {
	rc = OCIHandleAlloc (_env, (void**)&_error, OCI_HTYPE_ERROR , 0, 0);
	if ( oracle_ok(rc) )  {
	  _error_code = OCI_SUCCESS;
	  return;
	}
      }
      _error_code = rc;
    }

    /// Default destructor
    dbase_backend::~dbase_backend()   {
      if ( _dbc )   {
	action(CLOSE);
      }
      ::OCIHandleFree(_error, OCI_HTYPE_ERROR);
      _error = nullptr;
    }

    /// Execute prepared statement
    sqldb_status dbase_backend::execute(std::string& error, const std::string& sql_str)  {
      char* err = nullptr;
      sqldb_status ret = sqldb::ERROR; //do_exec(handle(), sql_str.c_str(), &err);
      if ( !sql_str.empty() )   {
	if ( ret != sqldb::OK && err != nullptr ) error = err;
      }
      return ret;
    }

    /// Prepare a new statement and allocate resources.
    sqldb_status dbase_backend::prepare(statement& bs, const std::string& sql_str)  {
      ErrorH   err {nullptr};
      if ( bs.intern.get() )   {
	bs.intern.reset();
      }
      result_t rc = ::OCIHandleAlloc(_env, (void**)&err, OCI_HTYPE_ERROR, 0, 0);
      if ( oracle_ok(rc) )   {
	auto* stm = new statement_backend(_env, _dbc, err);
	bs.intern.reset(stm);
	return stm->prepare(sql_str);
      }
      _error_code = rc;
      return sqldb::ERROR;
    }

    /// Open database
    sqldb_status dbase_backend::open(const std::string& connect_string)   {
      auto args = connection_args().parse(connect_string);
      if ( _dbc )  {
	::OCILogoff(_dbc, _error);
	::OCIHandleFree(_dbc, OCI_HTYPE_SVCCTX);
      }
      DbcH mydb = nullptr;
      const std::string& pwd   = args["password"];
      const std::string& user  = args["user"];
      const std::string& dbase = args["database"];
      result_t rc = ::OCILogon(_env, _error, &mydb,
			       (OraText*)user.c_str(), user.length(),
			       (OraText*)pwd.c_str(),  pwd.length(),
			       (OraText*)dbase.c_str(),dbase.length());
      if ( !oracle_ok(rc) )   {
	_error_code = rc;
	return sqldb::ERROR;
      }
      _dbc = mydb;
      name = dbase;
      _error_code = OCI_SUCCESS;
      return sqldb::OK;
    }

    /// Perform multiple actions
    int dbase_backend::action(action_type type)   {
      result_t rc = OCI_SUCCESS;
      switch(type)    {
      case BEGIN:    /// Start transaction
	rc = _dbc ? ::OCITransStart(_dbc, _error, 60, OCI_TRANS_NEW) : OCI_ERROR;
	break;
      case COMMIT:   /// Commit transaction
	rc = _dbc ? ::OCITransCommit(_dbc, _error, OCI_DEFAULT) : OCI_ERROR;
	break;
      case ROLLBACK: /// Rollback transaction
	rc = _dbc ? ::OCITransRollback(_dbc, _error, OCI_DEFAULT) : OCI_ERROR;
	break;
      case CLOSE:    /// Close database access
	if ( _dbc )   {
	  ::OCILogoff(_dbc, _error);
	  ::OCIHandleFree(_dbc, OCI_HTYPE_SVCCTX);
	}
	_dbc = nullptr;
	_error_code = OCI_SUCCESS;
	return sqldb::OK;
      case ERRNO:    /// Access error number of last failed statement
	return _error_code;
      default:
	throw std::runtime_error("Invalid actoin request received!");
      }
      _error_code = rc;
      return sqldb::ERROR;	  
    }
  }
}

/// SQLDB namespace declaration
namespace sqldb  {
  
  /// Open the database connection using all information in passed string
  template <> std::pair<std::shared_ptr<database::backend>, sqldb_status>
  database::backend::open<oracle>(const std::string& connect_string)   {
    auto db = std::make_shared<oracle_imp::dbase_backend>();
    sqldb_status ret = db->open(connect_string);
    if ( ret == sqldb::OK )
      return make_pair(db, sqldb::OK);
    return make_pair(std::shared_ptr<database::backend>(), ret);
  }
} /// End namespace sqldb
