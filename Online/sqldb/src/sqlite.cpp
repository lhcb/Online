//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================

/// Framework include files
#include <sqldb/sqlite.h>

/// SQLite3 header
#include <sqlite3.h>

/// C/C++ headers
#include <stdexcept>

#define SQLDB_SQLITE_INLINE

/// SQLDB namespace declaration
namespace sqldb  {
 
  namespace sqlite_imp  {
    ///
    inline bool sqlite_ok(int status)    {
      return status == SQLITE_OK;
    }
    ///
    inline field_type gen_field_type(int typ)   {
      switch(typ)   {
      case SQLITE_INTEGER:   return TYPE_LONGLONG;
      case SQLITE_FLOAT:     return TYPE_DOUBLE;
      case SQLITE_BLOB:      return TYPE_STRING;
      case SQLITE3_TEXT:     return TYPE_BLOB;
      case SQLITE_NULL:      return TYPE_NULL;
      default:   break;
      }
      throw std::runtime_error("Invalid data type encountered!");
    }
    ///
    sqldb_status do_exec(sqlite3* h, const char* sql, char** err=0)   {
      if ( h )  {
	char* e = nullptr;
	if ( !sqlite_ok(::sqlite3_exec(h, sql, nullptr, nullptr, &e)) )   {
	  if ( err != nullptr ) *err = e;
	  return (sqldb_status)::sqlite3_extended_errcode(h);
	}
	return sqldb::OK;
      }
      return sqldb::INVALID_HANDLE;
    }

    ///  Technology abstraction layer
    /**
     *
     *  \author  M.Frank
     *  \version 1.0
     *  \date    02.05.2017
     */
    struct column_bind  {
    public:
      sqlite::statement_t* stmt   { nullptr };
      std::size_t          column { 0 };
    public:
      sqldb_status bind_null(column_data& d)   {
	d.length = 0;
	d.item.pointer = nullptr;
	return sqldb::OK;
      }
      /// Generic parameter binding
      sqldb_status bind_str(column_data& d, const char* ptr, std::size_t len)   {
	if ( ptr )   {
	  pointers_t p(ptr);
	  d.bind_pointer(p.puint8, len);
	  ::sqlite3_bind_text(this->stmt, this->column+1, p.pstring, len, SQLITE_STATIC);
	  return sqldb::OK;
	}
	return bind_null(d);
      }
      /// Generic parameter binding
      sqldb_status bind_blob(column_data& d, const unsigned char* ptr, std::size_t len)   {
	if ( ptr )   {
	  d.bind_pointer(ptr, len);
	  ::sqlite3_bind_blob(this->stmt, this->column+1, d.item.pointer, len, SQLITE_STATIC);
	  return sqldb::OK;
	}
	return bind_null(d);
      }
      /// Type specific parameter bind
      template <typename T> sqldb_status bind_int(column_data& d, T& to, const T* from)   {
	to       = *from;
	d.length = sizeof(int);
	::sqlite3_bind_int(this->stmt, this->column+1, int(to));
	return sqldb::OK;
      }
      /// Type specific parameter bind
      sqldb_status bind_int64(column_data& d, int64_t& to, const int64_t* from)   {
	to       = *from;
	d.length = sizeof(int64_t);
	::sqlite3_bind_int64(this->stmt, this->column+1, to);
	return sqldb::OK;
      }
      /// Type specific parameter bind
      template <typename T> sqldb_status bind_flt(column_data& d, T& to, const T* from)   {
	to       = *from;
	d.length = sizeof(double);
	::sqlite3_bind_double(this->stmt, this->column+1, double(to));
	return sqldb::OK;
      }
    };
  
    ///  Technology abstraction layer
    /**
     *
     *  \author  M.Frank
     *  \version 1.0
     *  \date    02.05.2017
     */
    struct sqlite_statement_backend : public statement::backend {
    public:
      sqlite::statement_t* _handle      { nullptr };

      /// Initializing constructor
      sqlite_statement_backend() = default;
      /// Access to the internal statement handle. Throws exception if the handle is invalid
      sqlite::statement_t* handle()   const   {
	if ( this->_handle ) return this->_handle;
	throw std::runtime_error("Failed to access statement handle. [Invalid handle]");
      }
      /// Access error message on failure
      virtual std::string errmsg()  override  {
	return ::sqlite3_errmsg(::sqlite3_db_handle(this->_handle));
      }
      /// Access the number of rows affected by the last statement
      long rows_affected()  override   {
	return ::sqlite3_changes(::sqlite3_db_handle(this->_handle));
      }
      /// Character string of the prepared statement
      const char* sql()  override  {
	return ::sqlite3_expanded_sql(this->_handle);
      }
      ///
      sqldb_status bind(std::size_t col, field_type typ, const void* data, std::size_t len)  override  {
	pointers_t   ptr(data);
	column_data& d = this->params[col];
	column_bind  b {this->_handle, col};

	switch(d.type=typ)   {
	case TYPE_TINY:         return b.bind_int(d,   d.item.int8,   ptr.pint8);
	case TYPE_SHORT:	return b.bind_int(d,   d.item.int16,  ptr.pint16);
	case TYPE_LONG:         return b.bind_int(d,   d.item.int32,  ptr.pint32);
	case TYPE_LONGLONG:	return b.bind_int64(d, d.item.int64,  ptr.pint64);
	case TYPE_FLOAT:	return b.bind_flt(d,   d.item.real32, ptr.preal32);
	case TYPE_DOUBLE:	return b.bind_flt(d,   d.item.real64, ptr.preal64);
	case TYPE_STRING:	return b.bind_str(d,   ptr.pstring,   len);
	case TYPE_BLOB:	        return b.bind_blob(d,  ptr.puint8,    len);
	case TYPE_TIME:	        return b.bind_int (d,  d.item.int64,  ptr.pint64);
	default:  return invalid_statement("Invalid SQLite parameter data type");
	}
	return sqldb::OK;
      }
      /// Fetch next result row. Stop if result is NON-zero
      sqldb_status fetch_one()   {
	if ( auto* h = this->_handle )   {
	  int ret = ::sqlite3_step(h);
	  if( ret == SQLITE_ROW )  { 
	    for( std::size_t col=0; col < this->fields.size(); col++ )   {
	      auto& d = this->fields[col];
	      int typ = ::sqlite3_column_type(h,col);
	      d.type = gen_field_type(typ);
	      switch(typ)   {
	      case SQLITE_INTEGER:
		d.item.int64  = ::sqlite3_column_int64(h, col);
		d.length = sizeof(int64_t);
		break;
	      case SQLITE_FLOAT:
		d.item.real64 = ::sqlite3_column_double(h, col);
		d.length = sizeof(double);
		break;
	      case SQLITE_NULL:
	      case SQLITE_BLOB:
	      case SQLITE3_TEXT:
		d.item.pointer = ::sqlite3_column_blob(h, col);
		d.length       = ::sqlite3_column_bytes(h, col);
		break;
	      default:
		break;
	      }
	    }
	    return sqldb::OK;
	  }
	  else if ( ret == SQLITE_DONE )   {
	    return sqldb::DONE;
	  }
	  return sqldb::ERROR;
	}
	return invalid_statement();
      }
      /// Prepare a new statement and allocate resources.
      sqldb_status prepare(sqlite::dbase_t* db, const std::string& sql_str)    {
	sqlite::statement_t* thestmt = nullptr;
	if ( auto* h = this->_handle )   {
	  ::sqlite3_reset(h);
	  ::sqlite3_finalize(h);
	  this->_handle = nullptr;
	}
	int ret = ::sqlite3_prepare_v2(db, sql_str.c_str(), sql_str.length()+1, &thestmt, nullptr);
	if ( sqlite_ok(ret) )   {
	  std::size_t num_param = ::sqlite3_bind_parameter_count(thestmt);
	  std::size_t num_field = ::sqlite3_column_count(thestmt);
	  this->params.resize(num_param, column_data());
	  this->fields.resize(num_field, column_data());
	  this->_handle = thestmt;
	  this->stmt = this;
	  return sqldb::OK;
	}
	return sqldb::ERROR;
      }
      int action(action_type type)   override   {
	switch(type)   {
	case RESET:        /// Reset all data to re-execute the statement with new parameters
	  if( auto* h = this->_handle )   {
	    if ( !sqlite_ok(::sqlite3_clear_bindings(h)) )
	      return sqldb::ERROR;
	    else if ( !sqlite_ok(::sqlite3_reset(h)) )
	      return sqldb::ERROR;
	  }
	  return sqldb::OK;
	case FINALIZE:     /// Finalize and run-down the statement. Release all resources.
	  if( auto* h = this->_handle )   {
	    ::sqlite3_reset(h);
	    ::sqlite3_finalize(h);
	    this->_handle = nullptr;
	  }
	  return sqldb::OK;
	case EXEC:         /// Execute prepared statement
	  if( auto* h = this->_handle )   {
	    const char* str = ::sqlite3_sql(this->_handle);
	    // On select we call fetch_one after execute. Do nothing
	    if ( 0 == ::strncasecmp(str, "SELECT", 6) )
	      return sqldb::OK;
	    // For all other statements, call 'step' to execute the statement
	    int ret = ::sqlite3_step(h);
	    if( ret == SQLITE_ROW )
	      return sqldb::OK;
	    else if ( ret == SQLITE_DONE )
	      return sqldb::DONE;
	    else if ( ret == SQLITE_READONLY )
	      return sqldb::NO_PERMISSION;
	  }
	  return sqldb::ERROR;
	case FETCH:        /// Fetch next result row. Stop if result is NON-zero
	  return this->fetch_one();
	case ERRNO:        /// Access error number of last failed statement
	  return ::sqlite3_extended_errcode(::sqlite3_db_handle(this->_handle));
	default:
	  throw std::runtime_error("Invalid action request received!");
	}
      }
    };

    ///  Technology abstraction layer for the database
    /**
     *
     *  \author  M.Frank
     *  \version 1.0
     *  \date    02.05.2017
     */
    struct sqlite_dbase_backend : public database::backend  {
      sqlite::dbase_t* _handle  {nullptr};

      /// Access to the internal database handle. Throws exception if the handle is invalid
      sqlite::dbase_t* handle()   const   {
	if ( _handle ) return _handle;
	throw std::runtime_error("Invalid MySQL database handle");
      }
      /// Access error message on failure
      virtual std::string errmsg()  override  {
	return ::sqlite3_errmsg(_handle);
      }
      /// Access the number of rows affected by the last statement
      virtual long rows_affected()  override  {
	return ::sqlite3_changes(this->_handle);
      }
      /// Execute prepared statement
      virtual sqldb_status execute(std::string& error, const std::string& sql_str)  override  {
	char*        err = nullptr;
	sqldb_status ret = do_exec(this->handle(), sql_str.c_str(), &err);
	if ( ret != sqldb::OK && err != nullptr ) error = err;
	return ret;
      }
      /// Prepare a new statement and allocate resources.
      sqldb_status prepare(statement& bs, const std::string& sql_str)  override  {
	auto* stm_imp = new sqlite_statement_backend();
	bs.intern.reset(stm_imp);
	return stm_imp->prepare(this->_handle, sql_str);
      }
      /// Open database
      sqldb_status open(const std::string& connect_string)   {
	this->_handle = nullptr;
	if ( !sqlite_ok(::sqlite3_open(connect_string.c_str(), &this->_handle)) )
	  return sqldb::ERROR;
	this->name = connect_string;
	return sqldb::OK;
      }
      /// Perform multiple actions
      virtual int action(action_type type)  override   {
	switch(type)    {
	case BEGIN:    /// Start transaction
	  return do_exec(this->handle(), "BEGIN TRANSACTION;");
	case COMMIT:   /// Commit transaction
	  return do_exec(this->handle(), "COMMIT TRANSACTION;");
	case ROLLBACK: /// Rollback transaction
	  return do_exec(this->handle(), "ROLLBACK TRANSACTION;");
	case CLOSE:    /// Close database access
	  if ( this->_handle )
	    ::sqlite3_close(this->_handle);
	  this->_handle = nullptr;
	  return sqldb::OK;
	case ERRNO:    /// Access error number of last failed statement
	  return ::sqlite3_errcode(_handle);	  
	default:
	  throw std::runtime_error("Invalid actoin request received!");
	}
      }
    };
  }
}

/// SQLDB namespace declaration
namespace sqldb  {

  /// Open the database connection using all information in passed string
  template <> std::pair<std::shared_ptr<database::backend>, sqldb_status>
  database::backend::open<sqlite>(const std::string& connect_string)   {
    typedef sqlite_imp::sqlite_dbase_backend _imp_t;
    auto db = std::make_shared<_imp_t>();
    sqldb_status ret = db->open(connect_string);
    if ( ret == sqldb::OK )
      return make_pair(std::move(db), sqldb::OK);
    return make_pair(std::shared_ptr<database::backend>(), ret);
  }
}       // End namespace sqldb
