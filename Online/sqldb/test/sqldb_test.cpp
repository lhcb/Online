//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
//  Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
//  All rights reserved.
//
//  For the licensing terms see OnlineSys/LICENSE.
//
//--------------------------------------------------------------------------
//
//  Package    : RPC
//
//  Author     : Markus Frank
//==========================================================================


/// Framework include files
#include <sqldb/sqldb.h>

/// C/C++ include files
#include <iostream>

using namespace std;

class mysql;
class sqlite;
template <typename T> void test_db()   {
  cout << "Hello World!" << endl;

  sqldb::database db;
  if ( db.open<T>("") != sqldb::OK )  {
  }
  sqldb::statement stmt = sqldb::statement::create(db, "");
  if ( !stmt.is_valid() )   {
  }
  stmt.bind(1, 'a');
  stmt.bind(2, short(1));
  stmt.bind(3, int(10));
  stmt.bind(4, long(100));
  stmt.bind(5, float(1000e0));
  stmt.bind(6, double(1000e0));
  stmt.bind(7, "Hello World!");

  if ( stmt.execute() == sqldb::OK )   {
    while( stmt.fetch_one() == sqldb::OK )  {
      int8_t  tiny  = stmt.get<int8_t>(1);
      int16_t small = stmt.get<int16_t>(2);
      int32_t word  = stmt.get<int32_t>(3);
      int64_t huge  = stmt.get<int64_t>(4);
      float   flt   = stmt.get<float>(5);
      double  dbl   = stmt.get<double>(6);
      if ( tiny == 0 )  {
      }
      if ( small == 0 )   {
      }
      if ( word == 0 )   {
      }
      if ( huge == 0 )   {
      }
      if ( flt == 0e0 )   {
      }
      if ( dbl == 0e0 )   {
      }
    }
    stmt.reset();
    stmt.finalize();
  }
}  

extern "C" int sqldb_run_tests(int, char**)   {
#ifdef HAVE_MYSQL
  test_db<mysql>();
#endif
#ifdef HAVE_SQLITE
  test_db<sqlite>();
#endif
  return 0;
}
