"""
     Minimal Gaudi task in the online environment

     @author M.Frank
"""
from __future__ import print_function
__version__ = "1.0"
__author__  = "Markus Frank <Markus.Frank@cern.ch>"

import os
import sys
import Configurables
import Gaudi.Configuration as Gaudi

import GaudiOnline
import OnlineEnvBase as OnlineEnv

from Configurables import Online__FlowManager as FlowManager
application = GaudiOnline.Passthrough(outputLevel=OnlineEnv.OutputLevel,
                                      partitionName=OnlineEnv.PartitionName,
                                      partitionID=OnlineEnv.PartitionID,
                                      classType=GaudiOnline.Class1)
application.setup_fifolog()
application.setup_mbm_access('Monitor', True)
application.setup_hive(FlowManager("EventLoop"), 40)
application.setup_monitoring()
algo = None
algo = Configurables.StoreExplorerAlg('Explorer')
algo.PrintFreq = 0.0000005
ev_size = Configurables.Online__EventSize('EventSize')
wr = application.setup_algorithms(writer=[algo,ev_size], acceptRate=1.0)
wr.RequireODIN = False
application.input.MakeRawEvent         = False
application.config.burstPrintCount     = 20000
application.monSvc.DimUpdateInterval   = 1
# Mode slection::  synch: 0 async_queued: 1 sync_queued: 2
application.config.execMode            = 1
application.config.numEventThreads     = 5
application.config.MBM_numConnections  = 2
application.config.MBM_numEventThreads = 2
#
application.config.MBM_requests = [
    'EvType=2;TriggerMask=0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF;VetoMask=0,0,0,0;MaskType=ANY;UserType=USER;Frequency=PERC;Perc=100.0',
    'EvType=1;TriggerMask=0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF,0xFFFFFFFF;VetoMask=0,0,0,0;MaskType=ANY;UserType=USER;Frequency=PERC;Perc=100.0'
]
#
print('Setup complete....')

