import pydim
import errno
import sys
import os

global num_threads
num_threads = 5

def service_callback(tag):
  global num_threads
  print('Serving: number of threads: %d'%(num_threads))
  return (num_threads,)

service = None
i = 0
while i < len(sys.argv):
  if sys.argv[i] == '-service':
    service = sys.argv[i+1]
  i = i + 1

if not service:
  print('No service name given: exit EINVAL')
  sys.exit(errno.EINVAL)

if not pydim.dis_get_dns_node():
  print("No Dim DNS node found. Please set the environment variable DIM_DNS_NODE")
  sys.exit(errno.EINVAL)

print('Starting service %s DIM_DNS_NODE: %s'%(service, os.getenv('DIM_DNS_NODE'),))
num_threads_svc = pydim.dis_add_service(service,"I",service_callback,0)

if not num_threads_svc:
  sys.stderr.write("An error occurred while registering the service\n")
  sys.exit(errno.EINVAL)

pydim.dis_update_service(num_threads_svc)

pydim.dis_start_serving(service)

while True:
  try:
    text = input("Enter number of threads: ")
    if not num_threads:
      continue
    num_threads = int(text)
    pydim.dis_update_service(num_threads_svc)

  except:
    print('Ending input loop due to exception.')
    break

