#===============================================================================
#  LHCb Online software suite
#-------------------------------------------------------------------------------
# Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
# All rights reserved.
#
# For the licensing terms see OnlineSys/LICENSE.
#
# Author     : M.Frank
#
#===============================================================================
build_parse_this()   {
    SOURCE=${1}
    # get param to "."
    thisroot=$(dirname ${SOURCE})
    build_script=`basename ${SOURCE}`;
    THIS=$(cd ${thisroot} > /dev/null;pwd); export THIS;
    unset SOURCE thisroot
}
#===============================================================================
# Configure project
#
# Author     : M.Frank
#
#===============================================================================
do_print()
{
    echo "======>  [${build_script}] $*";
}
do_echo()
{
    do_print $*;
    $*;
}
change_directory()
{
  dir="$*";
  cd "${dir}";
  if [ $? -ne  0 ]; then
    do_print "ERROR: Failed to change to directory: "${dir}".";
    do_print "ERROR: Did you miss out something ?";
    exit 1;
  fi;
}
#===============================================================================
# Print help
#
# Author     : M.Frank
#
#===============================================================================
usage()
{
    echo " do_make -opt [-opt]"
    echo " -t --type        <build_type>         Supply build type Debug0|Debug|Release. Default: Debug0 ";
    echo " -s --source      <source directory>   Source directory for cmake                              ";
    echo "                                       Default: `pwd`                                          ";
    echo "                                       CMakeLists.txt required!                                ";
    echo " -b --build-dir   <build  directory>   Build  directory for cmake                              ";
    echo "                                       Default: `pwd`/build_dataflow.<binary-tag>              ";
    echo " -i --install-dir <install directory>  Installation directory for cmake                        ";
    echo "                                       Default: `pwd`/install_dataflow.<binary-tag>            ";
    echo " -n --notest                           Do not build test environment                           ";
    echo " -v --view        <path to LCG view>   Path to the LCG view directory.                         ";
    echo " -T --tag         <tag-name>           Binary tag to be used. Current: '${binary_tag}'         ";
    echo "                                       ==> Must be an existing build tag of the LCG view!      ";
    echo " -c --cmake                            Execute only cmake step                                 ";
    echo " -B --build                            Only build: no cmake, no install                        ";
    echo " -I --install                          Only install: no cmake, no build                        ";
    echo " -p --parallel    <number>             Number of threads for parallel build (make -j <number>) ";
    echo " -e --ebonly                           Build event builder only                                ";
    echo "                                       changes defaults to:                                    ";
    echo "                                       build-dir:   `pwd`/build_ebonly.<binary-tag>            ";
    echo "                                       install-dir: `pwd`/install_ebonly.<binary-tag>          ";
    echo " -A --sharp                            Enables SHaRP for the EB (Experimental feature)         ";
    echo " -g --basic-gaudi                      build all not LHCb related Gaudi stuff as well          ";
    echo "                                       Requires LCG view!                                      ";
    echo " -S --static                           build static libraries and link statically if possible. ";
    echo " -V --verbose)                         Enable VERBOSE build                                    ";
    echo "                                                                                               ";
    echo "                                                                                               ";
    echo " ===> Example to use the LCG view from ** Gaudi ** (/cvmfs/lhcb.cern.ch):                      ";
    echo "  cd /group/online/dataflow/cmtuser/ONLINE/ONLINE_v7r25                                        ";
    echo "  . /cvmfs/sft.cern.ch/lcg/releases/gcc/13.1.0/x86_64-el9/setup.sh                             ";
    echo "  ./build_standalone.sh --type Debug0 --tag x86_64-el9-gcc13-dbg --view /cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_105a -c -B -I";
    echo "                                                                                               ";
    echo " ===> Example to use the LCG view from ** SFT ** (/cvmfs/sft.cern.ch):                         ";
    echo "  cd /group/online/dataflow/cmtuser/ONLINE/ONLINE_v7r25                                        ";
    echo "  . /cvmfs/sft.cern.ch/lcg/releases/gcc/13.1.0/x86_64-el9/setup.sh                             ";
    echo "  ./build_standalone.sh --type Debug0 --tag x86_64-el9-gcc13-dbg --view /cvmfs/sft.cern.ch/lcg/releases/LCG_105a -c -B -I";
    echo "                                                                                               ";
    exit 1;
}
#===============================================================================
# Parse command line options
#
# Author     : M.Frank
#
#===============================================================================
parse_arguments()   {
  #
  PARSED_ARGUMENTS=$(getopt -a -n build_standalone -o hcBIneAgSVT:t:s:b:i:v:p: --longoptions help,cmake,build,install,notest,ebonly,sharp,basic-gaudi,static,verbose,tag:,type:,source:,build-dir:,install-dir:,view:,parallel: -- "[build_dataflow]" $*)
  VALID_ARGUMENTS=$?
  if [ "$VALID_ARGUMENTS" != "0" ]; then
    do_print "Invalid arguments to cmake step";
    usage;
  fi;
  #
  #
  eval set -- "$PARSED_ARGUMENTS";
  while :
  do
    case "${1}" in
	-t | --type)          build_type=${2};            shift 2 ;;
	-s | --source)        source_dir=${2};            shift 2 ;;
	-b | --build-dir)     build_dir=${2};             shift 2 ;;
	-i | --install-dir)   install_dir=${2};           shift 2 ;;
	-v | --view)          lcg_view=${2};              shift 2 ;;
	-T | --tag)           binary_tag=${2};            shift 2 ;;
	-c | --cmake)         exec_cmake=YES;             shift ;;
	-g | --basic-gaudi)   basic_gaudi=ON;             shift ;;
	-B | --build)         exec_build=YES;             shift ;;
	-I | --install)       exec_install=YES;           shift ;;
	-n | --notest)        install_tests=OFF;          shift ;;
	-p | --parallel)      parallel="-j ${2}";         shift 2 ;;
        -e | --ebonly)        eb_only=ON;                 shift ;;
        -A | --sharp)         sharp=ON;                   shift ;;
        -S | --static)        build_static=ON;            shift ;;
        -V | --verbose)       build_verbose="VERBOSE=1";  shift ;;
	-h | --help)    usage;                            shift ;;
	# -- means the end of the arguments; drop this, and break out of the while loop
	--) shift;  break;;
	# If invalid options were passed, then getopt should have reported an error,
	# which we checked as VALID_ARGUMENTS when getopt was called...
	*) do_print " Unexpected option: $1 - this should not happen."
            usage; shift; break;;
    esac;
  done;
}
#===============================================================================
#
#===============================================================================
build_parse_this $0;
if [ ! -f ${THIS}/build_standalone.sh ]; then
  do_print "Wrong working directory! [${THIS}]";
  exit 1;
fi;
#
#
do_print "++ PARSED_ARGUMENTS: $PARSED_ARGUMENTS";
export RDKAFKA_DIR=/group/online/dataflow/cmtuser/libraries/kafka/rdkafka-gcc-11.1.0-opt;
lcg_view_default=;   ##/cvmfs/sft.cern.ch/lcg/views/LCG_101/${binary_tag_default};
binary_tag_default=; ## x86_64-centos7-gcc11-dbg;
build_type=;
build_dir=;
build_verbose=;
source_dir=;
install_dir=;
lcg_view=${lcg_view_default};
binary_tag=;
exec_cmake=;
exec_build=;
exec_install=;
eb_only=OFF;
sharp=OFF;
parallel="-j 12";
build_static=;
basic_gaudi=OFF;
install_tests=ON;
#
parse_arguments $*;
#
# OK. Now check pre-conditions
#
if test -z "${binary_tag}"; then
    binary_tag=${binary_tag_default};
fi;
#
if test -z "${source_dir}"; then
    source_dir=`pwd`;
fi;
#
if test -z "${build_type}"; then
    if test -n "`echo ${binary_tag} | grep dbg`"; then
      build_type=Debug0;
    elif test -n "`echo ${binary_tag} | grep do0`"; then
      build_type=Debug0;
    else
      build_type=Release;
    fi;
    do_print "Set CMAKE build type to: ${build_type}";
fi;
#
# -------------------------------------------------------------------
# Set the correct view directory containing also the binary tag:
# -------------------------------------------------------------------
lcg_view_version="`basename ${lcg_view}`";
#
# -------------------------------------------------------------------
# OK. Now check pre-conditions
# -------------------------------------------------------------------
if [ -z "${binary_tag}" ]; then
    # This should never happen: if no binary tag, no LCG view!
    do_print "No valid binary tag supplied.";
    exit 1;
fi;
#
if [ ! -f ${source_dir}/CMakeLists.txt ]; then
    do_print "Invalid source directory: ${source_dir}";
    do_print "   No CMakeLists.txt file present.";
    exit 1;
fi;

if test -z "${exec_cmake}"; then
    echo "" > /dev/null;
elif test -z "${lcg_view}"; then
    do_print "Missing argument --view [path to LCG view]";
    usage;
elif [ ! -d ${lcg_view} ]; then
    do_print "LCG View path is wrong: no directory found.";
    usage;
fi;

#
if test "${build_static}" = "ON"; then
    build_static="-DBUILD_SHARED_LIBS=OFF";
fi;
#
build_prefix=dataflow;
if test "${eb_only}" = "ON"; then
    build_prefix=ebonly;
fi;
#
if test -z "${build_dir}"; then
    build_dir=${source_dir}/build.${build_prefix}.${lcg_view_version}.${binary_tag};
    build_dir=${source_dir}/build.${build_prefix}.${binary_tag};
fi;
#
if test -z "${install_dir}"; then
    install_dir=${source_dir}/install.${build_prefix}.${lcg_view_version}.${binary_tag};
    install_dir=${source_dir}/install.${build_prefix}.${binary_tag};
fi;
#
lcg_view=`realpath ${lcg_view}`;
build_dir=`realpath ${build_dir}`;
source_dir=`realpath ${source_dir}`;
install_dir=`realpath ${install_dir}`;
#
do_print "Using LCG view from ${lcg_view} Binary tag: ${binary_tag}  cmake: `which cmake`";
#
# -------------------------------------------------------------------
#  Prepare the setup:
# -------------------------------------------------------------------
#  1) using already existing build directory:
# -------------------------------------------------------------------
if test -f ${build_dir}/bin/thisonline.sh -a -z "${exec_cmake}"; then
    lcg_view="${lcg_view}/${binary_tag}";
    do_print "++";
    do_print "Using existing build: ${build_dir}";
    . ${build_dir}/bin/thisonline.sh;
#
elif [ -f "${lcg_view}/${binary_tag}/setup.sh" ]; then
    lcg_view="${lcg_view}/${binary_tag}";
    do_print "++";
    lcg_platform=${binary_tag};
    . ${lcg_view}/setup.sh;
    export LD_LIBRARY_PATH=${lcg_view}/lib64:${lcg_view}/lib;
    CMAKE_OPTS="-DONLINE_ROOT_SYS=${ROOTSYS} -DPLUGIN_SOURCES=${lcg_view}";
    COMP=`which c++`;
    DIR=`dirname ${COMP}`;
    DIR=`dirname ${DIR}`;
    . ${DIR}/setup.sh;
    export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:/usr/lib64/pkgconfig;
    do_print "Build from using LCG view: ${lcg_view}  platform: ${lcg_platform}";
    do_print "cmake:    `which cmake`";
    do_print "compiler: ${COMP} `which gcc` `which g++` `which c++`";
    do_print "compiler: `ls -laF ${DIR}/setup.sh`";
#
elif [ -d "${lcg_view}" ]; then
    lcg_platform=`echo "${binary_tag}" | tr "-" " " | awk '{ print $1"-"$2 }'`;
    lcg_comp=`echo "${binary_tag}" | tr "-" " " | awk '{ print $3 }'`;
    lcg_dbg=`echo "${binary_tag}" | tr "-" " " | awk '{ print $4 }'`;
    do_print "++  LCG view from Gaudi build: `which cmake`";
    do_print "Using LCG view from ${lcg_view} Binary tag: ${binary_tag}";
    do_print "cmake: `which cmake` platform: ${lcg_platform} compiler: ${lcg_comp}";
    #echo . ${lcg_view}/${lcg_comp}/11.1.0/${lcg_platform}/setup.sh
    #. ${lcg_view}/gcc/11.1.0/${lcg_platform}/setup.sh
    CMAKE_OPTS="-DLCG_BINARY_TAG=${binary_tag} -DPLUGIN_SOURCES=/cvmfs/lhcb.cern.ch/lib/lhcb/GAUDI/GAUDI_v36r11/GaudiPluginService";
#
else
    do_print "Do you really know what you want ?";
    usage;
fi;
#
#
#
do_print "===============================================================";
do_print "Configure cmake for building ${build_type} ";
do_print "                  Source directory:  ${source_dir} ";
do_print "                  Build directory:   ${build_dir} ";
do_print "                  Install directory: ${install_dir} ";
do_print "                  LCG view:          ${lcg_view} ";
do_print "                  Current directory: `pwd`";
do_print "===============================================================";
#
#
# A bit a queer logic, but I do not know how to do better:
#
if [ -z "${exec_cmake}" ]; then
    do_print "++ No cmake step required --> Skipped.";
else
  mkdir -p ${build_dir}/.plugins;
  change_directory ${build_dir};
  do_echo cmake -DCMAKE_INSTALL_PREFIX="${install_dir}" \
          -DCMAKE_BUILD_TYPE=${build_type}              \
	        ${build_static}                               \
          -DONLINE_ENABLE_TESTS=${install_tests}        \
          -DONLINE_BASIC_GAUDI=${basic_gaudi}           \
          -DSUPPORT_GAUDI=OFF ${CMAKE_OPTS}             \
          -DLCG_VIEW=${lcg_view}                        \
          -DEVENTBUILDER_ONLY=${eb_only}                \
		      -DUSE_SHARP=${sharp}                          \
        ${source_dir} | tee cmake_step.log;
  if [ $? -ne  0 ]; then
      do_print "ERROR: Failed cmake step!";
      if test -n "echo `which c++ | grep /usr`"; then
	  do_print "Are you sure you have the proper compiler setup ?";
      fi;
      exit 1;
  fi;
  do_print "CMake step finished successfully for directory: ${build_dir}.";
fi;
#
if test -n "${exec_build}"; then
  change_directory ${build_dir};
  do_print "Source Online setup: ${build_dir}/bin/thisonline.sh";
  . "${build_dir}/bin/thisonline.sh";
  do_echo cmake --build . ${parallel} ${build_verbose};
  if [ $? -ne  0 ]; then
      do_print "ERROR: Failed make step!";
      if test -n "echo `which c++ | grep /usr`"; then
	  do_print "Are you sure you have the proper compiler setup ?";
      fi;
      exit 1;
  fi;
  do_print "Build step finished successfully for directory: ${build_dir}.";
fi;
#
if test -n "${exec_install}"; then
  mkdir -p ${install_dir};
  if [ $? -ne  0 ]; then
    do_print "ERROR: Failed to create the install directory: ${install_dir}.";
    exit 1;
  fi;
  change_directory ${build_dir};
  if [ ! -f ${build_dir}/bin/thisonline.sh ]; then
    do_print "ERROR: cmake step was apparently not successful --> redo ?";
    exit 1;
  fi;
  do_print "PATH: $PATH";
  do_print "Executing build/installation step...";
  source ${build_dir}/bin/thisonline.sh;
  do_print "PATH: $PATH";
  do_echo cmake --install .;
  if [ $? -ne  0 ]; then
      do_print "ERROR: Failed install step!";
      exit 1;
  fi;
  do_print "Installation step finished successfully for directory: ${build_dir}.";
fi;
