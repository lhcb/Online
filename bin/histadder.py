#===============================================================================
#  LHCb Online software suite
#-------------------------------------------------------------------------------
# Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
# All rights reserved.
#
# For the licensing terms see OnlineSys/LICENSE.
#
# Author     : M.Frank
#
#===============================================================================
#
import os
import os.path
#
TODO_DIRECTORY='/hist/Savesets/ByRun/HLT2/ToMerge'
TEMP_DIRECTORY='/hist/Savesets/ByRun/HLT2/Temporary'
class hlt2_crash_recover:

  def __init__(self):
    pass

  def reschedule_init(self, run):
    files = os.listdir(TEMP_DIRECTORY)
    run_files = []
    for f in files:
      if f.find(str(run)) >= 0:
        run_files.append(f)
    runno = int(run)
    fdir = TODO_DIRECTORY + os.sep + str(int(runno/10000)*10000)
    if not os.path.exists(fdir):
      print(f'+++ Directory {fdir} does not exist. Creating.....')
      os.mkdir(fdir)
    fdir = fdir os.sep + str(int(runno/1000)*1000)
    if not os.path.exists(fdir):
      print(f'+++ Directory {fdir} does not exist. Creating.....')
      os.mkdir(fdir)
    fdir = fdir + os.sep + str(run)
    if not os.path.exists(fdir):
      print(f'+++ Directory {fdir} does not exist. Creating.....')
      os.mkdir(fdir)
    return (fdir, run_files)

  def reschedule1(self, run):
    fdir, run_files = self.reschedule_init(run)
    for f in run_files:
      items = f.split('_')
      fn = fdir + os.sep + f[len(items[0])+1:]
      print(f' {f}  ->  {fn}')
      os.rename(TEMP_DIRECTORY + os.sep + f, fn)

  def reschedule2(self, run):
    import time
    fdir, run_files = self.reschedule_init(run)
    count = 0
    for f in run_files:
      count = count + 1
      tim = time.strftime('%Y%m%dT%H%M%S')
      fn = fdir + os.sep + 'LHCb_HLT9%04d_HLT2_0-%6d-%s-EOR.root'%(count,int(run),tim,)
      print(f' {f}  ->  {fn}')
      os.rename(TEMP_DIRECTORY + os.sep + f, fn)


