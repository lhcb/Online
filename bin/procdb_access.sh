#!/bin/bash
#===============================================================================
#  LHCb Online software suite
#-------------------------------------------------------------------------------
# Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
# All rights reserved.
#
# For the licensing terms see OnlineSys/LICENSE.
#
# Author     : M.Frank
#
#===============================================================================
#
#
TYPE=$1;
shift;
ARGS="$*";
#
if test -z "${TEST}"; then
    echo "Invalid argument TYPE (1rst argument)";
fi;
#
export RELEASE_DIR=/group/online/dataflow/cmtuser/ONLINE/ONLINE_v7r21;
. /group/online/dataflow/scripts/preamble.sh;
echo "+++ Using procdb cli from `which gentest`";
procdb_file=/group/online/dataflow/cmtuser/data/hist_adder_${TYPE}.sqlite;
echo gentest libHistAdder.so processing_db_cli -F ${procdb_file} `echo $ARGS`;
gentest libHistAdder.so processing_db_cli -F ${procdb_file} `echo $ARGS`;
