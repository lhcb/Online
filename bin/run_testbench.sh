#!/bin/bash
#===============================================================================
#  LHCb Online software suite
#-------------------------------------------------------------------------------
# Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
# All rights reserved.
#
# For the licensing terms see OnlineSys/LICENSE.
#
# Author     : M.Frank
#
#===============================================================================
#
export ONLINE_DIRECTORY=/group/online/dataflow/cmtuser/ONLINE/ONLINE_v7r21;
export BINARY_TAG=x86_64_v2-el9-gcc13-do0;
#
cd ${ONLINE_DIRECTORY};
source setup.${BINARY_TAG}.vars;
cd ${ONLINE_DIRECTORY}/build.${BINARY_TAG}/Online/GaudiOnlineTests/qmtest_tmp;
#
python ${ONLINE_DIRECTORY}/Online/Testing/testbench/testbench.py \
    --architecture=${GAUDIONLINETESTSROOT}/tests/architectures/gaucho/counters_serialize_basic.xml \
    --runinfo=${ONLINE_DIRECTORY}/Online/Testing/testbench/online_env.py \
    --scenario=${ONLINE_DIRECTORY}/Online/Testing/testbench/dataflow_scenario.py \
    --runtime=${ONLINETESTBENCH}/online_runtime.py \
    --working-dir=gaucho_counters_serialize_basic  \
    --producer=GauchoExtractionProcess

