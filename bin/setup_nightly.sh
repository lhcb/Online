#!/bin/bash
#===============================================================================
#  LHCb Online software suite
#-------------------------------------------------------------------------------
# Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
# All rights reserved.
#
# For the licensing terms see OnlineSys/LICENSE.
#
# Author     : M.Frank
#
#===============================================================================
#
#
#
# =============================================================================
# Setup build configuration to use nightly build
#
#
# =============================================================================
export CMTCONFIG=x86_64_v2-el9-gcc13-dbg;
export CMTCONFIG=x86_64_v2-el9-gcc13-opt;
export CMTCONFIG=x86_64_v2-el9-clang16-opt;
if test -n "${1}"; then
    export CMTCONFIG="${1}";
    echo "Compiling for: $CMTCONFIG";
fi;
#
# =============================================================================
#
export BINARY_TAG=${CMTCONFIG};
#
NIGHTLY=/cvmfs/lhcbdev.cern.ch/nightlies/lhcb-gaudi-head/latest;
. ${NIGHTLY}/setupSearchPath.sh;
lb-set-platform ${CMTCONFIG};
#
#
export LHCb_DIR=${NIGHTLY}/LHCb/InstallArea/${CMTCONFIG};
export Gaudi_DIR=${NIGHTLY}/Gaudi/InstallArea/${CMTCONFIG};
export Detector_DIR=${NIGHTLY}/Detector/InstallArea/${CMTCONFIG};
#
# =============================================================================
#
#
make configure LCG_VERSION=104
if [ $? -ne  0 ]; then
    echo "ERROR: Failed to configure ${CMTCONFIG}.";
    exit 1;
fi;
#
#
make install LCG_VERSION=104 -j 12
if [ $? -ne  0 ]; then
    echo "ERROR: Failed to build ${CMTCONFIG}.";
    exit 1;
fi;
#
#
cd build.${CMTCONFIG};
if [ $? -ne  0 ]; then
    echo "ERROR: Failed to change directory to build.${CMTCONFIG}.";
    exit 1;
fi;
ctest -j 255;
# =============================================================================
