#!/bin/bash
#===============================================================================
#  LHCb Online software suite
#-------------------------------------------------------------------------------
# Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
# All rights reserved.
#
# For the licensing terms see OnlineSys/LICENSE.
#
# Author     : M.Frank
#
#===============================================================================
#
#
#
#===============================================================================
# Utilities to access process properties using the UTGID
# provided task runs with "exec -a ${UTGID} ..."
#===============================================================================
get_process_pid()
{
    pid=`ps -ef | grep " $1 " | grep -v grep | awk '{print \$2}'`;
    echo ${pid};
}
#
#===============================================================================
# Attach debugger to process by UTGID
attachdbg()
{
    ps -ef | grep " $1 " | grep -v grep;
    pid=`get_process_pid $1`;
    if test -z "${pid}"; then
	echo "NON existent process. Debugging failed";
    else
	echo "gdb --pid ${pid}";
	sudo  gdb --pid ${pid};
    fi;
}
#
#===============================================================================
# See mapped memory sections by UTGID
maps()
{
    ps -ef | grep " $1 " | grep -v grep;
    pid=`get_process_pid $1`;
    if test -z "${pid}"; then
	echo "NON existent process. Debugging failed";
    else
	echo "cat /proc/${pid}/maps";
	sudo  cat /proc/${pid}/maps  | less;
    fi;
}
#
#===============================================================================
# See open file descriptors by UTGID
files()
{
    ps -ef | grep " $1 " | grep -v grep;
    pid=`get_process_pid $1`;
    if test -z "${pid}"; then
	echo "NON existent process. Debugging failed";
    else
	echo "dire    /proc/${pid}/fd";
	sudo  ls -laF /proc/${pid}/fd;
    fi;
}
#
#===============================================================================
# Dump the process environment by UTGID
environ()
{
    ps -ef | grep " $1 " | grep -v grep;
    pid=`get_process_pid $1`;
    if test -z "${pid}"; then
	echo "NON existent process. Debugging failed";
    else
	echo "cat    /proc/${pid}/environ";
	sudo cat /proc/${pid}/environ | tr "\0" "\n";
    fi;
}
#
#===============================================================================
# Kill process using UTGID
killproc()
{
    ps -ef | grep " $1 " | grep -v grep;
    pid=`get_process_pid $1`;
    if test -z "${pid}"; then
	echo "NON existent process. Killing failed";
    else
	echo "kill -s 9 ${pid}";
	sudo kill -s 9 ${pid};
    fi;
}
#===============================================================================
# Process start time using UTGID
start_time()
{
    pid=`get_process_pid $1`;
    if test -z "${pid}"; then
	echo "NON existent process. Process inquiry failed";
    else
        ps -p ${pid} -o pid,lstart,start,etimes,etime,cmd;
    fi;
}
#===============================================================================
# List processes connected to a given partition name or node name
match_proc()
{
    if test -z "${1}"; then
	echo "NO partitio name supplied. Process inquiry failed";
    else
	ps -ef | grep "${1}";
    fi;    
}
#
#===============================================================================
# Start statistical profiler perf using UTGID
profile()
{
    task="${1}";
    shift;
    ps -ef | grep " ${task} " | grep -v grep;
    pid=`get_process_pid ${task}`;
    if test -z "${pid}"; then
	echo "NON existent process. Profiling failed";
    else
	CMD="perf record -o /tmp/${task}.data --event=cycles:u --aio --call-graph=dwarf,65528 --count=20000 --pid=${pid} $*";
	sudo rm -f /tmp/${task}.data;
	echo "${CMD}";
	sudo ${CMD};
	chmod 0666 /tmp/${task}.data;
    fi;
}
#
#===============================================================================
# Show profiler output identifying perf file using UTGID
prof_show()  {
    task="${1}";
    sudo perf report -i /tmp/${task}.data;
}
#
#===============================================================================
# Utilities to access the processing database of the HLT2 adder
#===============================================================================
procdb_hlt2()  {
    /group/online/dataflow/cmtuser/OnlineRelease/bin/procdb_access.sh HLT2 $*;
}
histadder()  {
    PYTHONPATH=/group/online/dataflow/cmtuser/OnlineRelease/bin:${PYTHONPATH} python3 -c "import histadder; histadder.hlt2_crash_recover().$*";
}

procdb_rerun()  {
    DIR=/hist/Savesets/ByRun/HLT2;
    RUN=${1};
    if test -n "${RUN}"; then
	RUN1000=$(((${RUN}/1000)*1000));
	RUN100000=$(((${RUN}/100000)*100000));
	TARGET=${DIR}/ToMerge/${RUN100000}/${RUN1000}/${RUN};
	if test -d ${DIR}/${RUN100000}; then
	    mkdir -p ${TARGET};
	    RESULT=$?;
	    if [ $RESULT != 0 ]; then
		echo "+++ Failed to create directory: ${TARGET}";
	    else
		cd ${DIR}/Temporary;
		mv *${RUN}*.root ${TARGET};
		RESULT=$?;
		if [ $RESULT != 0 ]; then
		    echo "+++ FAILED to move files *${RUN}*.root to directory: ${TARGET}";
		else
		    echo "+++ Moved files *${RUN}*.root to directory: ${TARGET}";
		    /group/online/dataflow/cmtuser/OnlineRelease/bin/procdb_access.sh HLT2 -r ${RUN} -S ENDED -m;
		    RESULT=$?;
		    echo "+++ Updated procdb entry for run ${RUN}. Result: ${RESULT}";
		fi;
	    fi;
	else
	    echo "+++ Invalid run number: ${RUN}";
	fi;
	cd -;
    else
	echo "+++ No run number given!";
    fi;
}
