##############################################################################
# cmake configuration file for @CMAKE_PROJECT_NAME@
#
# returns following variables:
#
#   @CMAKE_PROJECT_NAME@_FOUND      : set to TRUE if @CMAKE_PROJECT_NAME@ found
#   @CMAKE_PROJECT_NAME@_VERSION    : package version
#   @CMAKE_PROJECT_NAME@_ROOT       : path to this @CMAKE_PROJECT_NAME@ installation
#   @CMAKE_PROJECT_NAME@_LIBRARIES  : list of @CMAKE_PROJECT_NAME@ libraries
#   @CMAKE_PROJECT_NAME@_INCLUDE_DIRS  : list of paths to be used with INCLUDE_DIRECTORIES
#   @CMAKE_PROJECT_NAME@_LIBRARY_DIRS  : list of paths to be used with LINK_DIRECTORIES
#
# @author Jan Engels, Desy
##############################################################################
#
# Locate the @CMAKE_PROJECT_NAME@ install prefix. This CMake file is installed in two
#
get_filename_component(_thisdir "${CMAKE_CURRENT_LIST_FILE}" PATH)
get_filename_component(_lastdir "${_thisdir}" NAME)
if (_lastdir STREQUAL "cmake")
  get_filename_component(_thisdir "${_thisdir}" PATH)
endif()

set ( @CMAKE_PROJECT_NAME@_DIR                "${_thisdir}" )
set ( @CMAKE_PROJECT_NAME@_ROOT               "${_thisdir}" )
set ( @CMAKE_PROJECT_NAME@_VERSION            "@Online_VERSION@" )
set ( @CMAKE_PROJECT_NAME@_SOVERSION          "@Online_SOVERSION@" )
set ( @CMAKE_PROJECT_NAME@_BUILD_CXX_STANDARD  @CMAKE_CXX_STANDARD@ )
set ( ONLINE_COMPILER_LOCATION  "@ONLINE_COMPILER_LOCATION@")

include(OnlineBuild.cmake)
include(CMakeFindDependencyMacro)

set (CMAKE_MODULE_PATH  ${CMAKE_MODULE_PATH} ${@CMAKE_PROJECT_NAME@_DIR}/cmake  )

# ---------- include dirs -----------------------------------------------------
set(@CMAKE_PROJECT_NAME@_INCLUDE_DIRS "")

FIND_DEPENDENCY(Boost REQUIRED)

if(NOT CMAKE_PROJECT_NAME STREQUAL @CMAKE_PROJECT_NAME@)
  include("${@CMAKE_PROJECT_NAME@_ROOT}/cmake/@CMAKE_PROJECT_NAME@Config-targets.cmake")
endif()

# Backward compatible @CMAKE_PROJECT_NAME@_COMPONENT_LIBRARIES variable
SET(@CMAKE_PROJECT_NAME@_COMPONENT_LIBRARIES)
IF(@CMAKE_PROJECT_NAME@_FIND_COMPONENTS)
  MESSAGE(STATUS "|++> @CMAKE_PROJECT_NAME@: Looking for Components: ${@CMAKE_PROJECT_NAME@_FIND_COMPONENTS}" )
  FOREACH(comp ${@CMAKE_PROJECT_NAME@_FIND_COMPONENTS})
    IF(NOT TARGET Online::${comp})
      MESSAGE(FATAL_ERROR "|++> Did not find required component: ${comp}")
    ENDIF()
    LIST(APPEND @CMAKE_PROJECT_NAME@_COMPONENT_LIBRARIES Online::${comp})
  ENDFOREACH()
ENDIF()

# ---------- final checking ---------------------------------------------------
INCLUDE( FindPackageHandleStandardArgs )
# set ONLINE_FOUND to TRUE if all listed variables are TRUE and not empty
FIND_PACKAGE_HANDLE_STANDARD_ARGS( @CMAKE_PROJECT_NAME@ DEFAULT_MSG @CMAKE_PROJECT_NAME@_DIR @CMAKE_PROJECT_NAME@_INCLUDE_DIRS @CMAKE_PROJECT_NAME@_LIBRARIES )

SET( @CMAKE_PROJECT_NAME@_FOUND ${@CMAKE_PROJECT_NAME@_FOUND} )
