# =======================================================================
#
# Online cmake configuration
#
#
#
# =======================================================================
#
set(Online_SOVERSION ${Online_VERSION})
#
#
set(ONLINE_INSTALL_LIBDIR     "lib"     CACHE STRING "Install python packages in <prefix>/\${ONLINE_INSTALL_LIBDIR}" FORCE)
set(ONLINE_INSTALL_BINDIR     "bin"     CACHE STRING "Install python packages in <prefix>/\${ONLINE_INSTALL_BINDIR}" FORCE)
set(ONLINE_INSTALL_INCLUDEDIR "include" CACHE STRING "Install python packages in <prefix>/\${ONLINE_INSTALL_INCLUDEDIR}" FORCE)
set(ONLINE_INSTALL_PYTHONDIR  "python"  CACHE STRING "Install python packages in <prefix>/\${ONLINE_INSTALL_PYTHONDIR}" FORCE)
set(ONLINE_INSTALL_SCRIPTDIR  "scripts" CACHE STRING "Install scripts in <prefix>/\${ONLINE_INSTALL_SCRIPTDIR}" FORCE)
#
# -- Public dependencies
find_package(Boost         REQUIRED headers regex system filesystem iostreams program_options date_time thread)
find_package(Python        REQUIRED Interpreter Development)
find_package(ROOT          REQUIRED Core Hist Gui Rint GenVector)
find_package(AIDA          REQUIRED)
find_package(X11           REQUIRED Xt)
find_package(Motif         2.0 REQUIRED)
find_package(Threads       REQUIRED)
find_package(TBB           REQUIRED)

find_package(SQLite3       REQUIRED)
find_package(Oracle        REQUIRED)

find_package(PkgConfig     REQUIRED)
find_package(nlohmann_json REQUIRED)

pkg_check_modules(libzmq libzmq REQUIRED IMPORTED_TARGET)
pkg_check_modules(libzmq libzmq REQUIRED IMPORTED_TARGET)
pkg_check_modules(libsodium libsodium IMPORTED_TARGET) # optional
pkg_check_modules(zlib zlib REQUIRED IMPORTED_TARGET)

include(PCIE40)
find_package(MPI COMPONENTS CXX)

#
set(ONLINE_BUILD_DATE_TIME)
#
#
# =======================================================================
# Return the date (yyyy-mm-dd hh:mm:ss)
macro(online_build_date_time RESULT)
  execute_process(COMMAND "date" "+%Y-%m-%d %H:%M:%S" OUTPUT_VARIABLE ${RESULT})
  string(REGEX REPLACE "\n" "" ${RESULT} ${${RESULT}})
endmacro()
#
#
# =======================================================================
online_build_date_time(ONLINE_BUILD_DATE_TIME)
#
#
# =======================================================================
macro(online_print_setup)
  message(STATUS "++ Build ${PROJECT_NAME}:    ${ONLINE_BUILD_DATE_TIME}")
  message(STATUS "++ ONLINE_INSTALL_LIBDIR     ${ONLINE_INSTALL_LIBDIR}")
  message(STATUS "++ ONLINE_INSTALL_BINDIR     ${ONLINE_INSTALL_BINDIR}")
  message(STATUS "++ ONLINE_INSTALL_INCLUDEDIR ${ONLINE_INSTALL_INCLUDEDIR}")
  message(STATUS "++ ONLINE_INSTALL_PYTHONDIR  ${ONLINE_INSTALL_PYTHONDIR}")
  message(STATUS "++ ONLINE_INSTALL_SCRIPTDIR  ${ONLINE_INSTALL_SCRIPTDIR}")
endmacro(online_print_setup)
#
# =======================================================================
# -- Private dependencies
if(WITH_Online_PRIVATE_DEPENDENCIES)
    find_package(PostgreSQL) # optional
    if ( SUPPORT_GAUDI )
      find_package(cppgsl REQUIRED)
    endif()

    # Compression libraries
    pkg_check_modules(zstd libzstd IMPORTED_TARGET)
    pkg_check_modules(lzma liblzma IMPORTED_TARGET)
    pkg_check_modules(lz4 liblz4 IMPORTED_TARGET)

    if(NOT TARGET PkgConfig::numa)
      set(PKG_CONFIG_USE_CMAKE_PREFIX_PATH ON)
      list(APPEND CMAKE_PREFIX_PATH /usr)
      pkg_check_modules(numa numa IMPORTED_TARGET)
      list(REMOVE_ITEM CMAKE_PREFIX_PATH /usr)
    endif()

    if(NOT TARGET PkgConfig::mysqlclient)
        # we first try MySQL
        pkg_check_modules(mysqlclient mysqlclient IMPORTED_TARGET) # optional
        if(NOT TARGET PkgConfig::mysqlclient)
            # but allow a fallback on MariaDB
            pkg_check_modules(mysqlclient libmariadb IMPORTED_TARGET) # optional
        endif()
    endif()
endif()
#
#
# =======================================================================
function(online_library name)
  if ( BUILD_SHARED_LIBS )
    add_library(${name} SHARED ${ARGN})
  else()
    add_library(${name} STATIC ${ARGN})
  endif()
  target_include_directories(${name} PRIVATE include/)
  target_include_directories(${name} INTERFACE
                             $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include/>
                             $<INSTALL_INTERFACE:include/>)
  set_target_properties(${name} PROPERTIES VERSION ${Online_VERSION} SOVERSION ${Online_SOVERSION})
  install(TARGETS ${name} EXPORT Online
          LIBRARY  DESTINATION "${ONLINE_INSTALL_LIBDIR}"
          INCLUDES DESTINATION "${ONLINE_INSTALL_INCLUDEDIR}")
  if ( NOT SUPPORT_GAUDI )
    install(TARGETS ${name} LIBRARY DESTINATION ${PROJECT_BINARY_DIR}/lib)
  endif()
  add_library(Online::${name} ALIAS ${name})
endfunction(online_library)
#
#
# =======================================================================
function(online_interface_library name)
  add_library(${name} INTERFACE)
  target_include_directories(${name} INTERFACE
                             $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>)
  if(NOT "${ARGN}" STREQUAL "")
    target_link_libraries(${name} INTERFACE ${ARGN})
  endif()
  install(TARGETS ${name} EXPORT Online
          LIBRARY DESTINATION "${ONLINE_INSTALL_LIBDIR}"
          INCLUDES DESTINATION ${ONLINE_INSTALL_INCLUDEDIR})
  if ( NOT SUPPORT_GAUDI )
    install(TARGETS ${name} LIBRARY DESTINATION ${PROJECT_BINARY_DIR}/lib)
  endif()
  add_library(Online::${name} ALIAS ${name})
endfunction(online_interface_library)
#
#
# =======================================================================
function(online_executable name)
  add_executable(${name} ${ARGN})
  target_include_directories(${name} PRIVATE include/)
  add_executable(Online::${name} ALIAS ${name})
  if ( NOT SUPPORT_GAUDI )
    install(TARGETS ${name} RUNTIME DESTINATION ${PROJECT_BINARY_DIR}/bin)
  endif()
  install(TARGETS ${name} RUNTIME DESTINATION ${ONLINE_INSTALL_BINDIR})
endfunction(online_executable)
#
#
# =======================================================================
function(online_root_dictionary name)
  cmake_parse_arguments(ARG "" "" "SOURCES;OPTIONS;USES;DEFINITIONS;INCLUDES;NO_LIBRARY;MAKE_LIBRARY" ${ARGN} )
  set(inc_dirs ${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_SOURCE_DIR}/include)
  foreach(inc ${ARG_INCLUDES})
    LIST(APPEND inc_dirs ${inc})
  endforeach()

  set(comp_defs)
  foreach(def ${ARG_DEFINITIONS})
    LIST(APPEND comp_defs ${def})
  endforeach()

  foreach(DEP ${ARG_USES})
    # Get INCLUDE DIRECTORIES from Dependencies
    LIST(APPEND inc_dirs $<TARGET_PROPERTY:${DEP},INTERFACE_INCLUDE_DIRECTORIES>)
    # Get COMPILE DEFINITIONS from Dependencies
    LIST(APPEND comp_defs $<TARGET_PROPERTY:${DEP},INTERFACE_COMPILE_DEFINITIONS>)
  endforeach()
  set(prefix "${PROJECT_BINARY_DIR}/run")
  add_custom_command(OUTPUT  ${name}.cpp ${name}_rdict.pcm
    COMMAND ${prefix} ${ROOT_rootcling_CMD} -f ${name}.cpp
                 -rmf=${CMAKE_CURRENT_BINARY_DIR}/${name}.rootmap
                 -inlineInputHeader ${ARG_OPTIONS} ${ARG_INCLUDES}
                "$<$<BOOL:$<JOIN:${comp_defs},>>:-D$<JOIN:${comp_defs},;-D>>"
                "$<$<BOOL:$<JOIN:${inc_dirs},>>:-I$<JOIN:${inc_dirs},;-I>>"
                "$<JOIN:${headers},;>"
                 ${CMAKE_BINARY_DIR}/include/LinkDef.h
    DEPENDS ${headers}
    COMMAND_EXPAND_LISTS
  )
  add_custom_target(${name}-gen ALL DEPENDS ${name}.cpp ${name}_rdict.pcm)
  set_source_files_properties(${name}.cpp ${name}_rdict.pcm
                    PROPERTIES GENERATED TRUE
		    INCLUDE_DIRECTORIES  ${CMAKE_CURRENT_SOURCE_DIR}
                    COMPILE_FLAGS "-Wno-unused-function -Wno-overlength-strings -Wno-uninitialized")
  install(FILES
          ${CMAKE_CURRENT_BINARY_DIR}/${name}_rdict.pcm
          ${CMAKE_CURRENT_BINARY_DIR}/${name}.rootmap
          DESTINATION "${ONLINE_INSTALL_LIBDIR}")
  if( ${ARG_MAKE_LIBRARY} )
    online_library(${name} ${ARG_SOURCES} ${name}.cpp)
    #target_compile_definitions(${name} "$<$<BOOL:$<JOIN:${comp_defs},>>:-D$<JOIN:${comp_defs},;-D>>")
    #target_include_directories(${name} "$<$<BOOL:$<JOIN:${inc_dirs},>>:-D$<JOIN:${inc_dirs},;-D>>")
    target_link_libraries(${name}  PRIVATE ${ARG_USES} ROOT::Core)
    set_target_properties(${name}  PROPERTIES VERSION ${Online_VERSION} SOVERSION ${Online_SOVERSION})
  endif()
endfunction(online_root_dictionary)
#
#
# =======================================================================
function(online_reflex_dictionary name)
  cmake_parse_arguments(ARG ""
        "SELECTION"
        "INCLUDES;DEFINITIONS;OPTIONS;USES"
        ${ARGN} )
  set(inc_dirs ${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_SOURCE_DIR}/include)
  foreach(inc ${ARG_INCLUDES})
    LIST(APPEND inc_dirs ${inc})
  endforeach()

  set(comp_defs)
  foreach(def ${ARG_DEFINITIONS})
    LIST(APPEND comp_defs ${def})
  endforeach()

  foreach(DEP ${ARG_USES})
    # Get INCLUDE DIRECTORIES from Dependencies
    LIST(APPEND inc_dirs $<TARGET_PROPERTY:${DEP},INTERFACE_INCLUDE_DIRECTORIES>)
    # Get COMPILE DEFINITIONS from Dependencies
    LIST(APPEND comp_defs $<TARGET_PROPERTY:${DEP},INTERFACE_COMPILE_DEFINITIONS>)
  endforeach()

  # Generate sources of the dictionary
  set(dictname    ${name}Gen)
  set(gensrcdict  ${CMAKE_CURRENT_BINARY_DIR}/${dictname}.cxx)
  set(rootmapname ${CMAKE_CURRENT_BINARY_DIR}/${dictname}.rootmap)
  set(pcmfile     ${CMAKE_CURRENT_BINARY_DIR}/${dictname}_rdict.pcm)
  if(TARGET Python::Interpreter)
    # Ninja requires the DEPFILE to mention the relative path to the target file
    # (but the relative to what depends on the version of CMake) otherwise the dictionary
    # is always rebuilt.
    if(POLICY CMP0116)
      cmake_policy(PUSH)
      cmake_policy(SET CMP0116 NEW)
      file(RELATIVE_PATH dep_target "${CMAKE_CURRENT_BINARY_DIR}" "${gensrcdict}")
    else()
      file(RELATIVE_PATH dep_target "${CMAKE_BINARY_DIR}" "${gensrcdict}")
    endif()
    set(prefix "${PROJECT_BINARY_DIR}/run")
    add_custom_command(OUTPUT ${gensrcdict} ${rootmapname} ${pcmfile}
       COMMAND ${prefix} ${ROOT_genreflex_CMD} # comes from ROOTConfig.cmake
            ${ARG_INCLUDES}
            -o ${gensrcdict} --rootmap=${rootmapname} --rootmap-lib=lib${dictname}
            --select=${ARG_SELECTION} -std=c++${CMAKE_CXX_STANDARD}
            "$<$<BOOL:$<JOIN:${comp_defs},>>:-D$<JOIN:${comp_defs},;-D>>"
            "$<$<BOOL:$<JOIN:${inc_dirs},>>:-I$<JOIN:${inc_dirs},;-I>>"
            ${ARG_OPTIONS}
       COMMAND_EXPAND_LISTS)
    if(POLICY CMP0116)
      cmake_policy(POP)
    endif()
    add_custom_target(${dictname}-gen ALL DEPENDS "${gensrcdict};${rootmapname};${pcmfile}")
    set_source_files_properties(${gensrcdict} ${pcmfile}
                    PROPERTIES GENERATED TRUE
		    INCLUDE_DIRECTORIES  ${CMAKE_CURRENT_SOURCE_DIR}
                    COMPILE_FLAGS "-Wno-unused-function -Wno-overlength-strings")
    install(FILES ${pcmfile} ${rootmapname} DESTINATION "${ONLINE_INSTALL_LIBDIR}")
  endif()
  online_library(${name} ${dictname}.cxx)
  target_compile_options(${name} PRIVATE -Wno-uninitialized)
  target_link_libraries(${name}  ${ARG_USES})
endfunction(online_reflex_dictionary)
#
#
# =======================================================================
function(online_module name)
  if ( NOT BUILD_SHARED_LIBS )
    message(STATUS "++ Building Module as static lib: ${name}")
    online_library(${name} "${ARGN}")
    return()
  endif()
  add_library(${name} MODULE "${ARGN}")
  target_include_directories(${name} PRIVATE include/)
  set_target_properties(${name} PROPERTIES VERSION ${Online_VERSION} SOVERSION ${Online_SOVERSION})
  install(TARGETS ${name} EXPORT Online
          LIBRARY DESTINATION "${ONLINE_INSTALL_LIBDIR}"
          INCLUDES DESTINATION ${ONLINE_INSTALL_INCLUDEDIR})
  add_library(Online::${name} ALIAS ${name})
  get_property(dir_path TARGET Gaudi::GaudiPluginService PROPERTY DIR_PATH)
  set(prefix "${PROJECT_BINARY_DIR}/run")
  add_custom_command(TARGET ${name} POST_BUILD
     BYPRODUCTS ${name}.components
     COMMAND ${prefix} $<TARGET_FILE:Gaudi::listcomponents>
                 --output ${name}.components $<TARGET_FILE_NAME:${name}>
     WORKING_DIRECTORY "${LIBRARY_OUTPUT_PATH}"
  )
  if ( SUPPORT_GAUDI )
    # To append the path to the generated library to LD_LIBRARY_PATH with run
    _gaudi_runtime_prepend(ld_library_path $<TARGET_FILE_DIR:${name}>)
  endif()
  install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${name}.components DESTINATION "${ONLINE_INSTALL_LIBDIR}")
  if ( NOT SUPPORT_GAUDI )
    install(TARGETS ${name} LIBRARY DESTINATION ${PROJECT_BINARY_DIR}/lib)
    install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${name}.components DESTINATION "${PROJECT_BINARY_DIR}/lib")
  endif()
endfunction(online_module)
#
#
# =======================================================================
function(online_python_module name module)
  Python_add_library(${name} MODULE ${ARGN})
  target_link_libraries(${name} PRIVATE Python::Python Threads::Threads Online::rt Online::dl)
  target_include_directories(${name} PRIVATE include)
  set_target_properties(${name} PROPERTIES VERSION ${Online_VERSION} SOVERSION ${Online_SOVERSION})
  install(TARGETS ${name} LIBRARY DESTINATION "${ONLINE_INSTALL_PYTHONDIR}/${module}")
endfunction(online_python_module)
#
#
# =======================================================================
function(online_install_includes directory)
  install(DIRECTORY ${directory}/ DESTINATION "${ONLINE_INSTALL_INCLUDEDIR}")
endfunction(online_install_includes)
#
#
# =======================================================================
function(online_install_python directory)
  install(DIRECTORY ${directory}/ DESTINATION "${ONLINE_INSTALL_PYTHONDIR}")
endfunction(online_install_python)
#
#
# =======================================================================
function(online_install_scripts directory)
  install(DIRECTORY ${directory}/ DESTINATION TYPE BIN
          FILE_PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE
                           GROUP_READ GROUP_WRITE GROUP_EXECUTE
                           WORLD_READ WORLD_EXECUTE)
  if ( NOT SUPPORT_GAUDI )
    install(DIRECTORY ${directory}/ DESTINATION ${PROJECT_BINARY_DIR}/bin
          FILE_PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE
                           GROUP_READ GROUP_WRITE GROUP_EXECUTE
                           WORLD_READ WORLD_EXECUTE)
  endif()
endfunction(online_install_scripts)
#
#
# =======================================================================
# Generate LinkDef.h
function(online_generate_default_linkdef)
  if (WITH_Online_PRIVATE_DEPENDENCIES)
    if (NOT EXISTS ${CMAKE_BINARY_DIR}/include/LinkDef.h)
      file(GENERATE OUTPUT ${CMAKE_BINARY_DIR}/include/LinkDef.h CONTENT
"//==========================================================================
//  LHCb Online software suite
//--------------------------------------------------------------------------
// Copyright (C) Organisation europeenne pour la Recherche nucleaire (CERN)
// All rights reserved.
//
// For the licensing terms see OnlineSys/LICENSE.
//
// Author     : M.Frank
//
//==========================================================================
// Dummy LinkDef file to please rootcling when creating dictionaries
//==========================================================================
#ifndef LINKDEF_H
#define LINKDEF_H
#endif // LINKDEF_H
")
    endif()
  endif()
endfunction(online_generate_default_linkdef)
#
#
# =======================================================================
macro(online_apply_numa_defs component)
  if (numa_FOUND)
    message("-- +++ ${component}: numa found: ${numa_FOUND} Building numa control component.")
    target_compile_definitions(${component} PRIVATE HAVE_NUMA=100)
  else()
    message("-- +++ ${component}: warning: numa NOT found. NOT Building numa control component.")
  endif()
endmacro(online_apply_numa_defs)
#
