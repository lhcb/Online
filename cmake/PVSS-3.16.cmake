
set (WCCOA_installation     "/group/online/dataflow/cmtuser/libraries/WinCC_OA/3.16")
set (WCCOA_major_version    "V316")
set (WCCOA_native_version   "${WCCOA_major_version}")
set (WCCOA_api              "${WCCOA_installation}/api")
set (WCCOA_inc              "${WCCOA_api}/include")

set (ENV{PVSS_II}             /localdisk/pvss/STORAGE/config/config)
set (ENV{WCCOA_II}            /localdisk/pvss/STORAGE/config/config)

set (ENV{PVSS_II_HOME}             "${WCCOA_installation}")
set (ENV{WCCOA_II_HOME}            "${WCCOA_installation}")

set (ENV{PVSS_SYSTEM_ROOT}         "${WCCOA_installation}")
set (ENV{WCCOA_SYSTEM_ROOT}        "${WCCOA_installation}")

set (ENV{PROJECT_ROOT}             ".")
set (ENV{PLATFORM}                 "linux_RHEL4")

set (WCCOA_includes "${WCCOA_inc}/Basics/DpBasics" "${WCCOA_inc}/Basics/Utilities" "${WCCOA_inc}/Basics/Variables" "${WCCOA_inc}/Basics/NoPosix" "${WCCOA_inc}/Ctrl" "${WCCOA_inc}/BCMNew" "${WCCOA_inc}/Configs" "${WCCOA_inc}/Datapoint" "${WCCOA_inc}/Messages" "${WCCOA_inc}/Manager")

macro (add_wccoa_definitions lib)
  target_compile_definitions(${lib} PRIVATE -DLINUX2_6)
  target_compile_definitions(${lib} PRIVATE -DGLIBC2_3)
  target_compile_definitions(${lib} PRIVATE -D__PC)
  target_compile_definitions(${lib} PRIVATE -D__UNIX)
  target_compile_definitions(${lib} PRIVATE -DHAS_TEMPLATES=1)
  target_compile_definitions(${lib} PRIVATE -DBC_DESTRUCTOR)
  target_compile_definitions(${lib} PRIVATE -Dbcm_boolean_h)
  target_compile_definitions(${lib} PRIVATE -DOS_LINUX)
  target_compile_definitions(${lib} PRIVATE -DLINUX)
  target_compile_definitions(${lib} PRIVATE -D__STDC_LIMIT_MACROS)
  target_compile_definitions(${lib} PRIVATE -D__STDC_FORMAT_MACROS)
  target_compile_definitions(${lib} PRIVATE -DDLLEXP_BASICS=)
  target_compile_definitions(${lib} PRIVATE -DDLLEXP_DATAPOINT=)
  target_compile_definitions(${lib} PRIVATE -DDLLEXP_CONFIGS=)
  target_compile_definitions(${lib} PRIVATE -DDLLEXP_MANAGER=)
  target_compile_definitions(${lib} PRIVATE -DDLLEXP_MESSAGES=)
  target_compile_definitions(${lib} PRIVATE -DDLLEXP_CTRL=)
  target_compile_definitions(${lib} PRIVATE -DDLLEXP_BCM=)
  target_compile_definitions(${lib} PRIVATE -DGCC3)
  target_compile_definitions(${lib} PRIVATE -DPVSS_PLATFORM="linux_RHEL4")
  target_compile_definitions(${lib} PRIVATE -DPVSS_VERS_DLL="${WCCOA_major_version}")
endmacro()

set (WCCOA_LIBS "-L${WCCOA_installation}/bin/libwk.so -L${WCCOA_installation}/api/lib.linux -lManager${WCCOA_native_version} -lMessages${WCCOA_native_version} -lDatapoint${WCCOA_native_version} -lBasics${WCCOA_native_version} -lbcm${WCCOA_native_version} -lConfigs${WCCOA_native_version} -lace${WCCOA_native_version} -lQt5Core -lQt5Network -lwk -lstdc++ -lm -lc -lpthread -lrt -ldl -Wl,--as-needed")

