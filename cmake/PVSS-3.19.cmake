set (WCCOA_installation     "/group/online/dataflow/cmtuser/libraries/WinCC_OA/3.19")
set(API_ROOOT  "${WCCOA_installation}/api")

set(CMAKE_USER_MAKE_RULES_OVERRIDE ${API_ROOT}/CMakeOverrides.txt)
set(WCCOA_VERSION_MAJOR 3)
set(WCCOA_VERSION_MINOR 19)
set(WCCOA_VERSION_DISP  3.19)
set(WCCOA_LIB_PREFIX "")
set(WCCOA_LIB_INFIX V${WCCOA_VERSION_MAJOR}${WCCOA_VERSION_MINOR})
math(EXPR WCCOA_VERS "${WCCOA_VERSION_MAJOR} * 100000 +
                      ${WCCOA_VERSION_MINOR} * 1000"
)

set (WCCOA_native_version   "${WCCOA_major_version}")
set (WCCOA_api              "${WCCOA_installation}/api")
set (WCCOA_inc              "${WCCOA_api}/include")

set (ENV{PVSS_II}             /localdisk/pvss/STORAGE/config/config)
set (ENV{WCCOA_II}            /localdisk/pvss/STORAGE/config/config)

set (ENV{PVSS_II_HOME}             "${WCCOA_installation}")
set (ENV{WCCOA_II_HOME}            "${WCCOA_installation}")

set (ENV{PVSS_SYSTEM_ROOT}         "${WCCOA_installation}")
set (ENV{WCCOA_SYSTEM_ROOT}        "${WCCOA_installation}")

set (ENV{PROJECT_ROOT}             ".")
set (ENV{PLATFORM}                 "linux_RHEL4")

set (WCCOA_includes ${WCCOA_api}/include/Basics/BCM
    ${WCCOA_api}/include/Basics/Accu
    ${WCCOA_api}/include/Basics/Variables
    ${WCCOA_api}/include/Basics/Utilities
    ${WCCOA_api}/include/Basics/NoPosix
    ${WCCOA_api}/include/Basics/DpBasics
    ${WCCOA_api}/include/Basics/Crypto
    ${WCCOA_api}/include/Datapoint
    ${WCCOA_api}/include/Messages
    ${WCCOA_api}/include/Manager
    ${WCCOA_api}/include/Ctrl
    ${WCCOA_api}/include/V24
    ${WCCOA_api}/include/ComDrv
    ${WCCOA_api}/include/Configs
    ${WCCOA_api}/include/Configs/DrvConfigs/DrvCommon
    ${WCCOA_api}/include/Configs/DrvConfigs/ConvSmooth)

macro (add_wccoa_definitions lib)
  target_compile_definitions(${lib} PUBLIC -DDLLEXP_BASICS=
    -DDLLEXP_CONFIGS=
    -DDLLEXP_DATAPOINT=
    -DDLLEXP_MESSAGES=
    -DDLLEXP_MANAGER=
    -DDLLEXP_CTRL=-DPVI_VERSION_X=${WCCOA_VERSION_MAJOR}
    -DPVI_VERSION_Y=${WCCOA_VERSION_MINOR}
    -DPVI_VERSION_Z=0
    -DPVI_PATCH_NUM=0
    -DPVSS_VERSION="${WCCOA_VERSION_MAJOR}.${WCCOA_VERSION_MINOR}"
    -DPVSS_VERSION_DISP="${WCCOA_VERSION_DISP}"
    -DPVSS_PATCH=""
    -DPVSS_VERS=${WCCOA_VERS}
    -DPVSS_VERS_DLL="${WCCOA_VERS}"
    -DPVSS_FD_SETSIZE=8192
    -DPVSS_PLATFORM="${CMAKE_SYSTEM_NAME} ${CMAKE_SYSTEM_PROCESSOR}"
    -DUSE_OWN_ALLOCATOR
    -DOS_LINUX
    -D__STDC_LIMIT_MACROS
    -D__STDC_FORMAT_MACROS)
endmacro()

set (WCCOA_LIBS "-L${WCCOA_installation}/bin/libwk.so -L${WCCOA_installation}/api/lib.linux -L${WCCOA_installation}/bin -lManager${WCCOA_LIB_INFIX} -lMessages${WCCOA_LIB_INFIX} -lDatapoint${WCCOA_LIB_INFIX} -lBasics${WCCOA_LIB_INFIX} -lConfigs${WCCOA_LIB_INFIX} -lace${WCCOA_LIB_INFIX} -lQt5Core -lQt5Network -lstdc++ -lm -lc -lpthread -lrt -ldl -Wl,--as-needed")

