##find_package(rdkafka REQUIRED)
set(RDKAFKA_INCLUDE_DIR)
FIND_PATH(RDKAFKA_INCLUDE_DIR rdkafka.h
  /usr/local/include
  /usr/include
  /usr/include/rdkafka
  $ENV{RDKAFKA_DIR}/include/librdkafka
)

FIND_LIBRARY(RDKAFKA_LIBRARY rdkafka
  /usr/local/lib
  /usr/lib
  $ENV{RDKAFKA_DIR}/lib
)

FIND_LIBRARY(RDKAFKA_CPP_LIBRARY rdkafka++
  /usr/local/lib
  /usr/lib
  $ENV{RDKAFKA_DIR}/lib
)

IF(RDKAFKA_INCLUDE_DIR)
  ADD_DEFINITIONS(-DHAVE_RDKAFKA)
  IF(RDKAFKA_LIBRARY)
    set( RDKAFKA_LIBRARIES ${RDKAFKA_LIBRARY} ${RDKAFKA_CPP_LIBRARY})
    set( RDKAFKA_FOUND "YES" )
    add_definitions(-DHAVE_RDKAFKA)
  ENDIF(RDKAFKA_LIBRARY)
ENDIF(RDKAFKA_INCLUDE_DIR)

MARK_AS_ADVANCED( RDKAFKA_INCLUDE_DIR RDKAFKA_LIBRARY )

if (RDKAFKA_FOUND)
  message(STATUS "+++ Dataflow: RDKAFKA was found. The RDKAFKA components shall be built." )
  message(STATUS "+++           RDKAFKA_INCLUDE_DIR: ${RDKAFKA_INCLUDE_DIR}" )
  message(STATUS "+++           RDKAFKA_LIBRARIES:   ${RDKAFKA_LIBRARIES}" )
else()
  message(STATUS "+++ ${lib}: RDKAFKA was NOT found. The RDKAFKA components shall NOT be built!" )
endif()
