#!/bin/bash
# Source this script to set up the Online installation that this script is part of.
#
# This script if for the csh like shells, see thisonline.csh for csh like shells.
#
# Author: Pere Mato. F. Gaede, M.Frank
#
#   @CMAKE_PROJECT_NAME@
#   @CMAKE_PROJECT_NAME@_ROOT          : path to this @CMAKE_PROJECT_NAME@ installation
#   @CMAKE_PROJECT_NAME@_LIBRARIES     : list of @CMAKE_PROJECT_NAME@ libraries
#   @CMAKE_PROJECT_NAME@_INCLUDE_DIRS  : list of paths to be used with INCLUDE_DIRECTORIES
#   @CMAKE_PROJECT_NAME@_VERSION       : package version
#-------------------------------------------------------------------------------
#
online_parse_this()   {
    SOURCE=${1}
    package=${2};
    if [ "x${SOURCE}" = "x" ]; then
        if [ -f bin/this${package}.sh ]; then
            THIS="$PWD"; export THIS
        elif [ -f ./this${package}.sh ]; then
            THIS=$(cd ..  > /dev/null; pwd); export THIS
        else
            echo ERROR: must "cd where/${package}/is" before calling ". bin/this${package}.sh" for this version of bash!
            THIS=; export THIS
            return 1
        fi
    else
        # get param to "."
        thisroot=$(dirname ${SOURCE})
        THIS=$(cd ${thisroot}/.. > /dev/null;pwd); export THIS
    fi
    unset SOURCE package thisroot
}
#-----------------------------------------------------------------------------
online_add_path()   {
    path_name=${1}
    path_prefix=${2}
    eval path_value=\$$path_name
    # Prevent duplicates
    path_value=`echo ${path_value} | tr : '\n' | grep -v "${path_prefix}" | tr '\n' : | sed 's|:$||'`
    path_value="${path_prefix}${path_value:+:${path_value}}"
    eval export ${path_name}='${path_value}'
    unset path_name path_prefix path_value
}
#-----------------------------------------------------------------------------
online_add_library_path()    {
    p=${1};
    if [ @APPLE@ ]; then
        # Do not prepend system library locations on macOS. Stuff will break.
        [[ "$p" = "/usr/lib" || "$p" = "/usr/local/lib" ]] && unset p && return
        online_add_path DYLD_LIBRARY_PATH     "$p"
        online_add_path ONLINE_LIBRARY_PATH   "$p"
    else
        online_add_path LD_LIBRARY_PATH       "$p"
    fi
    unset p
}
#-----------------------------------------------------------------------------
#
SOURCE=${BASH_ARGV[0]}
if [ "x$SOURCE" = "x" ]; then
    SOURCE=${(%):-%N} # for zsh
fi
#
online_parse_this $SOURCE  online;
#
#----Online installation directory--------------------------------------------
export OnlineINSTALL=${THIS};
export Online_DIR=${THIS};
export Online_ROOT=${THIS};
export Online_VERSION=@Online_VERSION@;
#
#----------- source the LCG environment first --------------------------------
export ONLINE_COMPILER_LOCATION=@ONLINE_COMPILER_LOCATION@;
if test -z "${ONLINE_COMPILER_LOCATION}"; then
  echo "No ONLINE_COMPILER_LOCATION defined! Setup will not work!";
fi;
if [ -e ${ONLINE_COMPILER_LOCATION}/setup.sh ]; then
  . ${ONLINE_COMPILER_LOCATION}/setup.sh;
fi;
#
#-----------------------------------------------------------------------------
export ROOTSYS=@ONLINE_ROOT_SYS@;
if [ -n "@ONLINE_LCG_VIEW_SETUP@" ]; then
  . @ONLINE_LCG_VIEW_SETUP@;
else
  if test ! -f ${ROOTSYS}/bin/thisroot.sh; then
    echo "No ROOT setup found. This installation setup will not work!";
  fi;
  . ${ROOTSYS}/bin/thisroot.sh;
fi;
#-----------------------------------------------------------------------------
# Online run-time environment
#
@ONLINE_RUNTIME@
#
#----PATH---------------------------------------------------------------------
online_add_path PATH       ${ONLINE_RUNTIME_PATH};
online_add_path PATH       ${THIS}/@ONLINE_INSTALL_BINDIR@;
#----LIBRARY_PATH-------------------------------------------------------------
online_add_library_path    ${ONLINE_LIBRARY_PATH};
online_add_library_path    ${THIS}/@ONLINE_INSTALL_LIBDIR@;
#----PYTHONPATH---------------------------------------------------------------
online_add_path PYTHONPATH ${THIS}/@ONLINE_INSTALL_PYTHONDIR@;
#----ROOT_INCLUDE_PATH--------------------------------------------------------
online_add_path ROOT_INCLUDE_PATH ${THIS}/@ONLINE_INSTALL_INCLUDEDIR@;
#-----------------------------------------------------------------------------
if [ @APPLE@ ];
then
    export ONLINE_LIBRARY_PATH=${DYLD_LIBRARY_PATH};
else
    export ONLINE_LIBRARY_PATH=${LD_LIBRARY_PATH};
fi;
#-----------------------------------------------------------------------------
#
unset ROOTENV_INIT;
unset THIS;
unset SOURCE;
#-----------------------------------------------------------------------------
